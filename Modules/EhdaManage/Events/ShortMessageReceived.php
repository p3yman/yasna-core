<?php namespace Modules\EhdaManage\Events;

use Modules\Yasna\Services\YasnaEvent;

class ShortMessageReceived extends YasnaEvent
{
    public $model_name = 'short-message';
}
