<?php namespace Modules\EhdaManage\Events;

use Modules\Yasna\Services\YasnaEvent;

class NewCardRegistered extends YasnaEvent
{
    public $model_name = 'User';
}
