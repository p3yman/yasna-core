<?php namespace Modules\EhdaManage\Events;

use Modules\Yasna\Services\YasnaEvent;

class TooskaCreatedUserPending extends YasnaEvent
{
    public $model_name = "UserPending";
}
