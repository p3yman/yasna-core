<?php namespace Modules\EhdaManage\Events;

use Modules\Yasna\Services\YasnaEvent;

class TooskaUpdatedUserPending extends YasnaEvent
{
    public $model_name = "UserPending";
}
