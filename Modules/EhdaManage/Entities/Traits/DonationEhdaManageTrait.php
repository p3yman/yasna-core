<?php namespace Modules\EhdaManage\Entities\Traits;

trait DonationEhdaManageTrait
{
    public function ElectorDateMin($value)
    {
        $this->elector_query->whereDate('created_at', '>=', $value);
    }

    public function ElectorDateMax($value)
    {
        $this->elector_query->whereDate('created_at', '=<', $value);
    }
}
