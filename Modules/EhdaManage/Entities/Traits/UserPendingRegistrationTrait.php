<?php namespace Modules\EhdaManage\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserPendingRegistrationTrait
 *
 * @property $user_registered_at
 * @package Modules\EhdaManage\Entities\Traits
 */
trait UserPendingRegistrationTrait
{
    /**
     * @return bool
     */
    public function markAsRegistered()
    {
        return $this->statusUpdate([
             "deleted_at"         => $this->now(),
             "user_registered_at" => $this->now(),
        ]);
    }



    /**
     * @return mixed
     */
    public function markAsUnregistered()
    {
        return $this->statusUpdate([
             "deleted_at"         => null,
             "user_registered_at" => null,
        ]);
    }



    /**
     * @return bool
     */
    public function isRegistered()
    {
        return boolval($this->user_registered_at);
    }



    /**
     * @return bool
     */
    public function isNotRegistered()
    {
        return !$this->isRegistered();
    }



    /**
     * @return bool
     */
    public function shouldRegister()
    {
        chalk()->add("isOfflineVerified on listener " . $this->isOfflineVerified() . " #" . $this->id);
        chalk()->add("isNotRegistered on listener " . $this->isNotRegistered() . " #" . $this->id);

        return $this->isOfflineVerified() and $this->isNotRegistered();
    }



    /**
     * @return bool
     */
    public function shouldNotRegister()
    {
        return !$this->shouldRegister();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function registrableRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('offline_verified_at')->whereNull('user_registered_at');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRegistrableRecordsAttribute()
    {
        return $this->registrableRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function registeredRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->withTrashed()->whereNotNull('user_registered_at');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRegisteredRecordsAttribute()
    {
        return $this->registrableRecords()->get();
    }
}
