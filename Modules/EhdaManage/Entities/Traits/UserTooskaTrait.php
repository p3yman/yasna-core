<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 5/29/18
 * Time: 8:17 PM
 */

namespace Modules\EhdaManage\Entities\Traits;

use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;

trait UserTooskaTrait
{
    /**
     * generate accessor for 1020 state message
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getCardExistsMessageAttribute()
    {
        return trans('ehdamanage::tooska-api.1020_full', [
             'gender' => trans()->has($trans_path = "ehdamanage::people.gender_prefix.$this->gender") ? trans($trans_path) : "",
             'name'   => $this->full_name,
             'number' => $this->card_no,
             'card'   => FanapServiceProvider::url($this->card_short_url),
             'url'    => FanapServiceProvider::url(url('/')),
        ]);
    }



    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function followUpMessage()
    {
        return trans('ehdamanage::tooska-api.follow_up_message', [
             'number' => $this->card_no,
             'url'    => FanapServiceProvider::url(url('/')),
        ]);
    }
}
