<?php namespace Modules\EhdaManage\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class UserPendingOnlineTrait
 *
 * @property $online_requested_at
 * @property $online_verified_at
 * @property $online_rejected_at
 * @package Modules\EhdaManage\Entities\Traits
 */
trait UserPendingOnlineTrait
{


    /**
     * @return bool
     */
    public function markAsOnlineRequested()
    {
        return $this->statusUpdate([
             "online_requested_at" => $this->now(),
             "online_verified_at"  => null,
             "online_rejected_at"  => null,
        ]);
    }



    /**
     * @return bool
     */
    public function markAsOnlineVerified()
    {
        return $this->statusUpdate([
             "online_verified_at" => $this->now(),
             "online_rejected_at" => null,
        ]);
    }



    /**
     * @return bool
     */
    public function markAsOnlineRejected()
    {
        return $this->statusUpdate([
             "online_verified_at" => null,
             "online_rejected_at" => $this->now(),
        ]);
    }



    /**
     * @return bool
     */
    public function isOnlineRequested()
    {
        return boolval($this->online_requested_at);
    }



    /**
     * @return bool
     */
    public function isNotOnlineRequested()
    {
        return !$this->isOnlineRequested();
    }



    /**
     * @return bool
     */
    public function isOnlineVerified()
    {
        return boolval($this->online_verified_at);
    }



    /**
     * @return bool
     */
    public function isNotOnlineVerified()
    {
        return !$this->isOnlineVerified();
    }



    /**
     * @return bool
     */
    public function isOnlineRejected()
    {
        return boolval($this->online_rejected_at);
    }



    /**
     * @return bool
     */
    public function isNotOnlineRejected()
    {
        return !$this->isOnlineRejected();
    }



    /**
     * @return bool
     */
    public function isOnlineWaiting()
    {
        return $this->isOnlineRequested() and $this->isNotOnlineRejected() and $this->isNotOnlineVerified();
    }



    /**
     * @return bool
     */
    public function isNotOnlineWaiting()
    {
        return !$this->isOnlineWaiting();
    }



    /**
     * @return bool
     */
    public function isOnlinePending()
    {
        return $this->isNotOnlineRequested();
    }



    /**
     * @return bool
     */
    public function isNotOnlinePending()
    {
        return !$this->isOnlinePending();
    }



    /**
     * @param string $channel
     * @param string $channel
     *
     * @return Builder
     */
    public function onlineRequestedRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('online_requested_at');
    }



    /**
     * @return Collection
     */
    public function getOnlineRequestedRecordsAttribute()
    {
        return $this->onlineRequestedRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function onlineVerifiedRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('online_verified_at');
    }



    /**
     * @return Collection
     */
    public function getOnlineVerifiedRecordsAttribute()
    {
        return $this->onlineVerifiedRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function onlineRejectedRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('online_rejected_at');
    }



    /**
     * @return Collection
     */
    public function getOnlineRejectedRecordsAttribute()
    {
        return $this->onlineRejectedRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function onlineWaitingRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNull('online_requested_at');
    }



    /**
     * @return Collection
     */
    public function getOnlineWaitingRecordsAttribute()
    {
        return $this->onlineWaitingRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function onlinePendingRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()
                    ->whereNotNull('online_requested_at')
                    ->whereNull('online_verified_at')
                    ->whereNull('online_rejected_at')
             ;
    }



    /**
     * @return Collection
     */
    public function getOnlinePendingRecordsAttribute()
    {
        return $this->onlinePendingRecords()->get();
    }



    /**
     * @return string
     */
    public function onlineStatus()
    {
        if ($this->isOnlinePending()) {
            return "pending";
        }

        if ($this->isOnlineWaiting()) {
            return "requested";
        }

        if ($this->isOnlineVerified()) {
            return "verified";
        }

        if ($this->isOnlineRejected()) {
            return "rejected";
        }

        return "unknown";
    }



    /**
     * @return string
     */
    public function getOnlineStatusAttribute()
    {
        return $this->onlineStatus();
    }
}
