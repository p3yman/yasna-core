<?php

namespace Modules\EhdaManage\Entities\Traits;

use Carbon\Carbon;
use App\Models\User;
use Morilog\Jalali\jDate;

/**
 * Class UserEhdaManageTrait
 *
 * @package Modules\EhdaManage\Entities\Traits
 * @property $from_domain
 */
trait UserEhdaManageTrait
{


    /**
     * @param $value
     */
    public function electorAfterDateRegister($value)
    {
        $value = Carbon::parse($value)->startOfDay()->toDateTimeString();
        $this->elector()->where('created_at', '>=', $value);
    }



    /**
     * @param $value
     */
    public function electorBeforeDateRegister($value)
    {
        $value = Carbon::parse($value)->endOfDay()->toDateTimeString();
        $this->elector()->where('created_at', '<=', $value);
    }



    /**
     * @param $value
     */
    public function electorAfterUsersCardRegister($value)
    {
        $value = Carbon::parse($value)->startOfDay()->toDateTimeString();
        $this->elector()->where('card_registered_at', '>=', $value);
    }



    /**
     * @param $value
     */
    public function electorBeforeUsersCardRegister($value)
    {
        $value = Carbon::parse($value)->endOfDay()->toDateTimeString();
        $this->elector()->where('card_registered_at', '<=', $value);
    }



    /**
     * @param $value
     */
    public function electorHomeProvince($value)
    {
        $this->elector()->where('home_province', $value);
    }



    /**
     * @param $value
     */
    public function electorHomeCity($value)
    {
        $this->elector()->where('home_city', $value);
    }



    /**
     * @param $value
     */
    public function electorWorkProvince($value)
    {
        $this->elector()->where('work_province', $value);
    }



    /**
     * @param $value
     */
    public function electorWorkCity($value)
    {
        $this->elector()->where('work_city', $value);
    }



    /**
     * @param $value
     */
    public function electorJob($value)
    {
        $this->elector()->where('job', $value);
    }



    /**
     * @param $value
     */
    public function electorEduLevel($value)
    {
        $this->elector()->where('edu_level', $value);
    }



    /**
     * @param $bool
     */
    public function electorUsersbyCard($bool)
    {
        if ($bool) {
            $this->elector()->where('card_registered_at', '<>', null);
        } else {
            $this->elector()->where('card_registered_at', null);
        }
    }



    /**
     * @param $given_role
     */
    public function electorMustRole(array $given_role)
    {
        if (!is_array($given_role) or !count($given_role)) {
            return;
        }

        foreach ($given_role as $role) {
            $this->elector()->whereHas('roles', function ($query) use ($role) {
                $query->where('roles.slug', $role);
                $query = $this->considerStatusInElectorRole($query);
                $query = $this->considerPermissionsInElectorRole($query);
                $query = $this->considerBannedOrderInElectorRole($query);
                return $query;
            })
            ;
        }
    }



    /**
     * @param $given_role
     */
    public function electorAllUsersWithCardHolders(array $given_role)
    {
        if (!is_array($given_role) or !count($given_role)) {
            return;
        }

        $this->elector()->whereHas('roles', function ($query) use ($given_role) {
            $query->where('roles.slug', 'card-holder');
            return $query;
        })
        ;

        $this->elector()->whereHas('roles', function ($query) use ($given_role) {
            $query->whereIn('roles.slug', $given_role);
            $query = $this->considerStatusInElectorRole($query);
            $query = $this->considerPermissionsInElectorRole($query);
            $query = $this->considerBannedOrderInElectorRole($query);
            return $query;
        })
        ;
    }



    /**
     * @return array
     */
    public function namesMetaFields()
    {
        return [
             'user_name_first',
             'user_name_last',
             'user_name_father',
             'user_mobile',
             'user_edu_city',
             'user_work_city',
             'user_home_city',
        ];
    }



    /**
     * @param      $username
     * @param bool $as_role
     *
     * @return User
     */
    public function finder($username, $as_role = false)
    {
        $username_field = user()->usernameField();

        if ($as_role == 'admin') {
            $as_role = role()->adminRoles();
        }

        $electors = [
             $username_field => $username,
             "role"          => $as_role,
             "banned"        => false,
        ];

        $user = user()->elector($electors)->orderBy('created_at', 'desc')->first();

        if (!$user) {
            $user = user(-1);
        }

        return $user;
    }



    /**
     * Adds code_melli to the simple edit form
     */
    public function ehdaFormItems()
    {
        $this->addFormItem('code_melli')
             ->whichIsRequired()
             ->withClass('ltr')
             ->withPurificationRule('ed')
             ->withValidationRule('code_melli')
        ;
    }



    /**
     * @return int
     */
    public function generateCardNo()
    {
        return $this->id + 5000;
    }



    /**
     * Assigns card-holder role and card No, after save
     *
     * @return bool
     */
    public function assignRoleAndCardNo()
    {
        if (!$this->exists) {
            return false;
        }

        $this->attachRole('card-holder');
        return $this->update([
             "card_no"            => $this->generateCardNo(),
             "card_registered_at" => Carbon::now()->toDateTimeString(),
        ]);
    }



    /**
     * Assigns Volunteer Roles
     *
     * @param $role_slug
     * @param $status
     *
     * @return bool
     */
    public function assignVolunteerRole($role_slug, $status)
    {
        if ($this->exists and $this->is_not_a($role_slug)) {
            return $this->attachRole($role_slug, $status);
        }

        return false;
    }



    /**
     * @return bool
     */
    public function ehdaCanEdit()
    {
        if ($this->isDeveloper()) {
            return dev();
        }

        if ($this->is_an('admin')) {
            return $this->canEdit();
            //return user()->as('admin')->can('users-volunteers.edit');
        }

        if ($this->min(1)->is_an('admin')) {
            return user()->as('admin')->can('users-card-holder.edit');
        }

        if ($this->withDisabled()->is_an('admin')) {
            return user()->as('admin')->can('users-card-holder.edit');
        }

        return user()->as('admin')->can('users-card-holder.edit');
    }



    /**
     * @return bool
     */
    public function ehdaCannotEdit()
    {
        return !$this->ehdaCanEdit();
    }



    /**
     * @return array
     */
    public function getActivitiesArrayAttribute()
    {
        return explode_not_empty(',', str_replace(' ', null, $this->activities));
    }



    /**
     * @return bool|string
     */
    public function getFromDomainNameAttribute()
    {
        if ($this->from_domain) {
            $domain = model('domain', $this->from_domain);
            if ($domain->exists) {
                return $domain->title;
            }
        }

        return false;
    }



    /**
     * @return bool
     */
    public function cardDelete()
    {
        $this->detachRole('card-holder');
        $this->update([
             "card_no"            => 0,
             "card_registered_at" => null,
        ]);

        if (!$this->rolesQuery()->count()) {
            $this->delete();
        }

        return true; //<~~ Blind Feedback!
    }



    /**
     * @return array
     */
    protected function ehdaSearchableFields()
    {
        return [
             'code_melli',
             'card_no',
        ];
    }



    /**
     * @return string
     */
    public function getBirthDateOnCardEnAttribute()
    {
        if ($this->birth_date and $this->birth_date != '0000-00-00') {
            return jDate::forge($this->birth_date)->format('Y/m/d');
        } else {
            return '-';
        }
    }



    /**
     * @return string
     */
    public function getRegisterDateOnCardEnAttribute()
    {
        if ($this->card_registered_at and $this->card_registered_at != '0000-00-00') {
            return jDate::forge($this->card_registered_at)->format('Y/m/d');
        } else {
            return '-';
        }
    }
}
