<?php namespace Modules\EhdaManage\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserPendingOfflineTrait
 *
 * @property $offline_requested_at
 * @property $offline_verified_at
 * @property $offline_rejected_at
 * @package Modules\EhdaManage\Entities\Traits
 */
trait UserPendingOfflineTrait
{


    /**
     * @return bool
     */
    public function markAsOfflineRequested()
    {
        $data = $this->dataArrayForMarkAsRequested();

        return $this->statusUpdate($data);
    }



    /**
     * @return array
     */
    public function dataArrayForMarkAsRequested()
    {
        return [
             "offline_requested_at" => $this->now(),
             "offline_verified_at"  => null,
             "offline_rejected_at"  => null,
        ];
    }



    /**
     * @param array $data
     *
     * @return bool
     */
    public function markAsOfflineVerified(array $data)
    {
        $data = array_normalize($data, [
             "name_first"  => null,
             "name_last"   => null,
             "name_father" => null,
             "gender"      => 0,
        ]);

        return $this->statusUpdate([
             "offline_verified_at" => $this->now(),
             "offline_rejected_at" => null,
             'name_first'          => $data['name_first'],
             "name_last"           => $data['name_last'],
             'name_father'         => $data['name_father'],
             'gender'              => $data['gender'],
        ]);
    }



    /**
     * @return bool
     */
    public function markAsOfflineRejected()
    {
        return $this->statusUpdate([
             "offline_verified_at" => null,
             "offline_rejected_at" => $this->now(),
        ]);
    }



    /**
     * @return bool
     */
    public function isOfflineRequested()
    {
        return boolval($this->offline_requested_at);
    }



    /**
     * @return bool
     */
    public function isNotOfflineRequested()
    {
        return !$this->isOfflineRequested();
    }



    /**
     * @return bool
     */
    public function isOfflineVerified()
    {
        return boolval($this->offline_verified_at);
    }



    /**
     * @return bool
     */
    public function isNotOfflineVerified()
    {
        return !$this->isOfflineVerified();
    }



    /**
     * @return bool
     */
    public function isOfflineRejected()
    {
        return boolval($this->offline_rejected_at);
    }



    /**
     * @return bool
     */
    public function isNotOfflineRejected()
    {
        return !$this->isOfflineRejected();
    }



    /**
     * @return bool
     */
    public function isOfflinePending()
    {
        return $this->isOnlineVerified() and $this->isNotOfflineRequested();
    }



    /**
     * @return bool
     */
    public function isNotOfflinePending()
    {
        return !$this->isOfflinePending();
    }



    /**
     * @return bool
     */
    public function isOfflineWaiting()
    {
        return $this->isOfflineRequested() and $this->isNotOfflineRejected() and $this->isNotOfflineRejected();
    }



    /**
     * @return bool
     */
    public function isNotOfflineWaiting()
    {
        return !$this->isOfflineWaiting();
    }



    /**
     * @return Builder
     */
    public function offlinePendingRecords()
    {
        return $this->newInstance()->whereNotNull('online_verified_at')->whereNull('offline_requested_at');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOfflinePendingRecordsAttribute()
    {
        return $this->offlinePendingRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function offlineRequestedRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('offline_requested_at');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOfflineRequestedRecordsAttribute()
    {
        return $this->offlineRequestedRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function offlineWaitingRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()
                    ->whereNotNull('offline_requested_at')
                    ->whereNull('offline_verified_at')
                    ->whereNull('offline_rejected_at')
             ;
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOfflineWaitingRecordsAttribute()
    {
        return $this->offlineWaitingRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function offlineVerifiedRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('offline_verified_at');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOfflineVerifiedRecordsAttribute()
    {
        return $this->offlineVerifiedRecords()->get();
    }



    /**
     * @param string $channel
     *
     * @return Builder
     */
    public function offlineRejectedRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('offline_rejected_at');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getOfflineRejectedRecordsAttribute()
    {
        return $this->offlineRejectedRecords()->get();
    }



    /**
     * @return string
     */
    public function offlineStatus()
    {
        if ($this->isOfflinePending()) {
            return 'pending';
        }

        if ($this->isOfflineWaiting()) {
            return "requested";
        }

        if ($this->isOfflineVerified()) {
            return "verified";
        }

        if ($this->isOfflineRejected()) {
            return "rejected";
        }

        return "unknown";
    }



    /**
     * @return string
     */
    public function getOfflineStatusAttribute()
    {
        return $this->offlineStatus();
    }
}
