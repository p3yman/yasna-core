<?php
namespace Modules\EhdaManage\Entities\Traits;

use Carbon\Carbon;

/**
 * Class PostEhdaManageTrait
 *
 * @package Modules\EhdaManage\Entities\Traits
 * @method elector(array $parameters)
 */
trait PostEhdaManageTrait
{
    /**
     * @return array
     */
    public function getEventDailyDetail()
    {
        if (is_null($this->registers()->orderBy('created_at')->first(['created_at']))) {
            return [];
        }
        $firstEventDay = $this->registers()->orderBy('created_at')->first(['created_at'])->created_at;
        $endEventDay   = $this->registers()->orderByDesc('created_at')->first(['created_at'])->created_at;

        $firstEventDay = Carbon::parse($firstEventDay);
        $endEventDay   = Carbon::parse($endEventDay);
        $data          = [];
        $user          = model('user');
        $print         = model('printing');
        $i             = 0;
        while (!$firstEventDay->gt($endEventDay)) {
            $data[$i]['registerUser'] = $user->where('from_event_id', $this->id)
                                             ->whereDate('created_at', $firstEventDay->toDateString())
                                             ->count()
            ;
            $data[$i]['printUser']    = $print->where('event_id', $this->id)
                                              ->whereDate('printed_at', $firstEventDay->toDateString())->count()
            ;
            $data[$i]['date']         = $firstEventDay->toDateString();
            $firstEventDay            = $firstEventDay->addDay(1);
            $i++;
        }

        return $data;
    }



    /**
     * @return mixed
     */
    public function registrableEvents()
    {
        $event_elector = [
             "type"     => "event",
             "domain"   => "auto",
             "category" => "registrable",
        ];

        return $this->elector($event_elector);
    }
}
