<?php namespace Modules\EhdaManage\Entities\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * @package Modules\EhdaManage\Entities\Traits
 * @property $birth_date
 */
trait UserPendingBirthdayTrait
{
    /**
     * @param $date_sting
     *
     * @return bool
     */
    public function addBirthDate($date_sting)
    {
        $saved = $this->batchSaveBoolean([
             "birth_date" => $date_sting,
        ]);

        if ($saved) {
            $this->fireCreationEvent();
        }

        return $saved;
    }



    /**
     * @param array $data
     *
     * @return mixed
     */
    public function addTemporaryRecord(array $data)
    {
        $data['birth_date'] = null;

        return $this->addRecord($data);
    }



    /**
     * @return bool
     */
    public function isWaitingForBirthDate()
    {
        return !boolval($this->birth_date);
    }



    /**
     * @return bool
     */
    public function isNotWaitingForBirthDate()
    {
        return !$this->isWaitingForBirthDate();
    }



    /**
     * @return bool
     */
    public function hasBirthDate()
    {
        return $this->isNotWaitingForBirthDate();
    }



    /**
     * @return bool
     */
    public function hasnotBirthDate()
    {
        return !$this->hasBirthDate();
    }



    /**
     * @param string $channel
     * @param string $channel
     *
     * @return Builder
     */
    public function birthDateRequestedRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNull('birth_date');
    }



    /**
     * @return Collection
     */
    public function getBirthDateRequestedRecordsAttribute()
    {
        return $this->birthDateRequestedRecords()->get();
    }



    /**
     * @param string $channel
     * @param string $channel
     *
     * @return Builder
     */
    public function birthDateFilledRecords($channel = null)
    {
        if ($channel) {
            $this->build()->where('channel', $channel);
        }

        return $this->build()->whereNotNull('birth_date');
    }



    /**
     * @return Collection
     */
    public function getBirthDateFilledRecordsAttribute()
    {
        return $this->birthDateFilledRecords()->get();
    }



    /**
     * @param string $channel
     */
    public function deleteOldBirthDateWaitingRecords($channel = null)
    {
        $models = $this->birthDateRequestedRecords($channel)->where('created_at', '>', Carbon::yesterday());
        $models->forceDelete();
    }
}
