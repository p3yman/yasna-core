<?php namespace Modules\EhdaManage\Entities;

use Modules\EhdaManage\Entities\Traits\UserPendingBirthdayTrait;
use Modules\EhdaManage\Entities\Traits\UserPendingOfflineTrait;
use Modules\EhdaManage\Entities\Traits\UserPendingOnlineTrait;
use Modules\EhdaManage\Entities\Traits\UserPendingRegistrationTrait;
use Modules\EhdaManage\Events\TooskaCreatedUserPending;
use Modules\EhdaManage\Events\TooskaUpdatedUserPending;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserPending
 *
 * @package Modules\EhdaManage\Entities
 * @method $this newInstance($attributes = [], $exists = 0)
 */
class Log extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];



    /**
     * @return array
     */
    public function mainMetaFields()
    {
        return [

        ];
    }
}
