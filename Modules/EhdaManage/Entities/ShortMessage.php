<?php namespace Modules\EhdaManage\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShortMessage extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];
    protected $casts   = ['response' => 'array', 'request' => 'array'];



    /**
     * @return array
     */
    public function requestMetaFields()
    {
        return ['uid'];
    }



    /**
     * @return array
     */
    public function logMetaFields()
    {
        return ['response', 'request'];
    }
}
