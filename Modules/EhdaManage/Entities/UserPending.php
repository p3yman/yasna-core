<?php namespace Modules\EhdaManage\Entities;

use Modules\EhdaManage\Entities\Traits\UserPendingBirthdayTrait;
use Modules\EhdaManage\Entities\Traits\UserPendingOfflineTrait;
use Modules\EhdaManage\Entities\Traits\UserPendingOnlineTrait;
use Modules\EhdaManage\Entities\Traits\UserPendingRegistrationTrait;
use Modules\EhdaManage\Events\TooskaCreatedUserPending;
use Modules\EhdaManage\Events\TooskaUpdatedUserPending;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UserPending
 *
 * @package Modules\EhdaManage\Entities
 * @method $this newInstance($attributes = [], $exists = 0)
 */
class UserPending extends YasnaModel
{
    use SoftDeletes;

    const DEFAULT_CHANEL = 'tooska';

    use UserPendingOnlineTrait;
    use UserPendingOfflineTrait;
    use UserPendingRegistrationTrait;
    use UserPendingBirthdayTrait;

    protected $guarded = ["id"];



    /**
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             'name_first',
             "name_last",
             'name_father',
             'gender',
             'callback_url',
        ];
    }



    /**
     * @param array $data
     *
     * @return $this
     */
    public function addRecord($data)
    {
        $data = array_normalize($data, [
             "code_melli"   => null,
             "birth_date"   => null,
             "mobile"       => null,
             "callback_url" => null,
             "channel"      => self::DEFAULT_CHANEL,
             "sid"          => null,
        ]);

        $existing_record = $this->findByCodeMelli($data['code_melli']);

        if ($existing_record->exists and $existing_record->isUnderProcess()) {
            return $this->newInstance();
        }

        $this->deleteByCodeMelli($data['code_melli']);
        $new_record = $this->newInstance()->batchSave($data);
        $new_record->fireCreationEvent();

        return $new_record;
    }



    /**
     * @param $code_melli
     */
    public function deleteByCodeMelli($code_melli)
    {
        $record = $this->findByCodeMelli($code_melli);
        if ($record->exists) {
            $record->delete();
        }
    }



    /**
     * returns status, based on the Tooska api documentation.
     * Modules\EhdaManage\Http\Api\ApiResponse
     *
     * @return string
     */
    public function apiStatus()
    {
        if ($this->not_exists) {
            return ApiResponse::REQUEST_DOES_NOT_EXIST;
        }

        if ($this->isWaitingForBirthDate()) {
            return ApiResponse::AWAITING_BIRTH_DATE;
        }

        if ($this->isRegistered()) {
            return ApiResponse::USER_ALREADY_EXIST;
        }

        if ($this->isOfflineVerified()) {
            return ApiResponse::CARD_IS_PUBLISHING;
        }

        if ($this->isOfflineWaiting()) {
            return ApiResponse::WAITING_FOR_AHVAL_OFFLINE;
        }

        if ($this->isOnlineVerified()) {
            return ApiResponse::SUCCESS_ELEMENTARY_VALIDATION;
        }

        if ($this->isOnlineRejected()) {
            return ApiResponse::FAILED_VALIDATION_SEND_AGAIN;
        }

        if ($this->isOnlineWaiting()) {
            return ApiResponse::WAITING_FOR_AHVAL_ONLINE;
        }

        return ApiResponse::REQUEST_SUCCESS_AND_SEND_TO_AHVAL;
    }



    /**
     * @return bool
     */
    public function isUnderProcess()
    {
        return !boolval($this->isOfflineVerified() or $this->isOfflineRejected());
    }



    /**
     * @return bool
     */
    public function isNotUnderProcess()
    {
        return !$this->isUnderProcess();
    }



    /**
     * @param $code_melli
     *
     * @return mixed
     */
    public function findByCodeMelli($code_melli)
    {
        return $this
             ->withTrashed()
             ->orderBy('created_at', 'desc')
             ->grab($code_melli, 'code_melli')
             ;
    }



    /**
     * Fires Creation Event
     */
    public function fireCreationEvent()
    {
        if ($this->exists and $this->hasBirthDate()) {
            event(new TooskaCreatedUserPending($this));
        }

        if ($this->exists) {
            $this->fireUpdateEvent();
        }
    }



    /**
     * Fires Updated Event
     */
    public function fireUpdateEvent()
    {
        event(new TooskaUpdatedUserPending($this));
    }



    /**
     * @param $data
     *
     * @return bool
     */
    private function statusUpdate($data)
    {
        $saved = $this->batchSaveBoolean($data);
        if ($saved) {
            $this->fireUpdateEvent();
        }

        return $saved;
    }
}
