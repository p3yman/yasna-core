<?php namespace Modules\EhdaManage\Schedules;

use Modules\Yasna\Services\YasnaSchedule;

class RegisterTooskaApprovedUsersSchedule extends YasnaSchedule
{

    /**
     * Job
     */
    protected function job()
    {
        return module("ehdaManage")->tooskaRegisterController()->registerOneApprovedUser();
    }



    /**
     * @return string
     */
    protected function frequency()
    {
        return 'everyMinute';
    }
}
