<?php namespace Modules\EhdaManage\Schedules;

use Modules\Yasna\Services\YasnaSchedule;

class DispatchAhvalOfflinePackageSchedule extends YasnaSchedule
{

    /**
     * job
     */
    protected function job()
    {
        return module("ehdaManage")->ahvalOfflineController()->sendPackage();
    }



    /**
     * @return string
     */
    protected function frequency()
    {
        return 'hourly';
    }
}
