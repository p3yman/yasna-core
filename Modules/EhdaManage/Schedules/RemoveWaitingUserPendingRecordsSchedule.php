<?php namespace Modules\EhdaManage\Schedules;

use Modules\Yasna\Services\YasnaSchedule;

class RemoveWaitingUserPendingRecordsSchedule extends YasnaSchedule
{

    /**
     * job
     */
    protected function job()
    {
        model('UserPending')->deleteOldBirthDateWaitingRecords();
    }
}
