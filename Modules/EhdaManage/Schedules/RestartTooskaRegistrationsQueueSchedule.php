<?php

namespace Modules\EhdaManage\Schedules;

use Illuminate\Support\Carbon;
use Modules\Yasna\Services\YasnaSchedule;

class RestartTooskaRegistrationsQueueSchedule extends YasnaSchedule
{
    /**
     * Dispatches creation events of all UserPending models, which somehow are stocked in the queue.
     */
    protected function job()
    {
        model('user-pending')
             ->whereNull('online_requested_at')
             ->whereNull('online_rejected_at')
             ->whereNull('user_registered_at')
             ->whereNotNull('code_melli')
             ->whereNotNull('birth_date')
             ->where('created_at', '<=', now()->subHours(2))
             ->each(function ($model, $key) {
                 $model->fireCreationEvent();
             })
        ;
    }



    /**
     * @return string
     */
    protected function frequency()
    {
        return 'hourly';
    }
}
