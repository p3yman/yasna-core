@include("ehdamanage::printings.action.template" , [
	"route" => "add-to-excel" ,
	"title" => trans_safe("ehdaManage::ehda.printings.send_to_printing") ,
	"shape" => "primary" ,
	"note" => false ,
])