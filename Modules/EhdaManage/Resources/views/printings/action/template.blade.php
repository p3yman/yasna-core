{!!
widget("modal")
	->target("url:manage/cards/printings/save/$route")
	->label($title)
!!}

<div class="modal-body">
	{!!
	widget("hidden")
		->name('ids')
	!!}

	{!!
	widget("hidden")
		->name('browse_event_id')
		->value(0)
	!!}

	{!!
	widget("input")
		->id('txtCount')
		->label("tr:validation.attributes.items")
		->disabled()
		->inForm()
	!!}

	{!!
	widget("checkbox")
		->name('select_all')
		->label('tr:ehdaManage::ehda.printings.select_all')
		->inForm()
	!!}

	{!!
	widget("note")
		->label($note)
		->condition($note)
		->class("text-$shape")
!!}


</div>

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		'save_label' => $title  ,
		'save_shape' => $shape ,
	])
</div>

<script>
    gridSelector('get');
    $('[name=browse_event_id]').val($('#txtEventId').val());
</script>