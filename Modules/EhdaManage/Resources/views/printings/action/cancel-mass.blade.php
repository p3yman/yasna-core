@include("ehdamanage::printings.action.template" , [
	"route" => "cancel-printing" ,
	"title" => trans_safe("ehdaManage::ehda.printings.cancel") ,
	"shape" => "danger" ,
	"note" => trans_safe("ehdaManage::ehda.printings.cancel_hint") ,
])