@include("ehdamanage::printings.action.template" , [
	"route" => "revert-to-pending" ,
	"title" => trans_safe("ehdaManage::ehda.printings.revert_to_pending") ,
	"shape" => "danger" ,
	"note" => trans_safe("ehdaManage::ehda.printings.revert_to_pending_hint") ,
])