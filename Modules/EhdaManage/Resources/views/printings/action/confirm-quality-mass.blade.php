@include("ehdamanage::printings.action.template" , [
	"route" => "confirm-quality" ,
	"title" => trans_safe("ehdaManage::ehda.printings.verify_quality") ,
	"shape" => "success" ,
	"note" => trans_safe("ehdaManage::ehda.printings.confirm_good_print_hint") ,
])