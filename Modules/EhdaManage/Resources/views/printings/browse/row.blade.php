@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
])

<td>
	@include("ehdaManage::printings.browse.row-name")
</td>
<td>
	@include("ehdaManage::printings.browse.row-event")
</td>
<td>
	@include("ehdaManage::printings.browse.row-state")
</td>
<td>
	@include("ehdaManage::printings.browse.row-domain")
</td>
