@extends('manage::layouts.template')

@section('content')
	@include('ehdaManage::printings.browse.show-event')
	@include('ehdaManage::printings.browse.tabs')
	@include('ehdaManage::printings.browse.download-notice-container')
	@include('ehdaManage::printings.browse.toolbar')
	@include('ehdaManage::printings.browse.grid')
@endsection