@include("manage::widgets.grid" , [
	'table_id' => "tblPrintings" ,
	'row_view' => "ehdaManage::printings.browse.row",
	'handle' => "selector" ,
	'operation_heading' => false ,
	'headings' => $browse_columns ,
]     )
