@include("manage::widgets.grid-action" , [
	"id" => "Filter" ,
	"button_class" => "default" ,
	"button_label" => str_limit($event->title,20) ,
	"button_size" => "sm" ,
	"actions" => $events,
])