@include("manage::widgets.grid-text" , [
	'text' => $model->event? $model->event->title : '-'
])

@include("manage::widgets.grid-date" , [
	'date' => $model->created_at,
]     )
