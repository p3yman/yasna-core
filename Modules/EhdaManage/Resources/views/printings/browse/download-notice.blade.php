<div class="mv20 w70 margin-auto alert panel-primary text-center bg-white">

	<div class="mv20">
		{{ trans("ehdaManage::ehda.printings.popup_warning") }}
	</div>

	<div>
		<button onclick="window.location='{{url("manage/cards/printings/excel/$event->id")}}'"
				class="btn btn-lg btn-pink">
			<i class="fa fa-download mh10"></i>
			{{ trans("manage::forms.button.download_excel_file") }}
		</button>

	</div>

	<div>
		<button onclick="$(this).parent().parent().slideUp('fast')" class="btn btn-link btn-sm">
			{{ trans("manage::forms.general.no_need") }}
		</button>
	</div>

	<script>
        setTimeout(function () {
            window.location = '{{url("manage/cards/printings/excel/$event->id")}}'
        }, 2000);
	</script>
</div>