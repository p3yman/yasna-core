{{--
|--------------------------------------------------------------------------
| Name
|--------------------------------------------------------------------------
|
--}}

<div class="row">
	<div class="col-sm-6 font-yekan text-bold f20">
	{{ user()->full_name }}

	<!-- User Role -->
		<div class="role p25">
			<div class="btn-group">
				<button type="button" class="btn btn-defult btn-taha dropdown-toggle minWidthAuto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="m">{{ trans('validation.attributes.role_slug') }}</span>
					<span class="caret"></span>
				</button>

				<ul class="dropdown-menu">
					@foreach(user()->roles as $role)
						<li>
							<a href="javascript:void(0)">
								@if($role->spreadMeta()->icon)
									<i class="fa fa-{{ $role->spreadMeta()->icon }}"></i>
								@else
									<i class="fa fa-check"></i>
								@endif
								{{ $role->title }}
							</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	<!-- !END User Role -->
{{--
|--------------------------------------------------------------------------
| Buttons
|--------------------------------------------------------------------------
|
--}}
		<div class="row" style="margin-top: 30px">
			<div class="col-md-4">
				{!!
				widget("link")
				->label('tr:manage::settings.account')
				->target('url:manage/account')
				->icon('user')
				!!}
			</div>

			<div class="col-md-5">
				{!!
				widget("link")
				->label('tr:manage::template.logout')
				->target('url:logout')
				->color('danger')
				->icon('sign-out')
				!!}
			</div>
		</div>

	</div>
		<div class="col-sm-6">
			<div class="card" style=	"max-width: 450px; margin: auto;">
				<img style="box-shadow: 0 2px 5px rgba(0,0,0,0.2);
    					border-radius: 3px" src="{{ user()->cards('single','show') }}" alt="card" width="100%" onerror="this.style.display='none'">
			</div>
		</div>
</div>


<script>
	function showMore(btn) {
		$(btn).hide();
		$('.more-roles').slideDown();
    }
</script>
