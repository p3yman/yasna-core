@extends('manage::layouts.template')

@section('content')

	<div class="progress">
		<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
			 aria-valuemax="0">
		</div>
	</div>

	<button id="start" class="btn btn-primary">شروع تمیز سازی</button>


	<div class="row" style="margin-top: 10px">
		<div class="col-md-12">
			<ul id="list" class="list-group">
			</ul>
		</div>
	</div>
	<script>
        $(document).ready(function () {

            $("#start").click(function () {
                $(this).attr('disabled', 'disabled');
                callAjax();
            });

            function callAjax() {
                $.get("{{url('manage/ehda/check/user-ajax')}}", {}, function (data, status) {

                    var dataArray = JSON.parse(data);
                    console.log(dataArray);
                    var max       = dataArray.all;
                    var current   = dataArray.converted;
                    var list_id   = dataArray.converted_list_id;
                    var info      = dataArray.user_info;

                    updateProgress(max, current);
                    updateList(list_id, info);
                    if (max !== current)
                        callAjax();
                    console.log(max, current);
                });
            }

            function updateProgress(max, current) {
                var percent = parseInt((current / max) * 100);
                $('.progress-bar').attr('aria-valuemax', max).attr('aria-valuenow', current).css("width", percent + "%").text(percent + "%")
            }

            function updateList(list, info) {
                list.forEach(function (item, index) {
                    $('#list').append('<li class="list-group-item">' + item + " -> " + info[item].state + '</li>');
                })
            }
        });
	</script>
@stop