@php($statusColor=payment()->transaction()->trackingNumber($model->tracking_no)->getModel()->status_color)
<td>{{$model->name}}</td>
<td>{{pd($model->tracking_no)}}</td>
<td>{{pd(number_format($model->amount))}}</td>
<td>
	<label id=""
		   class="ph5 img-rounded bg-{{ ($statusColor=="green") ? "success" : $statusColor }}"
		   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
					<span class="f12">
						{{trans("ehdamanage::donate.".payment()->transaction()->trackingNumber($model->tracking_no)->status()['slug'])}}
					</span>
	</label>
</td>
<td>{{ payment()->transaction()->trackingNumber($model->tracking_no)->getModel()->type_text}}</td>
<td>{{pd(echoDate($model->created_at,"j F Y"))}}</td>