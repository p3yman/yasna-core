@extends('manage::layouts.template')

@section('content')

	{{--@if(user()->as('admin')->can('donation.export'))--}}
		{{--<div class="row ml-lg">--}}
			{{--{!! widget('button')->id('report')->label(trans('ehdamanage::donate.report'))->class('btn-success btn-taha')->icon('file-excel') !!}--}}
		{{--</div>--}}
	{{--@endif--}}

	<div class=" panel-toolbar row ">
		<div class="col-md-5">
			<p class="title">{{trans('ehdamanage::donate.table-header')}}</p>
		</div>
		@if(user()->as('admin')->can('donation.export'))
		<div class="col-md-7">
			{!!
				widget('button')
				->id('report')
				->label(trans('ehdamanage::donate.report'))
				->class('btn-success btn-taha pull-right')
				->icon('file-excel')
			 !!}
		</div>
		@endif
	</div>

	<div class="row">
		<div class="col-md-12">
			@include("manage::widgets.grid" , [
				'table_id' => "tblUsers" ,
				'row_view' => "ehdamanage::donate.grid-row" ,
				'headings' => [
				trans('ehdamanage::report.name'),
				trans('ehdamanage::donate.id'),
				trans("ehdamanage::donate.price"),
				trans("ehdamanage::donate.transaction-state"),
				trans("ehdamanage::donate.transaction-type"),
				trans("ehdamanage::donate.date"),
				] ,
			]     )
		</div>
	</div>
	<script>

        $(document).ready(function () {
			@if(isset($reportEmpty))
            setTimeout(function () {
                $.notify("{{trans('ehdamanage::donate.excel-empty')}}", {status: 'warning'});
            }, 1000);
			@endif
            $("#report").click(function () {
                masterModal("{{url("manage/ehda/donate/modal")}}");
            });
        });
	</script>
@stop

