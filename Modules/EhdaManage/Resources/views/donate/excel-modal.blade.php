{{
widget('modal')->label(trans('ehdamanage::donate.report-btn'))
			->id('excel-modal')
}}

<main style="margin: 10px">
	<form method="post" action="{{url('manage/ehda/donate/excel')}}">
		{{csrf_field()}}
		<div class="row mv-lg">
			<div class="col-md-12">
				 {!!
				  widget('PersianDatePicker')
					  ->id('donate-date')
					  ->inRange()
					  ->name('donate_date')
					  ->placeholderFrom("tr:ehdamanage::report.from")
					  ->placeholderTo("tr:ehdamanage::report.since")
					  ->onlyDatePicker()
					  ->inForm()
					  ->label('tr:ehdamanage::report.report')
				!!}
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				{{--<label for="job-input">{{trans('ehdamanage::donate.id')}}</label>--}}
				{!!
				   widget('text')
					->id('donate-id')
					->name('tracking-no')
					->blankValue('')
					->help('tr:ehdamanage::donate.id-describe')
					->inForm()
					->label('tr:ehdamanage::donate.id')
				!!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{{--<label for="job-input">{{trans('ehdamanage::donate.transaction-state')}}</label>--}}
				{!!
				   widget('combo')
					->id('trans-state')
					->name('trans-state')
					->options($status)
					->searchable()
					->valueField('value')
					->captionField('text')
					->blankValue('')
					->blankCaption(trans('ehdamanage::donate.all-state'))
					->inForm()
					->label('tr:ehdamanage::donate.transaction-state')
				!!}
			</div>
		</div>
		<div class="clearfix pv10">
			{!!
				widget('button')
					->id('submit')
					->type('submit')
					->label(trans('ehdamanage::report.submit'))
					->class('btn-info btn-taha pull-right')
			 !!}
		</div>
	</form>
</main>