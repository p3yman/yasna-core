@if(user()->as('admin')->can('donation.view'))
	<li id="ehda-report">
		<a href="{{url("manage/ehda/donate/list")}}">
			<em class="fa fa-flag"></em>
			<span>{{trans('ehdamanage::donate.sidebar')}}</span>
		</a>
	</li>
@endif