@extends('manage::layouts.template')

@php
	$genders = collect(EhdaTools::activeGendersTranses())->map(function ($val,$key){
		return ['key'=>$key,'caption'=>$val];
	})->toArray();
	$edu = collect(EhdaTools::getActiveEduLevelsTranses())->map(function ($val,$key){
		return ['key'=>$key,'caption'=>$val];
	})->toArray();

	$activities = model('activity')::all(['title','slug'])->toArray();
	$jobs = model('user')::where('job',"<>","")->groupBy('job')->get(['job'])->toArray();
	$labelStyle='color:#656565 !important';
@endphp


@section('content')

	<script src="{{Module::asset('ehdamanage:js/report.js')}}"></script>
	<style>
		.ambassador_user, .user_by_card_dates {
			display: none;
		}

		/*#report-parent label{*/
		/*width: auto;*/
		/*}*/
	</style>

	<div class="container-fluid" id="report-parent">


		<div class="panel panel-dark">
			<div class="panel-heading">
				<h3 class="f20 mv-lg">
					{{trans('ehdamanage::report.form')}}
				</h3>
			</div>
			<div class="tforms">
				<form action="{{url('manage/ehda/report')}}" method="get" id="report-form" class="form-horizontal">
					{{ csrf_field() }}
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<h4 class="page-header">
									{{trans('ehdamanage::report.personal_info')}}
								</h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								{{--<label for="city-input">{{trans('ehdamanage::report.name')}}</label>--}}
								{!!
									 widget('input')
										->id('name')
										->name('name')
										->value($request->get('name'))
										->label('tr:ehdamanage::report.name')
										->labelStyle($labelStyle)
								!!}
							</div>
							<div class="col-md-4">
								{{--<label for="city-input">{{trans('ehdamanage::report.family')}}</label>--}}
								{!!
									 widget('text')
										->id('family')
										->name('family')
										->value($request->get('family'))
										->label('tr:ehdamanage::report.family')
									    ->labelStyle($labelStyle)

								!!}
							</div>
							<div class="col-md-4">
								{{--<label for="city-input">{{trans('ehdamanage::report.father_name')}}</label>--}}
								{!!
									 widget('text')
										->id('father-name')
										->name('father-name')
										->value($request->get('father-name'))
										->label('tr:ehdamanage::report.father_name')
										->labelStyle($labelStyle)
								!!}
							</div>
						</div>
						<div class="row">

							<div class="col-md-4">
								{{--<label for="city-input">{{trans('ehdamanage::report.code_meli')}}</label>--}}
								{!!
								 widget('text')
								->id('code-meli')
								->name('code-meli')
								->type('text')
								->value($request->get('code-meli'))
								->label('tr:ehdamanage::report.code_meli')
								->labelStyle($labelStyle)
								!!}
							</div>

							<div class="col-md-4">
								{{--<label for="city-input">{{trans('ehdamanage::report.gender')}}</label>--}}
								{!!
							   widget('combo')
								->id('gender-input')
								->name('gender')
								->options($genders)
								->searchable()
								->valueField('key')
								->captionField('caption')
								->blankValue('')
								->blankCaption(trans('ehdamanage::report.combo_blank_value'))
								->value($request->get('gender'))

								->label('tr:ehdamanage::report.gender')
								->labelStyle($labelStyle)
								!!}
							</div>

							<div class="col-md-4">
								@include("ehdamanage::layouts.report_sections.age")
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								{{--<label for="city-input">{{trans('ehdamanage::report.mobile')}}</label>--}}
								{!!
							   widget('text')
								->id('mobile')
								->name('mobile')
								->type('mobile')->value($request->get('mobile'))

								->label('tr:ehdamanage::report.mobile')
								->labelStyle($labelStyle)
								!!}
							</div>

							<div class="col-md-4">
								{{--<label for="city-input">{{trans('ehdamanage::report.home_tele')}}</label>--}}
								{!!
								 widget('text')
								->id('home-tel')
								->name('home-tel')
								->type('mobile')
								->value($request->get('home-tel'))

								->label('tr:ehdamanage::report.home_tele')
								->labelStyle($labelStyle)
								!!}
							</div>

							@if($cardAccess)
								<div class="col-md-4">
									{{--<label for="city-input">{{trans('ehdamanage::report.code_post')}}</label>--}}
									{!!
									 widget('text')
									->id('code-post')
									->name('code-post')
									->value($request->get('code-post'))

									->label('tr:ehdamanage::report.code_post')
									->labelStyle($labelStyle)
									!!}
								</div>
							@endif
						</div>


						<div class="row">
							@include("ehdamanage::layouts.report_sections.home_province_city")
						</div>

						<div class="row">
							<div class="col-md-12">
								<h4 class="page-header">
									{{trans('ehdamanage::report.job_info')}}
								</h4>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<div class="col-md-4">
									{{--<label for="edu-input">{{trans('ehdamanage::report.edu')}}</label>--}}
									{!!
										   widget('combo')
										->id('edu-input')
										->name('edu')
										->options($edu)
										->searchable()
										->valueField('key')
										->captionField('caption')
										->blankValue('')
										->blankCaption(trans('ehdamanage::report.combo_blank_value'))
										->value($request->get('edu'))

										->label('tr:ehdamanage::report.edu')
										->labelStyle($labelStyle)
									!!}
								</div>
								@if($volunteerAccess)
									<div class="col-md-4">
										{{--<label for="job-input">{{trans('ehdamanage::report.job')}}</label>--}}
										{!!
										   widget('text')
										->id('job-input')
										->name('job')
										->value($request->get('job'))

										->label('tr:ehdamanage::report.job')
										->labelStyle($labelStyle)
										!!}
									</div>
								@endif
								<div class="col-md-4">
									{{--<label for="city-input">{{trans('ehdamanage::report.work_tele')}}</label>--}}
									{!!
									 widget('text')
									->id('work-tel')
									->name('work-tel')
									->type('mobile')
									->value($request->get('work-tel'))

									->label('tr:ehdamanage::report.work_tele')
									->labelStyle($labelStyle)
									!!}
								</div>
								@include("ehdamanage::layouts.report_sections.work_province_city")
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<h4 class="page-header">
									{{trans('ehdamanage::report.users_type')}}
								</h4>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row pv10">
								@if($cardAccess)
									<div {{(!$volunteerAccess)?"style=pointer-events:none":""}}>
										{!!
											  widget('checkbox')
											 ->id('user-by-card')
											 ->name('user-by-card')
											 ->label('tr:ehdamanage::report.user_by_card')
											 ->value( (is_null($request->get('user-by-card')) )? "1" : $request->get('user-by-card'))
										!!}
									</div>
								@endif
							</div>
							<div class="row ">
								@include('ehdamanage::layouts.report_sections.user_by_card')
							</div>
							<div class="row">
								@if($volunteerAccess)
									<div {{(!$cardAccess)?"style=pointer-events:none":""}}>
										{!!
											widget('checkbox')
										 ->id('ambassador-user')
										 ->name('ambassador-user')
										 ->label('tr:ehdamanage::report.ambassador_user')
										 ->value((is_null($request->get('ambassador-user')) )? "1" : $request->get('ambassador-user'))
										 !!}
									</div>
								@endif
							</div>
							@include('ehdamanage::layouts.report_sections.ambassador_user')
						</div>
					</div>

					<div class="panel-footer pv-lg">
						{!! widget('button')->id('submit')->type('submit')->label(trans('ehdamanage::report.submit'))->class('btn-primary')!!}
						{!! widget('button')->id('excel')->label(trans('ehdamanage::report.excel'))->class('btn-success')->icon('file-excel')!!}
					</div>
				</form>
			</div>
		</div>


		@if($search)
			<div class="row" style="margin-top:10px ">
				<div class="col-md-12">
					<p>
						{{trans('ehdamanage::report.result-count')." ".pd($resCount)}}
					</p>
				</div>
				<div class="col-md-12">
					@include("manage::widgets.grid" , [
						'table_id' => "tblUsers" ,
						'row_view' => "ehdamanage::layouts.report_sections.table_row" ,
						'headings' => [
						trans('ehdamanage::report.number'),
						trans('ehdamanage::report.fullname'),
						trans('ehdamanage::report.mobile'),
						trans('ehdamanage::report.email'),
						trans('ehdamanage::report.birth_day'),
						trans('ehdamanage::report.national_code'),
						] ,
					]     )
				</div>
			</div>
		@endif
	</div>
	@php($modalUrl=url('manage/ehda/report/excel'))
	<script>
        //run excel modal with form data
        $("#excel").click(function () {
            var queryString = "?" + $("#report-form").serialize();
            masterModal("{{$modalUrl}}" + queryString)
        })
	</script>
@stop
