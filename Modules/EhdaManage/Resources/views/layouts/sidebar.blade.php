@include("manage::layouts.sidebar-link" , [
	'icon' => "credit-card",
	'caption' => trans("ehdaManage::ehda.donation_card") ,
	'link' => "cards" ,
	"sub_menus" => $sub_menus = module('ehdaManage')->cardsController()->sidebarSubMenu() ,
	'permission' => sizeof($sub_menus) ? '' : 'dev',
]     )

@include("manage::layouts.sidebar-link" , [
	'icon' => "child",
	'caption' => trans("ehdaManage::ehda.volunteers.plural") ,
	'link' => "volunteers" ,
	"sub_menus" => $sub_menus = module('ehdaManage')->volunteersController()->sidebarSubMenu() ,
	'permission' => sizeof($sub_menus) ? '' : 'dev',
]     )