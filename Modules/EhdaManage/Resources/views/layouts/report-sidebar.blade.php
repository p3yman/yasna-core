@if(user()->as('admin')->canAny(array_merge(['card-holder.report'],model('role')->adminRoles('.report'))))
	<li id="ehda-report">
		<a href="{{url("manage/ehda/report")}}">
			<em class="fa fa-flag"></em>
			<span>{{trans('ehdamanage::report.menu')}}</span>
		</a>
	</li>
@endif