<div class="row pv20">
	<div class="col-md-6 ambassador_user">
		{{--<label>{{trans('ehdamanage::report.ambassador_user_register_date')}}</label>--}}
		{!!
			  widget('PersianDatePicker')
			 ->id('ambassador-user-register')
			 ->inRange()
			 ->name('ambassador_user_register')
			 ->placeholderFrom('tr:ehdamanage::report.from')
			 ->placeholderTo('tr:ehdamanage::report.since')

			 ->label('tr:ehdamanage::report.ambassador_user_register_date')
			  ->valueFrom($request->ambassador_user_register)
			  ->valueTo($request->to_ambassador_user_register)
			  ->withoutTime()
			  ->labelStyle($labelStyle)
		!!}
		</div>
	</div>


<div class="row mb-lg">
	<div class="col-md-6 ambassador_user">
		{{--<label for="state">{{trans('ehdamanage::report.state')}}</label>--}}
		{{--{!!--}}
			{{--widget('toggle')--}}
			{{--->id('state')--}}
			 {{--->name('state')--}}
			  {{--->label(' ')--}}
			  {{--->size('100')--}}
			  {{--->dataOn(trans('ehdamanage::report.enables'))--}}
			  {{--->dataOff(trans('ehdamanage::report.disables'))--}}
			  {{--->value($request->get('state'))--}}
			  {{--->label('tr:ehdamanage::report.state')--}}
		 {{--!!}--}}
		{!!
							   widget('combo')
							->id('state')
							->name('state')
							->options($status_list)
							->searchable()
							->valueField('value')
							->captionField('title')
							->blankValue('all')
							->blankCaption(trans('ehdamanage::report.combo_blank_value'))
							->value($request->get('status'))
							->label('tr:ehdamanage::report.state')
							->labelStyle($labelStyle)
						!!}
	</div>
</div>

@foreach($activities as $activity)
	<div class="row mb-lg">
		<div class="col-md-6 ambassador_user">
			{!!
				widget('checkbox')
				->id('activity')
				 ->name($activity['slug'])
				  ->label($activity['title'])
				  ->size('100')
			 !!}
		</div>
	</div>
@endforeach
