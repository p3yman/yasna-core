{{
widget('modal')->label(trans('ehdamanage::report.excel'))
			->id('excel-modal')
}}

<form method="post" action="{{url('manage/ehda/report/excel')}}" id="cols">
	<div class="row m">
		<h4>{{trans('ehdamanage::report.excel_fields')}}</h4>
	</div>
	<div class="panel panel-default m">
		<div class="panel-heading">
			<div class="panel-title">{{trans('ehdamanage::report.general_column')}}</div>
		</div>

		<div class="row m">
			<div class="col-md-2">
				{{widget('checkbox')->id('name')->name('name_check')->value(1)->label(trans('ehdamanage::report.name'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('family')->name('family_check')->value(1)->label(trans('ehdamanage::report.family'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('mobile')->name('mobile_check')->value(1)->label(trans('ehdamanage::report.mobile'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('email')->name('email_check')->value(1)->label(trans('ehdamanage::report.email'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('birth_day')->name('birth_day_check')->value(1)->label(trans('ehdamanage::report.birth_day'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('national_code')->name('national_code_check')->value(1)->label(trans('ehdamanage::report.national_code'))}}
			</div>
		</div>

		<div class="row m">
			<div class="col-md-2">
				{{widget('checkbox')->id('father_name')->name('father_name_check')->value(1)->label(trans('ehdamanage::report.father_name'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('code_id')->name('code_id_check')->value(1)->label(trans('ehdamanage::report.code_id'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('card_register_date')->name('card_register_date_check')->value(1)->label(trans('ehdamanage::report.card_register_date'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('home_address')->name('home_address_check')->value(1)->label(trans('ehdamanage::report.home_address'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('home_province')->name('home_province_check')->value(1)->label(trans('ehdamanage::report.home_province'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('home_city')->name('home_city_check')->value(1)->label(trans('ehdamanage::report.home_city'))}}
			</div>
		</div>
		<div class="row m">
			<div class="col-md-2">
				{{widget('checkbox')->id('home_tele')->name('home_tele_check')->value(1)->label(trans('ehdamanage::report.home_tele'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('edu')->name('edu_check')->value(1)->label(trans('ehdamanage::report.edu'))}}
			</div>
		</div>
	</div>
	@if($request->get('user-by-card')=="1")
		<div class="panel panel-default m">
			<div class="panel-heading">
				<div class="panel-title">{{trans('ehdamanage::report.cards_column')}}</div>
			</div>
			<div class=" row m">
				<div class="col-md-2">
					{{widget('checkbox')->id('card_no')->name('card_no_check')->value(1)->label(trans('ehdamanage::report.card_no'))}}
				</div>
				<div class="col-md-2">
					{{widget('checkbox')->id('postal')->name('postal_check')->value(1)->label(trans('ehdamanage::report.code_post'))}}
				</div>
			</div>
		</div>
	@endif

	@if($request->get('ambassador-user')=="1")
		<div class="panel panel-default m">
			<div class="panel-heading">
				<div class="panel-title">{{trans('ehdamanage::report.volunteers_column')}}</div>
			</div>
			<div class="row m">
				<div class="col-md-2">
					{{widget('checkbox')->id('marital')->name('marital_check')->value(1)->label(trans('ehdamanage::report.marital'))}}
				</div>
				<div class="col-md-2">
					{{widget('checkbox')->id('register_date')->name('register_date_check')->value(1)->label(trans('ehdamanage::report.register_date'))}}
				</div>
				<div class="col-md-2">
					{{widget('checkbox')->id('job')->name('job_check')->value(1)->label(trans('ehdamanage::report.job'))}}
				</div>
				<div class="col-md-2">
					{{widget('checkbox')->id('motivation')->name('motivation_check')->value(1)->label(trans('ehdamanage::report.motivation'))}}
				</div>
				<div class="col-md-4">
					{{widget('checkbox')->id('alloc_time')->name('alloc_time_check')->value(1)->label(trans('ehdamanage::report.alloc_time'))}}
				</div>
			</div>

			<div class="row m">
				<div class="col-md-2">
					{{widget('checkbox')->id('edu_field')->name('edu_field_check')->value(1)->label(trans('ehdamanage::report.edu_field'))}}
				</div>
			</div>
		</div>
	@endif

	{{widget('button')->id('excel-btn')->label(trans('ehdamanage::report.submit'))->class('btn-primary m')}}
</form>

<script>

    $(document).ready(function () {

        $("#excel-btn").click(function () {
            var queryString = $("#report-form").serialize();
            var cols        = $("#cols").serialize();
            var allData     = queryString + "&" + cols;

			window.location = "{{url('manage/ehda/report/export')}}" + "?ajax=1&" + allData;
            $(".close").click()
        })
    });
</script>