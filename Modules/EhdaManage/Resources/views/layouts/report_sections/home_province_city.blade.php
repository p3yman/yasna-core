@php
	$provinces = model('state')::where('parent_id','0')->orderBy('title')->get(['id','title'])->toArray();
	if (!empty($request->get('home-province'))){
	$cities = model('state')::where('parent_id',$request->get('home-province'))->orderBy('title')->get(['id','title'])->toArray();
	}
    else{
	$cities = model('state')::where('parent_id','<>','0')->orderBy('title')->get(['id','title'])->toArray();
    }
@endphp

<div class="col-md-4">
	{!!
		   widget('combo')
			->id('home-province-input')
			->name('home-province')
			->options($provinces)
			->searchable()
			->valueField('id')
			->captionField('title')
			->blankValue('')
			->blankCaption(trans('ehdamanage::report.combo_blank_value'))
			->value($request->get('home-province'))

			->label('tr:ehdamanage::report.home_province')
			->labelStyle($labelStyle)
		!!}
</div>

<div class="col-md-4">
	{!!
		 widget('combo')
		->id('home-city-input')
		->name('home-city')
		->options($cities)
		->searchable()
		->valueField('id')
		->captionField('title')
		->blankValue('')
		->blankCaption(trans('ehdamanage::report.combo_blank_value'))
		->value($request->get('home-city'))

		->label('tr:ehdamanage::report.home_city')
		->labelStyle($labelStyle)
	!!}
</div>

<script>

    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('input[name="_token"]').val()
            }
        });

        $("#home-province-input").change(function (e) {

            getCities($("#home-city-input"), $(this).val(), "{{url('manage/ehda/city_ajax')}}")
        });

        $("#home-city-input").change(function (e) {
            getProvince($("#home-province-input"), $(this).val(), "{{url('manage/ehda/province_ajax')}}")
        });
    });
</script>