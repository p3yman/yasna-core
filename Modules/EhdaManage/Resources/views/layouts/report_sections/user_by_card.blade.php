<div class="col-md-6 user_by_card_dates">
	{{--<label>{{trans('ehdamanage::report.user_by_card_register_date')}}</label>--}}
	{!!
		 widget('PersianDatePicker')

			 ->label('tr:ehdamanage::report.user_by_card_register_date')
			 ->placeholderFrom('tr:ehdamanage::report.from')
			 ->placeholderTo('tr:ehdamanage::report.since')
			 ->id('user-by-card-register')
			 ->inRange()
			 ->name('user_by_card_register')
			 ->valueFrom($request->user_by_card_register)
			  ->valueTo($request->to_user_by_card_register)
			  ->onlyDatePicker()
			  ->withoutTime()
			  ->labelStyle($labelStyle)
		 !!}

</div>