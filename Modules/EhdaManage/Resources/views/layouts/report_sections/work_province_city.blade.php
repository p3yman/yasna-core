@php
	$provinces = model('state')::where('parent_id','0')->orderBy('title')->get(['id','title'])->toArray();
	if (!empty($request->get('work-province'))){
	$cities = model('state')::where('parent_id',$request->get('work-province'))->orderBy('title')->get(['id','title'])->toArray();
	}
    else{
	$cities = model('state')::where('parent_id','<>','0')->orderBy('title')->get(['id','title'])->toArray();
    }
@endphp

<div class="col-md-4">
	{{--<label for="city-input"> {{trans('ehdamanage::report.work_province')}}</label>--}}
	{!!
	   widget('combo')
	->id('work-province-input')
	->name('work-province')
	->options($provinces)
	->searchable()
	->valueField('id')
	->captionField('title')
	->blankValue('')
	->blankCaption(trans('ehdamanage::report.combo_blank_value'))
	->value($request->get('work-province'))

	->label('tr:ehdamanage::report.work_province')
	->labelStyle($labelStyle)
	!!}
</div>

<div class="col-md-4">
	{{--<label for="city-input">{{trans('ehdamanage::report.work_city')}}</label>--}}
	{!!
	 widget('combo')
	->id('work-city-input')
	->name('work-city')
	->options($cities)
	->searchable()
	->valueField('id')
	->captionField('title')
	->blankValue('')
	->blankCaption(trans('ehdamanage::report.combo_blank_value'))
	->value($request->get('work-city'))

	->label('tr:ehdamanage::report.work_city')
	->labelStyle($labelStyle)
	!!}
</div>

<script>
    $(document).ready(function () {
        $("#work-province-input").change(function (e) {
            getCities($("#work-city-input"), $(this).val(), "{{url('manage/ehda/city_ajax')}}")
        });

        $("#work-city-input").change(function (e) {
            getProvince($("#work-province-input"), $(this).val(), "{{url('manage/ehda/province_ajax')}}")
        });
    })
</script>