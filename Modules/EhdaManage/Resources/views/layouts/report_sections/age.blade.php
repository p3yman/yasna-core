<div class="row">
	<div class="col-md-6">
			<div class="col-sm-8">
				{!!
				  widget('text')
				  ->id('age-min-input')
				  ->name('age-min')
				  ->value($request->get('age-min'))
				  ->label(trans('ehdamanage::report.from_age'))
				  ->labelStyle($labelStyle)

				!!}
			</div>
	</div>
	<div class="col-md-6">

			<div class="col-sm-8">
				{!!
				  widget('text')
				  ->id('age-max-input')
				  ->name('age-max')
				  ->value($request->get('age-max'))
				  			  ->label(trans('ehdamanage::report.to_age'))
				  ->labelStyle($labelStyle)
				!!}
			</div>
	</div>
</div>