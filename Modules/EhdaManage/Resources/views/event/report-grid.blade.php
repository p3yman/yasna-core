{{
 widget('modal')
 ->label($title)
 ->id('event-report')
}}
<div class="row" style="margin-top:10px ">
	<div class="col-md-12">
		<div class="panel panel-default {{--m20--}}">
			<div class="panel-body">
				<div class="table-type">
					<table id="{{$table_id or ''}}" class="table tableGrid {{$table_class or ''}} {{--table-striped--}}">
						<thead>
						<tr>
							<td>{{trans('ehdamanage::event-report.date')}}</td>
							<td>{{trans('ehdamanage::event-report.submits')}}</td>
							<td>{{trans('ehdamanage::event-report.prints')}}</td>

						</tr>
						</thead>
						<tbody>

						@foreach($data as $item)
							<tr>
								<td>{{pd(echoDate($item['date'],'j F Y'))}}</td>
								<td>{{pd($item['registerUser'])}}</td>
								<td>{{pd($item['printUser'])}}</td>
							</tr>
						@endforeach
						<tr class="disabled active">
							<td> </td>
							<td class="text-bold">{{trans('ehdamanage::event-report.total-register')}}</td>
							<td class="text-bold">{{trans('ehdamanage::event-report.total-print')}}</td>
						</tr>
						<tr class=" disabled">
							<td> </td>
							<td>{{pd($totalRegister)}}</td>
							<td>{{pd($totalPrint)}}</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>