@if(
$model->categories()->get()->where('slug','registrable')->count()
)
	@php($modalUrl=url('manage/ehda/event_report')."/".$model->id)
	{!!
		widget('button')
			->id('report')
			->label(trans('ehdamanage::event-report.button'))
			->on_click("masterModal('$modalUrl')")
	!!}

	@php($regTotal=$model->registers()->count())
	@php($printTotal=model('printing')->where('event_id',$model->id)->count())

	<div class="f11 mt-lg text-info">
		{{trans('ehdamanage::event-report.total-register')}}
		<span class="badge" style="color: #0d0a0a">{{pd($regTotal)}}</span>
	</div>

	<div class="f11 mt-sm text-info">
		{{trans('ehdamanage::event-report.total-print')}}
		<span class="badge" style="color: #0d0a0a">{{pd($printTotal)}}</span>
	</div>


@endif