@extends('manage::layouts.template')

@section('content')
	@include("manage::account.tabs")

	@if(user()->is_a('card-holder'))
		@include("ehdamanage::account.card.show")
	@else
		@include("ehdamanage::account.card.register")
	@endif
@endsection