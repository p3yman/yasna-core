<?php

widget("formOpen")
     ->id('frmEditor')
     ->target('name:account-card-register')
     ->class('js')
     ->render()
;

widget("input")
     ->name('name_first')
     ->value(user()->full_name)
     ->inForm()
     ->disabled()
     ->render()
;

widget("input")
     ->name('code_melli')
     ->value(user()->code_melli)
     ->inForm()
     ->disabled()
     ->render()
;

widget("group")
     ->start()
     ->render()
;

?>
<div class="disabled mv5">
	<i class="fa fa-check-square"></i>
	{{ trans('ehdaManage::ehda.cards.agreement_note_line_1') . ' ' . trans('ehdaManage::ehda.cards.agreement_note_line_2') }}
</div>
<?php

widget("group")
     ->stop()
     ->render()
;


widget("group")
     ->start()
     ->render()
;

widget("button")
     ->label("tr:manage::forms.button.save")
     ->shape('success')
     ->type('submit')
     ->render()
;

widget("group")
     ->stop()
     ->render()
;


widget('feed')->render();


widget("formClose")
     ->render();
