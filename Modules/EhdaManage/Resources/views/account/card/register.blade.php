<div class="alert alert-warning mv30 w80">
	{{ trans_safe("ehdaManage::ehda.cards.even_you_dont_have_card") }}
</div>
<div class="mv30 w80 panel panel-default">
	<div class="panel-heading">{{ trans_safe("ehdaManage::ehda.cards.register_full") }}</div>
	<div class="panel-body">
		@include("ehdamanage::account.card.register-form")
	</div>
</div>