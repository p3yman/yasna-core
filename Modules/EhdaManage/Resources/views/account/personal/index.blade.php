@extends('manage::layouts.template')

@section('content')
	@include("manage::account.tabs")

	<div class="mv30">
		@include("ehdamanage::volunteers.editor.form-container" , [
			"editor_mode" => "edit",
			"model" => user() ,
			"states" => model('state')->combo()
		])
	</div>

@endsection