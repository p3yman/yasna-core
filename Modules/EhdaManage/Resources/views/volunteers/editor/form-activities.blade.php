<?php

widget("group")
     ->start()
     ->label("tr:validation.attributes.activities")
     ->render()
;

foreach (model('activity')::sortedAll() as $activity) {
    widget("checkbox")
         ->name("_activity-$activity->slug")
         ->value(in_array($activity->slug, $model->activities_array))
         ->label($activity->title)
         ->render()
    ;
}

widget("group")
     ->stop()
     ->render()
;

widget("separator")->render();
