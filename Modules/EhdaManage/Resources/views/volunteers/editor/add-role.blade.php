{!!
widget("formOpen")
	->id('frmEditor')
	->target('name:volunteer-role')
	->class('js')
	->validation(!debugMode())
!!}

{!!
 widget("hidden")
     ->name('hashid')
     ->value($model->hashid)
 !!}

@include("ehdaManage::volunteers.editor.form-role")
@include("ehdamanage::volunteers.editor.form-buttons")

{!!
widget("formClose")
!!}