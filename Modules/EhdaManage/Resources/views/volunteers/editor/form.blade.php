<?php
/**
 * @var \App\Models\User                         $model
 * @var \Illuminate\Database\Eloquent\Collection $events
 * @var string                                   $editor_mode
 * @var array                                    $states
 */

widget("formOpen")
     ->id('frmEditor')
     ->target('name:volunteer-save')
     ->class('js')
     ->validation(!debugMode())
     ->render()
;

/*-----------------------------------------------
| Hidden Id ...
*/
widget("hidden")
     ->name('hashid')
     ->value($model->hashid)
     ->render()
;

/*-----------------------------------------------
| Code Melli ...
*/
widget("input")
     ->name('code_melli')
     ->value($model->code_melli)
     ->inForm()
     ->disabled()
     ->render()
;

widget("hidden")
     ->name('code_melli')
     ->value($model->code_melli)
     ->render()
;

/*-----------------------------------------------
| Role & Status ...
*/
if (!$model->id or !$model->withDisabled()->is_admin()) {
    echo view("ehdamanage::volunteers.editor.form-role", compact("default_role", "roles", "role_model"));
}

/*-----------------------------------------------
| Gender ...
*/
widget("combo")
     ->name('gender')
     ->value($model->gender)
     ->blankValueIf(!$model->id, '0')
     ->blankCaptionIf(!$model->id, ' ')
     ->required()
     ->options(module('ehdaManage')->CardsController()->genderCombo())
     ->inForm()
     ->render()
;

/*-----------------------------------------------
| Basic Information...
*/
widget("input")
     ->name('name_first')
     ->value($model->name_first)
     ->required()
     ->inForm()
     ->render()
;

widget("input")
     ->name('name_last')
     ->value($model->name_last)
     ->required()
     ->inForm()
     ->render()
;

widget("input")
     ->name('name_father')
     ->value($model->name_father)
     ->inForm()
     ->render()
;

widget("input")
     ->name("code_id")
     ->value($model->code_id)
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Birth Date and City ...
*/
widget("combo")
     ->name('birth_city')
     ->value($model->birth_city)
     ->options($states)
     ->searchable()
     ->inForm()
     ->blankValue(0)
     ->searchPlaceholder("tr:manage::forms.button.search")
     ->render()
;

widget("date-picker")
     ->name('birth_date')
     ->required()
     ->value($model->birth_date)
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Job and Education ...
*/
widget("input")
     ->name('job')
     ->value($model->job)
     ->inForm()
     ->render()
;

widget("combo")
     ->name('edu_level')
     ->blankValue('')
     ->value($model->edu_value)
     ->inForm()
     ->options(module('ehdaManage')->CardsController()->eduCombo())
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Contact Info ...
*/
widget("input")
     ->name('home_tel')
     ->value($model->home_tel)
     ->ltr()
     ->inForm()
     ->render()
;

widget("input")
     ->name('mobile')
     ->value($model->mobile)
     ->ltr()
     ->inForm()
     ->required()
     ->render()
;

widget("input")
     ->name('tel_emergency')
     ->value($model->tel_emergency)
     ->ltr()
     ->inForm()
     ->required()
     ->render()
;

widget("separator")->render();

widget("combo")
     ->name('home_city')
     ->value($model->home_city)
     ->blankValue('')
     ->options($states)
     ->searchable()
     ->searchPlaceholder("tr:manage::forms.button.search")
     ->required()
     ->inForm()
     ->render()
;

widget("input")
     ->name('email')
     ->value($model->email)
     ->ltr()
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Marital Status ...
*/
widget("combo")
     ->name('marital')
     ->blankValue('')
     ->value($model->marital)
     ->inForm()
     ->options(module('ehdaManage')->CardsController()->maritalCombo())
     ->render()
;


/*-----------------------------------------------
| Edu Field and City ...
*/
widget("input")
     ->name('edu_field')
     ->value($model->edu_field)
     ->inForm()
     ->render()
;

widget("combo")
     ->name('edu_city')
     ->value($model->edu_city)
     ->blankValue(0)
     ->options($states)
     ->searchable()
     ->inForm()
     ->searchPlaceholder("tr:manage::forms.button.search")
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Address ...
*/
widget("textarea")
     ->name('home_address')
     ->value($model->home_address)
     ->inForm()
     ->autoSize()
     ->rows(1)
     ->render()
;

widget("input")
     ->name('home_postal')
     ->value($model->home_postal)
     ->ltr()
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Work ...
*/
widget("combo")
     ->name('work_city')
     ->value($model->work_city)
     ->options($states)
     ->searchable()
     ->inForm()
     ->blankValue(0)
     ->searchPlaceholder("tr:manage::forms.button.search")
     ->render()
;

widget("textarea")
     ->name('work_address')
     ->value($model->work_address)
     ->inForm()
     ->autoSize()
     ->rows(1)
     ->render()
;


widget("input")
     ->name('work_tel')
     ->value($model->work_tel)
     ->ltr()
     ->inForm()
     ->render()
;

widget("input")
     ->name('work_postal')
     ->value($model->work_postal)
     ->ltr()
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Motivation ...
*/
widget("combo")
     ->name('familiarization')
     ->blankValue('')
     ->value($model->familiarization)
     ->inForm()
     ->options(module('ehdaManage')->volunteersController()->familiarizationCombo())
     ->render()
;

widget("input")
     ->name('motivation')
     ->value($model->motivation)
     ->inForm()
     ->render()
;

widget("input")
     ->name('alloc_time')
     ->value($model->alloc_time)
     ->inForm()
     ->render()
;

widget("separator")->render();


/*-----------------------------------------------
| Activities ...
*/
echo view("ehdamanage::volunteers.editor.form-activities", compact("model"));


/*-----------------------------------------------
| Password ...
*/
widget("group")
     ->start()
     ->label("tr:validation.attributes.password")
     ->render()
;

if ($model->id) {
    echo view("ehdamanage::cards.editor.form-password-edit");
} else {
    echo view("ehdamanage::cards.editor.form-password-create");
}

widget("group")
     ->stop()
     ->render()
;

widget("separator")->render();


/*-----------------------------------------------
| Save ...
*/
echo view("ehdamanage::volunteers.editor.form-buttons");

widget("formClose")->render();