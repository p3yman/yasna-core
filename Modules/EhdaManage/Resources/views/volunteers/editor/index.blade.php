@extends('manage::layouts.template')

@section('content')
	@if(!$model->code_melli)
		<div id="divInquiry">
			@include('ehdamanage::volunteers.editor.inquiry-container')
		</div>
	@endif

	<div id="divForm" class="{{ $model->code_melli ? '' : 'noDisplay' }}">
		@include("ehdamanage::volunteers.editor.form-container")
	</div>

	<div id="divCard" class="noDisplay" data-src="manage/volunteers/edit/show/-id-" data-id="">
	</div>

@endsection

@section('html_footer')
	@parent

	{!! Html::script (Module::asset("ehdamanage:js/card-editor.js")) !!}
@endsection