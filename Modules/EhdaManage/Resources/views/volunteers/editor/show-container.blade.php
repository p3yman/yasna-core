@extends('manage::layouts.template')

@section('content')


	<div class="panel panel-primary margin-auto w70">
		<div class="panel-heading">
			{{ trans_safe("ehdaManage::ehda.cards.inquiry") }}
		</div>
		<div class="panel-body">

			{{--<div class="alert alert-info">--}}
				{{--{{ trans_safe("ehdaManage::ehda.volunteers.already_volunteer") }}--}}
			{{--</div>--}}

			@include('ehdamanage::volunteers.editor.show')
		</div>
	</div>

	<div class="text-center">
		<a href="{{ url("manage/volunteers/create") }}">
			{{ trans_safe("ehdaManage::ehda.volunteers.new_inquiry") }}
		</a>
	</div>


@endsection