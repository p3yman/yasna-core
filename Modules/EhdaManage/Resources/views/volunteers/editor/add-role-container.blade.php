<div id="divAddRole" class="panel panel-purple margin-auto w70 mv20 noDisplay">
	<div class="panel-heading">
		{{ trans_safe("ehdaManage::ehda.volunteers.add_role") }}
	</div>
	<div class="panel-body">
		@include("ehdamanage::volunteers.editor.add-role")
	</div>
</div>