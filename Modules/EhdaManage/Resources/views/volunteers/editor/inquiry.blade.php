<?php
/**
 * @var \App\Models\User $model
 */

widget("formOpen")
     ->id('frmInquiry')
     ->target('name:volunteer-inquiry')
     ->label('tr:ehdamanage::ehda.cards.inquiry')
     ->class('js')
     ->noValidation()
     ->render()
;

widget("input")
     ->id('txtInquiry')
     ->name('code_melli')
     ->value($model->code_emlli)
     ->class('form-default')
     ->inForm()
	 ->required()
     ->render()
;

widget("group")
     ->start()
     ->render()
;

widget("button")
     ->label("tr:ehdaManage::ehda.cards.inquiry")
     ->shape('primary')
     ->type('submit')
     ->render()
;

widget("group")
     ->stop()
     ->render()
;

widget('feed')->render();
widget("formClose")->render();