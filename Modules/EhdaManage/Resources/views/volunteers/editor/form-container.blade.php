<div class="panel panel-primary margin-auto w90">
	<div class="panel-heading">
		{{ trans_safe( "ehdamanage::ehda.volunteers.$editor_mode" ) }}
	</div>

	<div class="panel-body">
		<div class="mb-xl">
			@include('useravatar::index',[
				"image" =>  userAvatar($model->hashid),
				"show_delete_button" => userHasAvatar($model->hashid),
				'user_hashid'=>$model->hashid,
				"value" => userHasAvatar($model->hashid) ? $model->avatar : ''
			])
			<hr>
		</div>
		@include('ehdamanage::volunteers.editor.form')
	</div>
</div>