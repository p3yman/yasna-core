<div class="row w50 text-center">
	<div class="font-yekan text-bold f20">
		{{ $model->full_name }}
	</div>


	<div class="mv20">
		{!!
		widget("button")
			->label("tr:ehdaManage::ehda.volunteers.view")
			->class("btn-lg btn-taha")
			->condition($model->ehdaCanEdit())
			->target("modal:manage/volunteers/act/$model->hashid/profile")
		!!}
		
		{!!
		widget("button")
			->label("tr:ehdaManage::ehda.volunteers.add_role")
			->class('btn-lg btn-taha')
			->target("$('#divAddRole').slideToggle('fast')")
		!!}

		{!!
		widget("button")
			->label("tr:manage::forms.button.edit")
			->class("btn-lg btn-taha")
			->condition($model->ehdaCanEdit())
			->target("url:manage/volunteers/edit/$model->hashid")
		!!}
	</div>


</div>

@include("ehdamanage::volunteers.editor.add-role-container")
