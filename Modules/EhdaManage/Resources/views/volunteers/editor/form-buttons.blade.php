<?php

widget("group")
     ->start()
     ->render()
;

widget("button")
     ->label("tr:manage::forms.button.save")
     ->shape('success')
     ->type('submit')
     ->render()
;

widget("group")
     ->stop()
     ->render()
;


widget('feed')->render();
