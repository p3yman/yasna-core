{!!
    widget('Modal')
    ->label(trans('ehdamanage::ehda.modal.title.volunteer'))
    ->id('volunteers-info')
 !!}
@php
	$edu = collect(EhdaTools::getActiveEduLevelsTranses())->toArray();
@endphp
<!-- Modal Body -->
<div class="modal-body">
	<!-- Title -->
	<div class="title ph25">
		<h4 class="mb-lg">
			<i class="fa fa-user mr-sm"></i>
			{{ $user->full_name }}
		</h4>
		<div class="text-muted">
			<small>
				<i class="fa fa-envelope mr-sm"></i>
				{{ $user->email }}
			</small>
		</div>
		<div class="text-muted">
			<small>
				<i class="fa fa-phone mr-sm"></i>
				{{ ad($user->mobile) }}
			</small>
		</div>
		<div class="mt-lg">
			<label id="" class="ph5 img-rounded"
				   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
					<span class="f12">
									<i class="fa fa-check mhl5"></i>
						{{trans('ehdamanage::general.users.volunteer')}}
					</span>
			</label>

			<label id="" class="ph5 img-rounded bg-success"
				   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
				<a href="javascript:void(0)"
				   onclick='masterModal("{{url('manage/cards/act/'.$hashID.'/profile')}}","lg")' class="f12">
					<i class="fa fa-globe mhl5"></i>
					{{trans('ehdamanage::general.users.card')}}
				</a>
			</label>
		</div>
	</div>
	<!-- !END Title -->

	<hr>

	<!-- Personal Information -->
	<div class="personal-info ph25">
		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.code_melli') }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ ad($user->code_melli) }}
			</div>
			<div class="col-sm-6">
				<span class="text-muted">
					{{  "(".trans('validation.attributes.code_id').": ".ad($user->code_melli).")" }}
				</span>
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.birthday') }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ ad(echoDate($user->birth_date,'Y-m-d')) }}
			</div>
			<div class="col-sm-6">
				{{  model('state',$user->birth_city)->title }}
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.education')}}
				</b>
			</div>
			<div class="col-sm-3">
				{{ $edu[$user->edu_level] }}
			</div>
			<div class="col-sm-6">
				{{ $user->edu_field}}
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.home_city') }}
				</b>
			</div>
			<div class="col-sm-3">
				{{trans('validation.attributes.tel')}}
				<span style="direction: ltr;">
					{{ ad($user->home_tel) }}
				</span>
			</div>
			<div class="col-sm-6">
				{{ $user->home_address }}
			</div>
		</div>
	</div>
	<!-- !END Personal Information -->

	<hr>

	<!-- Activities -->
	<div class="activities ph25">
		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.activities')}}
				</b>
			</div>
			<div class="col-sm-4 mv10">
				@if($postCount > 0)
					<div class="mv5">
						<i class="fa fa-clipboard text-info"></i>
						<span class="ph5">{{ pd($postCount) }}</span>
						{{ trans("ehdamanage::volunteer.content-activity") }}
					</div>
				@endif
				@if($printCount > 0)
					<div class="mv5">
						<i class="fa fa-credit-card text-info"></i>
						<span class="ph5">{{ pd($printCount) }}</span>
						{{ trans("ehdamanage::volunteer.register-activity") }}
					</div>
				@endif
				@if($userMakeCount > 0)
					<div class="mv5">
						<i class="fa fa-print text-info"></i>
						<span class="ph5">{{ pd($userMakeCount) }}</span>
						{{ trans("ehdamanage::volunteer.printing-activity") }}
					</div>
				@endif
			</div>
			<div class="col-sm-5 mv10">
				<div class="activities">
					@foreach($activities as $activity)
						<div class="mv5">
							<i class="fa fa-check text-primary mh5"></i>
							{{$activity->title}}
						</div>
					@endforeach
				</div>
				@if(!empty($user->alloc_time))
					<div class="time">
						<i class="fa fa-clock text-danger mh5"></i>
						{{ $user->alloc_time }}
					</div>
				@endif
			</div>
		</div>
	</div>

	<!-- !END Activities -->

	<hr>

	<!-- User Role -->
	<div class="role ph25">
		<div class="row">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.role_slug') }}
				</b>
			</div>
			<div class="col-sm-9">
				@foreach($user->roles as $role)
					<label id="" class="ph5 img-rounded bg"
						   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
			<span class="f10">
			<i class="fa fa-{{ $role->spreadMeta()->icon }} mhl5"></i>
				@if($user->is_a($role->slug))
					<a href="javascript:void" onclick='masterModal("{{url('manage/users/act/'.$hashID."/permits/".$role->slug)}}")'>
				{{ $role->title }}
				</a>
				@else
					{{ $role->title }}
				@endif
			</span>
					</label>
				@endforeach
			</div>
		</div>
	</div>
	<!-- !END User Role -->

	<hr>

	<!-- Log -->
	<div class="log ph25">
		<div class="row mv5">
			<div class="col-sm-3">
				<b>
					{{ trans('ehdamanage::ehda.modal.registration') }}
				</b>
			</div>
			<div class="col-sm-4">
				{{ ad(echoDate($user->created_at,"%d %B  %Y H:i")) }}
			</div>
		</div>
		<div class="row mv5">
			<div class="col-sm-3">
				<b>
					{{ trans('ehdamanage::ehda.modal.update') }}
				</b>
			</div>
			<div class="col-sm-4">
				{{ ad(echoDate($user->updated_at,"%d %B  %Y H:i")) }}
			</div>
			<div class="col-sm-5">
				<span class="text-muted">
				{{ trans('ehdamanage::ehda.modal.by',['person'=> model('user',$user->updated_by)->full_name ]) }}
				</span>
			</div>
		</div>
	</div>
	<!-- !END Log -->

</div>
<!-- !END Modal Body -->

<!-- Modal Footer -->
@if(user()->ehdaCanEdit())
	<div class="modal-footer">
		{!!
			widget('button')
			->label(trans('ehdamanage::forms.button.edit'))
			->target(url('manage/volunteers/edit/'.$hashID))
			->class('pull-right btn-taha')
		 !!}
	</div>
@endif
<!-- !END Modal Footer -->

