@include("manage::widgets.grid-text" , [
	'text' => $model->full_name,
	'class' => "font-yekan" ,
	'size' => "16" ,
	"link" => "modal:manage/volunteers/act/$model->hashid/profile" ,
]     )

@include('ehdaManage::volunteers.browse.row-card-badge')

@include("manage::widgets.grid-tiny" , [
	'text' => "$model->id|$model->hashid" ,
	'color' => "gray" ,
	'icon' => "bug",
	'class' => "font-tahoma" ,
	'locale' => "en" ,
]     )
