@if($model->card_registered_at)

	@include("manage::widgets.grid-badge" , [
		"text" => trans_safe("ehdaManage::ehda.donation_card") ,
		'icon' => "credit-card" ,
		"link" => user()->as('admin')->can('users-card-holder.view')? "modal:manage/cards/act/-hashid-/view" : null,
		'class' => "text-success" ,
	])

@else

	@include("manage::widgets.grid-badge" , [
		"text" => trans_safe("ehdaManage::ehda.without_donation_card") ,
		'icon' => "credit-card" ,
		"color" => "danger" ,
	])

@endif