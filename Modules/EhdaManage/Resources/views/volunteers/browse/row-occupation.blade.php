@include("manage::widgets.grid-text" , [
	'text' => $model->job,
	'div_class' => "mv5" ,
])



@include("manage::widgets.grid-tiny" , [
	"condition" => boolval($model->home_city) ,
	'text' => model('state',$model->home_city)->full_name,
	'icon' => "map-marker" ,
	'div_class' => "mv10" ,
])
