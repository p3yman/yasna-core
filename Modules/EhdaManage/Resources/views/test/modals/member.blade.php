{!!
    widget('Modal')
    ->label(trans('ehdamanage::ehda.modal.title.member'))
    ->id('volunteers-info')
 !!}

<!-- Modal Body -->
<div class="modal-body">
	<!-- Title -->
	<div class="title ph25">
		<div class="row">
			<div class="col-sm-6">
				<h4 class="">
					<i class="fa fa-user mr-sm"></i>
					{{ $user['full_name'] }}
				</h4>
				<div class="mb-lg">
					<i class="fa fa-id-card mr-sm"></i>
					{{ trans('ehdamanage::ehda.modal.card_number', ["number" => $user['card_number'] ]) }}
				</div>
				<div class="text-muted">
					<small>
						<i class="fa fa-envelope mr-sm"></i>
						{{ $user['email'] }}
					</small>
				</div>
				<div class="text-muted">
					<small>
						<i class="fa fa-phone mr-sm"></i>
						{{ pd($user['phone_number']) }}
					</small>
				</div>
				<div class="mt-lg">
					<label id="" class="ph5 img-rounded"
						   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
					<span class="f10">
									<i class="fa fa-check mhl5"></i>
								سفیر اهدا عضو
					</span>
					</label>

					<label id="" class="ph5 img-rounded bg-success"
						   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
						<a href="javascript:void(0)" class="f10">
							<i class="fa fa-globe mhl5"></i>
							کارت اهدا عضو
						</a>
					</label>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card" style="max-width: 450px; margin: auto;">
					<img src="{{ $user['card_image'] }}" alt="card" width="100%">
				</div>
			</div>
		</div>
	</div>
	<!-- !END Title -->

	<hr>

	<!-- Personal Information -->
	<div class="personal-info ph25">
		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.code_melli').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ pd($user['code_melli']) }}
			</div>
			<div class="col-sm-6">
				<span class="text-muted">
					{{  "(".trans('validation.attributes.code_id').": ".pd($user['code_melli']).")" }}
				</span>
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.birthday').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ pd($user['birthday']['date']) }}
			</div>
			<div class="col-sm-6">
				{{  $user['birthday']['location'] }}
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.education').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ $user['education']['degree'] }}
			</div>
			<div class="col-sm-6">
				{{ $user['education']['major'] }}
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.home_city').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{trans('validation.attributes.tel').": "}}
				<span style="direction: ltr;">
					{{ ad($user['phone_number']) }}
				</span>
			</div>
			<div class="col-sm-6">
				{{ $user['address'] }}
			</div>
		</div>
	</div>
	<!-- !END Personal Information -->

	<hr>

	<!-- Log -->
	<div class="log ph25">
		<div class="row mv5">
			<div class="col-sm-3">
				<b>
					{{ trans('ehdamanage::ehda.modal.registration').":" }}
				</b>
			</div>
			<div class="col-sm-4">
				{{ $user['registration']['date'] }}
			</div>
			<div class="col-sm-5">
				{!!
				    widget('button')
				    ->label("[".trans('ehdamanage::forms.button.delete')."]")
				    ->shape('link')
				    ->class('text-danger')
				    ->style('color:#f05050;')
				    ->onClick('openPanel(".js-deletePanel")')
				 !!}
			</div>
		</div>
		<div class="row mv5">
			<div class="col-sm-3">
				<b>
					{{ trans('ehdamanage::ehda.modal.update').":" }}
				</b>
			</div>
			<div class="col-sm-4">
				{{ $user['update']['date'] }}
			</div>
			<div class="col-sm-5">
				<span class="text-muted">
				{{ trans('ehdamanage::ehda.modal.by',['person'=> $user['update']['by'] ]) }}
				</span>
			</div>
		</div>
	</div>
	<!-- !END Log -->

</div>
<!-- !END Modal Body -->

<!-- Modal Footer -->
<div class="modal-footer">
	<div class="clearfix">
		{!!
			widget('button')
			->label(trans('ehdamanage::forms.button.edit'))
			->class('pull-right btn-taha')
		 !!}

		{!!
			widget('button')
			->label(trans('ehdamanage::ehda.cards.print'))
			->shape('primary')
			->class('pull-right btn-taha mr')
			->onClick('openPanel(".js-printPanel")')
		 !!}
	</div>

	<div class="panel mv10 panel-primary js-printPanel noDisplay">
		<div class="panel-heading">
			{{ trans('ehdamanage::ehda.printings.send_to') }}
		</div>
		<div class="panel-body">
			{!!
			    widget('formOpen')
			 !!}

			<div class="row">
				<div class="col-sm-8">
					{!!
						widget('combo')
						->options(['گزینه اول','گزینه دوم'])
					 !!}
				</div>
				<div class="col-sm-4">
					{!!
					    widget('button')
					    ->label(trans('ehdamanage::ehda.printings.send_to'))
					    ->shape('primary')
					    ->class('btn-block')
					 !!}
				</div>
			</div>
			{!!
			    widget('feed')
			 !!}

			{!!
			    widget('formClose')
			 !!}
		</div>
	</div>

	<div class="panel mv10 panel-danger js-deletePanel noDisplay">
		<div class="panel-heading">
			{{ trans('ehdamanage::ehda.cards.delete') }}
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-8">
					{{ trans('ehdamanage::ehda.cards.delete_warning_in_manage') }}
				</div>
				<div class="col-sm-4">
					{!!
					    widget('button')
					    ->label(trans('ehdamanage::ehda.cards.delete'))
					    ->shape('danger')
					    ->class('btn-block')
					 !!}
				</div>
			</div>
		</div>
	</div>
</div>
<!-- !END Modal Footer -->

<script>
	function openPanel(panel) {
		$(panel).slideToggle()
    }
</script>