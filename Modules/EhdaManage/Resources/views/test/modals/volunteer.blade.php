{!!
    widget('Modal')
    ->label(trans('ehdamanage::ehda.modal.title.volunteer'))
    ->id('volunteers-info')
 !!}

<!-- Modal Body -->
<div class="modal-body">
	<!-- Title -->
	<div class="title ph25">
		<h4 class="mb-lg">
			<i class="fa fa-user mr-sm"></i>
			{{ $user['full_name'] }}
		</h4>
		<div class="text-muted">
			<small>
				<i class="fa fa-envelope mr-sm"></i>
				{{ $user['email'] }}
			</small>
		</div>
		<div class="text-muted">
			<small>
				<i class="fa fa-phone mr-sm"></i>
				{{ pd($user['phone_number']) }}
			</small>
		</div>
		<div class="mt-lg">
			<label id="" class="ph5 img-rounded"
				   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
					<span class="f10">
									<i class="fa fa-check mhl5"></i>
								سفیر اهدا عضو
					</span>
			</label>

			<label id="" class="ph5 img-rounded bg-success"
				   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
				<a href="javascript:void(0)" class="f10">
					<i class="fa fa-globe mhl5"></i>
					کارت اهدا عضو
				</a>
			</label>
		</div>
	</div>
	<!-- !END Title -->

	<hr>

	<!-- Personal Information -->
	<div class="personal-info ph25">
		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.code_melli').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ pd($user['code_melli']) }}
			</div>
			<div class="col-sm-6">
				<span class="text-muted">
					{{  "(".trans('validation.attributes.code_id').": ".pd($user['code_melli']).")" }}
				</span>
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.birthday').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ pd($user['birthday']['date']) }}
			</div>
			<div class="col-sm-6">
				{{  $user['birthday']['location'] }}
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.education').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{ $user['education']['degree'] }}
			</div>
			<div class="col-sm-6">
				{{ $user['education']['major'] }}
			</div>
		</div>

		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.home_city').":" }}
				</b>
			</div>
			<div class="col-sm-3">
				{{trans('validation.attributes.tel').": "}}
				<span style="direction: ltr;">
					{{ ad($user['phone_number']) }}
				</span>
			</div>
			<div class="col-sm-6">
				{{ $user['address'] }}
			</div>
		</div>
	</div>
	<!-- !END Personal Information -->

	<hr>

	<!-- Activities -->
	<div class="activities ph25">
		<div class="row mv10">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.activities').":" }}
				</b>
			</div>
			<div class="col-sm-4 mv10">
				@if($user['activity']['content'] > 0)
					<div class="mv5">
						<i class="fa fa-clipboard text-info"></i>
						<span class="ph5">{{ pd($user['activity']['content'] ) }}</span>
						{{ "مشارکت در تولید و نشر محتوا" }}
					</div>
				@endif
				@if($user['activity']['content'] > 0)
					<div class="mv5">
						<i class="fa fa-credit-card text-info"></i>
						<span class="ph5">{{ pd($user['activity']['registration']) }}</span>
						{{ "مشارکت در ثبت‌نام کارت اهدا" }}
					</div>
				@endif
				@if($user['activity']['content'] > 0)
					<div class="mv5">
						<i class="fa fa-print text-info"></i>
						<span class="ph5">{{ pd($user['activity']['print']) }}</span>
						{{ "مشارکت در چاپ کارت اهدا" }}
					</div>
				@endif
			</div>
			<div class="col-sm-5 mv10">
				<div class="activities">
					@foreach($user['other_activities'] as $activity)
						<div class="mv5">
							<i class="fa fa-check text-primary mh5"></i>
							{{ $activity }}
						</div>
					@endforeach
				</div>
				<div class="time">
					<i class="fa fa-clock text-danger mh5"></i>
					{{ $user['active_time'] }}
				</div>
			</div>
		</div>
	</div>
	<!-- !END Activities -->

	<hr>

	<!-- User Role -->
	<div class="role ph25">
		<div class="row">
			<div class="col-sm-3">
				<b>
					{{ trans('validation.attributes.role_slug').": " }}
				</b>
			</div>
			<div class="col-sm-9">
				@foreach($user['roles'] as $role)
				<label id="" class="ph5 img-rounded bg-{{ $role['color'] }}"
					   style="opacity:0.8;padding-top: 2px;padding-bottom: 2px;border: 1px solid rgba(30, 30, 30, 0.2)">
					<span class="f10">
						<i class="fa fa-{{ $role['icon'] }} mhl5"></i>
						{{ $role['title'] }}
					</span>
				</label>
				@endforeach
			</div>
		</div>
	</div>
	<!-- !END User Role -->

	<hr>

	<!-- Log -->
	<div class="log ph25">
		<div class="row mv5">
			<div class="col-sm-3">
				<b>
					{{ trans('ehdamanage::ehda.modal.registration').":" }}
				</b>
			</div>
			<div class="col-sm-4">
				{{ $user['registration']['date'] }}
			</div>
		</div>
		<div class="row mv5">
			<div class="col-sm-3">
				<b>
					{{ trans('ehdamanage::ehda.modal.update').":" }}
				</b>
			</div>
			<div class="col-sm-4">
				{{ $user['update']['date'] }}
			</div>
			<div class="col-sm-5">
				<span class="text-muted">
				{{ trans('ehdamanage::ehda.modal.by',['person'=> $user['update']['by'] ]) }}
				</span>
			</div>
		</div>
	</div>
	<!-- !END Log -->

</div>
<!-- !END Modal Body -->

<!-- Modal Footer -->
<div class="modal-footer">
	{!!
	    widget('button')
	    ->label(trans('ehdamanage::forms.button.edit'))
	    ->class('pull-right btn-taha')
	 !!}
</div>
<!-- !END Modal Footer -->

