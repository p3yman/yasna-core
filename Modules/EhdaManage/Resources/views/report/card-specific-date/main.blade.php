{!!
    widget('Modal')
    	->label(trans('ehdamanage::ehda.cards.specific_date'))
    	->target(route('ehda-manage.report.cards.submit'))
    	->id('card-report')
!!}

<!-- Modal Body -->
<div class="modal-body">
	<div class="row">
		<div class="col-xs-12">
			<div class="row">
				<div class="col-lg-6">
					{!!
						widget('persian-date-picker')
							->name('date')
							->label(trans('ehdamanage::report.since'))
							->inForm()
							->max(\Carbon\Carbon::now())
							->withoutTime()
							->id('date')
					!!}
				</div>

				<div class="col-lg-6">
					{!!
						widget('combo')
							->name('register_method')
							->label(trans('ehdamanage::ehda.cards.register-method'))
							->inForm()
							->blankValue('')
							->blankCaption(trans('ehdamanage::report.all'))
							->options($register_method_options)
					!!}
				</div>

				<div class="col-lg-6">
					{!!
						widget('combo')
							->name('report_type')
							->label(trans('ehdamanage::report.report-type'))
							->inForm()
							->options([
								[
									'id' => 'daily',
									'title' => trans('ehdamanage::report.daily')
								],
								[
									'id' => 'hourly',
									'title' => trans('ehdamanage::report.hourly')
								],
							])
					!!}
				</div>

				<div class="col-xs-12">
					{!! widget('feed') !!}
				</div>
			</div>
		</div>
	</div>

	<div class="row" id="log-result-container" style="display: none">
		<div class="col-xs-12">
			<table id="log-result-table" class="table">
				<thead>
				<tr>
					<td>
						{{ trans('ehdamanage::event-report.date') }}
					</td>
					<td>
						{{ trans('ehdamanage::event-report.total-register') }}
					</td>
					<td>
						{{ trans('ehdamanage::event-report.prints') }}
					</td>
				</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

</div>
<!-- !END Modal Body -->


<!-- Modal Footer -->
<div class="modal-footer">
	{!!
		widget('button')
			->type('submit')
			->label(trans('ehdamanage::report.report'))
			->inForm()
			->color('info')
	!!}

	{!!
	    widget('button')
	    ->label(trans('manage::forms.button.cancel'))
	    ->target('$(".modal").modal("hide")')
	    ->shape('link')
	    ->id('btnCancel')
	!!}
</div>
<!-- !END Modal Footer -->


<script>
    function showLog(logs) {
        $('#log-result-container').slideUp();

        $('#log-result-table tbody').html('');
        $.each(logs, function (index, item) {
            let tr = $('<tr></tr>')
                .append('<td>' + item['date'] + '</td>')
                .append('<td>' + item['cards'] + '</td>')
                .append('<td>' + item['prints'] + '</td>');
            $('#log-result-table tbody').append(tr);
        });

        $('#log-result-container').slideDown();
    }
</script>