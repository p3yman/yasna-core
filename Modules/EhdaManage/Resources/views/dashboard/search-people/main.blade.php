{!!
	widget('form-open')
		->target(EhdaManageRouteTools::action('HomeController@searchPeople'))
		->method('get')
		->class('js')
!!}

{!!
	widget('input')
		->name('keyword')
		->placeholder(trans('ehdamanage::people.smart_finder_placeholder'))
!!}

{!!
	widget('button')
		->label(trans('ehdamanage::forms.button.find'))
		->type('submit')
		->shape('primary')
		->class('w100 mv5')
		->inForm()
!!}

{!!
	widget('feed')
!!}

{!!
	widget('form-close')
!!}
