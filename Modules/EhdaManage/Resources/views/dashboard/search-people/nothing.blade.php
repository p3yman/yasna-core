@include("manage::widgets.grid-text" , [
	'text' => $field =='code_melli' ? trans("ehdamanage::people.code_melli_not_found") : trans("ehdamanage::people.nobody_found"),
	'color' => "danger" ,
	'icon' => "exclamation-triangle" ,
]     )

@if($field=='code_melli')
	<div class="w100 text-center">
		@if(user()->as('admin')->can('users-card-holder.create'))
			<a href="{{ url("manage/cards/create/$keyword") }}"
			   class="btn btn-default w70">{{ trans("ehdamanage::ehda.cards.create") }}</a>
		@endif
		@if( user()->userRolesArray('create' , [] , model('role')::adminRoles()) )
			<a href="{{ url("manage/volunteers/create/admin/$keyword") }}"
			   class="btn btn-default w70">{{ trans("ehdamanage::ehda.volunteers.create") }}</a>
		@endif

			@include("ehdamanage::dashboard.search-people.inquiry")

	</div>
@endif
