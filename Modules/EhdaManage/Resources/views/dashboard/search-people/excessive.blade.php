<div style="margin-top: 10px">
	@include("manage::widgets.grid-text" , [
		'text' => trans("ehdamanage::forms.feed.all_count_search_results" , [
			'count' => $total_found ,
		]),
		'size' => "10" ,
		'link' => 'url:' . url("manage/users/browse/all/search/$keyword") ,
		'icon' => "caret-left" ,
		'condition' => user()->as('admin')->can('users-all') ,
	])
</div>
