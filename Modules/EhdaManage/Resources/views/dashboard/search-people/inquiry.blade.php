@include("manage::widgets.grid-text" , [
	'text' => trans("ehdamanage::people.commands.inquiry"),
	'icon' => "assistive-listening-systems" ,
	'class' => "btn btn-default w70" ,
	"color" => "white" ,
	'size' => "12" ,
	"condition" => dev() and module('ehdaManage')->cardsController()->checkCodeMelli($keyword) ,
	'link' => "modal:manage/ehda/act/inquiry/$keyword" ,
])