@include("ehdamanage::dashboard.search-people.show-name")
@include("ehdamanage::dashboard.search-people.show-card")
@include("ehdamanage::dashboard.search-people.show-volunteer")

@if(user()->is_a('manager'))
	@include("ehdamanage::dashboard.search-people.show-roles")
@endif

@if(dev())
	@include("ehdamanage::dashboard.search-people.show-login")
	@include("ehdamanage::dashboard.search-people.inquiry")
	@include("ehdamanage::dashboard.search-people.show-id")
@endif