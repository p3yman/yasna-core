@if($model->is_a('card-holder'))
	@include("manage::widgets.grid-text" , [
		'text' => trans("ehdamanage::ehda.donation_card"),
		'icon' => "credit-card" ,
		'color' => "success" ,
		'class' => "btn btn-default w70" ,
		'size' => "12" ,
		'link' => user()->as('admin')->can('users-card-holder.view')
			? "modal:manage/cards/act/-hash_id-/profile"
			: v0() ,
	])

@elseif(user()->as('admin')->can_any(['users-card-holder.create' , 'card.create']))
	@include("manage::widgets.grid-text" , [
		'text' => trans("ehdamanage::ehda.cards.register_full"),
		'icon' => "credit-card" ,
		'class' => "btn btn-default w70" ,
		'size' => "12" ,
		'link' => "url:manage/cards/create/$model->code_melli" ,
	])
@endif
