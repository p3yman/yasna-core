@include("manage::widgets.grid-text" , [
	'text' => trans("ehdamanage::ehda.volunteers.single"),
	'icon' => "child" ,
	'color' => "success" ,
	'class' => "btn btn-default w70" ,
	'size' => "12" ,
	'link' => user()->as('admin')->can('volunteer') ? "modal:manage/volunteers/act/-hash_id-/profile" : v0(),
	'condition' => $model->is_admin() ,
])
