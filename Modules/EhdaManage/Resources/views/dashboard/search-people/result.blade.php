@if($total_found == 0)
	@include("ehdamanage::dashboard.search-people.nothing")
@endif

@if($total_found > 0)
	@include("ehdamanage::dashboard.search-people.show")
@endif

@if($total_found > 1)
	@include("ehdamanage::dashboard.search-people.excessive")
@endif