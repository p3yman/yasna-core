@include("manage::widgets.grid-text" , [
	'text' => trans("ehdamanage::people.commands.login_as") ,
	'icon' => "sign-in" ,
	'class' => "btn btn-default w70" ,
	"color" => "white" ,
	'size' => "12" ,
	'link' => "modal:manage/users/act/-hashid-/login" ,
	'condition' => ($model->id != user()->id) and $model->isNotTrashed() ,
])
