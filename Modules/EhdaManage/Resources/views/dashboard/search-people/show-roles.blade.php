@include("manage::widgets.grid-text" , [
	'text' => trans("ehdamanage::people.role_management"),
	'icon' => "cog" ,
	'class' => "btn btn-default w70" ,
	'size' => "12" ,
	'link' => 'modal:manage/users/act/-hashid-/roles' ,
])
