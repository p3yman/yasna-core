@php
	foreach ($ageCategory as $category=>$count){
	   $arrLabel[] = trans("ehdamanage::chart.".$category);
	   $arrData[] = $count;
	}
@endphp

@include('manage::widgets.charts.bar.chartjs-bar',[
	'id'=> 'age-bar',
	'labels'=> $arrLabel,
	'data' => [
		[
			'backgroundColor' => '#23b7e5',
			'borderColor' => '#23b7e5',
			'data' => $arrData
		]
	]
])

