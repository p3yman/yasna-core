@include('manage::widgets.charts.pie.chartjs-pie',[
		'id' => 'volunteer-type',
			'type' => 'pie', // doughnut or pie
			'legend'=>true,
				'labels'=> [
					trans("ehdamanage::chart.enable_volunteer"),
					trans("ehdamanage::chart.waiting_volunteer"),
					trans("ehdamanage::chart.other_volunteers"),
				],
			'data' => [
	[
	'backgroundColor' => ['#f05050', '#7266ba'],
	'hoverBackgroundColor' => ['#f05050', '#7266ba'],
	'data' => [$enables_volunteer, $waiting_volunteer, $other_volunteer]
]
]

])
