@extends('manage::layouts.template')

@section('content')

	@include('ehdamanage::chart.provice-city')

	<div class="row">
		<div class="col-md-4">
			@include('ehdamanage::chart.register-count-in-month')
		</div>

		<div class="col-md-4">
			@include('ehdamanage::chart.age-ctegory')
		</div>

		<div class="col-md-4">
			@include('ehdamanage::chart.events')
		</div>
	</div>

	<div class="row">

	</div>

	{{--pie charts--}}
	<div class="row">
		<div class="col-md-4">
			@include('ehdamanage::chart.gender')
		</div>
		<div class="col-md-4">
			@include('ehdamanage::chart.type-of-users')
		</div>

		<div class="col-md-4">
			@include('ehdamanage::chart.type-of-volunteers')
		</div>
	</div>



@endsection