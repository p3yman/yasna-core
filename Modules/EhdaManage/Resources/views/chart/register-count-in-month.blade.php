@php
    foreach ($mothRegister as $month=>$count){
       $arrLabel[]=ad($month);
       $arrData[] = $count;
     }
@endphp

<div class="col-xs-12 text-left" style="text-align: center; margin-bottom: 15px">
	{!!
		widget('button')
			->target('modal:' . route('ehda-manage.report.cards', [], false))
			->label(trans('ehdamanage::ehda.cards.specific_date'))
	!!}
</div>

		@include('manage::widgets.charts.bar.chartjs-bar',[
            'id'=> 'month-register-bar',
            'labels'=> $arrLabel,
            'data' => [
                [
                    'backgroundColor' => '#23b7e5',
                    'borderColor' => '#23b7e5',
                    'data' => $arrData
                ]
            ]
        ])

