@php
    function random_color_part() {
         return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
      }

  function random_color() {
         return random_color_part() . random_color_part() . random_color_part();
         }

        foreach ($typeOfUsers as $api){
              if ($api['api_info']==="0")
                 $arrLabel[]=trans("ehdamanage::chart.site_users");
              else
                $arrLabel[] = $api['api_info']->name_first;
            $arrData[] = $api['api_users'];
            $arrColor[] = "#".random_color();
        }

@endphp

<div class= "@if(count($arrLabel) <= 3) col-lg-6 mb-lg @else col-lg-10 col-lg-offset-1 mb-lg @endif">
	<div>
		@include('manage::widgets.charts.pie.chartjs-pie',[
				'id' => 'chart-user-type',
			'type' => 'pie', // doughnut or pie
			'labels'=> $arrLabel,
			'legend' => true,
			'data' => [
  			  [
    		    'backgroundColor' => $arrColor,
     		   'hoverBackgroundColor' => $arrColor,
      			  'data' => $arrData
   				 ]
			]

		])
	</div>

	<label  class="label label-primary">{{trans('ehdamanage::chart.user-type')}}</label>
</div>


