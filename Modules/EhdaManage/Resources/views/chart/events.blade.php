@php
	foreach ($events as $title=>$count){
	   $arrLabel[]=str_limit($title,10);
	   $arrData[]=$count;
	}
@endphp


@include('manage::widgets.charts.bar.chartjs-bar',[
	'id'=> 'event-bar',
	'labels'=> $arrLabel,
	'data' => [
		[
			'backgroundColor' => '#23b7e5',
			'borderColor' => '#23b7e5',
			'data' => $arrData
		]
	]
])
