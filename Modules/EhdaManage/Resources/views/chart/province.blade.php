@php
  foreach ($provinceUsers as $key=>$value){
    $lables[]=$key;
    $data[]=$value;
}
@endphp

<div class="row">
	<div class="col-md-4">
					<div class="form-group">
						<label for="city-input">{{trans('ehdamanage::report.home_province')}}</label>
						{!!
						 widget('combo')
						->id('home-provinces-input')
						->name('home-provinces')
						->options($provinces)
						->searchable()
						->valueField('id')
						->captionField('title')
						->blankValue('')
						->blankCaption(trans('ehdamanage::chart.province-combo'))
						!!}
					</div>
				</div>
	<div id="provin-chart" class="p-lg">
				@include('manage::widgets.charts.bar.chartjs-bar',[
                 'id'=> 'province-bar',
           	     "parent_style" => "height: 450px; padding: 10px 0;" ,
                 'labels'=> $lables,
                  'data' => [
                   [
                        'backgroundColor' => '#23b7e5',
                        'borderColor' => '#23b7e5',
                       'data' => $data
                  ]
           	    ],
           	    "options" => [
           	    	"maintainAspectRatio" => false ,
           	    	"scales" => [
           	    		"xAxes" => [
							[
								"ticks" => [
									"stepSize" => "1" ,
									"autoSkip" => false ,
								] ,
							],
           	    		] ,
           	    	] ,
           	    ] ,
        ])
	</div>

</div>
