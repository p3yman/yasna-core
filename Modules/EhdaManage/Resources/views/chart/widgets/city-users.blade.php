@include('manage::widgets.charts.bar.chartjs-bar',[
	'id'=> 'province-bar',
	"parent_style" => "height: 450px; padding: 10px 0;" ,
	'labels'=> $lables,
  	'data' => [
		[
			'backgroundColor' => '#23b7e5',
			'borderColor' => '#23b7e5',
			'data' => $data
		]
	],
	"options" => [
		"maintainAspectRatio" => false ,
		"scales" => [
			"xAxes" => [
				[
					"ticks" => [
						"stepSize" => "1" ,
						"autoSkip" => false ,
					] ,
				],
			] ,
		] ,
	] ,
])



