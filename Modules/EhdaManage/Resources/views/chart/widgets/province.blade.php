@php( $provinceData = module('ehdamanage')->callControllerMethod('ChartController','provinceCardsData') )
<div id="province">
	@include('ehdamanage::chart.province',$provinceData)
</div>

<script>

    $(document).ready(function () {

        $('#home-provinces-input').change(function () {
            if ($(this).val() === ""){
                divReload('divWidget-province-city-chart-body');
            } else{
                $("#provin-chart")
                    .addClass("whirl")
                    .load("{{url('manage/ehda/chart/city_users/')}}" + "/" + $(this).val(), function () {
                        $(this).removeClass("whirl");
                    });
            }

        });
    })

</script>