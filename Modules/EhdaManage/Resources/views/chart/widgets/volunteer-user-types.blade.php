@php($data = module('ehdamanage')->callControllerMethod('ChartController','typeOfVolunteer'))
@php($userData=module('ehdamanage')->callControllerMethod('ChartController','typeOfUserData'))

<div class="row">
	@include('ehdamanage::chart.type-of-users',$userData)
	<div class="@if(count($userData['typeOfUsers']) <= 3) col-lg-6 mb-lg @else col-lg-10 col-lg-offset-1 mb-lg @endif">
		<div>
			@include('ehdamanage::chart.type-of-volunteers',$data)
		</div>

		<label class="label label-primary">{{trans('ehdamanage::chart.volunteer_chart')}}</label>
	</div>
</div>



