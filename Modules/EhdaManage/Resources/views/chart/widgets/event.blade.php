@php($data=module('ehdamanage')->callControllerMethod('ChartController','eventsRegistrable'))

@if(empty($data['events']))
	@include('ehdamanage::chart.widgets.empty-result',['message'=>trans('ehdamanage::report.event-empty')])
@else
	@include('ehdamanage::chart.events',$data)
@endif