@php( $genderData = module('ehdamanage')->callControllerMethod('ChartController','genderData') )
@php( $ageData = module('ehdamanage')->callControllerMethod('ChartController','ageCategoryData') )

<div class="col-md-4">
	@include('ehdamanage::chart.gender',$genderData)
</div>

<div class="col-md-8">
	@include('ehdamanage::chart.age-category',$ageData)
</div>