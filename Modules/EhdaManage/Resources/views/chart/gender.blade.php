@php

@endphp
	<div class="panel-body">
		@include ('manage::widgets.charts.pie.chartjs-pie',[
			'id' => 'chart-gender',
			'type' => 'doughnut', // doughnut or pie
			'legend' => true,
			'labels'=> [trans("ehdamanage::chart.man"), trans("ehdamanage::chart.women")],
			'data' => [
  			  [
      			  'backgroundColor' => ['#f05050', '#7266ba'],
      			  'hoverBackgroundColor' => ['#f05050', '#7266ba'],
      			  'data' => [$menCount,$womenCount]
    			]
			]
		])
	</div>
