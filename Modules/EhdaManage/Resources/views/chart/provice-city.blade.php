<script src="{{Module::asset('ehdamanage:js/chart/province.js')}}"></script>
@php
  foreach ($provinceUsers as $key=>$value){
    $lables[]=$key;
    $data[]=$value;
}
@endphp
@include('manage::widgets.charts.bar.chartjs-bar',[
            'id'=> 'province-bar',
            'labels'=> $lables,
            'data' => [
                [
                    'backgroundColor' => '#23b7e5',
                    'borderColor' => '#23b7e5',
                    'data' => $data
                ]
            ]
        ])