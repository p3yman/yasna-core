<?php
/**
 * @var \App\Models\User                         $model
 * @var \Illuminate\Database\Eloquent\Collection $events
 * @var string                                   $editor_mode
 * @var array                                    $states
 */

widget("formOpen")
     ->id('frmEditor')
     ->target('name:card-save')
     ->class('js')
     ->validation(!debugMode())
     ->render()
;

/*-----------------------------------------------
| Hidden Id ...
*/
widget("hidden")
     ->name('hashid')
     ->value($model->hashid)
     ->render()
;

/*-----------------------------------------------
| Code Melli ...
*/
widget("input")
     ->name('code_melli')
     ->value($model->code_melli)
     ->inForm()
     ->disabled()
     ->render()
;

widget("hidden")
     ->name('code_melli')
     ->value($model->code_melli)
     ->render()
;


/*-----------------------------------------------
| event_id when form is in edit mode ...
*/
if ($editor_mode == 'create') {
    echo view("ehdamanage::cards.editor.form-event", compact('model', 'events', 'editor_mode'));
}

/*-----------------------------------------------
| Gender ...
*/
widget("combo")
     ->name('gender')
     ->value($model->gender)
     ->blankValueIf(!$model->id, '0')
     ->blankCaptionIf(!$model->id, ' ')
     ->required()
     ->options(module('ehdaManage')->CardsController()->genderCombo())
     ->inForm()
     ->render()
;

/*-----------------------------------------------
| Basic Information...
*/
widget("input")
     ->name('name_first')
     ->value($model->name_first)
     ->required()
     ->inForm()
     ->render()
;

widget("input")
     ->name('name_last')
     ->value($model->name_last)
     ->required()
     ->inForm()
     ->render()
;

widget("input")
     ->name('name_father')
     ->value($model->name_father)
     ->inForm()
     ->render()
;

widget("input")
     ->name("code_id")
     ->value($model->code_id)
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Birth Date and City ...
*/
widget("combo")
     ->name('birth_city')
     ->value($model->birth_city)
     ->options($states)
     ->searchable()
     ->inForm()
     ->blankValue(0)
     ->searchPlaceholder("tr:manage::forms.button.search")
     ->render()
;

widget("date-picker")
     ->name('birth_date')
     ->required()
     ->value($model->birth_date)
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Job and Education ...
*/
widget("input")
     ->name('job')
     ->value($model->job)
     ->inForm()
     ->render()
;

widget("combo")
     ->name('edu_level')
     ->blankValue('')
     ->value($model->edu_value)
     ->inForm()
     ->options(module('ehdaManage')->CardsController()->eduCombo())
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Contact Info ...
*/
widget("input")
     ->name('home_tel')
     ->value($model->home_tel)
     ->ltr()
     ->inForm()
     ->render()
;

widget("input")
     ->name('mobile')
     ->value($model->mobile)
     ->ltr()
     ->inForm()
     ->required()
     ->render()
;

widget("separator")->render();

widget("combo")
     ->name('home_city')
     ->value($model->home_city)
     ->blankValue('')
     ->options($states)
     ->searchable()
     ->searchPlaceholder("tr:manage::forms.button.search")
     ->required()
     ->inForm()
     ->render()
;

widget("input")
     ->name('email')
     ->value($model->email)
     ->ltr()
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Edu Field and City ...
*/
widget("input")
     ->name('edu_field')
     ->value($model->edu_field)
     ->inForm()
     ->render()
;

widget("combo")
     ->name('edu_city')
     ->value($model->edu_city)
     ->blankValue(0)
     ->options($states)
     ->searchable()
     ->inForm()
     ->searchPlaceholder("tr:manage::forms.button.search")
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Address ...
*/
widget("textarea")
     ->name('home_address')
     ->value($model->home_address)
     ->inForm()
     ->autoSize()
     ->rows(1)
     ->render()
;

widget("input")
     ->name('home_postal')
     ->value($model->home_postal)
     ->ltr()
     ->inForm()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| News Letter ...
*/
widget("group")
     ->start()
     ->label("tr:validation.attributes.newsletter")
     ->render()
;

widget("checkbox")
     ->name("newsletter")
     ->value($model->newsletter)
     ->label("tr:ehdaManage::ehda.newsletter_membership")
     ->render()
;

widget("group")
     ->stop()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| Password ...
*/
widget("group")
     ->start()
     ->label("tr:validation.attributes.password")
     ->render()
;

if ($model->id) {
    echo view("ehdamanage::cards.editor.form-password-edit");
} else {
    echo view("ehdamanage::cards.editor.form-password-create");
}

widget("group")
     ->stop()
     ->render()
;

widget("separator")->render();

/*-----------------------------------------------
| event_id when form is in edit mode ...
*/
if ($editor_mode == 'edit') {
    echo view("ehdamanage::cards.editor.form-event", compact('model', 'events', 'editor_mode'));
}

/*-----------------------------------------------
| Save ...
*/
echo view("ehdamanage::cards.editor.form-buttons");

widget("formClose")->render();