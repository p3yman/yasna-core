<div class="row w70">
	<div class="col-md-6">
		@include("ehdamanage::cards.editor.show-card")
	</div>
	<div class="col-md-6">
		@include("ehdamanage::cards.editor.show-buttons")
	</div>
</div>