<?php
/**
 * @var \App\Models\User                         $model
 * @var \Illuminate\Database\Eloquent\Collection $events
 * @var string                                   $editor_mode
 * @var array                                    $states
 */

widget("formOpen")
     ->id('frmEditor')
     ->target('name:card-save-for-a-volunteer')
     ->class('js')
     ->validation(!debugMode())
     ->render()
;

widget("hidden")
     ->name('hashid')
     ->value($model->hashid)
     ->render()
;

widget("input")
     ->name('code_melli')
     ->value($model->code_melli)
     ->inForm()
     ->disabled()
     ->render()
;

widget("input")
     ->name('name_first')
     ->value($model->full_name)
     ->inForm()
	 ->disabled()
     ->render()
;

echo view("ehdamanage::cards.editor.form-event", compact('model', 'events', 'editor_mode'));

echo view("ehdamanage::cards.editor.form-buttons");
widget("formClose")->render();