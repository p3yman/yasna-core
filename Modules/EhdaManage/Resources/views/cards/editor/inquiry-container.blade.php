<div class="panel panel-primary margin-auto w70">
	<div class="panel-heading">
		{{ trans_safe("ehdaManage::ehda.cards.inquiry") }}
	</div>
	<div class="panel-body">
		@include('ehdamanage::cards.editor.inquiry')
	</div>
</div>