<?php

widget("group")
     ->start()
     ->render()
;

widget("button")
     ->label("tr:manage::forms.button.save")
     ->shape('success')
     ->type('submit')
     ->render()
;

widget("button")
     ->label("tr:ehdaManage::ehda.cards.save_and_send_to_print")
     ->value('print')
     ->shape('primary')
     ->type('submit')
     ->class('mh10')
     ->render()
;

widget("group")
     ->stop()
     ->render()
;


widget('feed')->render();
