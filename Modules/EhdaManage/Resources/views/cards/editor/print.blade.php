@include('manage::layouts.modal-start' , [
	'form_url' => route("card-print"),
	'modal_title' => trans_safe("ehdaManage::ehda.printings.send_to"),
])
<div class='modal-body' style="min-height: 300px">

	{!!
	widget("hidden")
		->name('id')
		->value($model->hashid)
	!!}

	{!!
	widget("input")
		->disabled()
		->name('name_first')
		->value($model->full_name)
		->inForm()
	!!}

	@include("ehdaManage::cards.editor.form-event" , [
		"fake" => $model->from_event_id = module('ehdaManage')->cardsController()->getLastUsedEvent() ,
		"events" => module('ehdaManage')->cardsEditController()->loadEvents() ,
		"editor_mode" => "create" ,  
	])

</div>

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		"save_label" => trans_safe("ehdaManage::ehda.printings.send_to") ,
	])
</div>

@include('manage::layouts.modal-end')