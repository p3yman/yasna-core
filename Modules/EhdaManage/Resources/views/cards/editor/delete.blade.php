{!!
widget("modal")
	->target("name:card-delete")
	->label("tr:ehdaManage::ehda.cards.delete")
!!}

<div class="modal-body">
	{!!
	widget("hidden")
		->value($model->id)
		->name('hashid')
	!!}

	{!!
	widget("input")
		->value($model->full_name)
		->label("tr:validation.attributes.name_first")
		->inForm()
		->disabled()
	!!}
</div>

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
	'save_label' => trans( "ehdaManage::ehda.cards.delete") ,
	'save_shape' => 'danger',
])
</div>