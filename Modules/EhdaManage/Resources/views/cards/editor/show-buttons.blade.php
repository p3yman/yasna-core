{{--
|--------------------------------------------------------------------------
| Print Button
|--------------------------------------------------------------------------
|
--}}

<div>
	{!!
	widget("button")
		->label("tr:ehdaManage::ehda.cards.send_to_print_quee")
		->class("btn-lg w50 m30")
		->shape('info')
		->target("modal:manage/cards/act/$model->hashid/print")
	!!}
</div>

{{--
|--------------------------------------------------------------------------
| Edit Button
|--------------------------------------------------------------------------
|
--}}

<div>
	{!!
	widget("button")
		->label("tr:manage::forms.button.edit")
		->class("btn-lg w50")
		->shape('green')
		->condition($model->ehdaCanEdit())
		->target("url:manage/cards/edit/$model->hashid")
	!!}
</div>

