{!!
widget("combo")
     ->name('from_event_id')
	 ->label("tr:validation.attributes.from_event_id")
     ->value($model->from_event_id)
     ->requiredIf($editor_mode=='create')
     ->options($events)
     ->inForm()
     ->blankValue(0)
     ->blankCaption(' ')
     ->helpIf($editor_mode=='edit' , "tr:ehdaManage::ehda.cards.event_hint_for_card_edits")
!!}