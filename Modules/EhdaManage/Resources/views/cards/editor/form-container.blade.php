<div class="panel panel-primary margin-auto w90">
	<div class="panel-heading">
		{{ trans_safe( "ehdamanage::ehda.cards.$editor_mode" ) }}
	</div>
	<div class="panel-body">
		@include('ehdamanage::cards.editor.form')
	</div>
</div>