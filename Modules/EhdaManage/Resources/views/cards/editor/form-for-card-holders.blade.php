@extends('manage::layouts.template')

@section('content')


		<div class="panel panel-purple margin-auto w90">
			<div class="panel-heading">
				{{ trans_safe( "ehdamanage::ehda.cards.$editor_mode" ) }}
			</div>
			<div class="panel-body">

				<div id="divCard" class="text-center" data-src="manage/cards/act/-id-/show" data-id="">
					@include("ehdamanage::cards.editor.show")
				</div>

			</div>
		</div>


@endsection