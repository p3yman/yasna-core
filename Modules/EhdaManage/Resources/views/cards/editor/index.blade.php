@extends('manage::layouts.template')

@section('content')
	@if(!$model->code_melli)
		<div id="divInquiry">
			@include('ehdamanage::cards.editor.inquiry-container')
		</div>
	@endif

	<div id="divForm" class="{{ $model->code_melli ? '' : 'noDisplay' }}">
		@include("ehdamanage::cards.editor.form-container")
	</div>

	<div id="divCard" class="noDisplay text-center" data-src="manage/cards/act/-id-/show" data-id="">
	</div>

@endsection

@section('html_footer')
	@parent

	{!! Html::script (Module::asset("ehdamanage:js/card-editor.js")) !!}
@endsection