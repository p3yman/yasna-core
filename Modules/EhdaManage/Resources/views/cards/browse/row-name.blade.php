@include("manage::widgets.grid-text" , [
	'text' => $model->full_name,
	'class' => "font-yekan" ,
	'size' => "16" ,
	"link" => "modal:manage/cards/act/-hashid-/profile" ,
]     )

@include("manage::widgets.grid-tiny" , [
	'text' => trans('validation.attributes.card_no').': '.$model->card_no,
	'color' => "darkgray" ,
	'icon' => "credit-card",
	"size" => "12" ,  
	"link" => "modal:manage/cards/act/-hashid-/profile" ,  
]     )


@include("manage::widgets.grid-tiny" , [
	'text' => "$model->id|$model->hashid" ,
	'color' => "gray" ,
	'icon' => "bug",
	'class' => "font-tahoma" ,
	'locale' => "en" ,
]     )
