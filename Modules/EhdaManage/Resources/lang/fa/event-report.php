<?php


return [
     'head'           => 'گزارش‌گیری',
     'button'         => "آمار",
     'date'           => "تاریخ",
     'submits'        => "آمار ثبت نام", //@TODO: @Parsa: 'submit' doesn't really mean that.
     'prints'         => 'تعداد چاپ کارت',
     'total-register' => 'مجموع ثبت‌نام‌ها',
     'total-print'    => 'مجموع چاپ کارت',
];
