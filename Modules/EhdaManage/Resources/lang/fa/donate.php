<?php

return [
     "sidebar"           => " کمک‌های مردمی",
     "price"             => "مبلغ ",
     "transaction-state" => "وضعیت ",
     "transaction-type"  => "نوع ",
     "date"              => "تاریخ ",
     "sent-to-gateway"   => "ارسال به درگاه",
     "verified"          => "پرداخت شده",
     "cancelled"         => "لغو شده",
     "report"            => "گزارش",
     "year"              => "سال",
     "month"             => "ماه",
     "day"               => "روز",
     "hour"              => "ساعت",
     "minute"            => "دقیقه",
     "excel-empty"       => "اطلاعاتی برای نمایش وجود ندارد.",
     "id"                => "شناسه پرداخت",
     "all-state"         => "همه",
     "table-header"      => "لیست کمک‌های مردمی",
     'id-describe'       => "برای انتخاب چند شناسه هم‌زمان می‌توانید از (فاصله) , (-) و یا (,) استفاده کنید. مثال: 23 78",
     'report-btn'        => 'گزارش',
];
