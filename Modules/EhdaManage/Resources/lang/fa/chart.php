<?php


return [
     'user_gender_chart'        => 'نمودار جنسیت اعضا',
     'user-type'                => 'انواع کاربران',
     'volunteer_chart'          => 'انواع سفیرها',
     'age_chart'                => 'رده سنی',
     'register_month_chart'     => ' تعداد ثبت نام در ۱۲ ماه اخیر',
     'city_province_chart'      => ' تعداد کارت عضویت در استان‌های مختلف',
     '0-10'                     => '۰ تا ۱۰',
     '10-20'                    => '۱۰ تا ۲۰',
     '20-30'                    => '۲۰ تا ۳۰',
     '30-40'                    => '۳۰ تا ۴۰',
     '40-50'                    => '۴۰ تا ۵۰',
     '60-...'                   => '۶۰ به بعد',
     'man'                      => "مرد",
     'women'                    => 'زن',
     'site_users'               => 'کاربران سایت',
     'enable_volunteer'         => 'سفیران فعال',
     'all_volunteer'            => 'همه سفیران',
     'waiting_volunteer'        => 'سفیران منتظر تایید',
     'event_chart'              => 'تعداد ثبت نام در ده رویداد آخر',
     'other_volunteers'         => 'سایر سفیران',
     "province-combo"           => "همه",
     "user-age-gender"          => "نمودار جنسیت و رده سنی اعضا",
     "province-city"            => "نمودار تعداد ثبت نام‌های هر استان",
     'volunteer-and-user-types' => 'انواع کاربر براساس نحوه ورود و انواع سفیران ',
];
