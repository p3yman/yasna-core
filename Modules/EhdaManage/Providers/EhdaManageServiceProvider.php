<?php

namespace Modules\EhdaManage\Providers;

use Modules\EhdaManage\Events\ShortMessageReceived;
use Modules\EhdaManage\Events\TooskaUpdatedUserPending;
use Modules\EhdaManage\Http\Middleware\FanapSignMiddleware;
use Modules\EhdaManage\Http\Middleware\MtnUSSDMiddleware;
use Modules\EhdaManage\Listeners\HandleContentWhenShortMessageReceived;
use Modules\EhdaManage\Listeners\RequestBirthDateWhenTooskaUpdatedUserPending;
use Modules\EhdaManage\Providers\Traits\ProviderCardsTrait;
use Modules\EhdaManage\Providers\Traits\ProviderTooskaTrait;
use Modules\Manage\Services\ManageDashboardWidget;
use Modules\Yasna\Services\YasnaProvider;
use Maatwebsite\Excel\ExcelServiceProvider;

class EhdaManageServiceProvider extends YasnaProvider
{
    use ProviderCardsTrait;
    use ProviderTooskaTrait;



    /**
     * Provider Index
     */
    public function index()
    {
        $this->registerProviders();
        $this->registerAliases();
        $this->registerWidgets();
        $this->registerSideBar();
        $this->registerTraits();
        $this->registerHandler();
        $this->registerMiddlewares();
        $this->registerFanapListeners();
        $this->printingIndex();
        $this->tooskaIndex();
        $this->registerDefaultWidgets();
    }



    /**
     * Register Providers
     */
    protected function registerProviders()
    {
        $this->addProvider(RouteServiceProvider::class);
        $this->addProvider(ToolsServiceProvider::class);
        $this->addProvider(ExcelServiceProvider::class);
    }



    /**
     * Register Aliases
     */
    protected function registerAliases()
    {
        $this->addAlias('EhdaManageRouteTools', RouteServiceProvider::class);
        $this->addAlias('EhdaTools', ToolsServiceProvider::class);
    }



    /**
     * Register Dashboard Widgets
     */
    public function registerWidgets()
    {
        module('manage')
             ->service('widgets_handler')
             ->add('ehda-manage')
             ->method('ehdamanage:handleWidgets')
             ->order(90)
        ;

        /*-----------------------------------------------------
        |Chart.js ...
        */
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("manage:libs/vendor/Chart.js/dist/Chart.js")
             ->order(35)
        ;
    }



    /**
     * Handle Dashboard Widgets
     */
    public static function handleWidgets()
    {
        $service = 'widgets';

        module('manage')
             ->service($service)
             ->add('search-people')
             ->blade('ehdamanage::dashboard.search-people.main')
             ->trans('ehdamanage::general.search-people')
             ->color('violet')
             ->icon('users')
             ->order(30)
        ;


        module('manage')
             ->service($service)
             ->add('gender-age')
             ->blade('ehdamanage::chart.widgets.gender-age')
             ->trans('ehdamanage::chart.user-age-gender')
             ->color('purple')
             ->icon('line-chart')
             ->order(31)
        ;

        module('manage')
             ->service($service)
             ->add('province-city-chart')
             ->blade('ehdamanage::chart.widgets.province')
             ->trans('ehdamanage::chart.province-city')
             ->color('green')
             ->icon('bar-chart')
             ->order(32)
        ;

        module('manage')
             ->service($service)
             ->add('register-month')
             ->blade('ehdamanage::chart.widgets.month')
             ->trans('ehdamanage::chart.register_month_chart')
             ->color('warning')
             ->icon('bar-chart')
             ->order(33)
        ;

        module('manage')
             ->service($service)
             ->add('events')
             ->blade('ehdamanage::chart.widgets.event')
             ->trans('ehdamanage::chart.event_chart')
             ->color('danger')
             ->icon('bar-chart')
             ->order(34)
        ;


        module('manage')
             ->service($service)
             ->add('volunteer-types')
             ->blade('ehdamanage::chart.widgets.volunteer-user-types')
             ->trans('ehdamanage::chart.volunteer-and-user-types')
             ->color('primary')
             ->icon('pie-chart')
             ->order(35)
        ;

        module('manage')
             ->service($service)
             ->update('manage-hello')
             ->blade('ehdamanage::hello_widget.index')
        ;
    }



    /**
     *
     */
    public function registerDefaultWidgets()
    {
        ManageDashboardWidget::addDefault('gender-age',3,0,0);
        ManageDashboardWidget::addDefault('province-city-chart',6,0,47);
        ManageDashboardWidget::addDefault('register-month',3,3,25);
        ManageDashboardWidget::addDefault('events',3,3,18);
        ManageDashboardWidget::addDefault('volunteer-types',3,0,27);
    }



    /**
     * Register Sidebar
     */
    public function registerSideBar()
    {
        service('manage:sidebar')
             ->add('report')
             ->blade('ehdamanage::layouts.report-sidebar')
             ->order(90)
        ;
        service('manage:sidebar')
             ->add('donate-report')
             ->blade('ehdamanage::donate.sidebar')
             ->order(30)
        ;
    }



    /**
     * Checks if a given string is compatible with a valid Iranian phone_number format or not.
     *
     * @param $value
     * @param $mood , accepts null, mobile, fixed
     *
     * @return bool
     */
    public static function isPhoneNumber($value, $mood = null)
    {
        if (strlen($value) != 11) {
            return false;
        }
        if (substr($value, 0, 1) != '0') {
            return false;
        }
        if (!ctype_digit($value)) {
            return false;
        }

        switch ($mood) {
            case "mobile":
                if (substr($value, 1, 1) != '9') {
                    return false;
                }
                break;

            case "fixed":
                break;
        }

        return true;
    }



    /**
     * Checks if a given string is compatible with a valid code_melli format or not.
     *
     * @param $value
     *
     * @return bool
     */
    public static function isCodeMelli($value)
    {
        if (!preg_match("/^\d{10}$/", $value)) {
            return false;
        }

        $check = (int)$value[9];
        $sum   = array_sum(array_map(function ($x) use ($value) {
            return ((int)$value[$x]) * (10 - $x);
        }, range(0, 8))) % 11;

        return ($sum < 2 && $check == $sum) || ($sum >= 2 && $check + $sum == 11);
    }



    /**
     * Register Model Traits
     */
    private function registerTraits()
    {
        module('yasna')->service('traits')
                       ->add()->trait('Ehdamanage:UserEhdaManageTrait')->to('User')
        ;
        module('yasna')->service('traits')
                       ->add()->trait('Ehdamanage:PostEhdaManageTrait')->to('Post')
        ;
        module('yasna')->service('traits')
                       ->add()->trait('Ehdamanage:DonationEhdaManageTrait')->to('Donation')
        ;
    }



    /**
     * Register Post Handlers
     */
    public function registerHandler()
    {
        $my_name = 'ehdamanage';
        module('posts')
             ->service('browse_headings_handlers')
             ->add($my_name)
             ->method("$my_name:EventReportController@addColumn")
        ;
    }



    /**
     * Register Middlewares
     */
    protected function registerMiddlewares()
    {
        $middlewares = [
             'fanap-sign' => FanapSignMiddleware::class,
             'mtn-ussd'   => MtnUSSDMiddleware::class,
        ];
        foreach ($middlewares as $alias => $class) {
            $this->addMiddleware($alias, $class);
        }
    }



    /**
     * Register event listeners
     */
    protected function registerFanapListeners()
    {
        $event_listeners = [
             ShortMessageReceived::class     => [
                  HandleContentWhenShortMessageReceived::class,
             ],
             TooskaUpdatedUserPending::class => [
                  RequestBirthDateWhenTooskaUpdatedUserPending::class,
             ],
        ];

        foreach ($event_listeners as $event => $listeners) {
            foreach ($listeners as $listener) {
                $this->listen($event, $listener);
            }
        }
    }



    /**
     * @return string
     */
    public static function cardHolderRoleSlug()
    {
        return 'card-holder';
    }
}
