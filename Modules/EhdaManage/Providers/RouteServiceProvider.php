<?php

namespace Modules\EhdaManage\Providers;

use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var string Namespace of Current Module's Controllers
     */
    protected static $controllersNamespace = '\Modules\EhdaManage\Http\Controllers\\';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public static function action(string $action, ...$otherParameters)
    {
        return action(self::$controllersNamespace . $action, ...$otherParameters);
    }
}
