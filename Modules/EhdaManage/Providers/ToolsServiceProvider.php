<?php

namespace Modules\EhdaManage\Providers;

use Illuminate\Support\ServiceProvider;

class ToolsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function activeGendersCodes()
    {
        return ['1', '2', '3'];
    }

    public static function getActiveGendersCodes()
    {
        return self::activeGendersCodes();
    }

    public static function activeGendersTranses()
    {
        $codes   = self::activeGendersCodes();
        $transes = [];
        foreach ($codes as $code) {
            $transes[$code] = trans("ehdafront::forms.gender.$code");
        }
        return $transes;
    }

    public static function getActiveGendersTranses()
    {
        return self::activeGendersTranses();
    }

    public static function activeEduLevelsCodes()
    {
        return ['0', '1', '2', '3', '4', '5', '6'];
    }

    public static function getActiveEduLevelsCodes()
    {
        return self::activeEduLevelsCodes();
    }

    public static function activeEduLevelsTranses($transSlug = 'edu_level_full')
    {
        $codes   = self::activeEduLevelsCodes();
        $transes = [];
        foreach ($codes as $code) {
            $transes[$code] = trans("ehdafront::people.$transSlug.$code");
        }
        return $transes;
    }

    public static function getActiveEduLevelsTranses()
    {
        return self::activeEduLevelsTranses();
    }

    public static function activeFamiliarizationCodes()
    {
        return ['0', '1', '2', '3', '4'];
    }

    public static function getActiveFamiliarizationCodes()
    {
        return self::activeFamiliarizationCodes();
    }

    public static function activeFamiliarizationTranses()
    {
        $codes   = self::activeFamiliarizationCodes();
        $transes = [];
        foreach ($codes as $code) {
            $transes[$code] = trans("ehdafront::people.familiarization.$code");
        }
        return $transes;
    }


    public static function getStatesCombo($empty = false)
    {
        $data = model('state')->combo();
        if ($empty) {
            array_unshift($data, ['id' => '', 'title' => '']);
        }
        return $data;
    }

    public static function getStatesComboHashid()
    {
        foreach (($states = self::getStatesCombo()) as $key => $state) {
            $states[$key]['originalId'] = $state['id'];
            $states[$key]['id']         = hashid_encrypt($state['id'], 'ids');
        }
        return $states;
    }
}
