<?php namespace Modules\EhdaManage\Providers\Traits;

use Modules\EhdaManage\Events\TooskaCreatedUserPending;
use Modules\EhdaManage\Events\TooskaUpdatedUserPending;
use Modules\EhdaManage\Http\Middleware\CheckTooskaToken;
use Modules\EhdaManage\Listeners\AhvalOnlineQueryWhenNewUserPendingCreated;
use Modules\EhdaManage\Listeners\BehdashtOnlineQueryWhenNewUserPendingCreated;
use Modules\EhdaManage\Listeners\TooskaUpdateListener;
use Modules\EhdaManage\Listeners\RegisterCardWhenUserPendingCompleted;
use Modules\EhdaManage\Schedules\DispatchAhvalOfflinePackageSchedule;
use Modules\EhdaManage\Schedules\RegisterTooskaApprovedUsersSchedule;
use Modules\EhdaManage\Schedules\RemoveWaitingUserPendingRecordsSchedule;
use Modules\EhdaManage\Schedules\RestartTooskaRegistrationsQueueSchedule;

trait ProviderTooskaTrait
{
    /**
     * index to tooska/ahval related things
     */
    public function tooskaIndex()
    {
        $this->registerTooskaListeners();
        $this->registerTooskaSchedules();
        $this->registerTooskaMiddlewares();
        $this->registerTooskaModelTraits();
    }



    /**
     * Middlewares
     */
    private function registerTooskaMiddlewares()
    {
        $this->addMiddleware('checkTooskaToken', CheckTooskaToken::class);
    }



    /**
     * register tooska/ahval listeners
     */
    private function registerTooskaListeners()
    {
        $server_location = env("SERVER_LOCATION");

        if ($server_location == 'internet1') {
            $this->listen(TooskaUpdatedUserPending::class, TooskaUpdateListener::class);
            $this->listen(TooskaCreatedUserPending::class, BehdashtOnlineQueryWhenNewUserPendingCreated::class);
            //$this->listen(TooskaUpdatedUserPending::class, RegisterCardWhenUserPendingCompleted::class);
        }
    }



    /**
     * accessor for trans
     */
    protected function registerTooskaModelTraits()
    {
        module('yasna')->service('traits')
                       ->add()->trait('Ehdamanage:UserTooskaTrait')->to('User')
        ;
    }



    /**
     * tooska/ahval listeners: intranet only
     */
    private function registerAhvalOnlineStarter()
    {
        $this->listen(TooskaCreatedUserPending::class, AhvalOnlineQueryWhenNewUserPendingCreated::class);
    }



    /**
     * register tooska/ahval Schedules
     */
    private function registerTooskaSchedules()
    {
        $server_location = env("SERVER_LOCATION");

        if ($server_location == 'internet1') {
            $this->addSchedule(RemoveWaitingUserPendingRecordsSchedule::class);
            $this->addSchedule(RestartTooskaRegistrationsQueueSchedule::class);
        }
    }



    /**
     * tooska/ahval Schedules: internet only
     */
    private function registerApprovedUsersSchedules()
    {
        $this->addSchedule(RegisterTooskaApprovedUsersSchedule::class);
    }



    /**
     * tooska/ahval Schedules: intranet only
     */
    private function registerAhavalOfflinePackageSchedule()
    {
        $this->addSchedule(DispatchAhvalOfflinePackageSchedule::class);
    }
}
