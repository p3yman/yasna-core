<?php namespace Modules\EhdaManage\Providers\Traits;

trait ProviderCardsTrait
{

    /**
     * A specialized index for printings purposes.
     */
    public function printingIndex()
    {
        $this->redefineUsernameField();
        $this->registerUsersSidebar();
        $this->registerCreateHandlers();
        $this->registerRoleSampleModules();
        $this->registerAccountSettings();
        $this->removeNormalUserCreationButtons();
    }



    /**
     * Registers Users Sidebar
     */
    private function registerUsersSidebar()
    {
        module('manage')
             ->service('sidebar')
             ->add('ehda-users')
             ->blade('ehdamanage::layouts.sidebar')
             ->order(1)
        ;
    }

    /**
     * Removes create buttons on top of yasna users page, so that everybody is forced to use ehda things
     */
    private function removeNormalUserCreationButtons()
    {
        module('users')
            ->service('browse_button_handlers')
            ->add('ehda-modifications')
            ->method("ehdaManage:CardsController@removeNormalUserCreationButtons");
    }

    /**
     * Redefines Username Field
     */
    private function redefineUsernameField()
    {
        config([
             "auth.providers.users.field_name" => "code_melli",
        ]);
    }



    /**
     * Create Button on top of the manage area
     */
    private function registerCreateHandlers()
    {
        module('manage')
             ->service('nav_create_handler')
             ->add('ehda-users')
             ->method('ehdaManage:EhdaUsersHandleController@manageCreateButtons')
        ;
    }



    /**
     * Module string, to be used as sample for role definitions.
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('cards')
             ->value('browse ,  create ,  edit  ,  delete ,  bin ,  print,report')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('volunteers')
             ->value('browse ,  create ,  edit  ,  delete ,  bin ,report')
        ;
    }



    /**
     * Account Settings
     */
    public function registerAccountSettings()
    {
        module('manage')
             ->service('account_settings')
             ->update('personal')
             ->blade('ehdaManage::account.personal.index')
        ;

        module('manage')
             ->service('account_settings')
             ->add('donation-card')
             ->link('donation-card')
             ->caption('trans:ehdaManage::ehda.donation_card')
             ->blade('ehdamanage::account.card.index')
             ->order(12)
        ;
    }
}
