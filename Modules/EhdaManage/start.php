<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}


if (!function_exists("setLog")) {
    /**
     * @param $title
     * @param $type
     * @param $data
     *
     * @return $this
     */
    function setLog($title, $type, $data)
    {
        $insert = model('log')->batchSave([
             'title' => $title,
             'type'  => $type,
             'data'  => json_encode($data),
        ]);

        return $insert;
    }
}

if (!function_exists("getQuery")) {

    /**
     * you can use for see your complete query with its values :)
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     *
     * @return string
     */
    function getQuery(\Illuminate\Database\Eloquent\Builder $builder)
    {
        $query       = $builder->toSql();
        $bind_values = $builder->getBindings();
        $values      = [];
        foreach ($bind_values as $value) {
            $values[] = "'" . $value . "'";
        }
        $str = str_replace_array('?', $values, $query);
        return $str;
    }
}

if (!function_exists("safeEchoDate")) {

    /**
     * @param \Carbon\Carbon $date
     * @param string         $format
     *
     * @return string
     */
    function safeEchoDate($date, $format = "y/m/d")
    {
        if (empty($date)) {
            return "";
        }

        if (checkdate($date->month, $date->day, $date->year)) {
            return echoDate($date, $format);
        }
        return "";
    }
}