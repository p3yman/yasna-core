<?php

namespace Modules\EhdaManage\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TookenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data[] = [
             'slug'          => 'tooska_token',
             'title'         => 'وب سرویس توسکا',
             'category'      => 'security',
             'order'         => '1',
             'data_type'     => 'text',
             'default_value' => 'ikgy6EkdNchv9mPqXBEk8ic6oBfc1K3tFeiaZUSb',
             'hint'          => '',
             'css_class'     => '',
             'is_localized'  => '0',
        ];
        yasna()->seed('settings', $data);
    }
}
