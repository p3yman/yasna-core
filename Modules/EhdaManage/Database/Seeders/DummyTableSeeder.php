<?php

namespace Modules\EhdaManage\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Filemanager\Entities\File;
use Modules\Yasna\Providers\DummyServiceProvider;

class DummyTableSeeder extends Seeder
{
    use DummyUserCreator;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->runUserGenerator(3000);
    }
}
