<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChanelToUserPendings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_pendings', function (Blueprint $table) {
            $table->string('channel')->default('tooska')->after('birth_date')->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_pendings', function (Blueprint $table) {
            $table->dropColumn('channel');
        });
    }
}
