<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_pendings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code_melli', 20)->nullable()->index();
            $table->string('mobile')->nullable()->index();
            $table->date('birth_date')->nullable()->index();

            $table->timestamp('online_requested_at')->nullable()->index();
            $table->timestamp('online_verified_at')->nullable()->index();
            $table->timestamp('online_rejected_at')->nullable()->index();

            $table->timestamp('offline_requested_at')->nullable()->index();
            $table->timestamp('offline_verified_at')->nullable()->index();
            $table->timestamp('offline_rejected_at')->nullable()->index();

            $table->timestamp('user_registered_at')->nullable()->index();

            $table->longText('meta')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->tinyInteger('converted')->default(0)->index();
            $table->index('created_at');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_pendings');
    }
}
