<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShortMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('short_messages', function (Blueprint $table) {
            $table->increments('id');

            /**
             * Account
             */
            $table->string('account_id')->nullable()->index();

            /*
             * Numbers
             */
            $table->string('source')->nullable()->index();
            $table->string('destination')->nullable()->index();

            /**
             * Request
             */
            $table->string('muid')->nullable()->index();

            /**
             * Service
             */
            $table->string('sid')->nullable()->index();


            /**
             * Channel
             */
            $table->string('channel_type')->nullable()->index();
            $table->string('channel')->nullable()->index();

            /*
             * Message Content
             */
            $table->text('text')->nullable();
            ;

            /*
             * Message Type
             */
            $table->string('message_type')->index();


            /*
             * Meta
             */
            $table->longText('meta')->nullable();

            /*
            | Change Logs
            */
            $table->timestamp('sent_at')->nullable()->index();
            $table->timestamp('delivered_at')->nullable()->index();
            $table->unsignedInteger('sent_by')->default(0)->index();

            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0)->index();
            $table->unsignedInteger('deleted_by')->default(0)->index();

            /*
            | Indexes
            */
            $table->tinyInteger('converted')->default(0)->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('short_messages');
    }
}
