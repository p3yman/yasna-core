<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSidToUserPendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_pendings', function (Blueprint $table) {
            $table->string('sid')
                  ->nullable()
                  ->index()
                  ->after('user_registered_at')
                 ->comment = "Session ID";
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_pendings', function (Blueprint $table) {
            $table->dropColumn('sid');
        });
    }
}
