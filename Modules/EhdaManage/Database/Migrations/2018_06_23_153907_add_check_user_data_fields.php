<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckUserDataFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('incomplete_at')->nullable()->index();
            $table->date('dead_at')->nullable()->index();
            $table->timestamp('names_rejected_at')->nullable()->index();
            $table->timestamp('home_province_rejected_at')->nullable()->index();
            $table->timestamp('work_province_rejected_at')->nullable()->index();
            $table->timestamp('edu_province_rejected_at')->nullable()->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('incomplete_at');
            $table->dropColumn('dead_at');
            $table->dropColumn('names_rejected_at');
            $table->dropColumn('home_province_rejected_at');
            $table->dropColumn('work_province_rejected_at');
            $table->dropColumn('edu_province_rejected_at');
        });
    }
}
