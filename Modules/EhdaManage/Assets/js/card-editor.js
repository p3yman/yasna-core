function cardEditor($mood, $para = '') {

    let $divCard = $("#divCard");

    switch ($mood) {
        case 1 :
            $divCard.slideUp('fast');
            $('#divInquiry,#divForm').slideToggle('fast');
            $('#frmEditor [name=code_melli]').val($('#txtInquiry').val());
            $('#frmEditor [name=gender]').focus();
            break;

        case 2:
            $divCard.attr('data-id', $para);
            divReload('divCard');
//			$('#imgCard').attr('src' , url('/card/show_card/mini/'+$para));
//			$('#txtCard').val( $para );
            $divCard.slideDown('fast');
            break;

        case 3: // <~~ Volunteer Editor
            $divForm.attr('data-id', $para);
            divReload('divForm');
            $('#divInquiry,#divForm').slideToggle('fast');

    }
}