$(document).ready(function () {
    //show and hide user inputs
    var userByCardCheckBox     = $('#user-by-card');
    var ambassadorUserCheckBox = $('#ambassador-user');

    userByCardCheckBox.change(function () {
        if (this.checked) {
            $(".user_by_card_dates").slideDown("fast");
        } else {
            $(".user_by_card_dates").slideUp("fast");
        }
    }).change();

    ambassadorUserCheckBox.change(function () {
        if (this.checked) {
            $(".ambassador_user").slideDown('fast');
        } else {
            $(".ambassador_user").slideUp('fast');
        }
    }).change();
    //end show and hide user inputs
});

/**
 *
 * @param element
 * @param cityID
 * @param url
 */
function getProvince(element, cityID, url) {
    $.post(url,
        {
            city_id: cityID
        },
        function (data, status) {
            var data = JSON.parse(data);
            element.val(data.id);
            element.selectpicker('refresh');
        })
}

/**
 *
 * @param element
 * @param province_id
 * @param url
 */
function getCities(element, province_id, url) {
    $.post(url,
        {
            'province_id': province_id
        },
        function (data, status) {
            var dataArray = JSON.parse(data);
            element.find('option').remove();
            //add items
            addAllItems(dataArray, element);
            element.selectpicker('refresh');
        });
}


/**
 *
 * @param data
 * @param ele
 */
function addAllItems(data, ele) {
    $.each(data, function (i, item) {
        ele.append($('<option>', {
            value: item.id,
            text : item.title
        }))
    });
}