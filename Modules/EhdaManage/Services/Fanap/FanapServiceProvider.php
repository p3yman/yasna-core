<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/26/18
 * Time: 4:07 PM
 */

namespace Modules\EhdaManage\Services\Fanap;

use App\Models\ShortMessage;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;
use Modules\EhdaManage\Events\ShortMessageSent;
use phpseclib\Crypt\RSA;
use Webpatser\Uuid\Uuid;

class FanapServiceProvider extends ServiceProvider
{
    const CHANNEL_NAME = '3432';



    /**
     * @param string|array $data
     *
     * @return string
     */
    public static function createSign($data)
    {
        $rsa = new RSA;
        $rsa->setHash('sha1');
        $rsa->setSignatureMode(RSA::ENCRYPTION_PKCS1);
        $rsa->loadKey(static::getPrivateKey(), RSA::PRIVATE_FORMAT_XML);

        return base64_encode($rsa->sign(static::generateStringToSign($data)));
    }



    /**
     * @param string|array $data
     * @param string       $signature
     *
     * @return bool
     */
    public static function verifySign($data, $signature)
    {
        $rsa = new RSA;
        $rsa->setHash('sha1');
        $rsa->setSignatureMode(RSA::ENCRYPTION_PKCS1);
        $rsa->loadKey(static::getPublicKey(true), RSA::PUBLIC_FORMAT_XML);
        return $rsa->verify(static::generateStringToSign($data), base64_decode($signature));
    }



    /**
     * @return bool|string
     */
    public static function getPrivateKey()
    {
        return static::getKey('private');
    }



    /**
     * @return bool|string
     */
    public static function getPublicKey($party = false)
    {
        return static::getKey('public', $party);
    }



    /**
     * Return public or private key
     *
     * @param $type
     *
     * @return bool|string
     */
    public static function getKey($type, $party = false)
    {
        $own_string = ($party ? 'party' : 'own');
        $cache_key  = "fanap.key.$own_string.$type";

        if (!Cache::has($cache_key)) {
            $cache_expire = now()->addHours(12);

            Cache::add($cache_key, static::getKeyFromFile($type, $party), $cache_expire);
        }

        return Cache::get($cache_key);
    }



    /**
     * Read public or private key from its file
     *
     * @param string $type
     * @param bool   $party
     *
     * @return bool|string
     */
    public static function getKeyFromFile($type, $party = false)
    {
        $own_string = ($party ? 'party' : 'own');

        return file_get_contents(implode(DIRECTORY_SEPARATOR, [
             __DIR__,
             'data',
             'key',
             $own_string,
             "$type.xml",
        ]));
    }



    /**
     * @param string|array $data
     *
     * @return null|string
     */
    protected static function generateStringToSign($data)
    {
        if (is_array($data)) {
            $data = implode(',', $data);
        }
        return is_string($data) ? $data : null;
    }



    /**
     * @param array $array
     *
     * @return bool
     */
    public static function arrayIsIndexed(array $array)
    {
        return (array_keys($array) === range(0, count($array) - 1));
    }



    /**
     * @param array $array
     *
     * @return bool
     */
    public static function arrayIsAssoc(array $array)
    {
        return !static::arrayIsIndexed($array);
    }



    /**
     * @return \Modules\Yasna\Services\ModuleHelper
     */
    public static function module()
    {
        return module(static::moduleName());
    }



    /**
     * @return string
     */
    protected static function moduleName()
    {
        $array = explode("\\", static::class);

        if (array_first($array) == 'Modules') {
            return $array[1];
        }

        return null;
    }



    /**
     * @return string
     */
    public static function generateUUID()
    {
        return Uuid::generate(4)->string;
    }



    /**
     * @param $phone_number
     *
     * @return null|string
     */
    public static function findChannel($phone_number)
    {
        $pre_numbers = config('ehdamanage.fanap.number-maps');
        foreach ($pre_numbers as $service => $patterns) {
            foreach ($patterns as $pattern) {
                if (preg_match('/^' . $pattern . '$/', $phone_number)) {
                    return $service;
                }
            }
        }

        return null;
    }



    /**
     * @param string|null $service_name
     *
     * @return string|null
     */
    public static function getServiceId($service_name)
    {
        $sid_map = config('ehdamanage.fanap.sid');
        if (array_key_exists($service_name, $sid_map)) {
            return $sid_map[$service_name];
        }

        return null;
    }



    /**
     * Returns the value of `MessageType` based on the given service name.
     *
     * @param string $service_name
     *
     * @return string|null
     */
    public static function getMessageType(string $service_name)
    {
        $map = config('ehdamanage.fanap.message-type');

        return ($map[$service_name] ?? null);
    }



    /**
     * @param $file_name
     *
     * @return array
     */
    public static function getDataFromJson($file_name, $assoc = true)
    {
        return json_decode(file_get_contents(
             implode(DIRECTORY_SEPARATOR, [
                  __DIR__,
                  'data',
                  'json',
                  $file_name,
             ])
             . '.json'
        ), $assoc);
    }



    /**
     * @param string $number
     * @param string $content
     *
     * @return bool
     */
    public static function sendMessage($number, $content)
    {
        $channel_type  = FanapServiceProvider::findChannel($number);
        $saved_message = static::sendMessageInnerProcess($number, $content, $channel_type);

        if ($channel_type and $saved_message and $saved_message->exists) {
            event(new ShortMessageSent($saved_message));
            return true;
        } else {
            return false;
        }
    }



    /**
     * @param string $number
     * @param string $content
     * @param string $channel_type
     *
     * @return ShortMessage|bool
     */
    protected static function sendMessageInnerProcess($number, $content, $channel_type)
    {
        try {
            $client       = new Client();
            $uid          = FanapServiceProvider::generateUUID();
            $date         = Carbon::now()->timezone('UTC')->format('Y-m-d\TH:i:s.000\Z');
            $sid          = FanapServiceProvider::getServiceId($channel_type);
            $message_type = FanapServiceProvider::getMessageType($channel_type);
            $sign         = FanapServiceProvider::createSign([
                 $date,
                 $uid,
                 $sid,
                 $channel_type,
                 $message_type,
                 $number,
                 $content,
            ]);

            $message_data = [
                 'headers' => [
                      'Content-Type' => 'application/json;  charset=utf-8',
                 ],
                 'json'    => ($data = [
                      'UID'      => $uid,
                      'Date'     => $date,
                      'Messages' => [
                           [
                                'UserPhoneNumber' => $number,
                                'ChannelType'     => $channel_type,
                                'Sid'             => $sid,
                                'Content'         => $content,
                                'MessageType'     => $message_type,
                                'Priority'        => 'Normal',
                                'Signature'       => $sign,
                           ],
                      ],
                 ]),
            ];

            $response = $client->post('https://xcp.fanap.plus/api/v5.0/message/post', $message_data);

            if (
                 ($response->getStatusCode() == 200) and
                 ($resArray = json_decode($response->getBody(), true)) and
                 is_array($resArray)
            ) {
                return model('short-message')->batchSave([
                     'uid'          => $uid,
                     'sid'          => $sid,
                     'sent_at'      => $date,
                     'channel_type' => $channel_type,
                     'destination'  => $number,
                     'message_type' => $message_type,
                     'text'         => $content,
                     'muid'         => $resArray['Muids'][0],
                     'puid'         => $resArray['Puid'],
                     'response'     => $resArray,
                ]);
            }
        } catch (\Exception $e) {
            return null;
        }

        return null;
    }



    /**
     * @param string $url
     *
     * @return string
     */
    public static function url(string $url)
    {
        if ($site_url = config('ehdamanage.domain')) {
            return str_replace(url('/'), $site_url, $url);
        }
        return $url;
    }
}
