<?php

namespace Modules\EhdaManage\Http\Api;

class ApiResponse
{
    const   TOKEN_NOT_EXIST                    = 1001;
    const   TOKEN_INCORRECT                    = 1002;
    const   TOKEN_EXPIRED                      = 1003;
    const   UNABLE_TO_RESPOND_THIS_TOKEN       = 1004;
    const   CODE_MELLI_INVALID                 = 1005;
    const   BIRTH_DAY_INVALID                  = 1006;
    const   TEL_MOBILE_INVALID                 = 1007;
    const   CALL_BACK_NOT_EXIST                = 1008;
    const   REQUEST_SUCCESS_AND_SEND_TO_AHVAL  = 1009;
    const   REQUEST_SUBMIT_SUCCESSFULLY        = 1010;
    const   REQUEST_DOES_NOT_EXIST             = 1011;
    const   ERROR_IN_HANDLING_REQUEST          = 1012;
    const   WAITING_FOR_AHVAL_ONLINE           = 1013;
    const   ERROR_IN_USER_VALIDATION           = 1014;
    const   FAILED_VALIDATION_SEND_AGAIN       = 1015;
    const   SUCCESS_ELEMENTARY_VALIDATION      = 1016;
    const   WAITING_FOR_AHVAL_OFFLINE          = 1017;
    const   UNABLE_TO_REGISTER_THIS_CODE_MELLI = 1018;
    const   CARD_IS_PUBLISHING                 = 1019;
    const   USER_ALREADY_EXIST                 = 1020;
    const   SYSTEM_EXCEPTION_TRY_AGAIN         = 1021;
    const   AWAITING_BIRTH_DATE                = 1022;
    const   USER_CARD_REGISTERED               = 200;

    private static $smsMessage;
    private static $smsFlag;



    /**
     * @param string $message
     */
    public static function setSMSmessage(string $message)
    {
        self::$smsMessage = $message;
    }



    /**
     * @param bool $flag
     */
    public static function setSMSflag(bool $flag)
    {
        self::$smsFlag = ($flag) ? 1 : 0;
    }



    /**
     * @param        $status
     * @param string $msg
     */
    public static function getError($status, $msg = 'error happen')
    {
        $array = [
             'status' => $status,
             'msg'    => $msg,
        ];

        if (isset(self::$smsFlag)) {
            $array['send_sms'] = self::$smsFlag;
        }
        if (isset(self::$smsMessage)) {
            $array['body_sms'] = self::$smsMessage;
        }

        header('Content-Type: application/json');
        echo \GuzzleHttp\json_encode($array);
        exit;
    }



    /**
     * @param $status
     * return success message
     */
    public static function getSuccess($status, $message)
    {
        $array = [
             'status' => $status,
             'msg'    => $message,
        ];

        if (isset(self::$smsFlag)) {
            $array['send_sms'] = self::$smsFlag;
        }
        if (isset(self::$smsMessage)) {
            $array['body_sms'] = self::$smsMessage;
        }

        header('Content-Type: application/json');
        echo \GuzzleHttp\json_encode($array);
        exit;
    }
}
