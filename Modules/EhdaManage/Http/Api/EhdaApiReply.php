<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 5/29/18
 * Time: 5:22 PM
 */

namespace Modules\EhdaManage\Http\Api;

use App\Models\UserPending;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;

class EhdaApiReply
{
    private $model;
    private $status;
    private $channel;
    private $statusText;
    private $tell;



    /**
     * @param UserPending $model
     *
     * @return EhdaApiReply
     */
    public static function model($model)
    {
        return new EhdaApiReply($model);
    }



    /**
     * EhdaApiReply constructor.
     *
     * @param UserPending $model
     */
    public function __construct($model)
    {
        $this->model = $model;
        if (!is_null($model)) {
            $this->status = $model->apiStatus();
        }
    }



    /**
     * @param $channel
     *
     * @return $this
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }



    /**
     * @param $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }



    /**
     * @param $text
     *
     * @return $this
     */
    public function setStatusText($text)
    {
        $this->statusText = $text;
        return $this;
    }



    /**
     * @param $tell
     *
     * @return $this
     */
    public function setTell($tell)
    {
        $this->tell = $tell;
        return $this;
    }



    /**
     * @return bool
     */
    public function send()
    {
        if (empty($this->statusText)) {
            if (is_null($this->status)) {
                return false;
            }
            $this->statusText = trans("ehdamanage::tooska-api.user_messages.$this->status");
        }
        //find and send message
        return $this->findOutChanel();
    }



    /**
     * todo:this is hard code and always run 3432
     */
    private function findOutChanel()
    {
        return $this->sendVia3432();
    }



    /**
     * @return bool
     */
    private function sendVia3432()
    {
        if (!$this->checkExistTell()) {
            return false;
        }

        return FanapServiceProvider::sendMessage($this->findTell(), $this->statusText);
    }



    /**
     * check that user has user mobile number
     *
     * @return bool
     */
    private function checkExistTell()
    {
        if (isset($this->tell)) {
            return true;
        }

        if (is_null($this->model)) {
            return false;
        }

        return true;
    }



    /**
     * find user mobile number
     *
     * @return mixed
     */
    private function findTell()
    {
        $user_mobile = null;
        if (isset($this->tell)) {
            $user_mobile = $this->tell;
        } else {
            if (isset($this->model)) {
                $user_mobile = $this->model->mobile;
            }
        }
        return $user_mobile;
    }
}
