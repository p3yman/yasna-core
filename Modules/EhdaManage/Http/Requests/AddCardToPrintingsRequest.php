<?php

namespace Modules\EhdaManage\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class AddCardToPrintingsRequest extends YasnaRequest
{
    protected $model_name = "User";



    /**
     * @return array
     */
    public function rules()
    {
        return [
             "from_event_id" => "required",
        ];
    }
}
