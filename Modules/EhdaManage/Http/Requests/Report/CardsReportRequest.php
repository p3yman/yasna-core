<?php

namespace Modules\EhdaManage\Http\Requests\Report;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class CardsReportRequest extends YasnaRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
             'date'            => 'required|before_or_equal:' . Carbon::now()->toDateString(),
             'register_method' => Rule::in($this->acceptedRegisterMethods()),
             'report_type'     => Rule::in(['daily', 'hourly']),
        ];
    }



    /**
     * Correct Date
     */
    public function corrections()
    {
        if (
             array_key_exists('date', $this->data) and
             ($milliseconds = (int)$this->data['date']) and
             is_int($milliseconds)
        ) {
            $this->data['date'] = Carbon::createFromTimestamp($milliseconds / 1000)->toDateString();
        } else {
            $this->data['date'] = null;
        }
    }



    /**
     * Return accepted items for the "register_method" field.
     *
     * @return array
     */
    protected function acceptedRegisterMethods()
    {
        $api_users = user()
             ->elector(['role' => 'api'])
             ->get()
             ->pluck('hashid')
             ->toArray()
        ;

        return array_merge(['web'], $api_users);
    }
}
