<?php

namespace Modules\EhdaManage\Http\Requests;

use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\YasnaRequest;

class CardSaveRequest extends YasnaRequest
{
    protected $model_name = "User";
    private $home_state;



    /**
     * @return bool
     */
    public function authorize()
    {
        if (user()->as('admin')->cannot('users-card-holder.create')) {
            return false;
        }

        if ($this->model->is_admin() and !$this->model->min(8)->is_admin()) {
            return true;
        }

        if ($this->id) {
            return $this->model->ehdaCanEdit();
        }

        return true;
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'code_melli'    => 'ed',
             'code_id'       => 'ed',
             'gender'        => 'number',
             'mobile'        => 'ed',
             'tel_emergency' => 'ed',
             'edu_city'      => 'number',
             'home_tel'      => 'ed',
             'home_address'  => 'pd',
             'home_postal'   => "ed",
             "birth_date"    => "date",
        ];
    }



    /**
     * @return array
     */
    public function mainRules()
    {
        return [
             'name_first'  => "required|min:2",
             'name_last'   => "required|min:2",
             'name_father' => 'min:2',
             'code_melli'  => "required|code_melli",
             'email'       => 'email|',
             'gender'      => 'required',
             'birth_city'  => '',
             'edu_level'   => 'numeric',
             'home_city'   => 'required',
             'birth_date'  => 'required|date',
             'mobile'      => 'required|phone:mobile',
             'home_tel'    => 'phone:fixed',
             'work_tel'    => 'phone:fixed',
        ];
    }



    /**
     * @return array
     */
    public function unityRules()
    {
        $existing_user = user()->finder($this->data['code_melli']);

        if ($existing_user->exists and $existing_user->id != $this->model->id) {
            return [
                 "unique_code_melli" => "required", //<~~ Custom message, set in $this->messages() method.
            ];
        }

        return [];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             "unique_code_melli.required" => trans_safe("ehdaManage::ehda.cards.inquiry_has_card"),
        ];
    }



    /**
     * Corrections
     */
    public function corrections()
    {
        $this->prepareHomeProvince();
        $this->prepareEduProvince();
        $this->setPasswordIfNecessary();
        $this->putForceChangePasswordFlag();
        $this->fillDomainData();
        $this->fillRegistrationDate();
        $this->removeEventInEditMode();
    }



    /**
     * Remove Event in Edit Mode
     */
    private function removeEventInEditMode()
    {
        $this->data['_from_event_id'] = $this->data['from_event_id'];

        if ($this->model->id) {
            unset($this->data['from_event_id']);
        }
    }



    /**
     * Prepares Home Province and merges it into the user-submitted data.
     */
    private function prepareHomeProvince()
    {
        $this->home_state = $home_city = model('state', $this->data['home_city']);

        if ($home_city->exists) {
            $this->data['home_province'] = $home_city->parent_id;
        } else {
            $this->data['home_province'] = 0;
            $this->data['home_city']     = 0;
        }
    }



    /**
     * Prepares Education Province and merges it into the user-submitted data.
     */
    private function prepareEduProvince()
    {
        $edu_city = model('state', $this->data['edu_city']);

        if ($edu_city->exists) {
            $this->data['edu_province'] = $edu_city->parent_id;
        } else {
            $this->data['edu_province'] = 0;
            $this->data['edu_city']     = 0;
        }
    }



    /**
     * Sets Password from mobile number, if necessary.
     */
    private function setPasswordIfNecessary()
    {
        if (!isset($this->data['_password_set_to_mobile'])) {
            $this->data['_password_set_to_mobile'] = 0;
        }
        if (!$this->model->id) {
            $this->data['_password_set_to_mobile'] = 1;
        }


        if ($this->data['_password_set_to_mobile']) {
            $this->data['password'] = Hash::make($this->data['mobile']);
        }
    }



    /**
     * Puts force-change-password, if necessary.
     */
    private function putForceChangePasswordFlag()
    {
        if ($this->data['_password_set_to_mobile']) {
            $this->data['password_force_change'] = 1;
        }
    }



    /**
     * Fills domain-data, according to the volunteer information, or user home_city.
     */
    private function fillDomainData()
    {
        $this->data['domain'] = user()->domain;

        if (!$this->data['domain'] or $this->data['domain'] == 'global') {
            $this->data['domain'] = $this->home_state->slug;
        }
    }



    /**
     * Fills registration date, for new registrations only.
     */
    private function fillRegistrationDate()
    {
        if (!$this->model->exists or $this->model->is_not_a('card-holder')) {
            $this->data['card_registered_at'] = now()->toDateTimeString();
        }
    }
}
