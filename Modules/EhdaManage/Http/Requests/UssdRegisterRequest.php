<?php

namespace Modules\EhdaManage\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\EhdaManage\Http\Utility\GuessDateFromPersianEntry;
use Modules\Yasna\Services\YasnaRequest;

class UssdRegisterRequest extends YasnaRequest
{

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator)
    {
        $error_array = [];
        $error_msg   = [];
        foreach ($validator->errors()->toArray() as $error) {
            foreach ($error as $status_code) {
                array_push($error_array, $status_code);
                array_push($error_msg, trans("ehdamanage::tooska-api.$status_code"));
            }
        }
        ApiResponse::getError($error_array, $error_msg);
    }



    /**
     * @return array
     */
    public function rules()
    {
        return [
             'code_melli'    => "required|code_melli",
             'birth_date'    => 'required',
             'tel_mobile'    => 'required|phone',
             'call_back_url' => 'required',
             'token'         => 'required',
        ];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             'code_melli.code_melli'  => ApiResponse::CODE_MELLI_INVALID,
             'code_melli.required'    => ApiResponse::CODE_MELLI_INVALID,
             'tel_mobile.phone'       => ApiResponse::TEL_MOBILE_INVALID,
             'tel_mobile.required'    => ApiResponse::TEL_MOBILE_INVALID,
             'birth_date.size'        => ApiResponse::BIRTH_DAY_INVALID,
             'call_back_url.required' => ApiResponse::CALL_BACK_NOT_EXIST,
             'token.required'         => ApiResponse::TOKEN_NOT_EXIST,
        ];
    }



    /**
     * make carbon and check correction of birth_day
     */
    public function corrections()
    {
        $status = ApiResponse::BIRTH_DAY_INVALID;
        $date   = GuessDateFromPersianEntry::ask($this->data['birth_date']);
        if (!$date) {
            ApiResponse::getError($status, trans("ehdamanage::tooska-api.$status"));
            return;
        }

        $this->data['birth_date'] = $date;
    }
}
