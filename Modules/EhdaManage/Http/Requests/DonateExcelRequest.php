<?php

namespace Modules\EhdaManage\Http\Requests;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaRequest;

class DonateExcelRequest extends YasnaRequest
{
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.

    public function authorize()
    {
        return user()->as('admin')->can('donation.export');
    }

    /**
     * just correction :)
     */
    public function corrections()
    {
        $this->unixToCarbon();
    }

    /**
     * change date unix to carbon
     */
    private function unixToCarbon()
    {
        if (!empty($this->data['donate_date'])) {
            $this->data['donate_date'] = $this->getTimeFromUnix($this->data['donate_date']);
        }
        if (!empty($this->data['to_donate_date'])) {
            $this->data['to_donate_date'] = $this->getTimeFromUnix($this->data['to_donate_date']);
        }
    }

    /**
     * @param $time
     *
     * @return bool|string
     */
    private function getTimeFromUnix($time)
    {
        $time = substr($time, 0, strlen($time) - 3);
        $time = Carbon::parse(date('Y-m-d', $time))->toDateString();
        return $time;
    }
}
