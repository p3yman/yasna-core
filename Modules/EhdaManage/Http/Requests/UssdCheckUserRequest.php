<?php

namespace Modules\EhdaManage\Http\Requests;

use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\Yasna\Services\YasnaRequest;
use Illuminate\Contracts\Validation\Validator;

class UssdCheckUserRequest extends YasnaRequest
{

    /**
     * @param Validator $validator
     */
    public function failedValidation(Validator $validator)
    {
        $errorArray = [];
        foreach ($validator->errors()->toArray() as $error) {
            foreach ($error as $message) {
                array_push($errorArray, $message);
            }
        }
        ApiResponse::getError($errorArray);
    }



    /**
     * @return array
     */
    public function rules()
    {
        return [
             'code_melli'    => "required|code_melli",
             'token'         => 'required',
        ];
    }


    /**
     * @return array
     */
    public function messages()
    {
        return [
             'code_melli.code_melli' => ApiResponse::CODE_MELLI_INVALID,
             'code_melli.required'   => ApiResponse::CODE_MELLI_INVALID,
        ];
    }
}
