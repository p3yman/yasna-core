<?php

namespace Modules\EhdaManage\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DonateListRequest extends YasnaRequest
{
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.

    public function authorize()
    {
        return user()->as('admin')->can('donation.view');
    }
}
