<?php

namespace Modules\EhdaManage\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CodeMelliInquiryRequest extends YasnaRequest
{


    /**
     * @return array
     */
    public function rules()
    {
        return [
             'code_melli' => "required|code_melli",
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'code_melli' => 'ed',
        ];
    }
}
