<?php

namespace Modules\EhdaManage\Http\Requests;

use App\Models\Activity;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class VolunteerSaveRequest
 *
 * @package Modules\EhdaManage\Http\Requests
 * @property User $model
 */
class VolunteerSaveRequest extends YasnaRequest
{
    protected $model_name = "User";



    /**
     * @return bool
     */
    public function authorize()
    {
        if ($this->model->id == user()->id and user()->isAdmin()) {
            return true;
        }
        if (!$this->model->id or !$this->model->withDisabled()->isAdmin()) {
            $role_slug = $this->data['role_slug'];
            return user()->can("users-$role_slug.create");
        }

        return $this->model->ehdaCanEdit();
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'code_melli'    => 'ed',
             'code_id'       => 'ed',
             'gender'        => 'number',
             'mobile'        => 'ed',
             'tel_emergency' => 'ed',
             'edu_city'      => 'number',
             'home_tel'      => 'ed',
             'home_address'  => 'pd',
             'home_postal'   => "ed",
             "birth_date"    => "date",
             "work_tel"      => "ed",
        ];
    }



    /**
     * @return array
     */
    public function mainRules()
    {
        return [
             'name_first'    => "required|min:2",
             'name_last'     => "required|min:2",
             'name_father'   => 'min:2',
             'code_melli'    => "required|code_melli",
             'email'         => 'email|',
             'gender'        => 'required',
             'birth_city'    => '',
             'edu_level'     => 'numeric',
             'home_city'     => 'required',
             'birth_date'    => 'required|date',
             'mobile'        => 'required|phone:mobile',
             'tel_emergency' => 'required',
             'home_tel'      => 'phone:fixed',
             'work_tel'      => 'phone:fixed',
        ];
    }



    /**
     * @return array
     */
    public function roleRules()
    {
        if ($this->model->withDisabled()->is_admin()) {
            return [];
        }

        return [
             'status'    => "required",
             'role_slug' => "required|exists:roles,slug",
        ];
    }



    /**
     * @return array
     */
    public function unityRules()
    {
        $existing_user = user()->finder($this->data['code_melli']);

        if ($existing_user->exists and $existing_user->id != $this->model->id and $this->model->id != user()->id) {
            return [
                 "unique_code_melli" => "required", //<~~ Custom message, set in $this->messages() method.
            ];
        }

        return [];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             "unique_code_melli.required" => trans_safe("ehdaManage::ehda.volunteers.already_volunteer"),
        ];
    }



    /**
     * Corrections
     */
    public function corrections()
    {
        $this->prepareHomeProvince();
        $this->prepareEduProvince();
        $this->prepareWorkProvince();
        $this->setPasswordIfNecessary();
        $this->putForceChangePasswordFlag();
        $this->prepareActivities();
    }



    /**
     * Activities
     */
    private function prepareActivities()
    {
        $this->data['activities'] = Activity::requestToString($this->data);
    }



    /**
     * Prepares Home Province and merges it into the user-submitted data.
     */
    private function prepareHomeProvince()
    {
        $this->home_state = $home_city = model('state', $this->data['home_city']);

        if ($home_city->exists) {
            $this->data['home_province'] = $home_city->parent_id;
        } else {
            $this->data['home_province'] = 0;
            $this->data['home_city']     = 0;
        }
    }



    /**
     * Sets Password from mobile number, if necessary.
     */
    private function setPasswordIfNecessary()
    {
        if (!isset($this->data['_password_set_to_mobile'])) {
            $this->data['_password_set_to_mobile'] = 0;
        }
        if (!$this->model->id) {
            $this->data['_password_set_to_mobile'] = 1;
        }


        if ($this->data['_password_set_to_mobile']) {
            $this->data['password'] = Hash::make($this->data['mobile']);
        }
    }



    /**
     * Prepares Education Province and merges it into the user-submitted data.
     */
    private function prepareEduProvince()
    {
        $edu_city = model('state', $this->data['edu_city']);

        if ($edu_city->exists) {
            $this->data['edu_province'] = $edu_city->parent_id;
        } else {
            $this->data['edu_province'] = 0;
            $this->data['edu_city']     = 0;
        }
    }



    /**
     * Prepares Work Province and merges it into the user-submitted data.
     */
    private function prepareWorkProvince()
    {
        $work_city = model('state', $this->data['work_city']);

        if ($work_city->exists) {
            $this->data['work_province'] = $work_city->parent_id;
        } else {
            $this->data['work_province'] = 0;
            $this->data['work_city']     = 0;
        }
    }



    /**
     * Puts force-change-password, if necessary.
     */
    private function putForceChangePasswordFlag()
    {
        if ($this->data['_password_set_to_mobile']) {
            $this->data['password_force_change'] = 1;
        }
    }
}
