<?php

namespace Modules\EhdaManage\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CardSaveForActiveVolunteersRequest extends YasnaRequest
{
    protected $model_name = "User";



    /**
     * @return bool
     */
    public function authorize()
    {
        return user()->as('admin')->can('users-card-holder.create');
    }



    /**
     * Data Corrections
     */
    public function corrections()
    {
        $this->data['_from_event_id'] = $this->data['from_event_id'];
    }
}
