<?php

namespace Modules\EhdaManage\Http\Requests;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaRequest;

class FilterRequest extends YasnaRequest
{
    protected $model_name = "";



    /**
     * Todo : it will be complete
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }



    /**
     * just correction :)
     */
    public function corrections()
    {
        $this->unixToCarbon();
        $this->createArrayActivity();
    }



    /**
     * create array from activities checkbox
     */
    private function createArrayActivity()
    {
        if (empty($this->data)) {
            return;
        }
        $activities               = model('activity')::all(['title', 'slug']);
        $this->data['activities'] = [];
        foreach ($activities as $activity) {
            if ($this->data[$activity->slug] == "1") {
                $this->data['activities'][] = $activity->slug;
            }
        }
    }



    /**
     * change date unix to carbon
     */
    private function unixToCarbon()
    {
        if (isset($this->data['user_by_card_register'])) {
            $this->data['user_by_card_register'] = $this->getTimeFromUnix($this->data['user_by_card_register']);
        }
        if (isset($this->data['to_user_by_card_register'])) {
            $this->data['to_user_by_card_register'] = $this->getTimeFromUnix($this->data['to_user_by_card_register']);
        }
        if (isset($this->data['ambassador_user_register'])) {
            $this->data['ambassador_user_register'] = $this->getTimeFromUnix($this->data['ambassador_user_register']);
        }
        if (isset($this->data['to_ambassador_user_register'])) {
            $this->data['to_ambassador_user_register'] = $this->getTimeFromUnix($this->data['to_ambassador_user_register']);
        }
    }



    /**
     * @param $time
     *
     * @return bool|string
     */
    private function getTimeFromUnix($time)
    {
        //return pre value without change
        if (!$this->isValidTimeStamp($time)) {
            return $time;
        }

        $time = substr($time, 0, strlen($time) - 3);
        $time = Carbon::parse(date('Y-m-d', $time))->toDateString();
        return $time;
    }



    /**
     * @param $timestamp
     *
     * @return bool
     */
    public function isValidTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === $timestamp)
             && ($timestamp <= PHP_INT_MAX)
             && ($timestamp >= ~PHP_INT_MAX);
    }
}
