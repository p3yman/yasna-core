<?php

namespace Modules\EhdaManage\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class VolunteerAddRoleRequest extends YasnaRequest
{
    protected $model_name = "User";



    /**
     * @return bool
     */
    public function authorize()
    {
        return user()->as('admin')->can('volunteers.create');
    }



    /**
     * @return array
     */
    public function mainRules()
    {
        return [
             'role_slug' => "required|exists:roles,slug",
             'status'    => "required",
        ];
    }



    /**
     * @return array
     */
    public function repeatingRules()
    {
        if (!$this->data['role_slug']) {
            return [];
        }

        if ($this->model->withDisabled()->is_not_a($this->data['role_slug'])) {
            return [];
        }

        return [
             "role" => "required",
        ];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             "role.required" => trans_safe("ehdaManage::ehda.volunteers.already_has_role"),
        ];
    }
}
