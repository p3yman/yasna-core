<?php

namespace Modules\EhdaManage\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PeopleSearchRequest extends YasnaRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'keyword' => "required|min:2",
        ];
    }



    public function purifier()
    {
        return [
             'keyword' => "ed",
        ];
    }
}
