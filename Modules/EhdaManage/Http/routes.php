<?php

Route::group(['middleware' => 'web'], function () {
    Route::group([
         'middleware' => ['auth', 'is:admin'],
         'prefix'     => 'manage/ehda',
         'namespace'  => 'Modules\EhdaManage\Http\Controllers',
    ], function () {
        Route::get('/', 'EhdaManageController@index');

        Route::group(['prefix' => 'act'], function () {
            Route::get('search-people', 'HomeController@searchPeople');
            Route::get('inquiry/{keyword}', 'HomeController@inquiry');
        });

        Route::get("/report", "ReportController@filter");
        Route::get("/report/excel", "ReportController@excelModal");

        //Route::post("/report/excel", "ReportController@excel");
        Route::get("/report/export", "ReportController@excel");
        //check user data
        Route::get('/check/user-data', "CheckUserDataController@loadPage");
        Route::get('/check/user-ajax', "CheckUserDataController@callAjax");

        Route::group(['prefix' => 'report', 'namespace' => 'Report'], function () {
            Route::get('cards', 'CardReportController@index')
                 ->name('ehda-manage.report.cards')
            ;
            Route::post('cards', 'CardReportController@submit')
                 ->name('ehda-manage.report.cards.submit')
            ;
        });

        Route::post("/city_ajax", "ReportAjaxController@getCity");
        Route::post("/province_ajax", "ReportAjaxController@getProvince");

        Route::get("/charts", "ChartController@index");
        Route::get("/chart/city_users/{id}", "ChartController@citiesCardDate");

        Route::get("/event_report/{event_id}", "EventReportController@index");

        Route::get("/donate/list", "DonateController@lists");
        Route::get("/donate/modal", "DonateController@modal");
        Route::post("/donate/excel", "DonateController@excel");

        Route::get('/modal/volunteer', 'TestEhdaManageController@volunteer');

        Route::get('/modal/member', 'TestEhdaManageController@member');

        Route::post('/card/print', 'CardsProfileController@printing');
        Route::post('/card/delete/', 'CardsProfileController@deleteCard');
    });
});


/*
|--------------------------------------------------------------------------
| Card-Holders
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin'],
          'prefix'     => 'manage/cards/',
          'namespace'  => module('ehdaManage')->getControllersNamespace(),
     ],
     function () {
         Route::get('/act/{hashid}/profile', "CardsProfileController@index");
         Route::get('/act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'CardsEditController@singleAction');
         Route::get('create/{code_melli?}', 'CardsEditController@create');
         Route::get('edit/{hashid}', 'CardsEditController@edit');

         Route::get('update/{hashid}/{request_role}', 'CardsBrowseController@update');
         Route::get('browse/search-for', 'CardsSearchController@search');
         Route::get('browse/search', 'CardsSearchController@panel');
         Route::get('browse/{criteria?}', 'CardsBrowseController@cardsIndex');

         Route::post('inquiry', 'CardsEditController@inquiry')->name('card-inquiry');
         Route::post('save', 'CardsEditController@save')->name('card-save');
         Route::post('delete', 'CardsProfileController@deleteCard')->name('card-delete');
         Route::post('print', 'PrintingsActionController@addSingleCard')->name('card-print');
         Route::post('save-volunteer', 'CardsEditController@saveForActiveVolunteer')->name('card-save-for-a-volunteer');
     }
);

/*-----------------------------------------------
| Printings ...
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin', 'can:users-card-holder.print'],
          'prefix'     => 'manage/cards/printings',
          'namespace'  => module('ehdaManage')->getControllersNamespace(),
     ],
     function () {
         Route::get('/act/{model_id}/{action}', 'PrintingsActionController@singleAction');
         Route::get('/excel/{event_id}', 'PrintingsActionController@excelDownload');
         Route::get('/', 'PrintingsController@index');
         Route::get('/{request_event?}/{request_tab?}', 'PrintingsController@index');

         Route::post('save/add-to-excel', 'PrintingsActionController@addToExcel');
         Route::post('save/cancel-printing', 'PrintingsActionController@cancelPrinting');
         Route::post('save/confirm-quality', 'PrintingsActionController@confirmQuality');
         Route::post('save/revert-to-pending', 'PrintingsActionController@revertToPending');
     }
);


/*
|--------------------------------------------------------------------------
| Volunteers
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin'],
          'prefix'     => 'manage/volunteers/',
          'namespace'  => module('ehdaManage')->getControllersNamespace(),
     ],
     function () {
         Route::get('/act/{hashid}/profile', "VolunteersProfileController@index");
         Route::get('/act/{model_id}/{action}/{option0?}/{option1?}/{option2?}',
              'VolunteersBrowseController@singleAction');
         Route::get('edit/show/{id}', 'VolunteersEditController@show');
         Route::get('create/{code_melli?}', 'VolunteersEditController@create');
         Route::get('edit/{hashid}', 'VolunteersEditController@edit');

         Route::get('update/{hashid}/{request_role}', 'VolunteersBrowseController@update');
         Route::get('browse/{role}/search-for', 'VolunteersSearchController@search');
         Route::get('browse/{role}/search', 'VolunteersSearchController@panel');
         Route::get('browse/{role?}/{criteria?}', 'VolunteersBrowseController@index');

         Route::post('inquiry', 'VolunteersEditController@inquiry')->name('volunteer-inquiry');
         Route::post('save', 'VolunteersEditController@volunteerSave')->name('volunteer-save');
         Route::post('add-role', 'VolunteersEditController@addRole')->name('volunteer-role');
     }
);

/*-----------------------------------------------
| Account Settings ...
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin'],
          'prefix'     => "account",
          'namespace'  => module('ehdaManage')->getControllersNamespace(),
     ],
     function () {
         Route::post('/register', 'AccountController@register')->name('account-card-register');
     });


Route::group(
     [
          'middleware' => ['api', 'checkTooskaToken'],
          'prefix'     => 'api/tooska/v1',
          'namespace'  => module('ehdaManage')->getControllersNamespace(),
     ],
     function () {
         Route::post('users', "TooskaController@register");
         Route::post('users/inquiry', "TooskaController@inquiry");
     });

Route::group(
     [
          'middleware' => ['api'],
          'prefix'     => 'parsa',
          'namespace'  => module('ehdaManage')->getControllersNamespace(),
     ],
     function () {
         Route::get('/', "TestController@index");
         Route::post('/test', function () {
             return trans('ehdamanage::tooska-api.user_messages.1020');
         });
     });

Route::group([
     'prefix'    => 'api',
     'namespace' => module('ehdaManage')->getControllersNamespace() . '\\Api',
], function () {
    Route::group([
         'prefix'    => 'fanap/v1',
         'namespace' => 'Fanap',
    ], function () {
        Route::group(['prefix' => 'messages'], function () {
            Route::get('test', 'MessagesController@test');
            Route::post('/', 'MessagesController@save')
                 ->middleware('fanap-sign')
            ;
        });
    });


    Route::group([
         'prefix'    => 'mtn/v1',
         'namespace' => 'Mtn',
    ], function () {
        Route::get('users', 'MtnIrancellController@index')
             ->middleware('mtn-ussd')
        ;
    });
});

Route::group([
     'namespace' => module('ehdaManage')->getControllersNamespace() . '\\Api',
], function () {
    Route::group([
         'namespace' => 'Mtn',
    ], function () {
        Route::get('hadi/{token?}/{type?}', 'MtnIrancellController@showLogs');
    });
});
