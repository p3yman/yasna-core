<?php

namespace Modules\EhdaManage\Http\Services;

use SoapClient;

class BehdashtSoap
{
    public $birth_date;
    public $birth_year;
    public $birth_month;
    public $birth_day;
    public $death_date;
    public $death_year;
    public $death_month;
    public $death_day;
    public $name_first;
    public $name_last;
    public $name_father;
    public $gender;
    public $fake       = false;
    private $code_melli;
    private $raw_result = false;
    private $soap_url   = 'http://10.2.2.245/EstelamPeyvandWrapper/service.asmx?WSDL';



    /**
     * BehdashtSoap constructor.
     *
     * @param $code_melli
     */
    public function __construct($code_melli)
    {
        $this->code_melli = $code_melli;
        $this->raw_result = $this->connect();

        $this->fillPublicProperties();
    }



    /**
     * @return array
     */
    private function inputArray()
    {
        return [
             'NationalCode' => $this->code_melli,
        ];
    }



    /**
     * @param $input
     *
     * @return mixed
     */
    private function connect()
    {
        if (env('APP_ENV') == 'local') {
            $this->fake = true;
            return 'fake';
        }

        $soap = new SoapClient($this->soap_url);

        try {
            $result = $soap->GetPersonByNationalCode($this->inputArray());
        } catch (\Exception $exception) { //TODO: to be checked on server!
            $result = false;
        }

        if ($result !== false) {
            $result = $result->GetPersonByNationalCodeResult;
        }

        return $result;
    }



    /**
     * @return bool
     */
    public function isDead()
    {
        if ($this->fake) {
            return false;
        }

        return $this->raw_result->DeathStatus;
    }



    /**
     * @return bool
     */
    public function isAlive()
    {
        return !$this->isDead();
    }



    /**
     * @return bool
     */
    public function isValid()
    {
        if ($this->raw_result === false) {
            return false;
        }

        return true;
    }



    /**
     * @return bool
     */
    public function isNotValid()
    {
        return !$this->isValid();
    }



    /**
     * Fills Public Properties if the result is valid
     */
    private function fillPublicProperties()
    {
        if ($this->isNotValid()) {
            return;
        }
        if ($this->fake) {
            $this->fillPublicPropertiesWithFakeData();
            return;
        }

        $this->name_first  = $this->raw_result->FirstName;
        $this->name_last   = $this->raw_result->LastName;
        $this->name_father = $this->raw_result->Father_FirstName;
        $this->gender      = $this->raw_result->Gender->Coded_string;
        $this->birth_year  = $this->raw_result->BirthDate->Year;
        $this->birth_month = $this->raw_result->BirthDate->Month;
        $this->birth_day   = $this->raw_result->BirthDate->Day;
        $this->death_year  = $this->raw_result->DeathDate->Year;
        $this->death_month = $this->raw_result->DeathDate->Month;
        $this->death_day   = $this->raw_result->DeathDate->Day;
    }



    /**
     * Fills fake data for the test purposes
     */
    private function fillPublicPropertiesWithFakeData()
    {
        $this->name_first  = dummy()::persianName();
        $this->name_last   = dummy()::persianFamily();
        $this->name_father = dummy()::persianName();
        $this->gender      = rand(1, 2);
        $this->birth_year  = rand(1300, 1380);
        $this->birth_month = rand(1, 12);
        $this->birth_day   = rand(1, 29);
        $this->death_year  = rand(1300, 1380);
        $this->death_month = rand(1, 12);
        $this->death_day   = rand(1, 29);
    }



    /**
     * @return array
     */
    public function __toArray()
    {
        return $this->toArray();
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return [
             "code_melli"  => $this->code_melli,
             "name_first"  => $this->name_first,
             "name_last"   => $this->name_last,
             "name_father" => $this->name_father,
             "birth_year"  => intval($this->birth_year),
             "birth_month" => intval($this->birth_month),
             "birth_day"   => intval($this->birth_day),
             "death_year"  => intval($this->death_year),
             "death_month" => intval($this->death_month),
             "death_day"   => intval($this->death_day),
             "gender"      => intval($this->gender),
             "alive"       => $this->isAlive(),
             "dead"        => $this->isDead(),
             "valid"       => $this->isValid(),
        ];
    }
}
