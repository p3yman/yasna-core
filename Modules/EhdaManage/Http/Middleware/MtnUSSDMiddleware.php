<?php

namespace Modules\EhdaManage\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MtnUSSDMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->passwordIsNotCorrect($request)) {
            abort(403, 'Bad Request!');
        }

        return $next($request);
    }



    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function passwordIsCorrect(Request $request)
    {
        return ($request->password == 'ehda123');
    }



    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function passwordIsNotCorrect(Request $request)
    {
        return !$this->passwordIsCorrect($request);
    }
}
