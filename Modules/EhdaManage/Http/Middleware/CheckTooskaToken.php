<?php

namespace Modules\EhdaManage\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\EhdaManage\Http\Api\ApiResponse;

class CheckTooskaToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->tokenProcess($request->token);
        return $next($request);
    }



    /**
     * check valid token ane exist
     *
     * @param $token
     */
    private function tokenProcess($token)
    {
        if (empty($token)) {
            $status = ApiResponse::TOKEN_NOT_EXIST;
            ApiResponse::getError($status, trans("ehdamanage::tooska-api.$status"));
        }
        if ($token != get_setting('tooska_token')) {
            $status = ApiResponse::TOKEN_INCORRECT;
            ApiResponse::getError($status, trans("ehdamanage::tooska-api.$status"));
        }
    }
}
