<?php

namespace Modules\EhdaManage\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;

class FanapSignMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->messagesAreValid($this->safeMessages($request))) {
            return $next($request);
        }
        return response('Bad Request!', 403);
    }



    /**
     * @param array $messages
     *
     * @return bool
     */
    protected function messagesAreValid(array $messages)
    {
        foreach ($messages as $message) {
            $message = collect($message);
            if (!$this->signIsValid($message)) {
                return false;
            }
        }
        return true;
    }



    /**
     * @param Collection $message
     *
     * @return bool
     */
    protected function signIsValid(Collection $message)
    {
        return FanapServiceProvider::verifySign([
             $message->get('ReceiveTime'),
             $message->get('Sid'),
             $message->get('ChannelType'),
             $message->get('Channel'),
             $message->get('Muid'),
             $message->get('Content'),
             $message->get('MessageType'),
             $message->get('AccountId'),
        ], $message->get('Signature'));
    }



    /**
     * @param Request $request
     *
     * @return array
     */
    protected function safeMessages(Request $request)
    {
        if (FanapServiceProvider::arrayIsAssoc($messages = $request->all())) {
            $messages = [$messages];
        }
        return $messages;
    }
}
