<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class VolunteersController extends YasnaController
{
    /**
     * Called from the blade, generating this sidebar menu
     *
     * @return array
     */
    public function sidebarSubMenu()
    {
        return [
             [
                  "link"    => "volunteers/create",
                  "caption" => trans("ehdaManage::ehda.volunteers.create"),
                  "permit"  => user()->as('admin')->can('users-volunteer'),
             ],
             [
                  "link"    => "volunteers/browse",
                  "caption" => trans('ehdaManage::ehda.volunteers.browse'),
                  "permit"  => user()->as('admin')->can('users-volunteer'),
             ],
        ];
    }



    /**
     * @return array
     */
    public function familiarizationCombo()
    {
        return [
            ['1', trans_safe("users::familiarization.friends")],
            ['2', trans_safe("users::familiarization.media")],
            ['3', trans_safe("users::familiarization.website")],
            ['4', trans_safe("users::familiarization.others")],
        ];
    }
}
