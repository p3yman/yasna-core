<?php namespace Modules\EhdaManage\Http\Controllers;

use App\Models\UserPending;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Modules\EhdaManage\Events\TooskaUpdatedUserPending;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\EhdaManage\Http\Api\EhdaApiReply;
use Modules\Yasna\Services\YasnaController;

class TestController extends YasnaController
{
    public function index()
    {
        //dd(user('1')->card_exists_message);
        //$this->tooskaUpdate(model('UserPending', 3));
        event(new TooskaUpdatedUserPending(model('UserPending', 1)));
    }



    public function back(Request $request)
    {
        return $request->all();
    }
}
