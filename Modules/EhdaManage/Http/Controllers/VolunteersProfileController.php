<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\Yasna\Services\YasnaController;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

class VolunteersProfileController extends YasnaController
{
    protected $base_model  = "User";
    protected $view_folder = "ehdamanage::volunteers.profile";



    /**
     * @param $hashid
     *
     * @return View|Response
     */
    public function index($hashid)
    {
        $view_data['user']          = $this->singleAction($hashid, 'index')->getData()['model'];
        $view_data['postCount']     = model('post')->where('created_by', $view_data['user']->id)->count();
        $view_data['printCount']    = model('printing')->where('created_by', $view_data['user']->id)->count();
        $view_data['userMakeCount'] = model('user')->where('created_by', $view_data['user']->id)->count();
        $view_data['activities']    = model('activity')
             ->whereIn('slug', explode_not_empty(',', $view_data['user']->activities))
             ->get()
        ;
        $view_data['hashID']        = $hashid;
        return $this->view('index', $view_data);
    }
}
