<?php namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Modules\EhdaFront\Entities\Printing;
use Modules\EhdaManage\Http\Requests\AddCardToPrintingsRequest;
use Modules\Yasna\Services\YasnaController;

/**
 * Class PrintingsActionController
 *
 * @package Modules\EhdaManage\Http\Controllers
 * @property Printing model($id,$option)
 */
class PrintingsActionController extends YasnaController
{
    protected $base_model  = "Printing";
    protected $view_folder = "ehdamanage::printings.action";
    protected $table;
    protected $request;
    protected $tab_origin;
    protected $new_data;



    /**
     * @param AddCardToPrintingsRequest $request
     *
     * @return string
     */
    public function addSingleCard(AddCardToPrintingsRequest $request)
    {
        $done = $this->model()->addSingleCard($request->from_event_id, $request->model->id);
        $this->rememberLastUsedEvent($request->from_event_id);

        return $this->jsonAjaxSaveFeedback($done);
    }



    /**
     * Shortcut to another controller action, in CardsController
     *
     * @param $id
     */
    private function rememberLastUsedEvent($id)
    {
        module('ehdaManage')->cardsController()->rememberLastUsedEvent($id);
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function addToExcel(Request $request)
    {
        $this->tab_origin = 'pending';
        $this->request    = $request;
        $this->new_data   = model('printing')->requiredDataForExcelPrinting();

        session()->put('excelDownload', true);

        return $this->commonActions();
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function revertToPending(Request $request)
    {
        $this->tab_origin = 'under_excel_printing';
        $this->request    = $request;
        $this->new_data   = model('printing')->requiredDataForReversion();

        return $this->commonActions();
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function cancelPrinting(Request $request)
    {
        $this->tab_origin = "pending";
        $this->request    = $request;
        $this->new_data   = model('printing')->requiredDataForCancellation();

        return $this->commonActions();
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function confirmQuality(Request $request)
    {
        $this->tab_origin = 'under_excel_printing';
        $this->request    = $request;
        $this->new_data   = model('printing')->requiredDataForQualityConfirmation();

        return $this->commonActions();
    }



    /**
     * Selects affecting table and stores into $this->table
     */
    private function tableSelector()
    {
        if ($this->request->select_all) {
            $this->table = $this->model()->selector([
                 "event_id" => $this->request->browse_event_id,
                 "criteria" => $this->tab_origin,
            ])
            ;
            return;
        }

        $this->table = $this->model()->whereIn('id', hashid(explode_not_empty(',', $this->request->ids)));
    }



    /**
     * @return bool
     */
    private function shiftRecords()
    {
        return $this->table->update($this->new_data);
    }



    /**
     * @param bool $done
     *
     * @return string
     */
    private function commonReturn($done)
    {
        return $this->jsonAjaxSaveFeedback($done, [
             'success_refresh' => true,
        ]);
    }



    /**
     * @return string
     */
    private function commonActions()
    {
        $this->tableSelector();
        $done = $this->shiftRecords();

        return $this->commonReturn($done);
    }



    /**
     * @param $event_id
     */
    public function excelDownload($event_id)
    {
        session()->put('excel_event_id', intval($event_id));

        Excel::create('Cards-To-Excel-For-Hard-Print', function ($excel) {
            $excel->sheet('print', function ($sheet) {
                $sheet->loadView('ehdaManage::printings.action.excel_file');
            });
        })
             ->download('xls')
        ;
    }



    /**
     * Generates table required for the Excel File (Called from the blade)
     */
    public function getExcelTable()
    {
        $event_id       = session()->get('excel_event_id');
        $selector_array = [
             "event_id" => $event_id,
             'criteria' => "under_excel_printing",
        ];

        return $this->model()->selector($selector_array)->get();
    }
}
