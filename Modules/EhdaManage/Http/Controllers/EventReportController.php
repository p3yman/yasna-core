<?php namespace Modules\EhdaManage\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Modules\Yasna\Services\YasnaController;

class EventReportController extends YasnaController
{
    private $viewData;



    public static function addColumn($arg)
    {
        service('posts:browse_headings')
             ->add('report')
             ->trans(trans('ehdamanage::event-report.head'))
             ->blade('ehdamanage::event.report-head')
             ->condition($arg['posttype']->slug == "event")
             ->width(200)
             ->order(40)
        ;
    }



    public function index($eventID)
    {
        $event                           = model('post')::find($eventID);
        $this->viewData['data']          = $event->getEventDailyDetail();
        $this->viewData['totalRegister'] = $event->registers()->count();
        $this->viewData['totalPrint']    = model('printing')->where('event_id', $eventID)->count();
        $this->viewData['title']         = $event->title;
        return view('ehdamanage::event.report-grid', $this->viewData);
    }
}
