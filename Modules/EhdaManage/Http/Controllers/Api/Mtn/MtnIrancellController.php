<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/26/18
 * Time: 4:54 PM
 */

namespace Modules\EhdaManage\Http\Controllers\Api\Mtn;

use App\Models\Log;
use App\Models\User;
use App\Models\UserPending;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Modules\EhdaManage\Events\ShortMessageReceived;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\EhdaManage\Http\Api\EhdaApiReply;
use Modules\EhdaManage\Http\Utility\GuessDateFromPersianEntry;
use Modules\EhdaManage\Providers\EhdaManageServiceProvider;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;
use Modules\Yasna\Services\YasnaController;

class MtnIrancellController extends YasnaController
{
    const CHANNEL_NAME = 'MTN';



    /**
     * MessagesController constructor.
     */
    public function __construct()
    {
        //$this->middleware('web')->except('save');
    }



    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($this->isCleaning($request)) {
            $this->cleanAction($request);
            return 'Cleaned';
        } else {
            return $this->indexHandleSession($request);
        }
    }



    /**
     * @param Request $request
     *
     * @return bool
     */
    protected function isCleaning(Request $request)
    {
        return ($request->clean == 'clean_session');
    }



    /**
     * @param Request $request
     *
     * @return void
     */
    protected function cleanAction(Request $request)
    {
        model('user-pending')
             ->birthDateRequestedRecords()
             ->whereMobile($request->MSISDN)
             ->whereChannel(static::CHANNEL_NAME)
             ->forceDelete()
        ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function indexHandleSession(Request $request)
    {
        $pending_user_model = $this->findCurrentPendingUserModel($request);

        if ($pending_user_model->exists) {
            return $this->indexExistedSession($pending_user_model, $request);
        } else {
            return $this->indexNewSession($request);
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function indexNewSession(Request $request)
    {
        $safe_content               = ed($request->input);
        $pre_requested_pending_user = $this->findPreRequestedPendingUser($safe_content);
        if ($this->isRunning($pre_requested_pending_user)) {
            return $this->generateResponse(trans('ehdamanage::tooska-api.request_running'), false);
        } else {
            if ($this->isCodeMelli($safe_content)) {
                return $this->saveNewPendingUser($safe_content, $request->msisdn, $request->SID)
                            ->generateResponse($this->generateUserMessage(ApiResponse::AWAITING_BIRTH_DATE))
                     ;
            } else {
                return $this->generateResponse(trans('ehdamanage::ussd.main-guide'));
            }
        }
    }



    /**
     * @param string $code_melli
     * @param string $mobile
     * @param string $sid
     *
     * @return $this
     */
    protected function saveNewPendingUser(string $code_melli, string $mobile, string $sid)
    {
        model('user-pending')->addTemporaryRecord([
             "code_melli" => $code_melli,
             "mobile"     => $mobile,
             "channel"    => static::CHANNEL_NAME,
             "sid"        => $sid,
        ]);

        return $this;
    }



    /**
     * @param UserPending $pending_user_model
     * @param Request     $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function indexExistedSession(UserPending $pending_user_model, Request $request)
    {
        $birth_date = $this->guessDate($request->input);
        $code_melli = $pending_user_model->code_melli;
        // If this is not birth date
        if ($birth_date) {
            return $this->handleReceivedBirthDate($code_melli, $birth_date, $pending_user_model);
        } else {
            return $this->generateResponse($this->generateUserMessage(ApiResponse::BIRTH_DAY_INVALID));
        }
    }



    /**
     * @param string $text
     *
     * @return false|Carbon
     */
    protected function guessDate(string $text)
    {
        return GuessDateFromPersianEntry::ask($text);
    }



    /**
     * @param string      $code_melli
     * @param Carbon      $birth_date
     * @param UserPending $pending_user_model
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function handleReceivedBirthDate(string $code_melli, Carbon $birth_date, UserPending $pending_user_model)
    {
        $user = $this->findUserWithCodeMelli($code_melli);
        if ($user and $user->is_a(EhdaManageServiceProvider::cardHolderRoleSlug())) {
            return $this->handleBirthDateOfACardHodler($user, $birth_date, $pending_user_model);
        } else {
            $pending_user_model->addBirthDate($birth_date->toDateString());
            return $this->generateResponse(trans('ehdamanage::tooska-api.request_succeeded'), false);
        }
    }



    /**
     * @param User   $user
     * @param Carbon $birth_date
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function handleBirthDateOfACardHodler(User $user, Carbon $birth_date, UserPending $pending_user_model)
    {
        if ($user->birth_date and $birth_date->startOfDay()->equalTo($user->birth_date)) {
            if (FanapServiceProvider::sendMessage($pending_user_model->mobile, $user->card_exists_message)) {
                $pending_user_model->hardDelete();
                return $this->generateResponse(trans('ehdamanage::tooska-api.request_succeeded'), false);
            } else {
                return $this->generateResponse(trans('ehdamanage::ussd.process-error'), false);
            }
        } else {
            return $this->generateResponse(
                 $this->generateUserMessage(ApiResponse::FAILED_VALIDATION_SEND_AGAIN),
                 false
            );
        }
    }



    /**
     * @param string $message
     *
     * @return User|null
     */
    protected function findUserWithCodeMelli($code_melli)
    {
        return user()->whereCodeMelli($code_melli)->limit(1)->first();
    }



    /**
     * @param string $api_response_status
     *
     * @return string
     */
    protected function generateUserMessage(string $api_response_status)
    {
        return trans('ehdamanage::tooska-api.ussd_messages.' . $api_response_status);
    }



    /**
     * @param string $text
     *
     * @return bool
     */
    protected function isCodeMelli(string $text)
    {
        return Validator::make(
             ['message' => $text],
             ['message' => 'required|code_melli']
        )->passes()
             ;
    }



    /**
     * @param Request $request
     *
     * @return UserPending
     */
    protected function findCurrentPendingUserModel(Request $request)
    {
        return model('user-pending')
                  ->birthDateRequestedRecords()
                  ->orderByDesc('created_at')
                  ->whereSid($request->SID)
                  ->whereChannel(static::CHANNEL_NAME)
                  ->first()
             ?? model('user-pending');
    }



    /**
     * @param $code_melli
     *
     * @return UserPending
     */
    protected function findPreRequestedPendingUser($code_melli)
    {
        return model('user-pending')
                  ->orderByDesc('created_at')
                  ->whereChannel(static::CHANNEL_NAME)
                  ->whereCodeMelli($code_melli)
                  ->first()
             ?? model('user-pending');
    }



    /**
     * @param UserPending $user_pending_model
     *
     * @return bool
     */
    protected function isRunning(UserPending $user_pending_model)
    {
        $running_statuses = ['1009', '1010', '1013', '1016', '1017', '1018', '1019  '];
        return in_array($user_pending_model->apiStatus(), $running_statuses);
    }



    /**
     * @param UserPending $user_pending_model
     *
     * @return bool
     */
    protected function isFinalized(UserPending $user_pending_model)
    {
        return !$this->isRunning($user_pending_model);
    }



    /**
     * @return string
     */
    protected function getCpRefIs()
    {
        //@todo: Make it dynamic
        return '12345';
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function logRequest(Request $request)
    {
        $all_request_data['header'] = $request->header();
        $all_request_data['body']   = $request->all();

        $insert = model('log')->batchSave([
             'title' => 'MTN Call Index: ' . Carbon::now()->toDateTimeString(),
             'type'  => 'MTN',
             'data'  => json_encode($all_request_data),
        ]);
        return $this;
    }



    /**
     * @param string $content
     * @param bool   $with_input
     * @param null   $cpRefId
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function generateResponse($content, $with_input = true, $cpRefId = null)
    {
        return response($content, 200, [
             'Freeflow'       => $this->freeflowValue($with_input),
             'cpRefId'        => $cpRefId ?? $this->getCpRefIs(),
             'Expires'        => -1,
             'Pragma'         => 'no-cache',
             'Cache-Control'  => 'max-age=0',
             'Content-Type'   => 'UTF-8',
             'Content-Length' => strlen($content),
        ]);
    }



    /**
     * @param bool $with_input
     *
     * @return string
     */
    protected function freeflowValue($with_input)
    {
        return ($with_input ? 'FC' : 'FB');
    }



    /**
     * @param null $token
     * @param null $type
     *
     * @return int
     */
    public function showLogs($token = null, $type = null)
    {
        if ($token != 'JafarKhan') {
            return 404;
        }

        $logs = model('log')->orderBy('id', 'DESC');

        if ($type) {
            $logs = $logs->where('type', $type);
        }

        $logs = $logs->limit(200)->get();

        if ($logs) {
            echo '<table style="border-collapse: collapse; border: solid 1px black;">';
            foreach ($logs as $log) {
                echo '<tr style="border: solid 1px black;">';
                echo '<td style="border: solid 1px black;">' . $log->id . '</td>';
                echo '<td style="border: solid 1px black;">' . $log->title . '</td>';
                echo '<td style="border: solid 1px black;"><pre>' . $this->prettyPrint($log->data) . '</pre></td>';
                echo '<td style="border: solid 1px black;">' . $log->type . '</td>';
                echo '<td style="border: solid 1px black;">' . $log->created_at . '</td>';
                echo '</tr>';
            }
            echo '</table>';
        }
    }



    /**
     * @param $json
     *
     * @return string
     */
    protected function prettyPrint($json)
    {
        $result          = '';
        $level           = 0;
        $in_quotes       = false;
        $in_escape       = false;
        $ends_line_level = null;
        $json_length     = strlen($json);

        for ($i = 0; $i < $json_length; $i++) {
            $char           = $json[$i];
            $new_line_level = null;
            $post           = "";
            if ($ends_line_level !== null) {
                $new_line_level  = $ends_line_level;
                $ends_line_level = null;
            }
            if ($in_escape) {
                $in_escape = false;
            } elseif ($char === '"') {
                $in_quotes = !$in_quotes;
            } elseif (!$in_quotes) {
                switch ($char) {
                    case '}':
                    case ']':
                        $level--;
                        $ends_line_level = null;
                        $new_line_level  = $level;
                        break;

                    case '{':
                    case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ":
                    case "\t":
                    case "\n":
                    case "\r":
                        $char            = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level  = null;
                        break;
                }
            } elseif ($char === '\\') {
                $in_escape = true;
            }
            if ($new_line_level !== null) {
                $result .= "\n" . str_repeat("\t", $new_line_level);
            }
            $result .= $char . $post;
        }

        return $result;
    }
}
