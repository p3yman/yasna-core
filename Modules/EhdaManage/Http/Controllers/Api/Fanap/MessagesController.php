<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/26/18
 * Time: 4:54 PM
 */

namespace Modules\EhdaManage\Http\Controllers\Api\Fanap;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Modules\EhdaManage\Events\ShortMessageReceived;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;
use Modules\Yasna\Services\YasnaController;

class MessagesController extends YasnaController
{
    /**
     * MessagesController constructor.
     */
    public function __construct()
    {
        $this->middleware('web')->except('save');
    }



    /**
     * @param Request $request
     */
    public function save(Request $request)
    {
        Cache::set('sms:' . md5($serialized = json_encode($request->all())), $serialized);

        if (FanapServiceProvider::arrayIsAssoc($messages = $request->all())) {
            $messages = [$messages];
        }
        $this->saveReceivedMessages($messages);
    }



    /**
     * @param array $messages
     */
    protected function saveReceivedMessages($messages)
    {
        foreach ($messages as $message) {
            $model = model('short-message')->batchSave([
                 'muid'         => ($message = collect($message))->get('Muid'),
                 'sid'          => $message->get('Sid'),
                 'delivered_at' => Carbon::parse($message->get('ReceiveTime')),
                 'channel_type' => $message->get('ChannelType'),
                 'channel'      => $message->get('Channel'),
                 'account_id'   => $message->get('AccountId'),
                 'source'       => $message->get('UserPhoneNumber'),
                 'destination'  => $message->get('Channel'),
                 'message_type' => $message->get('MessageType'),
                 'text'         => $message->get('Content'),
                 'request'      => $message,
            ]);

            event(new ShortMessageReceived($model));
        }
    }
}
