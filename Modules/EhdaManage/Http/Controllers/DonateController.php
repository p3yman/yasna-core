<?php namespace Modules\EhdaManage\Http\Controllers;

use Carbon\Carbon;
use  \Maatwebsite\Excel\Facades\Excel;
use Modules\EhdaManage\Http\Requests\DonateExcelRequest;
use Modules\EhdaManage\Http\Requests\DonateListRequest;
use Modules\Yasna\Services\YasnaController;
use Morilog\Jalali\jDate;

class DonateController extends YasnaController
{

    /**
     * @param DonateListRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * this is action method
     */
    public function lists(DonateListRequest $request)
    {
        $donations           = model('donation')->where('tracking_no', "<>", "")->orderBy('id', 'DESC')->paginate(50);
        $view_data['models'] = $donations;
        if ($request->session()->has('excel_empty')) {
            $view_data['reportEmpty'] = "1";
        }
        $view_data['page'] = [
             ["ehda/donate/list", trans('ehdamanage::donate.sidebar')],
        ];
        return view("ehdamanage::donate.list", $view_data);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * todo improve performance for get types of transaction
     */
    public function modal()
    {
        //$viewData['types']  = $this->getTypes();
        $view_data['status'] = $this->getStatus();

        return view("ehdamanage::donate.excel-modal", $view_data);
    }



    /**
     * @param DonateExcelRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * this is action method
     */
    public function excel(DonateExcelRequest $request)
    {
        $donataions = model('donation')->where('tracking_no', "<>", "");
        //if (!empty($request->get('trans-type'))) {
        //    $transType = $request->get('trans-type');
        //}
        if (!empty($request->get('trans-state'))) {
            $transState = $request->get('trans-state');
        }
        if (!empty($request->donate_date)) {
            $donateFrom = Carbon::parse($request->donate_date);
        }
        if (!empty($request->to_donate_date)) {
            $donateSince = Carbon::parse($request->to_donate_date);
        }

        if (!empty($request->get('tracking-no'))) {
            $track_no = $request->get('tracking-no');
            $donataions->whereIn('tracking_no', $this->getValueOfTrackingNo($track_no));
        }

        if (!empty($donateFrom) || !empty($donateSince)) {
            if (!empty($donateFrom)) {
                $donataions = $donataions->whereDate('created_at', '>=', $donateFrom);
            }

            if (!empty($donateSince)) {
                $donataions = $donataions->whereDate('created_at', '<=', $donateSince);
            }
        }

        $donataions = $donataions->get()->toArray();

        if (isset($transState)) {
            $donataions = $this->checkStatus($donataions, $transState);
        }

        //if (isset($transType)) {
        //    $donataions = $this->checkType($donataions, $transType);
        //}

        if (empty($donataions)) {
            return back()->with('excel_empty', '1');
        } else {
            $excelArr = $donataions;
        }

        Excel::create('Donation-Report-' . jDate::forge()->format('y-m-d'), function ($excel) use ($excelArr) {
            $excel->sheet('Sheetname', function ($sheet) use ($excelArr) {
                $sheet->appendRow([
                     trans('ehdamanage::donate.id'),
                     trans('ehdamanage::report.name'),
                     trans("ehdamanage::donate.price"),
                     trans("ehdamanage::donate.transaction-type"),
                     trans("ehdamanage::donate.transaction-state"),
                     trans("ehdamanage::donate.year"),
                     trans("ehdamanage::donate.month"),
                     trans("ehdamanage::donate.day"),
                     trans("ehdamanage::donate.hour"),
                     trans("ehdamanage::donate.minute"),
                ]);

                foreach ($excelArr as $item) {
                    $row[0] = $item['tracking_no'];
                    $row[1] = $item['name'];
                    $row[2] = $item['amount'];
                    $row[3] = $this->getTypes($item['tracking_no']);
                    $row[4] = $this->getStatus($item['id'])[0]['text'];
                    $jalali = jDate::forge($item['created_at']);
                    $row[5] = $jalali->format('Y');
                    $row[6] = $jalali->format('m');
                    $row[7] = $jalali->format('d');
                    $row[8] = $jalali->format('h');
                    $row[9] = $jalali->format('i');
                    $sheet->appendRow($row);
                }
            });
        })->export('xls')
        ;
        return response();
    }



    /**
     * @param $track_no
     *
     * @return array
     */
    private function getValueOfTrackingNo($track_no)
    {
        if (str_contains($track_no, ",")) {
            return explode_not_empty(",", $track_no);
        } elseif (str_contains($track_no, "-")) {
            return explode_not_empty('-', $track_no);
        } elseif (str_contains($track_no, " ")) {
            return explode_not_empty(" ", $track_no);
        }
        //not found delimiter
        return (array)$track_no;
    }



    /**
     * @param array $donations
     * @param       $type
     *
     * @return array of donations that have this type
     */
    private function checkType(array $donations, $type)
    {
        $resArr = [];
        foreach ($donations as $donation) {
            $donateType = payment()->transaction()->trackingNumber($donation['tracking_no'])->getModel()->type;
            if ($donateType == $type) {
                $resArr[] = $donation;
            }
        }
        return $resArr;
    }



    /**
     * @param array $donations
     * @param       $state
     *
     * @return array
     * check that specific state exist in one donation
     */
    private function checkStatus(array $donations, $state)
    {
        $resArr = [];
        foreach ($donations as $donation) {
            $statues   = $this->getStatus($donation['id']);
            $stateFlag = false;
            foreach ($statues as $status) {
                if ($status['value'] == $state) {
                    $stateFlag = true;
                }
            }
            if ($stateFlag) {
                $resArr[] = $donation;
            }
        }
        return $resArr;
    }



    /**
     * @param null $donateID
     *
     * @return array
     * get all of statues or all of one donation
     */
    private function getStatus($donateID = null)
    {
        $donates      = (is_null($donateID)) ? model('donation')
             ->where('tracking_no', "<>", "")
             ->get() :
             model('donation')
                  ->where('id', $donateID)
                  ->where('tracking_no', "<>", "")
                  ->get()
        ;
        $status       = [];
        $transactions = payment()->transaction();
        $statusFlag   = "";
        foreach ($donates as $donate) {
            $state = $transactions->trackingNumber($donate->tracking_no)->status();
            if ($state['slug'] != $statusFlag) {
                $statusFlag = $state['slug'];
                $status[]   = [
                     'value' => $state['slug'],
                     'text'  => trans('ehdamanage::donate.' . $state['slug']),
                ];
            }
        }
        return $status;
    }



    /**
     * @return array of donations types
     */
    private function getTypes($trackID = null)
    {
        if (!is_null($trackID)) {
            $type = payment()->transaction()->trackingNumber($trackID)->getModel()->type_text;
            return $type;
        }

        $view_data['types'] = config('payment.types');
        $types              = [];
        foreach ($view_data['types'] as $datum) {
            $types[] = [
                 "value" => $datum,
                 "text"  => trans('payment::types.' . $datum),
            ];
        }
        return $types;
    }
}
