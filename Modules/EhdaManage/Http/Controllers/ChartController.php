<?php namespace Modules\EhdaManage\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Modules\Yasna\Services\YasnaController;
use Morilog\Jalali\jDateTime;

class ChartController extends YasnaController
{
    const CACHE_TIME = 60 * 5;



    /**
     * @return array
     */
    public function genderData()
    {
        if (Cache::has('chart_gender')) {
            return Cache::get('chart_gender');
        }
        $men_count   = model('user')->elector(['gender' => '1'])->count();
        $women_count = model('user')->elector(['gender' => '2'])->count();
        $other_count = model('user')->elector(['gender' => '3'])->count();
        $result      = ['menCount' => $men_count, 'womenCount' => $women_count, 'otherCount' => $other_count];
        Cache::set('chart_gender', $result, self::CACHE_TIME);
        return $result;
    }



    /**
     * @return array
     * this type is according to login by api
     */
    public function typeOfUserData()
    {
        if (Cache::has('chart_type_of_users')) {
            return Cache::get('chart_type_of_users');
        }
        $switch    = [
             'roleString' => 'api',
        ];
        $api_users = model('user')->elector($switch)->get();

        foreach ($api_users as $api_user) {
            $users[$api_user->id] = [
                 "api_info"  => $api_user,
                 "api_users" => model('user')->where('created_by', $api_user->id)->count(),
            ];
        }
        $users[0] = [
             "api_info"  => "0",
             "api_users" => model('user')->where('created_by', "0")->count(),
        ];
        $result   = ['typeOfUsers' => $users];
        Cache::set('chart_type_of_users', $result, self::CACHE_TIME);
        return $result;
    }



    /**
     * @return array
     */
    public function provinceCardsData()
    {
        if (Cache::has('chart_province')) {
            return Cache::get('chart_province');
        }
        $provinces              = model('state')::getProvinces()->get();
        $view_data['provinces'] = $provinces->toArray();
        $user_by_card           = model('user')->elector([
             "users-by-card" => true,
        ]);
        $result                 = [];
        foreach ($provinces as $province) {
            $modelObj                 = clone $user_by_card;
            $result[$province->title] = $modelObj->where('home_province', $province->id)->count();
        }
        $view_data['provinceUsers'] = $result;
        Cache::set('chart_province', $view_data, self::CACHE_TIME);
        return $view_data;
    }



    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * //call by ajax in province widget
     */
    public function citiesCardDate($id)
    {
        $provinces    = model('state')::getProvinces()->get();
        $cities       = model("state")->where('parent_id', $id)->get();
        $user_by_card = model('user')->elector([
             "users-by-card" => true,
        ]);
        $lables       = [];
        $data         = [];
        foreach ($cities as $city) {
            $modelObj = clone $user_by_card;
            $lables[] = $city->title;
            $data[]   = $modelObj->where('home_city', $city->id)->count();
        }
        return view("ehdamanage::chart.widgets.city-users",
             ['data' => $data, 'lables' => $lables, 'province_id' => $id, 'provinces' => $provinces->toArray()]);
    }



    /**
     * @return array
     * latest 12 month users
     */
    public function get12MonthRegisters()
    {
        if (Cache::has('chart_latest_register')) {
            return Cache::get('chart_latest_register');
        }
        $today      = Carbon::today();
        $month_name = [];
        for ($i = 0; $i <= 12; $i++) {
            $jalali_today = jDateTime::toJalali($today->year, $today->month, $today->day);
            $jalali_year  = $jalali_today[0];
            $jalali_month = $jalali_today[1];
            $startDay     = 1;
            if ($jalali_month == 12) {
                $jalali_next_month = 1;
                $jalali_next_year  = $jalali_year + 1;
            } else {
                $jalali_next_month = $jalali_month + 1;
                $jalali_next_year  = $jalali_year;
            }
            $computed_start_date                                    = jDateTime::toGregorian($jalali_year,
                 $jalali_month,
                 $startDay);
            $computed_end_date                                      = jDateTime::toGregorian($jalali_next_year,
                 $jalali_next_month, $startDay);
            $start_carbon                                           = Carbon::createFromDate($computed_start_date[0],
                 $computed_start_date[1], $computed_start_date[2]);
            $end_carbon                                             = Carbon::createFromDate($computed_end_date[0],
                 $computed_end_date[1], $computed_end_date[2]);
            $register_count                                         = model('user')->elector([
                 'after-users-card-register'  => $start_carbon->startOfDay()->toDateTimeString(),
                 'before-users-card-register' => $end_carbon->endOfDay()->toDateTimeString(),
            ])->count()
            ;
            $month_name[jdate($today->timestamp)->format('%B  %Y')] = $register_count;
            $today->subMonth(1);
        }
        $result = ['mothRegister' => $month_name];
        Cache::set('chart_latest_register', $result, self::CACHE_TIME);
        return $result;
    }



    /**
     * @return array
     */
    public function ageCategoryData()
    {
        if (Cache::has('chart_age')) {
            return Cache::get('chart_age');
        }
        $res_array           = [];
        $res_array["0-10"]   = model('user')->elector(['age-max' => '10', 'age-min' => '0'])->count();
        $res_array["10-20"]  = model('user')->elector(['age-max' => '20', 'age-min' => '10'])->count();
        $res_array["20-30"]  = model('user')->elector(['age-max' => '30', 'age-min' => '20'])->count();
        $res_array["30-40"]  = model('user')->elector(['age-max' => '40', 'age-min' => '30'])->count();
        $res_array["40-50"]  = model('user')->elector(['age-max' => '50', 'age-min' => '40'])->count();
        $res_array["60-..."] = model('user')->elector(['age-min' => '60'])->count();
        $result              = ['ageCategory' => $res_array];
        Cache::set('chart_age', $result, self::CACHE_TIME);
        return $result;
    }



    /**
     * @return array
     * this type according to disable or enable of volunteers
     */
    public function typeOfVolunteer()
    {
        if (Cache::has('chart_volunteer_type')) {
            return Cache::get('chart_volunteer_type');
        }
        $res             = [];
        $volunteer_roles = model('role')->where('slug', 'like', 'volunteer-%_')->pluck('slug')->toArray();
        $switch          = [
             'role' => $volunteer_roles,
        ];
        $all_volunteers  = model('user')->elector($switch)->count();

        $switch = [
             'role'   => $volunteer_roles,
             'status' => 8,
        ];
        $res['enables_volunteer'] = model('user')->elector($switch)->count();

        $switch                   = [
             'role'   => $volunteer_roles,
             'status' => 1,
        ];
        $res['waiting_volunteer'] = model('user')->elector($switch)->count();

        $res['other_volunteer'] = $all_volunteers - array_sum(array_values($res));

        Cache::set('chart_volunteer_type', $res, self::CACHE_TIME);

        return $res;
    }



    /**
     * @return array
     * get latest 10 registrable event and their users
     */
    public function eventsRegistrable()
    {
        if (Cache::has('chart_event_registrable')) {
            return Cache::get('chart_event_registrable');
        }
        $posts = model('post')
             ->with('categories')
             ->where('type', 'event')
             ->orderByDesc('created_at')
             ->limit(10)
             ->get(['id', 'title'])
        ;

        $result = [];
        foreach ($posts as $post) {
            if ($post->categories->where('slug', 'registrable')->count()) {
                $result[$post->title] = $post->registers()->count();
            }
        }

        $result = ['events' => $result];
        Cache::set('chart_event_registrable', $result, self::CACHE_TIME);

        return $result;
    }
}
