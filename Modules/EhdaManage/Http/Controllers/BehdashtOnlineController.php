<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\EhdaManage\Entities\UserPending;
use Modules\EhdaManage\Http\Services\BehdashtSoap;
use Modules\Yasna\Services\YasnaController;
use Nwidart\Modules\Json;
use SoapClient;

class BehdashtOnlineController extends YasnaController
{
    protected $base_model = "UserPending";



    /**
     * @param UserPending $model
     *
     * @return bool
     */
    public function inquery($model)
    {
        chalk()->add("BehdashtOnlineController: started on model #$model->id");

        $person = new BehdashtSoap($model->code_melli);

        chalk()->add("BehdashtOnlineController: Soap called on #$model->id");
        chalk()->add($person);

        if ($person->isNotValid() or $person->isDead()) {
            chalk()->add("BehdashtOnlineController: found invalidity on #$model->id");

            $model->markAsOfflineRejected();
            $model->markAsOnlineRejected();

            chalk()->add("BehdashtOnlineController: set invalidity flags on #$model->id");
            return false;
        }

        chalk()->add("BehdashtOnlineController: checking birthday of #$model->id");

        if (!$this->checkBirthday($model, $person)) {
            chalk()->add("BehdashtOnlineController: Birthday failed on #$model->id");

            $model->markAsOnlineRejected();
            chalk()->add("BehdashtOnlineController: Flagged birthday failure on #$model->id");
            return false;
        }

        chalk()->add("BehdashtOnlineController: saving meta #$model->id");

        $return = $this->saveMeta($model, $person);

        chalk()->add("BehdashtOnlineController: saved meta #$model->id");

        chalk()->add("registerSelectedApprovedUser: start #$model->id");
        module('ehdaManage')->tooskaRegisterController()->registerSelectedApprovedUser($model);
        chalk()->add("registerSelectedApprovedUser: end #$model->id");


        return $return;
    }



    /**
     * @param UserPending  $model
     * @param BehdashtSoap $found_person
     *
     * @return bool
     */
    private function checkBirthday($model, $found_person)
    {
        $declared_date = jalaliArray($model->birth_date);

        $valid_year  = boolval($declared_date['year'] == $found_person->birth_year);
        $valid_month = boolval($declared_date['month'] == $found_person->birth_month);
        $valid_day   = boolval($declared_date['day'] == $found_person->birth_day);

        return $valid_year and $valid_month and $valid_day;
    }



    /**
     * @param UserPending  $model
     * @param BehdashtSoap $person
     *
     * @return bool
     */
    private function saveMeta($model, $person)
    {
        return $model->batchSaveBoolean([
             "name_first"           => $person->name_first,
             "name_last"            => $person->name_last,
             "name_father"          => $person->name_father,
             "gender"               => $person->gender,
             "online_requested_at"  => $this->now(),
             "online_verified_at"   => $this->now(),
             "offline_requested_at" => $this->now(),
             "offline_verified_at"  => $this->now(),
        ]);
    }
}
