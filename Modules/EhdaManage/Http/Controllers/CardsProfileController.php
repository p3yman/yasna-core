<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\EhdaManage\Http\Requests\CardDeleteRequest;
use Modules\Yasna\Services\YasnaController;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class CardsProfileController extends YasnaController
{
    protected $base_model  = "User";
    protected $view_folder = "ehdamanage::cards.profile";



    /**
     * @param $hashid
     *
     * @return View|Response
     */
    public function index($hashid)
    {
        $view_data['user']      = $this->singleAction($hashid, 'index')->getData()['model'];
        $view_data['printData'] = module('ehdamanage')->callControllerMethod('CardsEditController', 'loadEvents');
        $view_data['hashID']    = $hashid;
        return $this->view('index', $view_data);
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function printing(Request $request)
    {
        $done = model('Printing')->addSingleCard($request->event, $request->user_id);
        module('ehdaManage')->cardsController()->rememberLastUsedEvent($request->event);
        return $this->jsonAjaxSaveFeedback($done);
    }



    /**
     * @param CardDeleteRequest $request
     *
     * @return string
     */
    public function deleteCard(CardDeleteRequest $request)
    {
        $done = model('user', $request->id)->cardDelete();

        return $this->jsonAjaxSaveFeedback($done, [
             "success_callback" => "rowHide('tblUsers','$request->hashid')",
        ]);
    }
}
