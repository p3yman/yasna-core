<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class EhdaUsersHandleController extends YasnaController
{

    /**
     * Create Button on top of the manage area
     */
    public static function manageCreateButtons()
    {
        module('manage')
             ->service('nav_create')
             ->add('card-holder')
             ->caption("trans:ehdaManage::ehda.cards.register_full")
             ->condition(user()->as('admin')->can('cards.create'))
             ->link("url:manage/cards/create")
             ->icon('credit-card')
             ->color('success')
             ->order(1)
        ;

        module('manage')
             ->service('nav_create')
             ->add('volunteer')
             ->caption("trans:ehdaManage::ehda.volunteers.create")
             ->condition(user()->as('admin')->can('volunteers.create'))
             ->link("url:manage/volunteers/create")
             ->icon('child')
             ->color('success')
             ->order(2)
        ;
    }
}
