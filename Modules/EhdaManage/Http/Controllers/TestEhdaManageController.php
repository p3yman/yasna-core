<?php

namespace Modules\EhdaManage\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

use Nwidart\Modules\Facades\Module;

class TestEhdaManageController extends YasnaController
{
    public function volunteer()
    {
        $user = [
             "full_name" => "نگار جمالی‌فرد" ,
             "email" => "n.jamalifard@gmail.com" ,
             "phone_number" => "02122557733" ,
             "code_melli" => "00125428739" ,
             "birthday" => [
                  "date" => "۴ آبان ۱۳۷۲" ,
                  "location" => "تهران/ تهران" ,
             ] ,
             "education" => [
                  "degree" => "کارشناسی" ,
                  "major" => "فیزیک حالت جامد" ,
             ] ,
             "address" => "تهران، خیابان آیت، ساختمان رویال، پلاک ۳۷۶، واحد ۱۲" ,
             "activity" => [
                  "content" => "20" ,
                  "registration" => "15" ,
                  "print" => "12" ,
             ] ,
            "other_activities" => [
                 "برپایی غرفه‌‌های اهدا",
                 "برگزاری جشن نفس",
                 "برپایی غرفه‌‌های اهدا",
                 "برگزاری جشن نفس",
                 "برپایی غرفه‌‌های اهدا",
                 "برگزاری جشن نفس",
            ] ,
            "active_time" => "۵ روز در هفته" ,
             "roles" => [
                  [
                       "title" => "سفیر تهران" ,
                       "color" => "success" ,
                       "icon" => "check" ,
                  ],
                  [
                       "title" => "مدیریت نقش" ,
                       "color" => "default" ,
                       "icon" => "cog" ,
                  ],
             ] ,
            "registration" => [
                 "date" => "۱۸ اسفند ۱۳۹۵ [۵:۱۰]" ,
            ] ,
            "update" => [
                 "date" => "۱۸ اسفند ۱۳۹۵ [۵:۱۰]" ,
                 "by" => "نگار جمالی‌فرد" ,
            ] ,
        ];
        return view('ehdamanage::test.modals.volunteer', compact('user'));
    }



    public function member()
    {
        $user = [
             "full_name" => "نگار جمالی‌فرد" ,
             "card_number" => "120" ,
             "card_image" => Module::asset('ehdamanage:images/card.png'),
             "email" => "n.jamalifard@gmail.com" ,
             "phone_number" => "02122557733" ,
             "code_melli" => "00125428739" ,
             "birthday" => [
                  "date" => "۴ آبان ۱۳۷۲" ,
                  "location" => "تهران/ تهران" ,
             ] ,
             
             "education" => [
                  "degree" => "کارشناسی" ,
                  "major" => "فیزیک حالت جامد" ,
             ] ,
             "address" => "تهران، خیابان آیت، ساختمان رویال، پلاک ۳۷۶، واحد ۱۲" ,
             "activity" => [
                  "content" => "20" ,
                  "registration" => "15" ,
                  "print" => "12" ,
             ] ,
             "other_activities" => [
                  "برپایی غرفه‌‌های اهدا",
                  "برگزاری جشن نفس",
                  "برپایی غرفه‌‌های اهدا",
                  "برگزاری جشن نفس",
                  "برپایی غرفه‌‌های اهدا",
                  "برگزاری جشن نفس",
             ] ,
             "active_time" => "۵ روز در هفته" ,
             "roles" => [
                  [
                       "title" => "سفیر تهران" ,
                       "color" => "success" ,
                       "icon" => "check" ,
                  ],
                  [
                       "title" => "مدیریت نقش" ,
                       "color" => "default" ,
                       "icon" => "cog" ,
                  ],
             ] ,
             "registration" => [
                  "date" => "۱۸ اسفند ۱۳۹۵ [۵:۱۰]" ,
             ] ,
             "update" => [
                  "date" => "۱۸ اسفند ۱۳۹۵ [۵:۱۰]" ,
                  "by" => "نگار جمالی‌فرد" ,
             ] ,
        ];
        return view('ehdamanage::test.modals.member', compact('user'));
    }
}
