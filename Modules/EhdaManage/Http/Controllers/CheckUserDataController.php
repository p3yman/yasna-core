<?php namespace Modules\EhdaManage\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Modules\EhdaManage\Http\Services\BehdashtSoap;
use Modules\Yasna\Services\YasnaController;
use Morilog\Jalali\jDateTime;

/**
 * notice: for start checking you must set converted filed in user tbl to 0
 * Class CheckUserDataController
 *
 * @package Modules\EhdaManage\Http\Controllers
 */
class CheckUserDataController extends YasnaController
{
    protected $view_folder          = "ehdamanage::check_users";
    private $check_obj;
    private $user;
    private $list_of_converted_id = [];
    private $user_info            = [];
    private $users_count          = 100;



    /**
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function loadPage()
    {
        return $this->view('index');
    }



    /**
     * @return json
     */
    public function callAjax()
    {
        $users = $this->getUsers();

        foreach ($users as $user) {
            $this->user                   = $user;
            $this->list_of_converted_id[] = $user->id;
            if (!$this->isValidCodeMelli()) {
                continue;
            }
            if (!$this->checkRole()) {
                continue;
            }
            $this->check_obj = new BehdashtSoap($user->code_melli);
            if (!$this->checkValid()) {
                continue;
            }
            $this->checkDead();
            $this->checkNames();
            $this->checkMobile();
            $this->checkGender();
            $this->checkBirthDate();
            $this->checkProvince();
            $this->checkCardNo();
            $this->setFlags();
        }
        //count done items
        $converted_count = $this->calculateCount();

        //$this->resetConvertField();

        $result = [
             'all'               => model('user')->count(),
             'converted'         => $converted_count,
             'converted_list_id' => $this->list_of_converted_id,
             'user_info'         => $this->user_info,
        ];

        return json_encode($result);
    }



    /**
     * check that user has any of volunteer access or card-holder access
     *
     * @return bool
     */
    private function checkRole()
    {
        $volunteer_roles = model('role')->where('slug', 'like', '%volunteer-%')->get()->pluck('slug');
        if ($this->user->isAnyOf(array_merge($volunteer_roles->toArray(), ['card-holder']))) {
            return true;
        } else {
            $this->user_info[$this->user->id]['state'] = 'invalid-user';
            $this->user->batchSave([
                 'converted' => '1',
            ]);
            return false;
        }
    }



    /**
     *checking code melli standard
     */
    private function isValidCodeMelli()
    {
        $flag = Validator::make(['code_melli' => $this->user->code_melli], ['code_melli' => 'code_melli'])->fails();

        if ($flag) {
            $this->user_info[$this->user->id]['state'] = 'invalid-user';
            $this->user->batchSave([
                 'incomplete_at' => Carbon::now()->toDateTimeString(),
                 'converted'     => '1',
            ]);
            return false;
        }
        return true;
    }



    /**
     * check user is valid person
     */
    private function checkValid()
    {
        if (!$this->check_obj->isValid()) {
            $this->user_info[$this->user->id]['state'] = 'invalid-user';
            $this->user->batchSave([
                 'incomplete_at' => Carbon::now()->toDateTimeString(),
                 'converted'     => '1',
            ]);
            return false;
        } else {
            $this->user_info[$this->user->id]['state'] = 'valid-user';
            return true;
        }
    }



    /**
     * set flag after checks
     */
    private function setFlags()
    {
        $this->user->batchSave([
             'converted'         => '1',
             'ahval_verified_at' => Carbon::now()->toDateTimeString(),
        ]);
    }



    /**
     * reset state of converted filed
     */
    private function resetConvertField()
    {
        $this->user->batchSave(['converted' => 0]);
    }



    /**
     * @return count of user checks
     */
    private function calculateCount()
    {
        return model('user')->where(function ($q) {
            $q->whereNotNull('ahval_verified_at')
              ->orWhereNotNull('incomplete_at')
              ->orWhere('converted', '1')
            ;
        })->count()
             ;
    }



    /**
     * @return all user that should be check
     */
    private function getUsers()
    {
        return model('user')
             ->where('converted', '0')
             ->whereNull('ahval_verified_at')
             ->limit($this->users_count)
             ->get()
             ;
    }



    /**
     * if person is dead set flag
     */
    private function checkDead()
    {
        if (!$this->check_obj->isAlive()) {
            $date = jDateTime::createCarbonFromFormat('Y/m/d',
                 ed($this->check_obj->death_year) . '/' .
                 ed($this->check_obj->death_month) . '/' .
                 ed($this->check_obj->death_day)
            )->toDateString()
            ;

            if ($date) {
                $this->user->batchSave(['dead_at' => $date]);
            }
        };
    }



    /**
     * @return bool
     */
    private function checkNames()
    {
        $change_flag = false;
        if (pd($this->check_obj->name_first) !== $this->user->name_first) {
            $change_flag = true;
        }
        if (pd($this->check_obj->name_last) !== $this->user->name_last) {
            $change_flag = true;
        }
        if (pd($this->check_obj->name_father) !== $this->user->name_father) {
            $change_flag = true;
        }

        if ($change_flag) {
            model('user', $this->user->id)->batchSave([
                 'user_name_first'   => pd($this->user->name_first),
                 'user_name_last'    => pd($this->user->name_last),
                 'user_name_father'  => pd($this->user->name_father),
                 'name_first'        => pd($this->check_obj->name_first),
                 'name_last'         => pd($this->check_obj->name_last),
                 'name_father'       => pd($this->check_obj->name_father),
                 'names_rejected_at' => Carbon::now()->toDateTimeString(),
            ]);
        }
        return true;
    }



    /**
     * @return bool
     */
    private function checkMobile()
    {
        $number           = ed($this->user->mobile);
        $incomplete_array = [
             'incomplete_at' => Carbon::now()->toDateTimeString(),
             'mobile'        => null,
             'user_mobile'   => $this->user->mobile,
        ];

        if (starts_with($number, '989')) {
            $number = str_replace('989', '09', $number);
        }

        if (starts_with($number, '+989')) {
            $number = str_replace('+989', '09', $number);
        }

        if (starts_with($number, '00989')) {
            $number = str_replace('00989', '09', $number);
        }


        if (starts_with($number, '+00989')) {
            $number = str_replace('+00989', '09', $number);
        }

        if (starts_with($number, '9')) {
            $number = "0$number";
        }

        $flag = Validator::make(['mobile' => $number], ['mobile' => 'phone:mobile'])->fails();


        if ($flag) {
            $this->user->batchSave($incomplete_array);
            return false;
        }

        $success_array = ['mobile' => ed($number)];

        if (ed($number) !== $this->user->mobile) {
            $success_array['user_mobile'] = $this->user->mobile;
        }

        $this->user->batchSave($success_array);

        return true;
    }



    /**
     * override gender
     */
    private function checkGender()
    {
        $this->user->batchSave(['gender' => ed($this->check_obj->gender)]);
    }



    /**
     * override birth date
     */
    private function checkBirthDate()
    {
        $date = jDateTime::createCarbonFromFormat('Y/m/d',
             ed($this->check_obj->birth_year) . '/' .
             ed($this->check_obj->birth_month) . '/' .
             ed($this->check_obj->birth_day)
        )->toDateString()
        ;

        if ($date) {
            $this->user->batchSave(['birth_date' => $date]);
        }
    }



    /**
     * set empty province with their cities
     */
    private function checkProvince()
    {
        $this->checkHomeProvince();
        $this->checkWorkProvince();
        $this->checkEduProvince();
    }



    /**
     * checking work province field
     */
    private function checkWorkProvince()
    {
        if (empty($this->user->work_city)) {
            return;
        }

        $province = model('state', $this->user->work_city)->province()->id;
        if ($province) {
            $this->user->batchSave([
                 'work_province' => $province,
            ]);
        } else {
            $this->user->batchSave([
                 'work_province'             => null,
                 'user_work_city'            => $this->user->work_city,
                 'work_city'                 => null,
                 'work_province_rejected_at' => Carbon::now()->toDateTimeString(),

            ]);
        }
    }



    /**
     * checking home province field
     */
    private function checkHomeProvince()
    {
        if (empty($this->user->home_city)) {
            return;
        }

        $province = model('state', $this->user->home_city)->province()->id;
        if ($province) {
            $this->user->batchSave([
                 'home_province' => $province,
            ]);
        } else {
            $this->user->batchSave([
                 'home_province'             => null,
                 'user_home_city'            => $this->user->home_city,
                 'home_city'                 => null,
                 'home_province_rejected_at' => Carbon::now()->toDateTimeString(),
            ]);
        }
    }



    /**
     * checking edu province field
     */
    private function checkEduProvince()
    {
        if (empty($this->user->edu_city)) {
            return;
        }

        $province = model('state', $this->user->edu_city)->province()->id;
        if ($province) {
            $this->user->batchSave([
                 'edu_province' => $province,
            ]);
        } else {
            $this->user->batchSave([
                 'edu_province'             => null,
                 'user_edu_city'            => $this->user->edu_city,
                 'edu_city'                 => null,
                 'edu_province_rejected_at' => Carbon::now()->toDateTimeString(),
            ]);
        }
    }



    /**
     * set card_no
     */
    private function checkCardNo()
    {
        if ($this->user->is('card_holder')) {
            if ($this->user->card_no) {
                $this->user->assignRoleAndCardNo();
            }
        }
    }
}
