<?php namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\View;
use Modules\EhdaManage\Events\NewCardRegistered;
use Modules\EhdaManage\Http\Requests\CodeMelliInquiryRequest;
use Modules\EhdaManage\Http\Requests\CardSaveForActiveVolunteersRequest;
use Modules\EhdaManage\Http\Requests\CardSaveRequest;
use Modules\Yasna\Services\YasnaController;
use Symfony\Component\HttpFoundation\Response;

class CardsEditController extends YasnaController
{
    protected $base_model     = "User";
    protected $view_folder    = "ehdamanage::cards.editor";
    protected $request_role   = "card-holder";
    protected $editor_mode;
    protected $page;
    protected $model;
    protected $states;
    protected $events;
    protected $create_view;
    protected $view_arguments = [];



    /**
     * @param $hashid
     *
     * @return View|Response
     */
    public function edit($hashid)
    {
        /*-----------------------------------------------
        | Model ...
        */
        $this->loadUserByHashid($hashid);
        if (!$this->model->id or $this->model->withDisabled()->is_not_a($this->request_role)) {
            return $this->abort(410);
        }
        if ($this->model->ehdaCannotEdit()) {
            return $this->abort(403);
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->editor_mode = 'edit';
        $this->buildPageInfo();
        $this->loadEvents();
        $this->loadStates();
        $this->buildViewArguments();

        /*-----------------------------------------------
        | View ...
        */
        return $this->view('index', $this->view_arguments);
    }



    /**
     * @param bool $given_code_melli
     *
     * @return View|Response
     */
    public function create($given_code_melli = false)
    {
        /*-----------------------------------------------
        | Validations ...
        */
        if ($given_code_melli and !module("EhdaManage")->cardsController()->checkCodeMelli($given_code_melli)) {
            return $this->abort(410);
        }
        if (!user()->as($this->request_role)->canCreate()) {
            return $this->abort(403);
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->editor_mode = 'create';
        $this->buildPageInfo();
        $this->loadUserByCodeMelli($given_code_melli);
        $this->loadEvents();
        $this->loadStates();
        $this->loadAvailableRoles();
        $this->buildViewArguments();
        $this->chooseCreateView();

        /*-----------------------------------------------
        | View ...
        */
        return $this->view($this->create_view, $this->view_arguments);
    }



    /**
     * Reserved and intentionally left blank for the safety purposes.
     */
    protected function loadAvailableRoles()
    {
        //
    }



    /**
     * Chooses Create View
     */
    protected function chooseCreateView()
    {
        $this->create_view = 'index';

        if ($this->model->is_a('card-holder')) {
            $this->create_view = 'form-for-card-holders';
            return;
        }


        if ($this->model->min(8)->is_admin() and $this->model->ehdaCannotEdit()) {
            $this->create_view = 'form-for-volunteers';
            return;
        }
    }



    /**
     * Load Events Table
     *
     * @return Collection
     */
    public function loadEvents()
    {
        return $this->events = post()->registrableEvents()->orderBy('published_at', 'desc')->get();
    }



    /**
     * Loads states, in a way suitable to be shown in combo box.
     */
    private function loadStates()
    {
        $this->states = model('state')->combo();
    }



    /**
     * @param $code_melli
     */
    private function loadUserByCodeMelli($code_melli)
    {
        $this->loadABlankUser();
        $this->enrichUserByRealData($code_melli);

        $this->model->code_melli    = $code_melli;
        $this->model->from_event_id = module('ehdaManage')->CardsController()->getLastUsedEvent();
    }



    /**
     * Loads a New Instance of User
     */
    private function loadABlankUser()
    {
        $this->model             = model('user');
        $this->model->newsletter = 1;
        $this->model->birth_city = 0;
        $this->model->home_city  = 0;
        $this->model->newsletter = 1;
    }



    /**
     * Tries to find a non-card-holder user and replace data with already defined $this->model
     *
     * @param $code_melli
     */
    private function enrichUserByRealData($code_melli)
    {
        $found_model = user()->finder($code_melli);
        if ($found_model->id and $code_melli === $found_model->code_melli) {
            $this->model = $found_model;
        }
    }



    /**
     * @param $hashid
     */
    private function loadUserByHashid($hashid)
    {
        $this->model                = user($hashid);
        $this->model->from_event_id = module('ehdaManage')->CardsController()->getLastUsedEvent();
    }



    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    protected function buildPageInfo()
    {
        $this->page[0] = ['cards/browse', trans_safe("ehdaManage::ehda.donation_cards")];
        $this->page[1] = ["cards/$this->editor_mode", trans("ehdaManage::ehda.cards.$this->editor_mode")];
    }



    /**
     * Builds view arguments and stores in $this->view_arguments
     */
    protected function buildViewArguments()
    {
        $this->view_arguments = [
             "page"        => $this->page,
             "model"       => $this->model,
             "events"      => $this->events,
             "editor_mode" => $this->editor_mode,
             "states"      => $this->states,
        ];
    }



    /**
     * @param CodeMelliInquiryRequest $request
     *
     * @return string
     */
    public function inquiry(CodeMelliInquiryRequest $request)
    {
        $user = user()->finder($request->code_melli);


        if (!$user->exists) {
            return $this->inquiryResponseWhenNotFound();
        } elseif ($user->is_a('card-holder')) {
            return $this->inquiryResponseWhenHasCard($user);
        } elseif ($user->is_an('admin') and $user->is_not_a('card-holder')) {
            return $this->inquiryResponseWhenActiveVolunteerHasnotCard($user);
        } elseif ($user->withDisabled()->is_an('admin')) {
            return $this->inquiryResponseWhenDisabledVolunteer($user); //@TODO
        }

        return $this->jsonFeedback('invalid!');
    }



    /**
     * Inquiry Response When User Not Found At All
     *
     * @return string
     */
    protected function inquiryResponseWhenNotFound()
    {
        return $this->jsonFeedback([
             "ok"           => "1",
             "message"      => trans_safe("ehdaManage::ehda.cards.inquiry_success"),
             'callback'     => 'cardEditor(1)',
             'redirectTime' => 1,
        ]);
    }



    /**
     * @param $user
     *
     * @return string
     */
    private function inquiryResponseWhenHasCard($user)
    {
        return $this->jsonFeedback([
             "ok"           => "1",
             "message"      => trans_safe("ehdaManage::ehda.cards.inquiry_has_card"),
             "callback"     => "cardEditor(2, '$user->hashid')",
             "redirectTime" => "1",
        ]);
    }



    /**
     * @param $user
     *
     * @return string
     */
    private function inquiryResponseWhenActiveVolunteerHasnotCard($user)
    {
        return $this->jsonFeedback([
             "ok"           => "1",
             "message"      => trans_safe("ehdaManage::ehda.cards.inquiry_is_volunteer"),
             'redirect'     => url("manage/cards/create/$user->code_melli"),
             'redirectTime' => 1,
        ]);
    }



    /**
     * @param $user
     *
     * @return string
     */
    private function inquiryResponseWhenDisabledVolunteer($user)
    {
        return $this->jsonFeedback([
             "ok"       => "1",
             "message"  => trans_safe("ehdaManage::ehda.cards.inquiry_is_volunteer"),
             "redirect" => user()->as('admin')->can('cards.create') ? url("manage/cards/create/$user->code_melli") : '',
        ]);

        //@TODO: Suspicious Redirect condition!
    }



    /**
     * @param CardSaveRequest $request
     *
     * @return string
     */
    public function save(CardSaveRequest $request)
    {
        return $this->commonSave($request);
    }



    /**
     * @param CardSaveForActiveVolunteersRequest $request
     *
     * @return string
     */
    public function saveForActiveVolunteer(CardSaveForActiveVolunteersRequest $request)
    {
        return $this->commonSave($request);
    }



    /**
     * @param $request
     *
     * @return string
     */
    private function commonSave($request)
    {
        module('ehdaManage')->CardsController()->rememberLastUsedEvent($request->_from_event_id);

        $saved_model = $request->model->batchSave($request);

        $saved_model->assignRoleAndCardNo();
        $this->sendToPrint($request, $saved_model);

        if ($request->model->not_exists and $saved_model->exists) {
            event(new NewCardRegistered($saved_model));
        }

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             "success_redirect" => $this->successRedirect($request->model),
        ]);
    }



    /**
     * @param $model
     *
     * @return string
     */
    private function successRedirect($model)
    {
        if ($model->exists) {
            return url("manage/cards/edit/$model->hashid");
        } else {
            return url("manage/cards/create/");
        }
    }



    /**
     * @param $request
     * @param $user
     */
    private function sendToPrint($request, $user)
    {
        if ($request->_submit == 'print' and $user->exists) {
            model("Printing")->addSingleCard($request->_from_event_id, $user->id);
        }
    }
}
