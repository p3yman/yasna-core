<?php

namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\EhdaManage\Http\Requests\PeopleSearchRequest;
use Modules\EhdaManage\Http\Services\BehdashtSoap;
use Modules\EhdaManage\Providers\EhdaManageServiceProvider;
use Modules\Yasna\Services\YasnaController;

class HomeController extends YasnaController
{
    /**
     * @param PeopleSearchRequest $request
     *
     * @return string
     */
    public function searchPeople(PeopleSearchRequest $request)
    {
        /*
        | Search
        */
        $keyword = $request->keyword;
        $field   = null;

        if (str_contains($keyword, '@') and str_contains($keyword, '.')) { // <~~ Probably it's an email.
            $field = 'email';
        } elseif (str_contains($keyword, 'hashid')) {
            $keyword = trim(str_replace('hashid', null, $keyword));
            if (is_numeric($keyword)) {
                return $this->jsonFeedback(hashid_encrypt($keyword, 'ids'));
            } else {
                return $this->jsonFeedback(hashid_decrypt0($keyword, 'ids'));
            }
        } elseif (str_contains($keyword, 'id')) {
            $field   = 'id';
            $keyword = str_replace('id', null, $keyword);
        } elseif (EhdaManageServiceProvider::isCodeMelli($keyword)) {
            $field = 'code_melli';
        } elseif (EhdaManageServiceProvider::isPhoneNumber($keyword)) {
            $field = 'mobile';
        } elseif (is_numeric($keyword)) {
            $field = 'card_no';
        } else {
            $field = 'id';
        }

        $query       = model('user')->where($field, $keyword);
        $foundModels = $query->orderBy('id', 'desc');
        $model       = $foundModels->first();
        $total_found = $foundModels->count();

        $success_message = view(
             'ehdamanage::dashboard.search-people.result',
             compact('model', 'total_found', 'fi    eld', 'keyword', 'field')
        )->render();


        /*-----------------------------------------------
        | Result ...
        */
        return $this->jsonFeedback([
             'message'    => $success_message,
             'feed_class' => " ",
        ]);
    }



    /**
     * @param $code_melli
     *
     * @return string
     */
    public function inquiry($code_melli)
    {
        if (!module('ehdaManage')->cardsController()->checkCodeMelli($code_melli)) {
            return "Not Code Melli!";
        }

        $person = new BehdashtSoap($code_melli);

        dd($person->toArray());
        return '';
    }
}
