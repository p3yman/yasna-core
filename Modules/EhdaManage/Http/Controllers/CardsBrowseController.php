<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\Users\Http\Controllers\UsersBrowseController;

class CardsBrowseController extends UsersBrowseController
{

    /**
     * @param string $request_tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function cardsIndex($request_tab = 'all')
    {
        return $this->index('card-holder', $request_tab);
    }



    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    public function buildPageInfo()
    {
        parent::buildPageInfo();

        $browse_url = "cards/browse";

        $this->page[0][0] = $browse_url;
        $this->page[0][2] = $browse_url;
        $this->page[1][2] = $browse_url . "/$this->request_tab";
        $this->page[1][3] = $browse_url;
    }



    /**
     * Handles the on-demand services,related to users browse
     */
    public function handleServices()
    {
        parent::handleServices();
        $this->replaceCreateButton();
        $this->replaceHeadings();
    }



    /**
     * Replaces Create Button
     */
    private function replaceCreateButton()
    {
        module('users')
             ->service('browse_buttons')
             ->update('create')
             ->condition(user()->as('card-holder')->canCreate())
             ->link("url:manage/cards/create")
        ;
    }



    /**
     * Replaces Browse Headings
     */
    private function replaceHeadings()
    {
        module('users')
             ->service('browse_headings')
             ->update('name')
             ->blade('ehdaManage::cards.browse.row-name')
        ;

        module('users')
             ->service('browse_headings')
             ->add('register')
             ->caption("trans:ehdaManage::ehda.cards.register")
             ->blade('ehdaManage::cards.browse.row-register')
        ;

        module('users')
             ->service('browse_headings')
             ->add('state')
             ->caption("trans:validation.attributes.home_city")
             ->blade('ehdaManage::cards.browse.row-state')
        ;

        module('users')
             ->service('browse_headings')
             ->remove('status')
        ;
    }



    /**
     * Reads service-based row actions, depending on the model, and request_role, to render an indexed array of
     * actions, to be used on the "actions" tiny menu of each browse record.
     *
     * @param $model
     *
     * @return array
     */
    public function renderRowActions($model)
    {
        parent::renderRowActions($model);

        $this->modifyEditActionButton($model);
        $this->modifyDeleteActionButton($model);
        $this->addPrintButton($model);

        return module('users')
             ->service('row_actions')
             ->indexed('icon', 'caption', 'link', 'condition')
             ;
    }



    /**
     * @param $model
     */
    private function modifyDeleteActionButton($model)
    {
        module('users')
             ->service('row_actions')
             ->remove('block')
        ;

        module('users')
             ->service('row_actions')
             ->add('delete-card')
             ->order(99)
             ->icon('ban')
             ->condition($model->canDelete())
             ->trans('ehdaManage::ehda.cards.delete')
             ->link("modal:manage/cards/act/-hashid-/delete")
        ;
    }



    /**
     * @param $model
     */
    private function modifyEditActionButton($model)
    {
        module('users')
             ->service('row_actions')
             ->update('edit')
             ->link("url:manage/cards/edit/$model->hashid")
             ->condition($model->ehdaCanEdit())
        ;
    }



    /**
     * @param $model
     */
    private function addPrintButton($model)
    {
        module('users')
             ->service('row_actions')
             ->add('print')
             ->link("modal:manage/cards/act/$model->hashid/print")
             ->icon('print')
             ->caption('trans:ehdaManage::ehda.printings.send_to')
        ;
    }



    /**
     * Sets Row Refresh Url
     */
    public function setRowRefreshUrl()
    {
        $this->row_refresh_url = "manage/cards/update/-hashid-/$this->request_role";
    }



    /**
     * Sets Search Url
     */
    public function setSearchUrl()
    {
        $this->search_url = url("manage/cards/browse/search-for");
    }
}
