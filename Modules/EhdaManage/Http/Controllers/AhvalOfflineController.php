<?php namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Modules\EhdaManage\Entities\UserPending;
use Modules\Yasna\Services\YasnaController;

/**
 * Class AhvalOfflineController
 * @method UserPending model($id = 0, $with_trashed = 0)
 *
 * @package Modules\EhdaManage\Http\Controllers
 */
class AhvalOfflineController extends YasnaController
{
    protected $base_model = "UserPending";



    /**
     * Called from the DispatchAhvalOfflinePackageSchedule
     */
    public function sendPackage()
    {
        $table   = $this->model()->offlinePendingRecords();
        $package = $this->makePackage($table);
        $sent    = $this->dispatchPackage($package);

        if ($sent) {
            $this->flagDispatchedModels($table);
        }
    }



    /**
     * @param Builder $table
     *
     * @return mixed
     */
    private function makePackage($table)
    {
        //@TODO
        return 'folan';
    }



    /**
     * @param $package
     *
     * @return bool
     */
    private function dispatchPackage($package)
    {
        return true;
    }



    /**
     * @param Builder $table
     *
     * @return mixed
     */
    private function flagDispatchedModels($table)
    {
        $data = $this->model()->dataArrayForMarkAsRequested();

        return $table->update($data);
    }
}
