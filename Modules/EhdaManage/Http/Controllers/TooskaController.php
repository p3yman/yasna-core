<?php namespace Modules\EhdaManage\Http\Controllers;

use Carbon\Carbon;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\EhdaManage\Http\Api\EhdaApiReply;
use Modules\EhdaManage\Http\Requests\UssdCheckUserRequest;
use Modules\EhdaManage\Http\Requests\UssdRegisterRequest;
use Modules\Yasna\Services\YasnaController;

class TooskaController extends YasnaController
{
    private $request;



    /**
     * @param UssdRegisterRequest $request
     * return json
     */
    public function register(UssdRegisterRequest $request)
    {
        $this->request = $request;

        $this->checkForExistingCards();
        $this->checkForUnderProcessRequests();
        $this->addRequestToPendingTable();

        $this->dieWithSuccessRespond(ApiResponse::REQUEST_SUBMIT_SUCCESSFULLY);
    }



    /**
     * Tries to add a record in user_pendings table and throws appropriate response, if fails
     */
    private function addRequestToPendingTable()
    {
        $new_record = model('UserPending')->addRecord([
             "code_melli"   => $this->request->code_melli,
             "birth_date"   => $this->request->birth_date->toDateString(),
             "mobile"       => $this->request->tel_mobile,
             "callback_url" => $this->request->call_back_url,
        ]);

        if ($new_record->not_exists) {
            $this->dieWithErrorRespond(ApiResponse::ERROR_IN_HANDLING_REQUEST);
        }
    }



    /**
     * Looks in existing users and returns appropriate respond, if found
     */
    private function checkForExistingCards()
    {
        $existing_user = user()->finder($this->request->code_melli, 'card-holder');
        if ($existing_user->exists) {
            $this->sendSms($existing_user);
            $this->dieWithErrorRespond(ApiResponse::USER_ALREADY_EXIST);
        }
    }



    /**
     * send feedback to user if he/she registered before
     */
    private function sendSms($user)
    {
        if ($this->checkUserBirthDate($user)) {

            //if birth validate true
            $result = EhdaApiReply::model(null)
                        ->setStatusText($user->card_exists_message)
                        ->setTell($this->request->tel_mobile)
                        ->send()
            ;

        } else {
            EhdaApiReply::model(null)->setStatus('1015')->setTell($this->request->tel_mobile)->send();
        }
    }



    /**
     * @param $user
     *
     * @return bool
     */
    private function checkUserBirthDate($user)
    {
        if (!$user->birth_date) {
            return false;
        }

        return boolval($user->birth_date->toDateString() == $this->request->birth_date->toDateString());

        //$diff = $user->birth_date->diff($this->request->birth_date);
        //if (($diff->d != 0) || ($diff->m != 0) || ($diff->y != 0)) {
        //    return false;
        //}
        //return true;
    }



    /**
     * Checks the existing requests and returns appropriate respond, if found under process.
     */
    private function checkForUnderProcessRequests()
    {
        $existing_record = model("UserPending")->findByCodeMelli($this->request->code_melli);

        if ($existing_record->exists) {
            if ($existing_record->isUnderProcess()) {
                $this->dieWithErrorRespond(ApiResponse::REQUEST_SUCCESS_AND_SEND_TO_AHVAL);
            }
        }
    }



    /**
     * @param UssdCheckUserRequest $request
     * get status of user
     */
    public function inquiry(UssdCheckUserRequest $request)
    {
        $status = model('UserPending')->findByCodeMelli($request->code_melli)->apiStatus();

        if (ApiResponse::REQUEST_SUCCESS_AND_SEND_TO_AHVAL == $status) {
            ApiResponse::setSMSflag(true);
            ApiResponse::setSMSmessage(trans("ehdamanage::tooska-api.user_waiting"));
        } else {
            ApiResponse::setSMSflag(false);
            ApiResponse::setSMSmessage("");
        }

        ApiResponse::getSuccess($status, trans("ehdamanage::tooska-api.$status"));
    }



    /**
     * @param $status
     */
    private function dieWithErrorRespond($status)
    {
        ApiResponse::getError($status, trans("ehdamanage::tooska-api.$status"));
        die();
    }



    /**
     * @param $status
     */
    private function dieWithSuccessRespond($status)
    {
        ApiResponse::getSuccess($status, trans("ehdamanage::tooska-api.$status"));
        die();
    }
}
