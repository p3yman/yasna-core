<?php namespace Modules\EhdaManage\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaManage\Entities\UserPending;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\Yasna\Entities\User;
use Modules\Yasna\Services\YasnaController;

/**
 * Class TooskaRegisterController
 * @method UserPending model($id = 0, $with_trashed = 0)
 *
 * @property UserPending $pending_record
 * @property User        $user_record
 * @package Modules\EhdaManage\Http\Controllers
 */
class TooskaRegisterController extends YasnaController
{
    protected $base_model = "UserPending";

    private $pending_record;
    private $user_record;



    /**
     * Called from a minutely-ran schedule
     *
     * @deprecated
     * @return bool
     */
    public function registerOneApprovedUser()
    {
        $this->loadAPendingRecord();

        return $this->registerProcess();
    }



    /**
     * Called from a listener: RegisterCardWhenUserPendingCompleted
     *
     * @return bool
     */
    public function registerSelectedApprovedUser($pending_record)
    {
        chalk()->add("TooskaRegisterController, started on model #$pending_record->id");

        $this->pending_record = $pending_record;

        return $this->registerProcess();
    }



    /**
     * @return bool
     */
    private function registerProcess()
    {
        $pending_record = $this->pending_record;

        if (!$this->isPendingRecordValid()) {
            chalk()->add("TooskaRegisterController, found invalid model #$pending_record->id");
            return false;
        }

        $this->loadUserRecord();
        chalk()->add("TooskaRegisterController, loaded user record for model #$pending_record->id");

        $this->saveUser();
        chalk()->add("TooskaRegisterController, saved user for model #$pending_record->id");

        $this->deletePendingRecord();
        chalk()->add("TooskaRegisterController, deleted pending record for model #$pending_record->id");

        //$this->sendFeedbackToTooska();
        //chalk()->add("TooskaRegisterController, sent feedback for model #$pending_record->id");


        return true;
    }



    /**
     * Loads a random Pending Record
     */
    private function loadAPendingRecord()
    {
        $this->pending_record = $this->model()->registrableRecords()->first();
    }



    /**
     * Loads User Record
     */
    private function loadUserRecord()
    {
        $this->user_record = user();
        $this->loadExistingUserIfAny();
    }



    /**
     * Looks for Code Melli
     */
    private function loadExistingUserIfAny()
    {
        $this->user_record = user()->finder($this->pending_record->code_melli);
    }



    /**
     * @return bool
     */
    private function saveUser()
    {
        if ($this->user_record->exists) {
            return $this->updateUser();
        }

        return $this->createUser();
    }



    /**
     * @return bool
     */
    private function updateUser()
    {
        $data = $this->getDataFromPendingRecord();
        $this->user_record->batchSave($data);

        if ($this->user_record->is_not_a('card-holder')) {
            $this->user_record->assignRoleAndCardNo();
        }

        return $this->user_record->exists;
    }



    /**
     * @return bool
     */
    private function createUser()
    {
        $data             = $this->getDataFromPendingRecord();
        $data             = $this->injectCreatorIdIntoData($data);
        $data['password'] = $this->makePassword($data);
        $new_user         = user()->batchSave($data);

        $new_user->assignRoleAndCardNo();
        $this->user_record = $new_user;

        return $new_user->exists;
    }



    /**
     * Injects creator name
     */
    private function injectCreatorIdIntoData($data)
    {
        $channel        = $this->pending_record->channel;
        $creators_array = [
             "MTN"    => "825518",
             "tooska" => "825517",
             "3432"   => "825519",
        ];
        
        if (array_has($creators_array, $channel)) {
            $data['created_by'] = $creators_array[$channel];
        }

        return $data;
    }



    /**
     * @return bool
     */
    private function isPendingRecordValid()
    {
        return ($this->pending_record and $this->pending_record->exists);
    }



    /**
     * @return array
     */
    private function getDataFromPendingRecord()
    {
        $record = $this->pending_record;
        $record->spreadMeta();

        return [
             "code_melli"        => ed($record->code_melli),
             "name_first"        => pd($record->name_first),
             "name_last"         => pd($record->name_last),
             "name_father"       => pd($record->name_father),
             'mobile'            => $this->mobilePurifier(ed($record->mobile)),
             'birth_date'        => ed($record->birth_date),
             'gender'            => ed($record->gender),
             "ahval_verified_at" => $this->now(),
        ];
    }



    /**
     * Deletes and sets appropriate flag
     */
    private function deletePendingRecord()
    {
        $this->pending_record->refresh()->markAsRegistered();
    }



    /**
     * @param $data
     *
     * @return string
     */
    public function makePassword($data)
    {
        return Hash::make($data['mobile']);
    }



    /**
     * @param $mobile
     *
     * @return string
     */
    private function mobilePurifier($mobile)
    {
        if (starts_with($mobile, '989')) {
            $mobile = '0' . substr($mobile, strlen('98'));
        }

        return $mobile;
    }



    /**
     * Sends Feedback to Tooska
     */
    private function sendFeedbackToTooska()
    {
        $user       = $this->user_record;
        $pending    = $this->pending_record;
        $client     = new Client();
        $card_image = $user->cards('single', 'show');
        $url        = $pending->spreadMeta()->callback_url;

        $client->post($url, [
             'form_params' => [
                  'status'   => ApiResponse::USER_CARD_REGISTERED,
                  'msg'      => trans('ehdamanage::tooska-api.user_submit_msg'),
                  'sms_send' => '1',
                  'sms_body' => trans('ehdamanage:tooska-api.card_registered') . "\n" . $card_image,
             ],
        ]);
    }
}
