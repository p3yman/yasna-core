<?php

namespace Modules\EhdaManage\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Yasna\Services\YasnaController;
use Morilog\Jalali\Facades\jDate;
use Morilog\Jalali\jDateTime;

class EhdaManageController extends YasnaController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('ehdamanage::index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('ehdamanage::create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
    }



    /**
     * Show the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('ehdamanage::show');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('ehdamanage::edit');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy()
    {
    }
}
