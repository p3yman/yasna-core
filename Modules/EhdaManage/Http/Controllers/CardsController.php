<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class CardsController extends YasnaController
{


    /**
     * Sidebar sub-menu, called from the blade.
     *
     * @return array
     */
    public function sidebarSubMenu()
    {
        if (user()->is_not_a('manager')) {
            $this->removeAllUsersFromSidebar();
        }

        return [
             [
                  "link"    => "cards/create",
                  "caption" => trans("ehdaManage::ehda.cards.create"),
                  "permit"  => user()->as('admin')->can('users-card-holder.create'),
             ],
             [
                  "link"    => "cards/browse",
                  "caption" => trans('ehdaManage::ehda.cards.browse'),
                  "permit"  => user()->as('admin')->can('users-card-holder.browse'),
             ],
             [
                  "link"    => "cards/printings",
                  "caption" => trans("ehdaManage::ehda.printings.title"),
                  "permit"  => user()->as('admin')->can('users-card-holder.print'),
             ],
        ];
    }



    /**
     * Removes all-user sidebar menu, if the online admin is not a developer.
     */
    private function removeAllUsersFromSidebar()
    {
        module('manage')
             ->service('sidebar')
             ->remove('users')
        ;
    }



    /**
     * Removes create buttons on top of yasna users page, so that everybody is forced to use ehda things
     * Called from service environment, set by ProviderCardsTrait::removeNormalUserCreationButtons
     */
    public static function removeNormalUserCreationButtons()
    {
        service('users:browse_buttons')
             ->update('create')
             ->condition(false)
        ;
    }



    /**
     * @return array
     */
    public function genderCombo()
    {
        return [
             ['2', trans_safe("users::gender.female")],
             ['1', trans_safe("users::gender.male")],
             ['3', trans_safe("users::gender.other")],
        ];
    }



    /**
     * @return array
     */
    public function maritalCombo()
    {
        return [
             ['2', trans_safe("users::marital.single")],
             ['1', trans_safe("users::marital.married")],
        ];
    }



    /**
     * @return array
     */
    public function eduCombo()
    {
        $array = [];

        for ($i = 1; $i <= 6; $i++) {
            $array[] = [strval($i), trans_safe("users::edu.full.$i")];
        }

        return $array;
    }



    /**
     * @param $code_melli
     *
     * @return bool
     */
    public function checkCodeMelli($code_melli)
    {
        if (strlen($code_melli) != 10 or !is_numeric($code_melli)) {
            return false;
        }

        $code = str_split($code_melli);
        $err  = 0;

        foreach ($code as $k => $v) {
            if ($code[0] <> $v) {
                $err = 1;
                break;
            } else {
                $err = 2;
            }
        }
        if ($err == 1) {
            $valid  = 0;
            $jumper = 10;

            for ($i = 0; $i <= 8; $i++) {
                $valid += $code[$i] * $jumper;
                --$jumper;
            }

            $valid = $valid % 11;

            if ($valid >= 0 and $valid < 2) {
                if ($valid == $code[9]) {
                    return true;
                } else {
                    return false;
                }
            } else {
                $valid = 11 - $valid;
                if ($valid == $code[9]) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }

        // taha old code
        //if (!preg_match("/^\d{10}$/", $value)) {
        //    return false;
        //}
        //if ($value=='0000000000') {
        //    return false;
        //}
        //
        //$check = (int)$value[9];
        //$sum   = array_sum(array_map(function ($x) use ($value) {
        //    return ((int)$value[ $x ]) * (10 - $x);
        //}, range(0, 8))) % 11;
        //
        //return ($sum < 2 && $check == $sum) || ($sum >= 2 && $check + $sum == 11);
    }



    /**
     * Puts last used event id into the session
     *
     * @param int $id
     */
    public function rememberLastUsedEvent($id)
    {
        session()->put('user_last_used_event', $id);
    }



    /**
     * Returns last used event id from the session
     *
     * @return int
     */
    public function getLastUsedEvent()
    {
        return session()->get('user_last_used_event', 0);
    }
}
