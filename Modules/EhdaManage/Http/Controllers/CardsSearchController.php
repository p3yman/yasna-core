<?php namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Http\Request;

class CardsSearchController extends CardsBrowseController
{
    protected $search_request;
    protected $request_tab     = 'search';
    protected $showing_results = false;
    protected $request_role    = 'card-holder';



    /**
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function panel()
    {
        if (!$this->checkPermission()) {
            return $this->abort(403);
        }
        if (!$this->getRoleModel()) {
            return $this->abort(404);
        }

        $this->buildPageInfo();
        $this->setSearchUrl();
        $this->buildViewArguments();

        return $this->view('search', $this->view_arguments);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request)
    {
        $this->search_request  = $request;
        $this->showing_results = true;

        if (!$this->checkPermission()) {
            return $this->abort(403);
        }
        if (!$this->getRoleModel()) {
            return $this->abort(404);
        }

        $this->buildPageInfo();
        $this->setSearchUrl();
        $this->setRowRefreshUrl();
        $this->queryBuilder();
        $this->buildViewArguments();
        $this->handleServices();

        return $this->view('index', $this->view_arguments);
    }



    /**
     * Query Builder
     */
    public function queryBuilder()
    {
        $elector_switches = [
             "roleString" => $this->request_role,
             "search"     => $this->search_request->keyword,
        ];

        $this->models = model('User')
             ->elector($elector_switches)
             ->orderBy('created_at', 'desc')
             ->simplePaginate(20)
        ;
    }



    /**
     * Builds View Arguments
     */
    public function buildViewArguments()
    {
        parent::buildViewArguments();

        if ($this->showing_results) {
            $this->view_arguments['keyword'] = $this->search_request->keyword;
        }
    }



    /**
     * Build Page Info
     */
    public function buildPageInfo()
    {
        parent::buildPageInfo();

        if ($this->showing_results) {
            $this->page[1] = [
                 $this->request_tab,
                 trans('manage::forms.button.search_for') . SPACE . $this->search_request->keyword,
                 $this->page[1][2],//"users/browse/$this->request_role",
                 $this->page[0][0] . "/$this->request_tab",
            ];
        }
    }
}
