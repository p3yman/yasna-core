<?php

namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ReportAjaxController extends Controller
{
    /**
     * @param Request $request
     *
     * @return string (json result)
     *                todo set json response
     */
    public function getProvince(Request $request)
    {
        $cityId = $request->get('city_id');
        $data   = model('state')::find($cityId)->province()->toArray();
        return json_encode($data);
    }



    /**
     * @param Request $request
     *
     * @return string (json result)
     *                 todo set json response
     */
    public function getCity(Request $request)
    {
        $province_id = $request->get('province_id');
        $data        = $this->findCities($province_id);
        //make empty option
        $data[] = [
             "id"    => "",
             "title" => trans('ehdamanage::report.combo_blank_value'),
        ];
        return json_encode(array_reverse($data));
    }



    /**
     * @param $province_id
     *
     * @return mixed
     */
    private function findCities($province_id)
    {
        if (is_null($province_id)) {
            $data = model('state')::where('parent_id', '<>', '0')->orderBy('title', 'desc')->get(['id', 'title'])->toArray();
        } else {
            $data = model('state')::find($province_id)->cities()->orderBy('title', 'desc')->get(['id', 'title'])->toArray();
        }
        return $data;
    }
}
