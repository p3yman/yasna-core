<?php

namespace Modules\EhdaManage\Http\Controllers\Report;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Modules\EhdaManage\Http\Requests\FilterRequest;
use Modules\EhdaManage\Http\Requests\Report\CardsReportRequest;
use Modules\Users\Entities\Traits\UserElectorTrait;
use Modules\Yasna\Services\YasnaController;
use Morilog\Jalali\Facades\jDate;

class CardReportController extends YasnaController
{
    /**
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->moduleView('report.card-specific-date.main', [
             'register_method_options' => $this->cardReportRegisterMethodOptions(),
        ]);
    }



    /**
     * @param string $blade
     * @param array  ...$paramters
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function moduleView($blade, ...$paramters)
    {
        return $this->view($this->moduleAlias() . '::' . $blade, ...$paramters);
    }



    /**
     * Generate options to be used in the selector of `register-method`.
     *
     * @return array
     */
    protected function cardReportRegisterMethodOptions()
    {
        $static_options  = [
             [
                  'id'    => 'web',
                  'title' => trans('ehdamanage::ehda.cards.web'),
             ],
        ];
        $dynamic_options = user()
             ->elector(['role' => 'api'])
             ->get()
             ->map(function ($user) {
                 return [
                      'id'    => $user->hashid,
                      'title' => $user->full_name,
                 ];
             })->toArray()
        ;

        return array_merge($static_options, $dynamic_options);
    }



    /**
     * @param CardsReportRequest $request
     *
     * @return string
     */
    public function submit(CardsReportRequest $request)
    {
        $main_register_builder = $this->usersBuilder($request);
        $main_prints_builder   = model('printing')->select();
        $days                  = [];
        $periods               = $this->generatePeriods($request);
        $all_cards             = 0;
        $all_prints            = 0;

        foreach ($periods as $period) {
            $cards = (clone $main_register_builder)
                 ->where('card_registered_at', '>=', $period['from'])
                 ->where('card_registered_at', '<=', $period['to'])
                 ->count()
            ;

            $prints = (clone $main_prints_builder)
                 ->where('printed_at', '>=', $period['from'])
                 ->where('printed_at', '<=', $period['to'])
                 ->count()
            ;

            $all_cards  += $cards;
            $all_prints += $prints;

            $days[] = [
                 'date'   => $period['label'],
                 'cards'  => ad(number_format($cards)),
                 'prints' => ad(number_format($prints)),
            ];
        }

        $days[] = [
             'date'   => trans('ehdamanage::report.sum'),
             'cards'  => ad(number_format($all_cards)),
             'prints' => ad(number_format($all_prints)),
        ];

        return $this->jsonSaveFeedback(1, [
             'success_message'      => '',
             'success_callback'     => "showLog(" . json_encode($days) . ")",
             'success_feed_timeout' => 1,
        ]);
    }



    /**
     * Generate builder for selecting users.
     *
     * @param Request $request
     *
     * @return Builder
     */
    protected function usersBuilder(Request $request)
    {
        $builder         = user()->select();
        $register_method = $request->register_method;

        if ($register_method) {
            if ($register_method == 'web') {
                $builder->where('created_by', '0');
            } else {
                $api_id = hashid($register_method);
                $builder->where('created_by', $api_id);
            }
        }

        return $builder;
    }



    /**
     * Generate periods array based on report type.
     *
     * @param Request $request
     *
     * @return array
     */
    protected function generatePeriods(Request $request)
    {
        $report_type = $request->report_type;
        $date        = Carbon::parse($request->date);

        switch ($report_type) {
            case 'hourly':
                return $this->generateHourlyPeriods($date);
                break;

            case 'daily':
            default:
                return $this->generateDailyPeriods($date);
                break;
        }
    }



    /**
     * Generate periods array if report type is daily.
     *
     * @param Carbon $date
     *
     * @return array
     */
    protected function generateDailyPeriods(Carbon $date)
    {
        $start   = (clone $date)->subDays(31);
        $end     = clone $date;
        $current = clone $start;
        $periods = [];

        while ($current->lte($end)) {
            $periods[] = [
                 'label' => ad(echoDate($current, 'Y/m/d')),
                 'from'  => (clone $current)->startOfDay()->toDateTimeString(),
                 'to'    => (clone $current)->endOfDay()->toDateTimeString(),
            ];

            $current->addDay();
        }

        return array_reverse($periods);
    }



    /**
     * Generate periods array if report type is hourly.
     *
     * @param Carbon $date
     *
     * @return array
     */
    protected function generateHourlyPeriods(Carbon $date)
    {
        $now     = Carbon::now();
        $start   = (clone $date)->startOfDay();
        $end     = (clone $date)->endOfDay();
        $current = clone $start;
        $periods = [];

        if ($end->gt($now)) {
            $end = clone $now;
        }

        while ($current->lte($end)) {
            $from = (clone $current);
            $to   = (clone $current)->addHour();

            if ($to->gt($end)) {
                $to = clone $end;
            }

            $periods[] = [
                 'label' => trans('ehdamanage::report.from-to', [
                      'from' => ad($from->format('H:i')),
                      'to'   => ad($to->format('H:i')),
                 ]),
                 'from'  => $from->toDateTimeString(),
                 'to'    => $to->toDateTimeString(),
            ];

            $current->addHour();
        }

        return array_reverse($periods);
    }
}
