<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\Users\Http\Controllers\UsersBrowseController;

class VolunteersBrowseController extends UsersBrowseController
{
    /**
     * @param string $request_role
     * @param string $request_tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($request_role = 'admin', $request_tab = 'all')
    {
        return parent::index($request_role, $request_tab);
    }



    /**
     * Handles the on-demand services,related to users browse
     */
    public function handleServices()
    {
        parent::handleServices();
        $this->replaceCreateButton();
        $this->replaceHeadings();
    }



    /**
     * Replaces Browse Headings
     */
    private function replaceHeadings()
    {
        module('users')
             ->service('browse_headings')
             ->update('name')
             ->blade('ehdaManage::volunteers.browse.row-name')
        ;

        module('users')
             ->service('browse_headings')
             ->add('occupation')
             ->trans('validation.attributes.occupation')
             ->blade('ehdaManage::volunteers.browse.row-occupation')
        ;

        //dd(service("users:browse_headings")->read());
    }



    /**
     * Replaces Create Button
     */
    private function replaceCreateButton()
    {
        module('users')
             ->service('browse_buttons')
             ->update('create')
             ->condition(user()->as('admin')->can('volunteers.create'))
             ->caption(trans("ehdaManage::ehda.volunteers.create"))
             ->link("url:manage/volunteers/create")
             ->set('type', 'primary')
        ;
    }



    /**
     * Reads service-based row actions, depending on the model, and request_role, to render an indexed array of
     * actions, to be used on the "actions" tiny menu of each browse record.
     *
     * @param $model
     *
     * @return array
     */
    public function renderRowActions($model)
    {
        parent::renderRowActions($model);

        $this->modifyEditActionButton($model);

        return module('users')
             ->service('row_actions')
             ->indexed('icon', 'caption', 'link', 'condition')
             ;
    }



    /**
     * @param $model
     */
    private function modifyEditActionButton($model)
    {
        module('users')
             ->service('row_actions')
             ->update('edit')
             ->link("url:manage/volunteers/edit/$model->hashid")
             ->condition($model->ehdaCanEdit())
        ;
    }



    /**
     * Sets Search Url
     */
    public function setSearchUrl()
    {
        $this->search_url = url("manage/volunteers/browse/$this->request_role/search-for");
    }



    /**
     * Sets Row Refresh Url
     */
    public function setRowRefreshUrl()
    {
        $this->row_refresh_url = "manage/volunteers/update/-hashid-/$this->request_role";
    }

    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    public function buildPageInfo()
    {
        parent::buildPageInfo();

        $browse_url = "volunteers/browse/$this->request_role";

        $this->page[0][0] = $browse_url;
        $this->page[0][2] = $browse_url;
        $this->page[0][1] = trans_safe("ehdaManage::ehda.volunteers.plural");
        $this->page[1][2] = $browse_url . "/$this->request_tab";
        $this->page[1][3] = $browse_url;
    }
}
