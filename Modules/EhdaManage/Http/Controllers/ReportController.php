<?php

namespace Modules\EhdaManage\Http\Controllers;

use Carbon\Carbon;
use function GuzzleHttp\Psr7\normalize_header;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\View\View;
use Maatwebsite\Excel\Facades\Excel;
use Mockery\Exception;
use Modules\EhdaManage\Http\Requests\FilterRequest;
use Modules\Users\Entities\Traits\UserElectorTrait;

//todo remove all filters method from class and move they to elector

class ReportController extends Controller
{
    use UserElectorTrait;
    private $filter_query;
    private $elector_array = [];
    private $excelTitles   = [];
    private $excelRow      = [];
    private $excel_col     = [];
    private $volunteer_access;
    private $card_access;
    private $limit         = 50;
    private $excel_limit   = 10000;
    private $request;
    const CARD_HOLDER = 'card-holder';



    /**
     * @param FilterRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(FilterRequest $request)
    {
        $view_data['search']          = (empty($request->all())) ? false : true;
        $view_data['request']         = $request;
        $view_data['status_list']     = $this->getStatusItem();
        $this->volunteer_access       = user()->as('admin')->canAny(model('role')->adminRoles('.report'));
        $this->card_access            = user()->as('admin')->can('card-holder.report');
        $view_data['volunteerAccess'] = $this->volunteer_access;
        $view_data['cardAccess']      = $this->card_access;
        $this->request                = $request;
        $card_flag                    = $request->get('user-by-card');
        $volunteer_flag               = $request->get('ambassador-user');

        if ($view_data['search'] == false) {
            return view('ehdamanage::layouts.report', $view_data);
        }

        $this->filter_query = model('user');

        //run elector queries
        $this->ageFilter($request->get('age-min'), $request->get('age-max'));


        $this->chooseRolesLogicForElector($volunteer_flag, $card_flag);

        $this->homeCityFilter($request->get('home-province'), $request->get('home-city'));

        $this->workCityFilter($request->get('work-province'), $request->get('work-city'));

        $this->genderFilter($request->get('gender'));

        $this->jobFilter($request->get('job'));

        $this->eduFilter($request->get('edu'));

        $this->codeMelliFilter($request->get('code-meli'));
        $this->mobileFilter($request->get('mobile'));
        $this->runElectors($this->elector_array);
        //end electors

        $this->nameFilter($request->get('name'));
        $this->familyFilter($request->get('family'));
        $this->fatherFilter($request->get('father-name'));
        $this->homeTelFilter($request->get('home-tel'));
        $this->postalFilter($request->get('code-post'));
        $this->workTelFilter($request->get('work-tel'));

        $this->userByCardDateFilter($card_flag, $request->user_by_card_register,
             $request->to_user_by_card_register);

        //this is for when user only has volunteer access and he/she must see volunteer user
        if ($this->volunteer_access && !$this->card_access) {
            $volunteer_flag = 1;
        }

        $this->ambassadorUserFilter($volunteer_flag, [
             'user-register-from'  => $request->ambassador_user_register,
             'user-register-since' => $request->to_ambassador_user_register,
             'activity-slug'       => $request->activities,
             'state'               => $request->get('state'),
        ]);


        if ($request->has('ajax')) {
            return $this->filter_query;
        }

        $view_data['models']   = $this->filter_query->simplePaginate($this->limit)->appends($request->all());
        $view_data['resCount'] = $this->filter_query->count();
        $view_data['page']     = $this->setPage();
        $view_data['offset']   = (($request->page ?? 1) - 1) * $this->limit;

        return view('ehdamanage::layouts.report', $view_data);
    }



    /**
     * @param FilterRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function excelModal(FilterRequest $request)
    {
        $view_data['request'] = $request;
        return \view('ehdamanage::layouts.report_sections.excel_modal', $view_data);
    }



    /**
     * @param FilterRequest $request
     *
     * @return string
     */
    public function excel(FilterRequest $request)
    {
        $builder = $this->filter($request);

        Excel::create("Report-" . Carbon::now(), function ($excel) use ($builder, $request) {
            $excel->sheet('Sheetname', function ($sheet) use ($builder, $request) {
                $this->setExcelTitle($request->get('name_check'), trans('ehdamanage::report.name'), 'name_first');
                $this->setExcelTitle($request->get('family_check'), trans('ehdamanage::report.family'), 'name_last');
                $this->setExcelTitle($request->get('mobile_check'), trans('ehdamanage::report.mobile'), 'mobile');
                $this->setExcelTitle($request->get('email_check'), trans('ehdamanage::report.email'), 'email');
                $this->setExcelTitle($request->get('birth_day_check'), trans('ehdamanage::report.birth_day'),
                     'birth_date');
                $this->setExcelTitle($request->get('national_code_check'), trans('ehdamanage::report.national_code'),
                     'code_melli');
                $this->setExcelTitle($request->get('father_name_check'), trans('ehdamanage::report.father_name'),
                     'name_father');
                $this->setExcelTitle($request->get('code_id_check'), trans('ehdamanage::report.code_id'), 'code_id');
                $this->setExcelTitle($request->get('card_register_date_check'),
                     trans('ehdamanage::report.card_register_date'), 'card_registered_at');
                $this->setExcelTitle($request->get('home_address_check'), trans('ehdamanage::report.home_address'),
                     'home_address');
                $this->setExcelTitle($request->get('home_province_check'), trans('ehdamanage::report.home_province'),
                     'home_province');
                $this->setExcelTitle($request->get('home_city_check'), trans('ehdamanage::report.home_city'),
                     'home_city');
                $this->setExcelTitle($request->get('home_tele_check'), trans('ehdamanage::report.home_tele'),
                     'home_tel');
                $this->setExcelTitle($request->get('edu_check'), trans('ehdamanage::report.edu'), 'edu_level');
                $this->setExcelTitle($request->get('marital_check'), trans('ehdamanage::report.marital'), 'marital');
                $this->setExcelTitle($request->get('register_date_check'), trans('ehdamanage::report.register_date'),
                     'created_at');
                $this->setExcelTitle($request->get('job_check'), trans('ehdamanage::report.job'), 'job');
                $this->setExcelTitle($request->get('motivation_check'), trans('ehdamanage::report.motivation'),
                     'motivation');
                $this->setExcelTitle($request->get('alloc_time_check'), trans('ehdamanage::report.alloc_time'),
                     'alloc_time');
                $this->setExcelTitle($request->get('edu_field_check'), trans('ehdamanage::report.edu_field'),
                     'edu_field');
                $this->setExcelTitle($request->get('card_no_check'), trans('ehdamanage::report.card_no'), 'card_no');
                $this->setExcelTitle($request->get('postal_check'), trans('ehdamanage::report.code_post'),
                     'home_postal');

                $sheet->appendRow($this->excelTitles);

                $dataCollect = $builder->limit($this->excel_limit)->get($this->excel_col);

                foreach ($dataCollect as $item) {
                    $this->setExcelRow($request->get('name_check'), $item->name_first, 1);

                    $this->setExcelRow($request->get('family_check'), $item->name_last, 2);
                    $this->setExcelRow($request->get('mobile_check'), $item->mobile, 3);
                    $this->setExcelRow($request->get('email_check'), $item->email, 4);
                    try {
                        $this->setExcelRow($request->get('birth_day_check'), echoDate($item->birth_date, "y/m/d"), 5);
                    } catch (Exception $exception) {
                        $this->setExcelRow($request->get('birth_day_check'), "", 5);
                    }
                    $this->setExcelRow($request->get('birth_day_check'), echoDate($item->birth_date, "y/m/d"), 5);
                    $this->setExcelRow($request->get('national_code_check'), $item->code_melli, 6);
                    $this->setExcelRow($request->get('father_name_check'), $item->name_father, 7);
                    $this->setExcelRow($request->get('code_id_check'), $item->code_id, 8);
                    $this->setExcelRow($request->get('card_register_date_check'),
                         echoDate($item->card_registered_at, "y/m/d"), 9);

                    $this->setExcelRow($request->get('home_address_check'), $item->home_address, 10);
                    $this->setExcelRow($request->get('home_province_check'),
                         model('state', $item->home_province)->title, 11);
                    $this->setExcelRow($request->get('home_city_check'), model('state', $item->home_city)->title, 12);
                    $this->setExcelRow($request->get('home_tele_check'), $item->home_tel, 13);
                    $this->setExcelRow($request->get('edu_check'),
                         trans("ehdafront::people.edu_level_full.$item->edu_level"),
                         14);
                    $this->setExcelRow($request->get('marital_check'),
                         trans("ehdafront::people.marital.$item->marital"), 15);
                    try {
                        $this->setExcelRow($request->get('register_date_check'), echoDate($item->created_at, "y/m/d"),
                             16);
                    } catch (Exception $exception) {
                        $this->setExcelRow($request->get('register_date_check'), "", 16);
                    }
                    $this->setExcelRow($request->get('job_check'), $item->job, 17);
                    $this->setExcelRow($request->get('motivation_check'), $item->motivation, 18);
                    $this->setExcelRow($request->get('alloc_time_check'), $item->alloc_time, 19);
                    $this->setExcelRow($request->get('edu_field_check'), $item->edu_field, 20);
                    $this->setExcelRow($request->get('card_no_check'), $item->card_no, 21);
                    $this->setExcelRow($request->get('postal_check'), $item->home_postal, 22);
                    $sheet->appendRow($this->excelRow);
                }
            });
        })->export('xls')
        ;
    }



    /**
     * @param $flag
     * @param $title
     * @param $db_col_name
     */
    private function setExcelTitle($flag, $title, $db_col_name)
    {
        if ($flag != "1") {
            return;
        }
        $this->excel_col[]   = $db_col_name;
        $this->excelTitles[] = $title;
    }



    /**
     * @param $flag
     * @param $data
     * @param $index
     * set column of excel
     */
    private function setExcelRow($flag, $data, $index)
    {
        if ($flag != "1") {
            return;
        }
        $this->excelRow[$index] = $data;
    }



    /**
     * it maps statues of manager role
     *
     * @return array
     */
    private function getStatusItem()
    {
        $items = model('role', 'manager')->status_rule_array;
        $list  = [];
        $i     = 0;
        foreach ($items as $key => $value) {
            $list[$i]['title'] = trans('ehdamanage::people.criteria.' . $value);
            $list[$i]['value'] = $key;
            $i++;
        }
        return $list;
    }



    /**
     * @return array
     */
    private function setPage()
    {
        return [
             ["ehda/report", trans('ehdamanage::report.report')],
        ];
    }



    /**
     * @param $name
     */
    private function nameFilter($name)
    {
        if (!empty($name)) {
            $this->filter_query = $this->filter_query->where('name_first', 'like', '%' . $name . '%');
        }
    }



    /**
     * @param $name
     */
    private function familyFilter($name)
    {
        if (!empty($name)) {
            $this->filter_query = $this->filter_query->where('name_last', 'like', '%' . $name . '%');
        }
    }



    /**
     * @param $name
     */
    private function fatherFilter($name)
    {
        if (!empty($name)) {
            $this->filter_query = $this->filter_query->where('name_father', 'like', '%' . $name . '%');
        }
    }



    /**
     * @param $number
     */
    private function homeTelFilter($number)
    {
        if (!empty($number)) {
            $this->filter_query = $this->filter_query->where('home_tel', $number);
        }
    }



    /**
     * @param $code
     */
    private function postalFilter($code)
    {
        if (!empty($code)) {
            $this->filter_query = $this->filter_query->where('home_postal', $code);
        }
    }



    /**
     * @param $number
     */
    private function workTelFilter($number)
    {
        if (!empty($number)) {
            $this->filter_query = $this->filter_query->where('work_tel', $number);
        }
    }



    /**
     * @param $code
     */
    private function codeMelliFilter($code)
    {
        $this->elector_array['code-melli'] = $code;
    }



    /**
     * @param $number
     */
    private function mobileFilter($number)
    {
        $this->elector_array['mobile'] = $number;
    }



    /**
     * @param $ageMin
     * @param $ageMax
     */
    private function ageFilter($ageMin, $ageMax)
    {
        $this->elector_array['age-min'] = $ageMin;
        $this->elector_array['age-max'] = $ageMax;
    }



    /**
     * @param $province
     * @param $city
     */
    private function homeCityFilter($province, $city)
    {
        $this->elector_array['home-province'] = $province;
        $this->elector_array['home-city']     = $city;
    }



    /**
     * @param $province
     * @param $city
     */
    private function workCityFilter($province, $city)
    {
        $this->elector_array['work-province'] = $province;
        $this->elector_array['work-city']     = $city;
    }



    /**
     * @param $gender
     */
    private function genderFilter($gender)
    {
        $this->elector_array['gender'] = $gender;
    }



    /**
     * @param $job
     */
    private function jobFilter($job)
    {
        $this->elector_array['job'] = $job;
    }



    /**
     * @param $edu
     */
    private function eduFilter($edu)
    {
        if ($edu == 0) {
            return;
        }
        $this->elector_array['edu-level'] = $edu;
    }



    /**
     * @param $user_by_card_flag
     * @param $from_date
     * @param $to_date '
     */
    private function userByCardDateFilter($user_by_card_flag, $from_date, $to_date)
    {
        if (!$this->card_access) {
            return;
        }
        if ($user_by_card_flag == '1' || !$this->volunteer_access) {
            //$this->filter_query = $this->filter_query->where('card_registered_at', '<>', '');
            $this->dateFilter($from_date, $to_date, 'card_registered_at');
        }
    }



    /**
     * @param       $user_flag
     * @param array $data_array
     */
    private function ambassadorUserFilter($user_flag, array $data_array)
    {
        if (!$this->volunteer_access) {
            return;
        }

        if ($user_flag == '1' || !$this->card_access) {
            //$this->filter_query = $this->filter_query->where('volunteer_registered_at', '<>', '');
            $this->dateFilter($data_array['user-register-from'], $data_array['user-register-since'],
                 "volunteer_registered_at");
            $this->activityFilter($data_array['activity-slug']);
        }
    }



    /**
     * if two check box are enable but was not checked
     * if the condition does not checked when above situation is true, all users will be shown
     *
     * @param $volunteer_flag
     * @param $card_flag
     */
    private function chooseRolesLogicForElector($volunteer_flag, $card_flag)
    {
        if ($this->isUnSelectChecks()) {
            $this->statusFilter($volunteer_flag, $this->request->get('state'));
        } elseif ($volunteer_flag and $card_flag) {
            $this->statusFilter($volunteer_flag, $this->request->get('state'));
            $this->changeOrLogicToAnd($this->elector_array['role']);
        } else {
            $this->statusFilter($volunteer_flag, $this->request->get('state'));
            $this->addCardRoleElector($card_flag);
        }
    }



    /**
     * @return bool
     */
    private function isUnSelectChecks()
    {
        if ($this->request->get('ambassador-user') != '1' and $this->request->get('user-by-card') != '1') {
            if ($this->volunteer_access and $this->card_access) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }



    /**
     * todo : make this elector
     *
     * @param $from
     * @param $to
     * @param $field
     */
    private function dateFilter($from, $to, $field)
    {
        if (!empty($from)) {
            $from = Carbon::parse($from)->startOfDay()->toDateTimeString();
            $this->filter_query->where($field, '>=', $from);
        }
        if (!empty($to)) {
            $to = Carbon::parse($to)->endOfDay()->toDateTimeString();
            $this->filter_query->where($field, '<=', $to);
        }
    }



    /**
     * @param $slugs
     */
    private function activityFilter($slugs)
    {
        if (empty($slugs)) {
            return;
        }

        foreach ($slugs as $slug) {
            $this->filter_query = $this->filter_query->where('activities', 'like', '%' . $slug . '%');
        }
    }



    private function addCardRoleElector($card_flag)
    {
        if (!$card_flag) {
            return;
        }
        $this->elector_array['role']   = self::CARD_HOLDER;
        $this->elector_array['status'] = "all";
    }



    /**
     * @param      $volunteer_flag
     * @param      $status
     * @param bool $apply_complete_roles
     */
    private function statusFilter($volunteer_flag, $status, $apply_complete_roles = false)
    {
        if (!$volunteer_flag) {
            return;
        }
        $this->elector_array['role']   = ($apply_complete_roles) ? $this->findAvailableRoleForSearch(true) : $this->findAvailableRoleForSearch($this->isUnSelectChecks());
        $this->elector_array['status'] = $status;
    }



    /**
     * @param bool $with_cards
     *
     * @return array
     */
    private function findAvailableRoleForSearch($with_cards = false)
    {
        $roles = user()->userRolesArray('report');
        //remove card holder role from array
        if (!$with_cards) {
            if (($key = array_search(self::CARD_HOLDER, $roles)) !== false) {
                unset($roles[$key]);
            }
        }
        unset($roles['card-holder']);
        return $roles;
    }



    /**
     * @param $roles
     */
    private function changeOrLogicToAnd($roles)
    {
        $this->elector_array['all-users-with-card-holders'] = $roles;
        unset($this->elector_array['role']);
    }



    /**
     * @param $array
     */
    public function runElectors($array)
    {
        $this->filter_query = $this->filter_query->elector($array);
    }
}
