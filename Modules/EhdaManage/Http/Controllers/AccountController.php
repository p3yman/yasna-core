<?php namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Http\Request;
use Modules\EhdaManage\Events\NewCardRegistered;
use Modules\Yasna\Services\YasnaController;

class AccountController extends YasnaController
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public function register(Request $request)
    {
        user()->assignRoleAndCardNo();
        event(new NewCardRegistered(user()));

        return $this->jsonAjaxSaveFeedback(true, [
             'success_refresh' => true,
        ]);
    }
}
