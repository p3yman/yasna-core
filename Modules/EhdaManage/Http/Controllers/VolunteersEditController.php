<?php namespace Modules\EhdaManage\Http\Controllers;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Modules\EhdaManage\Http\Requests\VolunteerAddRoleRequest;
use Modules\EhdaManage\Http\Requests\CodeMelliInquiryRequest;
use Modules\EhdaManage\Http\Requests\VolunteerSaveRequest;
use Symfony\Component\HttpFoundation\Response;

class VolunteersEditController extends CardsEditController
{
    protected $base_model   = "User";
    protected $view_folder  = "ehdamanage::volunteers.editor";
    protected $request_role = "admin";
    protected $roles;
    protected $role_model;
    protected $default_role;



    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    protected function buildPageInfo()
    {
        $this->page[0] = ['volunteers/browse', trans_safe("ehdaManage::ehda.volunteers.plural")];
        $this->page[1] = ["volunteers/$this->editor_mode", trans("ehdaManage::ehda.volunteers.$this->editor_mode")];
    }



    /**
     * Events are not required herein.
     */
    public function loadEvents()
    {
        //
    }



    /**
     * Load available roles according to the current admin privileges
     */
    protected function loadAvailableRoles()
    {
        $exceptions       = array_add($this->model->withDisabled()->rolesArray(), 'n', 'card-holder');
        $role_slugs       = user()->userRolesArray('create', $exceptions);
        $this->roles      = model('role')->where('is_admin', 1)->whereIn('slug', $role_slugs)->orderBy('title')->get();
        $this->role_model = $this->roles->first();

        $this->guessDefaultRole();
    }



    /**
     * Guess Default Role
     */
    protected function guessDefaultRole()
    {
        if ($this->request_role and $this->request_role != 'admin') {
            $this->default_role = $this->request_role;
            return;
        }

        if ($this->roles->count() == 1) {
            $this->default_role = $this->role_model->slug;
            return;
        }
    }



    /**
     * @param CodeMelliInquiryRequest $request
     *
     * @return string
     */
    public function inquiry(CodeMelliInquiryRequest $request)
    {
        $user = user()->finder($request->code_melli);

        if (!$user->exists) {
            return $this->inquiryResponseWhenNotFound();
        }

        if ($user->withDisabled()->is_admin()) {
            return $this->inquiryResponseWhenAlreadyVolunteer($user);
        }

        return $this->inquiryResponseWhenExistsButNotVolunteer($user);
    }



    /**
     * @param $user
     *
     * @return string
     */
    private function inquiryResponseWhenAlreadyVolunteer($user)
    {
        return $this->jsonFeedback([
             "ok"       => "1",
             "message"  => trans_safe("ehdaManage::ehda.volunteers.already_volunteer"),
             "callback" => "cardEditor(2, '$user->hashid')",
        ]);
    }



    /**
     * @param $user
     *
     * @return string
     */
    private function inquiryResponseWhenExistsButNotVolunteer($user)
    {
        return $this->jsonFeedback([
             "ok"           => "1",
             "message"      => trans_safe("ehdaManage::ehda.volunteers.inquiry_success"),
             'redirect'     => url("manage/volunteers/create/$user->code_melli"),
             'redirectTime' => 1,
        ]);
    }



    /**
     * Builds view arguments and stores in $this->view_arguments
     */
    protected function buildViewArguments()
    {
        parent::buildViewArguments();

        $this->view_arguments['default_role'] = $this->default_role;
        $this->view_arguments['roles']        = $this->roles;
        $this->view_arguments['role_model']   = $this->role_model;
    }



    /**
     * Chooses Create View
     */
    protected function chooseCreateView()
    {
        if ($this->model->exists and $this->model->withDisabled()->is_admin()) {
            $this->create_view = 'show-container';
            return;
        }

        $this->create_view = 'index';
    }



    /**
     * @param VolunteerSaveRequest $request
     *
     * @return string
     */
    public function volunteerSave(VolunteerSaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request, ['role_slug', 'status']);
        $saved_model->assignVolunteerRole($request->role_slug, $request->status);

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             'success_redirect' => $this->redirectPage($request),
             "success_refresh"  => $this->shouldRefreshPage($request),
        ]);
    }



    /**
     * @param $request
     *
     * @return string
     */
    private function redirectPage($request)
    {
        if ($request->model->not_exists) {
            return url("manage/volunteers/create");
        }

        return null;
        //return url("manage/volunteers/edit/$request->hashid");
    }



    /**
     * @param $request
     *
     * @return bool
     */
    private function shouldRefreshPage($request)
    {
        if ($request->model->exists) {
            return true;
        }

        return false;
    }



    /**
     * @param $hashid
     *
     * @return Factory|ResponseFactory|Response
     */
    public function show($hashid)
    {
        $this->model = model('User', $hashid);
        $this->loadAvailableRoles();
        $this->buildViewArguments();

        return $this->view('show', $this->view_arguments);
    }



    /**
     * @param VolunteerAddRoleRequest $request
     *
     * @return string
     */
    public function addRole(VolunteerAddRoleRequest $request)
    {
        $ok = $request->model->attachRole($request->role_slug, $request->status);

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_refresh' => true,
        ]);
    }
}
