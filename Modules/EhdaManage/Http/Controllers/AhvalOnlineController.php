<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\EhdaManage\Entities\UserPending;
use Modules\Yasna\Services\YasnaController;

class AhvalOnlineController extends YasnaController
{
    protected $base_model = "UserPending";



    /**
     * Called from AhvalOnlineQueryWhenNewUserPendingCreated
     *
     * @param UserPending $model
     *
     * @return bool
     */
    public function sendQuestion($model)
    {
        //@TODO: CURL to Ahval online system
        $model->markAsOnlineRequested();
        return true;
    }
}
