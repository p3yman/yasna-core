<?php namespace Modules\EhdaManage\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class PrintingsController extends YasnaController
{
    protected $base_model     = "Printing";
    protected $view_folder    = "ehdamanage::printings";
    protected $allowed_tabs   = ['pending', 'under_excel_printing'];
    protected $browse_url     = "cards/printings";
    protected $has_error      = false;
    protected $browse_tabs    = [];
    protected $view_arguments = [];
    protected $browse_columns = [];
    protected $events_array   = [];
    protected $request_event;
    protected $toolbar_buttons;
    protected $request_tab;
    protected $event;
    protected $models;
    protected $page;



    /**
     * @param string $request_event
     * @param string $request_tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($request_event = 'all', $request_tab = 'pending')
    {
        $this->request_event = $request_event;
        $this->request_tab   = $request_tab;

        if (!$this->checkRequestTab()) {
            return $this->abort(404);
        }

        $this->loadEvent();
        $this->loadEvents();
        $this->buildPageInfo();
        $this->queryBuilder();
        $this->loadToolbarButtons();
        $this->loadBrowseTabs();
        $this->loadBrowseColumns();
        $this->buildViewArguments();

        return $this->view('browse.index', $this->view_arguments);
    }



    /**
     * Loads Request Event Instance
     */
    private function loadEvent()
    {
        if (!$this->request_event or $this->request_event == 'all') {
            $this->event        = post();
            $this->event->title = trans_safe("ehdaManage::ehda.printings.all_events");
            return;
        }

        $this->event = post($this->request_event);
        return;
    }



    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    private function buildPageInfo()
    {
        $this->page[0] = [
             $this->browse_url,
             trans_safe("ehdaManage::ehda.printings.title"),
             $this->browse_url,
        ];

        $this->page[1] = [
             $this->request_event,
             str_limit($this->event->title, 40),
             "$this->browse_url/$this->request_event",
        ];

        $this->page[2] = [
             $this->request_tab,
             trans_safe("ehdaManage::ehda.printings.$this->request_tab"),
             "$this->request_event/$this->request_tab",
        ];
    }



    /**
     * Builds view arguments and stores in $this->view_arguments
     */
    private function buildViewArguments()
    {
        $this->view_arguments = [
             "page"            => $this->page,
             "models"          => $this->models,
             "request_tab"     => $this->request_tab,
             "request_event"   => $this->request_event,
             "event"           => $this->event,
             "events"          => $this->events_array,
             "tabs"            => $this->browse_tabs,
             "toolbar_buttons" => $this->toolbar_buttons,
             "browse_columns"  => $this->browse_columns,
             "controller"      => $this,
        ];
    }



    /**
     * @return bool
     */
    private function checkRequestTab()
    {
        return boolval(in_array($this->request_tab, $this->allowed_tabs));
    }



    /**
     * Load Events Table
     */
    private function loadEvents()
    {
        $all_events           = post()->registrableEvents()->orderBy('published_at', 'desc')->get();
        $this->events_array[] = [
             $this->event->id ? "" : "check",
             trans_safe("ehdaManage::ehda.printings.all_events"),
             url("manage/cards/printings/all/$this->request_tab"),
        ];

        foreach ($all_events as $event) {
            $this->events_array[] = [
                $this->event->id == $event->id? "check" : "" ,
                $event->title,
                url("manage/cards/printings/$event->hashid/$this->request_tab"),
            ];
        }
    }



    /**
     * Runs the query builder and stores the result in $this->models
     */
    private function queryBuilder()
    {
        $selector = [
             "criteria" => $this->request_tab,
             "event_id" => $this->event->id,
             //"user_id"    => $this->user_id,
             //"created_by" => $this->created_by,
        ];

        $this->models = model('printing')
             ->selector($selector)
             ->orderBy('updated_at', 'desc')
             ->paginate(50)
        ;
    }



    /**
     * Browse Tabs
     */
    private function loadBrowseTabs()
    {
        foreach ($this->allowed_tabs as $tab) {
            $this->browse_tabs[] = [$this->request_event . "/" . $tab, trans_safe("ehdaManage::ehda.printings.$tab")];
        }
    }



    /**
     * Browse Toolbar Buttons
     */
    private function loadToolbarButtons()
    {
        $browse_url = "manage/$this->browse_url";

        $this->toolbar_buttons = [
             [
                  "id"      => "btnDownloadExcel",
                  "target"  => "void(0);window.location='" . url("$browse_url/excel/$this->request_event") . "'",
                  "type"    => "pink",
                  "icon"    => "download",
                  "caption" => trans("manage::forms.button.download_excel_file"),
                  "class"   => $this->request_tab == 'pending' ? 'noDisplay' : '',
             ],

             [
                  "target"    => "modal:$browse_url/act/mass/revert-to-pending",
                  "type"      => "danger",
                  "condition" => $this->request_tab != 'pending',
                  "icon"      => "undo",
                  "caption"   => trans_safe("ehdaManage::ehda.printings.revert_to_pending"),
             ],

             [
                  "target"    => "modal:$browse_url/act/mass/confirm-quality",
                  "type"      => "success",
                  "condition" => $this->request_tab != 'pending',
                  "icon"      => "check",
                  "caption"   => trans_safe("ehdaManage::ehda.printings.verify_quality"),
             ],

             [
                  "target"    => "modal:$browse_url/act/mass/add-to-excel",
                  "type"      => "primary",
                  "condition" => $this->request_tab == 'pending',
                  "icon"      => "file-excel-o",
                  "caption"   => trans_safe("ehdaManage::ehda.printings.send_to_printing"),
             ],

             [
                  "target"    => "modal:$browse_url/act/mass/cancel",
                  "type"      => "danger",
                  "condition" => $this->request_tab == 'pending',
                  "icon"      => "times",
                  "caption"   => trans_safe("ehdaManage::ehda.printings.cancel"),
             ],

        ];
    }



    /**
     * Browse Columns
     */
    private function loadBrowseColumns()
    {
        $this->browse_columns = [
             trans('validation.attributes.name_first'),
             trans('validation.attributes.from_event_id'),
             trans('validation.attributes.home_city'),
             trans('validation.attributes.domain'),
        ];
    }
}
