<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 1/9/2018
 * Time: 10:44 AM
 */

namespace Modules\EhdaManage\Http\Utility;

class ParsaClient
{
    private $url;
    private $curl;
    private $result         = null;
    private $options        = [];
    private $parameters;
    private $postParameters = [];
    private $error          = null;
    private $headers        = [];



    /**
     * ParsaClient constructor.
     *
     * @param null $url
     */
    public function __construct($url = null)
    {
        $this->url  = $url;
        $this->curl = curl_init();
        //todo : make optional these
        $this->options = [
             CURLOPT_RETURNTRANSFER => true,   // return web page
             CURLOPT_USERAGENT      => "Ehda", // name of client
             CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
             CURLOPT_TIMEOUT        => 120,    // time-out on response
             CURLOPT_URL            => $this->url,
        ];
        curl_setopt_array($this->curl, $this->options);
    }



    /**
     * @param $value
     *
     * @return $this
     */
    public function setUserAgent($value)
    {
        curl_setopt($this->curl, CURLOPT_USERAGENT, $value);
        return $this;
    }



    /**
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function setCustomHeader($key, $value)
    {
        $this->headers[] = "$key:$value";
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
        return $this;
    }



    /**
     * todo : add array data feature like post method
     */
    public function get()
    {
        if (!is_null($this->parameters)) {
            curl_setopt($this->curl, CURLOPT_URL, $this->url . "?" . $this->parameters);
        }
        $this->result = curl_exec($this->curl);
        $this->error  = curl_error($this->curl);
        $this->close();
    }



    /**
     * @param null $data
     */
    public function post($data = null)
    {
        if (!is_null($data)) {
            $this->postParameters = $data;
        }
        $jsonFields = json_encode($this->postParameters);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $jsonFields);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, [
             'Content-Type: application/json',
             'Content-Length:' . strlen($jsonFields),
        ]);

        $this->result = curl_exec($this->curl);
        $this->error  = curl_error($this->curl);
        $this->close();
    }



    /**
     * close connection
     */
    public function close()
    {
        curl_close($this->curl);
    }



    /**
     * @param $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url                  = $url;
        $this->options[CURLOPT_URL] = $this->url;
        curl_setopt_array($this->curl, $this->options);
        return $this;
    }



    /**
     * @param $name
     * @param $value
     *
     * @return $this
     */
    public function setParameter($name, $value)
    {
        $this->parameters            .= "&$name=$value";
        $this->postParameters[$name] = $value;
        return $this;
    }



    /**
     * return body of request
     */
    public function getResult()
    {
        return $this->result;
    }



    /**
     * if error happen this show it
     */
    public function getError()
    {
        return $this->error;
    }
}
