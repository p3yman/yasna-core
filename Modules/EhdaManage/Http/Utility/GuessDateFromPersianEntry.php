<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 5/29/18
 * Time: 2:59 PM
 */

namespace Modules\EhdaManage\Http\Utility;

use Morilog\Jalali\jDateTime;

class GuessDateFromPersianEntry
{
    private $date;
    private $viceVersa;
    private $year;
    private $month;
    private $day;



    /**
     * @param string $date
     *
     * @return false or carbon
     */
    public static function ask(string $date)
    {
        $obj = new GuessDateFromPersianEntry($date);
        return $obj->getDate();
    }



    /**
     * GuessDateFromPersianEntry constructor.
     *
     * @param string $date
     */
    public function __construct(string $date)
    {
        $this->date = ed(trim($date));
        $gussDate   = $this->parse();
        if (!$gussDate) {
            $this->date = false;
        }
    }



    /**
     * @return bool
     */
    public function getDate()
    {
        return $this->date;
    }



    /**
     * parse string step by step
     *
     * @return bool
     */
    private function parse()
    {
        if (!$this->checkLength()) {
            return false;
        }

        if (!$this->validation()) {
            return false;
        }

        $this->makeDate();

        if (!$this->checkDate()) {
            return false;
        }

        return  $this->makeCarbon();
    }



    /**
     * @return bool
     */
    private function checkLength()
    {
        if (strlen($this->date) != 8) {
            return false;
        }
        return true;
    }



    /**
     * guessing that is this date string?
     *
     * @return bool
     */
    private function validation()
    {
        $date = null;
        if (substr($this->date, 0, 2) == "13") {
            $date = $this->date;
        }
        if (substr($this->date, 4, 2) == "13") {
            $date            = $this->date;
            $this->viceVersa = true;
        }
        if (!is_null($date)) {
            return true;
        }
        return false;
    }



    /**
     * generate date from string
     */
    private function makeDate()
    {
        if ($this->viceVersa) {
            $this->year  = substr($this->date, 4, 4);
            $this->month = substr($this->date, 2, 2);
            $this->day   = substr($this->date, 0, 2);
        } else {
            $this->year  = substr($this->date, 0, 4);
            $this->month = substr($this->date, 4, 2);
            $this->day   = substr($this->date, 6, 2);
        }
    }



    /**
     * check generated for being real date
     *
     * @return bool
     */
    private function checkDate()
    {
        return jDateTime::checkDate($this->year, $this->month, $this->day, true);
    }



    /**
     * @return bool
     */
    private function makeCarbon()
    {
        $this->date = jDateTime::createCarbonFromFormat('Y/m/d', "$this->year/$this->month/$this->day");
        return true;
    }
}
