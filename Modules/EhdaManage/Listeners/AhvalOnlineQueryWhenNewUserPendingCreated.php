<?php namespace Modules\EhdaManage\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaManage\Events\TooskaCreatedUserPending;

class AhvalOnlineQueryWhenNewUserPendingCreated implements ShouldQueue
{
    public $tries = 5;



    public function handle(TooskaCreatedUserPending $event)
    {
        return module("ehdaManage")->ahvalOnlineController()->sendQuestion($event->model);
    }
}
