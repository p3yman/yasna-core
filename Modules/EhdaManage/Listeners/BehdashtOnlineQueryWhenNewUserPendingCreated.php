<?php namespace Modules\EhdaManage\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaManage\Events\TooskaCreatedUserPending;

class BehdashtOnlineQueryWhenNewUserPendingCreated implements ShouldQueue
{
    public $tries = 5;



    /**
     * @param TooskaCreatedUserPending $event
     *
     * @return mixed
     */
    public function handle(TooskaCreatedUserPending $event)
    {
        chalk()->add("Ran Behdasht listener on model #" . $event->model->id);
        return module("ehdaManage")->behdashtOnlineController()->inquery($event->model);
    }
}
