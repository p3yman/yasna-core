<?php namespace Modules\EhdaManage\Listeners;

use App\Models\UserPending;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaManage\Events\TooskaUpdatedUserPending;

class RegisterCardWhenUserPendingCompleted implements ShouldQueue
{
    public $tries = 5;



    /**
     * @param TooskaUpdatedUserPending $event
     */
    public function handle(TooskaUpdatedUserPending $event)
    {
        chalk()->add("Ran RegisterCard listener on model #" . $event->model->id);

        $event->model = model('user_pending', $event->model->id);

        $this->callRegisterMethod($event->model);
    }



    /**
     * @param UserPending $model
     */
    private function callRegisterMethod($model)
    {
        chalk()->add("Ran RcallRegisterMethod on listener #" . $model->id);

        if ($model->shouldRegister()) {
            chalk()->add("Ran if callRegisterMethod on listener #" . $model->id);
            module('ehdaManage')->tooskaRegisterController()->registerSelectedApprovedUser($model);
        } else {
            chalk()->add("Ran else callRegisterMethod on listener #" . $model->id);
        }
    }
}
