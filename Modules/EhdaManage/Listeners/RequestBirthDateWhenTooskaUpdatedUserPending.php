<?php

namespace Modules\EhdaManage\Listeners;

use Modules\EhdaManage\Events\TooskaUpdatedUserPending;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaManage\Http\Api\EhdaApiReply;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;

class RequestBirthDateWhenTooskaUpdatedUserPending implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }



    /**
     * Handle the event.
     *
     * @param \Modules\EhdaManage\Events\TooskaUpdatedUserPending $event
     *
     * @return void
     */
    public function handle(\Modules\EhdaManage\Events\TooskaUpdatedUserPending $event)
    {
        if (
             (($model = $event->model)->channel == FanapServiceProvider::CHANNEL_NAME) and
             $model->isWaitingForBirthDate()
        ) {
            EhdaApiReply::model($model)
                        ->send()
            ;
        }
    }
}
