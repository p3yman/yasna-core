<?php

namespace Modules\EhdaManage\Listeners;

use App\Models\User;
use App\Models\UserPending;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Modules\EhdaManage\Events\ShortMessageReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\EhdaManage\Http\Api\EhdaApiReply;
use Modules\EhdaManage\Http\Utility\GuessDateFromPersianEntry;
use Modules\EhdaManage\Providers\EhdaManageServiceProvider;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;

class HandleContentWhenShortMessageReceived implements ShouldQueue
{
    public $tries = 5;

    protected $event;
    protected $pending_user_model;



    /**
     * Handle the event.
     *
     * @param ShortMessageReceived $event
     *
     * @return bool
     */
    public function handle(ShortMessageReceived $event)
    {
        $this->event = $event;
        if ($this->isCodeMelli()) {
            $this->handleReceivedCodeMelli();
        } elseif ($this->mayBeBirthDate()) {
            $this->handleReceivedBirthDate();
        } else {
            FanapServiceProvider::sendMessage(
                 $this->event->model->source,
                 trans('ehdamanage::short-message.main-guide', [
                      'url' => FanapServiceProvider::url(url('/')),
                 ])
            );
        }

        return response('done');
    }



    /**
     * Make action for received code melli
     *
     * @return void
     */
    protected function handleReceivedCodeMelli()
    {
        if (($pending_model = $this->findPendingUserByCodeMelli(
             $code_melli = ed(($short_message_model = $this->event->model)->text)
        ))->exists) {
            EhdaApiReply::model($pending_model)->send();
        } else {
            model('user-pending')->addTemporaryRecord([
                 "code_melli" => $code_melli,
                 "mobile"     => $short_message_model->source,
                 "channel"    => FanapServiceProvider::CHANNEL_NAME,
            ]);
        }
    }



    /**
     * @return bool
     */
    protected function isCodeMelli()
    {
        return Validator::make(
             ['message' => ed($this->event->model->text)],
             ['message' => 'required|code_melli']
        )->passes()
             ;
    }



    /**
     * @return bool
     */
    protected function isNotCodeMelli()
    {
        return !$this->isCodeMelli();
    }



    /**
     * @param string $message
     *
     * @return bool
     */
    protected function isUser($message)
    {
        return boolval($this->findUserWithCodeMelli($message));
    }



    /**
     * @param string $message
     *
     * @return User
     */
    protected function findUserWithCodeMelli($code_melli)
    {
        return user()->whereCodeMelli($code_melli)->limit(1)->first();
    }



    /**
     * @param string|null $message
     *
     * @return UserPending
     */
    protected function findPendingUserByCodeMelli($code_melli)
    {
        return (
             model('user-pending')
                  ->whereCodeMelli($code_melli)
                  ->whereChannel(FanapServiceProvider::CHANNEL_NAME)
                  ->orderBy('created_at', 'desc')
                  ->first()
             ?? model('user-pending')
        );
    }



    /**
     * @return bool
     */
    protected function mayBeBirthDate()
    {
        return is_numeric(ed($message = $this->event->model->text)) and
             (($message_length = strlen(ed($message))) < 10) and
             ($message_length > 6) and
             ($this->pending_user_model = model('user-pending')
                  ->birthDateRequestedRecords()
                  ->orderByDesc('created_at')
                  ->whereMobile($this->event->model->source)
                  ->first());
    }



    /**
     * Make action for received birth date
     *
     * @return void
     */
    protected function handleReceivedBirthDate()
    {
        $pending_user_model = $this->getPendingUserModel();
        $birth_date         = $this->guessDate();
        $user               = $this->findUserWithCodeMelli($pending_user_model->code_melli);

        // If this is not a phone number
        if (!$birth_date) {
            // @todo: check status
            EhdaApiReply::model($pending_user_model)
                        ->setStatus(ApiResponse::BIRTH_DAY_INVALID)
                        ->send()
            ;
        } elseif ($user and $user->is_a(EhdaManageServiceProvider::cardHolderRoleSlug())) {
            if ($user->birth_date and $birth_date->startOfDay()->equalTo($user->birth_date)) {
                $pending_user_model->hardDelete();
                EhdaApiReply::model($pending_user_model)
                            ->setStatusText($user->card_exists_message)
                            ->send()
                ;
            } else {
                // @todo: check status
                EhdaApiReply::model($pending_user_model)
                            ->setStatus(ApiResponse::FAILED_VALIDATION_SEND_AGAIN)
                            ->send()
                ;
            }
        } else {
            $pending_user_model->addBirthDate($birth_date->toDateString());
        }
    }



    /**
     * @return false|Carbon
     */
    protected function guessDate()
    {
        return GuessDateFromPersianEntry::ask($this->event->model->text);
    }



    /**
     * @return UserPending
     */
    protected function getPendingUserModel()
    {
        return $this->pending_user_model ?? model('user-pending');
    }
}
