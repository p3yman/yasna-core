<?php namespace Modules\EhdaManage\Listeners;

use App\Models\User;

use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaManage\Entities\UserPending;
use Modules\EhdaManage\Events\TooskaUpdatedUserPending;
use Modules\EhdaManage\Http\Api\ApiResponse;
use Modules\EhdaManage\Http\Api\EhdaApiReply;
use Modules\EhdaManage\Http\Utility\ParsaClient;
use Modules\Yasna\Services\Chalk;

class TooskaUpdateListener implements ShouldQueue
{
    public $tries = 5;
    private $model;



    /**
     * @param TooskaUpdatedUserPending $event
     *
     * @return void
     */
    public function handle(TooskaUpdatedUserPending $event)
    {
        $this->model = $event->model;

        if ($this->model->channel == "tooska") {
            $this->tooskaUpdate($this->model);
        }

        $this->sendSmsToUser($this->model);
    }



    /**
     * @param UserPending $model
     *
     * @return null
     */
    private function tooskaUpdate(UserPending $model)
    {
        $url         = $model->spreadMeta()->callback_url;
        $ehda_client = new ParsaClient($url);
        $ehda_client->setParameter('status', $model->apiStatus())
                    ->setParameter('msg', trans('ehdamanage::tooska-api.' . $model->apiStatus()))
                    ->setParameter('sms_send', '0')
                    ->setParameter('sms_body', '')
                    ->post()
        ;
        if ($ehda_client->getError()) {
            return false;
        }
        return $ehda_client->getResult();
    }



    /**
     * @param UserPending $model
     *
     * @return bool
     */
    private function sendSmsToUser(UserPending $model)
    {
        $userStatus = $model->apiStatus();
        if (!$this->checkStatus($userStatus)) {
            return false;
        }

        if ($userStatus == ApiResponse::USER_ALREADY_EXIST) {
            $user = model('user')->finder($model->code_melli);
            EhdaApiReply::model($model)->setStatusText($user->card_exists_message)->send();
            $this->sendFollowUpMessage($user);
            return true;
        }
        EhdaApiReply::model($model)->send();
        return true;
    }



    /**
     * @param User $user
     */
    public function sendFollowUpMessage(User $user)
    {
        EhdaApiReply::model($this->model)->setStatusText($user->followUpMessage())->send();
    }



    /**
     * @param $status
     *
     * @return bool
     */
    private function checkStatus($status)
    {
        $statusList = [
             ApiResponse::WAITING_FOR_AHVAL_ONLINE,
             ApiResponse::FAILED_VALIDATION_SEND_AGAIN,
             ApiResponse::UNABLE_TO_REGISTER_THIS_CODE_MELLI,
             ApiResponse::WAITING_FOR_AHVAL_OFFLINE,
             ApiResponse::USER_ALREADY_EXIST,
        ];
        \chalk()->add('userStatus', $status);
        if (!in_array($status, $statusList)) {
            return false;
        }
        return true;
    }
}
