<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/13/19
 * Time: 1:02 PM
 */

namespace Modules\Vendor\Services\Container;

use Exception;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class Resource
{
    use ModuleRecognitionsTrait;

    /** @var string|null */
    protected $slug;



    /**
     * Resource constructor.
     *
     * @param string|null $slug
     */
    public function __construct(?string $slug)
    {
        $this->slug = $slug;
    }



    /**
     * If the resource is available.
     *
     * @return bool
     */
    public function isAvailable()
    {
        return (boolval($this->slug) and $this->cacheExists());
    }



    /**
     * If the cache exists.
     *
     * @return bool
     */
    public function cacheExists()
    {
        try {
            return cache()->has($this->cacheKey());
        } catch (Exception $exception) {
            return false;
        }
    }



    /**
     * Returns the value of the cache.
     *
     * @return \Illuminate\Contracts\Cache\Repository|null|array
     */
    public function getData()
    {
        try {
            return cache()->get($this->cacheKey());
        } catch (Exception $exception) {
            return null;
        }
    }



    /**
     * Updates the cache value.
     *
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function updateCache()
    {
        $requester = $this->generateRequester();
        if ($requester->isNotReady()) {
            return;
        }

        $result = $requester->getDetails();
        if (empty($result)) {
            return;
        }

        cache()->set($this->cacheKey(), $result, $this->cacheValidityTime());
    }



    /**
     * Generates and returns a new requester instance.
     *
     * @return Requester
     */
    protected function generateRequester()
    {
        return new Requester(
             $this->getRequestUrl(),
             $this->slug
        );
    }



    /**
     * Returns the URL for the requester.
     *
     * @return string
     */
    protected function getRequestUrl()
    {
        return $this->runningModule()->getConfig('yasna.domain');
    }



    /**
     * Returns the key of the cache.
     *
     * @return string
     */
    protected function cacheKey()
    {
        return "vendor.$this->slug";
    }



    /**
     * Returns the validity time of the cache (in minutes).
     *
     * @return float|int
     */
    protected function cacheValidityTime()
    {
        return (24 * 60);
    }
}
