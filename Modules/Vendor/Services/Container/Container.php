<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/13/19
 * Time: 12:50 PM
 */

namespace Modules\Vendor\Services\Container;

use Modules\Vendor\Services\Container\Resource;


class Container
{
    /**
     * @var Resource
     */
    protected $resource;



    /**
     * Container constructor.
     *
     * @param Resource $resource
     */
    public function __construct(Resource $resource)
    {
        $this->resource = $resource;
    }



    /**
     * If the service is available.
     *
     * @return bool
     */
    public function isNotAvailable()
    {
        return !$this->isAvailable();
    }



    /**
     * If the service is not available.
     *
     * @return bool
     */
    public function isAvailable()
    {
        return $this->resource->cacheExists();
    }



    /**
     * Returns the details.
     *
     * @return array|\Illuminate\Contracts\Cache\Repository|null
     */
    public function getDetails()
    {
        return $this->resource->getData();
    }



    /**
     * Returns the resource property
     *
     * @return Resource
     */
    public function getResource()
    {
        return $this->resource;
    }
}
