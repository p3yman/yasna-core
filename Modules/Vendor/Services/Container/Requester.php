<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/13/19
 * Time: 12:49 PM
 */

namespace Modules\Vendor\Services\Container;


use GuzzleHttp\Client;

class Requester
{
    /** @var string|null */
    protected $url;
    /** @var string */
    protected $slug;



    /**
     * Requester constructor.
     *
     * @param string|null $url
     * @param string      $slug
     */
    public function __construct(?string $url, string $slug)
    {
        $this->url  = $url;
        $this->slug = $slug;
    }



    /**
     * If this requester is ready to call the provider.
     *
     * @return bool
     */
    public function isNotReady()
    {
        return !$this->isReady();
    }



    /**
     * If this requester is ready to call the provider.
     *
     * @return bool
     */
    public function isReady()
    {
        return (boolval($this->url) and boolval($this->slug));
    }



    /**
     * Returns the details.
     *
     * @return array
     */
    public function getDetails()
    {
        $response = $this->getResponseArray();

        if (empty($response)) {
            return [];
        }

        return ($response['results'][0] ?? []);
    }



    /**
     * Returns the response array.
     *
     * @return array
     */
    protected function getResponseArray()
    {
        $response = $this->getResponse();

        if ($response->getStatusCode() != 200) {
            return [];
        }

        $response_array = json_decode($response->getBody(), true);

        return ($response_array and is_array($response_array))
             ? $response_array
             : [];
    }



    /**
     * Returns the response.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function getResponse()
    {
        $client = $this->getClient();

        return $client->get($this->getUri(), $this->getParameters());
    }



    /**
     * Returns the client instance.
     *
     * @return Client
     */
    protected function getClient()
    {
        return new Client([
             'base_uri'    => $this->url,
             'http_errors' => false,
        ]);
    }



    /**
     * Returns the request URI.
     *
     * @return string
     */
    protected function getUri()
    {
        return 'api/modular/v1/posts-list?type=customers';
    }



    /**
     * Returns the request parameters.
     *
     * @return array
     */
    protected function getParameters()
    {
        return [
             'type'   => 'customers',
             'slug'   => $this->slug,
             'locale' => 'fa',
        ];
    }
}
