<?php 

namespace Modules\Vendor\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class DemoController extends YasnaController
{
    protected $base_model  = ""; // @TODO <~~
    protected $view_folder = ""; // @TODO <~~ (Remove if same as parent)



    /**
     * Vendor demo UI
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->view('vendor::demo.demo');
    }
}
