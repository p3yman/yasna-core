<?php

namespace Modules\Vendor\Http\Controllers;

use Modules\Vendor\Services\Container\Container;
use Modules\Yasna\Services\YasnaController;

class MainController extends YasnaController
{
    /**
     * Renders the vendor page.
     */
    public function index()
    {
        if ($this->getContainer()->isNotAvailable()) {
            return $this->abort(410);
        }

        return $this->view('vendor::main', $this->getIndexData());
    }



    /**
     * Returns the data for the index page.
     *
     * @return array
     * @throws \Nwidart\Modules\Exceptions\InvalidAssetPath
     */
    protected function getIndexData()
    {
        $details = $this->getDetails();
        return [
             'project_title'   => get_setting('site_title'),
             'project_logo'    => $this->module()->getAsset('images/yasnaweb-logo.png'),
             'project_version' => ($details['project_version'] ?? null),
             'project_date'    => ($details['project_date'] ?? null),
        ];
    }



    /**
     * Returns the details from the vendor container.
     *
     * @return array
     */
    protected function getDetails()
    {
        return $this->getContainer()->getDetails();
    }



    /**
     * Returns the vendor container
     *
     * @return Container
     */
    protected function getContainer()
    {
        return app('vendor');
    }
}
