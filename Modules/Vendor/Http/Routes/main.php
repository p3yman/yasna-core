<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Vendor')->getControllersNamespace(),
], function () {
    Route::get('yasnateam', 'MainController@index')->name('vendor');
});

Route::group([
     'middleware' => ['web', 'is:dev'],
     'namespace'  => module('Vendor')->getControllersNamespace(),
], function () {
    Route::get('vendor/demo', 'DemoController@index')->name('demo');
});
