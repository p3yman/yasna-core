<?php

return [
     "page_titles" => [
          "home"        => "خانه",
          "faq"         => "پرسش‌های متداول",
          "contact"     => "تماس با ما",
          "blog"        => "بلاگ",
          "blog_inner"  => "بلاگ » ",
          "jobs"        => "فرصت‌های شغلی",
          "jobs_active" => "فرصت‌های شغلی فعال",
          "jobs_inner"  => "فرصت‌های شغلی » ",
          "jobs_apply"  => "ارسال رزومه",
     ],

     "call_to_action" => [
          "faq"           => [
               "title"       => "پاسخ پرسش خود را نیافتید؟ از ما بپرسید، پاسخ‌گو خواهیم بود",
               "button_text" => "تماس با ما",
          ],
          "start_project" => [
               "title"       => "پروژه‌ی فوق‌العاده‌ی بعدی خود را با ما شروع کنید",
               "button_text" => "شروع کنید",
          ],
     ],

     'tag'       => 'برچسب',
     'read_more' => 'ادامه‌ی مطلب',
     'know_more' => 'بیشتر بدانید',

     'yasna_team' => 'یسناتیم',

     'copyright' => 'حق و حقوق مادی و معنوی برای یسناتیم محفوظ است. ۱۳۹۷',

     "where_we_are" => "کجا هستیم؟",

     "version" => "ورژن :version",
     "date"    => "تاریخ: :date",

     "about_yasna" => "یسناوب یک سامانه تخصصی و پرکاربرد برای مدیریت محتوایی چندزبانه است که ما در «یَسناتیم» آن را طراحی کرده‌ایم. یَسناتیم چیست؟ خب اجازه دهید قبل از هر چیز کمی درباره خودمان بگوییم. ما گروهی کوچک و قدرتمند از متخصصان نرم‌افزار هستیم که زیر نظر شرکت توسعه فن‌آوری اطلاعات سورنا (سهامی خاص) فعالیت می‌کنیم.",

     "address" => "تهران، خیابان ابوذر غفاری، کوچه چهاردهم، پلاک ۸، واحد ۲",


];
