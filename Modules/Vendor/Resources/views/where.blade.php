<section id="where">

    <div class="part-title" data-aos="zoom-in-up">{{ trans('vendor::general.where_we_are') }}</div>

    <div class="container">

        @if(!isset($is_minimal) or !$is_minimal)
            <div id="map" data-lat="{{ get_setting('location')[0] }}" data-lng="{{ get_setting('location')[1] }}"></div>
        @endif

        <div class="contact-info">
            <div class="row">
                <div class="col-sm-4" data-aos="fade-up">
						<span class="icon gray xs">
							<img src="{{ Module::asset('vendor:images/icons/map.svg') }}">
						</span>
                    <div class="text">
                        {{ config('vendor.yasna.contact.address') }}
                    </div>
                </div>
                <div class="col-sm-4" data-aos="fade-up" data-aos-delay="100">
                    <span class="icon gray xs"><img src="{{ Module::asset('vendor:images/icons/phone.svg') }}"></span>
                    <div class="text phone">{{ pd( config('vendor.yasna.contact.phone') ) }}</div>
                </div>
                <div class="col-sm-4" data-aos="fade-up" data-aos-delay="150">
                    <span class="icon gray xs"><img src="{{ Module::asset('vendor:images/icons/mail.svg') }}"></span>
                    <div class="text email">
                        <a href="mailto:{{ config('vendor.yasna.contact.email') }}">
                            {{ config('vendor.yasna.contact.email') }}
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>
