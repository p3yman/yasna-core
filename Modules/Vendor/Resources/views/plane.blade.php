<!DOCTYPE html>
<html lang="{{ getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>


	<title>{{ trans('vendor::general.yasna_team') }}</title>

	@if( isLangRtl() )
		{!! Html::style(Module::asset('vendor:css/front-style.min.css')) !!}
	@else
		{!! Html::style(Module::asset('vendor:css/front-style-ltr.min.css')) !!}
	@endif
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
		  integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
		  crossorigin=""/>

</head>
<body>

<header id="main-header" class="{{ (isset($is_minimal) and $is_minimal) ? 'pb50' : '' }}">

	<div class="container">

		<div class="header-wrapper">

			<a href="{{ config('vendor.yasna.url') }}" id="logo" data-aos="flip-left">
				<img src="{{ Module::asset('vendor:images/logo.png') }}" width="200"
					 alt="{{ trans('vendor::genral.yasnateam') }}">
			</a>

		</div>

	</div>

	<div class="divider">
		@if(!isset($is_minimal) or !$is_minimal)
			<img src="{{ Module::asset('vendor:images/divider-white.svg') }}">
		@endif
	</div>

	<div class="bg-shapes">
		<span class="shape-1"></span>
		<span class="shape-2"></span>
		<span class="shape-3"></span>
	</div>

	{{-- Header big title --}}
	<div class="container">
		<div class="big-title">
			@yield('header-big-title')
		</div>
	</div>

</header>


{{-- Inner body --}}
@yield('inner-body')


<footer id="main-footer">

	<div class="content">

		<div class="container">

			<a href="{{ $__module->getConfig('yasna.domain') }}" id="logo-footer" data-aos="fade-right">
				<img src="{{ Module::asset('vendor:images/logo-footer.png') }}" width="156">
			</a>

			<div class="left" data-aos="fade-left">
				<div class="socials">
					<a href="{{ config('vendor.yasna.contact.instagram') }}" class="icon-in"></a>
					<a href="{{ config('vendor.yasna.contact.linkedin') }}" class="icon-li"></a>
				</div>
			</div>

		</div>

	</div>

	<div class="copyright">{{ trans('vendor::general.copyright') }}</div>

</footer>

{!! Html::script(Module::asset('vendor:libs/jquery.js')) !!}
{!! Html::script(Module::asset('vendor:libs/aos.js')) !!}
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
		integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
		crossorigin=""></script>
{!! Html::script(Module::asset('vendor:js/app.js')) !!}

@yield('footer-assets')

</body>
</html>
