@extends('vendor::plane')

@php
	$project_title= "پروژه معظم گاچت";
	$project_logo = "//yasna.team/modules/yasnateam/images/yasnaweb-logo.png";
	$project_version = "2.2.1";

	$is_minimal = true;
@endphp

@section('header-big-title')
 <h1 data-aos="fade-up" class="aos-init aos-animate">
	 <img src="{{ $project_logo }}" width="146" class="ml30">
	 {{ $project_title }}
 </h1>

 <div class="row">
	 <div class="col-sm-8 col-center">
		 <div class="text aos-init aos-animate" data-aos="fade-up" data-aos-delay="150">
			 <p>
				{{ trans('vendor::general.version', ["version" => pd($project_version) ]) }}
			 </p>
			<p>
				{{ trans('vendor::general.about_yasna') }}
			</p>
		 </div>
	 </div>
 </div>
@stop

@section('inner-body')

	@if(!isset($is_minimal) or !$is_minimal)
	 @include("vendor::content")
	@endif

	@include("vendor::where")


@stop
