<?php

namespace Modules\Vendor\Providers;

use Modules\Vendor\Schedules\UpdateVendorValueSchedule;
use Modules\Vendor\Services\Container\Container;
use Modules\Vendor\Services\Container\Resource;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class VendorServiceProvider
 *
 * @package Modules\Vendor\Providers
 */
class VendorServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerSingletons();
        $this->registerNeeds();
        $this->registerSchedules();
    }



    /**
     * Registers the singletons.
     */
    protected function registerSingletons()
    {
        $this->app->singleton('vendor', function () {
            return app()->make(Container::class);
        });
    }



    /**
     * Register the needs.
     */
    protected function registerNeeds()
    {
        $this->app->when(Container::class)
                  ->needs(Resource::class)
                  ->give(function () {
                      $slug = get_setting('vendor_slug');

                      return new Resource($slug);
                  })
        ;
    }



    /**
     * Register schedules.
     */
    protected function registerSchedules()
    {
        $this->addSchedule(UpdateVendorValueSchedule::class);
    }
}
