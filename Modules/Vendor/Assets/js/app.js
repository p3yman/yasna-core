jQuery(function($){
    AOS.init({
        once: true,
        duration: 550,
    });

    // Map
    if( $('#map').length ){
        var map_location = [ $('#map').data('lat'), $('#map').data('lng')];
        var map = L.map('map').setView(map_location, 16);
        map.scrollWheelZoom.disable();
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        L.marker(map_location).addTo(map);
    }



}); //End Of siaf!
