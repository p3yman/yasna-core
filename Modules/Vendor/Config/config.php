<?php

return [
     'name' => 'Vendor',

     "yasna" => [
          'domain' => env('YASNA_DOMAIN'),
          "url"    => "https://yasna.team",

          "contact" => [
               "email"     => "hello@yasna.team",
               "phone"     => "22505661 - 22324472",
               "address"   => trans('vendor::general.address'),
               "instagram" => "https://www.instagram.com/yasnateam/",
               "linkedin"  => "https://www.linkedin.com/company/yasnateam",
          ],

     ],
];
