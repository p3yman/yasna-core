<?php

namespace Modules\Vendor\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns the data to be seeded.
     *
     * @return array
     */
    public function data()
    {
        $module_handler = $this->runningModule();
        return [
             [
                  'slug'          => 'vendor_slug',
                  'title'         => $module_handler->getTrans('seeder.settings.vendor-slug'),
                  'category'      => 'upstream',
                  'order'         => '10',
                  'data_type'     => 'text',
                  'default_value' => '',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => 0,
             ],
        ];
    }
}
