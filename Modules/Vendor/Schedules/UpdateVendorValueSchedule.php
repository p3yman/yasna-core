<?php

namespace Modules\Vendor\Schedules;

use Modules\Vendor\Services\Container\Container;
use Modules\Vendor\Services\Container\Resource;
use Modules\Yasna\Services\YasnaSchedule;

class UpdateVendorValueSchedule extends YasnaSchedule
{
    /**
     * @inheritdoc
     */
    protected function job()
    {
        $this->getResource()->updateCache();
    }



    /**
     * Returns the vendor resource.
     *
     * @return Resource
     */
    protected function getResource()
    {
        return $this->getVendorContainer()->getResource();
    }



    /**
     * Returns the vendor container.
     *
     * @return Container
     */
    protected function getVendorContainer()
    {
        return app('vendor');
    }



    /**
     * @inheritdoc
     */
    protected function frequency()
    {
        return 'dailyAt:2:00';
    }
}
