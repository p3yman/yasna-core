{{--
|--------------------------------------------------------------------------
| Display Toggles
|--------------------------------------------------------------------------
| print, show-records, show-invoice
--}}


{!!
widget("link")
//	->id('btnCart-print')
    ->class('btn btn-primary btn-taha')
	->label('tr:cart::process.print')
	->target("url:manage/carts/print/$model->hashid")
	->newWindow()
!!}

{!!
widget("button")
	->id('btnCart-records')
	->shape('info')
	->label('tr:cart::process.records_show')
	->target("cartProcess('show-records')")
    ->class('-cart-panel btn-taha')
!!}

{!!
widget("button")
	->id('btnCart-invoice')
	->shape('info')
	->label('tr:cart::process.invoice_show')
	->hidden()
	->target("cartProcess('show-invoice')")
    ->class('-cart-panel btn-taha')
!!}


{{--
|--------------------------------------------------------------------------
| Status Buttons
|--------------------------------------------------------------------------
|
--}}
{!!
widget("button")
	->label('tr:cart::process.confirm')
	->color('success')
	->condition($model->isActionAllowed('confirm'))
	->type('submit')
	->value('confirm')
!!}

{!!
widget("button")
	->label('tr:cart::process.status')
	->color('purple')
	->condition($model->isActionAllowed('status'))
	->target("cartProcess('status')")
!!}

{!!
widget("button")
	->label('tr:cart::process.payment_status')
	->color('purple')
	->condition($model->isActionAllowed('paymentStatus'))
	->target("cartProcess('paymentStatus')")
!!}

{{--
|--------------------------------------------------------------------------
| Handle for Less Important Things
|--------------------------------------------------------------------------
|
--}}
{!!
widget("button")
	->id('btnCart-toggle')
    ->label('tr:cart::process.more')
    ->target("$('#divCart-more').slideToggle('fast')")
!!}


<script> /* TODO: Move it to a JS File! */
    function cartProcess(action) {

        let $panel_invoice  = $("#divCart-invoice");
        let $panel_records  = $("#divCart-records");
        let $button_print   = $("#btnCart-print");
        let $button_invoice = $("#btnCart-invoice");
        let $button_records = $("#btnCart-records");
        let $hidables       = $(".-cart-panel");

        $(".form-feed").slideUp('fast');

        switch (action) {
            case 'show-records' :
                $hidables.hide();
                $panel_records.slideDown('fast');
                $button_invoice.show();
                divReload('divCart-records');
                break;

            case 'show-invoice' :
                $hidables.hide();
                $panel_invoice.slideDown('fast');
                $button_print.show();
                $button_records.show();
                break;

            default:
                cartProcessPrepareForm(action);
                $(".-cart-form").toggle('fast');

        }
    }
</script>