<div class="modal-body">

	<div id="divCart-title">
		@include("cart::process.title")
	</div>

	<div id="divCart-invoice" class="-cart-panel -cart-invoice">
		@include("cart::process.invoice")
	</div>

	<div id="divCart-records" class="-cart-panel noDisplay" data-src="manage/carts/process/{{$model->hashid}}/records">
		@include("cart::process.records-loading")
	</div>

</div>

<div class="modal-footer">
	@include("cart::process.buttons")
	<div id="divCart-more" class="noDisplay mv10">
		@include("cart::process.more")
	</div>
	{!! widget("feed") !!}
</div>
