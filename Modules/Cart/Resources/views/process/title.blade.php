<?php
$lang = getLocale();
?>
{{--
|--------------------------------------------------------------------------
| Customer Name
|--------------------------------------------------------------------------
|
--}}
<div class="font-yekan text-bold f18">
	{{ $model->user->full_name }}
</div>

{{--
|--------------------------------------------------------------------------
| Status and Cart Code
|--------------------------------------------------------------------------
|
--}}
<div class="row">

	<div class="col-md-9">
		<span class="text-{{ $model->status_color }} f12">
			{{ $model->status_text }}
		</span>
		<span class="text-{{ $model->payment_status_color }} f12 mh5">
			{{ $model->payment_status_text }}
		</span>
		<span class="text-darkgray f12">
			@if($model->isRaised())
				(
				{{ trans("cart::carts.raised_at") }}
				:
				&nbsp
				@if($lang == 'fa')
					{{ pd(jdate($model->raised_at)->format('Y/m/j . H:i')) }}
					)
				@elseif($lang == 'en')
					{{ carbon()::createFromTimeStamp(strtotime($model->raised_at))->toDateTimeString()}}
					)
				@endif
			@endif
		</span>
	</div>



	<div class="col-md-3 text-bold mv5 text-gray" style="letter-spacing:3px ; text-align: left">
		{{ $model->code }}
		<span class="fa fa-barcode"></span>
	</div>

</div>
