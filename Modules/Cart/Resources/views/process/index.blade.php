{!!
widget("modal")
	->label('tr:cart::process.title')
	->target('name:cart-process')
	->validation(false)
!!}

{!!
widget("hidden")
	->name('hashid')
	->value($model->hashid)
!!}

<div class="-cart-form noDisplay">
	@include("cart::process.form")
</div>

<div class="-cart-form">
	@include("cart::process.display")
</div>