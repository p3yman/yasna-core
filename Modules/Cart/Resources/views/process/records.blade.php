<div>
	@foreach($records = $model->records as $record)
		@include("cart::process.record")
	@endforeach
</div>


{{--
|--------------------------------------------------------------------------
| Null Content
|--------------------------------------------------------------------------
|
--}}
@if($records->count() == 0)
	<div class="null text-center">
		{{ trans_safe("cart::records.nothing_yet") }}
	</div>
@endif
