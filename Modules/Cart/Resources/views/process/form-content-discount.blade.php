{!!
widget("input")
	->disabled()
	->inForm()
	->id('txtInvoiced')
	->label("tr:cart::carts.overall_price")
	->value($model->invoiced_amount)
	->addon( $model->currency()->title )
	->numberFormat()
	->containerClass('-process-form -process-discount')
!!}

{!!
widget("input")
	->name('added_discount')
	->inForm()
	->id('txtDiscount')
	->label('tr:cart::carts.discount')
	->value( $model->added_discount )
	->addon( $model->currency()->title )
	->numberFormat()
	->containerClass('-process-form -process-discount')
	->required()
!!}