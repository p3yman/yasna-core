{!!
widget("button")
    ->label('tr:cart::process.checkout')
    ->color('success')
    ->condition($model->isActionAllowed('checkout'))
	->target("cartProcess('checkout')")
!!}


{!!
widget("button")
	->label('tr:cart::process.remarks')
	->target("cartProcess('remarks')")
    ->condition($model->isActionAllowed('remarks'))
	->color('green')
!!}

{!!
widget("button")
	->label('tr:cart::process.discount')
	->target("cartProcess('discount')")
    ->condition($model->isActionAllowed('discount'))
	->color('pink')
!!}

{!!
widget("button")
	->label("tr:cart::process.checkoutUndo")
	->target("cartProcess('checkoutUndo')")
    ->condition($model->isActionAllowed('checkoutUndo'))
	->color('warning')
!!}

{!!
widget("button")
	->label('tr:cart::process.delete')
	->target("cartProcess('delete')")
	->condition($model->can('delete') and $model->isActionAllowed('delete'))
	->color('danger')
!!}

