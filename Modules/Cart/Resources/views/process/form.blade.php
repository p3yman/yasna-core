<div class="modal-body">

    <div id="divCart-title2">
        @include("cart::process.title")
    </div>

    {!!
    widget("separator")
    !!}

    <div style="min-height: 200px">
        @include("cart::process.form-content")
    </div>

</div>

<div class="modal-footer">
    {!!
    widget("button")
         ->type('submit')
         ->id('cmdDelete')
         ->value('delete')
         ->color('danger')
         ->hidden()
         ->label("tr:cart::process.delete")
    !!}
    {!!
    widget("button")
         ->type('submit')
         ->id('cmdSubmit')
         ->label('tr:manage::forms.button.send_and_save')
         ->color('primary')
    !!}

    {!!
    widget("button")
         ->label('tr:manage::forms.button.cancel')
         ->shape('link')
         ->target("cartProcess('form-cancel')")
    !!}

    {!!
    widget("feed")
    !!}
</div>


<script> /* TODO: Move it to a JS File! */
    function cartProcessPrepareForm(action)
    {
        let $hidables       = $(".-process-form");
        let $button         = $("#cmdSubmit");
        let $delete_button  = $("#cmdDelete,#noteDelete");
        let $remarks        = $("#txtRemarks").parent().parent();
        let $discounts      = $(".-process-discount");
        let $checkoutUndo   = $("#noteCheckoutUndo");
        let $checkout       = $("#noteCheckout");
        let $status         = $(".-process-status");
        let $payment_status = $(".-process-payment-status");

        $hidables.hide();
        $checkoutUndo.hide();
        $checkout.hide();
        $button.val(action).show();
        $delete_button.hide();

        switch (action) {
            case 'remarks' :
                $remarks.slideDown('fast');
                break;

            case 'discount':
                $discounts.slideDown('fast');
                $remarks.slideDown('fast');
                break;

            case 'checkoutUndo' :
                $remarks.slideDown('fast');
                $checkoutUndo.slideDown('fast');
                break;

            case 'checkout' :
                $remarks.slideDown('fast');
                $checkout.slideDown('fast');
                break;

            case 'status':
                $remarks.slideDown('fast');
                $status.slideDown('fast');
                break;

            case 'paymentStatus' :
                $remarks.slideDown('fast');
                $payment_status.slideDown('fast');
                break;

            case 'delete':
                $button.hide();
                $delete_button.slideDown('fast');
                break;
        }
    }
</script>
