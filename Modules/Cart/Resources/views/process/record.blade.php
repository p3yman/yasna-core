{{--
|--------------------------------------------------------------------------
| Header
|--------------------------------------------------------------------------
|
--}}
<div class="mvt20 mvb10 f12">
	{{--<span class="fa fa-star mhl10"></span>--}}
	<span class="font-yekan text-bold mhl10">
		{{ pd($loop->remaining + 1) }}.
	</span>
	<span class="font-yekan text-bold">
		{{ $record->title }}
	</span>
</div>


{{--
|--------------------------------------------------------------------------
| Other Details
|--------------------------------------------------------------------------
|
--}}
@include("manage::widgets.grid-text" , [
	'text' => $record->remarks,
	"condition" => $record->remarks ,
	'size' => "12" ,
	'color' => "gray" ,
	'class' => "text-light mh10" ,
]     )

{{--
|--------------------------------------------------------------------------
| User and Timestamp
|--------------------------------------------------------------------------
|
--}}
@include("manage::widgets.grid-date" , [
	'date' => $record->created_at ,
	'text' => trans("manage::forms.general.by") . SPACE . $record->creator->full_name ,
	"class" => "mh20" ,
]     )
