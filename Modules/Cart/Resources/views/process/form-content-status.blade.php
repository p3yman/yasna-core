{!!
widget("radio")
	->name('new_status')
	->value($model->status)
	->inForm()
	->label('tr:cart::process.new_status')
	->containerClass('-process-form -process-status')
	->options($model->statusOptions())
!!}

{!!
widget("radio")
	->name('payment_status')
	->value($model->payment_status)
	->inForm()
	->label('tr:cart::process.new_status')
	->containerClass('-process-form -process-payment-status')
	->options($model->paymentStatusOptions())
!!}

