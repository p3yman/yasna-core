@php
	//todo maybe it could be better performance
	$order_count = model('cart')
	->where('raised_at', '<=', now())
	->join('orders', 'orders.cart_id', 'carts.id')
	->where('orders.ware_id', $model->id)
	->sum('orders.count');

	$decimal_count = (is_decimal($order_count)) ? 2 : 0;
@endphp
{{ad(number_format($order_count,$decimal_count))}}

{{(!empty($model->unit))?trans('shop::units.'.$model->unit->slug):""}}