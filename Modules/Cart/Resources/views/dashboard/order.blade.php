<div class="well m0">
	<h3 class="m0">
		{{ $model->code }}
	</h3>

	<p class="clearfix mv20 mt0">
		<small class="mr-sm">
			{{ trans('cart::dashboard.order-finder.by')." ". $model->user->full_name }}
		</small>
		<small class="mr-sm">
			@php($timeStamp = $model->isRaised()? $model->raised_at : $model->created_at)

			{{ ad(jDate::forge($timeStamp)->format('j F Y')) }}
		</small>
	</p>

	<div class="row">
		<div class="col-md-6 text-left">
			@php($status = $model->status)

			@include('manage::widgets.grid-badge',[
				'text' => $model->status_text ,
				'color' => $model->status_color ,
				'icon' => $model->status_icon ,
			])

			@include('manage::widgets.grid-badge',[
				'text' => $model->payment_status_text ,
				'color' => $model->payment_status_color ,
				'icon' => $model->payment_status_icon ,
			])
		</div>
		<div class="col-md-6 text-right">
			{!!
				widget("button")
					->label("tr:cart::process.title")
					->target("modal:manage/carts/process/$model->hashid")
					->class('btn-outline')
					->shape('primary')
					->condition($model->can('process'))
			!!}
		</div>
	</div>
</div>
