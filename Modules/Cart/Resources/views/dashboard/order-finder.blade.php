<div style="min-height: 200px;">
	{!!
	widget('form-open')
		->target(route('search-order'))
		->method('get')
		->class('js')
!!}

	{!!
		widget('input')
		->label('tr:cart::dashboard.order-finder.enter_order_code')
		   ->name('keyword')
	 !!}

	{!!
		widget('button')
		->label('tr:cart::dashboard.order-finder.find')
		->shape('primary')
		->class('mt btn-taha')
		->type('submit')
	 !!}

	{!!
		widget('feed')
		->containerClass('mb0 mv20 pv ph0')
	!!}

	{!!
		widget('form-close')
	!!}
</div>
