@if($model->factor_request)
	<tr class="f20">

		<td class="text-center {{--bg-gray--}}" style="vertical-align: middle; width: 100px;">
			<i class="fa fa-arrow-down"></i>
		</td>

		<td colspan="2">
			<div class="pv20">
				{{ trans("cart::carts.factor.factor_request") }}
			</div>
		</td>
		<td style="vertical-align: middle">
			<h5>
				@if($model->factor_request == 1)
					{{ trans("cart::carts.factor.yes") }}
				@elseif($model->factor_request==0)
					{{trans("cart::carts.factor.no")}}
				@endif</h5>
		</td>
	</tr>
@endif
