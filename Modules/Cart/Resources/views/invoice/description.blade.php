@if($model->description)
	<tr class="f20">

		<td class="text-center" style="vertical-align: middle; width: 100px;">
			<i class="fa fa-comment-alt"></i>
		</td>

		<td colspan="2">
			<div class="pv20">
				{{ trans("cart::carts.description") }}
			</div>
		</td>
		<td style="vertical-align: middle">
			<h5>{{$model->description}}</h5>
		</td>
	</tr>
@endif
