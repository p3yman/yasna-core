@component('manage::layouts.printable')

	@slot('size')
		'A5-landscape'
	@endslot

	<div class="container mt">
		@include("cart::invoice.index")
	</div>

@endcomponent

