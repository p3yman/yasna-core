<tr class="f20">

	<td class="text-center {{--bg-gray--}}" style="vertical-align: middle; width: 100px;">
		<i class="fa fa-arrow-down"></i>
	</td>

	<td colspan="2">
		<div class="pv20">
			{{ trans("cart::carts.discount") }}
			@include("cart::invoice.discount_applied_codes")
		</div>
	</td>

	{{--<td style="vertical-align: middle"> - </td>--}}

	<td style="vertical-align: middle">
		{{ ad(number_format($model->added_discount)) }}
	</td>
</tr>
