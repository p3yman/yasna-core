<tr class="f20">

	<td class="text-center {{--bg-gray--}}" style="vertical-align: middle; width: 100px;">
		<i class="fa fa-star-o"></i>
	</td>

	<td class="f20">
		<div class="pv20">
			{{ trans("cart::carts.overall") }}
		</div>
	</td>

	<td colspan="2" class="f20" style="vertical-align: middle">
		{{ ad(number_format($model->invoiced_amount)) }}
		&nbsp;
		{{ trans("shop::currencies.$model->currency")  }}
	</td>
</tr>
