@if(!is_null($model->coupons))
	<div class="f12 text-gray">
		{{ trans("cart::process.applied_coupons") }}
		@foreach($model->coupons as $applied_coupon)
			<span class="ph10 bg-{{ array_rand(['green' => '', 'purple' => '', 'blue' => '']) }}">
		{{ $applied_coupon->code->slug }}
				| {{ ad($applied_coupon->applied_discount) }}</span>
		@endforeach
	</div>
@endif
