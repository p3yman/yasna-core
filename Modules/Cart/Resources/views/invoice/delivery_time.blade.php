@if($model->delivery_time)
	<tr class="f20">

		<td class="text-center {{--bg-gray--}}" style="vertical-align: middle; width: 100px;">
			<i class="fa fa-arrow-down"></i>
		</td>

		<td colspan="2">
			<div class="pv20">
				{{ trans("cart::carts.delivery.delivery_time") }}
			</div>
		</td>
		<td style="vertical-align: middle">
			<h5>{{ pd(jDate::forge((int) $model->work_day)->format('j F Y')) }}</h5>
			<h5>{{ pd($model->delivery_time) }}</h5>
		</td>
	</tr>
@endif
