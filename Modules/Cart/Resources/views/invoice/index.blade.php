@php
	$user = $model->user;
	$date = $model->isRaised()? $model->raised_at : $model->created_at;
	$lang = getLocale();
@endphp
<div class="panel b">
	<!-- Factor Info -->
	<table class="table table-bordered bg-white mb0 mt0">
		<tbody>
		<tr>
			<th class="{{--bg-gray--}}" style="width: 100px; vertical-align: middle">
				<strong>
					{{ trans('cart::carts.invoice_number') }}
				</strong>
			</th>
			<td>
				{{ $model->code }}
			</td>
		</tr>
		<tr>
			<th class="{{--bg-gray--}}" style="width: 100px; vertical-align: middle">
				<strong>
					{{ trans('cart::carts.order_date') }}
				</strong>
			</th>
			<td>
				@if($lang === 'fa')
					{{ pd(jDate::forge($date)->format('j F Y [H:i]')) }}
				@elseif($lang === 'en')
					{{ carbon()::createFromTimeStamp(strtotime($model->raised_at))->toDateTimeString()}}
				@endif
			</td>
		</tr>
		</tbody>
	</table>
	<!-- !END Factor Info -->
</div>

<div class="panel b">
	<!-- Seller and Buyer Details -->
	<table class="table table-bordered bg-white mb0 mt0">
		<tbody>
		<!-- Seller -->
		<tr>
			<th class="{{--bg-gray--}} text-center text-bold" style="width: 100px; vertical-align: middle">
				{{  trans('cart::carts.seller') }}
			</th>
			<td>
				<p>
				<span class="m5">
					<strong>
						{{  trans('cart::carts.seller').": " }}
					</strong>
					{{ get_setting('site_title') }}
				</span>
					<span class="m5">
					<strong>
						{{  trans('cart::carts.telephone').": " }}
					</strong>
						@foreach($telephones = get_setting('telephone') as $telephone)
							<span style="display: inline-block;direction: ltr">
								{{ ad($telephone) }}
							</span>
							@if($loop->iteration !== $loop->count)
								<span> | </span>
							@endif
						@endforeach
				</span>
					<span class="m5">
					<strong>
						{{  trans('cart::carts.email').": " }}
					</strong>
						@foreach($emails = get_setting('email') as $email)
							<span style="display: inline-block;direction: ltr">
								{{ $email }}
							</span>
							@if($loop->iteration !== $loop->count)
								<span> | </span>
							@endif
						@endforeach
				</span>
				</p>
				<p>
					<span class="m5">
					<strong>
						{{  trans('cart::carts.address').": " }}
					</strong>
						{{ ad(get_setting('address')) }}
				</span>
				</p>
			</td>
		</tr>
		<!-- !END Seller -->

		<!-- Buyer -->
		<tr>
			<th class="{{--bg-gray--}} text-center text-bold" style="width: 100px; vertical-align: middle">
				{{  trans('cart::carts.buyer') }}
			</th>
			<td>
				<p>
					<span class="m5">
						<strong>
							{{  trans('cart::carts.buyer').": " }}
						</strong>
						{{ $user->full_name }}
					</span>
					<span class="m5">
						<strong>
							{{  trans('cart::carts.telephone').": " }}
						</strong>
						<span style="display: inline-block; direction: ltr">
							{{ ad($user->mobile) }}
						</span>
					</span>
					<span class="m5">
						<strong>
							{{  trans('cart::carts.postal_code').": " }}
						</strong>
						<span style="display: inline-block; direction: ltr">
							{{ ad($user->postal_code) }}
						</span>
					</span>
				</p>
				<p>
					<span class="m5">
					<strong>
						{{  trans('cart::carts.address').": " }}
					</strong>
						@php
							$user_address = model('address', $model->address_id);
							if ($user_address->exists)
							{
								$user_address->spreadMeta();
								if ($user_address->floor)
								{
									$floor = $user_address->floor;
								}
								else
								{
									$floor = null;
								}
							}
						@endphp
						@if($address = $model->address_text)
							<span class="text-gray f12">{{ ad($address) }}</span>
							@if($floor)
								<br>
								<span class="text-gray f12">طبقه: {{ ad($floor) }}</span>
							@endif
						@else
							<span class="ph10 bg-warning f12">{{ trans_safe("cart::status.without_address") }}</span>
						@endif
				</span>
				</p>
			</td>
		</tr>
		<!-- !END Buyer -->

		</tbody>
	</table>
	<!-- !END Seller and Buyer Details -->
</div>


@include($__module->getBladePath('invoice.official_bill'))

<div class="panel b">
	<table class="table table-bordered bg-white">
		<thead>
		<tr class="{{--bg-gray--}}">
			<th class="text-bold text-center">
				{{ trans('cart::carts.row') }}
			</th>
			<th class="text-bold text-center">
				{{ trans('cart::carts.order_description') }}
			</th>
			<th class="text-bold text-center">
				{{ trans('cart::carts.amount') }}
			</th>
			<th class="text-bold text-center">
				{{ trans('cart::carts.price') }}
			</th>
		</tr>
		</thead>
		<tbody>
		@foreach( $model->orders as $order )
			<tr class="">
				@include("cart::invoice.order")
			</tr>
		@endforeach
		@include("cart::invoice.discount")
		@include("cart::invoice.delivery")
		@include("cart::invoice.tax")
		@include("cart::invoice.total")
		@include("cart::invoice.delivery_method")
		@include("cart::invoice.delivery_time")
		@include("cart::invoice.description")
		@include("cart::invoice.factor_request")
		</tbody>
	</table>
</div>

