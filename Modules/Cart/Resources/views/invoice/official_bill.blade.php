@if ($model->officialBillIsNotNeeded())
    @php return @endphp
@endif

<div class="panel b">
	<table class="table table-bordered bg-white mb0 mt0">
		<tr>
			<th colspan="2" class="text-bold">
				{{ $__module->getTrans('official-bill.singular') }}
			</th>
		</tr>

		<tr>
			<th class="text-bold">
				{{ $__module->getTrans('official-bill.company-name') }}
			</th>
			<td>
				{{ $model->getMeta('company_name') }}
			</td>
		</tr>

		<tr>
			<th class="text-bold">
				{{ $__module->getTrans('official-bill.economical-id') }}
			</th>
			<td>
				{{ ad($model->getMeta('economical_id')) }}
			</td>
		</tr>

		<tr>
			<th class="text-bold">
				{{ $__module->getTrans('official-bill.registration-id') }}
			</th>
			<td>
				{{ ad($model->getMeta('registration_id')) }}
			</td>
		</tr>
	</table>
</div>
