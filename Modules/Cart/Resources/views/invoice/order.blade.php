<td class="text-center {{--bg-gray--}}" style="vertical-align: middle; width: 100px;">
	{{ ad($loop->iteration) }}
</td>

<td>
	<div class="font-yekan text-bold m10">
		@php $post = $order->post @endphp
		@if ($post and $post->exists)
			{{ $post->title }}
		@endif

		/

		@php $ware = $order->ware @endphp
		@if ($ware and $ware->exists)
			{{ $ware->title }}
		@endif
	</div>

	<div class="m10">
		<span class="col-md-6 {{--text-darkgray--}}">
			<i class="fa fa-barcode"></i>
			<span class=" f14 text-bold" style="letter-spacing:3px">
				{{ $order->ware->code }}
			</span>
		</span>

		@if(user()->isDeveloper())
			<span class="text-ultralight {{--text-gray--}} pull-right font-tahoma f10">
				Order Id: {{ $order->id }} ; Ware Id: {{ $order->ware->id }}
			</span>
		@endif
	</div>

</td>


<td style="vertical-align: middle">
	{{ ad($order->count) }}
	&nbsp;
	@php $unit = $order->ware->unit @endphp
	@if ($unit)
		{{ $unit->title }}
	@endif
</td>

<td style="vertical-align: middle">
	<div>
		{{ ad(number_format($order->sale_price)) }}
	</div>
</td>
