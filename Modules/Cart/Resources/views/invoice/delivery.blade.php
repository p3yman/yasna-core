@php
	$model->spreadMeta();
@endphp
<tr class="f20">

	<td class="text-center {{--bg-gray--}}" style="vertical-align: middle; width: 100px;">
		<i class="fa fa-truck"></i>
	</td>

	<td colspan="2">
		<div class="pv20">
			{{ trans("cart::couriers.price") }}
			@if($model->courier_id)
				&nbsp;
				{{ trans("manage::forms.general.with") }}
				&nbsp;
				{{ $model->courier->title }}
				@if( $model->courier_discount )
					&nbsp;
					{{ trans("manage::forms.general.and") }}
					&nbsp;
					{{ ad(number_format($model->courier_discount)) }}
					&nbsp;
					{{ trans("shop::currencies.$model->currency")  }}
					&nbsp;
					{{ trans("cart::couriers.discount") }}
					&nbsp;
				@endif
			@endif
		</div>
		{{--<div>
			@if($address = $model->address_text)
				<span class="text-gray f12" >{{ pd($address) }}</span>
			@else
				<span class="ph10 bg-warning f12">{{ trans_safe("cart::status.without_address") }}</span>
			@endif
		</div>--}}
	</td>

	{{--<td style="vertical-align: middle"> - </td>--}}
	<td style="vertical-align: middle">
		{{ ad(number_format($model->delivery_price + $model->heavy_delivery_price ?? 0)) }}
	</td>
</tr>
