@php
	$shipping = model('shipping_method', $model->shipping_method_id);
	if ($shipping->exists)
	{
		$shipping->spreadMeta();
		if (isset($shipping->title))
		{
			$shipping_title = $shipping->title;
		}
		else
		{
			$shipping_title = null;
		}
	}

@endphp
@if($model->delivery_method)
	<tr class="f20">

		<td class="text-center {{--bg-gray--}}" style="vertical-align: middle; width: 100px;">
			<i class="fa fa-arrow-down"></i>
		</td>

		<td colspan="2">
			<div class="pv20">
				{{ trans("cart::carts.delivery.delivery_method") }}
			</div>
		</td>
		<td style="vertical-align: middle">
			@if($shipping_title)
				<h5>{{ $shipping->title }}</h5>
				<hr>
			@endif
			<h5>{{ trans("cart::carts.delivery." . $model->delivery_method) }}</h5>
		</td>
	</tr>
@endif
