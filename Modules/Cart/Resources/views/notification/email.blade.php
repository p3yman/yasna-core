@php
	$user = $model->user;
	$date = $model->isRaised()? $model->raised_at : $model->created_at;
@endphp

@component('mail::message')
	<div style="direction: rtl;text-align: right">
		@component('mail::panel')

		@component('mail::table')
			|{{ trans('cart::carts.invoice_number') }}|{{ $model->code }}|
			|:-:|:-:|
			|{{ trans('cart::carts.order_date') }}|{{ pd(jDate::forge($date)->format('j F Y [H:i]')) }}|
				|{{trans('cart::carts.seller') }} |  {{ get_setting('site_title') }}|
				|{{ trans('cart::carts.telephone')." ".trans('cart::carts.seller') }}|{{$seller_tel}}|
				|{{trans('cart::carts.email')." ".trans('cart::carts.seller')}}|{{$seller_email}}|
				|{{trans('cart::carts.address')." ".trans('cart::carts.seller')}}|{{ get_setting('address') }}|
				|{{  trans('cart::carts.buyer') }} | {{ $user->full_name }}|
				|{{ trans('cart::carts.telephone')." ".trans('cart::carts.buyer') }}|{{ $user->mobile }}|
				|{{trans('cart::carts.email')." ".trans('cart::carts.buyer')}}|{{ $user->email }}|
				|{{trans('cart::carts.address')." ".trans('cart::carts.buyer')}}|{{$buyer_address}}|
				|{{  trans('cart::carts.postal_code')." ".trans('cart::carts.buyer')}}|{{ad($model->postal_code)}}|
		@endcomponent

		@endcomponent

			@component('mail::table')
			|{{ trans('cart::carts.row') }} |{{ trans('cart::carts.order_description') }}| {{ trans('cart::carts.amount') }}| {{ trans('cart::carts.price') }}|
			|:-:|:-:|:-:|:-:|
				@foreach( $model->orders as $order )
					|{{ pd($loop->iteration) }}|{{ ($order->post)?$order->post->title:'' . ' / ' . $order->ware->title." ".$order->ware->code  }}|{{ pd($order->count)." ".$order->ware->unit->title }}|{{ pd(number_format($order->sale_price)) }}|
				@endforeach

			@endcomponent

			@component('mail::table')
				|	{{ trans("cart::carts.discount") }} |{{ pd(number_format($model->added_discount)) }}|
				|:-:|:-:|
				| {{ "**".$delivery_title."**" }} |	{{ "**".pd(number_format($model->delivery_price))."**" }}|
				|{{ "**".trans("cart::carts.tax")."**" }}|{{ "**".pd(number_format($model->tax_amount))."**" }}|
				|{{ "**".trans("cart::carts.overall")."**" }}|{{ "**".pd(number_format($model->invoiced_amount)) }}{{ trans("shop::currencies.$model->currency") ."**" }}|

				@endcomponent
			@endcomponent
	</div>
