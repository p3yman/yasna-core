<div >
	<span style="letter-spacing:1px" class="font-yekan text-bold f20 text-black">
		{{ $model->code }}
	</span>
</div>

@include("manage::widgets.grid-date" , [
	"date" => $model->isRaised()? $model->raised_at : $model->created_at,
])


