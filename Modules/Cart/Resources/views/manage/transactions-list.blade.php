{{widget('modal')->label(trans('cart::carts.transactions_list'))}}

<div class="modal-body">
	@include("manage::widgets.grid" , [
		'table_id' => "tblTransactions" ,
		'row_view' => "cart::manage.transactions-list-row" ,
		'handle' => "counter",
		'operation_heading' => false ,
		'headings' => [
			trans('payment::general.tracking-number'),
			trans("payment::general.date.created") ,
			trans("payment::general.transaction.status") ,
			trans("payment::general.amount.singular") ,
			trans('payment::general.type'),
			trans('payment::general.bank.title.singular'),
			trans("payment::general.reference-number") ,
		] ,
	]     )
</div>
<div class="modal-footer pv-lg">

	{!! widget('button')->id('cancel-btn')->label(trans('cart::carts.close'))->class(' btn btn-default ')->onClick('$(".modal").modal("hide")')!!}

</div>