@php
    $status = $model->status;
@endphp

@include("manage::widgets.grid-badge" , [
	'text' => $model->status_text ,
	'color' => $model->status_color ,
	'icon' => $model->status_icon ,
//	'link' => $model->cannot('process') ? '' : "modal:manage/carts/act/-hashid-/process" ,
]     )
