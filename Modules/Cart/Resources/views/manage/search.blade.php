@extends('manage::layouts.template')

@section('content')
	@include("cart::manage.tabs")

	<div class="panel panel-default w50 p20 margin-auto mv40">

	    <?php

	    widget("form-open")
		    ->target(url("manage/carts/browse/search/search"))
		    ->method('get')
		    ->render()
	    ;

	    widget('input')
		    ->name('keyword')
		    ->label("tr:manage::forms.button.search")
//		    ->inForm()
		    ->render()
	    ;

	    widget('group')
		    ->start()
		    ->render()
	    ;

	    widget('button')
		    ->type('submit')
		    ->shape('primary')
		    ->label("tr:manage::forms.button.search")
		    ->render()
	    ;

	    widget('group')
		    ->stop()
		    ->render()
	    ;

	    widget("form-close")
		    ->render();

	    ?>


	</div>

@endsection
