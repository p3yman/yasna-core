{!!
widget("combo")
	->name('type')
	->id('cmbType')
	->options( $model->record->typesCombo() )
	->inForm()
	->label('tr:cart::records.new')
	->blankValue('')
	->blankCaption('tr:cart::records.new_hint')
	->onChange('inlineProcessAction()')
!!}

{{--
|--------------------------------------------------------------------------
| Hidden
|--------------------------------------------------------------------------
|
--}}
{!!
widget("hidden")
	->name('hashid')
	->value($model->hashid)
!!}

{{--
|--------------------------------------------------------------------------
| Status Combo
|--------------------------------------------------------------------------
|
--}}
{!!
widget("combo")
	->name('new_status')
	->id('cmbStatus')
	->containerClass('-process')
	->options( $model->statusCombo() )
	->inForm()
	->label('tr:cart::status.new')
	->blankValue('')
	->hidden()
	->required()
!!}


{{--
|--------------------------------------------------------------------------
| Payment Status Combo
|--------------------------------------------------------------------------
|
--}}
{!!
widget("combo")
	->name('payment_status')
	->id('cmbPaymentStatus')
	->containerClass('-process')
	->options( $model->paymentStatusCombo() )
	->inForm()
	->label('tr:cart::status.payment')
	->value( $model->payment_status )
	->hidden()
	->required()
!!}


{{--
|--------------------------------------------------------------------------
| Invoice Discount
|--------------------------------------------------------------------------
|
--}}
{!!
widget("input")
	->disabled()
	->inForm()
	->id('txtInvoiced')
	->containerClass('-process')
	->label("tr:cart::carts.overall_price")
	->value($model->invoiced_amount)
	->addon( $model->currency()->title )
	->numberFormat()
	->hidden()
!!}

{!!
widget("input")
	->name('added_discount')
	->inForm()
	->id('txtDiscount')
	->containerClass('-process')
	->label('tr:cart::carts.discount')
	->value( $model->added_discount )
	->addon( $model->currency()->title )
	->numberFormat()
	->hidden()
	->required()
!!}


{{--
|--------------------------------------------------------------------------
| Remarks
|--------------------------------------------------------------------------
|
--}}
{!!
widget("textarea")
	->name('remarks')
//	->containerClass('-process')
	->id("txtRemarks")
	->inForm()
	->label('tr:cart::records.remarks')
	->hidden()
!!}


{{--
|--------------------------------------------------------------------------
| Script
|--------------------------------------------------------------------------
|
--}}
<script>
	function inlineProcessAction() {
		$type = $("#cmbType");
		$status = $("#cmbStatus-container");
		$payment = $("#cmbPaymentStatus-container");
		$invoiced = $("#txtInvoiced-container");
		$discount = $("#txtDiscount-container");
		$remarks = $("#txtRemarks-container");

		$(".-process").hide();
		$remarks.slideDown('fast');

		switch ($type.val()) {
			case 'status' :
				$status.slideDown('fast');
				break;

			case 'checkout' :
			case 'confirm' :
			case 'undo' :
			case 'remark' :
				break;

			case 'payment' :
				$payment.slideDown('fast');
				break;

			case 'discount' :
				$discount.slideDown('fast').focus();
				$invoiced.slideDown('fast').focus();
				break;

			default:
				$remarks.slideUp('fast');

		}
	}
</script>