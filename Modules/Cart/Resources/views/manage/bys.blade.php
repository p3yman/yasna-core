<div class="w80 ltr">
	@foreach($model->bys() as $key => $value)
		<div class="row text-{{ $value? 'green' : 'gray' }}">

			<div class="col-md-2">
				{{ $key }}
			</div>

			<div class="colo-md-6">
				{{ $value or '-' }}
			</div>
		</div>
	@endforeach

</div>
