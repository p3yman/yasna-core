<td>
	{{ ad($loop->iteration) }}
</td>

<td>
	{{ ad($model->tracking_no) }}
	@include("manage::widgets.grid-text" , [
		'condition' => user()->isDeveloper(),
		'text' => $model->id . ' | ' . $model->hashid,
		'color' => "primary",
	])
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->status_date ,
		'default' => "fixed" ,
		'format' => 'j F Y' ,
		'color' => '#1b72e2',
	])
</td>

<td>
	@include("manage::widgets.grid-badge" , [
		'icon' => $model->status_icon,
		'text' => $model->status_text,
		'color' => $model->status_color,
	])
</td>

<td>
	{{ ad(number_format($model->amount)) }}
	{{ $model->account->currency_text }}
</td>
<td>
	{{ $model->type_text }}
</td>
<td>
	{{ $model->account->name }}
</td>

<td>
	{{ ad($model->bank_reference_no) }}
</td>