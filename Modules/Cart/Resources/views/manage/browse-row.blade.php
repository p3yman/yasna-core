@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
	'refresh_url' => "manage/carts/update/$model->hashid",
])

<td>
	@include("cart::manage.browse-row-properties")
</td>

<td>
	@include("cart::manage.browse-row-cart-process")
</td>

<td>
	@include("cart::manage.browse-row-prices")
</td>

<td>
	@include("cart::manage.browse-row-status")
</td>

<td>
	@include("cart::manage.browse-row-payment")
</td>

@include("cart::manage.browse-row-actions")
