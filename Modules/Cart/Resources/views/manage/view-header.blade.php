<div class="row">
	<div class="col-md-9">


		<div class="font-yekan text-bold  f18">
			{{ trans("cart::carts.singular") }}
			&nbsp;
			{{ $model->user->full_name }}
		</div>


		<div class="mv5">

		<span class="text-{{ $model->status_color }} f12">
			{{ $model->status_text }}
		</span>

			<span class="text-darkgray mh5 f12">
			@if($model->isRaised())
					(
					{{ trans("cart::carts.raised_at") }}
					:
					&nbsp
					{{ pd(jdate($model->raised_at)->format('Y/m/j . H:i')) }}
					)
				@endif
		</span>

		</div>
	</div>


	<div class="col-md-3 f16 text-bold mv5" style="letter-spacing:3px ; text-align: left">
		{{ $model->code }}
	</div>
</div>