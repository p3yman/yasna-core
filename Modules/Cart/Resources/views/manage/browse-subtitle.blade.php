@if(request()->user and ($user = user(request()->user))->exists)
    <p class="title f16 mv5 mh20">
        ({{ $user->full_name }})
    </p>
@endif
