@extends('manage::layouts.template')

@section('content')
	@include("cart::manage.tabs")


	{{--
	|--------------------------------------------------------------------------
	| Toolbar
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::widgets.toolbar" , [
	   "subtitle_view" => "cart::manage.browse-subtitle",
	]     )


	{{--
	|--------------------------------------------------------------------------
	| Grid
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::widgets.grid" , [
		'table_id' => "tblCarts" ,
		'row_view' => "cart::manage.browse-row" ,
		'handle' => "selector" ,
		'operation_heading' => true ,
		'headings' => [
			trans("validation.attributes.properties") ,
			'',
			trans("shop::prices.invoice"),
			trans("validation.attributes.status"),
			trans("cart::status.payment"),
		] ,
	]     )

@endsection
