{!!
	widget("button")
		->label(ad(trans("cart::process.title") . " (" . $model->total_items . " " .trans("cart::orders.items") . ") "))
		->target("modal:manage/carts/process/$model->hashid")
		->condition($model->can('process'))
!!}


@include("manage::widgets.grid-tiny" , [
	'text' => $model->id . " | " . $model->hashid,
	'locale' => "en" ,
	'icon' => "bug" ,
	"condition" => dev(),
]     )
