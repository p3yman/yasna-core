@include("manage::widgets.grid-badge" , [
	'text' => $model->payment_status_text ,
	'color' => $model->payment_status_color ,
	'icon' => $model->payment_status_icon ,
	'link' => $model->cannot('process') ? '' : "modal:manage/carts/-id-/transactions/" ,
]     )
