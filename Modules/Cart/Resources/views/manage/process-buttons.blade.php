<?php

widget("separator")
	->render();

widget("button")
	->id('cmdRecordSubmit')
	->type('submit')
	->shape('primary')
	->hidden()
	->label('tr:manage::forms.button.save')
	->render()
;

widget('button')
	->id('cmdRecordNew')
	->shape('default')
	->label('tr:cart::records.new_button')
	->onClick("inlineProcessToggle()")
	->render()
;

widget("button")
	->id('cmdCancelModal')
	->shape('link')
	->label('tr:manage::forms.button.cancel')
	->target('$(".modal").modal("hide")')
	->render()
;

widget("button")
	->hidden()
	->id('cmdCancelNew')
	->shape('link')
	->label('tr:manage::forms.button.cancel')
	->onClick("inlineProcessToggle('browse')")
	->render()
;

widget("feed")
	->render();
?>


<script>
	function inlineProcessToggle(mode = 'new') {

		let $save_button = $("#cmdRecordSubmit");
		let $toggle_button = $('#cmdRecordNew');
		let $panel_new = $("#divRecordsNew");
		let $panel_browse = $("#divRecordsBrowse");
		let $cancel_new = $("#cmdCancelNew");
		let $cancel_modal = $("#cmdCancelModal");

		switch (mode) {
			case 'new' :
				$panel_browse.slideUp('fast');
				$panel_new.slideDown('fast');
				$toggle_button.hide();
				$save_button.show();
				$cancel_modal.hide();
				$cancel_new.show();
				break;

			case 'browse' :
				$panel_browse.slideDown('fast');
				$panel_new.slideUp('fast');
				$toggle_button.show();
				$save_button.hide();
				$cancel_modal.show();
				$cancel_new.hide();
				break;

		}

	}
</script>
