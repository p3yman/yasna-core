<div>
	<div id="accordion" role="tablist" aria-multiselectable="true" class="panel-group">
		@foreach($records = $model->records as $record)
			@include("cart::manage.process-record")
		@endforeach
	</div>
</div>
@if($records->count() == 0)
	<div class="null text-center">
		{{ trans_safe("cart::records.nothing_yet") }}
		<div class="mv10 text-gray">
			<button type="button" class="btn btn-lg btn-default" onclick="$('#cmdRecordNew').click()">
				{{ trans_safe("cart::records.new_button") }}
			</button>
		</div>
	</div>
@endif
