{!!
widget("modal")
	->label('tr:cart::carts.view')
	->blade('cart::manage.view-header')
	->model( $model )
!!}

<div class="modal-body">
	@include("cart::invoice.index")
</div>