{!!
widget("modal")
	->label('tr:cart::carts.view')
	->blade('cart::manage.view-header')
	->model( $model )
	->target('name:cart-save-process')
	->validation(false)
!!}

<div class="modal-body">
	<div id="divRecordsNew" class="noDisplay">
		@include("cart::manage.process-new")
	</div>
	<div id="divRecordsBrowse">
		@include("cart::manage.process-browse")
	</div>
	@include("cart::manage.process-buttons")
</div>

