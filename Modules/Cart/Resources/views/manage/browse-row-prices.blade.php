<div>
	<span style="letter-spacing:1px" class="f16 text-black">
		{{ ad(number_format( $model->invoiced_amount )) }}
	</span>
	<span class="mh10 f14">
		{{ $model->currency() }}
	</span>
</div>

{{--show debt amount--}}
@if(($model->invoiced_amount-$model->paid_amount)>0)
	@include("manage::widgets.grid-badge" , [
		'text' => trans('cart::carts.paid_amount')." : ".ad(number_format($model->paid_amount)) ." ". $model->currency() ,
		'color' => "success" ,
		'icon' => 'dollar' ,
	]     )


	@include("manage::widgets.grid-badge" , [
		'text' => trans('cart::carts.debt_amount')." : ". ad(number_format($model->invoiced_amount-$model->paid_amount ))." ". $model->currency() ,
		'color' => "danger" ,
		'icon' => 'exclamation-triangle' ,
	]     )
@endif

@include("manage::widgets.grid-text" , [
	'text' => trans("cart::carts.order_price") . ': ' . number_format($model->sale_price) ,
	'size' => "12" ,
	'color' => "gray" ,
	'class' => "text-light" ,
	'div_class' => "mv10" ,
]     )

