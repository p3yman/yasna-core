<div class="panel panel-default">

	<div id="record-{{$record->hashid}}" role="tab" class="panel-heading">
		<div class="panel-title">

			<a data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $record->hashid }}" class="collapsed"
			   aria-expanded="true" aria-controls="collapse-{{ $record->hashid }}">
				<span class="text-black f14">
					{{ $record->title }}
				</span>
				<div class="pull-right text-gray f10 mv5">
					{{ $record->relative_time }}
				</div>
			</a>
		</div>

	</div>


	{{-- panel accordion could be open on page load by adding "in" class to ".tabpanel" div --}}
	<div id="collapse-{{$record->hashid}}" role="tabpanel" aria-labelledby="record-{{$record->hashid}}"
		 class="panel-collapse collapse {{ $loop->first? 'in' : '' }}">
		<div class="panel-body">

			@include("manage::widgets.grid-text" , [
				'text' => $record->remarks,
				'size' => "12" ,
				'color' => "almostBlack" ,
				'class' => "text-light" ,
			]     )

			@include("manage::widgets.grid-date" , [
				'date' => $record->created_at ,
				'text' => trans("manage::forms.general.by") . SPACE . $record->creator->full_name ,
			]     )

		</div>
	</div>

</div>