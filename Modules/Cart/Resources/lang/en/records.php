<?php
return [
     'new'         => "New Record",
     'new_hint'    => ". . . Choose from the list . . .",
     'remarks'     => "Remarks",
     'new_button'  => "New Record",
     'nothing_yet' => "This order has no records yet.",

     'validation' => [
          'type'               => "Choose the new record type from the list.",
          'status'             => "Choose the status from the list.",
          "status_unchanged"   => "The chosen status is similar to the current cart status.",
          'discount_required'  => "Enter discount price.",
          'discount_max'       => "The discount cannot be more than the total invoice price.",
          'discount_unchanged' => "The new discount price is similar to the current invoice discount.",
          'remarks'            => "Write your remarks.",
     ],

     'message' => [
          'status_not_applicable' => "Status is not applicable, according to the current order status.",
          'cart_not_raisable'     => "The Cart cannot be raised according to the changes.",
     ],

     'type' => [
          'status'   => "Change Status",
          'remark'   => "Remark",
          'remarks'  => "Remarks",
          'edit'     => "Edit",
          'payment'  => "Manage Payment",
          'discount' => "Discount",
          'checkout' => "Checkout",
          'undo'     => "Checkout Revert",
          'confirm'  => "Confirm",
     ],

     'type-title' => [
          "confirm"      => "Confirm",
          'status'       => "Change Status",
          'remark'       => "Remark",
          'remarks'      => "Remarks",
          'payment'      => "Payment Status",
          'discount'     => "Discount",
          "checkoutUndo" => "Checkout Revert",
          "checkout"     => "Checkout",
     ],
];
