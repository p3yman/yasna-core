<?php
return [
     "order-finder" => [
          "title"            => "Cart Finder",
          "enter_order_code" => "Enter order code",
          "find"             => "Find!",
          "name"             => "Client Name",
          "order_date"       => "Order Date",
          "payment_status"   => "Payment Status",
          "order_status"     => "Order Status",
          "action"           => "Action",
          "by"               => "By",
     ],

];
