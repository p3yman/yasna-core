<?php
return [
     'do-you-need'     => 'Do you need official invoice?',
     'plural'          => 'Official Invoices',
     'singular'        => 'Official Invoice',
     'company-name'    => 'Company Name',
     'economical-id'   => 'Economic Identifier',
     'registration-id' => 'National Identifier / Registration Identifier',
];
