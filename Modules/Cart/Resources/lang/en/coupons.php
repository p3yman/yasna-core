<?php
return [
     "rules" => [
          "first-purchases" => "Limited to a Certain Number of First Purchases",
          "specific-wares"  => "Specific Wares",
          "min-purchase"    => "Minimum Purchase History",
     ],

     "fields" => [
          "number"            => "Number",
          "number-hint"       => "Specify the number of first purchases, supposed to benefit the discount.",
          "amount"            => "Amount",
          "min-purchase-hint" => "Specify the minimum purchase history of the user, to benefit the discount.",
     ],

     "order-category" => [
          "title"            => "set discount in depend on category of orders",
          "whitelist"        => "Whitelist Categories",
          "blacklist"        => "Blacklist Categories",
          "black-list-check" => "order that not into those categories",
     ],

     "order-tag" => [
          "title"            => "set discount in depend on tag of orders",
          "whitelist"        => "Whitelist Tags",
          "blacklist"        => "Blacklist Tags",
          "black-list-check" => "order that not into those tags",
     ],
];
