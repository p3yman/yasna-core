<?php
return [
     'pending'                    => "Pending",
     'under_packing'              => "Under Packing",
     'support_request'            => "Request Support",
     'under_delivery'             => "Under Delivery",
     'delivered'                  => "Delivered",
     'under_payment'              => "Under Payment",
     'under_payment_confirmation' => "Under Payment Confirmation",
     'not_raised'                 => "Not Raised",
     'archive'                    => "Archive",
     'closed'                     => "Closed",
     'paid'                       => "Paid",
     'unknown'                    => "Unknown",
     'search'                     => "Search",


     'new'             => "New Status",
     'payment'         => "Payment Status",
     "without_address" => "Without Address!",
];
