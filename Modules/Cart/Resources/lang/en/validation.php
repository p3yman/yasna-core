<?php
return [
     "ware_is_not_purchasable"   => "The ware \":title\" is not purchasable.",
     "ware_inventory_not_enough" => "The ware \":title\" has not enough inventory.",


     "attributes" => [
          "each_item_count"    => "Count of Each Item",
          "one_of_items"       => "One of the Items",
          "address"            => "Address",
          "shipping_method_id" => "Shipping Method",
          "coupons_codes"      => "Coupon Codes",
     ],
];
