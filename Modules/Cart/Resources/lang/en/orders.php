<?php
return [
     "title"             => "Item",
     'titles'            => "Orders",
     'including_x_items' => "Including :x Item(s)",
     "items_count"       => "Items Count",
     "items"             => "item(s)",
];
