<?php
return [
     "allowed"     => "White List",
     "not_allowed" => "Black List",

     "tag_rule" => "Tag",
     "tag_hint" => "If the shipping method should or should not be accessible for some tags, specify them on white or black list.",

     "category_rule" => "Category",
     "category_hint" => "If the shipping method should or should not be accessible for a specified category, select them from Allowed/Not Allowed fields.",

     "product_title" => "Product",
     "product_hint"  => "If this shipping method is available just for a set of products, or it's shouldn't be used by another set, please specify them on Allowed or Not Allowed fields.",

     "special_offer_rule" => "Special Offer",
     "special_offer_hint" => "If the shipping method should be available for a set of special offers, or it should not accessible for other one, specify them on the top fields.",
];
