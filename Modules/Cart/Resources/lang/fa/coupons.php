<?php
return [
     "rules" => [
          "first-purchases" => "محدود به چند خرید نخست",
          "specific-wares"  => "کالاهای خاص",
          "min-purchase"    => "حداقل سابقه خرید",
     ],

     "fields" => [
          "number"            => "تعداد",
          "number-hint"       => "تخفیف قرار است برای چند خرید اول کاربر مهیا باشد؟ این تعداد را مشخص کنید.",
          "amount"            => "مقدار",
          "min-purchase-hint" => "سابقهٔ خرید مشتری از فروشگاه حداقل چقدر باشد تا بتواند از این تخفیف استفاده کند؟ این مقدار را وارد کنید.",
     ],

     "order-category" => [
          "title"            => "اعمال یا ممنوعیت تخفیف روی دسته‌بندی‌های خاصی از کالا",
          "whitelist"        => "دسته‌بندی‌های مجاز",
          "blacklist"        => "دسته‌بندی‌های غیرمجاز",
          "black-list-check" => "کالایی که در این دسته‌ها قرار ندارند",
     ],

     "order-tag" => [
          "title"            => "اعمال یا ممنوعیت تخفیف روی تگ‌های خاصی از کالا",
          "whitelist"        => "تگ‌های مجاز",
          "blacklist"        => "تگ‌های غیرمجاز",
          "black-list-check" => "کالایی که این تگ‌ها را ندارد",
     ],
];
