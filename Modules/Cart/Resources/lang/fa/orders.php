<?php
return [
     "title"             => "قلم",
     'titles'            => "سفارش‌ها",
     'including_x_items' => "شامل :x قلم",
     "items_count"       => "تعداد اقلام",
     "items"             => "قلم",
];
