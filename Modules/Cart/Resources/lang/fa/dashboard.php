<?php
return [
     "order-finder" => [
          "title"            => "سفارش‌یاب",
          "enter_order_code" => "کد سفارش را وارد کنید",
          "find"             => "بیاب!",
          "name"             => "نام سفارش‌دهنده",
          "order_date"       => "تاریخ سفارش",
          "payment_status"   => "وضعیت پرداخت",
          "order_status"     => "وضعیت سفارش",
          "action"           => "عملیات",
          "by"               => "توسط",

     ],

];
