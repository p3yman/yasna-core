<?php
return [
     "ware_is_not_purchasable"   => "کالای «:title» قابلیت خرید ندارد.",
     "ware_inventory_not_enough" => "کالای «:title» موجودی کافی ندارد.",


     "attributes" => [
          "each_item_count"    => "تعداد هر کالا",
          "one_of_items"       => "یکی از موارد",
          "address"            => "نشانی",
          "shipping_method_id" => "روش حمل",
          "coupons_codes"      => "کدهای تخفیف",
     ],
];
