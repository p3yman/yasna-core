<?php
return [
     'do-you-need'     => 'فاکتور رسمی نیاز دارید؟',
     'plural'          => 'فاکتورهای رسمی',
     'singular'        => 'فاکتور رسمی',
     'company-name'    => 'نام شرکت',
     'economical-id'   => 'شماره‌ی اقتصادی',
     'registration-id' => 'شناسه‌ی ملی / شماره‌ی ثبت',
];
