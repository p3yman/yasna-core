<?php
return [
     'pending'                    => "منتظر تأیید",
     'under_packing'              => "در حال بسته‌بندی",
     'support_request'            => "درخواست پشتیبانی",
     'under_delivery'             => "در حال ارسال",
     'delivered'                  => "تحویل‌شده",
     'under_payment'              => "در انتظار پرداخت",
     'under_payment_confirmation' => "در انتظار تأیید پرداخت",
     'not_raised'                 => "در حال تکمیل سفارش",
     'archive'                    => "بایگانی",
     'closed'                     => "بایگانی",
     'paid'                       => "پرداخت‌شده",
     'unknown'                    => "نامشخص",
     'search'                     => "جست‌وجو",


     'new'             => "وضعیت جدید",
     'payment'         => "وضعیت پرداخت",
     "without_address" => "بدون آدرس!",
];
