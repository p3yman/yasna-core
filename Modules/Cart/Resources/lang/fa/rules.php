<?php
return [
     "allowed"     => "مجاز",
     "not_allowed" => "غیرمجاز",

     "tag_rule" => "تگ",
     "tag_hint" => "اگر روش حمل می‌بایست برای دسته‌ی خاصی از تگ‌ها در دسترس باشد/نباشد، آنها را در کادرهای مجاز یا غیرمجاز مشخص نمائید.",

     "category_rule" => "دسته‌بندی",
     "category_hint" => "اگر این روش حمل باید برای دسته‌بندی خاصی در دسترس باشد/نباشد، آنها را در لیست‌های مجاز یا غیرمجاز مشخص نمایید.",

     "product_title" => "محصول",
     "product_hint"  => "اگر روش حمل برا تنها دسته‌ای از محصولات در دسترس است و یا این روش حمل برای دسته‌ای از محصولات مناسب نیست، آن محصولات را در لیست مجاز یا غیرمجاز مشخص نمایید.",

     "special_offer_rule" => "پیشنهاد ويژه",
     "special_offer_hint" => "اگر روش حمل تنها در یک دسته از پیشنهادات ويژه باید در دسترس باشد و یا برای دسته‌ای از پیشنهادات ويژه مناسب نیست، آنها را در کادرهای بالا مشخص نمایید.",
];
