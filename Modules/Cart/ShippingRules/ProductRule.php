<?php

namespace Modules\Cart\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;


class ProductRule extends ShippingRule
{
    private $cart_products = [];



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("cart::rules.product_title");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("products_allowed")
             ->label("tr:cart::rules.allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add("selectize")
             ->name("products_not_allowed")
             ->label("tr:cart::rules.not_allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("cart::rules.product_hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->hasProductsAllowed() and $this->hasNotProductsNotAllowed();
    }



    /**
     * Return a combo ready-to-use array
     *
     * @return array
     */
    private static function combo(): array
    {
        $posttypes = posttypes()->having('shop')->pluck('slug');
        $posts     = post()->whereIn('type', $posttypes)
                           ->where([
                                'locale' => getLocale(),
                           ])
                           ->get()
                           ->map(
                                function ($post) {
                                    return [
                                         'value'   => $post->id,
                                         'caption' => $post->title,
                                    ];
                                }
                           )
        ;

        return $posts->toArray();
    }



    /**
     * Check some products of cart are on white list or not
     *
     * @return bool
     */
    private function hasProductsAllowed(): bool
    {
        $products = $this->getCartProducts();
        $allowed  = $this->getProductsAllowed();

        if (empty($products) || empty($allowed)) {
            return true;
        }

        return $this->hasIntersection($products, $allowed);
    }



    /**
     * Check the cart has some products which are on black list or not
     *
     * @return bool
     */
    private function hasProductsNotAllowed(): bool
    {
        $products    = $this->getCartProducts();
        $not_allowed = $this->getProductsNotAllowed();

        if (empty($products) || empty($not_allowed)) {
            return false;
        }

        return $this->hasIntersection($products, $not_allowed);
    }



    /**
     * Ensure that the cart has not any products which is on the black list
     *
     * @return bool
     */
    private function hasNotProductsNotAllowed(): bool
    {
        return !$this->hasProductsNotAllowed();
    }



    /**
     * Check array $a has an intersection with array $b or not
     *
     * @param array $a
     * @param array $b
     *
     * @return bool
     */
    private function hasIntersection(array $a, array $b): bool
    {
        return array_intersect($a, $b) != [];
    }



    /**
     * Return a ready-to-use array of allowed products
     *
     * @return array
     */
    private function getProductsAllowed(): array
    {
        return explode_not_empty(',', $this->para['products_allowed']);
    }



    /**
     * Return a ready-to-use array of not allowed products
     *
     * @return array
     */
    private function getProductsNotAllowed(): array
    {
        return explode_not_empty(',', $this->para['products_not_allowed']);
    }



    /**
     * Getter of $cart_products
     *
     * @return array
     */
    private function getCartProducts(): array
    {
        $products = $this->cart_products;
        if (!empty($products)) {
            return $products;
        }

        foreach ($this->invoice->getItems() as $order_packet) {
            if (isset($order_packet['order'], $order_packet['order']->post)) {
                $products = array_merge(
                     $products,
                     [$order_packet['order']->post->id]
                );
            }
        }

        $this->setCartProducts($products);

        return $products;
    }



    /**
     * Setter of $cart_products
     *
     * @param array $products
     *
     * @return ProductRule
     */
    private function setCartProducts(array $products): ProductRule
    {
        $this->cart_products = $products;

        return $this;
    }
}
