<?php

namespace Modules\Cart\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;


class SpecialSalesRule extends ShippingRule
{
    private $cart_offers = [];



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("cart::rules.special_offer_rule");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("offers_allowed")
             ->label("tr:cart::rules.allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add("selectize")
             ->name("offers_not_allowed")
             ->label("tr:cart::rules.not_allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("cart::rules.special_offer_hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->hasOffersAllowed() and $this->hasNotOffersNotAllowed();
    }



    /**
     * Return a combo ready-to-use array
     *
     * @return array
     */
    private static function combo(): array
    {
        $valid_offers = model('shop-offer')
             ->whereValid()
             ->get(['id', 'title'])
             ->map(function ($offer) {
                 return [
                      "value"   => $offer->id,
                      "caption" => $offer->title,
                 ];
             })
        ;

        return $valid_offers->toArray();
    }



    /**
     * Check some offers applied on the cart are on white list or not
     *
     * @return bool
     */
    private function hasOffersAllowed(): bool
    {
        $offers  = $this->getCartOffers();
        $allowed = $this->getOffersAllowed();

        if (empty($offers) || empty($allowed)) {
            return true;
        }

        return $this->hasIntersection($offers, $allowed);
    }



    /**
     * Check the cart has some applied offers on it which are on black list or
     * not
     *
     * @return bool
     */
    private function hasOffersNotAllowed(): bool
    {
        $offers      = $this->getCartOffers();
        $not_allowed = $this->getOffersNotAllowed();

        if (empty($offers) || empty($not_allowed)) {
            return false;
        }

        return $this->hasIntersection($offers, $not_allowed);
    }



    /**
     * Ensure that the cart has not any applied offer which is on the black list
     *
     * @return bool
     */
    private function hasNotOffersNotAllowed(): bool
    {
        return !$this->hasOffersNotAllowed();
    }



    /**
     * Check array $a has an intersection with array $b or not
     *
     * @param array $a
     * @param array $b
     *
     * @return bool
     */
    private function hasIntersection(array $a, array $b): bool
    {
        return array_intersect($a, $b) != [];
    }



    /**
     * Return a ready-to-use array of allowed offers
     *
     * @return array
     */
    private function getOffersAllowed(): array
    {
        return explode_not_empty(',', $this->para['offers_allowed']);
    }



    /**
     * Return a ready-to-use array of not allowed offers
     *
     * @return array
     */
    private function getOffersNotAllowed(): array
    {
        return explode_not_empty(',', $this->para['offers_not_allowed']);
    }



    /**
     * Getter for $cart_offers
     *
     * @return array
     */
    private function getCartOffers(): array
    {
        $offers = $this->cart_offers;
        if (!empty($offers)) {
            return $offers;
        }

        $wares  = $this->getCartWares();
        $offers = $this->getOffersOfWares($wares);

        $this->setCartOffers($offers);

        return $offers;
    }



    /**
     * Setter for $cart_offers
     *
     * @param array $offers
     *
     * @return SpecialSalesRule
     */
    private function setCartOffers(array $offers): SpecialSalesRule
    {
        $this->cart_offers = $offers;

        return $this;
    }



    /**
     * Returns a list of cart wares ID
     *
     * @return array
     */
    private function getCartWares(): array
    {
        $wares = [];

        foreach ($this->invoice->getItems() as $offer_packet) {
            $wares[] = $offer_packet['ware_id'];
        }

        return $wares;
    }



    /**
     * Get an array of ware IDs and return an array of IDs of offers
     *
     * @param array $wares
     *
     * @return array
     */
    private function getOffersOfWares(array $wares): array
    {
        $offers = model('shop-offer')
             ->whereValid()
             ->whereHas('wares', function ($query) use ($wares) {
                 $query->whereIn('shop_offer_ware.ware_id', $wares);
             })
             ->get(['id'])
        ;

        return $offers->pluck('id')
                      ->toArray()
             ;
    }
}
