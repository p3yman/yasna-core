<?php

namespace Modules\Cart\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;

/**
 * Class TagRule adds ability of defining constraints on tags of a cart. If the
 * cart has some tags on white list (called allowed) or has not any tag on the
 * black list (called not allowed) the cart can use a shipping method,
 * otherwise it's not allowed.
 *
 * @package Modules\Cart\ShippingRules
 */
class TagRule extends ShippingRule
{
    private static $all_tags  = [];
    private        $cart_tags = [];


    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("cart::rules.tag_rule");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("tags_allowed")
             ->label("tr:cart::rules.allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add("selectize")
             ->name("tags_not_allowed")
             ->label("tr:cart::rules.not_allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("cart::rules.tag_hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->hasTagsAllowed() and $this->hasNotTagsNotAllowed();
    }



    /**
     * Return a combo ready-to-use array
     *
     * @return array
     */
    private static function combo(): array
    {
        if (!empty(static::$all_tags)) {
            return static::$all_tags;
        }

        $posttypes = model('posttype')->whereHas('features', function ($query) {
            $query->where('slug', 'shop');
        })->get(['id'])->pluck('id')->toArray()
        ;

        $results = model('tag')->whereIn('posttype_id', $posttypes)
                               ->get()
                               ->map(
                                    function ($item) {
                                        return [
                                             'value'   => $item->id,
                                             'caption' => nl2br(htmlspecialchars($item->slug)),
                                        ];
                                    }
                               )
                               ->toArray()
        ;

        static::$all_tags = $results;

        return $results;
    }



    /**
     * Check some tags of cart are on white list or not
     *
     * @return bool
     */
    private function hasTagsAllowed(): bool
    {
        $tags    = $this->getEvaluatedCartTags();
        $allowed = $this->getTagsAllowed();

        if (empty($tags)) {
            return true;
        }

        return $this->hasIntersection($tags, $allowed);
    }



    /**
     * Check the cart has some tags which are on black list or not
     *
     * @return bool
     */
    private function hasTagsNotAllowed(): bool
    {
        $tags        = $this->getEvaluatedCartTags();
        $not_allowed = $this->getTagsNotAllowed();

        if (empty($tags)) {
            return false;
        }

        return $this->hasIntersection($tags, $not_allowed);
    }



    /**
     * Ensure that the cart has not any tag which is on the black list
     *
     * @return bool
     */
    private function hasNotTagsNotAllowed(): bool
    {
        return !$this->hasTagsNotAllowed();
    }



    /**
     * Check array $a has an intersection with array $b or not
     *
     * @param array $a
     * @param array $b
     *
     * @return bool
     */
    private function hasIntersection(array $a, array $b): bool
    {
        return array_intersect($a, $b) != [];
    }



    /**
     * Return a ready-to-use array of allowed tags
     *
     * @return array
     */
    private function getTagsAllowed(): array
    {
        return explode_not_empty(',', $this->para['tags_allowed']);
    }



    /**
     * Return a ready-to-use array of not allowed tags
     *
     * @return array
     */
    private function getTagsNotAllowed(): array
    {
        return explode_not_empty(',', $this->para['tags_not_allowed']);
    }



    /**
     * Get all tags of the cart
     *
     * @return array
     */
    private function getEvaluatedCartTags(): array
    {
        $tags = $this->getCartTags();
        if (!empty($tags)) {
            return $tags;
        }

        foreach ($this->invoice->getItems() as $id => $order) {
            $order_tags = $order['order']->post->tags->pluck('id')->toArray();
            $tags = array_merge(
                 $tags,
                 $order_tags
            );
        }

        $this->setCartTags($tags);

        return $tags;
    }



    /**
     * Getter for $cart_tags
     *
     * @return array
     */
    private function getCartTags(): array
    {
        return $this->cart_tags;
    }



    /**
     * Setter for $cart_tags
     *
     * @param array $tags
     *
     * @return TagRule
     */
    private function setCartTags(array $tags): TagRule
    {
        $this->cart_tags = $tags;

        return $this;
    }
}
