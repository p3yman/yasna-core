<?php

namespace Modules\Cart\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;

/**
 * Class CategoryRule add ability of setting constraints on a shipping method
 * based on categories of products
 *
 * @package Modules\Cart\ShippingRules
 */
class CategoryRule extends ShippingRule
{
    private $cart_categories = [];



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("cart::rules.category_rule");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("categories_allowed")
             ->label("tr:cart::rules.allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add("selectize")
             ->name("categories_not_allowed")
             ->label("tr:cart::rules.not_allowed")
             ->inForm()
             ->options(static::combo())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add('note')
             ->shape("default")
             ->label("tr:cart::rules.category_hint")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->hasCategoriesAllowed() and $this->hasNotCategoriesNotAllowed();
    }



    /**
     * Return a combo ready-to-use array
     *
     * @return array
     */
    private static function combo(): array
    {
        $posttypes = model('posttype')->whereHas(
             'features',
             function ($query) {
                 $query->where('slug', 'shop');
             }
        )
                                      ->get(['id'])
                                      ->pluck('id')
                                      ->toArray()
        ;

        $res = model('category')
             ->whereIn('posttype_id', $posttypes)
             ->get()
             ->map(function ($item) {
                 return [
                      'value'   => $item->id,
                      'caption' => $item->titleIn(getLocale()),
                 ];
             })
        ;

        return $res->toArray();
    }



    /**
     * Checks the cart has an order which the order is related to an allowed
     * category or not
     *
     * @return bool
     */
    private function hasCategoriesAllowed(): bool
    {
        $allowed_categories = $this->getAllowedCategories();
        $cart_categories    = $this->getCartCategories();

        if (empty($allowed_categories) || empty($cart_categories)) {
            return true;
        }

        return $this->hasIntersection($allowed_categories, $cart_categories);
    }



    /**
     * Checks the cart has an order which the order is related to a not allowed
     * category or not
     *
     * @return bool
     */
    private function hasCategoriesNotAllowed(): bool
    {
        $not_allowed_categories = $this->getNotAllowedCategories();
        $cart_categories        = $this->getCartCategories();


        if (empty($not_allowed_categories) || empty($cart_categories)) {
            return false;
        }

        return $this->hasIntersection(
             $not_allowed_categories,
             $cart_categories
        );
    }



    /**
     * Checks that the cart has not any order which one of them is related to a
     * not allowed category
     *
     * @return bool
     */
    private function hasNotCategoriesNotAllowed(): bool
    {
        return !$this->hasCategoriesNotAllowed();
    }



    /**
     * Checks that the array $a and $b have any intersection or not. If have
     * the result is true, otherwise it's false
     *
     * @param array $a
     * @param array $b
     *
     * @return bool
     */
    private function hasIntersection(array $a, array $b): bool
    {
        return array_intersect($a, $b) != [];
    }



    /**
     * Return array of allowed categories' ID
     *
     * @return array
     */
    private function getAllowedCategories(): array
    {
        return explode_not_empty(',', $this->para['categories_allowed']);
    }



    /**
     * Return array of not allowed categories' ID
     *
     * @return array
     */
    private function getNotAllowedCategories(): array
    {
        return explode_not_empty(',', $this->para['categories_not_allowed']);
    }



    /**
     * A getter for $cart_categories
     *
     * @return array
     */
    private function getCartCategories()
    {
        if (!empty($this->cart_categories)) {
            return $this->cart_categories;
        }

        $result = [];

        foreach ($this->invoice->getItems() as $order_packet) {
            if (isset($order_packet['order']->post) && isset($order_packet['order']->post->categories)) {
                $result = array_merge(
                     $result,
                     $order_packet['order']->post->categories->pluck('id')
                                                             ->toArray()
                );
            }
        }

        $this->setCartCategories($result);

        return $result;
    }



    /**
     * Setter of $cart_categories
     *
     * @param array $categories
     *
     * @return CategoryRule
     */
    private function setCartCategories(array $categories): CategoryRule
    {
        $this->cart_categories = $categories;

        return $this;
    }
}
