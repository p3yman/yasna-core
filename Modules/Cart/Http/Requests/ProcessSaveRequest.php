<?php

namespace Modules\Cart\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ProcessSaveRequest extends YasnaRequest
{
    protected $model_name = 'cart';

    public function authorize()
    {
        return $this->model->can('process');
    }

    public function purifier()
    {
        return [
            'added_discount' => "ed|numeric",
        ];
    }

    public function rules()
    {
        /*-----------------------------------------------
        | General Rules ...
        */
        $allowed_types = implode(',', array_pluck($this->model->record->typesCombo(), 0));

        session()->put('test', $allowed_types);
        $rules = [
            'type' => "required|in:$allowed_types",
        ];

        /*-----------------------------------------------
        | Type-Based Rules ...
        */
        $method_name = "rulesFor" . studly_case($this->data['type']);
        if (method_exists($this, $method_name)) {
            $rules = array_merge($rules, $this->$method_name());
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $rules;
    }

    public function rulesForStatus()
    {
        $allowed_status = implode(',', array_pluck($this->model->statusCombo(), 0));

        return [
            'new_status' => "required|in:$allowed_status",
        ];
    }

    public function rulesForPayment()
    {
        $allowed_status = implode(',', array_pluck($this->model->paymentStatusCombo(), 0));

        return [
            'payment_status' => "required|in:$allowed_status",
        ];
    }

    public function rulesForDiscount()
    {
        return [
            'added_discount' => "required|numeric|max:" . $this->model->invoiced_amount . "|" ,
        ];
    }

    public function rulesForRemark()
    {
        return [
            'remarks' => "required" ,
        ] ;
    }
}
