<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/1/19
 * Time: 11:06 AM
 */

namespace Modules\Cart\Http\Requests\V1;

use App\Models\Cart;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class SingleRequest
 *
 * @property Cart $model
 */
class SingleRequest extends YasnaRequest
{
    /**
     * Model Name
     *
     * @var string
     */
    protected $model_name = 'cart';

    /**
     * Whether trashed model is acceptable
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * Responder
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->modelExists()
             and
             $this->modelIsOwned()
             and
             $this->modelIsNotRaised()
        );
    }



    /**
     * Whether the model exists
     *
     * @return bool
     */
    protected function modelExists()
    {
        return $this->model->exists;
    }



    /**
     * Whether model is owned by the logged in user.
     *
     * @return bool
     */
    protected function modelIsOwned()
    {
        return ($this->model->user_id == user()->id);
    }



    /**
     * Whether model has not been raised.
     *
     * @return bool
     */
    protected function modelIsNotRaised()
    {
        return $this->model->isNotRaised();
    }
}
