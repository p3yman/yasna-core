<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/5/19
 * Time: 10:04 AM
 */

namespace Modules\Cart\Http\Requests\V1;


use Modules\Cart\Http\Requests\V1\Traits\PermissionTrait;

class PreviewRequest extends SingleRequest
{
    use PermissionTrait;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->modelExists()
             and
             $this->hasTruePermission()
        );
    }



    /**
     * Whether the logged in user has true access to the requested cart.
     *
     * @return bool
     */
    public function hasTruePermission()
    {
        return (
            // if the logged in user is requesting for an owned cart
             $this->modelIsOwned()
             or
             // if the logged in user has any carts permission
             $this->hasAnyCartPermission()
        );
    }
}
