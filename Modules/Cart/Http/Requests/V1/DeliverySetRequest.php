<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/12/19
 * Time: 11:50 AM
 */

namespace Modules\Cart\Http\Requests\V1;


use Illuminate\Validation\Rule;

class DeliverySetRequest extends SingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'address_id' => Rule::exists('addresses', 'id')
                                 ->where('user_id', user()->id),


             'shipping_method_id' => [
                 // check if at least one of the `address_id` or `shipping_method_id` has been sent
                 'required_without:address_id',

                 // check if availability of the requested shipping method
                 function (string $field, string $value, $fail) {
                     if ($this->model->canBeShippedBy($value)) {
                         return;
                     }

                     $fail(trans('validation.exists'));
                 },
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $module = $this->runningModule();

        return [
             'address_id'         => $module->getTrans('validation.attributes.address'),
             'shipping_method_id' => $module->getTrans('validation.attributes.shipping_method_id'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'address_id'         => 'dehash',
             'shipping_method_id' => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'address_id',
             'shipping_method_id',
        ];
    }
}
