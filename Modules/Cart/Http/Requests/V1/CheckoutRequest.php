<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/2/19
 * Time: 11:46 AM
 */

namespace Modules\Cart\Http\Requests\V1;


class CheckoutRequest extends SingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'coupon_codes'   => 'array',
             'coupon_codes.*' => 'string',
        ];
    }



    /**
     * @return bool
     */
    public function authorize()
    {
        return (
             parent::authorize()
             and
             $this->isCheckoutAllowed()
        );
    }



    /**
     * Whether the requested model is able to checkout.
     *
     * @return bool
     */
    protected function isCheckoutAllowed()
    {
        return $this->model->isCheckoutAllowed();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'coupon_codes' => $this->runningModule()->getTrans('validation.attributes.coupons_codes'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctCoupons();
    }



    /**
     * Corrects the codes array.
     */
    protected function correctCoupons()
    {
        $codes = $this->data['coupon_codes'] ?? [];

        if (!$codes) {
            return;
        }

        $codes = (array)$codes;

        $this->data['coupon_codes'] = array_map('ed', $codes);
    }
}
