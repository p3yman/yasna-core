<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/1/19
 * Time: 5:25 PM
 */

namespace Modules\Cart\Http\Requests\V1;


use Modules\Cart\Http\Requests\V1\Traits\CartItemsTrait;

class ItemsAddRequest extends SingleRequest
{
    use CartItemsTrait;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive([
             'items' => 'required',
        ], $this->itemsRules());
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctItems();
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'items',
        ];
    }
}
