<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/12/19
 * Time: 4:28 PM
 */

namespace Modules\Cart\Http\Requests\V1;


class CouponEvaluateRequest extends SingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'codes'   => [
                  'required',
                  'array',
             ],
             'codes.*' => 'string',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'codes' => $this->runningModule()->getTrans('validation.attributes.coupons_codes'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctCoupons();
    }



    /**
     * Corrects the codes array.
     */
    protected function correctCoupons()
    {
        $codes = $this->data['codes'] ?? [];

        if (!$codes) {
            return;
        }

        $codes = (array)$codes;

        $this->data['codes'] = array_map('ed', $codes);
    }
}
