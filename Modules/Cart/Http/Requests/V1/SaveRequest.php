<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/1/19
 * Time: 12:40 PM
 */

namespace Modules\Cart\Http\Requests\V1;


use Modules\Cart\Http\Requests\V1\Traits\CartItemsTrait;

class SaveRequest extends SingleRequest
{
    use CartItemsTrait;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(
             $this->itemsRules(),
             $this->createRules()
        );
    }



    /**
     * Returns the rules for the create action.
     *
     * @return array
     */
    public function createRules()
    {
        if ($this->tryingToUpdate()) {
            return [];
        }

        return [
             'currency' => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->tryingToCreate()) {
            return true;
        }

        return parent::authorize();
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_merge(
             $this->dynamicFillableFields(),
             $this->generalFillableFields()
        );
    }



    /**
     * Returns the part of the fillable fields based on the request.
     *
     * @return array
     */
    protected function dynamicFillableFields()
    {
        if ($this->tryingToUpdate()) {
            return [];
        }

        return $this->createFillableFields();
    }



    /**
     * Returns the fillable fields for the create action.
     *
     * @return array
     */
    protected function createFillableFields()
    {
        return [
             'currency',
        ];
    }



    /**
     * Returns the fillable fields which should be applied in both create and update actions.
     *
     * @return array
     */
    protected function generalFillableFields()
    {
        return [
             'items',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctUserId();
        $this->correctItems();
        $this->correctLocale();
    }



    /**
     * Corrects the user id
     */
    protected function correctUserId()
    {
        $this->data['user_id'] = user()->id;
    }



    /**
     * Corrects the locale filed.
     */
    protected function correctLocale()
    {
        if ($this->tryingToUpdate()) {
            return;
        }

        $this->data['locale'] = getLocale();
    }



    /**
     * Whether the request is trying to update.
     *
     * @return bool
     */
    public function tryingToUpdate()
    {
        return $this->filled('hashid');
    }



    /**
     * Whether the request is trying to create.
     *
     * @return bool
     */
    public function tryingToCreate()
    {
        return !$this->tryingToUpdate();
    }
}
