<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/1/19
 * Time: 10:15 AM
 */

namespace Modules\Cart\Http\Requests\V1;


use Modules\Cart\Http\Requests\V1\Traits\PermissionTrait;
use Modules\Yasna\Services\YasnaRequest;

class ListRequest extends YasnaRequest
{
    use PermissionTrait;


    protected $responder = 'white-house';



    /**
     * Returns the basic part of the rules.
     *
     * @return array
     */
    public function basicRules()
    {
        return [
             'active' => 'boolean',
        ];
    }



    /**
     * Returns the part of the rules related to the `user_id` field.
     *
     * @return array
     */
    public function userIdRules()
    {
        if ($this->getData('user_id') != '-') {
            return [];
        }

        return [
             'user_id' => 'invalid',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->requestedSelf()
             or
             $this->hasAnyCartPermission()
        );
    }



    /**
     * Whether current  user is requesting for own carts.
     *
     * @return bool
     */
    protected function requestedSelf()
    {
        $requested_user_id = ($this->data['user_id'] ?? null);

        return ($requested_user_id == user()->id);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctActive();
        $this->correctUserId();
    }



    /**
     * Corrects the active field.
     */
    protected function correctActive()
    {
        if ($this->isset('active')) {
            return;
        }

        $this->data['active'] = 1;
    }



    /**
     * Corrects the value of the `user_id` field.
     * <br>
     * If the field contain a valid hashid, it will be converted to the alternative id.
     * If not, it will be filled with `-` to be checked in the rules.
     */
    protected function correctUserId()
    {
        $value     = $this->getData('user_id');
        $dehashed  = hashid($value);
        $new_value = is_numeric($dehashed)
             ? $dehashed
             : user()->id;

        $this->setData('user_id', $new_value);
    }
}
