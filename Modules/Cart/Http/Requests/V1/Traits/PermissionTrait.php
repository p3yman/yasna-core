<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/5/19
 * Time: 10:04 AM
 */

namespace Modules\Cart\Http\Requests\V1\Traits;


trait PermissionTrait
{
    /**
     * If current user has any access on carts.
     *
     * @return bool
     */
    protected function hasAnyCartPermission()
    {
        return model('cart')->can('*');
    }



    /**
     * If current user has the specified permission on carts.
     *
     * @param string $permission
     *
     * @return bool
     */
    protected function hasCartPermission(string $permission = '*'):bool
    {
        return model('cart')->can($permission);
    }
}
