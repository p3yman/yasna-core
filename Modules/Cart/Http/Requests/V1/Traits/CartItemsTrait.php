<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/1/19
 * Time: 5:20 PM
 */

namespace Modules\Cart\Http\Requests\V1\Traits;


use App\Models\Ware;

trait CartItemsTrait
{

    /**
     * The Wares Fetched from the DB to Prevent Duplicate Queries
     *
     * @var array
     */
    protected $fetched_wares = [];



    /**
     * The rules of the items array.
     *
     * @return array
     */
    public function itemsRules()
    {
        return [
             'items'         => 'array',
             'items.*.count' => 'numeric',
             'items.*'       => function (string $field, array $value, $fail) {
                 /** @var Ware $ware */
                 $ware = $this->fetched_wares[$value['ware_id']];

                 if ($ware->isNotPurchasable()) {
                     $fail(
                          $this->runningModule()->getTrans('validation.ware_is_not_purchasable', [
                               'title' => $ware->full_title,
                          ])
                     );
                     return;
                 }

                 if (!$ware->inventoryIsEnough($value['count'])) {
                     $fail(
                          $this->runningModule()->getTrans('validation.ware_inventory_not_enough', [
                               'title' => $ware->full_title,
                          ])
                     );
                     return;
                 }

             },
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $module = $this->runningModule();

        return [
             'items.*.count' => $module->getTrans('validation.attributes.each_item_count'),
        ];
    }



    /**
     * Corrects the items array.
     */
    protected function correctItems()
    {
        $items = ($this->data['items'] ?? null);
        if (empty($items) or !is_array($items)) {
            return;
        }

        $this->data['items'] = $this->normalizeItemsArray($items);
    }



    /**
     * Normalizes the items array.
     *
     * @param array $items
     *
     * @return array
     */
    protected function normalizeItemsArray(array $items): array
    {
        $result = [];

        foreach ($items as $item) {
            if (!is_array($item)) {
                continue;
            }

            $result[] = $this->normalizeItem($item);
        }

        return array_filter($result);
    }



    /**
     * Normalizes an item in the items array.
     *
     * @param array $item_info
     *
     * @return array
     */
    protected function normalizeItem(array $item_info)
    {
        $hashid = ($item_info['ware_id'] ?? null);
        if (!$hashid) {
            $id = 0;
        }

        $ware = model('ware')->grabHashid($hashid);

        $count                    = ($item_info['count'] ?? 1);
        $id                       = $id ?? $ware->id;
        $this->fetched_wares[$id] = $ware;

        return [
             'ware_id' => $ware->id,
             'count'   => $count,
        ];
    }
}
