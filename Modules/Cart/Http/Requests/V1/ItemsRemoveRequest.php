<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/1/19
 * Time: 5:51 PM
 */

namespace Modules\Cart\Http\Requests\V1;


use Illuminate\Validation\Rule;

class ItemsRemoveRequest extends SingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'items'   => 'required|array',
             'items.*' => Rule::in($this->validItemsIds()),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'items.*' => $this->runningModule()->getTrans('validation.attributes.one_of_items'),
        ];
    }



    /**
     * Returns an array of the acceptable ids for items.
     *
     * @return array
     */
    protected function validItemsIds()
    {
        return $this
             ->model
             ->orders
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctItems();
    }



    /**
     * Corrects the items array.
     */
    protected function correctItems()
    {
        $items = ($this->data['items'] ?? []);

        if (!is_array($items)) {
            return;
        }

        $this->data['items'] = array_map('hashid', $items);
    }
}
