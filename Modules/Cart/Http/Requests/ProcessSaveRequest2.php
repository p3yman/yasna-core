<?php

namespace Modules\Cart\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ProcessSaveRequest2 extends YasnaRequest
{
    protected $model_name = "Cart";



    public function authorize()
    {
        if ($this->data['_submit'] == 'delete') {
            return $this->model->can('delete');
        }

        return $this->model->can('process');
    }



    public function purifier()
    {
        return [
             'added_discount' => "ed|numeric",
        ];
    }



    public function corrections()
    {
        $this->data['action'] = $this->data['_submit'];
    }



    public function rules()
    {
        $rules = [];

        /*-----------------------------------------------
        | Action ...
        */
        $allowed_actions = implode(',', $this->model->allowedActions());
        $rules['action'] = "required|in:$allowed_actions";

        /*-----------------------------------------------
        | Action-Based Rules ...
        */
        $method_name = "rulesFor" . studly_case($this->data['action']);
        if (method_exists($this, $method_name)) {
            $rules = array_merge($rules, $this->$method_name());
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $rules;
    }



    public function rulesForRemark()
    {
        return [
             'remarks' => "required",
        ];
    }



    public function rulesForDiscount()
    {
        $rules['added_discount'] = "required|numeric|max:" . $this->model->invoiced_amount . "|";

        if ($this->model->added_discount == $this->data['added_discount']) {
            $rules['added_discount'] .= 'email'; // <~~ Bad Code! I just misused email rule for something else.
        }

        return $rules;
    }



    public function rulesForStatus()
    {
        $allowed_status      = implode(',', array_pluck($this->model->statusOptions(), 0));
        $rules['new_status'] = "required|in:$allowed_status";

        chalk()->add($this->model->status);

        if ($this->model->status == $this->data['new_status']) {
            $rules['new_status'] .= '|email'; // <~~ Bad Code! I just misused email rule for something else.
        }

        return $rules;
    }



    public function rulesForPaymentStatus()
    {
        $allowed_status              = implode(',', array_pluck($this->model->paymentStatusOptions(), 0));
        $rules['payment_status'] = "required|in:$allowed_status";

        chalk()->add($this->model->payment_status);

        if ($this->model->payment_status == $this->data['payment_status']) {
            $rules['payment_status'] .= '|email'; // <~~ Bad Code! I just misused email rule for something else.
        }

        return $rules;
    }



    public function messages()
    {
        return [
             'new_status.required' => trans_safe("cart::records.validation.status"),
             'new_status.in'       => trans_safe("cart::records.validation.status"),
             "new_status.email"    => trans_safe("cart::records.validation.status_unchanged"),

             'payment_status.required' => trans_safe("cart::records.validation.status"),
             'payment_status.in'       => trans_safe("cart::records.validation.status"),
             "payment_status.email"    => trans_safe("cart::records.validation.status_unchanged"),

             'added_discount.required' => trans_safe("cart::records.validation.discount_required"),
             'added_discount.max'      => trans_safe("cart::records.validation.discount_max"),
             "added_discount.email"    => trans_safe("cart::records.validation.discount_unchanged"),

             'remarks.required' => trans_safe("cart::records.validation.remarks"),
        ];
    }
}
