<?php

namespace Modules\Cart\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CartSearchRequest extends YasnaRequest
{
    protected $model_name = "cart";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->can('*');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'keyword' => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'keyword' => "ed",
        ];
    }
}
