<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/cart-items-add
 *                    Items Add
 * @apiDescription    Add a Number of Items to a Specified Cart
 * @apiVersion        1.0.0
 * @apiName           Items Add
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} hashid The hashid of the cart
 * @apiParam {Array} items the items to be saved as the orders of the cart. Each item should have a `ware_id` field
 *           which is the hashid of the ware. A `count` field is acceptable for each item and will be filled with `1`
 *           if missed.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\SaveController controller()
 */
class ItemsAddEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Items Add";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SaveController@itemsAdd';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
