<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/cart-coupon-evaluate
 *                    Coupon Evaluate
 * @apiDescription    Evaluate coupon codes(s) for a cart.
 * @apiVersion        1.0.0
 * @apiName           Coupon Evaluate
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} hashid The hashid of the cart
 * @apiParam {Array} codes An array of the coupons to be evaluated.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "total_discount": 5000,
 *           "codes": [
 *                {
 *                     "code": "holiday-75681",
 *                     "is_applicable": true
 *                },
 *                {
 *                     "code": "holiday-94325",
 *                     "is_applicable": false
 *                },
 *                {
 *                     "code": "invalid-coupon",
 *                     "is_applicable": false
 *                }
 *           ]
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\CouponController controller()
 */
class CouponEvaluateEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Coupon Evaluate";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CouponController@evaluate';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'total_discount' => 5000,
             'codes'          =>
                  [
                       [
                            'code'          => 'holiday-75681',
                            'is_applicable' => true,
                       ],
                       [
                            'code'          => 'holiday-94325',
                            'is_applicable' => false,
                       ],
                       [
                            'code'          => 'invalid-coupon',
                            'is_applicable' => false,
                       ],
                  ],
        ]);
    }
}
