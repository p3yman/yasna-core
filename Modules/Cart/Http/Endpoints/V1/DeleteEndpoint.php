<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/cart-delete
 *                    Delete
 * @apiDescription    Delete a Cart
 * @apiVersion        1.0.0
 * @apiName           Delete
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} hashid The hashid of the cart
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": 1
 *      }
 * }
 * @apiErrorExample Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\DeleteController controller()
 */
class DeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Delete";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'DeleteController@delete';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => 1,
        ]);
    }
}
