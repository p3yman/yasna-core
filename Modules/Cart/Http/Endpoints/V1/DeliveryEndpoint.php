<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/cart-delivery
 *                    Delivery
 * @apiDescription    Set the delivery info of a cart.
 * @apiVersion        1.0.0
 * @apiName           Delivery
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} [address_id] The hashid of an address of the logged in user to be set as the cart's address
 * @apiParam {String} [shipping_method_id] The hashid of an available shipping method
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   Invalid-Inputs:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "cart.moreInfo.422",
 *      "errors": {
 *           "shipping_method_id": [
 *                [
 *                     "The selected Shipping Method is invalid."
 *                ]
 *           ]
 *      }
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\DeliveryController controller()
 */
class DeliveryEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Delivery";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'DeliveryController@set';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
