<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Cart\Http\Controllers\V1\OrdersController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/cart-flush-all-my-orders
 *                    Delete All Orders In Cart
 * @apiDescription    Delete All Orders In Cart
 * @apiVersion        1.0.0
 * @apiName           Flush Cart
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" : hashid(0),
 *          "authenticated_user": hashid(user()->id)
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method OrdersController controller()
 */
class FlushAllMyOrdersEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Delete All Orders In Cart";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'OrdersController@flushCart';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid"             => hashid(rand(1, 100)),
             "authenticated_user" => hashid(user()->id),
        ]);

    }
}
