<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/cart-list
 *                    List
 * @apiDescription    The list of Carts
 * @apiVersion        1.0.2
 * @apiName           List
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} [locale] The Locale of the Carts in the Result
 * @apiParam {String} [currency] The currency of the Carts in the Result
 * @apiParam {String} [user_id] The hashid of the user of the Carts in the Result
 * @apiParam {Boolean} [active=1] Weather the carts in the result should be active or not.
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {String} [per_page=15] Indicate the number of results on per page. (available only with `paginated=1`).
 * @apiParam {String} [page=1] Page number of returned result (available only with `paginated=1`).
 * @apiSuccessExample Active-Carts-Requested:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA",
 *           "count": 1
 *      },
 *      "results": [
 *           {
 *                "id": "PKEnN",
 *                "locale": "fa",
 *                "currency": "IRR",
 *                "total_items": 1,
 *                "updated_at": 1546326604,
 *                "created_at": 1546326604,
 *                "status": "not_raised",
 *                "payment_status": null,
 *                "user_id": "QKgJx",
 *                "user_name": "Jane Doe",
 *                "original_price": 12345600,
 *                "sale_price": 12340000,
 *                "delivery_price": 12300,
 *                "added_discount": 12300,
 *                "tax_amount": 123,
 *                "invoiced_amount": 123459900,
 *                "paid_amount": 0,
 *                "is_active": 1
 *           }
 *      ]
 * }
 * @apiSuccessExample Inactive-Carts-Requested:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA",
 *           "count": 1
 *      },
 *      "results": [
 *           {
 *                "id": "PKEnN",
 *                "locale": "fa",
 *                "currency": "IRR",
 *                "total_items": 1,
 *                "updated_at": 1546326604,
 *                "created_at": 1546326604,
 *                "status": "not_raised",
 *                "payment_status": null,
 *                "user_id": "QKgJx",
 *                "user_name": "Jane Doe",
 *                "original_price": 12345600,
 *                "sale_price": 12340000,
 *                "delivery_price": 12300,
 *                "added_discount": 12300,
 *                "tax_amount": 123,
 *                "invoiced_amount": 123459900,
 *                "paid_amount": 0,
 *                "is_active": 0,
 *                "is_raised": 1,
 *                "raised_at": 1550825808,
 *                "raised_by": {
 *                     "user_id": "qKXVA",
 *                     "user_name": "Jane Doe"
 *                },
 *                "is_confirmed": 0,
 *                "is_disbursed": 0,
 *                "is_paid": 0,
 *                "is_sent": 0,
 *                "is_delivered": 0,
 *                "delivered_at": 1551011516,
 *                "delivered_by": {
 *                     "user_id": null,
 *                     "user_name": null
 *                },
 *                "is_rejected": 0,
 *                "is_closed": 0
 *           },
 *      ]
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\ListController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ListController@index';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $cart = model('cart')->fill([
             'user_id'    => user()->id,
             'currency'   => 'IRR',
             'locale'     => getLocale(),
             'created_at' => now(),
        ]);
        return api()->successRespond([
             $cart->toListResource(),
        ], [
             'count' => 1,
        ]);
    }
}
