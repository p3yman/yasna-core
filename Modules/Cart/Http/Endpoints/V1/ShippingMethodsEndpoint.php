<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/cart-shipping-methods
 *                    Shipping Methods
 * @apiDescription    Read shipping methods for a cart
 * @apiVersion        1.0.0
 * @apiName           Shipping Methods
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} hashid the hashid of the cart to read its available shipping methods
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": [
 *           {
 *                "id": "qKXVA",
 *                "slug": "truck",
 *                "title": "Truck",
 *                "created_at": 1545743409,
 *                "updated_at": 1545743409
 *           }
 *      ]
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\ShippingController controller()
 */
class ShippingMethodsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Shipping Methods";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ShippingController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  'id'         => 'qKXVA',
                  'slug'       => 'truck',
                  'title'      => 'Truck',
                  'created_at' => 1545743409,
                  'updated_at' => 1545743409,
             ],
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
