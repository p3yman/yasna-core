<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/cart-save
 *                    Save
 * @apiDescription    Create a New Cart or Update an Existing One
 * @apiVersion        1.0.0
 * @apiName           Save
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} [hashid] Required only for updating a cart.
 * @apiParam {String} [currency] Required only for creating a cart.
 * @apiParam {Array} [items] the items to be saved as the orders of the cart. Each item should have a `ware_id` field
 *           which is the hashid of the ware. A `count` field is acceptable for each item and will be filled with `1`
 *           if missed.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Bad Request
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "cart.moreInfo.422",
 *      "errors": {
 *           "items": [
 *                [
 *                     "The Items must be an array."
 *                ]
 *           ]
 *      }
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\SaveController controller()
 */
class SaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SaveController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
