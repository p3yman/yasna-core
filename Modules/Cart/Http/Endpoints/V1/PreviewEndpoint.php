<?php

namespace Modules\Cart\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/cart-preview
 *                    Preview
 * @apiDescription    The Single Resource of a Cart
 * @apiVersion        1.0.0
 * @apiName           Preview
 * @apiGroup          Cart
 * @apiPermission     User
 * @apiParam {String} hashid The Hashid of the Cart
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "id": "qKXVA",
 *           "locale": "fa",
 *           "currency": "IRR",
 *           "total_items": 2,
 *           "updated_at": 1546328029,
 *           "created_at": 1546288200,
 *           "status": "not_raised",
 *           "payment_status": null,
 *           "user_id": "QKgJx",
 *           "user_name": "Jane Doe",
 *           "original_price": 494940,
 *           "sale_price": 494940,
 *           "delivery_price": 0,
 *           "added_discount": 0,
 *           "tax_amount": 54443,
 *           "invoiced_amount": 549383,
 *           "paid_amount": 0,
 *           "is_active": 1
 *           "items": [
 *                {
 *                     "id": "qKXVA",
 *                     "count": 2,
 *                     "currency": "IRR",
 *                     "locale": "fa",
 *                     "updated_at": 1546327791,
 *                     "created_at": 1546327791,
 *                     "ware_id": "KVpjA",
 *                     "ware_title": "Ware 1",
 *                     "post_id": "NwvWK",
 *                     "post_title": "Post 1",
 *                     "posttype_id": "qKXVA",
 *                     "posttype_title": "Products",
 *                     "single_original_price": 80982,
 *                     "single_sale_price": 80982,
 *                     "original_price": 161964,
 *                     "sale_price": 161964
 *                },
 *                {
 *                     "id": "QKgJx",
 *                     "count": 2,
 *                     "currency": "IRR",
 *                     "locale": "fa",
 *                     "updated_at": 1546328029,
 *                     "created_at": 1546328029,
 *                     "ware_id": "AEBnx",
 *                     "ware_title": "Ware 2",
 *                     "post_id": "KXzdA",
 *                     "post_title": "Post 2",
 *                     "posttype_id": "qKXVA",
 *                     "posttype_title": "Products",
 *                     "single_original_price": 166488,
 *                     "single_sale_price": 166488,
 *                     "original_price": 332976,
 *                     "sale_price": 332976
 *                }
 *           ]
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Cart\Http\Controllers\V1\SingleController controller()
 */
class PreviewEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Preview";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Cart\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SingleController@preview';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $cart = [
             'id'              => 'qKXVA',
             'locale'          => 'fa',
             'currency'        => 'IRR',
             'total_items'     => 2,
             'updated_at'      => 1546328029,
             'created_at'      => 1546288200,
             'status'          => 'not_raised',
             'payment_status'  => null,
             'user_id'         => 'QKgJx',
             'user_name'       => 'Jane Doe',
             'original_price'  => 494940,
             'sale_price'      => 494940,
             'delivery_price'  => 0,
             'added_discount'  => 0,
             'tax_amount'      => 54443,
             'invoiced_amount' => 549383,
             'paid_amount'     => 0,
             'is_active'       => 1,
             'items'           => [
                  [
                       'id'                    => 'qKXVA',
                       'count'                 => 2,
                       'currency'              => 'IRR',
                       'locale'                => 'fa',
                       'updated_at'            => 1546327791,
                       'created_at'            => 1546327791,
                       'ware_id'               => 'KVpjA',
                       'ware_title'            => 'Ware 1',
                       'post_id'               => 'NwvWK',
                       'post_title'            => 'Post 1',
                       'posttype_id'           => 'qKXVA',
                       'posttype_title'        => 'Products',
                       'single_original_price' => 80982,
                       'single_sale_price'     => 80982,
                       'original_price'        => 161964,
                       'sale_price'            => 161964,
                  ],

                  [
                       'id'                    => 'QKgJx',
                       'count'                 => 2,
                       'currency'              => 'IRR',
                       'locale'                => 'fa',
                       'updated_at'            => 1546328029,
                       'created_at'            => 1546328029,
                       'ware_id'               => 'AEBnx',
                       'ware_title'            => 'Ware 2',
                       'post_id'               => 'KXzdA',
                       'post_title'            => 'Post 2',
                       'posttype_id'           => 'qKXVA',
                       'posttype_title'        => 'Products',
                       'single_original_price' => 166488,
                       'single_sale_price'     => 166488,
                       'original_price'        => 332976,
                       'sale_price'            => 332976,
                  ],
             ],
        ];

        return api()->successRespond($cart);
    }
}
