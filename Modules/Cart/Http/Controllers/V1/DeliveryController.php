<?php

namespace Modules\Cart\Http\Controllers\V1;

use Modules\Cart\Http\Requests\V1\DeliverySetRequest;
use Modules\Yasna\Services\YasnaApiController;

class DeliveryController extends YasnaApiController
{
    /**
     * Sets the delivery info of the requested cart.
     *
     * @param DeliverySetRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function set(DeliverySetRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }
}
