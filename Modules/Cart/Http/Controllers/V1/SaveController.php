<?php

namespace Modules\Cart\Http\Controllers\V1;

use App\Models\Cart;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Modules\Cart\Http\Requests\V1\ItemsAddRequest;
use Modules\Cart\Http\Requests\V1\SaveRequest;
use Illuminate\Support\Collection as BaseCollection;
use Modules\Yasna\Services\YasnaApiController;

class SaveController extends YasnaApiController
{
    /**
     * The Request in User
     *
     * @var SaveRequest|ItemsAddRequest
     */
    protected $request;



    /**
     * Saves a new or updates an existing cart.
     *
     * @param SaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(SaveRequest $request)
    {
        $this->request = $request;
        $model         = $this->saveCart();

        return $this->modelSaveFeedback($model);
    }



    /**
     * Saves the cart instance.
     *
     * @return Cart
     */
    protected function saveCart()
    {
        $request = $this->request;

        // save in carts table
        $model = $request->model->batchSave($request, $this->saveOverflowArray());

        // save items
        $model = $this->saveItems($model);

        return $model;
    }



    /**
     * Saves the items of the saved cart.
     *
     * @param Cart $cart
     *
     * @return Cart
     */
    protected function saveItems(Cart $cart)
    {
        if (!$this->itemsToSave()->count()) {
            return $cart;
        }

        if ($this->request->tryingToCreate()) {
            return $this->addItemsFromRequest($cart);
        } else {
            return $this->updateItemsFromRequest($cart);
        }
    }



    /**
     * Adds the orders for a newly created cart based on the request.
     *
     * @param Cart $cart
     *
     * @return Cart
     */
    protected function addItemsFromRequest(Cart $cart)
    {
        $items = $this->itemsToSave();

        return $this->addItemsToCart($cart, $items);
    }



    /**
     * Adds the specified items to the given cart.
     *
     * @param Cart           $cart
     * @param BaseCollection $items
     *
     * @return Cart
     */
    protected function addItemsToCart(Cart $cart, BaseCollection $items)
    {
        foreach ($items as $item) {
            $cart->addOrder($item['ware_id'], $item['count']);
        }

        return $cart;
    }



    /**
     * Updates items of the given cart based on the request.
     *
     * @param Cart $cart
     *
     * @return Cart
     */
    protected function updateItemsFromRequest(Cart $cart)
    {
        $items     = $this->itemsToSave();
        $items_ids = $items->pluck('ware_id');
        $orders    = $cart->orders;

        // find orders in common between current orders and new orders
        $in_common = $orders->whereIn('ware_id', $items_ids);

        // find orders which should be deleted
        $removing = $orders->whereNotIn('ware_id', $items_ids);

        // find items which should be added as a new order.
        $in_common_ids = $in_common->pluck('ware_id');
        $adding_items  = $items->whereNotIn('ware_id', $in_common_ids);

        $this->updateOrdersWithItems($in_common, $items);
        $this->addItemsToCart($cart, $adding_items);
        $this->removeOrders($removing);

        $cart->refresh();

        return $cart;
    }



    /**
     * Updates orders of with the given items information.
     *
     * @param Collection     $orders
     * @param BaseCollection $items
     */
    protected function updateOrdersWithItems(Collection $orders, BaseCollection $items)
    {
        foreach ($orders as $order) {
            $related_item = $items->where('ware_id', $order->ware_id)->last();

            $order->update([
                 'count' => $related_item['count'],
            ]);
        }
    }



    /**
     * Removes the given orders.
     *
     * @param Collection $orders
     */
    protected function removeOrders(Collection $orders)
    {
        foreach ($orders as $order) {
            /** @var Order $order */
            $order->delete();
        }
    }



    /**
     * Returns the items from the request to be saved.
     *
     * @return BaseCollection
     */
    protected function itemsToSave()
    {
        return collect(
             $this->request->items ?: []
        );
    }



    /**
     * Overflow fields while saving a new item in the `carts` table.
     *
     * @return array
     */
    protected function saveOverflowArray()
    {
        return [
             'items',
        ];
    }



    /**
     * Adds requested items to the specified cart.
     *
     * @param ItemsAddRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function itemsAdd(ItemsAddRequest $request)
    {
        $this->request = $request;

        $cart = $request->model;
        $cart = $this->addItemsFromRequest($cart);

        return $this->modelSaveFeedback($cart);
    }
}
