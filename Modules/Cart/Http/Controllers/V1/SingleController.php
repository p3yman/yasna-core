<?php

namespace Modules\Cart\Http\Controllers\V1;

use Modules\Cart\Http\Requests\V1\PreviewRequest;
use Modules\Cart\Http\Requests\V1\SingleRequest;
use Modules\Yasna\Services\YasnaApiController;

class SingleController extends YasnaApiController
{
    /**
     * The action to preview the single resource of the requested cart.
     *
     * @param PreviewRequest $request
     *
     * @return array
     */
    public function preview(PreviewRequest $request)
    {
        return $this->success($request->model->toSingleResource());
    }



    /**
     * Revalidates the specified cart and returns it new validity status.
     *
     * @param SingleRequest $request
     *
     * @return array
     */
    public function revalidate(SingleRequest $request)
    {
        $cart = $request->model;
        return $this->success([
             'is_valid' => intval($cart->revalidate()),
        ], [
             'hashid' => $cart->hashid,
        ]);
    }
}
