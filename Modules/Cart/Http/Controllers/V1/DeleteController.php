<?php

namespace Modules\Cart\Http\Controllers\V1;

use Modules\Cart\Http\Requests\V1\SingleRequest;
use Modules\Yasna\Services\YasnaApiController;

class DeleteController extends YasnaApiController
{
    /**
     * Deletes the requested cart.
     *
     * @param SingleRequest $request
     *
     * @return array
     */
    public function delete(SingleRequest $request)
    {
        $done = $request->model->delete();
        $done = intval($done);

        return $this->success(compact('done'));
    }
}
