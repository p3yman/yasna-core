<?php

namespace Modules\Cart\Http\Controllers\V1;

use App\Models\Cart;
use Modules\Cart\Http\Requests\V1\CheckoutRequest;
use Modules\Coupons\Services\InvoiceObject;
use Modules\Yasna\Services\YasnaApiController;

class CheckoutController extends YasnaApiController
{
    /**
     * Checkout the requested model.
     *
     * @param CheckoutRequest $request
     *
     * @return array
     */
    public function checkout(CheckoutRequest $request)
    {
        $cart = $request->model;
        $cart = $this->applyCoupons($cart, $request);
        $done = intval($cart->checkout());

        return $this->success(compact('done'), [
             'hashid' => $cart->hashid,
        ]);
    }



    /**
     * Applies the coupon codes.
     *
     * @param Cart            $cart
     * @param CheckoutRequest $request
     *
     * @return Cart
     */
    protected function applyCoupons(Cart $cart, CheckoutRequest $request)
    {
        $invoice_object = $cart->getInvoiceObject();
        $codes          = $request->coupon_codes;

        foreach ($codes as $code) {
            $this->applyOneCoupon($cart, $invoice_object, $code);
        }

        $cart->refreshCalculations();

        return $cart;
    }



    /**
     * Applies the specified coupon code and marks it as used if it made any change on the discount amount.
     *
     * @param Cart          $cart
     * @param InvoiceObject $invoice_object
     * @param string        $code
     */
    protected function applyOneCoupon(Cart $cart, InvoiceObject $invoice_object, string $code)
    {
        $previous_amount    = $invoice_object->getTotalDiscount();
        $new_invoice_object = coupon()->evaluate($code, $invoice_object);
        $new_amount         = $new_invoice_object->getTotalDiscount();
        $applied_amount     = ($new_amount - $previous_amount);

        // if the code made any change on the discount amount
        if ($applied_amount) {
            $cart->markAsUsed($code, $cart->id, user()->id, $applied_amount);
        }
    }
}
