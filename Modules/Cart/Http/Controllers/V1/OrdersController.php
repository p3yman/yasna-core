<?php

namespace Modules\Cart\Http\Controllers\V1;

use App\Models\Cart;
use App\Models\Order;
use Modules\Cart\Http\Requests\V1\FlushMyOrdersRequest;
use Modules\Cart\Http\Requests\V1\ItemsRemoveRequest;
use Modules\Yasna\Services\YasnaApiController;

class OrdersController extends YasnaApiController
{
    /**
     * Removes the requested items from the specified cart.
     *
     * @param ItemsRemoveRequest $request
     *
     * @return array
     */
    public function remove(ItemsRemoveRequest $request)
    {
        $cart = $request->model;
        $ids  = $request->items;

        $done = $cart
             ->orders()
             ->whereIn('id', $ids)
             ->delete()
        ;

        $cart->refreshCalculations();

        return $this->success(compact('done'), [
             'hashid' => $cart->hashid,
        ]);
    }



    /**
     * flush all orders from cart
     *
     * @return array
     */
    public function flushCart()
    {
        /**@var Cart $cart */
        $cart = user()->carts()
                      ->where('raised_at', null)
                      ->orderBy('created_at', 'desc')
                      ->first()
        ;

        if (!$cart) {
            return $this->typicalSaveFeedback(true);
        }

        $orders = $cart->orders();

        if (!$orders->exists()) {
            return $this->typicalSaveFeedback(true);
        }

        $done = $orders->delete();

        return $this->typicalSaveFeedback($done);
    }
}
