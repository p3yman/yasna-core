<?php

namespace Modules\Cart\Http\Controllers\V1;

use Exception;
use Modules\Cart\Http\Requests\V1\CouponEvaluateRequest;
use Modules\Coupons\Services\InvoiceObject;
use Modules\Yasna\Services\YasnaApiController;

class CouponController extends YasnaApiController
{
    /**
     * Evaluates coupon(s) for a cart.
     *
     * @param CouponEvaluateRequest $request
     *
     * @return array
     */
    public function evaluate(CouponEvaluateRequest $request)
    {
        $cart            = $request->model;
        $requested_codes = $request->codes;
        $invoice_object  = $cart->getInvoiceObject();
        $codes           = $this->evaluateCoupons($invoice_object, $requested_codes);
        $total_discount  = $invoice_object->getTotalDiscount();

        return $this->success(compact('total_discount', 'codes'));
    }



    /**
     * Evaluates the given coupons on the given invoice object.
     *
     * @param InvoiceObject $invoice_object
     * @param array         $coupons
     *
     * @return array
     */
    protected function evaluateCoupons(InvoiceObject $invoice_object, array $coupons)
    {
        return collect($coupons)
             ->map(function (string $coupon) use ($invoice_object) {

                 return [
                      'code'          => $coupon,
                      'is_applicable' => $this->checkOneCoupon($invoice_object, $coupon),
                 ];
             })->toArray()
             ;
    }



    /**
     * Checks the effect of the specified coupon on the given invoice object.
     *
     * @param InvoiceObject $invoice_object
     * @param string        $coupon
     *
     * @return bool
     */
    protected function checkOneCoupon(InvoiceObject $invoice_object, string $coupon)
    {
        $previous_discount = $invoice_object->getTotalDiscount();
        $invoice           = coupon()->evaluate($coupon, $invoice_object);

        try {
            return ($invoice->getTotalDiscount() > $previous_discount);
        } catch (Exception $exception) {
            return false;
        }
    }
}
