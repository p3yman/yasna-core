<?php

namespace Modules\Cart\Http\Controllers\V1;

use App\Models\ShippingMethod;
use Modules\Cart\Http\Requests\V1\SingleRequest;
use Modules\Yasna\Services\YasnaApiController;

class ShippingController extends YasnaApiController
{
    /**
     * Action to read all the available shipping methods for the specified cart.
     *
     * @param SingleRequest $request
     *
     * @return array
     */
    public function list(SingleRequest $request)
    {
        $hashid            = $request->model_hashid;
        $available_methods = $request->model->availableShippingMethods();

        return $this->success(
             $this->mapShippingMethods($available_methods),
             compact('hashid')
        );
    }



    /**
     * Maps an array of the shipping methods to their list resources.
     *
     * @param array $shipping_methods
     *
     * @return array
     */
    protected function mapShippingMethods(array $shipping_methods): array
    {
        return collect($shipping_methods)
             ->map(function (ShippingMethod $method) {
                 return $method->toListResource();
             })->toArray()
             ;
    }
}
