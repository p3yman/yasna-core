<?php

namespace Modules\Cart\Http\Controllers\V1;

use App\Models\Cart;
use Illuminate\Database\Eloquent\Builder;
use Modules\Cart\Http\Requests\V1\ListRequest;
use Modules\Yasna\Services\YasnaApiController;

class ListController extends YasnaApiController
{
    /**
     * The Request in Use
     *
     * @var ListRequest
     */
    protected $request;

    /**
     * The Builder in User
     *
     * @var Builder
     */
    protected $builder;



    /**
     * Returns a list of the active carts of the logged in user.
     *
     * @param ListRequest $request
     *
     * @return array
     */
    public function index(ListRequest $request)
    {
        $this->request = $request;
        $this->getCartsBuilder();

        if ($request->isPaginated()) {
            return $this->listPaginated();
        }

        return $this->listFlat();
    }



    /**
     * List by Pagination
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listPaginated()
    {
        $builder   = $this->builder;
        $request   = $this->request;
        $paginator = $builder->paginate($request->perPage());
        $result    = $this->map($paginator);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * List All Together
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listFlat()
    {
        $builder = $this->builder;
        $result  = $builder->get();

        $meta = [
             "total" => $result->count(),
        ];

        $data = $this->map($result);

        return $this->success($data, $meta);
    }



    /**
     * Returns the proper builder.
     */
    protected function getCartsBuilder()
    {
        $this->builder = model('cart')
             ->elector($this->getElectorArray())
             ->orderByDesc('updated_at')
        ;
    }



    /**
     * Returns the elector array to be passed to the elector.
     *
     * @return array
     */
    protected function getElectorArray()
    {
        return $this
             ->request
             ->only($this->getElectorFields());
    }



    /**
     * Returns the field which should be passed from request to the elector.
     *
     * @return array
     */
    protected function getElectorFields()
    {
        return [
             'user_id',
             'locale',
             'currency',
             'active',
        ];
    }



    /**
     * Maps the given collection to a proper array and returns it.
     *
     * @param $collections
     *
     * @return array
     */
    protected function map($collections)
    {
        return $collections
             ->map(function (Cart $cart) {
                 return $cart->toListResource();
             })->toArray()
             ;
    }
}
