<?php

namespace Modules\Cart\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Cart\Http\Requests\CartSearchRequest;
use Modules\Yasna\Services\YasnaController;

class WidgetsController extends YasnaController
{
    protected $base_model  = "Cart";
    protected $view_folder = "cart::dashboard";



    /**
     * Search for requested Cart
     *
     * @param CartSearchRequest $request
     *
     * @return string
     * @throws \Throwable
     */
    public function searchCart(CartSearchRequest $request)
    {
        $no_result = false;
        $keyword   = $request->keyword;

        if ($this->isId($keyword)) {

            $id = $this->purifyId($keyword);

        } else {

            $id = $this->model()->code2id($keyword);

        }

        $model = $this->model($id);


        if (!$model->exists) {

            $no_result = true;
        }


        $success_message = view(
             'cart::dashboard.result',
             compact('no_result', 'model')
        )->render();


        /*-----------------------------------------------
        | Result ...
        */
        return $this->jsonFeedback([
             'message'    => $success_message,
             'feed_class' => " ",
             "callback"   => "setWidgetHeight($('#divWidget-order-finder'));",


        ]);
    }



    /**
     * Checks if keyword is id or hashid
     *
     * @param string $keyword
     *
     * @return bool
     */
    protected function isId(string $keyword)
    {
        return (strpos($keyword, 'id:') === 0);
    }



    /**
     * Purifies Id from and gets cart id
     *
     * @param string $keyword
     *
     * @return string
     */
    protected function purifyId(string $keyword)
    {
        return str_after($keyword, 'id:');
    }
}
