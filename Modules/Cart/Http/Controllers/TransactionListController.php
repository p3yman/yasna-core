<?php

namespace Modules\Cart\Http\Controllers;

use Modules\Yasna\Services\YasnaController;
use Nwidart\Modules\Collection;

class TransactionListController extends YasnaController
{
    /**
     * show list of a cart transactions
     *
     * @param integer $cart_id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cartsTransaction($cart_id)
    {
        if (model('cart', $cart_id)->cannot('process')) {
            return $this->abort(403);
        }

        $transactions        = $this->getTransactions($cart_id);
        $view_data['models'] = $transactions;

        return view("cart::manage.transactions-list", $view_data);
    }



    /**
     * get list of a cart transactions
     *
     * @param integer $cart_id
     *
     * @return Collection
     */
    private function getTransactions($cart_id)
    {
        return model('transaction')
             ->where('invoice_id', $cart_id)
             ->where('invoice_model', 'cart')
             ->orderBy('updated_at', "desc")
             ->get()
             ;
    }
}
