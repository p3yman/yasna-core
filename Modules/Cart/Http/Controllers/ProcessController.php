<?php namespace Modules\Cart\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Cart\Http\Requests\ProcessSaveRequest2;
use Modules\Yasna\Services\YasnaController;

class ProcessController extends YasnaController
{
    protected $base_model  = "Cart";
    protected $view_folder = "cart::process";



    public function index($cart_id, $view = 'index')
    {
        /*-----------------------------------------------
        | Model ...
        */
        $model = $this->model($cart_id, true);
        $model->spreadMeta();
        if (!$model or !$model->exists) {
            return $this->abort(410);
        }
        if ($model->cannot('process')) {
            return $this->abort(403);
        }

        /*-----------------------------------------------
        | View ...
        */
        return $this->view($view, compact('model'));
    }



    /**
     * save request
     *
     * @param ProcessSaveRequest2 $request
     *
     * @return string
     */
    public function save(ProcessSaveRequest2 $request)
    {
        $model  = $request->model;
        $method = "save" . studly_case($request->action);

        if ($request->action == 'delete') {
            return $this->delete($request);
        }
        if ($this->hasMethod($method)) {
            $ok = $this->$method($request);
        } else {
            return $this->jsonFeedback($method);
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback'   => "rowUpdate('tblCarts' , '$model->hashid');masterModal(url('manage/carts/process/$model->hashid'))",
             'success_modalClose' => false,
        ]);
    }



    protected function saveDiscount($request)
    {
        return $request->model->setDiscount($request->added_discount, $request->remarks);
    }



    protected function saveConfirm($request)
    {
        return $request->model->confirm();
    }



    protected function saveRemarks($request)
    {
        return $request->model->addRemarks($request->remarks);
    }



    protected function saveCheckoutUndo($request)
    {
        return $request->model->checkoutUndo($request->remarks);
    }



    protected function saveCheckout($request)
    {
        return $request->model->checkout($request->remarks);
    }



    protected function saveStatus($request)
    {
        return $request->model->setStatus($request->new_status, $request->remarks);
    }



    protected function savePaymentStatus($request)
    {
        return $request->model->setPaymentStatus($request->payment_status, $request->remarks);
    }



    /**
     * deletes a cart
     *
     * @param ProcessSaveRequest2 $request
     *
     * @return string
     */
    protected function delete($request): string
    {
        $deleted = $request->model->delete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "rowHide('tblCarts' , '$request->hashid')",
        ]);
    }
}
