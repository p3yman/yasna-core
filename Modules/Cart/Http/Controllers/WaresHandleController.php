<?php

namespace Modules\Cart\Http\Controllers;

use App\Models\Ware;
use Modules\Yasna\Services\YasnaController;

class WaresHandleController extends YasnaController
{
    /**
     * add wares list cols
     */
    public static function browseColumns()
    {
        module('shop')
             ->service('ware_browse_headings')
             ->add('order_count')
             ->trans("cart::carts.sale_count")
             ->blade("cart::ware.order_count_col")
             ->width('100')
             ->order(20)
        ;
    }



    /**
     * Registers ware export columns.
     */
    public static function exportColumns()
    {
        $service = service('shop:ware_export_columns');


        $service
             ->add('sale_count')
             ->trans("cart::carts.sale_count")
             ->order(20)
             ->value([
                  'parser' => function (Ware $ware) {
                      return model('cart')
                           ->where('raised_at', '<=', now())
                           ->join('orders', 'orders.cart_id', 'carts.id')
                           ->where('orders.ware_id', $ware->id)
                           ->sum('orders.count')
                           ;
                  },
             ])
        ;
    }
}
