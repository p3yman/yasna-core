<?php namespace Modules\Cart\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class PrintController extends YasnaController
{
    protected $base_model  = "Cart";
    protected $view_folder = "cart::invoice";



    public function single($hashid)
    {
        $model = $this->model($hashid);

        if (!$model or !$model->exists) {
            return $this->abort(410);
        }
        if ($model->cannot('process')) {
            return $this->abort(403) ;
        }

        return $this->view('print', compact('model')) ;
    }
}
