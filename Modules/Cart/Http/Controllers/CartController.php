<?php

namespace Modules\Cart\Http\Controllers;

use App\Models\Cart;
use Illuminate\Routing\Controller;
use Modules\Manage\Traits\ManageControllerTrait;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;

class CartController extends Controller
{
    use ManageControllerTrait;
    use CartControllerRequestSaveTrait;
    protected $base_model;
    protected $model_name  = "Cart";
    protected $view_folder = "cart::manage";



    public function __construct()
    {
        $this->base_model = new Cart();
    }

    protected function view($file_name, $arguments = [])
    {
        return $this->safeView("$this->view_folder.$file_name", $arguments);
    }



    /**
     * prepare browse tabs
     *
     * @return array
     */
    protected function browserTabs()
    {
        $array        = [
             'pending',
             'under_packing',
             'under_delivery',
             'under_payment',
             'not_raised',
             'support_request',
             'delivered',
             'archive',
        ];
        $result       = [];
        $query_string = "?".http_build_query(request()->toArray());


        foreach ($array as $item) {
            $result[] = [
                 'url'     => "browse/" . $item . $query_string,
                 'caption' => trans_safe("cart::status.$item"),
            ];
        }

        return $result;
    }



    /**
     * get the index page of the carts
     *
     * @param string                 $request_tab
     * @param SimpleYasnaListRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($request_tab = 'pending', SimpleYasnaListRequest $request)
    {
        /*-----------------------------------------------
        | Model ...
        */
        $builder = $this->base_model
             ->elector([
                  "criteria" => $request_tab,
                  "user"     => $request->user,
             ])
             ->orderBy('created_at', 'desc')
        ;
        $models  = $builder->paginate(user()->preference('max_rows_per_page'));
        $tabs    = $this->browserTabs();


        /*-----------------------------------------------
        | Page ...
        */
        $page = [
             ["carts", trans("cart::carts.title")],
             [$request_tab, trans("cart::status.$request_tab")],
        ];

        /*-----------------------------------------------
        | View ...
        */
        return $this->view("browse", compact('page', 'models', 'tabs', 'request_tab'));
    }



    public function viewForm($model)
    {

        /*-----------------------------------------------
        | Privileges ...
        */
        if ($model->cannot('view')) {
            return $this->abort(403);
        }

        /*-----------------------------------------------
        | View ...
        */
        return $this->view('view', compact('model'));
    }

    public function processForm($model)
    {

        /*-----------------------------------------------
        | Privileges ...
        */
        if ($model->cannot('process')) {
            return $this->abort(403);
        }

        /*-----------------------------------------------
        | View ...
        */
        return $this->view('process', compact('model'));
    }
}
