<?php
namespace Modules\Cart\Http\Controllers;

use Modules\Cart\Http\Requests\ProcessSaveRequest;

trait CartControllerRequestSaveTrait
{
    protected $request;
    protected $model;

    public function saveProcess(ProcessSaveRequest $request)
    {
        $method_name = camel_case("saveProcess_" . $request->type);
        if (!method_exists($this, $method_name)) {
            return $this->jsonFeedback($method_name . ' does not exist!');
        }

        $this->request = $request;
        $this->model   = $request->model;

        return $this->$method_name();
    }


    protected function saveProcessStatus()
    {
        $done = $this->model->applyStatus($this->request->new_status, $this->request->remarks);

        return $this->saveProcessFeedback($done, [
            'danger_message' => trans("cart::records.message.status_not_applicable"),
        ]);
    }

    protected function saveProcessRemark()
    {
        $done = $this->model->record->create('remark', $this->request->remarks);

        return $this->saveProcessFeedback($done);
    }

    protected function saveProcessPayment()
    {
        $done = $this->model->applyPaymentStatus($this->request->payment_status, $this->request->remarks);

        return $this->saveProcessFeedback($done, [
            'danger_message' => trans("cart::records.message.status_not_applicable"),
        ]);
    }

    protected function saveProcessDiscount()
    {
        $current_value = $this->model->added_discount;
        $new_value     = $this->request->added_discount;
        if ($current_value == $new_value) {
            return $this->jsonFeedback(trans_safe("cart::records.validation.discount_unchanged"));
        }

        $done = $this->model->addDiscount($new_value);

        if ($done) {
            $done = $this->model->record->create('discount', $this->request->remarks, $current_value, $new_value);
        }

        return $this->saveProcessFeedback($done);
    }

    protected function saveProcessCheckout()
    {
        $done = $this->model->checkout();
        if ($done) {
            $done = $this->model->record->create('status', $this->request->remarks, 'not_raised', 'pending');
        }

        return $this->saveProcessFeedback($done, [
            'danger_message' => trans("cart::records.message.cart_not_raisable") ,
        ]);
    }

    protected function saveProcessConfirm()
    {
        $done = $this->model->confirm() ;
        if ($done) {
            $done = $this->model->record->create('status', $this->request->remarks, 'pending', 'under_packing');
        }

        return $this->saveProcessFeedback($done, [
            'danger_message' => trans("cart::records.message.status_not_applicable") ,
        ]);
    }

    protected function saveProcessUndo()
    {
        $done = $this->model->checkoutUndo() ;
        if ($done) {
            $done = $this->model->record->create('status', $this->request->remarks, 'pending', 'not_raised');
        }

        return $this->saveProcessFeedback($done, [
            'danger_message' => trans("cart::records.message.status_not_applicable") ,
        ]);
    }

    protected function saveProcessFeedback($done, array $array = [])
    {
        $hashid = $this->model->hashid;
        $array  = array_default($array, [
            'success_callback'   => "rowUpdate('tblCarts' , '$hashid');masterModal( url('manage/carts/act/$hashid/process') )",
            'success_modalClose' => false,
        ]);

        return $this->jsonAjaxSaveFeedback($done, $array);
    }
}
