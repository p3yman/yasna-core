<?php
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin', 'can:carts'], //@TODO: CanMiddleware
          'prefix'     => 'manage/carts/',
          'namespace'  => module('cart')->controller(),
     ],
     function () {
         Route::get('/update/{item_id}', 'CartController@update');

         Route::get('/process/{model_id}/{action?}', 'ProcessController@index');
         Route::post('/process', 'ProcessController@save')->name('cart-process');
         Route::get('/print/{model_id}', 'PrintController@single');

         Route::get('/act/{model_id}/{action}/{option?}', 'CartController@singleAction');
         Route::get('/browse/search', 'CartController@searchPanel');
         Route::get('/browse/{request_tab?}/search', 'CartController@search');
         Route::get('/browse/{request_tab?}', 'CartController@index');
         Route::get('/', 'CartController@index');

         Route::get("/{id}/transactions/", "TransactionListController@cartsTransaction");

         Route::group(['prefix' => 'save'], function () {
             Route::post('process', 'CartController@saveProcess')
                  ->name('cart-save-process')
             ; // <~~ TODO: Remove When Refactore Complete!
         });
     }
);

/*
|--------------------------------------------------------------------------
| order Finder Widget
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth'],
          'prefix'     => 'manage/cart',
          'namespace'  => 'Modules\Cart\Http\Controllers',
     ],
     function () {
         Route::get('/search-order', 'WidgetsController@searchCart')->name('search-order');
     });
