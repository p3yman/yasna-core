<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("cart")) {
    /**
     * get a cart model instance
     * 
     * @param string|int $needle
     * @param bool       $with_trashed
     *
     * @return \App\Models\Cart
     */
    function cart($needle, $with_trashed = false)
    {
        if ($with_trashed) {
            return model('cart')->withTrashed()->find($needle);
        } else {
            return model('cart')->find($needle);
        }
    }
}

if (!function_exists("fireCartTestEmail")) {

    function fireCartTestEmail()
    {
        event(new \Modules\Cart\Events\CartCheckedOut(model('cart', 1)));
    }
}
