<?php

namespace Modules\Cart\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Services\YasnaModel;
use Morilog\Jalali\Facades\jDate;

class CartRecord extends YasnaModel
{
    use SoftDeletes;

    public static $meta_fields = [];
    protected $guarded     = ['id'];

    protected $types       = [
         'status',
         'remarks',
         'edit',
         'payment',
         'discount',
         'confirm',
         'checkout',
         'checkoutUndo',
         'pending-notification',
         'unpaid-notification',
    ];
    protected $types_combo = [];



    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function cart()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Cart');
    }



    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */
    public function getTitleAttribute()
    {
        $type     = trans("cart::records.type-title.$this->type");
        $new_ting = null;

        switch ($this->type) {
            case 'status':
            case 'payment':
                $new_ting = trans("cart::status.$this->new_thing");
                break;

            case 'discount':
                $new_ting = pd(number_format($this->new_thing)) . SPACE . $this->cart->currency()->title;
                break;
        }

        if ($new_ting) {
            return "$type ($new_ting)";
        } else {
            return $type;
        }
    }



    public function getRelativeTimeAttribute()
    {
        return pd(jDate::forge($this->created_at)->ago());
    }



    public function getRemarksAttribute($original)
    {
        return trim($original);
    }



    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */
    public function create($type, $remarks, $old_thing = null, $new_thing = null)
    {
        /*-----------------------------------------------
        | Safety ...
        */
        if (not_in_array($type, $this->types)) {
            return false;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->type       = $type;
        $this->old_thing  = $old_thing;
        $this->new_thing  = $new_thing;
        $this->remarks    = $remarks;
        $this->created_by = user()->id;
        return $this->save();
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    public function typesCombo()
    {
        $this->typesCombo_add('checkout', $this->cart->isNotRaised());
        $this->typesCombo_add('confirm', $this->cart->isPending());
        $this->typesCombo_add('status');
        $this->typesCombo_add('remark');
        //$this->typesCombo_add('edit', $this->cart->can('edit'));
        $this->typesCombo_add('payment', $this->cart->isRaised());
        $this->typesCombo_add('discount', $this->cart->isNotPaid());
        $this->typesCombo_add('undo', $this->cart->isPending());

        return $this->types_combo;
    }



    protected function typesCombo_add($item, $condition = true)
    {
        if (!$condition) {
            return false;
        }

        $this->types_combo[] = [
             $item,
             trans_safe("cart::records.type.$item"),
        ];

        return true;
    }
}
