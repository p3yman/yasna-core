<?php

namespace Modules\Cart\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Cart\Entities\Traits\CourierDescriptionTrait;
use Modules\Cart\Entities\Traits\CourierPriceTrait;
use Modules\Shop\Entities\Traits\WareTitleTrait;
use Modules\Yasna\Services\YasnaModel;

class Courier extends YasnaModel
{
    use SoftDeletes;
    use WareTitleTrait, CourierPriceTrait, CourierDescriptionTrait;



    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function carts()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Cart');
    }



    /**
     * Main Meta Fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return ['titles', 'prices', 'descriptions'];
    }
}
