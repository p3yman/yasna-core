<?php

namespace Modules\Cart\Entities;

use App\Models\Order;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Cart\Entities\Traits\CartActionsTrait;
use Modules\Cart\Entities\Traits\CartCodeTrait;
use Modules\Cart\Entities\Traits\CartCouponTrait;
use Modules\Cart\Entities\Traits\CartElectorTrait;
use Modules\Cart\Entities\Traits\CartOfficialBillTrait;
use Modules\Cart\Entities\Traits\CartRecordsTrait;
use Modules\Cart\Entities\Traits\CartResourcesTrait;
use Modules\Cart\Entities\Traits\CartScopeTrait;
use Modules\Cart\Entities\Traits\CartSmartSearchTrait;
use Modules\Cart\Entities\Traits\CartSelectorTrait;
use Modules\Cart\Entities\Traits\CartShippingTrait;
use Modules\Cart\Entities\Traits\CartStatusTrait;
use Modules\Payment\Services\Entity\TransactionsTrait;
use Modules\Shipping\Services\Shippable;
use Modules\Shop\Services\Currency;
use Modules\Yasna\Services\ModelTraits\YasnaStatusTrait;
use Modules\Yasna\Services\YasnaModel;

class Cart extends YasnaModel implements Shippable
{
    use SoftDeletes;
    use YasnaStatusTrait;
    use CartSelectorTrait, CartStatusTrait, CartCodeTrait, CartActionsTrait, CartRecordsTrait;
    use CartCouponTrait;
    use CartShippingTrait;
    use CartOfficialBillTrait;
    use CartSmartSearchTrait;
    use CartResourcesTrait;
    use CartElectorTrait;
    use CartScopeTrait;
    use TransactionsTrait;

    protected $guarded = ['id'];



    /*
     |--------------------------------------------------------------------------
     | Relations
     |--------------------------------------------------------------------------
     |
     */
    public function orders()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Order');
    }



    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'User');
    }



    /**
     * Safely returns the first user, selected by Laravel relation method `user()`
     *
     * @return \App\Models\User
     */
    public function getUserAttribute()
    {
        $user = $this->user()->first();
        if (!$user) {
            $user = user(-1);
        }

        return $user;
    }



    public function courier()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Courier');
    }



    public function currency()
    {
        return new Currency($this->currency);
    }



    public function address()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Address');
    }



    public function getAddressTextAttribute()
    {
        if ($this->address and $this->address->exists) {
            return trim($this->address->full_text);
        }

        return null;
    }



    public function mainMetaFields()
    {
        return ['original_delivery_price'];
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function can($permission, $user_role = 'admin')
    {
        /*-----------------------------------------------
        | Special Cases ...
        */
        switch ($permission) {
        }

        /*-----------------------------------------------
        | Final Check ...
        */
        return user()->as($user_role)->can("carts.$permission");
    }



    public function cannot($permission, $user_role = 'admin')
    {
        return !$this->can($permission, $user_role);
    }



    public function isRaisable()
    {
        return $this->isNotRaised() and $this->revalidate();
    }



    public function isNotRaisable()
    {
        return !$this->isRaisable();
    }



    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */
    public function getCourierDiscountAttribute()
    {
        $original_price = $this->getmeta('original_delivery_price');
        return max(0, $original_price - $this->delivery_price);
    }



    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */
    public function refreshCalculations()
    {
        $this->refreshCouponDiscountCalculation();

        $total_items     = $this->orders()->count();
        $delivered_items = $this->orders()->whereNotNull('delivered_at')->count();
        $original_price  = $this->orders()->sum('original_price');
        $sale_price      = $this->orders()->sum('sale_price');
        $tax_percent     = $this->applicableTaxPercent();

        $before_tax = $sale_price + $this->delivery_price - $this->added_discount;
        $tax_amount = round($before_tax * $tax_percent / 100);

        $custom_price    = $this->getMeta('heavy_delivery_price') ?? 0;
        $invoiced_amount = $before_tax + $tax_amount + $custom_price;


        return $this->update([
             'total_items'     => $total_items,
             'delivered_items' => $delivered_items,
             'original_price'  => $original_price,
             'sale_price'      => $sale_price,
             'tax_amount'      => $tax_amount,
             'invoiced_amount' => $invoiced_amount,
        ]);
    }



    /**
     * @param $amount
     *
     * @return bool
     * @deprecated
     */
    public function addDiscount($amount)
    {
        return $this->setDiscount($amount);
        //$done = $this->update([
        //     'added_discount' => $amount,
        //]);
        //if ($done) {
        //    $this->refreshCalculations();
        //}
        //
        //return $done;
    }



    public function addOrder($hashid_or_id, $count = 1)
    {
        /*-----------------------------------------------
        | Retrieve Record ...
        */
        $ware = model('ware', $hashid_or_id);
        if (!$ware or !$ware->exists) {
            return false;
        }

        /*-----------------------------------------------
        | Check Availability ...
        */
        if ($ware->in($this->locale)->in($this->currency)->isNotPurchasable()) {
            return false;
        }

        /*-----------------------------------------------
        | Insert or Update ...
        */
        $order = $this->orders()->whereWareId($ware->id)->first();
        if ($order and $order->exists) {
            $done = $order->addAnother($count);
        } else {
            $done = Order::addNew($this, $ware, $count);
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $done;
    }



    public function removeOrder($hashid_or_id)
    {
        $order = model('order', $hashid_or_id);

        if ($order->cart_id != $this->id) {
            return false;
        }

        return $order->remove();
    }



    public function addCourier($hashid_or_id, $apply_percent = 1)
    {
        /*-----------------------------------------------
        | Retrieve Record ...
        */
        $courier = model('courier', $hashid_or_id);
        if (!$courier or !$courier->exists) {
            return false;
        }

        /*-----------------------------------------------
        | Check Availability ...
        */
        if ($courier->hasnotCurrency($this->currency)) {
            return false;
        }

        /*-----------------------------------------------
        | Calculations ...
        */
        $original_price = $courier->priceIn($this->currency);
        $price          = round($original_price * $apply_percent);

        /*-----------------------------------------------
        | Insert or Update ...
        */
        $done = $this->batchSave([
             'original_delivery_price' => $original_price,
             'courier_id'              => $courier->id,
             'delivery_price'          => $price,
        ]);
        $done = $this->refreshCalculations();

        /*-----------------------------------------------
        | Return ...
        */
        return $done;
    }



    public function removeCourier()
    {
        $done = $this->batchSave([
             'original_delivery_price' => 0,
             'courier_id'              => 0,
             'delivery_price'          => 0,
        ]);

        if ($done) {
            $done = $this->refreshCalculations();
        }

        return $done;
    }



    public function applicableTaxPercent()
    {
        /*-----------------------------------------------
        | VAT ...
        */

        $percent = intval(get_setting('tax_vat_percent'));


        /*-----------------------------------------------
        | Return ...
        */
        return $percent;
    }



    public function mergeWith($hashid_or_id)
    {
        /*-----------------------------------------------
        | Retrieve Record ...
        */
        $incoming_cart = model('cart', $hashid_or_id);
        if (!$incoming_cart or !$incoming_cart->exists) {
            return false;
        }

        /*-----------------------------------------------
        | Safety ...
        */
        if ($this->isRaised() or $incoming_cart->isRaised()) {
            return false;
        }

        /*-----------------------------------------------
        | Transformation ...
        */
        $incoming_cart->orders()->update([
             'cart_id'  => $this->id,
             'user_id'  => $this->user_id,
             'currency' => $this->currency,
             'locale'   => $this->locale,
        ])
        ;
        $incoming_cart->records()->update([
             'cart_id' => $this->id,
        ])
        ;
        $done = $incoming_cart->forceDelete();
        $this->revalidate();

        /*-----------------------------------------------
        | Return ...
        */
        return boolval($done);
    }



    public function revalidate()
    {
        $processed_ids    = [];
        $invalidity_found = false;
        foreach ($this->orders()->orderBy('created_at', 'desc')->get() as $order) {

            /*-----------------------------------------------
            | Remove Duplications ...
            */
            if (in_array($order->ware_id, $processed_ids)) {
                $order->forceDelete();
            } else {
                $processed_ids[] = $order->ware_id;
            }

            /*-----------------------------------------------
            | Price Calculations ...
            */
            $order->refreshCalculations();
            $this->refreshCalculations();

            /*-----------------------------------------------
            | Invalidity ...
            */
            if ($order->sale_price <= 0) {
                $invalidity_found = true;
            }
        }

        return !$invalidity_found;
    }



    public function inventoryPickup()
    {
        $done = 0;
        foreach ($this->orders as $order) {
            $done += $order->inventoryPickup();
        }

        return $done;
    }



    public function inventoryPutBack()
    {
        $done = 0;

        foreach ($this->orders as $order) {
            $done += $order->inventoryPutBack();
        }
        return $done;
    }



    public function delete()
    {
        $this->orders()->delete();
        return parent::delete();
    }



    public function undelete()
    {
        $this->orders()->restore();
        return parent::undelete();
    }



    public function hardDelete()
    {
        if ($this->user_id) {
            return false;
        }

        $this->orders()->forceDelete();
        return parent::hardDelete();
    }



    /**
     * @return bool
     * @deprecated
     */
    public function forceDelete()
    {
        return $this->hardDelete();
    }
}
