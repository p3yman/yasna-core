<?php

namespace Modules\Cart\Entities;

use App\Models\Inventory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Cart\Entities\Traits\OrderResourcesTrait;
use Modules\Yasna\Services\YasnaModel;

class Order extends YasnaModel
{
    use SoftDeletes;
    use OrderResourcesTrait;

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function posttype()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Posttype');
    }



    public function post()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Post');
    }



    public function ware()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Ware');
    }



    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'User');
    }



    public function cart()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Cart');
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function isRejected()
    {
        return boolval($this->rejected_at);
    }



    public function inventory()
    {
        return Inventory::ask()->for($this->ware_id);
    }



    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */
    public function addAnother($count)
    {
        return $this->alterCount($count + $this->count);
    }



    public static function addNew($cart, $ware, $count)
    {
        $single_original_price = 0;
        $single_sale_price     = 0;

        if ($ware->in($cart->currency)->in($cart->locale)->isPurchasable()) {
            $single_original_price = $ware->price('original')->withPackage()->in($cart->currency)->get();
            $single_sale_price     = $ware->price('sale')->withPackage()->in($cart->currency)->get();
        }

        $done = self::create([
             'posttype_id'           => $ware->posttype_id,
             'post_id'               => $ware->postIn($cart->locale)->id,
             'ware_id'               => $ware->id,
             'cart_id'               => $cart->id,
             'user_id'               => $cart->user_id,
             'currency'              => $cart->currency,
             'locale'                => $cart->locale,
             'count'                 => $count,
             'single_original_price' => $single_original_price,
             'single_sale_price'     => $single_sale_price,
             'original_price'        => $single_original_price * $count,
             'sale_price'            => $single_sale_price * $count,
        ]);

        if ($done) {
            $cart->refreshCalculations();
        }

        return $done;
    }



    public function alterCount($new_count)
    {
        if (!$new_count) {
            $this->remove();
        }
        $done = $this->update([
             'count'          => $new_count,
             'original_price' => $this->single_original_price * $new_count,
             'sale_price'     => $this->single_sale_price * $new_count,
        ]);

        if ($done) {
            $this->cart->refreshCalculations();
        }


        return $done;
    }



    public function remove()
    {
        $done = $this->delete();
        if ($done) {
            $this->cart->refreshCalculations();
        }

        return $done;
    }



    public function refreshCalculations()
    {
        $ware                  = model('ware',
             $this->ware_id); // <~~ I didn't use Eloquent relation, on a purpose. This helper function returns a new model if not found.
        $single_original_price = 0;
        $single_sale_price     = 0;

        if ($ware->in($this->currency)->in($this->locale)->isPurchasable()) {
            $single_original_price = $ware->price('original')->withPackage()->in($this->currency)->get();
            $single_sale_price     = $ware->price('sale')->withPackage()->in($this->currency)->get();
        }

        return $this->update(
             [
                  'single_original_price' => $single_original_price,
                  'single_sale_price'     => $single_sale_price,
                  'original_price'        => $single_original_price * $this->count,
                  'sale_price'            => $single_sale_price * $this->count,
             ]
        );
    }



    public function inventoryPickup()
    {
        return $this->inventory()
                    ->type('sold_out')
                    ->description("cart:" . $this->cart_id)
                    ->in($this->currency)
                    ->withPackage()
                    ->alter($this->count)
             ;
    }



    public function inventoryPutBack()
    {
        $ware = $this->ware;
        if ($ware->hasnotInventorySystem()) {
            return true;
        }

        $done = Inventory::where('description', 'cart:' . $this->cart_id)
                         ->where('ware_id', $ware->id)
                         ->orWhere('ware_id', $ware->package_id)
                         ->delete()
        ;
        $ware->refreshCurrentInventory();

        return boolval($done);
    }
}
