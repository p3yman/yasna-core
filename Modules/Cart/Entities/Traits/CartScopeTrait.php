<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/2/19
 * Time: 3:51 PM
 */

namespace Modules\Cart\Entities\Traits;


use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

trait CartScopeTrait
{
    /**
     * Scope for the user.
     *
     * @param Builder         $builder
     * @param User|int|string $user
     *
     * @return Builder
     */
    public function scopeForUser(Builder $builder, $user)
    {
        if (!($user instanceof User)) {
            $user = user($user);
        }

        return $builder->where('user_id', $user->id);
    }



    /**
     * Scope for Active Carts
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeWhereActive(Builder $builder)
    {
        return $builder->whereNull('raised_at');
    }



    /**
     * Scope for Inactive Carts
     *
     * @param Builder $builder
     *
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeWhereInactive(Builder $builder)
    {
        return $builder->whereNotNull('raised_at');
    }
}
