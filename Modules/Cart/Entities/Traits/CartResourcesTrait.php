<?php

namespace Modules\Cart\Entities\Traits;

use App\Models\Address;
use App\Models\Order;
use App\Models\ShippingMethod;
use Carbon\Carbon;

trait CartResourcesTrait
{
    /**
     * boot CartResourcesTrait
     *
     * @return void
     */
    public static function bootCartResourcesTrait()
    {
        static::addDirectResources([
             "locale",
             "currency",
             "total_items",
             "code",
             "status",
             "payment_status",
             "user_id",
             'original_price',
             'sale_price',
             'delivery_price',
             'added_discount',
             'tax_amount',
             'invoiced_amount',
             'paid_amount',
             "raised_at",
             "raised_by",
             "confirmed_at",
             "confirmed_by",
             "disbursed_at",
             "disbursed_by",
             "paid_at",
             "paid_by",
             "sent_at",
             "sent_by",
             'delivered_at',
             'delivered_by',
             'rejected_at',
             'rejected_by',
             'closed_at',
             'closed_by',
        ]);
    }



    /**
     * get IsConfirmed resource.
     *
     * @return int
     */
    protected function getIsConfirmedResource()
    {
        if ($this->isNotRaised()) {
            return 0;
        }

        return intval($this->isConfirmed());
    }



    /**
     * get IsDisbursed resource.
     *
     * @return int
     */
    protected function getIsDisbursedResource()
    {
        if ($this->isNotRaised()) {
            return 0;
        }

        return intval($this->isDisbursed());
    }



    /**
     * get IsPaid resource.
     *
     * @return int
     */
    protected function getIsPaidResource()
    {
        if ($this->isNotRaised()) {
            return 0;
        }

        return intval($this->isPaid());
    }



    /**
     * get IsSent resource.
     *
     * @return int
     */
    protected function getIsSentResource()
    {
        if ($this->isNotRaised()) {
            return 0;
        }

        return intval($this->isSent());
    }



    /**
     * get IsDelivered resource.
     *
     * @return int
     */
    protected function getIsDeliveredResource()
    {
        if ($this->isNotRaised()) {
            return 0;
        }

        return intval($this->isDelivered());
    }



    /**
     * get IsRejected resource.
     *
     * @return int
     */
    protected function getIsRejectedResource()
    {
        if ($this->isNotRaised()) {
            return 0;
        }

        return intval($this->isRejected());
    }



    /**
     * get isClosed resource.
     *
     * @return int
     */
    protected function getIsClosedResource()
    {
        if ($this->isNotRaised()) {
            return 0;
        }

        return intval($this->isClosed());
    }



    /**
     * get Items resource.
     *
     * @return array
     */
    protected function getItemsResource()
    {
        return $this->getResourceFromCollection($this->orders);
    }



    /**
     * get Address resource.
     *
     * @return array
     */
    protected function getAddressResource()
    {
        if (module('Cart')->cannotUse("Address")) {
            return null;
        }

        $address = $this->address;

        return $address ? $address->toResource() : null;
    }



    /**
     * get ShippingMethod resource.
     *
     * @return array
     */
    protected function getShippingMethodResource()
    {
        /** @var ShippingMethod|null $method */
        $method = $this->shipping_method;

        return $method ? $method->toResource() : null;
    }



    /**
     * get UserName resource.
     *
     * @return string
     */
    protected function getUserResource()
    {
        return $this->getResourceForPersons($this->user_id);
    }



    /**
     * get IsActive resource.
     *
     * @return int
     */
    protected function getIsActiveResource()
    {
        return intval($this->isNotRaised());
    }



    /**
     * get IsRaised resource.
     *
     * @return int
     */
    protected function getIsRaisedResource()
    {
        return intval($this->isRaised());
    }

}
