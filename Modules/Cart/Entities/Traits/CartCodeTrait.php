<?php
namespace Modules\Cart\Entities\Traits;

trait CartCodeTrait
{
    /*
    |--------------------------------------------------------------------------
    | Selector
    |--------------------------------------------------------------------------
    |
    */

    public function findByCode($code)
    {
        return $this->find($this->code2id($code)) ;
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */
    public function getCodeAttribute()
    {
        if ($this->id) {
            return $this->id2Code();
        } else {
            return $this->codePrefix() . "-NEW";
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Private Factory
    |--------------------------------------------------------------------------
    |
    */
    protected function codePrefix()
    {
        return "PDK-";// get_setting("cart_prefix") ;
    }

    protected function codeMargin()
    {
        return 5678 ;
    }



    /**
     * Converts cart code to id
     *
     * @param string $code
     *
     * @return int
     */
    public function code2id(string $code)
    {
        /*-----------------------------------------------
        | Strip Prefix ...
        */
        $code = intval(str_replace($this->codePrefix(), null, $code));

        /*-----------------------------------------------
        | Undo Margin ...
        */
        $id = $code - $this->codeMargin() ;

        /*-----------------------------------------------
        | Actual Find ...
        */
        return $id ;
    }

    protected function id2Code()
    {
        $prefix = $this->codePrefix() ;
        $margin = $this->codeMargin() ;

        return $prefix . strval($margin + $this->id);
    }
}
