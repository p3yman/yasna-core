<?php
namespace Modules\Cart\Entities\Traits;

trait PosttypeCartTrait
{
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function orders()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Order') ;
    }
}
