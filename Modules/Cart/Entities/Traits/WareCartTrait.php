<?php

namespace Modules\Cart\Entities\Traits;

use Illuminate\Database\Schema\Builder;
use Illuminate\Support\Facades\DB;

trait WareCartTrait
{
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function orders()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Order');
    }



    /**
     * get wares with most sale
     *
     * @return Builder
     */
    public static function bestSellers()
    {
        return model('ware')
             ->select(DB::raw("SUM(orders.count) as order_count,wares.*"))
             ->join('orders', "wares.id", "=", "orders.ware_id")
             ->whereHas('orders', function ($q) {
                 $q->whereHas('cart', function ($q) {
                     $q->whereNotNull('raised_at');
                 });
             })->orderBy(DB::raw('SUM(orders.count)'), 'DESC')->groupBy('ware_id')
             ;
    }
}
