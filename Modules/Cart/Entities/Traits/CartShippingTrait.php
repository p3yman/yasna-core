<?php

namespace Modules\Cart\Entities\Traits;

use App\Models\ShippingMethod;
use Modules\Shipping\Entities\Traits\ShippingTrait;
use Modules\Shipping\Services\ShippingInvoiceObject;

/**
 * @property string $raised_at
 * @property string $created_at
 * @property int    $user_id
 */
trait CartShippingTrait
{
    use ShippingTrait;



    /**
     * @inheritdoc
     */
    public function toShippingInvoice(): ShippingInvoiceObject
    {
        $invoice = new ShippingInvoiceObject();

        $invoice->setEffectiveDate($this->raised_at ? $this->raised_at : $this->created_at);
        $invoice->setOriginalInstance($this);
        $invoice->setUser($this->user_id);

        foreach ($this->orders as $order) {
            $invoice->addItem([
                 "id"      => $order->id,
                 "order"   => $order,
                 "ware_id" => $order->ware_id,
                 "post_id" => $order->post_id,
            ]);
        }

        //    TODO: Do the rest!

        return $invoice;
    }



    /**
     * Standard One-to-One Relation with Shipping Methods.
     *
     * @return ShippingMethod|null
     */
    public function shippingMethod()
    {
        return $this->belongsTo(ShippingMethod::class);
    }



    /**
     * Accessor for the `shippingMethod()` Relation
     *
     * @return ShippingMethod|null
     */
    public function getShippingMethodAttribute()
    {
        return $this->getRelationValue('shippingMethod');
    }



    /**
     * Sets the specified shipping method as the shipping method of this cart.
     *
     * @param ShippingMethod|string|int $method An Object of the ShippingMethod Model or its Identifier
     *
     * @return bool
     */
    public function addShippingMethod($method)
    {
        if (!($method instanceof ShippingMethod)) {
            $method = model('shipping-method', $method);
        }

        if ($this->canNotBeShippedBy($method->id)) {
            return false;
        }

        $price = $method->priceFor($this->toShippingInvoice());
        $done  = $this->update([
             'shipping_method_id' => $method->id,
             'delivery_price'     => $price,
        ]);

        $this->refreshCalculations();

        return $done;
    }



    /**
     * Removes the applied shipping method and its effects.
     *
     * @return mixed
     */
    public function removeShippingMethod()
    {
        $done = $this->update([
             'original_delivery_price' => 0,
             'courier_id'              => 0,
             'delivery_price'          => 0,
        ]);

        if ($done) {
            $done = $this->refreshCalculations();
        }

        return $done;
    }



    /**
     * Checks if this cart can be shipped by the shipping method with the specified id.
     *
     * @param int $shipping_method_id
     *
     * @return bool
     */
    public function canBeShippedBy(int $shipping_method_id): bool
    {
        $available_ids = array_column($this->availableShippingMethods(), 'id');

        return in_array($shipping_method_id, $available_ids);
    }



    /**
     * Checks if this cart can not be shipped by the shipping method with the specified id.
     *
     * @param int $shipping_method_id
     *
     * @return bool
     */
    public function canNotBeShippedBy(int $shipping_method_id): bool
    {
        return !$this->canBeShippedBy($shipping_method_id);
    }
}
