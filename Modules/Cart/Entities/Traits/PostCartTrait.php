<?php
namespace Modules\Cart\Entities\Traits;

trait PostCartTrait
{
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function orders()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Order') ;
    }
}
