<?php
namespace Modules\Cart\Entities\Traits;

trait UserCartTrait
{
    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function orders()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Order');
    }

    public function carts()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Cart');
    }
}
