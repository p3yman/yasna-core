<?php
namespace Modules\Cart\Entities\Traits;

trait CartSelectorTrait
{
    protected $query;

    public function count($criteria)
    {
        return $this->selector([
            'criteria' => $criteria ,
        ])->count() ;
    }

    public function selector($parameters = [])
    {
        /*-----------------------------------------------
        | Normalize ...
        */
        $parameters = array_normalize($parameters, [
            'code'     => null,
            'criteria' => "all",
        ]);

        /*-----------------------------------------------
        | Process ...
        */
        $this->query = self::whereNotNull('id');
        foreach ($parameters as $parameter => $switch) {
            $method_name = "selector_$parameter";
            if (method_exists($this, $method_name) and $switch) {
                $this->$method_name($switch);
            }
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $this->query;
    }

    public function selector_criteria($parameter)
    {
        switch ($parameter) {
            case 'pending':
                return $this->query->whereNotNull('raised_at')->whereNull('confirmed_at')->whereNull('closed_at');

            case "under_packing":
                return $this->query->whereNotNull('confirmed_at')->whereNull('sent_at')->whereNull('closed_at');

            case 'support_request':
                return $this->query->whereNotNull('rejected_at')->whereNull('closed_at')->whereNull('closed_at');

            case 'under_delivery':
                return $this->query->whereNotNull('sent_at')->whereNull('delivered_at')->whereNull('closed_at');

            case 'under_payment':
                return $this->query->whereNull('paid_at')->whereNotNull('raised_at')->whereNull('closed_at');

            case 'not_raised':
                return $this->query->whereNull('raised_at')->whereNull('closed_at');

            case 'delivered':
                return $this->query->whereNotNull('delivered_at')->whereNull('closed_at');

            case 'archive':
                return $this->query->whereNotNull('closed_at');

            case 'all':
                return $this->query;

            default:
                return $this->query->where('id', 'invalid');

        }
    }

    public function selector_code($parameter)
    {
        $id = $this->code2id($parameter) ;

        return $this->query->where('id', $id) ;
    }
}
