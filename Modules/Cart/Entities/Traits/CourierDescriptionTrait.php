<?php

namespace Modules\Cart\Entities\Traits;

trait CourierDescriptionTrait
{
    /*
    |--------------------------------------------------------------------------
    | Get
    |--------------------------------------------------------------------------
    |
    */


    public function getDescriptionsAttribute()
    {
        return $this->getMeta('descriptions');
    }


    public function descriptionIn($locale)
    {
        $descriptions = $this->descriptions;
        if ($locale == 'first') {
            return array_first($descriptions);
        }
        if (isset($descriptions[$locale])) {
            return $descriptions[$locale];
        }

        return false;
    }

    public function getDescriptionAttribute()
    {
        if ($this->isIn('fa')) {
            return $this->descriptionIn('fa');
        } else {
            return $this->descriptionIn('first');
        }
    }
}
