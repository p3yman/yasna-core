<?php
namespace Modules\Cart\Entities\Traits;

use Carbon\Carbon;

trait CartStatusTrait
{
    protected $status_combo         = [];
    protected $payment_status_combo = [];



    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */
    public function getStatusAttribute()
    {
        if ($this->isClosed()) {
            return 'archive';
        }
        if ($this->isNotRaised()) {
            return 'not_raised';
        }
        if ($this->isPending()) {
            return 'pending';
        }
        if ($this->isNotSent()) {
            return 'under_packing';
        }
        if ($this->isUnderDelivery()) {
            return 'under_delivery';
        }
        if ($this->isDelivered()) {
            return 'delivered';
        }
        if ($this->isRejected()) {
            return 'support_request';
        }

        return 'unknown';
    }



    public function getPaymentStatusAttribute()
    {
        if ($this->isNotRaised()) {
            return null;
        }
        if ($this->isNotDisbursed()) {
            return 'under_payment';
        } elseif ($this->isNotPaid()) {
            return 'under_payment_confirmation';
        } elseif ($this->isPaid()) {
            return 'paid';
        }


        return 'unknown';
    }



    public function getPaymentStatusTextAttribute()
    {
        return $this->statusText($this->payment_status);
    }



    public function getPaymentStatusColorAttribute()
    {
        return $this->statusColor($this->payment_status);
    }



    public function getPaymentStatusIconAttribute()
    {
        return $this->statusIcon($this->payment_status);
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */

    public function isRaised()
    {
        return boolval($this->raised_at);
    }



    public function isNotRaised()
    {
        return !$this->isRaised();
    }



    public function isConfirmed()
    {
        return boolval($this->confirmed_at);
    }



    public function isNotConfirmed()
    {
        return !$this->isConfirmed();
    }



    public function isPending()
    {
        return $this->isRaised() and !$this->isConfirmed();
    }



    public function isNotPending()
    {
        return !$this->isPending();
    }



    public function isUnderAction()
    {
        return $this->isRaised() and !$this->isSent();
    }



    public function isDisbursed()
    {
        return boolval($this->disbursed_at);
    }



    public function isNotDisbursed()
    {
        return !$this->isDisbursed();
    }



    public function isPaid()
    {
        return boolval($this->paid_amount);
    }



    public function isNotPaid()
    {
        return !$this->isPaid();
    }



    public function isUnderPayment()
    {
        return $this->isRaised() and $this->isNotPaid();
    }



    public function isSent()
    {
        return boolval($this->sent_at);
    }



    public function isNotSent()
    {
        return $this->isRaised() and !$this->isSent();
    }



    public function isDelivered()
    {
        return boolval($this->delivered_at);
    }



    public function isUnderDelivery()
    {
        return $this->isSent() and !$this->isDelivered();
    }



    public function isRejected()
    {
        return boolval($this->rejected_at);
    }



    public function isClosed()
    {
        return boolval($this->closed_at);
    }



    public function isOpen()
    {
        return !$this->isClosed();
    }



    public function isCanceled()
    {
        if ($this->isClosed() and $this->created_by == $this->closed_by) {
            ;
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    |
    */
    public function applyStatus($new_status)
    {
        /*-----------------------------------------------
        | Preparations ...
        */

        $old_status = $this->status;
        $new_status = snake_case(studly_case($new_status));
        $method     = camel_case("apply" . $new_status . 'Status');

        /*-----------------------------------------------
        | Checks ...
        */
        if ($old_status == $new_status) {
            //dd('same');
            return false;
        }
        if (!method_exists($this, $method)) {
            //dd("$method doesn't exist");
            return false;
        }

        /*-----------------------------------------------
        | Run ...
        */
        $done = $this->$method();

        /*-----------------------------------------------
        | Feedback & Record Insert ...
        */
        return $done;
    }



    public function applyPaymentStatus($new_status)
    {
        /*-----------------------------------------------
        | Preparations ...
        */

        $old_status = $this->payment_status;
        $new_status = snake_case(studly_case($new_status));
        $method     = camel_case("apply" . $new_status . 'Status');

        /*-----------------------------------------------
        | Checks ...
        */
        if ($old_status == $new_status) {
            //dd('same');
            return false;
        }
        if (!method_exists($this, $method)) {
            //dd("$method doesn't exist");
            return false;
        }

        /*-----------------------------------------------
        | Run ...
        */
        $done = $this->$method();

        /*-----------------------------------------------
        | Feedback & Record Insert ...
        */
        return $done ;
    }



    public function applyNotRaisedStatus()
    {
        return $this->checkoutUndo(); // <~~ For the safety reasons, only works on the models, not currently approved by the admins!
    }



    public function applyPendingStatus()
    {
        if ($this->isNotRaised()) {
            return $this->checkout();
        }

        return $this->update([
             'confirmed_at' => null,
             'sent_at'      => null,
             'delivered_at' => null,
             'rejected_at'  => null,
             'closed_at'    => null,
             'confirmed_by' => 0,
             'sent_by'      => 0,
             'delivered_by' => 0,
             'rejected_by'  => 0,
             'closed_by'    => 0,
        ]);
    }



    public function applyUnderPackingStatus()
    {
        if ($this->isNotConfirmed()) {
            return $this->confirm();
        }

        return $this->update([
             'sent_at'      => null,
             'delivered_at' => null,
             'rejected_at'  => null,
             'closed_at'    => null,
             'sent_by'      => 0,
             'delivered_by' => 0,
             'rejected_by'  => 0,
             'closed_by'    => 0,
        ]);
    }



    public function applyUnderDeliveryStatus()
    {
        if ($this->isNotConfirmed()) {
            return false;
        }
        if ($this->isNotSent()) {
            $this->sent_at = $this->now();
            $this->sent_by = user()->id;
        }

        return $this->update([
             'sent_at'      => $this->sent_at,
             'sent_by'      => $this->sent_by,
             'delivered_at' => null,
             'rejected_at'  => null,
             'closed_at'    => null,
             'delivered_by' => 0,
             'rejected_by'  => 0,
             'closed_by'    => 0,
        ]);
    }



    public function applyDeliveredStatus()
    {
        if (!$this->isConfirmed()) {
            return false;
        }

        if ($this->isNotSent()) {
            $this->sent_at = $this->now();
            $this->sent_by = user()->id;
        }


        return $this->update([
             'sent_at'      => $this->sent_at,
             'delivered_at' => $this->now(),
             'rejected_at'  => null,
             'closed_at'    => null,
             'sent_by'      => $this->sent_by,
             'delivered_by' => $this->now(),
             'rejected_by'  => 0,
             'closed_by'    => 0,
        ]);
    }



    public function applyClosedStatus()
    {
        return $this->update([
             'closed_at' => $this->now(),
             'closed_by' => user()->id,
        ]);
    }



    public function applyOpenStatus()
    {
        return $this->update([
             'closed_at' => null,
             'closed_by' => 0,
        ]);
    }



    public function applyUnderPaymentStatus()
    {
        return $this->update([
             'disbursed_at' => null,
             'paid_at'      => null,
             'disbursed_by' => 0,
             'paid_by'      => 0,
        ]);
    }



    public function applyUnderPaymentConfirmationStatus()
    {
        return $this->update([
             'disbursed_at' => $this->now(),
             'paid_at'      => null,
             'disbursed_by' => user()->id,
             'paid_by'      => 0,
        ]);
    }



    public function applyPaidStatus()
    {
        if ($this->isNotDisbursed()) {
            $this->disbursed_at = $this->now();
            $this->delivered_by = user()->id;
        }

        return $this->update([
             'disbursed_at' => $this->disbursed_at,
             'paid_at'      => $this->now(),
             'disbursed_by' => $this->disturbed_by,
             'paid_by'      => user()->id,
        ]);
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    public function timestamps()
    {
        $data = [];
        foreach ($this->toArray() as $item => $value) {
            if (str_contains($item, '_at')) {
                $data[$item] = $value;
            }
        }

        return $data;
    }



    /**
     * Simply shows an array of all *_by fields, for easy debug purposes.
     * @return array
     */
    public function bys()
    {
        $result = [];
        $data   = $this->toArray();
        foreach ($data as $item => $value) {
            if (str_contains($item, '_by')) {
                if ($value) {
                    $result[$item] = $value . " @ " . $data[str_before($item, '_by') . '_at'];
                } else {
                    $result[$item] = null;
                }
            }
        }
        return $result;
    }



    public function statusOptions()
    {
        $available = ['under_packing', 'under_delivery', 'delivered'];
        $result    = [];

        foreach ($available as $status) {
            $result[] = [
                 $status,
                 $this->statusText($status),
            ];
        }

        return $result;
    }



    public function paymentStatusOptions()
    {
        $available = ['under_payment', 'under_payment_confirmation', 'paid'];
        $result    = [];

        foreach ($available as $status) {
            $result[] = [
                 $status,
                 $this->statusText($status),
            ];
        }

        return $result;
    }


    public function statusCombo()
    {
        $this->statusCombo_add('not_raised', $this->isPending());
        $this->statusCombo_add('pending', !$this->isPending());
        $this->statusCombo_add('under_packing', $this->isPending());
        $this->statusCombo_add('under_delivery', $this->isConfirmed() and !$this->isUnderDelivery());
        $this->statusCombo_add('delivered', $this->isConfirmed() and !$this->isDelivered());
        $this->statusCombo_add('closed', $this->isDelivered() and $this->isPaid() and !$this->isClosed());

        return $this->status_combo;
    }



    public function paymentStatusCombo()
    {
        $this->paymentStatusCombo_add('under_payment');
        $this->paymentStatusCombo_add('under_payment_confirmation');
        $this->paymentStatusCombo_add('paid');

        return $this->payment_status_combo;
    }



    protected function statusCombo_add($item, $condition = true)
    {
        $this->addToStatusCombo('status_combo', $item, $condition);
    }



    protected function paymentStatusCombo_add($item, $condition = true)
    {
        return $this->addToStatusCombo('payment_status_combo', $item, $condition);
    }



    protected function addToStatusCombo($combo_property, $item, $condition = true)
    {
        if (!$condition) {
            return false;
        }

        $this->$combo_property[] = [
             $item,
             trans_safe("cart::status.$item"),
        ];

        return true;
    }
}
