<?php

namespace Modules\Cart\Entities\Traits;

trait CartOfficialBillTrait
{
    /**
     * Checks if the official invoice is supported by the website.
     *
     * @return bool
     */
    public function officialBillIsSupported()
    {
        return get_setting('official_bill_support');
    }



    /**
     * Checks if the official invoice is not supported by the website.
     *
     * @return bool
     */
    public function officialBillIsNotSupported()
    {
        return !$this->officialBillIsSupported();
    }



    /**
     * Returns an array of meta fields based on the official invoice settings.
     *
     * @return array
     */
    public function officialBillMetaFields()
    {
        if ($this->officialBillIsSupported()) {
            return ['official_bill'];
        } else {
            return [];
        }
    }



    /**
     * Sets the value of the official bill meta.
     *
     * @param bool $value
     *
     * @return bool
     */
    public function withOfficialBill(bool $value = true)
    {
        if ($this->officialBillIsNotSupported()) {
            return false;
        }

        return $this->batchSaveBoolean(['official_bill' => intval($value)]);
    }



    /**
     * Unsets the value of the official bill meta.
     *
     * @return bool
     */
    public function withoutOfficialBill()
    {
        return $this->withOfficialBill(false);
    }



    /**
     * Checks if the official invoice is needed for this cart.
     *
     * @return bool
     */
    public function officialBillIsNeeded()
    {
        return boolval($this->getMeta('official_bill'));
    }



    /**
     * Checks if the official invoice is not needed for this cart.
     *
     * @return bool
     */
    public function officialBillIsNotNeeded()
    {
        return !$this->officialBillIsNeeded();
    }



    /**
     * Checks if the official invoice is activated.
     *
     * @return bool
     */
    public function officialBillIsActivated()
    {
        return $this->officialBillIsSupported() and $this->officialBillIsNeeded();
    }



    /**
     * Checks if the official invoice is not activated.
     *
     * @return bool
     */
    public function officialBillIsNotActivated()
    {
        return !$this->officialBillIsActivated();
    }



    /**
     * Returns an array of meta fields based on the official invoice requirenments.
     *
     * @return array
     */
    public function officialBillNeedsMetaFields()
    {
        if ($this->officialBillIsActivated()) {
            return [
                 'company_name',
                 'economical_id',
                 'registration_id',
            ];
        } else {
            return [];
        }
    }
}
