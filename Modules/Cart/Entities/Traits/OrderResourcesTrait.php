<?php

namespace Modules\Cart\Entities\Traits;

trait OrderResourcesTrait
{
    /**
     * boot OrderResourcesTrait
     *
     * @return void
     */
    public static function bootOrderResourcesTrait()
    {
        static::addDirectResources([
             "count",
             "currency",
             "locale",
             "ware_id",
             "post_id",
             "posttype_id",
             'single_original_price',
             'single_sale_price',
             'original_price',
             'sale_price',
        ]);
    }



    /**
     * get WareTitle resource.
     *
     * @return string
     */
    protected function getWareTitleResource()
    {
        $ware = $this->ware;

        return $ware ? $ware->title : null;
    }



    /**
     * get PostTitle resource.
     *
     * @return string
     */
    protected function getPostTitleResource()
    {
        $post = $this->post;

        return $post ? $post->title : null;

    }



    /**
     * get PosttypeTitle resource.
     *
     * @return string
     */
    protected function getPosttypeTitleResource()
    {
        $posttype = $this->posttype;

        return $posttype ? $posttype->titleIn($this->locale) : null;
    }
}
