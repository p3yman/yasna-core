<?php
namespace Modules\Cart\Entities\Traits;

trait CourierPriceTrait
{

    /*
    |--------------------------------------------------------------------------
    | Inquery
    |--------------------------------------------------------------------------
    |
    */
    public function isInCurrency($currency)
    {
        $currency = strtoupper($currency) ;
        return str_contains($this->currencies, $currency) ;
    }

    public function hasCurrency($currency)
    {
        return $this->isInCurrency($currency) ;
    }

    public function hasnotCurrency($currency)
    {
        return !$this->isInCurrency($currency) ;
    }


    /*
    |--------------------------------------------------------------------------
    | Get
    |--------------------------------------------------------------------------
    |
    */
    public function getPricesAttribute()
    {
        return $this->getMeta('prices');
    }

    public function priceIn($currency)
    {
        $currency = strtoupper($currency) ;
        if ($currency == 'first') {
            return array_first($this->prices);
        }
        if (isset($this->prices[ $currency ])) {
            return $this->prices[ $currency ];
        }

        return false;
    }

    public function getPriceAttribute()
    {
        if ($this->isInCurrency('IRR')) {
            return $this->priceIn('IRR');
        } else {
            return $this->priceIn('first');
        }
    }
}
