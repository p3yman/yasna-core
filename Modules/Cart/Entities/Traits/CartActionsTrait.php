<?php namespace Modules\Cart\Entities\Traits;

use Modules\Cart\Events\CartCheckedOut;
use Modules\Cart\Events\CartCheckoutUndid;
use Modules\Cart\Events\CartConfirmed;
use Modules\Cart\Events\CartPaymentStatusChanged;
use Modules\Cart\Events\CartStatusChanged;

trait CartActionsTrait
{
    protected $defined_actions = [
         'checkout',
         'confirm',
         'remarks',
         'discount',
         'checkoutUndo',
         'status',
         'paymentStatus',
         'delete',
         //TODO: rest of actions : EDIT , CLOSE , REOPEN
    ];



    /**
     * get an array of allowed actions
     *
     * @return array
     */
    public function allowedActions()
    {
        $array = [];
        foreach ($this->defined_actions as $action) {
            if ($this->isActionAllowed($action)) {
                $array[] = $action;
            }
        }

        return $array;
    }



    /**
     * check if the $action is allowed
     *
     * @param $action
     *
     * @return bool
     */
    public function isActionAllowed($action)
    {
        if (!$this->exists) {
            return false;
        }

        $method = "is" . studly_case($action) . 'Allowed';
        if (method_exists($this, $method)) {
            return $this->$method();
        }

        return false;
    }



    /**
     * check if the cart can be safely deleted
     *
     * @return bool
     */
    public function isDeleteAllowed()
    {
        return $this->isNotRaised();
    }



    /**
     * check if checkout is allowed
     *
     * @return bool
     */
    public function isCheckoutAllowed()
    {
        return $this->isNotRaised();
    }



    /**
     * check if cart confirm is allowed
     *
     * @return bool
     */
    public function isConfirmAllowed()
    {
        return $this->isPending();
    }



    /**
     * check if adding remarks is allowed
     *
     * @return bool
     */
    public function isRemarksAllowed()
    {
        return true;
    }



    /**
     * check if adding discount is allowed
     *
     * @return bool
     */
    public function isDiscountAllowed()
    {
        return $this->isNotPaid();
    }



    /**
     * check if checkout revert is allowed
     *
     * @return bool
     */
    public function isCheckoutUndoAllowed()
    {
        return $this->isPending();
    }



    /**
     * check if query status is allowed
     *
     * @return bool
     */
    public function isStatusAllowed()
    {
        return $this->isConfirmed() and $this->isOpen();
    }



    /**
     * check if query payment status is allowed
     *
     * @return bool
     */
    public function isPaymentStatusAllowed()
    {
        return $this->isRaised() and $this->isOpen();
    }



    /**
     * add remarks to the the current cart
     *
     * @param string $remarks
     *
     * @return bool
     */
    public function addRemarks($remarks)
    {
        if (!$this->isRemarksAllowed()) {
            return false;
        }

        return $this->createRecord('remarks', $remarks);
    }



    /**
     * confirm the current cart
     *
     * @param string $remarks
     *
     * @return bool
     */
    public function confirm($remarks = null)
    {
        $current_status = $this->status;
        if (!$this->isConfirmAllowed()) {
            return false;
        }


        $saved = $this->update([
             'confirmed_at' => $this->now(),
             'confirmed_by' => user()->id,
        ]);


        if ($saved) {
            $this->createRecord('confirm', $remarks, $current_status, 'under_packing');
        }

        event(new CartConfirmed($this));
        return $saved;
    }



    /**
     * set discount to the current cart
     *
     * @param  float $amount
     * @param string $remarks
     *
     * @return bool
     */
    public function setDiscount($amount, $remarks = null)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!$this->isDiscountAllowed()) {
            return false;
        }

        /*-----------------------------------------------
        | Preparation ...
        */
        $current_discount = $this->added_discount;


        /*-----------------------------------------------
        | Main Action ...
        */
        $done = $this->update([
             'added_discount' => $amount,
        ]);


        /*-----------------------------------------------
        | Following Actions ...
        */
        if ($done) {
            $this->createRecord('discount', $remarks, $current_discount, $amount);
            $this->refreshCalculations();
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $done;
    }



    /**
     * checkout the current cart
     *
     * @param string $remarks
     *
     * @return bool
     */
    public function checkout($remarks = null)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!$this->isCheckoutAllowed()) {
            return false;
        }


        /*-----------------------------------------------
        | Main Action ...
        */
        $done = $this->update([
             "raised_at" => $this->now(),
             "raised_by" => user()->id,
        ]);

        /*-----------------------------------------------
        | Consequences ...
        */
        if ($done) {
            $this->createRecord('checkout', $remarks);
            event(new CartCheckedOut($this));
        }


        /*-----------------------------------------------
        | Return ...
        */
        return $done;
    }



    /**
     * revert checkout of the current cart
     *
     * @param string $remarks
     *
     * @return bool
     */
    public function checkoutUndo($remarks = null)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!$this->isCheckoutUndoAllowed()) {
            return false;
        }

        /*-----------------------------------------------
        | Main Action ...
        */
        $done = $this->update([
             'raised_at' => null,
             'raised_by' => 0,
        ]);


        /*-----------------------------------------------
        | Consequences ...
        */
        if ($done) {
            $this->createRecord('checkoutUndo', $remarks);
            event(new CartCheckoutUndid($this));
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $done;
    }



    /**
     * set new status to the current cart
     *
     * @param string $new_status
     * @param string $remarks
     *
     * @return bool
     */
    public function setStatus($new_status, $remarks = null)
    {
        $old_status = $this->status;
        $done       = $this->applyStatus($new_status);
        if ($done) {
            $this->createRecord('status', $remarks, $old_status, $new_status);
            event(new CartStatusChanged($this));
        }

        return $done;
    }



    /**
     * set new payment status to the current cart
     *
     * @param string $new_status
     * @param string $remarks
     *
     * @return bool
     */
    public function setPaymentStatus($new_status, $remarks = null)
    {
        $old_status = $this->payment_status;
        $done       = $this->applyPaymentStatus($new_status);

        if ($done) {
            $this->createRecord('payment', $remarks, $old_status, $new_status);
            event(new CartPaymentStatusChanged($this));
        }

        return $done;
    }
}
