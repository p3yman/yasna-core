<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/2/19
 * Time: 3:49 PM
 */

namespace Modules\Cart\Entities\Traits;


use App\Models\User;

trait CartElectorTrait
{
    /**
     * Elector for User
     *
     * @param User|int|string $user
     */
    public function electorUser($user)
    {
        $this->electorUserId($user);
    }



    /**
     * Elector for User
     *
     * @param User|int|string $user
     */
    public function electorUserId($user)
    {
        $this->elector()->forUser($user);
    }



    /**
     * Elector for Locale
     *
     * @param string $locale
     */
    public function electorLocale(string $locale)
    {
        $this->elector()->where('locale', $locale);
    }



    /**
     * Elector for Currency
     *
     * @param string $currency
     */
    public function electorCurrency(string $currency)
    {
        $this->elector()->where('currency', $currency);
    }



    /**
     * Elector for the Activity.
     *
     * @param bool $value
     */
    public function electorActive(bool $value)
    {
        if ($value) {
            $this->elector()->whereActive();
        } else {
            $this->elector()->whereInactive();
        }
    }



    /**
     * apply criteria to the query builder
     *
     * @param string $criteria
     *
     * @return void
     */
    public function electorCriteria($criteria)
    {
        switch ($criteria) {
            case 'pending':
                $this->elector()->whereNotNull('raised_at')->whereNull('confirmed_at')->whereNull('closed_at');
                break;

            case "under_packing":
                $this->elector()->whereNotNull('confirmed_at')->whereNull('sent_at')->whereNull('closed_at');
                break;

            case 'support_request':
                $this->elector()->whereNotNull('rejected_at')->whereNull('closed_at')->whereNull('closed_at');
                break;

            case 'under_delivery':
                $this->elector()->whereNotNull('sent_at')->whereNull('delivered_at')->whereNull('closed_at');
                break;

            case 'under_payment':
                $this->elector()->whereNull('paid_at')->whereNotNull('raised_at')->whereNull('closed_at');
                break;

            case 'not_raised':
                $this->elector()->whereNull('raised_at')->whereNull('closed_at');
                break;

            case 'delivered':
                $this->elector()->whereNotNull('delivered_at')->whereNull('closed_at');
                break;

            case 'archive':
                $this->elector()->whereNotNull('closed_at');
                break;

            case 'all':
                break;

            default:
                $this->elector()->where('id', 'invalid');
        }

    }
}
