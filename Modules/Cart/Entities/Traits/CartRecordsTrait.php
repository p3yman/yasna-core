<?php namespace Modules\Cart\Entities\Traits;

trait CartRecordsTrait
{
    public function cartRecords()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'CartRecord');
    }



    public function records()
    {
        return $this->cartRecords();
    }



    public function getRecordsAttribute()
    {
        return $this->records()->orderBy('created_at', 'desc')->get();
    }



    public function getRecordAttribute()
    {
        return $this->cartRecords()
                    ->firstOrNew([
                         'id'      => "0",
                         'cart_id' => $this->id,
                    ])
             ;
    }



    public function createRecord($type, $remarks = null, $old_thing = null, $new_thing = null)
    {
        return $this->record->create($type, $remarks, $old_thing, $new_thing);
    }
}
