<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/19/18
 * Time: 12:03 PM
 */

namespace Modules\Cart\Entities\Traits;

use App\Models\Cart;

trait CartSmartSearchTrait
{
    /**
     * Calls methods starting with `smartSearch` till find an existed model with the given keyword.
     * If no cart found, a new instance of cart will be returned.
     *
     * @param string $keyword
     *
     * @return Cart
     */
    public static function smartSearch(string $keyword)
    {
        /** @var Cart $instance */
        $instance = static::instance();
        $methods  = static::getSmartSearchMethods();

        foreach ($methods as $method) {
            /** @var Cart|false $method_result */
            $method_result = $instance->$method($keyword);

            if ($method_result and $method_result->exists) {
                return $method_result;
            }
        }

        return $instance;
    }



    /**
     * Returns an array of method starting with `smartSearch`.
     *
     * @return array
     */
    public static function getSmartSearchMethods(): array
    {
        $methods = static::instance()->methodsArray();

        return collect($methods)
             ->filter(function (string $method) {
                 return preg_match('/^smartSearch(.)+$/', $method);
             })
             ->toArray()
             ;
    }



    /**
     * Tries to find a cart with a keyword starting with `id:` and followed with an id.
     *
     * @param string $keyword
     *
     * @return Cart|false
     */
    public static function smartSearchId(string $keyword)
    {
        if (!starts_with($keyword, 'id:')) {
            return false;
        }

        $id = str_after($keyword, 'id:');

        return static::grabId($id);
    }



    /**
     * Tries to find a cart with it code.
     *
     * @param string $keyword
     *
     * @return Cart
     */
    public static function smartSearchCode(string $keyword)
    {
        $id = static::instance()->code2id($keyword);

        return static::grabId($id);
    }
}
