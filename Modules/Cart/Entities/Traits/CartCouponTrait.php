<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/17/18
 * Time: 4:40 PM
 */

namespace Modules\Cart\Entities\Traits;

use App\Models\User;
use Exception;
use Modules\Coupons\Entities\Traits\CouponUsagesTrait;
use Modules\Coupons\Services\CouponHelperUsageTrait;
use Modules\Coupons\Services\InvoiceObject;

trait CartCouponTrait
{
    use CouponHelperUsageTrait;
    use CouponUsagesTrait;



    /**
     * Returns the applied coupon code.
     *
     * @return string|null
     */
    public function getAppliedCouponCode()
    {
        return $this->getMeta('applied_coupon_code');
    }



    /**
     * Checks if this cart has applied coupon code.
     *
     * @return bool
     */
    public function hasAppliedCouponCode()
    {
        return boolval($this->getAppliedCouponCode());
    }



    /**
     * Checks if this cart has not applied coupon code.
     *
     * @return bool
     */
    public function hasNotAppliedCouponCode()
    {
        return !$this->hasAppliedCouponCode();
    }



    /**
     * Save applied coupon code in cart's meta.
     *
     * @param string|null $code
     *
     * @return void
     */
    protected function saveAppliedCouponCode($code)
    {
        $this->batchSave(['applied_coupon_code' => $code]);

        $this->refresh();
    }



    /**
     * Calculate the discount with the given code.
     *
     * @param string $code
     *
     * @return float|int
     */
    public function calculateDiscountByCouponCode($code)
    {
        $invoice = coupon()->evaluate($code, $this->getInvoiceObject());

        try {
            return $invoice->getTotalDiscount();
        } catch (Exception $exception) {
            // Handled in bellow lines.
        }

        return 0;
    }



    /**
     * Applies the specified coupon code in the cart.
     *
     * @param string $code
     *
     * @return bool
     */
    public function applyCouponCode($code)
    {
        $calculated_discount = $this->calculateDiscountByCouponCode($code);
        if ($calculated_discount) {
            $this->saveAppliedCouponCode($code);

            $this->setDiscount($calculated_discount);

            return true;
        }

        return false;
    }



    /**
     * Checks if the specified code is applicable.
     *
     * @param string $code
     *
     * @return bool
     */
    public function couponCodeIsApplicable($code)
    {
        $calculated_discount_amount = $this->calculateDiscountByCouponCode($code);

        return boolval($calculated_discount_amount);
    }



    /**
     * Checks if the specified code is not applicable.
     *
     * @param string $code
     *
     * @return bool
     */
    public function couponCodeIsNotApplicable($code)
    {
        return !$this->couponCodeIsApplicable($code);
    }



    /**
     * Checks if the applied coupon code is still applicable.
     *
     * @return bool
     */
    public function appliedCouponCodeIsApplicable()
    {
        $code = $this->getAppliedCouponCode();

        return $this->couponCodeIsApplicable($code);
    }



    /**
     * Checks if the applied coupon code is not still applicable.
     *
     * @return bool
     */
    public function appliedCouponCodeIsNotAccessible()
    {
        return !$this->appliedCouponCodeIsApplicable();
    }



    /**
     * Clears the applied discount.
     */
    public function clearAppliedDiscount()
    {
        $this->update([
             'added_discount' => $this->applied_discounts_amount,
        ]);

        $this->saveAppliedCouponCode(null);
    }



    /**
     * Refreshes the discount's calculations
     */
    public function refreshCouponDiscountCalculation()
    {
        if ($this->appliedCouponCodeIsNotAccessible()) {
            $this->clearAppliedDiscount();
        }
    }



    /**
     * Returns the an instance of the `Modules\Coupons\Services\InvoiceObject` class
     * created form this cart.
     *
     * @return InvoiceObject
     */
    public function getInvoiceObject()
    {
        $invoice = coupon()->makeInvoice($this->created_at, $this->sale_price, $this->user);
        $orders  = $this->orders;
        $user    = $this->user;

        if ($user and $user->exists) {
            $invoice->setUser($user);
        }

        $invoice->setOriginalInstance($this);

        foreach ($orders as $order) {
            $invoice->addItem($order->sale_price, $order->id);
        }

        return $invoice;
    }



    /**
     * Returns a list of meta fields which are used in for handling coupon
     *
     * @return array
     */
    public function couponMetaFields()
    {
        return ['applied_coupon_code'];
    }



    /**
     * Marks the applied coupon as used.
     *
     * @param User|null $user
     *
     * @return int|null
     */
    public function markAppliedCouponCode(?User $user = null)
    {
        $code = $this->getAppliedCouponCode();

        if (!$code) {
            return null;
        }

        $user_id = $user ? $user->id : user()->id;

        return coupon()->markAsUsed($code, $this->id, $user_id, $this->added_discount);
    }



    /**
     * Marks the applied coupon as used.
     *
     * @param User|null $user
     *
     * @return int|null
     */
    public function markAppliedCouponCodeAsUsed(?User $user = null)
    {
        return $this->markAppliedCouponCode($user);
    }
}
