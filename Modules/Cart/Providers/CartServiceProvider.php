<?php

namespace Modules\Cart\Providers;

use Modules\Cart\CouponRules\FirstPurchasesRule;
use Modules\Cart\CouponRules\MinPurchaseHistoryRule;
use Modules\Cart\CouponRules\OrdersCategoryRule;
use Modules\Cart\CouponRules\OrdersTagRule;
use Modules\Cart\CouponRules\SpecificWaresRule;
use Modules\Cart\Events\CartCheckedOut;
use Modules\Cart\Events\CartCheckoutUndid;
use Modules\Cart\Listeners\ActionsOnCheckout;
use Modules\Cart\Listeners\ActionsOnCheckoutUndo;
use Modules\Cart\ShippingRules\CategoryRule;
use Modules\Cart\ShippingRules\ProductRule;
use Modules\Cart\ShippingRules\SpecialSalesRule;
use Modules\Cart\ShippingRules\TagRule;
use Modules\Yasna\Services\YasnaProvider;

class CartServiceProvider extends YasnaProvider
{
    use CartEndpointsTrait;



    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerModelTraits();
        $this->registerSidebar();
        $this->registerAllAliases();
        $this->registerListeners();
        $this->registerRoleSampleModules();
        $this->registerCouponsCommunication();
        $this->registerWidgets();
        $this->registerServiceHandler();
        $this->registerShippingRules();
        $this->registerEndpoints();
    }



    /**
     * register service handlers
     */
    private function registerServiceHandler()
    {
        module('shop')
             ->service('ware_browse_headings_handlers')
             ->add('cart')
             ->method("cart:WaresHandleController@browseColumns")
        ;

        module('shop')
             ->service("ware_export_columns_handler")
             ->add('cart')
             ->method("cart:WaresHandleController@exportColumns")
        ;
    }



    /**
     * register aliases
     *
     * @return void
     */
    public function registerAllAliases()
    {
        $this->addAlias('CartModule', CartServiceProvider::class);
    }



    /**
     * Module string, to be used as sample for role definitions.
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('orders')
             ->value('edit ,  view ,  delete ,  bin ,')
        ;
        module('users')
             ->service('role_sample_modules')
             ->add('carts')
             ->value('create ,  edit ,  process ,  report ,')
        ;
    }



    /**
     * register model traits
     *
     * @return void
     */
    public function registerModelTraits()
    {
        module('yasna')
             ->service('traits')
             ->add()->trait("Cart:PostCartTrait")->to("Post")
             ->add()->trait("Cart:PosttypeCartTrait")->to("Posttype")
             ->add()->trait("Cart:WareCartTrait")->to("Ware")
             ->add()->trait("Cart:UserCartTrait")->to("User")
        ;
    }



    /**
     * register sidebar
     *
     * @return void
     */
    public function registerSidebar()
    {
        service('manage:sidebar')
             ->add('carts')
             ->blade('cart::layouts.sidebar')
             ->order(51)
        ;
    }



    /**
     * register listeners
     *
     * @return void
     */
    public function registerListeners()
    {
        $this->listen(CartCheckoutUndid::class, ActionsOnCheckoutUndo::class);
        $this->listen(CartCheckedOut::class, ActionsOnCheckout::class);
    }



    /**
     * Registers things needed for communication with the `Coupons` module.
     */
    protected function registerCouponsCommunication()
    {
        if (function_exists('coupon')) {
            $this->registerCouponGroup();
            $this->registerCouponRules();
        }
    }



    /**
     * Registers coupon group.
     */
    protected function registerCouponGroup()
    {
        coupon()->registerGroup("Cart", $this->module()->getTrans('carts.singular'));
    }



    /**
     * Registers coupon rules.
     */
    protected function registerCouponRules()
    {
        coupon()->registerRule(FirstPurchasesRule::class);
        coupon()->registerRule(SpecificWaresRule::class);
        coupon()->registerRule(MinPurchaseHistoryRule::class);
        coupon()->registerRule(OrdersCategoryRule::class);
        coupon()->registerRule(OrdersTagRule::class);
    }



    /**
     * Dashboard Widget Handlers
     */
    public function registerWidgets()
    {
        module('manage')
             ->service('widgets_handler')
             ->add('cart-widgets')
             ->method('cart:handleWidgets')
        ;
    }



    /**
     * Dashboard Widgets
     */
    public static function handleWidgets()
    {
        $service = 'widgets';

        module('manage')
             ->service($service)
             ->add('order-finder')
             ->blade('cart::dashboard.order-finder')
             ->trans('cart::dashboard.order-finder.title')
             ->color('warning')
             ->icon('shopping-cart')
             ->condition(function () {
                 return model('cart')->can('*');
             })
        ;
    }



    /**
     * register shipping rules
     */
    protected function registerShippingRules()
    {
        shipping()->registerRule(TagRule::class);
        shipping()->registerRule(CategoryRule::class);
        shipping()->registerRule(ProductRule::class);
        shipping()->registerRule(SpecialSalesRule::class);
    }
}
