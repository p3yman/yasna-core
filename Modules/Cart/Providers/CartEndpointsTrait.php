<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/1/19
 * Time: 10:01 AM
 */

namespace Modules\Cart\Providers;


use Modules\Cart\Http\Endpoints\V1\CheckoutEndpoint;
use Modules\Cart\Http\Endpoints\V1\CouponEvaluateEndpoint;
use Modules\Cart\Http\Endpoints\V1\DeleteEndpoint;
use Modules\Cart\Http\Endpoints\V1\DeliveryEndpoint;
use Modules\Cart\Http\Endpoints\V1\FlushAllMyOrdersEndpoint;
use Modules\Cart\Http\Endpoints\V1\ItemsAddEndpoint;
use Modules\Cart\Http\Endpoints\V1\ItemsRemoveEndpoint;
use Modules\Cart\Http\Endpoints\V1\ListEndpoint;
use Modules\Cart\Http\Endpoints\V1\PreviewEndpoint;
use Modules\Cart\Http\Endpoints\V1\RevalidateEndpoint;
use Modules\Cart\Http\Endpoints\V1\SaveEndpoint;
use Modules\Cart\Http\Endpoints\V1\ShippingMethodsEndpoint;

trait CartEndpointsTrait
{
    /**
     * Registers the needs of the cart API based services only if the Endpoint module is enabled.
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(ListEndpoint::class);
        endpoint()->register(PreviewEndpoint::class);
        endpoint()->register(SaveEndpoint::class);
        endpoint()->register(ItemsAddEndpoint::class);
        endpoint()->register(ItemsRemoveEndpoint::class);
        endpoint()->register(DeleteEndpoint::class);
        endpoint()->register(RevalidateEndpoint::class);
        endpoint()->register(CheckoutEndpoint::class);
        endpoint()->register(ShippingMethodsEndpoint::class);
        endpoint()->register(DeliveryEndpoint::class);
        endpoint()->register(CouponEvaluateEndpoint::class);
        endpoint()->register(FlushAllMyOrdersEndpoint::class);
    }
}
