<?php

namespace Modules\Cart\Notifications;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Cart\Entities\Cart;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Notifications\Messages\MailMessage;

class InvoicePaidAdminNotification extends YasnaNotificationAbstract implements ShouldQueue
{

    use InvoiceEmailMethods;

    private $cart;



    /**
     * InvoicePaidAdminNotification constructor.
     *
     * @param Cart $cart
     */
    public function __construct($cart)
    {
        $this->cart = $cart;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
             $this->yasnaMailChannel(),
             'database',
        ];
    }



    /**
     * send cart factor to super admin with email
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
             ->subject(trans('cart::carts.invoice'))
             ->markdown('cart::notification.email', [
                  'model'          => $this->cart,
                  'seller_tel'     => $this->getTelephone(),
                  'seller_email'   => $this->getSellerEmail(),
                  'buyer_address'  => $this->getBuyerAddress($this->cart),
                  'delivery_title' => $this->generateDeliveryTitle($this->cart),
             ])
             ;

    }



    /**
     * send notification in our app
     *
     * @return array
     */
    public function toDatabase()
    {
        return [
             'cart_id' => $this->cart->id,
        ];
    }



    /**
     *  get the array representation of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'user_id'  => $notifiable->id,
             'order_id' => $this->cart->id,
        ];
    }



    /**
     * Send messages to admin team members when the The commodity are purchased
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $user_name = $this->cart->user->full_name;

        return trans_safe('cart::notification.send_sms_checkout_admin', [
             'buyer_name' => $user_name,
        ]);
    }
}
