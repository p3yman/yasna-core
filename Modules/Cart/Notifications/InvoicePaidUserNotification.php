<?php

namespace Modules\Cart\Notifications;

use App\Models\User;
use Modules\Cart\Entities\Cart;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InvoicePaidUserNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use InvoiceEmailMethods;

    private $cart;



    /**
     * InvoicePaidAdminNotification constructor.
     *
     * @param Cart $cart
     */
    public function __construct($cart)
    {
        $this->cart = $cart;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
             $this->yasnaSmsChannel(),
             'database',
        ];
    }



    /**
     * get the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage|void
     */
    public function toMail($notifiable)
    {

        if (empty($this->cart->user->email)) {
            return;
        }

        return (new MailMessage)
             ->subject(trans('cart::carts.invoice'))
             ->markdown('cart::notification.email', [
                  'model'          => $this->cart,
                  'seller_tel'     => $this->getTelephone(),
                  'seller_email'   => $this->getSellerEmail(),
                  'buyer_address'  => $this->getBuyerAddress($this->cart),
                  'delivery_title' => $this->generateDeliveryTitle($this->cart),
             ])
             ;

    }



    /**
     * send notification in our app
     *
     * @return array
     */
    public function toDatabase()
    {
        return [
             'cart_id' => $this->cart->id,
        ];
    }



    /**
     * get the array representation of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'user_id'  => $notifiable->id,
             'order_id' => $this->cart->id,
        ];
    }



    /**
     * Send messages to user when the commodity are purchased
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        return trans_safe('cart::notification.send_sms_checkout_user');
    }
}
