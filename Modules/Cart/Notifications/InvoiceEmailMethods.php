<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 10/17/18
 * Time: 10:36 AM
 */

namespace Modules\Cart\Notifications;


use Modules\Cart\Entities\Cart;

trait InvoiceEmailMethods
{
    /**
     * get seller telephone
     *
     * @return string
     */
    private function getTelephone()
    {
        $res = "";
        foreach ($telephones = get_setting('telephone') as $telephone) {
            $res .= ad($telephone);
        }
        return $res;
    }



    /**
     * get email of seller
     *
     * @return string
     */
    private function getSellerEmail()
    {
        $res = "";
        foreach ($emails = get_setting('email') as $email) {
            $res .= $email;
        }
        return $res;

    }



    /**
     * get buyer address
     *
     * @param Cart $cart
     *
     * @return string
     */
    private function getBuyerAddress($cart)
    {
        if ($address = $cart->address_text) {
            return ad($address);
        } else {
            return trans_safe("cart::status.without_address");
        }
    }



    /**
     * generate delivery title
     *
     * @param Cart $cart
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    private function generateDeliveryTitle($cart)
    {
        $res = trans("cart::couriers.price");
        if ($cart->courier_id) {
            $res .= " ".trans("manage::forms.general.with");

            $res .= " ".$cart->courier->title;
            if ($cart->courier_discount) {

                $res .= " ".trans("manage::forms.general.and");

                $res .= " ".pd(number_format($cart->courier_discount));

                $res .= " ".trans("shop::currencies.$cart->currency");

                $res .= " ".trans("cart::couriers.discount");
            }

        }

        return $res;
    }

}
