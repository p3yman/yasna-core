<?php namespace Modules\Cart\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Cart\Events\CartCheckoutUndid;

class ActionsOnCheckoutUndo implements ShouldQueue
{
    public $tries = 5;

    public function handle(CartCheckoutUndid $event)
    {
        $event->model->inventoryPutBack() ;
    }
}
