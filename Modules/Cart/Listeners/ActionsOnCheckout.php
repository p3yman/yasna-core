<?php namespace Modules\Cart\Listeners;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Cart\Entities\Cart;
use Modules\Cart\Events\CartCheckedOut;
use Modules\Cart\Notifications\InvoicePaidAdminNotification;
use Modules\Cart\Notifications\InvoicePaidUserNotification;

class ActionsOnCheckout implements ShouldQueue
{
    public $tries = 5;



    /**
     * handle operation of event
     *
     * @param CartCheckedOut $event
     */
    public function handle(CartCheckedOut $event)
    {
        $event->model->inventoryPickup();
        $this->sendNotification($event->model);
    }



    /**
     * send notifications
     *
     * @param Cart $cart
     */
    private function sendNotification($cart)
    {
        $this->sendAdminNotifications($cart);
        $this->sendUserNotifications($cart);
    }



    /**
     * send notifications for admins
     *
     * @param Cart $cart
     */
    private function sendAdminNotifications($cart)
    {
        if (!(bool)get_setting('send_admin_notification')) {
            return;
        }

        $admins = model('role')->where('slug', 'superadmin')->first()->users;
        foreach ($admins as $admin) {
            // The `pivot` relation was causing problems in serializing and unserializing model for queue.
            /** @var User $admin */
            $admin->unsetRelation('pivot');
            $admin->notify(new InvoicePaidAdminNotification($cart));
        }
    }



    /**
     * send notifications for customer
     *
     * @param Cart $cart
     */
    private function sendUserNotifications($cart)
    {
        if (!(bool)get_setting('send_user_notification')) {
            return;
        }

        if ($cart->user) {
            $cart->user->notify(new InvoicePaidUserNotification($cart));
        }
    }

}
