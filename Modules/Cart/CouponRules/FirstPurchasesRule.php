<?php

namespace Modules\Cart\CouponRules;

use App\Models\Cart;
use App\Models\User;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class FirstPurchasesRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("cart::coupons.rules.first-purchases");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("input")
             ->name("number")
             ->label("tr:cart::coupons.fields.number")
             ->validationRules('required|numeric|min:1')
             ->required()
             ->purificationRules('ed|numeric')
             ->inForm()
        ;

        $form->add('note')
             ->label("tr:cart::coupons.fields.number-hint")
             ->icon("lightbulb-o")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if ($this->passes()) {
            $this->markAsValid();
        } else {
            $this->markAsInvalid();
        }
    }



    /**
     * Returns the Cart model object.
     *
     * @return Cart
     */
    protected function getCart()
    {
        return $this->invoice->getOriginalInstance();
    }



    /**
     * Returns the User model object.
     *
     * @return User
     */
    protected function getUserInstance()
    {
        return $this->invoice->getUser();
    }



    /**
     * Returns count of paid carts for the user.
     *
     * @return int
     */
    protected function previousPaidCartsCount()
    {
        return $this->getUserInstance()
                    ->carts()
                    ->where('raised_at', '<=', now()->toDateString())
                    ->count()
             ;
    }



    /**
     * Returns the number of discountable purchases.
     *
     * @return int|string
     */
    protected function number()
    {
        return $this->para['number'];
    }



    /**
     * Checks if the invoice passes this rule.
     *
     * @return bool
     */
    protected function passes()
    {
        return ($this->previousPaidCartsCount() <= $this->number());
    }



    /**
     * Checks if the invoice fails this rules.
     *
     * @return bool
     */
    protected function fails()
    {
        return !$this->passes();
    }
}
