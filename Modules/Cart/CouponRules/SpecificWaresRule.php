<?php

namespace Modules\Cart\CouponRules;

use App\Models\Order;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;
use ReflectionClass;


class SpecificWaresRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans('cart::coupons.rules.specific-wares');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        // Using the Selectize widget may cause performance reducing in a huge number of wares.
        // So, it may we need to use text inputs, identify wares with their codes, and add them with ajax call.

        $form->add("selectize")
             ->name("wares_allowed")
             ->label("tr:coupons::rules.allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("hashid")
             ->captionField("title")
        ;

        $form->add("selectize")
             ->name("wares_not_allowed")
             ->label("tr:coupons::rules.not-allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("hashid")
             ->captionField("title")
        ;

        return $form;
    }



    /**
     * Returns a array to be used in selectize widget for selecting wares.
     *
     * @return array
     */
    protected static function getSelectizeOptions()
    {
        return model('ware')
             ->orderByDesc('created_at')
             ->get()
             ->map(function ($ware) {
                 return [
                      "hashid" => $ware->hashid,
                      "title"  => $ware->full_title,
                 ];
             })->toArray()
             ;
    }



    /**
     * Retrieves hashids for from the specified fields.
     *
     * @param string $field_name
     *
     * @return array
     */
    protected function retrieveWaresFromField(string $field_name)
    {
        $string  = ($this->para[$field_name] ?? '');
        $hashids = explode_not_empty(',', $string);

        return array_map('hashid', $hashids);
    }



    /**
     * Returns an array of allowed wares' IDs.
     *
     * @return array
     */
    protected function allowedWares()
    {
        return $this->retrieveWaresFromField('wares_allowed');
    }



    /**
     * Returns an array of disallowed wares' IDs.
     *
     * @return array
     */
    protected function disallowedWares()
    {
        return $this->retrieveWaresFromField('wares_not_allowed');
    }



    /**
     * Checks if the specified ware is allowed.
     *
     * @param Ware $ware
     *
     * @return bool
     */
    protected function wareIsAllowed(Ware $ware)
    {
        return in_array($ware->id, $this->allowedWares());
    }



    /**
     * Checks if the specified ware is disallowed.
     *
     * @param Ware $ware
     *
     * @return bool
     */
    protected function wareIsDisallowed(Ware $ware)
    {
        return in_array($ware->id, $this->disallowedWares());
    }



    /**
     * Checks if the specified ware is not disallowed.
     *
     * @param Ware $ware
     *
     * @return bool
     */
    protected function wareIsNotDisallowed(Ware $ware)
    {
        return !$this->wareIsDisallowed($ware);
    }



    /**
     * Returns a collection containing the orders of the invoiced cart.
     *
     * @return Collection
     */
    protected function invoiceOrders()
    {
        return $this->invoice->orders;
    }



    /**
     * Checks if the specified ware is discountable.
     *
     * @param Ware $ware
     *
     * @return bool
     */
    protected function wareIsDiscountable(Ware $ware)
    {
        return ($this->wareIsAllowed($ware) and $this->wareIsNotDisallowed($ware));
    }



    /**
     * Checks if the specified ware is not discountable.
     *
     * @param Ware $ware
     *
     * @return bool
     */
    protected function wareIsNotDiscountable(Ware $ware)
    {
        return !$this->wareIsDiscountable($ware);
    }



    /**
     * Calculates the discount amount for the specified order.
     *
     * @param Order $order
     *
     * @return int|float
     */
    protected function calculateOrderDiscount(Order $order)
    {
        $ware = $order->ware;

        if ($this->wareIsNotDiscountable($ware)) {
            return 0;
        }

        $count      = $order->count;
        $unit_price = $order->single_sale_price;
        $rulebook   = $this->rulebook;

        return $rulebook->calculateApplicableAmount($unit_price, $count);
    }



    /**
     * Calculates total discount of invoiced cart.
     *
     * @return void
     */
    protected function calculateItemsDiscount()
    {
        $order_ids = array_keys($this->invoice->getItems());
        $orders    = model('order')->whereIn('id', $order_ids)->get();

        foreach ($orders as $order) {
            $this->setItemDiscount($order->id, $this->calculateOrderDiscount($order));
        }
    }



    /**
     * Calculates total discount of invoiced cart.
     *
     * @return int|float
     */
    protected function calculateTotalDiscount()
    {
        return array_sum($this->getItemsDiscounts());
    }



    /**
     * Returns an array of items' discounts.
     *
     * @return array
     */
    protected function getItemsDiscounts()
    {
        $self_discounts = $this->getSelfDiscounts();
        return ($self_discounts['items'] ?? []);
    }



    /**
     * Returns discounts applied by this rule.
     *
     * @return array
     */
    protected function getSelfDiscounts()
    {
        $discounts = $this->invoice->getDiscounts();

        return ($discounts[$this->getIdentifier()] ?? []);
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->calculateItemsDiscount();
        $this->setTotalDiscount($this->calculateTotalDiscount());
    }
}
