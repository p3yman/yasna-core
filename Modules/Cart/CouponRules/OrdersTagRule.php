<?php

namespace Modules\Cart\CouponRules;

use Illuminate\Support\Debug\Dumper;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;
use Modules\Tag\Entities\Tag;
use Nwidart\Modules\Collection;


class OrdersTagRule extends DiscountRule
{

    private $tags_white;
    private $tags_black;
    private $blacklist;



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans('cart::coupons.order-tag.title');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("tags_white")
             ->label(trans('cart::coupons.order-tag.whitelist'))
             ->inForm()
             ->options(self::getSelectizeOptions())
             ->valueField("value")
             ->captionField("caption")
        ;

        $form->add("selectize")
             ->name("tags_black")
             ->label(trans('cart::coupons.order-tag.blacklist'))
             ->inForm()
             ->options(self::getSelectizeOptions())
             ->valueField("value")
             ->captionField("caption")
        ;

        return $form;
    }



    /**
     * get selectize options
     */
    private static function getSelectizeOptions()
    {
        $posttypes = model('posttype')->whereHas('features', function ($query) {
            $query->where('slug', 'shop');
        })->get(['id'])->pluck('id')->toArray()
        ;

        $res = model('tag')->whereIn('posttype_id', $posttypes)->get()->map(function ($item) {
            return ['value' => $item->id, 'caption' => $item->slug];
        })
        ;
        return $res->toArray();
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->tags_white = explode(',', $this->para['tags_white']);
        $this->tags_black = explode(',', $this->para['tags_black']);
        //$this->blacklist  = $this->para['tag_blacklist'];
        $this->apply();
    }



    /**
     * operation of discount
     */
    private function apply()
    {

        foreach ($this->invoice->getItems() as $id => $amount) {
            $order_tags = model('order', $id)->post->tags;

            if (!$this->isInWhitelist($order_tags)) {
                $this->setItemDiscount($id, 0);
            }

            if ($this->isInBlacklist($order_tags)) {
                $this->setItemDiscount($id, 0);
            }
        }

    }



    /**
     * check order Tag that in Tags whitelist
     *
     * @param Tag $order_tags
     *
     * @return bool
     */
    private function isInWhitelist($order_tags)
    {
        //it means user didn't choose any whitelist item and we skipp it
        if (empty($this->para['tags_white'])) {
            return true;
        }

        foreach ($order_tags as $tag) {

            $check_is_tag_in_list = $this->checkIsTagInList($tag->id, $this->tags_white);

            if ($check_is_tag_in_list) {
                return true;
            }
        }

        return false;
    }



    /**
     * check order Tag that in Tags blacklist
     *
     * @param Tag $order_tags
     *
     * @return bool
     */
    private function isInBlacklist($order_tags)
    {
        if (empty($this->para['tags_black'])) {
            return false;
        }

        if ($order_tags->count() == 0) {
            return true;
        }

        foreach ($order_tags as $tag) {

            $check_is_tag_in_list = $this->checkIsTagInList($tag->id, $this->tags_black);

            if ($check_is_tag_in_list) {
                return true;
            }
        }

        return false;
    }



    /**
     * check that tag exists in user list
     *
     * @param int $tag_id
     *
     * @return bool
     */
    private function checkIsTagInList($tag_id, $list)
    {
        return in_array($tag_id, $list);
    }
}
