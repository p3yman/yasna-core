<?php

namespace Modules\Cart\CouponRules;

use App\Models\Order;
use Illuminate\Support\Debug\Dumper;
use Modules\Categories\Entities\Category;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class OrdersCategoryRule extends DiscountRule
{

    private $categories_white;
    private $categories_black;
    private $blacklist;



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans('cart::coupons.order-category.title');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("categories_white")
             ->label(trans('cart::coupons.order-category.whitelist'))
             ->inForm()
             ->options(self::getSelectizeOptions())
             ->valueField("value")
             ->captionField("caption")
        ;


        $form->add("selectize")
             ->name("categories_black")
             ->label(trans('cart::coupons.order-category.blacklist'))
             ->inForm()
             ->options(self::getSelectizeOptions())
             ->valueField("value")
             ->captionField("caption")
        ;

        return $form;
    }



    /**
     * get selectize options
     */
    private static function getSelectizeOptions()
    {
        $posttypes = model('posttype')->whereHas('features', function ($query) {
            $query->where('slug', 'shop');
        })->get(['id'])->pluck('id')->toArray()
        ;

        $res = model('category')->whereIn('posttype_id', $posttypes)->get()->map(function ($item) {
            return ['value' => $item->id, 'caption' => $item->titleIn(getLocale())];
        })
        ;
        return $res->toArray();
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->categories_white = explode(',', $this->para['categories_white']);
        $this->categories_black = explode(',', $this->para['categories_black']);

        $this->apply();
    }



    /**
     * operation of rule
     */
    private function apply()
    {

        foreach ($this->invoice->getItems() as $id => $amount) {
            $order_cats = model('order', $id)->post->categories;


            if (!$this->isInWhiteList($order_cats)) {
                $this->setItemDiscount($id, 0);
            }

            if ($this->isInBlacklist($order_cats)) {
                $this->setItemDiscount($id, 0);
            }
        }
    }



    /**
     * check order category that in categories whitelist
     *
     * @param Category $order_cats
     *
     * @return bool
     */
    private function isInWhiteList($order_cats)
    {
        //it means user didn't choose any whitelist item and we skipp it
        if (empty($this->categories_white[0])) {
            return true;
        }

        foreach ($order_cats as $order_cat) {

            $is_order_cat_in_list = $this->checkOrderCatWithCatList($order_cat, $this->categories_white);

            if ($is_order_cat_in_list) {
                return true;
            }
        }

        return false;
    }



    /**
     *  check order category that in categories blacklist
     *
     * @param Category $order_cats
     *
     * @return bool
     */
    private function isInBlacklist($order_cats)
    {
        if ($order_cats->count() == 0 and !empty($this->para['categories_black'])) {
            return true;
        }

        foreach ($order_cats as $order_cat) {
            $is_order_cat_in_list = $this->checkOrderCatWithCatList($order_cat, $this->categories_black);
            if ($is_order_cat_in_list) {
                return true;
            }
        }

        return false;
    }



    /**
     * check order cat
     *
     * @param Order $order_cat
     *
     * @return bool
     */
    private function checkOrderCatWithCatList($order_cat, $cat_list)
    {
        while ($order_cat->parent_id > 0) {
            if ($this->isInCatList($order_cat, $cat_list)) {
                return true;
            }
            $order_cat = model('category', $order_cat->parent_id);
        }

        if ($this->isInCatList($order_cat, $cat_list)) {
            return true;
        }

        return false;
    }



    /**
     * check given category that in user category list
     *
     * @param Category $cat
     *
     * @return bool
     */
    private function isInCatList($cat, $cat_list)
    {
        return in_array($cat->id, $cat_list);
    }

}
