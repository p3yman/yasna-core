<?php

namespace Modules\Cart\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;
use Modules\Shop\Services\Currency;


class MinPurchaseHistoryRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("cart::coupons.rules.min-purchase");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add('text')
             ->name('amount')
             ->numberFormat()
             ->inForm()
             ->required()
             ->validationRules("required|numeric")
             ->purificationRules("ed|numeric")
             ->label(trans("cart::coupons.fields.amount"))
        ;

        $form->add('note')
             ->label("tr:cart::coupons.fields.min-purchase-hint")
             ->icon("lightbulb-o")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if ($this->checkRule()) {
            $this->markAsValid();
        } else {
            $this->markAsInvalid();
        }
    }



    /**
     * check the rule
     *
     * @return bool
     */
    protected function checkRule()
    {
        return $this->getUserPurchaseHistory() > $this->getMinDesiredAmount();
    }



    /**
     * get the minimum desired amount set in the rulebook
     *
     * @return float
     */
    protected function getMinDesiredAmount()
    {
        return (float)$this->para['amount'];
    }



    /**
     * get the user purchase history
     *
     * @return float
     */
    protected function getUserPurchaseHistory()
    {
        return model('cart')
             ->where('user_id', $this->invoice->getUser()->id)
             ->whereNotNull('paid_at')
             //->where('currency', "IRR") // @TODO: Why only IRR? -TAHA
             ->sum('invoiced_amount')
             ;
    }
}
