<?php

namespace Modules\Cart\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;

    private $main_data = [];



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'slug'          => 'tax_vat_percent',
                  'title'         => 'مالیات بر ارزش افزوده',
                  'category'      => 'database',
                  'order'         => '99',
                  'data_type'     => 'text',
                  'default_value' => '11',
             ],
             [
                  'slug'          => 'send_admin_notification',
                  'title'         => trans('cart::carts.admin_notify'),
                  'category'      => 'database',
                  'order'         => '100',
                  'data_type'     => 'boolean',
                  'default_value' => 'true',
             ],

             [
                  'slug'          => 'send_user_notification',
                  'title'         => trans('cart::carts.user_notify'),
                  'category'      => 'database',
                  'order'         => '101',
                  'data_type'     => 'boolean',
                  'default_value' => 'true',
             ],

             [
                  'slug'          => 'official_bill_support',
                  'title'         => $this->runningModule()->getTrans('seeder.official-bill-support'),
                  'category'      => 'database',
                  'order'         => '102',
                  'data_type'     => 'boolean',
                  'default_value' => 0,
             ],

        ];
    }
}
