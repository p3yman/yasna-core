<?php

namespace Modules\Cart\Database\Seeders;

use Illuminate\Database\Seeder;

class CartDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsTableSeeder::class);
        $this->call(InputsTableSeeder::class) ;
//		if(config('app.debug')) {
//			$this->call(DummyTableSeeder::class);
//		}
    }
}
