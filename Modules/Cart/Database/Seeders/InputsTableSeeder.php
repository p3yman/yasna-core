<?php

namespace Modules\Cart\Database\Seeders;

use App\Models\Feature;
use App\Models\Input;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class InputsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('inputs', $this->data());
        $this->relationships();
    }



    /**
     * attach inputs to posttype
     */
    protected function relationships()
    {
        $data = [
             'shop' => "tax_vat",
        ];

        foreach ($data as $key => $slugs) {
            $model = model("feature" , $key);
            if (!$model or !$model->id) {
                continue;
            }
            $model->attachInputs(explode(',', $slugs));

            foreach ($model->posttypes as $posttype) {
                $posttype->attachRequiredInputs();
            }
        }
    }



    /**
     * get  inputs array
     *
     * @return array
     */
    protected function data()
    {
        $data = [
             //[
             //     'slug'              => 'tax_vat',
             //     'title'             => 'مالیات بر ارزش افزوده',
             //     'order'             => '99',
             //     'type'              => 'downstream',
             //     'data_type'         => 'boolean',
             //     'css_class'         => '',
             //     'validation_rules'  => '',
             //     'purifier_rules'    => '',
             //     'default_value'     => '',
             //     'default_value_set' => '0',
             //     'hint'              => 'مقدار مالیات، در تنظیمات سایت تعریف می‌شود.',
             //     'meta-options'      => '',
             //],
        ];
        return $data;
    }
}
