<?php

namespace Modules\Cart\Database\Seeders;

use App\Models\Cart;
use App\Models\Posttype;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Cart\Entities\Courier;
use Modules\Yasna\Providers\DummyServiceProvider;
use Modules\Yasna\Providers\YasnaServiceProvider;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->couriers();
        $this->carts();
        $this->orders();
        $this->cartCalculations();
    }

    public function carts($count = 100, $truncate = true)
    {
        $data = [];

        for ($i = 1; $i <= $count; $i++) {
            $user     = User::inRandomOrder()->first();
            $an_admin = User::inRandomOrder()->first();
            $courier  = Courier::inRandomOrder()->first();

            if (!$user or !$courier) {
                continue;
            }

            $raised    = rand(0, 5);
            $confirmed = rand(0, 4) * $raised;
            $disbursed = rand(0, 3) * $raised;
            $paid      = rand(0, 1) * $disbursed;
            $sent      = rand(0, 3) * $confirmed;
            $delivered = rand(0, 2) * $sent;
            $closed    = rand(0, 1) * $delivered;
            $rejected  = !rand(0, 5) * $delivered;

            $data[] = [
                'user_id'      => $user->id,
                'courier_id'   => $courier->id,
                'title'        => DummyServiceProvider::persianWord(3),
                'currency'     => "IRR",
                'locale'       => "fa",
                'feedback'     => rand(1, 10),
                'created_at'   => $created_at = Carbon::now()->subDay(rand(1, 7)),
                'raised_at'    => $raised ? $created_at->addDay(rand(0, 1)) : null,
                'confirmed_at' => $confirmed ? $created_at->addDay(rand(0, 1)) : null,
                'disbursed_at' => $disbursed ? Carbon::now()->subDays(rand(1, 7)) : null,
                'paid_at'      => $paid ? Carbon::now()->subDays(rand(1, 7)) : null,
                'sent_at'      => $sent ? $created_at->addDay(rand(0, 1)) : null,
                'delivered_at' => $delivered ? $created_at->addDay(rand(1, 2)) : null,
                'rejected_at'  => $rejected ? $created_at->addDay(rand(0, 1)) : null,
                'closed_at'    => $closed ? $created_at->addDay(rand(0, 1)) : null,
                'raised_by'    => $raised ? $user->id : 0,
                'confirmed_by' => $confirmed ? $an_admin->id : 0,
                'disbursed_by' => $disbursed ? $user->id : 0,
                'paid_by'      => $paid ? $an_admin->id : 0,
                'sent_by'      => $sent ? $an_admin->id : 0,
                'delivered_by' => $delivered ? $an_admin->id : 0,
                'rejected_by'  => $rejected ? $an_admin->id : 0,
                'closed_by'    => $closed ? $an_admin->id : 0,
            ];
        }

        yasna()->seed('carts', $data, $truncate);
    }

    public function couriers($count = 10, $truncate = true)
    {
        $data = [];
        for ($i = 1; $i <= $count; $i++) {
            $data[] = [
                'slug'        => str_slug("dummy-shop-" . strtolower(DummyServiceProvider::englishWord())),
                'locales'     => "fa,en",
                'currencies'  => "IRR",
                'order'       => rand(1, 50),
                'meta-titles' => [
                    'fa' => DummyServiceProvider::persianWord(),
                    'en' => DummyServiceProvider::englishWord(),
                ],
                'meta-prices' => [
                    'IRR' => rand(1000, 10000),
                ],

            ];
        }

        yasna()->seed('couriers', $data, $truncate);
    }

    public function orders()
    {
        $data = [];
        foreach (Cart::all() as $cart) {
            $posttype = posttype("shop");

            for ($i = 1; $i <= rand(1, 10); $i++) {
                do {
                    $ware = $posttype->realWares()->where('is_available', 1)->inRandomOrder()->first();
                } while (!$ware);

                $data[] = [
                    'posttype_id'           => $posttype->id,
                    'post_id'               => $ware->post->id,
                    'ware_id'               => $ware->id,
                    'cart_id'               => $cart->id,
                    'user_id'               => $cart->user_id,
                    'currency'              => "IRR",
                    'locale'                => "fa",
                    'count'                 => $count = rand(10, 100) / 10,
                    'single_original_price' => $single_original_price = $ware->price('original')->get(),
                    'single_sale_price'     => $single_sale_price = $ware->price('sale')->get(),
                    'original_price'        => $single_original_price * $count,
                    'sale_price'            => $single_sale_price * $count,
                    'rejected_at'           => ($cart->isRejected() and rand(0, 1)) ? $cart->rejected_at : null,
                    'delivered_at'          => $cart->isDelivered() ? $cart->delivered_at : null,
                ];
            }
        }
        yasna()->seed('orders', $data, true);
    }

    public function cartCalculations()
    {
        foreach (Cart::all() as $cart) {
            $cart->refreshCalculations();
        }
    }
}
