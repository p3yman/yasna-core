<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_records', function (Blueprint $table) {
            $table->increments('id');

            /*-----------------------------------------------
            | Relations ...
            */
            $table->unsignedInteger('cart_id')->index();


            /*-----------------------------------------------
            | Main Info ...
            */
            $table->string('type')->index();
            $table->string('old_thing')->nullable();
            $table->string('new_thing')->nullable();
            $table->text('remarks')->nullable();

            /*-----------------------------------------------
            | Meta ...
            */
            $table->longText('meta')->nullable();

            /*-----------------------------------------------
            | Timestamp ...
            */
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('rejected_by')->default(0);
            $table->index('created_at');

            /*-----------------------------------------------
            | Reserved for Later Use ...
            */
            $table->boolean('converted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_records');
    }
}
