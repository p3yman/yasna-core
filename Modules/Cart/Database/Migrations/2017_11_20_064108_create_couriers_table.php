<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couriers', function (Blueprint $table) {
            $table->increments('id');

            /*-----------------------------------------------
            | Main Info ...
            */
            $table->string('slug')->index() ;
            $table->string('locales');
            $table->string('currencies')->nullable();
            $table->tinyInteger('order')->default(0);
            $table->text('region')->nullable();

            /*-----------------------------------------------
            | Meta ...
            */
            $table->longText('meta')->nullable();

            /*-----------------------------------------------
            | Timestamps ...
            */
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);

            $table->index('created_at');


            /*-----------------------------------------------
            | Reserved for Later Use ...
            */
            $table->boolean('converted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couriers');
    }
}
