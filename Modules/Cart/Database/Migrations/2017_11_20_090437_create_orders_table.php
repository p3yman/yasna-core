<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

            /*-----------------------------------------------
            | Relations ...
            */
            $table->unsignedInteger('posttype_id')->index();
            $table->unsignedInteger('post_id')->index();
            $table->unsignedInteger('ware_id')->index();
            $table->unsignedInteger('cart_id')->index();
            $table->unsignedInteger('user_id')->index();

            /*-----------------------------------------------
            | Main Info ...
            */
            $table->string('currency', 10);
            $table->string('locale', 10);
            $table->float('count', 15, 2)->default(1);

            /*-----------------------------------------------
            | Financial Fields ...
            */
            $table->float('single_original_price', 15, 2)->default(0)->index();
            $table->float('single_sale_price', 15, 2)->default(0)->index();
            $table->float('original_price', 15, 2)->default(0)->index();
            $table->float('sale_price', 15, 2)->default(0)->index();

            /*-----------------------------------------------
            | Meta ...
            */
            $table->longText('meta')->nullable();

            /*-----------------------------------------------
            | Timestamps ...
            */
            $table->timestamps();
            $table->timestamp('delivered_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('delivered_by')->default(0);
            $table->unsignedInteger('rejected_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);
            $table->index('created_at');

            /*-----------------------------------------------
            | Reserved for Later Use ...
            */
            $table->boolean('converted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
