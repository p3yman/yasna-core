<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');

            /*-----------------------------------------------
            | Relations ...
            */
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('courier_id')->index();
            $table->unsignedInteger('address_id')->index();

            /*-----------------------------------------------
            | Main Info ...
            */
            $table->string('title')->nullable(); // <~~ For custom usages of users
            $table->string('currency', 10);
            $table->string('locale', 10);
            $table->tinyInteger('feedback');

            /*-----------------------------------------------
            | Financial Fields ...
            */
            $table->float('original_price', 15, 2)->default(0)->index();
            $table->float('sale_price', 15, 2)->default(0)->index();
            $table->float('delivery_price', 15, 2)->default(0)->index();
            $table->float('added_discount', 15, 2)->default(0)->index();
            $table->float('tax_amount', 15, 2)->default(0)->index();
            $table->float('invoiced_amount', 15, 2)->default(0)->index();
            $table->float('paid_amount', 15, 2)->default(0)->index();

            /*-----------------------------------------------
            | Cache ...
            */
            $table->integer('total_items');
            $table->integer('delivered_items');

            /*-----------------------------------------------
            | Meta ...
            */
            $table->longText('meta')->nullable();


            /*-----------------------------------------------
            | Timestamps ...
            */
            $table->timestamps();
            $table->timestamp('raised_at')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('disbursed_at')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('sent_at')->nullable();
            $table->timestamp('delivered_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->timestamp('closed_at')->nullable();
            $table->softDeletes();

            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('raised_by')->default(0);
            $table->unsignedInteger('confirmed_by')->default(0);
            $table->unsignedInteger('disbursed_by')->default(0);
            $table->unsignedInteger('paid_by')->default(0);
            $table->unsignedInteger('sent_by')->default(0);
            $table->unsignedInteger('delivered_by')->default(0);
            $table->unsignedInteger('rejected_by')->default(0);
            $table->unsignedInteger('closed_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);

            $table->index('created_at');


            /*-----------------------------------------------
            | Reserved for Later Use ...
            */
            $table->boolean('converted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
