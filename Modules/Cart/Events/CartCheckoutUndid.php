<?php namespace Modules\Cart\Events;

use Modules\Yasna\Services\YasnaEvent;

class CartCheckoutUndid extends YasnaEvent
{
    public $model_name = "Cart";
}
