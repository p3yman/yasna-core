<?php namespace Modules\Cart\Events;

use Modules\Yasna\Services\YasnaEvent;

class CartPaymentStatusChanged extends YasnaEvent
{
    public $model_name = 'Cart';
}
