<?php namespace Modules\Cart\Events;

use Modules\Yasna\Services\YasnaEvent;

class CartConfirmed extends YasnaEvent
{
    public $model_name = "Cart";
}
