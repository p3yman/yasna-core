<?php namespace Modules\Cart\Events;

use Modules\Yasna\Services\YasnaEvent;

class CartStatusChanged extends YasnaEvent
{
    public $model_name = 'Cart';
}
