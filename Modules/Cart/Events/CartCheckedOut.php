<?php namespace Modules\Cart\Events;

use Modules\Yasna\Services\YasnaEvent;

class CartCheckedOut extends YasnaEvent
{
    public $model_name = 'Cart';
}
