<?php

return [
    'status' => [
        'color' => [
            'pending'                    => "warning",
            'under_packing'              => "pink",
            'support_request'            => "danger",
            'under_delivery'             => "primary",
            'delivered'                  => "green",
            'under_payment'              => "red",
            'under_payment_confirmation' => "warning",
            'paid'                       => "green",
            'not_raised'                 => "info",
            'archive'                    => "gray",
        ],
        'icon'  => [
            'pending'                    => "hourglass-half",
            'under_packing'              => "cubes",
            'support_request'            => "ticket",
            'under_delivery'             => "truck",
            'delivered'                  => "check",
            'under_payment'              => "money",
            'under_payment_confirmation' => "money",
            'paid'                       => "check",
            'not_raised'                 => "cart-arrow-down",
            'archive'                    => "archive",
        ],
    ],
];
