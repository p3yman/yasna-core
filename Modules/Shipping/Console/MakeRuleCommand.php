<?php

namespace Modules\Shipping\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MakeRuleCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-shipping-rule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a shipping rule class.';
    


    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('ShippingRules');
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Rule')) {
            return $file_name . "Rule";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "rule.stub";
    }
}
