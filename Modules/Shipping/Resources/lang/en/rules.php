<?php

return [
     "title"              => "Rules and Conditions",
     "nothing_added"      => "No rules for the time being!",
     "nothing_selected"   => "No rules are added. You may add them from the cross list.",
     "nothing_defined"    => "Nothing to select!",
     "nothing_found"      => "Nothing found!",
     "add"                => "New Rule",
     "remove"             => "Remove Rule",
     "edit"               => "Edit Rule",
     "save_rule_new"      => "Create Rule",
     "save_rule_existing" => "Update Rule",
     "delete_warning"     => "Deletion may have no effect to the already-processed carts.",

     "weekdays"      => "Week Days",
     "weekdays-hint" => "Specify the days, if this shipping method is only available for certain week days.",
     "allowed"       => "White List",
     "not_allowed"   => "Black List",

     "size"         => "Size",
     "allowed_size" => "Maximum Size of Product",
     "size-hint"    => "If this shipping method is suitable for certain size of products, choose maximum size which is shippable with this method.",

     "weight"         => "Weight",
     "allowed_weight" => "Maximum Allowed Weight",
     "weight_hint"    => "Specify the maximum weight which this shipping method can transfer it.",

     "date_rule"              => "Date",
     "allowed_start_date"     => "Start Date of Allowed Period",
     "allowed_end_date"       => "End Date of Allowed Period",
     "not_allowed_start_date" => "Start Date of Not-Allowed Period",
     "not_allowed_end_date"   => "End Date of Not-Allowed Period",
     "date_hint"              => "If this shipping method is allowed or not allowed in a specified period of time, you can define that period by setting its boundaries. Note that, if no value has been set for each limitation (Start/End dates on [Not]Allowed Period), its corresponded bound on the period will be considered as unlimited.",

     "user_role_rule" => "Users Role",
     "roles_hint"     => "If the shipping method should be accessible for a specified set of roles or restricted for another one, this rule can be used.",
];
