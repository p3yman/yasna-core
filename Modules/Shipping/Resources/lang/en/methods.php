<?php
return [
     "title"        => "Shipping Methods",
     "single"       => "Shipping Method",
     "no_result"    => "No Matching Result!",
     "no_content"   => "Nothing to Display!",
     "create"       => "Create a New Method",
     "edit"         => "Edit",
     "misc"         => "Misc",
     "save_warning" => "Your changes won't be effected unless you save the method!",

     "serializing_error" => "Error in Data Processing! The problem might fix by refreshing the page.",
     "titles_error"      => "Specify at least one title for the shipping method",

     "activate"   => "Activate",
     "deactivate" => "Deactivate",
];
