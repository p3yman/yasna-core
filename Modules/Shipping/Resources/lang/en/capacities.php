<?php

/*
|--------------------------------------------------------------------------
| Shipment Standard Capacities
|--------------------------------------------------------------------------
| The keys are defined in Modules\Coupons\Services as constant values.
| The aforesaid class has methods to read these translations.
*/


return [
     "0" => "Unknown",
     "1" => "Pocket Size",
     "2" => "Bike Size",
     "3" => "Car Size",
     "4" => "Pickup-Truck Size",
     "5" => "Truck Size",
     "6" => "Trailer Size",
     "7" => "Ship Size",
];
