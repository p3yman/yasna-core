<?php
return [
     "title"        => "روش‌های حمل",
     "single"       => "روش حمل",
     "no_result"    => "نتیجه‌ای پیدا نشد.",
     "no_content"   => "چیزی برای نمایش وجود ندارد.",
     "create"       => "ایجاد روش جدید",
     "edit"         => "ویرایش اطلاعات",
     "misc"         => "متفرقه",
     "save_warning" => "تغییرات شما بدون ذخیره کردن روش حمل ماندگار نمی‌شوند.",

     "serializing_error" => "بروز خطا در پردازش داده‌ها. شاید با ریفرش صفحه این مشکل برطرف شود.",
     "titles_error"      => "حداقل یک عنوان را برای این روش حمل وارد نمایید.",

     "activate"   => "فعال‌سازی",
     "deactivate" => "غیرفعال‌سازی",
];
