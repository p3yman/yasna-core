<span class="pull-right">

    {!!
    widget("button")
         ->label("tr:manage::forms.button.hard_delete")
         ->target(shipping()->gethardDeleteMethodLink($model->hashid))
         ->shape('danger')
         ->condition($model->isTrashed())
         ->class("btn-taha mh10")
    !!}

    {!!
    widget("button")
         ->label('tr:manage::settings.view_setting')
         ->target(shipping()->getEditMethodLink($model->hashid))
         ->shapeIf($model->isNotTrashed(),"primary")
         ->class("btn-taha")
    !!}

</span>
