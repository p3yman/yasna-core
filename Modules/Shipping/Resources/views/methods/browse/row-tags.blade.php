@if($model->rulesCount())

    <span class="f12 text-gray mh10">
        {{ trans("shipping::rules.title") }}:
    </span>

    @foreach($model->getRuleTitles() as $title)

        @include('manage::widgets.grid-badge',[
             "color" => "gray-light" ,
             "text"  => $title ,
             "icon"  => false ,
        ])

    @endforeach


@else

    @include('manage::widgets.grid-text',[
         "color" => "gray" ,
         "text"  => trans("shipping::rules.nothing_added") ,
    ])


@endif
