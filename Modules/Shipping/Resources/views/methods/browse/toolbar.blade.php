<h3 style="background: transparent;">
	{{ trans("shipping::methods.title") }}

	<span class="pull-right">
		{!!
		    widget('button')
		    ->label(trans("shipping::methods.create"))
		    ->icon('plus')
		    ->target(shipping()->getCreateMethodLink())
		 !!}
	</span>
</h3>
