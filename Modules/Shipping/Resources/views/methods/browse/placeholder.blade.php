<div class="box-placeholder box-placeholder-lg text-center js_no-result" style="display: {{$display or "none"}}">
    {{ $text or trans('shipping::methods.no_result') }}
</div>
