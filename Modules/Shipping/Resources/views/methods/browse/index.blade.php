@extends('manage::layouts.template')

@section("content")

    @include("shipping::methods.browse.toolbar")
    <div class="container container-md">


        @if($models->count())
            {{--@TODO: Search is disabled to find a method for searching, in future. --}}
            {{--@include('manage::downstream.setting-search')--}}
            @include("shipping::methods.browse.grid")
            @include("shipping::methods.browse.placeholder")

        @else

            @include("shipping::methods.browse.placeholder" , [
                "text" => trans("shipping::methods.no_content"),
                "display" => "",
            ])

        @endif
    </div>
@endsection
