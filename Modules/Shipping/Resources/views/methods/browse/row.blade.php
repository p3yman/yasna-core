<div class="feature-item panel b js_setting-category">

    <div class="image-box has-icon">
        <i class="fa {{ $model->icon }} f40 mh20"></i>
    </div>

    <div class="content">
        <div class="p25">
            <h4 class="text-primary pb-lg">
                {{ $model->title }}
            </h4>

            <div class="mv js_category-tags">
                @include("shipping::methods.browse.row-tags")
            </div>

            <div class="clearfix">
                @include("shipping::methods.browse.row-activeness")
            </div>

            <div class="clearfix">
                @include("shipping::methods.browse.row-buttons")
            </div>

        </div>

    </div>

</div>
