@include('manage::widgets.grid-tiny',[
     "color" => $model->isTrashed()? "danger" : "success" ,
     "text"  => $model->isTrashed()? trans("manage::forms.status_text.inactive") : trans("manage::forms.status_text.active") ,
     "icon"  => $model->isTrashed()? "times" : "check" ,
     "link"  => shipping()->getActivenessMethodLink($model->hashid)
])
