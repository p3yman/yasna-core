@foreach($models as $model)
    <div id="divMethod-{{$model->hashid}}" data-src="{{ shipping()->getRefreshMethodLink($model->hashid) }}">
        @include("shipping::methods.browse.row")
    </div>
@endforeach
