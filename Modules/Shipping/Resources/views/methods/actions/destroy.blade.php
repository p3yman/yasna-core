@include("manage::layouts.modal-destroy", [
    "form_url"   => route("shipping-method-destroy"),
    "save_value" => "delete",
])
