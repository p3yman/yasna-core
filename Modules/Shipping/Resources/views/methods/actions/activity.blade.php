@php
    $modal_label = $model->isTrashed()? trans("shipping::methods.activate") : trans("shipping::methods.deactivate")
@endphp

@include("manage::layouts.modal-delete", [
    "form_url"    => route("shipping-method-save-activeness"),
    "modal_title" => $modal_label,
    "save_label"  => $modal_label,
    "save_shape"  => $model->isTrashed()? "success" : "warning",
    "save_value"  => $model->isTrashed()? "undelete": "delete",
])
