@php
    $add_link = shipping()->getAddRuleLink($class);
@endphp

<a href="{{ v0() }}" class="list-group-item" onclick="{{ $add_link }}">
    <i class="fa fa-plus text-primary"></i>
    <span class="mh-sm ">{{ $title }}</span>
</a>
