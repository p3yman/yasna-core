@php
        $edit_link   = shipping()->getEditRuleLink($rule,$identifier);
        $remove_link = shipping()->getRemoveRuleLink($rule,$identifier);
@endphp

<li class="list-group-item js_sortable_role" data-role-id="{{ $identifier }}">
    <em class="fa fa-bars fa-fw text-muted mr-lg js_sortable_role_handle" style="cursor: grab;"></em>

    <span class="clickable" onclick="{{ $edit_link }}">
        {{ $rule['_class']::getTitle() }}
    </span>

    <span class="pull-right" onclick="{{ $remove_link }}">
        <em class="fa fa-times fa-fw text-muted f14 clickable" style="opacity: 0.3"></em>
    </span>
    <span class="pull-right" onclick="{{ $edit_link }}">
        <em class="fa fa-edit fa-fw text-muted f16 clickable"></em>
    </span>
</li>
