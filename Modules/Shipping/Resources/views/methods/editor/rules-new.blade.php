<div class="panel panel-default">
    <div class="panel-heading text-bold">
        {{ trans('shipping::rules.add') }}
    </div>
    <div class="panel-body">
        <div id="js_new_rules_total">
            <div class="list-group"
                 data-height="200"
                 data-scrollable=""
                 style="overflow: hidden; width: auto; height: 200px;">

                @if(count($available_rules))
                    @include("shipping::methods.editor.rules-new-loop")
                @else
                    <div class="box-placeholder">
                        <p class="text-center">
                            {{ trans('shipping::rules.nothing_defined') }}
                        </p>
                    </div>
                @endif

            </div>
        </div>

        <div id="js_new_rules_search" style=" display: none;">
            <div class="list-group"
                 data-height="200"
                 data-scrollable=""
                 style="overflow: hidden; width: auto; height: 200px;">
            </div>
        </div>


    </div>

    @if(count($available_rules))
        @include("shipping::methods.editor.rules-new-search")
    @endif
</div>

