<div class="panel">
    <div class="panel-body">

        {!!
        widget("note")
        	->shape('warning')
        	->label("tr:shipping::methods.save_warning")
        	->id("divSaveWarning")
        	->hidden()
        !!}

        {!!
             widget('button')
             ->label('tr:manage::forms.button.save')
             ->class('btn-success btn-taha')
             ->type('submit')
             ->value('save')
             ->icon('save')
        !!}

        {{--{!!--}}
        {{--widget("button")--}}
          {{--->icon('flask')--}}
        	{{--->label("tr:shipping::tester.title")--}}
          {{--->shape('info')--}}
        	{{--->class("btn-outline btn-taha")--}}
{{--//        	->target("masterModal('".route("coupons-test-rule-form")."')") TODO: Make the test button work!--}}
        {{--!!}--}}

        {!! widget("feed") !!}
    </div>
</div>
