@component('shipping::methods.editor.panel',[
	"heading" => trans('shipping::rules.title') ,
])

<div class="row">
    <div class="col-md-7">
        <ul id="divCurrentRules" class="list-group js_sortable_roles">
            @include("shipping::methods.editor.rules-current")
        </ul>
        {!! widget('hidden')->name('_roles_order')->id('ja_roles_order') !!}
    </div>
    <div class="col-md-5">
        @include("shipping::methods.editor.rules-new")
    </div>
</div>

@endcomponent
