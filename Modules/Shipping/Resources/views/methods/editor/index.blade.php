@extends('manage::layouts.template')

@section("content")
    {!! widget('form-open')->target("name:shipping-method-save")->class('js') !!}
    {!! widget('hidden')->name('hashid')->value($model->hashid) !!}

    @include("shipping::methods.editor.title")
    @include("shipping::methods.editor.rules")
    @include("shipping::methods.editor.misc")
    @include("shipping::methods.editor.buttons")

    {!! widget("hidden")->name("_shipping_method")->id("txtPageSerializedModel")->value($model->serialize()) !!}

    {!! widget('form-close') !!}
@endsection
