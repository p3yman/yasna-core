
<div class="panel-footer">
    <div class="input-group">
        <label for="search_rule" class="hidden"></label>
        @php
            $json = [];
            $count = 0;
            foreach ($available_rules as $key => $rule){
                $json[$count]['title'] = $rule;
                $json[$count]['link'] = $key;
                $count++;
            }
        @endphp
        @php $rules_json = json_encode($json); @endphp

        {!!
            widget('input')
            ->class('input-sm')
            ->id('js-search_rule')
         !!}

        <span class="input-group-addon bg-primary">
            <i class="fa fa-search m0"></i>
        </span>
    </div>
</div>

<script id="rules_store" data-message="{{ trans('shipping::rules.nothing_found') }}" data-value="{{ $rules_json }}"></script>
