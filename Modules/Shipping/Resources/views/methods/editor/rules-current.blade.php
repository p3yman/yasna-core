
@if(count($model->getRules()))
    @include("shipping::methods.editor.rules-current-loop")
@else
    <div class="box-placeholder">
        <p class="text-center p30 text-gray">
            {{ trans('shipping::rules.nothing_selected') }}
        </p>
    </div>
@endif

<script>
    $(document).ready(function () {
        getRolesOrder();
    })
</script>
