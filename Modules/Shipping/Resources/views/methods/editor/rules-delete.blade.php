{!!
widget("modal")
	->label($class::getTitle())
	->target("name:shipping-rule-remove-action")
!!}

{!!
widget("hidden")
	->name("_shipping_method")
	->id("txtSerializedMethod")
!!}

{!!
widget("hidden")
	->name("_identifier")
	->value($identifier)
!!}

{!!
widget("hidden")
	->name("_class")
	->value($class)
!!}

<div class="modal-body">
    {!!
    widget("note")
    	   ->label("tr:shipping::rules.delete_warning")
    	   ->shape('warning')
    !!}
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" , [
        "save_label" => trans("manage::forms.button.sure_delete"),
        "save_shape" => "warning",
    ])
</div>

<script>
    $("#txtSerializedMethod").val( $("#txtPageSerializedModel").val() )
</script>
