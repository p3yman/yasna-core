@component('shipping::methods.editor.panel',[
	"heading" => trans('validation.attributes.title') ,
])

    @foreach(get_setting("site_locales") as $locale)
        {!!
        widget("input")
        	->name("titles[$locale]")
        	->value($model->titleIn($locale))
        	->label("tr:yasna::lang.$locale")
        !!}
    @endforeach

@endcomponent
