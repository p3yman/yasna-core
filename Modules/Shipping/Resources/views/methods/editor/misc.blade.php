@component('shipping::methods.editor.panel',[
	"heading" => trans('shipping::methods.misc') ,
])
    {!!
    widget("text")
         ->name("slug")
         ->value($model->slug)
         ->label("tr:validation.attributes.slug")
    !!}


    {!!
    widget("icon-picker")
         ->name("icon")
         ->value($model->getMeta('icon'))
         ->label("tr:validation.attributes.icon")
    !!}

@endcomponent
