{!!
widget("modal")
	->label($class::getTitle())
	->target("name:shipping-rule-save")
!!}

{!!
widget("hidden")
	->name("_shipping_method")
	->id("txtSerializedMethod")
!!}

{!!
widget("hidden")
	->name("_identifier")
	->value($identifier)
!!}

{!!
widget("hidden")
	->name("_class")
	->value($class)
!!}

<div class="modal-body">
    {!! $class::renderForm($values) !!}
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" , [
        "save_label" => trans("shipping::rules.save_rule_" . ($identifier? "existing" : "new") ),
        "save_shape" => $identifier? "primary" : "success",
    ])
</div>

<script>
    $("#txtSerializedMethod").val( $("#txtPageSerializedModel").val() )
</script>
