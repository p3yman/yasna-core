/**
 * Coupons
 * -------------------------
 * Created by Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2018-08-28
 */

jQuery(function ($) {
    let radios = $('.radio_form_toggler input[type=radio]');
    let store  = $('#rules_store');
    let rules  = (store.length)? JSON.parse(store.attr('data-value')):[];
    let alertMessage = store.attr('data-message');
    let resultView = $('#js_new_rules_search');
    let totalView = $('#js_new_rules_total');
    $(document).ready(function () {
        makeSortable();

        // Show / Hide form fields associate with the radio
        radios.on('change', function () {
            radios.each(function (i, radio) {
                let target = $(radio).attr('data-toggle-view');
                if ($(radio).is(':checked')) {
                    // Show Checked one
                    $('#' + target).slideDown();
                } else {
                    // Hide rest
                    $('#' + target).hide();
                }
            });
        }).change();

        // Search in rules
        $('#js-search_rule').on('keyup', function () {
            let value = $(this).val().trim();
            reset();

            if (value.length) {
                showResult(searchArrayFor(value));
            }
        });

        // Coupon Example Creator
        $(document).on('change','#js_coupon_format :input' , function () {
            let id = $(this).attr('id');
            let value = $(this).val().trim();

            if(value.length){
                $('[data-input-id='+ id +']').empty().text(value);
            }else {
                $('[data-input-id='+ id +']').text("NoValue")
            }
        })
    });



    /**
     * Searches setting array for result
     * @param key
     * @return {{}}
     */
    function searchArrayFor(key) {
        let result      = {};
        let finalResult = {};

        result = rules.filter(function (rule) {
            return (new RegExp('^' + RegExp.escape(key)).test(rule.title))
        });
        return result;
    }


    /**
     * shows result with proper html element
     * @param results
     */
    function showResult(results) {
        let elements = "";

        if (results.length < 1) {
            elements = "<div class=\"box-placeholder\"><p class=\"text-center\">"+
                        alertMessage +"</p></div>";

        } else {
            elements = results.map(function (result) {
                let modalUrl = url('manage/shipping-methods/rules/add/' + result['link']).replace(/\\/g, '%5C');
                return "<a href=\"javascript:void(0)\" class=\"list-group-item\" onclick=\"masterModal('" +
                    modalUrl +"')\"><i class=\"fa fa-plus text-primary\"></i><span class=\"mh-sm\">"+
                        result['title'] +"</span></a>";
            }).join(" ");
        }

        updateResult(elements);
    }


    /**
     * shows result container and hides categories
     * @param result
     */
    function updateResult(result) {
        resultView.find('.list-group').empty().append(result);
        resultView.show();
        totalView.hide();
    }


    /**
     * resets view
     */
    function reset() {
        resultView.find('.list-group').empty();
        resultView.hide();
        totalView.show();
    }


    /**
     * escape RegExp in string
     * @param s
     */
    RegExp.escape = function (s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    };


    /**
     * Makes roles sortable
     */
    function makeSortable() {
        let sortable_fields = document.getElementsByClassName('js_sortable_roles');
        [].forEach.call(sortable_fields, function (el){
            Sortable.create(el, {
                group: 'specs',
                draggable: '.js_sortable_role',
                handle: '.js_sortable_role_handle',
                animation: 150,
                sort: true,
                onEnd: function () {
                    $('#divSaveWarning').slideDown();
                    getRolesOrder();
                    saveRulesOrder();
                },
            });
        });
    }


    /**
     * Save rules order
     */
    function saveRulesOrder()
    {
        forms_log('saving order...');

        let $div = $("#divCurrentRules");
        $div.addClass('loading');

        $.ajax({
            type: "POST",
            url: url('manage/shipping-methods/rules/save-order'),
            data: {
                order: $("#ja_roles_order").val(),
                rulebook: $("#txtPageSerializedModel").val(),
                _token: $("input[name=_token]").val()
            },
            cache: false
        })
            .done(function(html) {
                $div.html(html).removeClass('loading');
            })
        ;

    }

}); //End Of siaf!




/**
 * Get Roles Order
 */
function getRolesOrder(){
    let order = [];

    $.each($('.js_sortable_roles').find('.js_sortable_role'), function() {
        order.push($(this).data('role-id'));
    });

    $('#ja_roles_order').val(order.join(','));
}


/**
 * Updates model every second
 * @param serialized
 */
function updateSerializedModel(serialized)
{
    forms_log('updating...');
    $("#txtPageSerializedModel").val(serialized);
    setTimeout(function() {
        refreshCurrentRules();
    } , 1000);

}


/**
 * Refresh rules with ajax
 */
function refreshCurrentRules()
{
    forms_log('refreshing...');

    let $div = $("#divCurrentRules");
    $div.addClass('loading');

    $.ajax({
        type: "POST",
        url: url('manage/shipping-methods/refresh-rule'),
        data: {
            rulebook: $("#txtPageSerializedModel").val(),
            _token: $("input[name=_token]").val()
        },
        cache: false
    })
        .done(function(html) {
            $div.html(html).removeClass('loading');
        })
    ;
}
