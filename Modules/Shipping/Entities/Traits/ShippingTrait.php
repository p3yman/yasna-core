<?php

namespace Modules\Shipping\Entities\Traits;

use App\Models\ShippingMethod;
use Modules\Shipping\Services\ShippingInvoiceObject;

/**
 * Use this trait in the desired Shippable entity to implement some of the necessary methods.
 * The hosting entity is supposed to implement Modules\Shipping\Services\Shippable interface.
 * @method  ShippingInvoiceObject toShippingInvoice()
 */
trait ShippingTrait
{

    /**
     * @inheritdoc
     */
    public function availableShippingMethods(): array
    {
        $array = [];
        $invoice = $this->toShippingInvoice();

        foreach (ShippingMethod::all() as $method) {
            $i = clone $invoice;
            if ($method->isSuitableFor($i)) {
                $method->price = $method->priceFor($i);
                $array[] = $method;
            }
        }

        return $array;
    }
}
