<?php

namespace Modules\Shipping\Entities\Traits;

trait MethodResourcesTrait
{
    /**
     * boot MethodResourcesTrait
     *
     * @return void
     */
    public static function bootMethodResourcesTrait()
    {
        static::addDirectResources([
             "slug",
        ]);
    }



    /**
     * get Title resource.
     *
     * @return string
     */
    protected function getTitleResource()
    {
        return $this->getTitleInCurrentLocale();
    }



    /**
     * get Rules resource.
     *
     * @return array
     */
    protected function getRulesResource()
    {
        return $this->getRuleTitles();
    }
}
