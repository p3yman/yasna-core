<?php

namespace Modules\Shipping\Entities\Traits;

use Modules\Shipping\Services\ShippingInvoiceObject;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;

trait MethodProcessTrait
{
    /**
     * keep track of the processed invoices
     *
     * @var array
     */
    protected $processed_invoices = [];



    /**
     * determine if the current shipping method is suitable for the given invoice
     *
     * @param ShippingInvoiceObject $invoice
     *
     * @return bool
     */
    public function isSuitableFor(ShippingInvoiceObject $invoice): bool
    {
        $invoice = $this->processFor($invoice);

        return $invoice->isSuitable();
    }



    /**
     * determine the price of the current shipping method for the given invoice
     *
     * @param ShippingInvoiceObject $invoice
     *
     * @return float
     */
    public function priceFor(ShippingInvoiceObject $invoice): float
    {
        $invoice = $this->processFor($invoice);

        return $invoice->getPrice();
    }



    /**
     * process the given invoice against the current shipping method
     *
     * @param ShippingInvoiceObject $invoice
     * @param bool                  $force_fresh
     *
     * @return ShippingInvoiceObject
     */
    protected function processFor(ShippingInvoiceObject $invoice, $force_fresh = false)
    {
        $key = $invoice->md5();
        if (!$force_fresh and in_array($key, $this->processed_invoices)) {
            return $this->processed_invoices[$key];
        }

        foreach ($this->getRules() as $identifier => $params) {
            $invoice = $this->getOneRuleProcessed($params['_class'], $invoice, $params);
        }
        $this->processed_invoices[$key] = $invoice;

        return $invoice;
    }



    /**
     * get one rule processed
     *
     * @param string                $identifier
     * @param ShippingRule          $class
     * @param ShippingInvoiceObject $invoice
     * @param array                 $params
     *
     * @return ShippingInvoiceObject;
     */
    protected function getOneRuleProcessed($class, ShippingInvoiceObject $invoice, $params)
    {
        return $class::run($this, $invoice, $params);
    }
}
