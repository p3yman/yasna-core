<?php

namespace Modules\Shipping\Entities;

use Modules\Shipping\Entities\Traits\MethodProcessTrait;
use Modules\Shipping\Entities\Traits\MethodResourcesTrait;
use Modules\Yasna\Services\ModelTraits\YasnaLocaleTitlesTrait;
use Modules\Yasna\Services\ModelTraits\YasnaRuleParserTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingMethod extends YasnaModel
{
    use SoftDeletes;
    use YasnaLocaleTitlesTrait;
    use YasnaRuleParserTrait;
    use MethodProcessTrait;
    use MethodResourcesTrait;



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             'icon',
        ];
    }



    /**
     * get the icon attribute
     *
     * @return string
     */
    public function getIconAttribute()
    {
        $icon = $this->getMeta("icon");

        if (!$icon) {
            $icon = "fa-circle-o";
        }

        return $icon;
    }

}
