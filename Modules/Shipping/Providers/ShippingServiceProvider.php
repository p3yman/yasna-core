<?php

namespace Modules\Shipping\Providers;

use Modules\Shipping\Console\MakeRuleCommand;
use Modules\Shipping\ShippingRules\DateRule;
use Modules\Shipping\ShippingRules\PriceRule;
use Modules\Shipping\ShippingRules\SizeRule;
use Modules\Shipping\ShippingRules\UserRoleRule;
use Modules\Shipping\ShippingRules\WeekDaysRule;
use Modules\Shipping\ShippingRules\WeightRule;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class ShippingServiceProvider
 *
 * @package Modules\Shipping\Providers
 */
class ShippingServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerSettingSidebar();
        $this->registerServices();
        $this->registerArtisanCommands();
        $this->registerShippingRules();
    }



    /**
     * register setting sidebar
     */
    protected function registerSettingSidebar()
    {
        service("manage:settings_sidebar")
             ->add("shipping")
             ->link("shipping-methods")
             ->trans("shipping::methods.title")
             ->order(20)
        ;
    }



    /**
     * register services
     */
    protected function registerServices()
    {
        module("shipping")
             ->register(shipping()::RULES_SERVICE_NAME, "keeps the shipping rule classes");
    }



    /**
     * register artisan commands
     */
    protected function registerArtisanCommands()
    {
        $this->addArtisan(MakeRuleCommand::class);
    }



    /**
     * register shipping rules
     */
    protected function registerShippingRules()
    {
        shipping()->registerRule(WeekDaysRule::class);
        shipping()->registerRule(SizeRule::class);
        shipping()->registerRule(WeightRule::class);
        shipping()->registerRule(DateRule::class);
        shipping()->registerRule(UserRoleRule::class);
        shipping()->registerRule(PriceRule::class);
    }
}
