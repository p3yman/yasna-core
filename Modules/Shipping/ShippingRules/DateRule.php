<?php

namespace Modules\Shipping\ShippingRules;

use Carbon\Carbon;
use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;


/**
 * Class DateRule set constraints based on effective date of an invoice to
 * using a shipping method. It defines two types of constraint: White Period
 * and Black Period. White period is one which the shipping period is available
 * on that and the black is against of white.
 *
 * @package Modules\Shipping\ShippingRules
 */
class DateRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("shipping::rules.date_rule");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("persian_date_picker")
             ->name("allowed_start_date")
             ->label("tr:shipping::rules.allowed_start_date")
             ->purificationRules("date")
             ->inForm()
        ;

        $form->add("persian_date_picker")
             ->name("allowed_end_date")
             ->label("tr:shipping::rules.allowed_end_date")
             ->purificationRules("date")
             ->inForm()
        ;

        $form->add("persian_date_picker")
             ->name("not_allowed_start_date")
             ->label("tr:shipping::rules.not_allowed_start_date")
             ->purificationRules("date")
             ->inForm()
        ;

        $form->add("persian_date_picker")
             ->name("not_allowed_end_date")
             ->label("tr:shipping::rules.not_allowed_end_date")
             ->purificationRules("date")
             ->inForm()
        ;

        $form->add('note')
             ->shape("default")
             ->purificationRules("date")
             ->label(trans_safe("shipping::rules.date_hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->givenDateIsInAllowedRange() && $this->givenDateIsNotInNotAllowedRange();
    }



    /**
     * Checks invoice effective date is in allowed range or not. If it was, the
     * method returns true, otherwise it returns false
     *
     * @return bool
     */
    private function givenDateIsInAllowedRange(): bool
    {
        $given = $this->invoice->getEffectiveDate();
        $start = $this->carbonizeDate($this->para['allowed_start_date']);
        $end   = $this->carbonizeDate($this->para['allowed_end_date']);

        return $this->givenDateIsInRange($given, $start, $end);
    }



    /**
     * Checks invoice effective date is in allowed range or not. If it was, the
     * method returns false, otherwise it returns true
     *
     * @return bool
     */
    private function givenDateIsNotInAllowedRange(): bool
    {
        return !$this->givenDateIsInAllowedRange();
    }



    /**
     * Checks invoice effective date is in not allowed range or not. If it was,
     * the method returns true, otherwise it returns false
     *
     * @return bool
     */
    private function givenDateIsInNotAllowedRange(): bool
    {
        $given = $this->invoice->getEffectiveDate();
        $start = $this->carbonizeDate($this->para['not_allowed_start_date']);
        $end   = $this->carbonizeDate($this->para['not_allowed_end_date']);

        return $this->givenDateIsInRange($given, $start, $end);
    }



    /**
     * Checks invoice effective date is in not allowed range or not. If it was,
     * the method returns false, otherwise it returns true
     *
     * @return bool
     */
    private function givenDateIsNotInNotAllowedRange(): bool
    {
        return !$this->givenDateIsInNotAllowedRange();
    }



    /**
     * Checks a given time is on a range or not. If it was, it returns true,
     * otherwise return false
     *
     * @param Carbon      $given
     * @param Carbon|null $start
     * @param Carbon|null $end
     *
     * @return bool
     */
    private function givenDateIsInRange(
         Carbon $given,
         ?Carbon $start,
         ?Carbon $end
    ): bool {
        // if start time is not null, checks given time is after that
        if (!is_null($start) && $given->lt($start)) {
            return false;
        }

        // if end time is not null, checks given time is before that
        if (!is_null($end) && $given->gt($end)) {
            return false;
        }

        return true;
    }



    /**
     * Checks a given date is in range or not. If not return true, otherwise
     * return false
     *
     * @param Carbon      $given
     * @param Carbon|null $start
     * @param Carbon|null $end
     *
     * @return bool
     */
    private function givenDateIsNotInRange(
         Carbon $given,
         ?Carbon $start,
         ?Carbon $end
    ): bool {
        return !$this->givenDateIsInRange($given, $start, $end);
    }



    /**
     * Convert a given time to a Carbon instance. If something other than
     * string or Carbon instance given, it returns null
     *
     * @param mixed $date
     *
     * @return Carbon|null
     */
    private function carbonizeDate($date): ?Carbon
    {
        if ($date instanceof Carbon) {
            return $date;
        }

        if (is_string($date)) {
            if ($date == '') {
                return null;
            }

            return new Carbon($date);
        }

        return null;
    }
}
