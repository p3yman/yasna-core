<?php

namespace Modules\Shipping\ShippingRules\Abs;

use Modules\Shipping\Entities\ShippingMethod;
use Modules\Shipping\Services\PriceModifier;
use Modules\Shipping\Services\ShippingInvoiceObject;

/**
 * Class ShippingRule
 *
 * @property ShippingInvoiceObject $invoice
 * @package Modules\Shipping\ShippingRules\Abs
 */
trait RunTrait
{
    /**
     * static object builder
     *
     * @param ShippingMethod        $method
     * @param ShippingInvoiceObject $invoiceObject
     * @param array                 $parameters
     *
     * @return ShippingInvoiceObject
     */
    public static function run(ShippingMethod $method, ShippingInvoiceObject $invoice, array $parameters)
    {
        /**
         * var ShippingRule $object;
         */
        $called = static::calledClass();
        $object = new $called($method, $invoice, $parameters);

        return $object->process();
    }



    /**
     * process the rule and apply changes on the invoice object
     *
     * @param string $identifier
     *
     * @return ShippingInvoiceObject
     */
    protected function process(): ShippingInvoiceObject
    {
        $called = static::calledClass();

        $this->invoice->addAppliedRule($called);
        $this->invoice->applyRuleSuitability($called, $this->isSuitable());

        if (method_exists($this, "getPrice")) {
            $this->invoice->applyRulePrice(static::class, $this->getPrice(), $this->getPriceModifier());
        } else {
            $this->invoice->applyRulePrice(static::class, 0, PriceModifier::ADD_AMOUNT);
        }

        return $this->invoice;
    }



    /**
     * set applied rule (called) to the invoice
     */
    protected function setAppliedRule()
    {
        $this->invoice->addAppliedRule(static::calledClass());
    }
}
