<?php

namespace Modules\Shipping\ShippingRules\Abs;

use Modules\Shipping\Entities\ShippingMethod;
use Modules\Shipping\Services\PriceModifier;
use Modules\Shipping\Services\ShippingInvoiceObject;
use Modules\Yasna\Services\GeneralTraits\ClassRecognitionsTrait;

/**
 * Class ShippingRule
 * @method float getPrice()
 *
 * @package Modules\Shipping\ShippingRules\Abs
 */
abstract class ShippingRule
{
    use ClassRecognitionsTrait;
    use FormTrait;
    use RunTrait;

    /**
     * keep the invoice object
     *
     * @var ShippingInvoiceObject
     */
    protected $invoice;

    /**
     * keep the coupon-rulebook model object
     *
     * @var ShippingMethod
     */
    protected $method;

    /**
     * keep the user settings array
     *
     * @var array
     */
    protected $para;



    /**
     * AbstractRule constructor.
     *
     * @param ShippingMethod        $method
     * @param ShippingInvoiceObject $invoice
     * @param array                 $parameters
     */
    public function __construct(ShippingMethod $method, ShippingInvoiceObject $invoice, array $parameters = [])
    {
        $this->method  = $method;
        $this->invoice = $invoice;
        $this->para    = $parameters;
    }



    /**
     * get this rule's identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return self::calledClassName();
    }



    /**
     * get price modifier mode
     *
     * @return string
     */
    public function getPriceModifier(): string
    {
        return PriceModifier::ABSOLUTE;
    }



    /**
     * get human-identifiable title of the rule
     *
     * @return string
     */
    public abstract static function getTitle(): string;



    /**
     * determine whether or not the current rule is suitable for the passed InvoiceObject
     *
     * @return bool
     */
    public abstract function isSuitable(): bool;
}
