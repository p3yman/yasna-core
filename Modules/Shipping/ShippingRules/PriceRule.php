<?php

namespace Modules\Shipping\ShippingRules;

use Modules\Shipping\ShippingRules\Abs\ShippingRule;
use Modules\Manage\Services\Form;
use Modules\Shipping\Services\PriceModifier;


class PriceRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("shipping::rules.price");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("input")
             ->name("price")
             ->label("tr:shipping::rules.price")
             ->inForm()
             ->numberFormat()
             ->purificationRules("ed|numeric")
             ->type("float")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return true;
    }



    /**
     * get the price governed by the rule
     *
     * @return float
     */
    public function getPrice(): float
    {
        return (float) $this->para['price'];
    }
}
