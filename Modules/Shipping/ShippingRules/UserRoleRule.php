<?php

namespace Modules\Shipping\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;

/**
 * Class UserRoleRule sets a constraint on using a shipping method based on the
 * role of the user. It has a white list for specifying the roles which can use
 * this shipping method and a black list that is against of the black list.
 *
 * @package Modules\Shipping\ShippingRules
 */
class UserRoleRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("shipping::rules.user_role_rule");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("roles_allowed")
             ->label("tr:shipping::rules.allowed")
             ->inForm()
             ->options(static::getRolesCombo())
             ->valueField("slug")
             ->captionField("caption")
        ;

        $form->add("selectize")
             ->name("roles_not_allowed")
             ->label("tr:shipping::rules.not_allowed")
             ->inForm()
             ->options(static::getRolesCombo())
             ->valueField("slug")
             ->captionField("caption")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("shipping::rules.roles_hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        $allowed_roles     = $this->getAllowedRoles();
        $not_allowed_roles = $this->getNotAllowedRoles();

        return $this->userHasRoles($allowed_roles) && $this->userHasNotRoles($not_allowed_roles);
    }



    /**
     * Return a combo widget ready-to-use array of roles
     *
     * @return array
     */
    private static function getRolesCombo(): array
    {
        return role()->all()->map(function ($role) {
            return [
                 'slug'    => $role->slug,
                 'caption' => $role->title,
            ];
        })
                     ->toArray()
             ;
    }



    /**
     * Check that the user who is attached to the invoice has any roles of a
     * given set or not. if the user at least has one of the given roles, it
     * returns true, otherwise it returns false.
     *
     * @param array $roles Non-associative array of roles' slugs
     *
     * @return bool
     */
    private function userHasRoles(array $roles): bool
    {
        $user_roles = $this->getUserRoles();

        // if intersection of user's roles and a given roles set is not empty,
        // we can conclude that the user has some roles of the given set.
        return array_intersect($roles, $user_roles) != [];
    }



    /**
     * Check that that the user who is attached to the invoice, shouldn't have
     * any roles which are given. if the user at least has one of them, it
     * returns false, otherwise it returns true.
     *
     * @param array $roles Non-associative array of roles' slugs
     *
     * @return bool
     */
    private function userHasNotRoles(array $roles): bool
    {
        return !$this->userHasRoles($roles);
    }



    /**
     * Return a ready-to-use array of allowed roles
     *
     * @return array
     */
    private function getAllowedRoles(): array
    {
        return explode_not_empty(',', $this->para['roles_allowed']);
    }



    /**
     * Return a ready-to-use array of not-allowed roles
     *
     * @return array
     */
    private function getNotAllowedRoles(): array
    {
        return explode_not_empty(',', $this->para['roles_not_allowed']);
    }



    /**
     * Return user's roles of the user who attached to the invoice
     *
     * @return array
     */
    private function getUserRoles()
    {
        return $this->invoice->getUser()->roles->pluck('slug')->toArray();
    }
}
