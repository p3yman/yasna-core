<?php

namespace Modules\Shipping\ShippingRules;

use Carbon\Carbon;
use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;

/**
 * Class WeekDaysRule set constraint on shipping methods to using or not using
 * them on a certain days of week. It has a white list which determines that
 * the shipping method is available for using and a black list which act in an
 * inverse manner.
 *
 * @package Modules\Shipping\ShippingRules
 */
class WeekDaysRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("shipping::rules.weekdays");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("weekdays_allowed")
             ->label("tr:shipping::rules.allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("slug")
             ->captionField("title")
        ;

        $form->add("selectize")
             ->name("weekdays_not_allowed")
             ->label("tr:shipping::rules.not_allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("slug")
             ->captionField("title")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("shipping::rules.weekdays-hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->isInWhiteListedDays() and !$this->isInBlackListedDays();
    }



    /**
     * validate allowed days
     *
     * @return bool
     */
    private function isInWhiteListedDays(): bool
    {
        $array = explode_not_empty(',', $this->para['weekdays_allowed']);

        return in_array($this->requestedDay(), $array);
    }



    /**
     * validate un-allowed days
     *
     * @return bool
     */
    private function isInBlackListedDays(): bool
    {
        $array = explode_not_empty(',', $this->para['weekdays_not_allowed']);

        return in_array($this->requestedDay(), $array);
    }



    /**
     * get string representation of day of the week
     *
     * @return string
     */
    private function requestedDay()
    {
        $map = [
             Carbon::SATURDAY  => "sat",
             Carbon::SUNDAY    => "sun",
             Carbon::MONDAY    => "mon",
             Carbon::TUESDAY   => "tue",
             Carbon::WEDNESDAY => "wed",
             Carbon::THURSDAY  => "thu",
             Carbon::FRIDAY    => "fri",
        ];

        return $map[$this->invoice->getEffectiveDate()->dayOfWeek];
    }



    /**
     * get selectize options
     *
     * @return array
     */
    protected static function getSelectizeOptions()
    {
        $array = [];

        foreach (trans("yasna::weekdays") as $key => $value) {
            $array[] = [
                 "slug"  => $key,
                 "title" => $value,
            ];

        }

        return $array;
    }
}
