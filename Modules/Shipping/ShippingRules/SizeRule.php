<?php

namespace Modules\Shipping\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\Services\ShipmentVolume;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;


/**
 * Class SizeRule set a constraints based on total volume of the invoice. If
 * the total volume is higher than the shipping method's capacity, this rule
 * determine that the method is not suitable.
 *
 * @package Modules\Shipping\ShippingRules
 */
class SizeRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("shipping::rules.size");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("combo")
             ->name("allowed_size")
             ->label("tr:shipping::rules.allowed_size")
             ->inForm()
             ->options(ShipmentVolume::combo())
             ->valueField("id")
             ->captionField("caption")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("shipping::rules.size-hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->invoice->getTotalVolume() <= $this->para['allowed_size'];
    }
}
