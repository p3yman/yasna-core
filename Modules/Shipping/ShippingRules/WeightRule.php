<?php

namespace Modules\Shipping\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;

/**
 * Class WeightRule set a constraint on total weight of an invoice. If
 * the total weight of invoice is higher than the shipping method's capacity,
 * this rule determine that the requested shipping method is not suitable.
 *
 * @package Modules\Shipping\ShippingRules
 */
class WeightRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("shipping::rules.weight");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("input")
             ->name("allowed_weight")
             ->label("tr:shipping::rules.allowed_weight")
             ->inForm()
             ->type("number")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("shipping::rules.weight_hint"))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->invoice->getTotalWeight() <= $this->para['allowed_weight'];
    }
}
