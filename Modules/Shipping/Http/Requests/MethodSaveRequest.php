<?php

namespace Modules\Shipping\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class MethodSaveRequest extends YasnaRequest
{
    const SERIALIZED_FIELD_NAME = "_shipping_method";

    /**
     * @inheritdoc
     */
    protected $model_name = "shipping-method";



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'titles',
             'icon',
             'slug',
        ];
    }



    /**
     * get main validation rules
     *
     * @return array
     */
    public function mainRules()
    {
        $id = $this->getData('id');

        return [
             "titles" => "array|min:1",
             "slug"   => "required|unique:shipping_methods,slug,$id",
        ];
    }



    /**
     * get the rules relative to the serializing system
     *
     * @return array
     */
    public function serializingRules()
    {
        if (!$this->serialized) {
            return [
                 static::SERIALIZED_FIELD_NAME => "accepted",
            ];
        }

        return [];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             static::SERIALIZED_FIELD_NAME . ".accepted" => trans("shipping::methods.serializing_error"),
             "titles.min"                                => trans("shipping::methods.titles_error"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->filterTitleArray();
        $this->loadSerializedModel();
        $this->generateMetaContent();
    }



    /**
     * filter titles array to eliminate empty entries
     */
    protected function filterTitleArray()
    {
        $this->data['titles'] = array_filter($this->data['titles']);
    }



    /**
     * load serialized model included in the request
     */
    protected function loadSerializedModel()
    {
        try {
            $serialized       = model("shipping-method")::unserialize($this->data[static::SERIALIZED_FIELD_NAME]);
            $this->serialized = $serialized;
        } catch (\Exception $e) {
            $this->serialized = false;
        }
    }



    /**
     * generate the `rules` meta out of the serialized model
     */
    protected function generateMetaContent()
    {
        $this->data['rules'] = $this->serialized->getRules();
    }

}
