<?php

namespace Modules\Shipping\Http\Requests;

use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;
use Modules\Yasna\Services\YasnaRequest;

class RuleSaveRequest extends YasnaRequest
{
    /**
     * keep the form object
     *
     * @var Form
     */
    protected $form;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return $this->form->getValidationRules();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return $this->form->getPurificationRules();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return $this->form->getAttributeLabels();
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($hashid = false)
    {
        $serialized = $this->data['_shipping_method'];

        $this->model = model('shipping-method')::unserialize($serialized);
        $this->setFormObject($this->data['_class']);
    }



    /**
     * dynamically load and set form object
     *
     * @param ShippingRule $class
     */
    public function setFormObject($class)
    {
        $this->form = $class::getFormInstance();
    }
}
