<?php

namespace Modules\Shipping\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RuleReorderRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($hashid = false)
    {
        $serialized = $this->getData('rulebook');

        $this->model = model('shipping-method')::unserialize($serialized);
    }

}
