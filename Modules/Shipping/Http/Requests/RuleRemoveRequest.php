<?php

namespace Modules\Shipping\Http\Requests;

class RuleRemoveRequest extends RuleSaveRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [];
    }
}
