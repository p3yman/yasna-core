<?php

namespace Modules\Shipping\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class MethodActivenessRequest extends YasnaRequest
{
    protected $model_name = "shipping-method";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "_submit" => "in:delete,undelete",
        ];
    }
}
