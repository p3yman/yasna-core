<?php

namespace Modules\Shipping\Http\Controllers;

use Modules\Shipping\Entities\ShippingMethod;
use Modules\Shipping\Http\Requests\MethodSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class MethodsEditController extends YasnaController
{
    protected $base_model  = "shipping-method";
    protected $view_folder = "shipping::methods.editor";



    /**
     * MethodsEditController constructor.
     */
    public function __construct()
    {
        $this->registerPageAssets();
    }



    /**
     * get the editor form to create a new model
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm()
    {
        $model = $this->model();

        return $this->form($model);
    }



    /**
     * get the editor form to edit the requested model
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editForm(string $hashid)
    {
        $model = $this->model($hashid, true);

        if (!$model->exists) {
            return $this->abort(410);
        }

        return $this->form($model);
    }



    /**
     * save the form
     *
     * @param MethodSaveRequest $request
     *
     * @return string
     */
    public function save(MethodSaveRequest $request): string
    {
        $saved_model = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             "success_callback" => "$('#divSaveWarning').slideUp()",
             'success_redirect' => $request->model->exists ? '' : shipping()->getEditMethodLink($saved_model->hashid),
        ]);
    }



    /**
     * refresh the rules list section of the editor page by the serialized data passed via the POST request
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View
     */
    public function refreshRules(SimpleYasnaRequest $request)
    {
        $model = model("shipping-method")::unserialize($request->rulebook);

        return $this->view("rules-current", compact("model"));
    }



    /**
     * get the form blade
     *
     * @param ShippingMethod $model
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function form(ShippingMethod $model)
    {
        return $this->view("index", [
             "model"           => $model,
             "available_rules" => $this->getAvailableRulesArray(),
             "page"            => $this->getPageInfo($model),
        ]);
    }



    /**
     * get the array of available rules
     *
     * @return array
     */
    protected function getAvailableRulesArray(): array
    {
        return shipping()->getRegisteredRules();
    }



    /**
     * get the page info
     *
     * @param ShippingMethod $model
     *
     * @return array
     */
    protected function getPageInfo(ShippingMethod $model)
    {
        $page = (new MethodsBrowseController())->getPageInfo();

        if ($model->exists) {
            $page[] = [
                 "edit/$model->hashid",
                 $model->title?? trans("shipping::methods.edit"),
            ];
        } else {
            $page[] = [
                 "create",
                 trans("shipping::methods.create"),
            ];
        }

        return $page;
    }



    /**
     * register page assets
     */
    protected function registerPageAssets()
    {
        module('manage')
             ->service('template_bottom_assets')
             ->add('sortable-js')
             ->link("shipping:libs/Sortable.min.js")
             ->order(41)
        ;

        module('manage')
             ->service('template_bottom_assets')
             ->add('shipping-js')
             ->link("shipping:js/shipping.min.js")//@TODO: Copied from Coupons module!
             ->order(42)
        ;
    }
}
