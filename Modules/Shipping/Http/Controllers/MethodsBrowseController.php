<?php

namespace Modules\Shipping\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class MethodsBrowseController extends YasnaController
{
    protected $base_model  = "shipping-method";
    protected $view_folder = "shipping::methods.browse";



    /**
     * get the blade to the grid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->view("index", $this->getArguments());
    }



    /**
     * refresh grid row
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshGridRow($hashid)
    {
        $model = $this->model($hashid, true);

        if (!$model->exists) {
            return $this->abort(410);
        }

        return $this->view("row", compact("model"));
    }



    /**
     * get the arguments to be passed to the grid page
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             "page"   => $this->getPageInfo(),
             "models" => $this->getModels(),
        ];
    }



    /**
     * get the page info
     *
     * @return array
     */
    public function getPageInfo()
    {
        return [
             [
                  "shipping-methods",
                  trans("shipping::methods.title"),
             ],
        ];
    }



    /**
     * get the models
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function getModels()
    {
        return $this->model()->withTrashed()->orderBy("deleted_at")->orderBy('id','desc')->get();
    }
}
