<?php

namespace Modules\Shipping\Http\Controllers;

use Modules\Shipping\Http\Requests\MethodActivenessRequest;
use Modules\Yasna\Services\YasnaController;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

class MethodsActionController extends YasnaController
{
    protected $base_model  = "shipping-method";
    protected $view_folder = "shipping::methods.actions";



    /**
     * serve the form to delete/undelete actions
     *
     * @param string $hashid
     *
     * @return View|\Illuminate\View\View|Response
     */
    public function activenessForm(string $hashid)
    {
        return $this->singleAction($hashid, "activity");
    }



    /**
     * perform delete/undelete action
     *
     * @param MethodActivenessRequest $request
     *
     * @return string
     */
    public function activenessSave(MethodActivenessRequest $request): string
    {
        if ($request->_submit == "delete") {
            $done = $request->model->delete();
        } else {
            $done = $request->model->undelete();
        }

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback' => "divReload('divMethod-$request->hashid')",
        ]);
    }



    /**
     * serve the form of the hard delete action
     *
     * @param string $hashid
     *
     * @return View|\Illuminate\View\View|Response
     */
    public function hardDeleteForm(string $hashid)
    {
        return $this->singleAction($hashid, "destroy");
    }



    /**
     * destroy the model
     *
     * @param MethodActivenessRequest $request
     *
     * @return string
     */
    public function destroy(MethodActivenessRequest $request): string
    {
        $done = $request->model->hardDelete();

        return $this->jsonAjaxSaveFeedback( $done , [
        		'success_callback' => "$('#divMethod-$request->hashid').slideUp('fast')",
        ]);
    }

}
