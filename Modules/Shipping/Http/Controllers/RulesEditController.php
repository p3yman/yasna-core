<?php

namespace Modules\Shipping\Http\Controllers;

use App\Models\ShippingMethod;
use Modules\Shipping\Http\Requests\RuleRemoveRequest;
use Modules\Shipping\Http\Requests\RuleReorderRequest;
use Modules\Shipping\Http\Requests\RuleSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RulesEditController
 * The purpose of this class to control the create/edit/delete actions of a single rule.
 * These actions are called from the editor page.
 */
class RulesEditController extends YasnaController
{
    /**
     * @inheritdoc
     */
    protected $view_folder = "shipping::methods.editor";



    /**
     * prepare add rule form
     *
     * @param string $class
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function createForm($class)
    {
        if (!class_exists($class)) {
            return $this->abort(410);
        }

        $arguments = [
             "class"      => $class,
             "identifier" => 0,
             "values"     => [],
        ];

        return $this->view("rules-editor", $arguments);
    }



    /**
     * prepare edit rule form
     *
     * @param string $identifier
     * @param string $rule
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function editForm($identifier, $rule)
    {
        $values = json_decode($rule, true);
        $class  = $values['_class'];

        return $this->view("rules-editor", compact('values', 'class', 'identifier'));
    }



    /**
     * prepare delete rule form
     *
     * @param string $identifier
     * @param string $rule
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function removeForm($identifier, $rule)
    {
        $values = json_decode($rule, true);
        $class  = $values['_class'];

        return $this->view("rules-delete", compact('values', 'class', 'identifier'));
    }



    /**
     * save rule to the serialized instance of the object
     *
     * @param RuleSaveRequest $request
     *
     * @return string
     */
    public function save(RuleSaveRequest $request)
    {
        $model   = $request->model;
        $savable = $request->getMainFieldsArray();

        if ($request->_identifier) {
            $model->updateRule($request->_identifier, $savable);
        } else {
            $model->addRule($request->_class, $savable);
        }

        return $this->jsonAjaxSaveFeedback(true, [
             'success_callback' => $this->getSuccessCallback($model->serialize()),
        ]);
    }



    /**
     * delete rule from the serialized instance of the object
     *
     * @param RuleRemoveRequest $request
     *
     * @return string
     */
    public function remove(RuleRemoveRequest $request)
    {
        $model = $request->model;

        $model->deleteRule($request->_identifier);

        return $this->jsonAjaxSaveFeedback(true, [
             'success_callback' => $this->getSuccessCallback($model->serialize()),
        ]);
    }



    /**
     * save rules order
     *
     * @param RuleReorderRequest $request
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function saveOrder(RuleReorderRequest $request)
    {
        /** @var ShippingMethod $model */
        $model  = $request->model;
        $orders = explode_not_empty(",", $request->order);

        foreach ($orders as $order => $identifier) {
            $model->reorderRule($identifier, $order);
        }

        return $this->view("rules-current-with-refresh", compact("model"));
    }



    /**
     * get the js callback command
     *
     * @param string $serialized
     *
     * @return string
     */
    protected function getSuccessCallback(string $serialized): string
    {
        $commands = [
             "updateSerializedModel('$serialized')",
             "$('.form-feed').hide()",
             "$('#divSaveWarning').slideDown()",
        ];

        return implode(";", $commands);
    }

}
