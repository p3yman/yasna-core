<?php

Route::group([
    'middleware' => 'web' , "is:" . config("shipping.access"),
    'namespace'  => module('Shipping')->getControllersNamespace(), 
    'prefix'     => 'manage/shipping-methods'
], function () {
    Route::get('/' , 'MethodsBrowseController@index');
    Route::get('/refresh/{hashid}' , 'MethodsBrowseController@refreshGridRow')->name("shipping-method-row-refresh");

    Route::get('/create' , 'MethodsEditController@createForm')->name("shipping-method-create");
    Route::get('/edit/{hashid}' , 'MethodsEditController@editForm')->name("shipping-method-edit");

    Route::post('/save' , 'MethodsEditController@save')->name("shipping-method-save");
    Route::post('/refresh-rule/', 'MethodsEditController@refreshRules')->name("shipping-method-refresh-rules");

    Route::get('/activeness/{hashid}' , 'MethodsActionController@activenessForm')->name("shipping-method-activeness");
    Route::get('/hard-delete/{hashid}' , 'MethodsActionController@hardDeleteForm')->name("shipping-method-hard-delete");
    Route::post('/save/activeness' , 'MethodsActionController@activenessSave')->name("shipping-method-save-activeness");
    Route::post('/save/destroy' , 'MethodsActionController@destroy')->name("shipping-method-destroy");
});
