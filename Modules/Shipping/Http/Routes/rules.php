<?php

Route::group([
     'middleware' => 'web' , "is:" . config("shipping.access"),
     'namespace'  => module('Shipping')->getControllersNamespace(),
     'prefix'     => 'manage/shipping-methods/rules'
], function () {
    Route::get('/add/{class}', 'RulesEditController@createForm')->name("shipping-rule-add");
    Route::get('/edit/{identifier}/{rule}', 'RulesEditController@editForm')->name("shipping-rule-edit");
    Route::get('/remove/{identifier}/{rule}', 'RulesEditController@removeForm')->name("shipping-rule-remove");
    Route::post('/delete/', 'RulesEditController@remove')->name("shipping-rule-remove-action");
    Route::post('/save/', 'RulesEditController@save')->name("shipping-rule-save");
    Route::post('/save-order/', 'RulesEditController@saveOrder')->name("shipping-rule-save-order");
});
