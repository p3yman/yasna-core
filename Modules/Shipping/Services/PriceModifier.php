<?php

namespace Modules\Shipping\Services;

class PriceModifier
{
    const ABSOLUTE       = "absolute";
    const ADD_AMOUNT     = "add_amount";
    const ADD_PERCENT    = "add_percent";
    const DEDUCT_AMOUNT  = "deduct_amount";
    const DEDUCT_PERCENT = "deduct_percent";



    /**
     * get modified price
     *
     * @param float  $old_price
     * @param float  $new_amount
     * @param string $modifier
     *
     * @return float
     */
    public static function apply($old_price, $new_amount, string $modifier): float
    {
        $method = camel_case("get-$modifier-price");

        if (!method_exists(self::class, $method)) {
            return 0;
        }

        return (float)static::$method($old_price, $new_amount);
    }



    /**
     * get new price applied to the absolute rule
     *
     * @param float $old_price
     * @param float $new_amount
     *
     * @return float
     */
    protected static function getAbsolutePrice($old_price, $new_amount)
    {
        return $new_amount;
    }



    /**
     * get new price applied to the "add-amount" rule
     *
     * @param float $old_price
     * @param float $new_amount
     *
     * @return float
     */
    protected static function getAddAmountPrice($old_price, $new_amount)
    {
        return $old_price + $new_amount;
    }



    /**
     * get new price applied to the "add-percent" rule
     *
     * @param float $old_price
     * @param float $new_amount
     *
     * @return float
     */
    protected static function getAddPercentPrice($old_price, $new_amount)
    {
        return $old_price + ($old_price * $new_amount / 100);
    }



    /**
     * get new price applied to the "deduct-amount" rule
     *
     * @param float $old_price
     * @param float $new_amount
     *
     * @return float
     */
    protected static function getDeductAmountPrice($old_price, $new_amount)
    {
        return $old_price - $new_amount;
    }



    /**
     * get new price applied to the "deduct-percent" rule
     *
     * @param float $old_price
     * @param float $new_amount
     *
     * @return float
     */
    protected static function getDeductPercentPrice($old_price, $new_amount)
    {
        return $old_price - ($old_price * $new_amount / 100);
    }
}
