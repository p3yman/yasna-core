<?php

namespace Modules\Shipping\Services;


class ShippingHelper
{
    const RULES_SERVICE_NAME = "rules";

    use ShippingHelperLinksTrait;
    use ShippingHelperRulesTrait;

    /**
     * keep the order of registration, useful for rules and groups
     *
     * @var int
     */
    private static $order;
}
