<?php

namespace Modules\Shipping\Services;


interface Shippable
{
    /**
     * convert the shippable entity into a ShippingInvoiceObject
     *
     * @link https://github.com/mhrezaei/yasna-core/wiki/F.-Shipping#invoice-object
     * @return ShippingInvoiceObject
     */
    public function toShippingInvoice(): ShippingInvoiceObject;



    /**
     * get an array of the applicable shipping methods with prices inserted in the $method->shipping_price property
     *
     * @return array
     */
    public function availableShippingMethods(): array ;
}
