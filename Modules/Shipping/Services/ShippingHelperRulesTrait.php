<?php

namespace Modules\Shipping\Services;

trait ShippingHelperRulesTrait
{
    /**
     * register a new rule in the service environment
     *
     * @param string $class
     */
    public function registerRule(string $class)
    {
        $caption = $class::getTitle();

        module("shipping")
             ->service(static::RULES_SERVICE_NAME)
             ->add($class)
             ->class($class)
             ->caption($caption)
             ->order(static::$order++)
        ;
    }



    /**
     * get an array of registered rules
     *
     * @return array
     */
    public function getRegisteredRules()
    {
        return module("shipping")->service(static::RULES_SERVICE_NAME)->paired('caption', 'class');
    }
}
