<?php

namespace Modules\Shipping\Services;

use App\Models\User;
use Carbon\Carbon;
use Modules\Yasna\Services\GeneralTraits\Serializable;

/**
 * Class InvoiceObject
 * @method int getTotalVolume()
 * @method int getTotalWeight()
 * @method float getTotalAmount()
 * @method Carbon getEffectiveDate()
 * @method User getUser()
 * @method array getItems()
 * @method getOriginalInstance()
 */
class ShippingInvoiceObject
{
    use Serializable;

    /**
     * keep the key-value array of location parameters
     *
     * @var array
     */
    protected $location = [];

    /**
     * keep the total volume index of the shipment (according to ShipmentVolume class)
     *
     * @var int
     */
    protected $total_volume;

    /**
     * keep the total weight of the shipment in grams
     *
     * @var int
     */
    protected $total_weight;


    /**
     * keep the invoice total amount
     *
     * @var float
     */
    protected $total_amount;

    /**
     * keep the invoice effective date
     *
     * @var Carbon
     */
    protected $effective_date;

    /**
     * keep the invoice beneficiary
     *
     * @var User
     */
    protected $user;

    /**
     * keep the original instance of the invoice object
     *
     * @var mixed
     */
    protected $original_instance;

    /**
     * keep the array of invoiced items
     * the content would be something like this:
     * [
     *     'cart-12' => [
     *         "weight"  => 1000,
     *         "volume"  => 5,
     *         "object"  => $folan,
     *     ]
     * ]
     *
     * @var array
     */
    protected $items = [];

    /**
     * keep the details applied by each of the rules
     * the content would be something like this:
     * [
     *      'folanRule' => [
     *          "is_suitable" => true,
     *          "amount"      => 500,
     *          "modifier"    => PriceModifier::ABSOLUTE
     *      ],
     *      'fooRule' => [
     *          "is_suitable" => false,
     *      ],
     *      'barRule' => [
     *          "is_suitable" => true,
     *          "amount"      => 1000,
     *          "modifier"    => PriceModifier::DEDUCT_AMOUNT
     *      ],
     *      'thirdRule' => [
     *          "is_suitable" => true,
     *          "amount       => 1250,
     *          "modifier"    => PriceModifier::ADD_AMOUNT
     *      ],
     *      'fourthRule' => [
     *          "is_suitable" => true,
     *      ],
     * ]
     *
     * @var array
     */
    protected $applied_rules = [];

    /**
     * keep the result of the applied rules.
     * the content would be something like this:
     * [
     *     "is_suitable" => false,
     *     "amount"      => 1232.23,
     *     "modifier"    => PriceModifier::ADD_AMOUNT
     * ]
     *
     * @var array
     */
    protected $applied_result = null;



    /**
     * ShippingInvoiceObject constructor.
     *
     * @param int $total_volume
     * @param int $total_weight
     *
     * @return void
     */
    public function __construct(int $total_volume = 0, int $total_weight = 0)
    {
        $this->setTotalVolume($total_volume);
        $this->setTotalWeight($total_weight);
        $this->setUser(0);
        $this->effective_date = now();
    }



    /**
     * make a magic method to response getters
     *
     * @param string $method_name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($method_name, $arguments)
    {
        if (str_contains($method_name, "get")) {
            return $this->generalGetter($method_name);
        }

        return null;
    }



    /**
     * make a general getter method
     *
     * @param string $method_name
     *
     * @return mixed
     */
    protected function generalGetter($method_name)
    {
        $argument = snake_case(str_after($method_name, 'get'));
        if (isset($this->$argument)) {
            return $this->$argument;
        }

        return null;
    }



    /**
     * set $this->total_volume, according to the ShipmentVolume standards.
     *
     * @param int $volume_index
     *
     * @return void
     */
    public function setTotalVolume(int $volume_index)
    {
        $volume_index = (int)$volume_index;

        $volume_index = max($volume_index, ShipmentVolume::UNKNOWN);
        $volume_index = min($volume_index, ShipmentVolume::maxAllowed());

        $this->total_volume = $volume_index;
    }



    /**
     * set $this->total_weight in grams
     *
     * @param int $grams
     *
     * @return void
     */
    public function setTotalWeight(int $grams)
    {
        $this->total_weight = $grams;
    }



    /**
     * set the key/value pair in the $this->location array
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return void
     */
    public function setLocation(string $key, $value)
    {
        $this->location[$key] = $value;
    }



    /**
     * set $this->total_amount, casting it to the float data type
     *
     * @param float|int $amount
     *
     * @return void
     */
    public function setTotalAmount($amount)
    {
        $this->total_amount = (float)$amount;
    }



    /**
     * set $this->effective_date
     *
     * @param string|Carbon $date
     *
     * @return void
     */
    public function setEffectiveDate($date)
    {
        if ($date instanceof Carbon) {
            $this->effective_date = $date;
            return;
        }

        if (is_string($date)) {
            $this->effective_date = new Carbon($date);
            return;
        }

        $this->effective_date = now();
    }



    /**
     * set $this->user
     *
     * @param $user
     *
     * @return void
     */
    public function setUser($user)
    {
        if ($user instanceof User) {
            $this->user = $user;
            return;
        }

        $this->user = model("user", $user, true);
    }



    /**
     * set $this->original_instance
     *
     * @param mixed $instance
     *
     * @return void
     */
    public function setOriginalInstance($instance)
    {
        $this->original_instance = $instance;
    }



    /**
     * add an item by setting an array of optional key/value pairs of data
     *
     * @param array      $payload
     * @param string|int $identifier
     *
     * @return void
     */
    public function addItem(array $payload, $identifier = null)
    {
        if ($identifier) {
            $this->items[$identifier] = $payload;
        } else {
            $this->items[] = $payload;
        }
    }



    /**
     * add rule identifier to the list of applied rules
     *
     * @param string $rule_identifier
     *
     * @return void
     */
    public function addAppliedRule($rule_identifier)
    {
        $this->applied_rules[$rule_identifier] = [];
    }



    /**
     * apply a rule, marking the $is_suitable flag of the corresponding $this->applied to true or false
     *
     * @param string $rule_identifier
     * @param bool   $suitability
     *
     * @return void
     */
    public function applyRuleSuitability($rule_identifier, bool $suitability)
    {
        $this->applied_rules[$rule_identifier]['is_suitable'] = $suitability;
    }



    /**
     * apply rule price and set $this->applied_rules array accordingly
     *
     * @param string $rule_identifier
     * @param float  $amount
     * @param string $modifier (valid options are constant values in the PriceModifier class.)
     *
     * @return void
     */
    public function applyRulePrice($rule_identifier, $amount, string $modifier = PriceModifier::ABSOLUTE)
    {
        $this->applied_rules[$rule_identifier]['amount']   = $amount;
        $this->applied_rules[$rule_identifier]['modifier'] = $modifier;
    }



    /**
     * get an array of applied rule identifiers
     *
     * @return array
     */
    public function getAppliedRules()
    {
        return array_keys($this->applied_rules);
    }



    /**
     * get a raw detailed version of the applied rules
     *
     * @return array
     */
    public function getAppliedDetails()
    {
        return $this->applied_rules;
    }



    /**
     * get applied result from the content stored in the $this->applied_details + set $this->applied_result for later.
     *
     * @param bool $recalculate
     *
     * @return array
     */
    public function getAppliedResult($recalculate = false)
    {
        if (!is_null($this->applied_result) and !$recalculate) {
            return $this->applied_result;
        }

        $price       = 0;
        $is_suitable = true;

        foreach ($this->applied_rules as $rule) {
            if (isset($rule['is_suitable'])) {
                $is_suitable = ($is_suitable and (bool)$rule['is_suitable']);
            }

            if (!$is_suitable) {
                $price = 0;
                break;
            }

            $price = PriceModifier::apply($price, $rule['amount'], $rule['modifier']);
        }

        return $this->applied_result = [
             "is_suitable" => $is_suitable,
             "price"       => max($price, 0),
        ];
    }



    /**
     * get the total price according to the data in $this->applied_rules
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->getAppliedResult()['price'];
    }



    /**
     * @param string $key
     *
     * @return array|mixed
     */
    public function getLocation($key = null)
    {
        if (!$key) {
            return $this->location;
        }

        if (isset($this->location[$key])) {
            return $this->location[$key];
        }

        return null;
    }



    /**
     * get the is_suitable property according to the data in $this->applied_rules
     *
     * @return bool
     */
    public function isSuitable()
    {
        return (bool)$this->getAppliedResult()['is_suitable'];
    }



    /**
     * get the contrary of the $this->isSuitable()
     *
     * @return bool
     */
    public function isNotSuitable()
    {
        return !$this->isSuitable();
    }



    /**
     * convert the object to array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
             "total_weight"    => $this->total_weight,
             "total_volume"    => $this->total_volume,
             "location"        => $this->location,
             "user"            => $this->user->full_name,
             "effective_date"  => $this->effective_date,
             "items"           => $this->items,
             "total_amount"    => $this->total_amount,
             "applied_details" => $this->getAppliedDetails(),
             "applied_rules"   => $this->getAppliedRules(),
             "applied_result"  => $this->getAppliedResult(),
             "price"           => $this->getPrice(),
             "is_suitable"     => $this->isSuitable(),
        ];
    }



    /**
     * set total amount from the added items
     *
     * @return void
     */
    public function setTotalAmountFromAddedItems()
    {
        $amount = 0;

        foreach ($this->items as $item) {
            if (isset($item['amount'])) {
                $amount += (int)$item['amount'];
            }
        }

        $this->setTotalAmount($amount);
    }



    /**
     * set the total weight from the added items
     *
     * @return void
     */
    public function setTotalWeightFromAddedItems()
    {
        $weight = 0;

        foreach ($this->items as $item) {
            if (isset($item['weight'])) {
                $weight += (int)$item['weight'];
            }
        }

        $this->setTotalWeight($weight);
    }



    /**
     * set the total volume from the added items by selecting the biggest one
     *
     * @return void
     */
    public function setTotalVolumeFromAddedItems()
    {
        $biggest = 0;

        foreach ($this->items as $item) {
            if (isset($item['volume'])) {
                $biggest = max($biggest, (int)$item['volume']);
            }
        }

        $this->setTotalVolume($biggest);
    }



    /**
     * set all possible total things from the added items
     *
     * @return void
     */
    public function setTotalsFromAddedItems()
    {
        $this->setTotalWeightFromAddedItems();
        $this->setTotalVolumeFromAddedItems();
        $this->setTotalAmountFromAddedItems();
    }
}
