<?php

namespace Modules\Shipping\Services;

/**
 * Class ShipmentVolume
 * Provide index reference for the standard capacity sizes, with a bunch of helpful methods to use them in action.
 */
class ShipmentVolume
{
    const UNKNOWN      = 0;
    const POCKET_SIZE  = 1;
    const BIKE_SIZE    = 2;
    const CAR_SIZE     = 3;
    const PICKUP_SIZE  = 4;
    const TRUCK_SIZE   = 5;
    const TRAILER_SIZE = 6;
    const SHIP_SIZE    = 7;

    protected static $min = 1;
    protected static $max = 7;



    /**
     * get the minimum allowed capacity index
     *
     * @return int
     */
    public static function minAllowed()
    {
        return static::$min;
    }



    /**
     * get the maximum allowed capacity index
     *
     * @return int
     */
    public static function maxAllowed()
    {
        return static::$max;
    }



    /**
     * get the caption of the capacity index
     *
     * @param int    $index
     * @param string $locale
     *
     * @return string
     */
    public static function caption(int $index, $locale = null)
    {
        if (!$locale) {
            $locale = getLocale();
        }

        return trans("shipping::capacities.$index", [], $locale);
    }



    /**
     * get an array of available options, suitable to be used in the user interface
     *
     * @param bool   $including_zero
     * @param string $locale
     *
     * @return array
     */
    public static function options($including_zero = false, $locale = null)
    {
        $result = [];

        $min = static::$min;
        if ($including_zero) {
            $min = 0;
        }

        for ($i = $min; $i <= static::$max; $i++) {
            $result[$i] = static::caption($i, $locale);
        }

        return $result;
    }



    /**
     * get an array of available options, suitable to be used in a combo widget
     *
     * @param bool   $including_zero
     * @param string $locale
     *
     * @return array
     */
    public static function combo($including_zero = false, $locale = null)
    {
        $options = static::options($including_zero, $locale);
        $result  = [];

        foreach ($options as $key => $caption) {
            $result[] = [
                 'id'      => $key,
                 'caption' => $caption,
            ];
        }

        return $result;
    }
}
