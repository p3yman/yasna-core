<?php

namespace Modules\Shipping\Services;

trait ShippingHelperLinksTrait
{
    /**
     * get row-refresh link on the methods grid
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getRefreshMethodLink(string $hashid)
    {
        return $this->route("method-row-refresh", false, [
             "hashid" => $hashid,
        ]);
    }



    /**
     * get create link for the methods
     *
     * @return string
     */
    public function getCreateMethodLink()
    {
        return $this->route("method-create");
    }



    /**
     * get edit link for the methods
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getEditMethodLink(string $hashid)
    {
        return $this->route("method-edit", false, [
             "hashid" => $hashid,
        ]);
    }



    /**
     * get link to delete/undelete actions of a shipping method
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getActivenessMethodLink(string $hashid)
    {
        return $this->route("method-activeness", true, [
             "hashid" => $hashid,
        ]);
    }



    /**
     * get link to the hard-delete action of a shipping method
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getHardDeleteMethodLink(string $hashid)
    {
        return $this->route("method-hard-delete", true, [
             "hashid" => $hashid,
        ]);
    }



    /**
     * get the link for the add rule form (normally called from method editor)
     *
     * @param array $rule
     *
     * @return string
     */
    public function getAddRuleLink($rule)
    {
        return $this->route("rule-add", true, [
             "rule" => $rule, // urlencode(json_encode($rule)), //TODO: Should be double-checked!
        ]);
    }



    /**
     * get the link for the edit rule form (normally called from method editor)
     *
     * @param array  $rule
     * @param string $identifier
     *
     * @return string
     */
    public function getEditRuleLink($rule, $identifier)
    {
        return $this->route("rule-edit", true, [
             "rule"       => urlencode(json_encode($rule)),
             "identifier" => $identifier,
        ]);
    }



    /**
     * get the link for the removing rule form (normally called from method editor)
     *
     * @param array  $rule
     * @param string $identifier
     *
     * @return string
     */
    public function getRemoveRuleLink($rule, $identifier)
    {
        return $this->route("rule-remove", true, [
             "rule"       => urlencode(json_encode($rule)),
             "identifier" => $identifier,
        ]);
    }



    /**
     * get the route address
     *
     * @param string $name_suffix
     * @param bool   $in_modal
     * @param array  $array
     *
     * @return string
     */
    private function route(string $name_suffix, bool $in_modal = false, array $array = []): string
    {
        $route = route("shipping-$name_suffix", $array);

        if ($in_modal) {
            $route = "masterModal('$route')";
        }

        return $route;
    }
}
