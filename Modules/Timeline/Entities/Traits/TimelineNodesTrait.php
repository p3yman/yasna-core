<?php

namespace Modules\Timeline\Entities\Traits;

use App\Models\TimelineNode;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Timeline\Services\NodeAbstract;

/**
 * A trait to be attached to the models, using timeline features.
 * @method HasMany hasMany($class, $foreign_key)
 */
trait TimelineNodesTrait
{
    /**
     * get the relationship instance of the order nodes.
     *
     * @return HasMany
     */
    public function nodes()
    {
        return $this->hasMany(TimelineNode::class, "model_id")->where("model_name", $this->getClassName());
    }



    /**
     * get Timeline resource.
     *
     * @return array
     */
    protected function getTimelineResource()
    {
        $array = [];

        foreach ($this->nodes()->orderBy("created_at", "desc")->get() as $node) {
            $class = $node->class;
            /** @var NodeAbstract $instance */
            $instance = new $class($node);

            if ($instance->canView()) {
                $array[$node->hashid] = [
                     "id"         => $node->hashid,
                     "class"      => $node->class,
                     "title"      => $instance::title(),
                     "created_at" => $node->getResource("created_at"),
                     "created_by" => $node->getResource("created_by"),
                     "data"       => $instance->toArray(),
                ];
            }
        }

        return $array;
    }

}
