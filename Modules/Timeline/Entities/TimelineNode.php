<?php

namespace Modules\Timeline\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\TimelineNode as PublishedModel;

class TimelineNode extends YasnaModel
{
    use SoftDeletes;



    /**
     * insert a new node.
     *
     * @param array $data
     *
     * @return \App\Models\TimelineNode
     */
    public static function newRecord(array $data)
    {
        /** @var PublishedModel $return */
        $return = model("timeline-node")->batchSave($data);

        return $return;
    }



    /**
     * get the relationship instance to the related model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(MODELS_NAMESPACE . $this->model_name, "model_id");
    }



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "data",
        ];
    }

    /**
     * get data array from the meta.
     *
     * @return array
     */
    public function getDataArray()
    {
        return (array)$this->getMeta("data");
    }



    /**
     * get data value of the specific key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getData(string $key)
    {
        $data = $this->getDataArray();

        return isset($data[$key]) ? $data[$key] : null;
    }

}
