<?php

namespace Modules\Timeline\Providers;

use Modules\Timeline\Console\MakeNodeCommand;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class TimelineServiceProvider
 *
 * @package Modules\Timeline\Providers
 */
class TimelineServiceProvider extends YasnaProvider
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerArtisans();
    }



    /**
     * register Artisans
     *
     * @return void
     */
    private function registerArtisans()
    {
        $this->addArtisan(MakeNodeCommand::class);
    }

}
