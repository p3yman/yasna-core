<?php

namespace Modules\Timeline\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MakeNodeCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-node';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a timeline node in the chosen module.';



    /**
     * get the folder path.
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Http" . DIRECTORY_SEPARATOR . "TimelineNodes");
    }



    /**
     * get stub replacement.
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->class_name,
             "ALIAS"  => $this->module->getAlias(),
             "TITLE"  => snake_case(str_before($this->class_name, "Node")),
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Node')) {
            return $file_name . "Node";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "node.stub";
    }
}
