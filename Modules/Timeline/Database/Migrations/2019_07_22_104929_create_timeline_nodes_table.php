<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelineNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeline_nodes', function (Blueprint $table) {
            $table->increments('id');

            $table->string("model_name")->index();
            $table->unsignedInteger("model_id")->index();
            $table->string("class")->index();
            $table->string("type")->nullable()->index();

            $table->timestamp("confirmed_at")->nullable();
            $table->unsignedInteger("confirmed_by")->default(0);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeline_nodes');
    }
}
