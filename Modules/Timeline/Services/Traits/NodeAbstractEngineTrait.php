<?php

namespace Modules\Timeline\Services\Traits;

use Modules\Timeline\Events\NodeInserted;
use Modules\Timeline\Services\NodeAbstract;
use Modules\Yasna\Services\YasnaModel;
use App\Models\TimelineNode;

trait NodeAbstractEngineTrait
{
    /**
     * insert a new node attached to the model.
     *
     * @param YasnaModel  $model
     * @param array       $data
     * @param null|string $type
     *
     * @return NodeAbstract|null
     */
    public static function insert($model, array $data = [], ?string $type = null)
    {
        if (!static::canCreate($model)) {
            return null;
        }

        $array = [
             "model_name" => $model->getClassName(),
             "model_id"   => $model->id,
             "class"      => static::calledClass(),
             "type"       => $type,
             "data"       => array_normalize($data, static::defaults($model)),
        ];

        $node     = TimelineNode::newRecord($array);
        $instance = static::newStatic($node);
        $instance->raiseEvent();

        return $instance;
    }



    /**
     * indicate if the model already exists.
     *
     * @return bool
     */
    public function exists()
    {
        return $this->model->exists;
    }



    /**
     * get data from the node.
     *
     * @param string $attribute
     *
     * @return mixed
     */
    protected function getData(string $attribute)
    {
        return $this->node->getData($attribute);
    }



    /**
     * make a new instance out of the called class.
     *
     * @param TimelineNode $node
     *
     * @return NodeAbstract
     */
    private static function newStatic(TimelineNode $node)
    {
        return new static($node);
    }



    /**
     * raise an appropriate event for creation of this node.
     *
     * @return void
     */
    private function raiseEvent()
    {
        /** @var NodeAbstract $class */
        $class = $this;

        event(new NodeInserted($class));
    }
}
