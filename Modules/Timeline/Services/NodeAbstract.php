<?php

namespace Modules\Timeline\Services;

use App\Models\TimelineNode;
use Illuminate\Contracts\Support\Arrayable;
use Modules\Timeline\Services\Traits\NodeAbstractEngineTrait;
use Modules\Yasna\Services\GeneralTraits\ClassRecognitionsTrait;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class NodeAbstract
 */
abstract class NodeAbstract implements Arrayable
{
    use ClassRecognitionsTrait;
    use NodeAbstractEngineTrait;

    /** @var YasnaModel */
    public $model;

    /** @var TimelineNode */
    public $record;



    /**
     * NodeAbstract constructor.
     *
     * @param YasnaModel $node
     */
    public function __construct($node)
    {
        $this->record = $node;
        $this->model  = $node->parent;
    }



    /**
     * get the human-friendly title of the node.
     *
     * @return string
     */
    abstract public static function title();



    /**
     * indicate if the online user has enough permissions to create such a timeline node.
     *
     * @param YasnaModel $model
     *
     * @return bool
     */
    public static function canCreate($model)
    {
        return true; // override this to restrict users.
    }



    /**
     * get the custom fields and data, to be stored in the meta field of the model
     *
     * @param YasnaModel $model
     *
     * @return array
     */
    public static function defaults($model)
    {
        return [];
    }



    /**
     * indicate if the online user has enough permissions to view this timeline node.
     *
     * @return bool
     */
    public function canView()
    {
        return true; // override this to restrict users.
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        $array = [];

        foreach (static::defaults($this->model) as $key => $default) {
            $array[$key] = $this->getData($key);
        }

        return $array;
    }
}
