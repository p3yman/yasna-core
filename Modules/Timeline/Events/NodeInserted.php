<?php

namespace Modules\Timeline\Events;

use Modules\Timeline\Services\NodeAbstract;
use Modules\Yasna\Services\YasnaModel;

class NodeInserted
{
    /** @var NodeAbstract */
    public $node;

    /** @var YasnaModel */
    public $model;



    /**
     * NodeInserted constructor.
     *
     * @param NodeAbstract $node
     */
    public function __construct($node)
    {
        $this->node  = $node;
        $this->model = $node->model;

        chalk("node")->write("Raised event for " . $node::calledClassName());
    }
}
