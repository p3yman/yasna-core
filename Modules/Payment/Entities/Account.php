<?php

namespace Modules\Payment\Entities;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Modules\Payment\Entities\Traits\AccountElectorTrait;
use Modules\Payment\Entities\Traits\AccountFormTrait;
use Modules\Payment\Entities\Traits\AccountPermissionTrait;
use Modules\Payment\Entities\Traits\AccountResourceTrait;
use Modules\Payment\Entities\Traits\AccountScopeTrait;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class Account
 *
 * @property string $currency_text
 * @property string $currency_title
 * @property string $currency_trans
 * @property string $bank_name
 * @property bool   is_declared
 * @property string $simple_type
 * @property string $account_no
 * @property string $account_number
 * @property string $card_no
 * @property string $card_number
 * @property string $sheba_no
 * @property string $sheba_number
 * @property string $shaba_no
 * @property string $shaba_number
 * @property string $iban
 */
class Account extends YasnaModel
{
    use AccountFormTrait;
    use AccountResourceTrait;
    use AccountPermissionTrait;
    use AccountElectorTrait;
    use AccountScopeTrait;
    use SoftDeletes;

    protected $guarded = ['id'];
    protected $casts   = [
         'meta'         => "array",
         'published_at' => "date",
    ];

    protected $declarable_types = [
         'deposit',
         'shetab',
    ];

    protected static $saving_gate;
    protected static $saving_type;



    /**
     * Standard Many-to-Many Relationship with the transactions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Transaction');
    }



    /**
     * Returns a translated version of the currency.
     *
     * @return string
     */
    public function getCurrencyText()
    {
        return PaymentToolsServiceProvider::getCurrencyTitle($this->currency);
    }



    /**
     * Accessor for the `getCurrencyText()` Method.
     *
     * @return string
     */
    public function getCurrencyTextAttribute()
    {
        return $this->getCurrencyText();
    }



    /**
     * Accessor for the `getCurrencyText()` Method.
     *
     * @return string
     */
    public function getCurrencyTitleAttribute()
    {
        return $this->currency_text;
    }



    /**
     * Accessor for the `getCurrencyText()` Method.
     *
     * @return string
     */
    public function getCurrencyTransAttribute()
    {
        return $this->currency_text;
    }



    /**
     * Accessor for the `name` Attribute
     *
     * @return string|null
     */
    public function getBankNameAttribute()
    {
        return $this->name;
    }



    /**
     * Whether the transactions of this account may be marked as declared.
     *
     * @return bool
     */
    public function isDeclarable()
    {
        return in_array($this->type, $this->declarable_types);
    }



    /**
     * Accessor for the `isDeclarable()` Method
     *
     * @return bool
     */
    public function getIsDeclarableAttribute()
    {
        return $this->isDeclarable();
    }



    /**
     * Returns the simple type of this account.
     * <br>
     * The simple type will be:
     * - `online` if the account type is `online`
     * - `offline` if the account type is anything but `online`
     *
     * @return string|null
     */
    public function getSimpleType()
    {
        if (!$this->type) {
            return null;
        }

        return ($this->type == 'online') ? 'online' : 'offline';
    }



    /**
     * Accessor for the `getSimpleType()` Method
     *
     * @return string|null
     */
    public function getSimpleTypeAttribute()
    {
        return $this->getSimpleType();
    }



    /**
     * Returns the account number.
     *
     * @return string|null
     */
    public function getAccountNo()
    {
        return $this->getMeta('account_no');
    }



    /**
     * Accessor for the `getAccountNo()` Method
     *
     * @return string|null
     */
    public function getAccountNoAttribute()
    {
        return $this->getAccountNo();
    }



    /**
     * Accessor for the `getAccountNo()` Method
     *
     * @return string|null
     */
    public function getAccountNumberAttribute()
    {
        return $this->account_no;
    }



    /**
     * Returns the card number.
     * <br>
     * _If the account type is `shetab`, the account number will be assumed as the card number._
     *
     * @return string|null
     */
    public function getCardNo()
    {
        return $this->hasType('shetab') ? $this->getMeta('account_no') : null;
    }



    /**
     * Accessor for the `getCardNo()` Method
     *
     * @return string|null
     */
    public function getCardNoAttribute()
    {
        return $this->getCardNo();
    }



    /**
     * Accessor for the `getCardNo()` Method
     *
     * @return string|null
     */
    public function getCardNumberAttribute()
    {
        return $this->card_no;
    }



    /**
     * Returns the sheba number.
     * <br>
     * _If the account type is `sheba`, the account number will be assumed as the sheba number._
     *
     * @return string|null
     */
    public function getShebaNo()
    {
        return $this->hasType('sheba') ? $this->getMeta('account_no') : null;
    }



    /**
     * Accessor for the `getShebaNo()` Method.
     *
     * @return string|null
     */
    public function getShebaNoAttribute()
    {
        return $this->getShebaNo();
    }



    /**
     * Accessor for the `getShebaNo()` Method.
     *
     * @return string|null
     */
    public function getShebaNumberAttribute()
    {
        return $this->sheba_no;
    }



    /**
     * Accessor for the `getShebaNo()` Method.
     *
     * @return string|null
     */
    public function getShabaNoAttribute()
    {
        return $this->sheba_no;
    }



    /**
     * Accessor for the `getShebaNo()` Method.
     *
     * @return string|null
     */
    public function getShabaNumberAttribute()
    {
        return $this->sheba_no;
    }



    /**
     * Accessor for the `getShebaNo()` Method.
     *
     * @return string|null
     */
    public function getIbanAttribute()
    {
        return $this->sheba_no;
    }



    /**
     * Whether this account is published.
     *
     * @return bool
     */
    public function isPublished()
    {
        return (
             !$this->trashed() and
             $this->published_by and
             $this->published_at and
             $this->published_at <= Carbon::now()
        );
    }



    /**
     * Whether this account is not published.
     *
     * @return bool
     */
    public function isNotPublished()
    {
        return !$this->isPublished();
    }



    /**
     * Whether this account is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->isPublished();
    }



    /**
     * Whether this account is not active.
     *
     * @return bool
     */
    public function isNotActive()
    {
        return !$this->isActive();
    }



    /**
     * Fills the publish info of this account and activates this account.
     *
     * @return bool
     */
    public function publish()
    {
        return $this->batchSaveBoolean([
             'published_at' => now()->toDateTimeString(),
             'published_by' => user()->id,
        ]);
    }



    /**
     * Fills the publish info of this account and activates this account.
     *
     * @return bool
     */
    public function activate()
    {
        return $this->publish();
    }



    /**
     * Clears the publish info of this account and deactivates this account.
     *
     * @return bool
     */
    public function unpublish()
    {
        return $this->batchSaveBoolean([
             'published_at' => null,
             'published_by' => 0,
        ]);
    }



    /**
     * Clears the publish info of this account and deactivates this account.
     *
     * @return bool
     */
    public function deactivate()
    {
        return $this->unpublish();
    }



    /**
     * Whether this account is default.
     *
     * @return bool
     */
    public function isDefault()
    {
        return ($this->order == 1) ? true : false;
    }



    /**
     * Whether this account is not default.
     *
     * @return bool
     */
    public function isNotDefault()
    {
        return !$this->isDefault();
    }



    /**
     * Makes this account as the default account in its type and reorder the other accounts with that type.
     *
     * @return bool
     */
    public function makeDefault()
    {
        $family = $this
             ->family()
             ->where('id', '<>', $this->id)
             ->orderBy('order')->get()
        ;

        try {
            DB::beginTransaction();

            $this->update(['order' => 1]);

            foreach ($family as $key => $sister) {
                $sister->update(['order' => $key + 2]);
            }

            DB::commit();

            return true;
        } catch (Exception $exception) {
            return false;
        }
    }



    /**
     * Returns an instance of `Illuminate\Database\Eloquent\Relations\HasMany` class
     * to select to account with the same type.
     *
     * @return HasMany
     */
    public function family()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Account', 'type', 'type');
    }



    /**
     * Whether this account is online.
     *
     * @return bool
     */
    public function isOnline()
    {
        return $this->simple_type == 'online';
    }



    /**
     * Whether this account is offline.
     *
     * @return bool
     */
    public function isOffline()
    {
        return !$this->isOnline();
    }



    /**
     * Whether this account may be activated.
     *
     * @return bool
     */
    public function isActivable()
    {
        return $this->isNotPublished() and ($this->isOffline() || $this->slug);
    }



    /**
     * Whether this account may be deactivated.
     *
     * @return bool
     */
    public function isDeactivable()
    {
        return $this->isPublished();
    }



    /**
     * Whether this account type is equal to the specified type.
     *
     * @param string $type
     *
     * @return bool
     */
    public function hasType(string $type): bool
    {
        return $this->type == $type;
    }



    /**
     * Whether this account type is not equal to the specified type.
     *
     * @param string $type
     *
     * @return bool
     */
    public function hasNotType(string $type): bool
    {
        return !$this->hasType($type);
    }



    /**
     * The dynamic part of the meta fields.
     *
     * @return array
     */
    protected function dynamicMetaFields()
    {
        if ($gate = self::getSavingGate() ?: (($this->exists and $this->isOnline()) ? $this->slug : null)) {
            return array_keys(PaymentToolsServiceProvider::getGateFields($gate));
        } else {
            $fields = [];
            $type   = self::getSavingType() ?: ($this->exists ? $this->type : null);
            if (($type == 'shetab') or ($type == 'deposit') or ($type == 'sheba')) {
                $fields[] = 'account_no';
                $fields[] = 'owner';
                $fields[] = 'logo';
            }
            if ($type == 'deposit') {
                $fields[] = 'branch_code';
            }
            return $fields;
        }
    }



    /**
     * Sets the value of the `$saving_gate` property.
     *
     * @param string $gateSlug
     */
    public static function setSavingGate(string $gateSlug)
    {
        self::$saving_gate = $gateSlug;
    }



    /**
     * Returns the value of the `$saving_gate` property.
     *
     * @return string
     */
    protected static function getSavingGate()
    {
        return self::$saving_gate;
    }



    /**
     * Sets the value of the `$saving_type` property.
     *
     * @param string $type
     */
    public static function setSavingType(string $type)
    {
        self::$saving_type = $type;
    }



    /**
     * Returns the value of the `$saving_type` property.
     *
     * @return string
     */
    protected static function getSavingType()
    {
        return self::$saving_type;
    }



    /**
     * Returns a translated title of the type.
     *
     * @return string
     */
    public function getTypeTitle()
    {
        return PaymentToolsServiceProvider::getPaymentTypeTitle($this->type);
    }



    /**
     * Accessor for the `getTypeTitle()` Method
     *
     * @return string
     */
    public function getTypeTitleAttribute()
    {
        return $this->getTypeTitle();
    }
}
