<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Payment\Entities\Traits\TransactionPermissionTrait;
use Modules\Payment\Entities\Traits\TransactionResourcesTrait;
use Modules\Payment\Entities\Traits\TransactionStatusTrait;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction as TransactionHandler;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class Transaction
 *
 * @package Modules\Payment\Entities
 *
 * @property \App\Models\Account|null $account
 * @property \App\Models\User|null    $user
 * @property mixed                    $amount
 * @property mixed                    $payable_amount
 * @property mixed                    $paid_amount
 * @property TransactionHandler       $handler
 * @property string                   $type
 * @property string                   $type_text
 * @property string                   $simple_type
 * @property string                   $simple_type_text
 *
 */
class Transaction extends YasnaModel
{
    use SoftDeletes;
    use TransactionStatusTrait;
    use TransactionResourcesTrait;
    use TransactionPermissionTrait;

    protected $guarded = ['id'];
    protected $casts   = [
        'meta'          => "array",
        'paid_at'       => 'datetime',
        'declared_at'   => 'datetime',
        'verified_at'   => 'datetime',
        'cancelled_at'  => 'datetime',
        'rejected_at'   => 'datetime',
        'refunded_at'   => 'datetime',
        'auto_verify'   => "boolean",
        'bank_response' => "array",

    ];

    protected $_account;
    protected $_user;



    /**
     * Returns an array of the general meta fields.
     *
     * @return array
     */
    public function generalMetaFields()
    {
        return [
             'bank_response',
             'ref_num',
        ];
    }


    /*
    |--------------------------------------------------------------------------
    | Relation
    |--------------------------------------------------------------------------
    |
    */
    public function account()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Account');
    }

    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'User');
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */

    public function update(array $attributes = [], array $options = [])
    {
        return ($newId = \App\Models\Transaction::store(array_merge($attributes, $options, ['id' => $this->id])))
            ? self::find($newId)
            : false;
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */

    public function getAccountAttribute()
    {
        if (is_null($this->_account)) {
            $this->_account = $this->account()->first();
        }

        return $this->_account;
    }

    public function getUserAttribute()
    {
        if (is_null($this->_user)) {
            $this->_user = $this->user()->first();
        }

        return $this->_user;
    }

    public function getAmountAttribute()
    {
        return $this->payable_amount;
    }

    protected function conditionOccurred($part1, $part2 = null)
    {
        if (!$part1) {
            return true;
        } elseif (is_bool($part1)) {
            return $part1;
        } elseif (is_array($part1)) {
            $result = true;
            foreach ($part1 as $key => $value) {
                if (is_string($key)) {
                    $parameters = [$key, $value];
                } else {
                    $parameters = [$value];
                }
                $result = ($result and $this->conditionOccurred(...$parameters));
            }

            return $result;
        } else {
            return $this->checkConditions($part1, $part2);
        }
    }

    protected function checkConditions($part1, $part2 = null)
    {
        if (is_null($part2)) {
            return boolval($this->$part1);
        } else {
            return ($this->$part1 == $this->$part2);
        }
    }

    public function getHandlerAttribute()
    {
        return TransactionHandler::createWithModel($this);
    }

    public function getTypeAttribute()
    {
        $account = $this->account;

        return $account? $account->type: null;
    }

    public function getTypeTextAttribute()
    {
        return trans($this->moduleAlias() . "::types.$this->type");
    }

    public function getSimpleTypeAttribute()
    {
        return ($this->type == 'online') ? 'online' : 'offline';
    }

    public function getSimpleTypeTextAttribute()
    {
        return trans("payment::general.simple-type.$this->simple_type");
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */

    public function isOnline()
    {
        return $this->simple_type == 'online';
    }

    public function isOffline()
    {
        return !$this->isOnline();
    }

}
