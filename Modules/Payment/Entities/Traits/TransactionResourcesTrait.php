<?php

namespace Modules\Payment\Entities\Traits;

/**
 * @property string $invoice_model
 * @property int    $invoice_id
 * @property int    $tracking_no
 * @property string $client_ip
 * @property string $status
 * @property string $status_text
 * @property string $online_payment_status
 * @property int    $user_id
 * @property float  $verified_amount
 * @property float  $refunded_amount
 * @property mixed  $bank_reference_no
 * @property mixed  $bank_resnum
 * @property string $cancellation_reason
 * @property string $rejection_reason
 * @property string $refundation_reason
 */
trait TransactionResourcesTrait
{
    /**
     * boot TransactionResourcesTrait
     *
     * @return void
     */
    public static function bootTransactionResourcesTrait()
    {
        static::addDirectResources([
             "invoice_model",
             "invoice_id",
             "tracking_no",
             "client_ip",
             "title",
             "description",
             "status",
             "status_text",
             "status_applied_at",
             "online_payment_status",
             'payable_amount',
             'paid_amount',
             'verified_amount',
             'refunded_amount',
             'proceeded_at',
             'declared_at',
             'verified_at',
             'effected_at',
             'cancelled_at',
             'rejected_at',
             'refunded_at',
             'bank_reference_no',
             'bank_resnum',
             'cancellation_reason',
             'rejection_reason',
             'refundation_reason',
        ]);
    }



    /**
     * get Type resource.
     *
     * @return string
     */
    protected function getTypeResource()
    {
        $account = $this->account;

        if (!$account or $account->not_exists) {
            return null;
        }

        return $account->type;
    }



    /**
     * get TypeTitle resource.
     *
     * @return string
     */
    protected function getTypeTitleResource()
    {
        $account = $this->account;

        if (!$account or $account->not_exists) {
            return null;
        }

        return $account->type_title;
    }



    /**
     * get AccountId resource.
     *
     * @return string
     */
    protected function getAccountIdResource()
    {
        $account = $this->account;

        if (!$account or $account->not_exists) {
            return null;
        }

        return $account->hashid;
    }



    /**
     * get AccountName resource.
     *
     * @return string
     */
    protected function getAccountNameResource()
    {
        $account = $this->account;

        if (!$account or $account->not_exists) {
            return null;
        }

        return $account->name;
    }



    /**
     * get BankResponses resource.
     *
     * @return array
     */
    protected function getBankResponsesResource()
    {
        if ($this->isOffline()) {
            return [];
        }

        return $this->getMeta('bank_response') ?? [];
    }



    /**
     * get DueDate resource.
     *
     * @return int
     */
    protected function getDueDateResource()
    {
        return $this->getResourceForTimestamps($this->getAttribute("due_date"));
    }



    /**
     * get Model resource.
     *
     * @return array|null
     */
    protected function getModelResource()
    {
        if (!$this->invoice_model or !$this->invoice_id) {
            return null;
        }

        try {
            $model = model($this->invoice_model, $this->invoice_id);
        } catch (\Exception $e) {
            return null;
        }

        if (!$model->exists) {
            return null;
        }

        return $model->toResource();
    }



    /**
     * get User resource.
     *
     * @return array
     */
    protected function getUserResource()
    {
        $user = $this->user;

        if (!$user) {
            return null;
        }

        return $user->toResource(
             [
                  "contact_details_resource_group",
                  "full_name",
             ]
        );
    }

}
