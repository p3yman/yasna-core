<?php
namespace Modules\Payment\Entities\Traits;

trait AccountTrait
{
    protected $bank_type;
    protected $currency;
    protected $obj;

    public function onlineList()
    {
        $this->bank_type = 'online';
        return $this;
    }

    public function depositList()
    {
        $this->bank_type = 'deposit';
        return $this;
    }

    public function shetabList()
    {
        $this->bank_type = 'shetab';
        return $this;
    }

    public function shebaList()
    {
        $this->bank_type = 'sheba';
        return $this;
    }

    public function chequeList()
    {
        $this->bank_type = 'cheque';
        return $this;
    }

    public function cashList()
    {
        $this->bank_type = 'cash';
        return $this;
    }

    public function posList()
    {
        $this->bank_type = 'pos';
        return $this;
    }
    
    public function in($currency = 'default')
    {
        if ($currency == 'default') {
            $this->currency = get_setting('default_currency');
        } else {
            $this->currency = $currency;
        }
        return $this;
    }

    public function addonWhere()
    {
        $this->obj = $this;
        $this->obj = $this->set_bank_type($this->obj);
        $this->obj = $this->set_currency($this->obj);
        $this->obj = $this->extra_where($this->obj);
        return $this->obj;
    }

    public function get()
    {
        return $this->addonWhere()->get();
    }

    protected function set_bank_type($obj)
    {
        if (isset($this->bank_type)) {
            return $obj->where('type', $this->bank_type);
        } else {
            return $obj;
        }
    }

    protected function set_currency($obj)
    {
        if (isset($this->currency)) {
            return $obj->where('currency', $this->currency);
        } else {
            return $obj;
        }
    }

    protected function extra_where($obj)
    {
        return $obj;
    }
}
