<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 01/01/2018
 * Time: 11:18 AM
 */

namespace Modules\Payment\Entities\Traits;

use Carbon\Carbon;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction as TransactionHandler;

trait TransactionStatusTrait
{
    protected function applyStatus(array $statusData, array $additionalData = [])
    {
        return $this->update(array_merge($statusData, $additionalData));
    }

    public function applyProceededStatus(array $additionalData = [])
    {
        return $this->applyStatus([
            'proceeded_at' => Carbon::now(),
            'proceeded_by' => user()->id,
        ], $additionalData);
    }

    public function applyDeclaredStatus(array $additionalData = [])
    {
        return $this->applyStatus([
            'declared_at' => Carbon::now(),
            'declared_by' => user()->id,
        ], $additionalData);
    }

    public function applyCancelledStatus(array $additionalData = [])
    {
        return $this->applyStatus([
            'cancelled_at' => Carbon::now(),
            'cancelled_by' => user()->id,
        ], $additionalData);
    }

    public function getStatusAttribute()
    {
        $status = null;
        foreach (TransactionHandler::statusConditions() as $status => $condition) {
            if (!is_string($status)) {
                $status    = $condition;
                $condition = true;
            }
            if ($this->conditionOccurred($condition)) {
                break;
            }
        }
        return $status;
    }

    public function getStatusTextAttribute()
    {
        return trans($this->moduleAlias() . "::status.$this->status");
    }

    public function getStatusColorAttribute()
    {
        return config($this->moduleAlias() . ".status.color.$this->status");
    }

    public function getStatusIconAttribute()
    {
        return config($this->moduleAlias() . ".status.icon.$this->status");
    }

    public function getStatusDateAttribute()
    {
        switch ($this->status) {
            case "created":
                return $this->created_at;

            case "sent-to-gateway":
                return $this->proceeded_at;

            case "declared":
                return $this->declared_at;

            case "verified":
                return $this->verified_at;

            case "rejected":
                return $this->rejected_at;

            case "cancelled":
                return $this->cancelled_at;

            case "refunded":
                return $this->refunded_at;
        }
    }

    public function getStatusDatetimeAttribute()
    {
        return $this->status_date;
    }

    public function getIsVerifiedAttribute()
    {
        return $this->status == 'verified';
    }
}
