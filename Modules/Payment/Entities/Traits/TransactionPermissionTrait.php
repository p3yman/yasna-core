<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/8/19
 * Time: 5:14 PM
 */

namespace Modules\Payment\Entities\Traits;


trait TransactionPermissionTrait
{
    /**
     * Whether the logged in user has the specified permission on the transactions.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function can(string $permission = '*'): bool
    {
        return user()->as('admin')->can($this->permissionString($permission));
    }



    /**
     * Whether the logged in user has not the specified permission on the transactions.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function cannot(string $permission = '*'): bool
    {
        return !$this->can($permission);
    }



    /**
     * Returns the permission string for checking access on the permissions.
     *
     * @param string $permission
     *
     * @return string
     */
    public function permissionString(string $permission = '*'): string
    {
        return "payment_transactions.$permission";
    }
}
