<?php

namespace Modules\Payment\Entities\Traits;

use Carbon\Carbon;

trait TransactionValidationTrait
{
    protected function make_property_validation()
    {
        $this->user_id_validation();
        $this->account_id_validation();
        $this->invoice_id_validation();
        $this->client_ip_validation();
        $this->callback_url_validation();
        $this->payable_amount_validation();
        $this->auto_verify_validation();

        // validation offline payment
        $this->offline_payment_validation();

        return $this->make_validation;
    }

    protected function offline_payment_validation()
    {
        if ($this->online_payment_status != 1) {
            $this->paid_amount_validation();
            $this->verified_amount_validation();
            $this->refunded_amount_validation();
            $this->paid_at_validation();
            $this->declared_at_validation();
            $this->verified_at_validation();
            $this->cancelled_at_validation();
            $this->rejected_at_validation();
            $this->refunded_at_validation();

            $this->created_by_validation();
            $this->deleted_by_validation();
            $this->published_by_validation();
            $this->refunded_by_validation();
            $this->rejected_by_validation();
            $this->updated_by_validation();
            $this->verified_by_validation();
        }
    }

    protected function user_id_validation()
    {
        if (!$this->user_id >= 0) {
            $this->make_validation++;
        }
    }

    protected function account_id_validation()
    {
        if (!$this->account_id > 0) {
            $this->make_validation++;
        }
    }

    protected function invoice_id_validation()
    {
        if (!$this->invoice_id > 0) {
            $this->make_validation++;
        }
    }

    protected function client_ip_validation()
    {
        $client_ip_validator = Validator::make(['client_ip' => $this->client_ip], ['client_ip' => 'required|ip']);

        if ($client_ip_validator->fails()) {
            $this->make_validation++;
        }
    }

    protected function callback_url_validation()
    {
        $callback_url_validator = Validator::make(['callback_url' => $this->callback_url], ['callback_url' => 'required|url']);

        if ($callback_url_validator->fails()) {
            $this->make_validation++;
        }
    }

    protected function payable_amount_validation()
    {
        if ($this->payable_amount < 1) {
            $this->make_validation++;
        }
    }

    protected function auto_verify_validation()
    {
        if ($this->auto_verify) {
            $this->auto_verify = 1;
        } else {
            $this->auto_verify = 0;
        }
    }

    protected function check_online_bank($bank)
    {
        if ($bank->type == 'online') {
            $this->online_payment_status = 1;
        }
    }

    protected function paid_amount_validation()
    {
        if ($this->paid_amount < 0) {
            $this->make_validation++;
        }
    }

    protected function verified_amount_validation()
    {
        if ($this->verified_amount < 0) {
            $this->make_validation++;
        }
    }

    protected function refunded_amount_validation()
    {
        if ($this->refunded_amount < 0) {
            $this->make_validation++;
        }
    }

    protected function paid_at_validation()
    {
        if ($this->paid_at != null) {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $this->paid_at) === false) {
                $this->make_validation++;
            }
        }
    }

    protected function declared_at_validation()
    {
        if ($this->declared_at != null) {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $this->declared_at) === false) {
                $this->make_validation++;
            }
        }
    }

    protected function verified_at_validation()
    {
        if ($this->verified_at != null) {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $this->verified_at) === false) {
                $this->make_validation++;
            }
        }
    }

    protected function verified_by_validation()
    {
        if ($this->verified_by < 0) {
            $this->make_validation++;
        }
    }

    protected function cancelled_at_validation()
    {
        if ($this->cancelled_at != null) {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $this->cancelled_at) === false) {
                $this->make_validation++;
            }
        }
    }

    protected function rejected_at_validation()
    {
        if ($this->rejected_at != null) {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $this->rejected_at) === false) {
                $this->make_validation++;
            }
        }
    }

    protected function rejected_by_validation()
    {
        if ($this->rejected_by < 0) {
            $this->make_validation++;
        }
    }

    protected function refunded_at_validation()
    {
        if ($this->refunded_at != null) {
            if (Carbon::createFromFormat('Y-m-d H:i:s', $this->refunded_at) === false) {
                $this->make_validation++;
            }
        }
    }

    protected function refunded_by_validation()
    {
        if ($this->refunded_by < 0) {
            $this->make_validation++;
        }
    }

    protected function created_by_validation()
    {
        if ($this->created_by < 0) {
            $this->make_validation++;
        }
    }

    protected function updated_by_validation()
    {
        if ($this->updated_by < 0) {
            $this->make_validation++;
        }
    }

    protected function deleted_by_validation()
    {
        if ($this->deleted_by < 0) {
            $this->make_validation++;
        }
    }

    protected function published_by_validation()
    {
        if ($this->published_by < 0) {
            $this->make_validation++;
        }
    }
}
