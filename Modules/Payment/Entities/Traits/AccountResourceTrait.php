<?php

namespace Modules\Payment\Entities\Traits;

use Modules\Payment\Providers\PaymentToolsServiceProvider;

/**
 * @property string $slug
 */
trait AccountResourceTrait
{
    /**
     * boot AccountResourceTrait
     *
     * @return void
     */
    public static function bootAccountResourceTrait()
    {
        static::addDirectResources([
             'title',
             'currency',
             'type',
             'name',
             'slug',
             'account_no',
             'owner',
             'branch_code',
        ]);
    }



    /**
     * get IsActive resource.
     *
     * @return int
     */
    protected function getIsActiveResource()
    {
        return intval($this->isActive());
    }



    /**
     * get Gateway resource.
     *
     * @return array
     */
    protected function getGatewayResource()
    {
        if ($this->isOffline() or !$this->slug) {
            return null;
        }

        $rules  = PaymentToolsServiceProvider::getGateFields($this->slug);
        $fields = array_keys($rules);
        $result = [];

        foreach ($fields as $field) {
            $result[$field] = $this->getMeta($field);
        }

        return $result;
    }
}
