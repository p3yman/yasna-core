<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/8/19
 * Time: 9:39 AM
 */

namespace Modules\Payment\Entities\Traits;


trait AccountPermissionTrait
{
    /**
     * Whether the logged in user has the specified permission in at least one of the account types.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function can(string $permission = '*'): bool
    {
        // if the model has a type, the permission will be checked based on the type.
        $simple_type = $this->simple_type;
        if ($simple_type) {
            return $this->canForType($simple_type, $permission);
        }

        // if the model has not a type, a permission of any simple type will be acceptable.
        return (
             $this->canOnline($permission)
             or
             $this->canOffline($permission)
        );
    }



    /**
     * Whether the logged in user has not the specified permission in at least one of the account types.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function cannot(string $permission = '*'): bool
    {
        return !$this->can($permission);
    }



    /**
     * Whether the logged in user has the specified permission in the online accounts.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function canOnline(string $permission = '*'): bool
    {
        return $this->canForType('online', $permission);
    }



    /**
     * Whether the logged in user has not the specified permission in the online accounts.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function cannotOnline(string $permission = '*'): bool
    {
        return !$this->canOnline($permission);
    }



    /**
     * Whether the logged in user has the specified permission in the offline accounts.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function canOffline(string $permission = '*'): bool
    {
        return $this->canForType('offline', $permission);
    }



    /**
     * Whether the logged in user has not the specified permission in the offline accounts.
     *
     * @param string $permission
     *
     * @return bool
     */
    public function cannotOffline(string $permission = '*'): bool
    {
        return !$this->cannotOffline($permission);
    }



    /**
     * Whether the logged in user has the specified permission in the specified type of accounts.
     *
     * @param string $simple_type
     * @param string $permission
     *
     * @return bool
     */
    public function canForType(string $simple_type, string $permission = '*'): bool
    {
        $permission_string = $this->typePermissionString($simple_type, $permission);

        return user()->as('admin')->can($permission_string);
    }



    /**
     * Whether the logged in user has not the specified permission in the specified type of accounts.
     *
     * @param string $simple_type
     * @param string $permission
     *
     * @return bool
     */
    public function cannotForType(string $simple_type, string $permission = '*'): bool
    {
        return !$this->canForType($simple_type, $permission);
    }



    public function typePermissionString(string $simple_type, string $permission = '*')
    {
        return "payment_{$simple_type}_accounts.$permission";
    }
}
