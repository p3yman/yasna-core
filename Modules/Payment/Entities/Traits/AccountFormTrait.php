<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/7/19
 * Time: 11:51 AM
 */

namespace Modules\Payment\Entities\Traits;


use Illuminate\Validation\Rule;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Yasna\Services\ModelTraits\YasnaFormTrait;

trait AccountFormTrait
{
    use YasnaFormTrait;



    /**
     * The Main Part of the Form Data
     */
    public function mainFormItems()
    {
        $module = module('payment');

        $this->addFormItem('title')
             ->whichIsRequired()
             ->withClass('form-default')
        ;


        $this->addFormItem('currency')
             ->whichIsRequired()
             ->withValidationRule(
                  Rule::in(PaymentToolsServiceProvider::getAvailableCurrencies())
             )
             ->withClass('form-default')
        ;


        $this->addFormItem('type')
             ->whichIsRequired()
             ->withValidationRule(
                  Rule::in(PaymentToolsServiceProvider::allPaymentTypes())
             )
             ->withClass('form-default')
             ->withLabel($module->getTrans('general.type'))
        ;

    }



    /**
     * The Slug Part of the Form Data
     */
    public function slugFormItems()
    {
        if ($this->exists or $this->isOffline()) {
            return;
        }


        $this->addFormItem('slug')
             ->withValidationRule(
                  Rule::in(PaymentToolsServiceProvider::getAvailableGates(true))
             )->whichIsRequired()
             ->withLabel(module('payment')->getTrans('general.gate.title.singular'))
        ;
    }



    /**
     * The Gateway Part of the Form Data
     */
    public function gatewayFormItems()
    {
        if ($this->isOffline() or !$this->slug) {
            return;
        }

        $gateway_rules = PaymentToolsServiceProvider::getGateFields($this->slug);

        foreach ($gateway_rules as $field_name => $field_rules) {
            $rules_string = implode('|', $field_rules);

            $this->addFormItem($field_name)
                 ->withValidationRule($rules_string)
                 ->withLabel(module('payment')->getTrans("validation.attributes.$field_name"))
            ;

            if (in_array('required', $field_rules)) {
                $this->withClass('form-default');
            }

            if (in_array('numeric', $field_rules)) {
                $this->withPurificationRule('ed');
            }
        }
    }



    /**
     * The Online Part of the Form Data
     */
    public function onlineFormItems()
    {
        if ($this->hasNotType('online')) {
            return;
        }

        $this->addFormItem('name')->whichIsRequired();
    }



    /**
     * The Deposit Part of the Form Data
     */
    public function depositFormItems()
    {
        if ($this->hasNotType('deposit')) {
            return;
        }

        $module = module('payment');

        $this->addFormItem('account_no')
             ->whichIsRequired()
             ->withPurificationRule('ed')
        ;

        $this->addFormItem('owner')
             ->withLabel($module->getTrans('general.account.owner.name.singular'))
             ->whichIsRequired()
        ;

        $this->addFormItem('branch_code')
             ->withLabel($module->getTrans('general.bank.branch-code'))
             ->withValidationRule('numeric')
             ->withPurificationRule('ed')
        ;
    }



    /**
     * The Shetab Part of the Form Data
     */
    public function shetabFormItems()
    {
        if ($this->hasNotType('shetab')) {
            return;
        }

        $module = module('payment');

        $this->addFormItem('account_no')
             ->whichIsRequired()
             ->withValidationRule('digits:16')
             ->withPurificationRule('ed')
        ;

        $this->addFormItem('owner')
             ->withLabel($module->getTrans('general.account.owner.name.singular'))
             ->whichIsRequired()
        ;
    }



    /**
     * The Sheba Part of the Form Data
     */
    public function shebaFormItems()
    {
        if ($this->hasNotType('sheba')) {
            return;
        }

        $module = module('payment');

        $this->addFormItem('account_no')
             ->whichIsRequired()
             ->withPurificationRule('ed')
        ;

        $this->addFormItem('owner')
             ->withLabel($module->getTrans('general.account.owner.name.singular'))
             ->whichIsRequired()
        ;
    }
}
