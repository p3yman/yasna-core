<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/8/19
 * Time: 10:28 AM
 */

namespace Modules\Payment\Entities\Traits;


trait AccountElectorTrait
{
    /**
     * Elector to select active accounts only.
     *
     * @param bool $value
     */
    public function electorActivesOnly(bool $value)
    {
        if (!$value) {
            return;
        }

        $this->elector()->whereIsActive();
    }



    /**
     * Elector for type.
     *
     * @param string $type
     */
    public function electorType(string $type)
    {
        $this->elector()->where('type', $type);
    }
}
