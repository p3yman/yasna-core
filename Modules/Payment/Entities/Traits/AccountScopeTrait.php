<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/8/19
 * Time: 11:28 AM
 */

namespace Modules\Payment\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;

trait AccountScopeTrait
{
    /**
     * Scope to select active accounts only.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeWhereIsActive(Builder $builder): Builder
    {
        return $builder
             ->where('published_by', '>=', 1)
             ->whereNotNull('published_at')
             ->where('published_at', '<=', now()->toDateTimeString())
             ;
    }



    /**
     * Scope to select inactive accounts only.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeWhereIsNotActive(Builder $builder): Builder
    {
        return $builder->where(function (Builder $query) {
            $query->where('published_by', '<', 1)
                  ->orWhereNull('published_at')
                  ->orWhere('published_at', '>=', now()->toDateTimeString())
            ;
        });
    }


}
