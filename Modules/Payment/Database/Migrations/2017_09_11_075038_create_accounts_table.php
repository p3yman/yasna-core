<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('slug')->index();
            $table->string('title')->index();
            $table->string('type')->index(); // online or offline type
            $table->integer('order')->index();
            $table->string('currency')->default('IRR')->index();

            $table->longText('meta')->nullable();

            /*-----------------------------------------------
            | Change Logs ...
            */
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0)->index();
            $table->unsignedInteger('deleted_by')->default(0)->index();
            $table->timestamp('published_at')->nullable()->index();
            $table->unsignedInteger('published_by')->default(0)->index();

            /*-----------------------------------------------
            | Indexes ...
            */
            $table->tinyInteger('converted')->default(0)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
