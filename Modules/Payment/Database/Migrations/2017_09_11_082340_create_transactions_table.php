<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->default(0)->nullable()->index();
            $table->unsignedInteger('account_id')->index();
            $table->unsignedInteger('invoice_id')->default(0)->index();

            $table->string('tracking_no')->nullable()->index();
            $table->string('bank_reference_no')->nullable()->index();
            $table->string('bank_resnum')->nullable()->index();

            $table->string('client_ip')->nullalbe()->index();
            $table->boolean('auto_verify')->default(1)->index();
            $table->text('callback_url')->nullable();
            $table->string('online_payment_status')->nullable()->index(); //returned by the bank gateway.


            /*-----------------------------------------------
            | Amount ...
            */
            $table->float('payable_amount', 15, 2)->default(0)->index();
            $table->float('paid_amount', 15, 2)->default(0)->index();
            $table->float('verified_amount', 15, 2)->default(0)->index();
            $table->float('refunded_amount', 15, 2)->default(0)->index();


            /*-----------------------------------------------
            | Status ...
            */

            $table->timestamp('proceeded_at')->nullable()->index();
            $table->timestamp('declared_at')->nullable()->index();
            $table->timestamp('verified_at')->nullable()->index();
            $table->timestamp('effected_at')->nullable()->index();
            $table->timestamp('cancelled_at')->nullable()->index();
            $table->timestamp('rejected_at')->nullable()->index();
            $table->timestamp('refunded_at')->nullable()->index();
            $table->timestamp('due_date')->nullable()->index();

            $table->unsignedInteger('proceeded_by')->default(0)->index();
            $table->unsignedInteger('declared_by')->default(0)->index();
            $table->unsignedInteger('verified_by')->default(0)->index();
            $table->unsignedInteger('paid_by')->default(0)->index();
            $table->unsignedInteger('effected_by')->default(0)->index();
            $table->unsignedInteger('cancelled_by')->default(0)->index();
            $table->unsignedInteger('rejected_by')->default(0)->index();
            $table->unsignedInteger('refunded_by')->default(0)->index(); //might be useless

            $table->longText('meta')->nullable();
            $table->text('cancellation_reason')->nullable();
            $table->text('rejection_reason')->nullable();
            $table->text('refundation_reason')->nullable();

            /*-----------------------------------------------
            | Change Logs ...
            */
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0)->index();
            $table->unsignedInteger('deleted_by')->default(0)->index();

            /*-----------------------------------------------
            | Indexes ...
            */
            $table->tinyInteger('converted')->default(0)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
