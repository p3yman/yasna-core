<?php

namespace Modules\Payment\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns an array of the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'slug'          => 'site_currencies',
                  'title'         => $this->runningModule()->getTrans('seeder.settings.site-currencies'),
                  'category'      => 'upstream',
                  'order'         => '20',
                  'data_type'     => 'array',
                  'default_value' => 'IRR',
                  'hint'          => $this->runningModule()->getTrans('seeder.settings.site-currencies-helper'),
                  'css_class'     => '',
                  'is_localized'  => '0',

             ],
        ];
    }
}
