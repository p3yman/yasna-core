<?php

namespace Modules\Payment\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $array = [
             [
                  'name'             => 'بانک صادرات',
                  'slug'             => 'saderat',
                  'title'            => 'بانک صادرات - کارت به کارت',
                  'type'             => 'shetab',
                  'currency'         => 'IRR',
                  'meta-card_no'     => '6037-6915-2367-5981',
                  'meta-holder_name' => 'احمد زمانی',
             ],
             [
                  'name'                    => 'بانک سامان',
                  'slug'                    => 'saman',
                  'title'                   => 'بانک سامان - آنلاین',
                  'type'                    => 'online',
                  'currency'                => 'IRR',
                  'meta-username'           => 'saman123',
                  'meta-password'           => 'saman987',
                  'meta-merchant_id'        => '1298741',
                  'meta-online_payment_url' => 'https://sep.shaparak.ir/payment.aspx',
                  'meta-online_verify_url'  => 'https://sep.shaparak.ir/payments/referencepayment.asmx?wsdl',
                  'meta-online_refund_url'  => 'https://google.com/refund',
                  'meta-logo'               => 'x.jpg',
             ],
             [
                  'name'                   => trans('payment::gate.zarinpal'),
                  'slug'                   => 'zarinpal',
                  'title'                  => trans('payment::gate.zarinpal'),
                  'type'                   => 'online',
                  'currency'               => 'IRR',
                  'meta-merchant_id'       => 'f2916330-4793-11e8-9825-005056a205be',
                  'meta-description'       => trans('payment::general.zarinpal-description'),
                  "meta-payment_url"       => 'https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json',
                  'meta-payment_start_url' => 'https://www.zarinpal.com/pg/StartPay/',
                  'meta-verify_url'        => 'https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json',
             ],
        ];

        yasna()->seed('accounts', $array);
    }
}
