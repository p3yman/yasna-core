<?php

namespace Modules\Payment\Events;

use Modules\Yasna\Services\YasnaEvent;

class FailedPayment extends YasnaEvent
{
    public $model_name = 'Transaction';
}
