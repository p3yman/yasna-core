<?php
return [
     "settings" => [
          "site-currencies"        => "ارزهای قابل استفاده",
          "site-currencies-helper" => "همه‌ی ارزهایی که در سایت استفاده می‌شوند.",
     ],
];
