<?php
return [
     "invalid_invoice" => "فاکتور مرجع معتبر نیست.",

     "attributes" => [
          "username"           => "نام کاربری",
          "password"           => "گذرواژه",
          "merchant_id"        => "شماره شناسایی فروشنده",
          "terminal_id"        => "شماره شناسایی ترمینال",
          "private_key"        => "کلید خصوصی فروشنده",
          "public_key"         => "کلید عمومی فروشنده",
          "online_payment_url" => "آدرس پرداخت آنلاین",
          "online_check_url"   => "آدرس بررسی نتیجه‌ی پرداخت آنلاین",
          "online_verify_url"  => "آدرس رسیدگی آنلاین",
          "online_refund_url"  => "آدرس بازگشت وجه آنلاین",
          "payment_url"        => "آدرس درگاه پرداخت",
          "payment_start_url"  => "آدرس شروع تراکنش",
          "verify_url"         => "آدرس تصدیق اصالت تراکنش",
          "description"        => "عنوان درگاه",
          "account"            => "حساب",
          "callback_url"       => "آدرس بازگشت",
          "invoice_model"      => "نوع فاکتور",
          "payable_amount"     => "مبلغ قابل پرداخت",
          "referral_gateway"   => "درگاه ارجاع",
     ],
];
