<?php
return [
    "created"         => "پرداخت‌نشده",
    "sent-to-gateway" => "در انتظار درگاه",
    "declared"        => "در انتظار تأیید",
    "verified"        => "تأییدشده",
    "rejected"        => "ردشده",
    "cancelled"        => "لغوشده",
    "refunded"        => "برگشت‌داده‌شده",
];
