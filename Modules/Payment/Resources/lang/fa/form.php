<?php
return [
    "feed" => [
        "success" => [
            "saving-account" => "حساب با موفقیت ذخیره شد.",
        ],
        "error"   => [
            "saving-account"     => "ذخیره‌ی حساب با مشکل مواجه شد.",
            "activating-account" => "این حساب قابلیت فعال‌شدن ندارد.",
            "deleting-account"   => "این حساب امکان حذف شدن ندارد.",
        ],
    ],
];
