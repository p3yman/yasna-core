<?php
return [
     "payment-module"       => "ماژول پرداخت",
     "menu"                 => "فهرست",
     "currency"             => "واحد پول",
     "type"                 => "نوع",
     "default"              => "پیش‌فرض",
     "none-of-theme"        => "هیچ کدام",
     "all"                  => "همه",
     "tracking-number"      => "شماره‌ی پیگیری",
     "invoice-number"       => "شماره‌ی فاکتور",
     "reference-number"     => "شماره‌ی مرجع",
     "filter"               => "فیلتر",
     "flush-filters"        => "تخلیه‌ی فیلترها",
     "payer"                => "پرداخت‌کننده",
     'zarinpal-description' => "نام سایت یا فروشگاه شما",

     "date-of-creation"           => "تاریخ ایجاد",
     "time-of-creation"           => "زمان ایجاد",
     "date-from"                  => "از تاریخ",
     "date-to"                    => "تا تاریخ",
     "date-of-last-status-change" => "تاریخ آخرین تغییر وضعیت",

     "documentation" => [
          "title" => "راهنمای استفاده",
     ],


     "transaction" => [
          "title"  => [
               "plural" => "تراکنش‌ها",
          ],
          "create" => "ایجاد تراکنش",
          "status" => "وضعیت تراکنش",
     ],

     "account" => [
          "title"   => [
               "plural"   => "حساب‌ها",
               "singular" => "حساب",
          ],
          "online"  => [
               "plural" => "حساب‌های آنلاین",
          ],
          "offline" => [
               "plural" => "حساب‌های آفلاین",
          ],
          "owner"   => [
               "name" => [
                    "singular" => "نام صاحب حساب",
               ],
          ],
          "status"  => [
               "activated"   => "فعال",
               "deactivated" => "غیرفعال",
          ],
          "message" => [
               "is-default" => "این حساب در حال حاضر پیش‌فرض است.",
          ],
          "number"  => "شماره‌ی حساب",
     ],

     "bank" => [
          "title"       => [
               "singular" => "بانک",
          ],
          "name"        => [
               "singular" => "نام بانک",
          ],
          "logo"        => [
               "singular" => "لوگوی بانک",
          ],
          "branch-code" => "کد شعبه",
          "IBAN"        => "شماره‌ی شبا",
     ],

     "gate" => [
          "title"   => [
               "singular" => "درگاه",
          ],
          "message" => [
               "no-selection-hint" => "تا زمانی که درگاهی انتخاب نشود، این اکانت فعال نخواهد شد.",
          ],
     ],

     "date" => [
          "created" => "تاریخ ایجاد",
     ],

     "action" => [
          "edit"             => "ویرایش",
          "disable"          => "غیرفعال کردن",
          "enable"           => "فعال کردن",
          "make-default"     => "انتخاب به عنوان پیش‌فرض",
          "add-to-something" => "افزودن به «:something»",
          "edit-something"   => "ویرایش «:something»",
          "delete-something" => "حذف :something",
     ],

     "card" => [
          "number" => "شماره‌ی کارت",
     ],

     "simple-type" => [
          "online"  => "آنلاین",
          "offline" => "آفلاین",
     ],

     "export" => [
          "excel" => "خروجی اکسل",
     ],

     "amount" => [
          "singular" => "مبلغ",
     ],
];
