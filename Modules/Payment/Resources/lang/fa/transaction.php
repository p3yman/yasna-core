<?php
return [
     "new"      => "تراکنش جدید",
     "creditor" => "بستانکار",
     "debtor"   => "بدهکار",
     "type"     => "نوع تراکنش",
];
