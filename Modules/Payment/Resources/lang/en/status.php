<?php
return [
    "created"         => "Unpaid",
    "sent-to-gateway" => "Sent to Gateway",
    "declared"        => "Pending for Verification",
    "verified"        => "Verified",
    "rejected"        => "Rejected",
    "cancelled"        => "Canceled",
    "refunded"        => "Refunded",
];
