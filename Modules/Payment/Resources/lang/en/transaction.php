<?php
return [
     "new"      => "New Transaction",
     "creditor" => "Creditor",
     "debtor"   => "Debtor",
     "type"     => "Transaction Type",
];
