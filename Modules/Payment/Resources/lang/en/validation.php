<?php
return [
     "invalid_invoice" => "The invoice is invalid.",

     "attributes" => [
          "username"           => "Username",
          "password"           => "Password",
          "merchant_id"        => "Merchant Id",
          "terminal_id"        => "Terminal Id",
          "private_key"        => "Merchant Private Key",
          "public_key"         => "Merchant Public Key",
          "online_payment_url" => "Online Payment URL",
          "online_check_url"   => "Online Payment Result URL",
          "online_verify_url"  => "Online Verification URL",
          "online_refund_url"  => "Online Refund URL",
          "payment_url"        => "Gateway URL",
          "payment_start_url"  => "Payment-Start URL",
          "verify_url"         => "Verify URL",
          "description"        => "Gateway Title",
          "account"            => "Account",
          "callback_url"       => "Callback URL",
          "invoice_model"      => "Invoice Model",
          "payable_amount"     => "Payable Amount",
          "referral_gateway"   => "Referral Gateway",
     ],
];
