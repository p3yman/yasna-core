<?php
return [
     "payment-module"       => "Payment Module",
     "menu"                 => "Menu",
     "currency"             => "Currency",
     "type"                 => "Type",
     "default"              => "Default",
     "none-of-theme"        => "None",
     "all"                  => "All",
     "tracking-number"      => "Tracking Number",
     "invoice-number"       => "Invoice Number",
     "reference-number"     => "Reference Number",
     "filter"               => "Filter",
     "flush-filters"        => "Flush Filters",
     "payer"                => "Payer",
     'zarinpal-description' => "Your site name or your shop name",

     "date-of-creation"           => "Creation Date",
     "time-of-creation"           => "Creation Time",
     "date-from"                  => "From",
     "date-to"                    => "To",
     "date-of-last-status-change" => "Last Status Change Date",

     "documentation" => [
          "title" => "Documentation",
     ],


     "transaction" => [
          "title"  => [
               "plural" => "Transactions",
          ],
          "create" => "Create",
          "status" => "Status",
     ],

     "account" => [
          "title"   => [
               "plural"   => "Accounts",
               "singular" => "Account",
          ],
          "online"  => [
               "plural" => "Online Accounts",
          ],
          "offline" => [
               "plural" => "Offline Accounts",
          ],
          "owner"   => [
               "name" => [
                    "singular" => "Account Owner Name",
               ],
          ],
          "status"  => [
               "activated"   => "Activated",
               "deactivated" => "Deactivated",
          ],
          "message" => [
               "is-default" => "This is the default account.",
          ],
          "number"  => "Account Number",
     ],

     "bank" => [
          "title"       => [
               "singular" => "Bank",
          ],
          "name"        => [
               "singular" => "Bank Name",
          ],
          "logo"        => [
               "singular" => "Bank Logo",
          ],
          "branch-code" => "Branch Code",
          "IBAN"        => "IBAN",
     ],

     "gate" => [
          "title"   => [
               "singular" => "Gate",
          ],
          "message" => [
               "no-selection-hint" => "The account cannot be activated, since no gate is selected.",
          ],
     ],

     "date" => [
          "created" => "Creation Date",
     ],

     "action" => [
          "edit"             => "Edit",
          "disable"          => "Disable",
          "enable"           => "Enable",
          "make-default"     => "Set Default",
          "add-to-something" => "Add to \":something\"",
          "edit-something"   => "Edit \":something\"",
          "delete-something" => "Delete \":something\"",
     ],

     "card" => [
          "number" => "Card Number",
     ],

     "simple-type" => [
          "online"  => "Online",
          "offline" => "Offline",
     ],

     "export" => [
          "excel" => "Excel Export",
     ],

     "amount" => [
          "singular" => "Amount",
     ],
];
