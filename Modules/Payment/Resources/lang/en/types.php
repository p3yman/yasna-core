<?php
return [
    'online'  => 'Online Payment',
    'deposit' => 'Deposit',
    'shetab'  => 'Shetab',
    'sheba'   => 'Sheba',
    'cheque'  => 'Cheque',
    'cash'    => 'Cash',
    'pos'     => 'POS',
];
