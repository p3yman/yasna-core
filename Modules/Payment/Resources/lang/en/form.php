<?php
return [
    "feed" => [
        "success" => [
            "saving-account" => "Account saved successfully.",
        ],
        "error"   => [
            "saving-account"     => "An error occurred while saving account.",
            "activating-account" => "Account cannot be activated.",
            "deleting-account"   => "Account cannot be deleted.",
        ],
    ],
];
