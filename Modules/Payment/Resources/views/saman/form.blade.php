<form action="{{ $action }}" method="post" name="Saman" id="payment-form"
      @if(user()->isDeveloper()) target="_blank" @endif>
    <input type="hidden" name="Amount" id="Amount" value="{{ $amount }}"/>
    <input type="hidden" name="MID" id="MID" value="{{ $merchantId }}"/>
    <input type="hidden" name="ResNum" id="ResNum" value="{{ $trackingNumber }}"/>
    <input type="hidden" name="RedirectURL" id="RedirectURL" value="{{ $callback }}"/>
</form>
<script>
    document.getElementById("payment-form").submit();
</script>
