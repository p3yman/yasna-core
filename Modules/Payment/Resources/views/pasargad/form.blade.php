<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>

<form method='post' action='{{ $formAction }}' id="payment-form" style="display: none" name="pasargad"
      @if(user()->isDeveloper()) target="_blank" @endif>
    invoiceNumber<input type='text' name='invoiceNumber' value='{{ $trackingNumber }}'/><br/>
    invoiceDate<input type='text' name='invoiceDate' value='{{ $invoiceDate }}'/><br/>
    amount<input type='text' name='amount' value='{{ $amount }}'/><br/>
    terminalCode<input type='text' name='terminalCode' value='{{ $terminalCode }}'/><br/>
    merchantCode<input type='text' name='merchantCode' value='{{ $merchantCode }}'/><br/>
    redirectAddress<input type='text' name='redirectAddress' value='{{ $callback }}'/><br/>
    timeStamp<input type='text' name='timeStamp' value='{{ $timeStamp }}'/><br/>
    action<input type='text' name='action' value='{{ $action }}'/><br/>
    sign<input type='text' name='sign' value='{{ $sign }}'/><br/>
</form>
<script>
    document.getElementById("payment-form").submit();
</script>
</body>
</html>
