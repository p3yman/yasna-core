{!!
    widget('modal')
        ->targetIf($model->exists, PaymentRouteTools::action('Manage\Downstream\AccountsController@editSubmit'))
        ->targetIf(!$model->exists, PaymentRouteTools::action('Manage\Downstream\AccountsController@create', [
            'type' => $model->simple_type
        ]))->labelIf($model->exists, trans('payment::general.action.edit-something', ['something' => $model->title]))
        ->labelIf(!$model->exists, trans('payment::general.action.add-to-something', [
            'something' => trans("payment::general.account.$model->simple_type.plural")
        ]))->validation(false)
!!}
<div class='modal-body'>
    {!!
        widget('hidden')
            ->name('hashid')
            ->value($model->spreadMeta()->hashid)
            ->condition($model->exists)
    !!}
    
    {!!
        widget('input')
            ->name('title')
            ->label(trans('validation.attributes.title'))
            ->inForm()
            ->value($model->title)
            ->required()
    !!}
    
    {!!
        widget('input')
            ->name('name')
            ->label(trans('payment::general.bank.title.singular'))
            ->inForm()
            ->value($model->name)
            ->required()
    !!}
    
    {!!
        widget('combo')
            ->name('currency')
            ->label(trans('payment::general.currency'))
            ->options(PaymentTools::getAvailableCurrenciesCombo())
            ->value($model->currency)
            ->inForm()
            ->required()
     !!}
    
    @if ($model->exists and ($model->isOffline() or $model->slug))
        {!!
            widget('input')
                ->label(trans('payment::general.gate.title.singular'))
                ->value(PaymentTools::getGateTitle($model->slug))
                ->inForm()
                ->disabled()
        !!}
    @else
        {!!
            widget('combo')
                ->id('select-gate')
                ->name('slug')
                ->label(trans('payment::general.gate.title.singular'))
                ->options(PaymentTools::getAvailableGatesCombo(true))
                ->inForm()
                ->help(trans('payment::general.gate.message.no-selection-hint'))
         !!}
    @endif
    
    @if ($model->exists and $model->slug)
        @include('payment::downstream.gates.fields', [
            'fields' => PaymentTools::getGateFields($model->slug),
            'gateSlug' => $model->slug,
        ])
    @else
        <div class="additional-fields"></div>
    @endif
    {{----}}
    {{--<div class="form-group">--}}
    {{--<div class="col-sm-2">--}}
    {{--{!! widget('input')->placeholder('name') !!}--}}
    {{--</div>--}}
    {{--<div class="col-sm-10">--}}
    {{--{!! widget('input')->placeholder('value') !!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{----}}
    {{--<div class="form-group">--}}
    {{--<div class="col-sm-10 col-sm-offset-2">--}}
    {{--<button type="button" class="btn btn-primary btn-block btn-outline">--}}
    {{--<i class="fa fa-plus"></i>--}}
    {{--</button>--}}
    {{--</div>--}}
    {{--</div>--}}
</div>
<div class="modal-footer">
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.save'))
            ->shape('success')
            ->value('save')
            ->type('submit')
    !!}
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click('$(".modal").modal("hide")')
    !!}
    
    {!! widget('feed') !!}
</div>
