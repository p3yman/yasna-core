{!!
    widget('modal')->target(PaymentRouteTools::action('Manage\Downstream\AccountsController@delete'))
        ->label(trans('payment::general.action.delete-something', ['something' => $model->title]))
        ->noValidation()
!!}
<div class='modal-body'>
    
    {!! widget('hidden')->name('hashid')->value($model->spreadMeta()->hashid) !!}
    
    {!! widget('input')->inForm()->value($model->title)->label(trans('validation.attributes.title'))->disabled() !!}

</div>
<div class="modal-footer">
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.delete'))
            ->shape('danger')
            ->value('delete')
            ->type('submit')
    !!}
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click('$(".modal").modal("hide")')
    !!}
    
    {!! widget('feed') !!}
</div>
