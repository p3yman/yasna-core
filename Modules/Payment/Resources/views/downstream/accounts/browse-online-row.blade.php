@php
    $canEdit = user()->as('admin')->can('payment_online_accounts.edit');
    $canDelete = user()->as('admin')->can('payment_online_accounts.delete');
@endphp

@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
	'refresh_url' => PaymentRouteTools::action('Manage\Downstream\AccountsController@row', ['account' => $model->hashid]),
])

@include('payment::downstream.accounts.browse-cell-title')

@include('payment::downstream.accounts.browse-cell-bank')

<td>
    @include("manage::widgets.grid-text" , [
        'text' => $model->slug,
        'size' => "16" ,
        'class' => "font-yekan text-bold" ,
    ])
</td>

@include('payment::downstream.accounts.browse-cell-currency')

@include('payment::downstream.accounts.browse-cell-status')

@include('payment::downstream.accounts.browse-cell-action')