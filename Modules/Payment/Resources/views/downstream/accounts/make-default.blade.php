{!!
    widget('modal')->target(PaymentRouteTools::action('Manage\Downstream\AccountsController@makeDefault'))
        ->label(trans('payment::general.action.make-default'))
        ->noValidation()
!!}
<div class='modal-body'>
    
    {!! widget('hidden')->name('hashid')->value($model->spreadMeta()->hashid) !!}
    
    {!! widget('input')->inForm()->value($model->title)->label(trans('validation.attributes.title'))->disabled() !!}

</div>
<div class="modal-footer">
    
    {!!
        widget('button')
            ->label(trans('payment::general.action.make-default'))
            ->shape('success')
            ->value('male-default')
            ->type('submit')
    !!}
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click('$(".modal").modal("hide")')
    !!}
    
    {!! widget('feed') !!}
</div>