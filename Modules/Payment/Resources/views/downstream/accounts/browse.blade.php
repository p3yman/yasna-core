@extends('manage::layouts.template')
@section('content')
    @include("manage::downstream.tabs")
    {{--@include("manage::widgets.toolbar")--}}
    
    <div class="row">
        @isset($onlineAccounts)
            <div class="col-xs-12">
                @include("manage::widgets.toolbar" , [
                    'buttons' => [
                        [
                            'target' => 'modal:'
                                . PaymentRouteTools::action('Manage\Downstream\AccountsController@createForm', [
                                    'type' => 'online'
                                ], false),
                            'type' => "success",
                            'caption' => trans('manage::forms.button.add_to')
                                . ' '
                                . trans('payment::general.account.online.plural'),
                            'icon' => "plus-circle",
                            'condition' => user()->as('admin')->can('payment_online_accounts.create'),
                        ],
                    ],
                    'title' => $page[0][1] .' / '. $page[1][1] . ' / '  . trans('payment::general.account.online.plural')
                ])
            </div>
            <div class="col-xs-12" id="online-accounts-list">
                @include('payment::downstream.accounts.browse-table-online')
            </div>
        @endisset
        @isset($offlineAccounts)
            <div class="col-xs-12">
                @include("manage::widgets.toolbar" , [
                    'buttons' => [
                        [
                            'target' => 'modal:'
                                . PaymentRouteTools::action('Manage\Downstream\AccountsController@createForm', [
                                    'type' => 'offline'
                                ], false),
                            'type' => "success",
                            'caption' => trans('manage::forms.button.add_to')
                                . ' '
                                . trans('payment::general.account.offline.plural'),
                            'icon' => "plus-circle",
                            'condition' => user()->as('admin')->can('payment_offline_accounts.create'),
                        ],
                    ],
                    'title' => $page[0][1] .' / '. $page[1][1] . ' / '  . trans('payment::general.account.offline.plural')
                ])
            </div>
            <div class="col-xs-12" id="offline-accounts-list">
                @include('payment::downstream.accounts.browse-table-offline')
            </div>
        @endisset
    </div>
@stop

@include('payment::downstream.accounts.browse-scripts')