@section('html_footer')
    <script>
        function multiGridRowUpdate(tables, hashid) {
            let tr = tables.find('tr#tr-' + hashid);
            rowUpdate(tr.closest('table').attr('id'), hashid);
        }

        String.prototype.clearDigits = function () {
            let s = ad(this.replace(/((?![\u06F0-\u06F9\u0660-\u066990-9]).)/g, ''));
            return (typeof s != 'undefined') ? s : '';
        };

        $(document).ready(function () {
            $(document).on({
                change: function () {
                    let that = $(this);
                    let parent = that.closest('.form-group');
                    let target = $('.additional-fields');
                    $.ajax({
                        url: "{{ PaymentRouteTools::action('Manage\Downstream\GatesController@fields', [
                                'gate' => '__GATE__'
                            ]) }}".replace('__GATE__', that.val()),
                        beforeSend: function () {
                            target.html('');
                            parent.addClass('disabled-box');
                        },
                        success: function (rs) {
                            target.html(rs);
                        },
                        complete: function () {
                            parent.removeClass('disabled-box');
                        }
                    })
                },
            }, '#select-gate');
            
            $(document).on({
                change: function () {
                    let type = $(this).val();
                    let needed = $('.dynamic-field.dynamic-field-' + type);
                    let notNeeded = $('.dynamic-field').not('.dynamic-field-' + type);
                    needed.slideDown();
                    needed.find(':input').prop('disabled', false);
                    notNeeded.slideUp();
                    notNeeded.find(':input').prop('disabled', true);
                }
            }, '#offline-payment-type');
            
            $(document).on({
                input: function () {
                    let that = $(this);
                    let digits = that.val().clearDigits();
                    digits = digits.replace(/(\S{4})(?!$)/g, "$1-");
                    that.val(digits.substring(0,19));
                },
            }, '#card-number')
        });
    </script>
@append