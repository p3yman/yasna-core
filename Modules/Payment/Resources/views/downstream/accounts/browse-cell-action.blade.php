{{--
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		[
		    'edit',
		    trans('payment::general.action.edit'),
		    "modal:" . PaymentRouteTools::action('Manage\Downstream\AccountsController@editForm', ['account' => '-hashid-'], false),
		    $canEdit,
        ],
		[
		    'ban',
		    trans('payment::general.action.disable'),
		    "modal:" . PaymentRouteTools::action('Manage\Downstream\AccountsController@activenessForm', ['account' => '-hashid-'], false),
		    $canEdit and $model->isDeactivable(),
        ],
		[
		    'check',
		    trans('payment::general.action.enable'),
		    "modal:" . PaymentRouteTools::action('Manage\Downstream\AccountsController@activenessForm', ['account' => '-hashid-'], false),
		    $canEdit and $model->isActivable(),
        ],
		[
		    'thumbtack',
		    trans('payment::general.action.make-default'),
		    "modal:" . PaymentRouteTools::action('Manage\Downstream\AccountsController@makeDefaultForm', ['account' => '-hashid-'], false),
		    $canEdit and $model->isNotDefault(),
        ],
		//['wheelchair-alt' , trans('manage::forms.button.seeder_cheat') , "modal:manage/posts/upstream/posttype/seeder/-hashid-"],
		[
		    'trash-o',
		    trans('manage::forms.button.delete'),
		    "modal:" . PaymentRouteTools::action('Manage\Downstream\AccountsController@deleteForm', ['account' => '-hashid-'], false),
		    $canDelete,
        ],
	]
])