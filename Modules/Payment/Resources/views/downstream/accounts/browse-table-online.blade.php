<span class="hidden refresh">{{ PaymentRouteTools::action('Manage\Downstream\AccountsController@list', ['type' => 'online']) }}</span>
@include("manage::widgets.grid" , [
    'table_id' => "tbl-accounts-online",
    'table_class' => 'tbl-accounts',
    'row_view' => "payment::downstream.accounts.browse-online-row",
    'handle' => "counter",
    'models' => $onlineAccounts,
    'headings' => [
        trans('validation.attributes.title'),
        trans('payment::general.bank.title.singular'),
        trans('payment::general.gate.title.singular'),
        trans('payment::general.currency'),
        trans('validation.attributes.status'),
        trans('manage::forms.button.action'),
    ],
])
