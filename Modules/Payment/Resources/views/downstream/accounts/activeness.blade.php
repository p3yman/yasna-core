{!!
    widget('modal')->target(PaymentRouteTools::action('Manage\Downstream\AccountsController@activeness'))
        ->labelIf($model->isPublished(), trans('payment::general.action.disable'))
        ->labelIf($model->isNotPublished(), trans('payment::general.action.enable'))
        ->noValidation()
!!}
<div class='modal-body'>
    
    {!! widget('hidden')->name('hashid')->value($model->spreadMeta()->hashid) !!}
    
    {!! widget('input')->inForm()->value($model->title)->label(trans('validation.attributes.title'))->disabled() !!}

</div>
<div class="modal-footer">
    
    {!!
        widget('button')
            ->label($model->isPublished()
                ? trans('payment::general.action.disable')
                : trans('payment::general.action.enable'))
            ->shape($model->isNotPublished() ? 'success' : 'danger')
            ->value($model->isNotPublished() ? 'enable' : 'disable')
            ->type('submit')
    !!}
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click('$(".modal").modal("hide")')
    !!}
    
    {!! widget('feed') !!}
</div>