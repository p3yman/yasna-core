{!!
    widget('modal')
        ->targetIf($model->exists, PaymentRouteTools::action('Manage\Downstream\AccountsController@editSubmit'))
        ->targetIf(!$model->exists, PaymentRouteTools::action('Manage\Downstream\AccountsController@create', [
            'type' => 'offline'
        ]))->labelIf($model->exists, trans('payment::general.action.edit-something', ['something' => $model->title]))
        ->labelIf(!$model->exists, trans('payment::general.action.add-to-something', [
            'something' => trans("payment::general.account.offline.plural")
        ]))->validation(false)
!!}
<div class='modal-body'>
    {!!
        widget('hidden')
            ->name('hashid')
            ->value($model->spreadMeta()->hashid)
            ->condition($model->exists)
    !!}
    
    {!!
        widget('input')
            ->label(trans('payment::general.type'))
            ->value($model->type ? PaymentTools::getPaymentTypeTitle($model->type) : null)
            ->inForm()
            ->disabled()
            ->condition($model->exists)
    !!}
    
    {!!
        widget('combo')
            ->name('type')
            ->id('offline-payment-type')
            ->value($model->type)
            ->label(trans('payment::general.type'))
            ->options(PaymentTools::allOfflinePaymentTypesCombo())
            ->inForm()
            ->required()
            ->condition(!$model->exists)
    !!}
    
    {!!
        widget('input')
            ->name('title')
            ->label(trans('validation.attributes.title'))
            ->inForm()
            ->value($model->title)
            ->required()
    !!}
    
    {!!
        widget('combo')
            ->name('currency')
            ->label(trans('payment::general.currency'))
            ->options(PaymentTools::getAvailableCurrenciesCombo())
            ->value($model->currency)
            ->inForm()
            ->required()
    !!}
    
    {!!
        widget('input')
            ->name('name')
            ->label(trans('payment::general.bank.name.singular'))
            ->value($model->name)
            ->container_class('dynamic-field dynamic-field-shetab dynamic-field-deposit dynamic-field-sheba')
            ->inForm()
            ->required()
            ->condition(
                !$model->exists or
                ($model->type == 'shetab') or
                ($model->type == 'deposit') or
                ($model->type == 'sheba')
            )
    !!}
    
    {!!
        widget('input')
            ->name('account_no')
            ->id('card-number')
            ->label(trans('payment::general.card.number'))
            ->value(ad(PaymentTools::cardNumberToView($model->account_no)))
            ->class('ltr')
            ->container_class('dynamic-field dynamic-field-shetab')
            ->inForm()
            ->required()
            ->condition(!$model->exists or ($model->type == 'shetab'))
    !!}
    
    {!!
        widget('input')
            ->name('account_no')
            ->label(trans('payment::general.account.number'))
            ->value($model->account_no)
            ->class('ltr')
            ->container_class('dynamic-field dynamic-field-deposit')
            ->inForm()
            ->required()
            ->condition(!$model->exists or ($model->type == 'deposit'))
    !!}
    
    {!!
        widget('input')
            ->name('account_no')
            ->label(trans('payment::general.bank.IBAN'))
            ->value($model->account_no)
            ->class('ltr')
            ->container_class('dynamic-field dynamic-field-sheba')
            ->inForm()
            ->required()
            ->condition(!$model->exists or ($model->type == 'sheba'))
    !!}
    
    {!!
        widget('input')
            ->name('owner')
            ->label(trans('payment::general.account.owner.name.singular'))
            ->value($model->owner)
            ->container_class('dynamic-field dynamic-field-shetab dynamic-field-deposit dynamic-field-sheba')
            ->inForm()
            ->required()
            ->condition(
                !$model->exists or
                ($model->type == 'shetab') or
                ($model->type == 'deposit') or
                ($model->type == 'sheba')
            )
    !!}
    
    {!!
        widget('input')
            ->name('branch_code')
            ->value($model->branch_code)
            ->label(trans('payment::general.bank.branch-code'))
            ->container_class('dynamic-field dynamic-field-deposit')
            ->inForm()
            ->condition(!$model->exists or ($model->type == 'deposit'))
    !!}
    
    {{--@todo: user widget instead of including blade--}}
    @include('manage::widgets.input-photo', [
        'name' => 'logo',
        'label' => trans('payment::general.bank.logo.singular'),
        'container' => [
            'class' => 'dynamic-field dynamic-field-shetab dynamic-field-deposit dynamic-field-sheba'
        ],
        'value' => $model->logo,
        'condition' => !$model->exists or
            ($model->type == 'shetab') or
            ($model->type == 'deposit') or
            ($model->type == 'sheba')
    ])
</div>
<div class="modal-footer">
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.save'))
            ->shape('success')
            ->value('save')
            ->type('submit')
    !!}
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click('$(".modal").modal("hide")')
    !!}
    
    {!! widget('feed') !!}
</div>

<script>
    $('#offline-payment-type').change();
</script>
