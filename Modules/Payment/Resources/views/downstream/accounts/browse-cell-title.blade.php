<td>
    @include("manage::widgets.grid-text" , [
        'text' => $model->title,
        'size' => "16" ,
        'class' => "font-yekan text-bold" ,
        'link' => $canEdit
            ? "modal:" . PaymentRouteTools::action('Manage\Downstream\AccountsController@editForm', ['account' => '-hashid-'], false)
            : "",
    ])
    @include("manage::widgets.grid-badge" , [
        'icon' => 'check',
        'condition' => $model->isDefault(),
        'text' => trans("payment::general.default"),
        'color' => 'success',
    ])
    @include("manage::widgets.grid-date" , [
        'date' => $model->created_at ,
        'default' => "fixed" ,
        'format' => 'j F Y' ,
        'color' => '#1b72e2',
    ])
    @include("manage::widgets.grid-text" , [
        'condition' => user()->isDeveloper(),
        'text' => $model->id . ' | ' . $model->hashid,
        'color' => "primary",
    ])
</td>