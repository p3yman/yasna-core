<span class="hidden refresh">{{ PaymentRouteTools::action('Manage\Downstream\AccountsController@list', ['type' => 'offline']) }}</span>
@include("manage::widgets.grid" , [
    'table_id' => "tbl-accounts-offline",
    'table_class' => 'tbl-accounts',
    'row_view' => "payment::downstream.accounts.browse-offline-row",
    'handle' => "counter",
    'models' => $offlineAccounts,
    'headings' => [
        trans('validation.attributes.title'),
        trans('payment::general.bank.title.singular'),
        trans('payment::general.type'),
        trans('payment::general.currency'),
        trans('validation.attributes.status'),
        trans('manage::forms.button.action'),
    ],
])