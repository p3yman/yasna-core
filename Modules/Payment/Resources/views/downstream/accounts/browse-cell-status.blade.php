<td>
    @php
        $statusUrl = $canEdit
            ? "modal:" . PaymentRouteTools::action(
                'Manage\Downstream\AccountsController@activenessForm',
                ['account' => '-hashid-'],
                false
            ) : '';
    @endphp
    @include("manage::widgets.grid-badge" , [
        'icon' => 'check',
        'condition' => $model->isPublished(),
        'text' => trans("payment::general.account.status.activated"),
        'link' => $statusUrl ,
        'color' => 'success',
    ])
    @include("manage::widgets.grid-badge" , [
        'icon' => 'times',
        'condition' => $model->isNotPublished(),
        'text' => trans("payment::general.account.status.deactivated"),
        'link' => $model->isActivable() ? $statusUrl : '',
        'color' => 'danger',
    ])
</td>