@if (!isset($model))
    @php $model = model('account'); @endphp
@endif
@isset($fields)
    @foreach($fields as $field => $rules)
		@php
			$widget = PaymentTools::getGateFieldWidget($gateSlug, $field)
				?? widget(PaymentTools::getGateFieldWidgetName($gateSlug, $field));
		@endphp
        {!!
            $widget
                ->name($field)
                ->label(trans("payment::validation.attributes.$field"))
                ->inForm()
                ->requiredIf(in_array('required', $rules))
                ->value($model->$field ?: $model->meta($field))
         !!}
    @endforeach
@endisset
