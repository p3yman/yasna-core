@section('html_footer')
    <script>
        function toggleFilterPanel() {
            $('#filter-panel').slideToggle();
        }
        function showFilterPanel() {
            $('#filter-panel').slideDown();
        }
        function hideFilterPanel() {
            $('#filter-panel').slideUp();
        }
    </script>
@stop