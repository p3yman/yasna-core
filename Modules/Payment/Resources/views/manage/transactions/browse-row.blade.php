@include('manage::widgets.grid-row-handle', [
    'handle'      => "counter",
])

<td>
    @include("payment::manage.transactions.browse-row-date")
</td>

<td>
    @include("payment::manage.transactions.browse-row-amount")
</td>

<td>
    @include("payment::manage.transactions.browse-row-status")
</td>

<td>
    @include("payment::manage.transactions.browse-row-description")
</td>
