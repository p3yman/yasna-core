{!!
widget("modal")
	->label(trans('payment::transaction.new'))
	->target("name:payment-new-transaction-save")
!!}

<div class="modal-body">

    {!!
    widget("text")
         ->name("amount")
         ->required()
         ->numberFormat()
         ->inForm()
    !!}

    {!!
    widget("combo")
         ->name("type")
         ->required()
         ->inForm()
         ->options([
             ["creditor", trans("payment::transaction.creditor")],
             ["debtor", trans("payment::transaction.debtor")],
         ])
         ->blankCaption(null)
         ->blankValue(0)
    !!}

    {!!
    widget("combo")
         ->options(model("account")->whereNotNull("published_at")->get())
         ->name("account")
         ->required()
         ->inForm()
         ->label(trans("payment::general.account.title.singular"))
         ->blankValue(0)
         ->valueField("hashid")
         ->blankCaption(null)
    !!}


    {!!
    widget("text")
         ->name("title")
         ->inForm()
    !!}

    {!!
    widget("textarea")
         ->name("description")
         ->inForm()
    !!}

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
