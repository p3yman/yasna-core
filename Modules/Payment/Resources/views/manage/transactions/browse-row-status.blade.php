@include("manage::widgets.grid-badge" , [
    'icon' => $model->status_icon,
    'text' => $model->status_text,
    'color' => $model->status_color,
])

@include("manage::widgets.grid-date" , [
    'date' => $model->status_date ,
    'default' => "fixed" ,
    'format' => 'j F Y' ,
])
