@include("manage::widgets.grid-text" , [
    "size" => "16",
    'text' => number_format(abs($model->amount)) . SPACE . $model->account->currency_text,
    "color" => $model->payable_amount > 0? "green" : "orange",
    "icon" => $model->payable_amount > 0? "level-up": "level-down",
])

@include("manage::widgets.grid-tiny" , [
    "icon" => "bank",
    'text' => $model->type_text . " . " . $model->account->name,
])

@include("manage::widgets.grid-tiny" , [
    'text' => trans('payment::general.invoice-number') . ": ". $model->invoice_id,
])
