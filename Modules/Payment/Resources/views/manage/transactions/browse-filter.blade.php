<div class="col-xs-12" id="filter-panel" @if(!$filter->count()) style="display: none" @endif>
    <div class="panel panel-default panel-primary">
        <div class="panel-heading">
            {{ trans('payment::general.filter') }}
            <button type="button" class="close" onclick="hideFilterPanel()">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
        </div>
        {!!
            widget('form-open')
                ->target(request()->url())
                ->method('get')
        !!}
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">
                    {!!
                        widget('input')
                            ->name('tracking_no')
                            ->label(trans('payment::general.tracking-number'))
                            ->placeholder(trans('payment::general.tracking-number'))
                            ->value($filter->get('tracking_no'))
                    !!}
                </div>
                <div class="col-sm-3">
                    {!!
                        widget('input')
                            ->name('invoice_id')
                            ->label(trans('payment::general.invoice-number'))
                            ->placeholder(trans('payment::general.invoice-number'))
                            ->value($filter->get('invoice_id'))
                    !!}
                </div>
                <div class="col-sm-3">
                    {!!
                        widget('combo')
                            ->name('status')
                            ->label(trans('payment::general.transaction.status'))
                            ->options(TransactionHandler::availableStatusesCombo(trans('payment::general.all')))
                            ->value($filter->get('status'))
                    !!}
                </div>
                <div class="col-sm-3">
                    {!!
                        widget('combo')
                            ->name('type')
                            ->label(trans('payment::general.type'))
                            ->options(PaymentTools::allPaymentTypesCombo(trans('payment::general.all')))
                            ->value($filter->get('type'))
                    !!}
                </div>
                <div class="col-sm-3">
                    {!!
                        widget('persian-date-picker')
                            ->name('date_from')
                            ->label(trans('payment::general.date-from'))
                            ->placeholder(trans('payment::general.date-from'))
                            ->value($filter->get('date_from') ?: '')
                            ->withoutTime()
                    !!}
                </div>
                <div class="col-sm-3">
                    {!!
                        widget('persian-date-picker')
                            ->name('date_to')
                            ->label(trans('payment::general.date-to'))
                            ->placeholder(trans('payment::general.date-to'))
                            ->value($filter->get('date_to') ?: '')
                            ->withoutTime()
                    !!}
                </div>
                <div class="col-sm-3">
                    {!!
                        widget("combo")
                             ->name("creditor")
                             ->label(trans('payment::transaction.type'))
                             ->options([
                                 ["all" , trans("payment::general.all")],
                                 ["creditor", trans("payment::transaction.creditor")],
                                 ["debtor", trans("payment::transaction.debtor")],
                             ])
                             ->value($filter->get('creditor'))
                    !!}
                </div>
                <div class="col-sm-3">
                    {!!
                        widget('input')
                            ->name('payer')
                            ->label(trans('payment::general.payer'))
                            ->placeholder(trans('payment::general.payer'))
                            ->value($filter->get('payer'))
                    !!}
                </div>
            </div>
        </div>
        
        <div class="modal-footer">
            <button class="btn btn-primary pull-end btn-taha">{{ trans('payment::general.filter') }}</button>
            {!!
                widget('link')
                    ->target(request()->url())
                    ->label(trans('payment::general.flush-filters'))
                    ->class('btn btn-warning btn-taha')
                    ->condition($filter->count())
            !!}
        </div>
        
        {!!
            widget('form-close')
        !!}
    </div>
</div>