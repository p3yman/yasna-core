@include("manage::widgets.grid-text" , [
    'text' => $model->title,
])

@include("manage::widgets.grid-text" , [
    "condition" => $model->description,
    "size" => "10",
    'text' => $model->description,
])

@include("manage::widgets.grid-tiny" , [
    'condition' => user()->isDeveloper(),
    'text' => $model->id . ' | ' . $model->hashid,
    "icon" => "bug",
    'color' => "gray",
])


