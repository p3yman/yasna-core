@extends('manage::layouts.template')

@section('content')
    {{--
    |--------------------------------------------------------------------------
    | Toolbar
    |--------------------------------------------------------------------------
    |
    --}}
    <div class="col-xs-12">
        @include("manage::widgets.toolbar" , [
            'buttons' => [
                [
                    'caption' => trans('payment::general.filter'),
                    'icon' => "filter",
                    'target' => 'toggleFilterPanel()',
                    "class" => "btn-taha",
                ],
                [
                    'type' => "info",
                    'caption' => trans('payment::general.export.excel'),
                    'icon' => "file-excel-o",
                    'target' => PaymentRouteTools::action('Manage\TransactionController@export')
                        . (($queryString = request()->getQueryString()) ? ('?' . $queryString) : ''),
                    'condition' => $models->count() and user()->as('admin')->can('payment_transactions.export'),
                    "class" => "btn-taha",
                ],
                [
                    'caption' => trans('payment::transaction.new'),
                    'icon' => "plus",
                    "type" => "primary", 
                    "target" => "masterModal('".route("payment-new-transaction")."')",
                    "class" => "btn-taha",
                ],
            ],
            'title' => $page[0][1]
        ])
    </div>
    {{--
    |--------------------------------------------------------------------------
    | Filter
    |--------------------------------------------------------------------------
    |
    --}}
    @include('payment::manage.transactions.browse-filter')

    {{--
    |--------------------------------------------------------------------------
    | Grid
    |--------------------------------------------------------------------------
    |
    --}}
    <div class="col-xs-12" id="online-accounts-list">
        @include("manage::widgets.grid" , [
            'table_id' => "tblPosts",
            'row_view' => "payment::manage.transactions.browse-row",
            'handle' => "counter",
            'headings' => [
                trans("validation.attributes.date"),
                trans('payment::general.amount.singular'),
                trans('payment::general.transaction.status'),
                [trans("validation.attributes.description"),250]
            ],
        ])
    </div>
@stop

@include('payment::manage.transactions.scripts')
