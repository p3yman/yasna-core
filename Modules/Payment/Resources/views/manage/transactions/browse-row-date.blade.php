@include("manage::widgets.grid-text" , [
    "size" => "16",
    'text' => jDate::forge($model->created_at)->format('j F Y [H:i]'),
])

@include("manage::widgets.grid-tiny" , [
    'text' => trans('payment::general.tracking-number') . ": ". $model->tracking_no,
])

@include("manage::widgets.grid-tiny" , [
    'text' => trans('payment::general.reference-number') . ": ". $model->bank_reference_no,
])

