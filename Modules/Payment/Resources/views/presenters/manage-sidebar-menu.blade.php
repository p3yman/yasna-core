@include('manage::layouts.sidebar-link' , [
    'icon' => "credit-card",
    'caption' => trans('payment::general.transaction.title.plural') ,
    'link' => str_after(PaymentRouteTools::action('Manage\TransactionController@index', [], false), 'manage/') ,
])