<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists('payment')) {
    function payment()
    {
        return new Modules\Payment\Services\PaymentHandler\PaymentHandler();
    }
}
if (!function_exists('readRaw')) {

    function readRaw(\Illuminate\Database\Eloquent\Builder $builder)
    {
        $sql      = $builder->toSql();
        $bindings = $builder->getBindings();

        foreach ($bindings as $val) {
            $sql = str_replace_first('?', is_numeric($val) ? $val : "'$val'", $sql);
        }

        return $sql;
    }
}