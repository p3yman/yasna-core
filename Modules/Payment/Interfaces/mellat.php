<?php
/**
 * Created by PhpStorm.
 * User: Yasna-PC3
 * Date: 9/20/2017
 * Time: 4:18 PM
 */

namespace Modules\Payment\Interfaces;

class mellat implements iBank
{

    /**
     * Redirect user to gateway for payment
     */
    public function redirectToGateway($redirectUrl, $tracking)
    {
        // TODO: Implement redirectToGateway() method.
    }

    /**
     * Check all of parameters back from gateway
     */
    public function checkParams($request)
    {
        // TODO: Implement checkParams() method.
    }

    public function backFromGateway($bank, $transaction, $request)
    {
        // TODO: Implement backFromGateway() method.
    }

    /**
     * Process all of steps for verify payment
     */
    public function verifyPayment($trackingNumber)
    {
        // TODO: Implement verifyPayment() method.
    }

    /**
     * all of Process which refund a payment
     */
    public function refundPayment($trackingNumber)
    {
        // TODO: Implement refundPayment() method.
    }

    /**
     * all of Process which reverse a payment
     */
    public function reversePayment($trackingNumber)
    {
        // TODO: Implement reversePayment() method.
    }
}
