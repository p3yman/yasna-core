<?php
/**
 * Created by PhpStorm.
 * User: jafar
 * Date: 9/11/17
 * Time: 17:35
 */

namespace Modules\Payment\Interfaces;

use App\Entities\Transaction;
use Carbon\Carbon;
use SoapClient;

class Saman implements iBank
{
    protected $merchant='10002890';

    /**
     * Redirect transaction to gateway
     *
     * @param string $redirectUrl , $trackingNumber
     */
    public function redirectToGateway($redirectUrl, $trackingNumber)
    {
        $trans = Transaction::whereTracking_no($trackingNumber)->first();
        //dd($trans);
        if (!$trans->isPaid()) {
            $trans->update([
                'paid_at'=>Carbon::now()->toDateTimeString()
            ]);
            echo view('payment::saman_redirector')
                ->with([
                    'amount' =>$trans->payable_amount,
                    'merchant' => $this->merchant,
                    'resNum' => $trackingNumber,
                    'callBackUrl' => $redirectUrl
                ]);
        }
    }

    /**
     * Check all of parameters which back from gateway and test not null them
     */
    public function checkParams($request)
    {
        if ($this->existState($request)) { //if State is exist, it is True
            if ($this->existResnum($request) && $this->existRefnum($request)) { //if ResNum and RefNum are exist, it is True
                return true;
            } elseif ($this->existResnum($request) && !$this->existRefnum($request)) { //if ResNum is exist and RefNum is not exist, it is False
                $trans = Transaction::whereTracking_no($request->ResNum)->first();
                $trans->update([
                    'rejected_at'=>Carbon::now()->toDateTimeString(),
                    'rejected_reason'=> 'not received Refnum'
                ]);
                return false;
            } elseif (!$this->existResnum($request) && $this->existRefnum($request)) { //if ResNum is exist and RefNum is exist, it is False
                return false;
            } elseif (!$this->existResnum($request) && !$this->existRefnum($request)) { //if ResNum and RefNum are not exist, it is False
                return false;
            }
        } else { // if State is not exist, update is done and return false
            $trans = Transaction::whereTracking_no($request->ResNum)->first();
            $trans->update([
                'rejected_at'=>Carbon::now()->toDateTimeString(),
                'rejected_reason'=> 'not received state'
            ]);
            return false;
        }
    }

    /**
     * first check all of back parameters and then necessary call verify payment method
     *
     * @param string $redirectUrl , $trackingNumber
     *
     * @return redirect
     */
    public function backFromGateway($trackingNumber, $request)
    {
        //$tracking=hashid($trackingNumber);
        $transaction = Transaction::whereTracking_no($trackingNumber)->first();

        if ($this->checkParams($request)) {
            if ($this->checkRefnum($request)) {
                $transaction->update([
                    'reference_no'=> $request->RefNum
                ]);

                if ($this->checkResnum($trackingNumber, $request)) {
                    $transaction->update([
                        'restful_no'=>$request->ResNum,
                        'online_payment_status'=>$request->State
                    ]);

                    if (Transaction::checkAutoVerify($trackingNumber)) {
                        if ($this->verifyPayment($trackingNumber)) {
                            return redirect('http://'.$transaction->callback_url);
                        } else {
                            return redirect('http://'.$transaction->callback_url);
                            //return false;
                        }
                    } else {
                        $transaction->update([
                                    'online_payment_status'=>$request->State
                                ]);
                        return redirect('http://'.$transaction->callback_url);
                    }
                } else {
                    $transaction->update([
                        'rejected_reason'=>'no match resnum',
                        'rejected_at' => Carbon::now()->toDateTimeString(),
                        'rejected_by'=> intval(2)
                    ]);
                    return false;
                }
            } else {
                $transaction->update([
                    'rejected_reason'=>'duplicate refnum',
                      'rejected_at' => Carbon::now()->toDateTimeString(),
                      'rejected_by'=>intval(2)
                  ]);
                return false;
            }
        } else {
            $transaction->update([
                'rejected_reason'=> 'null parameters from bank',
                'rejected_by'=> intval(2),
                'rejected_at'=>Carbon::now()->toDateTimeString()
            ]);
            return redirect('http://'.$transaction->callback_url);
        }
    }


    /**
     * verify payment with tracking number and update necessary fields
     *
     * @param string $trackingNumber
     *
     * @return bool
     */
    public function verifyPayment($trackingNumber)
    {
        $transaction = Transaction::where('tracking_no', $trackingNumber)->first();

        if ((!$transaction->isVerified()) && ($transaction->auto_verify == 1)) { // if this transaction is not verified previously and is auto verify,it is True

            $wsdlURL = "https://sep.shaparak.ir/payments/referencepayment.asmx?WSDL";

            $soap = new SoapClient($wsdlURL);

            $result = $soap->VerifyTransaction($transaction->reference_no, $this->merchant);

            if ($result > 0) {
                if ($transaction->payable_amount == $result) {
                    $transaction->update([
                        'verified_at' => Carbon::now()->toDateTimeString(),
                        'verified_by' => intval(2),
                        'verified_amount'=>$result,
                        'paid_amount'=>$result
                    ]);
                    return true;
                } else {
                    $transaction->update([
                        'rejected_reason' => 'unsuccess verify',
                        'rejected_at'=>Carbon::now()->toDateTimeString(),
                        'rejected_by'=>intval(2)
                    ]);
                    return false;
                }
                $transaction->update([
                    'verified_at' => Carbon::now()->toDateTimeString(),
                    'verified_by' => intval(2),
                    'paid_amount'=>$result
                ]);
            } else {
                $transaction->update([
                    'online_payment_status' => $result,
                    'rejected_at'=> Carbon::now()->toDateTimeString(),
                    'rejected_reason'=>'result is negative',
                    'rejected_by'=>intval(2)
                ]);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * verify transaction which is not auto verify
     *
     * @param $trackingNumber
     *
     * @return redirect
     */
    public function manualVerify($trackingNumber)
    {
        $transaction = Transaction::where('tracking_no', $trackingNumber)->first();

        if (($transaction->isPending()) && ($transaction->auto_verify == 0 && $transaction->online_payment_status == 'ok' && $transaction->reference_no)) {
            if ($this->verifyPayment($transaction->tracking_no)) {
                dd('yes done');
            } else {
                dd('not done');
            }
        } else {
            dd('ref nadarim');
        }
    }

    /**
     * there isn't this method in Saman gateway and so, return false
     *
     * @param string  $trackingNumber
     *
     * @return bool
     */
    public function refundPayment($trackingNumber) //No needs to update any field
    {
        return false;
    }

    /**
     * reverse a transaction by tracking number
     *
     * @param string $trackingNumber
     *
     * @return bool
     */
    public function reversePayment($trackingNumber)
    {
        $transaction = $this->findByTrackingNo($trackingNumber);
        if (!$transaction->isRejected()) {
            $client = new SoapClient('https://sep.shaparak.ir/payments/referencepayment1.asmx?WSDL');

            $result = $client->verifyTransaction([
                'RefNum' => $transaction->RefNum,
                'MID' => $this->merchant,
                'Username' => $this->username,
                'Password' => $this->password
            ]);

            if ($result == 1) {
                $transaction->update([
                    'rejected_at' => Carbon::now()->toDateTimeString(),
                    'rejected_by' => intval(2)
                ]);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     |----------------------------------------------
     |  Other Methods in Saman Gateway
     |----------------------------------------------
    */

    /**
     * check the state of transaction and if OK , it shows a successfully transaction and if this is not OK, it shows failed transaction
     *
     * @param string $trackingNumber
     *
     * @return bool
     */
    public function checkState($value)
    {
        if ($value->State == 'OK') {
            return true;
        } elseif ($value->State == 'Cancelled By User') {
            return 'cancelled';
        } else {
            return false;
        }
    }

    /**
     * check the RefNum of transaction and if there is reference_no for this transaction, return True
     *
     * @param string $trackingNumber
     *
     * @return bool
     */
    public function checkRefnum($transaction)
    {
        $trans = Transaction::whereReference_no($transaction->RefNum)->first();

        if (!$trans) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check the ResNum of transaction and if there is restful_no for this transaction, return True
     *
     * @param string $tracking , $request
     *
     * @return bool
     */
    public function checkResnum($tracking, $request)
    {
        if ($tracking == $request->ResNum) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check the existence of State of transaction and if there is State for this transaction, return True
     *
     * @param string  $request
     *
     * @return bool
     */
    public function existState($request)
    {
        if (isset($request->State)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check the existence of RefNum of transaction and if there is RefNum for this transaction, return True
     *
     * @param string  $request
     *
     * @return bool
     */
    public function existRefnum($request)
    {
        if (isset($request->RefNum) && strlen($request->RefNum)==30) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * check the existence of ResNum of transaction and if there is ResNum for this transaction, return True
     *
     * @param string  $request
     *
     * @return bool
     */
    public function existResnum($request)
    {
        if (isset($request->ResNum)) {
            return true;
        } else {
            return false;
        }
    }
}
