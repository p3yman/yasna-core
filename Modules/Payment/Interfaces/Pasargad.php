<?php
/**
 * Created by PhpStorm.
 * User: Yasna-PC3
 * Date: 9/13/2017
 * Time: 3:42 PM
 */

namespace Modules\Payment\Interfaces;

class Pasargad implements iBank
{
    public function folan($name)
    {
        // TODO: Implement folan() method.
    }

    /**
     * Redirect user to gateway for payment
     */
    public function redirectToGateway()
    {
        echo view('payment::pasargad_redirector');
        //->with([
        //'amount' => $this->amount,
        //'merchant' => $this->config->get('gateway.saman.merchant'),
        //'resNum' => $this->transactionId(),
        //'callBackUrl' => $this->getCallback()
        //]);
    }

    /**
     * Back user from gateway for more process
     */
    public function backFromGateway()
    {
        // TODO: Implement backFromGateway() method.
    }

    /**
     * Check all of parameters back from gateway
     */
    public function checkParams()
    {
        // TODO: Implement checkParams() method.
    }

    /**
     * Process all of steps for verify payment
     */
    public function verifyPayment()
    {
        // TODO: Implement verifyPayment() method.
    }

    /**
     * all of Process which refund a payment
     */
    public function refundPayment()
    {
        // TODO: Implement refundPayment() method.
    }

    /**
     * all of Process which reverse a payment
     */
    public function reversePayment()
    {
        // TODO: Implement reversePayment() method.
    }
}
