<?php
namespace Modules\Payment\Interfaces;

interface iBank
{

    /**
     * Redirect user to gateway for payment
     */
    public function redirectToGateway($redirectUrl, $tracking);

    /**
     * Check all of parameters back from gateway
     */
    public function checkParams($request);


    public function backFromGateway($transaction, $request);

    /**
     * Process all of steps for verify payment(for auto verify transaction)
     */
    public function verifyPayment($trackingNumber);

    /**
     * Process all of steps for verify payment(for manual verify transaction)
     */
    public function manualVerify($trackingNumber);

    /**
     * all of Process which refund a payment
     */
    public function refundPayment($trackingNumber);

    /**
     * all of Process which reverse a payment
     */
    public function reversePayment($trackingNumber);
}
