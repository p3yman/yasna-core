<?php

namespace Modules\Payment\Http\Controllers\V1;

use App\Models\Account;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class AccountListController extends YasnaApiController
{
    /**
     * The action to get the accounts' list.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function list(SimpleYasnaRequest $request)
    {
        $builder = $this->generateBuilder($request);

        return $this->success(
             $this->mapAllAccounts($builder->get())
        );
    }



    /**
     * Generates the builder to select the accounts.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return Builder
     */
    protected function generateBuilder(SimpleYasnaRequest $request)
    {
        return model('account')
             ->elector($this->electorArray($request))
             ->orderBy('order')
             ;
    }



    /**
     * Returns the array to be passed to the elector to select the accounts.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    protected function electorArray(SimpleYasnaRequest $request)
    {
        return [
             'actives_only' => ($request->actives_only ?? true),
             'type'         => $request->type,
        ];

    }



    /**
     * Maps all found accounts to a proper array.
     *
     * @param Collection $collection
     *
     * @return array
     */
    protected function mapAllAccounts(Collection $collection)
    {
        return $collection
             // group the accounts by their types
             ->groupBy('type')
             // map to show items of each type
             ->map(function (Collection $type_accounts) {
                 return $this->mapTypeAccounts($type_accounts);
             })
             // sort by their types and in order of the available types.
             ->sortBy(function ($value, $key) {
                 return array_search($key, PaymentToolsServiceProvider::allPaymentTypes());
             })
             ->toArray()
             ;
    }



    /**
     * Maps accounts of a type to to a proper array.
     *
     * @param Collection $collection
     *
     * @return array
     */
    protected function mapTypeAccounts(Collection $collection)
    {
        return $collection
             ->map(function (Account $account) {
                 return $account->toListResource();
             })
             ->toArray()
             ;
    }
}
