<?php

namespace Modules\Payment\Http\Controllers\V1;

use Modules\Payment\Http\Requests\V1\TransactionSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class TransactionSaveController extends YasnaApiController
{
    /**
     * Saves a new transaction.
     *
     * @param TransactionSaveRequest $request
     *
     * @return array
     */
    public function save(TransactionSaveRequest $request)
    {
        $tracking_number = payment()
             ->transaction()
             ->accountId($request->account_id)
             ->callbackUrl($request->callback_url)
             ->invoiceModel($request->invoice_model)
             ->invoiceId($request->invoice_id)
             ->payableAmount($request->payable_amount)
             ->getTrackingNumber()
        ;

        $model = payment()->transaction()->trackingNumber($tracking_number)->getModel();

        return $this->modelSaveFeedback($model);
    }
}
