<?php

namespace Modules\Payment\Http\Controllers\V1;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Payment\Services\PaymentHandler\Search\SearchBuilder;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction as TransactionHandler;

class TransactionListController extends YasnaApiController
{
    /**
     * The Request in User
     *
     * @var SimpleYasnaRequest
     */
    protected $request;



    /**
     * Returns a list of the transactions in a white-house format.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(SimpleYasnaRequest $request)
    {
        $this->request = $request;

        $builder = $this->generateTransactionsBuilder();

        if ($request->isPaginated()) {
            return $this->listPaginated($builder);
        }

        return $this->listFlat($builder);
    }



    /**
     * Generates the builder to select the transactions for the result.
     *
     * @return SearchBuilder
     */
    protected function generateTransactionsBuilder()
    {
        $builder = payment()
             ->search()
             ->orderBy('created_at', 'desc')
        ;

        return $this->runFilters(
             $builder
        );
    }



    /**
     * List by Pagination.
     *
     * @param SearchBuilder $builder
     *
     * @return array
     */
    protected function listPaginated(SearchBuilder $builder)
    {
        $paginator = $builder->paginate($this->request->perPage());
        $result    = $this->mappedArray($paginator);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * List All Together
     *
     * @param SearchBuilder $builder
     *
     * @return array
     */
    protected function listFlat(SearchBuilder $builder)
    {
        $result = $builder->get();

        $meta = [
             "total" => $result->count(),
        ];

        $data = $this->mappedArray($result);

        return $this->success($data, $meta);
    }



    /**
     * Maps the given collection and returns the mapped array.
     *
     * @param Collection|LengthAwarePaginator $collection
     *
     * @return array
     */
    protected function mappedArray($collection)
    {
        return $collection
             ->map(function (Transaction $transaction) {
                 return $transaction->toListResource();
             })->toArray()
             ;
    }



    /**
     * Runs all the filter methods to modify the builder and generate the result.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function runFilters(SearchBuilder $builder)
    {
        $modifiers = $this->filterMethods();

        foreach ($modifiers as $modifier) {
            $builder = $this->$modifier($builder);
        }

        return $builder;
    }



    /**
     * Returns an array of the methods which their names end with `Filter`.
     *
     * @return array
     */
    protected function filterMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 return ends_with($method, 'Filter');
             })
             ->values()
             ->toArray()
             ;
    }



    /**
     * Applies the tracking number filter if needed.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function trackingNumberFilter(SearchBuilder $builder): SearchBuilder
    {
        if ($this->request->tracking_no) {
            $builder->where('tracking_no', ed($this->request->tracking_no));
        }

        return $builder;
    }



    /**
     * Applies the status filter if needed.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function statusFilter(SearchBuilder $builder): SearchBuilder
    {
        $status = $this->request->status;

        if ($status and TransactionHandler::statusIsValid($status)) {
            $builder->status($status);
        }

        return $builder;
    }



    /**
     * Applies the type filter if needed.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function typeFilter(SearchBuilder $builder): SearchBuilder
    {
        $type = $this->request->type;

        if ($type and PaymentToolsServiceProvider::isAcceptablePaymentType($type)) {
            $builder->$type();
        }

        return $builder;
    }



    /**
     * Applies the datetime from filter if needed.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function dateFromFilter(SearchBuilder $builder): SearchBuilder
    {
        $timestamp = $this->request->datetime_from;

        if ($this->isValidTimestamp($timestamp)) {
            $carbon = Carbon::createFromTimestamp($timestamp);

            $builder->createdAfter($carbon);
        }

        return $builder;
    }



    /**
     * Applies the datetime to filter if needed.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function dateToFilter(SearchBuilder $builder): SearchBuilder
    {
        $timestamp = $this->request->datetime_to;

        if ($this->isValidTimestamp($timestamp)) {
            $carbon = Carbon::createFromTimestamp($timestamp);

            $builder->createdAfter($carbon);
        }

        return $builder;
    }



    /**
     * Checks if the specified value is a valid unix timestamp.
     *
     * @param int|string|null $value
     *
     * @return bool
     */
    protected function isValidTimestamp($value): bool
    {
        if (!$value) {
            return false;
        }

        if (!is_string($value)) {
            $value = (string)$value;
        }


        return (
             ((string)(int)$value === $value)
             and
             ($value <= PHP_INT_MAX)
             and
             ($value >= ~PHP_INT_MAX)
        );
    }



    /**
     * Applies the bank name filter.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function bankNameFilter(SearchBuilder $builder): SearchBuilder
    {
        $bank_name = $this->request->bank_name;

        if ($bank_name) {
            $builder->where('accounts.name', 'like', '%' . $bank_name . '%');
        }

        return $builder;
    }



    /**
     * Applies user id filter.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function userFilter(SearchBuilder $builder): SearchBuilder
    {
        $user_hashid = $this->request->user;

        if (!$user_hashid) {
            return $builder;
        }

        $user_id = hashid($user_hashid);

        if (!is_int($user_id)) {
            return $builder;
        }

        $builder->where('user_id', $user_id);
        return $builder;
    }



    /**
     * Applies invoice id filter.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function invoiceIdFilter(SearchBuilder $builder): SearchBuilder
    {
        $invoice_hashid = $this->request->invoice_id;

        if (!$invoice_hashid) {
            return $builder;
        }

        $invoice_id = hashid($invoice_hashid);

        if (!is_int($invoice_id)) {
            return $builder;
        }

        $builder->where('invoice_id', $invoice_id);
        return $builder;
    }



    /**
     * Applies invoice model filter.
     *
     * @param SearchBuilder $builder
     *
     * @return SearchBuilder
     */
    protected function invoiceModelFilter(SearchBuilder $builder): SearchBuilder
    {
        $invoice_model = $this->request->invoice_model;

        if ($invoice_model) {
            $builder->where('invoice_model', $invoice_model);
        }

        return $builder;
    }
}
