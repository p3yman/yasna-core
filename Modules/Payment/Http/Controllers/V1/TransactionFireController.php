<?php

namespace Modules\Payment\Http\Controllers\V1;

use Modules\Payment\Http\Requests\V1\TransactionFireRequest;
use Modules\Yasna\Services\YasnaApiController;

class TransactionFireController extends YasnaApiController
{
    /**
     * Returns the need to fire a transaction by the front side.
     *
     * @param TransactionFireRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function fire(TransactionFireRequest $request)
    {
        $tracking_number = $request->model->tracking_no;
        $transaction     = payment()->transaction()->trackingNumber($tracking_number);
        $fire_result     = $transaction->fireForEndpoint();
        $result_code     = ($fire_result['code'] ?? 1);

        if ($result_code < 1) {
            return $this->clientError('payment-10001');
        }

        return $this->success($fire_result, ['hashid' => $request->model_hashid]);
    }
}
