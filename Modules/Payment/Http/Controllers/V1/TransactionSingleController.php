<?php

namespace Modules\Payment\Http\Controllers\V1;

use Modules\Payment\Http\Requests\V1\TransactionDetailRequest;
use Modules\Payment\Http\Requests\V1\TransactionPreviewRequest;
use Modules\Yasna\Services\YasnaApiController;

class TransactionSingleController extends YasnaApiController
{
    /**
     * Returns the single resource of a requested transaction.
     *
     * @param TransactionPreviewRequest $request
     *
     * @return array
     */
    public function preview(TransactionPreviewRequest $request)
    {
        return $this->success(
             $request->model->toSingleResource()
        );
    }



    /**
     * get the details of a transaction with her tracking number, solely to the owner of the transaction.
     *
     * @param TransactionDetailRequest $request
     *
     * @return array
     */
    public function track(TransactionDetailRequest $request)
    {
        $result = $request->model->toResource($request->including, $request->excluding);
        $meta = [
             "tracking_no" => $request->tracking_no,
        ];

        return $this->success($result, $meta);
    }
}
