<?php

namespace Modules\Payment\Http\Controllers\V1;

use Modules\Payment\Http\Requests\V1\AccountFormRequest;
use Modules\Payment\Http\Requests\V1\AccountSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class AccountSaveController extends YasnaApiController
{
    /**
     * The action to save a new or update an existing account.
     *
     * @param AccountSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(AccountSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * The actions to read the form items while saving an account.
     *
     * @param AccountFormRequest $request
     *
     * @return array
     */
    public function form(AccountFormRequest $request)
    {
        return $this->success($request->usableModel()->renderFormItems());
    }
}
