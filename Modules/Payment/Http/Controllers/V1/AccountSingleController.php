<?php

namespace Modules\Payment\Http\Controllers\V1;

use Modules\Payment\Http\Requests\V1\AccountActivateRequest;
use Modules\Payment\Http\Requests\V1\AccountDeactivateRequest;
use Modules\Payment\Http\Requests\V1\AccountDeleteRequest;
use Modules\Payment\Http\Requests\V1\AccountMakeDefaultRequest;
use Modules\Payment\Http\Requests\V1\AccountPreviewRequest;
use Modules\Yasna\Services\YasnaApiController;

class AccountSingleController extends YasnaApiController
{
    /**
     * Returns the single resource of the requested account.
     *
     * @param AccountPreviewRequest $request
     *
     * @return array
     */
    public function preview(AccountPreviewRequest $request)
    {
        return $this->success(
             $request->model->toSingleResource()
        );
    }



    /**
     * Deletes the specified account.
     *
     * @param AccountDeleteRequest $request
     *
     * @return array
     */
    public function delete(AccountDeleteRequest $request)
    {
        $done   = intval($request->model->delete());
        $hashid = $request->model_hashid;

        return $this->success(compact('done'), compact('hashid'));
    }



    /**
     * Activates the requested account.
     *
     * @param AccountActivateRequest $request
     *
     * @return array
     */
    public function activate(AccountActivateRequest $request)
    {
        $hashid = $request->model_hashid;
        $done   = $request->model->activate();

        return $this->success(compact('done'), compact('hashid'));
    }



    /**
     * Deactivates the requested account.
     *
     * @param AccountDeactivateRequest $request
     *
     * @return array
     */
    public function deactivate(AccountDeactivateRequest $request)
    {
        $hashid = $request->model_hashid;
        $done   = $request->model->deactivate();

        return $this->success(compact('done'), compact('hashid'));
    }



    /**
     * Marks the requested account as the default account in its type.
     *
     * @param AccountMakeDefaultRequest $request
     *
     * @return array
     */
    public function makeDefault(AccountMakeDefaultRequest $request)
    {
        $hashid = $request->model_hashid;
        $done   = $request->model->makeDefault();

        return $this->success(compact('done'), compact('hashid'));
    }
}
