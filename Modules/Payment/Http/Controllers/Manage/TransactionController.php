<?php

namespace Modules\Payment\Http\Controllers\Manage;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Payment\Http\Requests\NewTransactionRequest;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;
use Modules\Yasna\Providers\ValidationServiceProvider;
use Morilog\Jalali\Facades\jDateTime;

class TransactionController extends ManageAbstractController
{
    protected $variables = [];



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if ($this->hasNotPermission('payment_transactions.browse')) {
            return $this->abort(403);
        }

        $this->variables['page'] = [
             [str_after(request()->url(), 'manage/'), trans('payment::general.transaction.title.plural')],
        ];

        $this->selectTransactions();
        return view('payment::manage.transactions.index', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function export()
    {
        if ($this->hasNotPermission('payment_transactions.export')) {
            return $this->abort(403);
        }

        $this->selectTransactions(false);
        return Excel::create($this->getExportFileName(), function ($excel) {
            $excel->sheet('sheet', function ($sheet) {
                $sheet->appendRow([
                     '#',
                     trans('payment::general.tracking-number'),
                     trans('payment::general.invoice-number'),
                     trans('payment::general.amount.singular'),
                     trans('payment::general.currency'),
                     trans('payment::general.date-of-creation'),
                     trans('payment::general.time-of-creation'),
                     trans('payment::general.transaction.status'),
                     trans('payment::general.date-of-last-status-change'),
                     trans('payment::general.type'),
                     trans('payment::general.bank.title.singular'),
                     trans('payment::general.payer'),
                     trans('payment::general.reference-number'),
                ]);

                foreach ($this->variables['models'] as $key => $model) {
                    $sheet->appendRow([
                         $key + 1,
                         $model->tracking_no,
                         $model->invoice_id,
                         $model->amount,
                         $model->account->currency_text,
                         echoDate($model->created_at, 'Y/m/d'),
                         echoDate($model->created_at, 'H:i:s'),
                         $model->status_text,
                         echoDate($model->status_date, 'Y/m/d'),
                         $model->type_text,
                         $model->account->name,
                         $model->user->full_name,
                         $model->bank_reference_no,
                    ]);
                }
            });
        })->download('xls')
             ;
    }



    /**
     * get the new form
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function newForm()
    {
        return $this->safeView("payment::manage.transactions.new", []);
    }



    /**
     * save the new manual transaction
     *
     * @param NewTransactionRequest $request
     *
     * @return string
     */
    public function newSave(NewTransactionRequest $request)
    {
        $code = payment()
             ->transaction()
             ->accountId($request->account)
             ->callbackUrl(url(''))
             ->payableAmount($request->amount)
             ->title($request->title)
             ->description($request->description)
             ->invoiceId(0)
             ->getTrackingNumber()
        ;

        if(!is_numeric($code)) {
            return $code;
        }

        payment()->transaction()->trackingNumber($code)->makeVerified();

        return $this->jsonAjaxSaveFeedback(is_numeric($code), [
             'success_refresh' => true,
        ]);
    }



    /**
     * @return string
     */
    protected function getExportFileName()
    {
        return 'transactions-' . date('Ymd-His');
    }



    /**
     * @param bool $paginate
     *
     * @return $this
     */
    protected function selectTransactions($paginate = true)
    {
        $this->variables['models'] = payment()
             ->search()
             ->orderBy('created_at', 'desc')
        ;

        return $this->transactionFilter($paginate);
    }



    /**
     * @param $paginate
     *
     * @return $this
     */
    protected function transactionFilter($paginate)
    {
        $this->variables['filter'] = collect([]);
        return $this->transactionTrackingNoFilter()
                    ->transactionInvoiceIdFilter()
                    ->transactionStatusFilter()
                    ->transactionTypeFilter()
                    ->transactionDateFilter()
                    ->transactionCreditorFilter()
                    ->transactionBankFilter()
                    ->transactionPayerFilter()
                    ->transactionEndFilter($paginate)
             ;
    }



    /**
     * @return $this
     */
    protected function transactionTrackingNoFilter()
    {
        if (request()->has('tracking_no') and ($trNo = request()->get('tracking_no'))) {
            $this->variables['models']->where('tracking_no', ed($trNo));
            $this->variables['filter']->put('tracking_no', $trNo);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function transactionInvoiceIdFilter()
    {
        if (request()->has('invoice_id') and ($invId = request()->get('invoice_id'))) {
            $this->variables['models']->invoice(ed($invId));
            $this->variables['filter']->put('invoice_id', $invId);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function transactionStatusFilter()
    {
        if (
             request()->has('status') and
             ($status = request()->get('status')) and
             Transaction::statusIsValid($status)
        ) {
            $this->variables['models']->status($status);
            $this->variables['filter']->put('status', $status);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function transactionTypeFilter()
    {
        if (request()->has('type') and
             ($type = request()->get('type')) and
             PaymentToolsServiceProvider::isAcceptablePaymentType($type)
        ) {
            $this->variables['models']->$type();
            $this->variables['filter']->put('type', $type);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function transactionDateFilter()
    {
        return $this->applyDateFrom()->applyDateTo();
    }



    /**
     * @return $this
     */
    protected function applyDateFrom()
    {
        $milliseconds = request()->get('date_from');
        if (!$milliseconds) {
            return $this;
        }
        $carbon = $this->createCarbon($milliseconds);
        $this->variables['models']->createdAfter($carbon->startOfDay());
        $this->variables['filter']->put('date_from', $carbon->format('Y/m/d'));
        return $this;
    }



    /**
     * @return $this
     */
    protected function applyDateTo()
    {
        $milliseconds = request()->get('date_to');
        if (!$milliseconds) {
            return $this;
        }
        $carbon = $this->createCarbon($milliseconds);
        $this->variables['models']->createdBefore($carbon->endOfDay());
        $this->variables['filter']->put('date_to', $carbon->format('Y/m/d'));
        return $this;
    }



    /**
     * @param $milliseconds
     *
     * @return Carbon
     */
    protected function createCarbon($milliseconds)
    {
        return Carbon::parse($this->purifyDate($milliseconds));
    }



    /**
     * @param $milliseconds
     *
     * @return string|null
     */
    protected function purifyDate($milliseconds)
    {
        $purified_data = ValidationServiceProvider::purifier(['date' => $milliseconds], ['date' => 'date']);
        return $purified_data['date'];
    }



    /**
     * perform the creditor filter on transaction list
     *
     * @return TransactionController
     */
    protected function transactionCreditorFilter()
    {
        if (request('creditor')=="all"){
            return $this;
        }

        if (request('creditor')=="creditor") {
            $this->variables['models']->where('payable_amount' , '>', 0);
            $this->variables['filter']->put('creditor', 'creditor');
        }

        if (request('creditor')=='debtor'){
            $this->variables['models']->where('payable_amount' , '<', 0);
            $this->variables['filter']->put('creditor', 'debtor');
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function transactionBankFilter()
    {
        if (request()->has('bank_name') and ($bank = request()->get('bank_name'))) {
            $this->variables['models']->where('accounts.name', 'like', '%' . $bank . '%');
            $this->variables['filter']->put('bank_name', $bank);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function transactionPayerFilter()
    {
        if (request()->has('payer') and ($payer = request()->get('payer'))) {
            $this->variables['models']->join('users', 'users.id', 'transactions.user_id')
                                      ->where(
                                           DB::raw("concat(users.name_first, ' ', users.name_last)"),
                                           'like',
                                           '%' . $payer . '%'
                                      )
            ;
            $this->variables['filter']->put('payer', $payer);
        }
        return $this;
    }



    /**
     * @param $date
     *
     * @return bool
     */
    protected function dateIsValid($date)
    {
        return boolval(date_create($date));
    }



    /**
     * @param $paginate
     *
     * @return $this
     */
    protected function transactionEndFilter($paginate)
    {
        //        dd($this->variables['models']->raw(), __FILE__ . ' - ' . __LINE__);
        $this->variables['models'] = $paginate
             ? $this->variables['models']->paginate(50)->appends(\request()->all())
             : $this->variables['models']->get();
        return $this;
    }
}
