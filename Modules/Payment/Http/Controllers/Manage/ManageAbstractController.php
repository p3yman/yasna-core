<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 07/02/2018
 * Time: 12:39 PM
 */

namespace Modules\Payment\Http\Controllers\Manage;

use Illuminate\Routing\Controller;
use Modules\Manage\Traits\ManageControllerTrait;

abstract class ManageAbstractController extends Controller
{
    use ManageControllerTrait {
        abort as protected traitAbort;
        jsonAjaxSaveFeedback as private jsonAjaxSaveFeedback2;
    }

    protected $neededPermissions = [];

    public function abort($errorCode, $minimal = null, $affectHeader = true, $data = null)
    {
        return $this->traitAbort(
            $errorCode,
            is_null($minimal) ? $this->minimalViewNeeded() : $minimal,
            $affectHeader,
            $data
        );
    }

    protected function minimalViewNeeded()
    {
        return \request()->ajax();
    }

    protected function needPermission(string $permission)
    {
        $this->neededPermissions[] = $permission;
        return $this;
    }

    protected function hasNeededPermissions()
    {
        foreach ($this->neededPermissions as $permission) {
            if ($this->hasNotPermission($permission)) {
                return false;
            }
        }

        return true;
    }

    protected function hasNotNeededPermissions()
    {
        return !$this->hasNeededPermissions();
    }

    protected function hasPermission(string $permission)
    {
        return user()->as('admin')->can($permission);
    }

    protected function hasNotPermission(string $permission)
    {
        return !$this->hasPermission($permission);
    }

    protected function jsonAjaxSaveFeedback(...$parameters)
    {
        return $this->jsonAjaxSaveFeedback2(...$parameters);
    }
}
