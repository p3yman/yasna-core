<?php

namespace Modules\Payment\Http\Controllers\Manage\Downstream;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Payment\Http\Controllers\Manage\ManageAbstractController;
use Modules\Payment\Providers\PaymentToolsServiceProvider;

class GatesController extends ManageAbstractController
{
    public function fields(Request $request)
    {
        if ($gateSlug = $request->gate) {
            if (!PaymentToolsServiceProvider::gateExists($gateSlug)) {
                return $this->abort(403);
            }

            $fields = PaymentToolsServiceProvider::getGateFields($gateSlug);
        }

        return view('payment::downstream.gates.fields', compact('fields', 'gateSlug'));
    }
}
