<?php

namespace Modules\Payment\Http\Controllers\Manage\Downstream;

use App\Models\Account;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Modules\Payment\Http\Controllers\Manage\ManageAbstractController;
use Modules\Payment\Http\Requests\AccountSaveRequest;
use Modules\Payment\Http\Requests\AccountUpdateRequest;

class AccountsController extends ManageAbstractController
{
    protected static $variables              = [];
    protected static $onlineAccountsTypeSlug = 'online';
    protected static $acceptedSimpleTypes    = ['online', 'offline'];

    protected $processingAccountTypes = ['online', 'offline'];
    protected $processingConditions   = [];

    public static function index()
    {
        /*-----------------------------------------------
        | View File ...
        */
        self::$variables['view_file'] = "payment::downstream.accounts.browse";

        /*-----------------------------------------------
        | Browse Data ...
        */
        self::findOnlineAccounts();
        self::findOfflineAccounts();

        return self::$variables;
    }

    protected static function findOnlineAccounts()
    {
        if (user()->as('admin')->can('payment_online_accounts.browse')) {
            self::$variables['onlineAccounts'] = model('account')
                ->where('type', self::$onlineAccountsTypeSlug)
                ->orderBy('order')
                ->get();
        }
    }

    protected static function findOfflineAccounts()
    {
        if (user()->as('admin')->can('payment_offline_accounts.browse')) {
            self::$variables['offlineAccounts'] = model('account')
                ->where('type', '!=', self::$onlineAccountsTypeSlug)
                ->orderBy('type')
                ->orderBy('order')
                ->get();
        }
    }

    public function row(Request $request)
    {
        return $this->findAccountByRequest($request)
            ->needPermission('browse')
            ->renderViewDependingOnAccount('payment::downstream.accounts.browse-row');
    }

    public function list(Request $request)
    {
        if (self::simpleTypeIsNotAccepted($type = $request->type)) {
            return $this->abort(403);
        }

        switch ($type) {
            case  'online':
                self::findOnlineAccounts();
                break;
            case 'offline':
                self::findOfflineAccounts();
                break;
        }

        return view("payment::downstream.accounts.browse-table-$type", self::$variables);
    }

    public function createForm(Request $request)
    {
        if (self::simpleTypeIsNotAccepted($type = $request->type)) {
            return $this->abort(403);
        }

        // Checking permission ...
        $this->setProcessingAccountTypes($request->type);
        if ($this->hasNotPermission('create')) {
            return $this->abort(403);
        }

        $model = model('account');

        if ($type == 'online') {
            $model->type = 'online';
        }

        return view("payment::downstream.accounts.create-$type", compact('model'));
    }

    public function create(AccountSaveRequest $request)
    {
        $request->merge([
            'order' => ($lastOfThisType = model('account')
                ->whereType($request->type)
                ->orderBy('order', 'desc')
                ->first())
                ? ($lastOfThisType->order + 1)
                : 1,
        ]);
        if ($request->isOffline() or $request->slug) {
            $request->merge([
                'published_at' => Carbon::now(),
                'published_by' => user()->id,
            ]);
        }
        return $this->jsonAjaxSaveFeedback(
            ($isSaved = (($model = model('account')->batchSave($request->all())) and $model->exists)),
            [
                'success_callback' => $isSaved ? "divReload('$model->simple_type-accounts-list');" : "",

                'danger_message' => trans('payment::form.feed.error.saving-account'),
            ]
        );
    }

    public function editForm(Request $request)
    {
        return $this->findAccountByRequest($request)
            ->needPermission('edit')
            ->renderViewDependingOnAccount("payment::downstream.accounts.edit");
    }

    public function editSubmit(AccountUpdateRequest $request)
    {
        $updated = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback(($updated and $updated->exists), [
            'success_callback' => "multiGridRowUpdate($('.tbl-accounts'), '$updated->hashid');",
        ]);
    }

    public function activenessForm(Request $request)
    {
        return $this->findAccountByRequest($request)
            ->needPermission('edit')
            ->addCondition(function () {
                $model = self::$variables['model'];
                return ($model->isNotPublished() and $model->isActivable()) or
                    ($model->isPublished() and $model->isDeactivable());
            })
            ->renderViewDependingOnAccount('payment::downstream.accounts.activeness');
    }

    public function activeness(Request $request)
    {
        // Checking account existence ...
        $account = $this->findAccount($request->hashid);
        if (!$account->exists) {
            return $this->abort(410);
        }

        // Checking permission ...
        $this->setProcessingAccountTypes($account->simple_type);
        if ($this->hasNotPermission('edit')) {
            return $this->abort(403);
        }

        // Making action ...
        switch ($request->_submit) {
            case 'enable':
                if ($account->isActivable()) {
                    $ok = $account->update(['published_at' => Carbon::now(), 'published_by' => user()->id]);
                } else {
                    return $this->jsonAjaxSaveFeedback(0, [
                        'danger_message' => trans('payment::form.feed.error.activating-account'),
                    ]);
                }
                break;
            case 'disable':
                $ok = $account->update(['published_at' => null, 'published_by' => 0]);
                break;
            default:
                $ok = 0;
                break;
        }

        // Making response ...
        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "multiGridRowUpdate($('.tbl-accounts'), '$request->hashid');",
        ]);
    }

    public function makeDefaultForm(Request $request)
    {
        return $this->findAccountByRequest($request)
            ->needPermission('edit')
            ->addCondition(function () {
                return self::$variables['model']->isNotDefault();
            })
            ->renderViewDependingOnAccount('payment::downstream.accounts.make-default');
    }

    public function makeDefault(Request $request)
    {
        // Checking account existence ...
        $account = $this->findAccount($request->hashid);
        if (!$account->exists) {
            return $this->abort(410);
        }

        // Checking permission ...
        $this->setProcessingAccountTypes($simpleType = $account->simple_type);
        if ($this->hasNotPermission('edit')) {
            return $this->abort(403);
        }

        // Checking current order ...
        if ($account->isDefault()) {
            return $this->jsonAjaxSaveFeedback(0, [
                'danger_message' => trans('payment::general.account.message.is-default'),
            ]);
        }

        try {
            // Making action ...
            $family = model('account')
                ->where('id', '!=', $account->id)
                ->whereType($account->type)
                ->orderBy('order')
                ->get();

            DB::beginTransaction();

            $account->update(['order' => 1]);
            foreach ($family as $key => $sisterAccount) {
                $sisterAccount->update(['order' => $key + 2]);
            }

            DB::commit();

            // Making response ...
            return $this->jsonAjaxSaveFeedback(1, [
                'success_callback' => "divReload('$account->simple_type-accounts-list');",
            ]);
        } catch (Exception $exception) {
            return $this->abort(500);
        }
    }

    public function deleteForm(Request $request)
    {
        return $this->findAccountByRequest($request)
            ->needPermission('delete')
            ->renderViewDependingOnAccount('payment::downstream.accounts.delete');
    }

    public function delete(Request $request)
    {
        // Checking account existence ...
        $account = $this->findAccount($request->hashid);
        if (!$account->exists) {
            return $this->abort(410);
        }

        // Checking permission ...
        $this->setProcessingAccountTypes($account->simple_type);
        if ($this->hasNotPermission('delete')) {
            return $this->abort(403);
        }

        // Making response ...
        return $this->jsonAjaxSaveFeedback($account->delete(), [
            'success_callback' => "divReload('$account->simple_type-accounts-list');",

            'danger_message' => trans('payment::form.feed.error.deleting-account'),
        ]);
    }

    protected function findAccountByRequest(Request $request)
    {
        self::$variables['model'] = $this->findAccount($request->account);
        return $this;
    }

    /**
     * Find account with a given hashid
     *
     * @param string $hashid
     *
     * @return \App\Models\Account
     */
    protected function findAccount($hashid)
    {
        return model('account', $hashid);
    }

    protected function renderViewDependingOnAccount(string $view, array $parameters = [])
    {
        if (
            array_key_exists('model', self::$variables) and
            self::$variables['model'] and
            self::$variables['model']->exists and
            $this->processingConditionsConfirm()
        ) {
            if ($this->setProcessingAccountTypes(self::$variables['model']->simple_type)->hasNeededPermissions()) {
                return view($view, array_merge($parameters, self::$variables));
            } else {
                return $this->abort(403);
            }
        } else {
            return $this->abort(410);
        }
    }

    protected function setProcessingAccountTypes($types)
    {
        $this->processingAccountTypes = is_array($types) ? $types : [$types];
        return $this;
    }

    protected function getProcessingAccountTypes()
    {
        return $this->processingAccountTypes;
    }

    protected function hasPermission(string $permission)
    {
        foreach ($this->getProcessingAccountTypes() as $type) {
            if (!user()->as('admin')->can("payment_{$type}_accounts." . $permission)) {
                return false;
            }
        }

        return true;
    }

    protected function addCondition($newCondition)
    {
        $this->processingConditions[] = $newCondition;
        return $this;
    }

    protected function processingConditionsConfirm()
    {
        foreach ($this->processingConditions as $condition) {
            if (is_closure($condition)) {
                if (!$condition()) {
                    return false;
                }
            } else {
                if (!$condition) {
                    return false;
                }
            }
        }

        return true;
    }

    protected static function simpleTypeIsAccepted(string $type)
    {
        return in_array($type, self::$acceptedSimpleTypes);
    }

    protected static function simpleTypeIsNotAccepted(string $type)
    {
        return !self::simpleTypeIsAccepted($type);
    }
}
