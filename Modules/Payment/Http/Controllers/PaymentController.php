<?php

namespace Modules\Payment\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Database\ConnectionResolverInterface as Resolver;
use Modules\Payment\Events\FailedPayment;
use Modules\Payment\Services\PaymentHandler\PaymentHandler;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;

class PaymentController extends Controller
{
    public function index()
    {
        //        return payment()
        //            ->transaction()
        //            ->invoiceId(12)
        //            ->callback('http://localhost/yasna-core/public/manage/downstream/payment-accounts')
        //            ->amount(1000)
        //            ->fire()['response'];
    }



    public function callback(Request $request)
    {
        $transaction = $this->findTransaction($request);
        if (!$transaction) {
            return redirect('/');
        }

        $response = $transaction->callbackAction();

        if ($response['code'] > 0) {
            return $response['response'];
        }
        event(new FailedPayment($transaction->getModel()));
    }



    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return Transaction
     */
    protected function findTransaction(Request $request)
    {
        return Transaction::findByHashid($request->hashid);
    }



    public function checkparams($bank, $trackingNumber, Request $request)
    {
        $route = "Modules\\Payment\\Interfaces\\" . studly_case($bank);
        if (class_exists($route)) {
            $redirect = new $route();
            return $redirect->backFromGateway($trackingNumber, $request);
        }
    }
}
