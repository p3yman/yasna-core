<?php

namespace Modules\Payment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class TestController extends Controller
{
    public function create(Request $request)
    {
        return payment()
            ->transaction()
            ->invoiceId(12)
            ->callback('http://localhost/yasna-core/public/manage/payment/transactions')
            ->autoVerify(false)
            ->amount($request->amount ?: 1000)
            ->fire()['response'];
    }
}
