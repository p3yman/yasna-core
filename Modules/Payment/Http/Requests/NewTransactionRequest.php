<?php

namespace Modules\Payment\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class NewTransactionRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "amount"  => "required|numeric",
             "type"    => "required|in:creditor,debtor",
             "account" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "amount" => "ed|numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "amount",
             "type",
             "account",
             "title",
             "description",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveAccount();
        $this->correctAmount();
    }



    /**
     * resolve Account
     *
     * @return void
     */
    private function resolveAccount()
    {
        $account = model("account")->grabHashid($this->getData("account"));

        $this->setData("account", $account->id);
    }



    /**
     * correct amount to correspond the type (creator/debtor)
     *
     * @return void
     */
    private function correctAmount()
    {
        if ($this->getData('type') == "debtor") {
            $this->setData("amount", -$this->getData("amount"));
        }
    }

}
