<?php

namespace Modules\Payment\Http\Requests;

use App\Models\Account;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class AccountUpdateRequest extends YasnaRequest
{
    protected $model_name   = "account";
    protected $dynamicRules = null;

    public function rules()
    {
        return array_merge_recursive([
            'title'    => 'required',
            'currency' => 'required|in:' . implode(',', PaymentToolsServiceProvider::getAvailableCurrencies()),
            'slug'     => 'in:' . implode(',', PaymentToolsServiceProvider::getAvailableGates(true)),
        ], $this->getDynamicRules(), $this->typeRules());
    }

    protected function getDynamicRules()
    {
        if (is_null($this->dynamicRules)) {
            $this->dynamicRules = PaymentToolsServiceProvider::getGateFields($this->gateSlug());
        }

        return $this->dynamicRules;
    }

    protected function typeRules()
    {
        switch ($this->accountType()) {
            case 'online':
                return [
                    'name' => 'required',
                ];

            case 'deposit':
                return [
                    'account_no'  => 'required',
                    'owner'       => 'required',
                    'branch_code' => 'numeric',
                ];

            case 'shetab':
                return [
                    'account_no' => 'required|digits:16',
                    'owner'      => 'required',
                ];

            case 'sheba':
                return [
                    'account_no' => 'required',
                    'owner'      => 'required',
                ];

            default:
                return [];
        }
    }

    public function authorize()
    {
        return $this->model and
            $this->model->exists and
            user()->as('admin')->can("payment_{$this->simpleType()}_accounts.edit");
    }

    public function corrections()
    {
        if ($this->isOnline() and !$this->accountGateSlug()) {
            Account::setSavingGate($this->gateSlug());
        }

        if (($this->accountTypeIs('shetab')) and array_key_exists('account_no', $this->data)) {
            $this->data['account_no'] = str_replace('-', '', $this->data['account_no']);
        }
    }

    public function purifier()
    {
        return [
            'account_no' => 'ed',
        ];
    }

    public function attributes()
    {
        $attributes = [
            'slug'        => trans('payment::general.gate.title.singular'),
            'type'        => trans('payment::general.type'),
            'owner'       => trans('payment::general.account.owner.name.singular'),
            'branch_code' => trans('payment::general.bank.branch-code'),
        ];
        foreach (array_keys($this->getDynamicRules()) as $field) {
            $attributes[$field] = trans("payment::validation.attributes.$field");
        }

        return array_merge_recursive($attributes, $this->getTypeAttributes());
    }

    protected function getTypeAttributes()
    {
        switch ($this->accountType()) {
            case 'online':
                return [
                    'name' => trans('payment::general.bank.title.singular'),
                ];

            case 'deposit':
                return [
                    'account_no' => trans('payment::general.account.number'),
                    'name'       => trans('payment::general.bank.name.singular'),
                ];

            case 'shetab':
                return [
                    'account_no' => trans('payment::general.card.number'),
                    'name'       => trans('payment::general.bank.name.singular'),
                ];

            case 'sheba':
                return [
                    'account_no' => trans('payment::general.bank.IBAN'),
                    'nameF'      => trans('payment::general.bank.name.singular'),
                ];

            default:
                return [];
        }
    }

    public function all($keys = null)
    {
        $all     = parent::all();
        $removal = ['type'];
        if ($this->isOffline() or $this->accountGateSlug()) {
            $removal[] = 'slug';
        }
        return array_except($all, $removal);
    }

    public function isOnline()
    {
        return $this->accountTypeIs('online');
    }

    public function isOffline()
    {
        return !$this->isOnline();
    }

    public function simpleType()
    {
        return $this->isOnline() ? 'online' : 'offline';
    }

    public function accountType()
    {
        return $this->model->type;
    }

    public function accountTypeIs(string $type)
    {
        return $this->accountType() == $type;
    }

    protected function gateSlug()
    {
        return $this->isOnline() ? ($this->accountGateSlug() ?: $this->data['slug']) : null;
    }

    protected function accountGateSlug()
    {
        return $this->model->slug;
    }
}
