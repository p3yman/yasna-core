<?php

namespace Modules\Payment\Http\Requests\V1;

use App\Models\Transaction;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property Transaction $model
 */
class TransactionDetailRequest extends YasnaFormRequest
{
    protected $model_name                    = "Transaction";
    protected $model_slug_attribute          = "tracking_no";
    protected $model_slug_column             = "tracking_no";
    protected $should_allow_create_mode      = false;
    protected $should_load_model_with_hashid = false;
    protected $automatic_injection_guard     = false;
    protected $should_load_model_with_slug   = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->user_id == user()->id;
    }
}
