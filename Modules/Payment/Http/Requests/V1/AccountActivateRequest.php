<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/8/19
 * Time: 2:57 PM
 */

namespace Modules\Payment\Http\Requests\V1;


use App\Models\Account;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class AccountActivateRequest
 *
 * @property Account $model
 */
class AccountActivateRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = 'account';

    /**
     * Whether a trashed account is acceptable.
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->model->exists
             and
             $this->model->can('edit')
             and
             $this->model->isNotActive()
        );
    }
}
