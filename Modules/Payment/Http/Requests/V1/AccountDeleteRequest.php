<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/8/19
 * Time: 12:27 PM
 */

namespace Modules\Payment\Http\Requests\V1;


use App\Models\Account;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class AccountDeleteRequest
 *
 * @property Account $model
 */
class AccountDeleteRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = 'account';

    /**
     * Whether a trashed account is acceptable.
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model       = $this->model;
        $simple_type = $model->simple_type;


        return (
             $model->exists
             and
             $model->isNotTrashed()
             and
             $model->canForType($simple_type, 'delete')
        );
    }
}
