<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/13/19
 * Time: 5:41 PM
 */

namespace Modules\Payment\Http\Requests\V1;


use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class TransactionSaveRequest extends YasnaRequest
{
    /**
     * The Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->not_exists;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'account_id' => [
                  'required',
                  Rule::in($this->acceptableAccountIds()),
             ],

             'callback_url' => [
                  'required',
                  'url',
             ],


             'payable_amount' => [
                  'required',
                  'numeric',
             ],


             'invoice_id' => [
                  'required',
                  'numeric',
             ],

             'invoice_model' => function (string $field, string $value, $fail) {
                 $model_class = MODELS_NAMESPACE . studly_case($value);

                 // check if a valid model has been produced
                 if (!class_exists($model_class)) {
                     $fail(trans('validation.in'));
                 }

                 // check if the invoice model and the invoice id belong to an existed record
                 $invoice_id = $this->getData('invoice_id');
                 if (!$invoice_id) {
                     return;
                 }

                 $model_object = model($value, $invoice_id);
                 if ($model_object->not_exists) {
                     $fail($this->runningModule()->getTrans('validation.invalid_invoice'));
                 }

             },
        ];
    }



    /**
     * Returns an array of the acceptable account IDs.
     *
     * @return array
     */
    protected function acceptableAccountIds()
    {
        return model('account')
             ->whereIsActive()
             ->get(['id'])
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $module = $this->runningModule();

        return [
             'account_id'     => $module->getTrans('validation.attributes.account'),
             'callback_url'   => $module->getTrans('validation.attributes.callback_url'),
             'invoice_model'  => $module->getTrans('validation.attributes.invoice_model'),
             'payable_amount' => $module->getTrans('validation.attributes.payable_amount'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'account_id'     => 'dehash',
             'invoice_id'     => 'dehash',
             'payable_amount' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_keys($this->rules());
    }
}
