<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/14/19
 * Time: 10:02 AM
 */

namespace Modules\Payment\Http\Requests\V1;


use App\Models\Transaction;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class TransactionFireRequest
 *
 * @property Transaction $model
 */
class TransactionFireRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = 'transaction';

    /**
     * Whether a trashed account is acceptable.
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->modelExists()
             and
             $this->isOwner()
        );
    }



    /**
     * Whether the transaction model exists.
     *
     * @return bool
     */
    protected function modelExists()
    {
        return $this->model->exists;
    }



    /**
     * Whether the logged in user is the owner of the transaction model.
     *
     * @return bool
     */
    protected function isOwner()
    {
        return ($this->model->user_id == user()->id);
    }
}
