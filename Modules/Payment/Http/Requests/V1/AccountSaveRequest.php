<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/7/19
 * Time: 11:41 AM
 */

namespace Modules\Payment\Http\Requests\V1;


use App\Models\Account;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class AccountSaveRequest
 *
 * @property Account $model
 */
class AccountSaveRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = 'account';

    /**
     * The Main Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->validModelRequested()
             and
             $this->userHasAccess()
        );
    }



    /**
     * Whether a valid model requested.
     *
     * @return bool
     */
    protected function validModelRequested()
    {
        return $this->tryingToUpdate()
             ? $this->model->exists
             : true;
    }



    /**
     * Whether the logged in user has a true access.
     *
     * @return bool
     */
    protected function userHasAccess()
    {
        $type   = $this->desiredSimpleType();
        $action = $this->tryingToUpdate() ? 'edit' : 'create';

        return $this->model->canForType($type, $action);
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return $this->getFieldsColumn('validation_rules');
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return $this->getFieldsColumn('label');
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return $this->getFieldsColumn('purification_rules');
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctType();
        $this->correctSlug();
        $this->correctOrder();
        $this->correctPublishInfo();
        $this->correctShetabAccountNo();
        $this->prepareModel();
    }



    /**
     * Corrects type.
     */
    protected function correctType()
    {
        if ($this->tryingToUpdate()) {
            $this->data['type'] = $this->model->type;
        } else {
            $this->data['type'] = $this->desiredType();
        }
    }



    /**
     * Corrects slug.
     */
    protected function correctSlug()
    {
        if ($this->tryingToCreate()) {
            return;
        }

        unset($this->data['slug']);
    }



    /**
     * Corrects order.
     */
    protected function correctOrder()
    {
        $last_of_this_type = model('account')
             ->whereType($this->desiredType())
             ->orderByDesc('order')
             ->first()
        ;
        $order             = $last_of_this_type ? ($last_of_this_type->order + 1) : 1;

        $this->data['order'] = $order;
    }



    /**
     * Corrects publish info.
     */
    protected function correctPublishInfo()
    {
        $this->data['published_at'] = now()->toDateTimeString();
        $this->data['published_by'] = user()->id;
    }



    /**
     * Corrects the account number if the type is `shetab`
     */
    protected function correctShetabAccountNo()
    {
        if ($this->getSafeModel()->hasNotType('shetab')) {
            return;
        }

        $account_no = ($this->data['account_no'] ?? null);

        if (!$account_no) {
            return;
        }

        $this->data['account_no'] = str_replace('-', '', $account_no);
    }



    /**
     * Prepares the model for saving.
     */
    protected function prepareModel()
    {
        $model = $this->getSafeModel();
        if ($model->isOnline()) {
            $model->setSavingGate($model->slug);
        } else {
            $model->setSavingType($model->type);
        }
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $this->readModel();

        return array_keys($this->getFormItems());
    }



    /**
     * Reads the model instance at the start of the request.
     * <br>
     * _This method is being called in the `fillableFields()` method._
     */
    protected function readModel()
    {
        $this->loadRequestedModel();

        $this->model_name = null;
    }



    /**
     * Returns the specified column of the form data.
     *
     * @param string $column
     *
     * @return array
     */
    protected function getFieldsColumn(string $column)
    {
        return collect($this->getFormItems())
             ->map(function (array $field) use ($column) {
                 return $field[$column];

             })
             ->filter()
             ->toArray()
             ;
    }



    /**
     * Returns the form data.
     *
     * @return array
     */
    protected function getFormItems()
    {
        return $this->getSafeModel()->renderFormItems();
    }



    /**
     * Returns a safe model to be saved.
     *
     * @return Account
     */
    protected function getSafeModel()
    {
        return $this->tryingToUpdate()
             ? $this->getSafeModelForUpdate()
             : $this->getSafeModelForCreate();
    }



    /**
     * Returns a safe model to be saved for the update action.
     *
     * @return Account
     */
    protected function getSafeModelForUpdate()
    {
        return $this->model;
    }



    /**
     * Returns a safe model to be saved for the create action.
     *
     * @return Account
     */
    protected function getSafeModelForCreate()
    {
        $this->model->type = $this->desiredTypeForCreate();
        $this->model->slug = ($this->data['slug'] ?? null);

        return $this->model;
    }



    /**
     * Returns the desired simple type.
     *
     * @return string
     */
    protected function desiredSimpleType()
    {
        return $this->getSafeModel()->simple_type;
    }



    /**
     * Returns the desired type to be used.
     *
     * @return string
     */
    protected function desiredType()
    {
        return $this->tryingToUpdate()
             ? $this->desiredTypeForUpdate()
             : $this->desiredTypeForCreate();
    }



    /**
     * Returns the desired type to be used for the update action.
     *
     * @return string
     */
    protected function desiredTypeForUpdate()
    {
        return ($this->model->type ?? $this->defaultType());
    }



    /**
     * Returns the desired type to be used for the create action.
     *
     * @return string
     */
    protected function desiredTypeForCreate()
    {
        return ($this->data['type'] ?? $this->defaultType());
    }



    /**
     * Whether the model exists.
     *
     * @return bool
     */
    protected function modelExists()
    {
        return $this->model->exists;
    }



    /**
     * Whether the model not exists.
     *
     * @return bool
     */
    protected function modelNotExists()
    {
        return !$this->modelExists();
    }



    /**
     * Whether the client is trying to update an account.
     *
     * @return bool
     */
    protected function tryingToUpdate()
    {
        return $this->filled('hashid');
    }



    /**
     * Whether the client is trying to create and account.
     *
     * @return bool
     */
    protected function tryingToCreate()
    {
        return !$this->tryingToUpdate();
    }



    /**
     * Returns the default type.
     *
     * @return string
     */
    protected function defaultType()
    {
        return 'online';
    }
}
