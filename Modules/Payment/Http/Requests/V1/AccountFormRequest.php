<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/7/19
 * Time: 5:16 PM
 */

namespace Modules\Payment\Http\Requests\V1;


use App\Models\Account;
use Illuminate\Validation\Rule;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class AccountFormRequest
 *
 * @property Account $model
 */
class AccountFormRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = 'account';

    /**
     * Whether a trashed account is acceptable.
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * Returns the general rules.
     *
     * @return array
     */
    public function generalRules()
    {
        return [
             'type' => Rule::in(PaymentToolsServiceProvider::allPaymentTypes()),
        ];
    }



    /**
     * Returns the slug rules.
     *
     * @return array
     */
    public function slugRules()
    {
        if ($this->model->exists) {
            return [];
        }

        return [
             'slug' => Rule::in(PaymentToolsServiceProvider::getAvailableGates(true)),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'type',
             'slug',
        ];
    }



    /**
     * Returns a usable version of model to generate the form items.
     *
     * @return Account
     */
    public function usableModel()
    {
        if ($this->model->not_exists) {
            $this->model->type = $this->type;
            $this->model->slug = $this->slug;
        }

        return $this->model;
    }
}
