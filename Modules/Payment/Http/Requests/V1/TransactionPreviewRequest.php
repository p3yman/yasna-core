<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/9/19
 * Time: 12:58 PM
 */

namespace Modules\Payment\Http\Requests\V1;


use App\Models\Transaction;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class TransactionPreviewRequest
 *
 * @property Transaction $model
 */
class TransactionPreviewRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = 'transaction';

    /**
     * Whether a trashed account is acceptable.
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }
}
