<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/7/19
 * Time: 5:53 PM
 */

namespace Modules\Payment\Http\Requests\V1;


use App\Models\Account;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class AccountPreviewRequest
 *
 * @property Account $model
 */
class AccountPreviewRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = 'account';

    /**
     * Whether a trashed account is acceptable.
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Responder's Name
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->model->exists
             and
             $this->previewIsAllowed()
        );
    }



    /**
     * Whether the preview on the requested account is allowed for the logged in user.
     *
     * @return bool
     */
    protected function previewIsAllowed()
    {
        $simple_type = $this->model->simple_type;

        return $this->model->canForType($simple_type, "browse");
    }
}
