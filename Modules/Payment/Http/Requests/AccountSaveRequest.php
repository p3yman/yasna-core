<?php

namespace Modules\Payment\Http\Requests;

use App\Models\Account;
use Modules\Payment\Providers\PaymentToolsServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class AccountSaveRequest extends YasnaRequest
{
    protected $model_name   = "";
    protected $dynamicRules = null;

    public function rules()
    {
        return array_merge_recursive([
            'title'    => 'required',
            'currency' => 'required|in:' . implode(',', PaymentToolsServiceProvider::getAvailableCurrencies()),
            'slug'     => 'in:' . implode(',', PaymentToolsServiceProvider::getAvailableGates(true)),
            'type'     => 'required|in:' . implode(',', PaymentToolsServiceProvider::allPaymentTypes()),
        ], $this->getDynamicRules(), $this->typeRules());
    }

    public function attributes()
    {
        $attributes = [
            'slug'        => trans('payment::general.gate.title.singular'),
            'type'        => trans('payment::general.type'),
            'owner'       => trans('payment::general.account.owner.name.singular'),
            'branch_code' => trans('payment::general.bank.branch-code'),
        ];
        foreach (array_keys($this->getDynamicRules()) as $field) {
            $attributes[$field] = trans("payment::validation.attributes.$field");
        }

        return array_merge_recursive($attributes, $this->getTypeAttributes());
    }

    public function corrections()
    {
        if ($this->isOnline()) {
            $this->data['type'] = 'online';
            Account::setSavingGate($this->data['slug']);
        } else {
            if (($this->data['type'] == 'shetab') and array_key_exists('account_no', $this->data)) {
                $this->data['account_no'] = str_replace('-', '', $this->data['account_no']);
            }
            Account::setSavingType($this->data['type']);
        }
    }

    public function authorize()
    {
        return user()->as('admin')->can("payment_{$this->simpleType()}_accounts.create");
    }

    public function purifier()
    {
        return [
            'account_no' => 'ed',
        ];
    }

    protected function getDynamicRules()
    {
        if (is_null($this->dynamicRules)) {
            if ($this->isOnline()) {
                $this->dynamicRules = PaymentToolsServiceProvider::getGateFields($this->data['slug']);
            } else {
                $this->dynamicRules = [];
            }
        }

        return $this->dynamicRules;
    }

    protected function typeRules()
    {
        switch ($this->type) {
            case 'online':
                return [
                    'name' => 'required',
                ];

            case 'deposit':
                return [
                    'account_no'  => 'required',
                    'owner'       => 'required',
                    'branch_code' => 'numeric',
                ];

            case 'shetab':
                return [
                    'account_no' => 'required|digits:16',
                    'owner'      => 'required',
                ];

            case 'sheba':
                return [
                    'account_no' => 'required',
                    'owner'      => 'required',
                ];

            default:
                return [];
        }
    }

    protected function getTypeAttributes()
    {
        switch ($this->type) {
            case 'online':
                return [
                    'name' => trans('payment::general.bank.title.singular'),
                ];

            case 'deposit':
                return [
                    'account_no' => trans('payment::general.account.number'),
                    'name'       => trans('payment::general.bank.name.singular'),
                ];

            case 'shetab':
                return [
                    'account_no' => trans('payment::general.card.number'),
                    'name'       => trans('payment::general.bank.name.singular'),
                ];

            case 'sheba':
                return [
                    'account_no' => trans('payment::general.bank.IBAN'),
                    'nameF'      => trans('payment::general.bank.name.singular'),
                ];

            default:
                return [];
        }
    }

    public function isOnline()
    {
        return $this->route()->parameter('type') == 'online';
    }

    public function isOffline()
    {
        return !$this->isOnline();
    }

    public function simpleType()
    {
        return $this->isOnline() ? 'online' : 'offline';
    }
}
