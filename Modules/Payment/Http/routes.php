<?php

Route::group(['prefix' => 'payment', 'namespace' => 'Modules\Payment\Http\Controllers'], function () {
    Route::group(['middleware' => 'web'], function () {
        Route::get('/', 'PaymentController@index');
        Route::any('/check/{bank}/{trackingNumber}', 'PaymentController@checkparams')->name('bank.callback');
    });

    Route::match(['GET', 'POST'], 'callback/{hashid}', 'PaymentController@callback')
         ->name('payment.callback')
    ;
});


Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'prefix'     => 'manage/payment',
     'namespace'  => 'Modules\Payment\Http\Controllers\Manage',
], function () {


    Route::group(['prefix' => 'downstream', 'namespace' => 'Downstream'], function () {
        Route::group(['prefix' => 'accounts'], function () {
            Route::get('row/{account}', 'AccountsController@row');
            Route::get('list/{type}', 'AccountsController@list');

            Route::get('create/{type}', 'AccountsController@createForm');
            Route::post('create/{type}', 'AccountsController@create');

            Route::get('edit/{account}', 'AccountsController@editForm');
            Route::post('edit', 'AccountsController@editSubmit');

            Route::get('activeness/{account}', 'AccountsController@activenessForm');
            Route::post('activeness', 'AccountsController@activeness');

            Route::get('make-default/{account}', 'AccountsController@makeDefaultForm');
            Route::post('make-default', 'AccountsController@makeDefault');

            Route::get('delete/{account}', 'AccountsController@deleteForm');
            Route::post('delete', 'AccountsController@delete');
        });

        Route::group(['prefix' => 'gate'], function () {
            Route::get('fields/{gate}', 'GatesController@fields');
        });
    });

    Route::group(['prefix' => 'transactions'], function () {
        Route::get('/', 'TransactionController@index');
        Route::get('/export', 'TransactionController@export');
        Route::get('/new', 'TransactionController@newForm')->name("payment-new-transaction");
        Route::post('/new' , 'TransactionController@newSave')->name("payment-new-transaction-save");
    });
});
