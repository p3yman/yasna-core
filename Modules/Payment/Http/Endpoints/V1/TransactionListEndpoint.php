<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/payment-transaction-list
 *                    Transaction List
 * @apiDescription    Get a list of the transactions.
 * @apiVersion        1.0.0
 * @apiName           Transaction List
 * @apiGroup          Payment
 * @apiPermission     Admin
 * @apiParam {String} [tracking_no] The tracking number to filter the transactions in the result
 * @apiParam {String} [status] The status to filter the transactions in the result
 * @apiParam {String} [type] The type to filter the transactions in the result
 * @apiParam {String} [datetime_from] The start time to filter the transactions in the result
 * @apiParam {String} [datetime_to] The end time to filter the transactions in the result
 * @apiParam {String} [bank_name] Part of a bank name to filter the transactions in the result
 * @apiParam {String} [user] The hashid of a user to filter the transactions in the result
 * @apiParam {String} [invoice_id] The invoice id to filter the transactions in the result
 * @apiParam {String} [invoice_model] The invoice model to filter the transactions in the result
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer} [per_page=15] Indicate the number of results on per page. (available only with `paginated=1`).
 * @apiParam {Integer} [page=1] Page number of returned result (available only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      "total": 1,
 *      "authenticated_user": "QKgJx"
 *      },
 *      "results": [
 *           {
 *           "id": "qKXVA",
 *           "user_id": "qKXVA",
 *           "invoice_model": "Cart",
 *           "invoice_id": "NqVoA",
 *           "tracking_no": "1001",
 *           "client_ip": "127.0.0.1",
 *           "created_at": 1546691449,
 *           "status": "cancelled",
 *           "status_text": "Canceled",
 *           "status_applied_at": 1546691458,
 *           "type": "online",
 *           "type_title": "Online Payment",
 *           "account_id": "QKgJx",
 *           "account_name": "Saman Bank",
 *           "bank_responses": [
 *                {
 *                     "action": "callbackAction",
 *                     "response": {
 *                          "State": null,
 *                          "StateCode": null,
 *                          "ResNum": "1001",
 *                          "MID": "1298741",
 *                          "RefNum": null,
 *                          "CID": null,
 *                          "TRACENO": null,
 *                          "RRN": null,
 *                          "SecurePan": null
 *                     },
 *                     "time": "2019-01-05 16:00:58"
 *                     }
 *                ]
 *           }
 *      ]
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Cart\Http\Controllers\TransactionListController controller()
 */
class TransactionListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Transaction List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        $permission_string = model('transaction')->permissionString('browse');

        return $this
             ->user
             ->as('admin')
             ->can($permission_string)
             ;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TransactionListController@index';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  'id'                => 'qKXVA',
                  'user_id'           => 'qKXVA',
                  'invoice_model'     => 'Cart',
                  'invoice_id'        => 'NqVoA',
                  'tracking_no'       => '1001',
                  'client_ip'         => '127.0.0.1',
                  'created_at'        => 1546691449,
                  'status'            => 'cancelled',
                  'status_text'       => 'Canceled',
                  'status_applied_at' => 1546691458,
                  'type'              => 'online',
                  'type_title'        => 'Online Payment',
                  'account_id'        => 'QKgJx',
                  'account_name'      => 'Saman Bank',
                  'bank_responses'    =>
                       [
                            [
                                 'action'   => 'callbackAction',
                                 'response' =>
                                      [
                                           'State'     => null,
                                           'StateCode' => null,
                                           'ResNum'    => '1001',
                                           'MID'       => '1298741',
                                           'RefNum'    => null,
                                           'CID'       => null,
                                           'TRACENO'   => null,
                                           'RRN'       => null,
                                           'SecurePan' => null,
                                      ],
                                 'time'     => '2019-01-05 16:00:58',
                            ],
                       ],
             ],
        ], [
             "total" => 1,
        ]);
    }
}
