<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Payment\Http\Controllers\V1\TransactionSingleController;

/**
 * @api               {GET}
 *                    /api/modular/v1/payment-transaction-track
 *                    Transaction Track
 * @apiDescription    Get the after-payment details of a transaction record, with the resource of its attached model.
 * @apiVersion        1.0.0
 * @apiName           Transaction Track
 * @apiGroup          Payment
 * @apiPermission     User (Transaction Owner)
 * @apiParam {string} tracking_no  the tracking number of the transaction
 * @apiParam {array}  [including] the array of resources to be included in the result
 * @apiParam {array}  [excluding] the array of resources to be excluded from the result
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "status":200,
 *    "metadata":{
 *     "tracking_no":"1055",
 *       "authenticated_user":"aALnK"
 *    },
 *    "results":{
 *     "id":"qKXVA",
 *       "created_at":1560843380,
 *       "updated_at":1560856858,
 *       "deleted_at":null,
 *       "created_by":{
 *         "id":"aALnK",
 *          "full_name":"Taha Kamkar"
 *       },
 *       "updated_by":{
 *         "id":"aALnK",
 *          "full_name":"Taha Kamkar"
 *       },
 *       "deleted_by":{
 *         "id":"",
 *          "full_name":""
 *       },
 *       "invoice_model":"Cart",
 *       "invoice_id":"qKXVA",
 *       "tracking_no":"1055",
 *       "client_ip":"127.0.0.1",
 *       "title":null,
 *       "description":null,
 *       "status":"not_raised",
 *       "status_text":"Not Paid",
 *       "status_applied_at":1560857343,
 *       "online_payment_status":null,
 *       "type":"online",
 *       "type_title":"Online",
 *       "account_id":"QKgJx",
 *       "account_name":Saman Bank",
 *       "bank_responses":[
 *     ],
 *       "user_id":"aALnK",
 *       "payable_amount":365800,
 *       "paid_amount":500000,
 *       "verified_amount":0,
 *       "refunded_amount":0,
 *       "proceeded_at":null,
 *       "declared_at":null,
 *       "verified_at":null,
 *       "effected_at":null,
 *       "cancelled_at":null,
 *       "rejected_at":null,
 *       "refunded_at":null,
 *       "due_date":null,
 *       "bank_reference_no":null,
 *       "bank_resnum":null,
 *       "cancellation_reason":null,
 *       "rejection_reason":null,
 *       "refundation_reason":null,
 *       "locale":"fa",
 *       "currency":"IRR",
 *       "total_items":1,
 *       "code":"PDK-5679",
 *       "payment_status":null,
 *       "user_name":"Taha",
 *       "original_price":1000000,
 *       "sale_price":780000,
 *       "delivery_price":0,
 *       "added_discount":0,
 *       "tax_amount":85800,
 *       "invoiced_amount":865800,
 *       "is_active":1,
 *       "items":[
 *          {
 *              "id":"qKXVA",
 *             "created_at":1560843380,
 *             "updated_at":1560843380,
 *             "deleted_at":null,
 *             "created_by":{
 *              "id":"",
 *                "full_name":""
 *             },
 *             "updated_by":{
 *              "id":"",
 *                "full_name":""
 *             },
 *             "deleted_by":{
 *              "id":"",
 *                "full_name":""
 *             },
 *             "count":1,
 *             "currency":"IRR",
 *             "locale":"fa",
 *             "ware_id":"QKgJx",
 *             "ware_title":"Samsung Thing",
 *             "post_id":"xpvWN",
 *             "post_title":"Samsung",
 *             "posttype_id":"PNOLA",
 *             "posttype_title":"Shop",
 *             "single_original_price":1000000,
 *             "single_sale_price":780000,
 *             "original_price":1000000,
 *             "sale_price":780000
 *          }
 *       ],
 *       "shipping_method":null,
 *       "transactions":[
 *       ]
 *    }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-404",
 *      "userMessage": "Not Found!",
 *      "errorCode": "endpoint-404",
 *      "moreInfo": "endpoint.moreInfo.endpoint-404",
 *      "errors": []
 * }
 * @method TransactionSingleController controller()
 */
class TransactionTrackEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Transaction Track";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TransactionSingleController@track';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
