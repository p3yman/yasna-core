<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Payment\Http\Controllers\V1\TransactionFireController;

/**
 * @api               {GET}
 *                    /api/modular/v1/payment-transaction-fire
 *                    Transaction Fire
 * @apiDescription    Get the needs to fire a transaction with submitting a form.
 * @apiVersion        1.0.0
 * @apiName           Transaction Fire
 * @apiGroup          Payment
 * @apiPermission     User
 * @apiParam {String} hashid The hashid of the transaction
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "form_action": "https://sep.shaparak.ir/payment.aspx",
 *           "form_method": "POST",
 *           "form_data": {
 *                "Amount": 1000,
 *                "MID": "1234567",
 *                "ResNum": "1020",
 *                "RedirectURL": "http://yasna.local/payment/callback"
 *           }
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   Unable-to-Fire:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unable to fire the transaction.",
 *      "userMessage": "Unable to fire this transaction.",
 *      "errorCode": "payment-10001",
 *      "moreInfo": "payment.moreInfo.payment-10001",
 *      "errors": []
 * }
 * @method  TransactionFireController controller()
 */
class TransactionFireEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Transaction Fire";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TransactionFireController@fire';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'form_action' => 'https://sep.shaparak.ir/payment.aspx',
             'form_method' => 'POST',
             'form_data'   =>
                  [
                       'Amount'      => 1000,
                       'MID'         => '1234567',
                       'ResNum'      => '1020',
                       'RedirectURL' => 'http://yasna.local/payment/callback',
                  ],
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
