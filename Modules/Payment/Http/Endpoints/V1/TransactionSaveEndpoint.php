<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Payment\Http\Controllers\V1\TransactionSaveController;

/**
 * @api               {POST}
 *                    /api/modular/v1/payment-transaction-save
 *                    Transaction Save
 * @apiDescription    Save a new transaction.
 * @apiVersion        1.0.0
 * @apiName           Transaction Save
 * @apiGroup          Payment
 * @apiPermission     User
 * @apiParam {String} account_id The hashid of an active account.
 * @apiParam {String} callback_url The callback url to be redirected from the gateway.
 * @apiParam {String} [invoice_model] The name of the invoice model
 * @apiParam {String} invoice_id The hashid of the invoice id
 * @apiParam {String} payable_amount The payable amount.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Invalid-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "payment.moreInfo.422",
 *      "errors": {
 *           "callback_url": [
 *                [
 *                     "The Callback URL field is required."
 *                ]
 *           ]
 *      }
 * }
 * @method  TransactionSaveController controller()
 */
class TransactionSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Transaction Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TransactionSaveController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
