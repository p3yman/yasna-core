<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/payment-account-form
 *                    Account Form
 * @apiDescription    Returns the form items for the save endpoint.
 *                    <br>
 *                    This Endpoint has been provided to be used by the frontend developers to render the items in
 *                    their forms.
 * @apiVersion        1.0.0
 * @apiName           Account Form
 * @apiGroup          Payment
 * @apiPermission     Admin
 * @apiParam {String} [hashid] The hashid of the account to be used while rendering the form items.
 * @apiParam {String} [type] The type to be used while rendering the form. This field will affect only when no hashid
 *           has been specified or the model with the specified hashid does not exits.
 * @apiParam {String} [slug] The slug of the gateway to be used while rendering the form. This field will affect only
 *           when the type is `online` and no hashid has been specified or the model with the specified hashid does not
 *           exits.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "title": {
 *                "field_name": "title",
 *                "order": 1,
 *                "label": "Title",
 *                "hint": null,
 *                "purification_rules": null,
 *                "validation_rules": "| required ",
 *                "class": "form-default form-required ",
 *                "style": null,
 *                "extra": null,
 *                "is_required": true,
 *                "type": "input"
 *           },
 *                "currency": {
 *                "field_name": "currency",
 *                "order": 2,
 *                "label": "Currency",
 *                "hint": null,
 *                "purification_rules": null,
 *                "validation_rules": "in:\"IRR\"| required ",
 *                "class": "form-default form-required ",
 *                "style": null,
 *                "extra": null,
 *                "is_required": true,
 *                "type": "input"
 *           },
 *                "type": {
 *                "field_name": "type",
 *                "order": 3,
 *                "label": "Type",
 *                "hint": null,
 *                "purification_rules": null,
 *                "validation_rules": "in:\"online\",\"deposit\",\"shetab\",\"sheba\",\"cheque\",\"cash\",\"pos\"|
 *                required ",
 *                "class": "form-default form-required ",
 *                "style": null,
 *                "extra": null,
 *                "is_required": true,
 *                "type": "input"
 *           },
 *                "slug": {
 *                "field_name": "slug",
 *                "order": 4,
 *                "label": "Gate",
 *                "hint": null,
 *                "purification_rules": null,
 *                "validation_rules": "in:\"pasargad\",\"saman\",\"zarinpal\"| required ",
 *                "class": " form-required ",
 *                "style": null,
 *                "extra": null,
 *                "is_required": true,
 *                "type": "input"
 *           },
 *                "name": {
 *                "field_name": "name",
 *                "order": 5,
 *                "label": "Name",
 *                "hint": null,
 *                "purification_rules": null,
 *                "validation_rules": "| required ",
 *                "class": " form-required ",
 *                "style": null,
 *                "extra": null,
 *                "is_required": true,
 *                "type": "input"
 *           }
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Payment\Http\Controllers\V1\AccountSaveController controller()
 */
class AccountFormEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Account Form";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->canAny([
             model('account')->typePermissionString('online'),
             model('account')->typePermissionString('offline'),
        ]);
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'AccountSaveController@form';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'title'    =>
                  [
                       'field_name'         => 'title',
                       'order'              => 1,
                       'label'              => 'Title',
                       'hint'               => null,
                       'purification_rules' => null,
                       'validation_rules'   => '| required ',
                       'class'              => 'form-default form-required ',
                       'style'              => null,
                       'extra'              => null,
                       'is_required'        => true,
                       'type'               => 'input',
                  ],
             'currency' =>
                  [
                       'field_name'         => 'currency',
                       'order'              => 2,
                       'label'              => 'Currency',
                       'hint'               => null,
                       'purification_rules' => null,
                       'validation_rules'   => 'in:"IRR"| required ',
                       'class'              => 'form-default form-required ',
                       'style'              => null,
                       'extra'              => null,
                       'is_required'        => true,
                       'type'               => 'input',
                  ],
             'type'     =>
                  [
                       'field_name'         => 'type',
                       'order'              => 3,
                       'label'              => 'Type',
                       'hint'               => null,
                       'purification_rules' => null,
                       'validation_rules'   => 'in:"online","deposit","shetab","sheba","cheque","cash","pos"| required ',
                       'class'              => 'form-default form-required ',
                       'style'              => null,
                       'extra'              => null,
                       'is_required'        => true,
                       'type'               => 'input',
                  ],
             'slug'     =>
                  [
                       'field_name'         => 'slug',
                       'order'              => 4,
                       'label'              => 'Gate',
                       'hint'               => null,
                       'purification_rules' => null,
                       'validation_rules'   => 'in:"pasargad","saman","zarinpal"| required ',
                       'class'              => ' form-required ',
                       'style'              => null,
                       'extra'              => null,
                       'is_required'        => true,
                       'type'               => 'input',
                  ],
             'name'     =>
                  [
                       'field_name'         => 'name',
                       'order'              => 5,
                       'label'              => 'Name',
                       'hint'               => null,
                       'purification_rules' => null,
                       'validation_rules'   => '| required ',
                       'class'              => ' form-required ',
                       'style'              => null,
                       'extra'              => null,
                       'is_required'        => true,
                       'type'               => 'input',
                  ],
        ]);
    }
}
