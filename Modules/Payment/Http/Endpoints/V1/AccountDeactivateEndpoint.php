<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/payment-account-deactivate
 *                    Account Deactivate
 * @apiDescription    Deactivate an Account
 * @apiVersion        1.0.0
 * @apiName           Account Deactivate
 * @apiGroup          Payment
 * @apiPermission     Admin
 * @apiParam {String} hashid The hashid of the account to be deactivated.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "done": 1
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Payment\Http\Controllers\V1\AccountSingleController controller()
 */
class AccountDeactivateEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Account Deactivate";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAdmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'AccountSingleController@deactivate';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => 1,
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
