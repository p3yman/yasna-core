<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/payment-account-preview
 *                    Account Preview
 * @apiDescription    Read the single resource of an existing account.
 * @apiVersion        1.0.0
 * @apiName           Account Preview
 * @apiGroup          Payment
 * @apiPermission     Admin
 * @apiParam {String} hashid The Hashid of the Desired Account
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "id": "QKgJx",
 *           "type": "online",
 *           "created_at": 1546690275,
 *           "name": "Saman Bank",
 *           "slug": "saman",
 *           "username": "saman123",
 *           "password": "saman987",
 *           "merchant_id": "1298741",
 *           "online_payment_url": "https://sep.shaparak.ir/payment.aspx",
 *           "online_verify_url": "https://sep.shaparak.ir/payments/referencepayment.asmx?wsdl",
 *           "online_refund_url": "https://google.com/refund"
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Payment\Http\Controllers\V1\AccountSingleController controller()
 */
class AccountPreviewEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Account Preview";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAdmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'AccountSingleController@preview';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'id'                 => 'QKgJx',
             'type'               => 'online',
             'created_at'         => 1546690275,
             'name'               => 'Saman Bank',
             'slug'               => 'saman',
             'username'           => 'saman123',
             'password'           => 'saman987',
             'merchant_id'        => '1298741',
             'online_payment_url' => 'https://sep.shaparak.ir/payment.aspx',
             'online_verify_url'  => 'https://sep.shaparak.ir/payments/referencepayment.asmx?wsdl',
             'online_refund_url'  => 'https://google.com/refund',
        ]);
    }
}
