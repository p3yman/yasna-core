<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/payment-account-save
 *                    Account Save
 * @apiDescription    Create a new or update an existing account. The parameters of this endpoint are acceptable with
 *                    the form trait.
 * @apiVersion        1.0.0
 * @apiName           Account Save
 * @apiGroup          Payment
 * @apiPermission     User
 * @apiParam {String} [hashid] The hashid of the account to be updated.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Payment\Http\Controllers\V1\AccountSaveController controller()
 */
class AccountSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Account Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAdmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'AccountSaveController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
