<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/payment-account-list
 *                    Account List
 * @apiDescription    Get the list of accounts with some filters
 * @apiVersion        1.0.0
 * @apiName           Account List
 * @apiGroup          Payment
 * @apiPermission     Guest
 * @apiParam {String} [type] The type to filter the result.
 * @apiParam {Boolean} [actives_only=1] Whether only active items should be included in the result.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "online": [
 *                {
 *                     "id": "aALnK",
 *                     "title": "Bank 1",
 *                     "currency": "IRR",
 *                     "type": "online",
 *                     "is_active": 1,
 *                     "created_at": 1546854397
 *                },
 *                {
 *                     "id": "kxPdx",
 *                     "title": "Bank 2",
 *                     "currency": "IRR",
 *                     "type": "online",
 *                     "is_active": 1,
 *                     "created_at": 1546856914
 *                }
 *           ],
 *           "deposit": [
 *                {
 *                     "id": "pxkYN",
 *                     "title": "Bank 3",
 *                     "currency": "IRR",
 *                     "type": "deposit",
 *                     "is_active": 1,
 *                     "created_at": 1546865735
 *                }
 *           ],
 *           "shetab": [
 *                {
 *                     "id": "dAMYA",
 *                     "title": "Bank 4",
 *                     "currency": "IRR",
 *                     "type": "shetab",
 *                     "is_active": 1,
 *                     "created_at": 1546865799
 *                }
 *           ],
 *           "sheba": [
 *                {
 *                     "id": "PAdWN",
 *                     "title": "Bank 5",
 *                     "currency": "IRR",
 *                     "type": "sheba",
 *                     "is_active": 1,
 *                     "created_at": 1546865831
 *                },
 *                {
 *                     "id": "XAGkA",
 *                     "title": "Bank 6",
 *                     "currency": "IRR",
 *                     "type": "sheba",
 *                     "is_active": 1,
 *                     "created_at": 1546870908
 *                }
 *           ]
 *      }
 * }
 * @apiErrorExample   Forbidden
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Payment\Http\Controllers\V1\AccountListController controller()
 */
class AccountListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Account List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
        return $this->user->canAny([
             model('account')->typePermissionString('online', 'browse'),
             model('account')->typePermissionString('offline', 'browse'),
        ]);
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'AccountListController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'online'  =>
                  [
                       [
                            'id'         => 'aALnK',
                            'title'      => 'Bank 1',
                            'currency'   => 'IRR',
                            'type'       => 'online',
                            'is_active'  => 1,
                            'created_at' => 1546854397,
                       ],
                       [
                            'id'         => 'kxPdx',
                            'title'      => 'Bank 2',
                            'currency'   => 'IRR',
                            'type'       => 'online',
                            'is_active'  => 1,
                            'created_at' => 1546856914,
                       ],
                  ],
             'deposit' =>
                  [
                       [
                            'id'         => 'pxkYN',
                            'title'      => 'Bank 3',
                            'currency'   => 'IRR',
                            'type'       => 'deposit',
                            'is_active'  => 1,
                            'created_at' => 1546865735,
                       ],
                  ],
             'shetab'  =>
                  [
                       [
                            'id'         => 'dAMYA',
                            'title'      => 'Bank 4',
                            'currency'   => 'IRR',
                            'type'       => 'shetab',
                            'is_active'  => 1,
                            'created_at' => 1546865799,
                       ],
                  ],
             'sheba'   =>
                  [
                       [
                            'id'         => 'PAdWN',
                            'title'      => 'Bank 5',
                            'currency'   => 'IRR',
                            'type'       => 'sheba',
                            'is_active'  => 1,
                            'created_at' => 1546865831,
                       ],
                       [
                            'id'         => 'XAGkA',
                            'title'      => 'Bank 6',
                            'currency'   => 'IRR',
                            'type'       => 'sheba',
                            'is_active'  => 1,
                            'created_at' => 1546870908,
                       ],
                  ],
        ]);
    }
}
