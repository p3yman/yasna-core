<?php

namespace Modules\Payment\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/payment-transaction-preview
 *                    Transaction Preview
 * @apiDescription    Get the single resource of a transaction.
 * @apiVersion        1.0.0
 * @apiName           Transaction Preview
 * @apiGroup          Payment
 * @apiPermission     Admin
 * @apiParam {string} hashid The hashid of the transaction
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *           "id": "qKXVA",
 *           "user_id": "qKXVA",
 *           "invoice_model": "Cart",
 *           "invoice_id": "NqVoA",
 *           "tracking_no": "1001",
 *           "client_ip": "127.0.0.1",
 *           "created_at": 1546691449,
 *           "status": "cancelled",
 *           "status_text": "Canceled",
 *           "status_applied_at": 1546691458,
 *           "type": "online",
 *           "type_title": "Online Payment",
 *           "account_id": "QKgJx",
 *           "account_name": "Saman Bank",
 *           "bank_responses": [
 *                {
 *                     "action": "callbackAction",
 *                     "response": {
 *                          "State": null,
 *                          "StateCode": null,
 *                          "ResNum": "1001",
 *                          "MID": "1298741",
 *                          "RefNum": null,
 *                          "CID": null,
 *                          "TRACENO": null,
 *                          "RRN": null,
 *                          "SecurePan": null
 *                     },
 *                     "time": "2019-01-05 16:00:58"
 *                }
 *           ]
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Payment\Http\Controllers\V1\TransactionSingleController controller()
 */
class TransactionPreviewEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Transaction Preview";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        $permission_string = model('transaction')->permissionString('browse');

        return $this
             ->user
             ->as('admin')
             ->can($permission_string)
             ;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Payment\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TransactionSingleController@preview';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'id'                => 'qKXVA',
             'user_id'           => 'qKXVA',
             'invoice_model'     => 'Cart',
             'invoice_id'        => 'NqVoA',
             'tracking_no'       => '1001',
             'client_ip'         => '127.0.0.1',
             'created_at'        => 1546691449,
             'status'            => 'cancelled',
             'status_text'       => 'Canceled',
             'status_applied_at' => 1546691458,
             'type'              => 'online',
             'type_title'        => 'Online Payment',
             'account_id'        => 'QKgJx',
             'account_name'      => 'Saman Bank',
             'bank_responses'    =>
                  [
                       [
                            'action'   => 'callbackAction',
                            'response' =>
                                 [
                                      'State'     => null,
                                      'StateCode' => null,
                                      'ResNum'    => '1001',
                                      'MID'       => '1298741',
                                      'RefNum'    => null,
                                      'CID'       => null,
                                      'TRACENO'   => null,
                                      'RRN'       => null,
                                      'SecurePan' => null,
                                 ],
                            'time'     => '2019-01-05 16:00:58',
                       ],
                  ],
        ]);
    }
}
