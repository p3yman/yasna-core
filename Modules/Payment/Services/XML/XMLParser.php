<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 26/02/2018
 * Time: 11:25 AM
 */

namespace Modules\Payment\Services\XML;

class XMLParser
{
    public static function makeTree($data)
    {
        $result = [];
        $parser = xml_parser_create();
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, $data, $values, $tags);
        xml_parser_free($parser);
        $hash_stack = [];
        foreach ($values as $key => $val) {
            switch ($val['type']) {
                case 'open':
                    array_push($hash_stack, $val['tag']);
                    break;
                case 'close':
                    array_pop($hash_stack);
                    break;
                case 'complete':
                    array_push($hash_stack, $val['tag']);
                    // uncomment to see what this function is doing
//                    echo("\$result['" . implode($hash_stack, "']['") . "'] = '{$val['value']}';\n");
                    eval("\$result['" . implode($hash_stack, "']['") . "'] = '{$val['value']}';");
                    array_pop($hash_stack);
                    break;
            }
        }
        return $result;
    }
}
