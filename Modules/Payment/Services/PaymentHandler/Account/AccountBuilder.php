<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 12:16 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Account;

use App\Models\Account;
use Illuminate\Database\Eloquent\Builder;
use Modules\Payment\Services\PaymentHandler\Payment\PaymentTools;

/**
 * Class AccountBuilder
 *
 * @package Modules\Payment\Services\PaymentHandler\Account
 *
 * @method \Illuminate\Database\Eloquent\Collection get($columns = ['*'])
 * @method \Illuminate\Contracts\Pagination\LengthAwarePaginator paginate($perPage = null, $columns = ['*'], $pageName = 'page', $page = null)
 * @method \App\Models\Account|null first
 * @method int count
 *
 * @method $this online
 * @method $this deposit
 * @method $this shetab
 * @method $this sheba
 * @method $this cheque
 * @method $this cash
 * @method $this pos
 *
 */
class AccountBuilder
{
    protected $builder;

    protected static $outputMethods = ['get', 'paginate', 'first', 'count'];

    protected $showUnpublishedAccounts = false;
    protected $order                   = ['order', 'asc'];

    public function __construct()
    {
        $this->builder = Account::select();
    }

    public function __call($method, $parameters)
    {
        if (PaymentTools::paymentTypeExists($method)) {
            return $this->whereType($method);
        }

        if (self::isOutputMethod($method)) {
            return $this->callOutputMethod($method, ...$parameters);
        } else {
            $this->builder->$method(...$parameters);
        }

        return $this;
    }

    public function orderBy(...$parameters)
    {
        $this->order = $parameters;
        return $this;
    }

    public function currency($currency)
    {
        return $this->whereCurrency($currency);
    }

    public function showUnpublishedAccounts($show = true)
    {
        $this->showUnpublishedAccounts = $show;
        return $this;
    }

    public function showUnpublished(...$parameters)
    {
        return $this->showUnpublishedAccounts(...$parameters);
    }

    public function withUnpublished(...$parameters)
    {
        return $this->showUnpublishedAccounts(...$parameters);
    }

    protected function makeQueryReady()
    {
        if (!$this->showUnpublishedAccounts) {
            $this->builder->whereNotNull('published_at');
        }

        $this->builder->orderBy(...$this->order);
        return $this;
    }

    protected function callOutputMethod(string $method, ...$parameters)
    {
        return $builder = $this->makeQueryReady()->builder->$method(...$parameters);
    }

    protected static function getOutputMethods()
    {
        return self::$outputMethods;
    }

    protected static function isOutputMethod(string $method)
    {
        return in_array($method, self::getOutputMethods());
    }
}
