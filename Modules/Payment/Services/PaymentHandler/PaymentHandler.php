<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 11:34 AM
 */

namespace Modules\Payment\Services\PaymentHandler;

use BadMethodCallException;
use ErrorException;
use http\Exception;
use Illuminate\Database\Query\Builder;
use Modules\Payment\Services\PaymentHandler\Account\AccountBuilder;
use Modules\Payment\Services\PaymentHandler\Search\SearchBuilder;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;

/**
 * Class PaymentHandler
 *
 * @package Modules\Payment\Services\PaymentHandler
 *
 */
class PaymentHandler
{
    public function __call($method, $parameters)
    {
        if (method_exists($this, $method)) {
            return $this->$method(...$parameters);
        }

        throw new BadMethodCallException(
            sprintf('Call to undefined method %s::%s()', get_class($this), $method)
        );
    }

    public static function __callStatic($method, $parameters)
    {
        return (new self)->$method(...$parameters);
    }

    /*
    |--------------------------------------------------------------------------
    | Protected Mirrors
    |--------------------------------------------------------------------------
    |
    */

    public function accounts()
    {
        return new AccountBuilder();
    }

    public function transaction()
    {
        return new Transaction();
    }

    public function invoice($invoiceId)
    {
        return (new SearchBuilder)->invoiceId($invoiceId);
    }

    public function search()
    {
        return new SearchBuilder;
    }
}
