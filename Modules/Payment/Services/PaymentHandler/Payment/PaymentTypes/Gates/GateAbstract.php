<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 18/12/2017
 * Time: 04:53 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates;

use App\Models\Account;
use App\Models\Transaction as TransactionModel;
use Carbon\Carbon;
use Modules\Manage\Services\Widget;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;

abstract class GateAbstract implements GateInterface
{
    /** @var Transaction */
    protected $transaction;

    /** @var array */
    protected $steps_map = [
         'fire'   => 'online_payment_url',
         'check'  => 'online_check_url',
         'verify' => 'online_verify_url',
         'refund' => 'online_refund_url',
    ];

    protected static $additionalFieldsDefaultWidget = 'input';



    /**
     * GateAbstract constructor.
     *
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }



    /**
     * Returns the transaction model instance.
     *
     * @return TransactionModel
     */
    protected function getModel()
    {
        return $this->transaction->getModel()->spreadMeta();
    }



    /**
     * Refreshes the transaction model instance.
     *
     * @return $this
     */
    protected function refreshModel()
    {
        $this->transaction->refreshModel();
        return $this;
    }



    /**
     * Returns the account model instance.
     *
     * @return Account
     */
    protected function getAccountModel()
    {
        return $this->transaction->getAccountModel()->spreadMeta();
    }



    /**
     * Saves the response.
     *
     * @param string $action
     * @param mixed  $response
     *
     * @return $this
     */
    protected function saveResponse($action, $response)
    {
        $this->transaction->saveBankResponse([
             'action'   => $action,
             'response' => $response,
             'time'     => Carbon::now()->toDateTimeString(),
        ]);

        return $this;
    }



    /**
     * Returns the callback URL.
     *
     * @return string
     */
    protected function callBackUrl()
    {
        return route('payment.callback', ['hashid' => $this->getModel()->hashid]);
    }



    /**
     * Returns the URL to for the given step.
     *
     * @param string $step
     *
     * @return mixed
     */
    protected function getStepUrl($step)
    {
        return $this->getAccountModel()[$this->steps_map[$step]];
    }



    /**
     * Generates and returns a response based in the given status and additional data.
     *
     * @param int|string $status
     * @param array      $additional_data
     *
     * @return TransactionResponse
     */
    protected static function generateResponse($status, $additional_data = [])
    {
        if (!is_numeric($status) and is_string($status)) {
            $status = TransactionResponse::findStatusBySlug($status);
        }

        return (new TransactionResponse($status, $additional_data));
    }



    /**
     * Returns an array of the additional fields.
     *
     * @return array
     */
    public static function additionalFields()
    {
        $fields = static::additionalFieldsBasic();
        foreach ($fields as $key => $value) {
            if (is_numeric($key)) {
                unset($fields[$key]);
                $fields[$value] = [];
            } elseif (is_string($value)) {
                $fields[$key] = explode('|', $value);
            } elseif (
                 (!is_array($value)) and
                 (
                      (!is_object($value) and settype($value, 'string') !== false) or
                      (is_object($value) and method_exists($value, '__toString'))
                 )
            ) {
                $fields[$key] = [$value];
            }
        }
        return $fields;
    }



    /**
     * Returns an array of the basic additional fields.
     *
     * @return array
     */
    protected static function additionalFieldsBasic()
    {
        return [];
    }



    /**
     * Returns the widget name for the given field name.
     *
     * @param string $field
     *
     * @return string
     */
    public static function additionalFieldWidgetName(string $field)
    {
        return array_key_exists($field, $fields = static::additionalFieldSpecificWidgetsNames())
             ? $fields[$field]
             : static::$additionalFieldsDefaultWidget;
    }



    /**
     * Returns the widget names for the specific additional fields.
     *
     * @return array
     */
    public static function additionalFieldSpecificWidgetsNames()
    {
        return [];
    }



    /**
     * If this account has an additional field with given name.
     *
     * @param string $field
     *
     * @return bool
     */
    public static function hasAdditionalField(string $field)
    {
        return array_key_exists($field, static::additionalFields());
    }



    /**
     * Returns the widget for the given field name.
     *
     * @param string $field
     *
     * @return Widget|null
     */
    public static function additionalFieldWidget(string $field)
    {
        $widgets = static::additionalFieldSpecificWidgets();

        return ($widgets[$field] ?? null);
    }



    /**
     * Returns the widget for the specific additional fields.
     *
     * @return array|Widget[]
     */
    public static function additionalFieldSpecificWidgets()
    {
        return [];
    }
}
