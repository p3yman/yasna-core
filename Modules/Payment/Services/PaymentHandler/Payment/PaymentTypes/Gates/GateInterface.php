<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 18/12/2017
 * Time: 04:52 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates;

use Illuminate\Http\RedirectResponse;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;

interface GateInterface
{
    public function __construct(Transaction $transaction);



    /**
     * Fires the transaction.
     *
     * @return TransactionResponse|mixed
     */
    public function fire();



    /**
     * Returns an array of the data to be used while firing a transaction form the front site.
     *
     * @return array
     */
    public function fireForEndpoint(): array;



    /**
     * Returns the callback action.
     *
     * @return TransactionResponse|RedirectResponse
     */
    public function callbackAction();



    /**
     * Verifies the transaction.
     *
     * @return TransactionResponse
     */
    public function verify();



    /**
     * Refunds the transaction.
     *
     * @return TransactionResponse
     */
    public function refund();
}
