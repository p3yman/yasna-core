<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 18/12/2017
 * Time: 04:42 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Modules\Payment\Providers\PaymentServiceProvider;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction as TransactionHandler;

class Saman extends GateAbstract
{
    /**
     * Saman constructor.
     *
     * @param TransactionHandler $transaction
     */
    public function __construct(TransactionHandler $transaction)
    {
        parent::__construct($transaction);
    }



    /**
     * Fires the transaction.
     *
     * @return Response|TransactionResponse
     */
    public function fire()
    {
        if (($statusSlug = $this->transaction->status()['slug']) == 'created') {
            $model   = $this->getModel();
            $account = $this->getAccountModel();

            $model->applyProceededStatus();

            return response()->view('payment::saman.form', [
                 'action'         => $account->online_payment_url,
                 'amount'         => $model->payable_amount,
                 'merchantId'     => $account->merchant_id,
                 'trackingNumber' => $model->tracking_no,
                 'callback'       => $this->callBackUrl(),
            ]);
        } else {
            return self::generateResponse($statusSlug);
        }
    }



    /**
     * Returns the need to fire a transaction by the front side.
     *
     * @return array
     */
    public function fireForEndpoint(): array
    {
        $status_slug = $this->transaction->statusSlug();

        if ($status_slug != 'created') {
            return static::generateResponse($status_slug)->render();
        }

        $model   = $this->getModel();
        $account = $this->getAccountModel();

        $model->applyProceededStatus();

        return PaymentServiceProvider::endpointFormResponse(
             $account->online_payment_url,
             'POST',
             [
                  'Amount'      => $model->payable_amount,
                  'MID'         => $account->merchant_id,
                  'ResNum'      => $model->tracking_no,
                  'RedirectURL' => $model->callback_url,
             ]
        );
    }



    /**
     * Does the callback action.
     *
     * @return array|RedirectResponse|Redirector|TransactionResponse
     */
    public function callbackAction()
    {
        $model = $this->getModel();

        if ($this->refNumIsUnique()) {
            $this->saveResponse(__FUNCTION__, request()->all());

            if ($this->stateIsOk()) {
                $model->applyDeclaredStatus([
                     'bank_reference_no' => request('RefNum'),
                ]);

                if ($model->auto_verify) {
                    $this->verify();
                }
            } else {
                $this->transaction->makeCancelled();
            }
        }

        return $this->transaction->redirectToCallback();
    }



    /**
     * Whether the state is ok.
     *
     * @return bool
     */
    protected function stateIsOk()
    {
        return (strtoupper(request('State')) == 'OK');
    }



    /**
     * Whether the ref num is unique.
     *
     * @return bool
     */
    protected function refNumIsUnique()
    {
        return !boolval(
             ($refNum = request('RefNum')) and
             Transaction::where('bank_reference_no', $refNum)->first()
        );
    }



    /**
     * Verifies the transaction.
     *
     * @return TransactionResponse
     */
    public function verify()
    {
        $model   = $this->getModel();
        $refNum  = $model->bank_reference_no;
        $account = $this->getAccountModel();
        $wsdlURL = $account->online_verify_url;

        $soap = new \SoapClient($wsdlURL);
        $res  = $soap->VerifyTransaction($refNum, $account->merchant_id);

        $this->saveResponse(__FUNCTION__, $res);
        if ($res and $res > 0) {
            $updateData = [
                 'paid_amount' => $res,
            ];

            if ($model->payable_amount == $res) {
                $this->transaction->makeVerified();
            }

            $model->update($updateData);
            return self::generateResponse('successfully-verified');
        } else {
            $this->transaction->makeRejected(Carbon::now(), $res);

            return self::generateResponse('unverifiable');
        }
    }



    /**
     * Refunds the transaction.
     *
     * @return TransactionResponse
     */
    public function refund()
    {
        return self::generateResponse('non-refundable');
    }



    /**
     * Returns the basic of the additional fields.
     *
     * @return array
     */
    protected static function additionalFieldsBasic()
    {
        return [
             'username'           => 'required',
             'password'           => 'required',
             'merchant_id'        => ['required', 'numeric'],
             'online_payment_url' => 'required|url',
             'online_verify_url'  => 'required|url',
             'online_refund_url'  => 'url',
        ];
    }
}
