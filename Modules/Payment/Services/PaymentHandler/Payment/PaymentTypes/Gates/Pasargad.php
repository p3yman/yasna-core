<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 25/02/2018
 * Time: 04:34 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Modules\Payment\Providers\PaymentServiceProvider;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;
use Modules\Payment\Services\RSA\RSAKeyType;
use Modules\Payment\Services\RSA\RSAProcessor;
use Modules\Payment\Services\XML\XMLParser;

class Pasargad extends GateAbstract
{
    /**
     * An Array of the Actions and their Codes
     *
     * @var array
     */
    protected static $action_codes = [
         'purchase' => "1003",
         'refund'   => "1004",
    ];

    /**
     * Current Action in a Process
     *
     * @var string
     */
    protected $current_action;

    /**
     * Returns the Default Format of the Datetime Values
     *
     * @var string
     */
    protected static $default_datetime_format = 'Y/m/d H:i:s';



    /**
     * Fires the transaction.
     *
     * @return Response|TransactionResponse
     */
    public function fire()
    {
        if (($statusSlug = $this->transaction->status()['slug']) == 'created') {
            $model   = $this->getModel();
            $account = $this->getAccountModel();
            $this->setCurrentAction('purchase');

            $model->applyProceededStatus();

            $signValues = [
                 'merchantCode'   => $account->merchant_id,
                 'terminalCode'   => $account->terminal_id,
                 'trackingNumber' => $model->tracking_no,
                 'invoiceDate'    => $model->created_at->format(self::$default_datetime_format),
                 'amount'         => $model->payable_amount,
                 'callback'       => $this->callBackUrl(),
                 'action'         => $this->getCurrentActionCode(),
                 'timeStamp'      => Carbon::now()->format(self::$default_datetime_format),
            ];

            return response()->view('payment::pasargad.form', array_merge($signValues, [
                 'formAction' => $this->getStepUrl(__FUNCTION__),
                 'sign'       => $this->generateSign($signValues),
            ]));
        } else {
            return self::generateResponse($statusSlug);
        }
    }



    /**
     * Returns the need to fire a transaction by the front side.
     *
     * @return array
     */
    public function fireForEndpoint(): array
    {
        $status_slug = $this->transaction->statusSlug();

        if ($status_slug != 'created') {
            return static::generateResponse($status_slug)->render();
        }

        $this->setCurrentAction('purchase');

        $model = $this->getModel();
        $model->applyProceededStatus();

        $form_data = $this->generateFormDataForEndpoint();


        return PaymentServiceProvider::endpointFormResponse(
             $this->getStepUrl('fire'),
             'POST',
             $form_data
        );
    }



    /**
     * Generates and returns the form data for the `fireForEndpoint()` method.
     *
     * @return array
     */
    protected function generateFormDataForEndpoint()
    {
        $account     = $this->getAccountModel();
        $model       = $this->getModel();
        $sign_values = $this->generatePurchaseSignValues();
        $sign        = $this->generateSign($sign_values);


        return [
             'merchantCode'    => $account->merchant_id,
             'terminalCode'    => $account->terminal_id,
             'invoiceNumber'   => $model->tracking_no,
             'invoiceDate'     => $model->created_at->format(self::$default_datetime_format),
             'amount'          => $model->payable_amount,
             'redirectAddress' => $model->callback_url,
             'action'          => $this->getCurrentActionCode(),
             'timeStamp'       => Carbon::now()->format(self::$default_datetime_format),
             'sign'            => $sign,
        ];
    }



    /**
     * Generates and returns the sing value for the purchase process.
     *
     * @return array
     */
    protected function generatePurchaseSignValues()
    {
        $model   = $this->getModel();
        $account = $this->getAccountModel();

        return [
             'merchantCode'   => $account->merchant_id,
             'terminalCode'   => $account->terminal_id,
             'trackingNumber' => $model->tracking_no,
             'invoiceDate'    => $model->created_at->format(self::$default_datetime_format),
             'amount'         => $model->payable_amount,
             'callback'       => $model->callback_url,
             'action'         => $this->getCurrentActionCode(),
             'timeStamp'      => Carbon::now()->format(self::$default_datetime_format),
        ];
    }



    /**
     * Does the callback action.
     *
     * @return array|RedirectResponse|Redirector|TransactionResponse
     * @throws GuzzleException
     */
    public function callbackAction()
    {
        $this->callbackGetResult();
        return $this->transaction->redirectToCallback();
    }



    /**
     * Does the process of the callback action and saves the bank response.
     *
     * @throws GuzzleException
     */
    protected function callbackGetResult()
    {
        $this->saveResponse('callback', $requestArr = $this->callbackGetQueryArray())
             ->callbackPaymentResult($requestArr)
        ;
    }



    /**
     * Returns an array made of the query string to be used in the callback action.
     *
     * @return array
     */
    protected function callbackGetQueryArray()
    {
        return [
             'invoiceNumber'          => request('iN'),
             'invoiceDate'            => request('iD'),
             'transactionReferenceID' => request('tref'),
        ];
    }



    /**
     * Returns the payment result in the callback action.
     *
     * @param array $data
     *
     * @throws GuzzleException
     */
    protected function callbackPaymentResult(array $data)
    {
        $objIndex = 'resultObj';
        $model    = $this->getModel();
        $account  = $this->getAccountModel();

        // Check response correctness
        if (
             ($rs = $this->callCurl('check', ['invoiceUID' => $data['transactionReferenceID']])) and
             is_array($rs) and
             array_key_exists($objIndex, $rs) and
             is_array($rs[$objIndex]) and
             ($resultObj = collect($rs[$objIndex]))
        ) {
            // Check response validity
            if (
                 ($resultObj->get('invoiceNumber') == $model->tracking_no) and
                 ($resultObj->get('invoiceDate') == $model->created_at->format(self::$default_datetime_format)) and
                 ($resultObj->get('terminalCode') == $account->terminal_id) and
                 ($resultObj->get('merchantCode') == $account->merchant_id)
            ) {
                // Check payment result
                if (
                     ($resultObj->get('action') == $this->getActionCode('purchase')) and
                     ($result = $resultObj->get('result')) and
                     (strtolower($result) != 'false')
                ) {
                    $model->applyDeclaredStatus([
                         'bank_reference_no' => $data['transactionReferenceID'],
                         'paid_amount'       => $resultObj->get('amount'),
                    ]);
                    $this->autoVerify();
                } else {
                    $this->transaction->makeCancelled();
                }
            }
        }
    }



    /**
     * Calls the `verify()` method if the auto verify has been enabled in the transaction.
     *
     * @return $this
     * @throws GuzzleException
     */
    protected function autoVerify()
    {
        if ($this->getModel()->auto_verify) {
            $this->refreshModel()->verify();
        }

        return $this;
    }



    /**
     * Verifies the transaction.
     *
     * @return TransactionResponse
     * @throws GuzzleException
     */
    public function verify()
    {
        if ($this->transaction->statusSlug() == 'declared') {
            $objIndex   = 'actionResult';
            $model      = $this->getModel();
            $account    = $this->getAccountModel();
            $signValues = [
                 'merchantCode'  => $account->merchant_id,
                 'terminalCode'  => $account->terminal_id,
                 'invoiceNumber' => $model->tracking_no,
                 'invoiceDate'   => $model->created_at->format(self::$default_datetime_format),
                 'amount'        => $model->payable_amount,
                 'timeStamp'     => Carbon::now()->format(self::$default_datetime_format),
            ];
            $rs         = $this->callCurl(
                 'verify',
                 array_merge($signValues, ['sign' => $this->generateSign($signValues)])
            );

            if (
                // Check response correctness
                 $rs and
                 is_array($rs) and
                 array_key_exists($objIndex, $rs) and
                 is_array($rs[$objIndex]) and
                 ($resultObj = collect($rs[$objIndex]))
            ) {
                // Check verification result
                if (($result = $resultObj->get('result')) and (strtolower($result) != 'false')) {
                    // Check payment amount
                    if ($model->payable_amount == $model->paid_amount) {
                        $this->transaction->makeVerified();
                        return self::generateResponse('successfully-verified');
                    }
                } else {
                    $this->transaction->makeRejected(Carbon::now(), $resultObj->get('resultMessage'));
                }
            }
        }

        return self::generateResponse('unverifiable');
    }



    /**
     * Refunds the transaction.
     *
     * @return TransactionResponse
     * @throws GuzzleException
     */
    public function refund()
    {
        $objIndex   = 'actionResult';
        $model      = $this->getModel();
        $account    = $this->getAccountModel();
        $signValues = [
             'merchantCode'  => $account->merchant_id,
             'terminalCode'  => $account->terminal_id,
             'invoiceNumber' => $model->tracking_no,
             'invoiceDate'   => $model->created_at->format(self::$default_datetime_format),
             'amount'        => $model->payable_amount,
             'action'        => $this->getActionCode('refund'),
             'timeStamp'     => Carbon::now()->format(self::$default_datetime_format),
        ];
        $rs         = $this->callCurl(
             'refund',
             array_merge($signValues, ['sign' => $this->generateSign($signValues)])
        );

        if (
            // Check response correctness
             $rs and
             is_array($rs) and
             array_key_exists($objIndex, $rs) and
             is_array($rs[$objIndex]) and
             ($resultObj = collect($rs[$objIndex]))
        ) {
            // Check verification result
            if (($result = $resultObj->get('result')) and (strtolower($result) != 'false')) {
                $this->transaction->makeRefunded();
                return self::generateResponse('successfully-refunded');
            }
        }

        return self::generateResponse('non-refundable');
    }



    /**
     * Triggers the CURL request.
     *
     * @param string $step
     * @param array  $parameters
     * @param array  $headers
     * @param string $method
     *
     * @return array|null
     * @throws GuzzleException
     */
    protected function callCurl(string $step, array $parameters = [], array $headers = [], $method = 'post')
    {
        $client = new Client;

        $response = $client->request('POST', $this->getStepUrl($step), [
             'headers'     => array_merge([
                  'User-Agent' => request()->userAgent(),
                  'Accept'     => 'text/xml',
             ], $headers),
             'form_params' => $parameters,
             'exceptions'  => false,
        ]);
        if ($response->getStatusCode() === 200) {
            $rsTxt = $response->getBody()->getContents();

            // Store received response
            $this->saveResponse($step, $rsTxt);

            return XMLParser::makeTree($rsTxt);
        } else {
            $this->saveResponse($step, [
                 'status'  => $response->getStatusCode(),
                 'content' => $response->getBody()->getContents(),
            ]);

            return null;
        }
    }



    /**
     * Returns the code of the given action.
     *
     * @param string $action
     *
     * @return string
     */
    protected function getActionCode($action)
    {
        return self::$action_codes[$action];
    }



    /**
     * Returns the value of the `$current_action` property.
     *
     * @return string
     */
    protected function getCurrentActionCode()
    {
        return $this->getActionCode($this->current_action);
    }



    /**
     * Sets the value of the `$current_action` property.
     *
     * @param string $current_action
     *
     * @return $this
     */
    protected function setCurrentAction($current_action)
    {
        if (array_key_exists($current_action, self::$action_codes)) {
            $this->current_action = $current_action;
        }
        return $this;
    }



    /**
     * Generates a sign with the given data and returns it.
     *
     * @param array $array
     *
     * @return string
     */
    protected function generateSign(array $array = [])
    {
        $processor = new RSAProcessor($this->getAccountModel()->private_key, RSAKeyType::XMLString);
        $data      = "#" . implode('#', $array) . "#";
        $data      = sha1($data, true);
        $data      = $processor->sign($data);
        return base64_encode($data);
    }



    /**
     * Returns the basic additional fields.
     *
     * @return array
     */
    protected static function additionalFieldsBasic()
    {
        return [
             'merchant_id'        => ['required', 'numeric'],
             'terminal_id'        => ['required', 'numeric'],
             'private_key'        => 'required',
             'public_key'         => 'required',
             'online_payment_url' => 'required|url',
             'online_check_url'   => 'required|url',
             'online_verify_url'  => 'required|url',
             'online_refund_url'  => 'required|url',
        ];
    }



    /**
     * Returns an array of the additional fields which has specific widgets.
     *
     * @return array
     */
    public static function additionalFieldSpecificWidgetsNames()
    {
        return [
             'private_key' => 'textarea',
             'public_key'  => 'textarea',
        ];
    }
}
