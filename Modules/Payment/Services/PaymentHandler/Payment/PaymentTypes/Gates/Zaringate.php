<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 10/27/18
 * Time: 3:35 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates;


use Illuminate\Validation\Rule;
use Modules\Manage\Services\Widgets\Combo;
use Modules\Payment\Providers\PaymentServiceProvider;
use Modules\Payment\Services\EasyClient;

class Zaringate extends GateAbstract
{
    private $account;
    private $transaction_model;



    /**
     * trigger zarinpal gateway
     */
    public function fire()
    {
        $this->account           = $this->getAccountModel();
        $this->transaction_model = $this->getModel();
        $client                  = $this->getAuthorityCode();

        if (!$this->checkStatusTransAction()) {
            return self::generateResponse($this->transaction->statusSlug());
        }

        if ($client->getError()) {
            return self::generateResponse('connection-failed', ['data' => $client->getError()]);
        } else {
            /**
             * change status to waiting for gateway
             */
            $this->transaction->getModel()->applyProceededStatus();
            return $this->connectToGateway($client);
        }
    }



    /**
     * Returns the need to fire a transaction by the front side.
     *
     * @return array
     */
    public function fireForEndpoint(): array
    {
        $this->account           = $this->getAccountModel();
        $this->transaction_model = $this->getModel();
        $client                  = $this->getAuthorityCodeForEndpoint();

        if (!$this->checkStatusTransAction()) {
            return self::generateResponse($this->transaction->statusSlug())->render();
        }

        if ($client->getError()) {
            return self::generateResponse('connection-failed', ['data' => $client->getError()])->render();
        } else {
            /**
             * change status to waiting for gateway
             */
            $this->transaction->getModel()->applyProceededStatus();

            return $this->fireForEndpointResult($client);
        }
    }



    /**
     * get Authority code for connect gateway via SPA
     *
     * @return EasyClient
     */
    private function getAuthorityCodeForEndpoint()
    {
        $account     = $this->account;
        $transaction = $this->transaction_model;
        $client      = new EasyClient($account->payment_url);
        $client->setUserAgent('ZarinPal')
               ->setParameter('MerchantID', $account->merchant_id)
               ->setParameter('Amount', $this->rialToToman($transaction->payable_amount))
               ->setParameter('CallbackURL', $transaction->callback_url)
               ->setParameter('Description', $account->description)
               ->post()
        ;
        return $client;
    }



    /**
     * Returns the form data to connect to the Zarinpal gate.
     *
     * @param EasyClient $client
     *
     * @return array
     */
    protected function fireForEndpointResult(EasyClient $client): array
    {
        $result = json_decode($client->getResult(), true);

        if ($result['Status'] == 100) {
            $form_action = $this->account->payment_start_url . $result["Authority"];

            return PaymentServiceProvider::endpointFormResponse($form_action);
        } else {
            return self::generateResponse('gateway-error', ['gateway_result' => $result])->render();
        }
    }



    /**
     * check status of transaction that has to be created
     *
     * @return bool
     */
    private function checkStatusTransAction()
    {
        if ($this->transaction->statusSlug() == "created") {
            return true;
        }

        return false;
    }



    /**
     * get Authority code for connect gateway
     *
     * @return EasyClient
     */
    private function getAuthorityCode()
    {
        $account     = $this->account;
        $transaction = $this->transaction_model;
        $client      = new EasyClient($account->payment_url);
        $client->setUserAgent('ZarinPal')
               ->setParameter('MerchantID', $account->merchant_id)
               ->setParameter('Amount', $this->rialToToman($transaction->payable_amount))
               ->setParameter('CallbackURL', route('payment.callback', ['hashid' => $transaction->hashid]))
               ->setParameter('Description', $account->description)
               ->post()
        ;
        return $client;
    }



    /**
     * connect to zarinpal gateway
     *
     * @param EasyClient $client
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function connectToGateway($client)
    {
        $result = json_decode($client->getResult(), true);
        if ($result['Status'] == 100) {
            return response()->redirectTo($this->account->payment_start_url . $result["Authority"]);
        } else {
            return self::generateResponse('gateway-error', ['gateway_result' => $result]);
        }
    }



    /**
     * convert rial to toman
     *
     * @param int $amount
     *
     * @return float|int
     */
    private function rialToToman($amount)
    {
        return $amount / 10;
    }



    /**
     * zarinpal callback action
     */
    public function callbackAction()
    {
        $this->saveResponse(__FUNCTION__, request()->all());
        if (request('Status') == 'NOK') {
            $this->transaction->makeCancelled();
            return $this->transaction->redirectToCallback();
        }
        return $this->verifyCallBack(request('Authority'));
    }



    /**
     * make verify request
     *
     * @param string $authority
     *
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function verifyCallBack($authority)
    {
        $account = $this->getAccountModel();
        $client  = new EasyClient($account->verify_url);
        $client->setParameter('Amount', $this->rialToToman($this->transaction->getModel()->payable_amount))
               ->setParameter('Authority', $authority)
               ->setParameter('MerchantID', $account->merchant_id)
               ->post()
        ;

        if ($client->getError()) {
            $this->transaction->makeCancelled();
            return $this->transaction->redirectToCallback();
        } else {
            $res = $this->parseVerifyResult($client);
            if ($res['status'] != 'ok') {
                $this->transaction->makeRejected();
            } else {
                $this->transaction->makeVerified();
            }
            return $this->transaction->redirectToCallback();
        }
    }



    /**
     * parse and return status of verify request
     *
     * @param EasyClient $client
     *
     * @return array
     */
    public function parseVerifyResult($client)
    {
        $result = json_decode($client->getResult(), true);
        if ($result['Status'] == 100) {
            return ['status' => "ok", 'data' => $result];
        } else {
            return ['status' => 'nok', 'data' => $result];
        }
    }



    /**
     * add additional fields which this gateway require
     *
     * @return array
     */
    protected static function additionalFieldsBasic()
    {
        return [
             'referral_gateway'  => 'required|' . Rule::in(static::availableReferralGateways()),
             'description'       => 'required',
             'merchant_id'       => ['required'],
             'payment_url'       => 'required|url',
             'payment_start_url' => 'required|url',
             'verify_url'        => 'required|url',
        ];
    }



    /**
     * @inheritdoc
     */
    public static function additionalFieldSpecificWidgets()
    {
        return [
             'referral_gateway' => static::getReferralGatewayWidget(),
        ];
    }



    /**
     * Returns a widget for selecting the referral gateway.
     *
     * @return Combo
     */
    protected static function getReferralGatewayWidget()
    {
        $value_field   = 'id';
        $caption_field = 'title';
        $options       = static::availableReferralGatewaysCombo($value_field, $caption_field);

        return widget('combo')
             ->valueField($value_field)
             ->captionField($caption_field)
             ->options($options)
             ;
    }



    /**
     * Returns an array of the available referral gateways.
     *
     * @return array
     */
    protected static function availableReferralGateways()
    {
        return [
             'ZarinGate',
             'Asan',
             'Sep',
             'Sad',
             'Pec',
             'Fan',
        ];
    }



    /**
     * Returns a combo array for selecting the referral gateway.
     *
     * @param string $value_field
     * @param string $caption_field
     *
     * @return array
     */
    protected static function availableReferralGatewaysCombo(
         string $value_field = 'id',
         string $caption_field = 'title'
    ) {
        $options = static::availableReferralGateways();
        $result  = [];

        foreach ($options as $option) {
            $result[] = [
                 $value_field   => $option,
                 $caption_field => module('payment')->getTrans("zaringate.referral_gateway.$option"),
            ];
        }

        return $result;
    }



    public function verify()
    {
        // TODO: Implement verify() method.
    }



    public function refund()
    {
        // TODO: Implement refund() method.
    }
}
