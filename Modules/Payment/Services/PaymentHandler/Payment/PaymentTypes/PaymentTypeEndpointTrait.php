<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/14/19
 * Time: 10:24 AM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes;


use Modules\Payment\Providers\PaymentServiceProvider;

trait PaymentTypeEndpointTrait
{
    /**
     * Returns the need to fire a transaction by the front side.
     *
     * @return array
     */
    public function fireForEndpoint()
    {
        return PaymentServiceProvider::endpointFormResponse($this->getModel()->callback_url, 'GET');
    }
}
