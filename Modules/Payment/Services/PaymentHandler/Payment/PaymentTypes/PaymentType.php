<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 18/12/2017
 * Time: 01:28 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes;

use App\Models\Account;
use App\Models\Transaction as TransactionModel;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;

abstract class PaymentType implements PaymentTypeInterface
{
    use PaymentTypeEndpointTrait;


    /**
     * The Transaction in User
     *
     * @var Transaction
     */
    protected $transaction;



    /**
     * PaymentType constructor.
     *
     * @param Transaction $transaction
     */
    public function __construct(Transaction &$transaction)
    {
        $this->transaction = $transaction;
    }



    /**
     * Returns the model instance.
     *
     * @return TransactionModel
     */
    protected function getModel()
    {
        return $this->transaction->getModel()->spreadMeta();
    }



    /**
     * Returns the account model instance.
     *
     * @return Account
     */
    protected function getAccountModel()
    {
        return $this->transaction->getAccountModel()->spreadMeta();
    }



    /**
     * Fires the transaction and returns a returnable response.
     *
     * @return TransactionResponse
     */
    public function fire()
    {
        $model = $this->getModel();

        return self::generateResponse('returnable-response', [
             'response' => redirect($model->callback_url),
        ]);
    }



    /**
     * Verifies the transaction.
     *
     * @return TransactionResponse
     */
    public function verify()
    {
        return self::generateResponse('unverifiable');
    }



    /**
     * Refunds the transaction.
     *
     * @return TransactionResponse
     */
    public function refund()
    {
        return self::generateResponse('non-refundable');
    }



    /**
     * Returns the callback action.
     *
     * @return array|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callbackAction()
    {
        return $this->transaction->redirectToCallback();
    }



    /**
     * Returns an array of the validation rules to be used in the forms.
     *
     * @param string $method
     *
     * @return array
     */
    public function validationRules(string $method = '')
    {
        return array_merge_recursive(
             $this->generalValidationRules(),
             $this->methodValidationRules($method)
        );
    }



    /**
     * Returns the general validation rules to be used in the forms.
     *
     * @return array
     */
    public function generalValidationRules()
    {
        return [];
    }



    /**
     * Returns the method validation rules to be used in the forms.
     *
     * @param string $method
     *
     * @return array
     */
    public function methodValidationRules(string $method)
    {
        return [];
    }



    /**
     * Whether the res num may be set.
     *
     * @return bool
     */
    public static function canSetResNum()
    {
        return true;
    }



    /**
     * Generates response based on the given data.
     *
     * @param string $status
     * @param array  $additional_data
     *
     * @return TransactionResponse
     */
    protected static function generateResponse($status, $additional_data = [])
    {
        if (!is_numeric($status) and is_string($status)) {
            $status = TransactionResponse::findStatusBySlug($status);
        }

        return (new TransactionResponse($status, $additional_data));
    }
}
