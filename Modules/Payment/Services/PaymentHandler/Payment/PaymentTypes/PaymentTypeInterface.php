<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 18/12/2017
 * Time: 01:31 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes;

interface PaymentTypeInterface
{
    public function fire();

    public function callbackAction();

    public function validationRules(string $method = '');

    public function generalValidationRules();

    public function methodValidationRules(string $method);

    public function verify();

    public function refund();
}
