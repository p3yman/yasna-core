<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 18/12/2017
 * Time: 11:08 AM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes;

use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates\GateAbstract;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;

class Online extends PaymentType
{
    /**
     * Calls the `fire()` method of the gateway and returns its result.
     *
     * @return TransactionResponse
     */
    public function fire()
    {
        $gateway = $this->getGatewayClass();
        return (new  $gateway($this->transaction))->fire();
    }



    /**
     * Calls the `fireForEndpoint()` method of the gateway and returns its result.
     *
     * @return array
     */
    public function fireForEndpoint()
    {
        return $this->getGatewayInstance()->fireForEndpoint();
    }



    /**
     * Calls the `verify()` method of the gateway and returns its result.
     *
     * @return TransactionResponse
     */
    public function verify()
    {
        $gateway = $this->getGatewayClass();
        return (new  $gateway($this->transaction))->verify();
    }



    /**
     * Calls the `refund()` method of the gateway and returns its result.
     *
     * @return TransactionResponse
     */
    public function refund()
    {
        $gateway = $this->getGatewayClass();
        return (new  $gateway($this->transaction))->refund();
    }



    /**
     * Returns the callback action.
     *
     * @return array|RedirectResponse|Redirector
     */
    public function callbackAction()
    {
        $transaction = $this->transaction;
        if ($transaction->statusSlug() == 'sent-to-gateway') {
            $bank = $this->getGatewayClass();
            return (new  $bank($this->transaction))->callbackAction();
        } else {
            return self::generateResponse('unverifiable')->render();
        }
    }



    /**
     * Returns the full namespace of the gateway class.
     *
     * @return string
     */
    protected function getGatewayClass()
    {
        $gates_namespace = "Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates\\";
        $account_slug    = $this->getAccountModel()->slug;
        $account_class   = studly_case($account_slug);

        return $gates_namespace . $account_class;
    }



    /**
     * Returns a new instance of the gate class.
     *
     * @return GateAbstract
     */
    protected function getGatewayInstance()
    {
        $gateway = $this->getGatewayClass();

        return (new  $gateway($this->transaction));
    }



    /**
     * Returns the method validation rules to be used in the forms.
     *
     * @param string $method
     *
     * @return array
     */
    public function methodValidationRules(string $method)
    {
        $rules = [
             'create' => [
                  'auto_verify' => ['required'],
             ],
        ];

        return array_key_exists($method, $rules) ? $rules[$method] : [];
    }



    /**
     * Whether the res num may be set.
     *
     * @return bool
     */
    public static function canSetResNum()
    {
        return false;
    }
}
