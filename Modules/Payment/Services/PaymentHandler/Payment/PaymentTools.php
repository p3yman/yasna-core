<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 03:12 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Payment;

class PaymentTools
{
    public static function paymentTypeExists(string $type)
    {
        return in_array($type, self::paymentTypes());
    }

    public static function paymentTypes()
    {
        return config('payment.types');
    }
}
