<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 19/12/2017
 * Time: 01:05 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Transaction;

use Carbon\Carbon;
use App\Models\Transaction as TransactionModel;
use Closure;

trait TransactionStatusTrait
{
    protected static $finalizedStatuses = [
        'refunded',
        'cancelled',
        'rejected',
        'verified',
    ];

    /*
    |--------------------------------------------------------------
    | Modifying Transaction Status
    |--------------------------------------------------------------
    */

    protected function statusChangingGuard(string $methodName, ... $parameters)
    {
        if ($this->problemFound()) {
            return $this;
        }

        if ($this->exists()) {
            return $this->$methodName(...$parameters);
        } else {
            return $this->saveProblem(
                self::generateResponse(
                    'ineffective-action',
                    ['text' => "Unable to $methodName a not-created transaction."]
                ), $methodName);
        }
    }

    protected function makeVerifiedByForce(Carbon $time = null)
    {
        $time = self::safeTime($time);

        $updatingData = [
            'paid_amount'     => $amount = $this->getModelProperty('payable_amount'),
            'verified_amount' => $amount,
            'verified_by'     => user()->id,
            'verified_at'     => $time,
            'effected_at'     => $time,
        ];
        return $this->validateInputs($updatingData, '')->updateModel($updatingData);
    }
    
    protected function makeVerified(Carbon $time = null)
    {
        if (true/* @todo: conditions */) {
            return $this->makeVerifiedByForce($time);
        } else {
            return $this->saveProblem(
                self::generateResponse(
                    'ineffective-action',
                    ['text' => "Unable to confirm this transaction."]
                ), __FUNCTION__);
        }
    }

    protected function forceConfirm(...$parameters)
    {
        return $this->makeVerifiedByForce(...$parameters);
    }

    protected function confirm(...$parameters)
    {
        return $this->makeVerified(...$parameters);
    }

    protected function makeRefundedByForce(Carbon $time = null, string $reason = null, $amount = 0)
    {
        // @todo: very important to call refund method of gateway, back or specific payment type
        $time = self::safeTime($time);

        $updatingData = [
            'refunded_at'        => $time,
            'refundation_reason' => $reason,
            'refunded_amount'    => $amount,
            'refunded_by'        => user()->id,
        ];

        return $this->validateInputs($updatingData, '')->updateModel($updatingData);
    }

    protected function makeRefunded(Carbon $time = null, string $reason = null)
    {
        if (true/* @todo: conditions */) {
            return $this->makeRefundedByForce($time, $reason);
        } else {
            return $this->saveProblem(
                self::generateResponse(
                    'ineffective-action',
                    ['text' => "Unable to refund this transaction."]
                ), __FUNCTION__);
        }
    }

    protected function makeCancelledByForce(Carbon $time = null, string $reason = null)
    {
        $time = self::safeTime($time);

        $updatingData = [
            'cancelled_at'        => $time,
            'cancellation_reason' => $reason,
            'cancelled_by'        => user()->id,
        ];
        return $this->validateInputs($updatingData, '')->updateModel($updatingData);
    }

    protected function makeCancelled(Carbon $time = null, string $reason = null)
    {
        if (true/* @todo: conditions */) {
            return $this->makeCancelledByForce($time, $reason);
        } else {
            return $this->saveProblem(
                self::generateResponse(
                    'ineffective-action',
                    ['text' => "Unable to cancel this transaction."]
                ), __FUNCTION__);
        }
    }

    protected function makeRejectedByForce(Carbon $time = null, string $reason = null)
    {
        $time = self::safeTime($time);

        $updatingData = [
            'rejected_at'      => $time,
            'rejection_reason' => $reason,
            'rejected_by'      => user()->id,
        ];
        return $this->validateInputs($updatingData, '')->updateModel($updatingData);
    }

    protected function makeRejected(Carbon $time = null, string $reason = null)
    {
        if (true/* @todo: conditions */) {
            return $this->makeRejectedByForce($time, $reason);
        } else {
            return $this->saveProblem(
                self::generateResponse(
                    'ineffective-action',
                    ['text' => "Unable to reject this transaction."]
                ), __FUNCTION__);
        }
    }

    protected static function safeTime(Carbon $time = null)
    {
        if (is_null($time)) {
            $time = Carbon::now();
        }

        return $time;
    }

    protected function updateModel(array $updatingData)
    {
        if ($this->problemFound()) {
            return $this;
        }

        if ($this->exists()) {
            TransactionModel::store(array_merge($updatingData, ['id' => $this->getModelProperty('id')]));
            return $this->tracking($this->getModelProperty('tracking_no'));
        } else {
            return $this->saveProblem(
                self::generateResponse(
                    'ineffective-action',
                    ['text' => 'Unable to update a not-created transaction.']
                ), __FUNCTION__);
        }
    }

    /*
    |--------------------------------------------------------------
    | Reading Transaction Status
    |--------------------------------------------------------------
    */
    public function status()
    {
        if ($this->problemFound()) {
            return $this->getProblem();
        }

        $model = $this->getModel();

        if (!$model->exists) {
            return self::generateResponse('unstored-transaction');
        } else {
            return self::generateResponse($model->status);
        }
    }

    public function statusSlug()
    {
        return $this->status()['slug'];
    }

    public function statusCode()
    {
        return $this->status()['code'];
    }

    public function isFinalized()
    {
        return in_array($this->statusSlug(), self::finalizedStatuses());
    }

    public function isVerified()
    {
        return $this->getModel()->is_verified;
    }

    public static function statusConditions()
    {
        // Order of this array is important
        return [
            'refunded'        => 'refunded_at',
            'cancelled'       => 'cancelled_at',
            'rejected'        => 'rejected_at',
            'verified'        => ['verified_at', 'effected_at', 'paid_amount' => 'payable_amount'],
            'declared'        => 'declared_at',
            'sent-to-gateway' => 'proceeded_at',
            'created'         => true,
        ];
    }

    public static function availableStatuses()
    {
        return array_reverse(array_keys(self::statusConditions()));
    }

    public static function availableStatusesCombo($emptyOption = null)
    {
        $result = [];

        if (!is_null($emptyOption)) {
            $result[] = [
                'id'    => '',
                'title' => is_string($emptyOption) ? $emptyOption : trans('payment::general.none-of-theme'),
            ];
        }

        foreach (self::availableStatuses() as $status) {
            $result[] = ['id' => $status, 'title' => trans("payment::status.$status")];
        }
        return $result;
    }

    public static function statusIsValid(string $status)
    {
        return in_array($status, self::availableStatuses());
    }

    public static function isAcceptableTransactionStatus(...$parameters)
    {
        return self::statusIsValid(...$parameters);
    }

    public static function finalizedStatuses()
    {
        return self::$finalizedStatuses;
    }
}
