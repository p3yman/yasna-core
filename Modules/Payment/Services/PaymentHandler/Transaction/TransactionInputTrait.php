<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 03:42 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Transaction;

use App\Models\Account;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Payment\Services\PaymentHandler\Response\Error;
use App\Models\Transaction as TransactionModel;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;
use Modules\Yasna\Services\YasnaModel;

trait TransactionInputTrait
{
    protected static $fillableFields     = [
         'user_id',
         'account_id',
         'invoice_id',
         'client_ip',
         'auto_verify',
         'callback_url',
         'payable_amount',
         'invoice_model',
         'title',
         'description',
         'effected_at',
         'bank_resnum',
         'declared_at',
    ];
    protected static $changeableFields   = [
         'user_id',
         'account_id',
         'invoice_id',
         'client_ip',
         'auto_verify',
         'callback_url',
         'payable_amount',
         'paid_amount',

         'bank_resnum',

         'verified_amount',
         'verified_by',
         'verified_at',

         'refunded_amount',
         'refunded_by',
         'refunded_at',
         'refundation_reason',

         'proceeded_at',

         'declared_at',
         'paid_by',

         'rejected_by',
         'rejected_at',
         'rejection_reason',

         'cancelled_by',
         'cancelled_at',
         'cancellation_reason',
    ];
    protected static $cloneFields        = [
         'user_id',
         'account_id',
         'invoice_id',
         'client_ip',
         'auto_verify',
         'callback_url',
         'payable_amount',
    ];
    protected static $defaultPaymentType = 'online';

    protected $data = [];
    protected $account;



    /*
    |--------------------------------------------------------------
    | Creating New Transaction
    |--------------------------------------------------------------
    */

    protected static function isFillable($field)
    {
        return in_array($field, self::fillableFields());
    }



    protected static function fillableFields()
    {
        return self::$fillableFields;
    }



    protected static function cloneFields()
    {
        return self::$cloneFields;
    }



    protected static function changeableFields()
    {
        return self::$changeableFields;
    }



    protected function fillField($filed, $value)
    {
        if ($this->hasProblem('invalid-inputs')) {
            $this->clearProblem();
        }
        $this->data[$filed] = $value;

        return $this;
    }



    public function account_id($accountId)
    {
        return $this->setAccountObj($accountId)->fillField('account_id', $accountId);
    }



    protected function setAccountObj($accountId)
    {
        $undefinedAccountProblemSLug = 'undefined-account';
        if ($this->noProblem() or $this->problemIs($undefinedAccountProblemSLug)) {
            $this->clearProblem();
            if (($account = Account::find($accountId)) and $account->isPublished()) {
                $this->account = $account;
            } else {
                //$this->setProblem($undefinedAccountProblemSLug, __FUNCTION__);
            }
        }

        return $this;
    }



    public function account($accountIdentifier)
    {
        return $this->account_id($accountIdentifier);
    }



    public function bank(...$parameters)
    {
        return $this->account(...$parameters);
    }



    protected static function undefinedAccount($accountIdentifier)
    {
        //Error::triggerError('Undefined Account: "' . $accountIdentifier . '"');
    }



    public static function smartFindAccount($identifier)
    {
        if ($identifier instanceof Account) {
            $account = $identifier;
        } elseif (is_numeric($identifier)) {
            $account = Account::find($identifier);
        } elseif (
             count($dehashed = hashid_decrypt($identifier, 'ids')) and
             is_numeric($id = $dehashed[0])
        ) {
            $account = Account::find($dehashed[0]);
        } else {
            $account = model("account")->grabSlug($identifier);
        }

        return $account;
    }



    public function invoice($invoiceId)
    {
        return $this->invoiceId($invoiceId);
    }



    /**
     * set invoice_id and invoice_model
     *
     * @param YasnaModel $model
     *
     * @return $this
     * @throws \ReflectionException
     */
    public function invoiceObject(YasnaModel $model)
    {
        $this->invoice($model->id);
        $reflect = new \ReflectionClass($model);
        return $this->invoiceModel($reflect->getShortName());
    }



    public function amount($amount)
    {
        return $this->payableAmount($amount);
    }



    public function ip($ip)
    {
        return $this->clientIp($ip);
    }



    public function callback($url)
    {
        return $this->callbackUrl($url);
    }



    public function url($url)
    {
        return $this->callback_url($url);
    }



    public function auto_verify(int $value)
    {
        return $this->fillField('auto_verify', $value);
    }



    public function bank_resnum($resnum)
    {
        if ($this->getPaymentTypeResolver()->canSetResNum()) {
            $this->fillField('bank_resnum', $resnum);
        }

        return $this;
    }



    public function bankResnum($number)
    {
        return $this->bank_resnum($number);
    }



    public function resNum($number)
    {
        return $this->bank_resnum($number);
    }



    public function saveBankResponse($rs)
    {
        $model   = $this->getModel();
        $current = ($meta = $model->spreadMeta()->bank_response)
             ? (is_array($meta) ? $meta : [$meta])
             : [];

        $current[] = $rs;

        TransactionModel::store(['id' => $model->id, 'bank_response' => $current]);

        return $this;
    }



    protected function validateInputs(array $inputs, string $method = 'create')
    {
        if (($validator = Validator::make($inputs, $this->validationRules($method)))->fails()) {
            $function = isset(debug_backtrace()[1]['function']) ? debug_backtrace()[1]['function'] : '';
            $this->saveProblem(self::generateResponse(
                 'invalid-inputs',
                 ['messages' => array_merge(...array_values($validator->messages()->toArray()))]
            ), $function);
        }

        return $this;
    }



    public function getData($key = null)
    {
        return (is_null($key)) ?
             $this->data :
             (array_key_exists($key, $this->data) ? $this->data[$key] : null);
    }



    public function resetData()
    {
        $this->data = [];
        return $this;
    }



    public function getCompleteData()
    {
        return array_default($this->getData(), self::defaultData());
    }



    public static function defaultData()
    {
        return [
             'user_id'     => user()->id ?: null,
             //'account_id'  => ($defaultAccount = payment()->accounts()->online()->first()) ? $defaultAccount->id : 0,
             'client_ip'   => request()->ip(),
             'auto_verify' => 1,
        ];
    }



    /**
     * @return \App\Models\Account
     */
    public function getAccountModel()
    {
        if (!$this->account or !$this->account->exists) {
            $account = model("account")->grabId(
                 $this->exists() ? $this->model->account_id : $this->getData('account_id')
            );

            $this->account = $account;
        }

        return $this->account;
    }



    public static function acceptableCurrencies()
    {
        return ['IRR', 'IRT'];
    }



    public function validationRules(string $method = '')
    {
        return array_merge_recursive(
             self::generalValidationRules(),
             $this->typeValidationRules($method),
             self::methodValidationRules($method)
        );
    }



    public static function generalValidationRules()
    {
        return [
             'user_id'         => [
                  function ($field, $value) {
                      return ($value == 0) or boolval(User::find($value));
                  },
             ],
             'account_id'      => [''],
             'invoice_id'      => ['numeric'],
             'client_ip'       => ['ip'],
             'callback_url'    => ['url'],
             'payable_amount'  => ['numeric'],
             'paid_amount'     => ['numeric'],
             'verified_amount' => ['numeric'],
             'refunded_amount' => ['numeric'],
             'auto_verify'     => [
                 //                Rule::in([1, 0]),
                 //                'numeric',
                 'between:0,1',
             ],
        ];
    }



    public static function methodValidationRules(string $method)
    {
        $rules = [
             'create' => [
                  'account_id'     => [],
                  'invoice_id'     => ['required'],
                  'client_ip'      => ['required'],
                  'callback_url'   => ['required'],
                  'payable_amount' => ['required'],
             ],
        ];

        return array_key_exists($method, $rules) ? $rules[$method] : [];
    }



    public function typeValidationRules(string $method)
    {
        return ($paymentTypeResolver = $this->getPaymentTypeResolver())
             ? $paymentTypeResolver->validationRules($method)
             : [];
    }



    /*
    |--------------------------------------------------------------
    | Reading an Existed Transaction
    |--------------------------------------------------------------
    */

    public function trackingNumber($trackingNumber)
    {
        if ($transaction = TransactionModel::where('tracking_no', $trackingNumber)->first()) {
            return $this->setModel($transaction);
        } else {
            return $this->saveProblem(
                 self::generateResponse('invalid-inputs', [
                      'text' => 'Undefined Tracking Number: ' . $trackingNumber,
                 ]),
                 __FUNCTION__
            );
        }
    }



    public function tracking(... $parameters)
    {
        return $this->trackingNumber(...$parameters);
    }



    protected function setModel(TransactionModel $transaction)
    {
        $this->model = $transaction;
        return $this->setAccountObj($this->getModelProperty('account_id'));
    }
}
