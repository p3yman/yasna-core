<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 03:40 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Transaction;

use BadMethodCallException;
use App\Models\Transaction as TransactionModel;

class Transaction
{
    use TransactionMethodsGuardTrait;
    use TransactionInputTrait;
    use TransactionStatusTrait;
    use TransactionOutputTrait;
    use TransactionTypeTrait;
    use TransactionEndpointTrait;

    protected $model;

    protected static $methodsGuards = [
        'confirm'              => 'statusChangingGuard',
        'forceConfirm'         => 'statusChangingGuard',
        'makeVerified'         => 'statusChangingGuard',
        'makeVerifiedByForce'  => 'statusChangingGuard',
        'makeRefunded'         => 'statusChangingGuard',
        'makeRefundedByForce'  => 'statusChangingGuard',
        'makeCancelled'        => 'statusChangingGuard',
        'makeCancelledByForce' => 'statusChangingGuard',
        'makeRejected'         => 'statusChangingGuard',
        'makeRejectedByForce'  => 'statusChangingGuard',
    ];

    public function __construct()
    {
        $this->model = new \App\Models\Transaction();
    }

    /**
     * @param string $hashid
     *
     * @return $this|null
     */
    public static function findByHashid(string $hashid)
    {
        if (($transaction = TransactionModel::findByHashid($hashid)) and $transaction->exists) {
            return (new self)->tracking($transaction->tracking_no);
        }

        return null;
    }

    public function __call($method, $parameters)
    {
        if (self::isFillable($fieldName = snake_case($method))) {
            if (method_exists($this, $fieldName)) {
                return $this->$fieldName(...$parameters);
            } else {
                return $this->fillField($fieldName, ...$parameters);
            }
        } elseif (method_exists($this, $method) and array_key_exists($method, self::$methodsGuards)) {
            $guard = self::$methodsGuards[$method];
            return $this->$guard($method, ...$parameters);
        }

        throw new BadMethodCallException(
            sprintf('Call to undefined method %s::%s()', get_class($this), $method)
        );
    }

    public static function createWithModel(TransactionModel $model)
    {
        return (new self)->setModel($model);
    }

    public function clone(array $modifyingData = [])
    {
        $cloned       = (new self);
        $cloned->data = array_only(
            array_merge($this->getModel()->getAttributes(), $modifyingData),
            self::cloneFields()
        );
        return $cloned->store();
    }
}
