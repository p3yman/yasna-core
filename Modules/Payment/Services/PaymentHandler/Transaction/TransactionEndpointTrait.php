<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/14/19
 * Time: 10:14 AM
 */

namespace Modules\Payment\Services\PaymentHandler\Transaction;


trait TransactionEndpointTrait
{
    /**
     * Returns the need to fire a transaction by the front side.
     *
     * @return array
     */
    public function fireForEndpoint()
    {
        return $this->getPaymentTypeResolver()->fireForEndpoint();
    }
}
