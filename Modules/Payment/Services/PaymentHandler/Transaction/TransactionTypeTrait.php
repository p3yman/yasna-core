<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 18/12/2017
 * Time: 01:29 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Transaction;

use Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Online;
use Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\PaymentType;

trait TransactionTypeTrait
{
    protected static $paymentTypeNamespace = "Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\\";

    protected $typeResolver;



    /**
     * Returns the payment type resolver.
     *
     * @return PaymentType
     */
    protected function getPaymentTypeResolver()
    {
        if (!$this->typeResolver and class_exists($class = $this->getPaymentTypeClass())) {
            $this->typeResolver = new $class($this);
        }

        return $this->typeResolver;
    }



    /**
     * Returns the name of the related payment type's class.
     *
     * @return string
     */
    protected function getPaymentTypeClass()
    {
        return self::$paymentTypeNamespace . studly_case(camel_case($this->getAccountModel()->type));
    }
}
