<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 05:18 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Transaction;

use App\Models\Transaction as TransactionModel;
use http\Exception\BadMethodCallException;
use Modules\Payment\Services\PaymentHandler\Response\Error;
use Modules\Payment\Services\PaymentHandler\Response\TransactionResponse;

trait TransactionOutputTrait
{
    protected static $trackingChange = 1000;

    protected $problem = false;

    protected $updateGuard = [];



    public function getTrackingNumber()
    {
        if ($this->noProblem() and ($trackingNumber = $this->validateInputs(
                  $this->getCompleteData()
             )->store()->refreshModel()->getModelProperty('tracking_no'))) {
            return $trackingNumber;
        }

        return $this->getProblem();
    }



    public function getTracking()
    {
        return $this->getTrackingNumber();
    }



    public function getType()
    {
        return $this->getAccountModel()->type;
    }



    public function type()
    {
        return $this->getType();
    }



    public function isOnline()
    {
        return ($this->getType() == 'online');
    }



    public function isDeclarable()
    {
        return $this->getAccountModel()->is_declarable;
    }



    protected function getModelProperty($property)
    {
        return (!$this->problemFound() and ($model = $this->getModel()) and $model->exists) ? $model->$property : null;
    }



    /**
     * @return TransactionModel
     */
    public function getModel()
    {
        return $this->model->spreadMeta();
    }



    public function refreshModel()
    {
        $this->model->refresh();
        return $this;
    }



    private function store()
    {
        if (!$this->problemFound()) {
            if (!$this->exists()) {
                $data = $this->getCompleteData();
                $this->validateInputs($data)->storeModel($data)->resetData();
            }
        }

        return $this;
    }



    private function storeModel(array $data)
    {
        $modelId = TransactionModel::store($data);
        if ($modelId) {
            $this->model = TransactionModel::find($modelId);
            $this->model->update([
                 'tracking_no' => $this->model->id + self::$trackingChange,
            ]);

            return $this;
        }

        Error::triggerError('Unable to create transaction . ');
    }



    protected function getModelFillableData()
    {
        return array_only($this->model->toArray(), self::fillableFields());
    }



    protected function getModelChangeableData()
    {
        return array_only($this->model->toArray(), self::changeableFields());
    }



    public function fire()
    {
        if ($this->exists()) {
            $this->update();
        } else {
            $this->store();
        }
        $this->model->refresh();

        $this->validateInputs($this->getModelChangeableData());

        if ($this->problemFound()) {
            return $this->getProblem();
        }

        if ($this->getAccountModel()->isNotPublished()) {
            return $this->saveProblem(self::generateResponse('undefined-account'), __FUNCTION__)
                        ->getProblem()
                 ;
        }

        $response = $this->getPaymentTypeResolver()
                         ->fire($this)
        ;

        if ($response) {
            if ($response instanceof TransactionResponse) {
                return $response->render();
            } else {
                return self::generateResponse('returnable-response', ['response' => $response]);
            }
        } else {
            return $this->getProblem();
        }
    }



    public function callbackAction()
    {
        if ($this->isFinalized()) {
            return self::generateResponse(
                 'returnable-response',
                 ['response' => $this->redirectToCallback(false)]
            );
        }

        return (
        $response = $this->validateInputs($this->getModelChangeableData())
                         ->getPaymentTypeResolver()
                         ->callbackAction()
        )
             ? $response
             : $this->getProblem();
    }



    public function update()
    {
        if (!$this->problemFound()) {
            if ($this->exists()) {
                $data = $this->data;
                if (count($unacceptableFields = array_diff(array_keys($data), $this->updateFields()))) {
                    return self::generateResponse('invalid-inputs', [
                         'message' => 'Unable to update update : ' . implode(', ', $unacceptableFields),
                    ]);
                } else {
                    $data['id'] = $this->getModel()->id;
                    TransactionModel::store($data);
                    $this->resetData();
                    return self::generateResponse('successfully-updated');
                }
            } else {
                $this->setProblem('unstored-transaction', __FUNCTION__);
            }
        }

        return $this->getProblem();
    }



    protected function updateFields()
    {
        return array_diff(self::changeableFields(), $this->updateGuard);
    }



    public function exists()
    {
        return ($this->model and $this->model->exists);
    }



    protected function problemFound()
    {
        return !empty($this->problem);
    }



    protected function noProblem()
    {
        return !$this->problemFound();
    }



    protected function hasProblem($problemIdentifier = null)
    {
        if (is_null($problemIdentifier)) {
            return $this->problemFound();
        } else {
            return $this->problemIs($problemIdentifier);
        }
    }



    protected function problemIs($problemIdentifier)
    {
        if (
             $this->problemFound() and
             ($problem = $this->getProblem()) and
             ($problem['code'] == self::discoverResponseCode($problemIdentifier))
        ) {
            return true;
        } else {
            return false;
        }
    }



    protected function saveProblem(array $problemInfo, $method)
    {
        $this->problem = [
             'methodName' => $method,
             'info'       => $problemInfo,
        ];

        return $this;
    }



    protected function getProblem()
    {
        if ($this->problemFound()) {
            return empty($this->problem) ? self::generateResponse('unknown-error') : $this->problem['info'];
        } else {
            return false;
        }
    }



    protected function clearProblem()
    {
        $this->problem = false;

        return $this;
    }



    protected function setProblem($status, $function, $additionalData = [])
    {
        return $this->saveProblem(self::generateResponse($status, $additionalData), $function);
    }



    protected static function generateResponse($status, $additionalData = [])
    {
        return (new TransactionResponse(self::discoverResponseCode($status), $additionalData))->render();
    }



    protected static function discoverResponseCode($status)
    {
        if (!is_numeric($status) and is_string($status)) {
            $status = TransactionResponse::findStatusBySlug($status);
        }
        return $status;
    }



    public function redirectToCallback($returnFullResponse = true)
    {
        $model = $this->getModel();
        $url   = (str_contains($model->callback_url, '?')
                  ? ($model->callback_url . '&')
                  : ($model->callback_url . '?'))
             . http_build_query(['tracking_no' => $model->tracking_no]);

        $rs    = redirect($url);

        if ($returnFullResponse) {
            return self::generateResponse('returnable-response', [
                 'response' => $rs,
            ]);
        }

        return $rs;
    }



    public function verify()
    {
        if ($this->noProblem()) {
            return $this->isFinalized()
                 ? self::generateResponse('unverifiable')
                 : $this->getPaymentTypeResolver()->verify()->render();
        } else {
            return $this->getProblem();
        }
    }



    public function refund()
    {
        if ($this->noProblem()) {
            return $this->isVerified()
                 ? $this->getPaymentTypeResolver()->refund()->render()
                 : self::generateResponse('non-refundable');
        } else {
            return $this->getProblem();
        }
    }
}
