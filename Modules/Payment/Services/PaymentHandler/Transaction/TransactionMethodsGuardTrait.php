<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 19/12/2017
 * Time: 05:56 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Transaction;

trait TransactionMethodsGuardTrait
{
    protected function noErrorGuard($methodName, ...$parameters)
    {
        if ($this->problemFound()) {
            return $this;
        }

        return $this->$methodName(...$parameters);
    }
}
