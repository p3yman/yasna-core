<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 05:42 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Response;

class TransactionResponse
{
    protected static $responseDictionary = [
         100 => [
              'slug' => 'returnable-response',
              'text' => 'Returnable response is ready.',
         ],

         23 => [
              'slug' => 'successfully-refunded',
              'text' => 'Transaction has been refunded successfully.',
         ],

         22 => [
              'slug' => 'successfully-verified',
              'text' => 'Transaction has been verified successfully.',
         ],

         21 => [
              'slug' => 'successfully-updated',
              'text' => 'Transaction has been updated successfully.',
         ],

         1 => [
              'slug' => 'verified',
              'text' => 'Transaction has been verified.',
         ],

         0 => [
              'slug' => 'created',
              'text' => 'Transaction has been created.',
         ],

         -1 => [
              'slug' => 'declared',
              'text' => 'Client declared to payment.',
         ],

         -2 => [
              'slug' => 'sent-to-gateway',
              'text' => 'Transaction has been sent to gateway.',
         ],

         -3 => [
              'slug' => 'cancelled',
              'text' => 'Transaction has been cancelled.',
         ],

         -4 => [
              'slug' => 'rejected',
              'text' => 'Transaction has been rejected.',
         ],

         -5 => [
              'slug' => 'refunded',
              'text' => 'Transaction has been refunded.',
         ],

         -10 => [
              'slug' => 'invalid-inputs',
              'text' => 'Passed inputs are not valid.',
         ],

         -14 => [
              'slug' => 'unstored-transaction',
              'text' => 'This transaction has not been stored.',
         ],

         -15 => [
              'slug' => 'ineffective-action',
              'text' => 'You are trying to do an ineffective action.',
         ],

         -21 => [
              'slug' => 'undefined-account',
              'text' => 'Specified account isn\'t available.',
         ],

         -22 => [
              'slug' => 'unverifiable',
              'text' => 'This transaction can not be verified.',
         ],

         -23 => [
              'slug' => 'non-refundable',
              'text' => 'This transaction can not be refunded.',
         ],
         -30 => [
              'slug' => 'connection-failed',
              'text' => 'Connection Failed',
         ],
         -31 => [
              'slug' => 'gateway-error',
              'text' => 'zarinpal failed starting pay',
         ],

         -100 => [
              'slug' => 'unknown-error',
              'text' => 'Unknown Error Occurred.',
         ],
    ];

    protected static $unchangeableFields = ['code', 'slug', 'status'];

    protected $data;



    public static function create(...$parameters)
    {
        return new self(...$parameters);
    }



    public function __construct(int $code, $additional = [])
    {
        if (array_key_exists($code, self::responses())) {
            $this->data['code'] = $code;
            $this->data         = array_merge($this->data, array_except($additional, self::$unchangeableFields));
        } else {
            Error::triggerError('Undefined Response Code: "' . $code . '"');
        }
    }



    public function render()
    {
        return array_default($this->data(), self::responses()[$this->getDataItem('code')]);
    }



    public function data()
    {
        return array_merge($this->data, ['status' => ($this->data['code'] > 0)]);
    }



    public function getDataItem($key)
    {
        return $this->data()[$key];
    }



    public static function findStatusBySlug(string $slug)
    {
        foreach (self::responses() as $key => $response) {
            if ($response['slug'] == $slug) {
                return $key;
            }
        }

        Error::triggerError('Undefined Response Slug: "' . $slug . '"');
    }



    public static function responses()
    {
        return self::$responseDictionary;
    }
}
