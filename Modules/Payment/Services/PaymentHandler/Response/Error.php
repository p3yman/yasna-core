<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 17/12/2017
 * Time: 05:59 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Response;

class Error
{
    public static function triggerError($errorMessage, $errorType = E_USER_ERROR)
    {
        trigger_error($errorMessage, $errorType);
    }
}
