<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 24/12/2017
 * Time: 11:09 AM
 */

namespace Modules\Payment\Services\PaymentHandler\Search;

use App\Models\Transaction;
use BadMethodCallException;
use Illuminate\Database\Eloquent\Builder;
use SplStack;

class SearchBuilder
{
    use SearchBuilderConditionableFieldsTrait;
    use SearchBuilderTimeTrait;
    use SearchBuilderTypeTrait;
    use SearchBuilderAmountTrait;
    use SearchBuilderStatusTrait;
    use SearchBuilderOutputTrait;

    protected $builder;
    protected $magicMethodsStack;

    public function __construct()
    {
        $this->builder           = Transaction::select('transactions.*');
        $this->magicMethodsStack = new SplStack();
    }

    public function __call($method, $parameters)
    {
        $this->pushMagicMethod($method);

        foreach (class_uses(self::class) as $trait) {
            $traitResult = $this->callTraitMethod(__FUNCTION__, $trait, $method, $parameters);
            if (!$this->magicMethodIsTop($method)) { // This method has been popped from the stack
                return $traitResult;
            }
        }

        try {
            $this->popMagicMethod();
            if ((($result = $this->builder->$method(...$parameters)) instanceof Builder)) {
                $this->builder = $result;
                return $this;
            } else {
                return $result;
            }
        } catch (BadMethodCallException $e) {
            throw new BadMethodCallException(
                sprintf('Call to undefined method %s::%s()', get_class($this), $method)
            );
        }
    }

    protected function pushMagicMethod($method)
    {
        $this->magicMethodsStack->push($method);
    }

    protected function popMagicMethod()
    {
        return $this->magicMethodsStack->pop();
    }

    protected function magicMethodIsTop($method)
    {
        if ($this->magicMethodsStack->count() and ($this->magicMethodsStack->top() == $method)) {
            return true;
        } else {
            return false;
        }
    }

    protected function callTraitMethod($method, $trait, ...$parameters)
    {
        return method_exists(
            $this,
            $callerMethod = $method . str_before(str_after($trait, __CLASS__), 'Trait')
        )
            ? $this->$callerMethod(...$parameters)
            : $this;
    }

    protected function getFullColumnName($column, $table = 'transactions')
    {
        return $table . '.' . $column;
    }
}
