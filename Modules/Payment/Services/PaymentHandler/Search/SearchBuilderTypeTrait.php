<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 27/12/2017
 * Time: 11:45 AM
 */

namespace Modules\Payment\Services\PaymentHandler\Search;

use Modules\Payment\Providers\PaymentToolsServiceProvider;

trait SearchBuilderTypeTrait
{
    protected $accountsJoined = false;
    protected $selectingTypes = [];

    protected static $typeColumn = 'accounts.type';

    protected function __callType($method, $parameters)
    {
        if (PaymentToolsServiceProvider::isAcceptablePaymentType($method)) {
            $this->selectingTypes[] = $method;
            $this->popMagicMethod();
            return $this;
        }
    }

    public function offline()
    {
        return $this->joinAccount()->where(self::$typeColumn, '<>', 'online');
    }

    protected function joinAccount()
    {
        if (!$this->accountsJoined) {
            $this->join('accounts', 'transactions.account_id', '=', 'accounts.id');
            $this->accountsJoined = true;
        }

        return $this;
    }

    protected function modifyBuilderType($builder)
    {
        $this->registerModifyingTrait(self::class);
        if (count($this->selectingTypes)) {
            return $builder->whereIn(self::$typeColumn, $this->selectingTypes);
        }
        return $builder;
    }
}
