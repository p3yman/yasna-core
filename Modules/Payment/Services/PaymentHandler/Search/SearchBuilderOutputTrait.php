<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 27/12/2017
 * Time: 02:53 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Search;

trait SearchBuilderOutputTrait
{
    protected static $outputMethods = ['get', 'paginate', 'first', 'count'];
    protected $modifyingTraits = [];

    protected function __callOutput($method, $parameters)
    {
        if (self::isOutputMethod($method)) {
            $result = $this->callOutputMethod($method, ...$parameters);
            $this->popMagicMethod();
            return $result;
        }

        return $this;
    }

    protected function callOutputMethod(string $method, ...$parameters)
    {
        return $builder = $this->joinAccount()->getBuilder()->$method(...$parameters);
    }

    protected static function getOutputMethods()
    {
        return self::$outputMethods;
    }

    protected static function isOutputMethod(string $method)
    {
        return in_array($method, self::getOutputMethods());
    }

    public function getBuilder()
    {
        $builder = $this->builder;
        $this->resetModifyingTraits();

        foreach (class_uses(self::class) as $trait) {
            $methodResult = $this->callTraitMethod('modifyBuilder', $trait, $builder);
            if ($this->isModifyingTrait($trait)) {
                $builder = $methodResult;
            }
        }

        return $builder;
    }

    protected function resetModifyingTraits()
    {
        $this->modifyingTraits = [];
        return $this;
    }

    protected function isModifyingTrait(string $trait)
    {
        return in_array($trait, $this->modifyingTraits);
    }

    protected function registerModifyingTrait(string $trait)
    {
        $this->modifyingTraits[] = $trait;
        return $trait;
    }

    public function raw()
    {
        return readRaw($this->getBuilder());
    }
}
