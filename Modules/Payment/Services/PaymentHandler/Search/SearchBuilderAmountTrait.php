<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 27/12/2017
 * Time: 01:54 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Search;

trait SearchBuilderAmountTrait
{
    protected static $amountColumn = 'payable_amount';

    public function amountLessThan($value)
    {
        return $this->where(self::$amountColumn, '<=', $value);
    }

    public function amountMoreThan($value)
    {
        return $this->where(self::$amountColumn, '>=', $value);
    }

    public function amountBetween($minAmount, $maxAmount)
    {
        return $this->amountMoreThan($minAmount)->amountLessThan($maxAmount);
    }
}
