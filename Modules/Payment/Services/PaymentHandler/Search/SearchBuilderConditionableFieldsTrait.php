<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 27/12/2017
 * Time: 12:23 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Search;

use Modules\Yasna\Services\YasnaModel;

trait SearchBuilderConditionableFieldsTrait
{
    /**
     * An Array of the Fields which accept simple condition.
     *
     * @var array
     */
    protected static $conditionable_fields = [
         'user_id',
         'invoice_id',
         'invoice_model',
         'account_id',
         'payable_amount',
    ];



    /**
     * The call handler for the conditionable callings.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return $this
     */
    protected function __callConditionableFields($method, $parameters)
    {
        if (
             $this->isConditionableField($field_name = $method) or
             $this->isConditionableField($field_name = snake_case($method))
        ) {
            $result = $this->where($field_name, ... $parameters);
            $this->popMagicMethod();
            return $result;
        }

        return $this;
    }



    /**
     * Whether the specified field is a conditionable field.
     *
     * @param string $field_name
     *
     * @return bool
     */
    public static function isConditionableField(string $field_name): bool
    {
        return in_array($field_name, self::$conditionable_fields);
    }



    /**
     * A Short Way to Apply Condition on the invoice ID.
     *
     * @param mixed ...$parameters
     *
     * @return $this
     */
    public function invoice(...$parameters)
    {
        return $this->invoice_id(...$parameters);
    }



    /**
     * A Short Way to Apply Condition on the invoice ID.
     *
     * @param mixed ...$parameters
     *
     * @return $this
     */
    public function user(...$parameters)
    {
        return $this->user_id(...$parameters);
    }



    /**
     * A Short Way to Apply Condition on the invoice ID.
     *
     * @param mixed ...$parameters
     *
     * @return $this
     */
    public function amount(...$parameters)
    {
        return $this->payableAmount(...$parameters);
    }



    /**
     * Applies the conditions on the invoice id and invoice model based on the specified model instance.
     *
     * @param YasnaModel $model
     *
     * @return $this
     */
    public function invoiceObject(YasnaModel $model)
    {
        return $this->invoice($model->id)
                    ->invoiceModel($model->getClassName())
             ;
    }
}
