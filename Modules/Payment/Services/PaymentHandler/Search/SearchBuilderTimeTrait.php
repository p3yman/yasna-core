<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 27/12/2017
 * Time: 11:13 AM
 */

namespace Modules\Payment\Services\PaymentHandler\Search;

use Carbon\Carbon;

trait SearchBuilderTimeTrait
{
    protected static $timeTitles      = [
        'proceeded',
        'declared',
        'verified',
        'effected',
        'cancelled',
        'rejected',
        'refunded',
        'created',
        'updated',
        'deleted',
    ];
    protected static $timeMethodTypes = ['before', 'after', 'between'];

    protected function __callTime($method, $parameters)
    {
        if ($timeMethodInfo = self::getTimeMethodInfo($method)) {
            $timeTitle      = $timeMethodInfo['timeTitle'];
            $timeMethodType = $timeMethodInfo['type'];
            $methodName     = 'time' . $timeMethodType;
            $result         = $this->callTimeMethod($methodName, $timeTitle, ...$parameters);
            $this->popMagicMethod();
            return $result;
        }

        return $this;
    }

    public static function getTimeMethodTypes(string $function = null)
    {
        if ($function) {
            return array_map(function ($item) use ($function) {
                return $function($item);
            }, self::getTimeMethodTypes());
        }

        return self::$timeMethodTypes;
    }

    public static function getTimeTitles()
    {
        return self::$timeTitles;
    }

    public static function getTimeMethodInfo(string $method)
    {
        $types = self::getTimeMethodTypes('ucfirst');
        foreach ($types as $type) {
            if (ends_with($method, $type)) {
                if (in_array($timeTitle = str_before($method, $type), self::getTimeTitles())) {
                    return compact('timeTitle', 'type');
                } else {
                    break;
                }
            }
        }

        return false;
    }

    public static function getTimeColumnName(string $title)
    {
        $postfix = '_at';
        return ends_with($title, $postfix) ? $title : ($title . $postfix);
    }

    protected function callTimeMethod($method, $timeTitle, ...$otherParameters)
    {
        return $this->$method(self::getTimeColumnName($timeTitle), ...$otherParameters);
    }

    protected function timeBefore(string $columnName, Carbon $time)
    {
        return $this->timeCondition($columnName, '<=', $time);
    }

    protected function timeAfter(string $columnName, Carbon $time)
    {
        return $this->timeCondition($columnName, '>=', $time);
    }

    protected function timeBetween(string $columnName, Carbon $startTime, Carbon $endTime)
    {
        return $this->timeAfter($columnName, $startTime)->timeBefore($columnName, $endTime);
    }

    protected function timeCondition(string $columnName, string $operator, Carbon $time)
    {
        return $this->whereNotNull($columnName = $this->getFullColumnName($columnName))
            ->where($columnName, $operator, $time);
    }
}
