<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 26/12/2017
 * Time: 06:02 PM
 */

namespace Modules\Payment\Services\PaymentHandler\Search;

use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;

trait SearchBuilderStatusTrait
{
    public function status($status)
    {
        foreach (Transaction::statusConditions() as $stat => $condition) {
            if ($stat == $status) {
                $this->applyPositiveStatusCondition($condition);
                break;
            } else {
                $this->applyNegativeStatusCondition($condition);
            }
        }

        return $this;
    }

    protected function applyPositiveStatusCondition($part1, $part2 = null)
    {
        return $this->applyStatusCondition($part1, $part2);
    }

    protected function applyNegativeStatusCondition($part1, $part2 = null)
    {
        return $this->applyStatusCondition($part1, $part2, false);
    }

    protected function applyStatusCondition($part1, $part2 = null, $positive = true)
    {
        if ($part1 and !is_bool($part1)) {
            if (is_array($part1)) {
                $this->where(function ($query) use ($part1, $positive) {
                    foreach ($part1 as $key => $value) {
                        if (is_string($key)) {
                            $params = [$key, $value];
                        } else {
                            $params = [$value, null];
                        }
                        $params[] = $positive;
                        list($method, $whereParameters) = array_values(self::getStatusConditionInfo(...$params));
                        $whereParameters[] = $positive ? 'and' : 'or';
                        $query->$method(...$whereParameters);
                    }
                });
            } else {
                list($method, $parameters) = array_values(self::getStatusConditionInfo($part1, $part2, $positive));
                $this->$method(...$parameters);
            }
        }

        return $this;
    }

    protected static function getStatusConditionInfo($part1, $part2, $positive)
    {
        if (is_null($part2)) {
            $parameters = [$part1];
            if ($positive) {
                $method = 'whereNotNull';
            } else {
                $method = 'whereNull';
            }
        } else {
            $method = 'whereColumn';
            if ($positive) {
                $operator = '=';
            } else {
                $operator = '<>';
            }
            $parameters = [$part1, $operator, $part2];
        }

        return compact('method', 'parameters');
    }
}
