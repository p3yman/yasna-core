<?php
namespace Modules\Payment\Services;

use App\Models\Account;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use Modules\Payment\Entities\Traits\TransactionValidationTrait;

class Payment
{
    use TransactionValidationTrait;

    protected static $view_folder           = "manage::payment.";
    protected $user_id = 0;
    protected $account_id = null;
    protected $invoice_id = 0;
    protected $tracking_no;
    protected $bank_reference_no;
    protected $bank_resnum;
    protected $client_ip;
    protected $auto_verify = true;
    protected $callback_url = false;
    protected $online_payment_status;
    protected $payable_amount;
    protected $paid_amount = 0;
    protected $verified_amount = 0;
    protected $refunded_amount = 0;
    protected $paid_at = null;
    protected $declared_at = null;
    protected $verified_at = null;
    protected $cancelled_at = null;
    protected $rejected_at = null;
    protected $refunded_at = null;
    protected $verified_by = 0;
    protected $rejected_by = 0;
    protected $refunded_by = 0;
    protected $created_by = 0;
    protected $updated_by = 0;
    protected $deleted_by = 0;
    protected $published_by = 0;
    protected $debug = false;
    protected $make_validation = 0;
    protected $transaction_make_property = [
        'user_id',
        'account_id',
        'invoice_id',
        'client_ip',
        'auto_verify',
        'callback_url',
        'payable_amount',
    ];
    protected $new_transaction;

    /*
    |--------------------------------------------------------------------------
    | Builder
    |--------------------------------------------------------------------------
    |
    */

    public function __construct()
    {
    }

    public static function builder()
    {
        //		$class_name = "\\Modules\\Manage\\Services\\Widgets\\" . studly_case($widget_name);
//
        //		/*-----------------------------------------------
        //		| If Not Supported ...
        //		*/
        //		if(!class_exists($class_name)) {
        //			return new self($widget_name, false);
        //		}
//
        //		/*-----------------------------------------------
        //		| Instance Generation ...
        //		*/
        //		return new $class_name($widget_name);

        return new self();
    }

    /*
    |--------------------------------------------------------------------------
    | Transaction Chain Methods
    |--------------------------------------------------------------------------
    |
    */

    public function __call($name, $arguments)
    {
        $property_name = snake_case($name);

        /*-----------------------------------------------
        | Debug Helper ...
        */
        if ($this->debug) {
            ss($name);
        }

        /*-----------------------------------------------
        | Boolean Setup ...
        */
        if (!isset($arguments[0])) {
            $this->$property_name = true;
        }

        /*-----------------------------------------------
        | Normal Situation ...
        */
        else {
            $this->$property_name = $arguments[0];
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $this;
    }

    /**
     * @param null $user_id
     * @return $this
     */
    public function user_id($user_id = null)
    {
        if ($user_id and is_numeric($user_id)) {
            $this->user_id = $user_id;
        } else {
            $this->user_id = 0;
        }

        return $this;
    }

    /**
     * @param $account_id
     * @return $this
     */
    public function bank($account_id)
    {
        if ($account_id and is_numeric($account_id)) {
            $bank = Account::find($account_id);
            if ($bank and $bank->id) {
                $this->account_id = $account_id;
                $this->check_online_bank($bank);
            } else {
                $this->account_id = 0;
            }
        } else {
            $this->account_id = 0;
        }
        return $this;
    }

    public function accountId()
    {
        // nothing
    }

    public function clientIp($ip = null)
    {
        if ($ip) {
            $this->client_ip = $ip;
        } else {
            $this->client_ip = Request()->ip();
        }
    }

    public function callbackUrl($callback_url)
    {
        $this->callback_url = $callback_url;
    }

    public function amount($amount)
    {
        if (is_numeric($amount)) {
            $this->payable_amount = $amount;
        }

        return $this;
    }

    public function payableAmount()
    {
        // nothing
    }

    public function make()
    {
        if ($this->make_validation < 1) {
            $insert = [
                'user_id' => $this->user_id,
                'account_id' => $this->account_id,
                'invoice_id' => $this->invoice_id,
                'tracking_no' => $this->generateTrackingNo(),
                'client_ip' => $this->client_ip,
                'auto_verify' => $this->auto_verify,
                'callback_url' => $this->callback_url,
                'online_payment_status' => $this->online_payment_status,
                'payable_amount' => $this->payable_amount,
                'paid_amount' => $this->paid_amount,
                'verified_amount' => $this->verified_amount,
                'refunded_amount' => $this->refunded_amount,
                'paid_at' => $this->paid_at,
                'declared_at' => $this->declared_at,
                'verified_at' => $this->verified_at,
                'cancelled_at' => $this->cancelled_at,
                'rejected_at' => $this->rejected_at,
                'refunded_at' => $this->refunded_at,
                'verified_by' => $this->verified_by,
                'rejected_by' => $this->rejected_by,
                'refunded_by' => $this->refunded_by,
                'created_by' => $this->created_by,
                'updated_by' => $this->updated_by,
                'deleted_by' => $this->deleted_by,
                'published_by' => $this->published_by,
                
            ];

            $new_transaction = Transaction::store($insert);
            if ($new_transaction) {
                $new_transaction = Transaction::find($new_transaction);
                $this->new_transaction = $new_transaction->tracking_no;
            } else {
                $this->new_transaction = false;
            }
        } else {
            $this->new_transaction = false;
        }

        return $this;
    }

    public function getTracking()
    {
        if ($this->new_transaction > 0) {
            return $this->new_transaction;
        } else {
            return false;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */

    protected function generateTrackingNo()
    {
        $last = Transaction::orderBy('id', 'DESC')->first();
        if ($last and $last->id) {
            return $last->tracking_no++;
        } else {
            return 1001;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Render
    |--------------------------------------------------------------------------
    |
    */

    /*
    |--------------------------------------------------------------------------
    | Accounts methods
    |--------------------------------------------------------------------------
    |
    */
    public function account()
    {
        return new Account();
    }
}
