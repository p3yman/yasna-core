<?php

namespace Modules\Payment\Services\Entity\Transactions;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property Collection $transactions
 */
trait TransactionResourcesTrait
{
    /**
     * get Transactions Resource.
     *
     * @return array
     */
    public function getTransactionsResource()
    {
        $mapped = $this->transactions->map(function (Transaction $transaction) {
            return $transaction->toResource();
        });

        return $mapped->toArray();
    }
}
