<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/6/19
 * Time: 12:02 PM
 */

namespace Modules\Payment\Services\Entity\Transactions;


use Illuminate\Database\Eloquent\Collection;
use Modules\Payment\Services\PaymentHandler\Search\SearchBuilder;

trait TransactionsSearcherTrait
{
    /**
     * Returns an instance of `Modules\Payment\Services\PaymentHandler\Search\SearchBuilder` class
     * to select the list of transactions of this entity.
     *
     * @return SearchBuilder
     */
    public function transactionsSearcher()
    {
        $searcher = payment()->search();

        if ($this->shouldInteractWithModelName()) {
            return $searcher->invoiceObject($this);
        } else {
            return $searcher->invoice($this->id);
        }
    }



    /**
     * Returns an instance of `Modules\Payment\Services\PaymentHandler\Search\SearchBuilder` class
     * to select the list of verified transactions of this entity.
     *
     * @return SearchBuilder
     */
    public function verifiedTransactions()
    {
        return $this->transactionsSearcher()->status('verified');
    }



    /**
     * Returns the result of the searcher returns from the `verifiedTransactions()` method.
     *
     * @return Collection
     */
    public function getVerifiedTransactionsAttribute()
    {
        return $this->verifiedTransactions()->get();
    }



    /**
     * Weather this entity has any verified transaction.
     *
     * @return bool
     */
    public function hasVerifiedTransactions()
    {
        return boolval($this->verifiedTransactions()->count());
    }



    /**
     * Accessor for the `hasVerifiedTransactions()` Method
     *
     * @return bool
     */
    public function getHasVerifiedTransactionsAttribute()
    {
        return $this->hasVerifiedTransactions();
    }



    /**
     * Weather this entity has any verified transaction.
     * <br>
     * This is a mirror for the `hasVerifiedTransactions()` method.
     *
     * @return bool
     */
    public function hasVerifiedTransaction()
    {
        return $this->hasVerifiedTransactions();
    }



    /**
     * Accessor for the `hasVerifiedTransaction()` Method
     *
     * @return bool
     */
    public function getHasVerifiedTransactionAttribute()
    {
        return $this->hasVerifiedTransaction();
    }



    /**
     * Returns an instance of `Modules\Payment\Services\PaymentHandler\Search\SearchBuilder` class
     * to select the list of declared transactions of this entity.
     *
     * @return SearchBuilder
     */
    public function declaredTransactions()
    {
        return $this->transactionsSearcher()->status('declared');
    }



    /**
     * Returns the result of the searcher returns from the `declaredTransactions()` method.
     *
     * @return Collection
     */
    public function getDeclaredTransactionsAttribute()
    {
        return $this->declaredTransactions()->get();
    }



    /**
     * Weather this entity has any declared transaction.
     *
     * @return bool
     */
    public function hasDeclaredTransactions()
    {
        return boolval($this->declaredTransactions()->count());
    }



    /**
     * Accessor for the `hasDeclaredTransactions()` Method
     *
     * @return bool
     */
    public function getHasDeclaredTransactionsAttribute()
    {
        return $this->hasDeclaredTransactions();
    }



    /**
     * Weather this entity has any declared transaction.
     * <br>
     * This is a mirror for the `hasDeclaredTransactions()` method.
     *
     * @return bool
     */
    public function hasDeclaredTransaction()
    {
        return $this->hasDeclaredTransactions();
    }



    /**
     * Accessor for the `hasDeclaredTransaction()` Method.
     *
     * @return bool
     */
    public function getHasDeclaredTransactionAttribute()
    {
        return $this->hasDeclaredTransaction();
    }
}
