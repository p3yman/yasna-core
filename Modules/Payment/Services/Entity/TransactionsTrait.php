<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/5/19
 * Time: 3:33 PM
 */

namespace Modules\Payment\Services\Entity;


use App\Models\Transaction;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Payment\Services\Entity\Transactions\TransactionResourcesTrait;
use Modules\Payment\Services\Entity\Transactions\TransactionsSearcherTrait;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction as TransactionHandler;
use ReflectionException;

/**
 * Trait TransactionsTrait
 * <br>
 * This trait has been provided to be used in the models to connect them to the transactions.
 */
trait TransactionsTrait
{
    use TransactionsSearcherTrait;
    use TransactionResourcesTrait;

    /**
     * Whether the entity should interact with the model name.
     *
     * @var bool
     */
    protected $should_interact_with_model_name = true;



    /**
     * Standard One-to-Many Relationship with transactions
     *
     * @return HasMany
     */
    public function transactions()
    {
        $builder = $this->hasMany(Transaction::class, 'invoice_id');

        if ($this->shouldInteractWithModelName()) {
            $builder->where('invoice_model', $this->getClassName());
        }

        return $builder;
    }



    /**
     * Returns a new transaction instance created for this model.
     *
     * @return TransactionHandler
     * @throws ReflectionException
     */
    public function newTransaction()
    {
        $transaction = payment()->transaction();

        if ($this->shouldInteractWithModelName()) {
            return $transaction->invoiceObject($this);
        } else {
            return $transaction->invoice($this->id);
        }
    }



    /**
     * Whether the entity should interact with the model name.
     *
     * @return bool
     */
    protected function shouldInteractWithModelName()
    {
        return $this->should_interact_with_model_name;
    }
}
