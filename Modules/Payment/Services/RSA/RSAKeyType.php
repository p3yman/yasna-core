<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 25/02/2018
 * Time: 06:23 PM
 */

namespace Modules\Payment\Services\RSA;

class RSAKeyType
{
    const XMLFile   = 0;
    const XMLString = 1;
}
