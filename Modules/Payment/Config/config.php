<?php

return [
    'name' => 'Payment',

    'types' => [
        'online',
        'deposit',
        'shetab',
        'sheba',
        'cheque',
        'cash',
        'pos',
    ],

    'currencies' => [
        'IRR',
    ],

    'status' => [
        'color' => [
            'created'         => 'gray',
            'sent-to-gateway' => 'warning',
            'declared'        => 'info',
            'verified'        => 'green',
            'rejected'        => 'danger',
            'cancelled'       => 'danger',
            'refunded'        => 'pink',
        ],
        'icon'  => [
            'created'         => 'hourglass-o',
            'sent-to-gateway' => 'hourglass-half',
            'declared'        => 'clock',
            'verified'        => 'check',
            'rejected'        => 'ban',
            'cancelled'       => 'times',
            'refunded'        => 'undo',
        ],
    ],
];
