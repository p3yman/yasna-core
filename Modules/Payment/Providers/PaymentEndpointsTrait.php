<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/7/19
 * Time: 11:28 AM
 */

namespace Modules\Payment\Providers;


use Modules\Payment\Http\Endpoints\V1\AccountActivateEndpoint;
use Modules\Payment\Http\Endpoints\V1\AccountDeactivateEndpoint;
use Modules\Payment\Http\Endpoints\V1\AccountDeleteEndpoint;
use Modules\Payment\Http\Endpoints\V1\AccountFormEndpoint;
use Modules\Payment\Http\Endpoints\V1\AccountListEndpoint;
use Modules\Payment\Http\Endpoints\V1\AccountMakeDefaultEndpoint;
use Modules\Payment\Http\Endpoints\V1\AccountPreviewEndpoint;
use Modules\Payment\Http\Endpoints\V1\AccountSaveEndpoint;
use Modules\Payment\Http\Endpoints\V1\TransactionDetailEndpoint;
use Modules\Payment\Http\Endpoints\V1\TransactionFireEndpoint;
use Modules\Payment\Http\Endpoints\V1\TransactionListEndpoint;
use Modules\Payment\Http\Endpoints\V1\TransactionPreviewEndpoint;
use Modules\Payment\Http\Endpoints\V1\TransactionSaveEndpoint;
use Modules\Payment\Http\Endpoints\V1\TransactionTrackEndpoint;

trait PaymentEndpointsTrait
{
    /**
     * Registers the endpoints only when the `Endpoints` module is available.
     *
     * @return void
     */
    public function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(AccountSaveEndpoint::class);
        endpoint()->register(AccountFormEndpoint::class);
        endpoint()->register(AccountPreviewEndpoint::class);
        endpoint()->register(AccountListEndpoint::class);
        endpoint()->register(AccountDeleteEndpoint::class);
        endpoint()->register(AccountActivateEndpoint::class);
        endpoint()->register(AccountDeactivateEndpoint::class);
        endpoint()->register(AccountMakeDefaultEndpoint::class);

        endpoint()->register(TransactionListEndpoint::class);
        endpoint()->register(TransactionPreviewEndpoint::class);
        endpoint()->register(TransactionSaveEndpoint::class);
        endpoint()->register(TransactionFireEndpoint::class);
        endpoint()->register(TransactionTrackEndpoint::class);
    }



    /**
     * Returns a uniformed array to be used in endpoints like fire.
     * <br>
     * In some cases, the frontend developer may need to submit a form with some dynamic information. This method will
     * generates the information array.
     *
     * @param string $action
     * @param string $method
     * @param array  $data
     *
     * @return array
     */
    public static function endpointFormResponse(string $action, string $method = 'GET', array $data = []): array
    {
        return [
             'form_action' => $action,
             'form_method' => $method,
             'form_data'   => $data,
        ];
    }
}
