<?php

namespace Modules\Payment\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Manage\Services\Widget;
use Modules\Payment\Services\PaymentHandler\Payment\PaymentTypes\Gates\GateAbstract;
use Modules\Yasna\Services\ModuleHelper;

class PaymentToolsServiceProvider extends ServiceProvider
{
    protected static $available_currencies = null;
    protected static $gate_classes         = [];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Returns an array of all available currencies.
     *
     * @return array
     */
    public static function getAvailableCurrencies()
    {
        if (is_null(self::$available_currencies)) {

            $currencies = (get_setting('site_currencies') ?? static::getConfigCurrencies());

            self::$available_currencies = $currencies;
        }

        return self::$available_currencies;
    }



    /**
     * Returns the currencies from the module's config.
     *
     * @return array
     */
    public static function getConfigCurrencies()
    {
        return module('payment')->getConfig('currencies');
    }



    /**
     * Returns an array of the available currencies and their titles, ready to be used in combos.
     *
     * @return array
     */
    public static function getAvailableCurrenciesCombo(): array
    {
        $result = [];
        foreach (self::getAvailableCurrencies() as $currency) {
            $result[] = ['id' => $currency, 'title' => self::getCurrencyTitle($currency)];
        }
        return $result;
    }



    /**
     * Returns the translated title of the given currency.
     *
     * @param string $currency
     *
     * @return string
     */
    public static function getCurrencyTitle(string $currency): string
    {
        return trans("payment::currency.$currency");
    }



    /**
     * Returns the translated title of the given currency.
     *
     * @param string $currency
     *
     * @return string
     */
    public static function getCurrencyTrans(string $currency): string
    {
        return self::getCurrencyTitle($currency);
    }



    /**
     * Returns an array of the available payment gates.
     *
     * @param bool $to_slug Whether returning array should contains the slugs of the gates.
     *
     * @return array
     */
    public static function getAvailableGates($to_slug = false): array
    {
        $possible_files = static::getPossibleGateFiles();

        return collect($possible_files)
             // filter the abstract class and the interface of the gates
             ->filter(function (string $file_name) {
                 return (
                      ends_with($file_name, '.php')
                      and
                      !starts_with($file_name, 'Gate')
                 );
             })
             // map to the file name without the file extension
             ->map(function (string $file_name) use ($to_slug) {
                 $clear = str_before($file_name, '.php');

                 return $to_slug ? str_slug($clear) : $clear;
             })
             // convert to array
             ->toArray()
             ;
    }



    /**
     * Returns an array of the names of the files located in the gates root path.
     *
     * @return array
     */
    protected static function getPossibleGateFiles(): array
    {
        return scandir(
             static::getGatesRootPath()
        );
    }



    /**
     * Returns an array of the path of the folder which contains the gate files.
     *
     * @return string
     */
    protected static function getGatesRootPath(): string
    {
        return implode(DIRECTORY_SEPARATOR, [
             module('payment')->getPath(),
             'Services',
             'PaymentHandler',
             'Payment',
             'PaymentTypes',
             'Gates',
        ]);
    }



    /**
     * Returns an array of the available gates, ready to be used in the combos.
     *
     * @param bool   $empty_option  Whether the array should contain an empty option or not.
     * @param string $value_field   The key of the value field in each option.
     * @param string $caption_field The key of the caption field in each option
     *
     * @return array
     */
    public static function getAvailableGatesCombo(
         bool $empty_option = false,
         string $value_field = 'id',
         string $caption_field = 'title'
    ) {
        // map the slug of the available gates to options
        $result = collect(self::getAvailableGates(true))
             ->map(function (string $gate_slug) use ($value_field, $caption_field) {
                 return [
                      $value_field   => $gate_slug,
                      $caption_field => self::getGateTitle($gate_slug),
                 ];
             });

        // prepend an empty option if needed
        if ($empty_option) {
            $result->prepend(
                 [
                      $value_field   => '',
                      $caption_field => trans('payment::general.none-of-theme'),
                 ]
            );
        }

        return $result->toArray();
    }



    /**
     * Returns a translated title of the given gate.
     *
     * @param string $gate_slug
     *
     * @return string
     */
    public static function getGateTitle(string $gate_slug): string
    {
        return trans("payment::gate.$gate_slug");
    }



    /**
     * Returns a translated title of the given gate.
     *
     * @param string $gate_slug
     *
     * @return string
     */
    public static function getGateTrans(string $gate_slug): string
    {
        return self::getGateTitle($gate_slug);
    }



    /**
     * Whether a gate with the specified slug exists.
     *
     * @param string $gate_slug
     *
     * @return bool
     */
    public static function gateExists(string $gate_slug): bool
    {
        return (
             $gate_slug
             and
             class_exists(self::getGateClass($gate_slug))
        );
    }



    /**
     * Whether a gate with the specified slug does not exist.
     *
     * @param string $gate_slug
     *
     * @return bool
     */
    public static function gateNotExists(string $gate_slug): bool
    {
        return !static::gateExists($gate_slug);
    }



    /**
     * Returns an array of the fields of the specified gate.
     *
     * @param string $gate_slug
     *
     * @return array
     */
    public static function getGateFields(string $gate_slug): array
    {
        if (static::gateNotExists($gate_slug)) {
            return [];
        }

        $class_full_name = static::getGateClass($gate_slug);

        return $class_full_name::additionalFields();
    }



    /**
     * Returns the full namespace of a gate with the specified slug.
     *
     * @param string $gate_slug
     *
     * @return string
     */
    protected static function getGateClass(string $gate_slug): string
    {
        if (!array_key_exists($gate_slug, static::$gate_classes)) {
            $module_namespace   = static::getModuleHelper()->getNamespace();
            $additive_namespace = implode("\\", array_merge(
                 static::getGatesPathParts(),
                 [ucfirst(camel_case($gate_slug))]
            ));

            static::$gate_classes[$gate_slug] = $module_namespace . $additive_namespace;
        }

        return static::$gate_classes[$gate_slug];
    }



    /**
     * Returns the name of a widget based on the given gate and field name.
     *
     * @param string $gate_slug
     * @param string $field
     *
     * @return string|null
     */
    public static function getGateFieldWidgetName(string $gate_slug, $field)
    {
        if (static::gateExists($gate_slug)) {
            /** @var GateAbstract $class_full_name */
            $class_full_name = static::getGateClass($gate_slug);

            return $class_full_name::additionalFieldWidgetName($field);
        }

        return null;
    }



    /**
     * Returns an instance of the `Modules\Manage\Services\Widget` class to be used in the form of the specified gate.
     *
     * @param string $gate_slug
     * @param string $field
     *
     * @return Widget|null
     */
    public static function getGateFieldWidget(string $gate_slug, string $field)
    {
        if (static::gateExists($gate_slug)) {
            /** @var GateAbstract $class_full_name */
            $class_full_name = static::getGateClass($gate_slug);

            return $class_full_name::additionalFieldWidget($field);
        }

        return null;
    }



    /**
     * Returns a nes instance of the module of this class.
     *
     * @return ModuleHelper
     */
    public static function getModuleHelper()
    {
        return module('payment');
    }



    /**
     * Returns the parts of the root path of the gates.
     *
     * @return array
     */
    protected static function getGatesPathParts(): array
    {
        return [
             'Services',
             'PaymentHandler',
             'Payment',
             'PaymentTypes',
             'Gates',
        ];
    }



    /**
     * Returns an array of all the available payment types.
     *
     * @return array
     */
    public static function allPaymentTypes(): array
    {
        return config('payment.types');
    }



    /**
     * Returns a combo array of the payment types.
     *
     * @param bool $empty_option
     *
     * @return array
     */
    public static function allPaymentTypesCombo($empty_option = false)
    {
        return static::paymentTypesCombo(static::allPaymentTypes(), $empty_option);
    }



    /**
     * Returns an array of all the payment types.
     *
     * @return array
     */
    public static function allOfflinePaymentTypes(): array
    {
        return collect(static::allPaymentTypes())
             ->filter(function (string $type) {
                 return $type != 'online';
             })
             ->values()
             ->toArray()
             ;
    }



    /**
     * Returns a combo array of all the offline payment types.
     *
     * @param bool $empty_option
     *
     * @return array
     */
    public static function allOfflinePaymentTypesCombo($empty_option = false)
    {
        return static::paymentTypesCombo(static::allOfflinePaymentTypes(), $empty_option);
    }



    /**
     * Returns a combo array of the specified data.
     *
     * @param array       $data
     * @param string|null $empty_option
     * @param string      $value_field
     * @param string      $caption_field
     *
     * @return array
     */
    protected static function paymentTypesCombo(
         array $data,
         $empty_option = null,
         string $value_field = 'id',
         string $caption_field = 'title'
    ) {
        // map the data to combo options
        $result = collect($data)
             ->map(function (string $type) use ($value_field, $caption_field) {
                 return [
                      $value_field   => $type,
                      $caption_field => static::getPaymentTypeTitle($type),
                 ];
             });

        // prepend an empty option if needed
        if ($empty_option) {
            $result->prepend(
                 [
                      $value_field   => '',
                      $caption_field => is_string($empty_option)
                           ? $empty_option
                           : trans('payment::general.none-of-theme'),
                 ]
            );
        }

        return $result->toArray();
    }



    /**
     * Returns a translated title of the specified payment type.
     *
     * @param string $type
     *
     * @return string
     */
    public static function getPaymentTypeTitle(string $type): string
    {
        return trans("payment::types.$type");
    }



    /**
     * Returns a translated title of the specified payment type.
     *
     * @param string $type
     *
     * @return string
     */
    public static function getPaymentTypeTrans(string $type): string
    {
        return self::getPaymentTypeTitle($type);
    }



    /**
     * Whether the specified type is a valid payment type.
     *
     * @param string $type
     *
     * @return bool
     */
    public static function typeIsValid(string $type): bool
    {
        return in_array($type, self::allPaymentTypes());
    }



    /**
     * Whether the specified type is not a valid payment type.
     *
     * @param string $type
     *
     * @return bool
     */
    public static function typeIsNotValid(string $type): bool
    {
        return !static::typeIsValid($type);
    }



    /**
     * Whether the specified type is an acceptable payment type.
     *
     * @param string $type
     *
     * @return bool
     */
    public static function isAcceptablePaymentType(string $type)
    {
        return self::typeIsValid($type);
    }



    /**
     * Whether the specified type is not an acceptable payment type.
     *
     * @param string $type
     *
     * @return bool
     */
    public static function isNotAcceptablePaymentType(string $type)
    {
        return !static::isAcceptablePaymentType($type);
    }



    /**
     * Converts the given card number to a viewable version.
     *
     * @param string|null $number
     *
     * @return string|null
     */
    public static function cardNumberToView($number)
    {
        return $number
             ? rtrim(chunk_split($number, 4, '-'), '-')
             : null;
    }
}
