<?php

namespace Modules\Payment\Providers;

use Maatwebsite\Excel\ExcelServiceProvider;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction;
use Modules\Yasna\Services\YasnaProvider;

class PaymentServiceProvider extends YasnaProvider
{
    use PaymentEndpointsTrait;



    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerAliases();
        $this->registerProviders();
        $this->registerDownstream();
        $this->registerSidebar();
        $this->registerEndpoints();
        $this->registerRoleSampleModules();
    }



    /**
     * Registers aliases
     */
    protected function registerAliases()
    {
        $aliases = [
             'Payment'            => PaymentServiceProvider::class,
             'PaymentRouteTools'  => RouteServiceProvider::class,
             'PaymentTools'       => PaymentToolsServiceProvider::class,
             'TransactionHandler' => Transaction::class,
        ];

        foreach ($aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }
    }



    /**
     * Registers providers.
     */
    protected function registerProviders()
    {
        $providers = [
             RouteServiceProvider::class,
             PaymentToolsServiceProvider::class,
             ExcelServiceProvider::class,
        ];

        foreach ($providers as $class) {
            $this->addProvider($class);
        }
    }



    /**
     * Registers downstream services.
     */
    protected function registerDownstream()
    {
        module('manage')
             ->service('downstream')
             ->add('payment-accounts')
             ->link('payment-accounts')
             ->trans('payment::general.account.title.plural')
             ->method('payment:Manage\Downstream\AccountsController@index')
             ->condition(function () {
                 return user()->as('admin')->can('payment_online_accounts.browse') or
                      user()->as('admin')->can('payment_offline_accounts.browse');
             })
             ->order(40)
        ;
    }



    /**
     * Registers sidebar.
     */
    protected function registerSidebar()
    {
        service("manage:sidebar")
             ->add("transactionsSideBarMenu")
             ->blade($this->module()->getAlias() . "::presenters.manage-sidebar-menu")
             ->condition(function () {
                 return user()->as('admin')->can('payment_transactions.browse');
             })
             ->order(35)
        ;
    }



    /**
     * Registers the roles sample of the payment module.
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('payment_online_accounts')
             ->value('create , edit ,  browse ,  delete ,')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('payment_offline_accounts')
             ->value('create , edit ,  browse ,  delete ,')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('payment_transactions')
             ->value('browse , export ,')
        ;
    }
}
