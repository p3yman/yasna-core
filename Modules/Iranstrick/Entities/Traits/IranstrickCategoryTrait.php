<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/2/18
 * Time: 3:25 AM
 */

namespace Modules\Iranstrick\Entities\Traits;

use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

trait IranstrickCategoryTrait
{
    /**
     * @return null|string
     */
    public function getDirectUrlAttribute()
    {
        if (!$this->is_folder) {
            return CurrentModuleHelper::actionLocale('ProductController@category', [
                 'folder'   => $this->folder->slug,
                 'category' => $this->slug,
            ]);
        }

        return null;
    }
}
