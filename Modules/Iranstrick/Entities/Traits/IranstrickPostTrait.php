<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/2/18
 * Time: 2:51 AM
 */

namespace Modules\Iranstrick\Entities\Traits;

use Carbon\Carbon;
use Modules\Iranstrick\Providers\IranstrickServiceProvider;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

trait IranstrickPostTrait
{
    /**
     * @return string|null
     */
    public function getDirectUrlAttribute()
    {
        $property = "{$this->type}_direct_url";
        return $this->$property;
    }



    /**
     * @return string
     */
    public function getProductsDirectUrlAttribute()
    {
        return CurrentModuleHelper::action('ProductController@single', [
             'lang'   => $this->locale,
             'hashid' => $this->hashid,
             'title'  => IranstrickServiceProvider::urlHyphen($this->title),
        ]);
    }



    /**
     * @return string
     */
    public function getServicesDirectUrlAttribute()
    {
        return CurrentModuleHelper::action('ServiceController@single', [
             'lang'   => $this->locale,
             'hashid' => $this->hashid,
             'title'  => IranstrickServiceProvider::urlHyphen($this->title),
        ]);
    }



    /**
     * @return string
     */
    public function getNewsDirectUrlAttribute()
    {
        return CurrentModuleHelper::action('NewsController@single', [
             'lang'   => $this->locale,
             'hashid' => $this->hashid,
             'title'  => IranstrickServiceProvider::urlHyphen($this->title),
        ]);
    }



    /**
     * @return string
     */
    public function getExhibitionsDirectUrlAttribute()
    {
        return CurrentModuleHelper::action('ExhibitionsController@single', [
             'lang'   => $this->locale,
             'hashid' => $this->hashid,
             'title'  => IranstrickServiceProvider::urlHyphen($this->title),
        ]);
    }



    /**
     * Check id client can register for this post or not.
     *
     * @return bool
     */
    public function getCanRegisterAttribute()
    {
        return ($this->register_status == 'ACTIVE');
    }



    /**
     * @return string
     */
    public function getRegisterStatusAttribute()
    {
        if (
             $this->posttype->hasFeature('register') and
             $this->register_starts_at and
             $this->register_ends_at
        ) {
            if ((Carbon::now()->greaterThan(Carbon::parse($this->register_starts_at)))) {
                if (Carbon::now()->lessThan(Carbon::parse($this->register_ends_at))) {
                    return 'ACTIVE';
                } else {
                    return 'EXPIRED';
                }
            } else {
                return 'PENDING';
            }
        } else {
            return 'INACTIVE';
        }
    }



    /**
     * Apply publishment condition.
     */
    public function consistentElectorCriteria()
    {
        $this->elector()->whereNotNull('posts.published_at')->where('posts.published_by', '>', 0);
    }
}
