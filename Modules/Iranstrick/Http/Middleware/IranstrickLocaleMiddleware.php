<?php

namespace Modules\Iranstrick\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Iranstrick\Services\Tools\Module\ModuleHandler;

class IranstrickLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->lang) {
            app()->setLocale(ModuleHandler::create(get_class($this))->config('default-locale'));
        }
        return $next($request);
    }
}
