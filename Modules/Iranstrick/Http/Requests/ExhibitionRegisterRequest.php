<?php

namespace Modules\Iranstrick\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class ExhibitionRegisterRequest extends YasnaRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
             'post_id'              => [
                  'required',
                  Rule::exists('posts', 'id')
                      ->where('type', 'exhibitions'),
             ],
             'name'                 => 'required',
             'mobile'               => 'required|phone:mobile',
             'email'                => 'required|email',
             'g-recaptcha-response' => (!env('APP_DEBUG') ? 'required|captcha' : ''),
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'mobile'  => 'ed',
             'post_id' => 'dehash',
        ];
    }
}
