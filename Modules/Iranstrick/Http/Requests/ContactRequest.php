<?php

namespace Modules\Iranstrick\Http\Requests;

use App\Models\Post;
use Modules\Iranstrick\Providers\CommentingServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class ContactRequest extends YasnaRequest
{


    /**
     * @return bool
     */
    public function authorize()
    {
        if ($this->post) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'post_id'   => 'dehash',
             'parent_id' => 'dehash',
             'mobile'    => 'ed',
        ];
    }



    /**
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive($this->getGeneralRules(), $this->getPostRules());
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             'text.required_without' => trans('validation.required'),
        ];
    }



    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post ?: new Post;
    }



    /**
     * @return array
     */
    protected function getPostRules()
    {
        if (
             array_key_exists('post_id', $data = $this->data) and
             ($post = model('post')->grab($data['post_id'])) and
             $post->exists
        ) {
            $this->post = $post->spreadMeta();
            return self::safeRulesArray(CommentingServiceProvider::translateRules($this->post->rules));
        }

        return [];
    }



    /**
     * @return array
     */
    protected function getGeneralRules()
    {
        return self::safeRulesArray([
             'post_id'              => 'exists:posts,id',
             'parent_id'            => 'exists:comments,id',
             'email'                => 'email',
             'mobile'               => 'phone:mobile',
             'text'                 => 'required_without:message',
             'g-recaptcha-response' => (!env('APP_DEBUG') ? 'required|captcha' : ''),
        ]);
    }



    /**
     * @param array $rules
     *
     * @return array
     */
    protected static function safeRulesArray(array $rules)
    {
        return array_map(function ($item) {
            return is_string($item) ? explode('|', $item) : $item;
        }, $rules);
    }
}
