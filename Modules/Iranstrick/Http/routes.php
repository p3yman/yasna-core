<?php
/*
 * All Routes
 */
Route::group(['namespace' => 'Modules\Iranstrick\Http\Controllers'], function () {

    /**
     * Web Routes
     */
    Route::group(['middleware' => 'web'], function () {
        Route::group(['namespace' => 'Auth'], function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login');
        });


        Route::group(['middleware' => ['locale', 'iranstrick-locale']], function () {
            Route::get('/', 'IranstrickController@index');

            Route::group(['prefix' => '{lang}'], function () {
                Route::get('/', 'IranstrickController@index');
                Route::get('about', 'IranstrickController@about');

                /**
                 * Products
                 */
                Route::group(['prefix' => 'products'], function () {
                    Route::get('{folder}/{category}', 'ProductController@category');
                });
                Route::group(['prefix' => 'product'], function () {
                    Route::get('show/{hashid}/{title?}', 'ProductController@single');
                });


                /**
                 * Services
                 */
                Route::group(['prefix' => 'service'], function () {
                    Route::get('{hashid}/{title?}', 'ServiceController@single');
                });


                /**
                 * News
                 */
                Route::group(['prefix' => 'news'], function () {
                    Route::get('/', 'NewsController@index');
                    Route::get('{hashid}/{title?}', 'NewsController@single');
                });


                /**
                 * Contact
                 */
                Route::group(['prefix' => 'contact'], function () {
                    Route::get('/', 'ContactController@index');
                    Route::post('save', 'ContactController@save');
                });

                /**
                 * Exhibitions
                 */
                Route::group(['prefix' => 'expo'], function () {
                    Route::get('/', 'ExhibitionsController@index');
                    Route::get('{hashid}/{title?}', 'ExhibitionsController@single');
                    Route::post('register', 'ExhibitionsController@register');
                });
            });
        });
    });
});


/*
 * Test Routes
 */
Route::group(['middleware' => 'web', 'prefix' => 'iranstrick', 'namespace' => 'Modules\Iranstrick\Http\Controllers'],
     function () {
         //Route::get('/', 'IranstrickController@index');


         /*
          * Test Routes
          * */
         Route::group(['middleware' => 'locale'], function () {
             Route::group(['prefix' => '{lang?}'], function () {
                 Route::group(['prefix' => 'test'], function () {
                     Route::get('/', 'TestController@index');

                     Route::get('/about', 'TestController@about');

                     Route::get('/contact', 'TestController@contact');

                     Route::get('/news', 'TestController@news');

                     Route::get('/expo', 'TestController@expo');

                     Route::get('/news/single', 'TestController@singlePage');

                     Route::get('/show-product', 'TestController@showProduct');

                     Route::get('/brands', 'TestController@brands');
                 });
             });
         });
     });
