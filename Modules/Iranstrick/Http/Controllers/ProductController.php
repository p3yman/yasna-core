<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/2/18
 * Time: 1:05 AM
 */

namespace Modules\Iranstrick\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Iranstrick\Services\Tools\Controller\IranstrickFrontControllerAbstract;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

class ProductController extends IranstrickFrontControllerAbstract
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function category(Request $request)
    {
        return $this->categoryFindFolderAndCategory($request->folder, $request->category)
                    ->categoryResult()
             ;
    }



    /**
     * @param string|null $folder_slug
     * @param string|null $category_slug
     *
     * @return $this
     */
    protected function categoryFindFolderAndCategory($folder_slug, $category_slug)
    {
        if (
             $folder_slug and
             $category_slug and
             ($folder = model('posttype', $this->posttype_slugs['product'])
                  ->folders()
                  ->whereSlug($folder_slug)->first()
             ) and
             ($category = $folder->children()->whereSlug($category_slug)->first()) and
             $category->titleIn(getLocale())
        ) {
            $this->appendToVariables('category', $category);
        }
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function categoryResult()
    {
        return array_key_exists('category', $this->variables)
             ? $this
                  ->categorySisterhoodLink()
                  ->categoryFindPosts()
                  ->template('product.category.main')
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function categorySisterhoodLink()
    {
        $category = $this->variables['category'];
        foreach (availableLocales() as $locale) {
            if ($locale == getLocale()) {
                continue;
            }
            if (!$category->titleIn($locale)) {
                $this->sisterhood_links[$locale] = CurrentModuleHelper::actionLocale('IranstrickController@index');
            }
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function categoryFindPosts()
    {
        return $this->appendToVariables(
             'posts',
             model('post')
                  ->elector([
                       'type'     => $this->posttype_slugs['product'],
                       'category' => $this->variables['category']->id,
                       'locale'   => getLocale(),
                  ])
                  ->orderByDesc('published_at')
                  ->limit(60)
                  ->get()
        );
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(Request $request)
    {
        return (($post = model('post', $request->hashid)) and ($post->type == $this->posttype_slugs['product']))
             ? $this
                  ->appendToVariables('post', $post)
                  ->singleDefaultCategory()
                  ->singleSisterhoodLinks()
                  ->template('product.single.main')
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function singleDefaultCategory()
    {
        return $this->appendToVariables('default_category', $this->variables['post']->guessDefaultCategory());
    }



    /**
     * @return $this
     */
    protected function singleSisterhoodLinks()
    {
        $post             = $this->variables['post'];
        $default_category = $this->variables['default_category'];
        foreach (availableLocales() as $locale) {
            if (($sister = $post->in($locale))->exists) {
                $this->sisterhood_links[$locale] = $sister->direct_url;
            } else {
                $this->sisterhood_links[$locale] = ($default_category->direct_url
                     ?: CurrentModuleHelper::actionLocale('IranstrickController@index'));
            }
        }

        return $this;
    }
}
