<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/2/18
 * Time: 5:41 AM
 */

namespace Modules\Iranstrick\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Iranstrick\Services\Tools\Controller\IranstrickFrontControllerAbstract;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

class NewsController extends IranstrickFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->findPosttype()
                    ->findPosts()
                    ->indexResult()
             ;
    }



    /**
     * @return $this
     */
    protected function findPosttype()
    {
        return $this->appendToVariables('posttype', model('posttype', $this->posttype_slugs['news']));
    }



    /**
     * @return $this
     */
    protected function findPosts()
    {
        return $this->appendToVariables(
             'posts',
             model('post')->elector([
                  'type'   => $this->posttype_slugs['news'],
                  'locale' => getLocale(),
             ])->orderByDesc('published_at')
                          ->paginate(24)
        );
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function indexResult()
    {
        return ($this->variables['posttype']->exists and $this->variables['posts']->count())
             ? $this
                  ->exhibitionsSlideshow()
                  ->template('news.list.main')
             : $this->abort(404);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(Request $request)
    {
        return (
             ($post = model('post', $request->hashid)) and
             ($post->type == $this->posttype_slugs['news']) and
             ($posttype = model('posttype', $this->posttype_slugs['news']))->exists
        )
             ? $this
                  ->appendToVariables('post', $post)
                  ->appendToVariables('posttype', $posttype)
                  ->exhibitionsSlideshow()
                  ->singleSisterhoodLinks()
                  ->template('news.single.main')
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function singleSisterhoodLinks()
    {
        $post = $this->variables['post'];
        foreach (availableLocales() as $locale) {
            if (($sister = $post->in($locale))->exists) {
                $this->sisterhood_links[$locale] = $sister->direct_url;
            } else {
                $this->sisterhood_links[$locale] = CurrentModuleHelper::action('NewsController@index', [
                     'lang' => $locale,
                ]);
            }
        }

        return $this;
    }
}
