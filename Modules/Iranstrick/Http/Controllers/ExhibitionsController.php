<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/4/18
 * Time: 11:18 PM
 */

namespace Modules\Iranstrick\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Iranstrick\Http\Requests\ExhibitionRegisterRequest;
use Modules\Iranstrick\Services\Tools\Controller\IranstrickFrontControllerAbstract;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

class ExhibitionsController extends IranstrickFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->findPosttype()
                    ->findPosts()
                    ->indexResult()
             ;
    }



    /**
     * @return $this
     */
    protected function findPosttype()
    {
        return $this->appendToVariables('posttype', model('posttype', $this->posttype_slugs['expo']));
    }



    /**
     * @return $this
     */
    protected function findPosts()
    {
        return $this->appendToVariables(
             'posts',
             model('post')->elector([
                  'type'   => $this->posttype_slugs['expo'],
                  'locale' => getLocale(),
             ])->orderByDesc('published_at')
                          ->paginate(24)
        );
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function indexResult()
    {
        return ($this->variables['posttype']->exists and $this->variables['posts']->count())
             ? $this
                  ->newsSlideshow()
                  ->template('exhibitions.list.main')
             : $this->abort(404);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(Request $request)
    {
        return (
             ($post = model('post', $request->hashid)) and
             ($post->type == $this->posttype_slugs['expo']) and
             ($posttype = model('posttype', $this->posttype_slugs['expo']))->exists
        )
             ? $this
                  ->appendToVariables('post', $post)
                  ->appendToVariables('posttype', $posttype)
                  ->newsSlideshow()
                  ->singleSisterhoodLinks()
                  ->template('exhibitions.single.main')
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function singleSisterhoodLinks()
    {
        $post = $this->variables['post'];
        foreach (availableLocales() as $locale) {
            if (($sister = $post->in($locale))->exists) {
                $this->sisterhood_links[$locale] = $sister->direct_url;
            } else {
                $this->sisterhood_links[$locale] = CurrentModuleHelper::action('ExhibitionsController@index', [
                     'lang' => $locale,
                ]);
            }
        }

        return $this;
    }



    /**
     * Find News Posts
     *
     * @return $this
     */
    protected function newsSlideshow()
    {
        return $this->appendToVariables(
             'exhibitions',
             model('post')->elector([
                  'type'   => $this->posttype_slugs['news'],
                  'locale' => getLocale(),
             ])->orderByDesc('published_at')
                          ->limit(5)
                          ->get()
        );
    }



    /**
     * @param ExhibitionRegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(ExhibitionRegisterRequest $request)
    {
        if (model('enrollment')->where($request->only(['email', 'mobile', 'post_id']))->first()) {
            return redirect()
                 ->back()
                 ->withErrors(['_submit' => CurrentModuleHelper::trans('front.enrollment_duplicate')])
                 ->withInput(request()->all())
                 ;
        } else {
            $enrollment_model = model('enrollment')->batchSave($request->all());

            if ($enrollment_model->exists) {
                return redirect()
                     ->back()
                     ->with('success_message', CurrentModuleHelper::trans('front.enrollment_succeeded'))
                     ;
            } else {
                return redirect()
                     ->back()
                     ->withErrors(['_submit' => CurrentModuleHelper::trans('front.enrollment_failed')])
                     ->withInput(request()->all())
                     ;
            }
        }
    }
}
