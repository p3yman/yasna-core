<?php

namespace Modules\Iranstrick\Http\Controllers\Auth;

use Modules\Iranstrick\Services\Tools\Controller\IranstrickControllerTemplateTrait;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;
use Modules\Iranstrick\Services\Tools\Module\ModuleHandler;

class LoginController extends \Modules\Yasna\Http\Controllers\Auth\LoginController
{
    use IranstrickControllerTemplateTrait;


    protected $module;



    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        CurrentModuleHelper::assign(ModuleHandler::create(get_class($this)));
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        $this->setTemplateSiteTitle();
        return view(CurrentModuleHelper::bladePath('auth.login'));
    }
}
