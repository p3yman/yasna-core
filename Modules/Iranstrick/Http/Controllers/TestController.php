<?php

namespace Modules\Iranstrick\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class TestController extends Controller
{
    public function index()
    {
        return view('iranstrick::test.front.home.0');
    }

    public function about()
    {
        return view('iranstrick::test.front.about.0');
    }

    public function contact()
    {
        return view('iranstrick::test.front.contact.0');
    }

    public function news()
    {
        return view('iranstrick::test.front.news.0');
    }

    public function expo()
    {
        return view('iranstrick::test.front.expo.0');
    }

    public function brands()
    {
        return view('iranstrick::test.front.brands.0');
    }

    public function showProduct()
    {
        return view('iranstrick::test.front.show_product.0');
    }

    public function singlePage()
    {
        return view('iranstrick::test.front.pages.0');
    }
}
