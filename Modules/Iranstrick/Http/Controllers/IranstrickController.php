<?php

namespace Modules\Iranstrick\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Iranstrick\Services\Tools\Controller\IranstrickFrontControllerAbstract;

class IranstrickController extends IranstrickFrontControllerAbstract
{
    /**
     * Index Page of Any Locale
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this
             ->indexSlideShow()
             ->exhibitionsSlideshow()
             ->indexNews()
             ->indexLinks()
             ->template('home.main')
             ;
    }



    /**
     * Find SlideShow Posts
     *
     * @return $this
     */
    protected function indexSlideShow()
    {
        return $this->appendToVariables(
             'slideshow',
             model('post')->elector([
                  'type'   => $this->posttype_slugs['slideshow'],
                  'locale' => getLocale(),
             ])->orderByDesc('published_at')
                          ->limit(12)
                          ->get()
        );
    }



    /**
     * Find New Posts
     *
     * @return $this
     */
    protected function indexNews()
    {
        return $this->appendToVariables(
             'news',
             model('post')->elector([
                  'type'   => $this->posttype_slugs['news'],
                  'locale' => getLocale(),
             ])->orderByDesc('published_at')
                          ->limit(10)
                          ->get()
        );
    }



    /**
     * Find Products Categories
     *
     * @return $this
     */
    protected function indexLinks()
    {
        return $this->appendToVariables(
             'links',
             model('posttype', $this->posttype_slugs['product'])
                  ->categories()
                  ->whereIsFolder(false)
                  ->orderByDesc('created_at')
                  ->limit(24)
                  ->get()
        );
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function about(Request $request)
    {
        return $this->aboutStaticPost()
                    ->aboutResult()
             ;
    }



    /**
     * @return $this;
     */
    protected function aboutStaticPost()
    {
        return $this->appendToVariables(
             'static_post',
             (
                  (
                  $post = model('post')
                       ->elector([
                            'type' => $this->posttype_slugs['static'],
                            'slug' => 'about',
                       ])
                       ->first()
                  ) and
                  ($post = $post->in(getLocale())) and
                  $post->exists
             )
                  ? $post
                  : model('post')
        );
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function aboutResult()
    {
        return ($post = $this->variables['static_post'])->exists
             ? $this->template('about.main')
             : $this->abort(404);
    }
}
