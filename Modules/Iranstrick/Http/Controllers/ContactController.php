<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/4/18
 * Time: 8:33 PM
 */

namespace Modules\Iranstrick\Http\Controllers;

use Modules\Iranstrick\Http\Requests\ContactRequest;
use Modules\Iranstrick\Services\Tools\Controller\IranstrickFrontControllerAbstract;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

class ContactController extends IranstrickFrontControllerAbstract
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->indexCommentingPost()
                    ->template('contact.main')
             ;
    }



    /**
     * @return $this
     */
    protected function indexCommentingPost()
    {
        return $this->appendToVariables(
             'commenting_post',
             model('post')
                  ->elector([
                       'type'   => $this->posttype_slugs['commenting'],
                       'slug'   => 'contact',
                       'locale' => getLocale(),
                  ])
                  ->first() ?: model('post')
        );
    }



    /**
     * @param ContactRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(ContactRequest $request)
    {
        $this->request = $request;

        $savedComment = model('comment')
             ->batchSave($this->saveChangeIndexes($request->all()));

        if ($savedComment->exists) {
            return redirect()
                 ->back()
                 ->with('success_message', CurrentModuleHelper::trans('front.message_save_succeeded'))
                 ;
        } else {
            return redirect()
                 ->withInput($request->all())
                 ->withErrors([
                      'submit' => [
                           CurrentModuleHelper::trans('front.message_save_failed'),
                      ],
                 ])
                 ;
        }
    }



    /**
     * @param array $data
     *
     * @return array
     */
    protected function saveChangeIndexes(array $data)
    {
        $indexes_to_change = [
             'name'    => 'guest_name',
             'email'   => 'guest_email',
             'mobile'  => 'guest_mobile',
             'message' => 'text',
        ];
        foreach ($indexes_to_change as $from => $to) {
            if (array_key_exists($from, $data)) {
                $data[$to] = $data[$from];
                unset($data[$from]);
            }
        }

        return array_default($data, $this->saveAdditionalIndexes());
    }



    /**
     * @return array
     */
    protected function saveAdditionalIndexes()
    {
        return [
             'guest_ip'    => request()->ip(),
             'user_id'     => user()->id,
             'posttype_id' => $this->request->getPost()->posttype->id,
        ];
    }
}
