<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/2/18
 * Time: 4:35 AM
 */

namespace Modules\Iranstrick\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Iranstrick\Services\Tools\Controller\IranstrickFrontControllerAbstract;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

class ServiceController extends IranstrickFrontControllerAbstract
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(Request $request)
    {
        return (
             ($post = model('post', $request->hashid)) and
             ($post->type == $this->posttype_slugs['service']) and
             ($posttype = model('posttype', $this->posttype_slugs['service']))->exists
        )
             ? $this
                  ->appendToVariables('post', $post)
                  ->appendToVariables('posttype', $posttype)
                  ->singleSisterhoodLinks()
                  ->exhibitionsSlideshow()
                  ->template('service.single.main')
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function singleSisterhoodLinks()
    {
        $post = $this->variables['post'];
        foreach (availableLocales() as $locale) {
            if (($sister = $post->in($locale))->exists) {
                $this->sisterhood_links[$locale] = $sister->direct_url;
            } else {
                $this->sisterhood_links[$locale] = CurrentModuleHelper::action('IranstrickController@index', [
                     'lang' => $locale,
                ]);
            }
        }

        return $this;
    }
}
