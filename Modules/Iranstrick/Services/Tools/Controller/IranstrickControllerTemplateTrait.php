<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:29 PM
 */

namespace Modules\Iranstrick\Services\Tools\Controller;

use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;
use Modules\Iranstrick\Services\Tools\Template\IranstrickTemplateHandler as Template;

trait IranstrickControllerTemplateTrait
{
    protected $sisterhood_links = [];



    /**
     * Get the evaluated view contents for the given view in this current module.
     *
     * @param  string $view
     * @param  array  $data
     * @param  array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function template(...$parameters)
    {
        $this
             ->setTemplateSiteTitle()
             ->setTemplateOpenGraphs()
             ->setTemplateMainMenu()
             ->setTemplateFooterMenu()
             ->setTemplateLocalesUrls()
             ->setTemplateContactInfo()
             ->setTemplateYasnaInfo()
        ;
        return parent::template(...$parameters);
    }



    /**
     * @return $this
     */
    protected function setTemplateSiteTitle()
    {
        Template::setSiteTitle($settingTitle = get_setting('site_title'));
        Template::appendToPageTitle($settingTitle);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateOpenGraphs()
    {
        Template::setOpenGraphs([
             'title'       => Template::siteTitle(),
             'url'         => request()->url(),
             'image'       => Template::siteLogoUrl(),
             'description' => function () {
                 if (count($pageTitleArray = Template::pageTitle()) > 1) {
                     return implode(' - ', array_slice($pageTitleArray, 1));
                 }
                 return null;
             },
        ]);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateMainMenu()
    {
        Template::setMainMenu(menu('main')->items);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateFooterMenu()
    {
        Template::setFooterMenu(menu('footer')->items);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateLocalesUrls()
    {
        $current_route      = request()->route();
        $has_lang_parameter = $current_route->hasParameter('lang');
        $parameters         = $current_route->parameters();

        $result = [];
        foreach (availableLocales() as $language) {
            if ($language == getLocale()) {
                $url = '#';
            } elseif (array_key_exists($language, $this->sisterhood_links)) {
                $url = $this->sisterhood_links[$language];
            } elseif ($has_lang_parameter) {
                $url = action(
                     '\\' . $current_route->getActionName(),
                     array_merge($parameters, ['lang' => $language])
                );
            } else {
                $url = CurrentModuleHelper::action('IranstrickController@index', ['lang' => $language]);
            }
            $result[$language] = [
                 'title' => CurrentModuleHelper::trans('front.language-title', [], $language),
                 'url'   => $url,
            ];
        }
        Template::setLanguagesUrls($result);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateContactInfo()
    {
        if ($address = get_setting('address')) {
            Template::mergeWithContactInfo(['address' => $address]);
        }
        if ($telephone = get_setting('telephone') and is_array($telephone)) {
            Template::mergeWithContactInfo(['telephone' => $telephone]);
        }
        if ($email = get_setting('email') and is_array($email)) {
            Template::mergeWithContactInfo(['email' => $email]);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateYasnaInfo()
    {
        Template::setYasnaGroupTitle(get_setting('yasna_group_title'));
        Template::setYasnaGroupUrl(get_setting('yasna_group_url'));

        return $this;
    }
}
