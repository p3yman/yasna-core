<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:14 PM
 */

namespace Modules\Iranstrick\Services\Tools\Controller;

use Modules\Iranstrick\Services\Tools\Template\IranstrickTemplateHandler as Template;

abstract class IranstrickFrontControllerAbstract extends IranstrickControllerAbstract
{
    use IranstrickControllerTemplateTrait {
        template as protected traitTemplate;
    }
    use FeedbackControllerTrait {
        abort as protected traitAbort;
    }

    protected $variables = [];

    protected $posttype_slugs = [
         'slideshow'  => 'slideshows',
         'expo'       => 'exhibitions',
         'news'       => 'news',
         'product'    => 'products',
         'static'     => 'pages',
         'service'    => 'services',
         'commenting' => 'commenting',
    ];



    /**
     * @param       $errorCode
     * @param bool  $minimal if true minimal view will be loading
     * @param mixed $data
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    protected function abort(...$parameters)
    {
        return $this->setTemplateSiteTitle()
                    ->traitAbort(...$parameters)
             ;
    }



    /**
     * @param string $variableName
     * @param mixed  $variableValue
     *
     * @return $this
     */
    protected function appendToVariables(string $variableName, $variableValue)
    {
        $this->variables[$variableName] = $variableValue;

        return $this;
    }



    /**
     * Add variables of class to the sent parameters.
     *
     * @param mixed ...$parameters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function template(...$parameters)
    {
        $parameters[1] = array_merge(($parameters[1] ?? []), $this->variables);

        return $this->traitTemplate(...$parameters);
    }



    /**
     * Find Expo Posts
     *
     * @return $this
     */
    protected function exhibitionsSlideshow()
    {
        return $this->appendToVariables(
             'exhibitions',
             model('post')->elector([
                  'type'   => $this->posttype_slugs['expo'],
                  'locale' => getLocale(),
             ])->orderByDesc('published_at')
                          ->limit(5)
                          ->get()
        );
    }
}
