<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:37 PM
 */

namespace Modules\Iranstrick\Services\Tools\OOP;

use BadMethodCallException;
use SplStack;

trait MagicMethodsTrait
{
    protected $magic_methods_stack;



    /**
     * Handle unknown called methods.
     * <br>
     * This method is calling any method starting with "__call".
     * <br>
     * Any of them pop the called method from the stack (by calling popMagicMethod() method) loop will end.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        $this->pushMagicMethod($method);

        $callMethods = $this->filterMethods(function ($methodName) {
            return preg_match("/^__call[[:alnum:]]+/i", $methodName);
        });

        foreach ($callMethods as $magicMethod) {
            $methodResult = $this->$magicMethod($method, $parameters);
            if ($this->magicMethodIsNotTop($method)) { // This method has been popped from the stack
                return $methodResult;
            }
        }

        throw new BadMethodCallException(
             sprintf('Call to undefined method %s::%s()', get_class($this), $method)
        );
    }



    /**
     * Push method to the stack.
     *
     * @param string $method
     */
    protected function pushMagicMethod($method)
    {
        ($stack = &$this->getMagicMethodsStackPointer())->push($method);
    }



    /**
     * Pop method from the stack.
     *
     * @return string
     */
    protected function popMagicMethod()
    {
        return ($stack = &$this->getMagicMethodsStackPointer())->pop();
    }



    /**
     * Check if given method name is the last item in the stack.
     *
     * @param string $method
     *
     * @return bool
     */
    protected function magicMethodIsTop($method)
    {
        return ($this->getMagicMethodsstack()->count() and ($this->getMagicMethodsstack()->top() == $method));
    }



    /**
     * Check if given method name is not the last item in the stack.
     *
     * @param string $method
     *
     * @return bool
     */
    protected function magicMethodIsNotTop($method)
    {
        return !$this->magicMethodIsTop($method);
    }



    /**
     * @return SplStack
     */
    protected function getMagicMethodsStack()
    {
        return $this->magic_methods_stack ?: ($this->magic_methods_stack = new SplStack);
    }



    /**
     * @return SplStack
     */
    protected function &getMagicMethodsStackPointer()
    {
        $this->getMagicMethodsStack();
        return $this->magic_methods_stack;
    }



    /**********************************************************
     * Filtering Methods
     **********************************************************/


    /**
     * Filter all methods of this class with the given condition.
     *
     * @param callable $condition
     *
     * @return array
     */
    protected function filterMethods(callable $condition)
    {
        return array_where($this->getAllMethods(), $condition);
    }



    /**
     * Get list of all methods of this class.
     *
     * @return array
     */
    protected function getAllMethods()
    {
        return get_class_methods($this);
    }



    /**
     * Get list of all methods of this class.
     *
     * @return array
     */
    protected function getMethods()
    {
        return $this->getAllMethods();
    }



    /**
     * Get list of all methods of this class.
     *
     * @return array
     */
    protected function methods()
    {
        return $this->getAllMethods();
    }



    /**
     * Get list of all methods of this class.
     *
     * @return array
     */
    protected function allMethods()
    {
        return $this->getAllMethods();
    }
}
