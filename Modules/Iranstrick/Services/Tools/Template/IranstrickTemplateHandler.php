<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 11:52 AM
 */


namespace Modules\Iranstrick\Services\Tools\Template;

use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;

/**
 * Class IranstrickTemplateHandler
 *
 * @package Modules\Iranstrick\Services\Tools\Template
 */
class IranstrickTemplateHandler extends TemplateHandler
{
    protected static $site_title;
    protected static $page_title     = [];
    protected static $main_menu      = [];
    protected static $footer_menu    = [];
    protected static $languages_urls = [];
    protected static $site_logo_url;
    protected static $contact_info   = [];
    protected static $yasna_group_title;
    protected static $yasna_group_url;
    protected static $open_graphs    = [];



    /**
     * @retrun string
     */
    public static function siteLogoUrl()
    {
        if (is_null(static::$site_logo_url)) {
            if (
                 ($settingLogo = get_setting('site_logo')) and
                 ($logoFile = fileManager()->file($settingLogo)->getUrl())
            ) {
                static::$site_logo_url = $logoFile;
            } else {
                static::$site_logo_url = CurrentModuleHelper::asset('images/logo-' . getLocale() . '.png');
            }
        }

        return static::$site_logo_url;
    }
}
