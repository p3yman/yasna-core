<?php

namespace Modules\Iranstrick\Providers;

use Modules\Iranstrick\Http\Middleware\IranstrickLocaleMiddleware;
use Modules\Iranstrick\Services\Tools\Module\CurrentModuleHelper;
use Modules\Iranstrick\Services\Tools\Module\ModuleHandler;
use Modules\Iranstrick\Services\Tools\Template\IranstrickTemplateHandler;
use Modules\Yasna\Services\YasnaProvider;

class IranstrickServiceProvider extends YasnaProvider
{

    /**
     * Run boot options
     */
    public function index()
    {
        $this->registerProviders();
        $this->registerAliases();
        $this->registerModelTraits();
        $this->addMiddlewares();
    }



    /**
     * @return ModuleHandler
     */
    protected function module()
    {
        return ModuleHandler::create(get_class($this));
    }



    /**
     * @return string
     */
    protected function moduleName()
    {
        return $this->module()->getName();
    }



    /**
     * Register Providers
     */
    protected function registerProviders()
    {
        $providers = [
             CommentingServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->addProvider($provider);
        }
    }



    /**
     * Register Aliases
     */
    protected function registerAliases()
    {
        $aliases = [
             'Template'        => IranstrickTemplateHandler::class,
             'CurrentModule'   => CurrentModuleHelper::class,
             'CommentingTools' => CommentingServiceProvider::class,
        ];

        foreach ($aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }
    }



    /**
     * Register traits to be joined to models.
     */
    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleName() . ":IranstrickPostTrait")->to("Post")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleName() . ":IranstrickCategoryTrait")->to("Category")
        ;
    }



    /**
     * Add Middleware
     */
    protected function addMiddlewares()
    {
        $this->addMiddleware('iranstrick-locale', IranstrickLocaleMiddleware::class);
    }



    /**
     * @param string $str
     *
     * @return mixed This method returns a string or an array with the replaced values.
     */
    public static function urlHyphen($str)
    {
        return str_replace('+', '-', urlencode($str));
    }
}
