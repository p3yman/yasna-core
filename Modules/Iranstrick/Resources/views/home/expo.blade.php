@if ($exhibitions->count())
	@php
		$exhibitionPosttype = $exhibitions->first()->posttype;
		$listController = studly_case($exhibitionPosttype->slug) . 'Controller'
	@endphp
	@php $correctExhibitions = [] @endphp
	@foreach($exhibitions as $expo)
		@if (!($imageSrc = fileManager()->file($expo->featured_image)->getUrl()))
			@continue
		@endif
		@php
			$correctExhibitions[] = [
				'img' => $imageSrc,
				'title' => $expo->title,
				'link' => $expo->direct_url ?: '#',
			];
		@endphp
	@endforeach

	@if(count($correctExhibitions))
		<div class="widget">
			<div class="title-bar">
				<h3>{{ $exhibitionPosttype->titleIn(getLocale()) }}</h3>
				<a href="{{ CurrentModule::actionLocale("$listController@index") }}"
				   class="show-more">{{ CurrentModule::trans('front.see_all') }}</a>
			</div>
			<div class="content">
				<div class="owl-carousel expo-slider">
					@foreach($correctExhibitions as $expo)
						<div class="item">
							<a href="{{ $expo['link'] }}">
								<div class="image">
									<img src="{{ $expo['img'] }}" width="295" height="197">
								</div>
								<div class="text">
									<h5>{{ $expo['title'] }}</h5>
									{{--<p>Ad aliqua laboris occaecat commodo reprehenderit.</p>--}}
								</div>
							</a>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif
@endif

