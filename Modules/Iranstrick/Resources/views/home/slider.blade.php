@if ($slideshow->count())
	@php $correctSlides = [] @endphp
	@foreach($slideshow as $slide)
		@if (!($imageSrc = fileManager()->file($slide->featured_image)->getUrl()))
			@continue
		@endif
		@php
			$correctSlides[] = [
				'img' => $imageSrc,
				'title' => $slide->title2,
				'description' => $slide->abstract,
			];
		@endphp
	@endforeach

	@if (count($correctSlides))
		<div class="container-fluid">
			<div class="row">
				<div class="owl-carousel main-header-slider skew-slides">
					@foreach($correctSlides as $slide)
						<div class="item">
							<img src="{{ $slide['img'] }}">
							<div class="slide-text center">
								@if(strlen($slide['title']))
									<h3 class="bordered slide-title">{{ $slide['title'] }}</h3>
								@endif
								@if(strlen($slide['description']))
									<h4 class="slide-subtitle">{{ $slide['description'] }}</h4>
								@endif
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif
@endif