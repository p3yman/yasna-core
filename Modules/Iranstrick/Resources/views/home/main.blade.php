@extends(CurrentModule::bladePath('layouts.frame.main'))

@section('content')
	@include(CurrentModule::bladePath('home.slider'))
	<div class="container">
		<div class="row mobile-reverse">
			<div class="col-md-4">
				@include(CurrentModule::bladePath('home.expo'))
				{{--<div class="widget">--}}
					{{--<div class="title-bar">--}}
						{{--<h3>Downloads</h3>--}}
					{{--</div>--}}
					{{--<div class="content">--}}
						{{--<ul>--}}
							{{--<li><a href="">A beauty title comes here</a></li>--}}
							{{--<li><a href="">Another title comes here soon</a></li>--}}
							{{--<li><a href="">One more title</a></li>--}}
						{{--</ul>--}}
					{{--</div>--}}
				{{--</div>--}}
			</div>
			@include(CurrentModule::bladePath('home.highlights'))
		</div>
{{--		@include(CurrentModule::bladePath('home.links'))--}}
	</div>
@endsection