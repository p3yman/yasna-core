@if($news->count())
	<div class="col-md-8">
		<div class="title-bar">
			<h3>{{ CurrentModule::trans('front.highlights') }}</h3>
		</div>
		<div class="content">
			<div class="row">
				<div class="col-sm-5 col-xs-12">
					<div class="owl-carousel highlights-slider slider-theme-2 owl-hide-dots">
						@foreach($news as $new)
							<div class="item">
								<a href="{{ $new->direct_url }}">
									<div class="image">
										<img src="{{ fileManager()->file($new->featured_image)->getUrl() }}" width="295"
											 height="197">
									</div>
									<div class="text">
										<h5>{{ $new->title }}</h5>
									</div>
								</a>
							</div>
						@endforeach
					</div>
				</div>
				<div class="col-sm-7 col-xs-12">
					<div class="owl-carousel highlights-slider-text owl-hide-controls">
						@foreach($news as $new)
							<div class="item">
								{{--<h5 class="highilight-subtitle">Subtitle for this item</h5>--}}
								<p class="highlight-content">{{ $new->abstract }}</p>
								<a href="{{ $new->direct_url }}"
								   class="highlight-readmore">{{ CurrentModule::trans('front.more') }}</a>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
@endif