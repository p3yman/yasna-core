@if ($links->count())
	@php $correctLinks = [] @endphp
	@foreach($links as $category)
		@if (
			!($imageSrc = fileManager()->file($category->imageIn(getLocale()))->getUrl()) or
			!($category->titleIn(getLocale()))
		)
			@continue
		@endif
		@php
			$correctLinks[] = [
				'img' => $imageSrc,
				'title' => $category->titleIn(getLocale()),
				'link' => $category->direct_url
			];
		@endphp
	@endforeach

	@if (count($correctLinks))
		<div class="row">
			<div class="col-xs-12">
				<div class="links">
					<div class="title-bar text-center">
						<h3>{{ CurrentModule::trans('front.agency') }}</h3>
					</div>
					<div class="content">
						<div class="owl-carousel links-slider small-nav solid-nav offset-nav">
							@foreach($correctLinks as $link)
								<div class="item">
									<a href="{{ $link['link'] }}">
										<img src="{{ $link['img'] }}" title="{{ $link['title'] }}" height="120"
											 style="width:auto">
									</a>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
@endif
