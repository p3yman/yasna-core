<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<article class="article">
				<div class="title-bar">
					<h3 class="title">{{ $title }}</h3>
					<div class="show-more">
						@if($guidance_video = fileManager()->file($post->spreadMeta()->guidance_video)->getDownloadUrl())
							<a href="{{ $guidance_video }}" download="">
								<i class="fa fa-video-camera"></i>
								{{ CurrentModule::trans('validation.attributes.operation_video') }}
							</a>
						@endif
						@if($guidance_file = fileManager()->file($post->spreadMeta()->guidance_file)->getDownloadUrl())
							<a href="{{ $guidance_file }}" download="">
								<i class="fa fa-file-pdf-o"></i>
								{{ CurrentModule::trans('validation.attributes.operation_manual') }}
							</a>
						@endif
					</div>
				</div>

				<div class="content">
					@if ($imageUrl = ($image = fileManager()->file($post->featured_image)->resolve())->getUrl())
						<img src="{{ $imageUrl }}" alt="{{ $image->getTitle() }}" class="pull-right">
					@endif
					{!! $post->text !!}
				</div>
			</article>
		</div>

	</div>
</div>