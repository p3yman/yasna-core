@extends(CurrentModule::bladePath('layouts.frame.main'))

@php Template::appendToPageTitle($title = $post->title) @endphp

@section('content')
	@include(CurrentModule::bladePath('layouts.breadcrumb'), [
		'title' => $title,
		'items' => [
			[
				'title' => CurrentModule::trans('front.home_page'),
				'link' => CurrentModule::actionLocale('IranstrickController@index')
			],
			[
				'title' => $title
			]
		]
	])

	@include(CurrentModule::bladePath('product.single.content'))

@append