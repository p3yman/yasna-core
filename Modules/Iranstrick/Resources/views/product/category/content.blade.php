<div class="container">
	<div class="title-bar">
		<h3>{{ trans('iranstrick::front.about') }} {{ $title }}</h3>
	</div>
	@if ($description = $category->textIn(getLocale()))
		<div class="about-brand">
			{!! $description !!}
		</div>
	@endif

	@include(CurrentModule::bladePath('product.category.products'))
</div>