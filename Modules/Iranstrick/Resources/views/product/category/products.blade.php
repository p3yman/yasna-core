@if ($posts->count())
	<div class="title-bar">
		<h3>{{ trans('iranstrick::front.products') }}</h3>
	</div>

	<div class="thumbs-grid row">
		@foreach($posts as $product)
			@if(($loop->index % 4) == 0)
				<div class="row">
					@endif
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
						<div class="item">
							<a href="{{ $product->direct_url or '#' }}">
								<img src="{{ ($image = fileManager()->file($product->featured_image)->resolve())->getUrl() }}"
									 alt="{{ $image->getTitle() }}" class="media-object">
								<span class="media-title">{{ $product->title }}</span>
							</a>
							<p>{{ $product->abstract }}</p>
						</div>
					</div>
					@if((($loop->index % 4) == 3) or ($loop->iteration == $posts->count()))
				</div>
			@endif
		@endforeach
	</div>
@endif