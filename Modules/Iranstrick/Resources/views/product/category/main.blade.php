@extends(CurrentModule::bladePath('layouts.frame.main'))

@php Template::appendToPageTitle($title = $category->titleIn(getLocale())) @endphp

@section('content')
	@include(CurrentModule::bladePath('layouts.breadcrumb'), [
		'title' => $title,
		'items' => [
			[
				'title' => CurrentModule::trans('front.home_page'),
				'link' => CurrentModule::actionLocale('IranstrickController@index')
			],
			[
				'title' => $title
			]
		],
		'image' => ($image = fileManager()->file($category->imageIn(getLocale()))->resolve())->getUrl(),
		'image_alt' => $image->getTitle(),
	])

	@include(CurrentModule::bladePath('product.category.content'))
@append