<ul class="nav navbar-nav navbar-right">
	@each(CurrentModule::bladePath('layouts.frame.top_menu_item'), Template::mainMenu(), 'menu')

	<li class="languages">
		@foreach (Template::languagesUrls() as $language => $languageInfo)
			<a @if(($url = $languageInfo['url']) != '#') href="{{ $url }}" @endif @if($language == getLocale()) class="active" @endif>
				{!! $languageInfo['title'] !!}
			</a>
		@endforeach
	</li>
</ul>