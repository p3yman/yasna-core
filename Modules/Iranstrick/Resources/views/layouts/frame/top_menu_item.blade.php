@if ($menu instanceof \App\Models\Category)
	@php $title = $menu->titleIn(getLocale()) @endphp
	@php $children = $menu->children; @endphp
	@php $url = $menu->direct_url ?: '#' @endphp
@elseif($menu instanceof \App\Models\Post)
	@php $title = $menu->title @endphp
	@php $children = collect([]) @endphp
	@php $url = $menu->direct_url ?: '#' @endphp
@else
	@php $title = $menu->titleIn(getLocale()) @endphp
	@if (($slug = ($menu->spreadMeta()->label[getLocale()] ?? null)) == 'products')
		@php $children = model('posttype', $slug)->folders; @endphp
	@elseif($slug == 'services')
		@php
			$children = model('post')
				->elector([
					'type' => 'services',
					'locale' => getLocale(),
				])->orderByDesc('published_at')
					->get()
		@endphp
	@else
		@php $children = $menu->subMenus; @endphp
	@endif
	@php $url = url($menu->linkIn(getLocale())) @endphp
@endif
@if ($title)

	@php
		$has_children = boolval($children->count());
	@endphp


	<li>
		<a href="{{ $url }}"
		   @if($has_children) class="dropdown-toggle" data-toggle="dropdown" role="button" @endif >

			{{ $title }}

			@if($has_children)
				<span class="caret"></span>
			@endif
		</a>

		@if($has_children)
			<ul class="dropdown-menu">
				@each(CurrentModule::bladePath('layouts.frame.top_menu_item'), $children, 'menu')
			</ul>
		@endif
	</li>
@endif
