<nav class="navbar navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed btn btn-default" data-toggle="collapse"
					data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				{{--<span>Menu  </span>--}}
				<i class="fa fa-bars fa-lg"></i>
			</button>
			<a class="navbar-brand" href="{{ url('/' . getLocale()) }}">
				<img src="{{ Template::siteLogoUrl() }}" alt="IRANSTRICK" width="230">
			</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			@include(CurrentModule::bladePath('layouts.frame.top_menu'))
		</div>
	</div>
</nav>