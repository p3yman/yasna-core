<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="site-title">
					{{ Template::siteTitle() }}
				</div>
				<div class="contact-data">
					@if (array_key_exists('address', ($contact_info = Template::contactInfo())))
						<div class="address">
							<i class="fa fa-map-marker"></i>
							{{ $contact_info['address'] }}
						</div>
					@endif
					@if (array_key_exists('telephone', $contact_info))
						<div class="phone">
							<i class="fa fa-phone"></i>
							@foreach($contact_info['telephone'] as $phone)
								@if ($loop->index)
									<br>
								@endif
								{{ $phone }}
							@endforeach
						</div>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<ul class="footer-nav list-inline text-end">
					@foreach(Template::footerMenu() as $menu)
						<li>
							<a href="{{ $menu->linkIn(getLocale()) }}">
								{{ $menu->titleIn(getLocale()) }}
							</a>
						</li>
					@endforeach

					<li class="languages">
						@foreach (Template::languagesUrls() as $language => $language_info)
							@if ($loop->index)
								<span>/</span>
							@endif
							<a @if(($url = $language_info['url']) != '#') href="{{ $url }}" @endif
							@if($language == getLocale()) class="active" @endif>
								{!! $language_info['title'] !!}
							</a>
						@endforeach
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="credits">
				<p>
					<a>
						{{
							CurrentModule::trans('front.copyright', ['site' => Template::siteTitle()])
						}}
					</a>
					<br>
					<a href="{{ Template::yasnaGroupUrl() }}" target="_blank">
						{{
							CurrentModule::trans('front.prepared-by', ['name' => Template::yasnaGroupTitle()])
						}}
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>

{!! Html::script(CurrentModule::asset('js/main.js')) !!}

@include(CurrentModule::bladePath('layouts.frame.scripts'))

@yield('end_of_body')

</body>
</html>
