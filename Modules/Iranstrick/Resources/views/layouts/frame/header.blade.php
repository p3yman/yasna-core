<!DOCTYPE html>
@if(getLocale() == 'fa')
	<html dir="rtl" lang="fa" class="fa">
	@else
		<html lang="en">
		@endif
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width,initial-scale=1">
			<title>@yield('page_title', Template::implodePageTitle(' - '))</title>
			{!! Html::style(CurrentModule::asset('css/bootstrap.css')) !!}
			{!! Html::style(CurrentModule::asset('css/fonts.css')) !!}
			{!! Html::style(CurrentModule::asset('css/style.css')) !!}
			@if(getLocale() == 'fa')
				{!! Html::style(CurrentModule::asset('css/rtl.min.css')) !!}
				{!! Html::style(CurrentModule::asset('css/style-upgrade.min.css')) !!}
			@else
				{!! Html::style(CurrentModule::asset('css/style-upgrade-ltr.min.css')) !!}
			@endif
			<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

			{!! Html::script(CurrentModule::asset('js/jquery-2.1.1.min.js')) !!}
			{!! Html::script(CurrentModule::asset('js/bootstrap.min.js')) !!}
			{!! Html::script(CurrentModule::asset('js/owl.carousel.min.js')) !!}
			{!! Html::script(CurrentModule::asset('js/jquery.form.min.js')) !!}
			{!! Html::script(CurrentModule::asset('js/forms.js')) !!}

			<script src="https://use.fontawesome.com/42e9d0c0f0.js"></script>
			<script language="javascript">
                function base_url($ext) {
                    if (!$ext)
                        $ext = "";
                    var $result = '{{ URL::to(' / ') }}' + $ext;
                    return $result;
                }
			</script>

			@include(CurrentModule::bladePath('layouts.frame.open-graphs'))

		</head>

		<body>

	@include(CurrentModule::bladePath('layouts.frame.header_top_menu'))