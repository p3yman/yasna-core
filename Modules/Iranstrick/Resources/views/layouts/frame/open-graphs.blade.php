@foreach(Template::openGraphs() as $tagTitle =>  $tagContent)
	@if (is_closure($tagContent))
		@php $tagContent = $tagContent() @endphp
	@endif
	@if ($tagContent)
		<meta property="og:{{ $tagTitle }}" content="{{ $tagContent }}"/>
	@endif
@endforeach