<div class="sub-header">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h2 class="page-title">{{ $title or '' }}</h2>
				<ol class="breadcrumb">
					@foreach($items as $item)
						<li>
							@if($has_link = boolval($link = ($item['link'] ?? false)))
								<a href="{{ $link }}">
									@endif
									{{ $item['title'] or '' }}
									@if ($has_link)
								</a>
							@endif
						</li>
					@endforeach
				</ol>
				@if(isset($image) and $image)
					<img src="{{ $image }}" alt="{{ $image_alt or '' }}">
				@endif
			</div>
		</div>
	</div>
</div>