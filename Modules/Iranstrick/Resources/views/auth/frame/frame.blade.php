@include(CurrentModule::bladePath('auth.frame.header'))

@yield('content')

@include(CurrentModule::bladePath('auth.frame.footer'))