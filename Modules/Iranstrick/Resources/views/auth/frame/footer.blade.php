<!-- Load Scripts -->

{!! Html::script (CurrentModule::asset('libs/jquery.min.js')) !!}
{!! Html::script (CurrentModule::asset('libs/jquery.form.min.js')) !!}
{!! Html::script (CurrentModule::asset('js/app.js')) !!}
{!! Html::script (CurrentModule::asset('js/forms.js')) !!}
{!! Html::script (CurrentModule::asset('js/responsiveslides.js')) !!}

</body>

</html>