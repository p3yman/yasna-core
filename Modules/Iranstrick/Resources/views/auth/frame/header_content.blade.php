<!-- START: Header -->
<header id="main-header" class="auth">
	<div class="container">
		<a href="{{ url('') }}" id="logo">
			<img src="{{ CurrentModule::asset('images/logo-' . getLocale() . '.png') }}" width="234">
		</a>
	</div>
</header>
<!-- END: Header -->