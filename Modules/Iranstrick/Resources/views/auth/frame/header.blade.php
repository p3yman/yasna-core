<!DOCTYPE html>
<html lang="fa">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
	{!! Html::style(CurrentModule::asset('css/front-style.css')) !!}
	@if(getLocale() == 'en')
		{!! Html::style(CurrentModule::asset('css/front-style-en.css')) !!}
	@endif
	<script language="javascript">
        function base_url($ext) {
            if (!$ext) $ext = "";
            var $result = '{{ URL::to('/') }}' + $ext;
            return $result;
        }
	</script>
	<title>@yield('page_title', Template::implodePageTitle(' - '))</title>

@if(getLocale() == 'en')
	<body class="ltr">

	@if (false)
	</body>
@endif

@else
	<body>

	@if (false)
	</body>
@endif

@endif

@include(CurrentModule::bladePath('auth.frame.header_content'))