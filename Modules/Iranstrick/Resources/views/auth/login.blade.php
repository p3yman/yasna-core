@extends(CurrentModule::bladePath('auth.frame.frame'))

@php Template::appendToPageTitle(CurrentModule::trans('people.commands.login')) @endphp


@section('content')
	<div class="page-bg">
		<div class="container">
			<div class="col-sm-4 col-center">
				<section class="panel auth-content">
					<article>
						<h1 class="auth-title">{{ CurrentModule::trans('people.commands.login') }}</h1>
						{!! Form::open(['route' => 'login']) !!}

						<div class="field icon right">
							<input type="text" name="email"
								   placeholder="{{ CurrentModule::trans('validation.attributes.email') }}">
							<div class="icon-mail"></div>
						</div>
						<div class="field icon right">
							<input type="password" name="password"
								   placeholder="{{ CurrentModule::trans('validation.attributes.password') }}">
							<div class="icon-lock"></div>
						</div>

						@if (!env('APP_DEBUG'))
							{!! app('captcha')->render(getLocale()); !!}
						@endif

						@if($errors->count())
							<div class="alert alert-danger" style="margin-top: 10px;">
								{!! implode('<br>', array_merge(...array_values($errors->toArray()))) !!}
							</div>
						@endif

						<div class="field mt25">
							<button class="green block">{{ CurrentModule::trans('people.commands.login_into_site') }}</button>
						</div>

						{{--<div class="row">--}}
							{{--<div class="col-sm-12" style="text-align: center;">--}}
								{{--<a href="#"--}}
								   {{--class="simple-link">{{ CurrentModule::trans('people.commands.forget_password') }}</a>--}}
							{{--</div>--}}
						{{--</div>--}}

						{!! Form::close() !!}
					</article>
				</section>
			</div>
		</div>
	</div>
@append