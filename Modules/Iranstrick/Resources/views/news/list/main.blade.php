@extends(CurrentModule::bladePath('layouts.frame.main'))

@php Template::appendToPageTitle($title = $posttype->titleIn(getLocale())) @endphp


@section('content')
	@include(CurrentModule::bladePath('layouts.breadcrumb'), [
		'title' => $posttype->titleIn(getLocale()),
		'items' => [
			[
				'title' => CurrentModule::trans('front.home_page'),
				'link' => CurrentModule::actionLocale('IranstrickController@index')
			],
			[
				'title' => $title
			]
		]
	])

	@include(CurrentModule::bladePath('news.list.content'))
@append