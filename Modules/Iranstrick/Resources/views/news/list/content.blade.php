<div class="container">
	<div class="row mobile-reverse">
		<div class="col-md-4">
			@include(CurrentModule::bladePath('home.expo'))
		</div>

		<div class="col-md-8">
			@foreach($posts as $post)
				<article class="article">
					<div class="title-bar">
						<a href="{{ ($url = ($post->direct_url ?: '#')) }}">
							<h4 class="title">{{ $post->title }}</h4>
						</a>
						<span class="show-more">{{ ad(echoDate($post->published_at)) }}</span>
					</div>
					<div class="content">
						<a href="{{ $url }}">
							<img src="{{ ($image = fileManager()->file($post->featured_image)->resolve())->getUrl() }}"
								 alt="{{ $image->getTitle() }}" class="block">
						</a>
						{{ $post->abstract }}
					</div>
				</article>
				<div style="clear: both;"></div>
			@endforeach
		</div>
	</div>
</div>
