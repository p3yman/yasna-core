@if(!isset($condition) or $condition)
	@include(CurrentModule::bladePath('forms.group-start'))

	@include(CurrentModule::bladePath('forms.check') , [
		'label' => $self_label
	])

	@include(CurrentModule::bladePath('forms.group-end'))

@endif