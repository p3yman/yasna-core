@section('additional_content')
	@if ($post->can_register)
		@php $register_status = $post->register_status @endphp
		<div class="row" style="margin-bottom: 40px">
			@if (session()->has('success_message'))
				<div class="col-xs-12" id="success-message">
					<div class="alert alert-success">
						{!! session()->get('success_message') !!}
						@section('end_of_body')
							<script>
                                $(document).ready(function () {
                                    $('#success-message').scrollToView(-($('.navbar-fixed-top').outerHeight()));
                                });
							</script>
						@append
					</div>
				</div>
			@endif
			<div class="col-xs-12 text-center">

				<button class="btn btn-info" id="form-btn" data-toggle="collapse" data-target="#register-panel"
						onclick="$(this).hide()"
						@if($register_status != 'ACTIVE')
						disabled="disabled"
						@if($register_status == 'EXPIRED')
						title="{{ CurrentModule::trans('people.form.register_expired') }}"
						@endif
						@if($register_status == 'PENDING')
						title="{{ CurrentModule::trans('people.form.register_pending') }}"
						@endif
						@endif
				>{{ CurrentModule::trans('people.commands.register_for') . ' ' . $post->title }}</button>

				<div id="register-panel" class="collapse col-xs-12 m20">
					@include(CurrentModule::bladePath('forms.opener') , [
						'id' => 'register-form',
						'url' => CurrentModule::actionLocale('ExhibitionsController@register'),
						'class' => 'text-center'
					])

					@include(CurrentModule::bladePath('forms.hiddens') , ['fields' => [
						['post_id' , $post->hashid ],
					]])

					@include(CurrentModule::bladePath('forms.input') , [
						'name' => 'name',
						'class' => 'form-required form-default' ,
						'value' => old('name')
					])

					@include(CurrentModule::bladePath('forms.input') , [
						'name' => 'email',
						'class' => 'form-required ltr',
						'value' => old('email')
					])

					@include(CurrentModule::bladePath('forms.input') , [
						'name' => 'mobile',
						'class' => 'form-required ltr',
						'value' => old('mobile')
					])

					<div class="col-xs-12">
						@include(CurrentModule::bladePath('forms.button') , [
							'id' => 'btnRegister' ,
							'label' => trans('iranstrick::forms.button.signup'),
							'type' => 'submit',
							'class' => 'btn-success',
						])
					</div>

					<div class="col-xs-12">
						@if (!env('APP_DEBUG'))
							{!! app('captcha')->render(getLocale()); !!}
						@endif
					</div>

					<div class="col-xs-12" style="padding-top: 10px">
						<div class="row text-justify">
							@if ($errors->count())
								<div class="alert alert-danger">
									{!! implode('<br>', array_merge(...array_values($errors->toArray()))) !!}
								</div>
							@section('end_of_body')
								<script>
                                    $(document).ready(function () {
                                        $('#form-btn').trigger('click');
                                        $('#register-panel').scrollToView(-($('.navbar-fixed-top').outerHeight()));
                                    });
								</script>
							@append
							@endif
						</div>
					</div>


					@include(CurrentModule::bladePath('forms.closer'))
				</div>
			</div>
		</div>
	@endif
@append