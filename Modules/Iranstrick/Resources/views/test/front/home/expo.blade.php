@if(sizeof($expo))
    <div class="widget">
        <div class="title-bar">
            <h3>{{ trans('iranstrick::front.expo') }}</h3>
            <a href="{{ '#' }}" class="show-more">{{ trans('iranstrick::front.see_all') }}</a>
        </div>
        <div class="content">
            <div class="owl-carousel expo-slider">
                @foreach($expo as $ex)
                    <div class="item">
                        <a href="{{ $ex['link'] }}">
                            <div class="image">
                                <img src="{{ $ex['img'] }}" width="295" height="197">
                            </div>
                            <div class="text">
                                <h5>{{ $ex['title'] }}</h5>
                                {{--<p>Ad aliqua laboris occaecat commodo reprehenderit.</p>--}}
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif