@if(isset($agency) and sizeof($agency))
<div class="row">
    <div class="col-xs-12">
        <div class="links">
            <div class="title-bar text-center">
                <h3>{{ trans('iranstrick::front.agency') }}</h3>
            </div>
            <div class="content">
                <div class="owl-carousel links-slider small-nav solid-nav offset-nav">
                    @foreach($agency as $agen)
                        <div class="item">
                            <a href="{{ $agen['link'] }}">
                                <img src="{{ $agen['img'] }}" title="{{ $agen['title'] }}" height="120" style="width:auto">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endif