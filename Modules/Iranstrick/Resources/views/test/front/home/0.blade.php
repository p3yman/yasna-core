@extends('iranstrick::test.front.frame.frame')

@php
    $slider = [
        [
            "img" => Module::asset('iranstrick:images/demo/slider/slide1.jpg') ,
            "title" => "اسلاید اول" ,
            "title_two" => "توضیحات اضافه" ,
        ],
        [
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "title" => "اسلاید اول" ,
            "title_two" => "توضیحات اضافه" ,
        ],
        [
            "img" => Module::asset('iranstrick:images/demo/slider/slide3.jpg') ,
            "title" => "اسلاید اول" ,
            "title_two" => "توضیحات اضافه" ,
        ],
    ];

    $expo = [
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide1.jpg') ,
            "title" => "نمایشگاه بزرگ" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "title" => "نمایشگاه کوچک" ,
        ],
    ];

    $news = [
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
    ];

    $agency = [
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency1.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency2.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency1.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency2.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency1.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency2.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency1.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency2.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency1.png') ,
            "title" => "عنوان" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/agency/agency2.png') ,
            "title" => "عنوان" ,
        ],

    ];
@endphp

@section('page_title')
    {{--{{ Setting::get(Setting::getLocale() . '_site_title') }}--}}
@endsection

@section('content')
    @include('iranstrick::test.front.home.slider')
    <div class="container">
        <div class="row mobile-reverse">
            <div class="col-md-4">
                @include('iranstrick::test.front.home.expo')
                {{--<div class="widget">--}}
                    {{--<div class="title-bar">--}}
                        {{--<h3>Downloads</h3>--}}
                    {{--</div>--}}
                    {{--<div class="content">--}}
                        {{--<ul>--}}
                            {{--<li><a href="">A beauty title comes here</a></li>--}}
                            {{--<li><a href="">Another title comes here soon</a></li>--}}
                            {{--<li><a href="">One more title</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            @include('iranstrick::test.front.home.highlights')
        </div>
        @include('iranstrick::test.front.home.links')
    </div>
@endsection