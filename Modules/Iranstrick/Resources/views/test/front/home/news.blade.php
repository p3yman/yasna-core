@if(sizeof($news))
<div class="widget">
    <div class="title-bar">
        <h3>{{ trans('iranstrick::front.news') }}</h3>
        <a href="{{ url('/' . getLocale() . '/news') }}" class="show-more">{{ trans('iranstrick::front.see_all') }}</a>
    </div>
    <div class="content">
        <ul>
            @foreach($news as $new)
                <li><a href="{{ $new['link'] }}">{{ $new['title'] }}</a></li>
            @endforeach
        </ul>
    </div>
</div>
@endif