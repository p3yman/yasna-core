@extends('iranstrick::test.front.frame.frame')

@php
    $news = [
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
    ];

    $expo = [
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide1.jpg') ,
            "title" => "نمایشگاه بزرگ" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "title" => "نمایشگاه کوچک" ,
        ],
    ];
@endphp

@section('page_title')
    {{--{{ Setting::get(Setting::getLocale() . '_site_title') }} - {{ trans('iranstrick::front.news') }}--}}
@endsection

@section('content')
    <div class="sub-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-title">{{ trans('iranstrick::front.news') }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">{{ trans('iranstrick::front.home_page') }}</a>
                        </li>
                        <li class="active">{{ trans('iranstrick::front.news') }}</li>
                    </ol>
                    <img src="" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mobile-reverse">
            <div class="col-md-4">
                @include('iranstrick::test.front.home.expo')
            </div>
            <div class="col-md-8">
                @foreach($news as $new)
                <article class="article">
                    <div class="title-bar">
                        <a href="{{ $new['link'] }}"> <h4 class="title">{{ $new['title'] }}</h4></a>
                        <span class="show-more">{{ $new['published_at'] }}</span>
                    </div>
                    <div class="content">
                        <a href="{{ $new['link'] }}"><img src="{{ $new['img']}}" alt="{{ $new['title'] }}" class="block"></a>
                        {{ $new['abstract'] }}
                    </div>
                </article>
                    <div style="clear: both;"></div>
                @endforeach
                {{--<div class="row" style="text-align: center; margin: 0 auto;">--}}
                    {{--{!! $news->render() !!}--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection