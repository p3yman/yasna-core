@extends('iranstrick::test.front.frame.frame')

@php
    $page_title = "معرفی";
    $site_title = "ایران‌اشتریک";

    $about_img = Module::asset('iranstrick:images/demo/book2-final.jpg');
    $contact_content = '
           <p></p><p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Vazir,Roboto,Arial,sans-serif; font-size: 14px; text-align: justify;"><span style="font-size: 10pt;">تهران، خیابان بخارست، خیابان نهم، پلاک 6، طبقه 2، واحد 4 و 6</span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Vazir,Roboto,Arial,sans-serif; font-size: 14px; text-align: justify;"><span style="font-size: 10pt;">تلفن: 88550468</span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Vazir,Roboto,Arial,sans-serif; font-size: 14px; text-align: justify;"><span style="font-size: 10pt;">&nbsp;فکس:&nbsp; 88712661 </span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Vazir,Roboto,Arial,sans-serif; font-size: 14px; text-align: justify;"><span style="font-size: 10pt;">کدپستی: 1513735717<br></span></p>
<p style="box-sizing: border-box; margin: 0px 0px 10px; color: #333333; font-family: Vazir,Roboto,Arial,sans-serif; font-size: 14px; text-align: justify;"><span style="font-size: 10pt;">Email: info@iranstrick.com</span></p><p></p>
          ';
@endphp

@section('page_title')
    {{--{{ Setting::get(Setting::getLocale() . '_site_title') }} - {{ $page->title }}--}}
    {{ $site_title }}
@endsection

@section('content')
    <div class="sub-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-title">{{ $page_title }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">{{ trans('iranstrick::front.home_page') }}</a>
                        </li>
                        <li class="active">{{ $page_title }}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    {{--<script src="js/owl.carousel.min.js"></script>--}}
    <div class="container pt20 pb20">
        <div class="row">
            <div class="col-xs-12">
                <article class="article">
                    <div class="title-bar">
                        <h3 class="title">{{ $page_title }}</h3>
                    </div>
                    <div class="content">
                        <div class="row">
                            <div class="col-md-7">
                                {{--<p>{!! $page->text !!}</p>--}}
                                {!! $contact_content !!}
                                {{--<div id="map"></div>--}}
                            </div>
                            <div class="col-md-5 pr10 pl10">
                                @include('iranstrick::test.forms.opener' , [
                                    'id' => 'register-form',
                                    'url' => getLocale() . '/contact/save',
                                    'class' => 'js text-center'
                                ])
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control required" id="name_first" name="name_first"
                                               placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control required" id="email" name="email"
                                               placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="mobile" name="mobile"
                                               placeholder="Mobile Number">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Subject">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control required" id="text" placeholder="Message"
                                                  name="text"
                                                  rows="6">
                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        {{--{!! app('captcha')->display($attributes = [], $lang = getLocale(), $theme = 'blackglass') !!}--}}
                                    </div>
                                    <div class="row">
                                        <button type="submit" id="submit" name="submit"
                                                class="btn btn-primary pull-{{(getLocale() == 'fa') ? 'right' : 'left'}}">
                                            Send your message
                                        </button>
                                    </div>
                                </div>


                                <div class="col-xs-12" style="padding-top: 10px">
                                    <div class="row">
                                        <div class="row text-justify">
                                            @include('iranstrick::test.forms.feed')
                                        </div>
                                    </div>
                                </div>

                                @include('iranstrick::test.forms.closer')
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
@endsection