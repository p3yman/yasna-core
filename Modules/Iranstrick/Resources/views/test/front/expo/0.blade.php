@extends('iranstrick::test.front.frame.frame')

@php
    $news = [
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
        [
            "link" => "#" ,
            "title" => "خبر مهم" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "abstract" => "چکیده خبر‌ها چکیده خبر‌ها  چکیده خبر‌ها چکیده خبر‌ها" ,
        ],
    ];

    $expo = [
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide1.jpg') ,
            "title" => "نمایشگاه بزرگ" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "abstract" => "خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "title" => "نمایشگاه کوچک" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "abstract" => "خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide1.jpg') ,
            "title" => "نمایشگاه بزرگ" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "abstract" => "خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش" ,
        ],
        [
            "link" => "#" ,
            "img" => Module::asset('iranstrick:images/demo/slider/slide2.jpg') ,
            "title" => "نمایشگاه کوچک" ,
            "published_at" => "۲۸ مرداد ۱۳۹۶ [۱۴:۰۵]" ,
            "abstract" => "خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش. خلاصه توضیحات مربوط به این بخش" ,
        ],
    ];
@endphp

@section('page_title')
    {{--{{ Setting::get(Setting::getLocale() . '_site_title') }} - {{ trans('iranstrick::front.expo') }}--}}
@endsection

@section('content')
    <div class="sub-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-title">{{ trans('iranstrick::front.expo') }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">{{ trans('iranstrick::front.home_page') }}</a>
                        </li>
                        <li class="active">{{ trans('iranstrick::front.expo') }}</li>
                    </ol>
                    {{--<img src="" alt="">--}}
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mobile-reverse">
            <div class="col-md-4">
                @include('iranstrick::test.front.home.news')
            </div>
            <div class="col-md-8">
                @foreach($expo as $ex)
                <article class="article">
                    <div class="title-bar">
                        <a href="{{ $ex['link'] }}"><h4 class="title">{{ $ex['title'] }}</h4></a>
                        <h6 class="show-more">{{ $ex['published_at'] }}</h6>
                    </div>
                    <div class="content">
                        <a href="{{ $ex['link'] }}">
                            <img src="{{ $ex['img'] }}" alt="{{ $ex['title'] }}" class="block">
                        </a>
                        {{ $ex['abstract'] }}
                    </div>
                </article>
                    <div style="clear: both;"></div>
                @endforeach
                {{--<div class="row" style="text-align: center; margin: 0 auto;">--}}
                    {{--{!! $expo->render() !!}--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection