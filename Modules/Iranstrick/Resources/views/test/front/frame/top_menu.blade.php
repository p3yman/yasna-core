@php
    $products = [
        [
            "link" => "#" ,
            "title" => "منو" ,
            "children" => [
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
            ] ,
        ],
        [
            "link" => "#" ,
            "title" => "منو" ,
            "children" => [
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
            ] ,
        ],
        [
            "link" => "#" ,
            "title" => "منو" ,
            "children" => [
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
                [
                    "link" => "#" ,
                    "title" => "زیر منو" ,
                ],
            ] ,
        ],
    ];
    $services = [
        [
            "link" => "#" ,
            "title" => "منو" ,
        ],
        [
            "link" => "#" ,
            "title" => "منو" ,
        ],
        [
            "link" => "#" ,
            "title" => "منو" ,
        ],
    ];
@endphp

<ul class="nav navbar-nav navbar-right">
    <li><a href="{{ url('/fa/about') }}">{{ trans('iranstrick::front.about') }}</a></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">{{ trans('iranstrick::front.products') }} <span
                    class="caret"></span></a>
        <ul class="dropdown-menu">
            @if(sizeof($products))
                @foreach($products as $menu)
                    <li class="dropdown">
                        <a href="{{ url($menu['link']) }}"
                           class="dropdown-toggle" data-toggle="dropdown" role="button">{{ $menu['title'] }}
                            <span class="caret"></span></a>
                        @if(sizeof($menu['children']))
                            <ul class="dropdown-menu">
                                @foreach($menu['children'] as $child)
                                    <li>
                                        <a href="{{ $child['link'] }}">{{ $child['title'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            @endif
        </ul>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">{{ trans('iranstrick::front.our_services') }} <span
                    class="caret"></span></a>
        <ul class="dropdown-menu">
            @if(sizeof($services))
                @foreach($services as $menu)
                    <li>
                        <a href="{{ $menu['link'] }}">{{ $menu['title'] }}</a>
                    </li>
                @endforeach
            @endif
        </ul>
    </li>

    <li><a href="{{ url('/fa/news') }}">{{ trans('iranstrick::front.news') }}</a></li>
    <li><a href="{{ url('/fa/contact') }}">{{ trans('iranstrick::front.contact_us') }}</a></li>
    <li class="languages">
        <a href="{{ url('/en') }}">English</a>
        <a href="{{ url('/fa') }}" class="active">فارسی</a>
    </li>
</ul>