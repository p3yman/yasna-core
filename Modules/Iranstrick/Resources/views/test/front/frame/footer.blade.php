
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="site-title">
                    {{ "ایران‌اشتریک" }}
                </div>
                <div class="contact-data">
                    <div class="address">
                        <i class="fa fa-map-marker"></i>
                        {{ "تهران – خیابان احمد قصیر (بخارست) – خیابان نهم – شماره ۶ – واحد ۶ و ۴" }}
                    </div>
                    <div class="phone">
                        <i class="fa fa-phone"></i>
                        {{ "021-88550468" }}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <ul class="footer-nav list-inline text-end">
                    <li><a href="{{ url('/fa/about') }}">{{ trans('iranstrick::front.about') }}</a></li>
                    {{--<li><a href="">Products</a></li>--}}
                    {{--<li><a href="">Services</a></li>--}}
                    <li><a href="{{ url('/fa/news') }}">{{ trans('iranstrick::front.news') }}</a></li>
                    <li><a href="{{ url('/fa/contact') }}">{{ trans('iranstrick::front.contact_us') }}</a></li>
                    <li class="languages">
                        <a href="{{ url('/en') }}">English</a>
                        <span>/</span>
                        <a href="{{ url('/fa') }}" class="active">فارسی</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="credits">
                <p>
                    <a>حقوق این سایت برای
                        {{ "ایران اشتریک" }}
                        محفوظ است.
                    </a><br>
                    <a href="http://yasnateam.com" target="_blank">طراحی و اجرا گروه یسنا
                    </a>
                </p>
            </div>
        </div>
    </div>
</footer>

{!! Html::script(Module::asset('iranstrick:js/main.js')) !!}

@include('iranstrick::test.front.frame.scripts')

</body>
</html>
