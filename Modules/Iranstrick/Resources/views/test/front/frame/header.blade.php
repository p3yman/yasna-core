<!DOCTYPE html>
@if(getLocale() == 'fa')
    <html dir="rtl" lang="fa" class="fa">
    @else
        <html lang="en">
        @endif
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <title>@yield('page_title')</title>
            {!! Html::style(Module::asset('iranstrick:css/bootstrap.css')) !!}
            {!! Html::style(Module::asset('iranstrick:css/fonts.css')) !!}
            {!! Html::style(Module::asset('iranstrick:css/style.css')) !!}
            @if(getLocale() == 'fa')
                {!! Html::style(Module::asset('iranstrick:css/rtl.min.css')) !!}
                {!! Html::style(Module::asset('iranstrick:css/style-upgrade.min.css')) !!}
            @else
                {!! Html::style(Module::asset('iranstrick:css/style-upgrade-ltr.min.css')) !!}
            @endif
            <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

            {!! Html::script(Module::asset('iranstrick:js/jquery-2.1.1.min.js')) !!}
            {!! Html::script(Module::asset('iranstrick:js/bootstrap.min.js')) !!}
            {!! Html::script(Module::asset('iranstrick:js/owl.carousel.min.js')) !!}
            {!! Html::script(Module::asset('iranstrick:js/jquery.form.min.js')) !!}
            {!! Html::script(Module::asset('iranstrick:js/forms.js')) !!}

            <script src="https://use.fontawesome.com/42e9d0c0f0.js"></script>
            <script language="javascript">
                function base_url($ext) {
                    if (!$ext)
                        $ext = "";
                    var $result = '{{ URL::to(' / ') }}' + $ext;
                    return $result;
                }
            </script>
        </head>

        <body>

    @include('iranstrick::test.front.frame.header_top_menu')