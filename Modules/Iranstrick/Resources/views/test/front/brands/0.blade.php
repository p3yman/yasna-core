@extends('iranstrick::test.front.frame.frame')

@php
    $brand = [
        "title" => "محصول فلان" ,
        "img" => Module::asset('iranstrick:images/demo/product.jpg') ,
        "abstract" => '
            <p><span style="font-size: 10pt;">شرکت مایر اند سی آلمان از سال 1905 تاکنون، پیشرو در ارائه تکنولوژی های جدید در عرصه ساخت ماشین آلات گردبافی و ارائه خدمات در سراسر دنیا بوده است. همچنین امروزه با تنوع بالای ماشین آلات بافت و توسعه دامنه فعالیت های فنی - مهندسی توانسته است بالاترین میزان فروش در سال را نسبت به سایر رقبا، به خود اختصاص دهد.</span></p>
<p><span style="font-size: 10pt;"><strong>ماشین های دورو مناسب برای:</strong></span></p>
<ul>
<li><span style="font-size: 10pt;">تولید روکش صندلی</span></li>
<li><span style="font-size: 10pt;">تولید پوشاک بادی سایز</span></li>
<li><span style="font-size: 10pt;">تولید پارچه راه راه (رینگل)</span></li>
<li><span style="font-size: 10pt;">تولید پارچه های رو تشکی</span></li>
<li><span style="font-size: 10pt;">تولید پارچه ژاکارد</span></li>
<li><span style="font-size: 10pt;">تولید پارچه با گیج های ظریف تا گیج 44</span></li>
<li><span style="font-size: 10pt;">تولید پارچه ریب و اینترلاک</span></li>
</ul>
<p><strong><span style="font-size: 10pt;">ماشین های یکرو مناسب برای:</span></strong></p>
<ul>
<li><span style="font-size: 10pt;">تولید پوشاک بادی سایز</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های دورس تا 3 نخ</span></li>
<li><span style="font-size: 10pt;">بافت پارچه با گیج های فوق ظریف تا گیج 60 و تا دهنه 48</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های طرح دار (فول ژاکارد)</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های راه راه (رینگل)</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های حوله و مخمل ساده و ژاکارد</span></li>
<li><span style="font-size: 10pt;">توری بافی (Mesh)</span></li>
</ul>
<p><strong><span style="font-size: 10pt;">ماشین آلات مناسب برای بافت پارچه های بادی سایز:</span></strong></p>
<ul>
<li><span style="font-size: 10pt;">ماشین FV2.0 دو رو، با پلاتین و بدون پلاتین از دهنه 8 اینچ به بالا<br></span></li>
<li><span style="font-size: 10pt;">ماشین IV2.0 دو رو از دهنه 14 اینچ به بالا</span></li>
<li><span style="font-size: 10pt;">ماشین S4-3.2II یک رو از 15 اینچ به بالا</span></li>
</ul>
<p>&nbsp;</p>
        ' ,
    ];

    $products = [
        [
            "link" => "#" ,
            "title" => "محصول اول" ,
            "abstract" => "این یک خلاصه از کارایی محصول است." ,
            "img" => Module::asset('iranstrick:images/demo/product.jpg') ,
        ],
        [
            "link" => "#" ,
            "title" => "محصول دوم" ,
            "abstract" => "این یک خلاصه از کارایی محصول است." ,
            "img" => Module::asset('iranstrick:images/demo/product.jpg') ,
        ],
        [
            "link" => "#" ,
            "title" => "محصول سوم" ,
            "abstract" => "این یک خلاصه از کارایی محصول است." ,
            "img" => Module::asset('iranstrick:images/demo/product.jpg') ,
        ],
    ];
@endphp

@section('page_title')
    {{--{{ Setting::get(Setting::getLocale() . '_site_title') }} - {{ $brand->title }}--}}
@endsection

@section('content')
    <div class="sub-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-title">{{ $brand['title'] }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">{{ trans('iranstrick::front.home_page') }}</a>
                        </li>
                        <li class="active">{{ $brand['title'] }}</li>
                    </ol>
                    <img src="{{ $brand['img'] }}" alt="{{ $brand['title'] }}">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="title-bar">
            <h3>{{ trans('iranstrick::front.about') }} {{ $brand['title'] }}</h3>
        </div>
        <div class="about-brand">
            {!! $brand['abstract'] !!}
        </div>

        @if(sizeof($products))
        <div class="title-bar">
            <h3>{{ trans('iranstrick::front.products') }}</h3>
        </div>
        <div class="thumbs-grid row">
            <?php $row = 1; ?>
            @foreach($products as $product)
            @if($row == 4)
                <div class="row">
            @endif
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <div class="item">
                    <a href="{{ $product['link'] }}">
                        <img src="{{ $product['img'] }}" alt="{{ $product['title'] }}" class="media-object">
                        <span class="media-title">{{ $product['title'] }}</span>
                    </a>
                    <p>{{ $product['abstract']  }}</p>
                </div>
            </div>
            @if($row == 4)
                </div>
                @php
                    $row = 1;
                @endphp
            @else
                @php
                    $row++;
                @endphp
            @endif
            @endforeach
        </div>
        @endif
    </div>
@endsection
