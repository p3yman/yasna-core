@if($page->register_status != 'INACTIVE')
    {!! Html::style(Module::asset('iranstrick:css/tforms.min.css')) !!}
    <div class="row" style="margin-bottom: 40px">
        <div class="col-xs-12 text-center">
            <button class="btn btn-info" data-toggle="collapse" data-target="#register-panel"
                    onclick="$(this).hide()"
                    @if($page->register_status != 'ACTIVE')
                    disabled="disabled"
                    @if($page->register_status == 'EXPIRED')
                    title="{{ trans('iranstrick::people.form.register_expired') }}"
                    @endif
                    @if($page->register_status == 'PENDING')
                    title="{{ trans('iranstrick::people.form.register_pending') }}"
                    @endif
                    @endif
            >{{ trans('iranstrick::people.commands.register_for') . ' ' . $page->title }}</button>
        </div>
        <div id="register-panel" class="collapse col-xs-12 m20">
            @include('iranstrick::test.forms.opener' , [
                'id' => 'register-form',
                'url' => \App\Providers\SettingServiceProvider::getLocale() . '/pages/register',
                'class' => 'js text-center'
            ])

            @include('iranstrick::test.forms.hiddens' , ['fields' => [
                ['post_id' , $page->encrypted_id ],
            ]])

            @include('iranstrick::test.forms.input' , [
                'name' => 'name_first',
                'class' => 'form-required form-default' ,
            ])

            @include('iranstrick::test.forms.input' , [
                'name' => 'name_last',
                'class' => 'form-required form-required',
            ])

            @include('iranstrick::test.forms.input' , [
                'name' => 'email',
                'class' => 'form-required ltr',
            ])

            @include('iranstrick::test.forms.input' , [
                'name' => 'mobile',
                'class' => 'form-required ltr',
            ])

            @include("iranstrick::test.forms.input" , [
                'name' => "code_melli",
                'class' => "form-required",
            ])

            <div class="col-xs-12">
                @include('iranstrick::test.forms.button' , [
                    'id' => 'btnRegister' ,
                    'label' => trans('iranstrick::forms.button.signup'),
                    'type' => 'submit',
                    'class' => 'btn-success',
                ])
            </div>

            <div class="col-xs-12" style="padding-top: 10px">
                <div class="row text-justify">
                    @include('iranstrick::test.forms.feed')
                </div>
            </div>


            @include('iranstrick::test.forms.closer')
        </div>
    </div>
@endif
