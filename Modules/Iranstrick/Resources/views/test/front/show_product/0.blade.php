@extends('iranstrick::test.front.frame.frame')

@php
    $product = [
       "title" => "محصول" ,
            "operation_video" => "#" ,
            "operation_manual" => "#" ,
            "featured_image" => Module::asset('iranstrick:images/demo/slider/slide1.jpg') ,
            "text" => '
                  <p><span style="font-size: 10pt;">شرکت مایر اند سی آلمان از سال 1905 تاکنون، پیشرو در ارائه تکنولوژی های جدید در عرصه ساخت ماشین آلات گردبافی و ارائه خدمات در سراسر دنیا بوده است. همچنین امروزه با تنوع بالای ماشین آلات بافت و توسعه دامنه فعالیت های فنی - مهندسی توانسته است بالاترین میزان فروش در سال را نسبت به سایر رقبا، به خود اختصاص دهد.</span></p>
<p><span style="font-size: 10pt;"><strong>ماشین های دورو مناسب برای:</strong></span></p>
<ul>
<li><span style="font-size: 10pt;">تولید روکش صندلی</span></li>
<li><span style="font-size: 10pt;">تولید پوشاک بادی سایز</span></li>
<li><span style="font-size: 10pt;">تولید پارچه راه راه (رینگل)</span></li>
<li><span style="font-size: 10pt;">تولید پارچه های رو تشکی</span></li>
<li><span style="font-size: 10pt;">تولید پارچه ژاکارد</span></li>
<li><span style="font-size: 10pt;">تولید پارچه با گیج های ظریف تا گیج 44</span></li>
<li><span style="font-size: 10pt;">تولید پارچه ریب و اینترلاک</span></li>
</ul>
<p><strong><span style="font-size: 10pt;">ماشین های یکرو مناسب برای:</span></strong></p>
<ul>
<li><span style="font-size: 10pt;">تولید پوشاک بادی سایز</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های دورس تا 3 نخ</span></li>
<li><span style="font-size: 10pt;">بافت پارچه با گیج های فوق ظریف تا گیج 60 و تا دهنه 48</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های طرح دار (فول ژاکارد)</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های راه راه (رینگل)</span></li>
<li><span style="font-size: 10pt;">بافت پارچه های حوله و مخمل ساده و ژاکارد</span></li>
<li><span style="font-size: 10pt;">توری بافی (Mesh)</span></li>
</ul>
<p><strong><span style="font-size: 10pt;">ماشین آلات مناسب برای بافت پارچه های بادی سایز:</span></strong></p>
<ul>
<li><span style="font-size: 10pt;">ماشین FV2.0 دو رو، با پلاتین و بدون پلاتین از دهنه 8 اینچ به بالا<br></span></li>
<li><span style="font-size: 10pt;">ماشین IV2.0 دو رو از دهنه 14 اینچ به بالا</span></li>
<li><span style="font-size: 10pt;">ماشین S4-3.2II یک رو از 15 اینچ به بالا</span></li>
</ul>
<p>&nbsp;</p>
            ' ,
    ];

@endphp

@section('page_title')
    {{--{{ Setting::get(Setting::getLocale() . '_site_title') }} - {{ $product['title'] }}--}}
@endsection

@section('content')
    <div class="sub-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-title">{{ $product['title'] }}</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{ url('/') }}">{{ trans('iranstrick::front.home_page') }}</a>
                        </li>
                        <li class="active">{{ $product['title'] }}</li>
                    </ol>
                    {{--<img src="" alt="">--}}
                </div>
            </div>
        </div>
    </div>
    {!! Html::script(Module::asset('iranstrick:js/owl.carousel.min.js')) !!}


    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <article class="article">
                    <div class="title-bar">
                        <h3 class="title">{{ $product['title'] }}</h3>
                        <div class="show-more">
                            @if($product['operation_video'])
                                <a href="{{$product['operation_video']}}" download="">
                                    <i class="fa fa-video-camera"></i> {{trans('iranstrick::validation.attributes.operation_video')}}
                                </a>
                            @endif
                            @if($product['operation_manual'])
                                <a href="{{$product['operation_manual']}}" download="">
                                    <i class="fa fa-file-pdf-o"></i> {{trans('iranstrick::validation.attributes.operation_manual')}}
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="content">
                        {{--<div class="row">--}}
                        {{--<div class="col-sm-6">--}}
                        {{--<div class="owl-carousel main-header-slider">--}}
                        {{--<div class="item">--}}
                        {{--<img src="uploads/header-slide-1.jpg">--}}
                        {{--<div class="slide-text center">--}}
                        {{--<h3 class="underlined slide-title">Great Title for this slider</h3>--}}
                        {{--<h4 class="slide-subtitle">here will be some subtitle</h4>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                        {{--<img src="uploads/header-slide-2.jpg">--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                        {{--<img src="uploads/header-slide-3.jpg">--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                        {{--<img src="uploads/header-slide-1.jpg">--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                        {{--<img src="uploads/header-slide-2.jpg">--}}
                        {{--</div>--}}
                        {{--<div class="item">--}}
                        {{--<img src="uploads/header-slide-3.jpg">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6">--}}
                        {{--<ul class="list-unstyled">--}}
                        {{--<li>Dolor exercitation officia id anim do esse nisi proident quis occaecat ipsum.</li>--}}
                        {{--<li>Dolor exercitation officia id anim do esse nisi proident quis occaecat ipsum.</li>--}}
                        {{--<li>Dolor exercitation officia id anim do esse nisi proident quis occaecat ipsum.</li>--}}
                        {{--<li>Dolor exercitation officia id anim do esse nisi proident quis occaecat ipsum.</li>--}}
                        {{--<li>Dolor exercitation officia id anim do esse nisi proident quis occaecat ipsum.</li>--}}
                        {{--</ul>--}}
                        {{--<p>--}}
                        {{--Some other text about product.Cillum eu deserunt sunt quis proident cillum in sit elit mollit exercitation aute eiusmod. Amet ea sint minim incididunt veniam culpa exercitation id dolore. Commodo ex ipsum sit cupidatat nisi. Labore amet consectetur enim duis sit ex magna.--}}
                        {{--</p>--}}
                        {{--<div class="btn-group">--}}
                        {{--<a href="" class="btn btn-lg btn-success">Download</a>--}}
                        {{--<a href="" class="btn btn-default btn-lg">Show video</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <img src="{{ $product['featured_image'] }}" alt="{{ $product['title'] }}" class="pull-right">
                        {!! $product['text'] !!}
                    </div>
                </article>
            </div>

        </div>
    </div>
@endsection