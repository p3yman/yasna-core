@extends('iranstrick::test.front.frame.frame')

@php
    $page_title = "معرفی";
    $site_title = "ایران‌اشتریک";

    $about_img = Module::asset('iranstrick:images/demo/book2-final.jpg');
    $about_content = '
         <p style="text-align: justify;"><span style="font-size: 10pt;">شرکت ایـران اشـتریک با بیش از 42 سال تجـربه درصـنایع نســاجی همواره بهتـرین تکنــولوژی، کیفیـت و خدمـات را به مشتــریان خود ارائه داده است. متخصصین با تجـربه این شرکت با اجرای بزرگتـرین پروژه های کشور،&nbsp; نامی درخشـان از این مجموعه را در صنـعت باقی گذاشـته اند بیش از چهار دهه است که&nbsp; ایـن شرکت در جهت ارتـقاء صنایع نساجی کشـور و توسعه و شکوفایی&nbsp; آن کوشا بوده است.</span></p>
<p style="text-align: justify;"><span style="font-size: 10pt;">ارائـه دهنده&nbsp; طـرح توجـیـهی، انـتـخـاب مـاشیـن آلات، نـصـب و راه اندازی&nbsp; وخـدمـات پس از فـروش با به کارگیری دانش فنی روز دنیـا در زمینه:</span></p>
<p style="text-align: justify;"><span style="font-size: 10pt;"> ریسـندگـی، مقـدمات بافنـدگی، بـافنـدگی تـاری پـودی-حلقـوی، شستشو وسفیدگری، رنگرزی، چاپ و تکمیل، دوخت، تکمیل پوشـاک، تکمیل فرش، منسوجات بی بافت، کالای خواب، تایرکورد و سیستم تهویه صنعتی</span><br><br><br><br><br>&nbsp;<br><br></p>
    ';
@endphp

@section('page_title')
{{--{{ Setting::get(getLocale() . '_site_title') }} - {{ trans('iranstrick::front.about') }}--}}
@endsection

@section('content')
<div class="sub-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-title">{{ $page_title }}</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{ url('/') }}">{{ trans('iranstrick::front.home_page') }}</a>
                    </li>
                    <li class="active">{{ $page_title }}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<script src="js/owl.carousel.min.js"></script>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <article class="article">
                <div class="title-bar">
                    <h3 class="title">{{ $site_title }}</h3>
                </div>
                <div class="content">
                    <img src="{{ $about_img }}" alt="Image for a good title" class="pull-right">
                    {!! $about_content !!}
                </div>
            </article>
        </div>
    </div>
</div>
@endsection