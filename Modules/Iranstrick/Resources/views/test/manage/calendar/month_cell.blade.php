<div id="divCell-{{$month_day}}" class="cell">
	@if(Auth::user()->can('calendar.create'))
		<div class="add hoverHide">
			<a href="javascript:void(0)" onclick="handleSelector('{{$para['year']}}' , '{{$para['month']}}' , '{{$month_day}}')">
				<i class="fa fa-plus-circle"></i>
			</a>
		</div>
	@endif
	<div class="gregorian">
		{{ $month[$month_day]->toFormattedDateString() }}
	</div>
	<div class="day">
		@pd($month_day)
	</div>
	<div class="handles">
	</div>
</div>
