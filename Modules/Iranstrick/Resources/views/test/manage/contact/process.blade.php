@include('templates.modal.start' , [
	'partial' => true ,
    'form_url' => url("manage/contact/save/$model->id/answer"),
	'modal_title' => trans('manage.contact.view')
])
{{null, $model->spreadMeta()}}
<div class='modal-body'>
    <div class="row">
        <div class="col-xs-3 text-left">
            @include("manage.frame.widgets.grid-text", [
                'text' => $model->name_first . ':',
            ])
        </div>
        <div class="col-xs-9">
            @include("manage.frame.widgets.grid-text", [
                'text' => '"' . $model->text . '"',
            ])
        </div>
    </div>
    <div class="row m30">
        <div class="col-xs-6 text-center">
            @include("manage.frame.widgets.grid-text", [
                'text' => $model->email,
            ])
        </div>
        <div class="col-xs-6 text-center">
            @include("manage.frame.widgets.grid-text", [
                'text' => $model->mobile,
            ])
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @include('forms.textarea' , [
                'name' => 'answer_text',
                'class' => 'form-default fixed-width' ,
                'label' => false,
                'placeholder' => trans('manage.contact.send_answer')
            ])
            @include('forms.feed')
        </div>
        <div class="col-xs-12 text-center">
            @include('forms.button' , [
                'label' => trans('manage.contact.answer'),
                'shape' => 'success',
                'type' => 'submit' ,
            ])
            @include("forms.button", [
                'label' => trans('manage.contact.check'),
                'class' => 'btn-primary',
                'condition' => Auth::user()->can('contact.process') and !$model->isPublished(),
                'link' => '$.ajax({url:"' . url("manage/contact/save/$model->id/check") . '", success: function(){window.location.reload()}});',
            ])
            @include("forms.button", [
                'label' => trans('manage.contact.uncheck'),
                'class' => 'btn-primary',
                'condition' => Auth::user()->can('contact.process') and $model->isPublished() and !$model->trashed(),
                'link' => '$.ajax({url:"' . url("manage/contact/save/$model->id/uncheck") . '", success: function(){window.location.reload()}});',
            ])
            @include("forms.button", [
                'label' => trans('forms.button.delete'),
                'class' => 'btn-danger',
                'condition' => Auth::user()->can('contact.delete') and !$model->trashed(),
                'link' => '$.ajax({url:"' . url("manage/contact/save/$model->id/delete") . '", success: function(){window.location.reload()}});',
            ])
            @include("forms.button", [
                'label' => trans('forms.button.undelete'),
                'class' => 'btn-info',
                'condition' => Auth::user()->can('contact.bin') and $model->trashed(),
                'link' => '$.ajax({url:"' . url("manage/contact/save/$model->id/undelete") . '", success: function(){window.location.reload()}});',
            ])
            @include("forms.button", [
                'label' => trans('manage.contact.destroy'),
                'class' => 'btn-danger',
                'condition' => Auth::user()->can('contact.bin') and $model->trashed(),
                'link' => '$.ajax({url:"' . url("manage/contact/save/$model->id/destroy") . '", success: function(){window.location.reload()}});',
            ])
        </div>
    </div>
</div>
@include('templates.modal.end')