@extends('manage.frame.use.0')

@section('section')
    @include('manage.contact.tabs')

    {{--
    |--------------------------------------------------------------------------
    | Toolbar
    |--------------------------------------------------------------------------
    |
    --}}
    <div class="panel panel-toolbar row w100">
        <div class="col-md-6">
            <p class="title">
                {{trans('manage.contact.trans')}} : {{trans('posts.status.'.$requested_tab)}}
            </p>
        </div>
        <div class="col-md-6 tools">
            @if(Auth::user()->can('contact.create'))
                @include('manage.frame.widgets.toolbar_button' , [
                    'target' => url('manage/contact/create') ,
                    'type' => 'success' ,
                    'caption' => trans('manage.permits.create' ) ,
                    'icon' => 'plus-circle' ,
                ])
            @endif

            {{--@include('manage.frame.widgets.toolbar_search_inline' , [--}}
            {{--'target' => url('manage/posts/'.$branch->slug.'/searched/') ,--}}
            {{--'label' => trans('forms.button.search') ,--}}
            {{--'value' => isset($keyword)? $keyword : '' ,--}}
            {{--])--}}
        </div>
    </div>


    {{--
    |--------------------------------------------------------------------------
    | Grid
    |--------------------------------------------------------------------------
    |
    --}}

    @include('manage.frame.widgets.grid' , [
        'table_id' => 'tblContacts' ,
        'row_view' => 'manage.contact.browse-row' ,
        'counter' => true ,
        'headings' => [
            trans('validation.attributes.title') ,
            trans('validation.attributes.current_status'),
            trans('forms.button.action'),
        ],
    ])
@endsection