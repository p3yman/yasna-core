@include('manage.frame.use.tabs' , [
    'current' => $requested_tab,
	'tabs' => [
		['pending' , trans('manage.contact.pending') ],
		['published' , trans('manage.contact.checked') ],
		['all' , trans('posts.categories.all')],
		['bin' , trans('posts.manage.bin') ,'contact.bin'],
	] ,
])
