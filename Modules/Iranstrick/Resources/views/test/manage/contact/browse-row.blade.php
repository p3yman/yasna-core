@include('manage.frame.widgets.grid-rowHeader' , [
	'refresh_url' => "manage/contact/update/$model->id",
	'fake' => $model->spreadMeta() ,
])


<td>

    @include("manage.frame.widgets.grid-text" , [
        'text' => $model->title,
        'link' => Auth::user()->can('contact.process') ? "modal:manage/contact/-id-/process" : false,
    ])

    @include("manage.frame.widgets.grid-text", [
        'text' => str_limit($model->text, 150),
        'size' => 10,
    ])

    @include("manage.frame.widgets.grid-tiny", [
        'text' => $model->name_first,
        'size' => 10,
        'icon' => 'user',
        'color' => 'gray'
    ])

    @include("manage.frame.widgets.grid-date", [
        'date' => $model->created_at
    ])
</td>

<td>
    @include("manage.frame.widgets.grid-text", [
        'text' => $model->status_text,
        'color' => $model->status_color,
        'icon' => $model->status_icon,
    ])

    @include("manage.frame.widgets.grid-tiny", [
        'text' => trans('forms.general.by') .' ' . $model->say('published_by'),
        'icon' => 'user',
        'condition' => $model->isPublished(),
    ])

    @include("manage.frame.widgets.grid-tiny", [
        'text' => trans('forms.general.by') .' ' . $model->say('deleted_by'),
        'icon' => 'user',
        'condition' => $model->trashed(),
    ])

    @include("manage.frame.widgets.grid-date", [
        'date' => $model->published_at,
        'condition' => $model->isPublished(),
    ])

    @include("manage.frame.widgets.grid-date", [
        'date' => $model->deleted_at,
        'condition' => $model->trashed(),
    ])
</td>


@include('manage.frame.widgets.grid-actionCol' , [ 'actions' => [
		[
		    'eye' ,
		    trans('manage.permits.view'),
		    "modal:manage/contact/-id-/process",
		    'contact.process',
        ],
		[
		    'check',
            trans('manage.contact.check'),
            '$.ajax({url:"http://localhost/iranstrick-final/public/manage/contact/save/-id-/check", success: function(){window.location.reload()}});',
            'contact.process',
            !$model->isPublished(),
        ],
		[
		    'undo',
		    trans('manage.contact.uncheck'),
		    '$.ajax({url:"http://localhost/iranstrick-final/public/manage/contact/save/-id-/uncheck", success: function(){window.location.reload()}});',
		    'contact.process',
		    $model->isPublished() and !$model->trashed()
        ],
		[
		    'trash',
		    trans('forms.button.delete'),
		    '$.ajax({url:"http://localhost/iranstrick-final/public/manage/contact/save/-id-/delete", success: function(){window.location.reload()}});',
		    'contact.delete',
		    !$model->trashed()
        ],
		[
		    'undo',
		    trans('forms.button.undelete'),
		    '$.ajax({url:"http://localhost/iranstrick-final/public/manage/contact/save/-id-/undelete", success: function(){window.location.reload()}});',
		    'contact.bin',
		    $model->trashed()
        ],
		[
		    'times',
		    trans('manage.contact.destroy'),
		    '$.ajax({url:"http://localhost/iranstrick-final/public/manage/contact/save/-id-/destroy", success: function(){window.location.reload()}});',
		    'contact.bin',
		    $model->trashed()
        ],
//		['pencil' , trans('manage.permits.edit') , "url:manage/posts/".$model->branch()->slug."/edit/-id-" , '*' , $model->canEdit()],
//		['times' , trans('forms.button.hard_delete') , 'modal:manage/posts/-id-/hard_delete' , $model->branch()->slug.".bin" , $model->trashed() and Auth::user()->isDeveloper()] ,
]])