@extends(CurrentModule::bladePath('layouts.frame.main'))

@php Template::appendToPageTitle($title = CurrentModule::trans('front.contact_us')) @endphp

@section('content')
	@include(CurrentModule::bladePath('layouts.breadcrumb'), [
		'title' => $title,
		'items' => [
			[
				'title' => CurrentModule::trans('front.home_page'),
				'link' => CurrentModule::actionLocale('IranstrickController@index')
			],
			[
				'title' => $title
			]
		]
	])

	<div class="container pt20 pb20">
		<div class="row">
			<div class="col-xs-12">
				<article class="article">
					<div class="title-bar">
						<h3 class="title">{{ $title }}</h3>
					</div>
					<div class="content">
						<div class="row">
							@include(CurrentModule::bladePath('contact.contact-info'))
							@include(CurrentModule::bladePath('contact.contact-form'))
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
@endsection