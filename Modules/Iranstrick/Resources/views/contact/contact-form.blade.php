<div class="col-md-5 pr10 pl10">
	@if (
		$commenting_post->exists and
		count($fields = CommentingTools::translateFields($commenting_post->spreadMeta()->fields))
	)

		{!!
			Form::open([
				'url' => CurrentModule::actionLocale('ContactController@save'),
			])
		!!}

		<input type="hidden" name="post_id" value="{{ $commenting_post->hashid }}">

		<div class="col-xs-12">
			@foreach($fields as $fieldName => $fieldInfo)
				@include(CurrentModule::bladePath('contact.contact-input'))
			@endforeach
			<div class="form-group">
				@if (!env('APP_DEBUG'))
					<div class="form-input">
						{!! app('captcha')->render(getLocale()); !!}
					</div>
				@endif
			</div>

			<div class="row">
				<div class="col-xs-12">
					@if ($errors->count())
						<div class="alert alert-danger">
							{!! implode('<br />', array_merge(...array_values($errors->toArray()))) !!}
						</div>
					@elseif(session()->has('success_message'))
						<div class="alert alert-success">
							{{ session()->get('success_message') }}
						</div>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<button type="submit" id="_submit" name="_submit"
							class="btn btn-primary pull-{{(getLocale() == 'fa') ? 'right' : 'left'}}">
						{{ CurrentModule::trans('forms.button.send_your_message') }}
					</button>
				</div>
			</div>
		</div>


		<div class="col-xs-12" style="padding-top: 10px">
			<div class="row">
				<div class="row text-justify">
					@include('iranstrick::test.forms.feed')
				</div>
			</div>
		</div>

		{!! Form::close() !!}

	@endif
</div>
