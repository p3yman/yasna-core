<div class="col-md-7">
	@if (array_key_exists('address', ($contact_info = Template::contactInfo())))
		<p>
			<span style="font-size: 10pt">
				{{ CurrentModule::trans('front.address') }}:
				{{ $contact_info['address'] }}
			</span>
		</p>
	@endif
	@if (array_key_exists('telephone', $contact_info))
		<p>
			<span style="font-size: 10pt">
				{{ CurrentModule::trans('front.phone') }}:
				@foreach($contact_info['telephone'] as $phone)
					@if ($loop->index)
						,
					@endif
					<span>
						{{ $phone }}
					</span>
				@endforeach
			</span>
		</p>
	@endif
	@if (array_key_exists('email', $contact_info))
		<p>
			<span style="font-size: 10pt">
				{{ CurrentModule::trans('front.email') }}:
				@foreach($contact_info['email'] as $email)
					@if ($loop->index)
						,
					@endif
					<span>
						{{ $email }}
					</span>
				@endforeach
			</span>
		</p>
	@endif
</div>