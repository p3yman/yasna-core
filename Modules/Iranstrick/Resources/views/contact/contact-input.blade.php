<div class="form-group">
	@if ($fieldName == 'text')
		<textarea class="form-control required" id="{{ kebab_case($fieldName) }}" name="{{ $fieldName }}"
				  placeholder="{{ CurrentModule::trans("validation.attributes.$fieldName")}}"
				  rows="6">{{ old($fieldName) }}</textarea>
	@else
		<input type="text" class="form-control required" id="{{ kebab_case($fieldName) }}" name="{{ $fieldName }}"
			   placeholder="{{ CurrentModule::trans("validation.attributes.$fieldName") }}"
			   value="{{ old($fieldName) }}">
	@endif
</div>