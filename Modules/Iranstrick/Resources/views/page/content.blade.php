<div class="container">
	<div class="row mobile-reverse">
		<div class="col-md-4">
			@include(CurrentModule::bladePath('home.expo'))
		</div>

		<div class="col-md-8">
			<article class="article">
				<div class="title-bar">
					<h4 class="title">{{ $title }}</h4>
					<h6 class="show-more">{{ ad(echoDate($post->published_at)) }}</h6>
				</div>

				<div class="content">
					@if ($imageUrl = ($image = fileManager()->file($post->featured_image)->resolve())->getUrl())
						<img src="{{ $imageUrl }}" alt="{{ $image->getTitle() }}" class="pull-right">
					@endif
					{!! $post->text !!}
				</div>
			</article>

			@yield('additional_content')

		</div>
	</div>
</div>