@extends(CurrentModule::bladePath('layouts.frame.main'))

@php Template::appendToPageTitle($title = $post->title) @endphp

@section('content')
	@include(CurrentModule::bladePath('layouts.breadcrumb'), [
		'title' => $posttype->titleIn(getLocale()),
		'items' => [
			[
				'title' => CurrentModule::trans('front.home_page'),
				'link' => CurrentModule::actionLocale('IranstrickController@index')
			],
			[
				'title' => $title
			]
		]
	])

	@include(CurrentModule::bladePath('page.content'))

@append