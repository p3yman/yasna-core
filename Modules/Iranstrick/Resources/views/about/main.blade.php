@extends(CurrentModule::bladePath('layouts.frame.main'))

@php Template::appendToPageTitle($title = $static_post->title) @endphp

@section('content')
	@include(CurrentModule::bladePath('layouts.breadcrumb'), [
		'title' => $title,
		'items' => [
			[
				'title' => CurrentModule::trans('front.home_page'),
				'link' => CurrentModule::actionLocale('IranstrickController@index')
			],
			[
				'title' => $title
			]
		]
	])

	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<article class="article">
					<div class="title-bar">
						<h3 class="title">{{ Template::siteTitle() }}</h3>
					</div>
					<div class="content">
						@if ($imageUrl = ($image = fileManager()->file($static_post->featured_image))->getUrl())
							<img src="{{ $imageUrl }}" alt="{{ $image->getTitle() }}" class="pull-right">
						@endif
						{!! $static_post->text !!}
					</div>
				</article>
			</div>
		</div>
	</div>
@append
