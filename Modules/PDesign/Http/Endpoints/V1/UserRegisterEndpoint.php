<?php

namespace Modules\PDesign\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\PDesign\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/p-design-user-register
 *                    User Register
 * @apiDescription    register the user
 * @apiVersion        1.0.0
 * @apiName           User Register
 * @apiGroup          PDesign
 * @apiPermission     user
 * @apiParam {string} name_first  The first name of user
 * @apiParam {string} name_last  The last name of user
 * @apiParam {string} password  The password name of user
 * @apiParam {string} mobile  The mobile name of user
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *       "status": 200,
 *      "metadata": {
 *            "id": "aALnK",
 *             "access_token":
 *             "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS1jb3JlLnNoXC9hcGlcL21vZHVsYXJcL3YxXC9mcm9zdHktcmVnaXN0ZXItdXNlciIsImlhdCI6MTU2Mzc4MTY3MiwiZXhwIjoxNTY3MzgxNjcyLCJuYmYiOjE1NjM3ODE2NzIsImp0aSI6Indram9ZSWlCcFFpSUxwamsiLCJzdWIiOjcsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjciLCJwYXlsb2FkIjp7ImlkIjoiYUFMbksiLCJmdWxsX25hbWUiOiJcdTA2NDVcdTA2NDdcdTA2NDRcdTA2MjcgXHUwNjQ1XHUwNjJkXHUwNjQ1XHUwNjJmXHUwNmNjIiwicm9sZXMiOltdfX0.DJS8eCdEaBjLjrbGZvWPn40io9l4PBG7B4ob6xlWsqs",
 *             "token_type": "bearer",
 *             "expires_in": 3600000,
 *             "authenticated_user": null
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Error validating data",
 *      "userMessage": "Received data has errors"
 *      "errorCode": 422,
 *      "moreInfo": "p-design.moreInfo.422",
 *      "errors": [
 *                  "name_first": [
 *                      [
 *                      "The name_first field is required."
 *                    ]
 *               ],
 *                 "password": [
 *                      [
 *                      "The Password field is required."
 *                     ]
 *              ],
 *      ]
 * }
 * @method UserController controller() //
 *         controller
 */
class UserRegisterEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Register";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\PDesign\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@register';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "id"           => hashid(rand(1, 100)),
             "access_token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS1jb3JlLnNoXC9hcGlcL21vZHVsYXJcL3YxXC9mcm9zdHktcmVnaXN0ZXItdXNlciIsImlhdCI6MTU2Mzc4MTI1OSwiZXhwIjoxNTY3MzgxMjU5LCJuYmYiOjE1NjM3ODEyNTksImp0aSI6IkNvNFp4ajJVQ1VyQTZLWEwiLCJzdWIiOjQsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjciLCJwYXlsb2FkIjp7ImlkIjoib3hsak4iLCJmdWxsX25hbWUiOiJcdTA2NDVcdTA2NDdcdTA2NDRcdTA2MjcgXHUwNjQ1XHUwNjJkXHUwNjQ1XHUwNjJmXHUwNmNjIiwicm9sZXMiOltdfX0.5auSzTAY8jqdCpxAGTUF5_F2r1pe45CSikkwHtxVVHo",
             "token_type"   => "bearer",
             "expires_in"   => "3600000",
        ]);
    }
}
