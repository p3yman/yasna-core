<?php

namespace Modules\PDesign\Http\Requests\V1;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class UserRegisterRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "User";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name_first'           => 'required|persian:60',
             'name_last'            => 'required|persian:60',
             'pw'                   => 'required|same:password2|min:8|max:50',
             'mobile'               => [
                  'required',
                  'phone:mobile',
                   Rule::unique('persons'),
             ],
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name_first' => 'pd',
             'name_last'  => 'pd',
             'mobile'     => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'name_first',
             'name_last',
             'pw',
             'password2',
             'mobile',
             'g-recaptcha-response',
        ];
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {
        $this->setHashPassword();
        $this->unsetPassword2();
        $this->unsetGRecaptcha();
    }



    /**
     * hash the password
     *
     * @return void
     */
    protected function setHashPassword()
    {
        $password = $this->getData('pw');

        $password_hash = Hash::make($password);

        $this->setData('pw', $password_hash);
    }



    /**
     * unset password2
     *
     * @return void
     */
    protected function unsetPassword2()
    {
        $this->unsetData('password2');
    }



    /**
     * unset g-recaptcha-response
     *
     * @return void
     */
    protected function unsetGRecaptcha()
    {
        $this->unsetData('g-recaptcha-response');
    }
}
