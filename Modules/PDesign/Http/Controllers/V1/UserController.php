<?php

namespace Modules\PDesign\Http\Controllers\V1;

use Modules\PDesign\Http\Requests\V1\UserRegisterRequest;
use Modules\Persons\Entities\User;
use Modules\Yasna\Services\YasnaApiController;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends YasnaApiController
{
    /**
     * register the user.
     *
     * @param UserRegisterRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function register(UserRegisterRequest $request)
    {
        /** @var User $user */
        $user = $request->model->batchSave($request);

        $token = JWTAuth::fromUser($user);

        if (!$token) {
            return $this->clientError('40001');
        }

        return $this->typicalSaveFeedback(true, [
             "id"           => hashid($user->id),
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ]);
    }
}
