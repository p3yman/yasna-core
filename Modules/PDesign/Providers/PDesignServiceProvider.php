<?php

namespace Modules\PDesign\Providers;

use Modules\PDesign\Http\Endpoints\V1\UserRegisterEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class PDesignServiceProvider
 *
 * @package Modules\PDesign\Providers
 */
class PDesignServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndPoint();
    }

    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndPoint()
    {
        endpoint()->register(UserRegisterEndpoint::class);
    }
}
