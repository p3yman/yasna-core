<?php
return [
     'command'  => 'دستور',
     'commands' => 'دستورات پروژه‌ی «:project»',

     'command_not_found' => 'دستور یافت نشد.',

     'create'       => 'افزودن دستور جدید',
     'create_title' => 'افزودن دستور به پروژه‌ی «:project»',

     'run'       => 'اجرای آنی',
     'run_title' => 'اجرای آنی دستور در پروژه‌ی «:project»',

     'edit'       => 'ویرایش',
     'edit_title' => 'ویرایش دستور در پروژه‌ی «:project»',

     "clone"                       => "همسانه‌سازی در پروژه‌ی دیگر",
     "clone_commands_title"        => "کپی دسته‌ی دستورات از پروژه‌ی «:project»",
     "select_destination_projects" => "پروژه‌های مقصد",

     'command_state'                => 'وضعیت دستور',
     'command_states'               => 'وضعیت دستورات',
     'command_states_title'         => 'وضعیت اجرای دستورات پروژه‌ی «:project» بر بستر «:host»',
     'instant_command_states_title' => 'وضعیت اجرای دستور آنی در پروژه‌ی «:project»',
     'host'                         => 'بستر',
     'command_result'               => 'نتیجه‌ی اجرا',
     'status_pending'               => 'در صف اجرا',
     'status_running'               => 'در حال اجرا',
     'status_successful'            => 'اجرای موفق',
     'status_failed'                => 'اجرای ناموفق',
     'status_unknown'               => 'وضعیت نامعلوم',
     'command_elapsed_time'         => 'مدت زمان اجرا',

     'command_delete'         => 'حذف دستور',
     'command_delete_confirm' => 'آیا از حذف این دستور در پروژه‌ی «:project» اطمینان دارید؟',

     'type'         => 'نوع',
     'type_early'   => 'پیشین',
     'type_later'   => 'پسین',
     'type_instant' => 'آنی',
     'type_unknown' => 'نامشخص',
     'type_hint'    => 'دستورات پیشینی قبل از ایجاد نسخه‌ها اجرا می‌شوند و دستورات پسینی بعد از ایجاد آنها.',

     'all_commands'     => 'پیشین و پسین',
     'instant_commands' => 'آنی',

     "macro_usage" => "در تعریف دستورات می‌توانید از متغیرهای محیطی در قالب <a href=\"https://mustache.github.io/mustache.5.html\">سبیل</a> <code>{{}}</code> استفاده کنید. برای مثال متغیر محیطی <code>USER</code> را می‌توان به صورت <code>chmod -R {{USER}}:{{USER}} /var/www/html</code> استفاده کرد. در این صورت اگر مقدار آن <code>yasna</code> فرض شود، دستور در زمان اجرا به صورت <code>chmod -R yasna:yasna /var/www/html</code> تفسیر و اجرا خواهد شد.",
];
