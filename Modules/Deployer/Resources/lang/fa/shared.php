<?php

return [
     'shared_resources' => 'منابع اشتراکی پروژه‌ی «:project»',
     'shared_resource'  => 'منبع اشتراکی',

     'path' => 'مسیر',

     'create'                => 'افزودن منبع اشتراکی',
     'edit'                  => 'ویرایش',
     'add_shared_resources'  => 'افزودن منبع اشتراکی به پروژه‌ی «:project»',
     'edit_shared_resources' => 'ویرایش منبع اشتراکی به پروژه‌ی «:project»',

     'delete'               => 'حذف',
     'delete_confirm_title' => 'آیا از حذف این منبع اشتراکی از پروژه‌ی «:project» مطمئنید؟',
     'shared_delete'        => 'حذف منبع اشتراکی',

     'shared_resource_not_found' => 'منبع اشتراکی یافت نشد!',
];
