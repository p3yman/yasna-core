<?php
return [
     "Deployer"         => "دپلویر",
     'Index'            => 'صفحه‌ی اصلی',
     'Modules'          => "ماژول‌ها",
     'Create a Project' => 'ایجاد پروژه',
     'Projects'         => 'پروژه‌ها',
     'Deploys'          => 'دپلوی‌ها',
     'Servers'          => 'سرورها',
     'createProject'    => 'ایجاد پروژه',
     'operations'       => 'عملیات',
];
