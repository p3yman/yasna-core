<?php

return [
     "macros" => "متغیرهای محیطی",

     "no_acceptable_action"           => "عملیات درخواستی شما مجاز نیست.",
     "macro_not_found"                => "متغیر محیطی یافت نشد.",
     "macro_already_exist_on_project" => "متغیر محیطی «:macro» قبلا در این پروژه تعریف شده است.",

     "title_label"        => "متغیرهای محیطی پروژه‌ی «:project»",
     "host_title_label"   => "متغیرهای محیطی پروژه‌ی «:project» بر بستر «:host»",
     "macro"              => "متغیر محیطی",
     "value"              => "مقدار",
     "override"           => "بازنویسی",
     "is_host_overridden" => "وضعیت بازنویسی",
     "is_overridden"      => "بازنویسی شده است.",
     "is_not_overridden"  => "بازنویسی نشده است.",

     "create"          => "افزودن متغیر محیطی",
     "edit"            => "ویرایش",
     "delete"          => "حذف",
     "remove-override" => "حذف بازنویسی",

     "create_macro_title"   => "افزودن متغیر محیطی به پروژه‌ی «:project»",
     "edit_macro_title"     => "ویرایش متغیر محیطی در پروژه‌ی «:project»",
     "override_macro_title" => "بازنویسی متغیر محیطی پروژه‌ی «:project» در بستر «:host»",

     "macro_delete_confirm"          => "آیا از حذف این متغیر محیطی از پروژه‌ی «:project» مطمئنید؟",
     "macro_override_delete_confirm" => "آیا از حذف بازنویسی این متغیر محیطی از پروژه‌ی «:project» بر بستر «:host» مطمئنید؟",
     "macro_delete"                  => "حذف متغیر محیطی",
     "macro_override_delete"         => "حذف بازنویسی متغیر محیطی",

     "global_value_adding_form_desc"     => "متغیرهای محیطی زیر به صورت پیش‌فرض در سیستم تعریف شده و قابل استفاده‌اند:",
     "project_macro_adding_form_desc"    => "اگر پروژه در مسیر <code>home/dp/domains/yasna.team/</code> دپلوی می‌شود، این متغیر به این مقدار اشاره می‌کند. این مسیر شامل پوشه‌های <code>releases</code> و <code>shared</code> است.",
     "current_macro_adding_form_desc"    => "این ماکرو به پوشه‌ای اشاره می‌کند که اکنون به عنوان نسخه‌ی نهایی در دسترس است. برای مثال اگر نسخه‌ی <code>11</code> به عنوان نسخه‌ی نهایی انتخاب شده باشد، این ماکرو به مسیر <code>home/dp/domains/yasna.team/releases/11/</code> اشاره می‌کند.",
     "demo_macro_adding_form_desc"       => "این ماکرو به پوشه‌ای اشاره می‌کند که اکنون به عنوان نسخه‌ی نمایشی در دسترس است. برای مثال اگر نسخه‌ی <code>7</code> به عنوان نسخه‌ی نمایشی انتخاب شده باشد، این ماکرو به مسیر <code>home/dp/domains/yasna.team/releases/7/</code> اشاره می‌کند.",
     "subsequent_macro_adding_form_desc" => "این ماکرو به پوشه‌ای اشاره می‌کند که در حال حاضر تقاضای نوعی از عملیات بر روی آن صادر شده است. برای مثال اگر تقاضای نهایی شدن نسخه‌ی <code>3</code> در یک دستور دپلوی صادر شده باشد، این ماکرو به مسیر <code>home/dp/domains/yasna.team/releases/3/</code> اشاره می‌کند. محتوای این ماکرو در هر دستور دپلوی به ورژن بستگی خواهد داشت و امکان استفاده از آن در دستورات آنی وجود ندارد.",
];
