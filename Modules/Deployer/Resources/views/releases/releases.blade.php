{{
	widget('modal')
		->label($title_label)
}}

	<div class=“modal-body”>
		@include("manage::widgets.grid" , [
			'headings' => $headings,
			'row_view' => 'deployer::releases.version_row',
		])
	</div>
</div>
