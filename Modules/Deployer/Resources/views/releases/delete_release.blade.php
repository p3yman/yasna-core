@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.project.delete_release', ['hashid' => $model->hashid]),
	'title_value' => $model->project->name,
	'title_label' => '',
	'save_label' => trans('deployer::project.remove_the_release'),
	'save_shape' => 'danger',
	'cancel_link' => master_modal("deployer.project.releases", ['hashid' => $model->project->hashid], false)
])
