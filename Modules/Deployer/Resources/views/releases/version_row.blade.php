<?php
/* @var \Modules\Deployer\Entities\DeployerModuleVersion $model */
?>
@include('manage::widgets.grid-rowHeader', [
	'refresh_url' => route("deployer.project.reload_releases", ['hashid' => $model->hashid], false)
])

<td>
	{{ pd($model->version) }}
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->created_at,
		'default' => "fixed",
		'format' => 'j F Y',
		'size' => '14',
	])
</td>


<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->updated_at,
		'default' => "fixed",
		'format' => 'j F Y',
		'size' => '14',
	])
</td>

<td>
	@include("manage::widgets.grid-badge" , [
		'condition' => $model->is_an_open_release,
		'icon' => "unlock",
		'text' => trans("deployer::project.open_release"),
		'color' => 'success',
	])

	@include("manage::widgets.grid-badge" , [
		'condition' => $model->is_a_closed_release,
		'icon' => "lock",
		'text' => trans("deployer::project.closed_release"),
		'color' => 'warning',
	])
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['lock text-red', trans('deployer::project.close_release'), 'modal:' . route('deployer.project.close_release_confirm', ['hashid' => $model->hashid], false), $model->is_an_open_release],
		['eye', trans('deployer::project.select_a_release_as_demo'), 'modal:' . route('deployer.deploy.select_as_release_confirm', ['hashid' => $model->hashid, 'type' => 'demo'], false), $model->is_a_closed_release && Deployer::canDeployRelease()],
		['cube', trans('deployer::project.select_a_release_as_final'), 'modal:' . route('deployer.deploy.select_as_release_confirm', ['hashid' => $model->hashid, 'type' => 'final'], false), $model->is_a_closed_release && Deployer::canDeployRelease()],
		['folder-open', trans('deployer::project.view_modules'), 'modal:' . route('deployer.project.module_list', ['hashid' => $model->hashid], false)],
		['trash text-red', trans('deployer::project.remove_the_release'), modal_route('deployer.project.delete_release_confirm', ['hashid' => $model->hashid], false), $model->is_a_closed_release and $model->is_not_selected_release and Deployer::canDeleteRelease()],
	],
])
