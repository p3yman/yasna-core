@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.project.close_release', ['hashid' => $version->hashid]),
	'modal_title' => $title_label,
	'title_value' => $model->name,
	'title_label' => '',
	'save_label' => trans('deployer::project.close_release'),
	'save_shape' => 'warning',
	'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.releases", ['hashid' => $version->project->hashid], false))
])
