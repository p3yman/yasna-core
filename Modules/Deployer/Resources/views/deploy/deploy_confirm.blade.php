@include("manage::layouts.modal-delete" , [
	'form_url' => route('deployer.deploy.do_deploy_release', ['hashid' => $model->hashid, 'type' => $type]),
	'modal_title' => trans('deployer::project.confirm_message_to_start_deployment', ['name' => $model->project->name, 'type' => $release_as]),
	'title_value' => $model->version,
	'title_label' => trans('deployer::project.release_version'),
	'save_shape' => 'success',
	'save_label' => trans('deployer::project.start_deploy'),
])
