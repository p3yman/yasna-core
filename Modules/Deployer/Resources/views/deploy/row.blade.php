@include('manage::widgets.grid-rowHeader', [
	'refresh_url' => route("deployer.project.deploy_reload", ['hashid' => $model->hashid], false)
])

<td>
	{{ pd($model->release()->withTrashed()->first()->project->name) }}
</td>

<td>
	{{ pd($model->release()->withTrashed()->first()->version) }}
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->created_at,
		'default' => "fixed",
		'format' => 'j F Y، H:i:s',
		'size' => '14',
	])
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->updated_at,
		'default' => "fixed",
		'format' => 'j F Y، H:i:s',
		'size' => '14',
	])
</td>

<td>
	{{ trans("deployer::project.type_{$model->type}") }}
</td>

<td>
	@include("deployer::deploy._status")
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['info-circle', trans('deployer::project.deploy_details'), 'modal:' . route('deployer.deploy.deploy_details', ['hashid' => $model->hashid], false)],
		['ban', trans('deployer::project.cancel_deploy'), modal_route('deployer.deploy.deploy_cancellation_confirm', ['hashid' => $model->hashid], false), $model->is_pending_status],
	],
])
