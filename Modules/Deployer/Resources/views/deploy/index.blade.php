{{
	widget('modal')
		->label($title_label)
}}

<div class="modal-body">
	@include('deployer::deploy._grid')
</div>

<div class="modal-footer">
	@include("manage::widgets.toolbar" , [
		'title'             => '',
		'buttons'           => [
			[
				'target'  => modal_route("deployer.project.deploys_list", ['hashid' => $project->hashid], false),
				'caption' => trans('deployer::project.refresh'),
				'icon'    => "refresh",
				'type'    => "info",
			],
		],
	])
</div>
</div>
