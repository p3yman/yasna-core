<?php chalk()->write($model) ?>

<td>
	{{ $model->host->name }}
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->updated_at,
		'default' => "fixed",
		'format' => 'j F Y، H:i:s',
		'size' => '14',
	])
</td>

<td>
	@include("deployer::deploy._status")
</td>

<td>
	{{ ed($model->description) }}
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['terminal', trans('deployer::command.command_states'), modal_route('deployer.project.command_sates', ['hashid' => $model->hashid], false)],
	],
])
