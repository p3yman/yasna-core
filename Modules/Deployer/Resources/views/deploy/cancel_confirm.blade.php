@include("manage::layouts.modal-delete" , [
	'form_url' => route('deployer.deploy.deploy_cancellation_confirm', ['hashid' => $model->hashid]),
	'modal_title' => trans('deployer::project.confirm_message_to_cancel_deployment', ['project' => $model->project->name]),
	'title_value' => $model->release->version,
	'title_label' => trans('deployer::project.release_version'),
	'save_shape' => 'warning',
	'save_label' => trans('deployer::project.cancel_deploy'),
])
