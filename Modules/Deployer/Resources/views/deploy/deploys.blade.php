@extends('manage::layouts.template')

@section('content')
	@include("manage::widgets.tabs", [
		'current' => $tab,
		'tabs' => $tabs_data,
	])

	@include("manage::widgets.toolbar" , [
		'title'             => $title,
	])

	@include("manage::widgets.grid" , [
		'headings'          => $headings,
		'row_view'          => 'deployer::deploy.row',
		'table_id'          => 'deploys-list',
	])
@endsection
