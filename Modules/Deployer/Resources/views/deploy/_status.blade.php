@switch ($model->status)
	@case (0)  {{-- is on pending (just deploy) --}}
	@case (-1) {{-- is on pending (demo release) --}}
	@case (-2) {{-- is on pending (final release) --}}
	<span class="text-orange">
		<i class="fa fa-clock-o" aria-hidden="true"></i>
		{{ trans("deployer::project.pending") }}
	</span>
	@break
	@case (1) {{-- is on progress --}}
	<span class="text-green">
		<i class="fa fa-tasks" aria-hidden="true"></i>
		{{ trans("deployer::project.in_progress") }}
	</span>
	@break
	@case (2) {{-- has been complete --}}
	<span class="text-green">
		<i class="fa fa-check-circle" aria-hidden="true"></i>
		{{ trans("deployer::project.complete") }}
	</span>
	@break
	@case (3) {{-- has been failed --}}
	<span class="text-red">
		<i class="fa fa-times-circle" aria-hidden="true"></i>
		{{ trans("deployer::project.failed") }}
	</span>
	@break
	@case (4) {{-- has been cancelled --}}
	<span class="text-warning">
		<i class="fa fa-ban" aria-hidden="true"></i>
		{{ trans("deployer::project.cancelled") }}
	</span>
	@break
	@default {{-- unknokwn status --}}
	<span>
		<i class="fa fa-question-circle" aria-hidden="true"></i>
		{{ trans("deployer::project.unknown") }}
	</span>
@endswitch
