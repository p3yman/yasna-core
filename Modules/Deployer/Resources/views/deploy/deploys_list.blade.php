{{
	widget('modal')
		->label($title_label)
}}

<div class="modal-body">
	@include('deployer::deploy._deploy_grid')
</div>

<div class="modal-footer">
	@include("manage::widgets.toolbar" , [
		'title'             => '',
		'buttons'           => [
			[
				'target'  => modal_route("deployer.deploy.deploy_details", ['hashid' => $model->hashid], false),
				'caption' => trans('deployer::project.refresh'),
				'icon'    => "refresh",
				'type'    => "info",
			],
			[
				'target'  => "modal:" . route("deployer.project.deploys_list", ['hashid' => $project->hashid], false),
				'caption' => trans('deployer::project.back_to_previous_page'),
				'icon'    => "chevron-circle-right",
				'type'    => "default",
			],
		],
	])
</div>
</div>
