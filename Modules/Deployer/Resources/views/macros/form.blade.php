{!! widget('modal')->target(route('deployer.macros.save'))->label($modal_title) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}


	{!!
		widget('hidden')
			->name('project_id')
			->value(hashid($model->project_id))
	!!}


	{!!
		widget('hidden')
			->name('host_project_id')
			->value(hashid($model->host_project_id))
	!!}


	{!!
		widget('hidden')
			->name('old_macro')
			->value($model->macro)
	!!}

	{!!
		widget('input')
			->name('macro')
			->value($model->macro)
			->label(trans('deployer::macro.macro'))
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		widget('input')
			->name('value')
			->value($model->value)
			->label(trans('deployer::macro.value'))
			->class('form-required')
			->inForm()
			->required()
	!!}

	@include('deployer::macros.default_macros')
</div>
<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		'cancel_link' => master_modal("deployer.macros.index", ['hashid' => $project->hashid], false)
	])
</div>
