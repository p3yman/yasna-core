<div role="alert" class="alert alert-info">
	{{ trans('deployer::macro.global_value_adding_form_desc') }}
	<ul>
		<li>
			<code>PROJECT</code>: {!! trans('deployer::macro.project_macro_adding_form_desc') !!}
		</li>
		<li>
			<code>CURRENT</code>: {!! trans('deployer::macro.current_macro_adding_form_desc') !!}
		</li>
		<li>
			<code>DEMO</code>: {!! trans('deployer::macro.demo_macro_adding_form_desc') !!}
		</li>
		<li>
			<code>SUBSEQUENT</code>: {!! trans('deployer::macro.subsequent_macro_adding_form_desc') !!}
		</li>
	</ul>
</div>
