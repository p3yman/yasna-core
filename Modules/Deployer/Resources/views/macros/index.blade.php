{{widget('modal')->label($title_label)}}
	<div class="modal-body">
		@include("manage::widgets.grid" , [
			'headings' => $headings,
			'row_view' => 'deployer::macros.row',
		])
	</div>
	<div class="modal-footer">
		@include("manage::widgets.toolbar" , [
			'title'             => '',
			'buttons'           => [
				[
					'target'  => master_modal("deployer.macros.action", ['hashid' => $project->hashid, 'action' => 'create'], false),
					'caption' => trans("deployer::macro.create"),
					'icon'    => "plus",
					'type'    => "success",
				],
			],
		])
	</div>
</div>
