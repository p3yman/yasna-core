{!! widget('modal')->target(route('deployer.macros.override'))->label($modal_title) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}


	{!!
		widget('hidden')
			->name('project_id')
			->value(hashid($model->project_id))
	!!}


	{!!
		widget('hidden')
			->name('host_project_id')
			->value($hp_model->hashid)
	!!}

	{!!
		widget('hidden')
			->name('macro')
			->value($model->macro)
	!!}

	{!!
		widget('input')
			->name('macro_disabled')
			->value($model->macro)
			->label(trans('deployer::macro.macro'))
			->disabled()
			->inForm()
	!!}

	{!!
		widget('input')
			->name('value')
			->value($model->value)
			->label(trans('deployer::macro.value'))
			->class('form-required')
			->inForm()
			->required()
	!!}

	@include('manage::forms.buttons-for-modal' , [
     	'cancel_link' => master_modal("deployer.macros.server_index", ['hashid' => $hp_model->hashid], false)
	])
</div>
