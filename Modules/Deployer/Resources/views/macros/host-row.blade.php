<td>
	{{ $model->macro }}
</td>

<td>
	{{ $model->value }}
</td>

<td>
	@include('deployer::macros.is_overridden', ['is_overridden' => $model->overriddenByHost($hp_model->id)])
</td>
@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::macro.override'), modal_route('deployer.macros.override_modal', ['hashid' => $model->hashid, 'host' => $hp_model->hashid])],
		['trash text-red', trans('deployer::macro.remove-override'), modal_route('deployer.macros.override_delete_confirm', ['hashid' => $model->hashid]), $model->overriddenByHost($hp_model->id)],
	],
])
