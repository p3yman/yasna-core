@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.macros.delete_override', ['hashid' => $model->hashid]),
	'modal_title' => trans('deployer::macro.macro_override_delete_confirm', ['project' => $model->project->name, 'host' => $model->host_project->host->name]),
	'title_value' => $model->macro,
	'title_label' => trans('deployer::macro.macro'),
	'save_label' => trans('deployer::macro.macro_override_delete'),
	'cancel_link' => master_modal("deployer.macros.server_index", ['hashid' => hashid($model->host_project_id)], false)
])
