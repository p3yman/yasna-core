@switch($is_overridden)
	@case(true)
		<span class="text-warning">
			{{ trans('deployer::macro.is_overridden') }}
		</span>
	@break
	@case(false)
	@default
		<span>
			{{ trans('deployer::macro.is_not_overridden') }}
		</span>
@endswitch
