@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.macros.delete', ['hashid' => $model->hashid]),
	'modal_title' => trans('deployer::macro.macro_delete_confirm', ['project' => $model->project->name]),
	'title_value' => $model->macro,
	'title_label' => trans('deployer::macro.macro'),
	'save_label' => trans('deployer::macro.macro_delete'),
	'cancel_link' => master_modal("deployer.macros.index", ['hashid' => $model->project->hashid], false)
])
