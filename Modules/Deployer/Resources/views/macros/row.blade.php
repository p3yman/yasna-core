<td>
	{{ $model->macro }}
</td>

<td>
	{{ $model->value }}
</td>

<td>
	@include('deployer::macros.is_overridden', ['is_overridden' => $model->is_host_overridden])
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::macro.edit'), modal_route('deployer.macros.action', ['hashid' => $model->hashid, 'action' => 'edit'])],
		['trash text-red', trans('deployer::macro.delete'), modal_route('deployer.macros.delete_confirm', ['hashid' => $model->hashid])],
	],
])
