@include("manage::layouts.modal-delete" , [
	'form_url' => route('deployer.host.delete_host', ['host_id' => $model->hashid]),
	'modal_title' => trans('deployer::host.confirm_message_to_delete_host', ['name' => $model->name]),
	'title_value' => $model->name,
])
