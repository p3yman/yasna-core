<?php
/* @var array $headings */
?>

@extends('deployer::layouts.master')

@section('content')

	@include("manage::widgets.toolbar" , [
		'title'             => trans('deployer::host.servers'),
		'buttons'           => [
			[
			'target'  => "modal:".route('deployer.host.create'),
			'caption' => trans("deployer::host.create_a_new_server"),
			'icon'    => "plus",
			'type'    => "success",
			'condition' => DeployerModule::canCreateHost()(),
			],
		],
	])

	@include("manage::widgets.grid" , [
		'headings'          => $headings,
		'row_view'          => "deployer::hosts.row",
		'handle' 			=> 'counter',
		'table_id' 			=> "tblHosts"
	])

@endsection
