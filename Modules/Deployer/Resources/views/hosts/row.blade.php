<?php
/* @var \Modules\Deployer\Entities\DeployerHost $model */
?>

@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => route('deployer.host.reload_host', ['hashid' => $model->hashid], false)
])

<td>
	{{ $model->name }}
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->created_at,
		'default' => "fixed",
		'format' => 'j F Y',
		'size' => '14',
	])
</td>

<td>
	{{ $model->last_status }}
</td>


@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::host.edit_host'), 'modal:' . route('deployer.host.edit_host', ['host_id' => $model->hashid], false), $model->canEdit()],
		['trash text-red', trans('deployer::host.delete_host'), 'modal:' . route('deployer.host.delete_confirm_host', ['host_id' => $model->hashid], false), $model->canDelete()],
	],
])
