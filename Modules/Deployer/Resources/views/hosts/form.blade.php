<?php
/* @var \Modules\Deployer\Entities\DeployerHost $model */
/* @var string $titleLabel */
?>

{!! widget('modal')->target(route('deployer.host.save'))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
		widget('input')
			->name('name')
			->value($model->name)
			->label('tr:deployer::host.name')
			->help('tr:deployer::host.enter_a_representative_name_for_the_server')
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		widget('input')
			->name('domain')
			->value($model->domain)
			->label('tr:deployer::host.domain_of_server')
			->help('tr:deployer::host.enter_a_domain_which_point_to_this_server')
			->inForm()
	!!}

	{!!
		widget('input')
			->name('ip')
			->value($model->ip)
			->label('tr:deployer::host.ip_of_server')
			->help('tr:deployer::host.enter_an_ip_which_point_to_this_server')
			->inForm()
	!!}

	{!!
		widget('input')
			->name('port')
			->value($model->port)
			->label('tr:deployer::host.port_of_agent')
			->help('tr:deployer::host.port_of_agent_which_is_listen')
			->class('form-required')
			->inForm()
	!!}

	{!!
		widget('textarea')
			->name('description')
			->value($model->description)
			->label('tr:deployer::host.description')
			->help('tr:deployer::host.enter_a_representative_name_for_the_server')
			->inForm()
	!!}

	@include('manage::forms.buttons-for-modal' , [
	])
</div>
