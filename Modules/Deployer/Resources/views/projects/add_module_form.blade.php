<?php
/* @var string $title_label */
?>

{!! widget('modal')->target(route('deployer.project.save_module', ['hashid' => $version->hashid], false))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget("combo")
			->name('module_id')
			->id('combo_module_list')
			->label("tr:deployer::module.name")
			->searchable()
			->options($project->availableModules($version->id))
			->valueField('id')
			->captionField('name')
			->inForm()
			->on_change('updateVersionBox($(this).val())')
			->required()
			->blankValue('')
	!!}


	{!!
		widget("combo")
			->name('version_id')
			->id('combo_module_versions')
			->label("tr:deployer::project.version")
			->options([])
			->valueField('id')
			->captionField('version')
			->inForm()
			->required()
	!!}

	@include('manage::forms.buttons-for-modal' , [
		'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.module_list", ['hashid' => $version->hashid], false))
	])
</div>

<script type="text/javascript">
	// disable version select box on default initialization
    $('#combo_module_versions').prop('disabled', 'disabled');
    // and disable save btn
    $('#btnSave').prop('disabled', 'disabled');

	function updateVersionBox(hashid) {
	    let versionXHR;

	    if (hashid.length) {
	        versionXHR && versionXHR.abort();
	        versionXHR = $.ajax({
				type: "GET",
				url: "{{$get_version_url}}/" + hashid,
				success: function (options) {
				    // first remove previous results
                    $('#combo_module_versions').find('option').remove();

                    // then create new options and inject them to the select
				    options.forEach(function (v, i) {
				        $('#combo_module_versions').append($('<option>', {value: v.id, "data-content": v.version}));
					});

                    // enable select picker
                    $('#combo_module_versions').prop('disabled', false);

                    // and save button
                    $('#btnSave').prop('disabled', false);

                    // and finally refresh select picker
                    $('#combo_module_versions').selectpicker('refresh');
				}
			});
		} else {
	        // disable save button on selecting blank line
            $('#btnSave').prop('disabled', 'disabled');

            // remove results from version select box
            $('#combo_module_versions').find('option').remove();

            // refresh select picker
            $('#combo_module_versions').selectpicker('refresh');

            // and finally disable it
            $('#combo_module_versions').prop('disabled', 'disabled');
        }
	}
</script>
