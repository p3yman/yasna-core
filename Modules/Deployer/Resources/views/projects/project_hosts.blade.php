{{
	widget('modal')
		->label($title_label)
}}

	<div class="modal-body">
		@include("manage::widgets.grid" , [
			'headings' => $headings,
			'table_id' => 'folan',
			'row_view' => 'deployer::projects.project_hosts_row',
		])
	</div>
	<div class="modal-footer">
		@include("manage::widgets.toolbar" , [
			'title'             => '',
			'buttons'           => [
				[
					'target'  => "modal:" . route("deployer.project.add_host", ['hashid' => $project->hashid], false),
					'caption' => trans("deployer::project.add_a_new_server"),
					'icon'    => "plus",
					'type'    => "success",
				],
			],
		])
	</div>
</div>
