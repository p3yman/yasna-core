<?php
/* @var string $title_label */
?>

{!! widget('modal')->target(route('deployer.project.save_server', ['hashid' => $model->project->hashid], false))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
		widget('hidden')
			->name('project_id')
			->value($model->project->hashid)
	!!}

	{!!
		widget('hidden')
			->name('host_id')
			->value($model->host->hashid)
	!!}

	{!!
		widget("input")
			->name('host_id_text')
			->label("tr:deployer::host.name")
			->value($model->host->name)
			->inForm()
			->disabled()
			->required()
	!!}

	{!!
		widget('textarea')
			->name('deploy_path')
			->label(trans('deployer::project.deploy_path'))
			->value($model->deploy_path)
			->required()
			->inForm()
	!!}

	@include('manage::forms.buttons-for-modal' , [
		'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.server", ['hashid' => $model->project->hashid], false))
	])
</div>
