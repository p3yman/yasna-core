<?php
/* @var string $title_label */
?>

{!! widget('modal')->target(route('deployer.project.save_module', ['hashid' => $model->release->hashid], false))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
		widget('hidden')
			->name('module_id')
			->value($model->module->hashid)
	!!}

	{!!
		widget("combo")
			->name('version_id')
			->id('combo_module_versions')
			->label("tr:deployer::project.version")
			->options($versions)
			->value($model->moduleVersion->hashid)
			->valueField('id')
			->captionField('version')
			->inForm()
			->required()
	!!}

	@include('manage::forms.buttons-for-modal' , [
		'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.module_list", ['hashid' => $model->release->hashid], false))
	])
</div>
