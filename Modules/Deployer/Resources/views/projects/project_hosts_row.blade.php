<?php
/* @var \Modules\Deployer\Entities\DeployerHostProject $model */
?>
@include('manage::widgets.grid-rowHeader', [
	'refresh_url' => $a = route("deployer.project.server_reload", ['hashid' => $model->hashid], false)
])

<td>
	{{ $model->host->name }}
</td>

<td style="direction: ltr">
	{{ $model->deploy_path }}
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::project.edit_host_info'), 'modal:' . route('deployer.project.edit_host_info', ['hashid' => $model->hashid], false)],
		['language', trans('deployer::macro.macros'), modal_route('deployer.macros.server_index', ['hashid' => $model->hashid])],
		['trash text-red', trans('deployer::project.remove_host'), 'modal:' . route('deployer.project.remove_host_confirm', ['hashid' => $model->hashid], false)],
	],
])
