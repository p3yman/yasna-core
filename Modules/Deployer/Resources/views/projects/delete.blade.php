@include("manage::layouts.modal-delete" , [
	'form_url' => route('deployer.project.delete', ['module_id' => $model->hashid]),
	'modal_title' => trans('deployer::project.confirm_message_to_delete_project', ['name' => $model->name]),
	'title_value' => $model->name,
])
