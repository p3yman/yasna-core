<?php
/* @var \Modules\Deployer\Entities\DeployerProject $model */
?>

@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => route('deployer.project.reload_project', ['hashid' => $model->hashid], false)
])


<td>
	{{ $model->name }}
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->created_at,
		'default' => "fixed",
		'format' => 'j F Y',
		'size' => '14',
	])
</td>

<td>
	@if(is_null($model->support_date) or $model->support_date == '0000-00-00 00:00:00')
		{{ trans('deployer::project.project_has_unlimited_support_time') }}
	@else
		@include("manage::widgets.grid-date" , [
			'date' => $model->support_date,
			'default' => "fixed",
			'format' => 'j F Y',
			'size' => '14',
			'color' => $model->support_date_color,
		])
	@endif
</td>

<td>
	{{ pd($model->current_version) }}
	@include("deployer::projects._can_update", [
		'can_be_updated' => $model->currentVersionHasUpdate(),
		'inline' => false,
		'size' => 10,
	])
</td>

<td>
	{{ pd($model->demo_version) }}
	@include("deployer::projects._can_update", [
		'can_be_updated' => $model->demoVersionHasUpdate(),
		'inline' => false,
		'size' => 10,
	])
</td>

<td>
	{{ pd($model->open_version) }}
	@include("deployer::projects._can_update", [
		'can_be_updated' => $model->openVersionHasUpdate(),
		'inline' => false,
		'size' => 10,
	])
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['history', trans('deployer::project.releases'), modal_route('deployer.project.releases', ['hashid' => $model->hashid])],
		['server', trans('deployer::host.servers'), modal_route('deployer.project.server', ['hashid' => $model->hashid])],
		['cubes', trans('deployer::project.deploys'), modal_route('deployer.project.deploys_list', ['hashid' => $model->hashid])],
		['terminal ', trans('deployer::project.commands'), route('deployer.project.command_list', ['hashid' => $model->hashid])],
		['share-square-o ', trans('deployer::project.shared_resources'), modal_route('deployer.project.shared_resources_list', ['hashid' => $model->hashid])],
		['language', trans('deployer::macro.macros'), modal_route('deployer.macros.index', ['hashid' => $model->hashid])],
		['edit', trans('deployer::project.edit_project'), modal_route('deployer.project.edit', ['host_id' => $model->hashid]), $model->canEdit()],
		['trash text-red', trans('deployer::project.delete_project'), modal_route('deployer.project.delete_confirm', ['host_id' => $model->hashid]), $model->canDelete()],
	],
])
