<?php
/* @var string $title_label */
?>

{!! widget('modal')->target(route('deployer.project.save_server', ['hashid' => $project->hashid], false))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('project_id')
			->value($project->hashid)
	!!}

	{!!
		widget("combo")
			->name('host_id')
			->id('combo_host_id')
			->label("tr:deployer::host.name")
			->options($hosts)
			->valueField('id')
			->captionField('host')
			->inForm()
			->required()
	!!}

	{!!
		widget('textarea')
			->name('deploy_path')
			->label(trans('deployer::project.deploy_path'))
			->required()
			->inForm()
	!!}

	@include('manage::forms.buttons-for-modal' , [
		'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.server", ['hashid' => $project->hashid], false))
	])
</div>
