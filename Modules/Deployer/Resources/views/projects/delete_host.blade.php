@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.project.remove_host', ['hashid' => $model->hashid]),
	'modal_title' => trans('deployer::project.confirm_message_to_delete_host', ['name' => $model->host->name, 'project' => $model->project->name]),
	'title_value' => $model->host->name,
	'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.server", ['hashid' => $model->project->hashid], false))
])
