<?php
$inline = $inline ?? true;
$size = $size ?? null;
$class = $size ? "f$size" : null;
?>

@if(!$inline)<div>@endif
@if($can_be_updated)
	<span class="text-warning {{$class}}">
		<i class="fa fa-arrow-circle-o-up" aria-hidden="true"></i>
		{{ trans('deployer::module.update_available') }}
	</span>
@else
	<span class="text-success {{$class}}">
		<i class="fa fa-check-circle" aria-hidden="true"></i>
		{{ trans('deployer::module.is_last_version') }}
	</span>
@endif
@if(!$inline)</div>@endif
