<?php
/* @var string $title_label */
/* @var \Modules\Deployer\Entities\DeployerProject $model */
?>

{!! widget('modal')->target(route('deployer.project.save'))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
		widget('input')
			->name('name')
			->value($model->name)
			->label(trans('deployer::project.name_of_the_project'))
			->help('tr:deployer::project.please_enter_name_of_project')
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		widget('input')
			->name('starter_path')
			->value($model->starter_path)
			->label(trans('deployer::project.starter_path'))
			->help('tr:deployer::project.starter_path_description')
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		widget('input')
			->name('token')
			->value($model->token)
			->label(trans('deployer::project.token_of_the_project'))
			->help('tr:deployer::project.description_enter_token_of_project')
			->inForm()
	!!}

	{!!
		widget('combo')
			->name('status')
			->value($model->status)
			->label(trans('deployer::project.status_of_project'))
			->options(model('deployer_project')->statusTypesCombo())
			->valueField('id')
			->help('tr:deployer::project.select_status_of_project')
			->captionField('caption')
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		   widget('PersianDatePicker')
		   ->name('support_date')
		   ->value($model->support_date != '0000-00-00 00:00:00' ? $model->support_date : '')
		   ->id('support_date')
		   ->class('pdatepickerClass3')
		   ->label(trans('deployer::project.support_date'))
		   ->inForm()
		   ->help('tr:deployer::project.empty_selection_supposed_as_unlimited_support')
		   ->canNotScroll()
		   ->onlyDatePicker()
	!!}

	{!!
		   widget('PersianDatePicker')
		   ->name('update_check_at')
		   ->value($model->update_check_at)
		   ->id('update_check_at')
		   ->class('pdatepickerClass3')
		   ->label(trans('deployer::project.automatic_update_check'))
		   ->inForm()
		   ->help('tr:deployer::project.automatic_update_check_description')
		   ->canNotScroll()
		   ->onlyTimePicker()
	!!}

	{!!
		widget('textarea')
			->name('description')
			->value($model->description)
			->label(trans('deployer::project.description_of_the_project'))
			->help('tr:deployer::project.please_enter_description_of_project')
			->inForm()
	!!}

	@include('manage::forms.buttons-for-modal' , [
	])
</div>
