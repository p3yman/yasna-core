<?php
/* @var array $headings */
?>

@extends('deployer::layouts.master')

@section('content')
	@include("manage::widgets.toolbar" , [
		'title'             => trans('deployer::project.projects'),
		'buttons'           => [
			[
			'target'  => "modal:manage/deployer/project/create",
			'caption' => trans("deployer::project.create_a_new_project"),
			'icon'    => "plus",
			'type'    => "success",
			'condition' => DeployerModule::canCreateProject()(),
			],
		],
	])

	@include("manage::widgets.grid" , [
		'headings'          => $headings,
		'row_view'          => "deployer::projects.row",
		'handle' 			=> 'counter',
		'table_id' 			=> "tblProjects"
	])

@endsection
