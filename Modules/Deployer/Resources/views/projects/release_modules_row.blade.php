<?php
/* @var \Modules\Deployer\Entities\DeployerModuleVersion $model */
?>
@include('manage::widgets.grid-rowHeader', [
	'refresh_url' => route('deployer.project.module_list_reload', ['hashid' => $model->hashid], false)
])

<td>
	{{ $model->module->name }}
</td>

<td>
	{{ pd($model->moduleVersion->version) }}
	@include("deployer::projects._can_update", [
		'can_be_updated' => $model->moduleVersion->isNotLatestVersion(),
		'inline' => false,
		'size' => 10,
	])
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->moduleVersion->created_at,
		'default' => "fixed",
		'format' => 'j F Y',
		'size' => '14',
	])
</td>

@if($model->release->is_an_open_release)
@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::project.edit_module_version'), 'modal:' . route('deployer.project.edit_module_version', ['hashid' => $model->hashid], false)],
		['trash text-red', trans('deployer::project.remove_module'), 'modal:' . route('deployer.project.remove_module_confirm', ['hashid' => $model->hashid], false)],
	],
])
@endif
