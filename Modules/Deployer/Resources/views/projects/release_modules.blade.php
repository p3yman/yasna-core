{{
	widget('modal')
		->label($title_label)
}}

<div class="modal-body">
	@include("manage::widgets.grid" , [
		'headings' => $headings,
		'row_view' => 'deployer::projects.release_modules_row',
	])
</div>

<div class="modal-footer">
	@include("manage::widgets.toolbar" , [
		'title'             => '',
		'buttons'           => [
			[
				'target' => sprintf("masterModal('%s')", route("deployer.project.add_module", ['hashid' => $version->hashid], false)),
				'caption' => trans("deployer::project.add_a_new_module"),
				'icon'    => "plus",
				'type'    => "success",
				'condition' => $version->is_an_open_release,
			],
			[
				'target' => sprintf("masterModal('%s')", route('deployer.project.releases', ['hashid' => $project->hashid], false)),
				'caption' => trans('deployer::project.back_to_previous_page'),
				'icon' => 'chevron-circle-right',
				'type' => 'default',
			],
		],
	])
</div>
</div>
