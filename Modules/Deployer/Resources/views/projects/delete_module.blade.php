@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.project.remove_module', ['hashid' => $model->hashid]),
	'modal_title' => trans('deployer::project.confirm_message_to_delete_module', ['name' => $model->module->name]),
	'title_value' => $model->module->name,
	'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.module_list", ['hashid' => $model->release->hashid], false))
])
