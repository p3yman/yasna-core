@foreach(DeployerModule::getSidebarItems() as $sidebar)
    @include('manage::layouts.sidebar-link' , [
        'icon'          => $sidebar['icon'],
        'caption'       => $sidebar['name'],
        'condition'     => $sidebar['condition'] ?? true,
        'sub_menus'      => $sidebar['sub_menus'],
    ])
@endforeach