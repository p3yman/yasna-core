<?php
$command = $model->command()->withTrashed()->first()
?>

<td>
	{{ $command->command }}
</td>

<td>
	{{ trans(sprintf("deployer::command.%s", $command->type)) }}
</td>

<td>
	@include("deployer::commands._states", ['status' => $model->status])
	<br />
	<small>{{ pd($model->elapsed_time) }}</small>
</td>

<td style="direction: ltr">
	<pre style="background-color: inherit; margin-bottom: 0; padding: 0">
		{!! rtrim($model->result) !!}
	</pre>
</td>
