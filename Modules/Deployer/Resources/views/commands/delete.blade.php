@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.project.command_delete', ['hashid' => $model->hashid]),
	'modal_title' => trans('deployer::command.command_delete_confirm', ['project' => $model->project->name]),
	'title_value' => $model->command,
	'title_label' => trans('deployer::command.command'),
	'save_label' => trans('deployer::command.command_delete'),
	'cancel_link' => sprintf("masterModal('%s')", route("deployer.project.command_list", ['hashid' => $model->project->hashid], false))
])
