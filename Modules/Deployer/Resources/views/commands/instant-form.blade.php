{!! widget('modal')->target(route('deployer.project.command_save'))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}


	{!!
		widget('hidden')
			->name('project_id')
			->value($model->project_id)
	!!}


	{!!
		widget('hidden')
			->name('type')
			->value('type_instant')
	!!}


	{!!
		widget('textarea')
			->name('command')
			->value($model->command)
			->label(trans('deployer::command.command'))
			->class('form-required')
			->inForm()
			->required()
	!!}

	@include('manage::forms.buttons-for-modal' , [
		'label' => trans('deployer::command.run'),
		'shape' => 'inverse',
     	'cancel_link' => master_modal("deployer.project.command_list", ['hashid' => $project->hashid], false)
	])
</div>
