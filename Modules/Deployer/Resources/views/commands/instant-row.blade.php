<td>
	{{ $model->command }}
</td>

<td>
	{{ trans(sprintf("deployer::command.%s", $model->type)) }}
</td>

<td>
	@include('deployer::commands._states', ['status' => $model->state])
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['window-restore', trans('deployer::command.command_result'), modal_route('deployer.project.instant_command_states', ['hashid' => $model->hashid]), $model->isInstant()],
	],
])
