@include("manage::layouts.modal-start" , [
	'form_url' => route('deployer.project.commands_clone', ['hashid' => $project->hashid]),
	'modal_title' => $title,
])
		{!!widget("hidden")
				->name('type')
				->value('clone')
		!!}
	<div class='modal-body'>
		{!!widget("selectize")
			->name('_projects')
			->inForm()
			->options($project->projectsSelectizeList())
			->valueField('id')
			->captionField('title')
			->label("tr:deployer::command.select_destination_projects")
		!!}
	</div>

	<div class="modal-footer">
		@include("manage::forms.buttons-for-modal" )
	</div>
</div>
