{{
	widget('modal')->label($title)
}}
	<div class="modal-body">
		@include("manage::widgets.grid" , [
			'headings' => $headings,
			'row_view' => 'deployer::commands.project_instant_command_row',
		])
	</div>
	<div class="modal-footer">
		@include("manage::widgets.toolbar" , [
			'title'             => '',
			'buttons'           => [
				[
					'target'  => modal_route("deployer.project.instant_command_states", ['hashid' => $model->hashid], false),
					'caption' => trans('deployer::project.refresh'),
					'icon'    => "refresh",
					'type'    => "info",
				],
			],
		])
	</div>
</div>
