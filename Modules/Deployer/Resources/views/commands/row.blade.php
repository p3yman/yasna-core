<td>
	{{ $model->command }}
</td>

<td>
	{{ trans(sprintf("deployer::command.%s", $model->type)) }}
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::command.edit'), modal_route('deployer.project.command_edit', ['hashid' => $model->hashid]), $model->canEdit() and $model->isForDeployment()],
		['trash text-red', trans('deployer::command.command_delete'), modal_route('deployer.project.command_delete_confirm', ['hashid' => $model->hashid]), $model->canDelete() and $model->isForDeployment()],
	],
])
