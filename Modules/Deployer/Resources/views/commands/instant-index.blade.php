@extends('manage::layouts.template')
@section('content')
	@include("deployer::commands.tabs", [
		'current' => route('deployer.project.command_list', ['hashid' => $project->hashid, 'type' => $type], false),
		'tabs' => [
			[
				'caption'     => trans('deployer::command.all_commands'),
				'url'         => route('deployer.project.command_list', ['hashid' => $project->hashid, 'type' => 'el'], false),
			],
			[
				'caption'     => trans('deployer::command.instant_commands'),
				'url'         => route('deployer.project.command_list', ['hashid' => $project->hashid, 'type' => 'i'], false),
			],
		],
	])

	@include("manage::widgets.toolbar" , [
		'title'             => $title_label,
		'buttons'           => [
			[
				'target'  => master_modal("deployer.project.command_create", ['project' => $project->hashid], false),
				'caption' => trans("deployer::command.create"),
				'icon'    => "plus",
				'type'    => "success",
			],
			[
				'target'  => master_modal("deployer.project.command_run", ['project' => $project->hashid], false),
				'caption' => trans("deployer::command.run"),
				'icon'    => "bolt",
				'type'    => "inverse",
			],
			[
				'target'  	=> master_modal("deployer.project.commands_clone_modal", ['hashid' => $project->hashid], false),
				'caption' 	=> trans("deployer::command.clone"),
				'icon'    	=> "clone",
				'type'    	=> "primary",
				'condition' => $project->commands()->exists(),
			],
		],
	])

	@include("manage::widgets.grid" , [
		'headings' => $headings,
		'row_view' => 'deployer::commands.instant-row',
		'table_id' => 'commandsList',
	])
	<div role="alert" class="alert alert-info">
		{!! trans("deployer::command.macro_usage") !!}
	</div>
	@include('deployer::macros.default_macros')
@endsection

