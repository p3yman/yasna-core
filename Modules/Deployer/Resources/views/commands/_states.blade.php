@switch ($status)
	@case (0)  {{-- is pending --}}
	<span class="text-orange">
		<i class="fa fa-clock-o" aria-hidden="true"></i>
		{{ trans("deployer::command.status_pending") }}
	</span>
	@break
	@case (1) {{-- is running --}}
	<span class="text-green">
		<i class="fa fa-tasks" aria-hidden="true"></i>
		{{ trans("deployer::command.status_running") }}
	</span>
	@break
	@case (2) {{-- has been complete --}}
	<span class="text-green">
		<i class="fa fa-check-circle" aria-hidden="true"></i>
		{{ trans("deployer::command.status_successful") }}
	</span>
	@break
	@case (3) {{-- has been failed --}}
	<span class="text-red">
		<i class="fa fa-times-circle" aria-hidden="true"></i>
		{{ trans("deployer::command.status_failed") }}
	</span>
	@break
	@default {{-- unknokwn status --}}
	<span>
		<i class="fa fa-question-circle" aria-hidden="true"></i>
		{{ trans("deployer::command.status_unknown") }}
	</span>
@endswitch
