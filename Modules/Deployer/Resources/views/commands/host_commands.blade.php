{{
	widget('modal')->label($title)
}}
	<div class="modal-body">
		@include("manage::widgets.grid" , [
			'headings' => $headings,
			'row_view' => 'deployer::commands.host_command_row',
		])
	</div>
	<div class="modal-footer">
		@include("manage::widgets.toolbar" , [
			'title'             => '',
			'buttons'           => [
				[
					'target'  => modal_route("deployer.project.command_sates", ['hashid' => $model->hashid], false),
					'caption' => trans('deployer::project.refresh'),
					'icon'    => "refresh",
					'type'    => "info",
				],
				[
					'target'  => master_modal('deployer.deploy.deploy_details', ['hashid' => $deploy->hashid], false),
					'caption' => trans('deployer::project.back_to_previous_page'),
					'icon'    => 'chevron-circle-right',
					'type'    => 'default',
				],
			],
		])
	</div>
</div>
