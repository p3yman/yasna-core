<?php
/* @var \Modules\Deployer\Entities\DeployerModule $model */
/* @var string $titleLabel */
?>

{!! widget('modal')->target(route('deployer.module.save'))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
		widget('input')
			->name('name')
			->label('tr:deployer::module.name')
			->help('tr:deployer::module.please_enter_name_of_module')
			->value($model->name)
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		widget('textarea')
			->name('repository')
			->label('tr:deployer::module.address_of_repository')
			->help('tr:deployer::module.address_repository_on_ssh_or_https_format')
			->value($model->repository)
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		widget('textarea')
			->name('path')
			->label('tr:deployer::module.path_of_module')
			->help('tr:deployer::module.path_of_module_purification')
			->value($model->path)
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		widget('input')
			->name('secret_key')
			->label('tr:deployer::module.secret_key')
			->help('tr:deployer::module.please_enter_secret_key')
			->value($model->secret_key)
			->inForm()
	!!}

	{!!
		widget('textarea')
			->name('description')
			->value($model->description)
			->label('tr:deployer::module.description')
			->help('tr:deployer::module.please_enter_description_of_module')
			->inForm()
	!!}

	@include('manage::forms.buttons-for-modal' , [
	])
</div>
