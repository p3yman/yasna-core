<?php
/* @var array $headings */
?>

@extends('deployer::layouts.master')

@section('content')

	@include("manage::widgets.toolbar" , [
		'title'             => trans('deployer::module.modules'),
		'buttons'           => [
			[
			'target'  => "modal:manage/deployer/module/create",
			'caption' => trans("deployer::module.create_a_new_module"),
			'icon'    => "plus",
			'type'    => "success",
			'condition' => DeployerModule::canCreateModule()(),
			],
		],
	])

	@include("manage::widgets.grid" , [
		'headings'          => $headings,
		'row_view'          => "deployer::modules.row",
		'handle' 			=> 'counter',
		'table_id' 			=> "tblModules"
	])

@endsection
