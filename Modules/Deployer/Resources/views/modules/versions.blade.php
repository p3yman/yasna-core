{{
	widget('modal')
		->label($title_label)
}}

	<div class=“modal-body”>
		@include("manage::widgets.grid" , [
			'headings' => $headings,
			'row_view' => 'deployer::modules.version_row',
		])
	</div>
</div>
