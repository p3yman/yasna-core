<?php
/* @var \Modules\Deployer\Entities\DeployerModuleVersion $model */
?>

<td>
	{{ $model->version }}
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->created_at,
		'default' => "fixed",
		'format' => 'j F Y',
		'size' => '14',
	])
</td>
