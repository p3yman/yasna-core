<?php
/* @var \Modules\Deployer\Entities\DeployerModule $model */
?>

@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => route('deployer.module.reload_module', ['hashid' => $model->hashid], false)
])

<td>
	{{ $model->name }}
</td>

<td>
	@include("manage::widgets.grid-date" , [
		'date' => $model->created_at,
		'default' => "fixed",
		'format' => 'j F Y',
		'size' => '14',
	])
</td>

<td>
	{{ $model->repository }}
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::module.edit_module'), 'modal:' . route('deployer.module.edit', ['module_id' => $model->hashid], false), $model->canEdit()],
		['history', trans('deployer::module.versions'), 'modal:' . route('deployer.module.list_versions', ['hash_id' => $model->hashid], false)],
		['link', trans('deployer::module.hook_link'), 'modal:' . route('deployer.module.hook_link', ['token' => $model->hashid], false)],
		['trash text-red', trans('deployer::module.delete_module'), 'modal:' . route('deployer.module.delete_confirm', ['module_id' => $model->hashid], false), $model->canDelete()],
	],
])
