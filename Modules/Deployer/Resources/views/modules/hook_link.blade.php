{{
	widget('modal')
		->id($model->hashid)
		->label(trans("deployer::module.hook_link_title", ['name' => $model->name]))
}}

<div class=“modal-body”>
	<p style="padding: 15px; text-align: justify">
		{{ trans('deployer::module.hook_link_description') }}
	</p>
	<pre style="text-align:center">{{ route('deployer.module.add_release', ['token' => hashid_deployer($model->id)]) }}</pre>
</div>

</div>
