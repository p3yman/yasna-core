@include("manage::layouts.modal-delete" , [
	'form_url' => route('deployer.module.delete', ['module_id' => $model->hashid]),
	'modal_title' => trans('deployer::module.confirm_message_to_module_project', ['name' => $model->name]),
	'title_value' => $model->name,
])
