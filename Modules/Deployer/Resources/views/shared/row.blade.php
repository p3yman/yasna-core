<td style="direction: ltr">
	{{ $model->path }}
</td>

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['edit', trans('deployer::shared.edit'), modal_route('deployer.project.shared_resources_edit', ['hashid' => $model->hashid]), $model->canEdit()],
		['trash text-red', trans('deployer::shared.delete'), modal_route('deployer.project.shared_resources_delete_confirm', ['hashid' => $model->hashid]), $model->canEdit()],
	],
])
