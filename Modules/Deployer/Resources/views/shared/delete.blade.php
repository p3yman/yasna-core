@include("deployer::layouts.modal-delete" , [
	'form_url' => route('deployer.project.shared_resources_delete', ['hashid' => $model->hashid]),
	'modal_title' => trans('deployer::shared.delete_confirm_title', ['project' => $model->project->name]),
	'title_value' => $model->path,
	'title_label' => trans('deployer::shared.shared_resource'),
	'save_label' => trans('deployer::shared.shared_delete'),
	'cancel_link' => master_modal("deployer.project.shared_resources_list", ['hashid' => $model->project->hashid], false)
])
