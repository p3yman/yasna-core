{!! widget('modal')->target(route('deployer.project.shared_resources_save'))->label($title) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}


	{!!
		widget('hidden')
			->name('project_id')
			->value($model->project_id)
	!!}

	{!!
		widget('textarea')
			->name('path')
			->value($model->path)
			->label(trans('deployer::shared.path'))
			->class('form-required')
			->inForm()
			->ltr()
			->required()
	!!}

	@include('manage::forms.buttons-for-modal' , [
     	'cancel_link' => master_modal("deployer.project.shared_resources_list", ['hashid' => $project->hashid], false)
	])
</div>
