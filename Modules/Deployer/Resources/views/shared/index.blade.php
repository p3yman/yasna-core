{{
	widget('modal')->label($title)
}}
	<div class="modal-body">
		@include("manage::widgets.grid" , [
			'headings' => $headings,
			'row_view' => 'deployer::shared.row',
		])
	</div>
	<div class="modal-footer">
		@include("manage::widgets.toolbar" , [
			'title'             => '',
			'buttons'           => [
				[
					'target'  => master_modal("deployer.project.shared_resources_create", ['hashid' => $project->hashid], false),
					'caption' => trans("deployer::shared.create"),
					'icon'    => "plus",
					'type'    => "success",
				],
			],
		])
	</div>
</div>
