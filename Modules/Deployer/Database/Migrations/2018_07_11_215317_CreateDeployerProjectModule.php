<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerProjectModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ProjectModule schema. This table acts an pivot for project_version and module_version tables.
        Schema::create('deployer_project_module', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('project_version_id');
            $table->foreign('project_version_id')->references('id')->on('deployer_project_version')->onDelete('cascade');
            $table->unsignedInteger('project_id');
            $table->foreign('project_id')->references('id')->on('deployer_projects')->onDelete('cascade');
            $table->unsignedInteger('module_version_id');
            $table->foreign('module_version_id')->references('id')->on('deployer_module_version')->onDelete('cascade');
            $table->unsignedInteger('module_id');
            $table->foreign('module_id')->references('id')->on('deployer_modules')->onDelete('cascade');

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_project_module');
    }
}
