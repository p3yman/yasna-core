<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusColumnToASemanticAlternative extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_project_version', function (Blueprint $table) {
            $table->dropColumn('status');

            $table->timestamp('closed_at')->nullable()->after('version');
            $table->unsignedInteger('closed_by')->nullable()->after('closed_at');

            $table->index('closed_at');
            $table->index('closed_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_project_version', function (Blueprint $table) {
            $table->dropColumn('closed_at');
            $table->dropColumn('closed_by');

            $table->boolean('status')
                  ->defaultValue(false)
                  ->after('version')
                  ->comment('0: version is open, 1: version is closed')
            ;

            $table->index('status');
        });
    }
}
