<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class RenameDeployHostAndHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('deployer_deploy_history', 'deployer_deploy_histories');
        Schema::rename('deployer_deploy_host', 'deployer_deploy_hosts');
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('deployer_deploy_hosts', 'deployer_deploy_host');
        Schema::rename('deployer_deploy_histories', 'deployer_deploy_history');
    }
}
