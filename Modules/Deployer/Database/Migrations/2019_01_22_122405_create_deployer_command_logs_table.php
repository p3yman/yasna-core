<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerCommandLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployer_command_logs', function (Blueprint $table) {
            $table->increments('id');


            $table->unsignedInteger('command_id')->index();
            $table->foreign('command_id')
                  ->references('id')
                  ->on('deployer_commands')
                  ->onUpdate('cascade')
                  ->onDelete('cascade')
            ;

            $table->unsignedInteger('project_id')->index();
            $table->foreign('project_id')
                  ->references('id')
                  ->on('deployer_projects')
                  ->onUpdate('cascade')
                  ->onDelete('cascade')
            ;

            $table->unsignedInteger('deploy_history_id')->index();
            $table->foreign('deploy_history_id')
                  ->references('id')
                  ->on('deployer_deploy_histories')
                  ->onUpdate('cascade')
                  ->onDelete('cascade')
            ;

            $table->unsignedInteger('deploy_host_id');
            $table->foreign('deploy_host_id')
                  ->references('id')
                  ->on('deployer_deploy_hosts')
                  ->onUpdate('cascade')
                  ->onDelete('cascade')
            ;


            $table->unsignedTinyInteger('status')
                  ->nullable()
                  ->defaultValue(0)
                  ->comment('0:pending, 1: running, 2: successful, 3: failed')
            ;


            $table->text('result')->nullable();


            $table->index(['deploy_host_id', 'status']);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_command_logs');
    }
}
