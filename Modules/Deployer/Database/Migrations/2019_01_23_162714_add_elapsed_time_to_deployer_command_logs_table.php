<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddElapsedTimeToDeployerCommandLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_command_logs', function (Blueprint $table) {
            $table->string('elapsed_time')->after('result')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_command_logs', function (Blueprint $table) {
            $table->dropColumn('elapsed_time');
        });
    }
}
