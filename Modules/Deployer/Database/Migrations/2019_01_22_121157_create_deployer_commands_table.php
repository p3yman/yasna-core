<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerCommandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployer_commands', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('project_id');
            $table->foreign('project_id')
                  ->references('id')
                  ->on('deployer_projects')
                  ->onUpdate('cascade')
                  ->onDelete('cascade')
            ;

            $table->text('command');
            $table->unsignedTinyInteger('type')->comment('0: early, 1: later');

            $table->index(['project_id', 'type']);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_commands');
    }
}
