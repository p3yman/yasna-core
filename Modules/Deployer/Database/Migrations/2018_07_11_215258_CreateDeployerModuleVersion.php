<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerModuleVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // ModuleVersion schema
        Schema::create('deployer_module_version', function (Blueprint $table) {
            $table->increments('id');

            $table->string('version', 10);

            $table->unsignedInteger('module_id');
            $table->foreign('module_id')->references('id')->on('deployer_modules')->onDelete('cascade');

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->index('version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_module_version');
    }
}
