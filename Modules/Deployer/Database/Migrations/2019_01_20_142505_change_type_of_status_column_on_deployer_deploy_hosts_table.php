<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeOfStatusColumnOnDeployerDeployHostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_deploy_hosts', function (Blueprint $table) {
            $table->smallInteger('status')->comment('-2,-1,0: pending, 1: in_progress, 2: complete, 3: failed')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_deploy_hosts', function (Blueprint $table) {
            $table->unsignedTinyInteger('status')->comment('0: pending, 1: complete, 2: failed')->change();
        });
    }
}
