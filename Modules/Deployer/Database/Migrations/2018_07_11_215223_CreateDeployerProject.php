<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Project schema
        Schema::create('deployer_projects', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 40);
            $table->text('description')->nullable();
            $table->boolean('status')->defaultValue(true)->comment('0: down, 1: up');
            $table->timestamp('support_date')->nullable()->comment('End date of support. null means unlimited.');

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->index(['name', 'status', 'support_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_projects');
    }
}
