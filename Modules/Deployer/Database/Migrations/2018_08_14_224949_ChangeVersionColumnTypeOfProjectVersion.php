<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\Deployer\Entities\DeployerProjectVersion;

class ChangeVersionColumnTypeOfProjectVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_project_version', function (Blueprint $table) {
            $table->dropIndex('deployer_project_version_version_index');
            $table->unsignedInteger('version')->defaultValue(1)->change();
            $table->index('version');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_project_version', function (Blueprint $table) {
            $table->dropIndex('deployer_project_version_version_index');
            $table->string('version', 10)->change();
            $table->index('version');
        });
    }
}
