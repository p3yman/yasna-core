<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeployerDeployHostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployer_deploy_host', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('deploy_id')->comment('ID of item on deployer_deploy_history table.');
            $table->unsignedTinyInteger('status')->comment('0: pending, 1: complete, 2: failed');
            $table->text('description');

            $table->index('deploy_id');

            $table->foreign('deploy_id')->references('id')->on('deployer_deploy_history')->onDelete('cascade');;

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_deploy_host');
    }
}
