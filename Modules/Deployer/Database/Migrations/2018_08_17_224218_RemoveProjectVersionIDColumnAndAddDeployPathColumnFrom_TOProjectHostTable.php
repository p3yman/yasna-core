<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveProjectVersionIDColumnAndAddDeployPathColumnFromTOProjectHostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_host_project', function (Blueprint $table) {
            $table->dropForeign('deployer_host_project_project_version_id_foreign');
            $table->dropColumn('project_version_id');
            $table->text('deploy_path')->after('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_host_project', function (Blueprint $table) {
            $table->dropColumn('deploy_path');
            $table->unsignedInteger('project_version_id')->after('project_id');
            $table->foreign('project_version_id')->references('id')->on('deployer_project_version')->onDelete('cascade');
            $table->index('project_version_id');
        });
    }
}
