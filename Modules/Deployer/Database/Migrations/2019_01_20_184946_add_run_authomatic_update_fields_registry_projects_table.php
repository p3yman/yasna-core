<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRunAuthomaticUpdateFieldsRegistryProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_projects', function (Blueprint $table) {
            $table->time('update_check_at')->nullable()->after('open_version_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_projects', function (Blueprint $table) {
            $table->dropColumn('update_check_at');
        });
    }
}
