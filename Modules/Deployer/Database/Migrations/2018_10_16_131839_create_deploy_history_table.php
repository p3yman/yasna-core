<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeployHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployer_deploy_history', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('project_id');
            $table->unsignedInteger('project_version_id');

            $table->index('project_id');
            $table->index('project_version_id');

            $table->foreign('project_id')->references('id')->on('deployer_projects')->onDelete('cascade');;
            $table->foreign('project_version_id')
                  ->references('id')
                  ->on('deployer_project_version')
                  ->onDelete('cascade')
            ;;

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_deploy_history');
    }
}
