<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Module schema
        Schema::create('deployer_modules', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 40);
            $table->text('description')->nullable();
            $table->text('repository');

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_modules');
    }
}
