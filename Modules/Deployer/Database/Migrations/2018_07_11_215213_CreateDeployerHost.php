<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerHost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Host schema
        Schema::create('deployer_hosts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 40);
            $table->string('domain')->nullable();
            $table->string('ip', 15)->nullable();
            $table->string('port', 4);
            $table->text('description')->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_hosts');
    }
}
