<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerMacrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployer_macros', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('project_id');
            $table->foreign('project_id')->references('id')->on('deployer_projects')->onDelete('cascade');
            $table->unsignedInteger('host_project_id')->nullable();
            $table->foreign('host_project_id')->references('id')->on('deployer_host_project')->onDelete('cascade');
            $table->index(['project_id', 'host_project_id']);

            $table->string('macro', 255);
            $table->text('value');

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_macros');
    }
}
