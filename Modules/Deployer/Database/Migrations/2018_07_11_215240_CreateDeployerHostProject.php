<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerHostProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // HostProject schema. This table acts as a pivot table for Host, Project and ProjectVersion.
        Schema::create('deployer_host_project', function (Blueprint $table) {
            $table->increments('id');

            $table->string('token', 40)->comment('An identifier for detecting a specific host-project configuration.');
            $table->unsignedInteger('host_id');
            $table->foreign('host_id')->references('id')->on('deployer_hosts')->onDelete('cascade');
            $table->unsignedInteger('project_id');
            $table->foreign('project_id')->references('id')->on('deployer_projects')->onDelete('cascade');
            $table->unsignedInteger('project_version_id');
            $table->foreign('project_version_id')->references('id')->on('deployer_project_version')->onDelete('cascade');

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->index('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_host_project');
    }
}
