<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeployerSharedResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployer_shared_resources', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('project_id')->index();
            $table->foreign('project_id')
                  ->references('id')
                  ->on('deployer_projects')
                  ->onUpdate('cascade')
                  ->onDelete('cascade')
            ;

            $table->text('path');
            
            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deployer_shared_resources');
    }
}
