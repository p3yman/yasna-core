<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusFieldToProjectVersion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_project_version', function (Blueprint $table) {
            $table->boolean('status')
                  ->defaultValue(false)
                  ->after('version')
                  ->comment('0: version is open, 1: version is closed')
            ;

            $table->index('status');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_project_version', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
