<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreDescriptiveVersionColumnsToDeployerProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_projects', function (Blueprint $table) {
            $table->unsignedInteger('demo_version_id')->after('current_version')->nullable()->comment('The version which is used as demo version.');
            $table->unsignedInteger('open_version_id')->after('demo_version_id')->nullable()->comment('The version which is open for change on modules versions');

            $table->renameColumn('current_version', 'current_version_id')->comment('The version which is used as final version.')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_projects', function (Blueprint $table) {
            $table->dropColumn('demo_version_id');
            $table->dropColumn('open_version_id');

            $table->renameColumn('current_version_id', 'current_version');

        });
    }
}
