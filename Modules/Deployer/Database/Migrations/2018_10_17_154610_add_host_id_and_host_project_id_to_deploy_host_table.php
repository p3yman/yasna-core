<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHostIdAndHostProjectIdToDeployHostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_deploy_hosts', function (Blueprint $table) {
            $table->unsignedInteger('host_id')->after('deploy_id');
            $table->unsignedInteger('host_project_id')->after('host_id');

            $table->foreign('host_id')->references('id')->on('deployer_hosts')->onDelete('cascade');
            $table->foreign('host_project_id')->references('id')->on('deployer_host_project')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_deploy_hosts', function (Blueprint $table) {
            $table->dropColumn('host_id');
            $table->dropColumn('host_project_id');
        });
    }
}
