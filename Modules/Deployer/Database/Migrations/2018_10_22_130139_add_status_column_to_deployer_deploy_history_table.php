<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnToDeployerDeployHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deployer_deploy_histories', function (Blueprint $table) {
            $table->tinyInteger('status')->after('project_version_id');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deployer_deploy_histories', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
