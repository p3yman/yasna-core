<?php

namespace Modules\Deployer\Services;


use Illuminate\Support\Carbon;

/**
 * Class CarbonQuarterHelpersService Each hour (=60 minutes) has four quarter. First from minute 0 to 14, second from
 * 15 to 29, third from 30 to 44 and the last from 45 to 59. This service add some helpers to Carbon which make working
 * with quarters easier.
 *
 * @package Modules\Deployer\Services
 */
class CarbonQuarterHelpersService
{
    /**
     * Register some macros on Carbon in deployer enabled scope
     */
    public static function register()
    {
        static::registerLaterTimeQuarter();
        static::registerSoonerTimeQuarter();
        static::registerTimeQuarterSector();
    }



    /**
     * Add laterTimeQuarter macro to Carbon
     */
    protected static function registerLaterTimeQuarter()
    {
        /**
         * laterTimeQuarter set the minute to next quarter number. For example if the time is 14:23, the later quarter
         * will be 14:30.
         *
         * @return Carbon
         */
        Carbon::macro('laterTimeQuarter', function () {
            $m = ($this->timeQuarterSector() + 1) * 15;

            return $this->minute($m)->second(0);
        });
    }



    /**
     * Add soonerTimeQuarter macro to Carbon
     */
    protected static function registerSoonerTimeQuarter()
    {
        /**
         * soonerTimeQuarter the minute to previous quarter number. For example if the time is 14:23, the sooner quarter
         * will be 14:15.
         *
         * @return Carbon
         */
        Carbon::macro('soonerTimeQuarter', function () {
            $m = ($this->timeQuarterSector()) * 15;

            return $this->minute($m)->second(0);
        });
    }



    /**
     * Add timeQuarterSector macro to Carbon
     */
    protected static function registerTimeQuarterSector()
    {
        /**
         * timeQuarterSector return numeric equivalent of sector, start counting from zero.
         *      0: minute between 0 and 14
         *      1: minute between 15 and 29
         *      2: minute between 30 and 44
         *      3: minute between 45 and 59
         *
         * @return int
         */
        Carbon::macro('timeQuarterSector', function () {
            return intval($this->minute / 15);
        });
    }
}
