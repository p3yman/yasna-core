<?php

namespace Modules\Deployer\Services;

use App\Models\DeployerProject;
use App\Models\DeployerProjectVersion;
use Modules\Deployer\Contracts\DeployHistoryRepository;
use Modules\Deployer\Repositories\EloquentDeployHistoryRepository;

class UpdateProjectToLatestModulesVersionAndDeploy
{
    /**
     * Run automatic update-deploy service
     */
    public static function do()
    {
        $updater  = new static;
        $deployer = new EloquentDeployHistoryRepository();

        $query = model('deployer_project')->with([
             'demoVersion',
             'demoVersion.moduleVersions',
             'demoVersion.modulesInformation',
             'demoVersion.moduleVersions.sisterVersions',
             'openVersion',
             'openVersion.moduleVersions',
             'openVersion.modulesInformation',
             'openVersion.moduleVersions.sisterVersions',
             'currentVersion',
             'currentVersion.moduleVersions',
             'currentVersion.modulesInformation',
             'currentVersion.moduleVersions.sisterVersions',
        ]);


        $query->currentTimeQuarter()
              ->get()
              ->filter(function (DeployerProject $project) { // filter projects which have update
                  return $project->hasUpdate();
              })
              ->map(function (DeployerProject $project) use ($updater, $deployer) {
                  $version_for_deploy = $updater->setOpenVersionAsDeployVersion($project);
                  if ($version_for_deploy->modulesMassUpdate() and $deployer->saveRawHistory($version_for_deploy, DeployHistoryRepository::TYPE_FINAL)) {
                      return $updater->draftNewOpenVersion($project, $version_for_deploy);
                  }

                  return false;
              })
        ;
    }



    /**
     * Close open version of the project, create a new open version and return old open version
     *
     * @param DeployerProject $project
     *
     * @return DeployerProjectVersion|null
     */
    public function setOpenVersionAsDeployVersion(DeployerProject $project): ?DeployerProjectVersion
    {
        /* @var DeployerProjectVersion $open_version */
        $open_version = $project->open_version;
        if ($open_version->closeRelease()) {
            return $open_version;
        }

        return null;
    }



    /**
     * Replicate modules of previous open version to new one
     *
     * @param DeployerProject        $project
     * @param DeployerProjectVersion $old_open_version
     */
    public function draftNewOpenVersion(DeployerProject $project, DeployerProjectVersion $old_open_version)
    {
        if (($new_version = $project->attachNewVersion(true)) !== null) {
            $old_open_version->replicateModules($new_version);
        }
    }
}
