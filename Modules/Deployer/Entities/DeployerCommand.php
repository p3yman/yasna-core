<?php

namespace Modules\Deployer\Entities;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Providers\DeployerServiceProvider as DeployerModule;

use function is_int;

class DeployerCommand extends YasnaModel
{
    use SoftDeletes;

    const TYPE_EARLY   = 0;
    const TYPE_LATER   = 1;
    const TYPE_INSTANT = 2;



    /**
     * return alphabetic equivalent of type
     *
     * @param $type
     *
     * @return string
     */
    public static function alphabeticType($type): string
    {
        switch ($type) {
            case static::TYPE_EARLY:
                return 'type_early';
            case static::TYPE_LATER:
                return 'type_later';
            case static::TYPE_INSTANT:
                return 'type_instant';
            default:
                return 'type_unknown';
        }
    }



    /**
     * convert alphabetic type to numeric
     *
     * @param string $type
     *
     * @return int
     */
    public static function numericType($type): int
    {
        switch ($type) {
            case 'type_early':
                return static::TYPE_EARLY;
            case 'type_later':
                return static::TYPE_LATER;
            case 'type_instant':
                return static::TYPE_INSTANT;
            default:
                return 10;
        }
    }



    /**
     * Indicate user can edit this command or not
     *
     * @return bool
     */
    public function canEdit()
    {
        return DeployerModule::canDeployRelease()();
    }



    /**
     * Indicate user can delete this command or not
     *
     * @return bool
     */
    public function canDelete()
    {
        return DeployerModule::canDeployRelease()();
    }



    /**
     * determine current model has a type
     *
     * @param string|int $type
     *
     * @return bool
     */
    public function isTypeOf($type): bool
    {
        if (is_int($type)) {
            $type = static::alphabeticType($type);
        }

        return $this->type === $type;
    }



    /**
     * determine current model is an early command
     *
     * @return bool
     */
    public function isEarly(): bool
    {
        return $this->isTypeOf(static::TYPE_EARLY);
    }



    /**
     * determine current model is a later command
     *
     * @return bool
     */
    public function isLater(): bool
    {
        return $this->isTypeOf(static::TYPE_LATER);
    }



    /**
     * determine current model is a deployment command
     *
     * @return bool
     */
    public function isForDeployment(): bool
    {
        return $this->isEarly() or $this->isLater();
    }



    /**
     * determine current model is an instant command
     *
     * @return bool
     */
    public function isInstant(): bool
    {
        return $this->isTypeOf(static::TYPE_INSTANT);
    }



    /**
     * command logs
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerCommandLog', 'command_id');
    }



    /**
     * project owner of command
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerProject', 'project_id');
    }



    /**
     * return the state of command running
     *
     * @return int
     */
    public function getStateAttribute()
    {
        return $this->logs->min('status');
    }



    /**
     * filter async commands (commands which will be run just on deploy)
     *
     * @param Builder $query
     */
    public function scopeFilterDeploymentTypes($query)
    {
        $this->scopeFilterTypes($query, [self::TYPE_EARLY, self::TYPE_LATER]);
    }



    /**
     * filter instant commands (commands which will be run once on a demand)
     *
     * @param Builder $query
     */
    public function scopeFilterInstantTypes($query)
    {
        $query->orderBy('created_at', 'desc');
        $this->scopeFilterTypes($query, [self::TYPE_INSTANT]);
    }



    /**
     * filter commands based on types
     *
     * @param Builder $query
     * @param array   $types
     */
    public function scopeFilterTypes($query, array $types)
    {
        $query->whereIn('type', $types);
    }



    /**
     * `type` accessor
     *
     * @return string
     */
    public function getTypeAttribute($type): string
    {
        return static::alphabeticType($type);
    }



    /**
     * `type` mutator
     *
     * @param string $type
     */
    public function setTypeAttribute($type)
    {
        $this->attributes['type'] = static::numericType($type);
    }



    /**
     * combo ready to use array
     *
     * @return array
     */
    public function typesCombo(): array
    {
        return [
             [
                  'type'    => 'type_early',
                  'caption' => trans('deployer::command.type_early'),
             ],
             [
                  'type'    => 'type_later',
                  'caption' => trans('deployer::command.type_later'),
             ],
        ];
    }
}
