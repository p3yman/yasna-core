<?php

namespace Modules\Deployer\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeployerHostProject extends YasnaModel
{
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $table = 'deployer_host_project';

    /**
     * @inheritdoc
     */
    protected $guarded = ["id"];



    /**
     * Return host information of current project attached
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function host()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerHost', 'host_id');
    }



    /**
     * Return project information of current attached host
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerProject', 'project_id');
    }
}
