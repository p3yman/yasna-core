<?php

namespace Modules\Deployer\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeployerProjectModule extends YasnaModel
{
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $table = 'deployer_project_module';

    /**
     * @inheritdoc
     */
    protected $guarded = ["id"];



    /**
     * Return related module
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Modules\Yasna\Services\ModuleHelper
     */
    public function module()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerModule', 'module_id');
    }



    /**
     * version of attached module
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Modules\Yasna\Services\ModuleHelper
     */
    public function moduleVersion()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerModuleVersion', 'module_version_id');
    }



    /**
     * `module_version` accessor for moduleVersion relation
     *
     * @return DeployerModuleVersion
     */
    public function getModuleVersionAttribute()
    {
        if (!$this->relationLoaded('moduleVersion')) {
            $this->load('moduleVersion');
        }

        return $this->getRelation('moduleVersion');
    }



    /**
     * Return related release
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function release()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerProjectVersion', 'project_version_id');
    }



    /**
     * Upgrade the module version the latest released version of module
     *
     * @param bool $save
     *
     * @return bool|null
     */
    public function updateModuleToLatestVersion(bool $save = false): ?bool
    {
        $latest_version = $this->module_version->getLatest();
        if (!$latest_version) {
            return null;
        }

        $this->module_version_id = $latest_version->id;

        if ($save) {
            return $this->save();
        }

        return null;
    }
}
