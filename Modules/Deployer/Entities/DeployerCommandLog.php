<?php

namespace Modules\Deployer\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeployerCommandLog extends YasnaModel
{
    use SoftDeletes;

    const STATUS_PENDING    = 0;
    const STATUS_RUNNING    = 1;
    const STATUS_SUCCESSFUL = 2;
    const STATUS_FAILED     = 3;



    /**
     * command relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function command()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerCommand', 'command_id');
    }



    /**
     * deploy host relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deployHost()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerDeployHost', 'deploy_host_id');
    }



    /**
     * `deploy_host` accessor for deployHost relation
     *
     * @return \App\Models\DeployerDeployHost
     */
    public function getDeployHostAttribute()
    {
        if (!$this->relationLoaded('deployHost')) {
            $this->load('deployHost');
        }

        return $this->getRelation('deployHost');
    }
}
