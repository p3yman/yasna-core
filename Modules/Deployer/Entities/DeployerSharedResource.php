<?php

namespace Modules\Deployer\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Providers\DeployerServiceProvider as DeployerModule;

class DeployerSharedResource extends YasnaModel
{
    use SoftDeletes;



    /**
     * Indicate user can edit the model
     *
     * @return bool
     */
    public function canEdit()
    {
        return DeployerModule::canEditProject()();
    }



    /**
     * a belongs to relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerProject', 'project_id');
    }
}
