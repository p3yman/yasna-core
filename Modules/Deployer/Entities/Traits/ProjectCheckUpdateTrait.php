<?php

namespace Modules\Deployer\Entities\Traits;


trait ProjectCheckUpdateTrait
{
    /**
     * Shorthand for `currenVersionHasUpdate` method.
     *
     * @return bool
     */
    public function hasUpdate(): bool
    {
        return $this->currentVersionHasUpdate();
    }



    /**
     * Check the final version of the project (the one which is deployed) can be updated or not
     *
     * @return bool
     */
    public function currentVersionHasUpdate(): bool
    {
        if (!$this->current_version) {
            return false;
        }

        return $this->current_version->hasUpdate();
    }



    /**
     * Check the demo version of the project can be updated or not
     *
     * @return bool
     */
    public function demoVersionHasUpdate(): bool
    {
        if (!$this->demo_version) {
            return false;
        }

        return $this->demo_version->hasUpdate();
    }



    /**
     * Check the open version of the project (the version which can modified) can be updated or not
     *
     * @return bool
     */
    public function openVersionHasUpdate(): bool
    {
        if (!$this->open_version) {
            return false;
        }

        return $this->open_version->hasUpdate();
    }
}
