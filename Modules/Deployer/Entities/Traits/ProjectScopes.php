<?php

namespace Modules\Deployer\Entities\Traits;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

trait ProjectScopes
{
    /**
     * Restrct search on current time quarter
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeCurrentTimeQuarter($query)
    {
        $from = Carbon::now()->soonerTimeQuarter();
        $to   = Carbon::now()->laterTimeQuarter();

        return $query->whereNotNull('update_check_at')
                     ->where('update_check_at', '>=', $from->format("H:i:s"))
                     ->where('update_check_at', '<', $to->format("H:i:s"))
             ;
    }
}
