<?php

namespace Modules\Deployer\Entities\Traits;


use App\Models\DeployerModuleVersion;
use Illuminate\Database\Eloquent\Collection;

trait ProjectVersionCheckUpdateTrait
{
    /**
     * Check this version of the project can be updated or not
     *
     * @return bool
     */
    public function hasUpdate(): bool
    {
        return $this->updatableModules()->isNotEmpty();
    }



    /**
     * Return a list of updatable modules of version
     *
     * @return Collection
     */
    public function updatableModules(): Collection
    {
        return $this->module_versions
             ->filter(function (DeployerModuleVersion $module_version) {
                 return $module_version->isNotLatestVersion();
             });
    }
}
