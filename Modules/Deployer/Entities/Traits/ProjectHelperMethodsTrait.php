<?php

namespace Modules\Deployer\Entities\Traits;

use App\Models\DeployerProjectVersion;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

trait ProjectHelperMethodsTrait
{
    /**
     * Gets a selectize-suitable list of all projects
     *
     * @return array
     */
    public function projectsSelectizeList(): array
    {
        return static::where('id', '<>', $this->id)
                     ->get()
                     ->map(function ($project) {
                         return [
                              'id'    => $project->hashid,
                              'title' => $project->name,
                         ];
                     })
                     ->toArray()
             ;
    }



    /**
     * Define `support_date_color` property which reflects status of project's support
     *
     * @return string
     */
    public function getSupportDateColorAttribute()
    {
        $now         = Carbon::now();
        $supportDate = Carbon::parse($this->support_date);
        $diff        = $now->diffInDays($supportDate, false);

        if ($diff < 0) {
            return 'gray';
        } elseif ($diff < 10) {
            return 'red';
        } elseif ($diff < 30) {
            return 'warning';
        }

        return 'green';
    }



    /**
     * Indicate the project has an open version or not
     *
     * @return bool
     */
    public function hasOpenVersion()
    {
        return isset($this->currentVersion) && $this->currentVersion->status == static::VERSION_OPEN;
    }



    /**
     * Indicate the project has not an open version or has
     *
     * @return bool
     */
    public function hasNotOpenVersion()
    {
        return !$this->hasOpenVersion();
    }



    /**
     * Generate a new version for project and attach to it
     *
     * @param bool $save Indicate just create an instance of version model or also save it
     *
     * @return DeployerProjectVersion
     */
    public function attachNewVersion($save = false)
    {
        $versionModel = model('deployer-project-version');

        // set relations of project
        $versionModel->project_id = $this->id;

        // if already this project has a version use an increment of it
        if (isset($this->open_version) && $this->open_version->exists) {
            $versionModel->version = $this->open_version->version + 1;
        } else {
            // otherwise it's the first version of project!
            $versionModel->version = 1;
        }

        if ($save && $versionModel->save()) {
            $this->open_version_id = $versionModel->id;
            $this->save();
        }

        return $versionModel;
    }



    /**
     * Return a list of available modules for adding to the project
     *
     * @param int $release_version_id
     *
     * @return Collection|\Illuminate\Support\Collection
     */
    public function availableModules(int $release_version_id = 0)
    {
        $addedModules = [];

        if ($release_version_id !== 0) {
            $addedModules = model('deployer_project_module')->select('module_id')
                                                            ->where('project_version_id', $release_version_id)
                                                            ->get()
            ;
        }

        // TODO: Implement a firm logic for avaiable modules of a project
        /* @var Collection $modules */
        $modules = model('deployer_module')->select(['id', 'name'])
                                           ->whereNotIn('id', $addedModules)
                                           ->get()
        ;

        return $modules->map(
             function ($module) {
                 return [
                      'id'   => $module->hashid,
                      'name' => $module->name,
                 ];
             }
        );
    }



    /**
     * Return a `combo` widget compatible array
     *
     * @return array
     */
    public function statusTypesCombo()
    {
        return [
             [
                  'id'      => static::STATUS_ACTIVE,
                  'caption' => trans('deployer::project.active'),
             ],
             [
                  'id'      => static::STATUS_DEACTIVE,
                  'caption' => trans('deployer::project.deactive'),
             ],
        ];
    }
}
