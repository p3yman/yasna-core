<?php

namespace Modules\Deployer\Entities\Traits;

trait DeployHistoryElectorsTrait
{
    /**
     * filter based on status
     *
     * @param array $array
     */
    public function electorStatus($array)
    {
        $array = (array)$array;

        $this->elector()->whereIn('status', $array);
    }
}
