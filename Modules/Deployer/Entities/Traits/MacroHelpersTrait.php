<?php

namespace Modules\Deployer\Entities\Traits;

trait MacroHelpersTrait
{
    /**
     * Update all children macros of a overridden macro
     *
     * @param string $macro
     * @param string $old_macro
     */
    public function updateOverriddenMacros(string $macro, string $old_macro)
    {
        $current     = $this->macro;
        $this->macro = $old_macro;
        $this->hostOverridden()->update(['macro' => $macro]);
        $this->macro = $current;
    }
}
