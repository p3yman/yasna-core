<?php

namespace Modules\Deployer\Entities\Traits;

use App\Models\DeployerModuleVersion;
use App\Models\DeployerProjectModule;

trait ProjectVersionMassModulesUpdateTrait
{
    /**
     * Update all modules of this version to their latest released version
     *
     * @return bool
     */
    public function modulesMassUpdate(): bool
    {
        return $this->modules_information
             ->filter(function (DeployerProjectModule $project_module) {
                 return $project_module->module_version->isNotLatestVersion();
             })
             ->map(function (DeployerProjectModule $project_module) {
                 return $project_module->updateModuleToLatestVersion(true);
             })
             ->filter()
             ->isNotEmpty()
             ;
    }
}
