<?php

namespace Modules\Deployer\Entities\Traits;

use App\Models\DeployerProjectVersion;
use App\Models\DeployerSharedResource;

trait ProjectRelationsTrait
{
    /**
     * Return all hosts information which project will deploy on them
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hosts()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerHostProject', 'project_id');
    }



    /**
     * Return all versions of project
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function versions()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerProjectVersion', 'project_id');
    }



    /**
     * Return current version information of project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currentVersion()
    {
        return $this->BelongsTo(MODELS_NAMESPACE . 'DeployerProjectVersion', 'current_version_id');
    }



    /**
     * Accessor for currentVersion relation
     *
     * @return DeployerProjectVersion
     */
    public function getCurrentVersionAttribute()
    {
        if (!$this->relationLoaded('currentVersion')) {
            $this->load('currentVersion');
        }

        return $this->getRelation('currentVersion');
    }



    /**
     * Return demo version information of project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function demoVersion()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerProjectVersion', 'demo_version_id');
    }



    /**
     * Accessor for demoVersion relation
     *
     * @return DeployerProjectVersion
     */
    public function getDemoVersionAttribute()
    {
        if (!$this->relationLoaded('demoVersion')) {
            $this->load('demoVersion');
        }

        return $this->getRelation('demoVersion');
    }



    /**
     * Return open version information of project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function openVersion()
    {
        return $this->BelongsTo(MODELS_NAMESPACE . 'DeployerProjectVersion', 'open_version_id');
    }



    /**
     * Accessor for openVersion relation
     *
     * @return DeployerProjectVersion
     */
    public function getOpenVersionAttribute()
    {
        if (!$this->relationLoaded('openVersion')) {
            $this->load('openVersion');
        }

        return $this->getRelation('openVersion');
    }



    /**
     * Return last version information of project
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastVersion()
    {
        return $this->hasOne(MODELS_NAMESPACE . 'DeployerProjectVersion', 'project_id')
                    ->orderBy('id', 'desc')
                    ->limit(1)
             ;
    }



    /**
     * Return all modules of project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function modules()
    {
        return $this->belongsToMany(
             MODELS_NAMESPACE . 'DeployerModule',
             'deployer_project_module',
             'module_id',
             'project_id'
        )
                    ->distinct()
             ;
    }



    /**
     * return all history records of deployment process of a project version
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerDeployHistory', 'project_id')
                    ->orderBy('updated_at', 'desc')
             ;
    }



    /**
     * commands of project
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commands()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerCommand', 'project_id');
    }



    /**
     * a has many relation to shared resources
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sharedResources()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerSharedResource', 'project_id');
    }



    /**
     * Accessor for sharedResources relation
     *
     * @return DeployerSharedResource[]
     */
    public function getSharedResourcesAttribute()
    {
        if (!$this->relationLoaded('sharedResources')) {
            $this->load('sharedResources');
        }

        return $this->getRelation('sharedResources');
    }
}
