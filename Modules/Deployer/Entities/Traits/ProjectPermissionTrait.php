<?php

namespace Modules\Deployer\Entities\Traits;

use Modules\Deployer\Providers\DeployerServiceProvider as DeployerModule;

trait ProjectPermissionTrait
{
    /**
     * Indicate user can edit this project or not
     *
     * @return bool
     */
    public function canEdit()
    {
        return DeployerModule::canEditProject()();
    }



    /**
     * Indicate user can delete this project or not
     *
     * @return bool
     */
    public function canDelete()
    {
        return DeployerModule::canDeleteProject()();
    }
}
