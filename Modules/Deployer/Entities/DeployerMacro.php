<?php

namespace Modules\Deployer\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Entities\Traits\MacroHelpersTrait;

class DeployerMacro extends YasnaModel
{
    use SoftDeletes;
    use MacroHelpersTrait;



    /**
     * project of macro
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerProject', 'project_id');
    }



    /**
     * host project of macro
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hostProject()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerHostProject', 'host_project_id');
    }



    /**
     * `host_project` accessor for `hostProject` relation
     *
     * @return \App\Models\DeployerHostProject
     */
    public function getHostProjectAttribute()
    {
        if (!$this->relationLoaded('hostProject')) {
            $this->load('hostProject');
        }

        return $this->getRelation('hostProject');
    }



    /**
     * all overridden versions of a macro for a project
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overridden()
    {
        return $this->hasMany(static::class, 'project_id', 'project_id');
    }



    /**
     * return all host overridden macro of the current macro
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hostOverridden()
    {
        return $this->overridden()
                    ->whereMacro($this->macro)
                    ->whereNotNull('host_project_id')
             ;
    }



    /**
     * check the macro is overriden by a given host ot not
     *
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function overriddenByHost(int $id)
    {
        return $this->hostOverridden()
                    ->whereHostProjectId($id)
                    ->exists()
             ;
    }



    /**
     * `is_host_overridden` accessor, indicate this value is overridden or not
     *
     * @return bool
     */
    public function getIsHostOverriddenAttribute(): bool
    {
        return $this->hostOverridden()->exists();
    }
}
