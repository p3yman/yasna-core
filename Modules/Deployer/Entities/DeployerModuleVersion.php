<?php

namespace Modules\Deployer\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeployerModuleVersion extends YasnaModel
{
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $table = 'deployer_module_version';

    /**
     * @inheritdoc
     */
    protected $guarded = ["id"];



    /**
     * Return module of current version
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Modules\Yasna\Services\ModuleHelper
     */
    public function module()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerModule', 'module_id');
    }



    /**
     * HasMany to all versions related to a module
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sisterVersions()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerModuleVersion', 'module_id', 'module_id');
    }



    /**
     * Ensure this version, is not the last version of module
     *
     * @return bool
     */
    public function isNotLatestVersion(): bool
    {
        return $this->sisterVersions()
                    ->where('version', '>', $this->version)
                    ->exists()
             ;
    }



    /**
     * Ensure this version, is the last version of module
     *
     * @return bool
     */
    public function isLatestVersion(): bool
    {
        return !$this->isNotLatestVersion();
    }



    /**
     * Return the latest module version related to this version (same module)
     *
     * @return DeployerModuleVersion|null
     */
    public function getLatest(): ?DeployerModuleVersion
    {
        return $this->sisterVersions()
                    ->where('version', '>', $this->version)
                    ->orderBy('version', 'desc')
                    ->first()
             ;
    }
}
