<?php

namespace Modules\Deployer\Entities;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Entities\Traits\ProjectVersionCheckUpdateTrait;
use Modules\Deployer\Entities\Traits\ProjectVersionMassModulesUpdateTrait;
use Modules\Deployer\Http\Requests\ProjectModuleRequest;
use Modules\Yasna\Services\YasnaModel;

class DeployerProjectVersion extends YasnaModel
{
    use SoftDeletes;
    use ProjectVersionCheckUpdateTrait;
    use ProjectVersionMassModulesUpdateTrait;

    /**
     * @inheritdoc
     */
    protected $table = 'deployer_project_version';

    /**
     * @inheritdoc
     */
    protected $guarded = ["id"];



    /**
     * Prepare a string representation of class
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->version;
    }



    /**
     * Close current version
     *
     * @return bool
     */
    public function closeRelease()
    {
        $this->closed_at = date('c');
        $this->closed_by = user()->id;

        if ($this->save()) {
            return true;
        }

        return false;
    }



    /**
     * Replicate all modules of this release to the new release
     *
     * @param DeployerProjectVersion $project_version
     */
    public function replicateModules(DeployerProjectVersion $project_version)
    {
        foreach ($this->modulesInformation()->get() as $module_information) {
            $new_module_information                     = $module_information->replicate();
            $new_module_information->project_version_id = $project_version->id;

            $new_module_information->save();
        }
    }



    /**
     * Return project of this release
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerProject', 'project_id');
    }



    /**
     * Return module information of this release
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function modulesInformation()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerProjectModule', 'project_version_id');
    }



    /**
     * `modules_information` accessor for `modulesInformation` relation
     *
     * @return DeployerProjectModule
     */
    public function getModulesInformationAttribute()
    {
        if (!$this->relationLoaded('modulesInformation')) {
            $this->load('modulesInformation');
        }

        return $this->getRelation('modulesInformation');
    }



    /**
     * Return attached module versions to this release
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function moduleVersions()
    {
        return $this->hasManyThrough(
             MODELS_NAMESPACE . 'DeployerModuleVersion',
             MODELS_NAMESPACE . 'DeployerProjectModule',
             'project_version_id',
             'id',
             'id',
             'module_version_id'
        );
    }



    /**
     * Define `module_versions` accessor for `moduleVersions` relation
     *
     * @return Collection
     */
    public function getModuleVersionsAttribute()
    {
        if (!$this->relationLoaded('moduleVersions')) {
            $this->load('moduleVersions');
        }

        return $this->getRelation('moduleVersions');
    }



    /**
     * return history of deployments of a project version
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(DeployerDeployHistory::class, 'project_version_id');
    }



    /**
     * Add a module to current release
     *
     * @param ProjectModuleRequest $request
     *
     * @return YasnaModel
     */
    public function addModuleToVersion(ProjectModuleRequest $request)
    {
        $moduleVersion = $request->moduleVersion();

        $model = model('deployer_project_module', $request->hashid);

        $model->project_version_id = $this->id;
        $model->project_id         = $this->project->id;
        $model->module_version_id  = $moduleVersion->id;
        $model->module_id          = $moduleVersion->module->id;

        $model->save();

        return $model;
    }



    /**
     * Make a `is_an_open_release` attribute which indicate either project is open or not
     *
     * @return bool
     */
    public function getIsAnOpenReleaseAttribute()
    {
        return $this->closed_at === null;
    }



    /**
     * Make a `is_not_an_open_release` attribute which indicate either project is open or not
     *
     * @return bool
     */
    public function getIsNotAnOpenReleaseAttribute()
    {
        return !$this->is_an_open_release;
    }



    /**
     * Make a `is_a_closed_release` attribute which indicate either project is open or not
     *
     * @return bool
     */
    public function getIsAClosedReleaseAttribute()
    {
        return $this->is_not_an_open_release;
    }



    /**
     * Make a `is_not_a_closed_release` attribute which indicate either project is open or not
     *
     * @return bool
     */
    public function getIsNotAClosedReleaseAttribute()
    {
        return $this->is_an_open_release;
    }



    /**
     * `is_selected_release` accessor, indicate the version is used on production or not
     *
     * @return bool
     */
    public function getIsSelectedReleaseAttribute()
    {
        $project_versions = [
             $this->project->current_version_id,
             $this->project->demo_version_id,
        ];

        return in_array($this->id, $project_versions, true);
    }



    /**
     * `is_not_selected_release` accessor, indicate the version is used on production or not
     *
     * @return bool
     */
    public function getIsNotSelectedReleaseAttribute()
    {
        return !$this->getIsSelectedReleaseAttribute();
    }
}
