<?php

namespace Modules\Deployer\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Entities\Traits\DeployHistoryElectorsTrait;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class DeployerDeployHistory
 * A history of a deployment, is the pack of all information related to the status of deployment of a
 * version of a project on attached hosts.
 *
 * @package Modules\Deployer\Entities
 */
class DeployerDeployHistory extends YasnaModel
{
    use SoftDeletes;
    use DeployHistoryElectorsTrait;

    const STATUS_PENDING_INSTANT_CMD = -8;
    const STATUS_PENDING_DELETE      = -4;
    const STATUS_PENDING_FINAL       = -2;
    const STATUS_PENDING_DEMO        = -1;
    const STATUS_PENDING_JUST_DEPLOY = 0;
    const STATUS_IN_PROGRESS         = 1;
    const STATUS_COMPLETE            = 2;
    const STATUS_FAILED              = 3;
    const STATUS_CANCELED            = 4;



    /**
     * Define an accessor for overall status of a deployment
     *
     * @param int $value
     *
     * @return int status of deployment
     */
    public function getStatusAttribute($value)
    {
        if ($value == static::STATUS_CANCELED) {
            return static::STATUS_CANCELED;
        } elseif ($this->isAPendingStatus($value)) {
            return static::STATUS_PENDING_JUST_DEPLOY;
        }

        $hosts_status = $this->hosts;
        $status       = static::STATUS_COMPLETE;

        // We consider a deployment process as:
        //      failed, if at least one deployment gave failed result
        //      pending, if at least there was one pending deployment process
        //      complete, if all hosts deployment were successfully done
        foreach ($hosts_status as $hs) {
            if ($hs->status == static::STATUS_FAILED) {
                return static::STATUS_FAILED;
            } elseif ($this->isAPendingStatus($hs->status)) {
                $status = static::STATUS_PENDING_JUST_DEPLOY;
            }
        }

        return $status;
    }



    /**
     * return all hosts information of a deploy history. A host information, is information of deploying
     * a version of a project, on a specified host.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hosts()
    {
        return $this->hasMany(DeployerDeployHost::class, 'deploy_id')
                    ->orderBy('updated_at', 'desc')
             ;
    }



    /**
     * return related release of the deployment record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function release()
    {
        return $this->belongsTo(DeployerProjectVersion::class, 'project_version_id');
    }



    /**
     * Return the project which is related to current history
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(DeployerProject::class, 'project_id');
    }



    /**
     * `is_pending_status` accessor
     *
     * @return bool
     */
    public function getIsPendingStatusAttribute(): bool
    {
        return $this->isAPendingStatus($this->status);
    }



    /**
     * Check the status is a pending status or not
     *
     * @param int $status
     *
     * @return bool
     */
    private function isAPendingStatus(int $status): bool
    {
        return in_array(
             $status,
             [
                  static::STATUS_PENDING_JUST_DEPLOY,
                  static::STATUS_PENDING_DEMO,
                  static::STATUS_PENDING_FINAL,
                  static::STATUS_PENDING_DELETE,
             ]
        );
    }
}
