<?php

namespace Modules\Deployer\Entities;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Providers\DeployerServiceProvider as DeployerModule;

class DeployerHost extends YasnaModel
{
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $guarded = ["id"];



    /**
     * Return a collection of host information
     *
     * @return array
     */
    public static function availableHost()
    {
        // TODO implement a firm logic for available host
        return static::get()->map(
             function ($host) {
                 return [
                      'id'   => $host->hashid,
                      'host' => pd($host->name),
                 ];
             }
        )
             ;
    }



    /**
     * Limit query to a specific user
     *
     * @param Builder $query
     * @param int     $user_id
     *
     * @return void
     */
    public function scopeLimitToUser(Builder $query, $user_id)
    {
        $query->where(['created_by' => $user_id]);
    }



    /**
     * Limit query to current logged-in user
     *
     * @param Builder $query
     *
     * @return void
     */
    public function scopeLimitToCurrentUser(Builder $query)
    {
        $current_user = user();

        $query->limitToUser($current_user->id);
    }



    /**
     * Limit query to all authorized users
     *
     * @param Builder $query
     *
     * @return void
     */
    public function scopeLimitToAuthorizedUsers(Builder $query)
    {
        $query->limitToCurrentUser();
    }



    /**
     * Indicate user can delete the host or not
     *
     * @return bool
     */
    public function canDelete()
    {
        return DeployerModule::canDeleteHost()();
    }



    /**
     * Indicate user can information of the host or not
     *
     * @return bool
     */
    public function canEdit()
    {
        return DeployerModule::canEditHost()();
    }



    /**
     * Return all projects related to this host
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(
             MODELS_NAMESPACE . 'DeployerProject',
             'deployer_host_project',
             'project_id',
             'host_id'
        )
                    ->distinct()
             ;
    }



    /**
     * Return all project versions related to this hosts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function deploys()
    {
        return $this->belongsToMany(
             MODELS_NAMESPACE . 'DeployerProjectVersion',
             'deployer_host_project',
             'project_version_id',
             'host_id'
        )
                    ->distinct()
             ;
    }
}
