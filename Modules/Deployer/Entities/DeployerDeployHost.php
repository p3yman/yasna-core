<?php

namespace Modules\Deployer\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class DeployerDeployHost
 * This class hold information of deployment status of deploying a version of a project, on a specified host.
 *
 * @package Modules\Deployer\Entities
 */
class DeployerDeployHost extends YasnaModel
{
    use SoftDeletes;



    /**
     * return the host that release has been deployed on it
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function host()
    {
        return $this->belongsTo(DeployerHost::class, 'host_id');
    }



    /**
     * deploy category of the current host
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deploy()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'DeployerDeployHistory', 'deploy_id');
    }



    /**
     * commands have been run on host
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commandsLog()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerCommandLog', 'deploy_host_id');
    }



    /**
     * `commands_log` accessor for `commandsLog` relation
     *
     * @return DeployerCommandLog[]
     */
    public function getCommandsLogAttribute()
    {
        if (!$this->relationLoaded('commandsLog')) {
            $this->load('commandsLog');
        }

        return $this->getRelation('commandsLog');
    }
}
