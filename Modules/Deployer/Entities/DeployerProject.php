<?php

namespace Modules\Deployer\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Entities\Traits\ProjectCheckUpdateTrait;
use Modules\Deployer\Entities\Traits\ProjectHelperMethodsTrait;
use Modules\Deployer\Entities\Traits\ProjectPermissionTrait;
use Modules\Deployer\Entities\Traits\ProjectRelationsTrait;
use Modules\Deployer\Entities\Traits\ProjectScopes;
use Modules\Yasna\Services\YasnaModel;

class DeployerProject extends YasnaModel
{
    use SoftDeletes;
    use ProjectScopes;
    use ProjectRelationsTrait;
    use ProjectPermissionTrait;
    use ProjectCheckUpdateTrait;
    use ProjectHelperMethodsTrait;

    const STATUS_ACTIVE   = 1;
    const STATUS_DEACTIVE = 2;
    const VERSION_OPEN    = 0; // In open version nwe still can add module to the version
    const VERSION_CLOSED  = 1; // But in closed version no one can add module

    /**
     * @inheritdoc
     */
    protected $guarded = ["id"];
}
