<?php

namespace Modules\Deployer\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Deployer\Providers\DeployerServiceProvider as Deployer;

class DeployerModule extends YasnaModel
{
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $guarded = ["id"];



    /**
     * Return all versions of module
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function versions()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'DeployerModuleVersion', 'module_id')
                    ->orderBy('version', 'DESC')
             ;
    }



    /**
     * Return all projects with module
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(
             MODELS_NAMESPACE . 'DeployerProject',
             'deployer_project_module',
             'project_id',
             'module_id'
        )
                    ->distinct()
             ;
    }



    /**
     * Indicate user can delete the host or not
     *
     * @return bool
     */
    public function canDelete()
    {
        return Deployer::canDeleteModule()();
    }



    /**
     * Indicate user can information of the host or not
     *
     * @return bool
     */
    public function canEdit()
    {
        return Deployer::canEditModule()();
    }



    /**
     * Save a new version of module
     *
     * @param string $version
     * @param bool   $force
     *
     * @return bool
     * @throws \Exception
     */
    public function saveNewRelease(string $version, bool $force = false)
    {
        // first search this release version exist or not
        $release = $this->getRelease($version);

        // if the release exists and it's a forced saving process, delete old version and save it as new release otherwise reject it
        if ($release) {
            if ($force) {
                $release->delete();
                $this->draftNewRelease($version);

                return true;
            } else {
                return false;
            }
        }

        // if release doesn't exist, draft a new release and save it
        $this->draftNewRelease($version);

        return true;
    }



    /**
     * Return all project versions which uses module
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projectVersions()
    {
        return $this->belongsToMany(
             MODELS_NAMESPACE . 'DeployerProjectVersion',
             'deployer_project_module',
             'project_version_id',
             'module_id'
        )
                    ->distinct()
             ;
    }



    /**
     * Make a one-to-one relation to version model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function release()
    {
        return $this->hasOne(MODELS_NAMESPACE . 'DeployerModuleVersion', 'module_id');
    }



    /**
     * Return a specified version of module
     *
     * @param string $version
     *
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|null
     */
    public function getRelease(string $version)
    {
        return $this->release()
                    ->where(
                         [
                              'version'    => $version,
                              'deleted_at' => null,
                         ]
                    )
                    ->first()
             ;
    }



    /**
     * Draft a new version of module
     *
     * @param string $version
     * @param bool   $save
     *
     * @return YasnaModel
     */
    public function draftNewRelease(string $version, bool $save = true)
    {
        $release = model('deployer_module_version');

        $release->version   = $version;
        $release->module_id = $this->id;

        if ($save) {
            $release->save();
        }

        return $release;
    }
}
