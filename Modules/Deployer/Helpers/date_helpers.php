<?php
/**
 * Created by IntelliJ IDEA.
 * User: meysampg
 * Date: 7/10/18
 * Time: 8:11 PM
 */

/**
 * Convert JS timestamp to php corresponded
 *
 * @param string $timestamp js precision timestamp
 *
 * @return bool|Carbon\Carbon
 */
if (!function_exists('jsToCarbonNormalizer')) {
    function jsToCarbonNormalize(string $timestamp)
    {
        if (!strlen($timestamp)) {
            return false;
        }

        // remove js milliseconds digits
        $timestamp = substr($timestamp, 0, strlen($timestamp) - 3);
        // convert normalized timestamp to Carbon class
        $carbonClass = Carbon\Carbon::parse(date('Y-m-d H:i:s', $timestamp));

        return $carbonClass;
    }
}
