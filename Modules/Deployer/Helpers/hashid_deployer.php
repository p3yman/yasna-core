<?php
/**
 * Created by PhpStorm.
 * User: meysampg
 * Date: 8/8/18
 * Time: 10:33 AM
 */

if (!function_exists('hashid_deployer')) {
    /**
     * Encrypt/Decrypt a given token as a hashid
     *
     * @param $token
     *
     * @return array|int|string
     */
    function hashid_deployer($token)
    {
        return hashid($token, 'deployer_token');
    }
}
