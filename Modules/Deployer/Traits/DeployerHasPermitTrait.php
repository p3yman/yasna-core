<?php
/**
 * Created by PhpStorm.
 * User: meysampg
 * Date: 7/24/18
 * Time: 4:29 PM
 */

namespace Modules\Deployer\Traits;

trait DeployerHasPermitTrait
{
    /**
     * Return a closure which is ready to check a rule for a user as a deployer
     *
     * @param string $rule A rule selected in form of DeployerPermission::RULE_*
     *
     * @return \Closure
     */
    public static function hasPermit($rule)
    {
        $role = DeployerPermissions::ROLE_DEPLOYER;

        return function () use ($role, $rule) {
            return user()->as($role)->can($rule);
        };
    }



    /**
     * Generates array of `can` rules to pass to the middleware method of route. Each element must be in form of
     * `can:rule:role` and this method return a compatible array for a given set of rules.
     *
     * @param array $rules
     *
     * @return string
     */
    public static function canMiddleware(array $rules)
    {
        $role  = DeployerPermissions::ROLE_DEPLOYER;
        $orCan = 'or-can:'
             . implode('|', $rules)
             . ','
             . $role;

        return $orCan;
    }



    /**
     * A Shorthand to detect that user has a deployer role or not
     *
     * @return \Closure
     */
    public static function isADeployer()
    {
        $role = DeployerPermissions::ROLE_DEPLOYER;

        return function () use ($role) {
            return user()->is($role) or user()->isDeveloper();
        };
    }



    /**
     * Indicate user has permission to access to project or not
     *
     * @return \Closure
     */
    public static function canAccessProject()
    {
        return static::hasPermit(DeployerPermissions::RULE_PROJECT);
    }



    /**
     * Indicate user has permission to access to host or not
     *
     * @return \Closure
     */
    public static function canAccessHost()
    {
        return static::hasPermit(DeployerPermissions::RULE_HOST);
    }



    /**
     * Indicate user has permission to access to module or not
     *
     * @return \Closure
     */
    public static function canAccessModule()
    {
        return static::hasPermit(DeployerPermissions::RULE_MODULE);
    }



    /**
     * Indicate user has permission to create a project or not
     *
     * @return \Closure
     */
    public static function canCreateProject()
    {
        return static::hasPermit(DeployerPermissions::RULE_PROJECT_CREATE);
    }



    /**
     * Indicate user has permission to create a host or not
     *
     * @return \Closure
     */
    public static function canCreateHost()
    {
        return static::hasPermit(DeployerPermissions::RULE_HOST_CREATE);
    }



    /**
     * Indicate user has permission to create a module or not
     *
     * @return \Closure
     */
    public static function canCreateModule()
    {
        return static::hasPermit(DeployerPermissions::RULE_MODULE_CREATE);
    }



    /**
     * Indicate user has permission to edit a project or not
     *
     * @return \Closure
     */
    public static function canEditProject()
    {
        return static::hasPermit(DeployerPermissions::RULE_PROJECT_EDIT);
    }



    /**
     * Indicate user has permission to edit a host or not
     *
     * @return \Closure
     */
    public static function canEditHost()
    {
        return static::hasPermit(DeployerPermissions::RULE_HOST_EDIT);
    }



    /**
     * Indicate user has permission to edit a module or not
     *
     * @return \Closure
     */
    public static function canEditModule()
    {
        return static::hasPermit(DeployerPermissions::RULE_MODULE_EDIT);
    }



    /**
     * Indicate user has permission to delete a project or not
     *
     * @return \Closure
     */
    public static function canDeleteProject()
    {
        return static::hasPermit(DeployerPermissions::RULE_PROJECT_DELETE);
    }



    /**
     * Indicate user has permission to delete a host or not
     *
     * @return \Closure
     */
    public static function canDeleteHost()
    {
        return static::hasPermit(DeployerPermissions::RULE_HOST_DELETE);
    }



    /**
     * Indicate user has permission to delete a module or not
     *
     * @return \Closure
     */
    public static function canDeleteModule()
    {
        return static::hasPermit(DeployerPermissions::RULE_MODULE_DELETE);
    }



    /**
     * Indicate user has permission to deploy a release of a project or not
     *
     * @return \Closure
     */
    public static function canDeployRelease()
    {
        return static::hasPermit(DeployerPermissions::RULE_PROJECT_DEPLOY);
    }



    /**
     * Indicate user has permission to delete a release of a project or not
     *
     * @return \Closure
     */
    public static function canDeleteRelease()
    {
        return static::hasPermit(DeployerPermissions::RULE_RELEASE_DELETE);
    }
}
