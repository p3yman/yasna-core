<?php
/**
 * Created by PhpStorm.
 * User: meysampg
 * Date: 7/25/18
 * Time: 3:55 PM
 */

namespace Modules\Deployer\Traits;

/**
 * Interface DeployerPermissions
 * Define permission role and rules on a unified way for using in the module
 *
 * @package Modules\Deployer\Traits
 */
interface DeployerPermissions
{
    const ROLE_DEPLOYER          = 'deployer';
    const RULE_MAIN_PAGE_ACCESS  = 'common';
    const RULE_COMMON_STATISTICS = 'common.statistics';
    const RULE_PROJECT           = 'project';
    const RULE_PROJECT_BROWSE    = 'project.browse';
    const RULE_PROJECT_CREATE    = 'project.create';
    const RULE_PROJECT_EDIT      = 'project.edit';
    const RULE_PROJECT_DELETE    = 'project.delete';
    const RULE_PROJECT_DEPLOY    = 'project.deploy';
    const RULE_RELEASE_DELETE    = 'release.delete';
    const RULE_HOST              = 'host';
    const RULE_HOST_BROWSE       = 'host.browse';
    const RULE_HOST_CREATE       = 'host.create';
    const RULE_HOST_EDIT         = 'host.edit';
    const RULE_HOST_DELETE       = 'host.delete';
    const RULE_MODULE            = 'module';
    const RULE_MODULE_BROWSE     = 'module.browse';
    const RULE_MODULE_CREATE     = 'module.create';
    const RULE_MODULE_EDIT       = 'module.edit';
    const RULE_MODULE_DELETE     = 'module.delete';
}
