<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists('modal_route')) {
    /**
     * Return a ready-to-use master modal route
     *
     * @param string $route_name
     * @param array  $parameters
     * @param bool   $absolute
     *
     * @return string
     */
    function modal_route(string $route_name, array $parameters = [], bool $absolute = false)
    {
        return sprintf(
             "modal:%s",
             route($route_name, $parameters, $absolute)
        );
    }

    /**
     * Return a ready-to-use js master modal callback
     *
     * @param string $route_name
     * @param array  $parameters
     * @param bool   $absolute
     *
     * @return string
     */
    function master_modal(string $route_name, array $parameters = [], bool $absolute = false)
    {
        return sprintf(
             "masterModal('%s')",
             route($route_name, $parameters, $absolute)
        );
    }
}
