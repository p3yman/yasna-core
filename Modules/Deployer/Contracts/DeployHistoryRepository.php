<?php
/**
 * Created by PhpStorm.
 * User: meysampg
 * Date: 10/22/18
 * Time: 11:10 AM
 */

namespace Modules\Deployer\Contracts;

use App\Models\DeployerCommand;
use App\Models\DeployerProject;
use Illuminate\Support\Collection;
use Modules\Deployer\Entities\DeployerProjectVersion;

/**
 * Interface DeployHistoryRepository contracts on what a deploy history must be
 *
 * @package Modules\Deployer\Contracts
 */
interface DeployHistoryRepository
{
    const TYPE_JUST_DEPLOY = 0;
    const TYPE_DEMO        = -1;
    const TYPE_FINAL       = -2;
    const TYPE_DELETE      = -4;
    const TYPE_INSTANT_CMD = -8;



    /**
     * Return a resource which is correspond to a wanted DeployHistoryRepository
     *
     * @param string|int $hashid Hashid or ID of DeployHistoryRepository resource
     *
     * @return Object
     */
    public function get($hashid);



    /**
     * Return all deployment histories of a project version
     *
     * @param string|DeployerProjectVersion $version Hashid or the object of project version
     *
     * @return Collection
     */
    public function getHistoriesByProjectVersion($version);



    /**
     * Return the deployment history of a project version
     *
     * @param string|DeployerProjectVersion $version Hashid or the object of project version
     *
     * @return Object
     */
    public function getLastHistoryByProjectVersion($version);



    /**
     * Save a history record to inform to deployment for a new work!
     *
     * @param DeployerProjectVersion $version Object of project which must be deployed
     * @param int|string             $type
     *
     * @return bool
     */
    public function saveRawHistory(DeployerProjectVersion $version, $type = self::TYPE_JUST_DEPLOY): bool;



    /**
     * Save a instant run command history record
     *
     * @param DeployerProject $project
     * @param DeployerCommand $command
     *
     * @return bool
     */
    public function saveInstantCommandHistory(DeployerProject $project, DeployerCommand $command): bool;



    /**
     * Get numeric equivalent of a given type
     *
     * @param string $type
     *
     * @return int
     */
    public function getNumericHistoryType(string $type): int;
}
