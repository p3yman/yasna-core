<?php

namespace Modules\Deployer\Contracts;

use Illuminate\Support\Collection;

interface DeployerMacroRepository
{
    /**
     * Return a collection of macros for a project and overridden values on the attached host. If there is no attached
     * host (`$host_project_id` given with null value) the original macros will be returned, otherwise if there is an
     * overridden value for a macro, it will be return for that macro.
     *
     * @param int      $project_id
     * @param int|null $host_project_id
     *
     * @return Collection
     */
    public function get(int $project_id, ?int $host_project_id = null): Collection;



    /**
     * Override a project macro on an attached host. If the host already has an overrided value for the given macro,
     * the given value will be replaced with old one.
     *
     * @param int    $project_macro_id
     * @param int    $host_project_id
     * @param string $value
     *
     * @return bool
     */
    public function override(int $project_macro_id, int $host_project_id, string $value): bool;
}
