<?php 

namespace Modules\Deployer\Schedules;

use Modules\Deployer\Services\UpdateProjectToLatestModulesVersionAndDeploy;
use Modules\Yasna\Services\YasnaSchedule;

class AutomaticUpdateSchedule extends YasnaSchedule
{
    /**
     * @inheritdoc
     */
    protected function job()
    {
        UpdateProjectToLatestModulesVersionAndDeploy::do();
    }



    /**
     * @inheritdoc
     */
    protected function frequency()
    {
        return 'everyFifteenMinutes';
    }
}
