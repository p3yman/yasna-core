<?php
/**
 * Created by PhpStorm.
 * User: meysampg
 * Date: 10/22/18
 * Time: 11:15 AM
 */

namespace Modules\Deployer\Repositories;

use Exception;
use ReflectionClassConstant;
use App\Models\DeployerCommand;
use App\Models\DeployerProject;
use App\Models\DeployerDeployHost;
use App\Models\DeployerCommandLog;
use Illuminate\Database\Eloquent\Collection;
use Modules\Deployer\Contracts\DeployHistoryRepository;
use Modules\Deployer\Entities\DeployerDeployHistory;
use Modules\Deployer\Entities\DeployerProjectVersion;

use function sprintf;
use function is_numeric;
use function strtoupper;
use function not_in_array;

/**
 * Class EloquentDeployHistoryRepository implement the repository of DeployHistoryRepository upon the eloquent
 * persistent layer
 *
 * @package Modules\Deployer\Repositories
 */
class EloquentDeployHistoryRepository implements DeployHistoryRepository
{
    /**
     * @inheritdoc
     */
    public function get($hashid)
    {
        return model('deployer_deploy_history', $hashid)
             ->with('hosts');
    }



    /**
     * @inheritdoc
     */
    public function getHistoriesByProjectVersion($version)
    {
        $project_version_id = $this->getProjectVersionID($version);

        return model('deployer_deploy_history')
             ->elector([
                  'project_version_id' => $project_version_id,
             ])
             ->with('hosts')
             ->orderBy('created_at', 'desc')
             ->get()
             ;
    }



    /**
     * @inheritdoc
     */
    public function getLastHistoryByProjectVersion($version)
    {
        $project_version_id = $this->getProjectVersionID($version);

        return model('deployer_deploy_history')
             ->elector([
                  'project_version_id' => $project_version_id,
             ])
             ->with('hosts')
             ->orderBy('created_at', 'desc')
             ->limit(1)
             ->first()
             ;
    }



    /**
     * @inheritdoc
     */
    public function saveRawHistory(DeployerProjectVersion $version, $type = self::TYPE_JUST_DEPLOY): bool
    {
        // check project version model exists
        if (!$version->exists && not_in_array($type, [static::TYPE_DELETE])) {
            return false;
        }

        // convert type to numeric equivalent if it's required
        if (!is_numeric($type)) {
            $type = $this->getNumericHistoryType($type);
        }

        // save and get saved deploy history record
        $history = $this->saveHistoryRecordOfVersion($version, $type);
        if (!$history) {
            return false;
        }

        // save raw host record for current history record
        $saved_hosts_collection = $this->saveHostRecordsOfHistory($history, $type);

        // save command records for queued saved hosts
        $saved = $this->saveCommandRecordsForHosts($history->project, $saved_hosts_collection);

        return $saved;
    }



    /**
     * @inheritdoc
     */
    public function saveInstantCommandHistory(DeployerProject $project, DeployerCommand $command): bool
    {
        // save and get saved deploy history record
        $history = $this->saveHistoryRecordOfVersion($project->open_version, static::TYPE_INSTANT_CMD);
        if (!$history) {
            return false;
        }

        // save raw host record for current history record
        $saved_hosts_collection = $this->saveHostRecordsOfHistory($history, static::TYPE_INSTANT_CMD);

        return $this->attachCommandToQueuedHosts($command, $saved_hosts_collection)->filter()->isNotEmpty();
    }



    /**
     * @inheritdoc
     */
    public function getNumericHistoryType(string $type): int
    {
        $constant_key   = sprintf("TYPE_%s", strtoupper($type));
        $constant_class = new ReflectionClassConstant(static::class, $constant_key);

        try {
            $constant_value = $constant_class->getValue();
        } catch (Exception $e) {
            $constant_value = static::TYPE_JUST_DEPLOY;
        }

        return $constant_value;
    }



    /**
     * Save a pending history record, related to a project version
     *
     * @param DeployerProjectVersion $version
     * @param int                    $status
     *
     * @return DeployerDeployHistory|null
     */
    private function saveHistoryRecordOfVersion(DeployerProjectVersion $version, int $status): ?DeployerDeployHistory
    {
        $history = model('deployer_deploy_history');

        $history->status             = $status;
        $history->type               = $this->getAlphabeticValueOfStatus($status);
        $history->project_id         = $version->project->id;
        $history->project_version_id = $version->id;

        if ($history->save()) {
            return $history;
        }

        return null;
    }



    /**
     * Save host status records of a history
     *
     * @param DeployerDeployHistory $history
     * @param int                   $status
     *
     * @return Collection|null
     */
    private function saveHostRecordsOfHistory(DeployerDeployHistory $history, int $status): ?Collection
    {
        $hostsOfProject = $history->project->hosts;
        $resultColl     = $hostsOfProject->map(function ($hostProject) use ($history, $status) {
            $hostRecord = model('deployer_deploy_host');

            $hostRecord->deploy_id       = $history->id;
            $hostRecord->host_id         = $hostProject->host->id;
            $hostRecord->host_project_id = $hostProject->id;
            $hostRecord->status          = $status;

            if ($hostRecord->save()) {
                return $hostRecord;
            }

            return null;
        });

        // the true location of false is false :D
        return $resultColl->filter();
    }



    /**
     * Attach commands to queued hosts
     *
     * @param DeployerProject $project
     * @param Collection      $saved_hosts_collection
     * @param bool            $instant_commands
     *
     * @return bool
     */
    private function saveCommandRecordsForHosts(
         $project,
         Collection $saved_hosts_collection,
         bool $instant_commands = false
    ): bool {
        if (!$saved_hosts_collection) {
            return false;
        }

        $filterMethod = $this->getCommandsFilterMethod($instant_commands);

        $commands = $project->commands()->{$filterMethod}()->get();
        if ($commands->isEmpty()) {
            return true;
        }

        $commands->map(function ($command) use ($saved_hosts_collection) {
            $this->attachCommandToQueuedHosts($command, $saved_hosts_collection);
        });

        return true;
    }



    /**
     * Attach a command to queued hosts
     *
     * @param DeployerCommand $command
     * @param Collection      $queued_hosts
     *
     * @return Collection
     */
    private function attachCommandToQueuedHosts(DeployerCommand $command, Collection $queued_hosts)
    {
        return $queued_hosts->map(function (DeployerDeployHost $queued_host) use ($command) {
            $log_record = model('deployer_command_log');

            $log_record->command_id        = $command->id;
            $log_record->project_id        = $command->project_id;
            $log_record->deploy_history_id = $queued_host->deploy_id;
            $log_record->deploy_host_id    = $queued_host->id;
            $log_record->status            = DeployerCommandLog::STATUS_PENDING;

            if ($log_record->save()) {
                return $log_record;
            }

            return null;
        });
    }



    /**
     * return integer ID of a given project version
     *
     * @param int|string|\Object $version
     *
     * @return int|null
     */
    private function getProjectVersionID($version)
    {
        // if it's an integer, it's done
        if (is_int($version)) {
            return $version;
        }

        // if it's a string, use it as hashid
        if (is_string($version)) {
            $version = model('deployer_project_version', $version);
        }

        // if it's a `Object`, try to explore version
        if (is_object($version) && property_exists($version, 'project_version_id')) {
            return $version->project_version_id;
        }

        return null;
    }



    /**
     * Alphabetic equivalent of status
     *
     * @param int $status
     *
     * @return string
     */
    private function getAlphabeticValueOfStatus(int $status): string
    {
        switch ($status) {
            case static::TYPE_JUST_DEPLOY:
                return 'deploy';
            case static::TYPE_DEMO:
                return 'demo';
            case static::TYPE_FINAL:
                return 'final';
            case static::TYPE_DELETE:
                return 'delete';
            case static::TYPE_INSTANT_CMD:
                return 'instant_cmd';
            default:
                return 'unknown';
        }
    }



    /**
     * return filter method of fetching project commands
     *
     * @param bool $is_instant
     *
     * @return string
     */
    private function getCommandsFilterMethod(bool $is_instant): string
    {
        if ($is_instant) {
            return 'filterInstantTypes';
        }

        return 'filterDeploymentTypes';
    }
}
