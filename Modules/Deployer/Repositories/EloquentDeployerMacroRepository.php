<?php

namespace Modules\Deployer\Repositories;

use Illuminate\Support\Collection;
use Modules\Deployer\Contracts\DeployerMacroRepository;

use function is_null;

class EloquentDeployerMacroRepository implements DeployerMacroRepository
{
    /**
     * @inheritdoc
     */
    public function get(int $project_id, ?int $host_project_id = null): Collection
    {
        $macros = model('deployer-macro')->whereProjectId($project_id)->whereNull('host_project_id')->get();

        if (is_null($host_project_id)) {
            return $macros;
        }

        return $macros->map(function ($macro) use ($host_project_id) {
            // for each macro, every host can have only one overridden version at a time
            $overridden = $macro->hostOverridden()->whereHostProjectId($host_project_id);
            if ($overridden->exists()) { // replace overridden version with original if exists
                $macro = $overridden->first();
            }

            return $macro;
        });
    }



    /**
     * @inheritdoc
     */
    public function override(int $project_macro_id, int $host_project_id, string $value): bool
    {
        $macro = model('deployer-macro', $project_macro_id);
        if (!$macro->exists) {
            return false;
        }

        $overridden = $macro->hostOverridden()->whereHostProjectId($host_project_id);
        if ($overridden->exists()) {
            $host_macro = $overridden->first();

            $host_macro->value = $value;
        } else {
            $host_macro = $macro->replicate();

            $host_macro->host_project_id = $host_project_id;
            $host_macro->value           = $value;
        }

        return $host_macro->save();
    }
}
