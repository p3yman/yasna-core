<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;


class CancelReleaseRequest extends YasnaRequest
{
    protected $model_name = "deployer_deploy_history";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }
}
