<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CommandSaveRequest extends YasnaRequest
{
    protected $model_name = "deployer-command";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'command'    => 'required|string',
             'type'       => 'required|string|in:type_early,type_later,type_instant',
             'project_id' => 'string',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'project_id' => 'dehash',
             'command'    => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->restrictProjectChanging();
    }



    /**
     * Avoid change project if model exists
     */
    protected function restrictProjectChanging()
    {
        if ($this->model->exists) {
            unset($this->data['project_id']);
        }
    }
}
