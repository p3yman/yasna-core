<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CommandInstantRequest extends YasnaRequest
{
    protected $model_name = "deployer-command";



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        if (!$this->model_name) {
            return;
        }

        $this->data['id']    = $this->getRequestedModelId($id_or_hashid);
        $this->data['model'] = $this->model = $this->getModel();
        $this->refillRequestHashid();
        $this->failIfModelNotExist();
    }



    /**
     * get instant command model
     *
     * @return \Modules\Yasna\Services\YasnaModel
     */
    private function getModel()
    {
        $builder = model($this->model_name);

        if ($this->model_with_trashed) {
            $builder = $builder->withTrashed();
        }

        if (($model = $builder->whereId($this->data['id'])->filterInstantTypes()->first()) != null) {
            return $model;
        }

        return model($this->model_name);
    }
}
