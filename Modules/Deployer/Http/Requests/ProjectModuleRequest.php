<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ProjectModuleRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'version_id' => 'required|string',
             'module_id'  => 'required|string',
        ];
    }



    /**
     * Return corresponded DeployerModuleVersion model of the request
     *
     * @return \Modules\Deployer\Entities\DeployerModuleVersion
     */
    public function moduleVersion()
    {
        return model('deployer_module_version', $this->get('version_id'));
    }
}
