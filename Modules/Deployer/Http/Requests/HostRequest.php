<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;
use Modules\Deployer\Providers\DeployerServiceProvider as DeployerModule;

class HostRequest extends YasnaRequest
{
    protected $model_name = "deployer_host";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        // first check the model exists
        if ($this->model && $this->model->exists) {
            // if model exists and user has edit permission return true
            if (DeployerModule::canEditHost()) {
                return true;
            }

            // otherwise although model exist, but user doesn't have edit permission
            return false;
        }

        // model doesn't exist, so it's a creation process
        return DeployerModule::canCreateHost();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name'        => 'required|string|max:40',
             'domain'      => 'required_without:ip|url|max:120',
             'ip'          => 'required_without:domain|ip|max:15',
             'port'        => 'required|string|max:4',
             'description' => 'string',
        ];
    }
}
