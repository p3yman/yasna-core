<?php

namespace Modules\Deployer\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Modules\Deployer\Entities\DeployerHostProject;
use Modules\Yasna\Services\YasnaRequest;

class ProjectHostRequest extends YasnaRequest
{
    protected $model_name = "deployer_host_project";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'host_id'     => 'required|string',
             'deploy_path' => [
                  'required',
                  'string',
                  'regex:/^\/([\/\w\-]+)/', // validate path like `/var/www/folder/data_123`
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'project_id' => 'hashid',
             'host_id'    => 'hashid',
        ];
    }



    /**
     * A deploy path must be unique for a project. This means you can't insert
     * `/var/www/path` twice for a host in one project, but you can insert
     * `/var/www/path` for two host or mores in a project or even for
     * both project1 and project2. In other words, uniqueness is a
     * function of project_id.
     *
     * @return bool
     */
    public function canInsert()
    {
        $hasInserted = DeployerHostProject::where(
             [
                  'project_id'  => hashid($this->get('project_id')),
                  'host_id'     => hashid($this->get('host_id')),
                  'deploy_path' => $this->get('deploy_path'),
             ]
        )
                                          ->count()
        ;

        return $hasInserted == 0 ? true : false;
    }
}
