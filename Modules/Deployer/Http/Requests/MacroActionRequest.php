<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;


class MacroActionRequest extends YasnaRequest
{
    protected $model_name = "deployer-macro";



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->refillModel();
    }



    /**
     * refill model on create situations
     */
    protected function refillModel()
    {
        if ($this->action == 'create') {
            $this->model = model($this->model_name);

            $this->model->project_id = $this->id;
        }
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        if (!$this->model_name) {
            return;
        }

        $this->data['id']    = $this->getRequestedModelId($id_or_hashid);
        $this->data['model'] = $this->model = model($this->model_name, $this->data['id'], $this->model_with_trashed);
        $this->refillRequestHashid();
    }
}
