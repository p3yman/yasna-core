<?php

namespace Modules\Deployer\Http\Requests;

use function implode;
use Modules\Yasna\Services\YasnaRequest;

class MacroSaveRequest extends YasnaRequest
{
    protected $model_name = "deployer-macro";



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'macro'           => 'upper|ed',
             'project_id'      => 'dehash',
             'host_project_id' => 'dehash',
        ];
    }



    /**
     * general rules
     *
     * @return array
     */
    public function generalRules()
    {
        return [
             'project_id' => 'required|int',
        ];
    }



    /**
     * return rules related to the macro
     *
     * @return array
     */
    public function macroRules()
    {
        $rules   = 'required|string';
        $globals = config('deployer.global_macros');

        if ($this->isset('unique')) {
            $rules .= '|accepted';
        }

        if (!empty($globals)) {
            $rules .= "|not_in:" . implode(",", $globals);
        }

        return [
             'macro' => $rules,
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'macro' => trans('deployer::macro.macro'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'macro.accepted' => trans(
                  'deployer::macro.macro_already_exist_on_project',
                  ['macro' => $this->getData('macro')]
             ),
             'macro.not_in' => trans(
                  'deployer::macro.macro_already_exist_on_project',
                  ['macro' => $this->getData('macro')]
             ),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->removeEmptyHostProject();
        $this->makeSureMacroIsUniqueOnProject();
    }



    /**
     * indicate current model with another macro
     *
     * @return bool
     */
    public function macroIsChanged(): bool
    {
        return !empty($this->old_macro) and $this->macro !== $this->old_macro;
    }



    /**
     * remove empty host project from request
     */
    private function removeEmptyHostProject()
    {
        if (!$this->getData('host_project_id')) {
            $this->unsetData('host_project_id');
        }
    }



    /**
     * Make sure macro is unique on the project scope
     */
    private function makeSureMacroIsUniqueOnProject()
    {
        if ($this->isEditMode()) {
            return;
        }

        $builder = model($this->model_name)
             ->whereProjectId($this->getData('project_id'))
             ->whereMacro($this->macro)
        ;

        if ($this->isset('host_project_id')) {
            $builder = $builder->whereHostProjectId($this->getData('host_project_id'));
        } else {
            $builder = $builder->whereNull('host_project_id');
        }

        if ($builder->exists()) {
            $this->setData('unique', false);
        }
    }



    /**
     * Indicate it's a edit request
     *
     * @return bool
     */
    private function isEditMode(): bool
    {
        return $this->model->exists;
    }
}
