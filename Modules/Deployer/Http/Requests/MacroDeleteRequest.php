<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class MacroDeleteRequest extends YasnaRequest
{
    protected $model_name = "deployer-macro";
}
