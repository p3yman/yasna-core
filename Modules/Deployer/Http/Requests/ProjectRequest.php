<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;
use Modules\Deployer\Providers\DeployerServiceProvider as DeployerModule;

class ProjectRequest extends YasnaRequest
{
    protected $model_name = "deployer_project";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        // first check the model exists
        if ($this->model && $this->model->exists) {
            // if model exists and user has edit permission return true
            if (DeployerModule::canEditProject()) {
                return true;
            }

            // otherwise although model exist, but user doesn't have edit permission
            return false;
        }

        // model doesn't exist, so it's a creation process
        return DeployerModule::canCreateProject();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'support_date'    => 'date',
             'update_check_at' => 'date',
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name'            => 'string|required|max:40',
             'description'     => 'string',
             'status'          => 'boolean|required|in:0,1',
             'support_date'    => 'date',
             'update_check_at' => 'date',
        ];
    }
}
