<?php

namespace Modules\Deployer\Http\Requests;

use function array_keys;
use function explode_not_empty;
use Modules\Yasna\Services\YasnaRequest;

use function hashid;

class CloneCommandsRequest extends YasnaRequest
{
    protected $model_name = "deployer-project";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'type'     => 'in:clone',
             'projects' => 'required_if:type,clone',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'projects' => trans("deployer::command.select_destination_projects"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'projects',
             'type',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctProjects();
    }



    /**
     * convert given hashids to valid projects
     */
    private function correctProjects()
    {
        $ids = explode_not_empty(',', $this->getData("_projects"));
        $ids = hashid($ids);
        $this->unsetData('_projects');

        $builder = model($this->model_name)->whereIn('id', $ids);
        if ($builder->exists()) {
            $this->setData('projects', $builder->get());
        } else {
            $this->unsetData('projects');
        }
    }
}
