<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CommandSingleRequest extends YasnaRequest
{
    public $project_model;

    protected $model_name = "deployer-command";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'project' => 'string',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->fillProjectModel();
    }



    /**
     * Fill project model if found
     */
    protected function fillProjectModel()
    {
        $this->project_model = model('deployer-project')->grabHashid($this->input('project'));
    }
}
