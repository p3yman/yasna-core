<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;


class SharedResourceSaveRequest extends YasnaRequest
{
    protected $model_name = "deployer-shared-resource";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'path'       => 'required|string',
             'project_id' => 'string',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'project_id' => 'dehash',
             'path'       => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->restrictProjectChanging();
    }



    /**
     * Avoid change project if model exists
     */
    protected function restrictProjectChanging()
    {
        if ($this->model->exists) {
            unset($this->data['project_id']);
        }
    }
}
