<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CommandListRequest extends YasnaRequest
{
    protected $model_name = "deployer-project";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'protector' => $this->model->exists ? '' : 'accepted',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->checkModelExists();
    }



    /**
     * If project model not exist, block the request
     */
    protected function checkModelExists()
    {
        if (!$this->model->exists) {
            $this->setData('protector', 'covfefe');
        }
    }
}
