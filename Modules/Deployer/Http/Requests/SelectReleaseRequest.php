<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class SelectReleaseRequest extends YasnaRequest
{
    protected $model_name = "DeployerProjectVersion";



    /**
     * check given type is valid or not
     *
     * @return array
     */
    public function typeRules()
    {
        return [
             'type' => 'required|string|in:demo,final',
        ];
    }



    /**
     * Ensure model exist
     *
     * @return array
     */
    public function modelRules()
    {
        return [
             'protector' => $this->model->exists ? '' : 'required', // <~~ just as a fake guard
        ];
    }
}
