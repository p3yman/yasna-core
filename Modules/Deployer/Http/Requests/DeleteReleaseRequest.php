<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DeleteReleaseRequest extends YasnaRequest
{
    protected $model_name = "deployer_project_version";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        // TODO: must be complete on #4343
        return $this->model->exists or true;
    }
}
