<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class HostProjectRequest extends YasnaRequest
{
    protected $model_name = "deployer-host-project";

    public function rules()
    {
        return [

        ];
    }
}
