<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;


class SharedResourceSingleRequest extends YasnaRequest
{
    protected $model_name = "deployer-shared-resource";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'protector' => $this->model->exists ? '' : 'accepted',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->checkModelExists();
    }



    /**
     * If shared resource model not exist, block the request
     */
    protected function checkModelExists()
    {
        if (!$this->model->exists) {
            $this->setData('protector', 'covfefe');
        }
    }
}
