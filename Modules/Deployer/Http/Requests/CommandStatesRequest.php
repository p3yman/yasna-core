<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

/**
* Class    CommandStatesRequest
* @package $NAMESPACE$
*/
class CommandStatesRequest extends YasnaRequest
{
	protected $model_name = "deployer-deploy-host";
}
