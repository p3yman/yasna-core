<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

class ModuleReleaseRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'release'          => 'required|array',
             'release.tag_name' => [
                  'required',
                  'string',
                  'regex:/v[\d\.]+/', // this rule enforce semantic version style to the release tag
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    protected function fillableFields()
    {
        return ['*'];
    }
}
