<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Deployer\Providers\DeployerServiceProvider as DeployerModule;
use Modules\Yasna\Services\YasnaRequest;

class ModuleRequest extends YasnaRequest
{
    protected $model_name = "deployer_module";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        // first check the model exists
        if ($this->model && $this->model->exists) {
            // if model exists and user has edit permission return true
            if (DeployerModule::canEditModule()) {
                return true;
            }

            // otherwise although model exist, but user doesn't have edit permission
            return false;
        }

        // model doesn't exist, so it's a creation process
        return DeployerModule::canCreateModule();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name'        => 'required|string|max:40',
             'description' => 'string',
             'secret_key'  => 'string|max:15',
             'repository'  => [
                  'required',
                  'string',
                  'regex:/^(https\:\/\/|git@)(github|gitlab)\.com(\/|\:)\w+\/\S+/',
             ],
             'path'        => [
                  'string',
                  'regex:/^[\.\/]{0,2}[\w-\/]*$/',
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        // clearing heading ./ or / from the module path
        $this->data['path'] = preg_replace('/^[\.\/]{1,2}/', '', $this->data['path']);
    }
}
