<?php

namespace Modules\Deployer\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class MacroListRequest extends YasnaRequest
{
    protected $model_name = "deployer-project";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }
}
