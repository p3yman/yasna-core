<?php
/**
 * Created by PhpStorm.
 * User: meysampg
 * Date: 8/7/18
 * Time: 8:29 PM
 */

namespace Modules\Deployer\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GitAuthenticateMiddleware
{
    /**
     * Check a webhook call is valid or not
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next)
    {
        $headers = $request->headers;

        // first check it has a `X-Hub-Signature` header or not, if it's, it's from Github
        $githubX = $headers->get('X-Hub-Signature', null);
        if ($githubX) {
            // actullay github uses a HMAC encryption for check that requests are valid,
            // which we have done it now by a unique token. just now skip checking HMAC
            // validation for github hooks, but for more security it can be done on future.
            return $next($request);
        }

        // otherwise check it has a `X-Gitlab-Token` header or not, if it's, it's from Gitlab
        $gitlabX = $headers->get('X-Gitlab-Token', null);
        if ($gitlabX) {
            $gitlabExist = model('deployer')->where(['secret_key' => $gitlabX])->exist();
            if ($gitlabExist) {
                return $next($request);
            }
        }

        // Otherwise that's not an authorized request and should be rejected
        if ($request->ajax() || $request->wantsJson()) {
            return response('Unauthorized.', 401);
        } else {
            return new Response(view('yasna::errors.403'));
        }
    }
}
