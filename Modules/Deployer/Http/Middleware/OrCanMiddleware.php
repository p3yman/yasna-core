<?php

namespace Modules\Deployer\Http\Middleware;

/**
 * Created by PhpStorm.
 * User: meysampg
 * Date: 7/25/18
 * Time: 5:34 PM
 */

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrCanMiddleware
{
    /**
     * For some rules of a role, if one of them became true the request will supposed ok
     *
     * @param Request $request
     * @param Closure $next
     * @param string  $rules Rules in form of rule1[|rule2]
     * @param string  $role  Role which wanna check
     *
     * @return array|mixed
     */
    public function handle(Request $request, Closure $next, string $rules, string $role = 'admin')
    {
        if (strlen($rules)) {
            foreach (explode('|', $rules) as $rule) {
                if (user()->as($role)->can($rule)) {
                    return $next($request);
                }
            }
        }

        if ($request->ajax() || $request->wantsJson()) {
            return response('Unauthorized.', 401);
        } else {
            return new Response(view('yasna::errors.403'));
        }
    }
}
