<?php

namespace Modules\Deployer\Http\Controllers;

use App\Models\DeployerProject;
use Modules\Deployer\Entities\DeployerHost;
use Modules\Deployer\Entities\DeployerHostProject;
use Modules\Deployer\Http\Requests\ProjectHostRequest;
use Modules\Yasna\Services\YasnaController;

class ProjectServerController extends YasnaController
{
    protected $view_folder = "deployer::projects";
    protected $base_model  = "deployer_host_project";
    protected $row_view    = "project_hosts_row";



    /**
     * Add server to the project
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function listProjectServers(string $hashid)
    {
        // first get project model
        /* @var DeployerProject $project */
        $project = model('deployer_project', $hashid);
        // check for release existence
        if (!$project->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        // then get the list of all servers attached to the project
        /* @var DeployerHostProject[] $models */
        $models      = $project->hosts;
        $title_label = trans('deployer::project.hosts_list_modal_title', ['name' => $project->name]);
        $headings    = [
             trans('deployer::host.name'),
             trans('deployer::project.deploy_path'),
             trans('deployer::dashboard.operations'),
        ];

        return $this->view('project_hosts', compact('title_label', 'headings', 'models', 'project'));
    }



    /**
     * List all available servers for adding to the project
     *
     * @param string $hashid HashID of project
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function addServerToProject(string $hashid)
    {
        // get the project
        /* @var DeployerProject $project */
        $project = model('deployer_project', $hashid);
        // check for project existence
        if (!$project->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        // generate hosts for combo
        $hosts       = DeployerHost::availableHost();
        $title_label = trans('deployer::project.add_host_to_project_title', ['name' => $project->name]);

        return $this->view('add_host_form', compact('hosts', 'title_label', 'project'));
    }



    /**
     * Save information of a host which is attached to a project
     *
     * @param ProjectHostRequest $request
     * @param string             $hashid
     *
     * @return string|array
     */
    public function saveServerInformation(ProjectHostRequest $request, string $hashid)
    {
        // get the project
        /* @var DeployerProject $project */
        $project = model('deployer_project', $hashid);
        // check for project existence
        if (!$project->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $model = model('deployer_host_project');
        if (!$request->canInsert()) {
            return $this->jsonFeedback(trans('deployer::project.duplicate_deploy_path'));
        }

        $model = $request->model->batchSave($request);

        return [
             'callback' => sprintf(
                  "masterModal('%s')",
                  route('deployer.project.server', ['hashid' => $project->hashid], false)
             ),
        ];
    }



    /**
     * Edit deploy path of an attached host on a server
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function editServerInformation(string $hashid)
    {
        /* @var DeployerHostProject $project */
        $model = model('deployer_host_project', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.not_host_attached_host_found'));
        }

        // generate hosts for combo
        $title_label = trans(
             'deployer::project.edit_host_of_project_title',
             ['project' => $model->project->name, 'host' => $model->host->name]
        );

        return $this->view('edit_host_form', compact('title_label', 'model'));

    }



    /**
     * Show a confirm box for ensure that user wants to delete a server from a project
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function removeHostConfirm(string $hashid)
    {
        /* @var DeployerHostProject $model */
        $model       = model('deployer_host_project', $hashid);
        $title_label = trans('deployer::project.remove_host');

        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.not_host_attached_host_found'));
        }

        return $this->view('delete_host', compact('model', 'title_label'));

    }



    /**
     * Remove a host from a project
     *
     * @param string $hashid
     *
     * @return string|array
     * TODO: must define a firm logic for available host and delete rule for them
     */
    public function removeHost(string $hashid)
    {
        /* @var DeployerHostProject $model */
        $model = model('deployer_host_project', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.not_host_attached_host_found'));
        }

        $project = $model->project;
        $deleted = $model->delete();

        return [
             'callback' => sprintf(
                  "masterModal('%s')",
                  route('deployer.project.server', ['hashid' => $project->hashid], false)
             ),
        ];
    }
}
