<?php

namespace Modules\Deployer\Http\Controllers;

use function compact;
use Modules\Deployer\Http\Requests\SharedResourceSaveRequest;
use Modules\Deployer\Http\Requests\SharedResourceSingleRequest;
use Modules\Deployer\Http\Requests\SharedResourcesRequest;
use Modules\Yasna\Services\YasnaController;

class ProjectSharedResourcesController extends YasnaController
{
    protected $base_model  = "";
    protected $view_folder = "deployer::shared";



    /**
     * List of project's shared request
     *
     * @param SharedResourcesRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function index(SharedResourcesRequest $request)
    {
        $project = $request->model;
        $models  = $project->shared_resources;

        $title    = trans('deployer::shared.shared_resources', ['project' => $project->name]);
        $headings = [
             trans('deployer::shared.path'),
        ];

        return $this->view('index', compact('project', 'models', 'title', 'headings'));
    }



    /**
     * Save a shared resource model
     *
     * @param SharedResourceSaveRequest $request
     *
     * @return array
     */
    public function save(SharedResourceSaveRequest $request)
    {
        $model  = $request->model->batchSave($request);
        $hashid = $model->project->hashid;

        return [
             'callback' => master_modal('deployer.project.shared_resources_list', ['hashid' => $hashid], false),
        ];
    }



    /**
     * Create a shared resource
     *
     * @param SharedResourcesRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create(SharedResourcesRequest $request)
    {
        $project = $request->model;
        $model   = model('deployer-shared-resource');
        $title   = trans('deployer::shared.add_shared_resources', ['project' => $project->name]);

        $model->project_id = $project->hashid;

        return $this->view('form', compact('project', 'model', 'title'));
    }



    /**
     * Edit a shared resource
     *
     * @param SharedResourceSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(SharedResourceSingleRequest $request)
    {
        $model   = $request->model;
        $project = $model->project;
        $title   = trans('deployer::shared.edit_shared_resources', ['project' => $project->name]);

        $model->project_id = hashid($model->hashid);

        return $this->view('form', compact('project', 'model', 'title'));
    }



    /**
     * Show deleting confirm box
     *
     * @param SharedResourceSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirm(SharedResourceSingleRequest $request)
    {
        $model = $request->model;
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::shared.shared_resource_not_found'));
        }

        return $this->view('delete', compact('model'));
    }



    /**
     * Delete a shared resource
     *
     * @param SharedResourceSingleRequest $request
     *
     * @return array
     */
    public function delete(SharedResourceSingleRequest $request)
    {
        $hashid = hashid($request->model->project_id);

        $request->model->delete();

        return [
             'callback' => master_modal('deployer.project.shared_resources_list', ['hashid' => $hashid], false),
        ];
    }
}
