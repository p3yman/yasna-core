<?php

namespace Modules\Deployer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class DeployerController extends Controller
{
    protected $view_folder = "deployer::deployer";

    /**
     * Display a shortcut list for other locations
     *
     * @return Response
     */
    public function index()
    {
        return view('index');
    }
}
