<?php

namespace Modules\Deployer\Http\Controllers;

use App\Models\DeployerCommand;
use App\Models\DeployerProject;
use Modules\Deployer\Contracts\DeployHistoryRepository;
use Modules\Deployer\Http\Requests\CloneCommandsRequest;
use Modules\Deployer\Http\Requests\CommandInstantRequest;
use Modules\Deployer\Http\Requests\CommandListRequest;
use Modules\Deployer\Http\Requests\CommandSaveRequest;
use Modules\Deployer\Http\Requests\CommandSingleRequest;
use Modules\Deployer\Http\Requests\CommandStatesRequest;
use Modules\Yasna\Services\YasnaController;

use function compact;
use function master_modal;
use function sprintf;

class ProjectCommandController extends YasnaController
{
    protected $base_model  = "deployer-command";
    protected $view_folder = "deployer::commands";



    /**
     * Show early and later commands list of a project
     *
     * @param CommandListRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(CommandListRequest $request)
    {
        $project      = $request->model;
        $filterMethod = $this->getFilterMethod($request);
        $eagerLoads   = $this->getIndexEagerLoads($request);
        $models       = $project->commands()->with($eagerLoads)->{$filterMethod}()->get();
        $page         = $this->getPageArray($request, $project);
        $type         = $request->type ?? 'el';
        $index        = 'index';

        $title_label = trans('deployer::command.commands', ['project' => $project->name]);

        $headings = [
             trans('deployer::command.command'),
             trans('deployer::command.type'),
        ];

        if ($request->type == 'i') {
            $headings[] = trans('deployer::command.command_state');

            $index = 'instant-index';
        }

        return $this->view($index, compact('models', 'headings', 'project', 'title_label', 'page', 'type'));
    }



    /**
     * Add a command to a project
     *
     * @param CommandSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function create(CommandSingleRequest $request)
    {
        if (!$request->project_model->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $project = $request->project_model;
        $model   = model('deployer-command');

        $model->project_id = $project->hashid;
        $title_label       = trans('deployer::command.create_title', ['project' => $request->project_model->name]);

        return $this->view('form', compact('model', 'title_label', 'project'));
    }



    /**
     * Run a command instantly
     *
     * @param CommandSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function run(CommandSingleRequest $request)
    {
        if (!$request->project_model->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $project = $request->project_model;
        $model   = model('deployer-command');

        $model->project_id = $project->hashid;
        $title_label       = trans('deployer::command.run_title', ['project' => $request->project_model->name]);

        return $this->view('instant-form', compact('model', 'title_label', 'project'));
    }



    /**
     * Edit a project command
     *
     * @param CommandSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(CommandSingleRequest $request)
    {
        if (!$request->model->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $model       = $request->model;
        $project     = $model->project;
        $title_label = trans('deployer::command.edit_title', ['project' => $project->name]);

        $model->project_id = hashid($model->project_id);

        return $this->view('form', compact('model', 'title_label', 'project'));
    }



    /**
     * Create/update a project command
     *
     * @param CommandSaveRequest      $request
     * @param DeployHistoryRepository $repository
     *
     * @return array|string
     */
    public function save(CommandSaveRequest $request, DeployHistoryRepository $repository)
    {
        $saved = $request->model->batchSave($request);

        if ($request->type == "type_instant") {
            $saved = $repository->saveInstantCommandHistory($saved->project, $saved);
        }

        return $this->jsonSaveFeedback(
             $saved,
             [
                  'success_modalClose' => '1',
                  'success_refresh'    => '1',
             ]
        );
    }



    /**
     * Show deleting confirm box
     *
     * @param CommandSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirm(CommandSingleRequest $request)
    {
        $model = $request->model;
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        return $this->view('delete', compact('model'));
    }



    /**
     * delete a command
     *
     * @param CommandSingleRequest $request
     *
     * @return array|string
     */
    public function delete(CommandSingleRequest $request)
    {
        $model = $request->model;

        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $deleted = $model->delete();


        return $this->jsonSaveFeedback(
             $deleted,
             [
                  'success_modalClose' => '1',
                  'success_refresh'    => '1',
             ]
        );
    }



    /**
     * show command states on a server
     *
     * @param CommandStatesRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function commandStates(CommandStatesRequest $request)
    {
        $model = $request->model;
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::host.host_not_found'));
        }

        $project = $model->deploy->project;
        $host    = $model->host;
        $deploy  = $model->deploy;
        $models  = $model->commands_log;
        $title   = trans("deployer::command.command_states_title", ['host' => $host->name, 'project' => $project->name]);

        $headings = [
             trans("deployer::command.command"),
             trans("deployer::command.type"),
             trans("deployer::command.command_state"),
             trans("deployer::command.command_result"),
        ];

        return $this->view('host_commands', compact('model', 'models', 'title', 'headings', 'deploy'));
    }



    /**
     * show results of running an instant command
     *
     * @param CommandSingleRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function instantCommandStates(CommandSingleRequest $request)
    {
        $model = $request->model;
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::command.command_not_found'));
        }
        $model->load(['logs']);

        $project = $model->project;
        $models  = $model->logs;
        $title   = trans("deployer::command.instant_command_states_title", ['project' => $project->name]);

        $headings = [
             trans("deployer::command.command"),
             trans("deployer::command.host"),
             trans("deployer::command.command_state"),
             trans("deployer::command.command_result"),
        ];

        return $this->view('project_instant_commands', compact('model', 'models', 'title', 'headings'));

    }



    /**
     * show clone modal
     *
     * @param CloneCommandsRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function commandsCloneModal(CloneCommandsRequest $request)
    {
        $project = $request->model;

        $title = trans("deployer::command.clone_commands_title", ['project' => $project->name]);

        return $this->view('clone', compact('title', 'project'));
    }



    /**
     * clone commands of a project into given destination projects
     *
     * @param CloneCommandsRequest $request
     *
     * @return string
     */
    public function commandsClone(CloneCommandsRequest $request)
    {
        $project  = $request->model;
        $commands = $project->commands()->filterDeploymentTypes()->get();

        $data = $request->projects
             ->map(function ($dest_project) use ($commands) {
                 // remove old commands
                 $dest_project->commands()->delete();

                 // generate new set of commands for the destination project
                 return $commands->map(function ($command) use ($dest_project) {
                     $data = $command->only(["command", "type"]);

                     $data["type"]       = DeployerCommand::numericType($data["type"]);
                     $data["project_id"] = $dest_project->id;
                     $data["created_at"] = date('c');
                     $data["created_by"] = user()->id;
                     $data["updated_at"] = date('c');

                     return $data;
                 });
             })
             ->filter()
             ->flatten(1)
             ->toArray()
        ;

        if (!empty($data)) {
            model('deployer-command')::insert($data);
        }

        return $this->jsonAjaxSaveFeedback(true);
    }



    /**
     * get page info
     *
     * @param CommandListRequest $request
     * @param DeployerProject    $project
     *
     * @return array
     */
    protected function getPageArray(CommandListRequest $request, DeployerProject $project)
    {
        $request_tab = $request->input('tab', 'all');
        $url         = "deployer/project/commands?hashid=" . $project->hashid;

        $page = [
             [
                  'deployer/project',
                  trans('deployer::project.projects'),
             ],
             [
                  $url,
                  trans('deployer::command.commands', ['project' => $project->name]),
             ],
             [
                  $request_tab,
                  trans(sprintf("deployer::command.%s_commands", $request_tab)),
                  $url,
             ],
        ];

        return $page;
    }



    /**
     * return the filter method of commands filter
     *
     * @param CommandListRequest $request
     *
     * @return string
     */
    private function getFilterMethod(CommandListRequest $request): string
    {
        switch ($request->type) {
            case 'i':
                return 'filterInstantTypes';
            case 'el':
            default:
                return 'filterDeploymentTypes';
        }
    }



    /**
     * return relations wich must be eager loaded on the index, based on requested tab
     *
     * @param CommandListRequest $request
     *
     * @return array
     */
    private function getIndexEagerLoads(CommandListRequest $request): array
    {
        if ($request->type == 'i') {
            return [
                 'logs',
            ];
        }

        return [];
    }
}
