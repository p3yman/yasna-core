<?php

namespace Modules\Deployer\Http\Controllers;

use Modules\Deployer\Entities\DeployerModule;
use Modules\Deployer\Entities\DeployerModuleVersion;
use Modules\Deployer\Http\Requests\ModuleReleaseRequest;
use Modules\Deployer\Http\Requests\ModuleRequest;
use Modules\Yasna\Services\YasnaController;

class ModuleController extends YasnaController
{
    protected $view_folder = "deployer::modules";
    protected $row_view    = "row";
    protected $base_model  = "deployer_module";

    private $general_arguments;



    /**
     * ModuleController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->general_arguments = $this->initializeGeneralArgument();
    }



    /**
     * Return list of all modules
     * return Response
     */
    public function index()
    {
        $args = $this->generateArguments(
             [
                  'models'   => model('deployer_module')
                       ->whereNull('deleted_at')
                       ->paginate(10),
                  'headings' => [
                       trans('deployer::module.name'),
                       trans('deployer::module.created_at'),
                       trans('deployer::module.repository'),
                       trans('deployer::dashboard.operations'),
                  ],
             ]
        );

        return $this->view('index', $args);
    }



    /**
     * Create an module
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        $model       = model('deployer-module');
        $title_label = trans('deployer::module.create_a_new_module');

        return $this->view('deployer::modules.form', compact('model', 'title_label'));
    }



    /**
     * Edit general information of a module
     *
     * @param string $module_id Hash ID of module which wanna edit
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($module_id)
    {
        /* @var DeployerModule $model */
        $model = model('deployer_module', $module_id);

        if (!$model->exists || !$model->canEdit()) {
            return $this->jsonFeedback(trans('deployer::module.module_not_found'));
        }

        $title_label = trans('deployer::module.edit_module_title', ['name' => $model->name]);

        return $this->view('form', compact('model', 'title_label'));
    }



    /**
     * Create or update a module
     *
     * @param ModuleRequest $request
     *
     * @return string
     */
    public function save(ModuleRequest $request)
    {
        $model = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback(
             $model->exists,
             [
                  'success_callback' => "rowUpdate('tblModules','$request->hashid')",
             ]
        );
    }



    /**
     * Return content of confirm modal for deciding about deleting a module
     *
     * @param string $module_id Hash ID of module which wanna delete
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirm($module_id)
    {
        /* @var DeployerModule $model */
        $model       = model('deployer_module', $module_id);
        $title_label = trans('deployer::module.delete_module', ['name' => $model->name]);

        if (!$model->exists || !$model->canDelete()) {
            return $this->jsonFeedback(trans('deployer::module.module_not_found'));
        }

        return $this->view('delete', compact('model', 'title_label'));
    }



    /**
     * Trash a project (safe delete)
     *
     * @param string $project_id Hash ID of project which wanna delete
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($project_id)
    {
        /* @var DeployerModule $model */
        $model = model('deployer_module', $project_id);
        $name  = $model->name;

        if (!$model->exists || !$model->canDelete()) {
            return $this->abort('410');
        }

        $deleted = $model->delete();

        return $this->jsonAjaxSaveFeedback(
             $deleted,
             [
                  'success_message' => trans(
                       'deployer::module.module_has_been_deleted_successfully',
                       ['name' => $name]
                  ),
                  'success_refresh' => 1,
             ]
        );
    }



    /**
     * Show a unique callback url for adding a new release to the module
     *
     * @param string $module_id
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function hookLink(string $module_id)
    {
        /* @var DeployerModule $model */
        $model       = model('deployer_module', $module_id);
        $title_label = trans('deployer::module.hook_link_title', ['name' => $model->name]);

        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::module.module_not_found'));
        }

        return $this->view('hook_link', compact('model', 'title_label'));

    }



    /**
     * Save a new release of a module
     *
     * @param ModuleReleaseRequest $request
     * @param string               $token
     *
     * @return string Message response for sending to origin caller
     * @throws \Exception
     */
    public function addRelease(ModuleReleaseRequest $request, string $token)
    {
        // decrypt release token and grab corresponded model
        $model_id = hashid_deployer($token);
        /* @var DeployerModule $model */
        $model           = model('deployer_module', $model_id);
        $release         = $request->get('release');
        $feedbackMessage = "There are some errors on adding a new release ({$release['tag_name']}) to module({$token})";

        if ($model->exists && $model->saveNewRelease($release['tag_name'])) {
            $feedbackMessage = "New release ({$release['tag_name']}) has been successfully published for the module({$token})";
        }

        return $this->jsonFeedback($feedbackMessage);
    }



    /**
     * List all versions of a module
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function listVersions(string $hashid)
    {
        $model = model('deployer_module', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::module.module_not_found'));
        }

        $title_label = trans('deployer::module.versions_title', ['name' => $model->name]);
        $models      = $model->versions()->orderBy('version', 'DESC')->get(); // get all versions of current module
        $headings    = [
             trans('deployer::module.version'),
             trans('deployer::module.release_date'),
        ];

        return $this->view('versions', compact('models', 'title_label', 'headings'));
    }



    /**
     * Generate general information arguments
     *
     * @param array $arguments
     *
     * @return array
     */
    private function generateArguments(array $arguments): array
    {
        return array_merge_recursive($this->general_arguments, $arguments);
    }



    /**
     * Initialize general information as view parameters
     *
     * @return array
     */
    private function initializeGeneralArgument(): array
    {
        return [
             "page"       => [
                  ['deployer', trans("deployer::dashboard.Deployer")],
                  ['module', trans("deployer::dashboard.Modules")],
             ],
             "controller" => $this,
        ];
    }
}
