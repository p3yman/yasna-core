<?php

namespace Modules\Deployer\Http\Controllers;

use App\Models\DeployerProject;
use App\Models\DeployerProjectVersion;
use Modules\Deployer\Contracts\DeployHistoryRepository;
use Modules\Deployer\Http\Requests\DeleteReleaseRequest;
use Modules\Yasna\Services\YasnaController;

use function master_modal;

class ProjectReleaseController extends YasnaController
{
    protected $view_folder = "deployer::releases";
    protected $row_view    = "version_row";
    protected $base_model  = "deployer_project_version";



    /**
     * List all versions of a project
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function releasesList(string $hashid)
    {
        // first check project with this hash ID exists or not, if not return an error
        /* @var DeployerProject $project_model */
        $project_model = model('deployer_project', $hashid);
        if (!$project_model->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        // get all versions of this project
        $models = $project_model->versions()->orderBy('version', 'DESC')->limit(10)->get();

        $title_label = trans('deployer::project.release_modal_title', ['name' => $project_model->name]);
        $headings    = [
             trans('deployer::project.version'),
             trans('deployer::project.creation_date'),
             trans('deployer::project.release_date'),
             trans('deployer::project.release_status'),
             trans('deployer::dashboard.operations'),
        ];

        return $this->view('releases', compact('title_label', 'headings', 'models'));
    }



    /**
     * Show a confirm box for make an assurance about finalizing the release
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function closeReleaseConfirm(string $hashid)
    {
        // first get version model of project
        /* @var DeployerProjectVersion $version */
        $version = model('deployer_project_version', $hashid);
        if (!$version->exists) {
            return $this->jsonFeedback(trans('deployer::project.release_not_found'));
        }

        if ($version->is_a_closed_release) {
            return $this->jsonFeedback(trans('deployer::project.could_not_modify_a_closed_release'));
        }

        $model       = $version->project;
        $title_label = trans(
             'deployer::project.close_release_of_project',
             ['name' => $model->name, 'version' => pd($version)]
        );

        return $this->view('close_release', compact('model', 'version', 'title_label'));
    }



    /**
     * Close a given project release and make an open candidate
     *
     * @param string $hashid
     *
     * @return array|string
     */
    public function closeRelease(string $hashid)
    {
        // first get version model of project
        /* @var DeployerProjectVersion $version */
        $version = model('deployer_project_version', $hashid);
        if (!$version->exists) {
            return $this->jsonFeedback(trans('deployer::project.release_not_found'));
        }

        if ($version->is_a_closed_release) {
            return $this->jsonFeedback(trans('deployer::project.could_not_modify_a_closed_release'));
        }

        // get project of the release
        $project = $version->project()->with('openVersion')->first();

        // try to close this release and release a new open version
        if (($released = $version->closeRelease()) === true) {
            $released = $project->attachNewVersion(true);

            $version->replicateModules($released);
        }

        return [
             'callback' => sprintf(
                  "masterModal('%s')",
                  route('deployer.project.releases', ['hashid' => $project->hashid], false)
             ),
        ];
    }



    /**
     * Delete confirmation for remove a release from DB and clients
     *
     * @param DeleteReleaseRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteReleaseConfirm(DeleteReleaseRequest $request)
    {
        $model = $request->model;
        if (!$model->is_a_closed_release) {
            return $this->jsonFeedback(trans('deployer::project.can_not_delete_an_open_release'));
        }

        if ($model->is_selected_release) {
            return $this->jsonFeedback(trans('deployer::project.can_not_delete_a_selected_release'));
        }

        $title = trans(
             'deployer::project.delete_release_of_project',
             ['project' => $model->project->name, 'version' => pd($model->version)]
        );

        return $this->view('delete_release', compact('model', 'title'));

    }



    /**
     * delete a release from DB and client servers
     *
     * @param DeleteReleaseRequest    $request
     * @param DeployHistoryRepository $history
     *
     * @return array|string
     */
    public function deleteRelease(DeleteReleaseRequest $request, DeployHistoryRepository $history)
    {
        $model  = $request->model;
        $hashid = $model->project->hashid;

        if ($model->is_selected_release) {
            return $this->jsonFeedback(trans('deployer::project.can_not_delete_a_selected_release'));
        }

        if ($request->model->delete()) {
            // save a delete history record
            $history->saveRawHistory($model, DeployHistoryRepository::TYPE_DELETE);

            return [
                 'callback' => master_modal('deployer.project.releases', ['hashid' => $hashid], false),
            ];
        }

        return $this->jsonFeedback(trans('deployer::project.error_on_deleting_release'));
    }
}
