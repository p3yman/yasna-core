<?php

namespace Modules\Deployer\Http\Controllers;

use App\Models\DeployerModule;
use App\Models\DeployerProjectModule;
use App\Models\DeployerProjectVersion;
use Modules\Deployer\Http\Requests\ProjectModuleRequest;
use Modules\Yasna\Services\YasnaController;

use function master_modal;

class ProjectModuleController extends YasnaController
{
    protected $base_model  = "DeployerProjectModule";
    protected $view_folder = "deployer::projects";
    protected $row_view    = "release_modules_row";



    /**
     * Return list of modules of a release
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function moduleList(string $hashid)
    {
        // first get project version model
        /* @var DeployerProjectVersion $version */
        $version = model('deployer_project_version', $hashid);
        // check for release existence
        if (!$version->exists) {
            return $this->jsonFeedback(trans('deployer::project.release_not_found'));
        }
        // then get list of all module attached to this release
        /* @var DeployerProjectModule[] $models */
        $models      = $version->modulesInformation;
        $project     = $version->project;
        $title_label = trans(
             'deployer::project.release_modules_modal_title',
             ['name' => $version->project->name, 'version' => pd($version->version)]
        );
        $headings    = [
             trans('deployer::module.name'),
             trans('deployer::module.version'),
             trans('deployer::module.release_date'),
        ];

        if ($version->is_an_open_release) {
            $headings[] = trans('deployer::dashboard.operations');
        }

        return $this->view('release_modules', compact('title_label', 'headings', 'models', 'version', 'project'));
    }



    /**
     * Return a form to adding a module to a given release
     *
     * @param string $hashid project release token
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function addModule(string $hashid)
    {
        // first get project version model
        /* @var DeployerProjectVersion $version */
        $version = model('deployer_project_version', $hashid);
        // check for release existence
        if (!$version->exists) {
            return $this->jsonFeedback(trans('deployer::project.release_not_found'));
        }

        $get_version_url = route('deployer.project.module_versions', ['hashid' => ''], false);
        $project         = $version->project;
        $title_label     = trans(
             'deployer::project.add_module_to_release_title',
             ['name' => $project->name, 'version' => pd($version->version)]
        );

        return $this->view(
             'add_module_form',
             compact('title_label', 'headings', 'version', 'project', 'get_version_url')
        );
    }



    /**
     * Add a module to the release
     *
     * @param ProjectModuleRequest $request
     * @param string               $hashid
     *
     * @return string|array
     */
    public function saveModule(ProjectModuleRequest $request, string $hashid)
    {
        // first get project version model
        /* @var DeployerProjectVersion $version */
        $version = model('deployer_project_version', $hashid);
        // check for release existence
        if (!$version->exists) {
            return $this->jsonFeedback(trans('deployer::project.release_not_found'));
        }

        if ($version->is_a_closed_release) {
            return $this->jsonFeedback(trans('deployer::project.could_not_modify_a_closed_release'));
        }

        $addedModule = $version->addModuleToVersion($request);

        if ($addedModule->exists) {
            return [
                 'callback' => master_modal('deployer.project.module_list', ['hashid' => $hashid], false),
            ];
        }

        return $this->jsonFeedback(trans('deployer::project.problem_on_adding_module_to_release'));
    }



    /**
     * Edit version of an attached module of a release
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function editModuleVersion(string $hashid)
    {
        /* @var DeployerProjectModule $model */
        $model = model('deployer_project_module', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.module_not_found_in_project'));
        }

        $versions    = $this->moduleVersions($model->module->id, false);
        $title_label = trans('deployer::project.edit_module_version_title', ['name' => $model->module->name]);

        return $this->view('edit_module_form', compact('model', 'versions', 'title_label'));
    }



    /**
     * Confirm to delete a module from a release
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function removeModuleConfirm(string $hashid)
    {
        /* @var DeployerProjectModule $model */
        $model = model('deployer_project_module', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.module_not_found_in_project'));
        }


        $title_label = trans('deployer::project.remove_module', ['name' => $model->name]);

        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.module_not_found_in_project'));
        }

        return $this->view('delete_module', compact('model', 'title_label'));
    }



    /**
     * Remove a module from the release
     *
     * @param string $hashid
     *
     * @return string|array
     */
    public function removeModule(string $hashid)
    {
        // get attached module to release
        $model = model('deployer_project_module', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.module_not_found_in_project'));
        }

        // ensure release is not closed
        if ($model->release->is_a_closed_release) {
            return $this->jsonFeedback(trans('deployer::project.could_not_modify_a_closed_release'));
        }

        $release = $model->release;
        $deleted = $model->delete();

        return [
             'callback' => master_modal('deployer.project.module_list', ['hashid' => $release->hashid], false),
        ];
    }



    /**
     * List all available versions of a modules
     *
     * @param string $hashid
     * @param bool   $return_errors
     *
     * @return string|array
     */
    public function moduleVersions(string $hashid, $return_errors = true)
    {
        // first get module
        /* @var DeployerModule $version */
        $module = model('deployer_module', $hashid);
        // check for module existence
        if (!$module->exists) {
            if ($return_errors) {
                return $this->jsonFeedback(trans('deployer::module.module_not_found'));
            }

            return [];
        }

        return $module->versions->map(
             function ($version) {
                 return [
                      'id'      => $version->hashid,
                      'version' => pd($version->version),
                 ];
             }
        );
    }
}
