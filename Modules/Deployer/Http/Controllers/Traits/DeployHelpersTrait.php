<?php

namespace Modules\Deployer\Http\Controllers\Traits;

use App\Models\DeployerDeployHistory;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

use function array_map;

trait DeployHelpersTrait
{
    /**
     * get page information
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    protected function getPageInformation(SimpleYasnaRequest $request)
    {
        $tab = $request->tab ?? 'all';

        $page[0] = [
             "deployer/deploys",
             trans('deployer::project.deploy_page_title_all'),
        ];

        if ($request->keyword) {
            $page[1] = [
                 $tab,
                 trans('deployer::project.search-result'),
                 route('deployer.project.all_deploys', ['keyword' => $request->keyword], false),
            ];

            return $page;
        }

        $page[1] = [
             $tab,
             trans("deployer::project.deploy_type_$tab"),
             $tab,
        ];

        return $page;
    }



    /**
     * Generate ready-to-use information for grid tabs
     *
     * @param array $data
     *
     * @return array
     */
    protected function getTabsInformation(array $data): array
    {
        return array_map(
             function ($tab) {
                 return $this->getTabInformation($tab);
             },
             $data
        );
    }



    /**
     * Generate data of a tab
     *
     * @param string $tab
     *
     * @return array
     */
    protected function getTabInformation(string $tab): array
    {
        return [
             'caption' => trans("deployer::project.deploy_type_$tab"),
             'url'     => $tab,
        ];
    }



    /**
     * get elector filter
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    protected function getElectors(SimpleYasnaRequest $request): array
    {
        $tab = $request->tab ?? 'all';

        return [
             'status' => $this->getStatusBasedOnTab($tab),
        ];
    }



    /**
     * get status types based on tab
     *
     * @param string|null $tab
     *
     * @return array|null
     */
    protected function getStatusBasedOnTab(?string $tab): ?array
    {
        switch ($tab) {
            case 'all':
                return null;
            case 'pending':
                return [
                     DeployerDeployHistory::STATUS_PENDING_JUST_DEPLOY,
                     DeployerDeployHistory::STATUS_PENDING_DEMO,
                     DeployerDeployHistory::STATUS_PENDING_FINAL,
                     DeployerDeployHistory::STATUS_PENDING_DELETE,
                ];
            case 'running':
                return [
                     DeployerDeployHistory::STATUS_IN_PROGRESS,
                ];
            case 'done':
                return [
                     DeployerDeployHistory::STATUS_COMPLETE,
                ];
            case 'failed':
                return [
                     DeployerDeployHistory::STATUS_FAILED,
                ];
            case 'canceled':
                return [
                     DeployerDeployHistory::STATUS_CANCELED,
                ];
            default:
                return [-999];
        }
    }
}
