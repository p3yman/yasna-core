<?php

namespace Modules\Deployer\Http\Controllers;

use App\Models\DeployerMacro;
use Modules\Deployer\Contracts\DeployerMacroRepository;
use Modules\Deployer\Http\Requests\HostMacroListRequest;
use Modules\Deployer\Http\Requests\MacroActionRequest;
use Modules\Deployer\Http\Requests\MacroDeleteRequest;
use Modules\Deployer\Http\Requests\MacroListRequest;
use Modules\Deployer\Http\Requests\MacroSaveRequest;
use Modules\Yasna\Services\YasnaController;

use function hashid;
use function is_null;
use function compact;

class ProjectMacroController extends YasnaController
{
    protected $base_model  = "deployer-macro";
    protected $view_folder = "deployer::macros";



    /**
     * show list of macros of a project
     *
     * @param MacroListRequest        $request
     * @param DeployerMacroRepository $repository
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(MacroListRequest $request, DeployerMacroRepository $repository)
    {
        $project = $request->model;
        $models  = $repository->get($project->id);

        $title_label = trans('deployer::macro.title_label', ['project' => $project->name]);
        $headings    = [
             trans('deployer::macro.macro'),
             trans('deployer::macro.value'),
             trans('deployer::macro.is_host_overridden'),
        ];

        return $this->view('index', compact('project', 'models', 'headings', 'title_label'));
    }



    /**
     * show list of macros of a host project
     *
     * @param HostMacroListRequest    $request
     * @param DeployerMacroRepository $repository
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function hostIndex(HostMacroListRequest $request, DeployerMacroRepository $repository)
    {
        $hp_model = $request->model;
        $host     = $hp_model->host;
        $project  = $hp_model->project;
        $models   = $repository->get($project->id, $hp_model->id);

        $title_label = trans('deployer::macro.host_title_label', ['project' => $project->name, 'host' => $host->name]);
        $headings    = [
             trans('deployer::macro.macro'),
             trans('deployer::macro.value'),
             trans('deployer::macro.is_host_overridden'),
        ];

        return $this->view('host-index', compact('project', 'models', 'headings', 'title_label', 'hp_model'));
    }



    /**
     * show form for creating/editing a macro
     *
     * @param MacroActionRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function action(MacroActionRequest $request)
    {
        if (!$this->acceptableActions($request->action)) {
            return $this->jsonFeedback(trans('deployer::macro.no_acceptable_action'));
        }

        $model   = $request->model;
        $project = $model->project;
        if (!$project->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $modal_title = trans("deployer::macro.{$request->action}_macro_title", ['project' => $project->name]);

        return $this->view('form', compact('model', 'modal_title', 'project'));
    }



    /**
     * Override a macro on a host
     *
     * @param MacroActionRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function overrideModal(MacroActionRequest $request)
    {
        $model    = $request->model;
        $project  = $model->project;
        $hp_model = model('deployer-host-project', $request->host);
        if (!$model->exists or !$project->exists or !$hp_model->exists) {
            return $this->jsonFeedback(trans('deployer::macro.macro_not_found'));
        }

        $modal_title = trans("deployer::macro.override_macro_title",
             ['project' => $project->name, 'host' => $hp_model->host->name]);

        return $this->view('override-form', compact('model', 'modal_title', 'project', 'hp_model'));
    }



    /**
     * save a macro
     *
     * @param MacroSaveRequest $request
     *
     * @return array
     */
    public function save(MacroSaveRequest $request)
    {
        /* @var DeployerMacro $model */
        $model  = $request->model->batchSave($request, ['old_macro']);
        $hashid = hashid($model->project_id);

        if ($request->macroIsChanged()) {
            $request->model->updateOverriddenMacros($model->macro, $request->old_macro);
        }

        return [
             'callback' => master_modal('deployer.macros.index', ['hashid' => $hashid], false),
        ];
    }



    /**
     * override a macro
     *
     * @param MacroSaveRequest        $request
     * @param DeployerMacroRepository $repository
     *
     * @return array|string
     */
    public function override(MacroSaveRequest $request, DeployerMacroRepository $repository)
    {
        if (!$request->model->exists || empty($request->host_project_id)) {
            return $this->jsonFeedback(trans('deployer::macro.macro_not_found'));
        }

        $repository->override($request->model->id, $request->host_project_id, $request->value);

        return [
             'callback' => master_modal('deployer.macros.server_index', ['hashid' => hashid($request->host_project_id)], false),
        ];
    }



    /**
     * show delete confirm box
     *
     * @param MacroDeleteRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirm(MacroDeleteRequest $request)
    {
        $model = $request->model;
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::macro.macro_not_found'));
        }

        return $this->view('delete', compact('model'));
    }



    /**
     * show delete an overridden confirm box
     *
     * @param MacroDeleteRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function overrideDeleteConfirm(MacroDeleteRequest $request)
    {
        $model = $request->model;
        if (!$model->exists or is_null($model->host_project_id)) {
            return $this->jsonFeedback(trans('deployer::macro.macro_not_found'));
        }

        return $this->view('override-delete', compact('model'));
    }



    /**
     * delete a set of macros
     *
     * @param MacroDeleteRequest $request
     *
     * @return array|string
     */
    public function delete(MacroDeleteRequest $request)
    {
        $model  = $request->model;
        $hashid = hashid($model->project_id);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::macro.macro_not_found'));
        }

        if ($model->delete()) {
            $model->hostOverridden()->delete();
        }

        return [
             'callback' => master_modal('deployer.macros.index', ['hashid' => $hashid], false),
        ];
    }



    /**
     * delete an overridden macro
     *
     * @param MacroDeleteRequest $request
     *
     * @return array|string
     */
    public function overrideDelete(MacroDeleteRequest $request)
    {
        $model  = $request->model;
        $hashid = hashid($model->host_project_id);
        if (!$model->exists or is_null($model->host_project_id)) {
            return $this->jsonFeedback(trans('deployer::macro.macro_not_found'));
        }

        $model->delete();

        return [
             'callback' => master_modal('deployer.macros.server_index', ['hashid' => $hashid], false),
        ];
    }



    /**
     * ensure action is acceptable
     *
     * @param string|null $action
     *
     * @return bool
     */
    private function acceptableActions(?string $action): bool
    {
        return in_array($action, ['edit', 'create']);
    }
}
