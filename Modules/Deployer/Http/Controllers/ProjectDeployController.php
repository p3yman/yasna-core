<?php

namespace Modules\Deployer\Http\Controllers;

use App\Models\DeployerDeployHistory;
use Modules\Deployer\Contracts\DeployHistoryRepository;
use Modules\Deployer\Entities\DeployerProjectVersion;
use Modules\Deployer\Http\Controllers\Traits\DeployHelpersTrait;
use Modules\Deployer\Http\Requests\CancelReleaseRequest;
use Modules\Deployer\Http\Requests\SelectReleaseRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

use function not_in_array;
use function redirect;
use function route;
use function sprintf;

class ProjectDeployController extends YasnaController
{
    use DeployHelpersTrait;

    protected $base_model  = "deployer_deploy_history";
    protected $view_folder = "deployer::deploy";
    protected $row_view    = "row";



    /**
     * return deployment history of a project
     *
     * @param string|int $hash_id Hashid or ID of project
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function index($hash_id)
    {
        // get the project model
        $project = model('deployer_project', $hash_id);
        if (!$project->exists) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $models      = $project->history()->paginate(10);
        $title_label = trans('deployer::project.deploy_modal_title', ['name' => $project->name]);
        $headings    = [
             trans('deployer::project.version'),
             trans('deployer::project.created_at'),
             trans('deployer::project.deploy_type'),
             trans('deployer::project.status'),
             trans('deployer::dashboard.operations'),
        ];

        return $this->view('index', compact('title_label', 'headings', 'models', 'project'));
    }



    /**
     * show all deploys records
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function all(SimpleYasnaRequest $request)
    {
        $tab  = $request->tab ?? 'all';
        $tabs = ['all', 'pending', 'running', 'done', 'failed', 'canceled'];
        if (not_in_array($tab, $tabs)) {
            return $this->abort(404);
        }

        $electors = $this->getElectors($request);
        $models   = model($this->base_model)->elector($electors)
                                            ->with('release')
                                            ->orderByDesc('updated_at')
                                            ->paginate()
        ;

        $title     = trans("deployer::project.deploy_page_title_{$tab}");
        $headings  = [
             trans('deployer::project.project'),
             trans('deployer::project.version'),
             trans('deployer::project.created_at'),
             trans('deployer::project.updated_at'),
             trans('deployer::project.deploy_type'),
             trans('deployer::project.status'),
             trans('deployer::dashboard.operations'),
        ];
        $tabs_data = $this->getTabsInformation($tabs);
        $page      = $this->getPageInformation($request);

        return $this->view('deploys', compact('title', 'headings', 'models', 'tabs_data', 'tab', 'page'));
    }



    /**
     * Return detail list of a deployment process
     *
     * @param string|int $hashid Hashid or id of deploy history
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deployDetails($hashid)
    {
        // get the project model
        $model = model('deployer_deploy_history', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('deployer::project.deploy_history_not_found'));
        }

        $models      = $model->hosts;
        $project     = $model->project;
        $release     = $model->release()->withTrashed()->first();
        $title_label = trans(
             'deployer::project.deploy_detail_modal_title',
             [
                  'name'    => $release->project->name,
                  'version' => pd($release->version),
             ]
        );
        $headings    = [
             trans('deployer::host.name'),
             trans('deployer::project.last_deploy_state_update_time'),
             trans('deployer::project.deploy_status'),
             trans('deployer::project.deploy_details'),
        ];

        return $this->view('deploys_list', compact('title_label', 'headings', 'models', 'project', 'model'));
    }



    /**
     * Set a ready-for-deploy flag for deployer
     *
     * @param SelectReleaseRequest    $request
     * @param DeployHistoryRepository $history
     *
     * @return string|array
     */
    public function deployRelease(SelectReleaseRequest $request, DeployHistoryRepository $history)
    {
        /* @var $model DeployerProjectVersion */
        $model = $request->model;
        $type  = $request->input('type');

        // save a deployment history record
        $history->saveRawHistory($model, $type);

        return [
             'callback' => sprintf(
                  "masterModal('%s')",
                  route('deployer.project.deploys_list', ['hashid' => $model->project->hashid], false)
             ),
        ];
    }



    /**
     * Select a release as demo or final release
     *
     * @param SelectReleaseRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function selectAsReleaseConfirm(SelectReleaseRequest $request)
    {
        /* @var $model DeployerProjectVersion */
        $model      = $request->model;
        $type       = $request->input('type');
        $release_as = trans(sprintf("deployer::project.as_%s", $type));

        return $this->view('deploy_confirm', compact('model', 'type', 'release_as'));
    }



    /**
     * confirm to cancel a deployment request
     *
     * @param CancelReleaseRequest $request
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function cancelConfirm(CancelReleaseRequest $request)
    {
        $model = $request->model;
        if (!$model->is_pending_status) {
            return $this->jsonFeedback(trans('deployer::project.just_pending_request_can_be_canceled'));
        }

        return $this->view('cancel_confirm', compact('model'));
    }



    /**
     * cancel a request
     *
     * @param CancelReleaseRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     */
    public function cancel(CancelReleaseRequest $request)
    {
        $model = $request->model;
        if (!$model->is_pending_status) {
            return $this->jsonFeedback(trans('deployer::project.just_pending_request_can_be_canceled'));
        }

        $model->status = DeployerDeployHistory::STATUS_CANCELED;
        if ($model->save()) {
            return $this->jsonAjaxSaveFeedback(true, [
                 'success_redirect' => route('deployer.deploy.all_deploys', ['tab' => 'canceled']),
            ]);
        }

        return $this->jsonFeedback(trans('deployer::project.problem_on_canceling_the_request'));
    }
}
