<?php

namespace Modules\Deployer\Http\Controllers;

use Modules\Deployer\Entities\DeployerProject;
use Modules\Deployer\Http\Requests\ProjectRequest;
use Modules\Yasna\Services\YasnaController;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class ProjectController
 *
 * @package Modules\Deployer\Http\Controllers
 */
class ProjectController extends YasnaController
{
    protected $view_folder = "deployer::projects";
    protected $row_view    = "row";
    protected $base_model  = "deployer_project";

    private $general_arguments;



    /**
     * ProjectController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->general_arguments = $this->initializeGeneralArgument();
    }



    /**
     * Return list of all projects
     * return Response
     */
    public function index()
    {
        $models = model('deployer_project')->whereNull('deleted_at')
                                           ->with([
                                                'demoVersion',
                                                'demoVersion.moduleVersions',
                                                'demoVersion.modulesInformation',
                                                'demoVersion.moduleVersions.sisterVersions',
                                                'openVersion',
                                                'openVersion.moduleVersions',
                                                'openVersion.modulesInformation',
                                                'openVersion.moduleVersions.sisterVersions',
                                                'currentVersion',
                                                'currentVersion.moduleVersions',
                                                'currentVersion.modulesInformation',
                                                'currentVersion.moduleVersions.sisterVersions',
                                           ])
                                           ->paginate(10)
        ;
        $args   = $this->generateArguments(
             [
                  'models'   => $models,
                  'headings' => [
                       trans('deployer::project.name'),
                       trans('deployer::project.created_at'),
                       trans('deployer::project.support_date'),
                       trans('deployer::project.current_version'),
                       trans('deployer::project.demo_version'),
                       trans('deployer::project.open_version'),
                       trans('deployer::dashboard.operations'),
                  ],
             ]
        );

        return $this->view('index', $args);
    }



    /**
     * Create a new project
     * return Response
     */
    public function create()
    {
        $model       = model('deployer-project');
        $title_label = trans('deployer::project.create_a_new_project');

        return $this->view('deployer::projects.form', compact('model', 'title_label'));
    }



    /**
     * @param string $project_id Hash ID of project which wanna edit
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($project_id)
    {
        /* @var DeployerProject $model */
        $model = model('deployer_project', $project_id);

        if (!$model->exists || !$model->canEdit()) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        $title_label = trans('deployer::project.edit_project_title', ['name' => $model->name]);

        return $this->view('form', compact('model', 'title_label'));
    }



    /**
     * Save project model on create or update action
     *
     * @param ProjectRequest $request
     *
     * @return string
     */
    public function save(ProjectRequest $request)
    {
        /* @var DeployerProject $projectModel */
        // save project model
        $projectModel = $request->model->batchSave($this->getProjectData($request));

        // then check for open version of this project, if doesn't exist, create one
        if ($projectModel->hasNotOpenVersion()) {
            $projectModel->attachNewVersion(true);
        }

        return $this->jsonAjaxSaveFeedback(
             $projectModel->exists,
             [
                  'success_callback' => "rowUpdate('tblProjects','$request->hashid')",
             ]
        );
    }



    /**
     * Return content of confirm modal for deciding about deleting a project
     *
     * @param string $project_id Hash ID of project which wanna delete
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirm($project_id)
    {
        /* @var DeployerProject $model */
        $model       = model('deployer_project', $project_id);
        $title_label = trans('deployer::project.delete_project', ['name' => $model->name]);

        if (!$model->exists || !$model->canDelete()) {
            return $this->jsonFeedback(trans('deployer::project.project_not_found'));
        }

        return $this->view('delete', compact('model', 'title_label'));
    }



    /**
     * Trash a project (safe delete)
     *
     * @param string $project_id Hash ID of project which wanna delete
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($project_id)
    {
        /* @var DeployerProject $model */
        $model = model('deployer_project', $project_id);
        $name  = $model->name;

        if (!$model->exists || !$model->canDelete()) {
            return $this->abort('410');
        }

        $deleted = $model->delete();

        return $this->jsonAjaxSaveFeedback(
             $deleted,
             [
                  'success_message' => trans(
                       'deployer::project.project_has_been_deleted_successfully',
                       ['name' => $name]
                  ),
                  'success_refresh' => 1,
             ]
        );
    }



    /**
     * @inheritdoc
     */
    public function update($model_hashid, $spare = null)
    {
        $model = model($this->base_model)->with(['currentVersion', 'openVersion', 'demoVersion'])->first();
        if (!$model or !$model->exists) {
            return $this->abort(410, true);
        }

        return $this->view($this->row_view, compact('model'));
    }



    /**
     * Generate argument for view based on a default set
     *
     * @param array $arguments
     *
     * @return array
     */
    private function generateArguments(array $arguments): array
    {
        return array_merge_recursive($this->general_arguments, $arguments);
    }



    /**
     * Initialize general information as view parameters
     *
     * @return array
     */
    private function initializeGeneralArgument(): array
    {
        return [
             "page"       => [
                  ['deployer', trans("deployer::dashboard.Deployer")],
                  ['project', trans("deployer::dashboard.Projects")],
             ],
             "controller" => $this,
        ];
    }



    /**
     * Correct project saving data and return it
     *
     * @param YasnaRequest $request
     *
     * @return array
     */
    private function getProjectData(YasnaRequest $request): array
    {
        $data = $request->toArray();

        if (!isset($data["update_check_at"]) or strlen($data["update_check_at"]) == 0) {
            $data["update_check_at"] = null;
        }

        return $data;
    }
}
