<?php

namespace Modules\Deployer\Http\Controllers;

use Modules\Deployer\Entities\DeployerHost;
use Modules\Deployer\Http\Requests\HostRequest;
use Modules\Yasna\Services\YasnaController;

/**
 * Class HostController
 *
 * @package Modules\Deployer\Http\Controllers
 */
class HostController extends YasnaController
{
    protected $view_folder = "deployer::hosts";
    protected $row_view    = "row";
    protected $base_model  = "deployer_host";

    private $general_arguments;



    /**
     * HostController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->general_arguments = $this->initializeGeneralArgument();
    }



    /**
     * Return list of all hosts
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $args = $this->generateArguments([
            //            'page' => [['index', trans("deployer::dashboard.Index")]],
            'models'   => model('deployer_host')->whereNull('deleted_at')->limitToCurrentUser()->paginate(10),
            'headings' => [
                 trans('deployer::host.name'),
                 trans('deployer::host.created_at'),
                 trans('deployer::host.last_status'),
                 trans('deployer::dashboard.operations'),
            ],
        ]);

        return $this->view('index', $args);
    }



    /**
     * Create a new host
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        $model       = model('deployer_host');
        $title_label = trans('deployer::host.add_a_new_server');

        return $this->view('form', compact('model', 'title_label'));
    }



    /**
     * Save host model on create or update action
     *
     * @param HostRequest $request
     *
     * @return string
     */
    public function save(HostRequest $request)
    {
        $model = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($model->exists, [
             'success_callback' => "rowUpdate('tblHosts','$request->hashid')",
        ]);
    }



    /**
     * Edit information of a saved host
     *
     * @param int $host_id ID of host
     *
     * @return string
     */
    public function edit($host_id)
    {
        /* @var $model DeployerHost */
        $model = model('deployer_host', $host_id);

        if (!$model->exists || !$model->canEdit()) {
            return $this->jsonFeedback(trans('deployer::host.host_not_found'));
        }

        $title_label = trans('deployer::host.edit_a_server', ['name' => $model->name]);
        return $this->view('form', compact('model', 'title_label'));
    }



    /**
     * Return content of confirm modal for deciding about deleting a host
     *
     * @param int $host_id ID of host
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirm($host_id)
    {
        /* @var $model DeployerHost */
        $model       = model('deployer_host', $host_id);
        $title_label = trans('deployer::host.delete_host', ['name' => $model->name]);

        if (!$model->exists || !$model->canDelete()) {
            return $this->jsonFeedback(trans('deployer::host.host_not_found'));
        }

        return $this->view('delete', compact('model', 'title_label'));
    }



    /**
     * Trash a host (safe delete)
     *
     * @param int $host_id ID of host to delete
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($host_id)
    {
        /* @var $model DeployerHost */
        $model = model('deployer_host', $host_id);
        $name  = $model->name;

        if (!$model->exists || !$model->canDelete()) {
            return $this->abort('410');
        }

        $deleted = $model->delete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_message' => trans('deployer::host.host_has_been_deleted_successfully', ['name' => $name]),
             'success_refresh' => 1,
        ]);
    }



    /**
     * Generate arguments based on some default parameters
     *
     * @param array $arguments
     *
     * @return array
     */
    private function generateArguments(array $arguments): array
    {
        return array_merge_recursive($this->general_arguments, $arguments);
    }



    /**
     * Initialize general information as view parameters
     *
     * @return array
     */
    private function initializeGeneralArgument(): array
    {
        return [
             "page"       => [
                  ['deployer', trans("deployer::dashboard.Deployer")],
                  ['server', trans("deployer::dashboard.Servers")],
             ],
             "controller" => $this,
        ];
    }
}
