<?php

use Modules\Deployer\Providers\DeployerServiceProvider as Deployer;

Route::group([
     'middleware' => ['web', 'auth'],
     'prefix'     => 'manage/deployer',
     'namespace'  => 'Modules\Deployer\Http\Controllers',
], function () {
    // Main page of module
    Route::get('/', 'DeployerController@index')
         ->name('deployer.deployer.index')
         ->middleware(Deployer::CanMiddleware([Deployer::RULE_COMMON_STATISTICS]))
    ;

    /*-----------------------------------------------
     | Project Routes
     */
    Route::get('/project', 'ProjectController@index')
         ->name('deployer.project.index')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_BROWSE]))
    ;
    Route::get('/reload_project/{hashid}', 'ProjectController@update')
         ->name('deployer.project.reload_project')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_BROWSE]))
    ;
    Route::get('/project/create', 'ProjectController@create')
         ->name('deployer.project.create')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_CREATE]))
    ;
    Route::get('/project/edit/{hash_id}', 'ProjectController@edit')
         ->name('deployer.project.edit')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/save', 'ProjectController@save')
         ->name('deployer.project.save')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_CREATE, Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/delete/{hash_id}', 'ProjectController@deleteConfirm')
         ->name('deployer.project.delete_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DELETE]))
    ;
    Route::post('/project/delete/{hash_id}', 'ProjectController@delete')
         ->name('deployer.project.delete')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DELETE]))
    ;


    /*-----------------------------------------------
     | Project Shared Resources
     */
    Route::get('/project/shared_resources', 'ProjectSharedResourcesController@index')
         ->name('deployer.project.shared_resources_list')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/create_resource', 'ProjectSharedResourcesController@create')
         ->name('deployer.project.shared_resources_create')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/edit_resource', 'ProjectSharedResourcesController@edit')
         ->name('deployer.project.shared_resources_edit')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/save_resource', 'ProjectSharedResourcesController@save')
         ->name('deployer.project.shared_resources_save')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/delete', 'ProjectSharedResourcesController@deleteConfirm')
         ->name('deployer.project.shared_resources_delete_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/delete', 'ProjectSharedResourcesController@delete')
         ->name('deployer.project.shared_resources_delete')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;


    /*-----------------------------------------------
     | Project Releases Routes
     */
    Route::get('/project/releases/{hashid}', 'ProjectReleaseController@releasesList')
         ->name('deployer.project.releases')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_BROWSE]))
    ;
    Route::get('/project/reload_releases/{hashid}', 'ProjectReleaseController@update')
         ->name('deployer.project.reload_releases')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_BROWSE]))
    ;
    Route::get('/project/close_release_confirm/{hashid}', 'ProjectReleaseController@closeReleaseConfirm')
         ->name('deployer.project.close_release_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/close_release/{hashid}', 'ProjectReleaseController@closeRelease')
         ->name('deployer.project.close_release')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/delete_release_confirm', 'ProjectReleaseController@deleteReleaseConfirm')
         ->name('deployer.project.delete_release_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_RELEASE_DELETE]))
    ;
    Route::post('/project/delete_release', 'ProjectReleaseController@deleteRelease')
         ->name('deployer.project.delete_release')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_RELEASE_DELETE]))
    ;


    /*-----------------------------------------------
     | Project Modules Routes
     */
    Route::get('/project/module_list/{hashid}', 'ProjectModuleController@moduleList')
         ->name('deployer.project.module_list')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/module_list_reload/{hashid}', 'ProjectModuleController@update')
         ->name('deployer.project.module_list_reload')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/add_module/{hash_id}', 'ProjectModuleController@addModule')
         ->name('deployer.project.add_module')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/save_module/{hash_id}', 'ProjectModuleController@saveModule')
         ->name('deployer.project.save_module')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/module_versions/{hash_id}', 'ProjectModuleController@moduleVersions')
         ->name('deployer.project.module_versions')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/remove_module_confirm/{hash_id}', 'ProjectModuleController@removeModuleConfirm')
         ->name('deployer.project.remove_module_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/edit_module_version/{hash_id}', 'ProjectModuleController@editModuleVersion')
         ->name('deployer.project.edit_module_version')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/remove_module/{hash_id}', 'ProjectModuleController@removeModule')
         ->name('deployer.project.remove_module')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;


    /*-----------------------------------------------
     | Project Servers Routes
     */
    Route::get('/project/server/{hash_id}', 'ProjectServerController@listProjectServers')
         ->name('deployer.project.server')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/server_reload/{hashid}', 'ProjectServerController@update')
         ->name('deployer.project.server_reload')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/add_server/{hash_id}', 'ProjectServerController@addServerToProject')
         ->name('deployer.project.add_host')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/save_server/{hash_id}', 'ProjectServerController@saveServerInformation')
         ->name('deployer.project.save_server')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/edit_server_info/{hash_id}', 'ProjectServerController@editServerInformation')
         ->name('deployer.project.edit_host_info')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/remove_server_confirm/{hash_id}', 'ProjectServerController@removeHostConfirm')
         ->name('deployer.project.remove_host_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/remove_server/{hash_id}', 'ProjectServerController@removeHost')
         ->name('deployer.project.remove_host')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;


    /*-----------------------------------------------
     | Standalone Deploy Routes
     */
    Route::get('/deploys/{tab?}', 'ProjectDeployController@all')
         ->name('deployer.deploy.all_deploys')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/deploys/cancel/{hashid}', 'ProjectDeployController@cancelConfirm')
         ->name('deployer.deploy.deploy_cancellation_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/deploys/cancel/{hashid}', 'ProjectDeployController@cancel')
         ->name('deployer.deploy.deploy_cancellation')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;

    /*-----------------------------------------------
     | Project Deploy Routes
     */
    Route::get('/project/deploys_list/{hash_id}', 'ProjectDeployController@index')
         ->name('deployer.project.deploys_list')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/deploy_reload/{hashid}', 'ProjectDeployController@update')
         ->name('deployer.project.deploy_reload')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/deploy_details/{hash_id}', 'ProjectDeployController@deployDetails')
         ->name('deployer.deploy.deploy_details')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::post('/project/deploy_release', 'ProjectDeployController@deployRelease')
         ->name('deployer.deploy.do_deploy_release')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/select_as_release', 'ProjectDeployController@selectAsReleaseConfirm')
         ->name('deployer.deploy.select_as_release_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;


    /*-----------------------------------------------
     | Project Commands Routes
     */
    Route::get('/project/commands', 'ProjectCommandController@index')
         ->name('deployer.project.command_list')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/instant_command', 'ProjectCommandController@instant')
         ->name('deployer.project.instant_command_list')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/commands/create', 'ProjectCommandController@create')
         ->name('deployer.project.command_create')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/commands/run', 'ProjectCommandController@run')
         ->name('deployer.project.command_run')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/commands/edit', 'ProjectCommandController@edit')
         ->name('deployer.project.command_edit')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::post('/project/commands/save', 'ProjectCommandController@save')
         ->name('deployer.project.command_save')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/commands/delete', 'ProjectCommandController@deleteConfirm')
         ->name('deployer.project.command_delete_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_DELETE]))
    ;
    Route::post('/project/commands/delete', 'ProjectCommandController@delete')
         ->name('deployer.project.command_delete')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_DELETE]))
    ;
    Route::get('/project/command_states', 'ProjectCommandController@commandStates')
         ->name('deployer.project.command_sates')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/instant_command_states', 'ProjectCommandController@instantCommandStates')
         ->name('deployer.project.instant_command_states')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::get('/project/commands_clone_modal', 'ProjectCommandController@commandsCloneModal')
         ->name('deployer.project.commands_clone_modal')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;
    Route::post('/project/commands_clone', 'ProjectCommandController@commandsClone')
         ->name('deployer.project.commands_clone')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_DEPLOY]))
    ;


    /*-----------------------------------------------
     | Project Environmental Variables Routes
     */
    Route::post('/project/macros/save', 'ProjectMacroController@save')
         ->name('deployer.macros.save')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/macros/deleteConfirm/{hashid}', 'ProjectMacroController@deleteConfirm')
         ->name('deployer.macros.delete_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/macros/delete/{hashid}', 'ProjectMacroController@delete')
         ->name('deployer.macros.delete')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/server_macros/{hashid}', 'ProjectMacroController@hostIndex')
         ->name('deployer.macros.server_index')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/macros/override_modal/{hashid}/{host}', 'ProjectMacroController@overrideModal')
         ->name('deployer.macros.override_modal')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/macros/override', 'ProjectMacroController@override')
         ->name('deployer.macros.override')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/macros/override_delete_confirm/{hashid}', 'ProjectMacroController@overrideDeleteConfirm')
         ->name('deployer.macros.override_delete_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::post('/project/macros/delete_override/{hashid}', 'ProjectMacroController@overrideDelete')
         ->name('deployer.macros.delete_override')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/macros/{hashid}', 'ProjectMacroController@index')
         ->name('deployer.macros.index')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;
    Route::get('/project/macros/{action}/{hashid?}', 'ProjectMacroController@action')
         ->name('deployer.macros.action')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_PROJECT_EDIT]))
    ;


    /*-----------------------------------------------
     | Module Routes
     */
    Route::get('/module', 'ModuleController@index')
         ->name('deployer.module.index')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_BROWSE]))
    ;
    Route::get('/reload_module/{hashid}', 'ModuleController@update')
         ->name('deployer.module.reload_module')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_BROWSE]))
    ;
    Route::get('/module/create', 'ModuleController@create')
         ->name('deployer.module.create')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_CREATE]))
    ;
    Route::get('/module/edit/{hash_id}', 'ModuleController@edit')
         ->name('deployer.module.edit')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_EDIT]))
    ;
    Route::post('/module/save', 'ModuleController@save')
         ->name('deployer.module.save')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_CREATE, Deployer::RULE_MODULE_EDIT]))
    ;
    Route::get('/module/delete/{hash_id}', 'ModuleController@deleteConfirm')
         ->name('deployer.module.delete_confirm')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_DELETE]))
    ;
    Route::post('/module/delete/{hash_id}', 'ModuleController@delete')
         ->name('deployer.module.delete')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_DELETE]))
    ;
    Route::get('/module/hook_link/{hash_id}', 'ModuleController@hookLink')
         ->name('deployer.module.hook_link')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE]))
    ;
    Route::get('/module/list_versions/{hash_id}', 'ModuleController@listVersions')
         ->name('deployer.module.list_versions')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_BROWSE]))
    ;


    /*-----------------------------------------------
     | Server Routes
     */
    Route::get('/server', 'HostController@index')
         ->name('deployer.host.index')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_HOST_BROWSE]))
    ;
    Route::get('/reload_host/{hashid}', 'HostController@update')
         ->name('deployer.host.reload_host')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_MODULE_BROWSE]))
    ;
    Route::get('/server/create', 'HostController@create')
         ->name('deployer.host.create')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_HOST_CREATE]))
    ;
    Route::post('/server/save', 'HostController@save')
         ->name('deployer.host.save')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_HOST_CREATE, Deployer::RULE_HOST_EDIT]))
    ;
    Route::get('/server/edit/{host_id}', 'HostController@edit')
         ->name('deployer.host.edit_host')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_HOST_EDIT]))
    ;
    Route::get('/server/delete/{host_id}', 'HostController@deleteConfirm')
         ->name('deployer.host.delete_confirm_host')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_HOST_DELETE]))
    ;
    Route::post('/server/delete/{host_id}', 'HostController@delete')
         ->name('deployer.host.delete_host')
         ->middleware(Deployer::canMiddleware([Deployer::RULE_HOST_DELETE]))
    ;
});

Route::group([
     'middleware' => ['git-authenticate'],
     'prefix'     => 'manage/deployer',
     'namespace'  => 'Modules\Deployer\Http\Controllers',
], function () {
    Route::post('/callback/module/add-release/{token}', 'ModuleController@addRelease')
         ->name('deployer.module.add_release')
         ->where('token', '[\w\-]+')
    ;
});
