<?php

namespace Modules\Deployer\Providers;

use Modules\Deployer\Contracts\DeployerMacroRepository;
use Modules\Deployer\Contracts\DeployHistoryRepository;
use Modules\Deployer\Http\Middleware\GitAuthenticateMiddleware;
use Modules\Deployer\Http\Middleware\OrCanMiddleware;
use Modules\Deployer\Repositories\EloquentDeployerMacroRepository;
use Modules\Deployer\Repositories\EloquentDeployHistoryRepository;
use Modules\Deployer\Schedules\AutomaticUpdateSchedule;
use Modules\Deployer\Services\CarbonQuarterHelpersService;
use Modules\Deployer\Traits\DeployerHasPermitTrait;
use Modules\Deployer\Traits\DeployerPermissions;
use Modules\Yasna\Services\YasnaProvider;

class DeployerServiceProvider extends YasnaProvider implements DeployerPermissions
{
    use DeployerHasPermitTrait;




    /**
     * Return an array contains information of deployer modules in sidebar
     *
     * @return array
     */
    public static function getSidebarItems()
    {
        return [
             [
                  'name'      => trans("deployer::dashboard.Deployer"),
                  'icon'      => 'fas fa-code-branch',
                  'condition' => static::serviceShouldBeLoaded(),
                  'sub_menus' => [
                       [
                            'caption'   => trans("deployer::dashboard.Projects"),
                            'link'      => 'deployer/project',
                            'condition' => static::canAccessProject()(),
                       ],
                       [
                            'caption'   => trans("deployer::dashboard.Modules"),
                            'link'      => 'deployer/module',
                            'condition' => static::canAccessModule()(),
                       ],
                       [
                            'caption'   => trans("deployer::dashboard.Servers"),
                            'link'      => 'deployer/server',
                            'condition' => static::canAccessHost()(),
                       ],
                       [
                            'caption'   => trans("deployer::dashboard.Deploys"),
                            'link'      => 'deployer/deploys',
                            'condition' => static::canAccessProject()(),
                       ],
                  ],
             ],
        ];
    }



    /**
     * Determine deployer service should be ran or not
     *
     * @return \Closure Return a closure which indicate access permission with deployer role
     */
    public static function serviceShouldBeLoaded()
    {
        /**
         * @return bool
         */
        return static::isADeployer();
    }



    /**
     * Provider index
     */
    public function index()
    {
        $this->registerSidebar()
             ->registerAliases()
             ->registerHelpers()
             ->registerRoleSampleModules()
             ->registerMiddlewares()
             ->registerBindings()
             ->registerSchedules()
             ->registerMacros()
        ;
    }



    /**
     * Register all concrete implementation of abstract injected materials
     */
    public function registerBindings()
    {
        $this->app->bind(DeployHistoryRepository::class, EloquentDeployHistoryRepository::class);
        $this->app->bind(DeployerMacroRepository::class, EloquentDeployerMacroRepository::class);

        return $this;
    }



    /**
     * Module string, to be used as sample for role definitions.
     *
     * @return static
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('common')
             ->value('statistics,')
        ;
        module('users')
             ->service('role_sample_modules')
             ->add('project')
             ->value('create, edit, delete, browse,')
        ;
        module('users')
             ->service('role_sample_modules')
             ->add('host')
             ->value('create, edit, delete, browse,')
        ;
        module('users')
             ->service('role_sample_modules')
             ->add('module')
             ->value('create, edit, delete, browse,')
        ;

        return $this;
    }



    /**
     * Register sidebar menu of Deployer module
     *
     * @return static
     */
    public function registerSidebar()
    {
        service('manage')
             ->service('sidebar')
             ->add('deployer')
             ->blade("deployer::layouts.sidebar")
             ->order(70)
             ->condition(static::serviceShouldBeLoaded())
        ;

        return $this;
    }



    /**
     * register aliases for classes
     *
     * @return static
     */
    public function registerAliases()
    {
        $this->addAlias('DeployerModule', DeployerServiceProvider::class);

        return $this;
    }



    /**
     * register helpers of module
     *
     * @return static
     */
    public function registerHelpers()
    {
        $helpers = [
             'date_helpers',
             'hashid_deployer',
        ];

        $this->registerHelpersByFileName($helpers);

        return $this;
    }



    /**
     * Register middlewares of the module
     *
     * @return $this
     */
    public function registerMiddlewares()
    {
        $this->addMiddleware('or-can', OrCanMiddleware::class);
        $this->addMiddleware('git-authenticate', GitAuthenticateMiddleware::class);

        return $this;
    }



    /**
     * Register schedules
     *
     * @return $this
     */
    public function registerSchedules()
    {
        $this->addSchedule(AutomaticUpdateSchedule::class);

        return $this;
    }



    /**
     * Register macros
     *
     * @return $this
     */
    public function registerMacros()
    {
        CarbonQuarterHelpersService::register();

        return $this;
    }



    /**
     * register helpers which exists on module
     *
     * @param array $helpers List of helpers file name
     */
    private function registerHelpersByFileName(array $helpers)
    {
        foreach ($helpers as $helper) {
            $path = __DIR__ . "/../Helpers/{$helper}.php";

            if (file_exists($path)) {
                require_once $path;
            }
        }
    }
}
