<?php

namespace Modules\Census\Providers;

use Modules\Census\Console\CensorsCalculatorCommand;
use Modules\Census\Http\Endpoints\V1\GetStatsEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class CensusServiceProvider
 *
 * @package Modules\Census\Providers
 */
class CensusServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerAPIEndpoints();
        $this->registerCLICommands();
    }



    /**
     * Register artisan commands.
     */
    private function registerCLICommands()
    {
        $this->addArtisan(CensorsCalculatorCommand::class);
    }



    /**
     * Register all endpoints provided by this module
     */
    protected function registerAPIEndpoints()
    {
        if ($this->cannotUseModule("Endpoint")) {
            return;
        }

        endpoint()->register(GetStatsEndpoint::class);
    }
}
