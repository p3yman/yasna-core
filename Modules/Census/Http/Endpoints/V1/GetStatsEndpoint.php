<?php

namespace Modules\Census\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/census-get-stats
 *                    Get Stats
 * @apiDescription    Get Census Statss
 * @apiVersion        1.0.0
 * @apiName           Get Stats
 * @apiGroup          Census
 * @apiPermission     User
 * @apiParam {String[]} indices Indicate which stats indices must be returned on result.
 * @apiParam {String=daily,monthly,yearly} type Indicate the type of stats which must be returned
 * @apiParam {Integer=0,1} [paginated=0] Indicate the result must be paginated (`1`) or not (`0`).
 * @apiParam {Integer} [per_page=15] Indicate how much entities must be returned on paginated result.
 * @apiParam {Integer} [page=1] Indicate which page on a paginated result must be showed.
 * @apiParam {Array[]} [sort] Enable user to sort fields. On this scope sort can be done based on items on `indices`.
 *           Default sort type will be `ascending` and for changing this direction to descending you must append a `-`
 *           sign in heading of field. (for example: `sort[]=year&sort[]=-month` will do an ascending sort first on
 *           `year` column, then will do a descending sort on `month` column for each collection.).
 * @apiParam {Integer} [from] The UNIX timestamp indicates the starting point of interval for filtering returned stats.
 * @apiParam {Integer} [to] The UNIX timestamp indicates the ending point of interval for filtering returned stats.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      "total": 4
 *      },
 *      "results": [
 *          {
 *              "type": "daily",
 *              "year": 1397,
 *              "month": 9,
 *              "day": 22,
 *              "case_total_counts": 0
 *          },
 *          {
 *              "type": "daily",
 *              "year": 1397,
 *              "month": 9,
 *              "day": 21,
 *              "case_total_counts": 1
 *          },
 *          {
 *              "type": "daily",
 *              "year": 1397,
 *              "month": 9,
 *              "day": 20,
 *              "case_total_counts": 2
 *          },
 *          {
 *              "type": "daily",
 *              "year": 1397,
 *              "month": 9,
 *              "day": 19,
 *              "case_total_counts": 0
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Census\Http\Controllers\V1\CensusController controller()
 */
class GetStatsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get Stats";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Census\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CensusController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $dummy_stats = [];

        for($month = 1; $month < 13; $month++){

            $dummy_stats[] = [
                 "type"       => "yearly",
                 "year"       => 1397,
                 "month"      => $month,
                 "day"        => 22,
                 "all"        => rand(8000,10000),
                 "donated"    => rand(1000, 8000),
                 "follow-up"  => rand(1000, 8000),
                 "healed"     => rand(1000, 8000),
                 "unfeasible" => rand(1000, 8000),
                 "dead"       => rand(1000, 8000),
            ];
        }

        return api()->successRespond($dummy_stats,
             [
                  "total" => "4",
             ]
        );
    }
}
