<?php

namespace Modules\Census\Http\Controllers\V1;

use App\Models\Censor;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Census\Http\Controllers\V1\Traits\SortTrait;
use Modules\Census\Http\Requests\V1\GetStatsRequest;
use Modules\Yasna\Services\YasnaApiController;

class CensusController extends YasnaApiController
{
    use SortTrait;

    protected $model_name = "censor";



    /**
     * list the unities, supporting search etc.
     *
     * @param GetStatsRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function list(GetStatsRequest $request)
    {
        $model         = model($this->model_name);
        $elector_array = $this->getElectors($request);
        $builder       = $model->elector($elector_array);
        $this->applyRequestedSort($builder, $request);

        if ($request->isPaginated()) {
            return $this->listPaginated($builder, $request);
        }

        return $this->listFlat($builder, $request);
    }



    /**
     * list by pagination
     *
     * @param Builder         $builder
     * @param GetStatsRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listPaginated(Builder $builder, $request)
    {
        $paginator = $builder->paginate($request->perPage());
        $result    = $this->mappedArray($paginator, $request);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * list all together
     *
     * @param Builder         $builder
     * @param GetStatsRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listFlat(Builder $builder, GetStatsRequest $request)
    {
        $meta = [
             "total" => $builder->count(),
        ];
        $data = $this->mappedArray($builder->get(), $request);

        return $this->success($data, $meta);
    }



    /**
     * Return a mapped array
     *
     * @param Collection|LengthAwarePaginator $collection
     * @param GetStatsRequest                 $request
     *
     * @return array
     */
    protected function mappedArray($collection, GetStatsRequest $request)
    {
        return $collection->map(function (Censor $stat) use ($request) {
            return $this->toListResource($stat, $request);
        })->toArray()
             ;
    }



    /**
     * Return a list of elector based on the request
     *
     * @param GetStatsRequest $request
     *
     * @return array
     */
    protected function getElectors(GetStatsRequest $request): array
    {
        return array_normalize(
             array_filter($request->all()),
             [
                  'type'     => 'yearly',
                  'interval' => $request->getFilteringInterval(),
             ]
        );
    }



    /**
     * Return a list view of a given censor model
     *
     * @param Censor          $stat
     * @param GetStatsRequest $request
     *
     * @return array
     */
    protected function toListResource(Censor $stat, GetStatsRequest $request): array
    {
        $results = [
             'type'  => $stat->type,
             'year'  => $stat->year,
             'month' => $stat->month,
             'day'   => $stat->day,
        ];

        foreach ($request->getIndices() as $index) {
            if ($stat->fieldIsNotInBlacklist($index) and $stat->canView($index)) {
                $results[$index] = $stat->{$index};
            } else {
                $results[$index] = null;
            }
        }

        return $results;
    }
}
