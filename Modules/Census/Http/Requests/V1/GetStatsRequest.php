<?php

namespace Modules\Census\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;
use Morilog\Jalali\jDate;

class GetStatsRequest extends YasnaRequest
{
    protected $model_name = "censor";
    protected $responder  = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'paginated' => 'integer|in:0,1',
             'per_page'  => 'integer|min:1|max:50',
             'page'      => 'integer|min:1',
             'indices'   => 'required|array|min:1',
             'indices.*' => 'string',
             'type'      => 'string|in:yearly,monthly,daily',
             'sort'      => 'array',
             'sort.*'    => 'string',
             'from'      => 'int',
             'to'        => 'int',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'paginated',
             'per_page',
             'page',
             'indices',
             'indices.*',
             'type',
             'sort',
             'sort.*',
             'from',
             'to',
        ];
    }



    /**
     * Return a list of requested indices
     *
     * @return array
     */
    public function getIndices(): array
    {
        return $this->input('indices', []);
    }



    /**
     * Return the filtering interval.
     *
     * @return array
     */
    public function getFilteringInterval(): array
    {
        return [
             'from' => $this->getStartInterval(),
             'to'   => $this->getEndInterval(),
        ];
    }



    /**
     * Return information about the start datetime of filtering interval.
     *
     * @return array
     */
    private function getStartInterval(): array
    {
        return $this->intervalGenerator('from');
    }



    /**
     * Return information about the end datetime of filtering interval.
     *
     * @return array
     */
    private function getEndInterval(): array
    {
        return $this->intervalGenerator('to');
    }



    /**
     * Generate a datetime filtering based on a given requested field.
     *
     * @param string $field
     *
     * @return array
     */
    private function intervalGenerator(string $field): array
    {
        $jdate = null;
        if ($this->filled($field)) {
            $timestamp = intval($this->input($field));
            $jdate     = jDate::forge($timestamp);
        }

        if ($jdate) {
            return $this->datetimeArrayGeneratorBasedOnType($jdate);
        }

        return $this->datetimeArrayGenerator();
    }



    /**
     * Strategy runner for getting information from `*DatetimeGenerator` methods based on the request type.
     *
     * @param jDate $jdate
     *
     * @return array
     */
    private function datetimeArrayGeneratorBasedOnType(jDate $jdate): array
    {
        $type             = $this->input('type', 'yearly');
        $generator_method = studly_case(sprintf("%s_datetime_generator", $type));
        if (method_exists($this, $generator_method)) {
            return $this->{$generator_method}($jdate);
        }

        return $this->datetimeArrayGenerator();
    }



    /**
     * Return yearly datetime filtering array.
     *
     * @param jDate $jdate
     *
     * @return array
     */
    private function yearlyDatetimeGenerator(jDate $jdate): array
    {
        return $this->datetimeArrayGenerator(intval($jdate->format('Y')));
    }



    /**
     * Return monthly datetime filtering array.
     *
     * @param jDate $jdate
     *
     * @return array
     */
    private function monthlyDatetimeGenerator(jDate $jdate): array
    {
        return $this->datetimeArrayGenerator(
             intval($jdate->format('Y')),
             intval($jdate->format('n'))
        );
    }



    /**
     * Return daily datetime filtering array.
     *
     * @param jDate $jdate
     *
     * @return array
     */
    private function dailyDatetimeGenerator(jDate $jdate): array
    {
        return $this->datetimeArrayGenerator(
             intval($jdate->format('Y')),
             intval($jdate->format('n')),
             intval($jdate->format('j'))
        );
    }



    /**
     * Generate a datetime array based on given values.
     *
     * @param int|null $year
     * @param int|null $month
     * @param int|null $day
     *
     * @return array
     */
    private function datetimeArrayGenerator(?int $year = null, ?int $month = null, ?int $day = null): array
    {
        return compact('year', 'month', 'day');
    }
}
