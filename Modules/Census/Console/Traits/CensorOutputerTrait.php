<?php

namespace Modules\Census\Console\Traits;

use App\Models\Censor;

/**
 * Trait CensorOutputerTrait add some functions to print appropriate output on console.
 *
 * @package Modules\Census\Console\Traits
 */
trait CensorOutputerTrait
{
    /**
     * Printout a success censor saving message.
     *
     * @param Censor $model
     */
    private function successCensorSave(Censor $model)
    {
        $placeholder = $this->placeholderString($model);
        $output      = sprintf("%s census successfully calculated and saved.", $placeholder);

        $this->info($output);
    }



    /**
     * Printout a failed censor saving message.
     *
     * @param Censor $model
     */
    private function failedCensorSave(Censor $model)
    {
        $placeholder = $this->placeholderString($model);
        $output      = sprintf("%s census calculation and saving has been failed.", $placeholder);

        $this->error($output);
    }



    /**
     * A placeholder suitable string generator.
     *
     * @param Censor $model
     *
     * @return string
     */
    private function placeholderString(Censor $model): string
    {
        $placeholder_generator = studly_case(sprintf("%s_placeholder", $model->type));

        return $this->{$placeholder_generator}($model);
    }



    /**
     * A yearly placeholder suitable string generator.
     *
     * @param Censor $model
     *
     * @return string
     */
    private function yearlyPlaceholder(Censor $model): string
    {
        return sprintf("%d (type: %s)", $model->year, $model->type);
    }



    /**
     * A monthly placeholder suitable string generator.
     *
     * @param Censor $model
     *
     * @return string
     */
    private function monthlyPlaceholder(Censor $model): string
    {
        return sprintf("%d-%d (type: %s)", $model->year, $model->month, $model->type);
    }



    /**
     * A daily placeholder suitable string generator.
     *
     * @param Censor $model
     *
     * @return string
     */
    private function dailyPlaceholder(Censor $model): string
    {
        return sprintf("%d-%d-%d (type: %s)", $model->year, $model->month, $model->day, $model->type);
    }
}
