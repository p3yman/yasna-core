<?php

namespace Modules\Census\Console\Traits;

use App\Models\Censor;
use Morilog\Jalali\jDate;
use Morilog\Jalali\jDateTime;

/**
 * Trait CensorStatsProvidersTrait add data provider for different types of stats.
 *
 * @package Modules\Census\Console\Traits
 */
trait CensorStatsProvidersTrait
{
    use CensorCalculationTrait;
    use CensorOutputerTrait;



    /**
     * Do daily stats gathering.
     *
     * @param array $indices
     * @param int   $elapsed
     *
     * @throws \Exception
     */
    public function dailyStatsProvider(array $indices, int $elapsed)
    {
        $model = $this->prepareModel('daily', $elapsed);
        $this->fillAndSaveModel($model, $indices);

    }



    /**
     * Do monthly stats gathering.
     *
     * @param array $indices
     * @param int   $elapsed
     *
     * @throws \Exception
     */
    public function monthlyStatsProvider(array $indices, int $elapsed)
    {
        $model = $this->prepareModel('monthly', $elapsed);
        $this->fillAndSaveModel($model, $indices);
    }



    /**
     * Do yearly stats gathering.
     *
     * @param array $indices
     * @param int   $elapsed
     *
     * @throws \Exception
     */
    public function yearlyStatsProvider(array $indices, int $elapsed)
    {
        $model = $this->prepareModel('yearly', $elapsed);
        $this->fillAndSaveModel($model, $indices);
    }



    /**
     * Fill a given model with data gathered for a set of indices and save the result on DB;
     *
     * @param Censor $model
     * @param array          $indices
     *
     * @throws \Exception
     */
    private function fillAndSaveModel(Censor $model, array $indices)
    {
        $this->calculateStatsAndFillModel($model, $indices);

        if ($model->save()) {
            $this->successCensorSave($model);
            return;
        }

        $this->failedCensorSave($model);
    }



    /**
     * Calculate stats for a given set of indices and fill a given model with it.
     *
     * @param Censor $model
     * @param array          $indices
     *
     * @throws \Exception
     */
    private function calculateStatsAndFillModel(Censor $model, array $indices)
    {
        foreach ($indices as $index => $criteria) {
            $this->fillModelWithGivenIndex($model, $index, $criteria);
        }
    }



    /**
     * Fill the the property `$index` of the model with the given $criteria.
     *
     * @param Censor $model
     * @param int|string     $index
     * @param string|array   $criteria
     *
     * @throws \Exception
     */
    private function fillModelWithGivenIndex(Censor $model, $index, $criteria)
    {
        // if criteria is an array, index is a column name and the criteria contains model and method
        if (is_array($criteria)) {
            $period          = $this->getCarbonizedPeriod($model);
            $model->{$index} = $this->calculateBasedOnCriteria($criteria, $period);
            return;
        }

        // the criteria is a string and calculation is a cumulative based on the last records
        $model->{$criteria} = $this->cumulativeStats($model, $criteria);
    }



    /**
     * Pre-fill model based on the type of the given provider.
     *
     * @param string $type
     * @param int    $elapsed
     *
     * @return Censor
     */
    private function prepareModel(string $type, int $elapsed): Censor
    {
        $datetime = $this->getDatetime($type, $elapsed);
        $model = model('censor')->elector($datetime + ['type' => $type])->first();
        if ($model) {
            return $model;
        }

        $model = model('censor');

        $model->type  = $type;
        $model->year  = $datetime['year'];
        $model->month = $datetime['month'];
        $model->day   = $datetime['day'];

        return $model;
    }



    /**
     * Return datetime of the stat record based on current time and this fact that requested record is for past or now.
     * In the context of this package, `the past` is one unit before the type (i.e. last day for daily type, last month
     * for monthly type and last year for yearly type).
     *
     * @param string $type
     * @param int    $elapsed
     *
     * @return array
     */
    private function getDatetime(string $type, int $elapsed): array
    {
        $jdate = jDate::forge();

        if (str_contains("yearly", $type)) {
            if ($elapsed) {
                $jdate = jDate::forge(sprintf('-%d year', $elapsed));
            }

            return [
                 'year'  => $jdate->format('Y'),
                 'month' => 0,
                 'day'   => 0,
            ];
        }

        if (str_contains("monthly", $type)) {
            if ($elapsed) {
                $jdate = jDate::forge(sprintf('-%d month', $elapsed));
            }

            return [
                 'year'  => $jdate->format('Y'),
                 'month' => $jdate->format('n'),
                 'day'   => 0,
            ];
        }

        if ($elapsed) {
            $jdate = jDate::forge(sprintf('-%d day', $elapsed));
        }

        return [
             'year'  => $jdate->format('Y'),
             'month' => $jdate->format('n'),
             'day'   => $jdate->format('j'),
        ];

    }



    /**
     * Generate an array contains two carbon objects which indicate the start and end range of data filtering.
     *
     * @param Censor $model
     *
     * @return array
     */
    private function getCarbonizedPeriod(Censor $model): array
    {
        $carbon_type_getter = studly_case(sprintf("get_%s_carbonized_period", $model->type));

        return $this->{$carbon_type_getter}($model);
    }



    /**
     * Generate an array contains two carbon objects which indicate the start and end range of daily filtering.
     *
     * @param Censor $model
     *
     * @return array
     */
    private function getDailyCarbonizedPeriod(Censor $model): array
    {
        $jdate = jDate::forge();
        if (($sub = jDate::forge()->format('j') - $model->day) != 0) {
            $jdate = jDate::forge(sprintf("-%d day", $sub));
        }

        $day = $jdate->format('Y/m/d 15:00:00');

        return [
             'start_date' => jDateTime::createCarbonFromFormat('Y/m/d H:i:s', $day)->startOfDay(),
             'end_date'   => jDateTime::createCarbonFromFormat('Y/m/d H:i:s', $day)->endOfDay(),
        ];
    }



    /**
     * Generate an array contains two carbon objects which indicate the start and end range of monthly filtering.
     *
     * @param Censor $model
     *
     * @return array
     */
    private function getMonthlyCarbonizedPeriod(Censor $model): array
    {
        if (($sub = jDate::forge()->format('n') - $model->month) != 0) {
            $jdate = jDate::forge(sprintf("-%d month", $sub));
        }

        $start = $jdate->format('Y/m/01 15:00:00');
        $end   = $jdate->format('Y/m/t 15:00:00');

        return [
             'start_date' => jDateTime::createCarbonFromFormat('Y/m/d H:i:s', $start)->startOfDay(),
             'end_date'   => jDateTime::createCarbonFromFormat('Y/m/d H:i:s', $end)->endOfDay(),
        ];
    }



    /**
     * Generate an array contains two carbon objects which indicate the start and end range of monthly filtering.
     *
     * @param Censor $model
     *
     * @return array
     */
    private function getYearlyCarbonizedPeriod(Censor $model): array
    {
        $jdate = jDate::forge();
        if (($sub = jDate::forge()->format('Y') - $model->year) != 0) {
            $jdate = jDate::forge(sprintf("-%d year", $sub));
        }

        $start    = $jdate->format('Y/01/01 15:00:00');
        $last_day = jDateTime::checkDate($jdate->format('Y'), 12, 30) ? 30 : 29;
        $end      = $jdate->format("Y/12/{$last_day} 15:00:00");

        return [
             'start_date' => jDateTime::createCarbonFromFormat('Y/m/d H:i:s', $start)->startOfDay(),
             'end_date'   => jDateTime::createCarbonFromFormat('Y/m/d H:i:s', $end)->endOfDay(),
        ];
    }
}
