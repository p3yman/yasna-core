<?php

namespace Modules\Census\Console\Traits;

use App\Models\Censor;
use Exception;

/**
 * Trait CensorCalculationTrait add ability of calculating related to census.
 *
 * @package Modules\Census\Console\Traits
 */
trait CensorCalculationTrait
{
    /**
     * @param array $criteria
     * @param array $period An array contains start and end carbon objects interval for filtering.
     *
     * @return int
     * @throws Exception
     */
    private function calculateBasedOnCriteria(array $criteria, array $period): int
    {
        if (count($criteria) != 2) {
            throw new Exception('The given set of criteria must be an non-associative array contains FQN of model and a method.');
        }

        try {
            $class  = $criteria[0];
            $method = $criteria[1];

            return (new $class)->{$method}($period);
        } catch (Exception $e) {
            throw $e;
        }
    }



    /**
     * Calculate the cumulative stats for a given criteria.
     *
     * @param Censor $model
     * @param string         $criteria
     *
     * @return int
     */
    private function cumulativeStats(Censor $model, string $criteria): int
    {
        $period = $this->getElectorStatsPeriodFilter($model);
        $result = model('censor')->elector($period)
                                          ->sum($criteria)
        ;

        return $result;
    }



    /**
     * Create a elector usable filter array.
     *
     * @param Censor $model
     *
     * @return array
     */
    private function getElectorStatsPeriodFilter(Censor $model): array
    {
        $period = array_filter([
             'year'  => $model->year,
             'month' => $model->month,
             'day'   => $model->day,
        ]);

        return ['period' => $period];
    }
}
