<?php

namespace Modules\Census\Console;

use App\Models\Censor;
use Illuminate\Console\Command;
use Modules\Census\Console\Traits\CensorStatsProvidersTrait;
use Symfony\Component\Console\Input\InputOption;
use Exception;

class CensorsCalculatorCommand extends Command
{
    use CensorStatsProvidersTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'census:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gathering censuses data';



    /**
     * For each type (suppose we call it `Type`) there must be a method `TypeStats` on `Censor` model
     * which returns an array of indices which must be used to gathering statistical data. Also there must be a
     * `TypeStatsProvider` method on this class which tries to gathering information from the given indices set.
     *
     * @inheritdoc
     */
    public function handle()
    {
        $elapsed       = intval($this->option('elapsed'));
        $type          = camel_case(sprintf("%s_stats", $this->option('type')));
        $type_provider = camel_case(sprintf("%s_stats_provider", $this->option('type')));

        if (method_exists(Censor::class, $type)) {
            $stats_index = (new Censor)->{$type}();

            try {
                $this->{$type_provider}($stats_index, $elapsed);
            } catch (Exception $e) {
                $this->error(sprintf("An error occurred, %s", $e->getMessage()));
            }

            return;
        }

        $this->error(sprintf("Please implement a %s method on Censor model.", $type));
    }



    /**
     * @inheritdoc
     */
    protected function getOptions()
    {
        return [
             ['type', 't', InputOption::VALUE_OPTIONAL, 'The type of gathering data (yearly, monthly, daily)', 'daily'],
             ['elapsed', 'e', InputOption::VALUE_OPTIONAL, 'The elapsed time from now (2 for type day means 2 days ago)', 0],
        ];
    }
}
