<?php

namespace Modules\Census\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;

trait CensorElectorTrait
{
    /**
     * Filter censor table based on the year.
     *
     * @param int $year
     */
    public function electorYear(int $year)
    {
        $this->electorFieldValue('year', $year);
    }



    /**
     * Filter censor table based on the month.
     *
     * @param int $month
     */
    public function electorMonth(int $month)
    {
        $this->electorFieldValue('month', $month);
    }



    /**
     * Filter censor table based on the day.
     *
     * @param int $day
     */

    public function electorDay(int $day)
    {
        $this->electorFieldValue('day', $day);
    }



    /**
     * Filter censor table based on a given period array
     *
     * @param array $period
     */
    public function electorPeriod(array $period)
    {
        $where = array_filter(array_normalize(
             $period,
             [
                  'year'  => 0,
                  'month' => 0,
                  'day'   => 0,
             ]
        ));

        $builder = $this->elector();

        if (!isset($where['day'])) {
            $builder->where('day', '<>', 0);
        }

        if (!isset($where['month'])) {
            $builder->where('month', '<>', 0);
        }

        $builder->where($where);
    }



    /**
     * Filter all results which are on a given interval. The interval array must have two key: `from` and `to` which
     * each key has an array value contains a datetime array.
     *
     * @param array $interval
     */
    public function electorInterval(array $interval)
    {
        if (!isset($interval['from'], $interval['to'])) {
            return;
        }

        $builder = $this->elector();
        $this->setIntervalConstraint('year', $builder, $interval);
        $this->setIntervalConstraint('month', $builder, $interval);
        $this->setIntervalConstraint('day', $builder, $interval);
    }



    /**
     * Filter based on type
     *
     * @param string $type
     */
    public function electorType(string $type)
    {
        $this->electorFieldValue('type', $type);
    }



    /**
     * Set interval constraints based on a given type
     *
     * @param string  $type
     * @param Builder $builder
     * @param array   $interval
     */
    private function setIntervalConstraint(string $type, $builder, array $interval)
    {
        $from = array_filter($interval['from']);
        $to   = array_filter($interval['to']);

        if (isset($from[$type], $to[$type])) {
            if ($from[$type] == $to[$type]) {
                $builder->where($type, $from[$type]);
            } else {
                $builder->whereBetween($type, [$from[$type], $to[$type]]);
            }
        } elseif (isset($from[$type])) {
            $builder->where($type, '>=', $from[$type]);
        } elseif (isset($to[$type])) {
            $builder->where($type, '<=', $to[$type]);
        }
    }
}
