<?php

namespace Modules\Census\Entities;

use Modules\Census\Entities\Traits\CensorElectorTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Censor
 *
 * @package Modules\Census\Entities
 * @property int    $year
 * @property int    $month
 * @property int    $day
 * @property string $type
 * @property int    $case_total_counts
 */
class Censor extends YasnaModel
{
    use SoftDeletes;
    use CensorElectorTrait;



    /**
     * Return columns of the array which can not sort based on them.
     *
     * @return array
     */
    public static function notSortableFields(): array
    {
        return [
             'created_at',
             'updated_at',
             'deleted_at',
             'created_by',
             'updated_by',
             'deleted_by',
             'meta',
             'converted',
        ];
    }



    /**
     * Return statistical indices which must be gathered daily. Information which must be censused every day. The array
     * generally must be in this format:
     *      ```[
     *          'column_name' => [ModelName::class, 'methodNameForGettingStatisticalResults'],
     *      ]```
     * Which the `column_name` is a column on `registry_censors` table, `ModelName::class` is a model which the stats
     * is about it and `methodNameForGettingStatisticalResults` is a static method inside the model which implement a
     * logic for calculation of stats and finally returns an `int`.
     *
     * @return array
     */
    public function dailyStats(): array
    {
        return $this->getIndicesArray("dailyStats");
    }



    /**
     * Return statistical indices which must be gathered monthly. The monthly array indicates stats which must be
     * gathered monthly. This array supports format discussed on `dailyStats`, also it can be in `['column_name_1',
     * 'column_name_2']` format. The later just calculate the accumulative stats of each column for a month.
     *
     * @return array
     */
    public function monthlyStats(): array
    {
        return $this->getIndicesArray("monthlyStats");
    }



    /**
     * Return statistical indices which must be gathered yearly. The yearly array indicates stats which must be
     * calculated yearly. It acts similar to `monthlyStats`.
     *
     * @return array
     */
    public function yearlyStats(): array
    {
        return $this->getIndicesArray("yearlyStats");
    }



    /**
     * Indicate that a field can be viewed by current request-sender user.
     *
     * @param $field
     *
     * @return bool
     */
    public function canView($field): bool
    {
        $can_view_method = studly_case(sprintf("can_view_%s", $field));
        if (method_exists($this, $can_view_method)) {
            return $this->{$can_view_method}();
        }

        return true;
    }



    /**
     * Indicate user can request arbitrary a given field or not
     *
     * @param $field
     *
     * @return bool
     */
    public function fieldIsInBlacklist($field): bool
    {
        $blacklist = [
             'id',
             'type',
             'month',
             'year',
             'created_at',
             'updated_at',
             'deleted_at',
             'created_by',
             'updated_by',
             'deleted_by',
             'meta',
             'converted',
        ];

        return in_array($field, $blacklist);
    }



    /**
     * Indicate user can request arbitrary a given field or not
     *
     * @param $field
     *
     * @return bool
     */
    public function fieldIsNotInBlacklist($field): bool
    {
        return !$this->fieldIsInBlacklist($field);
    }



    /**
     * Return all methods which provide indices for a given type (in this model or from an added trait)
     *
     * @param $type
     *
     * @return array
     */
    private function getIndicesArray($type): array
    {
        $results = [];
        $type    = strtolower($type);
        $methods = get_class_methods(static::class);

        foreach ($methods as $method) {
            $method = strtolower($method);
            if ($method != $type and str_contains($method, $type)) {
                $result = $this->{$method}();
                if (is_array($result)) {
                    $results += $result;
                }
            }
        }

        return $results;
    }
}
