<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistryCensorsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_censors', function (Blueprint $table) {
            $table->increments('id');

            $table->string('type', 10)->index();

            $table->unsignedSmallInteger('year')->index();
            $table->unsignedTinyInteger('month')->nullable()->default(0)->index();
            $table->unsignedTinyInteger('day')->nullable()->default(0)->index();

            $table->index(['year', 'month', 'day']);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_censors');
    }
}
