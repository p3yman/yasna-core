<?php

namespace Modules\Coupons\Providers;

use Modules\Coupons\CouponRules\OrdersRangePriceRule;
use Modules\Coupons\CouponRules\ParticularUserRule;
use Modules\Coupons\CouponRules\UserCompleteFieldsRule;
use Modules\Coupons\Console\MakeRuleCommand;
use Modules\Coupons\CouponRules\AmountLimitRule;
use Modules\Coupons\CouponRules\CodeUsageRule;
use Modules\Coupons\CouponRules\DateLimitRule;
use Modules\Coupons\CouponRules\MinItemCountRule;
use Modules\Coupons\CouponRules\MinimumAmountRule;
use Modules\Coupons\CouponRules\UserSpecialNameRule;
use Modules\Coupons\CouponRules\UserRegisterDateRule;
use Modules\Coupons\CouponRules\UserRoleRule;
use Modules\Coupons\CouponRules\UserStateRule;
use Modules\Coupons\CouponRules\WeekdaysRule;
use Modules\Yasna\Services\YasnaProvider;

class CouponsServiceProvider extends YasnaProvider
{

    /**
     * will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerServices();
        $this->registerSidebar();
        $this->registerSampleCouponGroup();
        $this->registerCouponRules();
        $this->registerConsistentCouponRules();
        $this->registerArtisanCommands();
        $this->registerRoleSampleModules();
    }



    /**
     * register sample coupon group.
     */
    protected function registerSampleCouponGroup()
    {
        $condition = function () {
            return debugMode();
        };

        coupon()->registerGroup('sample', 'trans:coupons::rulebooks.sample', $condition);
    }



    /**
     * register services
     */
    protected function registerServices()
    {
        module("coupons")
             ->register(coupon()::RULES_SERVICE_NAME, "keeps the coupon rule classes")
             ->register(coupon()::CONSISTENT_RULES_SERVICE_NAME, "keeps the mandatory always-included rule classes")
             ->register("models", "keeps the name of attached models")
        ;
    }



    /**
     * register sidebar menu
     */
    protected function registerSidebar()
    {
        service('manage:sidebar')
             ->add('coupon-rulebooks')
             ->blade('coupons::sidebar.index')
             ->order(81)
             ->condition(function() {
                 return coupon()->canBrowseRulebook();
             })
        ;
    }



    /**
     * register coupon rules
     */
    protected function registerCouponRules()
    {
        coupon()->registerRule(DateLimitRule::class);
        coupon()->registerRule(WeekdaysRule::class);
        coupon()->registerRule(UserRoleRule::class);
        coupon()->registerRule(UserStateRule::class);
        coupon()->registerRule(MinimumAmountRule::class);
        coupon()->registerRule(MinItemCountRule::class);
        coupon()->registerRule(UserSpecialNameRule::class);
        coupon()->registerRule(UserRegisterDateRule::class);
        coupon()->registerRule(UserCompleteFieldsRule::class);
        coupon()->registerRule(OrdersRangePriceRule::class);
    }



    /**
     * register coupons consistent rules
     */
    protected function registerConsistentCouponRules()
    {
        coupon()->registerConsistentRule(AmountLimitRule::class);
        coupon()->registerConsistentRule(CodeUsageRule::class);
        coupon()->registerConsistentRule(ParticularUserRule::class);
    }



    /**
     * register artisan commands
     */
    protected function registerArtisanCommands()
    {
        $this->addArtisan(MakeRuleCommand::class);
    }



    /**
     * register role-sample-modules
     */
    protected function registerRoleSampleModules()
    {
        $permits = [
             'rulebook-browse',
             'rulebook-create',
             'rulebook-edit',
             'rulebook-delete',
             //'rulebook-report',
             'code-browse',
             'code-create',
             'code-edit',
             'code-delete',
             'code-report',
        ];

        module("users")
             ->service("role_sample_modules")
             ->add("coupons")
             ->value(implode(" , ", $permits))
        ;
    }
}
