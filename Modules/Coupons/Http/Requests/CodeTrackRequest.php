<?php

namespace Modules\Coupons\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CodeTrackRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->_submit == 'delete') {
            return coupon()->canDeleteCodes();
        }

        if ($this->_submit == 'restore') {
            return coupon()->canReportCodes();
        }

        return coupon()->canReportCodes();
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        $this->data['model'] = $this->model = couponCode()->withTrashed()->grabSlug($this->data['code']);
    }
}
