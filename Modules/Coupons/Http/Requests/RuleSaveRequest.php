<?php

namespace Modules\Coupons\Http\Requests;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;
use Modules\Yasna\Services\YasnaRequest;

class RuleSaveRequest extends YasnaRequest
{
    /**
     * keep the form object
     *
     * @var Form
     */
    protected $form;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return $this->form->getValidationRules();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return $this->form->getPurificationRules();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return $this->form->getAttributeLabels();
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($hashid = false)
    {
        $serialized = $this->data['_rulebook'];

        $this->model = model('coupon-rulebook')::unserialize($serialized);
        $this->setFormObject($this->data['_class']);
    }



    /**
     * dynamically load and set form object
     *
     * @param DiscountRule $class
     */
    public function setFormObject($class)
    {
        $this->form = $class::getFormInstance();
    }
}
