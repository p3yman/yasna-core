<?php

namespace Modules\Coupons\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RulebookDeleteRequest extends YasnaRequest
{
    protected $model_name = "coupon-rulebook";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return coupon()->canDeleteRulebook();
    }
}
