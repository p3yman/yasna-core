<?php

namespace Modules\Coupons\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CodeGenerateRequest extends YasnaRequest
{
    protected $model_name = "coupon-rulebook";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return coupon()->canCreateCodes();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "prefix"   => "max:15",
             "postfix"  => "max:15",
             "pattern"  => "required|numeric|min:1|max:3",
             "length"   => "numeric|max:30",
             "quantity" => "required|numeric|min:1|max:500",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "length"   => "ed|numeric",
             "quantity" => "ed|numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "quantity" => trans_safe("coupons::codes.quantity"),
             "pattern"  => trans_safe("coupons::codes.pattern"),
             "length"   => trans_safe("coupons::codes.length"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['pattern'] = $this->redefinePattern($this->data['pattern']);
        $this->data['prefix']  = $this->escapeSpaces($this->data['prefix']);
        $this->data['postfix'] = $this->escapeSpaces($this->data['postfix']);
    }



    /**
     * redefine pattern according to the preset model constants
     *
     * @param string $pattern
     *
     * @return int
     */
    protected function redefinePattern($pattern)
    {
        $map = [
             "numeric"      => couponCode()::SLUG_TYPE_NUMERIC,
             "alphabetic"   => couponCode()::SLUG_TYPE_ALPHABETIC,
             "alphanumeric" => couponCode()::SLUG_TYPE_ALPHANUMERIC,
        ];

        if (isset($map[$pattern])) {
            return $map[$pattern];
        }

        return 0;
    }



    /**
     * escape spaces from the strings
     *
     * @param $string
     *
     * @return string
     */
    protected function escapeSpaces($string)
    {
        return str_replace(" ", "", $string);
    }
}
