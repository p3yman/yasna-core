<?php

namespace Modules\Coupons\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RuleTestRequest extends YasnaRequest
{


    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "total_amount"        => "ed|numeric",
             "effective_date"      => "date",
             "show_details"        => "bool",
             "discount_amount"     => "ed|numeric",
             "discount_max_amount" => "ed|numeric",
             "discount_percent"    => "ed|numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($hashid = false)
    {
        $serialized = $this->data['_rulebook'];

        $this->model = model('coupon-rulebook')::unserialize($serialized);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->discoverUser();
        $this->model->applicable_amount = $this->data['discount_amount'];

        if ($this->data['discount_percent']) {
            $this->model->applicable_percent = $this->data['discount_percent'];
            $this->model->applicable_amount  = $this->data['discount_max_amount'];
        }

    }



    /**
     * discover user by the given username
     */
    protected function discoverUser()
    {
        if ($this->data['username']) {
            $this->data['user'] = user()->findByUsername($this->data['username']);
            return;
        }

        $this->data['user'] = model('user');
    }

}
