<?php

namespace Modules\Coupons\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RulebookSaveRequest extends YasnaRequest
{
    protected $serialized = false;
    protected $model_name = "coupon-rulebook";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->model->isset()) {
            return coupon()->canCreateRulebook();
        }

        return coupon()->canEditRulebook();
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'title',
             'model_name',
             'code_usage_limit',
        ];
    }



    /**
     * get main validation rules
     *
     * @return array
     */
    public function mainRules()
    {
        $id = $this->data['id'];

        return [
             "title" => "required|unique:coupon_rulebooks,title,$id",
        ];
    }



    /**
     * get type-dependant validation rules
     *
     * @return array
     */
    public function typeRules()
    {
        if ($this->data['_discount_type'] == 'percent') {
            return [
                 "_discount_percent" => "required|numeric|min:1|max:100",
            ];
        }

        return [
             "_discount_amount" => "required|numeric|min:1",
        ];
    }



    /**
     * get the user-dependant validation rules
     *
     * @return array
     */
    public function usernameRules()
    {
        if ($this->data['_user_type'] != 'particular') {
            return [];
        }

        return [
             "_username" => "required",
             "user_id"   => "numeric|min:1",
        ];
    }



    /**
     * get the rules relative to the serializing system
     *
     * @return array
     */
    public function serializingRules()
    {
        if (!$this->serialized) {
            return [
                 "_rulebook" => "accepted",
            ];
        }

        return [];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "_discount_percent" => trans("coupons::editor.discount_amount"),
             "_discount_amount"  => trans("coupons::editor.discount_amount"),
             "_username"         => trans("coupons::rulebooks.username"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "_rulebook.accepted" => trans("coupons::editor.serializing_error"),
             "user_id.min"   => trans("coupons::rulebooks.user_not_found"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "_discount_percent"    => "ed|numeric",
             "_discount_max_amount" => "ed|numeric",
             "_discount_amount"     => "ed|numeric",
             "code_usage_limit"     => "ed|numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->normalizeAmounts();
        $this->loadSerializedModel();
        $this->generateMetaContent();
        $this->normalizeCodeUsageLimits();
        $this->discoverUserId();
    }



    /**
     * decide the `user_id` based on the given `_username`
     */
    protected function discoverUserId()
    {
        if ($this->data['_user_type'] != 'particular' or !$this->data['_username']) {
            $this->data['user_id'] = 0;
            return;
        }

        $user = user()->findByUsername($this->data['_username']);

        $this->data['user_id'] = $user->id;
    }



    /**
     * normalize code usage limits
     */
    protected function normalizeCodeUsageLimits()
    {
        if ($this->data['_usage_limit'] == 'unlimited') {
            $this->data['code_usage_limit'] = 0;
        } elseif ($this->data['_usage_limit'] == 'one') {
            $this->data['code_usage_limit'] = 1;
        }
    }



    /**
     * normalize amount lists
     */
    protected function normalizeAmounts()
    {
        if ($this->data['_discount_type'] == 'percent') {
            $this->data['applicable_percent'] = $this->data['_discount_percent'];
            $this->data['applicable_amount']  = $this->data['_discount_max_amount'];
        } else {
            $this->data['applicable_percent'] = 0;
            $this->data['applicable_amount']  = $this->data['_discount_amount'];
        }
    }



    /**
     * load serialized model included in the request
     */
    protected function loadSerializedModel()
    {
        try {
            $serialized       = model("coupon-rulebook")::unserialize($this->data['_rulebook']);
            $this->serialized = $serialized;
        } catch (\Exception $e) {
            $this->serialized = false;
        }
    }



    /**
     * generate the `rules` meta out of the serialized model
     */
    protected function generateMetaContent()
    {
        $this->data['rules'] = $this->serialized->getRules();
    }
}
