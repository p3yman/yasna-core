<?php

namespace Modules\Coupons\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RulebookUndeleteRequest extends YasnaRequest
{
    protected $model_name = "coupon-rulebook";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return coupon()->canRestoreRulebook();
    }
}
