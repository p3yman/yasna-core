<?php

namespace Modules\Coupons\Http\Requests;

class RuleDeleteRequest extends RuleSaveRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [];
    }
}
