<?php

Route::group([
     'middleware' => ['web','auth','is:admin'],
     'prefix'     => 'manage/coupons/rules',
     'namespace'  => 'Modules\Coupons\Http\Controllers',
], function () {
    Route::get('/add/{class}', 'RulesEditController@createForm')->name("coupons-add-rule");
    Route::get('/edit/{identifier}/{rule}', 'RulesEditController@editForm')->name("coupons-edit-rule");
    Route::get('/delete/{identifier}/{rule}', 'RulesEditController@deleteForm')->name("coupons-delete-rule");
    Route::post('/delete/', 'RulesEditController@delete')->name("coupons-delete-rule-action");
    Route::post('/save/', 'RulesEditController@save')->name("coupons-save-rule");
});
