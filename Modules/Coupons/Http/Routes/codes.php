<?php

Route::group([
     'middleware' => ['web','auth','is:admin'],
     'namespace'  => module('Coupons')->getControllersNamespace(),
     'prefix'     => 'manage/coupons/codes',
], function () {
    Route::get('tracker', 'CodesTrackerController@index')->name("coupons-code-tracker-form");
    Route::post('tracker', 'CodesTrackerController@submit')->name("coupons-code-tracker-submit");

    Route::get('{hashid}', 'CodesController@index')->name("coupons-codes-browser");
    Route::get('{hashid}/refresh', 'CodesController@refreshCodes')->name("coupons-code-refresh");
    Route::get('create/{rulebook}', 'CodesController@generateForm')->name("coupons-codes-create");
    Route::post('/generate', 'CodesController@generate')->name("coupons-codes-generate");

    Route::get('list/refresh-row/{hashid}', 'CodesController@listRefreshRow')->name("coupons-code-list-refresh");
    Route::post('list/action', 'CodesController@listAction')->name("coupons-codes-list-action");
    Route::get('list/{hashid}/{batch?}', 'CodesController@list')->name("coupons-codes-list");

    Route::get('download/excel', 'CodesController@downloadExcel')->name("coupons-codes-download-excel");
    Route::get('download/{hashid}/{batch?}', 'CodesController@downloadForm')->name("coupons-codes-download-form");
    Route::post('download', 'CodesController@downloadAction')->name("coupons-codes-download-action");
});
