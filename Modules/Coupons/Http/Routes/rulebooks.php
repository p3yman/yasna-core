<?php

Route::group([
     'middleware' => ['web','auth','is:admin'],
     'prefix'     => 'manage/coupons', //<~~ Intentionally avoided to include `rulebooks` keyword.
     'namespace'  => 'Modules\Coupons\Http\Controllers',
], function () {
    /*-----------------------------------------------
    | RulebooksEditController ...
    */
    Route::get('/{group}/new/{tab}', 'RulebooksEditController@createForm')->name("coupons-new-rulebook");
    Route::get('/{group}/edit/{hashid}', 'RulebooksEditController@editForm')->name("coupons-edit-rulebook");
    Route::post('/refresh-rule/', 'RulebooksEditController@refreshRules')->name("coupons-refresh-rules");
    Route::post("/save", "RulebooksEditController@save")->name("coupons-save");

    Route::get('/test-rule/', 'RulebooksEditController@testForm')->name("coupons-test-rule-form");
    Route::post('/test-rule/', 'RulebooksEditController@testRun')->name("coupons-test-rule");

    Route::get('/duplicate/{hashid}', 'RulebooksEditController@duplicate')->name("coupons-duplicate-rulebook");


    /*-----------------------------------------------
    | RulebooksBrowseController ...
    */
    Route::get('/refresh/{hashid}', 'RulebooksBrowseController@update')->name("coupons-refresh-rulebook");
    Route::get('browse/{group}/{tab?}', 'RulebooksBrowseController@index')->name("coupons-browse-rulebook");

    /*-----------------------------------------------
    | RulebooksActionController ...
    */
    Route::get('/act/{hash_id}/{action}', 'RulebooksActionController@singleAction');
    Route::post('/delete', 'RulebooksActionController@delete')->name("coupons-delete-rulebook");
    Route::post('/undelete', 'RulebooksActionController@undelete')->name("coupons-undelete-rulebook");
    Route::post('/destroy', 'RulebooksActionController@hardDelete')->name("coupons-destroy-rulebook");

});
