<?php

namespace Modules\Coupons\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\YasnaController;

class RulebooksBrowseController extends YasnaController
{
    /**
     * @inheritdoc
     */
    protected $base_model = "coupon-rulebook";

    /**
     * @inheritdoc
     */
    protected $view_folder = "coupons::rulebooks.browse";

    /**
     * keep the requested group of coupons (registered via service provider)
     *
     * @var string
     */
    protected $request_group;

    /**
     * keep the title of the requested group from the registered service
     *
     * @var string
     */
    protected $request_group_caption;

    /**
     * keep the browser tabs
     *
     * @var array
     */
    protected $tabs;

    /**
     * keep the requested tab
     *
     * @var string
     */
    protected $request_tab;

    /**
     * keep page Information (used most probably on nav-bar)
     *
     * @var array
     */
    protected $page;

    /**
     * keep models collection
     *
     * @var Collection
     */
    protected $models;



    /**
     * return browse index
     *
     * @param string $request_group
     * @param string $request_tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($request_group, $request_tab = null)
    {
        if (coupon()->cannotBrowseRulebook()) {
            return $this->abort(403);
        }

        $this->setRequestedGroup($request_group);
        $this->setRequestedGroupCaption();
        $this->setTabs();
        $this->setRequestedTab($request_tab);
        $this->setPageInfo();
        $this->setModels();
        $this->registerAssets();

        return $this->view("index", $this->getViewArray());
    }



    /**
     * set the requested group and abort if not allowed
     *
     * @param string $request_group
     */
    protected function setRequestedGroup(string $request_group)
    {
        $allowed_groups = array_keys(service("coupons:models")->read());

        if (not_in_array($request_group, $allowed_groups)) {
            abort(404);
        }

        $this->request_group = $request_group;
    }



    /**
     * set the title of the requested group from the registered service
     */
    protected function setRequestedGroupCaption()
    {
        $groups = service("coupons:models")->read();

        $this->request_group_caption = $groups[$this->request_group]['caption'];
    }



    /**
     * set the browser tabs
     */
    protected function setTabs()
    {
        $allowed_tabs = ['general', 'particular', 'bin'];//, 'search'];
        $result       = [];

        foreach ($allowed_tabs as $tab) {
            $result[$tab] = [
                 "caption" => trans_safe("coupons::rulebooks.$tab"),
                 "url"     => "$this->request_group/$tab",
            ];
        }

        $this->tabs = $result;
    }



    /**
     * set the requested tab and abort if now allowed
     *
     * @param string $request_tab
     */
    protected function setRequestedTab($request_tab)
    {
        $allowed_tabs = array_keys($this->tabs);

        if (!$request_tab or empty($request_tab)) {
            $request_tab = $allowed_tabs[0];
        }
        if (not_in_array($request_tab, $allowed_tabs)) {
            abort(404);
        }

        $this->request_tab = $request_tab;
    }



    /**
     * set page Information (used most probably on nav-bar)
     */
    protected function setPageInfo()
    {
        $this->page[0] = [
             $url = "coupons/browse",
             trans_safe("coupons::rulebooks.title"),
             $url,
        ];

        $this->page[1] = [
             $this->request_group,
             $this->request_group_caption,
             $url = "$url/$this->request_group",
        ];

        $this->page[2] = [
             $this->request_tab,
             $this->tabs[$this->request_tab]['caption'],
             "$url/$this->request_tab",
        ];

    }



    /**
     * set models
     */
    protected function setModels()
    {
        $this->models = model($this->base_model)
             ->elector([
                  "group"    => $this->request_group,
                  "criteria" => $this->request_tab,
             ])
             ->orderBy("id", "desc")
             ->paginate(20)
        ;
    }



    /**
     * get the array required to render the view
     *
     * @return array
     */
    protected function getViewArray(): array
    {
        return [
             "current_group" => $this->request_group,
             "current_tab"   => $this->request_tab,
             "tabs"          => $this->tabs,
             "page"          => $this->page,
             "models"        => $this->models,
        ];
    }



    /**
     * Registers page assets
     */
    public function registerAssets()
    {
        /*-----------------------------------------------
        | Easy pie Chart ...
        */
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("manage:libs/vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js")
             ->order(35)
        ;
    }
}
