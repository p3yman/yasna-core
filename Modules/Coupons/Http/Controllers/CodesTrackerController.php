<?php

namespace Modules\Coupons\Http\Controllers;

use Modules\Coupons\Entities\CouponCode;
use Modules\Coupons\Http\Requests\CodeTrackRequest;
use Modules\Yasna\Services\YasnaController;

class CodesTrackerController extends YasnaController
{
    protected $base_model  = "coupon-code";
    protected $view_folder = "coupons::codes.tracker";



    /**
     * return an empty tracker blade
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if (coupon()->cannotReportCodes()) {
            return $this->abort(403);
        }

        return $this->view("index");
    }



    /**
     * submit code tracker first stage
     *
     * @param CodeTrackRequest $request
     *
     * @return string
     */
    public function submit(CodeTrackRequest $request)
    {
        $model  = $request->model;
        $method = camel_case("submit-$request->_submit");

        if (!$model->exists) {
            return $this->jsonFeedback(trans("coupons::codes.code_not_found"));
        }

        if ($this->hasMethod($method)) {
            return $this->$method($model);
        }


        return $this->jsonFeedback();
    }



    /**
     * return track result
     *
     * @param CouponCode $model
     *
     * @return string
     */
    protected function submitTrack($model)
    {
        return $this->jsonFeedback([
             "feed_class" => " ",
             "message"    => $this->view("result", compact('model'))->render(),
        ]);
    }



    /**
     * delete a code and return track result
     *
     * @param CouponCode $model
     *
     * @return string
     */
    protected function submitDelete($model)
    {
        $model->delete();

        return $this->submitTrack($model);
    }



    /**
     * restore a deleted code and return track result
     *
     * @param CouponCode $model
     *
     * @return string
     */
    protected function submitRestore($model)
    {
        $model->undelete();

        return $this->submitTrack($model);
    }
}
