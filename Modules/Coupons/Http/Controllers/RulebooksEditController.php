<?php

namespace Modules\Coupons\Http\Controllers;

use Modules\Coupons\CouponRules\DateLimitRule;
use Modules\Coupons\Http\Requests\RulebookSaveRequest;
use Modules\Coupons\Http\Requests\RuleTestRequest;
use Modules\Coupons\Services\InvoiceObject;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Symfony\Component\HttpFoundation\Response;
use Modules\Coupons\Entities\CouponRulebook;
use Modules\Yasna\Services\YasnaController;

/**
 * Class RulebooksEditController
 * @method CouponRulebook model($id = 0, $with_trashed = 0)
 *
 * @package Modules\Coupons\Http\Controllers
 */
class RulebooksEditController extends YasnaController
{
    /**
     * @inheritdoc
     */
    protected $base_model = "coupon-rulebook";

    /**
     * @inheritdoc
     */
    protected $view_folder = "coupons::rulebooks.editor";



    /**
     * RulebooksEditController constructor.
     */
    public function __construct()
    {
        $this->registerPageAssets();
    }



    /**
     * return the editor blade for a new rulebook
     *
     * @param string $group
     * @param string $tab
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function createForm(string $group, string $tab)
    {
        if (coupon()->cannotCreateRulebook()) {
            return $this->abort(403);
        }

        if (not_in_array($tab, ["particular", "general"])) {
            $tab = "general";
        }

        $model                   = $this->model();
        $model->model_name       = $group;
        $model->tab              = $tab;
        $model->code_usage_limit = 1;

        $model = $this->addDefaultRulesToNewModel($model, $tab);

        return $this->showForm($model);
    }



    /**
     * return the editor blade for an existing rulebook
     *
     * @param string $group
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function editForm(string $group, $hashid)
    {
        $model = $this->model($hashid);
        if (!$model->exists) {
            abort(404);
        }

        if (coupon()->cannotEditRulebook()) {
            return $this->abort(403);
        }

        return $this->showForm($model->spreadMeta());
    }



    /**
     * return the editor blade for a duplicated rulebook
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function duplicate(string $hashid)
    {
        $model = $this->model($hashid);
        if (!$model->exists) {
            abort(404);
        }

        if (coupon()->cannotCreateRulebook()) {
            return $this->abort(403);
        }

        $model->id    = 0;
        $model->title = null;

        return $this->showForm($model);
    }



    /**
     * refresh the rules list section of the editor page by the serialized data passed via the POST request
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function refreshRules(SimpleYasnaRequest $request)
    {
        $model = CouponRulebook::unserialize($request->rulebook);

        return $this->view("rules-current", compact("model"));
    }



    /**
     * save the rulebook
     *
     * @param RulebookSaveRequest $request
     *
     * @return string
     */
    public function save(RulebookSaveRequest $request): string
    {
        $saved_model = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             "success_callback" => "$('#divSaveWarning').slideUp()",
             'success_redirect' => $request->model->exists ? '' : $saved_model->edit_link,
        ]);
    }



    /**
     * return the test blade form
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function testForm()
    {
        return $this->view("test-form");
    }



    /**
     * run the test and return the result
     *
     * @param RuleTestRequest $request
     *
     * @return string
     */
    public function testRun(RuleTestRequest $request)
    {
        $invoice = $this->getTestInvoice($request);
        $invoice = coupon()->simulate($request->model, $invoice);
        $total   = $invoice->getTotalDiscount();

        if ($request->_submit == 'details' and dev()) {
            return $this->jsonFeedback([
                 "feed_class" => " ",
                 "message"    => $this->view("test-result", compact('invoice'))->render(),
            ]);
        }

        return $this->jsonFeedback([
             "ok"      => "1",
             "message" => trans("coupons::tester.applicable_discount") . SPACE . ad(number_format($total)),
        ]);
    }



    /**
     * get the InvoiceObject required for the purpose of the test
     *
     * @param RuleTestRequest $request
     *
     * @return InvoiceObject
     */
    protected function getTestInvoice(RuleTestRequest $request): InvoiceObject
    {
        $invoice = coupon()->makeInvoice($request->effective_date, $request->total_amount);

        if ($request->username) {
            $invoice->setUser($request->user);
        }

        return $invoice;
    }



    /**
     * add default rules to the new model
     *
     * @param CouponRulebook $model
     * @param string         $tab
     *
     * @return CouponRulebook
     */
    protected function addDefaultRulesToNewModel(CouponRulebook $model, string $tab): CouponRulebook
    {
        if ($tab == 'general') {
            $model->addRule(DateLimitRule::class, []);
        } else {
            //TODO: Add Particular user condition when got ready
        }

        return $model;
    }



    /**
     * render the form with additional required parameters
     *
     * @param CouponRulebook $model
     *
     * @return \Illuminate\Support\Facades\View
     */
    protected function showForm(CouponRulebook $model)
    {
        $arguments = [
             "model"           => $model,
             "available_rules" => $this->getAvailableRulesArray(),
             "page"            => $this->getNavBar($model),
        ];

        return $this->view("index", $arguments);
    }



    /**
     * create and return the page nav-bar
     *
     * @param CouponRulebook $model
     *
     * @return array
     */
    protected function getNavBar($model)
    {
        $array = [];

        $array[] = [
             $url = "coupons/" . $model->model_name,
             trans_safe("coupons::rulebooks.title"),
             $url,
        ];

        if ($model->isset()) {
            $array[] = [
                 $additive = "edit/$model->hashid",
                 trans("coupons::rulebooks.edit"),
                 "$url/$additive",
            ];
        } else {
            $array[] = [
                 $additive = "new/$model->tab",
                 trans("coupons::rulebooks.new"),
                 "$url/$additive",
            ];
        }

        return $array;
    }



    /**
     * register page assets
     */
    protected function registerPageAssets()
    {
        //module('manage')
        //     ->service('template_bottom_assets')
        //     ->add("easy-pie-chart")
        //     ->link("manage:libs/vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js")
        //     ->order(35)
        //;

        module('manage')
             ->service('template_bottom_assets')
             ->add('coupons-js')
             ->link("coupons:js/coupons.min.js")
             ->order(42)
        ;
    }



    /**
     * get an array of the available rules
     *
     * @return array
     */
    protected function getAvailableRulesArray(): array
    {
        return coupon()->getRegisteredRules();
    }
}
