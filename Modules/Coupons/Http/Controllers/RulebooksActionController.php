<?php

namespace Modules\Coupons\Http\Controllers;

use Modules\Coupons\Http\Requests\RulebookDeleteRequest;
use Modules\Coupons\Http\Requests\RulebookUndeleteRequest;
use Modules\Yasna\Services\YasnaController;

class RulebooksActionController extends YasnaController
{
    protected $base_model  = "coupon-rulebook";
    protected $view_folder = "coupons::rulebooks.actions";



    /**
     * delete rulebook action
     *
     * @param RulebookDeleteRequest $request
     *
     * @return string
     */
    public function delete(RulebookDeleteRequest $request)
    {
        $deleted = $request->model->delete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "rowHide('tblRulebooks','$request->hashid')",
        ]);
    }



    /**
     * undelete rulebook action
     *
     * @param RulebookUndeleteRequest $request
     *
     * @return string
     */
    public function undelete(RulebookUndeleteRequest $request)
    {
        $deleted = $request->model->undelete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "rowHide('tblRulebooks','$request->hashid')",
        ]);
    }



    /**
     * hardDelete rulebook action
     *
     * @param RulebookUndeleteRequest $request
     *
     * @return string
     */
    public function hardDelete(RulebookUndeleteRequest $request)
    {
        $deleted = $request->model->hardDelete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "rowHide('tblRulebooks','$request->hashid')",
        ]);
    }
}
