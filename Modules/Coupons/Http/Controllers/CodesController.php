<?php

namespace Modules\Coupons\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use Modules\Coupons\Entities\CouponRulebook;
use Modules\Coupons\Http\Requests\CodeGenerateRequest;
use Modules\Coupons\Http\Requests\CodesDownloadRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class CodesController extends YasnaController
{
    protected $base_model  = "coupon-code";
    protected $view_folder = "coupons::codes";



    /**
     * return the editor blade for the browser page
     *
     * @param string $rulebook_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(string $rulebook_hashid)
    {
        $model = couponRulebook($rulebook_hashid);
        if (!$model->exists) {
            return $this->abort(410);
        }
        if (coupon()->cannotBrowseCodes()) {
            return $this->abort(403);
        }

        $this->registerPageAssets();

        return $this->view("browse.index", [
             "model"       => $model,
             "total_codes" => $model->codes()->count(),
             "page"        => $this->getNavBar($model),
        ]);
    }



    /**
     * return the index blade for the code list page
     *
     * @param string $rulebook_hashid
     * @param int    $batch
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function list(string $rulebook_hashid, $batch = 0)
    {
        $rulebook = couponRulebook($rulebook_hashid);
        if (!$rulebook->exists) {
            return $this->abort(410);
        }
        if (coupon()->cannotBrowseCodes()) {
            return $this->abort(403);
        }

        $codes = couponCode()->elector([
             "rulebook" => $rulebook_hashid,
             "batch"    => $batch,
        ])->withTrashed()->orderBy("used_count")->get()
        ;

        $modal_title = trans("coupons::codes.title");
        if ($batch) {
            $modal_title .= SPACE . trans("coupons::codes.batch-title", ['n' => $batch]);
        }

        return $this->view("list.index", compact("rulebook", "codes", "modal_title"));
    }



    /**
     * code list actions
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function listAction(SimpleYasnaRequest $request)
    {
        if (str_contains($request->_submit, "delete-")) {
            return $this->listActionDelete(str_after($request->_submit, "-"));
        }

        if (str_contains($request->_submit, "restore-")) {
            return $this->listActionRestore(str_after($request->_submit, "-"));
        }

        return $this->jsonFeedback();
    }



    /**
     * refresh a row in the code list view
     *
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function listRefreshRow($hashid)
    {
        $model  = $this->model($hashid, true);
        $reload = true;

        if (!$model->exists) {
            return $this->abort(410);
        }
        if (coupon()->cannotBrowseCodes()) {
            return $this->abort(403);
        }

        return $this->view("list.row", compact("model", "reload"));
    }



    /**
     * return the code section blade for the browser page
     *
     * @param string $rulebook_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshCodes(string $rulebook_hashid)
    {
        $model       = couponRulebook($rulebook_hashid);
        $total_codes = $model->getTotalCodes();

        if (!$model->exists) {
            return $this->abort(410);
        }
        if (coupon()->cannotBrowseCodes()) {
            return $this->abort(403);
        }


        return $this->view("browse.codes", compact("model", "total_codes"));
    }



    /**
     * return the blade responsible to draw the code generation form
     *
     * @param string $rulebook_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function generateForm($rulebook_hashid)
    {
        $rulebook = $this->findRulebook($rulebook_hashid);
        if (!$rulebook->exists) {
            return $this->abort(410);
        }
        if (coupon()->cannotCreateCodes()) {
            return $this->abort(403);
        }

        $arguments = [
             "hashid"        => $rulebook_hashid,
             "patterns"      => $this->getCodePatterns(),
             "is_particular" => $rulebook->is_particular,
        ];

        return $this->view("editor.index", $arguments);
    }



    /**
     * generate the codes
     *
     * @param CodeGenerateRequest $request
     *
     * @return string
     */
    public function generate(CodeGenerateRequest $request)
    {
        /**
         * @var \App\Models\CouponRulebook $model
         */
        $model = $request->model;

        $total_generated = $model->generateCodes(
             $request->quantity,
             $request->prefix,
             $request->pattern,
             $request->length,
             $request->postfix
        );

        return $this->jsonAjaxSaveFeedback($total_generated, [
             'success_callback' => "divReload('divCouponCodes')",
             "success_message"  => trans("coupons::codes.n_codes_generated", [
                  "n" => ad($total_generated),
             ]),
        ]);
    }



    /**
     * return the blade responsible to export codes
     *
     * @param string $rulebook_hashid
     * @param int    $batch
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function downloadForm($rulebook_hashid, $batch = 0)
    {
        $model = couponRulebook($rulebook_hashid);
        if (!$model->exists) {
            return $this->abort(410);
        }
        if (coupon()->cannotExportCodes()) {
            return $this->abort(403);
        }

        return $this->view("action.download", compact("model", "batch"));
    }



    /**
     * download action
     *
     * @param CodesDownloadRequest $request
     *
     * @return string
     */
    public function downloadAction(CodesDownloadRequest $request)
    {
        $data = [
             $request->model->hashid,
             $request->batch,
             $request->filter,
        ];
        session()->put('coupon-codes', implode(',', $data));

        return $this->jsonAjaxSaveFeedback(true, [
             'success_redirect' => route("coupons-codes-download-excel"),
        ]);
    }



    /**
     * download an excel file.
     */
    public function downloadExcel()
    {
        if (coupon()->cannotExportCodes()) {
            $this->abort(403);
            return;
        }

        Excel::create("discount-codes", function ($excel) {
            $excel->sheet("print", function ($sheet) {
                $sheet->loadView("$this->view_folder.action.excel");
            });
        })->download('xls')
        ;
    }



    /**
     * get an array of code patterns and their equivalent title
     *
     * @return array
     */
    protected function getCodePatterns(): array
    {
        $choices = ['numeric', 'alphabetic', 'alphanumeric'];
        $result  = [];

        foreach ($choices as $choice) {
            $result[] = [
                 $choice,
                 trans("coupons::codes.$choice"),
            ];
        }

        return $result;
    }



    /**
     * find and get Rulebook instance by its hashid
     *
     * @param string $hashid
     *
     * @return CouponRulebook
     */
    protected function findRulebook(string $hashid)
    {
        return model('coupon-rulebook', $hashid);
    }



    /**
     * register page assets
     */
    protected function registerPageAssets()
    {
        module('manage')
             ->service('template_bottom_assets')
             ->add("easy-pie-chart")
             ->link("manage:libs/vendor/jquery.easy-pie-chart/dist/jquery.easypiechart.js")
             ->order(35)
        ;

        module('manage')
             ->service('template_bottom_assets')
             ->add('coupons-js')
             ->link("coupons:js/coupons.min.js")
             ->order(42)
        ;
    }



    /**
     * create and return the page nav-bar
     *
     * @param CouponRulebook $model
     *
     * @return array
     */
    protected function getNavBar($model)
    {
        $array = [];

        $array[] = [
             $url = "coupons/" . $model->model_name,
             trans_safe("coupons::rulebooks.title"),
             $url,
        ];

        $array[] = [
             "coupons/codes/$model->hashid",
             trans_safe("coupons::codes.title"),
             "coupons/codes/$model->hashid",
        ];

        return $array;
    }



    /**
     * code list delete action
     *
     * @param string $hashid
     *
     * @return string
     */
    protected function listActionDelete($hashid)
    {
        if (coupon()->cannotDeleteCodes()) {
            return $this->jsonFeedback();
        }

        $deleted = $this->model($hashid)->delete();

        return $this->jsonFeedback([
             "callback" => "rowUpdate('tblCodes','$hashid')",
        ]);
    }



    /**
     * code list undelete action
     *
     * @param string $hashid
     *
     * @return string
     */
    protected function listActionRestore($hashid)
    {
        if (coupon()->cannotRestoreCodes()) {
            return $this->jsonFeedback();
        }

        $done = $this->model($hashid, true)->undelete();

        return $this->jsonFeedback([
             "callback" => "rowUpdate('tblCodes','$hashid')",
        ]);
    }
}
