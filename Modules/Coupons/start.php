<?php

if (!function_exists("coupon")){
    /**
     * get an instance of CouponHelper
     *
     * @return \Modules\Coupons\Services\CouponHelper
     */
    function coupon()
    {
        return new \Modules\Coupons\Services\CouponHelper();
    }
}

if (!function_exists("couponRulebook")) {

    /**
     * return a model instance
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\CouponRulebook
     */
    function couponRulebook($id_or_hashid = 0, $with_trashed = false)
    {
        return model('coupon-rulebook', $id_or_hashid, $with_trashed);
    }

}

if (!function_exists("couponCode")) {

    /**
     * return a model instance
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\CouponCode
     */
    function couponCode($id_or_hashid = 0, $with_trashed = false)
    {
        return model('coupon-code', $id_or_hashid, $with_trashed);
    }
}
