<?php

namespace Modules\Coupons\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Coupons\Services\InvoiceObject;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponQuery extends YasnaModel
{
    use SoftDeletes;



    /**
     * get the code of this usage record
     *
     * @return BelongsTo
     */
    public function code()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "CouponCode", "code_id");
    }



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             'invoice',
        ];
    }



    /**
     * get the invoice object stored in the meta field
     *
     * @return InvoiceObject|bool
     */
    public function getInvoiceAttribute()
    {
        $serialized = $this->getMeta('invoice');

        if(!$serialized) {
            return false;
        }

        return InvoiceObject::unserialize($serialized);
    }

}
