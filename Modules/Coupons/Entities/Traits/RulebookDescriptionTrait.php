<?php

namespace Modules\Coupons\Entities\Traits;

/**
 * Class CouponRulebook
 *
 * @property int $applicable_percent
 * @property int $applicable_amount
 */
trait RulebookDescriptionTrait
{
    /**
     * get the description of the rulebook
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return $this->getDescriptionPercent() . SPACE . $this->getDescriptionAmount();
    }



    /**
     * get the percent part of the description
     *
     * @return string
     */
    protected function getDescriptionPercent()
    {
        if (!$this->applicable_percent) {
            return null;
        }

        return trans("coupons::rulebooks.description_percent", [
             "n" => ad($this->applicable_percent),
        ]);
    }



    /**
     * get the amount part of the description
     *
     * @return string
     */
    protected function getDescriptionAmount()
    {
        if (!$this->applicable_amount) {
            return null;
        }

        return trans("coupons::rulebooks.description_amount", [
             "n" => ad(number_format($this->applicable_amount)),
        ]);
    }
}
