<?php

namespace Modules\Coupons\Entities\Traits;

/**
 * Trait CodeSlugTrait
 * Belongs to CouponCode class
 */
trait CodeSlugTrait
{
    /**
     * get a randomly generated slug, making sure it have never been used in the database
     *
     * @param string $prefix
     * @param int    $pattern_type
     * @param int    $pattern_length
     * @param string $postfix
     *
     * @return string
     */
    public static function getRandomUniqueSlug($prefix, $pattern_type = 1, $pattern_length = 5, $postfix = null)
    {
        $max_tries = 5;
        $tried     = 0;

        do {
            $string = static::getRandomSlug($prefix, $pattern_type, $pattern_length, $postfix);
            $used   = static::where('slug', $string)->withTrashed()->count();

            if (!$used) {
                return $string;
            }
        } while ($tried++ < $max_tries);

        return null;
    }



    /**
     * get a randomly generated slug
     *
     * @param string $prefix
     * @param int    $pattern_type
     * @param int    $pattern_length
     * @param string $postfix
     *
     * @return string
     */
    private static function getRandomSlug($prefix, $pattern_type = 1, $pattern_length = 5, $postfix = null)
    {
        $pieces = [
             $prefix,
             static::getPattern($pattern_type, $pattern_length),
             $postfix,
        ];

        return implode("-", array_filter($pieces));
    }



    /**
     * get random pattern, based on the specified type and length
     *
     * @param int $type
     * @param int $length
     *
     * @return string
     */
    private static function getPattern(int $type = 1, int $length = 5)
    {
        $numbers   = "123456789";
        $alphabets = "BCDFGHJKLMNPQRSTVWXZ";
        $string    = $alphabets . $numbers;
        $result    = "";

        if ($type == static::SLUG_TYPE_NUMERIC) {
            $string = $numbers;
        } elseif ($type == static::SLUG_TYPE_ALPHABETIC) {
            $string = $alphabets;
        }

        $result .= substr(str_shuffle($string), 0, $length);

        return $result;
    }
}
