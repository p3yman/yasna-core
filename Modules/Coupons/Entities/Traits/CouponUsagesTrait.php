<?php

namespace Modules\Coupons\Entities\Traits;

/**
 * Trait CouponUsagesTrait This trait prepare ability of working with coupons of
 * a model.
 *
 * @package Modules\Coupons\Entities\Traits
 * @property \Illuminate\Database\Eloquent\Collection coupons
 * @property float                                    applied_discounts_amount
 */
trait CouponUsagesTrait
{
    /**
     *  return all coupons attached to a model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function coupons()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'CouponUsage', 'model_id')
                    ->where([
                         'model_name' => $this->getClassName(),
                    ])
             ;
    }



    /**
     * return total amount of applied discount on model and define an
     * artificial variable called `applied_discounts_amount`
     *
     * @return float
     */
    public function getAppliedDiscountsAmountAttribute()
    {
        return $this->coupons()->sum('applied_discount');
    }
}
