<?php

namespace Modules\Coupons\Entities\Traits;

trait CodeElectorTrait
{
    /**
     * elector for rulebook
     *
     * @param string $hashid
     */
    public function electorRulebook($hashid)
    {
        $this->elector()->where('rulebook_id', hashid($hashid));
    }



    /**
     * elector for batch number
     *
     * @param int $batch
     */
    public function electorBatch($batch)
    {
        if ($batch) {
            $this->elector()->where("batch", $batch);
        }
    }



    /**
     * elector for filters
     *
     * @param string $filter
     */
    public function electorFilter($filter)
    {
        if ($filter == 'unused') {
            $this->elector()->where("used_count", 0);
            return;
        }

        if($filter=='usable') {
            $rulebook = couponRulebook($this->getElector("rulebook"));
            $limit = $rulebook->getMeta("code_usage_limit");

            if(!$limit) {
                return;
            }

            $this->elector()->where("used_count" , "<" , $limit);
            return;
        }

    }
}
