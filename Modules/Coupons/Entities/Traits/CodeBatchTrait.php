<?php

namespace Modules\Coupons\Entities\Traits;

use App\Models\CouponUsage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CouponCode
 *
 * @property int $rulebook_id
 * @property int $batch
 * @package Modules\Coupons\Entities\Traits
 */
trait CodeBatchTrait
{
    /**
     * keep the number of the codes with the same rulebook and batch number
     *
     * @var bool|int
     */
    protected $sisters_count = false;

    /**
     * keep the number of the used codes with the same rulebook and batch number
     *
     * @var bool|int
     */
    protected $used_sisters_count = false;

    /**
     * keep the number of the unused codes with the same rulebook and batch number
     *
     * @var bool|int
     */
    protected $unused_sisters_count = false;

    /**
     * keep the usage percent of the codes with the same rulebook and batch number
     *
     * @var bool|int
     */
    protected $sisters_usage_percent = false;



    /**
     * get batch title
     *
     * @return string
     */
    public function getBatchTitleAttribute()
    {
        $part1 = trans("coupons::codes.batch-title", [
             "n" => $this->batch,
        ]);
        $part2 = echoDate($this->created_at);

        return ad("$part1: $part2");
    }



    /**
     * get a builder of the sisters (records with same parent and same batch number)
     *
     * @return Builder
     */
    public function sisters()
    {
        return static::where("rulebook_id", $this->rulebook_id)->where("batch", $this->batch);
    }



    /**
     * make an accessor to get a collection of the sisters
     *
     * @return Collection
     */
    public function getSistersAttribute()
    {
        return $this->sisters()->get();
    }



    /**
     * get the total number of sisters
     *
     * @param bool $fresh
     *
     * @return int
     */
    public function getSistersCount($fresh = false)
    {
        $this->calculateSistersUsageStats($fresh);

        return $this->sisters_count;
    }



    /**
     * get the total number of unused sisters
     *
     * @param bool $fresh
     *
     * @return int
     */
    public function getUsedSistersCount($fresh = false)
    {
        $this->calculateSistersUsageStats($fresh);

        return $this->used_sisters_count;
    }



    /**
     * get the total number of unused sisters
     *
     * @param bool $fresh
     *
     * @return int
     */
    public function getUnusedSistersCount($fresh = false)
    {
        $this->calculateSistersUsageStats($fresh);

        return $this->unused_sisters_count;
    }



    /**
     * get the usage percent of sisters
     *
     * @param bool $fresh
     *
     * @return bool
     */
    public function getSistersUsagePercent($fresh = false)
    {
        $this->calculateSistersUsageStats($fresh);

        return $this->sisters_usage_percent;
    }



    /**
     * get the total discounted amount of the codes with the same rulebook and batch number
     *
     * @return float
     */
    public function getSistersTotalDiscounted()
    {
        $id_array = $this->sisters()->get()->pluck('id')->toArray();

        return CouponUsage::getTotalDiscount($id_array);
    }



    /**
     * calculate sisters usage stats and stores them in protected properties
     *
     * @param bool $force_fresh
     *
     * @return array
     */
    protected function calculateSistersUsageStats($force_fresh = false)
    {
        if ($force_fresh or $this->sisters_count === false) {
            $this->sisters_count         = $this->sisters()->count();
            $this->unused_sisters_count  = $this->sisters()->where('used_count', 0)->count();
            $this->used_sisters_count    = $this->sisters_count - $this->unused_sisters_count;
            $this->sisters_usage_percent = floor($this->used_sisters_count * 100 / $this->sisters_count);
        }

        return $this->getAllSisterUsageStats();
    }



    /**
     * get all sister usage stats
     *
     * @return array
     */
    public function getAllSisterUsageStats()
    {
        return [
             "total"   => $this->sisters_count,
             "used"    => $this->used_sisters_count,
             "unused"  => $this->unused_sisters_count,
             "percent" => $this->sisters_usage_percent,
        ];
    }
}
