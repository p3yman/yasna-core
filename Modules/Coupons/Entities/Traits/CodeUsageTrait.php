<?php

namespace Modules\Coupons\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Coupons\Entities\CouponRulebook;

/**
 * Class CouponCode
 *
 * @property string         $model_name
 * @property CouponRulebook $rulebook
 * @property int            $used_count
 * @property int            $remained_count
 */
trait CodeUsageTrait
{
    private $usage_limit_cached = false;



    /**
     * get the usages of this code
     *
     * @return HasMany
     */
    public function usages()
    {
        return $this->hasMany(MODELS_NAMESPACE . "CouponUsage", "code_id");
    }



    /**
     * get the usages of this code
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUsagesAttribute()
    {
        return $this->usages()->orderBy('id', 'desc')->get();
    }



    /**
     * mark a coupon code as used
     *
     * @param int              $model_id
     * @param int              $user_id
     * @param int|double|float $applied_discount
     *
     * @return bool
     */
    public function markAsUsed(int $model_id, int $user_id, $applied_discount)
    {
        $is_saved = $this->makeUsageRecord($model_id, $user_id, $applied_discount);
        if ($is_saved) {
            $this->updateSelfUsageFields();
        }

        return $is_saved;
    }



    /**
     * create a usage record in the relationship table
     *
     * @param int              $model_id
     * @param int              $user_id
     * @param int|double|float $applied_discount
     *
     * @return bool
     */
    protected function makeUsageRecord(int $model_id, int $user_id, $applied_discount)
    {
        return model('coupon-usage')->batchSaveBoolean([
             "code_id"          => $this->id,
             "user_id"          => $user_id,
             "model_name"       => $this->model_name,
             "model_id"         => $model_id,
             "applied_discount" => $applied_discount,

        ]);
    }



    /**
     * update usage fields in the current model
     *
     * @return bool
     */
    protected function updateSelfUsageFields()
    {
        return $this->batchSaveBoolean([
             "used_count" => $this->usages()->count(),
        ]);
    }



    /**
     * get usage limit of the code, according to what defined in its parent rulebook
     *
     * @return int
     */
    public function getUsageLimit()
    {
        return max((int)$this->rulebook->getMeta("code_usage_limit"), 0);
    }



    /**
     * get usage limit of the code, according to what defined in its parent rulebook and the one already cached
     *
     * @return int
     */
    public function getUsageLimitAttribute()
    {
        if ($this->usage_limit_cached === false) {
            $this->usage_limit_cached = $this->getUsageLimit();
        }

        return $this->usage_limit_cached;
    }



    /**
     * get the number of remained usages
     *
     * @return int
     */
    public function getRemainedCountAttribute()
    {
        $limit = $this->getUsageLimit();

        if (!$limit) {
            return 999999;
        }

        return max($limit - $this->used_count, 0);
    }



    /**
     * return used percentage
     *
     * @return float|int
     */
    public function usedPercent()
    {
        if (!$this->usage_limit) {
            return 0;
        }

        return min(round($this->used_count * 100 / $this->usage_limit), 100);
    }



    /**
     * return used percentage
     *
     * @return float|int
     */
    public function getUsedPercentAttribute()
    {
        return $this->usedPercent();
    }



    /**
     * return remained percentage
     *
     * @return float|int
     */
    public function remainedPercent()
    {
        if (!$this->usage_limit) {
            return 0;
        }

        return min(round($this->remained_count * 100 / $this->usage_limit), 100);
    }



    /**
     * return remained percentage
     *
     * @return float|int
     */
    public function getRemainedPercentAttribute()
    {
        return $this->remainedPercent();
    }



    /**
     * get the total discounts of the given code
     *
     * @return float
     */
    public function getTotalDiscountedAttribute()
    {
        return $this->usages()->sum('applied_discount');
    }
}
