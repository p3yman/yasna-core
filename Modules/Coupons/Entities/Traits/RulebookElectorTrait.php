<?php

namespace Modules\Coupons\Entities\Traits;

use Watson\Rememberable\Query\Builder;

/**
 * add elector methods to the CouponRulebook class
 * @method Builder elector()
 */
trait RulebookElectorTrait
{
    /**
     * add criteria conditions to the elector query
     *
     * @param string $criteria
     *
     * @return void
     */
    protected function electorCriteria($criteria)
    {
        if ($criteria == "bin") {
            $this->elector()->onlyTrashed();
            return;
        }

        if ($criteria == "particular") {
            $this->elector()->where('user_id', ">", 0);
        } else {
            $this->elector()->where("user_id", 0);
        }
    }



    /**
     * add group (model) condition to the elector query
     *
     * @param string $group
     *
     * @return void
     */
    protected function electorGroup($group)
    {
        $this->elector()->where("model_name", $group);
    }



    /**
     * add user condition to the elector query
     *
     * @TODO: add support to username entries
     *
     * @param int $user_identifier
     *
     * @return void
     */
    protected function electorUser($user_identifier)
    {
        $user_id = hashid_number($user_identifier);

        $this->elector()->where("user_id", $user_id);
    }
}
