<?php

namespace Modules\Coupons\Entities\Traits;

use App\Models\CouponRulebook;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CouponCode
 *
 * @property CouponRulebook $rulebook
 */
trait CodeRulebookTrait
{
    /**
     * get the rulebook of this code
     *
     * @return BelongsTo
     */
    public function rulebook()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "CouponRulebook", "rulebook_id");
    }



    /**
     * get the model name from the rulebook relationship
     *
     * @return string
     */
    public function getModelNameAttribute()
    {
        $rulebook = $this->rulebook;

        if ($rulebook) {
            return $rulebook->model_name;
        }

        return null;
    }
}
