<?php

namespace Modules\Coupons\Entities\Traits;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class CouponRulebook
 *
 * @property int $user_id
 */
trait RulebookUserTrait
{
    /**
     * get the code of this usage record
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * get the user row
     *
     * @return User
     */
    public function getUserAttribute()
    {
        if (!$this->user_id) {
            return model("user");
        }

        return model("User", $this->user_id);
    }



    /**
     * get the `is_particular` property, regardless of what may have been saved in the deprecated database field
     *
     * @return bool
     */
    public function getIsParticularAttribute()
    {
        return boolval($this->user_id);
    }



    /**
     * get the `username` property, according to the `user_id` in the database
     *
     * @return string
     */
    public function getUsernameAttribute()
    {
        if (!$this->user_id) {
            return null;
        }

        return $this->user->username;
    }

}
