<?php

namespace Modules\Coupons\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Coupons\Services\InvoiceObject;

trait CodeQueryTrait
{
    /**
     * get the usages of this code
     *
     * @return HasMany
     */
    public function queries()
    {
        return $this->hasMany(MODELS_NAMESPACE . "CouponQuery", "code_id");
    }



    /**
     * log a query
     *
     * @param InvoiceObject $invoice
     *
     * @return bool
     */
    public function logQuery(InvoiceObject $invoice)
    {
        model("coupon-query")->batchSave([
             "code_id" => $this->id,
             "invoice" => $invoice->serialize(),
        ]);
    }



    /**
     * get the total queries ran on the current code
     *
     * @return int
     */
    public function getTotalQueriesAttribute()
    {
        return $this->queries()->count();
    }

}
