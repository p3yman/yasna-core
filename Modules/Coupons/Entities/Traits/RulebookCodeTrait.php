<?php

namespace Modules\Coupons\Entities\Traits;

use App\Models\CouponCode;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait RulebookCodeTrait
{
    /**
     * get meta fields related to the codes
     *
     * @return array
     */
    public function codesMetaFields()
    {
        return [
             'code_usage_limit',
        ];
    }



    /**
     * get the codes of this rulebook
     *
     * @return HasMany
     */
    public function codes()
    {
        return $this->hasMany(MODELS_NAMESPACE . "CouponCode", "rulebook_id");
    }



    /**
     * get the codes of this rulebook, grouped by the batch numbers
     *
     * @return HasMany
     */
    public function batches()
    {
        return $this->codes()->groupBy("batch");
    }



    /**
     * get the total number of defined codes
     *
     * @return int
     */
    public function getTotalCodes()
    {
        return $this->codes()->count();
    }



    /**
     * get the total number of unused codes
     *
     * @return int
     */
    public function getTotalUnusedCodes()
    {
        return $this->codes()->where('used_count', 0)->count();
    }



    /**
     * get the total number of used (at least once) codes
     *
     * @return int
     */
    public function getTotalUsedCodes()
    {
        return $this->codes()->where('used_count', '>', 0)->count();
    }



    /**
     * get a randomly generated slug, making sure it have never been used in the database
     *
     * @param string $prefix
     * @param int    $pattern_type
     * @param int    $pattern_length
     * @param string $postfix
     * @param int    $batch
     *
     * @return CouponCode
     */
    public function generateCode($prefix, $pattern_type = 1, $pattern_length = 5, $postfix = null, $batch = 0)
    {
        $slug = couponCode()::getRandomUniqueSlug($prefix, $pattern_type, $pattern_length, $postfix);

        return couponCode()->batchSave([
             "rulebook_id" => $this->id,
             "slug"        => $slug,
             "batch"       => $batch ?: $this->getLastBatchNumber() + 1,
        ]);
    }



    /**
     * get a randomly generated slug, making sure it have never been used in the database
     *
     * @param int    $quantity
     * @param string $prefix
     * @param int    $pattern_type
     * @param int    $pattern_length
     * @param string $postfix
     *
     * @return int
     */
    public function generateCodes($quantity, $prefix, $pattern_type = 1, $pattern_length = 5, $postfix = null)
    {
        $total = 0;
        $batch = $this->getLastBatchNumber() + 1;

        for ($i = 1; $i <= $quantity; $i++) {
            $inserted = $this->generateCode($prefix, $pattern_type, $pattern_length, $postfix, $batch);
            if ($inserted->exists) {
                $total++;
            }
        }

        return $total;
    }



    /**
     * get the last used batch number
     *
     * @return int
     */
    protected function getLastBatchNumber()
    {
        $first = $this->codes()->orderBy("batch", 'desc')->first();

        if (!$first) {
            return 0;
        }

        return $first->batch;
    }

}
