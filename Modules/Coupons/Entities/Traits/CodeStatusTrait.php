<?php

namespace Modules\Coupons\Entities\Traits;

use Modules\Yasna\Services\ModelTraits\YasnaStatusTrait;

/**
 * Class CouponCode
 *
 * @property int $usage_limit
 */
trait CodeStatusTrait
{
    use YasnaStatusTrait;



    /**
     * get status text
     *
     * @return string
     */
    public function getStatus()
    {
        if ($this->isTrashed()) {
            return "deleted";
        }

        if ($this->usage_limit and $this->usage_limit > $this->used_count) {
            return "used";
        }

        return "usable";
    }



    /**
     * get status text
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->getStatus();
    }

}
