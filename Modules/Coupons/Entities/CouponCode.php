<?php

namespace Modules\Coupons\Entities;

use Modules\Coupons\Entities\Traits\CodeBatchTrait;
use Modules\Coupons\Entities\Traits\CodeElectorTrait;
use Modules\Coupons\Entities\Traits\CodeQueryTrait;
use Modules\Coupons\Entities\Traits\CodeRulebookTrait;
use Modules\Coupons\Entities\Traits\CodeSlugTrait;
use Modules\Coupons\Entities\Traits\CodeStatusTrait;
use Modules\Coupons\Entities\Traits\CodeUsageTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponCode extends YasnaModel
{
    use SoftDeletes;

    use CodeRulebookTrait;
    use CodeSlugTrait;
    use CodeBatchTrait;
    use CodeUsageTrait;
    use CodeQueryTrait;
    use CodeElectorTrait;
    use CodeStatusTrait;

    const SLUG_TYPE_NUMERIC      = 1;
    const SLUG_TYPE_ALPHABETIC   = 2;
    const SLUG_TYPE_ALPHANUMERIC = 3;
}
