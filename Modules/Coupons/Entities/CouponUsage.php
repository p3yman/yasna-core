<?php

namespace Modules\Coupons\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponUsage extends YasnaModel
{
    use SoftDeletes;



    /**
     * get the user of this usage record
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * get the user row
     *
     * @return User
     */
    public function getUserAttribute()
    {
        return model("User", $this->user_id);
    }



    /**
     * get the code of this usage record
     *
     * @return BelongsTo
     */
    public function code()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "CouponCode", "code_id");
    }



    /**
     * get total discount applied on the rows with the given ids
     *
     * @param array $ids
     *
     * @return float
     */
    public static function getTotalDiscount($ids)
    {
        return static::whereIn('code_id', $ids)->sum('applied_discount');
    }

}
