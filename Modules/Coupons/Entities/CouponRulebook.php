<?php

namespace Modules\Coupons\Entities;

use Modules\Coupons\Entities\Traits\RulebookCodeTrait;
use Modules\Coupons\Entities\Traits\RulebookDescriptionTrait;
use Modules\Coupons\Entities\Traits\RulebookElectorTrait;
use Modules\Coupons\Entities\Traits\RulebookUserTrait;
use Modules\Yasna\Services\ModelTraits\YasnaRuleParserTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponRulebook extends YasnaModel
{
    use SoftDeletes;

    use YasnaRuleParserTrait;
    use RulebookCodeTrait;
    use RulebookElectorTrait;
    use RulebookDescriptionTrait;
    use RulebookUserTrait;


    /**
     * get the edit link of the model
     *
     * @return string
     */
    public function getEditLinkAttribute()
    {
        return route("coupons-edit-rulebook", [
             "group"  => $this->model_name,
             "hashid" => $this->hashid,
        ]);
    }



    /**
     * get the code browser link of the model
     *
     * @return string
     */
    public function getCodesLinkAttribute()
    {
        return route("coupons-codes-browser", [
             "hashid" => $this->hashid,
        ]);

    }



    /**
     * calculate applicable discount, for the given $amount multiplied by the custom given $count
     *
     * @param float|int $amount
     * @param int       $count
     *
     * @return float
     */
    public function calculateApplicableAmount($amount, $count = 1)
    {
        if ($this->applicable_percent) {
            $amount = $amount * 100 / $this->applicable_percent;
        }

        if ($this->applicable_amount) {
            $amount = min($this->applicable_amount, $amount);
        }

        return $amount * $count;
    }

}
