<?php
return [

     "title"              => "Title",
     "discount_amount"    => "Discount Amount",
     "rules"              => "Rules",
     "new_rule"           => "New Rule",
     "no_rules_selected"  => "No Selected Rule! Choose rules from opposite list.",
     "serializing_error"  => "Error in Data Processing! The problem might fix by refreshing the page.",
     "no_rules_found"     => "No Rules Found!",
     "save-rule-new"      => "Add New Rule",
     "save-rule-existing" => "Update Rule",
     "delete-warning"     => "Removing this rule will affect discount codes that have not yet been used.",
     "percent_sign"       => "%",

     "usage_limit"           => "Codes Usage Limit",
     "usage_limit_one"       => "Just Once",
     "usage_limit_one_hint"  => "Each discount code can be used only one time.",
     "usage_limit_count"     => "More Than Once",
     "usage_limit_none"      => "Unlimited",
     "usage_limit_none_hint" => "Discount codes can be used without limitation, over and over again.",
     "save_warning"          => "Your changes will be discarded without saving the scenario.",

     "save"   => "Save",
     "copy"   => "Copy",
     "delete" => "Remove",
     "back"   => "Back",
     "add"    => "Add",
     "cancel" => "Cancel",
];
