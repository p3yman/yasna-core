<?php
return [
     "dynamic" => [
          "coupons"         => "Discount Codes",
          'rulebook-browse' => 'Browse Scenario',
          'rulebook-create' => 'Create Scenario',
          'rulebook-edit'   => 'Edit Scenario',
          'rulebook-delete' => 'Delete Scenario',
          'rulebook-report' => 'Report Scenarios',
          'code-browse'     => 'Browse Codes',
          'code-create'     => 'Create Discount Code',
          'code-edit'       => 'Edit Discount Code',
          'code-delete'     => 'Delete Discount Code',
          'code-report'     => 'Report Codes',
     ],
];
