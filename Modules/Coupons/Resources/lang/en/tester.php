<?php
return [
     "title"               => "Simple Test",
     "total_amount"        => "Total Invoice Amount",
     "effective_date"      => "Invoice Date",
     "run"                 => "Run Tester",
     "run_with_details"    => "Run Test with Details",
     "applicable_discount" => "Applicable Discount:",
     "username_hint"       => "You can enter a username to be applied in the calculation",
];
