<?php
return [
     "attributes" => [
          "amount"          => "Amount",
          "percent"         => "Percent",
          "credit"          => "Credit",
          "max"             => "Maximum",
          "choose_pattern"  => "Choose Pattern",
          "pattern"         => "Pattern",
     ],
];
