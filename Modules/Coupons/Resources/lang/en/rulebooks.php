<?php
return [
     "title"      => "Discount Scenario",
     "new"        => "New Scenario",
     "edit"       => "Edit Scenario",
     "sample"     => "Sample Scenario",
     "general"    => "Group Discounts",
     "particular" => "Individual Discounts",
     "bin"        => "Bin",
     "search"     => "Search",
     "details"    => "Details",
     "delete"     => "Delete Scenario",
     "delete-all" => "Remove Scenario and all unused codes.",

     "description_percent" => ":n Percent",
     "description_amount"  => "Maximum :n",

     "duplicate" => "Scenario Duplication",

     "user"             => "User",
     "for_general"      => "For General Usage",
     "for_particular"   => "For Individual Usage",
     "for_general_hint" => "Anyone within the discount terms can use this discount scenario.",
     "user_not_found"   => "User not Found",
     "username"         => "Username",
];
