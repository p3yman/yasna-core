<?php
return [
     "dynamic" => [
          "coupons"         => "کدهای تخفیف",
          'rulebook-browse' => 'نمایش سناریوها',
          'rulebook-create' => 'ایجاد سناریو',
          'rulebook-edit'   => 'ویرایش سناریو',
          'rulebook-delete' => 'حذف سناریو',
          'rulebook-report' => 'گزارش‌گیری از سناریوها',
          'code-browse'     => 'مشاهده کدها',
          'code-create'     => 'ایجاد کد تخفیف',
          'code-edit'       => 'ویرایش کد تخفیف',
          'code-delete'     => 'حذف کد تخفیف',
          'code-report'     => 'گزارش‌گیری از کدها',
     ],
];
