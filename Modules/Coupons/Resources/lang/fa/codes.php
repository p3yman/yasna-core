<?php
return [
     "title"               => "کدهای تخفیف",
     "single-title"        => "کد",
     "batch"               => "گروه",
     "batch-title"         => "گروه :n",
     "all-together"        => "همه با هم",
     "create"              => "ساخت کد(های) تازه",
     "details"             => "جزئیات کد تخفیف",
     "browse"              => "مشاهده کدها",
     "browse_all"          => "مشاهده همه کدها",
     "report"              => "گزارش‌گیری",
     "download"            => "دانلود",
     "download_all"        => "دانلود همه کدها",
     "download-excel"      => "دریافت فایل اکسل",
     "stats"               => "آمار کدها",
     "empty-list"          => "فعلاً کدی وجود ندارد. اولین دسته را شما بسازید.",
     "pattern"             => "الگوی خودکار",
     "numeric"             => "الگوی عددی",
     "alphabetic"          => "الگوی الفبایی",
     "alphanumeric"        => "الگوی مخلوط",
     "form_note"           => "کدها به انگلیسی ساخته می‌شوند و به کوچکی و بزرگی حروف حساس نیستند. پیشوند و پسوند اختیاری هستند، اما بهتر است با کاراکترهای انگلیسی وارد شوند.",
     "prefix"              => "پیشوند",
     "postfix"             => "پسوند",
     "length"              => "تعداد کاراکترهای الگو",
     "length_hint"         => "بهترین انتخاب برای تعداد کاراکترها، بین پنج تا ده کاراکتر است.",
     "quantity"            => "تعداد کد تخفیف",
     "code_count_hint"     => "چند کد تخفیف برای این قانون لازم دارید؟ تعداد آن‌ها را وارد کنید.",
     "sample_viewer"       => ".نمونه‌ای از کدی که ممکن است تولید شود را اینجا ببینید",
     "sample_viewer_error" => "تولید کد با این پارامترهای ورودی امکان‌پذیر نیست.",
     "n_codes_generated"   => ":n کد تخفیف با الگوی تعیین‌شده تولید شد.",
     "percent_sign"        => "٪",
     "total_discounted"    => "جمع تخفیف داده شده",
     "total_codes"         => "کل کدهای تخفیف",
     "total_used"          => "استفاده‌شده",
     "total_unused"        => "باقی‌مانده",
     "delete"              => "حذف کد",
     "restore"             => "بازیافت کد",
     "delete_panel"        => "پنل حذف و بازیافت",
     "unlimited"           => "نامحدود",
     "infinity_sign"       => "∞",

     "track"           => "ردیابی",
     "code_track"      => "ردیابی کد تخفیف",
     "code_not_found"  => "چنین کد تخفیفی تعریف نشده است.",
     "overview"        => "خلاصه وضعیت",
     "creation_time"   => "زمان ایجاد",
     "usages"          => "موارد استفاده",
     "queries"         => "موارد استعلام",
     "unused_yet"      => "هنوز از این کد تخفیف استفاده‌ای نشده است.",
     "no_queries_yet"  => "هنوز استعلامی صورت نگرفته است.",
     "user_info"       => "استفاده‌کننده",
     "applied_amount"  => "میزان تخفیف",
     "inquiry_result"  => "تخفیف اعلام‌شده",
     "inquiry_invoice" => "مبلغ فاکتور",
     "beneficiary"     => "استفاده‌کنندهٔ مجاز",

     "filter"        => "فیلتر",
     "filter-unused" => "فقط کدهایی که تاکنون استفاده نشده‌اند",
     "filter-usable" => "فقط کدهای قابل استفاده",
     "filter-all"    => "همه کدهای تخفیف",

];
