<?php
return [
     "deleted" => "حذف‌شده",
     "used"    => "استفاده‌شده",
     "usable"  => "قابل استفاده",
];
