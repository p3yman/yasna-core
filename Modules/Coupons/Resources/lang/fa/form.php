<?php
return [
     "attributes" => [
          "amount"          => "میزان",
          "percent"         => "درصدی",
          "credit"          => "اعتباری",
          "max"             => "حداکثر",
          "choose_pattern"  => "انتخاب الگو",
          "pattern"         => "الگو",
     ],
];
