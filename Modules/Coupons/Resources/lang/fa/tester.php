<?php
return [
     "title"               => "آزمایش ساده",
     "total_amount"        => "مبلغ کل فاکتور",
     "effective_date"      => "تاریخ فاکتور",
     "run"                 => "انجام آزمایش",
     "run_with_details"    => "انجام آزمایش با جزئیات",
     "applicable_discount" => "تخفیف قابل ارائه:",
     "username_hint"       => "می‌توانید شناسهٔ کاربری یک شخص را وارد کنید تا در محاسبات منظور شود",
];
