<div class="box-placeholder">
    <div class="text-center p20 text-gray">
        @if(!isset($no_text) or !$no_text)
            {{ trans('coupons::codes.empty-list') }}
        @endif

        <p class="m10">
            {!!
                widget('button')
                    ->label('tr:coupons::codes.create')
                    ->class('btn-default btn-taha')
                    ->icon('plus-circle')
                    ->onClick("masterModal('".coupon()->getCodeGeneratorLink($model->hashid)."')")
            !!}
        </p>


    </div>
</div>
