@component('coupons::layout.panel',[
	"heading" => $model->title ,
])

@foreach($model->getRules() as $rule)
    @include("manage::widgets.grid-badge" , [
        'text' => $rule['_class']::getTitle(),
    ])

@endforeach

<div class="pull-right">
    {!!
    widget("button")
         ->label("tr:coupons::rulebooks.edit")
         ->condition(coupon()->canEditRulebook())
         ->target($model->edit_link)
    !!}

</div>

@endcomponent
