{{--
|--------------------------------------------------------------------------
| Create Codes
|--------------------------------------------------------------------------
|
--}}

{!!
widget("button")
    ->condition(coupon()->canCreateCodes())
    ->shape('primary')
    ->icon('plus-circle')
    ->label('tr:coupons::codes.create')
    ->class('btn-default btn-taha')
    ->icon('plus-circle')
    ->onClick("masterModal('".coupon()->getCodeGeneratorLink($model->hashid)."')")
!!}

{{--
|--------------------------------------------------------------------------
| Code Tracker Tool
|--------------------------------------------------------------------------
| 
--}}
{!!
widget("button")
     ->condition(coupon()->canReportCodes())
	->shape("info")
	->icon("search")
	->label("tr:coupons::codes.code_track")
	->class("btn-taha")
	->onClick(coupon()->getTrackerLink())
!!}

{{--
|--------------------------------------------------------------------------
| List Tool
|--------------------------------------------------------------------------
|
--}}
{!!
 widget('button')
	->label('tr:coupons::codes.browse_all')
	->shape('primary btn-outline')
	->icon('eye')
	->condition($total_codes)
	->target(coupon()->getCodesListLink($model->hashid))
!!}

{{--
|--------------------------------------------------------------------------
| Download Tool
|--------------------------------------------------------------------------
|
--}}
{!!
widget("button")
     ->condition($total_codes and coupon()->canExportCodes())
	->label("tr:coupons::codes.download_all")
	->shape("info btn-outline")
	->icon("download")
	->target(coupon()->getCodesDownloaderLink($model->hashid))
!!}
