@extends('manage::layouts.template')

@section('content')
    @include("coupons::codes.browse.title")

    <div class="mv10">
        @include("coupons::codes.browse.buttons")
    </div>

    <div id="divCouponCodes" data-src="{{ route("coupons-code-refresh" , ['hashid' => $model->hashid]) }}">
        @include("coupons::codes.browse.codes")
    </div>
@endsection
