{{--@component('coupons::layout.panel',[--}}
	{{--"heading" => trans('coupons::codes.title') ,--}}
{{--])--}}

@if($total_codes)
    @include("coupons::codes.browse.loop")
@else
    @include("coupons::codes.browse.placeholder")
@endif


{{--@endcomponent--}}
