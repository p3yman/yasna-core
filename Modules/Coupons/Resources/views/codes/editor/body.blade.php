@include("coupons::codes.editor.pattern")

{!!
widget("hidden")
	->name("quantity")
	->value(1)
!!}

{!!
widget("note")
	->label("tr:coupons::codes.form_note")
!!}

{!!
     widget('input')
     ->label('tr:coupons::codes.length')
     ->name('length')
     ->help("tr:coupons::codes.length_hint")
     ->id("txtCharCount")
     ->onChange("couponSampleGenerator()")
     ->value(5)
     ->inForm()
!!}

{!!
     widget('input')
     ->label('tr:coupons::codes.quantity')
     ->name('quantity')
     ->required()
     ->disabledIf($is_particular)
     ->help("tr:coupons::codes.code_count_hint")
     ->inForm()
!!}
