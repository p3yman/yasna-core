<div class="form-group" id="js_coupon_format">
    <div class="col-sm-12" >
        <div class="row">

            <div class="col-sm-4">
                {!!
                     widget('input')
                     ->placeholder('tr:coupons::codes.postfix')
                     ->id('txtPostfix')
                     ->onChange("couponSampleGenerator()")
                     ->name("postfix")
                 !!}
            </div>

            <div class="col-sm-4">
                {!!
                     widget('combo')
                     ->options($patterns)
                     ->id('txtPattern')
                     ->onChange("couponSampleGenerator()")
                     ->name("pattern")
                 !!}
            </div>

            <div class="col-sm-4">
                {!!
                     widget('input')
                     ->placeholder('tr:coupons::codes.prefix')
                     ->id('txtPrefix')
                     ->onChange("couponSampleGenerator()")
                     ->name("prefix")
                 !!}
            </div>
        </div>
    </div>
</div>

<div class="mb">
    <div class="alert alert-info f16 text-center ltr">
        <span id="spnCodeSample">{{ trans("coupons::codes.sample_viewer") }}</span>
        <span id="spnCodeSampleError" class="noDisplay">{{ trans("coupons::codes.sample_viewer_error") }}</span>
        {{--<span data-input-id="coupon_prefix">PREFIX</span>-<span data-input-id="coupon_pattern">PATTERN</span>-<span data-input-id="coupon_postfix">POSTFIX</span>--}}
    </div>
</div>
