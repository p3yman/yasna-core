{!!
	widget('modal')
	->label('tr:coupons::codes.create')
	->target("name:coupons-codes-generate")
!!}

{!!
widget("hidden")
	->name("hashid")
	->value($hashid)
!!}

<div class="modal-body">
    @include("coupons::codes.editor.body")
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" , [
        "save_label" => trans("coupons::codes.create"),
        "save_shape" => "primary",
    ])
</div>
