{{-- @arg CouponCode $batch --}}

<div class="list-group">



    <div class="list-group-item b0">

        <span class="label label-primary" style="min-width: 55px;display: inline-block;padding: 0.6em;">
            {{ pd($batch->getSistersCount()) }}
        </span>

        <span class="mh f16">
            {{ trans('coupons::codes.total_codes') }}
        </span>
    </div>


    <div class="list-group-item b0">

        <span class="label label-danger" style="min-width: 55px;display: inline-block;padding: 0.6em;">
            {{ pd($batch->getUsedSistersCount()) }}
        </span>
        <span class="mh f16">
            {{ trans('coupons::codes.total_used') }}
        </span>
    </div>



    <div class="list-group-item b0">
        <span class="label label-success" style="min-width: 55px;display: inline-block;padding: 0.6em;">
            {{ pd($batch->getUnusedSistersCount()) }}
        </span>
        <span class="mh f16">
            {{ trans('coupons::codes.total_unused') }}
        </span>

    </div>

</div>
