{{-- @arg CouponCode $batch --}}

<div class="panel panel-default">
    <div class="panel-heading text-bold">
        {{ $batch->batch_title }}
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-9">
                @include("coupons::codes.item.group-details")
            </div>
            <div class="col-md-3">
                @include("coupons::codes.item.group-buttons")
                {{--@include('coupons::sections.coupons.action-buttons')--}} {{-- TODO: move it here and make it dynamic --}}
            </div>
        </div>
    </div>
</div>

