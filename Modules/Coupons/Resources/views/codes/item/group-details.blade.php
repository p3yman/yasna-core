{{-- @arg CouponCode $batch --}}

<div class="row">
    <div class="col-sm-5 text-center">
        @include("coupons::codes.item.group-details-chart")
    </div>
    <div class="col-sm-7">
        @include("coupons::codes.item.group-details-stats")
    </div>
</div>
