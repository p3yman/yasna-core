{{-- @arg CouponCode $batch --}}

@php
    $bar_color = "#27c24c";
    $percent = $batch->getSistersUsagePercent();
    if ($percent > 33.33){
         $bar_color = "#ff902b";
    }
    if ($percent > 66.66){
         $bar_color = "#f05050";
    }
@endphp

@include('manage::widgets.charts.pie.easypiechart',[
     "id" => "piechart_$batch->hashid" ,
     "percent" => $percent ,
     "barColor" => $bar_color ,
     "trackColor" => "#edf1f2" ,
     "text" => pd($percent).trans('coupons::codes.percent_sign') ,
])

<div class="mv">
    <p class="m0">
        <small>
            {{ trans('coupons::codes.total_discounted') }}
        </small>
    </p>
    <p class="m0 lead">
        {{ ad(number_format($batch->getSistersTotalDiscounted())) }}
    </p>
</div>
