{!!
 widget('button')
	->label('tr:coupons::codes.browse')
	->shape('primary btn-outline')
	->class('btn-block mb')
	->icon('eye')
	->target(coupon()->getCodesListLink($model->hashid, $batch->batch))
!!}

{!!
 widget('button')
	->label('tr:coupons::codes.report')
	->shape('info btn-outline')
	->class('btn-block')
	->icon('file-text-o')
	->condition(false)
!!}

{!!
widget("button")
     ->condition(coupon()->canExportCodes())
	->label("tr:coupons::codes.download")
	->shape("info btn-outline")
	->class("btn-block")
	->icon("download")
	->target(coupon()->getCodesDownloaderLink($model->hashid, $batch->batch))
!!}
