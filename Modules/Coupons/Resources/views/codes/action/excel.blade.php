<table>
    <tr>
        <td>Code</td>
        <td>Usage Count</td>
    </tr>
    @foreach(coupon()->getExcelCodes() as $code)
        <tr>
            <td>{{ $code->slug }}</td>
            <td>{{ $code->used_count }}</td>
        </tr>
    @endforeach
</table>
