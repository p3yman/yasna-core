{!!
widget("modal")
	->label("tr:coupons::codes.title")
	->target("name:coupons-codes-download-action")
!!}

<div class="modal-body" style="min-height: 300px">

    {!!
    widget("hidden")
         ->name("hashid")
         ->value($model->hashid)
    !!}

    {!!
    widget("combo")
         ->name("batch")
         ->value($batch)
         ->options($model->batches()->get())
         ->captionField('batch_title')
         ->valueField("batch")
         ->blankCaption("tr:coupons::codes.all-together")
         ->blankValue(0)
         ->label("tr:coupons::codes.batch")
         ->inForm()
    !!}

    {!!
    widget("combo")
    	   ->name("filter")
    	   ->value("all")
    	   ->label("tr:coupons::codes.filter")
    	   ->inForm()
    	   ->options([
    	       ["unused", trans("coupons::codes.filter-unused")],
    	       ["usable", trans("coupons::codes.filter-usable")],
    	       ["all", trans("coupons::codes.filter-all")],
    	   ])
    !!}

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" , [
        "save_label" => trans("coupons::codes.download-excel"),
        "save_shape" => "primary",
    ])
</div>
