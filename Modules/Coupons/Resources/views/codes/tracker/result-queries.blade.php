<div class="panel-heading">
    {{ trans('coupons::codes.queries') }}
</div>

@if($model->total_queries)
    <div class="panel-body" data-height="300" data-scrollable="">
        @include("coupons::codes.tracker.result-queries-browse")
    </div>
@else
    @include("coupons::codes.tracker.result-placeholder",[
        "text" => trans("coupons::codes.no_queries_yet"),
    ])
@endif
