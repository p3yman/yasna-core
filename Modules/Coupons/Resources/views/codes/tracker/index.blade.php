{{-- Called from CodesTrackerController@index --}}

{!!
widget("modal")
	->target("name:coupons-code-tracker-submit")
	->label("tr:coupons::codes.code_track")
!!}

<div class="modal-body">
    @include("coupons::codes.tracker.body")
</div>

<div class="modal-footer">
    {!! widget('feed') !!}
</div>
