<tr>
    <td class="text-center">
        <i class="fa fa-user fa-2x text-info"></i>
    </td>

    <td>
        <div class="text-primary">
            {{ $usage->user->full_name }}
        </div>

        <div class="text-muted">
            {{ ad(echoDate($usage->created_at)) }}
        </div>
    </td>

    <td>
        {{ ad(number_format($usage->applied_discount)) }}
    </td>
</tr>
