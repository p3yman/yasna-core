@include("coupons::codes.tracker.result-overview-heading")

@if($model->usage_limit)
    @include("coupons::codes.tracker.result-overview-diagrams")
@endif

@include("coupons::codes.tracker.result-overview-table")
