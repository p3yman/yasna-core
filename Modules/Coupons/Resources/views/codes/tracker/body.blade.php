{{-- Called from index.blade --}}

{!!
widget("input")
	->name("code")
	->label("tr:coupons::codes.single-title")
	->required()
	->inForm()
!!}

@include("manage::forms.buttons-for-modal" , [
    "save_label" => trans("coupons::codes.code_track"),
    "save_shape" => "primary",
    "save_value" => "track",
    "no_feed" => true,
])
