<div class="panel-body">
    <div class="table-grid table-grid-align-middle">
        <div class="row text-center">


            <div class="col-sm-6">
                @include("coupons::codes.tracker.result-overview-diagrams-used")
            </div>

            <div class="col-sm-6">
                @include("coupons::codes.tracker.result-overview-diagrams-unused")
            </div>


        </div>
    </div>
</div>
