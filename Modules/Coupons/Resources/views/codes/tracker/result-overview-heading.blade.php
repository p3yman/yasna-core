<div class="panel-heading">

    <div class="pull-right">
        <div class="label label-{{ $model->status_color }}">
            {{ $model->status_text }}
        </div>
    </div>

    <h4 class="m0 mb5">
        {{ trans('coupons::codes.overview') }}
    </h4>

    <small class="text-muted">
        {{ trans('coupons::codes.creation_time').': '. ad(echoDate($model->created_at)) }}
    </small>

</div>
