<div class="panel-heading">
    {{ trans('coupons::codes.usages') }}
</div>

@if($model->used_count)
    <div class="panel-body" data-height="300" data-scrollable="">
        @include("coupons::codes.tracker.result-usages-browse")
    </div>
@else
    @include("coupons::codes.tracker.result-placeholder",[
        "text" => trans("coupons::codes.unused_yet"),
    ])
@endif
