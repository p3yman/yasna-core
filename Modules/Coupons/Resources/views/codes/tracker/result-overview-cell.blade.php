<div class="row row-table row-flush">
    <div class="col-xs-4 text-center text-{{$color}}">
        <em class="fa fa-{{$icon}} fa-2x"></em>
    </div>
    <div class="col-xs-8">
        <div class="panel-body text-center">
            <h4 class="mt0">
                @if(is_numeric($number))
                    {{ ad(number_format($number)) }}
                @else
                    {{$number}}
                @endif
            </h4>
            <p class="mb0 text-muted">
                {{ trans("coupons::codes.$caption") }}
            </p>
        </div>
    </div>
</div>
