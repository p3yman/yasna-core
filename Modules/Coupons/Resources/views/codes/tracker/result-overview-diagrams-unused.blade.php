@include('manage::widgets.charts.pie.easypiechart',[
    'id'=> 'unusedCoupons',
    'percent' => $model->remained_percent,
    'barColor' => '#4cd137',
    'trackColor' => '#e4eaec',
    'lineWidth' => '10',
    "text" => ad($model->remained_percent).trans('coupons::codes.percent_sign') ,
])

<h4>
    {{ trans('coupons::codes.total_unused') }}
</h4>
