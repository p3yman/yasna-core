<table class="table">
    <tbody>
    <tr>
        <td colspan="2">
            <div class="row row-table row-flush">

                <div class="col-xs-6 br bb">
                    @include("coupons::codes.tracker.result-overview-cell" , [
                        "color" => "pink",
                        "icon" => "gift",
                        "number" => $model->rulebook->description,
                        "caption" => "applied_amount",
                    ])
                </div>

                <div class="col-xs-6 bb">
                    @include("coupons::codes.tracker.result-overview-cell" , [
                        "color" => "blue",
                        "icon" => "user",
                        "number" => $model->rulebook->user_id? $model->rulebook->user->full_name : trans("coupons::codes.infinity_sign"),
                        "caption" => "beneficiary",
                    ])
                </div>

            </div>
            <div class="row row-table row-flush">

                <div class="col-xs-6 br bb">
                    @include("coupons::codes.tracker.result-overview-cell" , [
                        "color" => "info",
                        "icon" => "ticket",
                        "number" => $model->usage_limit? $model->usage_limit : trans("coupons::codes.infinity_sign"),
                        "caption" => "quantity",
                    ])
                </div>

                <div class="col-xs-6 bb">
                    @include("coupons::codes.tracker.result-overview-cell" , [
                        "color" => "danger",
                        "icon" => "dollar",
                        "number" => $model->total_discounted,
                        "caption" => "total_discounted",
                    ])
                </div>

            </div>
            <div class="row row-table row-flush">

                <div class="col-xs-6 br">
                    @include("coupons::codes.tracker.result-overview-cell" , [
                        "color" => "orange",
                        "icon" => "hourglass-end",
                        "number" => $model->used_count,
                        "caption" => "total_used",
                    ])
                </div>

                <div class="col-xs-6">
                    @include("coupons::codes.tracker.result-overview-cell" , [
                        "color" => "success",
                        "icon" => "hourglass-start",
                        "number" => $model->usage_limit? $model->remained_count: trans("coupons::codes.infinity_sign"),
                        "caption" => "total_unused",
                    ])
                </div>

            </div>
        </td>
    </tr>
    </tbody>
</table>
