<tr>
    <td class="text-center">
        {{ ad($loop->iteration) }}
    </td>

    <td>
        <div class="text-primary">
            {{ $query->invoice->getUser()->full_name }}
        </div>

        <div class="text-muted">
            {{ ad(echoDate($query->created_at)) }}
        </div>
    </td>

    <td>
        {{ ad(number_format($query->invoice->getTotalAmount()))  }}
    </td>

    <td>
        {{ ad(number_format($query->invoice->getTotalDiscount()))  }}
    </td>
</tr>
