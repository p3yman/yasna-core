{{--
|--------------------------------------------------------------------------
| Delete / Undelete Command
|--------------------------------------------------------------------------
|
--}}
{!!
widget("button")
	->type('submit')
	->label("tr:coupons::codes.delete")
	->shape('danger')
	->condition($model->isNotTrashed() and coupon()->canDeleteCodes())
	->value("delete")
!!}

{!!
widget("button")
	->type('submit')
	->label("tr:coupons::codes.restore")
	->shape('warning')
	->condition($model->isTrashed() and coupon()->canRestoreCodes())
	->value("restore")
!!}

{{--
|--------------------------------------------------------------------------
| Codes Manager
|--------------------------------------------------------------------------
|
--}}
{!!
widget("button")
     ->label("tr:coupons::codes.browse_all")
     ->target($model->rulebook->codes_link)
     ->condition(coupon()->canBrowseCodes())
     ->newWindow()
!!}

{{--
|--------------------------------------------------------------------------
| Rulebook Editor
|--------------------------------------------------------------------------
|
--}}

{!!
widget("link")
     ->label("tr:coupons::rulebooks.edit")
     ->class("btn btn-default btn-taha")
     ->condition(coupon()->canEditRulebook())
     ->target($model->rulebook->edit_link)
     ->newWindow()
!!}

