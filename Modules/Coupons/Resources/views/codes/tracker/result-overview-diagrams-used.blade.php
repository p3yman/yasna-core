@include('manage::widgets.charts.pie.easypiechart',[
    'id'=> 'usedCoupons',
    'percent' => $model->used_percent,
    'barColor' => '#e74c3c',
    'trackColor' => '#e4eaec',
    'lineWidth' => '10',
    "text" => ad($model->used_percent).trans('coupons::codes.percent_sign') ,
])

<h4>
    {{ trans('coupons::codes.total_used') }}
</h4>
