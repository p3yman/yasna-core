<table class="table table-bordered">

    <thead>
    <tr>
        <th width="50px"></th>
        <th>
            {{ trans('coupons::codes.user_info') }}
        </th>
        <th>
            {{ trans('coupons::codes.inquiry_invoice') }}
        </th>
        <th>
            {{ trans('coupons::codes.inquiry_result') }}
        </th>
    </tr>
    </thead>

    @foreach($model->queries as $query)
        @include("coupons::codes.tracker.result-queries-row")
    @endforeach
    <tbody>
    </tbody>
</table>
