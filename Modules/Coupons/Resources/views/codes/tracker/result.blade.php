{{--
  Called from CodesTrackerController@submit
  @var CouponCode $model
--}}


<div class="panel panel-default">
    @include("coupons::codes.tracker.result-overview")
</div>

<div class="panel panel-default">
    @include("coupons::codes.tracker.result-usages")
</div>

<div class="panel panel-default">
    @include("coupons::codes.tracker.result-queries")
</div>

<div>
    @include("coupons::codes.tracker.result-buttons")
</div>

<script>
    /*
     * Function called in bladed for the sake of async loading
     * */
    $(function ()
    {

        $('[data-scrollable]').each(function ()
        {

            var element       = $(this),
                defaultHeight = 250;

            element.slimScroll({
                height: (element.data('height') || defaultHeight)
            });

        });
    });
</script>
