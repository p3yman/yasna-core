<table class="table table-bordered">

    <thead>
    <tr>
        <th width="50px"></th>
        <th>
            {{ trans('coupons::codes.user_info') }}
        </th>
        <th>
            {{ trans('coupons::codes.applied_amount') }}
        </th>
    </tr>
    </thead>
        @foreach($model->usages as $usage)
            @include("coupons::codes.tracker.result-usages-row")
        @endforeach
    <tbody>
    </tbody>
</table>
