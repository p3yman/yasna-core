{{--
    Called from CodesController@index
    @var Eloquent $rulebook
    @var Collection $codes
    @var string $modal_title
--}}

{!!
widget("modal")
	->label(ad($modal_title))
	->target("name:coupons-codes-list-action")
!!}

<div class="modal-body">
    @include("coupons::codes.list.grid")
</div>

<div class="modal-footer">
    @include("coupons::codes.list.buttons")
</div>
