{{--
    Called from coupons::codes.list.index
    @var Eloquent $rulebook
    @var Collection $codes
    @var string $modal_title
--}}

@include("manage::widgets.grid" , [
    'headings'          => [
        trans("coupons::codes.single-title"),
        trans("coupons::codes.total_used"),
        trans("coupons::codes.total_unused"),
        '',
    ],
    "models"            => $codes,
    'row_view'          => "coupons::codes.list.row",
    'table_id'          => "tblCodes",
    'handle'            => 'counter',
    'operation_heading' => false,
])
