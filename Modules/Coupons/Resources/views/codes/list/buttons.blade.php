{!!
widget("button")
     ->condition(coupon()->canDeleteCodes() or coupon()->canRestoreCodes())
	->label("tr:coupons::codes.delete_panel")
	->shape("danger btn-outline")
	->onClick("$('.-code-delete-button').toggle()")
!!}
