{!!
widget("button")
        ->label("tr:coupons::codes.delete")
        ->shape('link')
        ->class("btn-xs -code-delete-button text-red f10")
        ->hiddenIf(!isset($reload) or !$reload)
        ->type('submit')
        ->value("delete-$model->hashid")
        ->condition($model->isNotTrashed() and coupon()->canDeleteCodes())
        ->onClick("$('#tr-$model->hashid').addClass('loading')")
!!}

{!!
widget("button")
        ->label("tr:coupons::codes.restore")
        ->shape('link')
        ->class("btn-xs -code-delete-button text-blue f10")
        ->hiddenIf(!isset($reload) or !$reload)
        ->type('submit')
        ->value("restore-$model->hashid")
        ->condition($model->isTrashed() and coupon()->canRestoreCodes())
        ->onClick("$('#tr-$model->hashid').addClass('loading')")
!!}
