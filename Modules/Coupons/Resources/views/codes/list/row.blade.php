{{--
    Called from coupons::codes.list.grid & CodesController@listRefreshRow
    @var Eloquent $model
--}}

@include('manage::widgets.grid-row-handle' , [
    "handle" => "counter",
    "refresh_url" => route("coupons-code-list-refresh", ['hashid' => $model->hashid]),
])

<td dir="ltr" align="center">
    @if($model->isTrashed())
        <del>{{ $model->slug }}</del>
    @else
        {{ $model->slug }}
    @endif
</td>

<td>
    {{ ad(number_format($model->used_count)) }}
</td>

<td>
    @if($model->getUsageLimit())
        {{ ad(number_format($model->remained_count)) }}
    @else
        {{ trans("coupons::editor.usage_limit_none") }}
    @endif
</td>

<td>
    @if(coupon()->canDeleteCodes() or coupon()->canRestoreCodes())
        @include("coupons::codes.list.row-delete")
    @endif
</td>
