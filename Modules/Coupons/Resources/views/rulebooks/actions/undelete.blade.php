@include("manage::layouts.modal-delete" , [
    "modal_title" => trans("manage::forms.button.undelete"),
    "save_label" => trans("manage::forms.button.undelete"),
    "save_shape" => "primary",
    'form_url' => route("coupons-undelete-rulebook") ,
])
