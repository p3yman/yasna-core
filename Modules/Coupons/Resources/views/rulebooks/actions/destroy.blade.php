@include("manage::layouts.modal-delete" , [
    "modal_title" => trans("manage::forms.button.hard_delete"),
    "save_label" => trans("manage::forms.button.sure_hard_delete"),
    "save_shape" => "danger",
    'form_url' => route("coupons-destroy-rulebook") ,
])
