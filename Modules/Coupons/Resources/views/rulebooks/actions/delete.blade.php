@include("manage::layouts.modal-delete" , [
    "modal_title" => trans("coupons::rulebooks.delete"),
    "save_label" => trans("coupons::rulebooks.delete-all"),
    'form_url' => route("coupons-delete-rulebook") ,
])
