{!!
widget("modal")
	->label($class::getTitle())
	->target("name:coupons-save-rule")
!!}

{!!
widget("hidden")
	->name("_rulebook")
	->id("txtSerializedRulebook")
!!}

{!!
widget("hidden")
	->name("_identifier")
	->value($identifier)
!!}

{!!
widget("hidden")
	->name("_class")
	->value($class)
!!}

<div class="modal-body">
    {!! $class::renderForm($values) !!}
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" , [
        "save_label" => trans("coupons::editor.save-rule-" . ($identifier? "existing" : "new") ),
        "save_shape" => $identifier? "primary" : "success",
    ])
</div>

<script>
    $("#txtSerializedRulebook").val( $("#txtPageSerializedModel").val() )
</script>
