{!!
widget("modal")
	->label($class::getTitle())
	->target("name:coupons-delete-rule-action")
!!}

{!!
widget("hidden")
	->name("_rulebook")
	->id("txtSerializedRulebook")
!!}

{!!
widget("hidden")
	->name("_identifier")
	->value($identifier)
!!}

{!!
widget("hidden")
	->name("_class")
	->value($class)
!!}

<div class="modal-body">
    {!!
    widget("note")
    	   ->label("tr:coupons::editor.delete-warning")
    	   ->shape('warning')
    !!}
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" , [
        "save_label" => trans("manage::forms.button.sure_delete"),
        "save_shape" => "warning",
    ])
</div>

<script>
    $("#txtSerializedRulebook").val($("#txtPageSerializedModel").val())
</script>
