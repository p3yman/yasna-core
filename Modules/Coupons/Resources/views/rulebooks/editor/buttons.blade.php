<div class="panel">
    <div class="panel-body">

        {!!
        widget("note")
        	->shape('warning')
        	->label("tr:coupons::editor.save_warning")
        	->id("divSaveWarning")
        	->hidden()
        !!}

        {!!
             widget('button')
             ->label('tr:coupons::editor.save')
             ->class('btn-success btn-taha')
             ->type('submit')
             ->value('save')
             ->icon('save')
        !!}
        {!!
             widget('button')
             ->label('tr:coupons::rulebooks.duplicate')
             ->class('btn-primary btn-taha')
             ->icon('copy')
             ->condition($model->isset() and coupon()->canCreateRulebook())
             ->target(coupon()->getRulebookDuplicatorLink($model->hashid))
             ->newWindow()
        !!}

        {!!
        widget("button")
          ->icon('flask')
        	->label("tr:coupons::tester.title")
          ->shape('info')
        	->class("btn-outline btn-taha")
        	->target("masterModal('".route("coupons-test-rule-form")."')")
        !!}

        {!! widget("feed") !!}

        <div class="pull-right">
            {!!
            widget("button")
                 ->label("tr:coupons::codes.title")
                 ->icon("ticket")
                 ->target($model->codes_link)
                 ->condition($model->exists and coupon()->canBrowseCodes())
            !!}
        </div>
    </div>
</div>

