@extends('manage::layouts.template')

@section('content')
    {!! widget('form-open')->target("name:coupons-save")->class('js') !!}
    {!! widget('hidden')->name('hashid')->value($model->hashid) !!}
    {!! widget('hidden')->name('model_name')->value($model->model_name) !!}

    @include("coupons::rulebooks.editor.title")
    @include("coupons::rulebooks.editor.type")

    <div class="row">
        <div class="col-md-6">
            @include("coupons::rulebooks.editor.usage")
        </div>
        <div class="col-md-6">
            @include("coupons::rulebooks.editor.user")
        </div>
    </div>

    @include("coupons::rulebooks.editor.rules")
    @include("coupons::rulebooks.editor.buttons")

    {!! widget("hidden")->name("_rulebook")->id("txtPageSerializedModel")->value($model->serialize()) !!}

    {!! widget('form-close') !!}
@stop
