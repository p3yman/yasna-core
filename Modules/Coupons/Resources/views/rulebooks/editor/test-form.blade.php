{!!
widget("modal")
	->target("name:coupons-test-rule")
	->label("tr:coupons::tester.title")
!!}

{!!
widget("hidden")
	->name("_rulebook")
	->id("txtSerializedRulebook")
!!}

{!!
widget("hidden")
	->name("discount_amount")
	->id("txtDiscountAmountTester")
!!}

{!!
widget("hidden")
	->name("discount_max_amount")
	->id("txtDiscountMaxAmountTester")
!!}

{!!
widget("hidden")
	->name("discount_percent")
	->id("txtDiscountPercentTester")
!!}

<div class="modal-body">
    {!!
    widget("input")
         ->label("tr:coupons::tester.total_amount")
         ->name("total_amount")
         ->required()
         ->numberFormat()
         ->value(100000)
         ->inForm()
    !!}

    {!!
    widget("persian-date-picker")
    	->label("tr:coupons::tester.effective_date")
    	->name("effective_date")
    	->required()
    	->value(now()->toDateTimeString())
    	->inForm()
    !!}

    {!!
    widget("input")
    	->label("tr:coupons::rulebooks.user")
    	->name("username")
    	->inForm()
    	->help("tr:coupons::tester.username_hint")
    !!}
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" , [
        "save_label"  => trans("coupons::tester.run"),
        "save_value"  => "run",
        "extra_label" => dev()? trans("coupons::tester.run_with_details") : null,
        "extra_shape" => "primary",
        "extra_value" => "details",
    ])
</div>

<script>
    $("#txtSerializedRulebook").val($("#txtPageSerializedModel").val());
    $("#txtDiscountAmountTester").val($("#txtDiscountAmount").val());
    $("#txtDiscountMaxAmountTester").val($("#txtDiscountMaxAmount").val());
    $("#txtDiscountPercentTester").val($("#txtDiscountPercent").val());
</script>
