@include('coupons::layout.form.radio',[
     "name" => "_discount_type" ,
     "value" => "credit" ,
     "class" => "radio_form_toggler" ,
     "toggle_view" => "credit_container" ,
     "label" => trans('coupons::form.attributes.credit') ,
     "is_checked" => !boolval($model->applicable_percent) ,
])

<div class="row p-lg" id="credit_container" style="display: none;">
    <div class="col-md-6">
        {!!
            widget('input')
                ->id("txtDiscountAmount")
                ->label('tr:coupons::form.attributes.amount')
                ->name('_discount_amount')
                ->inForm()
                ->numberFormat()
                ->value($model->applicable_amount)
         !!}
    </div>
</div>
