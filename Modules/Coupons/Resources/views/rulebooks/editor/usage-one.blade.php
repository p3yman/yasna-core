@include('coupons::layout.form.radio',[
     "name" => "_usage_limit" ,
     "value" => "one" ,
     "class" => "radio_form_toggler" ,
     "toggle_view" => "usage_limit_one_container" ,
     "label" => trans('coupons::editor.usage_limit_one') ,
     "is_checked" => $model->code_usage_limit == 1 ,
])

<div class="row p-lg" id="usage_limit_one_container" style="display: none;">
    {!!
    widget("note")
        ->label("tr:coupons::editor.usage_limit_one_hint")
        ->icon('check')
        ->class("text-info")
    !!}
</div>
