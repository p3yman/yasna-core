@include('coupons::layout.form.radio',[
     "name" => "_user_type" ,
     "value" => "general" ,
     "class" => "radio_form_toggler" ,
     "toggle_view" => "user_general_container" ,
     "label" => trans('coupons::rulebooks.for_general') ,
     "is_checked" => $model->tab != "particular" ,
])

<div class="row p-lg" id="user_general_container" style="display: none;">
    {!!
    widget("note")
        ->label("tr:coupons::rulebooks.for_general_hint")
        ->icon('check')
        ->class("text-info")
    !!}
</div>
