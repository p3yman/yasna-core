@component('coupons::layout.panel',[
	"heading" => trans('coupons::editor.discount_amount') ,
])

<div class="mb">
    @include("coupons::rulebooks.editor.type-percent")
</div>

<div class="mb">
    @include("coupons::rulebooks.editor.type-amount")
</div>
@endcomponent

