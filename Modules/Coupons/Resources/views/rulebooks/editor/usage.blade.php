@component('coupons::layout.panel',[
	"heading" => trans('coupons::editor.usage_limit') ,
	"style" => "height:300px",
])

<div class="mb">
    @include("coupons::rulebooks.editor.usage-one")
</div>

<div class="mb">
    @include("coupons::rulebooks.editor.usage-limited")
</div>

<div class="mb">
    @include("coupons::rulebooks.editor.usage-unlimited")
</div>

@endcomponent

