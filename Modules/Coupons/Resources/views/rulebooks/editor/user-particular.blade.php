@include('coupons::layout.form.radio',[
     "name" => "_user_type" ,
     "value" => "particular" ,
     "class" => "radio_form_toggler" ,
     "toggle_view" => "user_particular_container" ,
     "label" => trans('coupons::rulebooks.for_particular') ,
     "is_checked" => $model->tab == "particular" ,
])

<div class="row p-lg" id="user_particular_container" style="display: none;">
    {!!
    widget("input")
        ->name("_username")
        ->label("tr:coupons::rulebooks.username")
        ->value($model->username)
    !!}
</div>
