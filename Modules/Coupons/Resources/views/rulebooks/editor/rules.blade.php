@component('coupons::layout.panel',[
	"heading" => trans('coupons::editor.rules') ,
])

<div class="row">
    <div id="divCurrentRules" class="col-md-7">
        @include("coupons::rulebooks.editor.rules-current")
    </div>
    <div class="col-md-5">
        @include("coupons::rulebooks.editor.rules-new")
    </div>
</div>

@endcomponent
