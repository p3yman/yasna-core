@include('coupons::layout.form.radio',[
     "name" => "_usage_limit" ,
     "value" => "unlimited" ,
     "class" => "radio_form_toggler" ,
     "toggle_view" => "usage_limit_unlimited_container" ,
     "label" => trans('coupons::editor.usage_limit_none') ,
     "is_checked" => $model->code_usage_limit == 0 ,
])

<div class="row p-lg" id="usage_limit_unlimited_container" style="display: none;">
    {!!
    widget("note")
        ->label("tr:coupons::editor.usage_limit_none_hint")
        ->icon('check')
        ->class("text-orange")
    !!}
</div>
