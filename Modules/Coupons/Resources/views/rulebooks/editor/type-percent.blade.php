@include('coupons::layout.form.radio',[
     "name" => "_discount_type" ,
     "value" => "percent" ,
     "class" => "radio_form_toggler" ,
     "toggle_view" => "percent_container" ,
     "label" => trans('coupons::form.attributes.percent') ,
     "is_checked" => boolval($model->applicable_percent) ,
])

<div class="row p-lg" id="percent_container" style="display: none;">
    <div class="col-sm-6">
        {!!
            widget('input')
                ->id("txtDiscountPercent")
                ->label('tr:coupons::form.attributes.amount')
                ->name('_discount_percent')
                ->inForm()
                ->addon('tr:coupons::editor.percent_sign')
                ->value($model->applicable_percent ?: '')
         !!}
    </div>
    <div class="col-sm-6">
        {!!
            widget('input')
                ->id("txtDiscountMaxAmount")
                ->label('tr:coupons::form.attributes.max')
                ->name('_discount_max_amount')
                ->inForm()
                ->numberFormat()
                ->value($model->applicable_amount)
         !!}
    </div>
</div>
