@php
    $edit_route = route("coupons-add-rule" , [
        "class" => $class,
    ]);
@endphp

<a href="{{ v0() }}" class="list-group-item" onclick="masterModal('{{ $edit_route }}')">
    <i class="fa fa-plus text-primary"></i>
    <span class="mh-sm ">{{ $title }}</span>
</a>
