@include('coupons::layout.form.radio',[
     "name" => "_usage_limit" ,
     "value" => "limited" ,
     "class" => "radio_form_toggler" ,
     "toggle_view" => "usage_limit_limited_container" ,
     "label" => trans('coupons::editor.usage_limit_count') ,
     "is_checked" => $model->code_usage_limit > 1 ,
])

<div class="row p-lg" id="usage_limit_limited_container" style="display: none;">
    {!!
    widget("input")
        ->name("code_usage_limit")
        ->label("tr:coupons::editor.usage_limit")
        ->value($model->code_usage_limit? $model->code_usage_limit: 10)
    !!}
</div>
