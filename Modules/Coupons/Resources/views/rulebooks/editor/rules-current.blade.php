<ul class="list-group sortable" data-sortable-id="0" aria-dropeffect="move">
    @if(count($model->getRules()))
        @include("coupons::rulebooks.editor.rules-current-loop")
    @else
        <div class="box-placeholder">
            <p class="text-center p30 text-gray">
                {{ trans('coupons::editor.no_rules_selected') }}
            </p>
        </div>
    @endif
</ul>
