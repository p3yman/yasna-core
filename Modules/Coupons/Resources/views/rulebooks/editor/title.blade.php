@component('coupons::layout.panel',[
	"heading" => trans('coupons::editor.title') ,
])
{!!
     widget('input')
         ->name('title')
         ->value($model->title)
!!}
@endcomponent
