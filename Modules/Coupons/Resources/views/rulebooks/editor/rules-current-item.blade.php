@php
    $edit_route = route("coupons-edit-rule" , [
        "rule" => urlencode(json_encode($rule)),
        "identifier" => $identifier,
    ]);
    $delete_rule = route("coupons-delete-rule" , [
        "rule" => urlencode(json_encode($rule)),
        "identifier" => $identifier,
    ]);
@endphp

<li class="list-group-item" data-item-sortable-id="0" draggable="true" role="option" aria-grabbed="false" style="">
    <em class="fa fa-check fa-fw text-muted mr-lg"></em>

    <span class="clickable" onclick="masterModal('{{ $edit_route }}')">{{ $rule['_class']::getTitle() }}</span>

    <span class="pull-right" onclick="masterModal('{{ $delete_rule }}')">
        <em class="fa fa-times fa-fw text-muted f14 clickable" style="opacity: 0.3"></em>
    </span>
    <span class="pull-right" onclick="masterModal('{{ $edit_route }}')">
        <em class="fa fa-edit fa-fw text-muted f16 clickable"></em>
    </span>
</li>
