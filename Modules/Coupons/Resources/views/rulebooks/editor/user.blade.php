@component('coupons::layout.panel',[
	"heading" => trans('coupons::rulebooks.user') ,
	"style" => "height:300px",
])

<div class="mb">
    @include("coupons::rulebooks.editor.user-particular")
</div>

<div class="mb">
    @include("coupons::rulebooks.editor.user-general")
</div>

@endcomponent

