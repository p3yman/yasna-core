@include("manage::widgets.grid-text" , [
     'text' => $model->title,
     'size' => "16" ,
     'class' => "font-yekan text-bold" ,
     'link' =>  $model->edit_link ,
])

@include("manage::widgets.grid-text" , [
    "size" => "12",
    "text" => $model->description,
])

@include('manage::widgets.grid-date', [
     "by" => $model->creator->full_name,
     'date' => $model->created_at,
])
