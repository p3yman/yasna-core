@include('manage::widgets.grid-row-handle', [
    'refresh_url' => route("coupons-refresh-rulebook" , [
        "hashid" => $model->hashid,
    ]),
    'handle'      => 'selector',
])

<td>
    @include("coupons::rulebooks.browse.row-title")
</td>

<td>
    @include("coupons::rulebooks.browse.row-user")
</td>

<td>
    @include("coupons::rulebooks.browse.row-status")
</td>

@include("manage::widgets.grid-actionCol" , [
    "actions" => [
        ['edit', trans("coupons::rulebooks.edit"), $model->edit_link, $model->isNotTrashed() and coupon()->canEditRulebook()],
        ['ticket' , trans("coupons::codes.title"), $model->codes_link, $model->isNotTrashed() and coupon()->canBrowseCodes()],
        ['copy', trans("coupons::rulebooks.duplicate"), "urlN:".coupon()->getRulebookDuplicatorLink($model->hashid),$model->isNotTrashed() and  coupon()->canCreateRulebook()],
        ['trash', trans("coupons::rulebooks.delete"), coupon()->getRulebookDeleteLink($model->hashid), $model->isNotTrashed() and coupon()->canDeleteRulebook()],
        ['undo', trans("manage::forms.button.undelete"), coupon()->getRulebookUndeleteLink($model->hashid), $model->isTrashed() and coupon()->canRestoreRulebook()],
        ['times', trans("manage::forms.button.hard_delete"), coupon()->getRulebookDestroyLink($model->hashid), $model->isTrashed() and coupon()->canRestoreRulebook()],
    ],
])
