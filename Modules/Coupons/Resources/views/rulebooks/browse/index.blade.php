@extends('manage::layouts.template')

@section('content')
    @include("coupons::rulebooks.browse.tabs")
    @include("coupons::rulebooks.browse.toolbar")
    @include("coupons::rulebooks.browse.grid")
@endsection
