@if($model->is_particular)

    @include("manage::widgets.grid-text" , [
        "text" => $model->user->full_name,
        "icon" => "user-o",
    ])

@else

    @include("manage::widgets.grid-text" , [
        "text" => trans("coupons::rulebooks.for_general"),
        "icon" => "users",
    ])

@endif
