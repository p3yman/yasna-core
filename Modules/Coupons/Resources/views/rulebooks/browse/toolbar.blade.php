@include("manage::widgets.toolbar" , [
    'title'  => $page[0][1] . ' / ' . $page[1][1],
//    'search' => [
//        'target' => '',
//     ],
    'buttons' => [
        [
            "target"    => coupon()->getTrackerLink(),
            "caption"   => trans("coupons::codes.code_track"),
            "icon"      => "search",
            "type"      => "default",
            "condition" => coupon()->canReportCodes(),
        ],
        [
            'target'    => coupon()->getNewRulebookLink($current_group, $current_tab),
            'caption'   => trans_safe("coupons::rulebooks.new"),
            'icon'      => "plus",
            "type"      => "primary",
            "condition" => coupon()->canCreateRulebook()
        ],
    ],
])
