@include("manage::widgets.grid-text" , [
     'text' => trans("coupons::codes.title") . ": " . ad($model->getTotalCodes()),
     'size' => "12" ,
     "link" => "url:".$model->codes_link,
])

@include("manage::widgets.grid-text" , [
     'text' => trans("coupons::codes.total_used") . ": " . ad($model->getTotalUsedCodes()),
     'size' => "12" ,
//     'link' =>  TODO: Coupon Show/Report modal link ,
])

{{-- TODO: Present it on some kind of graph --}}

@include("manage::widgets.grid-text" , [
     'text' => trans("coupons::status.deleted"),
     "condition" => $model->isTrashed(),
     "color" => "red",
])

