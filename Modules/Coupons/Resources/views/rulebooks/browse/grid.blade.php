@include("manage::widgets.grid" , [
    'headings'          => [
        trans("validation.attributes.title"),
        trans("coupons::rulebooks.user"),
        trans("validation.attributes.status"),
    ],
    'row_view'          => "coupons::rulebooks.browse.row",
    'table_id'          => "tblRulebooks",
    'handle'            => 'selector',
    'operation_heading' => true,
])
