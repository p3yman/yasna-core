@php
	$patterns = [
		[
			"caption_field" => trans('coupons::form.attributes.choose_pattern') ,
			"value_field" => "alphabetic" ,
		],
		[
			"caption_field" => trans('coupons::coupons.patterns.alphabetic') ,
			"value_field" => "alphabetic" ,
		],
		[
			"caption_field" => trans('coupons::coupons.patterns.numeric') ,
			"value_field" => "numeric" ,
		],
	];
@endphp

{!!
	widget('modal')
	->label('tr:coupons::coupons.add_new_coupon')
	->target('dkldsh')
	->class('js')
!!}

<div class="modal-body">

	<!-- Pattern -->
	<div class="form-group" id="js_coupon_format">
		<div class="col-sm-12" >
			<div class="row">
				<div class="col-sm-4">
					{!!
						widget('input')
						->placeholder('tr:coupons::form.placeholder.postfix')
						->id('coupon_postfix')
					 !!}
				</div>
				<div class="col-sm-4">
					{!!
						widget('combo')
						->options($patterns)
						->captionField('caption_field')
						->valueField('value_field')
						->id('coupon_pattern')
					 !!}
				</div>
				<div class="col-sm-4">
					{!!
						widget('input')
						->placeholder('tr:coupons::form.placeholder.prefix')
						->id('coupon_prefix')
					 !!}
				</div>
			</div>
		</div>
	</div>

	<div class="mb">
		<div class="alert alert-info f16 text-center ltr">
			<span data-input-id="coupon_prefix">PREFIX</span>-<span data-input-id="coupon_pattern">PATTERN</span>-<span data-input-id="coupon_postfix">POSTFIX</span>
		</div>
	</div>
	<!-- !END Pattern -->

	<!-- Character Count -->
	{!!
		widget('input')
		->label('tr:coupons::form.attributes.character_count')
		->inForm()
	 !!}
	<!-- !END Character Count -->

	<!-- Coupon Count -->
	{!!
		widget('input')
		->label('tr:coupons::form.attributes.coupon_count')
		->inForm()
	 !!}
	<!-- !END Coupon Count -->

</div>

<div class="modal-footer">
	{!!
	    widget('button')
	    ->label('tr:coupons::coupons.cancel')
	    ->class('btn-link')
	 !!}
	{!!
	    widget('button')
	    ->label('tr:coupons::coupons.add')
	    ->class('btn-success btn-taha')
	 !!}
</div>
