<div class="panel" style="{{ $style or "" }}">
	<div class="panel-heading">
		<div class="f16 text-bold">
			{{ $heading }}
		</div>
	</div>
	<div class="panel-body">
		{{ $slot }}
	</div>
</div>
