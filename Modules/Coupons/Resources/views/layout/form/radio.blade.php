<div class="radio c-radio {{ $class or "" }}">
	<label>
		<input type="radio"
			   name="{{ $name }}"
			   value="{{ $value }}"
			   @if(isset($is_checked) and $is_checked) checked @endif
			   data-toggle-view="{{ $toggle_view or "" }}"
		>
		<span class="fa fa-circle"></span>
		{{ $label }}
	</label>
</div>
