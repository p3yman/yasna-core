@include("manage::layouts.sidebar-link" , [
	'icon' => "gift",
	'caption' => trans("coupons::rulebooks.title") ,
	'link' => "coupons" ,
	"sub_menus" => $sub_menus = service('coupons:models')->read() ,
	'permission' => sizeof($sub_menus) ? '' : 'dev',
]     )
