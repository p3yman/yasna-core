<?php

return [
     'name' => 'Coupons',

     'status' => [
          'color' => [
               "deleted" => "inverse",
               "used"    => "danger",
               "usable"  => "success",
          ],
     ],
];
