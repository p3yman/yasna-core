<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponRulebooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_rulebooks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('organization_id')->default(0)->index();
            $table->string('title')->index();
            $table->string('model_name')->index();

            $table->float('applicable_percent', 15, 2)->default(0);
            $table->float('applicable_amount', 15, 2)->default(0);
            $table->boolean('is_particular')->default(0);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_rulebooks');
    }
}
