<?php

namespace Modules\Coupons\Services;

use App\Models\User;
use Carbon\Carbon;
use Modules\Yasna\Services\GeneralTraits\Serializable;

/**
 * Class InvoiceObject
 * Keeps the required data to validate a submitted coupon
 * @method float getTotalAmount()
 * @method Carbon getEffectiveDate()
 * @method User getUser()
 * @method array getItems()
 * @method array getAppliedRules()
 * @method array getDiscounts()
 * @method getOriginalInstance()
 */
class InvoiceObject
{
    use Serializable;

    /**
     * keep the invoice total amount
     *
     * @var float
     */
    protected $total_amount;

    /**
     * keep the invoice effective date
     *
     * @var Carbon
     */
    protected $effective_date;

    /**
     * keep the invoice beneficiary
     *
     * @var User
     */
    protected $user;

    /**
     * keep the original instance of the invoice object
     *
     * @var mixed
     */
    protected $original_instance;

    /**
     * keep the array of invoiced items
     *
     * @var array
     */
    protected $items;

    /**
     * keep the list of rules applied
     *
     * @var array
     */
    protected $applied_rules;

    /**
     * keep discounts made by each rule
     * the array would be something like this:
     * [
     *      'folanRule' => [
     *          "total" => 1230",
     *          "items" => [
     *              '12' => 12,
     *              '13' => 0,
     *          ],
     *      ],
     *      'fooRule' => [
     *          "total" => 1000,
     *      ],
     * ]
     *
     * @var array
     */
    protected $discounts;

    /**
     * keep the flag holding if the evaluation failed because of the invalid code number
     *
     * @var bool
     */
    protected $invalid_code = false;



    /**
     * InvoiceObject constructor.
     *
     * @param string|Carbon   $effective_date the invoice effective date
     * @param float|int       $total_amount   the total invoice amount
     * @param int|string|User $user           the user asking discount, sent by instance or identified by id / hashid
     */
    public function __construct($effective_date, $total_amount = 0, $user = 0)
    {
        $this->setEffectiveDate($effective_date);
        $this->setTotalAmount($total_amount);
        $this->setUser($user);
    }



    /**
     * make a magic method to response getters
     *
     * @param string $method_name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($method_name, $arguments)
    {
        if (str_contains($method_name, "get")) {
            return $this->generalGetter($method_name);
        }

        return null;
    }



    /**
     * make a general getter method
     *
     * @param string $method_name
     *
     * @return mixed
     */
    protected function generalGetter($method_name)
    {
        $argument = snake_case(str_after($method_name, 'get'));
        if (isset($this->$argument)) {
            return $this->$argument;
        }

        return null;
    }



    /**
     * set $this->total_amount, casting it to the float data type
     *
     * @param float|int $amount
     */
    public function setTotalAmount($amount)
    {
        $this->total_amount = (float)$amount;
    }



    /**
     * set $this->effective_date
     *
     * @param string|Carbon $date
     */
    public function setEffectiveDate($date)
    {
        if ($date instanceof Carbon) {
            $this->effective_date = $date;
            return;
        }

        if (is_string($date)) {
            $this->effective_date = new Carbon($date);
            return;
        }

        $this->effective_date = now();
    }



    /**
     * set $this->user
     *
     * @param $user
     */
    public function setUser($user)
    {
        if ($user instanceof User) {
            $this->user = $user;
            return;
        }

        $this->user = model("user", $user, true);
    }



    /**
     * set $this->original_instance
     *
     * @param mixed $instance
     */
    public function setOriginalInstance($instance)
    {
        $this->original_instance = $instance;
    }



    /**
     * add an item by setting a new amount to it
     *
     * @param float|int  $amount     the item's price
     * @param string|int $identifier the item's identifier (whatever known to the submitter)
     */
    public function addItem($amount, $identifier = null)
    {
        if ($identifier) {
            $this->items[$identifier] = $amount;
        } else {
            $this->items[] = $amount;
        }

        $this->setTotalAmountFromAddedItems();
    }



    /**
     * add rule identifier to the list of applied rules
     *
     * @param string $rule_identifier
     */
    public function addAppliedRule($rule_identifier)
    {
        $this->applied_rules[] = $rule_identifier;
    }



    /**
     * set a total discount in $this->discounts
     *
     * @param string    $rule_identifier
     * @param float|int $amount
     *
     * @return $this
     */
    public function setTotalDiscount($rule_identifier, $amount)
    {
        $amount = min($amount, $this->total_amount);

        $this->discounts[$rule_identifier]['total'] = (float)$amount;

        return $this;
    }



    /**
     * set an item discount in $this->discounts
     *
     * @param string    $rule_identifier
     * @param string    $item_identifier
     * @param float|int $amount
     *
     * @return $this
     */
    public function setItemDiscount($rule_identifier, $item_identifier, $amount)
    {
        if (!isset($this->items[$item_identifier])) {
            return $this;
        }

        $amount = min($amount, $this->items[$item_identifier]);

        $this->discounts[$rule_identifier]['items'][$item_identifier] = (float)$amount;

        return $this;
    }



    /**
     * mark the invoice as invalid because of the invalid code
     *
     * @return $this
     */
    public function markAsInvalid()
    {
        $this->invalid_code = true;

        return $this;
    }



    /**
     * get the total calculated discount
     *
     * @return float
     */
    public function getTotalDiscount()
    {
        $collect = [];

        if (!is_array($this->discounts)) {
            return 0;
        }

        foreach ($this->discounts as $discount) {
            if (isset($discount['total'])) {
                $collect[] = $discount['total'];
            }
        }

        return min($collect);
    }



    /**
     * get item discounts
     */
    public function getItemDiscounts(): array
    {
        $array = [];

        foreach ((array)$this->discounts as $discount) {
            if (isset($discount['items'])) {
                foreach ($discount['items'] as $key => $item_discount) {
                    if (isset($array[$key])) {
                        $array[$key] = min($array[$key], $item_discount);
                    } else {
                        $array[$key] = $item_discount;
                    }
                }
            }
        }

        return $array;
    }



    /**
     * convert the object to array
     *
     * @return array
     */
    public function toArray(): array
    {
        return [
             "user"           => $this->user->full_name,
             "effective_date" => $this->effective_date,
             "items"          => $this->items,
             "total_amount"   => $this->total_amount,
             "applied_rules"  => $this->applied_rules,
             "discounts"      => $this->discounts,
             "invalid_code"   => $this->invalid_code,
             "total_discount" => $this->getTotalDiscount(),
             "item_discounts" => $this->getItemDiscounts(),
        ];
    }



    /**
     * set total amount from added items
     */
    protected function setTotalAmountFromAddedItems()
    {
        $total_amount = array_sum($this->items);

        $this->setTotalAmount($total_amount);
    }
}
