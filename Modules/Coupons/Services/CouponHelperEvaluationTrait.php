<?php

namespace Modules\Coupons\Services;

use App\Models\CouponCode;
use Carbon\Carbon;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Coupons\Entities\CouponRulebook;

trait CouponHelperEvaluationTrait
{
    /**
     * evaluate the given coupon(s) code, by applying its rule to the given invoice
     *
     * @param CouponCode|string|array $code
     * @param InvoiceObject           $invoice
     *
     * @return InvoiceObject
     */
    public function evaluate($code, InvoiceObject $invoice): InvoiceObject
    {
        $codes = (array)$code;

        foreach ($codes as $code) {
            $invoice = $this->evaluateOneCode($code, $invoice);
        }

        return $invoice;
    }



    /**
     * run a evaluate simulation without a valid code number
     *
     * @param CouponRulebook $rulebook
     * @param InvoiceObject  $invoice
     *
     * @return InvoiceObject
     */
    public function simulate(CouponRulebook $rulebook, InvoiceObject $invoice): InvoiceObject
    {
        $code           = couponCode();
        $code->rulebook = $rulebook;

        $invoice = $this->evaluateThroughConsistentRules($code, $invoice);
        $invoice = $this->evaluateThroughNormalRules($code, $invoice);

        return $invoice;
    }



    /**
     * run an evaluation test
     *
     * @deprecated from 97/07/10
     *
     * @param CouponRulebook $rulebook
     * @param float|int      $total_amount
     * @param string|Carbon  $effective_date
     *
     * @return InvoiceObject
     */
    public function evaluateTester($rulebook, $total_amount, $effective_date): InvoiceObject
    {
        $code           = couponCode();
        $code->rulebook = $rulebook;

        $invoice = coupon()->makeInvoice($effective_date, $total_amount);

        $invoice = $this->evaluateThroughConsistentRules($code, $invoice);
        $invoice = $this->evaluateThroughNormalRules($code, $invoice);

        return $invoice;
    }



    /**
     * evaluate one coupon code at a time, by applying its rule to the given invoice
     *
     * @param CouponCode|string $code
     * @param InvoiceObject     $invoice
     *
     * @return InvoiceObject
     */
    protected function evaluateOneCode($code, InvoiceObject $invoice): InvoiceObject
    {
        $code = $this->codeFinder($code);

        if (!$code->exists or !$code->rulebook) {
            return $invoice->markAsInvalid();
        }

        $invoice = $this->evaluateThroughConsistentRules($code, $invoice);
        $invoice = $this->evaluateThroughNormalRules($code, $invoice);

        $code->logQuery($invoice);

        return $invoice;
    }



    /**
     * go through the rules and run them one by one
     *
     * @param CouponCode    $code
     * @param InvoiceObject $invoice
     *
     * @return InvoiceObject
     */
    protected function evaluateThroughNormalRules(CouponCode $code, InvoiceObject $invoice): InvoiceObject
    {
        $rules = $code->rulebook->getRules();

        foreach ($rules as $rule) {
            $invoice = $this->evaluateInOneClass($rule['_class'], $code, $invoice, $rule);
        }

        return $invoice;
    }



    /**
     * go through the consistent rules and run them one by one
     *
     * @param CouponCode    $code
     * @param InvoiceObject $invoice
     *
     * @return InvoiceObject
     */
    protected function evaluateThroughConsistentRules(CouponCode $code, InvoiceObject $invoice): InvoiceObject
    {
        $rules = coupon()->getAllRegisteredConsistentRules();

        foreach ($rules as $rule) {
            $invoice = $this->evaluateInOneClass($rule['class'], $code, $invoice);
        }

        return $invoice;
    }



    /**
     * run one discount rule
     *
     * @param DiscountRule  $class
     * @param CouponCode    $code
     * @param InvoiceObject $invoice
     *
     * @return InvoiceObject
     */
    protected function evaluateInOneClass($class, CouponCode $code, InvoiceObject $invoice, array $data = [])
    {
        try {
            $invoice = $class::run($code, $invoice, $data);
        } catch (\Exception $e) {
            // TODO: Calling some kind of marker would be nice herein.
        }

        return $invoice;
    }



    /**
     * find the coupon by its slug, or return the instance if directly given
     *
     * @param CouponCode|string $code
     *
     * @return CouponCode
     */
    protected function codeFinder($code): CouponCode
    {
        if ($code instanceof CouponCode) {
            return $code;
        }

        return couponCode()->grabSlug($code);
    }
}
