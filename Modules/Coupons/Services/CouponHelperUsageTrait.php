<?php

namespace Modules\Coupons\Services;

use App\Models\CouponCode;

trait CouponHelperUsageTrait
{
    /**
     * mark code(s) as used on a particular model
     *
     * @param string|array     $code_slug
     * @param int              $model_id
     * @param int              $user_id
     * @param int|double|float $applied_discount
     *
     * @return int
     */
    public function markAsUsed($code_slug, int $model_id, int $user_id, $applied_discount)
    {
        $slugs = (array)$code_slug;
        $done  = 0;

        foreach ($slugs as $slug) {
            $code = couponCode()->grabSlug($slug);

            $done += $this->markOneAsUsed($code, $model_id, $user_id, $applied_discount);
        }

        return $done;
    }



    /**
     * mark a single code as used on a particular model
     *
     * @param CouponCode       $code
     * @param int              $model_id
     * @param int              $user_id
     * @param int|double|float $applied_discount
     *
     * @return int
     */
    protected function markOneAsUsed(CouponCode $code, int $model_id, int $user_id, $applied_discount)
    {
        if (!$code->exists) {
            return 0;
        }

        return (int)$code->markAsUsed($model_id, $user_id, $applied_discount);
    }
}
