<?php

namespace Modules\Coupons\Services;


trait CouponHelperLinksTrait
{
    /**
     * get code generator link
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getCodeGeneratorLink(string $hashid)
    {
        return route("coupons-codes-create", ["rulebook" => $hashid,]);
    }



    /**
     * get the link to the code exporter modal box
     *
     * @param string $hashid
     * @param int    $batch
     *
     * @return string
     */
    public function getCodesDownloaderLink(string $hashid, int $batch = 0)
    {
        $route = route("coupons-codes-download-form", [
             "hashid" => $hashid,
             "batch"  => $batch,
        ]);

        return "masterModal('$route')";
    }



    /**
     * get the link to the code list modal box
     *
     * @param string $hashid
     * @param int    $batch
     *
     * @return string
     */
    public function getCodesListLink(string $hashid, int $batch = 0)
    {
        $route = route("coupons-codes-list", [
             "hashid" => $hashid,
             "batch"  => $batch,
        ]);

        return "masterModal('$route')";
    }



    /**
     * get tracker modal box link
     *
     * @return string
     */
    public function getTrackerLink()
    {
        $route = route("coupons-code-tracker-form");

        return "masterModal('$route')";
    }



    /**
     * get the link to create a new rulebook
     *
     * @param string $current_group
     * @param string $current_tab
     *
     * @return string
     */
    public function getNewRulebookLink($current_group, $current_tab)
    {
        return route("coupons-new-rulebook", [
             "group" => $current_group,
             "tab"   => $current_tab,
        ]);
    }



    /**
     * get rulebook duplicator link
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getRulebookDuplicatorLink(?string $hashid)
    {
        return route("coupons-duplicate-rulebook", [
             "hashid" => $hashid,
        ]);
    }



    /**
     * get rulebook delete modal link
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getRulebookDeleteLink(string $hashid)
    {
        return "modal:manage/coupons/act/$hashid/delete";
    }



    /**
     * get rulebook undelete modal link
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getRulebookUndeleteLink(string $hashid)
    {
        return "modal:manage/coupons/act/$hashid/undelete";
    }



    /**
     * get rulebook destroy modal link
     *
     * @param string $hashid
     *
     * @return string
     */
    public function getRulebookDestroyLink(string $hashid)
    {
        return "modal:manage/coupons/act/$hashid/destroy";
    }

}
