<?php

namespace Modules\Coupons\Services;


use Carbon\Carbon;

class CouponHelper
{
    const RULES_SERVICE_NAME            = "rules";
    const CONSISTENT_RULES_SERVICE_NAME = "consistent-rules";

    use CouponHelperRulesTrait;
    use CouponHelperEvaluationTrait;
    use CouponHelperUsageTrait;
    use CouponHelperExcelTrait;
    use CouponHelperLinksTrait;
    use CouponHelperPermitTrait;

    /**
     * keep the order of registration, useful for rules and groups
     *
     * @var int
     */
    private static $order;



    /**
     * call and return an invoice object constructor
     *
     * @param float|int     $total_amount
     * @param string|Carbon $effective_date
     * @param int           $user
     *
     * @return InvoiceObject
     */
    public function makeInvoice($effective_date, $total_amount = 0, $user = 0)
    {
        return (new InvoiceObject($effective_date, $total_amount, $user));
    }



    /**
     * register a model to be used as a valid group of discount rulebooks
     *
     * @param string        $model_name
     * @param string        $caption
     * @param bool|\Closure $condition
     */
    public function registerGroup(string $model_name, string $caption, $condition = true)
    {
        $closure = function() {
            return coupon()->canBrowseRulebook();
        };

        service("coupons:models")
             ->add($model_name)
             ->link("coupons/browse/$model_name")
             ->caption($caption)
             ->order(static::$order++)
             ->condition($condition and $closure)
        ;

    }
}
