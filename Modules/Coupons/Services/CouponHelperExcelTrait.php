<?php

namespace Modules\Coupons\Services;

use App\Models\CouponCode;
use Illuminate\Database\Eloquent\Collection;

trait CouponHelperExcelTrait
{
    /**
     * get the collection of codes, required for the excel export
     *
     * @return Collection
     */
    public function getExcelCodes()
    {
        $config = explode(',', session()->pull('coupon-codes'));

        $builder = couponCode()->elector([
             "rulebook" => $config[0],
             "batch"    => $config[1],
             "filter"   => $config[2],
        ]);

        return $builder->get();
    }
}
