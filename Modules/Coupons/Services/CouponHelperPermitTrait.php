<?php

namespace Modules\Coupons\Services;


trait CouponHelperPermitTrait
{
    /**
     * check if the current user can browse rulebooks
     *
     * @return bool
     */
    public function canBrowseRulebook(): bool
    {
        return user()->can("coupons.rulebook-browse");
    }



    /**
     * check if the current user cannot browse rulebooks
     *
     * @return bool
     */
    public function cannotBrowseRulebook(): bool
    {
        return !$this->canBrowseRulebook();
    }



    /**
     * check if the current user can create rulebooks
     *
     * @return bool
     */
    public function canCreateRulebook(): bool
    {
        return user()->can("coupons.rulebook-create");
    }



    /**
     * check if the current user cannot create rulebooks
     *
     * @return bool
     */
    public function cannotCreateRulebook(): bool
    {
        return !$this->canCreateRulebook();
    }



    /**
     * check if the current user can edit rulebooks
     *
     * @return bool
     */
    public function canEditRulebook(): bool
    {
        return user()->can("coupons.rulebook-edit");
    }



    /**
     * check if the current user cannot edit rulebooks
     *
     * @return bool
     */
    public function cannotEditRulebook(): bool
    {
        return !$this->canEditRulebook();
    }



    /**
     * check if the current user can delete rulebooks
     *
     * @return bool
     */
    public function canDeleteRulebook(): bool
    {
        return user()->can("coupons.rulebook-delete");
    }



    /**
     * check if the current user cannot delete rulebooks
     *
     * @return bool
     */
    public function cannotDeleteRulebook(): bool
    {
        return !$this->canDeleteRulebook();
    }



    /**
     * check if the current user can restore deleted rulebooks
     *
     * @return bool
     */
    public function canRestoreRulebook(): bool
    {
        return user()->can("coupons.rulebook-delete");
    }



    /**
     * check if the current user cannot restore deleted rulebooks
     *
     * @return bool
     */
    public function cannotRestoreRulebook(): bool
    {
        return !$this->canRestoreRulebook();
    }



    /**
     * check if the current user can get reports from rulebooks
     *
     * @return bool
     */
    public function canReportRulebook(): bool
    {
        return user()->can("coupons.rulebook-browse");
    }



    /**
     * check if the current user cannot get reports from rulebooks
     *
     * @return bool
     */
    public function cannotReportRulebook(): bool
    {
        return !$this->canReportRulebook();
    }



    /**
     * check if the current user can browse codes
     *
     * @return bool
     */
    public function canBrowseCodes(): bool
    {
        return user()->can("coupons.code-browse");
    }



    /**
     * check if the current user cannot browse codes
     *
     * @return bool
     */
    public function cannotBrowseCodes(): bool
    {
        return !$this->canBrowseCodes();
    }



    /**
     * check if the current user can create codes
     *
     * @return bool
     */
    public function canCreateCodes(): bool
    {
        return user()->can("coupons.code-create");
    }



    /**
     * check if the current user cannot create codes
     *
     * @return bool
     */
    public function cannotCreateCodes(): bool
    {
        return !$this->canCreateCodes();
    }



    /**
     * check if the current user can edit codes
     *
     * @return bool
     */
    public function canEditCodes(): bool
    {
        return user()->can("coupons.code-edit");
    }



    /**
     * check if the current user cannot edit codes
     *
     * @return bool
     */
    public function cannotEditCodes(): bool
    {
        return !$this->canEditCodes();
    }



    /**
     * check if the current user can delete codes
     *
     * @return bool
     */
    public function canDeleteCodes(): bool
    {
        return user()->can("coupons.code-delete");
    }



    /**
     * check if the current user cannot delete codes
     *
     * @return bool
     */
    public function cannotDeleteCodes(): bool
    {
        return !$this->canDeleteCodes();
    }



    /**
     * check if the current user can restore deleted codes
     *
     * @return bool
     */
    public function canRestoreCodes(): bool
    {
        return user()->can("coupons.code-delete");
    }



    /**
     * check if the current user cannot restore deleted codes
     *
     * @return bool
     */
    public function cannotRestoreCodes(): bool
    {
        return !$this->canRestoreCodes();
    }



    /**
     * check if the current user can get reports from codes
     *
     * @return bool
     */
    public function canReportCodes(): bool
    {
        return user()->can("coupons.code-report");
    }



    /**
     * check if the current user cannot get reports from codes
     *
     * @return bool
     */
    public function cannotReportCodes(): bool
    {
        return !$this->canReportCodes();
    }



    /**
     * check if the current user can export the codes
     *
     * @return bool
     */
    public function canExportCodes(): bool
    {
        return user()->can("coupons.code-report");
    }



    /**
     * check if the current user cannot export the codes
     *
     * @return bool
     */
    public function cannotExportCodes(): bool
    {
        return !$this->canExportCodes();
    }
}
