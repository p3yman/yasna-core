<?php

namespace Modules\Coupons\Services;

trait CouponHelperRulesTrait
{
    /**
     * register a new rule in the service environment
     *
     * @param string $class
     */
    public function registerRule(string $class)
    {
        $caption = $class::getTitle();

        module("coupons")
             ->service(static::RULES_SERVICE_NAME)
             ->add($class)
             ->class($class)
             ->caption($caption)
             ->order(static::$order++)
        ;
    }



    /**
     * register a new consistent rule in the service environment
     *
     * @param string $class
     */
    public function registerConsistentRule(string $class)
    {
        module("coupons")
             ->service(static::CONSISTENT_RULES_SERVICE_NAME)
             ->add($class)
             ->class($class)
             ->order(static::$order++)
        ;
    }



    /**
     * get an array of registered rules
     *
     * @return array
     */
    public function getRegisteredRules()
    {
        return module("coupons")->service(static::RULES_SERVICE_NAME)->paired('caption', 'class');
    }



    /**
     * get an array of registered consistent rules
     *
     * @return array
     */
    public function getAllRegisteredConsistentRules()
    {
        return module("coupons")->service(static::CONSISTENT_RULES_SERVICE_NAME)->read();
    }
}
