<?php

namespace Modules\Coupons\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MakeRuleCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-discount-rule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a coupon discount rule class.';
    


    /**
     * get the target folder os path
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('CouponRules');
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Rule')) {
            return $file_name . "Rule";
        }
        return $file_name;
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "rule.stub";
    }
}
