<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class UserCompleteFieldsRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans('coupons::rules.complete-fields-rule');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add('selectize')
             ->name('fields')
             ->options(self::getOptions())
             ->valueField('value')
             ->captionField('title')
             ->required()
             ->validationRules("required")
             ->inForm()
             ->label(trans('coupons::rules.complete-fields'))
        ;

        $form->add('note')
             ->label("tr:coupons::rules.complete-fields-hint")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $fields        = explode(',', $this->para['fields']);

        if ($this->applyRule($fields)) {
            $this->markAsValid();
        } else {
            $this->markAsInvalid();
        }
    }



    /**
     * check that fields aren't empty
     *
     * @param $fields
     *
     * @return bool
     */
    private function applyRule($fields)
    {
        $model = $this->invoice->getUser();

        foreach ($fields as $field) {
            if (in_array($field, $this->zeroEmptyFields())) {
                if ($model->{$field} == 0) {
                    return false;
                }
            } else {
                    if (empty($model->{$field})) {
                    return false;
                }
            }
        }
        return true;
    }



    /**
     * get fields that if be zero it's empty
     *
     * @return array
     */
    private function zeroEmptyFields()
    {
        return [
             'gender',
        ];
    }



    /**
     * return options of selectize
     *
     * @return array
     */
    private static function getOptions()
    {
        return [
             [
                  "value" => 'name_first',
                  "title" => trans('coupons::rules.name-field'),
             ],
             [
                  "value" => 'name_last',
                  "title" => trans('coupons::rules.family-field'),
             ],
             [
                  "value" => 'name_father',
                  "title" => trans('coupons::rules.father-name-field'),
             ],
             [
                  "value" => 'code_melli',
                  "title" => trans('coupons::rules.code-melli-field'),
             ],
             [
                  "value" => 'email',
                  "title" => trans('coupons::rules.email-field'),
             ],
             [
                  "value" => 'gender',
                  "title" => trans('coupons::rules.gender-field'),
             ],
             [
                  "value" => 'mobile',
                  "title" => trans('coupons::rules.mobile-field'),
             ],
             [
                  "value" => 'tel',
                  "title" => trans('coupons::rules.tel-field'),
             ],
             [
                  "value" => 'home_address',
                  "title" => trans('coupons::rules.home-address-field'),
             ],
             [
                  "value" => 'birth_date',
                  "title" => trans('coupons::rules.birth-date-field'),
             ],
        ];
    }
}
