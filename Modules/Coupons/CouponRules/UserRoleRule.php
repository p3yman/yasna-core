<?php

namespace Modules\Coupons\CouponRules;


use App\Models\User;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Coupons\Services\InvoiceObject;
use Modules\Manage\Services\Form;

class UserRoleRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rules.user-role");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("roles_allowed")
             ->label("tr:coupons::rules.allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("slug")
             ->captionField("title")
        ;

        $form->add("selectize")
             ->name("roles_not_allowed")
             ->label("tr:coupons::rules.not-allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("slug")
             ->captionField("title")
        ;

        return $form;
    }



    /**
     * get selectize options
     *
     * @return array
     */
    protected static function getSelectizeOptions()
    {
        $roles = role()->orderBy('title')->get();
        $array = [];

        foreach ($roles as $role) {
            $array[] = [
                 "slug"  => $role->slug,
                 "title" => $role->title,
            ];

        }

        return $array;
    }



    /**
     * Returns an array of allowed roles.
     *
     * @return array
     */
    protected function allowedRoles()
    {
        return explode_not_empty(',', $this->para['roles_allowed']);
    }



    /**
     * Returns an array of baned roles.
     *
     * @return array
     */
    protected function banedRoles()
    {
        return explode_not_empty(',', $this->para['roles_not_allowed']);
    }



    /**
     * Returns the user object
     *
     * @return User
     */
    protected function getUser()
    {
        return $this->invoice->getUser();
    }



    /**
     * Checks if the user has any of allowed roles.
     *
     * @return bool
     */
    protected function hasAnyOfAllowedRoles()
    {
        $allowed_roles = $this->allowedRoles();
        if (empty($allowed_roles)) {
            return false;
        }
        return $this->getUser()->hasAnyOfRoles($allowed_roles);
    }



    /**
     * Checks if the user has any of baned roles.
     *
     * @return bool
     */
    protected function hasAnyOfBannedRoles()
    {
        $baned_roles = $this->banedRoles();
        if (empty($baned_roles)) {
            return false;
        }
        return $this->getUser()->hasAnyOfRoles($baned_roles);
    }



    /**
     * Checks if the user has not any of baned roles.
     *
     * @return bool
     */
    protected function hasNotAnyOfBanedRoles()
    {
        return !$this->hasAnyOfBannedRoles();
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if ($this->hasAnyOfAllowedRoles() and $this->hasNotAnyOfBanedRoles()) {
            $this->markAsValid();
        } else {
            $this->markAsInvalid();
        }
    }
}
