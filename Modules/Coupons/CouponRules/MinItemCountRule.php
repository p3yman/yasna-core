<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class MinItemCountRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rules.minimum-items");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("note")
             ->label("tr:coupons::rules.minimum-items-hint")
             ->icon("lightbulb-o")
        ;

        $form->add('input')
             ->name('min')
             ->label('tr:coupons::rules.minimum-items')
             ->inForm()
             ->purificationRules('ed')
             ->validationRules('required|numeric|integer|min:1')
        ;

        return $form;
    }



    /**
     * Returns the minimum items of a discountable invoice.
     *
     * @return int
     */
    protected function minimumItemsNumber()
    {
        return $this->para['min'];
    }



    /**
     * Returns count of items of the invoice.
     *
     * @return int
     */
    protected function invoiceItemsNumber()
    {
        return count($this->invoice->getItems());
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if ($this->invoiceItemsNumber() >= $this->minimumItemsNumber()) {
            $this->markAsValid();
        } else {
            $this->markAsInvalid();
        }
    }
}
