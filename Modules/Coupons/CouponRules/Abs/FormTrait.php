<?php

namespace Modules\Coupons\CouponRules\Abs;

use Modules\Manage\Services\Form;

trait FormTrait
{
    /**
     * make the form content
     *
     * @return Form
     */
    protected static function makeForm(): Form
    {
        // Override this method to define the form items.

        return form();
    }



    /**
     * render the form
     *
     * @param array $values
     *
     * @return void
     */
    public static function renderForm($values = [])
    {
        static::makeForm()->render($values);
    }



    /**
     * get the form instance
     *
     * @return Form
     */
    public static function getFormInstance()
    {
        return static::makeForm();
    }
}
