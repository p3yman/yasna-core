<?php

namespace Modules\Coupons\CouponRules\Abs;

use App\Models\CouponCode;
use Modules\Coupons\Entities\CouponRulebook;
use Modules\Coupons\Services\InvoiceObject;
use Modules\Yasna\Services\GeneralTraits\ClassRecognitionsTrait;

abstract class DiscountRule
{
    use ClassRecognitionsTrait;
    use FormTrait;
    use RunTrait;

    /**
     * keep the invoice object
     *
     * @var InvoiceObject
     */
    protected $invoice;

    /**
     * keep the coupon-cde model object
     *
     * @var CouponCode
     */
    protected $code;

    /**
     * keep the coupon-rulebook model object
     *
     * @var CouponRulebook
     */
    protected $rulebook;

    /**
     * keep the user settings array
     *
     * @var array
     */
    protected $para;



    /**
     * AbstractRule constructor.
     *
     * @param CouponCode    $code
     * @param InvoiceObject $invoice
     * @param array         $parameters
     */
    public function __construct(CouponCode $code, InvoiceObject $invoice, array $parameters = [])
    {
        $this->code     = $code;
        $this->invoice  = $invoice;
        $this->rulebook = $code->rulebook;
        $this->para     = $parameters;
    }



    /**
     * get human-identifiable title of the rule
     *
     * @return string
     */
    public abstract static function getTitle(): string;



    /**
     * get this rule's identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return self::calledClassName();
    }



    /**
     * handle the rule logic based on the injected dependencies and write changes to the InvoiceObject
     *
     * @return void
     */
    public abstract function handle();
}
