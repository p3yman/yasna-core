<?php

namespace Modules\Coupons\CouponRules\Abs;

use App\Models\CouponCode;
use Modules\Coupons\Services\InvoiceObject;

/**
 * Class DiscountRule
 *
 * @property InvoiceObject $invoice
 * @package Modules\Coupons\CouponRules\Abs
 */
trait RunTrait
{
    /**
     * static object builder
     *
     * @param CouponCode    $code
     * @param InvoiceObject $invoice
     * @param array         $parameters
     *
     * @return InvoiceObject
     */
    public static function run(CouponCode $code, InvoiceObject $invoice, array $parameters): InvoiceObject
    {
        /**
         * @var DiscountRule $object
         */
        $called = static::calledClass();
        $object = new $called($code, $invoice, $parameters);

        return $object->process();
    }



    /**
     * process the rule
     *
     * @return InvoiceObject
     */
    protected function process(): InvoiceObject
    {
        $this->setAppliedRule();
        $this->handle();

        return $this->invoice;
    }



    /**
     * set applied rule (called) to the invoice
     */
    protected function setAppliedRule()
    {
        $this->invoice->addAppliedRule(static::calledClass());
    }



    /**
     * set a total discount in $this->invoice
     *
     * @param float|int $amount
     *
     * @return $this
     */
    protected function setTotalDiscount($amount)
    {
        $this->invoice->setTotalDiscount($this->getIdentifier(), $amount);

        return $this;
    }



    /**
     * set discount for a particular item in $this->invoice
     *
     * @param string    $identifier
     * @param float|int $amount
     */
    protected function setItemDiscount($identifier, $amount)
    {
        $this->invoice->setItemDiscount($this->getIdentifier(), $identifier, $amount);
    }



    /**
     * mark $this->invoice as valid for the purpose of the current running rule
     */
    protected function markAsValid()
    {
        // nothing indeed :)
    }



    /**
     * mark $this->invoice as invalid for the purpose of the current running rule
     */
    protected function markAsInvalid()
    {
        $this->setTotalDiscount(0);
    }
}
