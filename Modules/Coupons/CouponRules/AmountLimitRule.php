<?php

namespace Modules\Coupons\CouponRules;


use Modules\Coupons\CouponRules\Abs\DiscountRule;

class AmountLimitRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rules.amount-limit");
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if ($this->rulebook->applicable_percent) {
            $this->handleForPercentTypes();
        } else {
            $this->handleForAmountTypes();
        }
    }



    /**
     * handle for percent type rulebooks
     */
    private function handleForPercentTypes()
    {
        $total_invoiced      = $this->invoice->getTotalAmount();
        $percent             = $this->rulebook->applicable_percent;
        $limit               = $this->rulebook->applicable_amount;
        $discount_by_percent = round($total_invoiced * $percent / 100, 2);

        if ($limit) {
            $applicable_discount = min($limit, $discount_by_percent);
        } else {
            $applicable_discount = $discount_by_percent;
        }

        $this->setTotalDiscount($applicable_discount);
    }



    /**
     * handle for amount type rulebooks
     */
    private function handleForAmountTypes()
    {
        $this->setTotalDiscount($this->rulebook->applicable_amount);
    }
}
