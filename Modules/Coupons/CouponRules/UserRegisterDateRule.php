<?php

namespace Modules\Coupons\CouponRules;

use Carbon\Carbon;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class UserRegisterDateRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans('coupons::rules.user_register_date_rule');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add('PersianDatePicker')
             ->label('tr:coupons::rules.since')
             ->purificationRules("date")
             ->name('since')
             ->inForm()
        ;

        $form->add('PersianDatePicker')
             ->label('tr:coupons::rules.up-to')
             ->purificationRules("date")
             ->name('to')
             ->inForm()
        ;

        $form->add('note')
             ->label("tr:coupons::rules.user-register-hint")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $register_date = carbon()::parse($this->invoice->getUser()->created_at);

        if (!$this->checkLowerLimit($register_date) or !$this->checkUpperLimit($register_date)) {
            $this->markAsInvalid();
        }
    }



    /**
     * check if the upper limit rule is satisfied
     *
     * @param Carbon $register_date
     *
     * @return bool
     */
    protected function checkUpperLimit(Carbon $register_date): bool
    {
        if (!$this->para['to']) {
            return true;
        }

        $date = carbon()::parse($this->para['to']);

        return $date->greaterThanOrEqualTo($register_date);
    }



    /**
     * check if the lower limit rule is satisfied
     *
     * @param Carbon $register_date
     *
     * @return bool
     */
    protected function checkLowerLimit(Carbon $register_date): bool
    {
        if (!$this->para['since']) {
            return true;
        }

        $date = carbon()::parse($this->para['since']);

        return $date->lessThanOrEqualTo($register_date);
    }
}
