<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class UserStateRule extends DiscountRule
{
    private $province_white;
    private $province_black;



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rules.province-rule");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form      = form();
        $provinces = model('state')::where('parent_id', '0')->orderBy('title')->get(['id', 'title'])->toArray();

        $form->add('selectize')
             ->name('province_white')
             ->options($provinces)
             ->valueField('id')
             ->captionField('title')
             ->inForm()
             ->label(trans('coupons::rules.province-white'))
        ;


        $form->add('selectize')
             ->name('province_black')
             ->options($provinces)
             ->valueField('id')
             ->captionField('title')
             ->inForm()
             ->label(trans('coupons::rules.province-black'))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->province_white = $this->para['province_white'];
        $this->province_black = $this->para['province_black'];
        $this->applyRule();
    }



    /**
     * apply rule on a user
     */
    private function applyRule()
    {

        if (!$this->isInWhitelist()) {
            $this->markAsInvalid();
        }

        if ($this->isInBlacklist()) {
            $this->markAsInvalid();
        }
    }



    /**
     * check that user is in province whitelist
     *
     * @return bool
     */
    private function isInWhitelist()
    {
        if (empty($this->province_white)) {
            return true;
        }
        return $this->userInProvince($this->province_white);
    }



    /**
     * check that user is in province blacklist
     *
     * @return bool
     */
    private function isInBlacklist()
    {
        if (empty($this->invoice->getUser()->province)) {
            return !empty($this->province_black);
        }

        return $this->userInProvince($this->province_black);
    }



    /**
     * check user province with incoming provinces
     *
     * @param string $list
     *
     * @return bool
     */
    private function userInProvince($list): bool
    {
        $province          = $this->invoice->getUser()->province;
        $accepted_province = explode(',', $list);
        return in_array($province, $accepted_province);
    }


}
