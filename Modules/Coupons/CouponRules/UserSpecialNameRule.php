<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class UserSpecialNameRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans('coupons::rules.name-rule');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add('note')
             ->shape('info')
             ->label("tr:coupons::rules.name-rule-hint");

        $form->add('text')
             ->name('name')
             ->required()
             ->validationRules("required")
             ->inForm()
             ->label(trans('coupons::rules.name'))
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if (str_contains($this->invoice->getUser()->name_first, $this->para['name'])) {
            $this->markAsValid();
        } else {
            $this->markAsInvalid();
        }
    }
}
