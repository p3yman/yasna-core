<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;

class ParticularUserRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rulebooks.for_particular");
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if(!$this->rulebook->user_id) {
            return;
        }

        if ($this->invoice->getUser()->id != $this->rulebook->user_id) {
            $this->markAsInvalid();
        }
    }
}
