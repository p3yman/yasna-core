<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class OrdersRangePriceRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans('coupons::rules.order-range-price');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add('text')
             ->name('min_price')
             ->inForm()
             ->numberFormat()
             ->purificationRules("ed|numeric")
             ->validationRules("numeric")
             ->label(trans('coupons::rules.min-price'))
        ;

        $form->add('text')
             ->name('max_price')
             ->numberFormat()
             ->inForm()
             ->purificationRules("ed|number")
             ->validationRules("numeric")
             ->label(trans('coupons::rules.max-price'))
        ;
        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->applyLowerLimits();
        $this->applyUpperLimit();
    }



    /**
     * apply rule's lower limits on each invoice item
     */
    protected function applyLowerLimits()
    {
        if (empty($this->para['min_price'])) {
            return;
        }

        foreach ($this->invoice->getItems() as $identifier => $amount) {
            if ($amount < $this->para['min_price']) {
                $this->setItemDiscount($identifier, 0);
            }
        }
    }



    /**
     * apply rule's upper limits on each invoice item
     */
    protected function applyUpperLimit()
    {
        if (empty($this->para['max_price'])) {
            return;
        }

        foreach ($this->invoice->getItems() as $identifier => $amount) {
            if ($amount > $this->para['max_price']) {
                $this->setItemDiscount($identifier, 0);
            }
        }
    }
}
