<?php

namespace Modules\Coupons\CouponRules;


use Carbon\Carbon;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Coupons\Services\InvoiceObject;
use Modules\Manage\Services\Form;

class DateLimitRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rules.date-limit");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("persian-date-picker")
             ->name("starts_at")
             ->label("tr:coupons::rules.starts_at")
             ->purificationRules("date")
             ->inForm()
        ;

        $form->add("persian-date-picker")
             ->name("ends_at")
             ->label("tr:coupons::rules.ends_at")
             ->purificationRules("date")
             ->inForm()
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if (!$this->validateLowerDate() or !$this->validateUpperDate()) {
            $this->setTotalDiscount(0);
        }
    }



    /**
     * validate lower date
     *
     * @return bool
     */
    private function validateLowerDate(): bool
    {
        if (!$this->para['starts_at']) {
            return true;
        }

        return $this->invoice->getEffectiveDate()->greaterThan($this->para['starts_at']);
    }



    /**
     * validate upper date
     *
     * @return bool
     */
    private function validateUpperDate(): bool
    {
        if (!$this->para['ends_at']) {
            return true;
        }

        return $this->invoice->getEffectiveDate()->lessThan($this->para['ends_at']);
    }
}
