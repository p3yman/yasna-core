<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class MinimumAmountRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rules.minimum-amount");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add('input')
             ->name('minimum_amount')
             ->label('tr:coupons::rules.minimum-amount')
             ->inForm()
             ->purificationRules('ed')
             ->validationRules('required|numeric|min:1')
        ;

        return $form;
    }



    /**
     * Returns the minimum discountable amount.
     *
     * @return float|string
     */
    protected function minimumAmount()
    {
        return $this->para['minimum_amount'];
    }



    /**
     * Returns the amount of the invoice object.
     *
     * @return float
     */
    protected function invoiceAmount()
    {
        return $this->invoice->getTotalAmount();
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if ($this->invoiceAmount() >= $this->minimumAmount()) {
            $this->markAsValid();
        } else {
            $this->markAsInvalid();
        }
    }
}
