<?php

namespace Modules\Coupons\CouponRules;

use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;


class CodeUsageRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::editor.usage_limit");
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $used_count  = $this->code->usages()->count();
        $usage_limit = (int)$this->rulebook->getMeta('code_usage_limit');

        if ($used_count and $usage_limit and $used_count >= $usage_limit) {
            $this->markAsInvalid();
            return;
        }

        $this->markAsValid();
    }
}
