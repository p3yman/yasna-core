<?php

namespace Modules\Coupons\CouponRules;

use Carbon\Carbon;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Manage\Services\Form;

class WeekdaysRule extends DiscountRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("coupons::rules.weekdays");
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("selectize")
             ->name("weekdays_allowed")
             ->label("tr:coupons::rules.allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("slug")
             ->captionField("title")
        ;

        $form->add("selectize")
             ->name("weekdays_not_allowed")
             ->label("tr:coupons::rules.not-allowed")
             ->inForm()
             ->options(static::getSelectizeOptions())
             ->valueField("slug")
             ->captionField("title")
        ;

        $form->add('note')
             ->shape("default")
             ->label(trans_safe("coupons::rules.weekdays-hint"))
        ;

        return $form;
    }



    /**
     * get selectize options
     *
     * @return array
     */
    protected static function getSelectizeOptions()
    {
        $array = [];

        foreach (trans("yasna::weekdays") as $key => $value) {
            $array[] = [
                 "slug"  => $key,
                 "title" => $value,
            ];

        }

        return $array;
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        if (!$this->validateAllowedDays() or !$this->validateNotAllowedDays()) {
            $this->setTotalDiscount(0);
        }
    }



    /**
     * validate allowed days
     *
     * @return bool
     */
    private function validateAllowedDays(): bool
    {
        $array = explode_not_empty(',', $this->para['weekdays_allowed']);

        return in_array($this->requestedDay(), $array);
    }



    /**
     * validate un-allowed days
     *
     * @return bool
     */
    private function validateNotAllowedDays(): bool
    {
        $array = explode_not_empty(',', $this->para['weekdays_not_allowed']);

        return not_in_array($this->requestedDay(), $array);
    }



    /**
     * get string representation of day of the week
     *
     * @return string
     */
    private function requestedDay()
    {
        $map = [
             Carbon::SATURDAY  => "sat",
             Carbon::SUNDAY    => "sun",
             Carbon::MONDAY    => "mon",
             Carbon::TUESDAY   => "tue",
             Carbon::WEDNESDAY => "wed",
             Carbon::THURSDAY  => "thu",
             Carbon::FRIDAY    => "fri",
        ];

        return $map[$this->invoice->getEffectiveDate()->dayOfWeek];
    }
}
