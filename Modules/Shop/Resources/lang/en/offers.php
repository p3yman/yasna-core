<?php
return [
     "plural_title"    => "Special Offers",
     "singular_title"  => "Special Offer",
     "create"          => "New Set",
     "items"           => "Items",
     "item_management" => "Items Management",
     "remove"          => "Remove",
     "expire_date"     => "Expire Time",
     "list_updated"    => "The list of the special offers has been updated.",
     "activate"        => "Activate",
     "deactivate"      => "Deactivate",

     "discount_percentage"       => "Discount Percentage",
     "maximum_applicable_amount" => "Maximum Applicable Amount",
     "percent_sign"              => "%",

     "single-ware-help" => "After submitting this form, this ware is only in the selected offers.",
     "single-post-help" => "After submitting this form, all wares of this products will be attached to the selected offers.",
     "no-offers-yet"    => "No special offers available!",
];
