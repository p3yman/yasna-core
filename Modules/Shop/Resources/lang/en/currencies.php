<?php
return [
     'available'    => "Available Currencies",
     'default'      => "Default Currency",
     'setting_hint' => "Separate currencies universal code with English comma (,). The first one will be assumed as default.",

     'USD' => "US Dollar",
     'IRR' => "Iranian Rial",
     'IRT' => "Iranian Toman",
     'GBP' => "British Pound",
     'EUR' => "Euro",
     'VIP' => "VIP Amount (Rials)",
     'CWR' => "Coworker Amount (Rials)",
];
