<?php
return [
    'plural'                 => "Prices",
    'singular'               => "Price",
    'discount'               => "Discount",
    'invoice'                => "Invoice Price",
    'customer_price'         => "Customer Price",
    'supply_price'           => "Supply Price",
    'main'                   => "Main Price",
    'sale'                   => "Discount Price",
    'set_discount'           => "Set Discount",
    'set_discount_expire'    => "Set Expiration Date",
    'remove_discount_expire' => "Remove Expiration Date",
    'sale_expire'            => "Auto Expiration ",

    'validation' => [
        'sale_vs_price' => "Discount price should be lower than main price.",
    ],
];
