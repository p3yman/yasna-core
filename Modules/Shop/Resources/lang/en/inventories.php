<?php
return [
     'title'                 => "Inventory",
     'initial'               => "Initial Inventory",
     'type'                  => "Control Method",
     'type_setting'          => "Inventory Control System",
     'alteration'            => "Inventory Alternation",
     'alteration_cancel'     => "Cancel Inventory Alternation",
     'alter'                 => "Alternation Amount",
     'reason'                => "Alternation Reason",
     'alter_hint'            => "Specify inventory's change amount.",
     'edit_hint'             => "Changes from sales on the site are spontaneous. You can submit unusual changes in this page. Current inventory: :amount",
     'dependency_impossible' => "No other independent product is active to link inventory control system.",
     'dependency_possible'   => "After saving the dependency, any change in referenced product inventory will also affect this product.",

     'sold_out'     => "Sale",
     'supplied_in'  => "Supply",
     'returned_in'  => "Returned from the Customer",
     'returned_out' => "Return to the Provider",
     'thrown_out'   => "Thrown Out",
     'current'      => "Current Inventory",
     'fee'          => "Fee",

     'disabled'    => "Disabled",
     'independent' => "Independent",
     'dependent'   => "Dependent to Another Product",

     'dependency' => "Reference Product",

     "enough_text"     => ":inventory :unit Available",
     "not_enough_text" => "Not Enough Inventory",
];
