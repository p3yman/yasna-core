<?php
return [
     "ware-unique-url-pattern"      => "Products URL Pattern",

     "prefix_title" => 'Product Code Prefix',
     "prefix_hint" => "This will be added to the beginning of the product code. Use English uppercase letters.",

     "shop_location" => "Shop Position",
];
