<?php
return [
     "title"           => "Product Label",
     "plural"          => "Labels",
     'empty_list'      => "No label for has been defined fo selling :name.",
     'create_first'    => "Create the first label!",
     "create_master"   => "Create New Group",
     "create_child"    => "Create New Label",
     "edit_master"     => "Edit Group",
     "edit_child"      => "Edit Label",
     "titles_required" => "Enter a title in at least one language.",
     "slug_not_unique" => "Slug is not unique.",
];
