<?php
return [
    'plural'                  => "Packaging",
    'singular'                => "Packaging",
    'create'                  => "Add New Packaging",
    'edit'                    => "Edit Packaging",
    'empty_list'              => "No package is defined for selling :name.",
    'create_first'            => "Create the first packaging.",
    'feel_at_least_one_title' => "Title is required",
    'capacity'                => "Package Capacity",
    'prices_hint'             => "Price zero means free packaging.",

    'validation' => [
        'capacity_required' => "Package capacity is required.",
        'capacity_min'      => "Package capacity should be more than zero.",
    ],
];
