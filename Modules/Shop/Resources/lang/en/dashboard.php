<?php
return [
     "ware-finder" => [
          "title"           => "Ware Finder",
          "enter_ware_code" => "Enter Ware Code",
          "find"            => "Find!",
          "name"            => "Product Name",
          "order_date"      => "Order Date",
          "payment_status"  => "Payment Status",
          "order_status"    => "Order Status",
          "action"          => "Action",
     ],

];
