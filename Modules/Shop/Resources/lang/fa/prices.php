<?php
return [
    'plural'                 => "قیمت‌ها",
    'singular'               => "قیمت",
    'discount'               => "تخفیف",
    'invoice'                => "قیمت فاکتور",
    'customer_price'         => "قیمت برای مصرف‌کننده",
    'supply_price'           => "قیمت تأمین",
    'main'                   => "قیمت اصلی",
    'sale'                   => "قیمت حراج",
    'set_discount'           => "ثبت تخفیف",
    'set_discount_expire'    => "ثبت تاریخ انقضا",
    'remove_discount_expire' => "حذف تاریخ انقضا",
    'sale_expire'            => "انقضای خودکار",

    'validation' => [
        'sale_vs_price' => "قیمت حراج باید کم‌تر از قیمت اصلی باشد.",
    ],
];
