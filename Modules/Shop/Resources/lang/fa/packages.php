<?php
return [
    'plural'                  => "بسته‌بندی‌ها",
    'singular'                => "بسته‌بندی",
    'create'                  => "بسته‌بندی جدید",
    'edit'                    => "ویرایش بسته‌بندی",
    'empty_list'              => "هیچ بسته‌ای برای فروش :name تعریف نشده است.",
    'create_first'            => "شما اولین بسته‌بندی را تعریف کنید!",
    'feel_at_least_one_title' => "تکمیل عنوان الزامی است.",
    'capacity'                => "گنجایش بسته",
    'prices_hint'             => "قیمت صفر، یعنی بسته‌بندی رایگان!",

    'validation' => [
        'capacity_required' => "گنجایش بسته‌ی انتخاب شده، حتماً باید وارد شود.",
        'capacity_min'      => "گنجایش بسته، باید بالاتر از صفر باشد.",
    ],
];
