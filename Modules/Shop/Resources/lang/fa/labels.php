<?php
return [
     "title"           => "برچسب محصولات",
     "plural"          => "برچسب‌ها",
     'empty_list'      => "هیچ برچسبی برای فروش :name تعریف نشده است.",
     'create_first'    => "شما اولین برچسب را تعریف کنید!",
     "create_master"   => "دسته جدید",
     "create_child"    => "برچسب جدید",
     "edit_master"     => "ویرایش دسته",
     "edit_child"      => "ویرایش برچسب",
     "titles_required" => "عنوان را در حداقل یکی از زبان‌ها وارد کنید.",
     "slug_not_unique" => "نامک تکراری است.",
];
