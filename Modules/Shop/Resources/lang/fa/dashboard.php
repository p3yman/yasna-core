<?php
return [
     "ware-finder" => [
          "title"           => "یابنده محصول",
          "enter_ware_code" => "کد محصول را وارد کنید",
          "find"            => "بیاب!",
          "name"            => "نام محصول",
          "order_date"      => "تاریخ سفارش",
          "payment_status"  => "وضعیت پرداخت",
          "order_status"    => "وضعیت سفارش",
          "action"          => "عملیات",


     ],

];
