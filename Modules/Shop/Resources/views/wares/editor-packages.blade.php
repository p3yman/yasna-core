@include("shop::wares.editor-code")
@include('manage::forms.sep')

{{--
|--------------------------------------------------------------------------
| Unit
|--------------------------------------------------------------------------
|
--}}
{!!
widget("combo")
	->name("unit_id")
	->options(model('unit')::all())
	->value($model->unit_id)
//	->searchable()
	->blankValue( $model->unit_id? null : "")
	->required()
	->captionField("title")
	->inForm()
!!}

{{--
|--------------------------------------------------------------------------
| Package
|--------------------------------------------------------------------------
|
--}}
{!!
widget("combo")
	->name("package_id")
	->id("cmbPackageId")
	->condition( $model->posttype->packages()->count() )
	->value( $model->package_id )
	->label("tr:shop::packages.singular")
	->options($model->posttype->packages)
	->searchable()
	->blankValue('')
	->onChange("inlineWarePackage()")
	->inForm()
!!}

{{--
|--------------------------------------------------------------------------
| Capacity
|--------------------------------------------------------------------------
|
--}}

{{
widget("input")
	->name('package_capacity')
	->id('txtPackageCapacity')
	->label("tr:shop::packages.capacity")
	->value( $model->package_capacity )
	->inForm()
	->hidden()
	->numberFormat()
	->required()
}}




{{--
|--------------------------------------------------------------------------
| Script
|--------------------------------------------------------------------------
|
--}}
<script>
	inlineWarePackage();
	function inlineWarePackage() {
		let $package = $('#cmbPackageId');
		let $capacity_container = $('#txtPackageCapacity-container');
		let $capacity_input = $("#txtPackageCapacity");

		if ($package.val()) {
			$capacity_container.slideDown('fast');
			$capacity_input.focus();
		}
		else {
			$capacity_container.slideUp('fast');
			$capacity_input.val('');
		}
	}
</script>
