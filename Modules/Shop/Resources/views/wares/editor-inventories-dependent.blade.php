{{--
|--------------------------------------------------------------------------
| Normal Situations
|--------------------------------------------------------------------------
|
--}}
{!!
widget("combo")
	->name("inventory_master_id")
	->value($model->inventory_master_id)
	->label("tr:shop::inventories.dependency")
	->options( $combo = $model->inventoryMastersCombo() )
	->required()
	->captionField("full_title")
	->searchable()
	->inForm()
!!}

{{--
|--------------------------------------------------------------------------
| Notices
|--------------------------------------------------------------------------
|
--}}
@if($combo->count())
	{!!
	widget("note")
		->color('info')
		->label("tr:shop::inventories.dependency_possible")
	!!}
@else
	{!!
	widget("note")
		->color('danger')
		->icon('exclamation-triangle')
		->label("tr:shop::inventories.dependency_impossible")
	!!}
@endif
