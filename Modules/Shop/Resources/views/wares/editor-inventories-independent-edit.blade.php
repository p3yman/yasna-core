{{--
|--------------------------------------------------------------------------
| Current Inventory
|--------------------------------------------------------------------------
|
--}}

{{
widget("input")
	->label("tr:shop::inventories.current")
	->value( pd(number_format( $model->inventory()->get() )) )
	->disabled()
	->inForm()
}}

{{--
|--------------------------------------------------------------------------
| Change Panel
|--------------------------------------------------------------------------
|
--}}
<div class="noDisplay -inventory-change">

    {!!
    widget("separator")
         ->label('tr:shop::inventories.alteration')
     !!}

    {!!
    widget("combo")
         ->name('_inv_reason')
         ->value('supplied_in')
         ->label('tr:shop::inventories.reason')
         ->options( model('inventory')::typesCombo() )
         ->required()
         ->inForm()
               !!}

    {!!
    widget('input')
        ->name('_inventory_alteration')
        ->id('txtInventoryAlteration')
        ->label("tr:shop::inventories.alter")
        ->required()
        ->numberFormat()
        ->inForm()
     !!}

    {!!
    widget("input")
        ->name('_inventory_fee')
        ->label('tr:shop::inventories.fee')
        ->numberFormat()
        ->inForm()
     !!}

    @include("shop::wares.editor-inventories-accounts")

    {!!
    widget('textarea')
        ->name('_inv_description')
        ->label('tr:validation.attributes.description')
        ->autoSize()
        ->inForm()
     !!}

</div>


{{--
|--------------------------------------------------------------------------
| Toggle Button
|--------------------------------------------------------------------------
|
--}}
@php
    widget("group")->start()->render() ;

    $toggle_js_command = "$('.-inventory-change').slideToggle('fast');$('#txtInventoryAlteration').val('').focus()" ;

    widget("button")
         ->type('button')
         ->class('-inventory-change')
         ->shape('default')
         ->label('tr:shop::inventories.alteration')
         ->target($toggle_js_command)
         ->render()
    ;

    widget('button')
         ->type('button')
         ->class('-inventory-change')
         ->hidden()
         ->shape('default')
         ->label('tr:shop::inventories.alteration_cancel')
         ->target($toggle_js_command)
         ->render()
    ;

    widget('group')->stop()->render() ;

@endphp
