{{-- Original Price --}}
@include("manage::widgets.grid-text" , [
	'text'  => number_format($ware->original_price) . SPACE . trans_safe("shop::currencies.$ware->default_currency"),
	"class" => $ware->sale_price < $ware->original_price ? "line-through" : '' ,
]     )

{{--Sale Price --}}
@php
    $sale_price  = number_format($ware->sale_price) . SPACE ;
    $sale_price .= trans_safe("shop::currencies.$ware->default_currency");
@endphp

@include("manage::widgets.grid-text" , [
	'text'      => $sale_price,
	"condition" => $ware->sale_price < $ware->original_price ,
]     )

{{-- Sale Expire Date --}}
@if($sale_expire and $ware->sale_price < $ware->original_price)
    @include("manage::widgets.grid-text" , [
         'text'      => trans("manage::forms.general.till") . SPACE . ad(echoDate($sale_expire)),
         "condition" => $sale_expire ,
    ])
@endif
