<div class="modal-footer" style="text-align: right">
	{!!
	widget("button")
		->shape('primary')
		->class('btn-taha -order-edit')
		->label("tr:shop::wares.new")
		->onClick("masterModal(url('manage/shop/wares/create/$model->hashid'))")
	!!}

	{!!
	widget("button")
		->label('tr:shop::wares.change_order')
		->class('btn-taha -order-edit')
		->onClick('orderFormToggle()')
	!!}

	{!!
	widget("button")
		->shape('success')
		->class('btn-taha -order-edit')
		->type('submit')
		->label('tr:shop::wares.save_order')
		->hidden()
	!!}

	{!!
	widget("button")
		->label('tr:shop::wares.discard_order')
		->onClick('orderFormToggle()')
		->class('btn-taha -order-edit')
		->hidden()
	!!}

	{!!
	widget("button")
		->class('btn-taha -order-edit')
		->label('tr:manage::forms.button.refresh')
		->onClick("masterModal(url('$model->ware_index_link'))")
		->condition(dev())
//		->shape('inverse')
	!!}


	{!!	widget("feed") !!}
</div>