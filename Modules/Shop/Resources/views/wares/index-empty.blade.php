{{
widget("modal")
	->label( $modal_title )
	->target("name:shop-wares-save-order")
	->render()
}}

<div class='modal-body text-center'>
	<div class="null">
		{{ trans_safe("shop::wares.no_wares_defined") }}


		<div>
			{!!
			widget("button")
				->label("tr:shop::wares.new")
				->onClick("masterModal(url('manage/shop/wares/create/$model->hashid'))")
				->class('mv10 btn-taha')
				->size('lg')
			!!}
		</div>

	</div>

</div>
