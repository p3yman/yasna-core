{{
widget("modal")
	->label( $modal_title )
	->target("name:shop-wares-save-order")
	->render()
}}

<div class='modal-body'>
	@include("shop::wares.index-body")
	@include("shop::wares.index-order")
</div>
@include("shop::wares.index-buttons")
