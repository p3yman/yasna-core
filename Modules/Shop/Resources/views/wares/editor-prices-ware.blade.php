@php
	widget("separator") // <~~ Separator
		->label("tr:shop::currencies.$currency")
		->render()

	;

	widget("input") // <~~ Main Price
		->name("_price_$currency")
		->value( $detail['original_price'] )
		->numberFormat()
		->label('tr:shop::prices.main')
		->addon( 'tr:shop::prices.set_discount')
		->addonClick( "$('#divDiscount-$currency').slideDown('fast')"  )
		->addonClass( "f8 clickable text-bold" )
		->help("tr:shop::wares.prices_hint")
		->inForm()
		->render()

	;

	widget("input") // <~~ Sale Price
		->name("_sale_$currency")
		->inForm()
		->value( $detail['on_sale'] ? $detail['sale_price'] : null )
		->numberFormat()
		->label("tr:shop::prices.sale")
		->addon("tr:shop::prices.set_discount_expire")
		->addonClick( "$('#divDiscountDate-$currency').slideDown('fast')" )
		->addonClass( "f8 clickable text-bold" )
		->containerId("divDiscount-$currency")
		->hiddenIf( !$detail['on_sale'] )
		->render()

	;

	widget("date-picker")
		->name("_discount_expires_at_$currency") // <~~ Sale Expire Date
		->id("txtDiscountDate-$currency")
		->label("tr:shop::prices.sale_expire")
		->value( $detail['sale_expire'] )
		->addon("tr:shop::prices.remove_discount_expire")
		->addonClick("$('#divDiscountDate-$currency').slideUp('fast');$('#txtDiscountDate-$currency').val('')")
		->addonClass("f8 clickable text-bold")
		->containerId("divDiscountDate-$currency")
		->hiddenIf( !$detail['sale_expire'] )
		->inForm()
		->render()
;
@endphp
