<ul class="nav nav-tabs" role="tablist">

	@foreach( $tab_list as $key => $tab_item)

		<li role="presentation" class="{{ $tab_item == $tab_default? 'active' : '' }}"  >
			<a id="aWare-{{ $key }}" href="#divWare-{{ $tab_item }}" aria-controls="divWare-{{ $tab_item }}" role="tab" data-toggle="tab" onclick="inlineWareButton( {{ $key }} , {{ $loop->last }})">

{{--				<span class="fa fa-{{ trans("shop::wares.icons.$tab_item") }} f16"></span>--}}
				<span class="mh10">
					{{ trans("shop::wares.tabs.$tab_item") }}
				</span>
			</a>
		</li>

	@endforeach

</ul>




{{--
|--------------------------------------------------------------------------
| Panels
|--------------------------------------------------------------------------
|
--}}


<div class="tab-content">
	@foreach($tab_list as $tab_item)
		<div role="tabpanel" class="tab-pane p20 {{$tab_item == $tab_default? 'active' : ''}}" id="divWare-{{ $tab_item }}">
			@include("shop::wares.editor-$tab_item")
		</div>
	@endforeach
</div>
