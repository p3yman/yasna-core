{{
//widget('separator')
//	->render()
//.
widget("button")
	->id('btnSave')
	->icon('save')
	->type('submit')
	->label("tr:manage::forms.button.save")
	->shape('success')
	->hidden( !$model->id )
	->class('btn-taha -delete-toggle')
	->value('save')
	->render()
.
widget('button')
	->id('btnDelete')
	->icon('times')
	->type('submit')
	->label('tr:manage::forms.button.sure_hard_delete')
	->value('delete')
	->shape('danger')
	->hidden()
	->class('-delete-toggle')
	->render()
.
widget("button")
	->id("btnNext")
	->type('button')
	->label("tr:shop::wares.next_tab")
	->shape('default')
	->target('$("#aWare-1").click()')
	->condition( !$model->id )
	->render()
.
widget('button')
	->type('button')
	->id('btnCancel')
	->shape('link')
	->label(trans('manage::forms.button.cancel'))
	->target( (isset($back_to_list) and $back_to_list) ? "modal:manage/shop/wares/index/$post_hashid" : '$(".modal").modal("hide")')
	->render()
.
widget('feed')
	->render()
}}


<script>
    function inlineWareButton(key, last = false)
    {
             @if(!$model->id)
        let $save_button = $('#btnSave');
        let $next_button = $('#btnNext');
        let next_key     = ( key + 1).toString();

        if (last) {
            $next_button.hide();
            $save_button.show();
        }
        else {
            $next_button.show().attr('onclick', '$("#aWare-' + next_key + '").click()')
            ;
        }
        @endif
    }
</script>
