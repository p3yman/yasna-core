@php
	widget('input')
		->name('_inventory_alteration')
		->label('tr:shop::inventories.initial')
		->numberFormat()
		->inForm()
		->render()
	;
	widget('input')
		->name('_inventory_fee')
		->label('tr:shop::prices.supply_price')
		->numberFormat()
		->inForm()
		->render()
	;

@endphp

@include("shop::wares.editor-inventories-accounts")
