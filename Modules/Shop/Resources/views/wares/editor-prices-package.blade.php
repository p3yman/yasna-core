@php
	widget("input") // <~~ Main Price
		->name("_price_$currency")
		->value( $detail['original_price'] )
		->numberFormat()
		->label("tr:shop::currencies.$currency")
		->help("tr:shop::packages.prices_hint")
		->inForm()
		->render()

	;

	widget("input") // <~~ Sale Price
		->name("_sale_$currency")
		->inForm()
		->value( 0 )
		->numberFormat()
		->label("tr:shop::prices.sale")
		->addon("tr:shop::prices.set_discount_expire")
		->addonClick( "$('#divDiscountDate-$currency').slideDown('fast')" )
		->addonClass( "f8 clickable text-bold" )
		->containerId("divDiscount-$currency")
		->hidden()
		->render()

	;

	widget("datepicker")
		->name("_discount_expires_at_$currency") // <~~ Sale Expire Date
		->id("txtDiscountDate-$currency")
		->label("tr:shop::prices.sale_expire")
		->value( null )
		->addon("tr:shop::prices.remove_discount_expire")
		->addonClick("$('#divDiscountDate-$currency').slideUp('fast');$('#txtDiscountDate-$currency').val('')")
		->addonClass("f8 clickable text-bold")
		->containerId("divDiscountDate-$currency")
		->hidden( )
		->inForm()
		->render()
;
@endphp
