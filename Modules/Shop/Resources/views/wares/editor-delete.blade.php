{{--
|--------------------------------------------------------------------------
| First Button
|--------------------------------------------------------------------------
| Just activates the second button for additional safety.
--}}

{!!
widget("button")
	->label('tr:shop::wares.delete')
	->class('btn-taha -delete-toggle')
	->onClick("$('.-delete-toggle').toggle()")
!!}


{{--
|--------------------------------------------------------------------------
| Warning
|--------------------------------------------------------------------------
| Actual button is located in editor-buttons.blade, which will be toggled herein.
--}}
{!!
widget("note")
	->hidden()
	->shape('danger')
	->label('tr:shop::wares.delete_warning')
	->class('-delete-toggle')
!!}

