@include("shop::wares.editor-code")
@include('manage::forms.sep')

{{--@include("manage::widgets.input-photo" , [--}}
	{{--'name' => "image",--}}
	{{--'value' => $model->spreadMeta()->image ,--}}
	{{--'condition' => $model->isPackage(),--}}
{{--])--}}

{!!
widget("input")
	->name('custom_code')
	->label('tr:shop::wares.custom_code')
	->value($model->custom_code)
	->inForm()
!!}

{!!
widget("selectize")
	->name('_labels')
	->inForm()
	->value( $model->selectizeValue() )
	->options($model->selectizeList())
	->valueField('slug')
	->captionField('title')
	->label("tr:shop::labels.plural")
!!}


{!!
widget("toggle")
	->name("is_available")
	->label("tr:shop::wares.availability")
	->value($model->is_available)
	->inForm()
!!}

{!!
widget("group")
	->condition($model->id)
	->blade('shop::wares.editor-delete')
	->separated()
!!}

