{!!
widget("input")
	->label("tr:shop::wares.code")
	->value( $model->code )
	->disabled()
	->inForm()
	->addon( user()->isDeveloper()? "id: " . $model->id : null )
	->addonClass('text-bold')
!!}