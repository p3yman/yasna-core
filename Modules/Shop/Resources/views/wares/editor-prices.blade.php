@include("shop::wares.editor-code")

{!!
widget("separator")
	->condition($model->isPackage())
!!}

@foreach($model->current_prices as $currency => $detail)
	@if($model->isPackage())
		@include("shop::wares.editor-prices-package")
	@else
		@include("shop::wares.editor-prices-ware")
	@endif
@endforeach

{!!
widget("hidden")
	->name('_currencies')
	->value( implode( ',' , array_keys( $model->current_prices ) ) )
!!}

