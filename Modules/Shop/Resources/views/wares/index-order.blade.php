<script>orderRun()</script>

{!!
widget("hidden")
	->name('order')
	->id('txtOrder')
!!}

{!!
widget("hidden")
	->name('hashid')
	->value($model->hashid)
!!}

{!!
widget("note")
	->hidden()
	->class('-order-edit')
	->label('tr:shop::wares.order_hint')
	->icon('check')
	->color('success')
!!}