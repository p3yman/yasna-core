{!!
	widget('modal')
		->target(route('shop.post.wares.offers.save'))
		->label($__module->getTrans('offers.singular_title'))
!!}

<div class="modal-body" style="padding-bottom: 30px">
	{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

	@php $has_any_ware = $wares->count() @endphp

	@if ($has_any_ware)
		@include($__module->getBladePath('wares.offers-selectize'), [
			'value' => $current_offers->pluck('slug')->implode(','),
			"help" => $__module->getTrans('offers.single-post-help')
		])
	@else
		{{ $__module->getTrans('wares.no_wares_defined') }}
	@endif
</div>

<div class="modal-footer">
	{!!
		widget('button')
			->label(trans('manage::forms.button.save'))
			->shape('success')
			->value('save')
			->type('submit')
			->condition($has_any_ware and boolval(count($offers_combo)))
	!!}

	{!!
		widget('button')
			->label(trans('manage::forms.button.cancel'))
			->id("btnCancel")
			->shape('link')
			->target('$(".modal").modal("hide")')
	!!}

	{!! widget('feed') !!}

</div>
