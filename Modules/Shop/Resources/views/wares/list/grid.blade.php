@include("manage::widgets.grid" , [
     'table_id' => "tblWares" ,
     'row_view' => "shop::wares.list.row" ,
     'handle'=>'selector',
     'operation_heading' => true ,
     'headings' => service('shop:ware_browse_headings')->indexed('caption' , 'width' , 'condition' ) ,
])
