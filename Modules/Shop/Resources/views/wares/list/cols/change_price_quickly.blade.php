{{
widget('form-open')
->target(route('shop-wares-save-quickly'))
->class('js')
}}

{{
	widget('hidden')
	->name('hashid')
	->value($model->hashid)
}}

{{
	widget('input')
	->name('original_price')
	->label('tr:shop::prices.main')
	->value($model->original_price)
	->numberFormat()
}}

{{
	widget('input')
	->name('sale_price')
	->label("tr:shop::prices.sale")
	->value($model->sale_price)
	->numberFormat()
}}

{!!
	widget("persian-date-picker")
	->name("discount_expires_at") // <~~ Sale Expire Date
	->label("tr:shop::prices.sale_expire")
	->withoutTime()
	->value($sale_expire)
!!}

<div>
	{{widget('feed')}}
</div>

<div class="p10">
	{!! widget('button')->id('submit')->type('submit')->label('tr:shop::wares.save_quickly')->shape('primary')!!}
	{!! widget('button')->id('close-form')->label('tr:shop::wares.close_form')->onClick($toggler)->shape('link') !!}
</div>

{{widget('form-close')}}
