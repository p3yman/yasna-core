
@forelse($model->currentPrices() as $ke=>$val)
	 @if($val['sale_expire'])
		 {{echoDate($val['sale_expire'])}}
		 @else
		      {{trans('shop::wares.empty_expire_date')}}
		 @endif

       @empty
			{{trans('shop::wares.empty_expire_date')}}
@endforelse
