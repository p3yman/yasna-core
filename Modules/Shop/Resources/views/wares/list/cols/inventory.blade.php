@php
	$posttype = $model->posttype;
@endphp


@if ($posttype->has('shop_inventory'))
	@php
		$inventory = $model->current_inventory;
	@endphp

	@if ($inventory)

		@include('manage::widgets.grid-badge', [
			'text' => $__module->getTrans('inventories.enough_text', [
					'unit' => $model->unit->title,
					'inventory' => $inventory,
				]),
			'color' => 'success',
			'icon' => 'check',
		])

	@else

		@include('manage::widgets.grid-badge', [
			'text' => $__module->getTrans('inventories.not_enough_text'),
			'color' => 'danger',
			'icon' => 'times',
		])

	@endif
@endif

{!!
	widget('form-open')
		->target(route('shop-wares-save-availability'))
		->class('js')
		->id("change-availability-$model->hashid")
!!}

{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

{!!
	widget('toggle')
		->dataOn($__module->getTrans('wares.availability_positive'))
		->dataOnStyle('success')
		->dataOff($__module->getTrans('wares.availability_negative'))
		->dataOffStyle('danger')
		->size(100)
		->value($model->is_available)
		->buttonSize('mini')
		->name('is_available')
		->label(' ')
		->containerClass("available-toggle")
!!}

{!! widget('form-close') !!}

