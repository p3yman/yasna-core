<div id="divWarePrice-{{$model->hashid}}">
	@include("shop::wares.index-price", [
          "ware"        => $model,
		"sale_expire" => $sale_expire = array_first($model->current_prices)['sale_expire'],
     ])
	{!!
     widget("button")
          ->target($toggler = "$('#divWarePrice-$model->hashid,#divWarePrice-editor-$model->hashid').slideToggle('fast')")
          ->label("tr:shop::wares.quick_price_edit")
     !!}
</div>


<div id="divWarePrice-editor-{{$model->hashid}}" class="noDisplay">
	@include('shop::wares.list.cols.change_price_quickly')
</div>
