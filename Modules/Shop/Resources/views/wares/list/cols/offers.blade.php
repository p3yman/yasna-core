@forelse($model->offers as $offer)
	@include('manage::widgets.grid-badge', [
	   'text' => $offer->title,
	   'color' => 'info',
	   'icon' => 'bullhorn'
	])
@empty
	@include('manage::widgets.grid-text', [
	   'text' => $__module->getTrans('wares.empty_offer'),
	   'color' => 'gray'
	])
@endforelse
