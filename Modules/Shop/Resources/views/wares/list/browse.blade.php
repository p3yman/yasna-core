@extends('manage::layouts.template')
@section('content')

	@if($count_of_visible_item > 1)
		@include("manage::widgets.tabs", [
		'current' => "?type=".$current_tab,
		'tabs'    => $posttypes
		])
	@endif

	@include("shop::wares.list.toolbar")
	@include("shop::wares.list.filter")
	@include("shop::wares.list.grid")

	<script>
		@if($has_filter)
        $('#divFilters').show();
		@endif

        function resetWareFilterForm() {
            location.href = url('/manage/shop/ware-list?type=' + "{{$current_tab}}");
        }

        $(document).ready(function () {
            $(document).on({
                change: function() {
                    let that = $(this);

                    that.closest('form').trigger('submit');
                }
            }, '.available-toggle input[type=checkbox]')
        });
	</script>
@stop
