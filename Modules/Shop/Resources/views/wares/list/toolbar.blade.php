@include("manage::widgets.toolbar" , [
    'title'        => trans('shop::wares.master_list'),
    "buttons"      => $controller->toolbarButtons(),
    'mass_actions' => $controller->massActionsButton(),
])
