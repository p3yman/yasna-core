@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
	'refresh_url' => url('manage/shop/ware/row/'.$model->hashid),
])


@foreach( service('shop:ware_browse_headings')->read()  as $heading)
	<td>
		@include($heading['blade'])
	</td>
@endforeach

@include("manage::widgets.grid-actionCol"  , [
	'actions' => module('Shop')->callControllerMethod('WareListController','renderRowActions',$model),
])