@include("manage::layouts.modal-mass", [
	'form_url'    => route("shop-ware-list-mass-activeness"),
	'modal_title' => trans("shop::wares.availability_change"),
	'save_label'  => trans("shop::wares.availability_mass_activate"),
	'save_shape'  => "success",
	"save_value"  => "activate",
     "extra_label" => trans("shop::wares.availability_mass_deactivate"),
     "extra_shape" => "warning",
     "extra_value" => "deactivate",
])
