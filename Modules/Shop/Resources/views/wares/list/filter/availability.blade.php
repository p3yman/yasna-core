<div class="col-md-4">
	<div class="row" id="checkboxes" style="padding-top: 40px">
		<div class="col-md-6">
			{!!
				widget('checkbox')
					->id('available')
					->name('available')
					->label('tr:shop::wares.available_wares')
					->value( (is_null($request->get('available')) )? "0" : $request->get('available'))
			!!}
		</div>
		<div class="col-md-6">
			{!!
				widget('checkbox')
					->id('unavailable')
					->name('unavailable')
					->label('tr:shop::wares.unavailable_wares')
					->value( (is_null($request->get('unavailable')) )? "0" : $request->get('unavailable'))
			!!}
		</div>
	</div>
</div>

<script>
    $('#checkboxes input[type="checkbox"]').on('change', function () {
        $('#checkboxes input[type="checkbox"]').not(this).prop('checked', false);
    });
</script>
