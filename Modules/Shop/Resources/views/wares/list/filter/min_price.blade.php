<div class="col-md-2">
	{!!
		widget('text')
			->id('price-min-input')
			->name('min_price')
			->value($request->get('min_price'))
			->numberFormat()
			->label(trans('shop::wares.price_min'))
	!!}
</div>
