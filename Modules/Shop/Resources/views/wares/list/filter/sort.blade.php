<div class="col-md-2">
	@php
		$sort_options = model('ware')->sortableFieldsCombo('key', 'caption');
		$default_sort_by = $controller->defaultSortBy();
	@endphp

	{!!
		widget('combo')
			->id('sort_by')
			->name('sort_by')
			->options($sort_options)
			->valueField('key')
			->captionField('caption')
			->blankCaption(trans('shop::wares.blank_item'))
			->value($request->get('sort_by') ?? $default_sort_by)
			->label($__module->getTrans('wares.sort_by'))
	!!}
</div>


<div class="col-md-2">
	@php
		$default_sort_direction = $controller->defaultSortBy();
	@endphp

	{!!
		widget('combo')
			->id('sort_direction')
			->name('sort_direction')
			->options([
				[
					'key' => 'asc',
					'caption' => $__module->getTrans('wares.sort_asc'),
				],
				[
					'key' => 'desc',
					'caption' => $__module->getTrans('wares.sort_desc'),
				]
			])
			->valueField('key')
			->captionField('caption')
			->blankCaption(trans('shop::wares.blank_item'))
			->value($request->get('sort_direction') ?? $default_sort_direction)
			->label($__module->getTrans('wares.sort_direction'))
	!!}
</div>
