<div class="col-md-2">
	{!!
		widget('text')
			->id('custom_code')
			->name('custom_code')
			->label($__module->getTrans('wares.custom_code'))
			->value($request->get('custom_code'))
	!!}
</div>
