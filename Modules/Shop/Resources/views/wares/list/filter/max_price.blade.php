<div class="col-md-2">
	{!!
		widget('text')
			->id('price-max-input')
			->name('max_price')
			->value($request->get('max_price'))
			->numberFormat()
			->label(trans('shop::wares.price_max'))
	!!}
</div>
