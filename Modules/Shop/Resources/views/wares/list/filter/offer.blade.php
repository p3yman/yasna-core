@php
	$offers = model('shop-offer')->validCombo('key', 'caption');
@endphp


<div class="col-md-4">
	{!!
		widget('combo')
			->id('offer')
			->name('offer')
			->options($offers)
			->searchable()
			->valueField('key')
			->captionField('caption')
			->blankValue('')
			->blankCaption(trans('shop::wares.blank_item'))
			->value($request->get('offer'))
			->label('tr:shop::offers.plural_title')
	!!}
</div>
