@php
	$posttype = posttype($current_tab);
	$products = post()
		->elector(['type' => $posttype->slug])
		->whereHas('wares')
		->get()
		->map(function ($post) {
			return [
				'key' => $post->hashid,
				'caption' => $post->title,
			];
		})->toArray()
@endphp

<div class="col-md-4">
	{!!
		widget('combo')
			->id('post')
			->name('post')
			->options($products)
			->searchable()
			->valueField('key')
			->captionField('caption')
			->blankValue('')
			->blankCaption(trans('shop::wares.blank_item'))
			->value($request->get('post'))
			->label('tr:shop::wares.product')
	!!}
</div>
