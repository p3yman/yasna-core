@php
	$categories = posttype($current_tab)
		->categories()
		->whereIsFolder(0)
		->get()
		->map(function ($category) {
			return [
				'key' => $category->hashid,
				'caption' => $category->titleIn(getLocale()),
			];
		})->toArray()
@endphp

<div class="col-md-4">
	{!!
		   widget('combo')
			 ->id('category')
			 ->name('category')
			 ->options($categories)
			 ->searchable()
			 ->valueField('key')
			 ->captionField('caption')
			 ->blankValue('')
			 ->blankCaption(trans('shop::wares.blank_item'))
			 ->value($request->get('cat'))
			 ->label('tr:shop::wares.category')
	!!}
</div>
