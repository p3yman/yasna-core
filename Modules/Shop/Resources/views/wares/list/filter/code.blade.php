<div class="col-md-2">
	{!!
		widget('text')
			->id('code')
			->name('code')
			->label($__module->getTrans('wares.code'))
			->value($request->get('code'))
	!!}
</div>
