<div id="divFilters" class="panel noDisplay">
	<div class="tforms">
		<form action="{{url('manage/shop/ware-list')}}" method="get" id="report-form" class="form-horizontal">

			{{ widget('hidden')->name('type')->value($current_tab) }}


			<div class="panel-body">
				<div class="row">
					@foreach(module('shop')->service('wares_filter')->read() as $service)
						@include($service['blade'])
					@endforeach
				</div>
			</div>

			<div class="panel-footer pv-lg">
				{!! widget('button')->id('submit')->type('submit')->label(trans('shop::wares.submit_filters'))->class('btn-primary')!!}
				{!! widget('button')->id('reset')->label(trans('shop::wares.remove_filters'))->class('btn-warning')->onClick('resetWareFilterForm()')!!}
			</div>
		</form>
	</div>
</div>
