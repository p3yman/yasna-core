@php
	$hashid = $ware->hashid;
	$modal_url = url("manage/shop/wares/edit/$hashid");
@endphp

<div class="dragbox" id="drag-{{ $hashid }}">

	<div class="w100 panel panel-default mv10 p10 reorder__container" id="panel-{{ $hashid }}">
		<div class="panel-body">

			{{-- Title --}}
			<div class="col-md-6">
				@include("shop::wares.index-title" , [
				    "with_link" => false,
				    "with_post" => false,
				])
			</div>

			{{-- Price --}}
			<div class="col-md-3">
				@include("shop::wares.index-price" , [
					"sale_expire" => array_first($ware->current_prices)['sale_expire'],
				])
			</div>

			{{-- Edit Button --}}
			<div class="col-md-3 -order-edit ">
				{!!
                     widget('button')
                     ->class('btn-default mt-sm pull-right -order-edit')
                     ->label('tr:shop::wares.edit')
                     ->onClick( "masterModal('$modal_url')" )
                 !!}
			</div>

			{{-- Sort Handle --}}
			<div class="col-md-3 -order-edit noDisplay">
				<em class="fa fa-arrows fa-fw text-muted pull-right mr-lg mv-lg reorder__handler text-success f20" style="cursor: move;"></em>
			</div>

		</div>
	</div>
</div>
