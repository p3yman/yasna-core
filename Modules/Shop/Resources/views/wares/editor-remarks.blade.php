@include("shop::wares.editor-code")
@include('manage::forms.sep')


@foreach($model->posttype->locales_array as $locale)

	{!!
	widget("textarea")
		->name("remarks[$locale]")
		->value($model->remarkIn($locale))
		->label("tr:manage::forms.lang.$locale")
		->size(2)
		->autoSize()
		->class( in_array($locale , ['fa' , 'ar']) ? 'rtl' : 'ltr' )
		->inForm()
	!!}

@endforeach
