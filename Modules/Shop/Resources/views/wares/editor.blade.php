{{
	widget("modal")
	->target( $model->isPackage()? "name:shop-packages-save" : "name:shop-wares-save")
	->validation(false)
	->label( $modal_title )
	->render()
}}


<div class='modal-body'>
	@include("shop::wares.editor-hiddens")
	@include("shop::wares.editor-tabs")
</div>

<div class="modal-footer">
	@include("shop::wares.editor-buttons")
</div>

@include('manage::layouts.modal-end')
