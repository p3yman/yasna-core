{!!
		widget('selectize')
			->inForm()
			->label($__module->getTrans('offers.plural_title'))
			->name('offers')
			->options($offers_combo)
			->valueField('slug')
			->captionField('title')
			->value($value)
			->help($help ?? null)
			->condition(count($offers_combo))
!!}

@if (empty($offers_combo))
	<div class="col-sm-12">
		<div class="alert alert-danger">
			{{ $__module->getTrans('offers.no-offers-yet') }}
		</div>
	</div>
@endif
