{{-- Ware Title--}}

@include("manage::widgets.grid-text" , [
    'text'  => $ware->post->title . " / " . $ware->title,
    'class' => "f14 font-bold font-yekan",
    "link"  => $with_link? "modal:manage/shop/wares/edit/$ware->hashid/0" : null,
])

{{-- Ware Code --}}
@include("manage::widgets.grid-tiny" , [
    'text'   => $ware->code,
    'locale' => "en",
    'icon'   => "barcode",
])

{{-- Developer Info --}}
@include("manage::widgets.grid-tiny" , [
     "condition" => dev(),
     'text'      => "$ware->id | $ware->hashid ",
     'color'     => "gray",
     'icon'      => "bug",
     'locale'    => "en",
])
