@include('manage::layouts.sidebar-link' , [
    'icon'       => "shopping-bag",
    'caption'    => trans('shop::wares.title'),
    'sub_menus'  => (new \Modules\Shop\Services\Sidebar)->get(),
])
