{!!
	widget('input')
		->inForm()
		->name('mass')
		->label($__module->getTrans('wares.mass'))
		->value($model->mass ? ad($model->mass) : '')
		->addon($__module->getTrans('units.g'))
		->numberFormat()
!!}

{!!
	widget('input')
		->inForm()
		->name('length')
		->label($__module->getTrans('wares.length'))
		->value($model->length ? ad($model->length) : '')
		->addon($__module->getTrans('units.cm'))
		->numberFormat()
!!}

{!!
	widget('input')
		->inForm()
		->name('width')
		->label($__module->getTrans('wares.width'))
		->value($model->width ? ad($model->width) : '')
		->addon($__module->getTrans('units.cm'))
		->numberFormat()
!!}

{!!
	widget('input')
		->inForm()
		->name('height')
		->label($__module->getTrans('wares.height'))
		->value($model->height ? ad($model->height) : '')
		->addon($__module->getTrans('units.cm'))
		->numberFormat()
!!}

{!!
	 widget('combo')
		 ->id('approximated_size')
		 ->name('approximated_size')
		 ->label($__module->getTrans('wares.approxi_size'))
		 ->inForm()
		 ->options(\Modules\Shipping\Services\ShipmentVolume::combo(true, getLocale()))
		 ->valueField('id')
		 ->captionField('caption')
		 ->value($model->approximated_size)
!!}
