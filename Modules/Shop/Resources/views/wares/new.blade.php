<div class="w100 panel panel-default mv10 p10 clickable" onclick="masterModal('{{ url("manage/shop/wares/create/$model->hashid") }}')">
	<div class="panel-body">


		{{--
		|--------------------------------------------------------------------------
		| Title
		|--------------------------------------------------------------------------
		|
		--}}
		<div class="col-md-9">

			@include("manage::widgets.grid-text" , [
				'text' => trans("shop::wares.new") . '...',
				'class' => "f16 font-bold font-yekan" ,
			]     )

		</div>

	</div>
</div>