{{--
|--------------------------------------------------------------------------
| Code
|--------------------------------------------------------------------------
|
--}}

@include("shop::wares.editor-code")
@include('manage::forms.sep')

{{--
|--------------------------------------------------------------------------
| Titles
|--------------------------------------------------------------------------
|
--}}

@foreach($model->posttype->locales_array as $locale)
	{!!
	widget("input")
		->name("titles[$locale]")
		->value($model->titleIn($locale))
		->label("tr:manage::forms.lang.$locale")
		->class(in_array($locale , ['fa' , 'ar']) ? 'rtl' : 'ltr')
		->inForm()
	!!}
@endforeach

{{--
|--------------------------------------------------------------------------
| Hint
|--------------------------------------------------------------------------
|
--}}
{!!
widget("note")
	->condition( $model->posttype->locales_count > 1 )
	->shape('default')
	->class("text-info")
	->label("tr:shop::wares.titles_hint")
!!}