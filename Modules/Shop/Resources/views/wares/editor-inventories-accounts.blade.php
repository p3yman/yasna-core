@php
    $accounts = model("account")->whereNotNull("published_at")->get();
@endphp

@if($accounts->count())
    {!!
    widget("combo")
         ->options($accounts)
         ->name("_inventory_account")
         ->inForm()
         ->label(trans("payment::general.account.title.singular"))
         ->blankValue(0)
         ->blankCaption(null)
    !!}
@else
    {!!
    widget("hidden")
    	->name("_inventory_account")
    	->value(0)
    !!}
@endif
