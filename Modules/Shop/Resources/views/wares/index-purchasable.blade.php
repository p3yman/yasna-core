@include("manage::widgets.grid-badge" , [
	"text" => trans("shop::wares.not_purchasable") ,
	"color" => "danger" ,
	"icon" => "times" ,
	"condition" => $ware->isNotPurchasable() ,
]     )

