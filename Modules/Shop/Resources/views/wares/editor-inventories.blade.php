@include("shop::wares.editor-code")
@include('manage::forms.sep')

{{--
|--------------------------------------------------------------------------
| Main Combo
|--------------------------------------------------------------------------
|
--}}
@php
	widget("hidden")
		->name("_inv_type")
		->id('cmbInventoryType')
		->value("independent")
		->condition( $model->isPackage() )
		->render()
	;

	widget('combo')
		->name("_inv_type")
		->id("cmbInventoryType")
		->label("tr:shop::inventories.type")
		->value( $model->inventory_type )
		->options( $model->inventoryTypeCombo() )
		->required()
		->inForm()
		->onChange("inlineWareInventory()")
		->condition( $model->isNotPackage() )
		->render()
	;

@endphp

{{--
|--------------------------------------------------------------------------
| Dependent Mode
|--------------------------------------------------------------------------
|
--}}
@if($model->isNotPackage())
	<div id="divInventory-dependent" class="noDisplay">
		@include("shop::wares.editor-inventories-dependent")
	</div>
@endif


{{--
|--------------------------------------------------------------------------
| Independent
|--------------------------------------------------------------------------
|
--}}
<div id="divInventory-independent" class="noDisplay">
	@if($model->id)
		@include('shop::wares.editor-inventories-independent-edit')
	@else
		@include('shop::wares.editor-inventories-independent-new')
	@endif
</div>


{{--
|--------------------------------------------------------------------------
| Script
|--------------------------------------------------------------------------
|
--}}
<script>
   inlineWareInventory();
   function inlineWareInventory() {
      let $combo       = $('#cmbInventoryType');
      let $dependent   = $('#divInventory-dependent');
      let $independent = $('#divInventory-independent');

      switch ($combo.val()) {
         case "dependent" :
            $independent.hide();
            $dependent.slideDown('fast');
            break;
         case 'independent' :
            $dependent.hide();
            $independent.slideDown('fast');
            break;
         default:
            $independent.hide();
            $dependent.hide();
      }
   }
</script>
