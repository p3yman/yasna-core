{!!
	widget('modal')
		->target(route('shop.wares.offers.save'))
		->label($__module->getTrans('offers.singular_title'))
!!}

<div class="modal-body" style="padding-bottom: 30px">
	{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

	@include($__module->getBladePath('wares.offers-selectize'), [
		'value' => $model->offers()
						->get(['slug'])
						->pluck('slug')
						->implode(','),
		"help" => $__module->getTrans('offers.single-ware-help')
	])
</div>

<div class="modal-footer">
	{!!
		widget('button')
			->label(trans('manage::forms.button.save'))
			->shape('success')
			->value('save')
			->type('submit')
			->condition(boolval(count($offers_combo)))
	!!}

	{!!
		widget('button')
			->label(trans('manage::forms.button.cancel'))
			->id("btnCancel")
			->shape('link')
			->target('$(".modal").modal("hide")')
	!!}

	{!! widget('feed') !!}
</div>
