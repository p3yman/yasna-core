@extends('manage::layouts.template')

@section('content')
    @include("shop::inventories.browse.toolbar")
    @include("shop::inventories.browse.filters")
    @include("shop::inventories.browse.grid")
@endsection
