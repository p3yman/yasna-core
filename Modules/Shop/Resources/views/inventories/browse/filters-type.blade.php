{!!
widget("combo")
     ->name('reason')
     ->value($filters['reason'])
     ->label('tr:shop::inventories.reason')
     ->options( model('inventory')::typesCombo() )
    	->blankValue("")
!!}
