@include("manage::widgets.grid-text" , [
    "color" => $model->isInlet()? "green" : "orange",
    "text" => number_format($model->alteration) . SPACE . $model->ware->unit->title,
    "size" => 14,
    "icon" => $model->isInlet()? "level-up": "level-down",
])


@include("manage::widgets.grid-text" , [
    "color" => $model->isInlet()? "green" : "orange",
    "text" => "(".$model->typeText().")",
    "size" => 10,
])

{{-- Developer Info --}}
@include("manage::widgets.grid-tiny" , [
     "condition" => dev(),
     'text'      => "$model->id | $model->hashid ",
     'color'     => "gray",
     'icon'      => "bug",
     'locale'    => "en",
])

@include("manage::widgets.grid-date" , [
    "date" => $model->effected_at,
])
