@include('manage::widgets.grid-row-handle', [
//    'refresh_url' => $url,
//    'handle'      => $handle,
])

<td>
    @include("shop::wares.index-title" , [
        "ware" => $model->ware,
        "with_post" => true,
        "with_link" => true,
    ])
</td>

<td>
    @include("shop::inventories.browse.row-alter")
</td>

<td>
    @include("shop::inventories.browse.row-account")
</td>
