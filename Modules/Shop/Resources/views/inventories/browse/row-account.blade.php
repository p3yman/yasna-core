@include("manage::widgets.grid-text" , [
    "text" => number_format($model->fee * $model->alteration),
    "size" => 14,
])

@include("manage::widgets.grid-text" , [
    "text" => trans("shop::inventories.fee") . ":" . SPACE . number_format($model->fee),
    "size" => 10,
])

@if($model->account)
    @include("manage::widgets.grid-text" , [
        "text" => $model->account->title,
        "size" => 10,
        "icon" => "bank",
    ])
@endif
