@include("manage::widgets.grid" , [
    'headings'          => [
        trans("shop::wares.product"),
        trans("shop::inventories.alter"),
        trans("shop::inventories.price")
    ],
    'row_view'          => "shop::inventories.browse.row",
    'table_id'          => "tblInventories",
    'handle'            => "counter",
    'operation_heading' => false,
])
