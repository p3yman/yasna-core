{!!
widget("form-open")
	->method("get")
!!}

<div id="divFilters" class="panel panel-default {{ $is_filtered? "" : "noDisplay" }}">
    <div class="panel-heading">
        <h3 class="panel-title">{{ trans("shop::wares.filters") }}</h3>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-sm-4"> @include("shop::inventories.browse.filters-type") </div>
            <div class="col-sm-8"> @include("shop::inventories.browse.filters-ware") </div>
        </div>

        {{--<div class="row">-- TODO: To be used later for filtering against dates }}
            {{--<div class="col-sm-6"></div>--}}
            {{--<div class="col-sm-6"></div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-sm-12">
                @include("shop::inventories.browse.filters-button")
            </div>
        </div>
    </div>
</div>

{!!widget("form-close") !!}

