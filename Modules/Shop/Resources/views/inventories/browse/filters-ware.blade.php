{!!
widget("combo")
	->name("ware")
	->value($filters['ware'])
	->label("tr:shop::wares.product")
	->options( ware()->allInventoryMastersCombo() )
	->captionField("full_title")
	->valueField('hashid')
	->searchable()
	->blankValue("")
!!}
