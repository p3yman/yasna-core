@include("manage::widgets.toolbar" , [
    'title' => trans('shop::inventories.log') . ($is_filtered? SPACE . trans("shop::inventories.filtered") : '' ),
    "buttons" => [
        [
            'target'  => "$('#divFilters').slideToggle('fast')",
	       'type'    => "default btn-taha",
	       'caption' => trans("shop::wares.filters"),
	       'icon'    => "filter",
        ],
    ],
])
