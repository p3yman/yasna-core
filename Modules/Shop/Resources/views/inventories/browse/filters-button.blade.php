{!!
widget("button")
	->type("submit")
	->label("tr:manage::forms.button.find")
	->shape("primary mv5")
	->value(1)
!!}

{!!
widget("button")
	->type("submit")
	->condition($is_filtered)
	->label("tr:shop::inventories.remove_filters")
	->shape("link mv5")
	->value(0)
!!}
