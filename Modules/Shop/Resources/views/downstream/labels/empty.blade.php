<div class="text-center">
    <div class="f20 text-ultralight mv30 text-info">
        {{ trans("shop::labels.empty_list" , [
             'name' => $posttype->title ,
        ]) }}
    </div>
    <a class="f15 text-ultralight" href="{{v0()}}" onclick="masterModal('{{url("manage/shop/downstream/labels/create-master/$posttype->hashid")}}')">
        {{ trans("shop::labels.create_first") }}
    </a>
</div>