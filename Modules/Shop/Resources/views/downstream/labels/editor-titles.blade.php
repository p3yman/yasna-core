@foreach($posttype->localesArray() as $locale)
    {!!
    widget("input")
    	->name($locale)
    	->label("tr:yasna::lang.$locale")
    	->value($model->titleIn($locale))
    	->inForm()
    !!}
@endforeach