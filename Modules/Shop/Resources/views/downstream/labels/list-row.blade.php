@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => null
])

{{--
|--------------------------------------------------------------------------
| Title
|--------------------------------------------------------------------------
|
--}}
<td>
    @include("manage::widgets.grid-text" , [
         'text' => $model->title,
         'class' => "font-yekan text-bold" ,
         'link' => "modal:manage/shop/downstream/labels/edit/-hashid-" ,
    ])
</td>

{{--
|--------------------------------------------------------------------------
| Labels
|--------------------------------------------------------------------------
|
--}}

<td>
    @include("shop::downstream.labels.list-children")
</td>

<td>
    @include("shop::downstream.labels.list-child-add")
</td>