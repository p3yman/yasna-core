{!!
widget("modal")
	->target('name:downstream-label-save')
	->labelIf($model->isNotSet() and !$model->parent_id , trans('shop::labels.create_master'))
	->labelIf($model->isNotSet() and $model->parent_id , trans('shop::labels.create_child'))
	->labelIf($model->isset() and !$model->parent_id , trans('shop::labels.edit_master'))
	->labelIf($model->isset() and $model->parent_id , trans('shop::labels.edit_child'))
	->validation(false)
!!}

<div class="modal-body">
    @include("shop::downstream.labels.editor-hiddens")
    @include("shop::downstream.labels.editor-slug")
    @include("shop::downstream.labels.editor-titles")
</div>
<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
