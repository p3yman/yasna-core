{!!
widget("input")
	->name('slug')
	->value(str_after($model->slug , "|"))
	->required()
	->inForm()
!!}