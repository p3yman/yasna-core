<div class="col-md-{{ $models->count() > 1 ? "6" : "12" }}">
    <div class="panel panel-default" >
        <div class="panel-heading">
            <i class="fa fa-{{$model->icon}} f18"></i>
            <span class="font-yekan f18 mh10">
							{{ $model->title }}
						</span>

            <span class="pull-right">
							<button type="button" class="btn btn-default btn-sm" onclick="masterModal('{{url("manage/shop/downstream/labels/create-master/$model->hashid")}}')">
								<b class="fa fa-plus text-gray"></b>
							</button>
							<button type="button" class="btn btn-default btn-sm" onclick="divReload('divLabel-{{$model->hashid}}')">
								<b class="fa fa-refresh text-gray ph0"></b>
							</button>
						</span>
        </div>

        <div id="divLabel-{{$model->hashid}}" class="panel-body" style="height: 200px;overflow: scroll" data-src="manage/shop/downstream/labels/list/{{$model->hashid}}">
            <i class="fa fa-spin fa-cog f20 text-gray"></i>
            <script>divReload("divLabel-{{$model->hashid}}")</script>
        </div>

    </div>
</div>
