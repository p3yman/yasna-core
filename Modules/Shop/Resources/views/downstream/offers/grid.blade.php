@include("manage::widgets.grid" , [
    'headings'          => [
        trans("validation.attributes.title"),
        trans("validation.attributes.status"),
        trans("shop::offers.expire_date"),
        trans("shop::offers.items"),
    ],
    'row_view'          => "shop::downstream.offers.row",
    'table_id'          => "tblOffers",
    'handle'            => "counter",
    'operation_heading' => true,
])
