@include("manage::widgets.grid-actionCol" , [
"actions" => [
    ["pencil", trans("manage::forms.button.edit")       , $model->getActionLink('edit')    , $model->canEdit()   ],
    ["cubes",  trans("shop::offers.items")              , $model->getActionLink('items')   , $model->canView()   ],
    ["trash",  trans("shop::offers.deactivate")         , $model->getActionLink("delete")  , $model->canDelete() ],
    ["undo",   trans("shop::offers.activate")           , $model->getActionLink("undelete"), $model->canBin()    ],
    ["times",  trans("manage::forms.button.hard_delete"), $model->getActionLink("destroy") , $model->canBin()    ],
],
])
