@include("manage::widgets.grid-text", [
    "text"  => $model->isTrashed()? trans("manage::forms.status_text.inactive") : trans("manage::forms.status_text.active"),
    "color" => $model->isTrashed()? "red" : "green",
    "link"  => $model->getToggleLink()
])
