@include("manage::layouts.modal-undelete", [
    "form_url" => route("shop-downstream-offer-undelete"),
    "modal_title" => trans("shop::offers.activate"),
    "save_label"  => trans("shop::offers.activate"),
])
