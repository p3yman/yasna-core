@include("manage::widgets.toolbar" , [
    "title" => trans('shop::offers.plural_title'),
    "buttons" => [
        [
            "target" => model("shop-offer")->getCreateLink(),
            "caption" => trans("shop::offers.create"),
            "icon" => "plus-circle"
        ],
    ],
])
