@include("manage::layouts.modal-delete", [
    "form_url"    => route("shop-downstream-offer-delete"),
    "modal_title" => trans("shop::offers.deactivate"),
    "save_label"  => trans("shop::offers.deactivate"),
])
