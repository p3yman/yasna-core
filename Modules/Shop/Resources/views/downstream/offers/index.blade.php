@extends('manage::layouts.template')

@section('content')
    @include("manage::downstream.tabs")

    @include("shop::downstream.offers.toolbar")
    @include("shop::downstream.offers.grid")

@endsection
