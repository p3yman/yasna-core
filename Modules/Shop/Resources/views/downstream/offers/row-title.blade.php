@include("manage::widgets.grid-text" , [
    "text" => $model->title,
    "size" => "14",
    "link" => $model->canEdit()? $model->getActionLink('edit') : null,
])
