@include('manage::widgets.grid-row-handle', [
    'refresh_url' => $model->getRefreshLink(),
    'handle'      => "counter",
])

<td>
    @include("shop::downstream.offers.row-title")
</td>

<td>
    @include("shop::downstream.offers.row-status")
</td>

<td>
    @include("shop::downstream.offers.row-expires")
</td>


<td>
    @include("shop::downstream.offers.row-items")
</td>

@include("shop::downstream.offers.row-actions")

