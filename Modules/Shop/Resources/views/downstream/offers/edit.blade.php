{!!
widget("modal")
	->target($model->getSaveLink())
	->label($model->exists? trans("manage::forms.button.edit_info") : trans("shop::offers.create"))
!!}

<div class="modal-body">
    {!!
    widget("hidden")
    	->name("hashid")
    	->value($model->hashid)
    !!}

    {!!
    widget("input")
    	->name("title")
    	->value($model->title)
    	->inForm()
    	->required()
    !!}

    {!!
    widget("input")
         ->name("slug")
         ->value($model->slug)
         ->inForm()
         ->condition(dev())
    !!}

    {!!
    widget("persian-date-picker")
    	->name("expires_at")
    	->value((string)$model->expires_at)
    	->label("tr:shop::offers.expire_date")
    	->inForm()
    !!}

    {!!
	widget("input")
		->name("discount_percentage")
		->value($model->discount_percentage)
		->label("tr:shop::offers.discount_percentage")
		->numberFormat()
		->max(100)
		->min(0)
        ->addon("tr:shop::offers.percent_sign")
		->inForm()
	!!}

    {!!
	widget("input")
		->name("maximum_applicable_amount")
		->value($model->maximum_applicable_amount)
		->label("tr:shop::offers.maximum_applicable_amount")
		->inForm()
		->numberFormat()
	!!}
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
