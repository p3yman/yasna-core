{!!
widget("button")
	->id("btnDelete-$model->hashid")
	->label("tr:shop::offers.remove")
	->target("$('.-ware-delete').slideUp('fast');$('#btnDeleteAction-$model->hashid').slideDown('fast')")
!!}

{!!
widget("button")
	->id("btnDeleteAction-$model->hashid")
	->hidden()
     ->label("tr:manage::forms.button.sure_delete")
     ->shape('danger')
     ->type('submit')
     ->value("delete-$model->hashid")
     ->class("-ware-delete")
!!}
