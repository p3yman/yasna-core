{!!
widget("modal")
	->label($modal_label)
	->target(route("shop-downstream-offer-items"))
!!}

<div class="modal-body">
    @include("shop::downstream.offers.wares.grid")
</div>

<div class="modal-footer">
    {!! widget("hidden")->name("id")->value($offer->hashid) !!}
    {!!widget("feed") !!}
</div>
