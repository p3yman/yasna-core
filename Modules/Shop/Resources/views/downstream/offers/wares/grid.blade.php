@include("manage::widgets.grid" , [
    'headings'          => [
        trans("validation.attributes.title"),
        trans("shop::wares.code"),
        trans("shop::offers.expire_date"),
        '',
    ],
    'row_view'          => "shop::downstream.offers.wares.row",
    'table_id'          => "tblOfferWares",
    'handle'            => "counter",
    'operation_heading' => false,
])
