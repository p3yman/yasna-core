@include('manage::widgets.grid-row-handle')

<td>
    @include("manage::widgets.grid-text" , [
        'text' => $model->title,
        'class' => "f16 font-bold font-yekan" ,
    ]     )
</td>

<td>
    @include("manage::widgets.grid-text" , [
        'text' => "$model->code",
        'locale' => "en" ,
    ]     )
</td>

<td>
    @include("manage::widgets.grid-date" , [
        "color" => $model->isOfferExpired()? "red" : "green",
        "date" => $model->offer_expires_at,
        "size" => "11",
        "dash_if_empty" => true,
    ])
</td>

<td>
    @include("shop::downstream.offers.wares.delete")
</td>
