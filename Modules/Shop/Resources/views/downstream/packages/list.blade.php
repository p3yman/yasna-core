@include("manage::widgets.grid" , [
	'table_id' => "tblPackages-{{$posttype->hashid}}",
	'row_view' => "shop::downstream.packages.list-row",
	'handle' => "counter",
	'headings' => [
		trans('validation.attributes.title') ,
		trans("validation.attributes.price") ,
		$posttype->has('shop_inventory') ? trans("validation.attributes.inventory") : "NO"
	],
])