@include('manage::layouts.modal-start' , [
	'form_url' => route("package-downstream-save"),
	'modal_title' => trans($model->id? "shop::packages.edit" : "shop::packages.create"),
])
<div class='modal-body'>

	{{--
	|--------------------------------------------------------------------------
	| Hidden Fields
	|--------------------------------------------------------------------------
	|
	--}}


	@include('manage::forms.hiddens' , ['fields' => [
		['hashid' , $model->hashid],
		['_posttype_hashid' , hashid($model->posttype_id)]
	]])


	{{--
	|--------------------------------------------------------------------------
	| Titles
	|--------------------------------------------------------------------------
	|
	--}}
	@foreach($model->posttype->locales_array as $locale)
		@include('manage::forms.input' , [
			'name' => "_title_in_$locale",
			'value' => $model->titleIn($locale) ,
			'label' => trans("validation.attributes.title") . SPACE .  trans("manage::forms.lang.$locale") ,
			'class' =>  in_array($locale , ['fa' , 'ar']) ? 'rtl' : 'ltr' ,
		])

	@endforeach

	{{--
	|--------------------------------------------------------------------------
	| Prices
	|--------------------------------------------------------------------------
	|
	--}}
	@if(!$model->id)

		@include('manage::forms.sep' , [
			'label' => trans("shop::prices.customer_price"),
		]      )

		@foreach($model->posttype->currencies_array as $currency)
			@include('manage::forms.input' , [
				'name' => "_price_$currency",
				'value' => $model->price('original')->in($currency)->get() ,
				'class' => "form-numberFormat" ,
				'label' => trans("shop::currencies.$currency") ,
			])
		@endforeach

	@endif

	{{--
	|--------------------------------------------------------------------------
	| Inventory
	|--------------------------------------------------------------------------
	|
	--}}
	@if($model->posttype->has('shop_inventory') and !$model->id)

		@include('manage::forms.sep' , [
			'label' => trans("shop::inventories.type_setting"),
		]      )


		@include('manage::forms.input' , [
			'name' => "current_inventory",
			'value' => '' ,
			'label' => trans("shop::inventories.initial") ,
			'class' => "form-numberFormat" ,
		]      )

		@include('manage::forms.input' , [
			'name' => "_price_inventory",
			'value' => "" ,
			'label' => trans("shop::prices.supply_price") ,
			'class' => "form-numberFormat" ,
		]      )


	@endif


	{{--
	|--------------------------------------------------------------------------
	| Footer
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.buttons-for-modal')

</div>
@include('manage::layouts.modal-end')