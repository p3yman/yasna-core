<div class="text-center">
	<div class="f20 text-ultralight mv30 text-info">
		{{ trans("shop::packages.empty_list" , [
			'name' => $posttype->title ,
		]) }}
	</div>
	<a class="f15 text-ultralight" href="{{v0()}}" onclick="masterModal('{{url("manage/shop/packages/create/$posttype->hashid")}}')">
		{{ trans("shop::packages.create_first") }}
	</a>
</div>