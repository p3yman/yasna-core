@include('manage::layouts.modal-start' , [
	'form_url' => route("package-downstream-save-price"),
	'modal_title' => $model->posttype->title . " / " . $model->title ,
])
<div class='modal-body'>

	@include('manage::forms.hiddens' , ['fields' => [
		['hashid' , $model->hashid],
	]])
	
	@foreach($model->posttype->currencies_array as $currency)
		@php
			$defalult = boolval($loop->first) ? 'form-default' : '';
		@endphp
		@include('manage::forms.input' , [
			'name' => "_price_$currency",
			'value' => $model->originalPrice()->in($currency)->get() ,
			'class' => "form-numberFormat $defalult" ,
			'label' => trans("shop::currencies.$currency") ,
		])
	@endforeach

	@include('manage::forms.buttons-for-modal')

</div>
@include('manage::layouts.modal-end')
