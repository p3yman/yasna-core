@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => null
])


{{--
|--------------------------------------------------------------------------
| Title
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'class' => "font-yekan text-bold" ,
		'link' => "modal:manage/shop/packages/edit/-hashid-" ,
	])
</td>


{{--
|--------------------------------------------------------------------------
| Price
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => number_format($model->original_price),
	])
</td>

{{--
|--------------------------------------------------------------------------
| Inventory
|--------------------------------------------------------------------------
|
--}}
@if($posttype->has('shop_inventory'))
	<td>
		@include("manage::widgets.grid-text" , [
			'text' => number_format($model->current_inventory),
		])
	</td>
@endif