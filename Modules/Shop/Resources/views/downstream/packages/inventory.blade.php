@include('manage::layouts.modal-start' , [
	'form_url' => route("package-downstream-save-inventory"),
	'modal_title' => $model->posttype->title . " / " . $model->title ,
])
<div class='modal-body'>

	@include('manage::forms.hiddens' , ['fields' => [
		['hashid' , $model->hashid],
	]])

	{{--
	|--------------------------------------------------------------------------
	| Quantity
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.input' , [
		'name' => "alteration",
		'value' => null ,
		'label' => trans("shop::inventories.alteration") ,
		'class' => "form-required form-default form-numberFormat" ,
	])

	@include('manage::forms.select' , [
		'name' => "type",
		'value' => "supplied_in" ,
		'options' => model('inventory')::typesCombo() ,
		'class' => "form-required" ,
	]      )
	
	@include('manage::forms.input' , [
		'name' => "fee",
		'class' => "form-numberFormat" ,
	]      )

	@include('manage::forms.textarea' , [
		'name' => "description",
		'class' => "form-autoSize" ,
	]      )

	


	@include('manage::forms.buttons-for-modal')

</div>
@include('manage::layouts.modal-end')