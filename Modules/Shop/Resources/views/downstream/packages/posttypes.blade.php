@extends('manage::layouts.template')

@section('content')
	@include("manage::downstream.tabs")
	@include("manage::widgets.toolbar")

	<div class="row">
		@php
			$printed = 0;
		@endphp
		@foreach($models as $model)
			@if($model->can('setting'))
				@include("shop::downstream.packages.posttypes-one")
				@php
					$printed++;
				@endphp
			@endif
		@endforeach
	</div>

	@if(!$printed)
		<div class="null">
			{{ trans('manage::forms.feed.nothing') }}
		</div>
	@endif

@endsection
