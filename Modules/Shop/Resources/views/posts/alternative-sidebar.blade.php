@if(!get_setting('fold_posts_on_manage_sidebar'))
    @include("posts::layouts.sidebar_folded", [
       "groups" => (new \Modules\Shop\Services\Sidebar())->getFoldedPosttypesSidebar(),
    ])
@else
    @include("posts::layouts.sidebar_unfolded", [
       "posttypes" => (new \Modules\Shop\Services\Sidebar())->getFlatPosttypesSidebar(),
    ])
@endif
