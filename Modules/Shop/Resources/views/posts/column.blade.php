{!!
widget("button")
	->label($__module->getTrans('wares.list') . ' (' . ad($model->total_wares) . ')')
	->target("modal:$model->ware_index_link")
!!}
