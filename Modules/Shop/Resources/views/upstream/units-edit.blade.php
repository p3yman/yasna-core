@include('manage::layouts.modal-start' , [
	'form_url' => route("unit-save"),
	'modal_title' => trans( $model->id? "shop::units.edit" : "shop::units.create"),
])
<div class='modal-body'>

	@include('manage::forms.hiddens' , ['fields' => [
		['hashid' , $model->hashid],
	]])

	@include('manage::forms.input' , [
		'name' => "slug",
		'value' => $model->slug ,
		'class' => "form-required form-default" ,
		'hint' => trans("shop::units.title_hint") ,
	]      )

	@include('manage::forms.select' , [
		'name' => "concept",
		'value' => $model->concept ,
		'options' => $model->conceptsCombo() ,
		'caption_field' => "concept_title_fa" ,
		'value_field' => "concept" ,
		'class' => "form-required" ,
	]      )

	@include('manage::forms.input' , [
		'name' => "ratio",
		'value' => $model->ratio ,
		'class' => "form-required" ,
		'hint' => trans("shop::units.ratio_hint") ,
	]      )


	@include('manage::forms.buttons-for-modal')

</div>
@include('manage::layouts.modal-end')