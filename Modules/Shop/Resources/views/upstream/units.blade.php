@extends('manage::layouts.template')

@section('content')
	@include("manage::upstream.tabs")

	@include("manage::widgets.toolbar" , [
	'buttons' => [
		[
			'target' => "modal:manage/shop/upstream/unit/create/0",
			'type' => "success",
			'caption' => trans('manage::forms.button.add'),
			'icon' => "plus-circle",
		],
	],
])

	@include("manage::widgets.grid" , [
		'table_id' => "tblUnits",
		'row_view' => "shop::upstream.units-row",
		'handle' => "counter",
		'headings' => [
			trans('validation.attributes.title'),
			'',
			trans("validation.attributes.concept"),
			trans("validation.attributes.equivalent") ,
			trans('manage::forms.button.action'),
		],
	])

@endsection