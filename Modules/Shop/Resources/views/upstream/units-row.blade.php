@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => "manage/shop/upstream/unit/row/$model->hashid"
])

{{--
|--------------------------------------------------------------------------
| Title
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'size' => "16" ,
		'class' => "font-yekan text-bold" ,
		'link' =>  "modal:manage/shop/upstream/unit/edit/-hashid-" ,
	])
	@include("manage::widgets.grid-tiny" , [
		'text' => "$model->id|$model->slug" ,
		'color' => "gray" ,
		'icon' => "bug",
		'class' => "font-tahoma" ,
		'locale' => "en" ,
	]     )
</td>


{{--
|--------------------------------------------------------------------------
| Status
|--------------------------------------------------------------------------
|
--}}
<td>
	<div class="mv10">
		@include("manage::widgets.grid-badge" , [
			'condition' => $model->trashed() ,
			'icon' => "times",
			'text' => trans("manage::forms.status_text.deleted"),
			'link' => "modal:manage/shop/upstream/unit/activeness/-hashid-" ,
			'color' => 'danger',
		])
	</div>
</td>


{{--
|--------------------------------------------------------------------------
| Concept
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->conceptTitle() ,
	])
</td>


{{--
|--------------------------------------------------------------------------
| Equivalent
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->ratio == 1 ? "-" : $model->equivalentFull() ,
	])
</td>

{{--
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['pencil' , trans('manage::forms.button.edit') , "modal:manage/shop/upstream/unit/edit/-hashid-" ],
		['trash-o' , trans('manage::forms.button.soft_delete') , "modal:manage/shop/upstream/unit/activeness/-hashid-" , !$model->trashed() and $model->slug != 'root'] ,
		['recycle' , trans('manage::forms.button.undelete') , "modal:manage/shop/upstream/unit/activeness/-hashid-" , $model->trashed()],
	]
])
