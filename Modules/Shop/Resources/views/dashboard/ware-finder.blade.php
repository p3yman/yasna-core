{!!
	widget('form-open')
		->target(route('search-ware'))
		->method('get')
		->class('js')
!!}

{!!
    widget('input')
    ->label('tr:shop::dashboard.ware-finder.enter_ware_code')
   	->name('identifier')
 !!}

{!!
    widget('button')
    ->label('tr:shop::dashboard.ware-finder.find')
    ->shape('primary')
    ->class('mt btn-taha')
    ->type('submit')
 !!}

{!!
	widget('feed')
!!}

{!!
	widget('form-close')
!!}
