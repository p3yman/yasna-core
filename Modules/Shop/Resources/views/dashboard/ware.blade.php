<hr>
<div class="row mt-xl">
	<fieldset>

		<div class="col-sm-3 col-xs-5 text-bold">
			{{ trans('shop::dashboard.ware-finder.name').":" }}
		</div>
		<div class="col-sm-9">
			{{ $name or "محصول قشنگ" }}
		</div>
	</fieldset>

	<fieldset>
		<div class="col-sm-3 col-xs-5 text-bold">
			{{ trans('shop::dashboard.ware-finder.order_date').":" }}
		</div>
		<div class="col-sm-9 col-xs-7">
			{{ $date or "۲۰ مهر ۱۳۹۷" }}
		</div>
	</fieldset>

	<fieldset>
		<div class="col-sm-3 col-xs-5 text-bold">
			{{ trans('shop::dashboard.ware-finder.payment_status').":" }}
		</div>
		<div class="col-sm-9 col-xs-7">
			@include('manage::widgets.grid-badge',[
				"icon" => false ,
				"color" => "warning" ,
				"text" => "در حال پردازش" ,
			])
		</div>
	</fieldset>

	<fieldset>
		<div class="col-sm-3 col-xs-5 text-bold">
			{{ trans('shop::dashboard.ware-finder.order_status').":" }}
		</div>
		<div class="col-sm-9 col-xs-7">
			@include('manage::widgets.grid-badge',[
				"icon" => false ,
				"color" => "success" ,
				"text" => "پرداخت شده" ,
			])
		</div>
	</fieldset>

	<fieldset>
		<div class="col-sm-3 col-xs-5 text-bold">
			{{ trans('shop::dashboard.ware-finder.action').":" }}
		</div>
		<div class="col-sm-9 col-xs-7">
			<button class="btn btn-default btn-taha">
				{{ trans('shop::dashboard.ware-finder.action') }}
			</button>
		</div>
	</fieldset>
</div>