<?php

namespace Modules\Shop\Services;

/**
 * control and manage the array, required to produce shops sidebar
 */
class Sidebar
{
    /**
     * get the sidebar items
     *
     * @return array
     */
    public function get(): array
    {
        $statics = [
             $this->getAllItemsLink(),
             $this->getInventoryLogLink(),
        ];

        if (!empty($this->getSidebarServices())) {
            $statics = array_merge($statics, $this->getSidebarServices());
        }

        return array_merge(
             $this->getPostItems(),
             $statics
        );
    }



    /**
     * get the posttypes sidebar, flat mode
     *
     * @param bool $header_title
     *
     * @return array
     */
    public function getFlatPosttypesSidebar($header_title = false)
    {
        $collection = posttypes()->having("manage_sidebar")->notHaving("shop");

        if ($header_title) {
            $collection = $collection->where("header_title", $header_title);
        }

        return $collection->sortBy('order')->get();
    }



    /**
     * get the posttypes sidebar, folded mode
     *
     * @return array
     */
    public function getFoldedPosttypesSidebar()
    {
        return posttypes()
             ->having("manage_sidebar")
             ->notHaving("shop")
             ->groupBy('header_title')
             ->sortBy('order')
             ->get()
             ;
    }



    /**
     * get all shop sidebar
     *
     * @return array
     */
    protected function getSidebarServices()
    {
        $arr = [];
        foreach (service('shop:shop_sidebar')->read() as $service) {
            $arr[] = $service;
        }
        return $arr;
    }



    /**
     * get the `all-items` sidebar link
     *
     * @return array
     */
    private function getAllItemsLink()
    {
        return [
             'link'    => "shop/ware-list",
             'caption' => trans('shop::wares.master_list'),
        ];
    }



    /**
     * get the `inventory-log` sidebar link
     *
     * @return array
     */
    private function getInventoryLogLink()
    {
        return [
             'link'      => "shop/inventories-log",
             'caption'   => trans('shop::inventories.log'),
             "condition" => posttypes()->having("shop_inventory")->count(),
        ];
    }



    /**
     * get posttype links, having shop feature
     *
     * @return array
     */
    private function getPostItems()
    {
        $collection = posttypes()->having("manage_sidebar")->having("shop")->sortBy('order')->get();
        $array      = [];

        foreach ($collection as $posttype) {
            $array[] = [
                 "caption"    => $posttype->title,
                 "link"       => "posts/" . $posttype->slug,
                 'condition'  => $posttype->can(),
                 'permission' => "posts-" . $posttype->slug,
            ];
        }

        return $array;
    }
}
