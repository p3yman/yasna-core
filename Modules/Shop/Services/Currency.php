<?php
namespace Modules\Shop\Services;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;

class Currency
{
    protected $code ;
    protected $title ;
    public function __construct($code = null)
    {
        $this->code = strtoupper($code) ;
        $this->title = $this->title() ;
    }

    public function __toString()
    {
        return strval($this->title()) ;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'title':
                return $this->title() ;

            default:
                return null ;
        }
    }

    public function title($code=null)
    {
        if (!$code) {
            $code = $this->code ;
        }

        return trans_safe('shop::currencies.' . strtoupper($code)) ;
    }
}
