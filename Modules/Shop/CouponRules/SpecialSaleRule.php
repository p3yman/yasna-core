<?php

namespace Modules\Shop\CouponRules;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Modules\Cart\Entities\Order;
use Modules\Coupons\CouponRules\Abs\DiscountRule;
use Modules\Shop\Entities\ShopOffer;
use Modules\Shop\Entities\Ware;

/**
 * Class SpecialSaleRule checks the invoice for wares which are on special sale
 * offer and update their discounts, if there were a better (higher) discount
 * on them!
 *
 * @package Modules\Shop\CouponRules
 */
class SpecialSaleRule extends DiscountRule
{
    /**
     * This collection saves all wares prices where updated from a special sales
     * set. The underlying array of this collection is in [ware_id =>
     * updated_price] format.
     *
     * @var Collection $all_wares
     */
    private $all_wares;



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return trans("shop::rules.special_offer_rule");
    }



    /**
     * @inheritdoc
     */
    public function handle()
    {
        $orders = $this->getOrders();
        $wares  = $this->getWares($orders);
        $offers = $this->getOffers($wares);

        $this->processOrdersAndUpdateTheirPriceBasedOnOffers(
             $orders,
             $offers,
             $wares
        );
        $this->updatedDiscountedAmountOnInvoice();
    }



    /**
     * Return collection of all orders of an invoice
     *
     * @return Collection
     */
    private function getOrders(): Collection
    {
        $orderIDs = $this->invoice->getItems();

        return model('order')->whereIn('id', array_keys($orderIDs))->get();
    }



    /**
     * Return array of all wares ID
     *
     * @param Collection $orders
     *
     * @return array
     */
    private function getWares(Collection $orders): array
    {
        return $orders->map(function ($order) {
            return $order->ware_id;
        })
                      ->toArray()
             ;
    }



    /**
     * Get an array of ware IDs and return an array of IDs of offers
     *
     * @param array $wares
     *
     * @return EloquentCollection
     */
    private function getOffers(array $wares): EloquentCollection
    {
        $offers = model('shop-offer')
             ->whereValid()
             ->whereHas('wares', function ($query) use ($wares) {
                 $query->whereIn('shop_offer_ware.ware_id', $wares);
             })
             ->with('wares')
             ->get()
        ;

        return $offers;
    }



    /**
     * Return all wares which will be applied in this rule
     *
     * @return Collection
     */
    private function getAllWares(): Collection
    {
        return $this->all_wares = collect([]);
    }



    /**
     *  Update the invoice with new discounted amount
     */
    private function updatedDiscountedAmountOnInvoice()
    {
        $this->getAllWares()->each(function ($ware) {
            $this->invoice->setItemDiscount(
                 $this->getIdentifier(),
                 $ware->order->id,
                 $ware->amount
            );
        })
        ;
    }



    /**
     * Apply special sale offers on orders
     *
     * @param Collection $orders
     * @param Collection $offers
     * @param array      $wares
     */
    private function processOrdersAndUpdateTheirPriceBasedOnOffers(
         Collection $orders,
         Collection $offers,
         array $wares
    ) {
        // For each offer, first get the wares in the offer which are on our
        // wares list and then try to update its price
        $offers->each(function ($offer) use ($wares, $orders) {
            $offer->wares->filter(function ($ware) use ($wares, $orders) {
                return in_array($ware->id, $wares);
            })
                         ->each(function ($ware) use ($offer, $orders) {
                             $this->addToAllWares($ware, $offer, $orders);
                         })
            ;
        });
    }



    /**
     * Checks if the offered discount amount is higher that the current
     * discount on the ware, update the discount with the higher value.`
     *
     * @param Ware       $ware
     * @param ShopOffer  $offer
     * @param Collection $orders
     */
    private function addToAllWares(
         Ware $ware,
         ShopOffer $offer,
         Collection $orders
    ) {
        if (is_null($this->all_wares)) {
            $this->all_wares = new Collection();
        }

        $offer->spreadMeta();

        $saved_ware = $this->all_wares->get($ware->id);
        $new_amount = $this->calculateApplicableDiscount(
             $ware->salePrice()->get(),
             $offer->discount_percentage,
             $offer->maximum_applicable_amount
        );

        // check if already the ware has been added to the collection, modify
        // its information only if it has higher discount amount, otherwise
        // just push it
        if (is_null($saved_ware) || $new_amount > $saved_ware->amount) {
            $for_save = (object)[
                 'amount' => $new_amount,
                 'order'  => $this->getOrderOfWare($orders, $ware),
            ];

            $this->all_wares->put($ware->id, $for_save);
        }
    }



    /**
     * Return the order model which related to a ware on an invoice
     *
     * @param Collection $orders
     * @param Ware       $ware
     *
     * @return Order
     */
    private function getOrderOfWare(Collection $orders, Ware $ware): Order
    {
        $order_location = $orders->search(function ($order) use ($ware) {
            return $order->ware_id == $ware->id;
        });

        return $orders->get($order_location);
    }



    /**
     * Calculate applicable discount amount based on discount percentage and an
     * upper limit on it.
     *
     * @param float      $price
     * @param int        $percent
     * @param float|null $limit
     *
     * @return float
     */
    private function calculateApplicableDiscount(
         float $price,
         int $percent,
         float $limit = null
    ): float {
        $discount_by_percent = round($price * $percent / 100, 2);

        if ($limit) {
            $applicable_discount = min($limit, $discount_by_percent);
        } else {
            $applicable_discount = $discount_by_percent;
        }

        return $applicable_discount;
    }
}
