<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaListRequest;


class InventoriesLogRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "Inventory";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return posttypes()->having("shop_inventory")->count();
    }



    /**
     * get the electors, to be applied on the model
     *
     * @return array
     */
    public function getElectors()
    {
        return [
             "wares" => $this->getData("ware") ? hashid($this->getData("ware")) : null,
             "types" => $this->getData("reason") ? $this->getData("reason") : null,
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if (!$this->getData("_submit")) {
            $this->unsetData("ware");
            $this->unsetData("reason");
        }
    }
}
