<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class WareChangeAvailabilityRequest extends YasnaRequest
{
    protected $model_name         = "ware";
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'is_available',
        ];
    }
}
