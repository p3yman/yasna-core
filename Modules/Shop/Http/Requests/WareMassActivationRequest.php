<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class WareMassActivationRequest extends YasnaRequest
{
    protected $model_name = "ware";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->posttype->can("shop");
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($hashid = false)
    {
        $this->data['ids'] = hashid(explode_not_empty(",", $this->data['ids']));

        parent::loadRequestedModel($this->data['ids'][0]);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if ($this->_submit == "activate") {
            $this->data['is_available'] = 1;
        } else {
            $this->data['is_available'] = 0;
        }
    }
}
