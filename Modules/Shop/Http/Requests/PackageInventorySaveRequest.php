<?php

namespace Modules\Shop\Http\Requests;

use App\Models\Inventory;
use Illuminate\Foundation\Http\FormRequest;

class PackageInventorySaveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'alteration' => "required|numeric|min:1",
            'type'       => "required|in:" . implode(',', Inventory::$available_types),
            'fee'        => "numeric",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $value    = parent::all();
        $purified = customValidation()::purifier($value, [
            'fee'        => "ed|numeric",
            'alteration' => "ed|numeric",
        ]);

        return $purified;
    }
}
