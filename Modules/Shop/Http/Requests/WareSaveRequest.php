<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Modules\Yasna\Services\YasnaRequest;

class WareSaveRequest extends YasnaRequest
{
    use ModuleRecognitionsTrait;

    protected $model_name = "Ware";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $posttype = posttype($this->model->posttype_id);
        if (!$posttype) {
            $posttype = posttype($this->data['posttype_id']);
        }

        if ($this->model->isPackage()) {
            return $posttype->can('setting');
        }

        return $posttype->canShop();
    }



    /**
     * Validation rules for titles
     *
     * @return array
     */
    public function titleRules(): array
    {
        if ($this->data['_submit'] == 'save') {
            return [
                 "titles" => "array|required",
            ];
        }

        return [];
    }



    /**
     * Validation rules for units
     *
     * @return array
     */
    public function unitRules(): array
    {
        if ($this->data['_submit'] == 'save' and $this->model->isNotPackage()) {
            return [
                 "unit_id" => "required|exists:units,id",
            ];
        }

        return [];
    }



    /**
     * Validation rules for packages
     *
     * @return array
     */
    public function packageRules(): array
    {
        if ($this->getData("package_id") and $this->data['_submit'] == 'save') {
            return [
                 "package_id"       => "exists:wares,id",
                 "package_capacity" => "required_unless:package_id,|numeric|min:1",
            ];
        }

        return [];
    }



    /**
     * Validation rules for prices
     *
     * @return array
     */
    public function priceRules(): array
    {
        $price_rules = [];

        if ($this->data['_submit'] == 'save') {
            foreach ($this->data['_currencies'] as $currency) {
                if ($this->data["_sale_$currency"] > 0 and $this->data["_sale_$currency"] > $this->data["_price_$currency"]) {
                    $price_rules['price'] = "required";
                    break;
                }
            }
        }

        return $price_rules;
    }



    /**
     * Returns validation rules for physical sizes.
     *
     * @return array
     */
    protected function physicalSizesRules()
    {
        return [
             'mass'   => 'numeric|min:0.01',
             'length' => 'numeric|min:0.01',
             'width'  => 'numeric|min:0.01',
             'height' => 'numeric|min:0.01',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        $rules = $this->mainPurifiers();
        $rules = $this->priceAndInventoryPurifiers($rules);

        return $rules;
    }



    /**
     * Main Purifier Rules
     *
     * @return array
     */
    private function mainPurifiers(): array
    {
        return [
             'posttype_id'      => "hashid",
             'id'               => "hashid",
             'titles'           => "array",
             'remarks'          => "array",
             'is_available'     => "boolean",
             '_currencies'      => "array",
             'package_capacity' => "ed|numeric",
             'mass'             => "ed",
             'length'           => "ed",
             'width'            => "ed",
             'height'           => "ed",
        ];
    }



    /**
     * Price and Inventory Purifiers
     *
     * @param array $rules
     *
     * @return array
     */
    private function priceAndInventoryPurifiers(array $rules): array
    {
        foreach ($this->data as $field => $value) {
            if (str_contains($field, "price") or str_contains($field, "inventory") or str_contains($field, "sale")) {
                $rules[$field] = "ed|numeric";
            }
            if (str_contains($field, "_at_")) {
                $rules[$field] = "ed|date";
            }
        }

        return $rules;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->setSisterhoodForNewModels();
        $this->setLocalesArray();
        $this->setOrderOnNewModels();
        $this->setInventoryMasterId();
        $this->nullifySisterhoodForPackages();
        $this->correctPhysicalSizes();
    }



    /**
     * set post sisterhood for new models
     */
    public function setSisterhoodForNewModels()
    {
        if (!$this->model->id) {
            $this->model->sisterhood  = $this->data['sisterhood'];
            $this->model->posttype_id = $this->data['posttype_id'];
        }
    }



    /**
     * Corrects the physical sizes.
     */
    protected function correctPhysicalSizes()
    {
        $physical_sizes = ['mass', 'length', 'width', 'height'];

        foreach ($physical_sizes as $size) {
            $value = ($this->data[$size] ?? null);
            if (!$value) {
                continue;
            }
            // Remove unacceptable characters
            $value = preg_replace("/[^(0-9\/\.)]/", "", $value);

            // Replace persian decimal point
            $value = str_replace('/', '.', $value);

            $this->data[$size] = $value;
        }
    }



    /**
     * nullifies sisterhood for packages
     *
     * @return void
     */
    private function nullifySisterhoodForPackages(): void
    {
        if ($this->model->exists and $this->model->isPackage()) {
            $this->data['sisterhood'] = null;
        }
    }



    /**
     * Sets inventory master id if required
     *
     * @return void
     */
    private function setInventoryMasterId(): void
    {
        if (!isset($this->data['_inv_type']) or $this->data['_inv_type'] == 'independent') {
            $this->data['inventory_master_id'] = 0;
        }
    }



    /**
     * Sets an array of the available locales, based on the submitted titles
     *
     * @return void
     */
    private function setLocalesArray(): void
    {
        $this->data['locales'] = implode(",", array_keys($this->data['titles']));
    }



    /**
     * Sets order on new models
     *
     * @return void
     */
    private function setOrderOnNewModels(): void
    {
        if (!$this->model->exists) {
            $this->data['order'] = 100;
        }
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'price.required'                   => trans("shop::prices.validation.sale_vs_price"),
             'titles.required'                  => trans("shop::wares.validation.titles"),
             'package_capacity.required_unless' => trans("shop::packages.validation.capacity_required"),
             'package_capacity.min'             => trans("shop::packages.validation.capacity_min"),
        ];

        //TODO: Improvement: different error for the times when only one title is requested.
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'mass'   => $this->runningModule()->getTrans('wares.mass'),
             'length' => $this->runningModule()->getTrans('wares.length'),
             'width'  => $this->runningModule()->getTrans('wares.width'),
             'height' => $this->runningModule()->getTrans('wares.height'),
        ];
    }
}
