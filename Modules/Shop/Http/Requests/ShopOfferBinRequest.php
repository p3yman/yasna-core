<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ShopOfferBinRequest extends YasnaRequest
{
    protected $model_name = "shop-offer";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->isTrashed();
    }
}
