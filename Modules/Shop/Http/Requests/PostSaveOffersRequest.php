<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/21/18
 * Time: 6:05 PM
 */

namespace Modules\Shop\Http\Requests;


class PostSaveOffersRequest extends PostOffersRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'offers' => 'array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctOffers();
    }



    /**
     * Corrects the offers array.
     */
    protected function correctOffers()
    {
        $value = ($this->data['offers'] ?? '');

        $this->data['offers'] = explode_not_empty(',', $value);;
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['offers'];
    }
}
