<?php

namespace Modules\Shop\Http\Requests;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaRequest;

class QuickSavePriceRequest extends YasnaRequest
{
    protected $model_name = "Ware";



    /**
     * set permit for save price
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->model->posttype->can('shop');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'original_price' => 'numeric',
             'sale_price'     => 'numeric',
             'price'          => $this->priceRules(),
        ];
    }



    /**
     * set rules for when sales price bigger than original price
     *
     * @return string
     */
    public function priceRules()
    {
        if ($this->data["sale_price"] > 0 and $this->data["sale_price"] > $this->data["original_price"]) {
            return 'required';
        }
        return '';
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'price.required' => trans("shop::prices.validation.sale_vs_price"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'original_price' => 'ed|numeric',
             'sale_price'     => 'ed|numeric',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['discount_expires_at'] = $this->getTimeFromUnix($this->data['discount_expires_at']);
    }



    /**
     * @param string(timestamp) $time
     *
     * @return bool|string
     */
    private function getTimeFromUnix($time)
    {
        //return pre value without change
        if (!$this->isValidTimeStamp($time)) {
            return $time;
        }

        $time = substr($time, 0, strlen($time) - 3);
        $time = Carbon::parse(date('Y-m-d', $time))->toDateString();
        return $time;
    }



    /**
     * check that it's valid timestamp
     *
     * @param string(timestamp) $timestamp
     *
     * @return bool
     */
    public function isValidTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === $timestamp)
             && ($timestamp <= PHP_INT_MAX)
             && ($timestamp >= ~PHP_INT_MAX);
    }
}
