<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PostOffersRequest extends YasnaRequest
{
    /**
     * The Main Model
     *
     * @var string
     */
    protected $model_name         = "post";
    /**
     * Whether the model should be find from trashed.
     *
     * @var bool
     */
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }
}
