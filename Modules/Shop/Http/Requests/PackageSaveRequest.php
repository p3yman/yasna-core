<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageSaveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function all($keys = null)
    {
        $rules = [] ;
        $data = parent::all() ;
        foreach ($data as $field => $value) {
            if (str_contains($field, "price") or str_contains($field, "inventory")) {
                $rules[$field] = "ed|numeric" ;
            }
        }

        return customValidation()::purifier($data, $rules) ;
    }
}
