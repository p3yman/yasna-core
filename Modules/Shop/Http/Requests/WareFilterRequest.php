<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class WareFilterRequest extends YasnaRequest
{
    protected $model_name = "Ware";



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        $registered_filters = module('shop')
             ->service('wares_filter')
             ->read()
        ;

        // read the `purifier` field from the registered filters
        return collect($registered_filters)
             ->map(function (array $item) {
                 return ($item['purifier'] ?? null);
             })->filter()
             ->toArray()
             ;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->unsetData('hashid');
    }
}
