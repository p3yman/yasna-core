<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ShopItemsActionRequest extends YasnaRequest
{
    protected $model_name = "shop-offer";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return !is_null($this->data['action']);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $submit = $this->data['_submit'];

        $this->data['action'] = str_before($submit, "-");
        $this->data['item']   = str_after($submit, "-");
    }
}
