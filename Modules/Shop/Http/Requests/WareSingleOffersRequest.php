<?php

namespace Modules\Shop\Http\Requests;


class WareSingleOffersRequest extends WareSingleRequest
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }
}
