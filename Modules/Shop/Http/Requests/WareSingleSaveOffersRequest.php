<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class WareSingleSaveOffersRequest extends WareSingleOffersRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'offers' => 'array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctOffers();
    }



    /**
     * Corrects the offers array.
     */
    protected function correctOffers()
    {
        $value = ($this->data['offers'] ?? '');

        $this->data['offers'] = explode_not_empty(',', $value);;
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['offers'];
    }
}
