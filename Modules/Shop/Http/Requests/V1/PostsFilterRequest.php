<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/12/18
 * Time: 12:43 PM
 */

namespace Modules\Shop\Http\Requests\V1;


use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class PostsFilterRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'min_price'     => 'numeric',
             'max_price'     => 'numeric',
             'labels'        => 'array',
             'labels.*'      => Rule::in($this->acceptableLabels()),
             'tags'          => 'array',
             'categories'    => 'array',
             'has_inventory' => 'boolean',
             'offers'        => 'array',
             'offers.*'      => Rule::in($this->acceptableOffers()),
             'is_available'  => 'boolean',
             'sort_by'       => Rule::in($this->acceptableSortByValues()),
             'sort'          => Rule::in(['asc', 'desc']),
        ];
    }



    /**
     * Returns an array of ids and slugs of all the acceptable labels.
     *
     * @return array
     */
    protected function acceptableLabels()
    {
        $labels = model('label')->where('model_name', 'Ware')->get();
        $ids    = $labels->pluck('id')->toArray();
        $slugs  = $labels->pluck('slug')->toArray();

        return array_merge($ids, $slugs);
    }



    /**
     * Returns an array of ids and slugs of all the valid offers.
     *
     * @return array
     */
    protected function acceptableOffers()
    {
        $valid_offers = model('shop_offer')->whereValid()->get();
        $ids          = $valid_offers->pluck('id')->toArray();
        $slugs        = $valid_offers->pluck('slug')->toArray();

        return array_merge($ids, $slugs);
    }



    /**
     * Returns the acceptable values for the `sort_by` field.
     *
     * @return array
     */
    protected function acceptableSortByValues()
    {
        return array_keys($this->sortByMap());
    }



    /**
     * Returns the map form the values of the `sort_by` fields to the applicable columns.
     *
     * @return array
     */
    protected function sortByMap()
    {
        return [
             'time' => 'posts.published_at',
        ];
    }



    /**
     * Returns the column based on the given value of the `sort_by` field.
     *
     * @param string $input
     *
     * @return string
     */
    public function getSortByColumn(string $input)
    {
        $map = $this->sortByMap();

        return $map[$input];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'post'      => 'dehash',
             'min_price' => 'ed|numeric',
             'max_price' => 'ed|numeric',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctHashidSlugArrays();
    }



    /**
     * Corrects fields which should contain an array of hashids or slugs.
     */
    protected function correctHashidSlugArrays()
    {
        $hashid_array_fields = ['labels', 'categories', 'offers'];

        foreach ($hashid_array_fields as $hashid_array_field) {
            $this->correctHashidSlugArrayField($hashid_array_field);
        }
    }



    /**
     * Corrects the value of the specified field. This field should be an `array`.
     * * Any hashid item in the array will be converted to an `id`.
     * * Any numeric item in the array will be ignored.
     * * Other type of values will be remained in the original values.
     *
     * @param string $field_name
     */
    protected function correctHashidSlugArrayField(string $field_name)
    {
        $field_value = ($this->data[$field_name] ?? []);

        if (!is_array($field_value)) {
            return;
        }

        // filter numeric values
        $field_value = array_filter($field_value, function ($category) {
            return !is_numeric($category);
        });

        $this->data[$field_name] = array_map(function ($category) {
            $id = hashid($category);

            return ($id ?: $category);
        }, $field_value);
    }



    /**
     * Whether a `posttype` field has been specified in the request.
     *
     * @return bool
     */
    public function posttypeSpecified()
    {
        return boolval($this->input('posttype'));
    }



    /**
     * Whether a `posttype` field has not been specified in the request.
     *
     * @return bool
     */
    public function posttypeMissing()
    {
        return !$this->posttypeSpecified();
    }
}
