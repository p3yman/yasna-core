<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ShopOfferSaveRequest extends YasnaRequest
{
    protected $model_name = "shop-offer";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id = $this->data['id'];

        return [
             "title"                     => "required:unique:shop_offers,title,$id",
             "slug"                      => "unique:shop_offers,slug,$id",
             "maximum_applicable_amount" => "numeric|min:0",
             "discount_percentage"       => "numeric|between:0,100",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if (!dev()) {
            unset($this->data['slug']);
        }

        $this->data['maximum_applicable_amount'] = str_replace(
             ",",
             "",
             $this->maximum_applicable_amount
        );

        $this->data['discount_percentage'] = str_replace(
             ",",
             "",
             $this->discount_percentage
        );
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "expires_at"                => "date",
             "maximum_applicable_amount" => "ed",
             "discount_percentage"       => "ed",
        ];
    }
}
