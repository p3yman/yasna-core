<?php

namespace Modules\Shop\Http\Requests;

use App\Models\Posttype;
use Modules\Yasna\Services\YasnaRequest;

class DownstreamLabelSaveRequest extends YasnaRequest
{
    protected $model_name = "label";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "slug"   => "required" . ($this->isSlugAccepted() ? '' : '|accepted'),
             "titles" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['posttype'] = $this->getPosttype();
        $this->data['titles']   = $this->getTitlesArray($this->data['posttype']);
        $this->data['slug']     = $this->getCorrectedSlug($this->data['posttype']);
    }



    /**
     * Returns a purified slug with necessary posttype identifier added to its beginning.
     *
     * @param Posttype $posttype
     *
     * @return string
     */
    private function getCorrectedSlug(Posttype $posttype)
    {
        if ($this->data['slug']) {
            return $posttype->slug . ware()::LABEL_POSTTYPE_DIVIDER . str_slug($this->data['slug']);
        }

        return '';
    }



    /**
     * Gets Posttype instance from the submitted hashid.
     *
     * @return \App\Models\Posttype
     */
    private function getPosttype()
    {
        return posttype($this->data['posttype']);
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "titles.required" => trans_safe("shop::labels.titles_required"),
             "slug.accepted"   => trans_safe("shop::labels.slug_not_unique"),
        ];
    }



    /**
     * Combines all the titles in different locales into a single array.
     *
     * @param Posttype $posttype
     *
     * @return array
     */
    private function getTitlesArray(Posttype $posttype)
    {
        $valid_locales = $posttype->localesArray();
        $titles        = [];

        foreach ($valid_locales as $locale) {
            if ($this->data[$locale]) {
                $titles[$locale] = $this->data[$locale];
            }
        }

        return $titles;
    }



    /**
     * Checks if the slug is unique
     *
     * @return bool
     */
    private function isSlugAccepted()
    {
        $label = ware()->findLabel($this->data['slug']);

        return $label->isNotSet() or $label->id == $this->model->id;
    }
}
