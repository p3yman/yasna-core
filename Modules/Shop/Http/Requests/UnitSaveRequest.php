<?php

namespace Modules\Shop\Http\Requests;

use App\Models\Unit;
use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;

class UnitSaveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $id    = hashid($input['hashid']);

        return [
            'slug' => 'required|alpha_dash|forbidden_chars:_|not_in:' . Unit::$reserved_slugs . '|unique:units,slug,' . $id . ',id',
            'concept' => "required" ,
             'ratio' => "required|numeric" ,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $value = parent::all() ;
        $purified = ValidationServiceProvider::purifier($value, [
            'ratio' => "ed" ,
        ]);
        return $purified ;
    }
}
