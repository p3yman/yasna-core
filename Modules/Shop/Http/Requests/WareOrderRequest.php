<?php

namespace Modules\Shop\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class WareOrderRequest extends YasnaRequest
{
    protected $model_name = "Post";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canShop();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'order' => "array|required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'order.required' => trans_safe("shop::wares.order_error"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $order_string        = str_after($this->data['order'], 'main-column=');
        $order_string        = str_replace('drag-', null, $order_string);
        $this->data['order'] = explode(',', $order_string);
    }
}
