<?php


/*
|--------------------------------------------------------------------------
| Upstream Settings
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:developer'],
          'prefix'     => 'manage/shop/upstream',
          'namespace'  => 'Modules\Shop\Http\Controllers',
     ],
     function () {
         Route::get('/unit/{action}/{hash_id?}', 'UpstreamController@unitAction');

         Route::group(['prefix' => 'save'], function () {
             Route::post('/unit', 'UpstreamController@unitSave')->name('unit-save');
             Route::post('/unit-activeness', 'UpstreamController@unitActiveness')->name('unit-activeness');
         });
     }
);

/*
|--------------------------------------------------------------------------
| Downstream Packages
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth'],
          'prefix'     => 'manage/shop/downstream/package',
          'namespace'  => module('shop')->getControllersNamespace(),
     ],
     function () {
         Route::get('list/{hash_id}/', 'DownstreamController@packageList');
         Route::get('{action}/{hash_id}', 'DownstreamController@packageAction');
     }
);

/*
|--------------------------------------------------------------------------
| Downstream Labels
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth'],
          'prefix'     => 'manage/shop/downstream/labels',
          'namespace'  => module('shop')->getControllersNamespace(),
     ],
     function () {
         Route::get('list/{posttype_hash_id}/', 'LabelsDownstreamController@labelsList');
         Route::get('create-master/{hash_id}', 'LabelsDownstreamController@createMasterForm');
         Route::get('create-child/{hash_id}', 'LabelsDownstreamController@createChildForm');
         Route::get('edit/{hash_id}', 'LabelsDownstreamController@editForm');
         Route::post('save', 'LabelsDownstreamController@save')->name('downstream-label-save');
     }
);

/*
|--------------------------------------------------------------------------
| Packages Management
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth'],
          'prefix'     => 'manage/shop/packages',
          'namespace'  => module('shop')->getControllersNamespace(),
     ],
     function () {
         Route::get('create/{hashid}', 'PackagesEditController@createForm');
         Route::get('edit/{hashid}', 'PackagesEditController@editForm');
         Route::post('/save', 'PackagesEditController@save')->name('shop-packages-save');
     }
);

/*
|--------------------------------------------------------------------------
| Wares Management
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth'],
          'prefix'     => 'manage/shop/wares',
          'namespace'  => module('shop')->getControllersNamespace(),
     ],
     function () {
         Route::get('index/{hashid}', 'WaresController@index');
         Route::get('create/{hashid}', 'WaresEditController@createForm');
         Route::get('edit/{hashid}/{back_to_list?}', 'WaresEditController@editForm');

         Route::get('{hashid}/special-offers', 'WaresOffersController@single')->name('shop.wares.offers');
         Route::post('special-offers', 'WaresOffersController@singleSave')->name('shop.wares.offers.save');

         Route::get('offers/{hashid}', 'WaresOffersController@postOffers')->name('shop.post.wares.offers');
         Route::post('offers', 'WaresOffersController@postOffersSave')->name('shop.post.wares.offers.save');

         Route::group(['prefix' => 'save'], function () {
             Route::post('/', 'WaresEditController@save')->name('shop-wares-save');
             Route::post('/quickly','WaresEditController@savePriceQuickly')->name('shop-wares-save-quickly');
             Route::post('/order', 'WaresEditController@saveOrder')->name('shop-wares-save-order');
             Route::post('availability', 'WaresEditController@saveAvailability')->name('shop-wares-save-availability');
         });
     }
);

/*
|--------------------------------------------------------------------------
| Ware Finder Widget
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth'],
          'prefix'     => 'manage/shop',
          'namespace'  => 'Modules\Shop\Http\Controllers',
     ],
     function () {
         Route::get('/ware/row/{hashid}', 'WareListController@update');
     }
);
