<?php

namespace Modules\Shop\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/shop-posts-filter
 *                    Posts Filter
 * @apiDescription    filter through the shopping posts and their wares
 * @apiVersion        1.0.0
 * @apiName           Posts Filter
 * @apiGroup          Shop
 * @apiPermission     none
 * @apiParam {String} [posttype] hashid or slug of the posttype. if not presented or a not shopping posttype presented,
 *           all the shopping posttypes will be included.
 * @apiParam {Float} [min_price] the minimum price
 * @apiParam {Float} [max_price] the maximum price
 * @apiParam {String} [post] the hashid of a post
 * @apiParam {String} [search] a phrase to search in the posts' titles and slugs
 * @apiParam {Array} [labels] an array of hashids or slugs of wares' labels
 * @apiParam {Array} [tags] an array of the slugs of the posts' tags
 * @apiParam {Array} [categories] an array of hashids or slugs of posts' categories
 * @apiParam {Boolean} [has_inventory=0] If `1`, the below posts will be included:
 * - posts of posttypes without the `shop_inventory` feature
 * - posts of posttypes with the `shop_inventory` feature which have remained inventory
 * @apiParam {Array} [offers] an array of hashids or slugs of wares' offers
 * @apiParam {Boolean} [is_available=0] If `1`, only available wares will be included.
 * @apiParam {String} [custom_code] a phrase to search in the wares' custom codes
 * @apiParam {String=time} [sort_by=time] the value which describe how to sort the result
 * @apiParam {String=asc,desc} [sort=desc] the type of sorting the results
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {String} [per_page=15] Indicate the number of results on per page. (available only with `paginated=1`).
 * @apiParam {String} [page=1] Page number of returned result (available only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "total": 1
 *      },
 *      "results": [
 *           {
 *                "id": "pxkYN",
 *                "title": "Post Title",
 *                "abstract": "",
 *                "featured_image":
 *                "http://yasna.local/uploads/posts/products/featured-image/image/1542021114_yMTZxGyCyWWDPJbbT7C5iICFoVqEq3_original.jpg",
 *                "wares": [
 *                     {
 *                          "id": "DAvQN",
 *                          "title": "Ware Title",
 *                          "sale_price": 90000,
 *                          "original_price": 100000,
 *                          "custom_code": "",
 *                          "is_available": 1
 *                     }
 *                ]
 *           }
 *      ]
 * }
 * @apiErrorExample   Bad Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "shop.moreInfo.422",
 *      "errors": {
 *           "sort": [
 *                [
 *                     "The selected sort is invalid."
 *                ]
 *           ]
 *      }
 * }
 * @method  \Modules\Shop\Http\Controllers\V1\PostsFilterController controller()
 */
class PostsFilterEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Posts Filter";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Shop\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostsFilterController@filter';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  'id'             => 'pxkYN',
                  'title'          => 'Post Title',
                  'abstract'       => '',
                  'featured_image' => 'http://yasna.local/uploads/posts/products/featured-image/image/1542021114_yMTZxGyCyWWDPJbbT7C5iICFoVqEq3_original.jpg',
                  'wares'          =>
                       [
                            [
                                 'id'             => 'DAvQN',
                                 'title'          => 'Ware Title',
                                 'sale_price'     => 90000,
                                 'original_price' => 100000,
                                 'custom_code'    => '',
                                 'is_available'   => 1,
                            ],
                       ],
             ],
        ], [
             'total' => 1,
        ]);
    }
}
