<?php

namespace Modules\Shop\Http\Controllers\V1;

use App\Models\Post;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Shop\Http\Controllers\V1\Traits\PostsFilterBuilderTrait;
use Modules\Shop\Http\Controllers\V1\Traits\PostsFilterElectorsTrait;
use Modules\Shop\Http\Requests\V1\PostsFilterRequest;
use Modules\Yasna\Services\YasnaApiController;

class PostsFilterController extends YasnaApiController
{
    use PostsFilterElectorsTrait;
    use PostsFilterBuilderTrait;


    /**
     * The Request in User
     *
     * @var PostsFilterRequest
     */
    protected $request;

    /**
     * The Builder in User
     *
     * @var Builder
     */
    protected $builder;



    /**
     * Filters the shop posts.
     *
     * @param PostsFilterRequest $request
     *
     * @return array
     */
    public function filter(PostsFilterRequest $request)
    {
        $this->request = $request;

        $this->generateFilterBuilder();


        if ($request->isPaginated()) {
            return $this->listPaginated();
        }

        return $this->listFlat();
    }



    /**
     * Generates the builder for the filter.
     */
    protected function generateFilterBuilder()
    {
        // get the dynamic elector data based on the request
        $elector_data  = $this->getElectorData();
        $this->builder = post()->elector($elector_data);

        // modify the builder based on the request
        $this->runFilterBuilderMethods();
    }



    /**
     * List by Pagination
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listPaginated()
    {
        $builder   = $this->builder;
        $request   = $this->request;
        $paginator = $builder->paginate($request->perPage());
        $result    = $this->mappedArray($paginator);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * List All Together
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listFlat()
    {
        $builder = $this->builder;

        $meta = [
             "total" => $builder->count(),
        ];

        $data = $this->mappedArray($builder->get());

        return $this->success($data, $meta);
    }



    /**
     * Returns an array version of the posts in the given collection.
     *
     * @param LengthAwarePaginator|Collection $collection
     *
     * @return array
     */
    protected function mappedArray($collection)
    {
        return $collection
             ->groupBy('id')
             ->map(function (Collection $collection) {
                 /** @var Post $post */
                 $post = $collection->first();

                 return $post->toFilterResource($collection);
             })
             ->values()
             ->toArray()
             ;
    }
}
