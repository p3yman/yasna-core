<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/12/18
 * Time: 12:47 PM
 */

namespace Modules\Shop\Http\Controllers\V1\Traits;


trait PostsFilterElectorsTrait
{
    /**
     * Returns an array ot be used as the elector data.
     *
     * @return array
     */
    protected function getElectorData(): array
    {
        $methods      = $this->getFilterElectorMethods();
        $elector_data = [];

        foreach ($methods as $method) {
            $elector_data = array_merge_recursive($elector_data, $this->$method());
        }

        return $elector_data;
    }



    /**
     * Returns an array of the methods which should be ran to generate the elector data.
     *
     * @return array
     */
    protected function getFilterElectorMethods(): array
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method_name) {
                 return starts_with($method_name, 'filterElector');
             })
             ->toArray()
             ;
    }



    /**
     * Returns the part of elector data to apply condition on the posttype.
     *
     * @return array
     */
    protected function filterElectorPosttype(): array
    {
        return [
             'type' => $this->getApplicablePosttypes(),
        ];
    }



    /**
     * Returns an array of the slugs of the applicable posttypes.
     *
     * @return array
     */
    protected function getApplicablePosttypes(): array
    {
        $shop_posttypes = $this->getShopPosttypes();

        // If no posttype specified in the request
        if ($this->request->posttypeMissing()) {
            return $shop_posttypes;
        }

        $specified_posttype = posttype($this->request->posttype);
        $intersect          = array_intersect($shop_posttypes, [$specified_posttype->slug]);

        // If The specified posttype is one of the shop posttypes.
        if (count($intersect)) {
            return $intersect;
        } else {
            return $shop_posttypes;
        }
    }



    /**
     * Returns an array of the slugs of the shop posttypes.
     *
     * @return array
     */
    protected function getShopPosttypes(): array
    {
        return model('feature', 'shop')
             ->posttypes()
             ->get(['posttypes.slug'])
             ->pluck('slug')
             ->toArray()
             ;
    }



    /**
     * Returns the effect of the `search` field from the request in the elector data.
     *
     * @return array
     */
    protected function filterElectorSearch(): array
    {
        $search = $this->request->search;

        if (!$search) {
            return [];
        }

        return [
             'search' => $search,
        ];
    }



    /**
     * Returns the effect of the `categories` field from the request in the elector data.
     *
     * @return array
     */
    protected function filterElectorCategories(): array
    {
        $categories = $this->request->categories;

        if (empty($categories)) {
            return [];
        }

        return [
             'categories' => $categories,
        ];
    }
}
