<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/12/18
 * Time: 1:27 PM
 */

namespace Modules\Shop\Http\Controllers\V1\Traits;


trait PostsFilterBuilderTrait
{
    /**
     * Runs all the methods starting with `filterBuilder` to modify the query builder before running it.
     */
    protected function runFilterBuilderMethods()
    {
        $methods = $this->getFilterBuilderMethods();

        foreach ($methods as $method) {
            $this->$method();
        }
    }



    /**
     * Returns an array of the methods which should be ran to modify the filter's query builder.
     *
     * @return array
     */
    protected function getFilterBuilderMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method_name) {
                 return starts_with($method_name, 'filterBuilder');
             })
             ->toArray()
             ;
    }



    /**
     * Applies the selecting columns in the query builder.
     */
    protected function filterBuilderSelect()
    {
        $this->builder->select([
            // columns in `posts`
            'posts.*',
            // columns in `wares`
            'wares.id as ware_id',
            'wares.original_price as ware_original_price',
            'wares.sale_price as ware_sale_price',
            'wares.meta as ware_meta',
            'wares.custom_code as ware_custom_code',
            'wares.is_available as ware_is_available',
        ]);
    }



    /**
     * Applies join with the `wares` table on the query builder.
     */
    protected function filterBuilderJoin()
    {
        $this->builder->join('wares', 'posts.sisterhood', 'wares.sisterhood');
    }



    /**
     * Applies the `post` field from the request into the query builder.
     */
    protected function filterBuilderPost()
    {
        $id = $this->request->post;

        if (!$id) {
            return;
        }

        $id = intval($id);


        $this->builder->where('posts.id', $id);
    }



    /**
     * Applies the `min_price` field from the request into to the query builder.
     */
    protected function filterBuilderMinPrice()
    {
        $min_price = $this->request->min_price;

        if (!$min_price) {
            return;
        }

        $this->builder->where('wares.sale_price', '>=', $min_price);
    }



    /**
     * Applies the `max_price` field from the request into to the query builder.
     */
    protected function filterBuilderMaxPrice()
    {
        $max_price = $this->request->max_price;

        if (!$max_price) {
            return;
        }

        $this->builder->where('wares.sale_price', '<=', $max_price);
    }



    /**
     * Applies the `labels` field from the request into the query builder.
     */
    protected function filterBuilderLabels()
    {
        $labels = $this->request->labels;

        if (empty($labels)) {
            return;
        }

        $this
             ->builder
             ->whereExists(function ($query) use ($labels) {
                 $query->from('labels');
                 $query->whereColumn('model_id', 'wares.id');
                 $query->where('labels_pivot.model_name', 'Ware');
                 $query->join('labels_pivot', 'labels_pivot.label_id', 'labels.id');

                 $query->where(function ($inner_query) use ($labels) {
                     $inner_query->whereIn('labels.id', $labels);
                     $inner_query->orWhereIn('labels.slug', $labels);
                 });
             });
    }



    /**
     * Applies the `tags` field from the request into the query builder.
     */
    protected function filterBuilderTags()
    {
        $tags = $this->request->tags;

        if (empty($tags)) {
            return;
        }

        $this
             ->builder
             ->whereHas('tags', function ($query) use ($tags) {
                 $query->whereIn('slug', $tags);
             });
    }



    /**
     * Applies the `has_inventory` field from the request into the query builder. If the `has_inventory` has a `true`
     * value, the bellow groups of posts will be included:
     * - posts of posttypes without the `shop_inventory` feature
     * - posts of posttypes with the `shop_inventory` feature which have remained inventory
     */
    protected function filterBuilderHasInventory()
    {
        $has_inventory = $this->request->has_inventory;

        if (!$has_inventory) {
            return;
        }

        $inventory_posttypes = $this->getInventoryPosttypes();

        $this->builder->where(function ($query) use ($inventory_posttypes) {

            // posts of posttypes with the `shop_inventory` feature which have remained inventory
            $query->where(function ($inner_query) use ($inventory_posttypes) {
                $inner_query->whereIn('type', $inventory_posttypes);
                $inner_query->where('current_inventory', '>', 0);
            });

            // posts of posttypes without the `shop_inventory` feature
            $query->orWhereNotIn('type', $inventory_posttypes);
        });
    }



    /**
     * Returns an array of the slugs of the posttypes with the `shop_inventory` feature.
     *
     * @return array
     */
    protected function getInventoryPosttypes()
    {
        return model('feature', 'shop_inventory')
             ->posttypes()
             ->get(['posttypes.slug'])
             ->pluck('slug')
             ->toArray()
             ;
    }



    /**
     * Applies the `offers` field from the request into the query builder.
     */
    protected function filterBuilderOffers()
    {
        $offers = $this->request->offers;

        if (empty($offers)) {
            return;
        }

        $this
             ->builder
             ->join('shop_offer_ware', function ($query) use ($offers) {
                 // the main condition of the join
                 $query->whereColumn('shop_offer_ware.ware_id', 'wares.id');

                 // include only offers with the requested slugs or ids
                 $query->whereIn('shop_offer_ware.offer_id', function ($query) use ($offers) {
                     // offers with the requested slugs or ids
                     $query->select('id')
                           ->from('shop_offers')
                           ->whereIn('id', $offers)
                           ->orWhereIn('slug', $offers)
                     ;
                 });
             });
    }



    /**
     * Applies the `is_available` field from the request into the query builder.
     */
    protected function filterBuilderIsAvailable()
    {
        $is_available = $this->request->is_available;

        if (!$is_available) {
            return;
        }

        $this->builder->where('wares.is_available', 1);
    }



    /**
     * Applies the `custom_code` field from the request into the query builder.
     */
    protected function filterBuilderCustomCode()
    {
        $custom_code = $this->request->custom_code;

        if (!$custom_code) {
            return;
        }

        $this->builder->where('wares.custom_code', 'like', "%$custom_code%");
    }



    /**
     * Sorts the result of the query builder.
     */
    protected function filterBuilderOrder()
    {
        $request      = $this->request;
        $sort         = ($request->sort ?? 'decs');
        $sort_by      = ($request->sort_by ?? 'time');
        $order_column = $request->getSortByColumn($sort_by);

        $this->builder->orderBy($order_column, $sort);
    }
}
