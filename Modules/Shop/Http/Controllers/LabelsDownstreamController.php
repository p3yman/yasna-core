<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Label;
use App\Models\Posttype;
use Modules\Shop\Http\Requests\DownstreamLabelSaveRequest;
use Modules\Yasna\Services\YasnaController;

class LabelsDownstreamController extends YasnaController
{
    protected $view_folder = "shop::downstream.labels";



    /**
     * Index of the downstream settings, shop tab.
     *
     * @return array
     */
    public static function index()
    {
        return [
             "view_file" => "shop::downstream.labels.index",
             "models"    => posttype()->having('shop')->orderBy('id', 'desc')->get(),
        ];
    }



    /**
     * Prepares data and Returns the blade responsible to show a list of master blades
     *
     * @param string $posttype_hashid
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function labelsList(string $posttype_hashid)
    {
        $posttype = posttype($posttype_hashid);
        if ($posttype->isNotSet()) {
            return $this->abort(410);
        }
        if ($posttype->cannot('setting')) {
            return $this->abort(403);
        }

        $models = $posttype->definedShopMasterLabels()->orderBy('id', 'desc')->get();

        if (!$models->count()) {
            return $this->view('empty', compact('posttype'));
        }

        return $this->view('list', compact('posttype', 'models'));
    }



    /**
     * Returns the form of master create
     *
     * @param string $posttype_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createMasterForm(string $posttype_hashid)
    {
        $posttype = posttype($posttype_hashid);
        $label    = ware()->getNewLabel();

        return $this->editorForm($label, $posttype);
    }



    /**
     * Returns the form of child create
     *
     * @param string $master_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createChildForm(string $master_hashid)
    {
        $master           = label($master_hashid);
        $posttype         = $this->getPosttypeFromLabel($master);
        $label            = ware()->getNewLabel();
        $label->parent_id = $master_hashid;

        return $this->editorForm($label, $posttype);
    }



    /**
     * Returns an editor form. Both master and child forms are handled here.
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editForm(string $hashid)
    {
        $label    = label($hashid);
        $posttype = $this->getPosttypeFromLabel($label);

        return $this->editorForm($label, $posttype);
    }



    /**
     * Returns the editor blade
     *
     * @param Label    $model
     * @param Posttype $posttype
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    private function editorForm($model, $posttype)
    {
        if ($posttype->isNotSet()) {
            return $this->abort(410);
        }
        if ($posttype->cannot('setting')) {
            return $this->abort(403);
        }

        return $this->view('editor', compact('model', 'posttype'));
    }



    /**
     * Saves the model according to the request and sends a div-refreshment signal if successful.
     *
     * @param DownstreamLabelSaveRequest $request
     *
     * @return string
     */
    public function save(DownstreamLabelSaveRequest $request)
    {
        if ($request->model->isset()) {
            $saved = $this->saveEditLabel($request);
        } else {
            $saved = $this->saveNewLabel($request);
        }

        return $this->jsonAjaxSaveFeedback($saved, [
             'success_callback' => "divReload('divLabel-" . $request->posttype->hashid . "')",
        ]);
    }



    /**
     * Tries to insert a new label record
     *
     * @param DownstreamLabelSaveRequest $request
     *
     * @return bool
     */
    private function saveNewLabel(DownstreamLabelSaveRequest $request): bool
    {
        return ware()->defineLabel(
             $request->slug,
             $request->titles,
             $request->parent
        );
    }



    /**
     * Tries to update a label record
     *
     * @param DownstreamLabelSaveRequest $request
     *
     * @return bool
     */
    private function saveEditLabel(DownstreamLabelSaveRequest $request): bool
    {
        return $request->model->changeTitlesAndSlug($request->titles, $request->slug);
    }



    /**
     * Gets a safe posttype instance from the pattern of label's slug.
     *
     * @param Label $label
     *
     * @return Posttype
     */
    private function getPosttypeFromLabel(Label $label)
    {
        $slug = str_before($label->slug, ware()::LABEL_POSTTYPE_DIVIDER);
        return posttype($slug);
    }
}
