<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Ware;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Shop\Http\Requests\WareFilterRequest;
use Modules\Shop\Http\Requests\WareMassActivationRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class WareListController extends YasnaController
{
    protected $base_model  = 'ware';
    protected $view_folder = 'shop::wares.list';
    protected $row_view    = 'row';

    /** @var WareFilterRequest */
    private $request;
    private $model;
    private $view_data;
    private $has_filter = false;
    /**
     * @var string $posttype this is hashid of posttype
     */
    private $posttype;



    /**
     * show list of wares
     *
     * @param WareFilterRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(WareFilterRequest $request)
    {
        $this->request = $request;
        $has_access    = $this->prepareData();
        if (!$has_access) {
            return $this->abort(403);
        }

        $this->setHasFilter($request);

        $elector = $this->waresFilterElector();

        $this->model = $this->model->elector($elector);

        $this->callServices();
        $this->view_data['models']     = $this->getFilterResult();
        $this->view_data['has_filter'] = $this->has_filter;

        return $this->view("browse", $this->view_data);
    }



    /**
     * Returns a download response for the excel export.
     *
     * @param WareFilterRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function excel(WareFilterRequest $request)
    {
        $this->request = $request;
        $has_access    = $this->prepareData();
        if (!$has_access) {
            return $this->abort(403);
        }

        $result = $this->generateExportResult($request);
        $this->handleExportServices();


        return Excel::create($this->generateExportFileName(), function ($excel) use ($result) {
            $excel->sheet('sheet', function ($sheet) use ($result) {
                $sheet->appendRow($this->getExcelHeadings());

                foreach ($result as $key => $model) {
                    $sheet->appendRow($this->getExcelRow($model, $key));
                }
            });
        })->download('xls')
             ;
    }



    /**
     * Returns the result to be rendered in the excel export.
     *
     * @param WareFilterRequest $request
     *
     * @return Collection
     */
    protected function generateExportResult(WareFilterRequest $request)
    {
        $elector     = $this->waresFilterElector();
        $this->model = $this->model->elector($elector);
        $builder     = $this->getFilterFinalizedBuilder();

        return $builder->get();
    }



    /**
     * Handles the export services.
     */
    protected function handleExportServices()
    {
        $this->module()->service('ware_export_columns_handler')->handle();
    }



    /**
     * Generates and returns a name for a excel file.
     *
     * @return string
     */
    protected function generateExportFileName()
    {
        return 'wares-' . date('Ymd-His');
    }



    /**
     * Returns an array of heading for the excel export.
     *
     * @return array
     */
    protected function getExcelHeadings()
    {
        $registered = service('shop:ware_export_columns')->read();
        $columns    = array_column($registered, 'caption');
        array_unshift($columns, '#');

        return $columns;
    }



    /**
     * Returns an array based on the specified ware to be rendered in the excel export file.
     *
     * @param Ware $ware
     * @param int  $key
     *
     * @return array
     */
    protected function getExcelRow(Ware $ware, int $key)
    {
        $columns = service('shop:ware_export_columns')->read();
        $row     = [$key + 1];

        foreach ($columns as $column) {
            $parser = $column['value']['parser'];

            $row[] = $parser($ware);
        }

        return $row;
    }



    /**
     * Returns the elector to be used in the filter builder.
     *
     * @return array
     */
    protected function waresFilterElector()
    {
        $data = request()->toArray();

        $complete_data = array_default($data, $this->staticFilterElector());

        return array_filter($complete_data);
    }



    /**
     * Returns the static part of the elector. (in case of no filter in the query string)
     *
     * @return array
     */
    protected function staticFilterElector()
    {
        return [
             'type' => $this->posttype,
        ];
    }



    /**
     * set tabs array
     *
     * @return boolean
     */
    private function setTabs()
    {
        $this->posttype = $this->request->get('type');
        $types          = posttype()->whereHas('features', function ($q) {
            $q->where('slug', 'shop');
        })->get()
        ;
        $posttypes      = [];


        $has_access = $this->selectDefaultPosttype($types);

        if (!$has_access) {
            return false;
        }

        $this->view_data['current_tab'] = $this->posttype;
        $this->view_data['page']        = $this->setPage();
        $visible_item                   = 0;
        foreach ($types as $posttype) {
            $tab_access  = $posttype->canShop();
            $posttypes[] = [
                 'caption'   => $posttype->title,
                 'url'       => '?type=' . $posttype->hashid,
                 'condition' => $tab_access,
            ];
            if ($tab_access) {
                $visible_item++;
            }
        }

        $this->view_data['count_of_visible_item'] = $visible_item;
        $this->view_data['posttypes']             = $posttypes;
        return true;
    }



    /**
     * select default posttype for view
     *
     * @param Collection $types
     *
     * @return bool
     */
    private function selectDefaultPosttype($types)
    {
        if (empty($this->posttype)) {

            foreach ($types as $posttype) {
                if ($posttype->canShop()) {
                    $this->posttype = $posttype->hashid;
                    return true;
                }
            }

            //if user cant see any posttype abort 404
            return false;
        }

        if (is_numeric($this->posttype)) {
            return false;
        }

        if (posttype($this->posttype)->cannotShop()) {
            return false;
        }

        return true;
    }



    /**
     * this is for show filter box in ui when user uses filter in reload page
     */
    private function setHasFilter()
    {
        $filter_data = request()->except('type');

        if (empty($filter_data)) {
            return;
        }

        $this->has_filter = true;
    }



    /**
     * @inheritdoc
     */
    public function update($model_hashid, $spare = null)
    {
        $this->callServices();
        return parent::update($model_hashid, $spare);
    }



    /**
     * prepare action data
     *
     * @return boolean
     */
    private function prepareData()
    {
        $this->model                   = model('ware');
        $this->view_data['controller'] = $this;
        $this->view_data['request']    = $this->request;
        return $this->setTabs();
    }



    /**
     * get result of filter queries
     *
     * @return LengthAwarePaginator
     */
    private function getFilterResult()
    {
        $result = $this->getFilterFinalizedBuilder()->paginate(15);

        if ($this->has_filter) {
            $request = $this->request->toArray();
            $result = $result->appends($request);
        }


        return $result;
    }



    /**
     * Returns a builder to be used in the list an excel action.
     *
     * @return Builder
     */
    protected function getFilterFinalizedBuilder()
    {
        return $this->model->orderBy(...$this->orderParameters());
    }



    /**
     * Return the parameters to be passed to the `orderBy()` method.
     *
     * @return array
     */
    protected function orderParameters()
    {
        $sort_by        = $this->request->get('sort_by') ?? $this->defaultSortBy();
        $sort_direction = $this->request->get('sort_direction') ?? $this->defaultSortDirection();

        return [$sort_by, $sort_direction];
    }



    /**
     * generate ware actions
     *
     * @param Ware $model
     *
     * @return array
     */
    public function renderRowActions($model)
    {
        module('shop')
             ->service('ware_row_action_handlers')
             ->handle([
                  "model" => $model,
             ])
        ;

        return module('shop')
             ->service('ware_row_actions')
             ->indexed('icon', 'caption', 'link', 'condition')
             ;
    }



    /**
     * get toolbar buttons from the service environment
     *
     * @return array
     */
    public function toolbarButtons()
    {
        service("shop:toolbar_buttons_handlers")->handle();
        $array = [];

        foreach (service("shop:toolbar_buttons")->read() as $service) {
            $array[$service['key']] = [
                 'target'  => $service['link'],
                 'caption' => $service['caption'],
                 'icon'    => $service['icon'],
                 "type"    => $service['color'] . " btn-taha",
            ];
        }

        return $array;
    }



    /**
     * get mass action buttons
     *
     * @return array
     */
    public function massActionsButton()
    {
        service("shop:mass_action_handlers")->handle();

        return service('shop:mass_actions')->indexed('icon', 'caption', 'link');
    }



    /**
     * save mass-activation of the wares
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function massActiveness(WareMassActivationRequest $request)
    {
        $done = model("ware")->whereIn("id", $request->ids)->update([
             "is_available" => $request->is_available,
             "updated_at"   => now(),
             "updated_by"   => user()->id,
        ])
        ;

        return $this->jsonAjaxSaveFeedback($done);
    }



    /**
     * call handler services
     */
    private function callServices()
    {
        service('shop:ware_browse_headings_handlers')->handle();
    }



    /**
     * @return array
     */
    private function setPage()
    {
        $posttype = posttype($this->posttype);
        return [
             ["shop/ware-list/", trans('shop::wares.list')],
             ["?type=" . $posttype->hashid, $posttype->title],
        ];
    }



    /**
     * Returns the first sortable field as the default sort by value.
     *
     * @return string
     */
    public function defaultSortBy()
    {
        return array_first(model('ware')->sortableFields());
    }



    /**
     * Returns the default sort direction.
     *
     * @return string
     */
    public function defaultSortDirection()
    {
        return 'asc';
    }

}
