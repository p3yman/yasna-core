<?php 

namespace Modules\Shop\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Yasna\Services\YasnaController;

class WidgetsController extends YasnaController
{
    protected $base_model  = ""; // @TODO <~~
    protected $view_folder = ""; // @TODO <~~ (Remove if same as parent)



    /**
     * Search for requested ware
     *
     * @param Request $request
     *
     * @return string
     * @throws \Throwable
     */
    public function searchWare(Request $request)
    {
        //dd($request->identifier);

        $no_result = false;

        $success_message = view(
             'shop::dashboard.result',
             compact('no_result')
             //compact('model', 'total_found', 'fi    eld', 'keyword', 'field')
        )->render();


        /*-----------------------------------------------
        | Result ...
        */
        return $this->jsonFeedback([
             'message'    => $success_message,
             'feed_class' => " ",
             
        ]);
    }
}
