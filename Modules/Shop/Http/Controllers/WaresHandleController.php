<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Ware;
use Modules\Yasna\Services\YasnaController;

class WaresHandleController extends YasnaController
{
    /**
     * handle wares list columns
     */
    public static function browseColumns()
    {
        module('shop')
             ->service('ware_browse_headings')
             ->add('name')
             ->trans("shop::wares.name")
             ->blade("shop::wares.list.cols.name")
             ->width('100')
             ->order(10)
        ;

        module('shop')
             ->service('ware_browse_headings')
             ->add('price')
             ->trans("shop::wares.tabs.prices")
             ->blade("shop::wares.list.cols.price")
             ->width('100')
             ->order(13)
        ;

        module('shop')
             ->service('ware_browse_headings')
             ->add('offers')
             ->trans("shop::offers.plural_title")
             ->blade("shop::wares.list.cols.offers")
             ->width('100')
             ->order(14)
        ;

        module('shop')
             ->service('ware_browse_headings')
             ->add('inventory')
             ->trans("shop::wares.sale_status")
             ->blade("shop::wares.list.cols.inventory")
             ->width('100')
             ->order(15)
        ;
    }



    /**
     * handle row actions
     *
     * @param array $data
     */
    public static function generateActions($data)
    {
        $model = $data['model'];

        module('shop')
             ->service('ware_row_actions')
             ->add('edit')
             ->caption(trans('shop::wares.edit'))
             ->link("modal:manage/shop/wares/edit/$model->hashid/0")
             ->icon('edit')
        ;

        module("shop")
             ->service("ware_row_actions")
             ->add("post-edit")
             ->caption(trans("posts::general.content_management"))
             ->link($model->post->edit_link)
             ->icon("file-text-o")
             ->condition($model->post->canEdit())
        ;

        module('shop')
             ->service('ware_row_actions')
             ->add('special-offer')
             ->caption(trans('shop::offers.singular_title'))
             ->link("modal:" . route('shop.wares.offers', ['hashid' => $model->hashid], false))
             ->icon('bullhorn')
        ;
    }



    /**
     * handle toolbar buttons
     */
    public static function toolbarButtons()
    {
        service("shop:toolbar_buttons")
             ->add("filter")
             ->trans("shop::wares.filters")
             ->icon("filter")
             ->link("$('#divFilters').slideToggle('fast')")
             ->color("default")
             ->order(1)
        ;

        service("shop:toolbar_buttons")
             ->add("export")
             ->trans("shop::wares.excel_export")
             ->icon("download")
             ->link(route('shop-wares-excel', request()->getQueryString()))
             ->color("info")
             ->order(2)
        ;
    }



    /**
     * register mass actions
     */
    public static function massActions()
    {
        $activeness_url = route("shop-ware-list-mass-action", [
             "action" => "activeness",
        ]);
        service("shop:mass_actions")
             ->add("activeness")
             ->icon("check")
             ->trans("shop::wares.availability_change")
             ->link("masterModal('$activeness_url')")
             ->order(20)
        ;
    }



    /**
     * Registers the filters
     */
    public static function filterForm()
    {
        $order = 0;

        $specific_filters = [
             'min_price'   => [
                  'purifier' => 'ed|numeric',
             ],
             'max_price'   => [
                  'purifier' => 'ed|numeric',
             ],
             'offer',
             'category',
             'availability',
             'product',
             'code'        => [
                  'purifier' => 'ed',
             ],
             'custom_code' => [
                  'purifier' => 'ed',
             ],
             'sort',
        ];

        foreach ($specific_filters as $key => $value) {
            $order++;

            if (is_numeric($key)) {
                $blade = $value;
                $info  = [];
            } else {
                $blade = $key;
                $info  = $value;
            }


            static::registerFilterItem($blade, $order, $info);
        }

    }



    /**
     * Registers an specific filter with the given data
     *
     * @param string $blade
     * @param int    $order
     * @param array  $info
     */
    public static function registerFilterItem(string $blade, int $order, array $info = [])
    {
        $module = module('shop');

        // register the service's self
        $service = service($module->getAlias() . ':wares_filter')
             ->add($blade)
             ->blade($module->getBladePath("wares.list.filter.$blade"))
             ->order($order)
        ;

        // add the extra fields of the service
        foreach ($info as $key => $value) {
            $service->set($key, $value);
        }
    }



    /**
     * Registers ware export columns.
     */
    public static function exportColumns()
    {
        $service = service('shop:ware_export_columns');


        $service
             ->add('name')
             ->trans("shop::wares.name")
             ->order(10)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->title;
                  },
             ])
        ;

        $service
             ->add('product')
             ->trans("shop::wares.product")
             ->order(10)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->post->title;
                  },
             ])
        ;

        $service
             ->add('code')
             ->trans("shop::wares.code")
             ->order(11)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->code;
                  },
             ])
        ;

        $service
             ->add('custom_code')
             ->trans("shop::wares.custom_code")
             ->order(12)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->custom_code;
                  },
             ])
        ;

        $service
             ->add('original_price')
             ->trans("shop::prices.main")
             ->order(13)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->original_price;
                  },
             ])
        ;

        $service
             ->add('sale_price')
             ->trans("shop::prices.sale")
             ->order(14)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->original_price;
                  },
             ])
        ;

        $service
             ->add('offers')
             ->trans("shop::offers.plural_title")
             ->order(15)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->offers->pluck('title')->implode(", ");
                  },
             ])
        ;

        $service
             ->add('inventory')
             ->trans("shop::inventories.title")
             ->order(16)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->current_inventory;
                  },
             ])
        ;

        $service
             ->add('available')
             ->trans("shop::wares.availability_positive")
             ->order(17)
             ->value([
                  'parser' => function (Ware $ware) {
                      return $ware->is_available;
                  },
             ])
        ;
    }
}
