<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Post;
use App\Models\Ware;
use Modules\Shop\Events\WaresReordered;
use Modules\Shop\Events\WareUpdated;
use Modules\Shop\Http\Requests\QuickSavePriceRequest;
use Modules\Shop\Http\Requests\WareChangeAvailabilityRequest;
use Modules\Shop\Http\Requests\WareOrderRequest;
use Modules\Shop\Http\Requests\WareSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class WaresEditController extends YasnaController
{
    protected $base_model  = "Ware";
    protected $view_folder = "shop::wares";
    protected $editor_tabs = ['titles', 'packages', 'inventories', 'prices', 'sizes', 'remarks', 'settings'];



    /**
     * Saves the record from submitted form data
     *
     * @param WareSaveRequest $request
     *
     * @return string
     */
    public function save(WareSaveRequest $request)
    {
        if ($request->_submit == 'delete') {
            return $this->delete($request);
        }
        $old_model = clone $request->model;

        /**
         * @var Ware $saved_model
         */
        $saved_model = $request->model->batchSave($request);
        if ($saved_model->exists) {
            $this->setWareRelations($request, $saved_model);
            $this->raiseSaveEvent($saved_model, $old_model);
        }

        $update_callback = $this->saveUpdateCallback($saved_model);

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             'success_callback'   => "$('#btnCancel').click();"
                  . $update_callback,
             'success_modalClose' => false,
        ]);
    }



    /**
     * Returns the part of save action's callback which depends on the caller referrer URL.
     *
     * @param Ware $model
     *
     * @return string
     */
    protected function saveUpdateCallback(Ware $model)
    {
        $previous_url   = url()->previous();
        $wares_list_url = route('shop-wares-list');

        if ($previous_url == $wares_list_url) {
            return "rowUpdate('auto', '$model->hashid')";
        } else {
            $post_hashid = $model->post->hashid;
            return "rowUpdate('tblPosts', '$post_hashid');";
        }
    }



    /**
     * Deletes a ware record
     *
     * @param WareSaveRequest $request
     *
     * @return string
     */
    protected function delete(WareSaveRequest $request)
    {
        $refresh_link = $request->model->post->ware_index_link;
        $post_hashid  = $request->model->post->hashid;
        $ok           = $request->model->delete();

        if ($ok) {
            $this->raiseSaveEvent($request->model);
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback'   => "masterModal(url('$refresh_link'));rowUpdate('tblPosts', '{$post_hashid}');",
             'success_modalClose' => false,
        ]);
    }



    /**
     * Handles save order action
     *
     * @param WareOrderRequest $request
     *
     * @return string
     */
    public function saveOrder(WareOrderRequest $request)
    {
        foreach ($request->order as $order => $hashid) {
            model('Ware', $hashid)->update([
                 "order" => $order + 1,
            ]);
        }
        event(new WaresReordered($request->model));

        $refresh_link = $request->model->ware_index_link;
        return $this->jsonAjaxSaveFeedback(true, [
             'success_callback'   => "masterModal(url('$refresh_link'))",
             'success_modalClose' => false,
        ]);
    }



    /**
     * save price quickly
     *
     * @param QuickSavePriceRequest $request
     *
     * @return string
     */
    public function savePriceQuickly(QuickSavePriceRequest $request)
    {
        $ok = false;
        /**
         * @var Ware $model
         */
        $model = $request->model;


        if (posttype($model->posttype_id)->cannotShop()) {
            return $this->abort(403);
        }

        if ($request->has('original_price')) {
            $ok = $model->setPrice($request->original_price, [
                 'currency' => $model->default_currency,
            ]);
        }


        if ($request->has('sale_price')) {
            $ok = $model->setPrice($request->sale_price, [
                 'currency'   => $model->default_currency,
                 'type'       => "sale",
                 'expires_at' => ($request->sale_price != 0) ? $request->discount_expires_at : null,
            ]);

        }


        $this->raiseSaveEvent($model);

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('tblWares', '$model->hashid');",
        ]);
    }



    /**
     * Sets all ware relations
     *
     * @param WareSaveRequest $request
     * @param Ware            $model
     *
     * @return void
     */
    protected function setWareRelations($request, $model): void
    {
        $this->setInventories($request, $model);
        $model->setPrices($request->toArray());
        $model->saveLabels($request->_labels);
    }



    /**
     * Raises a save event
     *
     * @param Ware      $model
     * @param Ware|null $old_model
     *
     * @return void
     */
    protected function raiseSaveEvent($model, $old_model = null): void
    {
        event(new WareUpdated($model, $old_model));
    }



    /**
     * Sets Inventory data on the saved model
     *
     * @param WareSaveRequest $request
     * @param Ware            $model
     *
     * @return void
     */
    private function setInventories($request, $model): void
    {
        if ($model->posttype->hasnot('shop_inventory')) {
            return;
        }

        $array = [
             "alteration" => $request->_inventory_alteration,
             "fee"        => $request->_inventory_fee,
        ];

        if ($request->model->exists) {
            $array['type']        = $request->_inv_reason;
            $array['description'] = $request->_inv_description;
            $array['account_id']  = $request->_inventory_account;
        }

        $model->setInventory($array);
    }



    /**
     * Prepares editor form for the creation purpose.
     *
     * @param string $post_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm($post_hashid)
    {
        $post = post($post_hashid);
        if (!$post->exists or $post->hasnot('shop')) {
            return $this->abort(410);
        }
        if ($post->cannotShop()) {
            return $this->abort(403);
        }

        $model       = $this->newWareInstance($post);
        $tab_list    = $this->editorTabs($model);
        $tab_default = 'titles';
        $modal_title = trans_safe("shop::wares.new");

        $back_to_list = true;

        return $this->view('editor',
             compact('model', 'post_hashid', 'tab_list', 'modal_title', 'tab_default', 'back_to_list'));
    }



    /**
     * Prepares editor for the edition purpose.
     *
     * @param string   $ware_hashid
     * @param int|bool $back_to_list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editForm($ware_hashid, $back_to_list = true)
    {
        $model = ware($ware_hashid);
        if (!$model->exists) {
            return $this->abort(410);
        }
        if ($model->post->cannot('shop')) {
            return $this->abort(403);
        }

        $post_hashid = $model->post->hashid; // <~~ Used on form Cancel Button
        $tab_list    = $this->editorTabs($model);
        $tab_default = "prices";
        $modal_title = trans_safe("shop::wares.edit");

        return $this->view('editor',
             compact('model', 'post_hashid', 'tab_list', 'modal_title', 'tab_default', 'back_to_list'));
    }



    /**
     * Creates a fresh new instance of Ware model, enriched with post data
     *
     * @param Post $post
     *
     * @return Ware
     */
    private function newWareInstance(Post $post): Ware
    {
        $ware               = ware();
        $ware->sisterhood   = $post->sisterhood;
        $ware->posttype_id  = $post->posttype->id;
        $ware->is_available = true;

        return $ware;
    }



    /**
     * Gets an array of editor tabs
     *
     * @param Ware $ware
     *
     * @return array
     */
    protected function editorTabs(Ware $ware): array
    {
        $array = $this->editor_tabs;

        if ($ware->posttype->hasnot('shop_inventory')) {
            array_splice($array, array_search('inventories', $array), 1);
        }

        return $array;
    }



    /**
     * Change the `is_available` field of the requested ware.
     *
     * @param WareChangeAvailabilityRequest $request
     *
     * @return string
     */
    public function saveAvailability(WareChangeAvailabilityRequest $request)
    {
        /** @var Ware $saved */
        $saved = $request->model->batchSave([
             "is_available" => boolval($request->is_available),
        ]);

        $this->raiseSaveEvent($saved, $request->model);

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             'success_callback' => "rowUpdate('tblWares', '$request->model_hashid');",
        ]);
    }
}
