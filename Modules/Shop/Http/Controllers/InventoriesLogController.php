<?php

namespace Modules\Shop\Http\Controllers;

use Modules\Shop\Http\Requests\InventoriesLogRequest;
use Modules\Yasna\Services\YasnaController;

class InventoriesLogController extends YasnaController
{
    protected $base_model  = "Inventory";
    protected $view_folder = "shop::inventories";



    /**
     * get the browse index page
     *
     * @param InventoriesLogRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(InventoriesLogRequest $request)
    {
        $models      = $this->getBrowseEloquentBuilder($request)->paginate(20);
        $page        = $this->getBrowseBreadcrumbs($request);
        $filters     = $this->getBrowseFilters($request);
        $is_filtered = $this->isBrowseFiltered($request);

        return $this->view("browse.index", compact("models", "page", "is_filtered", "filters"));
    }



    /**
     * get Eloquent builder instance for browse
     *
     * @param InventoriesLogRequest $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function getBrowseEloquentBuilder(InventoriesLogRequest $request)
    {
        return model('inventory')
             ->elector($request->getElectors())
             ->orderBy("effected_at", "DESC")
             ;
    }



    /**
     * get browse page breadcrumbs
     *
     * @param InventoriesLogRequest $request
     *
     * @return array
     */
    private function getBrowseBreadcrumbs(InventoriesLogRequest $request)
    {
        return [
             [
                  "shop/ware-list",
                  trans("shop::wares.title"),
             ],
             [
                  "shop/inventories-log/browse",
                  trans('shop::inventories.log'),
             ],
        ];
    }



    /**
     * get browse filters array
     *
     * @param InventoriesLogRequest $request
     *
     * @return array
     */
    private function getBrowseFilters(InventoriesLogRequest $request)
    {
        return [
             "ware"   => $request->ware,
             "reason" => $request->reason,
        ];
    }



    /**
     * specify if the browse panel is filtered
     *
     * @param InventoriesLogRequest $request
     *
     * @return bool
     */
    private function isBrowseFiltered(InventoriesLogRequest $request)
    {
        return boolval($request->_submit);
    }
}
