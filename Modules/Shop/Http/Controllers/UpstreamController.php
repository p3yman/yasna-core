<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Unit;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Manage\Traits\ManageControllerTrait;
use Modules\Shop\Http\Requests\UnitSaveRequest;

class UpstreamController extends Controller
{
    use ManageControllerTrait ;

    /*
    |--------------------------------------------------------------------------
    | Units
    |--------------------------------------------------------------------------
    |
    */
    public static function unitIndex($search_request)
    {
        /*-----------------------------------------------
        | View File ...
        */
        $result['view_file'] = "shop::upstream.units";

        /*-----------------------------------------------
        | Search Request ...
        */
        $keyword = $search_request->keyword;

        /*-----------------------------------------------
        | Model ...
        */
        $result['models'] = Unit::withTrashed()->orderBy('slug')->paginate(user()->preference('max_rows_per_page'));

        /*-----------------------------------------------
        | Return ...
        */

        return $result;
    }

    public function unitAction($action, $hashid = false)
    {
        /*-----------------------------------------------
        | Action Check ...
        */
        if (!in_array($action, ['edit', 'create', 'activeness', 'row' ])) {
            return $this->abort(404, true);
        }

        /*-----------------------------------------------
        | Model ...
        */
        if (in_array($action, ['create'])) {
            $model = new Unit();
        } else {
            $model = Unit::findByHashid($hashid, true, [
                'with_trashed' => true,
            ]);
            if (!$model or !$model->id) {
                return $this->abort(410, true);
            }
        }


        /*-----------------------------------------------
        | View ...
        */
        if ($action == 'create') {
            $view_file = "shop::upstream.units-edit";
        } else {
            $view_file = "shop::upstream.units-$action";
        }


        return view($view_file, compact('model'));
    }

    public function unitActiveness(Request $request)
    {
        switch ($request->toArray()['_submit']) {
            case 'delete':
                $model = Unit::find($request->id);
                if (!$model or $model->slug == 'root') {
                    return $this->jsonFeedback(trans('manage::forms.general.sorry'));
                }
                $ok = Unit::where('id', $request->id)->delete();
                break;

            case 'restore':
                $ok = Unit::withTrashed()->where('id', $request->id)->restore();
                break;

            default:
                $ok = false;
        }

        $hashid = hashid($request->id) ;

        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "rowUpdate('tblUnits','$hashid')",
        ]);
    }

    public function unitSave(UnitSaveRequest $request)
    {
        /*-----------------------------------------------
        | Model Detection ...
        */
        $id = hashid($request->hashid) ;
        if ($id) {
            $model = Unit::withTrashed()->find($id);
            if (!$model) {
                return $this->jsonFeedback();
            }
        }

        /*-----------------------------------------------
        | Save ...
        */
        $ok = Unit::store($request);

        /*-----------------------------------------------
        | Feedback ...
        */
        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "rowUpdate('tblUnits','$request->hashid')",
        ]);
    }
}
