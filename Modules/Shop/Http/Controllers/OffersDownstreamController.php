<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\ShopOffer;
use Modules\Shop\Http\Requests\ShopItemsActionRequest;
use Modules\Shop\Http\Requests\ShopOfferBinRequest;
use Modules\Shop\Http\Requests\ShopOfferDeleteRequest;
use Modules\Shop\Http\Requests\ShopOfferSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class OffersDownstreamController extends YasnaController
{
    protected $base_model  = "shop-offer";
    protected $view_folder = "shop::downstream.offers";



    /**
     * get an index view of the downstream settings, offers tab.
     *
     * @return array
     */
    public static function index()
    {
        return [
             "view_file" => "shop::downstream.offers.index",
             "models"    => model("shop-offer")->withTrashed()->orderBy("title")->paginate(50),
        ];
    }



    /**
     * get a refreshed blade of grid row for the given hashid
     *
     * @param $hashid
     */
    public function refreshRow($hashid)
    {
        return $this->view("row", [
             "model" => $this->model($hashid, true),
        ]);
    }



    /**
     * get the editor blade for a new instance
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm()
    {
        return $this->view("edit", [
             "model" => $this->model(),
        ]);
    }



    /**
     * save the editor form
     *
     * @param ShopOfferSaveRequest $request
     *
     * @return string
     */
    public function save(ShopOfferSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             'success_callback' => "rowUpdate('tblOffers','$request->hashid')",
        ]);
    }



    /**
     * soft-delete the given record
     *
     * @param ShopOfferDeleteRequest $request
     *
     * @return string
     */
    public function delete(ShopOfferDeleteRequest $request)
    {
        $done = $request->model->delete();

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback' => "rowUpdate('tblOffers','$request->hashid')",
        ]);
    }



    /**
     * undelete the given record
     *
     * @param ShopOfferBinRequest $request
     *
     * @return string
     */
    public function undelete(ShopOfferBinRequest $request)
    {
        $done = $request->model->undelete();

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback' => "rowUpdate('tblOffers','$request->hashid')",
        ]);
    }



    /**
     * undelete the given record
     *
     * @param ShopOfferBinRequest $request
     *
     * @return string
     */
    public function destroy(ShopOfferBinRequest $request)
    {
        $done = $request->model->hardDelete();

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback' => "rowHide('tblOffers','$request->hashid')",
        ]);
    }



    /**
     * get the index view of the wares list
     *
     * @param ShopOffer $offer
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function itemsForm($offer)
    {
        $modal_label = $offer->title . ": " . $offer->item_management_title;
        $models      = $offer->wares;

        return $this->view("wares.index", compact("offer", "models", "modal_label"));
    }



    /**
     * handle actions on the items list-view
     *
     * @param ShopItemsActionRequest $request
     *
     * @return string
     */
    public function itemsAction(ShopItemsActionRequest $request)
    {
        $method = camel_case("items-$request->action");

        if ($this->hasMethod($method)) {
            return $this->$method($request->model, $request->item);
        }

        return $this->jsonFeedback($method);
    }



    /**
     * delete an item from the offer
     *
     * @param ShopOffer $model
     * @param string    $hashid
     *
     * @return string
     */
    public function itemsDelete($model, $hashid)
    {
        $done = $model->detachWares(hashid($hashid));

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback'   => $model->getActionLink('items').";rowUpdate('tblOffers','$model->hashid')",
             "success_modalClose" => false,
        ]);
    }
}
