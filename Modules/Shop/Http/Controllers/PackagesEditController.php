<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Posttype;
use App\Models\Ware;
use Modules\Shop\Events\PackageUpdated;
use Modules\Shop\Http\Requests\WareSaveRequest;

class PackagesEditController extends WaresEditController
{
    protected $editor_tabs = ['titles', 'inventories', 'prices', 'settings'];



    /**
     * @inheritdoc
     */
    public function save(WareSaveRequest $request)
    {
        if ($request->_submit == 'delete') {
            return $this->delete($request);
        }

        /**
         * @var Ware $saved_model
         */
        $saved_model = $request->model->batchSave($request);

        if ($saved_model->exists) {
            $this->setWareRelations($request, $saved_model);
            $this->raiseSaveEvent($saved_model);
        }

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             'success_callback' => "divReload('divPackage-" . $saved_model->posttype->hashid . "' )",
        ]);
    }



    /**
     * @inheritdoc
     */
    protected function delete(WareSaveRequest $request)
    {
        $refresh_link = $request->model->posttype->hashid;
        $ok           = $request->model->delete();

        if ($ok) {
            $this->raiseSaveEvent($request->model);
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "divReload('divPackage-$refresh_link')",
        ]);
    }



    /**
     * @inheritdoc
     */
    protected function raiseSaveEvent($model): void
    {
        event(new PackageUpdated($model));
    }



    /**
     * @inheritdoc
     */
    public function editForm($ware_hashid, $back_to_list = true)
    {
        $model = ware($ware_hashid);
        if (!$model->exists) {
            return $this->abort(410);
        }
        if ($model->post->cannot('shop')) {
            return $this->abort(403);
        }

        $post_hashid = 0;
        $tab_list    = $this->editorTabs($model);
        $modal_title = trans("shop::packages.edit");
        $tab_default = 'prices';

        return $this->view('editor', compact('model', 'post_hashid', 'tab_list', 'modal_title', 'tab_default'));
    }



    /**
     * @inheritdoc
     */
    public function createForm($posttype_hashid)
    {
        $posttype = posttype($posttype_hashid);
        if (!$posttype->exists) {
            return $this->abort(410);
        }
        if ($posttype->cannot('setting')) {
            return $this->abort(403);
        }

        $model       = $this->newWareInstance($posttype);
        $tab_list    = $this->editorTabs($model);
        $tab_default = 'titles';
        $modal_title = trans("shop::packages.create");
        $post_hashid = 0;

        return $this->view('editor', compact('model', 'post_hashid', 'tab_list', 'modal_title', 'tab_default'));
    }



    /**
     * Creates a fresh new instance of Ware model, enriched with post data
     *
     * @param Posttype $posttype
     *
     * @return Ware
     */
    private function newWareInstance(Posttype $posttype): Ware
    {
        $ware               = ware();
        $ware->sisterhood   = null;
        $ware->posttype_id  = $posttype->id;
        $ware->is_available = true;

        return $ware;
    }
}
