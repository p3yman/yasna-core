<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Post;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Shop\Http\Requests\PostOffersRequest;
use Modules\Shop\Http\Requests\PostSaveOffersRequest;
use Modules\Shop\Http\Requests\WareSingleOffersRequest;
use Modules\Shop\Http\Requests\WareSingleSaveOffersRequest;
use Modules\Yasna\Services\YasnaController;
use Modules\Yasna\Services\YasnaRequest;

class WaresOffersController extends YasnaController
{
    /**
     * The Main Model
     *
     * @var string
     */
    protected $base_model = "ware";
    /**
     * The Main Views' Folder
     *
     * @var string
     */
    protected $view_folder = "shop::wares";



    /**
     * Renders the content of the single special offers modal.
     *
     * @param WareSingleOffersRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(WareSingleOffersRequest $request)
    {
        return $this->view('offers', [
             'model'        => $request->model,
             'offers_combo' => $this->getOffersCombo(),
        ]);
    }



    /**
     * Returns an array for combo of offers.
     *
     * @return array
     */
    protected function getOffersCombo()
    {
        return model('shop-offer')
             ->all()
             ->map(function ($model) {
                 return $model->only(['slug', 'title']);
             })->toArray()
             ;
    }



    /**
     * Saves the offers of a single ware.
     *
     * @param WareSingleSaveOffersRequest $request
     *
     * @return string
     */
    public function singleSave(WareSingleSaveOffersRequest $request)
    {
        $this->saveWareOffers($request);

        return $this->jsonAjaxSaveFeedback(1, [
             'success_message'  => $this->module()->getTrans('offers.list_updated'),
             'success_callback' => "rowUpdate('tblWares', '$request->model_hashid')",
        ]);
    }



    /**
     * Saves the offers of the ware.
     *
     * @param WareSingleSaveOffersRequest $request
     */
    protected function saveWareOffers(WareSingleSaveOffersRequest $request)
    {
        $ware           = $request->model;
        $new_offers     = $this->requestedOffersCollection($request);
        $current_offers = $ware->offers;
        $detaching      = $current_offers->diff($new_offers);
        $attaching      = $new_offers->diff($current_offers);

        $this->attachWareOffers($attaching, $ware);
        $this->detachWareOffers($detaching, $ware);
    }



    /**
     * Attaches the ware to the attaching offers.
     *
     * @param Collection $attaching
     * @param Ware       $ware
     */
    protected function attachWareOffers(Collection $attaching, Ware $ware)
    {
        foreach ($attaching as $offer) {
            $offer->attachWares([$ware->id]);
        }
    }



    /**
     * Detaches the ware from the detaching offers.
     *
     * @param Collection $detaching
     * @param Ware       $ware
     */
    protected function detachWareOffers(Collection $detaching, Ware $ware)
    {
        foreach ($detaching as $offer) {
            $offer->detachWares($ware->id);
        }
    }



    /**
     * Returns a collection of offers which their slug is are the request.
     *
     * @param YasnaRequest $request
     *
     * @return Collection
     */
    protected function requestedOffersCollection(YasnaRequest $request)
    {
        return model('shop-offer')
             ->whereIn('slug', $request->offers)
             ->get()
             ;
    }



    /**
     * Renders a form to add wares of a post to special offers.
     *
     * @param PostOffersRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function postOffers(PostOffersRequest $request)
    {
        $model          = $request->model;
        $offers_combo   = $this->getOffersCombo();
        $wares          = $model->wares;
        $current_offers = $this->getWaresGeneralOffers($wares);

        return $this->view('post-offers', compact('model', 'offers_combo', 'wares', 'current_offers'));
    }



    /**
     * Returns the intersect of offers of all wares based on the given wares.
     *
     * @param Collection|HasMany $wares
     *
     * @return Collection
     */
    protected function getWaresGeneralOffers($wares)
    {
        $wares_offers = $wares->pluck('offers');
        $intersect    = $wares_offers->first();

        foreach ($wares_offers as $ware_offers) {
            $intersect = $intersect->intersect($ware_offers);
        }

        return $intersect;
    }



    /**
     * Saves th offers of wares of the specified post and returns a proper response.
     *
     * @param PostSaveOffersRequest $request
     *
     * @return string
     */
    public function postOffersSave(PostSaveOffersRequest $request)
    {
        $this->saveWaresOffers($request);

        return $this->jsonAjaxSaveFeedback(1, [
             'success_message'  => $this->module()->getTrans('offers.list_updated'),
             'success_callback' => "rowUpdate('tblPosts', '$request->model_hashid')",
        ]);
    }



    /**
     * Saves th offers of wares of the specified post.
     *
     * @param PostSaveOffersRequest $request
     */
    protected function saveWaresOffers(PostSaveOffersRequest $request)
    {
        $offers    = $this->requestedOffersCollection($request);
        $wares     = $request->model->wares;
        $wares_ids = $wares->pluck('id')->toArray();

        foreach ($offers as $offer) {
            $offer->attachWares($wares_ids);
        }
    }
}
