<?php

namespace Modules\Shop\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class WaresController extends YasnaController
{
    protected $view_folder = "shop::wares";
    protected $base_model  = "Ware";



    /**
     * Lists the wares relative to a particular post
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(string $hashid)
    {
        $post = post($hashid);
        if (!$post->exists or $post->hasnot('shop')) {
            return $this->abort(410);
        }
        if ($post->cannotShop()) {
            return $this->abort(403);
        }

        $total_wares    = $post->wares()->count();
        $view_arguments = [
             "model"       => $post,
             "modal_title" => $this->indexModalTitle($total_wares),
        ];

        if (!$total_wares) {
            return $this->view('index-empty', $view_arguments);
        }

        return $this->view('index', $view_arguments);
    }



    /**
     * Makes Index modal titles
     *
     * @param int $total_wares
     *
     * @return string
     */
    private function indexModalTitle(int $total_wares)
    {
        $main_title  = trans("shop::wares.list");
        $total_wares = pd($total_wares);

        return "$main_title ($total_wares) ";
    }
}
