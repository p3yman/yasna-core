<?php

namespace Modules\Shop\Http\Controllers;

use App\Models\Posttype;
use App\Models\Price;
use App\Models\Ware;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Manage\Traits\ManageControllerTrait;
use Modules\Shop\Http\Requests\PackageInventorySaveRequest;
use Modules\Shop\Http\Requests\PackageSaveRequest;

class DownstreamController extends Controller
{
    use ManageControllerTrait;

    public function __construct()
    {
        $this->middleware('is:super');
    }

    protected function view($file, $data)
    {
        return module('shop')->view("downstream." . $file, $data);
    }

    /*
    |--------------------------------------------------------------------------
    | Packages
    |--------------------------------------------------------------------------
    |
    */

    public static function packageIndex()
    {
        /*-----------------------------------------------
        | View File ...
        */
        $result['view_file'] = "shop::downstream.packages.posttypes";

        /*-----------------------------------------------
        | Model ...
        */
        $selector         = [
            'features' => "shop",
        ];
        $result['models'] = Posttype::selector($selector)->orderBy('order')->orderBy('title')->get();

        /*-----------------------------------------------
        | Return ...
        */

        return $result;
    }

    public function packageList($posttype_hashid)
    {
        /*-----------------------------------------------
        | Posttype ...
        */
        $posttype = Posttype::findByHashid($posttype_hashid);
        if (!$posttype or !$posttype->exists) {
            return $this->abort(410);
        }
        if ($posttype->cannot('setting')) {
            return $this->abort(403) ;
        }

        /*-----------------------------------------------
        | Wares ...
        */
        $models = Ware::selector([
            'posttype_id' => $posttype->id,
            'sisterhood'  => null,
        ])
                      ->orderBy('updated_at', 'desc')
                      ->get()
        ;

        /*-----------------------------------------------
        | View ...
        */
        if ($models->count()) {
            $view = 'list';
        } else {
            $view = 'empty';
        }

        return $this->view("packages.$view", compact("posttype", "models"));
    }
}
