<?php

Route::group(
     [
          'middleware' => ['web', 'auth'],
          'prefix'     => 'manage/shop',
          'namespace'  => 'Modules\Shop\Http\Controllers',
     ],
     function () {
         Route::get('/search-ware', 'WidgetsController@searchWare')->name('search-ware');
         Route::get('/ware-list', 'WareListController@index')->name('shop-wares-list');
         Route::get('excel', 'WareListController@excel')->name('shop-wares-excel');
         Route::get('mass/{action}/{option?}', 'WareListController@massAction')->name("shop-ware-list-mass-action");

         Route::post('activeness' , 'WareListController@massActiveness')->name("shop-ware-list-mass-activeness");
     }
);

