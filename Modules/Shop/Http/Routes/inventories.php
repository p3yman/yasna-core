<?php

Route::group([
    'middleware' => 'web', 
    'namespace'  => module('Shop')->getControllersNamespace(), 
    'prefix'     => 'manage/shop/inventories-log'
], function () {
    Route::get('/' , 'InventoriesLogController@index');
    Route::get('/browse' , 'InventoriesLogController@index');
});
