<?php

Route::group([
     'middleware' => ['web', 'auth', "is:super"],
     'namespace'  => module('Shop')->getControllersNamespace(),
     'prefix'     => 'manage/shop/downstream/offers',
], function () {
    Route::get('refresh/{hashid}', 'OffersDownstreamController@refreshRow')->name("shop-downstream-offer-refresh");
    Route::get('action/{hashid}/{action}', 'OffersDownstreamController@singleAction')
         ->name("shop-downstream-offer-action")
    ;

    Route::post('save', 'OffersDownstreamController@save')->name("shop-downstream-offer-save");
    Route::post('delete', 'OffersDownstreamController@delete')->name("shop-downstream-offer-delete");
    Route::post('undelete', 'OffersDownstreamController@undelete')->name("shop-downstream-offer-undelete");
    Route::post('destroy', 'OffersDownstreamController@destroy')->name("shop-downstream-offer-destroy");
    Route::post('items', 'OffersDownstreamController@itemsAction')->name("shop-downstream-offer-items");
});

