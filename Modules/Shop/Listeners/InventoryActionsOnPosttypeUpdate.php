<?php

namespace Modules\Shop\Listeners;

use Modules\Posts\Events\PosttypeUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InventoryActionsOnPosttypeUpdate implements ShouldQueue
{
    public $tries = 5;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param PosttypeUpdated $event
     */
    public function handle(PosttypeUpdated $event)
    {
        $array = [] ;
        foreach ($event->posttype->wares as $ware) {
            $count = $ware->refreshCurrentInventory();
            $array[$ware->id] = $count ;
        }

        //cache()->set('test' , $array , 10);
    }
}
