<?php namespace Modules\Shop\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Shop\Events\PackageUpdated;

class CacheRefreshOnPackageUpdated implements ShouldQueue
{
    public $tries = 5;

    public function handle(PackageUpdated $event)
    {
        $package = $event->model;

        /*-----------------------------------------------
        | Inventory ...
        */
        $package->refreshCurrentInventory();
        $package->refreshCurrentPrices();
        $package->insiders()->update([
            'package_inventory' => $package->current_inventory ,
             'has_enough_package' => $package->has_enough_inventory ,
        ]);

        /*-----------------------------------------------
        | Prices ...
        */
        foreach ($package->insiders()->get() as $ware) {
            $ware->cachePrices();
        }
    }
}
