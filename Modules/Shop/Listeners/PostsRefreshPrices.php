<?php namespace Modules\Shop\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;

class PostsRefreshPrices implements ShouldQueue
{
    public $tries = 5;



    public function handle($event)
    {
        $model   = $event->model;
        $method  = "sistersWhen" . $model->className() . "Given";
        $sisters = $this->$method($model);

        foreach ($sisters as $post) {
            $post->refreshCurrentPrices();
        }
    }



    public function sistersWhenWareGiven($model)
    {
        return $model->post->sisters;
    }



    public function sistersWhenPostGiven($model)
    {
        return $model->sisters;
    }
}
