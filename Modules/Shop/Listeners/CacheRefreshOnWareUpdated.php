<?php namespace Modules\Shop\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Shop\Events\WareUpdated;

class CacheRefreshOnWareUpdated implements ShouldQueue
{
    public $tries = 5;



    /**
     * handle the listener
     *
     * @param WareUpdated $event
     *
     * @return void
     */
    public function handle(WareUpdated $event)
    {
        $event->model->refreshCurrentInventory();
        $event->model->cachePrices();
    }
}
