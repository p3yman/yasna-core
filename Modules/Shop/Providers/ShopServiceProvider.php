<?php

namespace Modules\Shop\Providers;

use Illuminate\Support\Facades\Schema;
use Modules\Posts\Events\PosttypeUpdated;
use Modules\Shop\Console\ShopResetCommand;
use Modules\Shop\CouponRules\SpecialSaleRule;
use Modules\Shop\Events\PackageUpdated;
use Modules\Shop\Events\WaresReordered;
use Modules\Shop\Events\WareUpdated;
use Modules\Shop\Http\Endpoints\V1\PostsFilterEndpoint;
use Modules\Shop\Listeners\CacheRefreshOnPackageUpdated;
use Modules\Shop\Listeners\CacheRefreshOnWareUpdated;
use Modules\Shop\Listeners\InventoryActionsOnPosttypeUpdate;
use Modules\Shop\Listeners\PostsRefreshPrices;
use Modules\Shop\Schedules\RemoveExpiredDiscountsSchedule;
use Modules\Yasna\Services\YasnaProvider;

class ShopServiceProvider extends YasnaProvider
{

    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerServices();
        $this->registerBottomAssets();
        $this->registerModelTraits();
        $this->registerUpstreamTabs();
        $this->registerDownstream();
        $this->registerHandlers();
        $this->registerFilters();
        $this->registerAliases();
        $this->registerSchedules();
        $this->registerListeners();
        $this->registerArtisans();
        $this->registerSideBar();
        $this->registerConsistentCouponRules();
        //$this->registerWidgets();
        $this->registerEndpoints();
    }



    /**
     * register sidebar item
     */
    private function registerSideBar()
    {
        service('manage:sidebar')
             ->add('ware-list')
             ->blade('shop::wares.sidebar')
             ->order(90)
             ->condition(function () {
                 if (!Schema::hasTable('posttypes')) {
                     return false;
                 }

                 $posttypes = posttype()->whereHas('features', function ($q) {
                     $q->where('slug', 'shop');
                 })->get()
                 ;
                 foreach ($posttypes as $posttype) {
                     if (method_exists($posttype, 'canShop')) {
                         if ($posttype->canShop()) {
                             return true;
                         }
                     }
                 }
                 return false;
             })
        ;

        if (config("shop.reform_posts_sidebar")) {
            service("manage:sidebar")
                 ->update("posts")
                 ->blade("shop::posts.alternative-sidebar")
            ;
        }
    }



    /**
     * register model traits
     */
    protected function registerModelTraits()
    {
        /*-----------------------------------------------
        | Ware Model ...
        */

        module('yasna')
             ->service('traits')
             ->add()->trait("Shop:CategoryShopTrait")->to("Category")
             ->add()->trait("Shop:PosttypeShopTrait")->to("Posttype")
             ->add()->trait("Shop:PostShopTrait")->to("Post")
             ->add()->trait('Shop:PostShopChainTrait')->to("post")
        ;

        $this->addModelTrait('PostOfferTrait', 'Post');
        $this->addModelTrait('PostShopFilterTrait', 'Post');
        $this->addModelTrait('PostShopResourcesTrait', 'Post');
    }



    /**
     * register upstream tabs
     */
    protected function registerUpstreamTabs()
    {
        module('manage')
             ->service('upstream_tabs')
             ->add()
             ->link('units')
             ->trans("shop::units.title")
             ->method('shop:UpstreamController@unitIndex')
             ->order(81)
        ;
    }



    /**
     * Registers downstream settings tabs, required for the purpose of this
     * module.
     */
    protected function registerDownstream()
    {
        service('manage:downstream')
             ->add('shop-packages')
             ->link('packages')
             ->trans('shop::packages.plural')
             ->method('shop:DownstreamController@packageIndex')
             ->order(31)
        ;

        service('manage:downstream')
             ->add('shop-labels')
             ->link('shop-labels')
             ->trans('shop::labels.title')
             ->method("shop:LabelsDownstreamController@index")
             ->order(32)
        ;

        service('manage:downstream')
             ->add('shop-offers')
             ->link('shop-offers')
             ->trans('shop::offers.plural_title')
             ->method("shop:OffersDownstreamController@index")
             ->order(33)
        ;
    }



    /**
     * register handlers
     */
    protected function registerHandlers()
    {
        module('posts')
             ->service('browse_headings_handlers')
             ->add('shop')
             ->method("shop:handleBrowseHeadings")
        ;
        module('posts')
             ->service('row_action_handlers')
             ->add('shop')
             ->method('shop:handleBrowseRowActions')
        ;

        module('shop')
             ->service('ware_browse_headings_handlers')
             ->add('shop')
             ->method("shop:WaresHandleController@browseColumns")
        ;

        module('shop')
             ->service('ware_row_action_handlers')
             ->add('shop')
             ->method("shop:WaresHandleController@generateActions")
        ;

        module("shop")
             ->service("toolbar_buttons_handlers")
             ->add("shop")
             ->method("shop:WaresHandleController@toolbarButtons")
        ;

        module("shop")
             ->service("mass_action_handlers")
             ->add("shop")
             ->method("shop:WaresHandleController@massActions")
        ;

        module("shop")
             ->service("ware_export_columns_handler")
             ->add("shop")
             ->method("shop:WaresHandleController@exportColumns")
        ;

    }



    /**
     * Registers the ware filters
     */
    protected function registerFilters()
    {
        $this->module()->callControllerMethod('WaresHandleController', 'filterForm');
    }



    /**
     * register aliases
     */
    protected function registerAliases()
    {
        $this->addAlias('ShopModule', ShopServiceProvider::class);
    }



    /**
     * register services
     */
    protected function registerServices()
    {
        service('shop')
             ->register('ware_browse_headings',
                  'Array of the blades, responsible to show the content of each browse column (On Demand).')
             ->register('ware_browse_headings_handlers',
                  "Methods, responsible to generate the wares browse columns.")
             ->register('ware_row_actions',
                  'Array, responsible to show the content of each action column (On Demand).')
             ->register('ware_row_action_handlers',
                  "Methods, responsible to generate the wares actions columns.")
             ->register("mass_action_handlers",
                  "Methods, responsible to generate 'Mass Actions' button, above grid view.")
             ->register("mass_actions",
                  "Array of the 'Mass Actions' button, above grid view. (On Demand).")
             ->register("toolbar_buttons_handlers",
                  "Methods, responsible to generate toolbar buttons, above grid view.")
             ->register("toolbar_buttons",
                  "Array of the 'Mass Actions' button, above grid view. (On Demand).")
             ->register("wares_filter",
                  "Responsible to wares filter form")
             ->register('ware_export_columns',
                  'Array of the blades, responsible to show the content of each column in wares export.')
             ->register('ware_export_columns_handler',
                  "Methods, responsible to generate the wares export columns.")
             ->register('shop_sidebar',
                  "Responsible to make shop sidebar.")
        ;
    }



    /**
     * register schedules
     */
    protected function registerSchedules()
    {
        $this->addSchedule(RemoveExpiredDiscountsSchedule::class);
    }



    /**
     * register listeners
     */
    protected function registerListeners()
    {
        $this->listen(PosttypeUpdated::class,
             InventoryActionsOnPosttypeUpdate::class);
        $this->listen(WareUpdated::class, CacheRefreshOnWareUpdated::class);
        $this->listen(WareUpdated::class, PostsRefreshPrices::class);
        $this->listen(PackageUpdated::class,
             CacheRefreshOnPackageUpdated::class);
        $this->listen(WaresReordered::class, PostsRefreshPrices::class);
    }



    /**
     * register bottom assets
     */
    public function registerBottomAssets()
    {
        /*-----------------------------------------------
        | Shop Custom js ...
        */
        module('manage')
             ->service('template_bottom_assets')
             ->add("shop-script")
             ->link("shop:js/shop-script.js")
             ->order(44)
        ;
    }



    /**
     * handler for posts browse headings
     *
     * @param array $arguments
     */
    public static function handleBrowseHeadings($arguments)
    {
        $posttype = $arguments['posttype'];

        module('posts')
             ->service('browse_headings')
             ->add('shop')
             ->trans("shop::wares.title")
             ->blade("shop::posts.column")
             ->order(11)
             ->condition($posttype->has('shop'))
        ;
    }



    /**
     * Handles the row actions of the posts' grid.
     *
     * @param array $arguments
     */
    public static function handleBrowseRowActions($arguments)
    {
        $model = $arguments['model'];

        module('posts')
             ->service('row_actions')
             ->add('special_offers')
             ->icon('bullhorn')
             ->trans(module('shop')->getTrans('offers.singular_title'))
             ->link("modal:" . route('shop.post.wares.offers',
                       ['hashid' => $model->hashid], false))
             ->condition($model->has('shop') and $model->isNotTrashed())
             ->order(21)
        ;
    }



    /**
     * Register artisan command(s).
     */
    public function registerArtisans()
    {
        $this->addArtisan(ShopResetCommand::class);
    }



    /**
     * register coupons consistent rules
     */
    protected function registerConsistentCouponRules()
    {
        coupon()->registerConsistentRule(SpecialSaleRule::class);
    }



    /**
     * Dashboard Widget Handlers
     */
    public function registerWidgets()
    {
        module('manage')
             ->service('widgets_handler')
             ->add('shop-widgets')
             ->method('shop:handleWidgets')
        ;
    }



    /**
     * Registers the endpoints only when the `Endpoints` module is available.
     *
     * @return void
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(PostsFilterEndpoint::class);
    }



    /**
     * Dashboard Widgets
     */
    public static function handleWidgets()
    {
        $service = 'widgets';

        module('manage')
             ->service($service)
             ->add('ware-finder')
             ->blade('shop::dashboard.ware-finder')
             ->trans('shop::dashboard.ware-finder.title')
             ->color('warning')
             ->icon('shopping-cart')
             ->condition(function () {
                 return dev();
             })
        ;
    }
}
