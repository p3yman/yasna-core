<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Label\Entities\Traits\LabelsTrait;
use Modules\Shop\Entities\Traits\WareChainTrait;
use Modules\Shop\Entities\Traits\WareCodesTrait;
use Modules\Shop\Entities\Traits\WareElectorTrait;
use Modules\Shop\Entities\Traits\WareInventoryTrait;
use Modules\Shop\Entities\Traits\WareLabelsTrait;
use Modules\Shop\Entities\Traits\WareOfferTrait;
use Modules\Shop\Entities\Traits\WarePackageTrait;
use Modules\Shop\Entities\Traits\WarePostTrait;
use Modules\Shop\Entities\Traits\WarePriceTrait;
use Modules\Shop\Entities\Traits\WareQueriesTrait;
use Modules\Shop\Entities\Traits\WareRemarkTrait;
use Modules\Shop\Entities\Traits\WareResourcesTrait;
use Modules\Shop\Entities\Traits\WareSortTrait;
use Modules\Shop\Entities\Traits\WareTitleTrait;
use Modules\Yasna\Services\ModelTraits\UniqueUrlTrait;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class Ware
 *
 * @package Modules\Shop\Entities
 * @property integer $approximated_size
 */
class Ware extends YasnaModel
{
    use SoftDeletes;
    use WareLabelsTrait;
    use WareOfferTrait;
    use WareInventoryTrait, WareTitleTrait, WarePriceTrait, WareRemarkTrait,
         WarePackageTrait, WareChainTrait, WarePostTrait, WareCodesTrait;
    use UniqueUrlTrait;
    use WareQueriesTrait;
    use WareResourcesTrait;
    use WareElectorTrait;
    use WareSortTrait;

    const LABEL_POSTTYPE_DIVIDER = '|';



    /**
     * Meta fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return ['titles', 'remarks', 'image'];
    }



    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    public function unit()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Unit");
    }



    /*
    |--------------------------------------------------------------------------
    | Assessors
    |--------------------------------------------------------------------------
    |
    */
    public function getFullTitleAttribute()
    {
        if ($this->sisterhood) {
            return $this->post->title . " / " . $this->title . " (" . $this->code . ") ";
        } else {
            return $this->title;
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Selectors
    |--------------------------------------------------------------------------
    |
    */
    public static function selector($parameters = [])
    {
        $switch = array_normalize($parameters, [
             'posttype_id' => false,
             'package_id'  => false,
             'unit_id'     => false,
             'sisterhood'  => false,
             //'locale' => false , //TODO
             'criteria'    => false,
             //'price' => false , //TODO
             //'min_price' => false , //TODO
             //'max_price' => false , //TODO
        ]);

        $table = self::where('id', '>', '0');

        /*-----------------------------------------------
        | Simple Things ...
        */
        if ($switch['posttype_id'] !== false) {
            $table->where('posttype_id', $switch['posttype_id']);
        }
        if ($switch['package_id'] !== false) {
            $table->where('package_id', $switch['package_id']);
        }
        if ($switch['unit_id'] !== false) {
            $table->where('unit_id', $switch['unit_id']);
        }
        if ($switch['sisterhood'] !== false) {
            $table->where('sisterhood', $switch['sisterhood']);
        }

        /*-----------------------------------------------
        | Criteria ...
        */
        if ($switch['criteria'] !== false) {
            switch ($switch['criteria']) {
                case 'all':
                    break;

                case 'all_with_trashed':
                case 'with_trashed':
                    $table = $table->withTrashed();
                    break;

                case 'bin':
                case 'only_trashed':
                    $table = $table->onlyTrashed();
                    break;

                default:
                    $table = $table->where('id', '0');
                    break;

            }
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $table;
    }
}
