<?php

namespace Modules\Shop\Entities\Traits;

trait OfferLinksTrait
{

    /**
     * get create link in the manage area
     *
     * @return string
     */
    public function getCreateLink()
    {
        return $this->getActionLink('create');
    }



    /**
     * get grid-row refresh link
     *
     * @return string
     */
    public function getRefreshLink()
    {
        return $this->route("refresh", false, [
             "hashid" => $this->hashid,
        ]);
    }



    /**
     * get editor save link in the manage area
     *
     * @return string
     */
    public function getSaveLink()
    {
        return $this->route("save");
    }



    /**
     * get modal action link
     *
     * @param $action
     *
     * @return string
     */
    public function getActionLink(string $action)
    {
        return $this->route("action", true, [
             "action" => $action,
             "hashid" => $this->hashid,
        ]);
    }



    /**
     * get modal action link for enabling and disabling the offer
     *
     * @return string
     */
    public function getToggleLink()
    {
        if ($this->isTrashed()) {
            return $this->getActionLink("undelete");
        }

        return $this->getActionLink("delete");
    }



    /**
     * get the route address
     *
     * @param string $name_suffix
     * @param bool   $in_modal
     * @param array  $array
     *
     * @return string
     */
    private function route(string $name_suffix, bool $in_modal = false, array $array = []): string
    {
        $route = route("shop-downstream-offer-$name_suffix", $array);

        if ($in_modal) {
            $route = "masterModal('$route')";
        }

        return $route;
    }
}
