<?php
namespace Modules\Shop\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait PosttypeShopTrait
{

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function wares()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Ware') ;
    }

    public function packages()
    {
        return $this->wares()->whereNull('sisterhood') ;
    }

    public function realWares()
    {
        return $this->wares()->whereNotNull('sisterhood') ;
    }

    public function getPackagesAttribute()
    {
        return $this->packages()->orderBy('created_at', 'desc')->get() ;
    }


    public function inventories()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'Inventory')->withTimestamps();
    }

    public function getCurrenciesArrayAttribute()
    {
        $currencies = $this->getMeta('currencies') ;
        if (!$currencies) {
            return ['IRR'] ;
        } else {
            $array = array_filter(explode(',', $currencies));
            if (!sizeof($array)) {
                return ['IRR'];
            } else {
                return $array;
            }
        }
    }

    public function getCurrenciesCountAttribute()
    {
        return count($this->currencies_array);
    }

    public function getDefaultCurrencyAttribute()
    {
        return $this->currencies_array[0];
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function canShop()
    {
        return $this->can('shop') ;
    }

    public function cannotShop()
    {
        return $this->cannot('shop') ;
    }



    /**
     * Returns a builder of all defined labels, relative to the wares of this posttype.
     * Specific language is taken into account if provided.
     *
     * @param string|null $locale
     *
     * @return Builder
     */
    public function definedShopLabels(string $locale = null)
    {
        return ware()->definedLabels($locale)->where('slug', 'like', "%$this->slug" . ware()::LABEL_POSTTYPE_DIVIDER. "%");
    }



    /**
     * Returns a builder of all defined master top-level labels, relative to the wares of this posttype.
     *
     * @return Builder
     */
    public function definedShopMasterLabels()
    {
        return $this->definedShopLabels()->where('parent_id', 0);
    }
}
