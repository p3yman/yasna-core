<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/30/18
 * Time: 10:04 AM
 */

namespace Modules\Shop\Entities\Traits;


use App\Models\ShopOffer;
use Illuminate\Database\Eloquent\Collection;

trait OfferComboTrait
{
    /**
     * Returns a combo array of the valid offers.
     *
     * @param string $value_field
     * @param string $caption_field
     *
     * @return array
     */
    public static function validCombo(string $value_field = 'id', string $caption_field = 'title'): array
    {
        $collection = static::whereValid()->get();

        return static::generateCombo($collection, $value_field, $caption_field);
    }



    /**
     * Returns a combo array of the expired offers.
     *
     * @param string $value_field
     * @param string $caption_field
     *
     * @return array
     */
    public static function expiredCombo(string $value_field = 'id', string $caption_field = 'title')
    {
        $collection = static::whereExpired()->get();

        return static::generateCombo($collection, $value_field, $caption_field);
    }



    /**
     * Generates a combo array of the specified collection with the specified value and caption fields.
     *
     * @param Collection $collection
     * @param string     $value_field
     * @param string     $caption_field
     *
     * @return array
     */
    protected static function generateCombo(
         Collection $collection,
         string $value_field = 'id',
         string $caption_field = 'title'
    ): array {
        return $collection
             ->map(function (ShopOffer $offer) use ($value_field, $caption_field) {
                 return [
                      $value_field   => $offer->hashid,
                      $caption_field => $offer->title,
                 ];
             })->toArray()
             ;
    }
}
