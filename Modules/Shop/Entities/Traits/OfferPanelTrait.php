<?php

namespace Modules\Shop\Entities\Traits;

/**
 * Class ShopOffer
 *
 * @property int $total_wares
 */
trait OfferPanelTrait
{
    /**
     * get item management title, to be shown in admin panel
     *
     * @return string
     */
    public function getItemManagementTitleAttribute()
    {
        return ad(trans("shop::offers.item_management") . " ($this->total_wares) ");
    }

}
