<?php
namespace Modules\Shop\Entities\Traits;

use App\Models\Price;
use Carbon\Carbon;

trait WarePriceTrait
{
    protected $cached_price_array = false;



    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function prices()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Price");
    }



    /*
    |--------------------------------------------------------------------------
    | Get...
    |--------------------------------------------------------------------------
    |
    */
    public function price($type = 'sale')
    {
        return Price::ask($type)->for($this);
    }



    public function originalPrice()
    {
        return $this->price('original');
    }



    public function salePrice()
    {
        return $this->price('sale');
    }



    public function currentPrices($fresh_value = false)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!$fresh_value and $this->cached_price_array) {
            return $this->cached_price_array;
        }

        /*-----------------------------------------------
        | Calculation ...
        */
        $result = [];

        foreach ($this->currencies_array as $currency) {
            $array['original_price'] = $this->originalPrice()->in($currency)->get();
            $array['sale_price']     = $this->salePrice()->in($currency)->get();
            $array['on_sale']        = boolval($array['original_price'] > $array['sale_price']);
            $array['sale_expire']    = $this->salePrice()->in($currency)->getExpireDate();

            $result[$currency] = $array;
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $this->cached_price_array = $result;
    }



    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */
    public function getCurrenciesArrayAttribute()
    {
        return $this->posttype->currencies_array;
    }



    public function getDefaultCurrencyAttribute()
    {
        return $this->currencies_array[0];
    }



    public function getCurrentPricesAttribute()
    {
        return $this->currentPrices();
    }



    /*
    |--------------------------------------------------------------------------
    | Set...
    |--------------------------------------------------------------------------
    |
    */

    public function setPrice($new_price, $options)
    {
        /*-----------------------------------------------
        | Options ...
        */
        $options = array_normalize($options, [
             'currency'   => get_setting('default_currency'),
             'date'       => Carbon::now(),
             'type'       => "original",
             'expires_at' => null,
        ]);

        /*-----------------------------------------------
        | Query ...
        */
        $new_model = Price::create([
             'ware_id'     => $this->id,
             'amount'      => $new_price,
             'currency'    => $options['currency'],
             'type'        => $options['type'],
             'expires_at'  => $options['expires_at'] ? $options['expires_at'] : null,
             'effected_at' => $options['date'],
             'created_by'  => user()->id,
        ]);

        /*-----------------------------------------------
        | Feedback ...
        */

        return $new_model->id;
    }



    public function setPrices($data)
    {
        foreach ($this->posttype->currencies_array as $currency) {
            /*-----------------------------------------------
            | Main Price ...
            */
            $price = $data["_price_$currency"];
            $this->setPrice($price, [
                 'currency' => $currency,
            ]);

            /*-----------------------------------------------
            | Discount ...
            */
            if (isset($data["_sale_$currency"])) {
                $price = $data["_sale_$currency"];
                if (!$price) {
                    $price = $data["_price_$currency"];
                }
                
                $this->setPrice($price, [
                     'currency'   => $currency,
                     'type'       => "sale",
                     'expires_at' => $data["_discount_expires_at_$currency"],
                ]);
            }
        }

        return $this;
    }



    /*
    |--------------------------------------------------------------------------
    | Cache Management
    |--------------------------------------------------------------------------
    |
    */
    public function cachePrices()
    {
        $this->refreshCurrentPrices();
    }



    public function refreshCurrentPrices()
    {

        /*-----------------------------------------------
        | Calculation ...
        */
        $original_price = $this->price('original')->withPackage()->get();
        $sale_price     = $this->price('sale')->withPackage()->get();
        $currencies     = $this->calculateCurrenciesCacheField();

        /*-----------------------------------------------
        | Self Update ...
        */
        $this->update([
             'original_price' => $original_price,
             'sale_price'     => $sale_price,
             'currencies'     => $currencies,
        ]);

        /*-----------------------------------------------
        | Return ...
        */
        return $sale_price;
    }



    public function refreshPostPrices()
    {
        foreach ($this->posts() as $post) {
            $post->refreshCurrentPrices();
        }
    }



    protected function calculateCurrenciesCacheField()
    {
        /*-----------------------------------------------
        | Calculations ...
        */
        $result = [];
        foreach ($this->current_prices as $currency => $detail) {
            if ($detail['on_sale']) {
                $result[] = "$currency*";
            } elseif ($detail['original_price']) {
                $result[] = $currency;
            }
        }

        /*-----------------------------------------------
        | Return ...
        */
        return implode(",", $result);
    }
}
