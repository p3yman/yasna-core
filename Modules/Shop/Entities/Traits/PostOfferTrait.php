<?php

namespace Modules\Shop\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait PostOfferTrait
{
    /**
     * Scope to find Post with specified valid offer(s)
     *
     * @param Builder          $query
     * @param int|string|array $offer Id or array of IDs
     */
    public function scopeWithValidOffer(Builder $query, $offer)
    {
        $query->whereHas('wares', function ($query) use ($offer) {
            $query->whereHas('validOffers', function ($query) use ($offer) {
                $ids = (array)$offer;

                $query->whereIn('shop_offers.id', $ids);
            });
        });
    }



    /**
     * Scope to find Post with specified valid offer(s)
     *
     * @param int|string|array $offer Id or array of IDs
     */
    public function electorValidOffer($offer)
    {
        $this->elector()->withValidOffer($offer);
    }



    /**
     * Scope to find Post with specified offer(s)
     *
     * @param Builder          $query
     * @param int|string|array $offer Id or array of IDs
     */
    public function scopeWithOffer(Builder $query, $offer)
    {
        $query->whereHas('wares', function ($query) use ($offer) {
            $query->whereHas('offers', function ($query) use ($offer) {
                $ids = (array)$offer;

                $query->whereIn('shop_offers.id', $ids);
            });
        });
    }



    /**
     * Scope to find Post with specified offer(s)
     *
     * @param int|string|array $offer Id or array of IDs
     */
    public function electorOffer($offer)
    {
        $this->elector()->withOffer($offer);
    }
}
