<?php
namespace Modules\Shop\Entities\Traits;

trait WareTitleTrait
{

    /**
     * Determines if a ware is defined in a particular locale
     *
     * @param string $locale
     *
     * @return bool
     */
    public function isIn($locale)
    {
        $locale = strtolower($locale);
        return boolval($this->titleIn($locale));
    }



    /**
     * Gets array of titles
     *
     * @return array
     */
    public function titlesArray()
    {
        return (array)$this->getMeta('titles');
    }



    /**
     * Gets title in a particular language
     *
     * @param string $locale
     *
     * @return string
     */
    public function titleIn($locale)
    {
        $array = $this->titlesArray();

        if (!count($array)) {
            return null;
        }
        if ($locale == 'first') {
            return $array[0];
        }
        if (isset($array[$locale])) {
            return $array[$locale];
        }

        return null;
    }



    /**
     * Gets the first title, with higher priority for Persian.
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        if ($this->isIn('fa')) {
            return $this->titleIn('fa');
        } else {
            return $this->titleIn('first');
        }
    }
}
