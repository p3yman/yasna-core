<?php

namespace Modules\Shop\Entities\Traits;

use Modules\Shop\Entities\Scopes\ShopOfferExpiryScope;

/**
 * Class ShopOffer
 *
 * @property string $expires_at
 */
trait OfferExpiryTrait
{
    /**
     * boot the current trait
     */
    public static function bootOfferExpiryTrait()
    {
        static::addGlobalScope(new ShopOfferExpiryScope());
    }



    /**
     * check if the offer is already expired
     *
     * @return bool
     */
    public function isExpired()
    {
        return !$this->isValid();
    }



    /**
     * check if the offer is not expired
     *
     * @return bool
     */
    public function isValid()
    {
        if (is_null($this->expires_at)) {
            return true;
        }

        return $this->expires_at > now();
    }

}
