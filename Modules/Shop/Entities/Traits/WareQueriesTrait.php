<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/5/18
 * Time: 12:56 PM
 */

namespace Modules\Shop\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;

trait WareQueriesTrait
{
    /**
     * Returns a builder to find wares which are on sale.
     * <br>
     * This method is working with prices in default the currency
     * which are being cached in the `wares` table automatically.
     *
     * @return Builder
     */
    public static function sales()
    {
        return static::instance()
                     ->whereColumn('sale_price', '<', 'original_price')
             ;
    }
}
