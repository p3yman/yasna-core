<?php

namespace Modules\Shop\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;

trait PostShopChainTrait
{
    protected $chain_time; // <~~ Is not currently supported!
    protected $chain_currency;
    protected $chain_locale;



    /**
     * Adds the specified currency or locale to the chain properties.
     *
     * @param string $mixed_locale_or_currency
     *
     * @return PostShopChainTrait
     */
    public function _in($mixed_locale_or_currency) // <~~ Collides with the locale manager of the post!
    {
        if (in_array(strtolower($mixed_locale_or_currency), $this->posttype->locales_array)) {
            return $this->inLocale($mixed_locale_or_currency);
        } else {
            return $this->inCurrency($mixed_locale_or_currency);
        }
    }



    /**
     * Adds the specified locale to the chain properties.
     *
     * @param string $locale
     *
     * @return $this
     */
    public function inLocale($locale)
    {
        $this->chain_locale = strtolower($locale);
        return $this;
    }



    /**
     * Adds the specified currency to the chain properties.
     *
     * @param string $currency
     *
     * @return $this
     */
    public function inCurrency($currency)
    {
        $this->chain_currency = strtoupper($currency);
        return $this;
    }



    /**
     * Adds the specified time to the chain properties.
     *
     * @param string $date_time_string
     *
     * @return $this
     */
    public function at($date_time_string) //  <~~ Is not currently supported!
    {
        $this->chain_time = $date_time_string;
        return $this;
    }



    /**
     * Returns a builder for all the purchasable wares.
     *
     * @return HasMany
     */
    public function purchasableWares()
    {
        $this->normalizeChainVariables();
        $min_inventory = intval($this->posttype->getMeta('min_inventory_stop'));

        return $this->waresIn($this->chain_locale)
                    ->where('is_available', true)
                    ->where('currencies', 'like', "%" . $this->chain_currency . "%")
                    ->where('current_inventory', '>=', $min_inventory)
             ;
    }



    /**
     * Returns a builder for all the wares on sale.
     *
     * @return HasMany
     */
    public function onSaleWares()
    {
        $this->normalizeChainVariables();
        $min_inventory = intval($this->posttype->getMeta('min_inventory_stop'));

        return $this->waresIn($this->chain_locale)
                    ->where('is_available', true)
                    ->where('currencies', 'like', "%" . $this->chain_currency . "*%")
                    ->where('current_inventory', '>=', $min_inventory)
             ;
    }



    /**
     * Checks if at least one of the wares is purchasable.
     *
     * @return bool
     */
    public function isPurchasable()
    {
        $this->normalizeChainVariables();
        return boolval($this->purchasableWares()->count());
    }



    /**
     * Checks if at least one of the wares is on sale.
     *
     * @return bool
     */
    public function isOnSale()
    {
        $this->normalizeChainVariables();
        return boolval($this->onSaleWares()->count());
    }



    /**
     * Normalizes the chain properties.
     */
    protected function normalizeChainVariables()
    {
        /*-----------------------------------------------
        | Locale ...
        */
        if (!$this->chain_locale) {
            $this->chain_locale = getLocale();
        }

        /*-----------------------------------------------
        | Currency ...
        */
        if (!$this->chain_currency) {
            $this->chain_currency = $this->currencies_array[0];
        }

        // TIME
        // CURRENCY
    }
}
