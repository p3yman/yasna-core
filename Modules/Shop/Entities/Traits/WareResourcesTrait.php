<?php

namespace Modules\Shop\Entities\Traits;

use App\Models\Label;
use App\Models\Price;
use Carbon\Carbon;

trait WareResourcesTrait
{
    /**
     * boot WareResourcesTrait
     *
     * @return void
     */
    public static function bootWareResourcesTrait()
    {
        static::addDirectResources([
             'full_title',
             'sale_price',
             'original_price',
             'code',
             'custom_code',
             'is_available',
             'is_purchasable',
             'current_inventory',
             'package_inventory',
             'has_enough_inventory',
             'has_enough_package',
        ]);
    }



    /**
     * get Title resource.
     *
     * @return string
     */
    protected function getTitleResource()
    {
        return $this->titleIn(getLocale());
    }



    /**
     * get Labels resource.
     *
     * @return array
     */
    protected function getLabelsResource()
    {
        $collection = $this->labels()->get();
        $resources  = ['title', 'slug'];

        return $this->getResourceFromCollection($collection, $resources);
    }



    /**
     * get PriceChanges resource.
     *
     * @return array
     */
    protected function getPriceChangesResource()
    {
        return [
             'sale'     => $this->getPriceChangesHistory('sale'),
             'original' => $this->getPriceChangesHistory('original'),
        ];
    }



    /**
     * Returns the last prices changes to be used in the resources
     *
     * @param string $type The Type of the Prices in the Result  (`sale`/`original`)
     * @param int    $limit
     *
     * @return mixed
     */
    protected function getPriceChangesHistory(string $type, int $limit = 50)
    {
        return $this
             ->prices()
             ->where('type', $type)
             ->orderByDesc('effected_at')
             ->limit($limit)
             ->get()
             ->reverse()
             ->map(function (Price $price) {
                 return [
                      'amount'      => $price->amount,
                      'currency'    => $price->currency,
                      'effected_at' => Carbon::parse($price->effected_at)->getTimestamp(),
                 ];
             })
             ;
    }
}
