<?php
namespace Modules\Shop\Entities\Traits;

use App\Models\Ware;

trait PostShopTrait
{
    use PostAvailabilityTrait;
    /*
    |--------------------------------------------------------------------------
    | Relation
    |--------------------------------------------------------------------------
    |
    */

    public function wares()
    {
        return $this->hasMany(Ware::class, 'sisterhood', 'sisterhood')->orderBy('order');
    }



    public function waresIn($locale)
    {
        return $this->wares()->where('locales', 'like', "%$locale%");
    }



    public function getWaresAttribute()
    {
        return $this->wares()->get();
    }




    public function firstWare()
    {
        return $this->wares()->firstOrNew([
             'sisterhood' => $this->sisterhood,
        ])
             ;
    }



    public function firstWareIn($locale)
    {
        return $this->waresIn($locale)->first();
    }



    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */
    public function getCurrenciesArrayAttribute()
    {
        return $this->posttype->currencies_array;
    }



    public function getDefaultCurrencyAttribute()
    {
        return $this->currencies_array[0];
    }



    public function getWareIndexLinkAttribute()
    {
        return "manage/shop/wares/index/$this->hashid" ;
    }



    /*
    |--------------------------------------------------------------------------
    | Cache Management
    |--------------------------------------------------------------------------
    |
    */
    public function refreshCurrentPrices()
    {
        $first_ware = $this->firstWare(); //<~~ TODO: Make it Safe!

        $this->store([
             "id" => $this->id ,
             'original_price' => $first_ware->original_price,
             'sale_price'     => $first_ware->sale_price,
             "total_wares" => $this->wares()->count() ,
        ]);

        return $first_ware->sale_price;
    }



    public function refreshCurrentInventory()
    {
        //TODO
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function canShop()
    {
        return $this->can('shop');
    }



    public function cannotShop()
    {
        return $this->cannot('shop');
    }
}
