<?php
namespace Modules\Shop\Entities\Traits;

trait WarePackageTrait
{

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function package()
    {
        if ($this->package_id) {
            $package = self::find($this->package_id);
        }

        if (!isset($package) or !$package or !$package->exists) {
            $package = new self();
        }

        return $package;
    }

    public function insiders()
    {
        return self::where('package_id', $this->id);
    }

    public function getPackageAttribute()
    {
        return $this->package();
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */


    public function isPackage()
    {
        return !boolval($this->sisterhood);
    }

    public function isNotPackage()
    {
        return !$this->isPackage();
    }

    public function hasPackage()
    {
        return boolval($this->package_id) ;
    }

    public function hasnotPackage()
    {
        return !$this->hasPackage() ;
    }

    /*
    |--------------------------------------------------------------------------
    | Assessors
    |--------------------------------------------------------------------------
    |
    */


    public function getIsPackageAttribute()
    {
        return $this->isPackage();
    }
}
