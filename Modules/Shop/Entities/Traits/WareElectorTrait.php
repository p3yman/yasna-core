<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/29/18
 * Time: 3:50 PM
 */

namespace Modules\Shop\Entities\Traits;


trait WareElectorTrait
{
    /**
     * Elector for Posttype
     *
     * @param string|int $posttype_identifier
     */
    public function electorPosttype($posttype_identifier)
    {
        $posttype = posttype($posttype_identifier);

        $this->elector()->where('posttype_id', $posttype->id);
    }



    /**
     * Elector for Posttype
     *
     * @param string|int $posttype_identifier
     */
    public function electorType($posttype_identifier)
    {
        $this->electorPosttype($posttype_identifier);
    }



    /**
     * Elector for Min Price
     *
     * @param string|float $price
     */
    public function electorMinPrice($price)
    {
        $this->elector()->where('original_price', '>=', $price);
    }



    /**
     * Elector for Max Price
     *
     * @param string|float $price
     */
    public function electorMaxPrice($price)
    {
        $this->elector()->where('original_price', '<=', $price);
    }



    /**
     * Elector for Offers
     *
     * @param string|int $offer_identifier
     */
    public function electorOffer($offer_identifier)
    {
        $offer = model('shop-offer', $offer_identifier);

        $this->elector()->whereHas('offers', function ($query) use ($offer) {
            $query->where('shop_offers.id', $offer->id);
        })
        ;
    }



    /**
     * Elector for One or More Categories
     *
     * @param string|int|array $category_identifier
     */
    public function electorCategory($category_identifier)
    {
        $this->electorCategories((array)$category_identifier);
    }



    /**
     * Elector for an Array of Categories
     *
     * @param array $categories_identifiers
     */
    public function electorCategories(array $categories_identifiers)
    {
        $identifiers = $this->categoriesUsableIdentifiers($categories_identifiers);

        // relation with posts
        $this->elector()->whereHas('posts', function ($query1) use ($identifiers) {
            // relation with categories
            $query1->whereHas('categories', function ($query2) use ($identifiers) {

                // condition in categories
                $query2->where(function ($query3) use ($identifiers) {

                    $query3->whereIn('categories.id', $identifiers);
                    $query3->orWhereIn('categories.slug', $identifiers);

                });
            });
        })
        ;
    }



    /**
     * Returns an array of identifiers to be used in categories elector.
     *
     * @param array $categories_identifiers
     *
     * @return array
     */
    protected function categoriesUsableIdentifiers(array $categories_identifiers)
    {
        return collect($categories_identifiers)
             ->map(function ($identifier) {
                 // if it is ID
                 if (is_numeric($identifier)) {
                     return $identifier;
                 }

                 // if it is Hashid
                 $dehash = hashid($identifier);
                 if ($dehash) {
                     return $dehash;
                 }

                 return $identifier;
             })->toArray()
             ;
    }



    /**
     * Elector for Available Items
     *
     * @param bool $value
     */
    public function electorAvailable(bool $value)
    {
        $this->electorAvailability($value);
    }



    /**
     * Elector for Unavailable Items
     *
     * @param bool $value
     */
    public function electorUnavailable(bool $value)
    {
        $this->electorAvailability(!$value);
    }



    /**
     * Elector for availability
     *
     * @param bool $value
     */
    public function electorAvailability(bool $value)
    {
        $this->elector()->whereIsAvailable($value);
    }



    /**
     * Elector for One or More Categories
     *
     * @param string|int|array $post_identifier
     */
    public function electorPost($post_identifier)
    {
        $this->electorPosts((array)$post_identifier);
    }



    /**
     * Elector for an Array of Posts
     *
     * @param array $posts_identifiers
     */
    public function electorPosts(array $posts_identifiers)
    {
        $identifiers = $this->postsUsableIdentifiers($posts_identifiers);

        // relation with posts
        $this->elector()->whereHas('posts', function ($query) use ($identifiers) {
            // condition in posts
            $query->whereIn('posts.id', $identifiers);
        })
        ;
    }



    /**
     * Returns an array of identifiers to be used in posts elector.
     *
     * @param array $posts_identifiers
     *
     * @return array
     */
    protected function postsUsableIdentifiers(array $posts_identifiers)
    {
        return collect($posts_identifiers)
             ->map(function ($identifier) {
                 // if it is ID
                 if (is_numeric($identifier)) {
                     return $identifier;
                 }

                 // if it is Hashid
                 $dehash = hashid($identifier);
                 if ($dehash) {
                     return $dehash;
                 }

                 return $identifier;
             })->toArray()
             ;
    }



    /**
     * Elector for System Generated Code
     *
     * @param string $code
     */
    public function electorCode(string $code)
    {
        $this->elector()->whereCode($code);
    }



    /**
     * Elector for Custom Code
     *
     * @param string $code
     */
    public function electorCustomCode(string $code)
    {
        $this->elector()->whereCustomCode($code);
    }
}
