<?php

namespace Modules\Shop\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait WareCodesTrait
{
    /**
     * Gets ware code
     *
     * @return string
     */
    public function getCodeAttribute()
    {
        $prefix = $this->getCodePrefix();
        $margin = $this->getCodeMargin();

        if ($this->id) {
            return $prefix . strval($margin + $this->id);
        } else {
            return $prefix . "-NEW";
        }
    }



    /**
     * Gets shop ware code prefix from setting
     *
     * @return string
     */
    protected function getCodeDynamicPrefix()
    {
        return get_setting("shop_ware_prefix");
    }



    /**
     * Gets shop ware code margin
     *
     * @return int
     */
    protected function getCodeMargin()
    {
        return 1234;
    }



    /**
     * Gets ware code complete prefix
     *
     * @return string
     */
    protected function getCodePrefix()
    {
        $prefix = $this->getCodeDynamicPrefix();
        if ($prefix) {
            $prefix .= "-";
        }

        return $prefix;
    }



    /**
     * Scope to select ware with code.
     *
     * @param Builder $builder
     * @param string  $code
     *
     * @return Builder
     */
    public function scopeWhereCode(Builder $builder, string $code)
    {
        $prefix       = $this->getCodePrefix();
        $margin       = $this->getCodeMargin();
        $numeric_part = str_after($code, $prefix);
        $id           = intval($numeric_part - $margin);

        return $builder->where('id', $id);
    }
}
