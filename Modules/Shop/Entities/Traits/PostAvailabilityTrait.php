<?php

namespace Modules\Shop\Entities\Traits;

trait PostAvailabilityTrait
{
    /**
     * refresh availability filters on post model
     *
     * @return bool
     */
    public function refreshAvailability()
    {
        $is_available         = $this->wares()->where("is_available", 1)->exists();
        $has_enough_inventory = $this->wares()->where("has_enough_inventory", 1)->exists();

        return $this->update([
             "is_available"         => $is_available,
             "has_enough_inventory" => $has_enough_inventory,
        ]);
    }



    /**
     * filter posts based on the `is_available` and `has_enough_inventory` properties.
     *
     * @param bool $available
     *
     * @return void
     */
    public function electorAvailable($available)
    {
        if ($available) {
            $this->elector()->where("is_available", 1)->where("has_enough_inventory", 1);
        }
        else {
            $this->elector()->where(function($q) {
                return $q->where("is_available", 0)->orWhere("has_enough_inventory", 0);
            });
        }
    }
}
