<?php
namespace Modules\Shop\Entities\Traits;

trait CategoryShopTrait
{
    /*
    |--------------------------------------------------------------------------
    | Relation
    |--------------------------------------------------------------------------
    |
    */
    public function wares()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Ware')->withTimestamps();
    }


    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */


    public static function shopMetaFields()
    {
        ///@TODO: TO BE DEPRECIATED!
        return [
            'ware_type', // <~~ hard , soft
            'labels',
            'min_inventory_alarm',
            'min_inventory_stop',
        ];
    }
}
