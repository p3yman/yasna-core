<?php

namespace Modules\Shop\Entities\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ShopOffer
 */
trait OfferWaresTrait
{
    /**
     * get many-to-many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function wares()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . "Ware", "shop_offer_ware", "offer_id")
                    ->withTimestamps()
                    ->withPivot("expires_at")
             ;
    }



    /**
     * get many-to-many relationship of the expired records
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function expiredWares()
    {
        return $this->wares()->wherePivot('expires_at', '<', now());
    }



    /**
     * get dynamic property of the equivalent relationship instance
     *
     * @return Collection
     */
    public function getExpiredWaresAttribute()
    {
        return $this->wares->where('pivot.expires_at', '<', now());
    }



    /**
     * get many-to-many relationship of the unexpired records
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function validWares()
    {
        return $this->wares()->where(function ($query) {
            $query->where($this->table . 'expires_at', '>=', now())->orWhere($this->table . "expires_at", null);
        })
             ;
    }



    /**
     * get dynamic property of the equivalent relationship instance
     *
     * @return Collection
     */
    public function getValidWaresAttribute()
    {
        return $this->validWares()->get(); //<~~ @TODO: This will cause problems in eager-loading
    }



    /**
     * attach wares to the offer
     *
     * @param int|array          $id_array
     * @param null|Carbon|string $expire_date
     *
     * @return array
     */
    public function attachWares($id_array, $expire_date = null)
    {
        if (!$this->exists) {
            return [];
        }

        $attached = [];

        foreach ($id_array as $id) {
            $attached = array_merge($attached, $this->attachWare($id, $expire_date)['attached']);
        }

        $this->updateTotalWares();

        return $attached;
    }



    /**
     * attach a single ware to the offer
     *
     * @param  int               $id
     * @param null|Carbon|string $expire_date
     */
    protected function attachWare($id, $expire_date = null)
    {
        return $this->wares()->syncWithoutDetaching([$id => ['expires_at' => $expire_date]]);
    }



    /**
     * detach wares from the offer
     *
     * @param int|array $id_array
     *
     * @return int
     */
    public function detachWares($id_array = null)
    {
        if (!$this->exists) {
            return 0;
        }

        $return = $this->wares()->detach($id_array);
        $this->updateTotalWares();

        return $return;
    }



    /**
     * update total wares
     *
     * @return bool
     */
    public function updateTotalWares()
    {
        return $this->batchSaveBoolean([
             "total_wares" => $this->wares()->count(),
        ]);
    }


}
