<?php
namespace Modules\Shop\Entities\Traits;

trait WareChainTrait
{
    protected $chain_time;
    protected $chain_currency;
    protected $chain_toman_request = false;
    protected $chain_locale;

    /*
    |--------------------------------------------------------------------------
    | Chain Setters
    |--------------------------------------------------------------------------
    |
    */
    public function in($mixed_locale_or_currency)
    {
        if (in_array(strtolower($mixed_locale_or_currency), $this->posttype->locales_array)) {
            return $this->inLocale($mixed_locale_or_currency);
        } else {
            return $this->inCurrency($mixed_locale_or_currency);
        }
    }

    public function inLocale($locale)
    {
        $this->chain_locale = strtolower($locale);
        return $this;
    }

    public function inCurrency($currency)
    {
        $currency = strtoupper($currency);

        /*-----------------------------------------------
        | Toman Special Provisions ...
        */
        if ($currency == 'IRT') {
            $currency                  = 'IRR';
            $this->chain_toman_request = true;
        }

        /*-----------------------------------------------
        | Actual Set ...
        */
        $this->chain_currency = $currency;
        return $this;
    }

    public function at($date_time_string)
    {
        $this->chain_time = $date_time_string;
        return $this;
    }

    /*
    |--------------------------------------------------------------------------
    | Public Get Methods
    |--------------------------------------------------------------------------
    |
    */
    public function isPurchasable()
    {
        return $this->checkAvailability() and $this->checkLocale() and $this->checkInventory() and $this->checkCurrency();
    }

    public function isNotPurchasable()
    {
        return !$this->isPurchasable();
    }


    public function isViewable()
    {
        return $this->checkLocale();
    }

    public function isNotViewable()
    {
        return !$this->isViewable();
    }

    public function isOnSale()
    {
        return $this->isPurchasable() and $this->checkSale();
    }

    public function isNotOnSale()
    {
        return !$this->isOnSale();
    }

    /*
    |--------------------------------------------------------------------------
    | Private Checks
    |--------------------------------------------------------------------------
    |
    */


    public function checkAvailability()
    {
        return boolval($this->is_available);
    }

    public function checkLocale()
    {
        if (!$this->chain_locale) {
            $this->chain_locale = getLocale();
        }
        return $this->isIn($this->chain_locale);
    }

    public function checkInventory()
    {
        if ($this->hasnotInventorySystem()) {
            return true;
        }

        $return = boolval($this->has_enough_inventory) ;

        if ($this->hasPackage()) {
            $return = boolval($return and boolval($this->has_enough_package)) ;
        }

        return $return ;
    }

    public function checkInventoryNoCache()
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($this->posttype->hasnot('shop_inventory')) {
            return true;
        }

        /*-----------------------------------------------
        | Check Inventory ...
        */
        $main_inventory = $this->inventory()->at($this->chain_time)->get();
        if (!$main_inventory or $main_inventory < $this->posttype->meta('min_inventory_stop')) {
            return false;
        }

        /*-----------------------------------------------
        | Default Return ...
        */
        return true and $this->checkPackageInventory();
    }

    public function checkPackageInventory()
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($this->hasnotPackage()) {
            return true;
        }

        /*-----------------------------------------------
        | Check Inventory ...
        */
        $package_inventory = $this->package()->inventory()->at($this->chain_time)->get();
        if ($package_inventory and $package_inventory > $this->posttype->meta('min_inventory_stop')) {
            return true;
        }

        /*-----------------------------------------------
        | Safe Return ...
        */
        return false;
    }

    public function checkCurrency()
    {
        if ($this->isPackage()) {
            return true;
        }
        $price = $this->price('original')
                      ->in($this->chain_currency)
                      ->at($this->chain_time)
                      ->get()
        ;
        return boolval($price);
    }

    public function checkSale()
    {
        $discount = $this->price('discount')
                         ->in($this->chain_currency)
                         ->at($this->chain_time)
                         ->get()
        ;

        return boolval($discount);
    }
}
