<?php

namespace Modules\Shop\Entities\Traits;

use App\Models\Posttype;
use Modules\Label\Entities\Traits\LabelsTrait;

/**
 * Class Ware
 *
 * @property Posttype $posttype
 */
trait WareLabelsTrait
{
    use LabelsTrait;



    /**
     * Attaches labels by their slugs.
     *
     * @param $labels
     *
     * @return bool|int
     */
    public function saveLabels($labels)
    {
        $this->detachAllLabels();
        return $this->attachLabels(explode_not_empty(',', $labels));
    }



    /**
     * Generates the required data for the selectize label selector tool.
     *
     * @return array
     */
    public function selectizeList()
    {
        $posttype = $this->posttype;
        $array    = [];

        if (!$posttype) {
            return [];
        }

        $labels = $posttype->definedShopLabels()->where('parent_id', '>', '0')->get();
        foreach ($labels as $label) {
            $array[] = [
                 "slug"  => $label->slug,
                 "title" => $label->short_title,
            ];
        }

        return $array;
    }
}
