<?php
namespace Modules\Shop\Entities\Traits;

use App\Models\Inventory;
use App\Models\Post;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait WareInventoryTrait
{

    /**
     * get inventories relationship of a ware.
     *
     * @return HasMany
     */
    public function inventories()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Inventory");
    }



    /**
     * get the ware item which is specified as the master of the current ware.
     *
     * @return $this|Ware
     */
    public function master()
    {
        /*-----------------------------------------------
        | If Independent ...
        */
        if (!$this->inventory_master_id) {
            return $this;
        }

        /*-----------------------------------------------
        | If dependent ...
        */
        $master = self::find($this->inventory_master_id);
        if (!$master or !$master->exists) {
            return new self();
        }

        return $master;
    }



    /**
     * get the ware item which is specified as the master of the current ware.
     *
     * @return $this|Ware
     */
    public function getMasterAttribute()
    {
        return $this->master();
    }



    /**
     * get the value of the minimum inventory, which triggers the low-limit-alarm.
     *
     * @return int
     */
    public function getLowLimitAlarmAttribute()
    {
        if ($this->hasnotInventorySystem()) {
            return -1;
        }

        return $this->posttype->meta('min_inventory_stop');
    }



    /**
     * get the value of the minimum inventory, which triggers the low-limit-stop alarm and prevent further selling.
     *
     * @return int
     */
    public function getLowLimitStopAttribute()
    {
        if ($this->hasnotInventorySystem()) {
            return -1;
        }

        return $this->posttype->meta('min_inventory_alarm');
    }



    /**
     * get the inventory chain starter
     *
     * @return InventoryChainTrait
     */
    public function inventory()
    {
        return Inventory::ask()->for($this);
    }



    /**
     * get the inventory type
     *
     * @return string
     */
    public function getInventoryTypeAttribute()
    {
        /*-----------------------------------------------
        | When Not Active from the Posttype Settings ...
        */

        if ($this->posttype->hasnot('shop_inventory')) {
            return 'disabled';
        }


        /*-----------------------------------------------
        | When Following Other Product ...
        */
        if ($this->inventory_master_id > 0) {
            return 'dependent';
        }


        /*-----------------------------------------------
        | When Independent ...
        */
        return 'independent';
    }



    /**
     * identify whether the ware has inventory system active
     *
     * @return bool
     */
    public function hasInventorySystem()
    {
        return $this->posttype->has('shop_inventory');
    }



    /**
     * identify whether the ware has NOT inventory system active
     *
     * @return bool
     */
    public function hasnotInventorySystem()
    {
        return !$this->hasInventorySystem();
    }



    /**
     * set inventory
     *
     * @param array $options
     *
     * @return bool
     */
    public function setInventory($options = [])
    {
        /*-----------------------------------------------
        | Options ...
        */
        $switch = array_normalize($options, [
             "account_id"  => "0",
             'alteration'  => "0",
             'type'        => "supplied_in",
             'description' => null,
             'fee'         => "0",
             'condition'   => true,
             'currency'    => "IRR",
        ]);

        /*-----------------------------------------------
        | Bypass ...
        */
        if (!$switch['condition']) {
            return false;
        }

        /*-----------------------------------------------
        | Action ...
        */
        $done = $this->inventory()
                    ->type($switch['type'])
                    ->description($switch['description'])
                    ->in($switch['currency'])
                    ->withAccount($switch['account_id'])
                    ->fee($switch['fee'])
                    ->alter($switch['alteration'])
             ;

        $this->refreshCurrentInventory();

        return $done;
    }



    /**
     * refresh current inventory
     *
     * @return int
     */
    public function refreshCurrentInventory()
    {
        /*-----------------------------------------------
        | Calculation ...
        */
        $new_inventory         = $this->inventory()->get();
        $new_package_inventory = 0;
        if ($this->hasPackage()) {
            $new_package_inventory = $this->package()->inventory()->get();
        }

        /*-----------------------------------------------
        | Self Update ...
        */
        $this->update([
             'current_inventory'    => $new_inventory,
             'package_inventory'    => $new_package_inventory,
             'has_enough_inventory' => $this->inventoryIsEnough($new_inventory),
             'has_enough_package'   => $this->inventoryIsEnough($new_package_inventory),
        ]);

        /*-----------------------------------------------
        | Insiders (if Package) ...
        */
        if ($this->isPackage()) {
            $this->insiders()->update([
                 'package_inventory'  => $new_inventory,
                 'has_enough_package' => boolval($new_inventory > $this->low_limit_stop),
            ])
            ;
        }

        /*-----------------------------------------------
        | Related Post ...
        */
        /** @var Post $post */
        $post = $this->post;
        if($post) {
            $post->refreshAvailability();
        }


        /*-----------------------------------------------
        | Return ...
        */
        return intval($new_inventory);
    }



    /**
     * refresh the enough_inventory property
     *
     * @return bool
     */
    public function refreshEnoughInventory()
    {
        $has_enough_inventory = $this->inventoryIsEnough($this->current_inventory);
        $has_enough_package   = $this->inventoryIsEnough($this->package_inventory);

        if ($has_enough_inventory != $this->has_enough_inventory or $has_enough_package != $this->has_enough_package) {
            return $this->update([
                 'has_enough_package'   => $has_enough_package,
                 'has_enough_inventory' => $has_enough_inventory,
            ]);
        }

        return false;
    }



    /**
     * get the array suitable to make an inventory-type selector
     *
     * @return array
     */
    public function inventoryTypeCombo()
    {
        $base_array = [ /*'disabled' ,*/
                        'independent',
                        'dependent',
        ];
        $output     = [];

        foreach ($base_array as $item) {
            $output[] = [
                 $item,
                 trans("shop::inventories.$item"),
            ];
        }

        return $output;
    }



    /**
     * get the collection of wares, suitable to form a master-inventory selector
     *
     * @return Collection
     */
    public function inventoryMastersCombo()
    {
        return $this->posttype
             ->wares()
             ->with("posts")
             ->whereNotNull('sisterhood')
             ->where('id', '!=', $this->id)
             ->where('inventory_master_id', 0)
             ->get()
             ;
    }



    /**
     * get an array of all inventory-enabled wares
     *
     * @return array
     */
    public function allInventoryMastersCombo()
    {
        $types = posttypes()->having("shop_inventory")->get()->pluck("id")->toArray();

        return ware()
             ->with("posts")
             ->whereIn("posttype_id", $types)
             ->where('inventory_master_id', 0)
             ->get()
             ;
    }



    /**
     * identify if the inventory is enough to sell the ware
     *
     * @param int $number
     *
     * @return bool
     */
    public function inventoryIsEnough($number = 0)
    {
        return boolval($this->low_limit_stop <= intval($number));
    }
}
