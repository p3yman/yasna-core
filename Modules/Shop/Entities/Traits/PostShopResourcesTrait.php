<?php

namespace Modules\Shop\Entities\Traits;

trait PostShopResourcesTrait
{
    /**
     * get Wares resource.
     *
     * @return array
     */
    protected function getWaresResource()
    {
        if ($this->hasnot('shop')) {
            return [];
        }

        return $this->getResourceFromCollection($this->wares);
    }
}
