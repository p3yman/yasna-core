<?php
namespace Modules\Shop\Entities\Traits;

use App\Models\Ware;
use Carbon\Carbon;

trait PriceChainTrait
{
    protected $chain_ware_id;
    protected $chain_ware;
    protected $chain_with_package;
    protected $chain_package_id;
    protected $chain_time;
    protected $chain_debug_mode;
    protected $chain_type;
    protected $chain_currency;
    protected $chain_invalid         = false;
    protected $chain_toman_request   = false;
    protected $chain_supported_types = ['original', 'sale', 'discount'];

    /*
    |--------------------------------------------------------------------------
    | Builder
    |--------------------------------------------------------------------------
    |
    */

    public static function ask($type = 'sale')
    {
        $object = new self();
        return $object->seek($type);
    }

    /*
    |--------------------------------------------------------------------------
    | Chain Methods
    |--------------------------------------------------------------------------
    |
    */

    public function seek($type)
    {
        $this->chain_type = $type;
        return $this;
    }

    public function for($ware)
    {
        if (is_int($ware)) {
            $ware = Ware::find($ware);
            if (!$ware or !$ware->exists) {
                $ware = new Ware();
            }
        }

        $this->chain_ware       = $ware;
        $this->chain_package_id = $ware->package_id;
        $this->chain_ware_id    = $ware->id;
        return $this;
    }

    public function withPackage()
    {
        if ($this->chain_ware->package_id) {
            $this->chain_with_package = true;
        }

        return $this;
    }

    public function withoutPackage()
    {
        $this->chain_with_package = false;
        return $this;
    }

    public function at($date_time_string)
    {
        $this->chain_time = $date_time_string;
        return $this;
    }

    public function debug()
    {
        $this->chain_debug_mode = true;

        return $this;
    }

    public function debugIf($condition)
    {
        if (boolval($condition)) {
            $this->debug();
        }

        return $this;
    }

    public function in($currency)
    {
        $currency = strtoupper($currency);

        /*-----------------------------------------------
        | Toman Special Provisions ...
        */
        if ($currency == 'IRT') {
            $currency                  = 'IRR';
            $this->chain_toman_request = true;
        }

        /*-----------------------------------------------
        | Actual Set ...
        */
        $this->chain_currency = $currency;
        return $this;
    }

    public function forPackage()
    {
        $this->chain_ware_id      = $this->chain_package_id;
        $this->chain_with_package = false;
        return $this;
    }

    public function get()
    {
        /*-----------------------------------------------
        | Normalizer ...
        */

        $this->normalizer();

        /*-----------------------------------------------
        | Bypass ...
        */
        if ($this->chain_invalid) {
            return false;
        }

        /*-----------------------------------------------
        | Process
        */
        $method_name = "getResult" . studly_case($this->chain_type);

        $result = $this->$method_name();

        /*-----------------------------------------------
        | For Package ...
        */
        if ($this->chain_with_package) {
            $result += $this->forPackage()->$method_name();
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $result;
    }

    public function getExpireDate()
    {
        $this->normalizer();
        $model = self::where('ware_id', $this->chain_ware_id)
                     ->where('type', 'sale')
                     ->where('currency', $this->chain_currency)
                     ->where('effected_at', '<', $this->chain_time)
                     ->where('expires_at', '>=', $this->chain_time)
                     ->first()
        ;

        if ($model and $model->exists) {
            return $model->expires_at;
        } else {
            return false;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Protected Factory
    |--------------------------------------------------------------------------
    |
    */
    protected function normalizer()
    {
        if (!$this->chain_ware_id) {
            $this->chain_invalid = true;
        }

        if (!$this->chain_time) {
            $this->chain_time = Carbon::now();
        }

        if (!in_array($this->chain_type, $this->chain_supported_types)) {
            $this->chain_invalid = true;
        }

        if (!$this->chain_currency) {
            $this->in($this->chain_ware->posttype->currencies_array[0]);
        }
    }

    protected function getResultOriginal()
    {
        $model = self::where('ware_id', $this->chain_ware_id)
                     ->where('type', 'original')
                     ->where('currency', $this->chain_currency)
                     ->where('effected_at', '<=', $this->chain_time)
                     ->orderBy('effected_at', 'desc')
                     ->first()
        ;

        if ($model and $model->exists) {
            return $this->applyTomanIfNecessary($model->amount);
        } else {
            return false;
        }
    }

    protected function getResultSale()
    {
        $model = self::where('ware_id', $this->chain_ware_id)
                     ->where('type', 'sale')
                     ->where('currency', $this->chain_currency)
                     ->where('effected_at', '<=', $this->chain_time)
                     ->where(function ($query) {
                         $query->whereNull('expires_at')
                               ->orWhere('expires_at', '>=', $this->chain_time)
                         ;
                     })
                     ->orderBy('effected_at', 'desc')
                     ->first()
        ;

        if ($model and $model->exists and $model->amount > 0) {
            return $this->applyTomanIfNecessary($model->amount);
        } else {
            return $this->seek('original')->get();
        }
    }

    protected function getResultDiscount()
    {
        return $this->seek('original')->get() - $this->seek('sale')->get();
    }

    protected function applyTomanIfNecessary($amount)
    {
        if ($this->chain_toman_request) {
            $amount = round($amount / 10);
        }

        return $amount;
    }
}
