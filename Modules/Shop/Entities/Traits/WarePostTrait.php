<?php

namespace Modules\Shop\Entities\Traits;

use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Ware
 *
 * @property Collection $posts
 * @property int        $posttype_id
 */
trait WarePostTrait
{
    protected $cached_post;
    protected $cached_first_post;



    /**
     * get related posttype
     *
     * @return BelongsTo
     */
    public function posttype()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Posttype');
    }



    /**
     * get the related posttype instance, from the singleton system
     *
     * @return \App\Models\Posttype
     */
    public function getPosttypeAttribute()
    {
        return posttype($this->posttype_id);
    }



    /**
     * get related posts
     *
     * @param string $locale
     *
     * @return HasMany
     */
    public function posts($locale = null)
    {
        $table = $this->hasMany(MODELS_NAMESPACE . 'Post', 'sisterhood', 'sisterhood');

        if ($locale) {
            $table->where('locale', $locale);
        }

        return $table;
    }



    /**
     * get related posts in a specified language
     *
     * @param string $locale
     *
     * @return Post|HasMany
     */
    public function postIn($locale)
    {
        return $this->posts->where("locale", $locale)->first();
    }



    /**
     * get the first related post
     *
     * @return Post
     */
    public function getPostAttribute()
    {
        if (!$this->cached_first_post) {
            $this->cached_first_post = $this->posts->first();
        }

        if (!$this->cached_first_post) {
            return post();
        }

        return $this->cached_first_post;
    }
}
