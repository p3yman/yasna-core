<?php
namespace Modules\Shop\Entities\Traits;

trait WareRemarkTrait
{
    /**
     * Gets array of remarks
     *
     * @return array
     */
    public function remarksArray()
    {
        return (array)$this->getMeta('remarks');
    }



    /**
     * Gets an array of remarks
     *
     * @return array
     */
    public function getRemarksAttribute()
    {
        return $this->remarksArray();
    }



    /**
     * Gets remark in a particular language
     *
     * @param string $locale
     *
     * @return string
     */
    public function remarkIn($locale)
    {
        $array = $this->remarksArray();

        if (!count($array)) {
            return null;
        }
        if ($locale == 'first') {
            return $array[0];
        }
        if (isset($array[$locale])) {
            return $array[$locale];
        }

        return null;
    }
}
