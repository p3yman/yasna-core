<?php

namespace Modules\Shop\Entities\Traits;

use App\Models\ShopOffer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Ware
 *
 * @property Collection $offers
 * @property array      $pivot
 */
trait WareOfferTrait
{
    /**
     * get many-to-many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function offers()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . "ShopOffer", "shop_offer_ware", null, "offer_id")
                    ->withTimestamps()
                    ->withPivot("expires_at")
             ;
    }



    /**
     * get many-to-many relationship of the expired offers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function expiredOffers()
    {
        return $this->offers()->wherePivot('expires_at', '<', now());
    }



    /**
     * get dynamic property of the equivalent relationship instance
     *
     * @return Collection
     */
    public function getExpiredOffersAttribute()
    {
        return $this->offers->where("pivot.expires_at", "<", now());
    }



    /**
     * get many-to-many relationship of the unexpired offers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function validOffers()
    {
        return $this->offers()->where(function ($query) {
            $query->where($this->table . 'shop_offers.expires_at', '>=', now())
                  ->orWhere($this->table . "shop_offers.expires_at", null)
            ;
        })
             ;
    }



    /**
     * get dynamic property of the equivalent relationship instance
     *
     * @return Collection
     */
    public function getValidOffersAttribute()
    {
        return $this->validOffers()->get(); //<~~ @TODO: This will cause problems in eager-loading
    }



    /**
     * attach the ware to a specified offer
     *
     * @param int                $offer_id
     * @param null|Carbon|string $expire_date
     *
     * @return bool|int
     */
    public function addToOffer($offer_id, $expire_date = null)
    {
        $offer = $this->getOfferInstance($offer_id);

        if (!$offer->exists) {
            return false;
        }

        $result = $offer->attachWares($this->id, $expire_date);

        return count($result);
    }



    /**
     * detach the ware from a specified offer
     *
     * @param int $offer_id
     *
     * @return bool
     */
    public function removeFromOffer($offer_id)
    {
        $offer = $this->getOfferInstance($offer_id);

        if (!$offer->exists) {
            return false;
        }

        return boolval($offer->detachWares($this->id));
    }



    /**
     * detach the ware from all the offers
     *
     * @return int
     */
    public function removeFromAllOffers()
    {
        $done = 0;

        foreach ($this->offers as $offer) {
            $done += $offer->detachWares($this->id);
        }

        return $done;
    }



    /**
     * get the pivot's expiry time
     *
     * @return string|null
     */
    public function getOfferExpiresAtAttribute()
    {
        if (!isset($this->pivot['expires_at'])) {
            return null;
        }

        return $this->pivot['expires_at'];
    }



    /**
     * check if the offer is expired already
     *
     * @return bool
     */
    public function isOfferExpired()
    {
        if (!isset($this->pivot['expires_at'])) {
            return false;
        }

        if (is_null($this->pivot['expires_at'])) {
            return false;
        }

        return $this->pivot['expires_at'] < now()->toDateTimeString();
    }



    /**
     * check if the offer is still valid
     *
     * @return bool
     */
    public function isOfferValid()
    {
        return !$this->isOfferExpired();
    }



    /**
     * get Offer instance by its identifier
     *
     * @param int $identifier
     *
     * @return ShopOffer
     */
    protected function getOfferInstance($identifier)
    {
        return model("shop-offer", $identifier);
    }
}
