<?php

namespace Modules\Shop\Entities\Traits;

/**
 * Class ShopOffer
 *
 * @property string $slug
 */
trait OfferCanTrait
{
    /**
     * check if the current model can be edited by the current user
     *
     * @return bool
     */
    public function canEdit()
    {
        return user()->isSuperadmin();
    }



    /**
     * check if the current model can be viewed by the current user
     *
     * @return bool
     */
    public function canView()
    {
        return $this->canEdit();
    }



    /**
     * check if the current model can be deleted by the current user
     *
     * @return bool
     */
    public function canDelete()
    {
        if (!dev() and $this->slug) {
            return false;
        }

        return $this->isNotTrashed() and user()->isSuperadmin();
    }



    /**
     * check if the current model can be undeleted/destroyed by the current user
     *
     * @return bool
     */
    public function canBin()
    {
        return $this->isTrashed() and user()->isSuperadmin();
    }
}
