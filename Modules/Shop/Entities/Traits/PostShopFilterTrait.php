<?php

namespace Modules\Shop\Entities\Traits;

use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;

trait PostShopFilterTrait
{
    /**
     * Returns an array resource for the filter endpoint.
     *
     * @param Collection|array $collection
     *
     * @return array
     */
    public function toFilterResource($collection = [])
    {
        $featured_image_hashid = $this->featured_image;
        $featured_image        = $featured_image_hashid
             ? fileManager()->file($featured_image_hashid)->getUrl()
             : null;

        return [
             'id'             => $this->hashid,
             'title'          => $this->title,
             'abstract'       => $this->abstract,
             'featured_image' => $featured_image,
             'wares'          => $this->toFilterWaresResource($collection),
        ];
    }



    /**
     * Returns an array of wares based on the given filtered collection.
     *
     * @param Collection|array $collection
     *
     * @return array
     */
    protected function toFilterWaresResource($collection = [])
    {
        $result = [];

        foreach ($collection as $item) {
            /** @var Ware $ware */
            $ware     = model('ware');
            $ware->id = $item->ware_id;
            $ware->fill([
                 'sale_price'     => $item->ware_sale_price,
                 'original_price' => $item->ware_original_price,
                 'custom_code'    => $item->ware_custom_code,
                 'is_available'   => $item->ware_is_available,
            ]);
            $ware->setMetaArray(json_decode($item->ware_meta, true));

            $result[] = [
                 'id'             => $ware->hashid,
                 'title'          => $ware->titleIn(getLocale()),
                 'sale_price'     => $ware->sale_price,
                 'original_price' => $ware->original_price,
                 'custom_code'    => $ware->custom_code,
                 'is_available'   => $ware->is_available,
            ];
        }

        return $result;
    }
}
