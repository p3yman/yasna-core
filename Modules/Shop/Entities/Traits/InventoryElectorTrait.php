<?php

namespace Modules\Shop\Entities\Traits;

use App\Models\Posttype;

trait InventoryElectorTrait
{
    /**
     * remove the inventory logs, the user has not access to
     */
    protected function consistentElectorPermissions()
    {
        $types = posttypes()->having("shop_inventory")->get();

        $array = [];

        /** @var Posttype $type */
        foreach ($types as $type) {
            if ($type->canShop()) {
                $array[] = $type->id;
            }
        }

        $this->elector()->whereIn("posttype_id", $array);
    }



    /**
     * perform elector against wares by one id, or array of ids, but not hashid.
     *
     * @param array|int $wares
     *
     * @return void
     */
    protected function electorWares($wares)
    {
        $this->elector()->whereIn("ware_id", (array)$wares);
    }



    /**
     * perform elector against the given type(s).
     *
     * @param array|string $types
     *
     * @return void
     */
    protected function electorTypes($types)
    {
        $this->elector()->whereIn("type", (array)$types);
    }
}
