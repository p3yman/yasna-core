<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/30/18
 * Time: 3:17 PM
 */

namespace Modules\Shop\Entities\Traits;


trait WareSortTrait
{
    /**
     * Sortable Fields for Prices
     *
     * @return array
     */
    public function pricesSortableFields()
    {
        $module = module('shop');

        return [
             'original_price' => $module->getTrans('prices.main'),
             'sale_price'     => $module->getTrans('prices.sale'),
        ];
    }



    /**
     * Sortable Fields for Codes
     *
     * @return array
     */
    public function codeSortableFields()
    {
        $module = module('shop');

        return [
             'custom_code' => $module->getTrans('wares.custom_code'),
             'id'          => $module->getTrans('wares.code'),
        ];
    }
}
