<?php
namespace Modules\Shop\Entities\Traits;

use App\Models\Ware;
use Carbon\Carbon;

trait InventoryChainTrait
{
    protected $chain_ware;
    protected $chain_ware_id;
    protected $chain_time;
    protected $chain_invalid       = false;
    protected $chain_type          = 'sold_out';
    protected $chain_description   = null;
    protected $chain_currency      = 0;
    protected $chain_toman_request = false;
    protected $chain_with_package  = false;
    protected $chain_account_id    = 0;
    protected $chain_fee           = 0;



    /*
    |--------------------------------------------------------------------------
    | Builder
    |--------------------------------------------------------------------------
    |
    */

    public static function ask()
    {
        return new self();
    }



    /*
    |--------------------------------------------------------------------------
    | Chain Methods
    |--------------------------------------------------------------------------
    |
    */

    public function for ($ware)
    {
        $this->findWare($ware);
        $this->replaceMasterIfNecessary();

        return $this;
    }



    public function at($date_time_string)
    {
        $this->chain_time = $date_time_string;
        return $this;
    }



    public function type($type)
    {
        $this->chain_type = $type;
        return $this;
    }



    public function description($description)
    {
        $this->chain_description = $description;
        return $this;
    }



    public function in($currency)
    {
        $currency = strtoupper($currency);

        /*-----------------------------------------------
        | Toman Special Provisions ...
        */
        if ($currency == 'IRT') {
            $currency                  = 'IRR';
            $this->chain_toman_request = true;
        }

        /*-----------------------------------------------
        | Actual Set ...
        */
        $this->chain_currency = $currency;
        return $this;
    }



    public function withPackage()
    {
        $this->chain_with_package = true;
        return $this;
    }



    public function withoutPackage()
    {
        $this->chain_with_package = false;
        return $this;
    }



    /**
     * set an account id
     *
     * @param int $account_id
     *
     * @return $this
     */
    public function withAccount($account_id = 0)
    {
        $this->chain_account_id = $account_id;

        return $this;
    }



    /**
     * set fee
     *
     * @param float $amount
     *
     * @return $this
     */
    public function fee($amount = 0)
    {
        $this->chain_fee = $amount;

        return $this;
    }


    /*
    |--------------------------------------------------------------------------
    | Action Methods
    |--------------------------------------------------------------------------
    |
    */


    public function get()
    {
        /*-----------------------------------------------
        | Normalizer ...
        */
        $this->normalizer();

        /*-----------------------------------------------
        | Bypass ...
        */
        if ($this->chain_invalid) {
            return false;
        }
        if ($this->chain_ware->hasnotInventorySystem()) {
            return false;
        }

        /*-----------------------------------------------
        | Process
        */
        return $this->getResult();
    }

    public function alter($alteration)
    {
        $this->normalizer();
        $tracking_no = $this->saveTransaction($this->chain_account_id, $this->chain_fee);

        /*-----------------------------------------------
        | Self ...
        */
        $this->insert([
             'alteration'  => $this->chain_ware->inventory_master_id ? $alteration * $this->chain_ware->package_capacity : $alteration,
             "fee"         => $this->chain_fee,
             'type'        => $this->chain_type,
             'description' => $this->chain_description,
             'posttype_id' => $this->chain_ware->posttype_id,
             'ware_id'     => $this->chain_ware_id,
             "account_id"  => $this->chain_account_id,
             "tracking_no" => $tracking_no,
             //'fee'         => $this->chain_ware->price()
             //                                  ->in($this->chain_currency)
             //                                  ->withoutPackage()
             //                                  ->get(),
        ]);

        /*-----------------------------------------------
        | Package ...
        */
        if ($this->chain_with_package and $this->chain_ware->package_id) {
            $this->insert([
                 'alteration'  => $alteration,
                 'type'        => $this->chain_type,
                 "fee"         => $this->chain_fee,
                 'description' => $this->chain_description,
                 'posttype_id' => $this->chain_ware->posttype_id,
                 'ware_id'     => $this->chain_ware->package_id,
                 "account_id"  => $this->chain_account_id,
                 "tracking_no" => $tracking_no,
                 //'fee'         => $this->chain_ware->package->price()
                 //                                           ->in($this->chain_currency)
                 //                                           ->get(),
            ]);
        }

        /*-----------------------------------------------
        | Cache Update ...
        */
        $this->chain_ware->refreshCurrentInventory();


        /*-----------------------------------------------
        | Blind Return ...
        */
        return true;
    }



    /*
    |--------------------------------------------------------------------------
    | Protected Factory
    |--------------------------------------------------------------------------
    |
    */
    protected function normalizer()
    {
        if (!$this->chain_ware_id) {
            $this->chain_invalid = true;
        }

        if (!$this->chain_time) {
            $this->chain_time = Carbon::now();
        }

        if (!$this->chain_currency) {
            $this->in($this->chain_ware->posttype->currencies_array[0]);
        }
    }



    protected function getResult()
    {
        return self::where('ware_id', $this->chain_ware_id)
                   ->where('effected_at', '<', $this->chain_time)
                   ->sum('alteration')
             ;
    }



    protected function findWare($ware)
    {
        if (is_int($ware)) {
            $ware = Ware::find($ware);
            if (!$ware or !$ware->exists) {
                $ware = new Ware();
            }
        }

        $this->chain_ware       = $ware;
        $this->chain_package_id = $ware->package_id;
        $this->chain_ware_id    = $ware->id;
    }



    protected function replaceMasterIfNecessary()
    {
        if ($this->chain_ware->inventory_master_id) {
            $this->chain_ware_id = $this->chain_ware->inventory_master_id;
        }
    }



    /**
     * save transaction record
     *
     * @param int   $account_id
     * @param float $amount
     *
     * @return int
     */
    private function saveTransaction($account_id, $amount)
    {
        if (!$account_id or !$amount) {
            return 0;
        }

        return payment()
             ->transaction()
             ->accountId($account_id)
             ->invoiceId(0)
             ->callbackUrl('http://www.fake.com')
             ->payableAmount($amount)
             ->getTrackingNumber();
    }
}
