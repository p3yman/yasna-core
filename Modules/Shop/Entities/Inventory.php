<?php

namespace Modules\Shop\Entities;

use App\Models\Account;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Shop\Entities\Traits\InventoryChainTrait;
use Modules\Shop\Entities\Traits\InventoryElectorTrait;
use Modules\Yasna\Services\YasnaModel;

class Inventory extends YasnaModel
{
    use SoftDeletes;
    use InventoryChainTrait;
    use InventoryElectorTrait;

    public static $available_types = [
         'sold_out', // when something is sold
         'supplied_in', // when something is supplied
         'returned_in', // when returned from customer,
         'returned_out', // when returned to the supplier
         'thrown_out', //when thrown away, like expired or broken or however removed from the store without any reason.
    ];
    protected     $casts           = [
         'meta' => "array",
    ];



    /**
     * get the array required to build a types combo
     *
     * @return array
     */
    public static function typesCombo()
    {
        $array = [];
        foreach (self::$available_types as $type) {
            $array[] = [
                 'id'    => $type,
                 'title' => trans("shop::inventories.$type"),
            ];
        }

        return $array;
    }



    /**
     * inserts a record into the inventories table
     *
     * @param array $data
     *
     * @return bool
     */
    public static function insert($data)
    {
        /*-----------------------------------------------
        | Type Evaluation ...
        */
        if (!in_array($data['type'], self::$available_types)) {
            return false;
        }

        /*-----------------------------------------------
        | Effective Time ...
        */
        if (!isset($data['effected_at']) or !$data['effected_at']) {
            $data['effected_at'] = Carbon::now()->toDateTimeString();
        }


        /*-----------------------------------------------
        | Amount Modification ...
        */
        $data['alteration'] = abs($data['alteration']);
        if (str_contains($data['type'], 'out')) {
            $data['alteration'] = -$data['alteration'];
        }

        /*-----------------------------------------------
        | Query ...
        */
        return self::create($data);
    }



    /**
     * get the related posttype
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posttype()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Posttype');
    }



    /**
     * get the related ware
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ware()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Ware');
    }



    /**
     * get the related account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }



    /**
     * determine if the record is an inlet change in inventory
     *
     * @return bool
     */
    public function isInlet()
    {
        return ends_with($this->type, "_in");
    }



    /**
     * determine if the record is an inlet change in inventory
     *
     * @return bool
     */
    public function isOutlet()
    {
        return ends_with($this->type, "_out");
    }



    /**
     * get type text in the current locale
     *
     * @return string
     */
    public function typeText()
    {
        return trans("shop::inventories.$this->type");
    }
}
