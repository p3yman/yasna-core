<?php

namespace Modules\Shop\Entities;

use Modules\Shop\Entities\Traits\OfferCanTrait;
use Modules\Shop\Entities\Traits\OfferComboTrait;
use Modules\Shop\Entities\Traits\OfferExpiryTrait;
use Modules\Shop\Entities\Traits\OfferLinksTrait;
use Modules\Shop\Entities\Traits\OfferPanelTrait;
use Modules\Shop\Entities\Traits\OfferWaresTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopOffer extends YasnaModel
{
    use SoftDeletes;
    use OfferLinksTrait;
    use OfferPanelTrait;
    use OfferCanTrait;
    use OfferWaresTrait;
    use OfferExpiryTrait;
    use OfferComboTrait;



    /**
     * Add meta fields related to the discount amount
     *
     * @return array
     */
    public function discountMetaFields(): array
    {
        return [
             'maximum_applicable_amount',
             'discount_percentage',
        ];
    }
}
