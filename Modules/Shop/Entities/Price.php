<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Shop\Entities\Traits\PriceChainTrait;
use Modules\Yasna\Services\YasnaModel;

class Price extends YasnaModel
{
    use SoftDeletes;
    use PriceChainTrait;

    protected static $available_types = [
        'original',
        'sale',
        'supply',
    ];
    protected $casts           = [
        'meta'        => "array",
        'is_sale'     => 'boolean',
        'effected_at' => 'datetime',
        'expires_at'  => 'datetime',
    ];


    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */
    public function ware()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Ware');
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    public function whereExpiredPrices()
    {
        return self::whereNotNull('expires_at')
                   ->where('expires_at', '<', now())
                   ->orderBy('effected_at', 'desc')
                   ->groupBy('ware_id')
            ;
    }

    public function expiredPrices()
    {
        return $this->whereExpiredPrices()->get() ;
    }
}
