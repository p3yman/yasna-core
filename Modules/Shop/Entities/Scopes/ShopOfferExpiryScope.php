<?php

namespace Modules\Shop\Entities\Scopes;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ShopOfferExpiryScope implements Scope
{

    /**
     * @inheritdoc
     */
    public function apply(Builder $builder, Model $model)
    {
    }



    /**
     * register the extending methods
     *
     * @param Builder $builder
     */
    public function extend(Builder $builder)
    {
        $builder->macro("whereExpired", function (Builder $builder, $date = null) {
            return $this->whereExpired($builder, $date);
        });
        $builder->macro("whereNotExpired", function (Builder $builder, $date = null) {
            return $this->whereNotExpired($builder, $date);
        });
        $builder->macro("whereValid", function (Builder $builder, $date = null) {
            return $this->whereNotExpired($builder, $date);
        });
    }



    /**
     * add `whereExpired($date)` scope
     *
     * @param Builder            $builder
     * @param string|Carbon|null $date
     *
     * @return Builder
     */
    protected function whereExpired(Builder $builder, $date = null)
    {
        if (!$date) {
            $date = now();
        }

        return $builder->where("expires_at", '<', $date);
    }



    /**
     * add `whereNotExpired($date)` scope
     *
     * @param Builder $builder
     * @param null    $date
     *
     * @return Builder
     */
    protected function whereNotExpired(Builder $builder, $date = null)
    {
        if (!$date) {
            $date = now();
        }

        return $builder->where(function ($query) use ($date) {
            $query->where("expires_at", ">=", $date)->orWhere("expires_at", null);
        });
    }
}
