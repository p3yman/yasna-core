<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Services\YasnaModel;

class Unit extends YasnaModel
{
    use SoftDeletes;
    public static $reserved_slugs = 'root,admin,upstream';

    /*
    |--------------------------------------------------------------------------
    | Relation
    |--------------------------------------------------------------------------
    |
    */

    public function parent()
    {
        /*-----------------------------------------------
        | If Self ...
        */
        if ($this->ratio == 1) {
            return $this;
        }

        /*-----------------------------------------------
        | Otherwise ...
        */
        $parent = self::withTrashed()->where('concept', $this->concept)->where('ratio', 1)->first();

        return $parent;
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function getTitleAttribute($locale = false)
    {
        return self::trans($this->slug, $locale);
    }

    public function conceptTitle($locale = false)
    {
        return self::trans($this->concept, $locale);
    }

    public function getConceptTitleFaAttribute()
    {
        return $this->conceptTitle('fa');
    }


    public function equivalent($amount = 1, $unit_slug = false)
    {
        /*-----------------------------------------------
        | Finding the destination ...
        */
        if ($unit_slug) {
            $destination = self::withTrashed()->where('concept', $this->concept)->where('slug', $unit_slug)->first();
        } else {
            $destination = $this->parent();
        }

        /*-----------------------------------------------
        | Safety ...
        */
        if (!$destination or !$destination->exists) {
            return false;
        }

        /*-----------------------------------------------
        | Conversion ...
        */
        $parent_equivalent = $amount * $this->ratio;
        $final_equivalent  = $parent_equivalent / $destination->ratio;

        return $final_equivalent;
    }

    public function equivalentFull($amount = 1, $unit_slug = false, $locale = false)
    {
        if (!$unit_slug) {
            $unit_slug = $this->parent()->slug;
        }

        return $this->equivalent($amount, $unit_slug) . SPACE . self::trans($unit_slug, $locale);
    }


    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    public static function trans($slug, $locale = false)
    {
        if (!$locale) {
            $locale = getLocale();
        }

        return trans("shop::units.$slug", [], $locale);
    }

    public function conceptsCombo()
    {
        return self::groupBy('concept')->get();
    }
}
