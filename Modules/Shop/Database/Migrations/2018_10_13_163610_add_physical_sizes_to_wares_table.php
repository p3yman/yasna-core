<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhysicalSizesToWaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wares', function (Blueprint $table) {
            $table->float('height')->after('locales');
            $table->float('width')->after('locales');
            $table->float('length')->after('locales');
            $table->float('mass')->after('locales');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wares', function (Blueprint $table) {
            $table->dropColumn([
                 'mass',
                 'length',
                 'width',
                 'height',
            ]);
        });
    }
}
