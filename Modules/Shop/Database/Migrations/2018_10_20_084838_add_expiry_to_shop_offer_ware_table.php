<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiryToShopOfferWareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shop_offer_ware', function (Blueprint $table) {

            $table->timestamp('expires_at')->after('ware_id')->nullable()->index();

        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shop_offer_ware', function (Blueprint $table) {
            $table->dropColumn("expires_at");
        });
    }
}
