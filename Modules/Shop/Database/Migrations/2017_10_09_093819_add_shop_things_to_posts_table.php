<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopThingsToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->float('original_price', 15, 2)->default(0)->after('meta')->index(); // <~~ Default Currency Only!
            $table->float('sale_price', 15, 2)->default(0)->after('meta')->index(); // <~~ Default Currency Only!
            $table->integer('total_wares')->default(0)->after('meta')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn(['original_price', 'sale_price','total_wares']);
        });
    }
}
