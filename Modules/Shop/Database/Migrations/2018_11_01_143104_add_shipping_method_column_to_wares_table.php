<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddShippingMethodColumnToWaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wares', function (Blueprint $table) {
            $table->unsignedTinyInteger('approximated_size')
                  ->after('width')
                  ->defaultValue(0)
            ;
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wares', function (Blueprint $table) {
            $table->dropColumn('approximated_size');
        });
    }
}
