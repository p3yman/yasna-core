<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopOfferWareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_offer_ware', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('offer_id')->index();
            $table->unsignedInteger('ware_id')->index();

            $table->timestamps();

            $table->foreign("offer_id")->references("id")->on("shop_offers")->onDelete("cascade");
            $table->foreign("ware_id")->references("id")->on("wares")->onDelete("cascade");
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_offer_ware');
    }
}
