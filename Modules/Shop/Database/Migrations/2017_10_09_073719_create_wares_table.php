<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wares', function (Blueprint $table) {
            $table->increments('id');

            /*-----------------------------------------------
            | Relations ...
            */
            $table->unsignedInteger('posttype_id')->index();
            $table->unsignedInteger('package_id')->index();
            $table->unsignedInteger('unit_id')->index();
            $table->string('sisterhood')->nullable()->index();
            $table->unsignedInteger('inventory_master_id')
                  ->default(0)
            ; // <~~ 0 for manual check, if enabled in posttype!

            /*-----------------------------------------------
            | Main Info ...
            */
            $table->string('locales');
            $table->string('currencies')->nullable();
            $table->text('labels')->nullable(); // <~~ something like: "en-color:red fr-color:rouge"
            $table->tinyInteger('order')->default(0);
            $table->tinyInteger('fee_unit')->default(1); //<~~ Price for how-much? Ex. each 1000 gr => $123
            $table->boolean('is_available')->default(1); // <~~ Available, even if inventory system is engaged.
            $table->integer('package_capacity')->default(0);

            /*-----------------------------------------------
            | Cache ...
            */
            $table->float('original_price', 15, 2)->default(0)->index(); // <~~ Default Currency Only!
            $table->float('sale_price', 15, 2)->default(0)->index(); // <~~ Default Currency Only!
            $table->integer('current_inventory')->default(0)->index();
            $table->integer('package_inventory')->default(0)->index();
            $table->boolean('has_enough_inventory')->default(0)->index(); // <~~ Considering the package
            $table->boolean('has_enough_package')->default(0)->index(); // <~~ Considering the package

            /*-----------------------------------------------
            | Meta ...
            */
            $table->longText('meta')->nullable();


            /*-----------------------------------------------
            | Timestamps ...
            */
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);

            $table->index('created_at');

            /*-----------------------------------------------
            | Reserved for Later Use ...
            */
            $table->boolean('converted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wares');
    }
}
