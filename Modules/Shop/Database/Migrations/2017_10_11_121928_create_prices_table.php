<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('ware_id')->index();
            $table->float('amount', 15, 2)->default(0)->index();
            $table->string('currency', 10)->default('IRR')->index();

            $table->string('type')->index();
            $table->timestamp('expires_at')->nullable() ;

            /*-----------------------------------------------
            | Meta ...
            */
            $table->longText('meta')->nullable();


            /*-----------------------------------------------
            | Timestamps ...
            */
            $table->timestamps();
            $table->timestamp('effected_at')->nullable();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);


            /*-----------------------------------------------
            | Reserved for Later Use ...
            */
            $table->boolean('converted')->default(0);

            /*-----------------------------------------------
            | Foreign Keys ...
            */
            $table->foreign('ware_id')->references('id')->on('wares')->onDelete('cascade');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
