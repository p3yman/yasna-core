<?php

namespace Modules\Shop\Database\Seeders;

use App\Models\Input;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class InputsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('inputs', $this->data());
    }

    protected function data()
    {
        $data = [
            [
                'slug'              => 'sale_type',
                'title'             => 'نوع فروش',
                'order'             => '40',
                'type'              => 'downstream',
                'data_type'         => 'combo',
                'css_class'         => 'form-required',
                'validation_rules'  => 'required',
                'purifier_rules'    => 'ed',
                'default_value'     => '',
                'default_value_set' => '0',
                'hint'              => '',
                'meta-options'      => "soft:خدمات یا محصولات نرم‌افزاری قابل دانلود" . LINE_BREAK . 'hard: محصولات فیزیکی',
            ],
            [
                'slug'              => 'min_inventory_alarm',
                'title'             => 'حداقل موجودی برای هشدار به ادمین',
                'order'             => '99',
                'type'              => 'downstream',
                'data_type'         => 'text',
                'css_class'         => '',
                'validation_rules'  => 'numeric',
                'purifier_rules'    => 'ed',
                'default_value'     => '15',
                'default_value_set' => '0',
                'hint'              => 'اگر هشدار لازم ندارید، عدد صفر را وارد کنید.',
                'meta-options'      => '',

            ],
            [
                'slug'              => 'min_inventory_stop',
                'title'             => 'حداقل موجودی برای توقف فروش',
                'order'             => '99',
                'type'              => 'downstream',
                'data_type'         => 'text',
                'css_class'         => '',
                'validation_rules'  => 'numeric',
                'purifier_rules'    => 'ed',
                'default_value'     => '5',
                'default_value_set' => '0',
                'hint'              => 'اگر به این قابلیت نیازی ندارید، عدد صفر را وارد کنید.',
                'meta-options'      => '',

            ],
             [
                 'slug' => "currencies" ,
                  'title' => trans("shop::currencies.available") ,
                  'order' => "99" ,
                  'type' => "upstream" ,
                  'data_type' => "text" ,
                 'css_class' => "ltr" ,
                  'default_value' => "IRR,USD" ,
                  'hint' => trans("shop::currencies.setting_hint") ,
                 'purifier_rules'    => 'upper',
                 'validation_rules'  => '',
            ],

        ];
        return $data ;
    }
}
