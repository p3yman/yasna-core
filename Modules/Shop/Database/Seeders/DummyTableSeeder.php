<?php

namespace Modules\Shop\Database\Seeders;

use App\Models\Post;
use App\Models\Posttype;
use App\Models\Unit;
use App\Models\User;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection as BaseCollection;
use Modules\Yasna\Providers\DummyServiceProvider;

class DummyTableSeeder extends Seeder
{
    /** @var Collection */
    protected $shop_posttypes;

    /** @var Collection */
    protected $users_list;

    /** @var array|Collection[] */
    protected $packages_list = [];

    /** @var BaseCollection */
    protected $inserted_posts;

    /** @var Collection */
    protected $units;

    /** @var Collection */
    protected $summarized_wares;



    /**
     * DummyTableSeeder constructor.
     */
    public function __construct()
    {
        $this->inserted_posts = collect();
    }



    /**
     * Runs a lightweight version of the seeder to be called through a request.
     */
    public static function runThroughRequest()
    {
        $obj = new static();

        $obj->call(PosttypesTableSeeder::class);

        $obj->labels(5, 20);
        $obj->packages(10, true);
        $obj->posts(20);
        $obj->wares(5);
    }



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ShopDatabaseSeeder::class);
        $this->call(PosttypesTableSeeder::class);

        $this->labels(5, 20);
        $this->packages(10, true);
        $this->posts();
        $this->wares(5);
    }



    /**
     * Seeds dummy data into posts table
     *
     * @param int $children
     * @param int $masters
     */
    private function labels(int $children = 100, int $masters = 10)
    {
        $this->labelsMaster($masters);
        $this->labelsSlaves($children);
    }



    /**
     * Seeds some parent labels.
     *
     * @param int $total
     */
    private function labelsMaster(int $total = 10)
    {
        for ($i = 1; $i <= $total; $i++) {
            label()->define('Ware', "shop" . ware()::LABEL_POSTTYPE_DIVIDER . str_slug(dummy()::englishWord()),
                 dummy()::persianWord());
        }
    }



    /**
     * Seeds some children label
     *
     * @param int $total
     */
    private function labelsSlaves(int $total = 100)
    {
        $masters = label()->mastersOf('Ware')->get();

        for ($i = 1; $i <= $total; $i++) {
            $random_parent = $masters->random();
            $random_slug   = "shop" . ware()::LABEL_POSTTYPE_DIVIDER . str_slug(dummy()::englishWord());
            $titles        = [
                 "fa" => dummy()::persianWord(),
                 "en" => dummy()::englishWord(),
            ];

            label()->define("Ware", $random_slug, $titles, $random_parent->id);
        }
    }



    /**
     * Seeds dummy data into posts table
     *
     * @param int  $count
     * @param bool $truncate
     */
    private function posts($count = 100, $truncate = true)
    {
        if ($truncate) {
            post()->where("slug", "like", "%dummy-shop-%")->forceDelete();
        }

        for ($i = 0; $i < $count; $i++) {
            $deleted   = boolval(rand(1, 20) > 18);
            $published = boolval(rand(1, 20) > 10);
            $type      = posttype('shop');

            $this->inserted_posts[] = post()->batchSave([
                 'slug'         => str_slug("dummy-shop-" . strtolower(DummyServiceProvider::englishWord())),
                 'type'         => $type->slug,
                 'title'        => DummyServiceProvider::persianTitle(),
                 'title2'       => rand(1, 5) > 4 ? DummyServiceProvider::persianTitle() : '',
                 'locale'       => array_random($type->locales_array),
                 'sisterhood'   => hashid(rand(5000, 500000)),
                 'abstract'     => DummyServiceProvider::persianWord(rand(10, 50)),
                 'text'         => DummyServiceProvider::persianText(rand(1, 30)),
                 'deleted_at'   => $deleted ? now()->toDateTimeString() : null,
                 'deleted_by'   => $deleted ? $this->randomUser()->id : 0,
                 'published_at' => $published ? now()->toDateTimeString() : null,
                 'published_by' => $published ? $this->randomUser()->id : 0,
                 'created_by'   => $this->randomUser()->id,
                 'owned_by'     => $this->randomUser()->id,
                 'moderated_by' => $published ? $this->randomUser()->id : rand(0, 1),
                 'moderated_at' => now()->toDateTimeString(),
            ]);
        }
    }



    /**
     * Seeds dummy data into wares table to be used as packages
     *
     * @param int  $count
     * @param bool $truncate
     */
    private function packages($count = 100, $truncate = false)
    {
        $data = [];

        for ($i = 1; $i <= $count; $i++) {
            $posttype = $this->randomShopPosttype();

            if (!$posttype) {
                continue;
            }

            $data[] = [
                 'posttype_id'  => $posttype->id,
                 'package_id'   => 0,
                 'unit_id'      => 0,
                 'sisterhood'   => null,
                 'locales'      => "fa,en",
                 'currencies'   => "IRR",
                 'order'        => rand(1, 50),
                 'is_available' => rand(0, 1),
                 'meta-titles'  => [
                      'fa' => DummyServiceProvider::persianWord(),
                      'en' => DummyServiceProvider::englishWord(),
                 ],
            ];
        }

        yasna()->seed('wares', $data, $truncate);

        if ($truncate) {
            model('Price')->whereNotNull('id')->forceDelete();
            model('Inventory')->whereNotNull('id')->forceDelete();
        }
    }



    /**
     * Seeds dummy data into wares table to be used as products
     *
     * @param int  $limit
     * @param bool $truncate
     */
    private function wares($limit = 5, $truncate = false)
    {
        if ($truncate) {
            // delete all wares which belong to a post.
            model('ware')->whereNotNull("sisterhood")->forceDelete();
        }

        $data = [];

        foreach ($this->inserted_posts as $post) {
            $data = array_merge($data, $this->postWares($post, $limit));
        }

        yasna()->seed('wares', $data, false);

        $this->prices();
        $this->inventories();
    }



    /**
     * Returns an array of the wares to be inserted based on the given posttype and limit.
     *
     * @param Post $post
     * @param int  $limit The maximum number of wares to be inserted for the post.
     *
     * @return array
     */
    protected function postWares(Post $post, int $limit)
    {
        $data = [];

        $posttype = $post->posttype();
        $package  = $this->randomPosttypePackage($posttype);

        if ($posttype->not_exists or $package->not_exists or $post->not_exists) {
            return $data;
        }

        $count = rand(0, $limit);

        for ($i = 0; $i < $count; $i++) {

            $data[] = [
                 'posttype_id'      => $posttype->id,
                 'package_id'       => $package->id,
                 'unit_id'          => $this->randomUnit()->id,
                 'sisterhood'       => $post->sisterhood,
                 'locales'          => "fa,en",
                 'order'            => rand(1, $count),
                 'is_available'     => rand(0, 1),
                 'package_capacity' => rand(1, 100),
                 'meta-titles'      => [
                      'fa' => DummyServiceProvider::persianWord(),
                      'en' => DummyServiceProvider::englishWord(),
                 ],
            ];
        }

        return $data;
    }



    /**
     * Seeds dummy data into prices table
     */
    private function prices()
    {
        $data = [];

        foreach ($this->summarizedWares() as $ware) {
            $original_price = rand(1000, 100000);
            $sale_prices    = rand(1000, $original_price);
            $date           = now()->subDays(rand(0, 12));

            $data[] = [
                 'ware_id'     => $ware->id,
                 'amount'      => $original_price,
                 'currency'    => "IRR",
                 'type'        => "original",
                 'expires_at'  => null,
                 'effected_at' => $date,
                 'created_by'  => user()->id,
                 'created_at'  => $date,
            ];


            $data[] = [
                 'ware_id'     => $ware->id,
                 'amount'      => $sale_prices,
                 'currency'    => "IRR",
                 'type'        => "sale",
                 'expires_at'  => $date->addDays(rand(0, 10)),
                 'effected_at' => $date->addDays(rand(0, 3)),
                 'created_by'  => user()->id,
                 'created_at'  => $date,
            ];
        }

        yasna()->seed('prices', $data);
    }



    /**
     * Seeds dummy data into inventories table
     */
    private function inventories()
    {
        foreach ($this->summarizedWares() as $ware) {
            $ware->setInventory([
                 'alteration' => rand(100, 1000),
                 'fee'        => rand(1000, 100000),
            ]);
        }
    }



    /**
     * Returns an instance of the Posttype model which has been selected randomly from the shop posttypes.
     *
     * @return Posttype
     */
    protected function randomShopPosttype()
    {
        return $this->shopPosttypes()->random();
    }



    /**
     * Returns the posttypes with `shop` feature.
     *
     * @return Collection
     */
    protected function shopPosttypes()
    {
        if (!$this->shop_posttypes) {
            $this->shop_posttypes = posttype()->having('shop')->get();
        }

        return $this->shop_posttypes;
    }



    /**
     * Returns an instance of the User model which has been selected in random order.
     *
     * @return User
     */
    protected function randomUser()
    {
        return $this->usersList()->random();
    }



    /**
     * Returns a random collection of the user which will contain 10 users at most.
     *
     * @return Collection
     */
    protected function usersList()
    {
        if (!$this->users_list) {
            $this->users_list = user()->limit(10)->inRandomOrder()->get();
        }

        return $this->users_list;
    }



    /**
     * Returns a random package of the given posttype.
     *
     * @param Posttype $posttype
     *
     * @return Ware
     */
    protected function randomPosttypePackage(Posttype $posttype)
    {
        $packages = $this->posttypePackages($posttype);

        return ($packages->count() ? $packages->random() : ware());
    }



    /**
     * Returns a random collection of the packages for the specified posttype which will contain 10 items at most.
     *
     * @param Posttype $posttype
     *
     * @return Collection
     */
    protected function posttypePackages(Posttype $posttype)
    {
        $slug = $posttype->slug;

        if (!array_key_exists($slug, $this->packages_list)) {
            $this->packages_list[$slug] = $posttype->packages()->limit(10)->get();
        }

        return $this->packages_list[$slug];
    }



    /**
     * Returns a random unit.
     *
     * @return Unit
     */
    protected function randomUnit()
    {
        $units = $this->units();

        return $units->count() ? $units->random() : model('unit');
    }



    /**
     * Returns a random collection of the units.
     *
     * @return Collection
     */
    protected function units()
    {
        if (!$this->units) {
            $this->units = model('unit')->whereConcept('weight')->inRandomOrder()->get();
        }

        return $this->units;
    }



    /**
     * Returns a collection of the wares which each one contains only the `id` and `posttype_id` fields.
     *
     * @return Collection
     */
    protected function summarizedWares()
    {
        if (!$this->summarized_wares) {
            $inserted_posts_sisterhoods = collect($this->inserted_posts)
                 ->pluck('sisterhood')
                 ->toArray()
            ;

            $this->summarized_wares = model('ware')
                 ->whereIn('sisterhood', $inserted_posts_sisterhoods)
                 ->select(['id', 'posttype_id'])
                 ->get()
            ;
        }

        return $this->summarized_wares;
    }
}
