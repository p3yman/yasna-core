<?php

namespace Modules\Shop\Database\Seeders;

use App\Models\Feature;
use Illuminate\Database\Seeder;
use Modules\Yasna\Providers\YasnaServiceProvider;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->mainData());
        $this->relationships();
    }

    protected function mainData()
    {
        $data = [
            [
                'slug'      => 'shop',
                'title'     => 'فروشگاه',
                'order'     => '13',
                'icon'      => 'shopping-basket',
                'post_meta' => '',
                'meta-hint' => '',
            ],
            [
                'slug'      => "shop_inventory",
                'title'     => trans("shop::inventories.type_setting"),
                'order'     => "14",
                'icon'      => "paw",
                'meta-hint' => "بدون قابلیت فروشگاه، فایده‌ای ندارد.",
            ],
        ];

        return $data;
    }

    protected function relationships()
    {
        $data = [
            'shop' => "sale_type,gallery_thumb_size,max_per_page,fresh_time_duration,min_inventory_stop,min_inventory_alarm,currencies",
        ];

        foreach ($data as $key => $slugs) {
            $model = model("feature", $key);
            if (!$model or !$model->id) {
                continue;
            }
            $model->attachInputs(explode(',', $slugs));
        }
    }
}
