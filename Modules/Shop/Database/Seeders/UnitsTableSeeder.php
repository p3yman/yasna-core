<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('units', $this->mainData());
    }

    public function mainData()
    {
        $data = [
            [
                'concept' => "quantity" ,
                'slug' => "numbers" ,
                'ratio' => "1" ,
            ],

             [
                 'concept' => "weight" ,
                  'slug' => "g" ,
                  'ratio' => "1" ,
            ],
            [
                'concept' => "weight" ,
                'slug' => "kg" ,
                'ratio' => "1000" ,
            ],
            [
                'concept' => "weight" ,
                'slug' => "mithqal" ,
                'ratio' => "4.68" ,
            ],

            [
                'concept' => "length" ,
                'slug' => "m" ,
                'ratio' => "1" ,
                '_deleted' => "1" ,
            ] ,
            [
                'concept' => "length" ,
                'slug' => "cm" ,
                'ratio' => "0.01" ,
                '_deleted' => "1" ,
            ],

             [
                 'concept' => "area" ,
                  'slug' => "m2" ,
                  'ratio' => "1" ,
                 '_deleted' => "1" ,
            ],
            [
                'concept' => "volume" ,
                'slug' => "litre" ,
                'ratio' => "1" ,
                '_deleted' => "1" ,
            ],
             [
                 'concept' => "volume" ,
                  'slug' => "m3" ,
                  'ratio' => "1000" ,
                 '_deleted' => "1" ,
            ],
             [
                 'concept' => "time" ,
                  'slug' => "second" ,
                  'ratio' => "1" ,
                 '_deleted' => "1" ,
            ],
             [
                 'concept' => "capacity" ,
                  'slug' => "byte" ,
                  'ratio' => "1" ,
                 '_deleted' => "1" ,
            ],
        ] ;

        return $data ;
    }
}
