<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->mainData());
    }



    /**
     * Returns an array of the main data to be seeded.
     *
     * @return array
     */
    public function mainData()
    {
        return [
             [
                  'slug'          => 'shop_ware_prefix',
                  'title'         => trans("shop::seeder.prefix_title"),
                  'category'      => 'upstream',
                  'order'         => '12',
                  'data_type'     => 'text',
                  'default_value' => 'YSP',
                  'hint'          => trans("shop::seeder.prefix_hint"),
                  'css_class'     => 'ltr',
                  'is_localized'  => '',
             ],
             [
                  'slug'          => model('ware')->getUniqueUrlPatternSlug(),
                  'title'         => $this->runningModule()->getTrans('seeder.ware-unique-url-pattern'),
                  'category'      => 'upstream',
                  'order'         => '20',
                  'data_type'     => 'text',
                  'default_value' => '',
                  'hint'          => module('yasna')->getTrans('seeders.unique-url-pattern-hint'),
                  'css_class'     => 'ltr',
                  'is_localized'  => '1',
             ],
             [
                  'slug'          => 'shop_location',
                  'title'         => trans("shop::seeder.shop_location"),
                  'category'      => 'database',
                  'order'         => '12',
                  'data_type'     => 'state',
                  'default_value' => '135',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
        ];
    }
}
