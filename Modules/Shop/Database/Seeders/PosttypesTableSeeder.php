<?php

namespace Modules\Shop\Database\Seeders;

use App\Models\Posttype;
use Illuminate\Database\Seeder;
use Modules\Yasna\Providers\YasnaServiceProvider;

class PosttypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('posttypes', $this->mainData());
        $this->relationships();
    }



    /**
     * prepare data
     * //TODO: Persian words are to be moved to the trans file.
     *
     * @return array
     */
    protected function mainData()
    {
        if (model("posttype","shop")->exists) {
            return [];
        }

        return [
             [
                  'slug'           => 'shop',
                  'title'          => 'محصولات',
                  'order'          => '11',
                  'singular_title' => 'محصول',
                  'template'       => 'shop',
                  'icon'           => 'shopping-basket',
                  'locales'        => "fa,en",
             ],
             [
                  'slug'           => "fruits",
                  'title'          => "میوه‌جات",
                  'order'          => "11",
                  'singular_title' => "میوه",
                  'template'       => "shop",
                  'icon'           => "apple",
             ],
        ];

    }



    /**
     * attach relationships
     *
     * @param bool $data |array
     */
    protected function relationships($data = false)
    {
        if (!$data) {
            $data = [
                 'shop'   => "text,single_view,searchable,manage_sidebar,abstract,locales,album,list_view,category,pin,category_image,featured_image,rss,shop,shop_inventory",
                 'fruits' => "text,single_view,searchable,manage_sidebar,abstract,locales,album,list_view,category,pin,category_image,featured_image,rss,shop",
            ];
        }

        foreach ($data as $key => $slugs) {
            $model = posttype($key);
            if (!$model or !$model->id) {
                continue;
            }
            $model->attachFeatures(explode(',', $slugs));
            $model->attachRequiredInputs();
            $model->cacheFeatures();
        }
    }
}
