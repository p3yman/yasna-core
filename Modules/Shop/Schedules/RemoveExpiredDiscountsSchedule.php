<?php namespace Modules\Shop\Schedules;

use Modules\Shop\Entities\Price;
use Modules\Yasna\Services\YasnaSchedule;

class RemoveExpiredDiscountsSchedule extends YasnaSchedule
{
    // Feel free to incorporate additional() and condition() methods, or even override handle() method as appropriates.

    protected function job()
    {
        $expired_prices = model('price')->expiredPrices();
        foreach ($expired_prices as $key => $price) {
            $ware = model('ware', $price->ware_id) ;
            $ware->refreshCurrentPrices();
            if ($key==0) {
                $ware->refreshPostPrices();
            }
        }
    }
}
