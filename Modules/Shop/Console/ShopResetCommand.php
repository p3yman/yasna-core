<?php

namespace Modules\Shop\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;

class ShopResetCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'shop:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Truncates all the tables in the 'Cart' and 'Shop' modules.";



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->truncateCartModuleTables();
        $this->truncateShopModuleTables();
    }



    /**
     * Truncates the 'Cart' module tables.
     */
    protected function truncateCartModuleTables()
    {
        model('courier')->truncate();
        $this->info("'couriers' table is truncated.");

        model('cart')->truncate();
        $this->info("'carts' table is truncated.");

        model('order')->truncate();
        $this->info("'orders' table is truncated.");

        model('cart_record')->truncate();
        $this->info("'cart_records' table is truncated.");
    }



    /**
     * Truncates the 'Shop' module tables.
     */
    protected function truncateShopModuleTables()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        model('ware')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->info("'wares' table is truncated.");

        model('unit')->truncate();
        $this->info("'units' table is truncated.");

        model('price')->truncate();
        $this->info("'prices' table is truncated.");

        model('inventory')->truncate();
        $this->info("'inventories' table is truncated.");
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //['example', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            //['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
