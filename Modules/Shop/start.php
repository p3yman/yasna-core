<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("ware")) {
    /**
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return \App\Models\Ware
     */
    function ware($id = 0, $with_trashed = false)
    {
        return model('ware', $id, $with_trashed);
    }
}

if (!function_exists("unit")) {

    /**
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return \App\Models\Unit
     */
    function unit($id = 0, $with_trashed = false)
    {
        return model('unit', $id, $with_trashed);
    }
}

if (!function_exists("price")) {

    /**
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return \App\Models\Price
     */
    function price($id = 0, $with_trashed = false)
    {
        return model('price', $id, $with_trashed);
    }
}

if (!function_exists("inventory")) {

    /**
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return \App\Models\Inventory
     */
    function inventory($id = 0, $with_trashed = false)
    {
        return model('inventory', $id, $with_trashed);
    }
}

if (!function_exists("is_decimal")) {
    /**
     * check that number is decimal or not
     *
     * @param float|int $val
     *
     * @return bool
     */
    function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }
}