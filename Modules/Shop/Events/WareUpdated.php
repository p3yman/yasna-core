<?php

namespace Modules\Shop\Events;

use App\Models\Ware;
use Modules\Yasna\Services\YasnaEvent;

class WareUpdated extends YasnaEvent
{
    public $model_name = 'Ware';
    public $old_model  = null;



    /**
     * WareUpdated constructor.
     *
     * @param string|int|Ware      $model
     * @param string|int|Ware|bool $old_model
     */
    public function __construct($model, $old_model = null)
    {
        $this->oldModelSelector($old_model);

        parent::__construct($model);
    }



    /**
     * Selects the old model if exists.
     *
     * @param string|int|Ware|bool $model
     */
    public function oldModelSelector($model)
    {
        $found_model = model($this->model_name, $model);

        if ($found_model->not_exists) {
            return;
        }

        $this->old_model = $found_model;
    }



    /**
     * Return an instance based on the old model.
     *
     * @return Ware
     */
    public function getOldModel()
    {
        return ($this->old_model ?: model($this->model_name));
    }
}
