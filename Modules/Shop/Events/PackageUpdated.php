<?php

namespace Modules\Shop\Events;

use Illuminate\Queue\SerializesModels;

class PackageUpdated
{
    use SerializesModels;
    public $model ;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model)
    {
        if (is_numeric($model) or is_string($model)) {
            $model = model('ware', $model) ;
        }
        $this->model = $model;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
