/**
 * Module Shop Js
 * Author: Negar Jamalifard
 * 2017-1-8
 */

// Gets order of wares
function generateSortOrder(event, ui) {

    $(ui.item).find('.reorder__handler').click();
    let sortorder = '';
    let $this     = $('.column');
    let itemorder = $this.sortable('toArray');
    let columnId  = $this.attr('id');
    sortorder += columnId + '=' + itemorder.toString();

    $('#txtOrder').val(sortorder);
}

function orderFormToggle() {
    $('.-order-edit').toggle();
//    $('#txtOrder').val('');
}


function orderRun() {
    $('.column').sortable({
        connectWith         : '.column',
        handle              : '.reorder__handler',
        cursor              : 'move',
        placeholder         : 'placeholder',
        forcePlaceholderSize: true,
        opacity             : 0.4,
        stop                : function (event, ui) {
            generateSortOrder(event, ui);
        }
    })
        .disableSelection();

}
