<?php

if (!function_exists("agenda")) {
    /**
     * Model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\AgendaTopic
     */
    function agenda($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\AgendaTopic $model */
        $model = model('agenda-topic', $id_or_hashid, $with_trashed);

        return $model;
    }
}

