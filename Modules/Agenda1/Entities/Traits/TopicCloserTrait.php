<?php

namespace Modules\Agenda1\Entities\Traits;

use Carbon\Carbon;

/**
 * Class AgendaTopic
 *
 * @property Carbon $closed_at
 * @package Modules\Agenda1\Entities\Traits
 */
trait TopicCloserTrait
{

    /**
     * close the open topic
     *
     * @param int|null $progress
     *
     * @return bool
     */
    public function close($progress = null)
    {
        if ($this->isClosed()) {
            return false;
        }
        if (is_null($progress)) {
            $progress = $this->refreshCurrentProgress();
        }

        $data = [
             "topic_id" => $this->id,
             "progress" => $progress,
             "text"     => trans("agenda1::vocab.has_closed", [
                  "name" => user()->full_name,
             ]),
        ];

        $record = $this->getNewRecordInstance()->batchSave($data);

        if (!$record->exists) {
            return false;
        }

        $this->refreshCurrentProgress();
        $this->batchSave([
             "closed_at" => now()->toDateTimeString(),
             "closed_by" => user()->id,
        ]);

        return true;
    }



    /**
     * reopen the closed topic
     *
     * @param int|null $progress
     *
     * @return bool
     */
    public function reopen($progress = null)
    {
        if ($this->isOpen()) {
            return false;
        }
        if (is_null($progress)) {
            $progress = $this->refreshCurrentProgress();
        }

        $data = [
             "topic_id" => $this->id,
             "progress" => $progress,
             "text"     => trans("agenda1::vocab.has_reopened", [
                  "name" => user()->full_name,
             ]),
        ];

        $record = $this->getNewRecordInstance()->batchSave($data);

        if (!$record->exists) {
            return false;
        }

        $this->refreshCurrentProgress();
        $this->batchSave([
             "closed_at" => null,
             "closed_by" => 0,
        ]);

        return true;
    }



    /**
     * indicate if the topic is closed
     *
     * @return bool
     */
    public function isClosed()
    {
        return boolval($this->closed_at);
    }



    /**
     * indicate if the topic is open
     *
     * @return bool
     */
    public function isOpen()
    {
        return !$this->isClosed();
    }

}
