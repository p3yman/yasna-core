<?php

namespace Modules\Agenda1\Entities\Traits;

trait RecordPermitsTrait
{
    /**
     * check if the online user can create a topic
     *
     * @return bool
     */
    public function canCreate()
    {
        return $this->can("records");
    }



    /**
     * check if the online user can create a topic
     *
     * @return bool
     */
    public function canBrowse()
    {
        return $this->can("records");
    }



    /**
     * check if the online user can view a topic
     *
     * @return bool
     */
    public function canView()
    {
        return $this->can("records");
    }



    /**
     * check if the online user can edit a topic
     *
     * @return bool
     */
    public function canEdit()
    {
        return $this->can("records");
    }



    /**
     * check if the online user can delete a topic
     *
     * @return bool
     */
    public function canDelete()
    {
        return $this->can("records");
    }



    /**
     * check if the online user can see secret things
     *
     * @return bool
     */
    public function canSeeSecrets()
    {
        return user()->can("agenda.secrets");
    }



    /**
     * check if the online user can do something
     *
     * @param string $permit
     *
     * @return bool
     */
    private function can(string $permit = "*"): bool
    {
        if ($this->isSecret() and user()->cannot("agenda.secrets")) {
            return false;
        }
        return user()->can("agenda.$permit");
    }
}
