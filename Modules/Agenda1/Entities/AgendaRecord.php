<?php

namespace Modules\Agenda1\Entities;

use App\Models\AgendaTopic;
use Modules\Agenda1\Entities\Traits\RecordPermitsTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AgendaRecord
 *
 * @property AgendaTopic $topic
 */
class AgendaRecord extends YasnaModel
{
    use SoftDeletes;
    use RecordPermitsTrait;


    /**
     * get the related topic
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo(AgendaTopic::class, "topic_id");
    }



    /**
     * check if the record is a secret one
     *
     * @return bool
     */
    public function isSecret()
    {
        return (bool)$this->is_secret;
    }



    /**
     * check if the record is not a secret one
     *
     * @return bool
     */
    public function isNotSecret()
    {
        return !$this->isSecret();
    }
}
