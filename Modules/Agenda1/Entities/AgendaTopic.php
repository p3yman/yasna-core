<?php

namespace Modules\Agenda1\Entities;

use App\Models\AgendaRecord;
use Modules\Agenda1\Entities\Traits\TopicCloserTrait;
use Modules\Agenda1\Entities\Traits\TopicPermitsTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgendaTopic extends YasnaModel
{
    use SoftDeletes;
    use TopicPermitsTrait;
    use TopicCloserTrait;


    /**
     * get records relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany(AgendaRecord::class, "topic_id");
    }



    /**
     * get a safe instance of the last record
     *
     * @return \App\Models\AgendaRecord
     */
    public function lastRecord()
    {
        /** @var AgendaRecord $record */
        $record = $this->records()->orderBy('id', 'desc')->first();

        if (!$record) {
            return model("agenda-record");
        }

        return $record;
    }



    /**
     * check if the topic is a secret one
     *
     * @return bool
     */
    public function isSecret()
    {
        return (bool)$this->is_secret;
    }



    /**
     * check if the topic is not a secret one
     *
     * @return bool
     */
    public function isNotSecret()
    {
        return !$this->isSecret();
    }



    /**
     * refresh the current progress of the topic based on the last record
     *
     * @return int
     */
    public function refreshCurrentProgress()
    {
        $last = $this->lastRecord();

        if ($last->exists) {
            $new = $last->progress;
        } else {
            $new = $this->progress;
        }

        $this->update([
             "current_progress" => $new,
        ]);

        return $new;
    }



    /**
     * get a new record instance
     *
     * @return AgendaRecord
     */
    public function getNewRecordInstance()
    {
        /** @var AgendaRecord $instance */
        $instance           = model('agenda-record');
        $instance->topic_id = $this->id;

        return $instance;

    }
}
