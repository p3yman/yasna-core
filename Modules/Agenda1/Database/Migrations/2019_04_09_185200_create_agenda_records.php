<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_records', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("topic_id")->index();
            $table->text("text");
            $table->boolean("is_secret")->default(0);
            $table->tinyInteger("progress")->default(0);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_records');
    }
}
