<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaTopics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_topics', function (Blueprint $table) {
            $table->increments('id');

            $table->string("title")->index();
            $table->text("text")->nullable();
            $table->boolean("is_secret")->default(0);
            $table->tinyInteger("progress")->default(0);
            $table->tinyInteger("current_progress")->default(0);

            $table->timestamps();

            $table->timestamp("deadlined_at")->nullable()->index();
            $table->timestamp("closed_at")->nullable()->index();
            $table->unsignedInteger("closed_by")->default(0);

            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_topics');
    }
}
