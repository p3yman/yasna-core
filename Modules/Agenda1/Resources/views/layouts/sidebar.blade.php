@include('manage::layouts.sidebar-link' , [
    'icon'       => "map",
    'caption'    => trans("agenda1::vocab.title"),
    'link'       => "agenda/browse",
    'condition'  => agenda()->canBrowse(),
])
