<div class="progress ">
    <div role="progressbar"
         aria-valuenow="{{ $model->current_progress }}"
         aria-valuemin="0"
         aria-valuemax="100"
         style="width: {{ $model->current_progress }}%"
         class="progress-bar progress-bar-{{ $model->isClosed()? "danger" : "info" }} progress-bar-striped "
    >

        <span class="">% {{ pd($model->current_progress) }} </span>

    </div>
</div>
