@include("agenda1::action.progress")
@include("agenda1::action.single" , [
    "topic" => $model,
])

@foreach($model->records as $record)
    @if($record->canView())
        @include("agenda1::action.single" , [
            "model" => $record,
            "topic" => $model,
        ])
    @endif
@endforeach
