<div class="w90 panel p5">

    <div class="text-align panel-body">

        <div class="progress progress-xs">
            <div role="progressbar"
                 aria-valuenow="{{ $model->progress }}"
                 aria-valuemin="0"
                 aria-valuemax="100"
                 style="width: {{ $model->progress }}%"
                 class="progress-bar progress-xs progress-bar-{{$topic->isClosed()? "danger" : "info"}} progress-bar-striped "
            >
            </div>
        </div>

        @if($model->isSecret())
            <i class="fa fa-lock text-danger mv10"></i>
        @else
            {{--<i class="fa fa-unlock text-success"></i>--}}
        @endif

        {!! nl2br($model->text) !!}

        @include('manage::widgets.grid-date', [
             "by" => $model->creator->full_name,
             'date' => $model->created_at,
        ])

    </div>

</div>
