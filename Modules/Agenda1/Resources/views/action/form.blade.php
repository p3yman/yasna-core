{!! widget('hidden')->name('hashid')->value($model->hashid) !!}
{!! widget('hidden')->name('topic_id')->value(hashid($model->topic_id)) !!}

{!!
widget("textarea")
     ->name("text")
     ->value($model->text)
     ->inForm()
     ->id("txtText")
!!}

{!!
widget("text")
 ->name("progress")
 ->label("tr:agenda1::vocab.progress")
 ->value($model->progress)
 ->inForm()
 ->required()
!!}

{!!
widget("checkbox")
     ->name("is_secret")
     ->condition(agenda()->canSeeSecrets())
     ->value($model->is_secret)
     ->label("tr:agenda1::vocab.is_secret")
     ->inForm()
!!}
