{{--
|--------------------------------------------------------------------------
| Add New Record
|--------------------------------------------------------------------------
|
--}}

@include('manage::forms.button' , [
	'class'     => "-viewer btn-taha" ,
	'label'     => trans("agenda1::vocab.record_new"),
	"shape"     => "primary",
	'link'      => "$('.-viewer,.-form').toggle();$('#txtText').focus()",
	"condition" => $model->canManageRecords(),
])

{{--
|--------------------------------------------------------------------------
| Open and Close Buttons
|--------------------------------------------------------------------------
|
--}}
@include("manage::forms.button", [
    "class"     => "-viewer btn-taha",
    "label"     => $model->isClosed()? trans("agenda1::vocab.make_reopen") : trans("agenda1::vocab.make_closed"),
    "shape"     => $model->isClosed()? "warning" : "danger",
    "link"      => "masterModal('".route("agenda-topic-" . ($model->isClosed()? "reopen" : "close"), [$model->hashid])."')",
    "condition" => $model->canManageRecords(),
])

{{--
|--------------------------------------------------------------------------
| Cancel (Viewer Mode)
|--------------------------------------------------------------------------
|
--}}

@include('manage::forms.button' , [
	'label' => trans('manage::forms.button.cancel'),
	'class' => "-viewer" ,
	'shape' => 'link',
	'link' => '$(".modal").modal("hide")'
])

{{--
|--------------------------------------------------------------------------
| Save (Form Mode)
|--------------------------------------------------------------------------
|
--}}

@include('manage::forms.button' , [
	'class' => "-form btn-taha noDisplay" ,
	'label' => trans('manage::forms.button.save'),
	"shape" => "primary",
	"type" => "success",
])


{{--
|--------------------------------------------------------------------------
| Cancel (Form Mode)
|--------------------------------------------------------------------------
|
--}}

@include('manage::forms.button' , [
	'label' => trans('manage::forms.button.cancel'),
	'class' => "-form noDisplay" ,
	'shape' => 'link',
	'link'  => "$('.-viewer,.-form').toggle()"
])


{{--
|--------------------------------------------------------------------------
| Feedback (Form Mode)
|--------------------------------------------------------------------------
|
--}}

<div class="-form noDisplay m5">
    @include('manage::forms.feed')
</div>
