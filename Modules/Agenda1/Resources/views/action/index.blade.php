{!!
widget("modal")
	->label($model->title)
	->target("name:agenda-record-save")
!!}

<div class="modal-body">
    <div class=" -viewer">
        @include("agenda1::action.viewer")
    </div>
    <div class="-form noDisplay">
        @include("agenda1::action.form" , [
            "model" => $model->getNewRecordInstance(),
        ])
    </div>
</div>

<div class="modal-footer">
    @include("agenda1::action.buttons")
</div>
