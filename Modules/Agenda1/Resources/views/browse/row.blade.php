@include('manage::widgets.grid-row-handle', [
    'refresh_url' => route("agenda-topic-row-refresh", [
        "hashid" => $model->hashid,
    ]),
    'handle'      => "counter",
])

<td>
    @include("agenda1::browse.row-title")
</td>

<td>
    @include("agenda1::browse.row-progress")
</td>

<td>
    @include("agenda1::browse.row-deadline")
</td>


@include("manage::widgets.grid-actionCol" , [
    "actions" => [
        [
            "eye",
            trans("agenda1::vocab.topic_view"),
            "masterModal('".route("agenda-topic-view", [$model->hashid])."')",
            $model->canView()
        ],
        [
            "pencil",
            trans("agenda1::vocab.topic_edit"),
            "masterModal('".route("agenda-topic-edit", [$model->hashid])."')",
            $model->canEdit()
        ],
    ],
])
