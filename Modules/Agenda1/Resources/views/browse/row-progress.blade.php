@include("manage::widgets.grid-text" , [
     'text' => $model->current_progress . " % ",
     'size' => "12" ,
])

@include("manage::widgets.grid-text" , [
     'text' => $model->isClosed()? trans("agenda1::vocab.close") : trans("agenda1::vocab.open"),
     'color' => $model->isClosed()? 'red' : 'green',
     'size' => "14" ,
])
