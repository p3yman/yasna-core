@include("manage::widgets.grid" , [
    'headings'          => [
        trans("validation.attributes.title"),
        trans("agenda1::vocab.progress"),
        trans("agenda1::vocab.deadlined_at"),
    ],
    'row_view'          => "agenda1::browse.row",
    'table_id'          => "tblAgenda",
    'handle'            => "counter",
    'operation_heading' => true,
])
