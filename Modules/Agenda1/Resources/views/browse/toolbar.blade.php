@include("manage::widgets.toolbar" , [
    'title' => trans("agenda1::vocab.title"),
    'buttons' => [
        [
	    'target'    => "modal:".route("agenda-topic-create"),
	    'caption'   => trans("agenda1::vocab.topic_create"),
	    'icon'      => "add",
	    "type"      => "primary",
	    "condition" => agenda()->canCreate(),
        ],
    ],
])
