@include('manage::widgets.grid-date', [
     'date' => $model->deadlined_at,
     "size" => 14,
     "condition" => boolval($model->deadlined_at),
     "icon" => "",
])

@include("manage::widgets.grid-text" , [
     'text' => "-",
     'size' => "12" ,
     "color"  => "gray",
     "condition" => !$model->deadlined_at,
])
