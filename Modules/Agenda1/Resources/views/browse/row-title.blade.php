@include("manage::widgets.grid-text" , [
     'text' => $model->title,
     'size' => "16" ,
     'class' => "font-yekan text-bold" ,
     'link' =>  $model->canView()? "masterModal('".route("agenda-topic-view", [$model->hashid])."')" : null ,
])

@include("manage::widgets.grid-tiny" , [
     'icon' => "bug",
     'text' => "$model->id|$model->hashid",
     'color' => 'gray',
])

@include('manage::widgets.grid-date', [
     "by" => $model->creator->full_name,
     'date' => $model->created_at,
])

