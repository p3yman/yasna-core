{!!
widget("modal")
	->labelIf(!$model->exists, trans("agenda1::vocab.topic_create"))
	->labelIf($model->exists, trans("agenda1::vocab.topic_edit"))
	->target("name:agenda-topic-save")
!!}

<div class="modal-body">

    {!! widget('hidden')->name('hashid')->value($model->hashid) !!}

    {!!
    widget("text")
    	->name("title")
    	->value($model->title)
    	->inForm()
    	->required()
    !!}

    {!!
    widget("textarea")
    	->name("text")
    	->value($model->text)
    	->inForm()
    !!}

    {!!
    widget("text")
     ->name("progress")
     ->label("tr:agenda1::vocab.progress_init")
     ->value($model->progress)
     ->inForm()
    !!}

    {!!
    widget("persian-date-picker")
    	->name("deadlined_at")
    	->label("tr:agenda1::vocab.deadlined_at")
    	->value($model->deadlined_at)
    	->inForm()
    !!}


    {!!
    widget("checkbox")
    	->name("is_secret")
    	->condition(agenda()->canSeeSecrets())
    	->value($model->is_secret)
    	->label("tr:agenda1::vocab.is_secret")
    	->inForm()
    !!}

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
