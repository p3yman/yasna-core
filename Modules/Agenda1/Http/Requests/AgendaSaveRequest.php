<?php

namespace Modules\Agenda1\Http\Requests;

use Modules\Agenda1\Entities\AgendaTopic;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class AgendaSaveRequest
 *
 * @property AgendaTopic $model
 */
class AgendaSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "agenda-topic";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->createMode()) {
            return $this->model->canCreate();
        }

        return $this->model->canEdit();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "title"    => "required",
             "progress" => "numeric|between:0,100",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "progress"     => "ed",
             "deadlined_at" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "progress"     => trans("agenda1::vocab.progress_init"),
             "deadlined_at" => trans("agenda1::vocab.deadlined_at"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "title",
             "text",
             "progress",
             "deadlined_at",
             "is_secret",
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        if (!$this->getData("deadlined_at")) {
            $this->unsetData("deadlined_at");
        }

        if ($this->createMode() or !$this->model->records->count()) {
            $this->setData("current_progress", $this->getData("progress"));
        }
    }
}
