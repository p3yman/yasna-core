<?php

namespace Modules\Agenda1\Http\Requests;

use App\Models\AgendaRecord;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class RecordSaveRequest
 *
 * @property AgendaRecord $model
 */
class RecordSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "agenda-record";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;

    /** @var bool */
    protected $invalid_topic = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->createMode()) {
            return $this->model->canCreate();
        }

        return $this->model->canEdit();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "text"     => "required",
             "progress" => "numeric|between:0,100",
             "topic_id" => $this->invalid_topic ? "invalid" : "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "progress" => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "progress" => trans("agenda1::vocab.progress"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "text",
             "progress",
             "is_secret",
             'topic_id',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveTopic();
    }



    /**
     * resolve topic instance
     */
    private function resolveTopic()
    {
        if ($this->editMode()) {
            $this->unsetData("topic_id");
            return;
        }

        $topic = model('agenda-topic')->grabHashid($this->getData('topic_id'));

        if (!$topic->exists) {
            $this->invalid_topic = true;
            return;
        }

        $this->setData("topic_id", $topic->id);
    }

}
