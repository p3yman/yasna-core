<?php

namespace Modules\Agenda1\Http\Controllers;

use App\Models\AgendaRecord;
use App\Models\AgendaTopic;
use Modules\Agenda1\Http\Requests\RecordSaveRequest;
use Modules\Agenda1\Http\Requests\TopicCloseRequest;
use Modules\Yasna\Services\YasnaController;

/**
 * Class EditController
 * @method AgendaTopic model($id = 0, $with_trashed = false)
 */
class ActionController extends YasnaController
{
    protected $base_model  = "agenda-topic";
    protected $view_folder = "agenda1::action";



    /**
     * get action view
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($hashid)
    {
        $model = $this->model($hashid);

        if (!$model->exists) {
            return $this->abort(404);
        }
        if (!$model->canView()) {
            return $this->abort(403);
        }

        return $this->view("index", compact("model"));
    }



    /**
     * save agenda record
     *
     * @param RecordSaveRequest $request
     *
     * @return string
     */
    public function save(RecordSaveRequest $request)
    {
        /** @var AgendaRecord $saved */
        $saved = $request->model->batchSave($request);

        if ($saved->exists) {
            $saved->topic->refreshCurrentProgress();
        }

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             "success_callback"   => $this->getSaveCallback($saved),
             "success_modalClose" => "0",
        ]);
    }



    /**
     * close a topic and refresh the modal window
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function close($hashid)
    {
        /** @var AgendaTopic $model */
        $model = $this->model($hashid);

        if (!$model->exists) {
            return $this->abort(410);
        }
        if (!$model->canManageRecords()) {
            return $this->abort(403);
        }

        $model->close();

        return $this->view("index", compact("model"));
    }



    /**
     * reopen a closed topic and refresh the modal window
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function reopen($hashid)
    {
        /** @var AgendaTopic $model */
        $model = $this->model($hashid);

        if (!$model->exists) {
            return $this->abort(410);
        }
        if (!$model->canManageRecords()) {
            return $this->abort(403);
        }

        $model->reopen();

        return $this->view("index", compact("model"));
    }



    /**
     * get after-save JS callback string
     *
     * @param AgendaRecord $record
     *
     * @return string
     */
    private function getSaveCallback(AgendaRecord $record)
    {
        $hashid = hashid($record->topic_id);

        $array = [
             "rowUpdate('tblAgenda', '$hashid')",
             "masterModal('" . route("agenda-topic-view", [$hashid]) . "')",
        ];

        return implode(";", $array);
    }
}
