<?php

namespace Modules\Agenda1\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\YasnaController;

class BrowseController extends YasnaController
{
    protected $base_model  = "agenda-topic";
    protected $view_folder = "agenda1::browse";



    /**
     * get the grid page view
     *
     * @param string $tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($tab = "all")
    {
        if (!agenda()->canBrowse()) {
            return $this->abort(403);
        }

        $args = [
             "models" => $this->getModels($tab),
             "page"   => $this->getPageTracker($tab),
        ];

        return $this->view("index", $args);
    }



    /**
     * get the collection of models to be shown
     *
     * @param string $tab
     *
     * @return Collection
     */
    private function getModels(string $tab)
    {
        $builder = agenda()->newBuilderInstance();

        if (!agenda()->canSeeSecrets()) {
            $builder = $builder->where('is_secret', 0);
        }

        return $builder->orderBy("created_at", "desc")->paginate(20);
    }



    /**
     * get the page tracker array
     *
     * @param string $tab
     *
     * @return array
     */
    private function getPageTracker(string $tab)
    {
        return [
             "0" => ["manage/agenda/browse", trans("agenda1::vocab.title")],
        ];
    }
}
