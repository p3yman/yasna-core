<?php

namespace Modules\Agenda1\Http\Controllers;

use App\Models\AgendaTopic;
use Modules\Agenda1\Http\Requests\AgendaSaveRequest;
use Modules\Yasna\Services\YasnaController;

/**
 * Class EditController
 * @method AgendaTopic model($id = 0, $with_trashed = false)
 */
class EditController extends YasnaController
{
    protected $base_model  = "agenda-topic";
    protected $view_folder = "agenda1::editor";



    /**
     * get create form view
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm()
    {
        if (!agenda()->canCreate()) {
            return $this->abort(403);
        }

        $model = $this->model();

        return $this->view("index", compact("model"));
    }



    /**
     * get edit form view
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editForm($hashid)
    {
        $model = $this->model($hashid);

        if (!$model->exists) {
            return $this->abort(404);
        }
        if (!$model->canEdit()) {
            return $this->abort(403);
        }

        return $this->view("index", compact("model"));
    }



    /**
     * save the new agenda topic
     *
     * @param AgendaSaveRequest $request
     *
     * @return string
     */
    public function save(AgendaSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             "success_callback" => "rowUpdate('tblAgenda', '$request->hashid')",
        ]);
    }
}
