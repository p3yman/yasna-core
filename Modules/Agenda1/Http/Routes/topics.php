<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Agenda1')->getControllersNamespace(),
     'prefix'     => 'manage/agenda',
], function () {
    Route::get('browse/{tab?}', 'BrowseController@index');
    Route::get('browse/row-refresh/{hashid}', 'BrowseController@update')->name("agenda-topic-row-refresh");

    Route::get('create', 'EditController@createForm')->name("agenda-topic-create");
    Route::get('edit/{hashid}', 'EditController@editForm')->name("agenda-topic-edit");
    Route::get('view/{hashid}', 'ActionController@index')->name("agenda-topic-view");
    Route::post('save', 'EditController@save')->name("agenda-topic-save");

    Route::get('topic-close/{hashid}', 'ActionController@close')->name("agenda-topic-close");
    Route::get('topic-reopen/{hashid}', 'ActionController@reopen')->name("agenda-topic-reopen");
    Route::post('record-save', 'ActionController@save')->name("agenda-record-save");
});
