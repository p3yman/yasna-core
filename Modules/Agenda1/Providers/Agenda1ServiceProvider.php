<?php

namespace Modules\Agenda1\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class Agenda1ServiceProvider
 *
 * @package Modules\Agenda1\Providers
 */
class Agenda1ServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerUserPermissions();
        $this->registerManageRelatedThings();
    }



    /**
     * register manage-related things
     *
     * @return void
     */
    private function registerManageRelatedThings()
    {
        if ($this->cannotUseModule("manage")) {
            return;
        }

        $this->registerManageSidebar();
    }



    /**
     * register Manage Sidebar
     *
     * @return void
     */
    private function registerManageSidebar()
    {
        service('manage:sidebar')
             ->add("agenda")
             ->blade("agenda1::layouts.sidebar")
             ->order(1)
        ;
    }



    /**
     * register UserPermissions
     *
     * @return void
     */
    private function registerUserPermissions()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('agenda')
             ->value('browse ,  create ,  edit ,  delete ,  view , records , secrets')
        ;
    }

}
