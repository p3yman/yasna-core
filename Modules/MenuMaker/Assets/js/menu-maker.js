/**
 * Copyright (c) 2018. Programmer, Negar Jamalifard
 */

let updateCall = true;
let timer      = new Timer();

// Removes menu items (if no submenu exist)
function deleteMenuItem(el, $token) {
    let $this    = $(el);
    let item     = $this.parents('.dd-item').first();
    let itemId   = item.attr('data-id');
    let parentId = item.attr('data-parent-id');
    let list     = item.parent('.dd-list');
    if (item.children('.dd-list').length) {
    } else {
        menuLoading();
        $.ajax({
            method : 'POST',
            url    : url('manage/menus/' + parentId + '/links/' + itemId + '/hard_delete'),
            data   : {
                '_submit': 'destroy',
                '_token' : $token
            },
            success: function () {
                if (list.children('.dd-item').length > 1) {
                    item.remove();
                } else {
                    list.remove();
                }
                menuLoading();
                checkSubMenu();
            },
            error  : function () {
                menuLoading();
                $('.js-error-delete').trigger('click');
            }
        });
    }
}

function toggleDeleteBtn(el) {
    let $this      = $(el);
    let item       = $this.parents('.dd-item').first();
    let firstStep  = item.find('.delete-step1').first();
    let secondStep = item.find('.delete-step2').first();

    firstStep.toggleClass('noDisplay');
    secondStep.toggleClass('noDisplay');
}

function menuLoading() {
    $('.menu-list').toggleClass('whirl');
}

function updateOutput(e, $hash_id, $token) {
    let list = e.length ? e : $(e.target), output = list.data('output');
    if (window.JSON) {
        var status = {};
        $('.menu-maker-input-status').each(function () {
            var el      = $(this);
            var hash_id = el.attr('data-id');

            if (el.is(':checked')) {
                status[hash_id] = 1;
            } else {
                status[hash_id] = 0;
            }
        });

        timer.delay(function () {

            $.ajax({
                method    : 'POST',
                url       : url('manage/menus/' + $hash_id + '/links/order/order'),
                data      : {
                    'order' : window.JSON.stringify(list.nestable('serialize')),
                    'status': status,
                    '_token': $token
                },
                beforeSend: function () {
                    $.notify('در حال ذخیره سازی', {
                        status: "warning",
                        pos   : "bottom-center"
                    });
                },
                success   : function (response) {
                    response = window.JSON.parse(response);
                    $.notify.closeAll();
                    $.notify(response['message'], {
                        status: "success",
                        pos   : "bottom-center"
                    });
                    updateCall = true;
                },
                error     : function () {
                    $('.js-error-delete').trigger('click');
                }
            });

        }, 1);

    } else {
        output.val('JSON browser support required for this demo.');
    }

    return updateCall
}

function checkSubMenu() {

    let items = $('.dd-item');

    items.each(function () {
        let el = $(this);

        if (el.children('.dd-list').length) {
            el.find('.delete-step1').addClass('noDisplay');
            el.find('.delete-placeholder').removeClass('noDisplay');

            el.find('.delete-step2').addClass('noDisplay');
        } else {
            el.find('.delete-step1').removeClass('noDisplay');
            el.find('.delete-placeholder').addClass('noDisplay');

            el.find('.delete-step2').addClass('noDisplay');
        }
    });
}

function subItemsReload(div_id, additive = '') {

    //Preparations...
    var $div       = $("#" + div_id);
    var reload_url = $("#" + div_id + " .refresh").html();

    if (!reload_url) {
        reload_url = $div.attr('data-src') + additive;
        reload_url = reload_url.replaceAll("-id-", $div.attr('data-id'));
        reload_url = url(reload_url);
    }

    if (!reload_url) return;

    menuLoading();
    forms_log('loading [' + reload_url + '] in [' + div_id + ']');

    //Ajax...
    $.ajax({
        url  : reload_url,
        cache: false
    })
        .done(function (html) {
                $div.html(html);

                checkSubMenu();

                setTimeout(function () {
                    menuLoading();
                }, 2000);

            }
        )
    ;
}

function checkStatus($this, $hash_id, $parent_id) {
    var checkbox = $($this);
    var children = $('li[data-id="' + $hash_id + '"]')
        .children('ol.dd-list')
        .find('input[type="checkbox"]');
    if (checkbox.is(':checked')) {
        children.prop('checked', true);
    } else {
        children.prop('checked', false);
    }


}
