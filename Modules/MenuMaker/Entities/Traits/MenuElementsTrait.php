<?php

namespace Modules\MenuMaker\Entities\Traits;

/**
 * Trait ElementsTrait
 *
 * @package Modules\MenuMaker\Entities\Traits
 */
trait MenuElementsTrait
{

    /**
     * Return elements
     *
     * @return array
     */
    public function elements()
    {
        $elements = $this->elements;
        array_push($elements, "titles");

        return $elements;
    }



    /**
     * Get all elements
     *
     * @return array
     */
    public function getElementsAttribute()
    {
        return [
             'icons',
             'links',
             'label',
             'style',
             'description',
             'condition',
             'target',
             'classAtr',
             'idAtr',
             'color',
             'html',
             'onClick',
             'onDblClick',
             'onMouseEnter',
             'onMouseLeave',
             'onMouseOver',
        ];
    }



    /**
     * Get type of element
     *
     * @param string $element
     *
     * @return string
     */
    public function ElementType($element)
    {
        $elements = [
             'icons'        => 'icon-picker',
             'links'        => 'input',
             'label'        => 'input',
             'style'        => 'textarea',
             'description'  => 'textarea',
             'condition'    => 'input',
             'target'       => 'toggle',
             'classAtr'     => 'input',
             'idAtr'        => 'input',
             'color'        => 'color-picker',
             'html'         => 'textarea',
             'onClick'      => 'input',
             'onDblClick'   => 'input',
             'onMouseEnter' => 'input',
             'onMouseLeave' => 'input',
             'onMouseOver'  => 'input',
        ];

        return $elements[$element] ?? 'input';
    }
}
