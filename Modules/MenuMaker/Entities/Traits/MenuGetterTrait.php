<?php

namespace Modules\MenuMaker\Entities\Traits;

/**
 * Trait ElementsTrait
 *
 * @package Modules\MenuMaker\Entities\Traits
 */
trait MenuGetterTrait
{

    /**
     * Return all languages in locales field if exist in current languages site
     *
     * @return array
     */
    public function getEnabledLanguagesAttribute()
    {
        $languages         = [];
        $enabled_languages = explode('|', $this->locales);
        $locales           = setting('site_locales')->gain();

        foreach ($locales as $locale) {
            if (in_array($locale, $enabled_languages)) {
                array_push($languages, $locale);
            }
        }
        return (array)$languages;
    }



    /**
     * Gets the title of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function titleIn($locale)
    {
        return $this->titles[$locale] ?? null;
    }



    /**
     * Gets the title of a specified link in current language
     *
     * @return null|string
     */
    public function getTitleAttribute()
    {
        return $this->titleIn(getLocale());
    }



    /**
     * Returns the first item in the `titles` array.
     *
     * @return string|null
     */
    public function firstTitle()
    {
        return array_first($this->titles);
    }



    /**
     * Accessor for the `firstTitle()`  Method.
     *
     * @return string|null
     */
    public function getFirstTitleAttribute()
    {
        return $this->firstTitle();
    }



    /**
     * Returns the title in the given locale or the first title if the locale title has not been presented.
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function safeTitleIn(string $locale)
    {
        return ($this->titleIn($locale) ?: $this->firstTitle());
    }



    /**
     * Accessor for the `safeTitleIn()` method.
     *
     * @return string|null
     */
    public function getSafeTitleAttribute()
    {
        return $this->safeTitleIn(getLocale());
    }



    /**
     * Gets the link of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function linkIn($locale)
    {
        return $this->links[$locale] ?? null;
    }



    /**
     * Gets the link of a specified link in current language
     *
     * @return null|string
     */
    public function getLinkAttribute()
    {
        return $this->linkIn(getLocale());
    }



    /**
     * Gets the icon of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function iconIn($locale)
    {
        return $this->icons[$locale] ?? null;
    }



    /**
     * Gets the icon of a specified link in current language
     *
     * @return null|string
     */
    public function getIconAttribute()
    {
        return $this->iconIn(getLocale());
    }



    /**
     * Gets the description of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function descriptionIn($locale)
    {
        return $this->meta['description'][$locale] ?? null;
    }



    /**
     * Gets the description of a specified link in current language
     *
     * @return null|string
     */
    public function getDescriptionAttribute()
    {
        return $this->descriptionIn(getLocale());
    }



    /**
     * Gets the label of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function labelIn($locale)
    {
        return $this->meta['label'][$locale] ?? null;
    }



    /**
     * Gets the label of a specified link in current language
     *
     * @return null|string
     */
    public function getLabelAttribute()
    {
        return $this->labelIn(getLocale());
    }



    /**
     * Gets the style of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function styleIn($locale)
    {
        return $this->meta['style'][$locale] ?? null;
    }



    /**
     * Gets the style of a specified link in current language
     *
     * @return null|string
     */
    public function getStyleAttribute()
    {
        return $this->styleIn(getLocale());
    }



    /**
     * Gets the condition of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function conditionIn($locale)
    {
        return $this->meta['condition'][$locale] ?? null;
    }



    /**
     * Gets the condition of a specified link in current language
     *
     * @return null|string
     */
    public function getConditionAttribute()
    {
        return $this->conditionIn(getLocale());
    }



    /**
     * Gets the target of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function targetIn($locale)
    {
        return $this->meta['target'][$locale] ?? null;
    }



    /**
     * Gets the target of a specified link in current language
     *
     * @return null|string
     */
    public function getTargetAttribute()
    {
        return $this->targetIn(getLocale());
    }



    /**
     * Gets the classAtr of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function classAtrIn($locale)
    {
        return $this->meta['classAtr'][$locale] ?? null;
    }



    /**
     * Gets the classArt of a specified link in current language
     *
     * @return null|string
     */
    public function getClassArtAttribute()
    {
        return $this->classAtrIn(getLocale());
    }



    /**
     * Gets the idAtr of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function idAtrIn($locale)
    {
        return $this->meta['idAtr'][$locale] ?? null;
    }



    /**
     * Gets the idArt of a specified link in current language
     *
     * @return null|string
     */
    public function getIdArtAttribute()
    {
        return $this->idAtrIn(getLocale());
    }



    /**
     * Gets the color of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function colorIn($locale)
    {
        return $this->meta['color'][$locale] ?? null;
    }



    /**
     * Gets the color of a specified link in current language
     *
     * @return null|string
     */
    public function getColorAttribute()
    {
        return $this->colorIn(getLocale());
    }



    /**
     * Gets the html of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function htmlIn($locale)
    {
        return $this->meta['html'][$locale] ?? null;
    }



    /**
     * Gets the html of a specified link in current language
     *
     * @return null|string
     */
    public function getHtmlAttribute()
    {
        return $this->htmlIn(getLocale());
    }



    /**
     * Gets the onClick of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function onClickIn($locale)
    {
        return $this->meta['onClick'][$locale] ?? null;
    }



    /**
     * Gets the onClick of a specified link in current language
     *
     * @return null|string
     */
    public function getOnClickAttribute()
    {
        return $this->onClickIn(getLocale());
    }



    /**
     * Gets the onDblClick of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function onDblClickIn($locale)
    {
        return $this->meta['onDblClick'][$locale] ?? null;
    }



    /**
     * Gets the onDblClick of a specified link in current language
     *
     * @return null|string
     */
    public function getOnDblClickAttribute()
    {
        return $this->onDblClickIn(getLocale());
    }



    /**
     * Gets the onMouseEnter of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function onMouseEnterIn($locale)
    {
        return $this->meta['onMouseEnter'][$locale] ?? null;
    }



    /**
     * Gets the onMouseEnter of a specified link in current language
     *
     * @return null|string
     */
    public function getOnMouseEnterAttribute()
    {
        return $this->onMouseEnterIn(getLocale());
    }



    /**
     * Gets the onMouseLeave of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function onMouseLeaveIn($locale)
    {
        return $this->meta['onMouseLeave'][$locale] ?? null;
    }



    /**
     * Gets the onMouseLeave of a specified link in current language
     *
     * @return null|string
     */
    public function getOnMouseLeaveAttribute()
    {
        return $this->onMouseLeaveIn(getLocale());
    }



    /**
     * Gets the onMouseOver of a specified link in a specified language
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function onMouseOverIn($locale)
    {
        return $this->meta['onMouseOver'][$locale] ?? null;
    }



    /**
     * Gets the onMouseOver of a specified link in current language
     *
     * @return null|string
     */
    public function getOnMouseOverAttribute()
    {
        return $this->onMouseLeaveIn(getLocale());
    }



    /**
     * Get settings attribute from meta field
     *
     * @return null|array
     */
    public function getSettingsAttribute()
    {
        if (is_null($this->parent_id) && isset($this->meta['settings'])) {
            return $this->meta['settings'];
        }
        return null;
    }

}
