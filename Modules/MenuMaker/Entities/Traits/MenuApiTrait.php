<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/11/18
 * Time: 3:56 PM
 */

namespace Modules\MenuMaker\Entities\Traits;


use App\Models\Menu;

trait MenuApiTrait
{
    /**
     * Returns an array representation of a menu item.
     *
     * @param string|null $locale
     * @param bool        $children
     *
     * @return array
     */
    public function apiArray(?string $locale = null, bool $children = false)
    {
        $data = $this->generalApiArray($locale);

        if ($children) {
            $data['children'] = $this
                 ->orderedChildren()
                 ->get()
                 ->filter(function (Menu $menu) use ($locale) {
                     return $menu->isEnabled($locale);
                 })
                 ->map(function (Menu $menu) use ($locale, $children) {
                     return $menu->apiArray($locale, $children);
                 })
            ;
        }

        return $data;
    }



    /**
     * Returns an array of general information.
     *
     * @param string $locale
     *
     * @return array
     */
    protected function generalApiArray(string $locale)
    {
        // If is root item
        if ($this->parent_id == 0) {
            return $this->getGeneralApiArrayAsPosition($locale);
        } else {
            return $this->getGeneralApiArrayAsMenuItem($locale);
        }
    }



    /**
     * Returns an array of general information assuming that this is a position.
     *
     * @param string $locale
     *
     * @return array
     */
    protected function getGeneralApiArrayAsPosition(string $locale)
    {
        return [
             'title' => $this->safeTitleIn($locale),
             'slug'  => $this->getOriginal('slug'),
        ];
    }



    /**
     * Returns an array of general information assuming that this is not a position.
     *
     * @param string $locale
     *
     * @return array
     */
    protected function getGeneralApiArrayAsMenuItem(string $locale)
    {
        return [
             'title'        => $this->titleIn($locale),
             'link'         => $this->linkIn($locale),
             'icon'         => $this->iconIn($locale),
             'style'        => $this->styleIn($locale),
             'description'  => $this->descriptionIn($locale),
             'condition'    => $this->conditionIn($locale),
             'target'       => $this->targetIn($locale),
             'classAtr'     => $this->classAtrIn($locale),
             'idAtr'        => $this->idAtrIn($locale),
             'color'        => $this->colorIn($locale),
             'html'         => $this->htmlIn($locale),
             'label'        => $this->labelIn($locale),
             'onClick'      => $this->onClickIn($locale),
             'onDblClick'   => $this->onDblClickIn($locale),
             'onMouseEnter' => $this->onMouseEnterIn($locale),
             'onMouseLeave' => $this->onMouseLeaveIn($locale),
             'onMouseOver'  => $this->onMouseOverIn($locale),
        ];
    }



    /**
     * Accessor for the `apiArray()` Method
     *
     * @return array
     */
    public function getApiArrayAttribute()
    {
        return $this->apiArray();
    }
}
