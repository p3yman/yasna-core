<?php

namespace Modules\MenuMaker\Entities;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MenuMaker\Entities\Traits\MenuApiTrait;
use Modules\MenuMaker\Entities\Traits\MenuElementsTrait;
use Modules\MenuMaker\Entities\Traits\MenuGetterTrait;
use Modules\Yasna\Services\YasnaModel;
use PhpParser\Builder;

class Menu extends YasnaModel
{
    use SoftDeletes, MenuElementsTrait, MenuGetterTrait, MenuApiTrait;

    protected $guarded = ['id'];
    protected $table   = 'menus';
    private   $locale  = null;
    private   $status  = null;
    private   $slug    = null;
    const ITEM_REQUIRED = "0";
    const ITEM_OPTIONAL = "1";
    const ITEM_HIDDEN   = "2";

    protected $casts = [
         'titles' => 'array',
         'icons'  => 'array',
         'links'  => 'array',
         'slug'   => 'string',
    ];



    /**
     * submenus belongs to a parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }



    /**
     * Define access scopes to menu item as positions
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeParents($query)
    {
        return $query->whereNull('parent_id');
    }



    /**
     * submenus of menu item, a one-to-many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }



    /**
     * Submenus of menu item as a one-to-many relationship and in order.
     * @return HasMany
     */
    public function orderedChildren()
    {
        return $this->children()->orderBy('order');
    }



    /**
     * Check this item, that has any children or no
     *
     * @return bool
     */
    public function hasChildren()
    {
        if ($this->children()->count()) {
            return true;
        }
        return false;
    }



    /**
     * submenus of menu item, a one-to-many relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subMenus()
    {
        return $this->hasMany(self::class, 'parent_id');
    }



    /**
     * Return submenus of menu item with trashed items
     *
     * @return Builder
     */
    public function childrenWithTrashed()
    {
        return $this->children()->withTrashed();
    }



    /**
     * local scope on slugs
     *
     * @param Builder $query
     * @param string  $slug
     *
     * @return Builder
     */
    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }



    /**
     * local scope on status
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }



    /**
     * Check the value of key in meta->settings, 2 is hidden
     *
     * @param string $key
     *
     * @return bool
     */
    public function isHidden($key)
    {
        return $this->itemSettingValue($key) == self::ITEM_HIDDEN; // 2
    }



    /**
     * Check the value of key in meta->settings, 2 is hidden
     *
     * @param string $key
     *
     * @return bool
     */
    public function isNotHidden($key)
    {
        return !$this->isHidden($key);
    }



    /**
     * Get value's of setting
     *
     * @param string $key
     *
     * @return string
     */
    public function itemSettingValue($key)
    {
        if (is_string($this->settings)) {
            $settings = (array)json_decode($this->settings);
        } elseif (is_array($this->settings)) {
            $settings = $this->settings;
        } else {
            $settings = ['link' => '0'];
        }

        return $settings[$key] ?? self::ITEM_HIDDEN; // 2
    }



    /**
     * Soft delete all children from current item
     */
    public function softDeleteAllChildren()
    {
        $children = $this->childrenWithTrashed()->get();
        foreach ($children as $item) {
            if (!is_null($item->childrenWithTrashed()->get())) {
                $item->softDeleteAllChildren();
            }
            $item->delete();
        }
    }



    /**
     * Restore all children from current item
     */
    public function restoreAllChildren()
    {
        $children = $this->childrenWithTrashed()->get();
        foreach ($children as $item) {
            if ($item->childrenWithTrashed()->count()) {
                $item->restoreAllChildren();
            }
            $item->restore();
        }
    }



    /**
     * Hard delete all children from current item
     */
    public function hardDeleteAllChildren()
    {
        $children = $this->childrenWithTrashed()->get();
        foreach ($children as $item) {
            if ($item->childrenWithTrashed()->count()) {
                $item->hardDeleteAllChildren();
            }
            $item->hardDelete();
        }
    }



    /**
     * count children from current item
     *
     * @return int
     */
    public function CountAllChildren()
    {
        $n        = 0;
        $children = $this->children()->get();
        foreach ($children as $item) {
            $n++;
            if ($item->children()->count()) {
                $n += $item->CountAllChildren();
            }
        }
        return $n;
    }



    /**
     * Set locales
     *
     * @param array $locale
     */
    public function setLocalesAttribute(array $locale)
    {
        $locales = collect($locale)->toArray();
        $locales = array_unique($locales);
        if (is_array($locales)) {
            $locales = array_filter($locales, function ($var) {
                return $var != null;
            });
        }
        $str_locales                 = implode('|', $locales);
        $this->attributes['locales'] = $str_locales;
    }



    /**
     * Check the language, that exist in enabled language or no
     *
     * @param string $language
     *
     * @return bool
     */
    public function isEnabled($language)
    {
        $enable_languages = $this->enabled_languages;

        return in_array($language, $enable_languages);
    }



    /**
     * check locale used in row
     *
     * @param string $locale
     *
     * @return bool
     */
    public function definedIn($locale)
    {
        return (strpos($this->locales, $locale) !== false) ? true : false;
    }



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             'style',
             'description',
             'condition',
             'target',
             'classAtr',
             'idAtr',
             'color',
             'html',
             'label',
             'onClick',
             'onDblClick',
             'onMouseEnter',
             'onMouseLeave',
             'onMouseOver',
             'settings',
             'menu_id',
        ];
    }



    /**
     * Get status of one element as required 0 => required; 1 => optional; 2 => hidden
     *
     * @param string $key
     *
     * @return string
     */
    public function status($key)
    {
        return $this->itemSettingValue($key);
    }



    /**
     * Return submenus in specified position
     *
     * @param string $slug
     *
     * @return Collection
     */
    private function getBySlug($slug)
    {
        $item = $this->parents()->slug($slug)->first();

        // check model exists or not
        if (!$item or !$item->id or !$item->exists) {
            return collect();
        }

        $sub_menus = collect($item->children)->filter(function ($item) {
            return $item->isEnabled($this->locale);
        });

        if ($this->status !== null) {
            $sub_menus = collect($item->children)->filter(function ($item) {
                return $item->isEnabled($this->locale) && $item->status == 1;
            });
        }

        return $sub_menus;
    }



    /**
     * Set the locale
     *
     * @param string $lang
     *
     * @return $this
     */
    private function locale($lang)
    {
        $this->locale = $lang;
        return $this;
    }



    /**
     * Return submenus with current locale
     *
     * @param string $slug
     *
     * @return Collection
     */
    public function get($slug)
    {
        $lang = getLocale();
        return $this->locale($lang)->getBySlug($slug);
    }



    /**
     * Return the submenu with custom language
     *
     * @param string $slug
     * @param string $locale
     *
     * @return Collection
     */
    public function getIn($slug, $locale = null)
    {
        $locales = setting('site_locales')->gain();

        if (!in_array($locale, $locales)) {
            $locale = getLocale();
        }

        return $this->locale($locale)->getBySlug($slug)->sortBy('order');
    }



    /**
     * @param string $slug
     * @param string $locale
     *
     * @return Collection
     */
    public function getInNotNull($slug, string $locale)
    {
        return $this
             ->getIn($slug, $locale)
             ->reject(function ($item) use ($locale) {
                 return !str_contains($item->locales, $locale);
             })
             ->sortBy('order')
             ;
    }



    /**
     * Return all positions as menu
     *
     * @deprecated
     * @return array
     */
    public function list()
    {
        return $this->parents()->pluck('slug')->toArray();
    }



    /**
     * Return the specified menu model
     *
     * @param string $hash_id
     *
     * @return YasnaModel
     */
    public function setting($hash_id)
    {
        $model = model('menu', $hash_id);

        if (!$model or !$model->id or !$model->exists) {
            $model = model('menu');
        }

        return $model;
    }



    /**
     * @param string $role
     * @param string $action
     * @param bool   $string
     *
     * @return bool|string
     */
    public function userCan($role, $action, $string = false)
    {
        return $string ? 'can:post-menu-maker.' . $action . '@' . $role : user()
             ->as($role)
             ->can('post-menu-maker.' . $action)
             ;
    }



    /**
     * Set enable status
     *
     * @return $this
     */
    public function enableStatus()
    {
        $this->status = 1;

        return $this;
    }



    /**
     * Set disable status
     *
     * @return $this
     */
    public function disableStatus()
    {
        $this->status = 0;

        return $this;
    }



    /**
     *  Return an eloquent builder of all the defined positions.
     *
     * @return Builder
     */
    public function positions()
    {
        return $this->parents();
    }



    /**
     * Return an array of all the defined positions
     *
     * @return array
     */
    public function positionsArray()
    {
        return $this->positions()->orderBy('order')->get()->pluck('slug')->toArray();
    }



    /**
     * set menu position as slug
     *
     * @param string $slug
     *
     * @return $this
     */
    public function setPosition($slug)
    {
        $this->slug = $slug;
        return $this;
    }



    /**
     * return Builder of submenu
     *
     * @return HasMany
     */
    public function items()
    {
        if ($this->slug) {
            $first_parent = ($this->parents()->slug($this->slug)->first() ?? static::instance());

            return $first_parent->children()->orderBy('order');
        }

        return $this->children()->orderBy('order');
    }


}
