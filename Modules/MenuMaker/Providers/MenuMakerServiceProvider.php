<?php

namespace Modules\MenuMaker\Providers;

use Illuminate\Support\Facades\Validator;
use Modules\MenuMaker\Http\Endpoints\V1\ListEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class MenuMakerServiceProvider
 *
 * @package Modules\MenuMaker\Providers
 */
class MenuMakerServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerBottomAssets();
        $this->registerSettingsSidebars();
        $this->registerCustomValidator();
        $this->registerEndpoint();
    }



    /**
     * Register Sidebar
     */
    protected function registerSettingsSidebars()
    {
        module('manage')
             ->service('settings_sidebar')
             ->add('menumaker')
             ->link('menus')
             ->trans('menumaker::general.menu_maker')
             ->order(20)
             ->permit(function () {
                 return user()->isSuperadmin();
             })
        ;
    }



    /**
     * Register array_not_empty validator
     */
    protected function registerCustomValidator()
    {
        Validator::extend('array_not_empty', function ($attribute, $value, $parameters, $validator) {
            $arr = collect($value)->reject(function ($value, $key) {
                return is_null($value);
            });
            return count($arr->toArray()) > 0;
        });
    }



    /**
     * Register assets
     */
    protected function registerBottomAssets()
    {
        /*-----------------------------------------------
        | Nestable ...
        */
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("manage:libs/vendor/nestable/jquery.nestable-rtl.js")
             ->order(35)
        ;

        /*-----------------------------------------------
        | Timer js ...
        */
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("yasna:libs/timer-js/src/timer.js")
             ->order(44)
        ;
        /*-----------------------------------------------
        | Menu Builder js ...
        */
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("menumaker:js/menu-maker.js")
             ->order(50)
        ;
    }



    /**
     * Registers the endpoints only when the `Endpoints` module is available.
     *
     * @return void
     */
    protected function registerEndpoint()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }


        endpoint()->register(ListEndpoint::class);
    }
}
