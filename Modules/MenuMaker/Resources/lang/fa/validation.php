<?php
return [
     'array_not_empty' => ':attribute نباید خالی باشد',
     "attributes"      => [
          "titles" => "عنوان‌ها",
          "links"  => "لینک‌ها",
          "icons"  => "آیکن‌ها",
          'labels' => 'برچسب‌ها',
     ],
     'label'           => 'برچسب',
];
