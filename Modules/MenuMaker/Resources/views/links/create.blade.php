{!!
    Widget('modal')
    ->target(route('manage.menus.links.store',
            [
                'hash_id'=> $hash_id
            ]
     ))
    ->label(
            $item->id == 0
                ? "tr:menumaker::general.create"
                : "tr:menumaker::general.edit"
            )
!!}

<div class="modal-body">
	<ul class="nav nav-tabs" role="tablist">
		@foreach( $enabled_languages as $key => $locale)
			<li role="presentation" class="{{ $locale == getLocale() ? 'active' : '' }}">
				<a id="aWare-{{ $key }}" href="#divWare-{{ $locale }}" aria-controls="divWare-{{ $locale }}" role="tab"
				   data-toggle="tab">
					<span><img style="width: 25px;" src="{{ asset('modules/manage/images/lang-' . $locale . '.png') }}"></span>
					<span class="mh10">{{ trans("menumaker::general.locale.$locale") }}</span>
				</a>
			</li>
		@endforeach
	</ul>

	<div class="tab-content">
		@foreach($enabled_languages as $locale)
			<div role="tabpanel" class="tab-pane p20 {{$locale == 'fa' ? 'active' : ''}}" id="divWare-{{ $locale }}">


				<div class="well">
					{!!
						widget('toggle')
						->id('lang-'.$locale)
						->name('locales['.$locale.']')
						->label(trans('menumaker::general.status'))
						->value(1)
					!!}
				</div>
				<div id="div-{{ $locale }}">
					@include('menumaker::links.create_items',['locale' => $locale])
				</div>
			</div>


			<script>
                $(function () {
                    $('#lang-{{ $locale }}').siblings('div.wrapper').find('div.toggle')
                        .change(function () {
                            var div = $('#div-{{$locale}}');
                            if ($(this).hasClass('off')) {
                                div.css('pointer-events', 'none');
                                div.css('opacity', '0.4');
                            } else {
                                div.css('pointer-events', 'auto');
                                div.css('opacity', '1');
                            }
                        });
                });
			</script>

		@endforeach
	</div>

</div>

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		'separator' => false
	])
</div>
