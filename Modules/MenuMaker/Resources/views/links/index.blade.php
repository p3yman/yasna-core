@extends('manage::layouts.template')
@section('content')

	@include("manage::widgets.tabs", [
		'current' => $page[1][0],
		'tabs' => $tabs
	])

	<div id="items">
		<div class="container-fluid">
			<h3 class="page-header"> {{ trans('menumaker::general.main_title') }}
				<small class="text-muted">{{ trans('menumaker::general.description') }}</small>
			</h3>

			@include("manage::widgets.toolbar_button" , [
				'target' => "modal:" . route('manage.menus.links.create', [ 'hash_id' => $hash_id,]) . ":md",
				'type' => "success",
				'caption' => trans('menumaker::general.add_link'),
				'icon' => "plus-circle",
				'condition' => user()->isSuperadmin(),
			])

			@include("manage::widgets.toolbar_button" , [
			'target' => "modal:" . route('manage.menus.edit',['hash_id' => $menu->hashid],false)  . ':lg',
			'type' => "primary",
			'caption' => trans('menumaker::general.menu_maker_setting'),
			'icon' => "cube",
			'condition' => user()->isDeveloper(),
			])

			<div class="row">
				<div class="col-md-6">
					@include('menumaker::links.nestable.index',[
					"id" => "nestable" ,
					"maxDepth" => "10" ,
					"menu"=>$menu,
					"list" => $sub_items
					])
				</div>
			</div>

		</div>
	</div>

@endsection
