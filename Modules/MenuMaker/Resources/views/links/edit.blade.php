{!!
    Widget('modal')
    ->target(route('manage.menus.links.update',
            [
                'hash_id'=> $hash_id,
                'submenu_hash_id'=> $item->hashid
            ]
     ))
    ->label(
            $item->id == 0
                ? "tr:menumaker::general.create"
                : "tr:menumaker::general.edit"
            )
!!}

<div class="modal-body">
	<ul class="nav nav-tabs" role="tablist">
		@foreach( $locales as $key => $locale)
			@php
				$style = (! in_array($locale , $parent_languages))?'style="pointer-events: none;visibility: hidden;"':'';
			@endphp
			<li role="presentation" class="{{ $locale == getLocale() ? 'active' : '' }}" >
				<a id="aWare-{{ $key }}" href="#divWare-{{ $locale }}" aria-controls="divWare-{{ $locale }}" role="tab"
				   data-toggle="tab">
					<span><img style="width: 25px;" src="{{ asset('modules/manage/images/lang-' . $locale . '.png') }}"></span>
					<span class="mh10">{{ trans("menumaker::general.locale.$locale") }}</span>
				</a>
			</li>
		@endforeach
	</ul>

	<div class="tab-content">
		@foreach($locales as $locale)
			@php
				$style = (! in_array($locale , $parent_languages))?'style="pointer-events: none;visibility: hidden;"':'';
			@endphp
			<div role="tabpanel" class="tab-pane p20 {{$locale == 'fa' ? 'active' : ''}}" id="divWare-{{ $locale }}" {!! $style !!}>
				<div class="well">
					{!!
						widget('toggle')
						->id('lang-'.$locale)
						->name('locales['.$locale.']')
						->label(trans('menumaker::general.status'))
						->value(in_array($locale , $enabled_languages))
					!!}
				</div>
				<div id="div-{{ $locale }}">
					@include('menumaker::links.edit_items',['locale' => $locale,'menu'=>$menu])
				</div>
			</div>

			<script>
                $(function () {
                    $('#lang-{{ $locale }}').siblings('div.wrapper').find('div.toggle')
                        .change(function () {
                            var div = $('#div-{{$locale}}');
                            if ($(this).hasClass('off')) {
                                div.css('pointer-events', 'none');
                                div.css('opacity', '0.4');
                            } else {
                                div.css('pointer-events', 'auto');
                                div.css('opacity', '1');
                            }
                        });
                });
			</script>

		@endforeach
	</div>
</div>

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		'separator' => false
	])
</div>
