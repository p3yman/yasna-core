{{-- title --}}
{!!
    widget('input')
        ->value()
        ->name('titles['.$locale.']')
        ->label(trans('menumaker::general.title'))
        ->ltr($locale == 'fa' ? false : true)
        ->inForm()
!!}

{{-- link --}}
{!!
    widget('input')
        ->name('links['.$locale.']')
		->condition($item->isNotHidden('links'))
        ->label(trans('menumaker::general.link'))
        ->ltr()
        ->inForm()
!!}

{{-- icon --}}
{!!
    widget('icon-picker')
        ->name('icons['.$locale.']')
        ->condition($item->isNotHidden('icons'))
        ->label(trans('menumaker::general.icon'))
        ->inForm()

!!}

{{-- label --}}
{!!
    widget('input')
        ->name('label['.$locale.']')
		->condition($item->isNotHidden('label'))
        ->label(trans('menumaker::general.label'))
        ->ltr()
		->inForm()
!!}


{{-- description --}}
{!!
    widget('textarea')
        ->name('description['.$locale.']')
        ->condition($item->isNotHidden('description'))
        ->label(trans('menumaker::general.description'))
        ->inForm()
!!}

{{-- style --}}
{!!
    widget('textarea')
        ->name('style['.$locale.']')
        ->condition($item->isNotHidden('style'))
        ->label(trans('menumaker::general.style'))
        ->ltr()
        ->inForm()
!!}

{{-- html --}}
{!!
    widget('textarea')
        ->name('html['.$locale.']')
        ->condition($item->isNotHidden('html'))
        ->label(trans('menumaker::general.html'))
        ->ltr()
        ->inForm()
!!}

<br>

{{-- class --}}
{!!
    widget('input')
        ->name('classAtr['.$locale.']')
        ->condition($item->isNotHidden('classAtr'))
        ->label(trans('menumaker::general.classAtr'))
        ->ltr()
		->inForm()
!!}

{{-- id --}}
{!!
    widget('input')
        //->value($item->idAtrIn($locale))
        ->name('idAtr['.$locale.']')
         ->condition($item->isNotHidden('idAtr'))
        ->label(trans('menumaker::general.idAtr'))
        ->ltr()
		->inForm()
!!}

<br>
{{-- color --}}
{!!
    widget('color-picker')
        ->id('color-'.$locale)
        ->name('color['.$locale.']')
         ->condition($item->isNotHidden('color'))
		->label(trans('menumaker::general.color'))
        ->inForm()
!!}

{{-- onClick --}}
{!!
    widget('input')
        ->name('onClick['.$locale.']')
         ->condition($item->isNotHidden('onClick'))
        ->label(trans('menumaker::general.onClick'))
        ->ltr()
        ->inForm()
!!}

{{-- onDblClick --}}
{!!
    widget('input')
        ->name('onDblClick['.$locale.']')
        ->condition($item->isNotHidden('onDblClick'))
        ->label(trans('menumaker::general.onDblClick'))
        ->ltr()
		->inForm()
!!}

{{-- onMouseEnter --}}
{!!
    widget('input')
        ->name('onMouseEnter['.$locale.']')
         ->condition($item->isNotHidden('onMouseEnter'))
        ->label(trans('menumaker::general.onMouseEnter'))
		->ltr()
		->inForm()
!!}

{{-- onMouseLeave --}}
{!!
    widget('input')
        ->name('onMouseLeave['.$locale.']')
        ->condition($item->isNotHidden('onMouseLeave'))
        ->label(trans('menumaker::general.onMouseLeave'))
        ->ltr()
		->inForm()
!!}

{{-- onMouseOver --}}
{!!
    widget('input')
        ->name('onMouseOver['.$locale.']')
        ->condition($item->isNotHidden('onMouseOver'))
        ->label(trans('menumaker::general.onMouseOver'))
        ->ltr()
		->inForm()
!!}

{{-- onMouseOver --}}
{!!
    widget('input')
        ->name('onMouseOver['.$locale.']')
        ->condition($item->isNotHidden('onMouseOver'))
        ->label(trans('menumaker::general.condition'))
        ->ltr()
		->inForm()
!!}

<br>

{{-- target --}}
{!!
	widget('toggle')
		->id('target')
		->name('target['.$locale.']')
        ->condition($item->isNotHidden('target'))
		->label(trans('menumaker::general.target'))
		->inForm()
!!}


{!!
	widget('input')
	->type('hidden')
	->value($item->hashid)
	->name('hashid')
!!}
