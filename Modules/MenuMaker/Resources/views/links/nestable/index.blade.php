{{--Menu--}}
<div id="{{ $id }}" class="dd menu-list"
	 data-src="{{ route('manage.menus.links.reload',[ 'hash_id' => $hash_id], false ) }}"
	 onload="checkSubMenu()">
	@include('menumaker::links.nestable.list',[
	"id" => $id ,
	"menu" => $menu ,
	"list" => $list ,
])
</div>

{{--Hidden Elemants--}}
<input type="hidden" value="{{ csrf_token() }}" id="csrf_token">
<input type="hidden" value="{{ $hash_id }}" id="hash_id">

{{--Js Nestable config and init--}}
<script>
    $(document).ready(function () {
        $('#{{ $id }}').nestable({
            group   : 1,
            maxDepth: "15"
        })
            .on('change', function () {
                // output initial serialised data
                let el = $('#{{ $id }}').data('output', $('#{{ $id }}-output'));
                updateOutput(el, '{{ $hash_id }}', '{{ csrf_token() }}');
                checkSubMenu();
            });
        checkSubMenu();
    });
</script>


