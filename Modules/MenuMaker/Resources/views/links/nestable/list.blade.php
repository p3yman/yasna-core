<ol class="dd-list">

	@foreach($list as $item)
		<li data-id="{{ $item->hash_id }}" data-parent-id="{{ $my_parent ?? $hash_id }}" class="dd-item">
			<div class="dd-handle dd3-handle">Drag</div>
			<div class="dd3-content">

				<span class="menu-title">
					@if($menu->isNotHidden('icons'))
						<i class="fa {{ $item->iconIn(getLocale()) }}"></i>
					@endif
					{{ $item->safe_title }}
				</span>
				<div class="controls pull-right f18 mr0">

					@if(menumaker()->userCan('admin', 'edit'))
						<button class="btn btn-info btn-xs" style="font-size: 12px;" onclick="masterModal('{{ route('manage.menus.links.edit',['hash_id'=>$menu->hashid,'submenu_hash_id'=>$item->hashid]) }}' ,'md')">
							<em class="fa fa-pencil"></em>
						</button>
					@endif

					@if(menumaker()->userCan('admin', 'delete'))
						<button class="btn btn-danger btn-xs delete-step1" onclick="toggleDeleteBtn(this)"
								style="font-size: 12px;margin-right: 4px;"><em class="fa fa-trash-o"></em></button>
						<button class="btn btn-danger btn-xs delete-placeholder noDisplay" disabled="disabled"
								style="font-size: 12px;margin-right: 4px;"><em class="fa fa-trash-o"></em></button>
						<div class="delete-step2 pull-right mh5 noDisplay">
							<button class="btn btn-xs btn-danger" onclick="deleteMenuItem(this, '{{ csrf_token() }}')"
									style="font-size: 12px;">
								{{ trans('menumaker::general.delete') }}
							</button>
							<button class="btn btn-xs btn-default" onclick="toggleDeleteBtn(this)"
									style="font-size: 12px;">
								{{ trans('menumaker::general.cancel') }}
							</button>
						</div>
					@endif

					@if(menumaker()->userCan('admin', 'delete'))
						<span class="pull-right ml">
								{!!
							widget('checkbox')
							->name('status[]')
						 	->value($item->status == 1)
						 	->class('menu-maker-input-status')
						 	->containerClass(' m0')
						 	->setExtra('data-id',$item->hashid)
						 	->setExtra('data-parent-id',$my_parent ?? $hash_id)
						 	->onChange("checkStatus(this, $item->hashid,$hash_id)") // $my_parent ?? $hash_id
						 !!}
							</span>
					@endif
				</div>
			</div>
			@includeWhen(count($item->children->toArray()),'menumaker::links.nestable.list',[
				"id" => $id ,
				"menu" => $menu ,
				"list" => $item->children ,
				"my_parent" => $item->hash_id
			])
		</li>
	@endforeach
</ol>
