{{-- title --}}
{!!
    widget('input')
        ->value($item->titleIn($locale))
        ->name('titles['.$locale.']')
        ->label(trans('menumaker::general.title'))
        ->ltr($locale == 'fa' ? false : true)
        ->inForm()
!!}

{{-- link --}}
{!!
    widget('input')
    	->value($item->linkIn($locale))
        ->name('links['.$locale.']')
		->condition($menu->isNotHidden('links'))
        ->label(trans('menumaker::general.link'))
        ->ltr()
        ->inForm()
!!}

{{-- icon --}}
{!!
    widget('icon-picker')
        ->value($item->iconIn($locale))
        ->name('icons['.$locale.']')
        ->condition($menu->isNotHidden('icons'))
        ->label(trans('menumaker::general.icon'))
        ->inForm()

!!}

{{-- label --}}
{!!
    widget('input')
 		->value($item->labelIn($locale))
        ->name('label['.$locale.']')
		->condition($menu->isNotHidden('label'))
        ->label(trans('menumaker::general.label'))
        ->ltr()
		->inForm()
!!}


{{-- description --}}
{!!
    widget('textarea')
    ->value($item->descriptionIn($locale))
        ->name('description['.$locale.']')
        ->condition($menu->isNotHidden('description'))
        ->label(trans('menumaker::general.description'))
        ->inForm()
!!}

{{-- style --}}
{!!
    widget('textarea')
    ->value($item->styleIn($locale))
        ->name('style['.$locale.']')
        ->condition($menu->isNotHidden('style'))
        ->label(trans('menumaker::general.style'))
        ->ltr()
        ->inForm()
!!}

{{-- html --}}
{!!
    widget('textarea')
    ->htmlIn($item->descriptionIn($locale))
        ->name('html['.$locale.']')
        ->condition($menu->isNotHidden('html'))
        ->label(trans('menumaker::general.html'))
        ->ltr()
        ->inForm()
!!}

<br>

{{-- class --}}
{!!
    widget('input')
    ->value($item->classAtrIn($locale))
        ->name('classAtr['.$locale.']')
        ->condition($menu->isNotHidden('classAtr'))
        ->label(trans('menumaker::general.classAtr'))
        ->ltr()
		->inForm()
!!}

{{-- id --}}
{!!
    widget('input')
    ->value($item->idAtrIn($locale))
        ->name('idAtr['.$locale.']')
         ->condition($menu->isNotHidden('idAtr'))
        ->label(trans('menumaker::general.idAtr'))
        ->ltr()
		->inForm()
!!}

<br>
{{-- color --}}
{!!
    widget('color-picker')
    ->value($item->colorIn($locale))
        ->id('color-'.$locale)
        ->name('color['.$locale.']')
         ->condition($menu->isNotHidden('color'))
		->label(trans('menumaker::general.color'))
        ->inForm()
!!}

{{-- onClick --}}
{!!
    widget('input')
    ->value($item->onClickIn($locale))
        ->name('onClick['.$locale.']')
         ->condition($menu->isNotHidden('onClick'))
        ->label(trans('menumaker::general.onClick'))
        ->ltr()
        ->inForm()
!!}

{{-- onDblClick --}}
{!!
    widget('input')
    ->value($item->onDblClickIn($locale))
        ->name('onDblClick['.$locale.']')
        ->condition($menu->isNotHidden('onDblClick'))
        ->label(trans('menumaker::general.onDblClick'))
        ->ltr()
		->inForm()
!!}

{{-- onMouseEnter --}}
{!!
    widget('input')
    ->value($item->onMouseEnterIn($locale))
        ->name('onMouseEnter['.$locale.']')
         ->condition($menu->isNotHidden('onMouseEnter'))
        ->label(trans('menumaker::general.onMouseEnter'))
		->ltr()
		->inForm()
!!}

{{-- onMouseLeave --}}
{!!
    widget('input')
    ->value($item->onMouseLeaveIn($locale))
        ->name('onMouseLeave['.$locale.']')
        ->condition($menu->isNotHidden('onMouseLeave'))
        ->label(trans('menumaker::general.onMouseLeave'))
        ->ltr()
		->inForm()
!!}

{{-- onMouseOver --}}
{!!
    widget('input')
    ->value($item->onMouseOverIn($locale))
        ->name('onMouseOver['.$locale.']')
        ->condition($menu->isNotHidden('onMouseOver'))
        ->label(trans('menumaker::general.onMouseOver'))
        ->ltr()
		->inForm()
!!}

{{-- onMouseOver --}}
{!!
    widget('input')
    ->value($item->onMouseOverIn($locale))
        ->name('onMouseOver['.$locale.']')
        ->condition($menu->isNotHidden('onMouseOver'))
        ->label(trans('menumaker::general.condition'))
        ->ltr()
		->inForm()
!!}

<br>

{{-- target --}}
{!!
	widget('toggle')
	->value($item->targetIn($locale))
		->id('target')
		->name('target['.$locale.']')
        ->condition($menu->isNotHidden('target'))
		->label(trans('menumaker::general.target'))
		->inForm()
!!}


{!!
	widget('input')
	->type('hidden')
	->value($item->hashid)
	->name('hashid')
!!}

{!!
	widget('input')
	->type('hidden')
	->value($menu->hashid)
	->name('menu_id')
!!}
