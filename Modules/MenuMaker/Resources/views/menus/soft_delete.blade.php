{!! Widget('modal')->target(route('manage.menus.soft_delete',['hash_id'=> $model->hashid]))->label("tr:menumaker::general.soft_delete") !!}

<div class="modal-body">
	{!!
        widget('input')
            ->value($model->slug)
            ->name('slug')
            ->label(trans('menumaker::general.slug'))
            ->ltr()
            ->disabled(true)
            ->inForm()
    !!}
</div>

@include('manage::forms.buttons-for-modal', [ 'save_shape' => 'danger', 'save_label' => trans("menumaker::general.soft_delete"), 'save_value' => 'delete' ])
