@extends('manage::layouts.template')
@section('content')


	@include("manage::widgets.tabs", [
		'current' => $page[0][0],
		'tabs' => $tabs
	])


	@include("manage::widgets.toolbar" , [
	'buttons' => [
	[
	'target' => "modal:" . route('manage.menus.create'),
	'type' => "success",
	'caption' => trans('menumaker::general.add'),
	'icon' => "plus-circle",
	'condition' => user()->is_a('developer')
	]]])


	@php
		$headings = [
			trans('menumaker::general.title'),
			trans('menumaker::general.slug'),
			trans('menumaker::general.updated_at'),
			trans('menumaker::general.subItems'),
		];
	if(user()->is_a('developer'))
		array_push($headings,trans('menumaker::general.action'));
	@endphp


	<div id="items">
		@include("manage::widgets.grid" , [
			'models' => $items,
			'table_id' => "tblConfigs",
			'row_view' => "menumaker::menus.row",
			'handle' => "counter",
			'headings' => $headings,
		])
	</div>

@endsection
