{!! Widget('modal')->target(route('manage.menus.store'))->label(trans('menumaker::general.create_new_menu')) !!}

<div class="modal-body">


    {!!
    widget('input')
        ->value()
        ->name('titles['.getLocale().']')
        ->label(trans('menumaker::general.title'))
        ->disabled(isset($disabled) && $disabled ? true : false)
        ->ltr(getLocale() == 'fa' ? false : true)
        ->inForm()
    !!}

    {!!
        widget('input')
            ->value()
            ->name('slug')
            ->label(trans('menumaker::general.slug'))
            ->disabled(isset($disabled) && $disabled ? true : false)
            ->ltr()
            ->inForm()
    !!}

    {!!
        widget('input')
            ->value(menu()->parents()->count() + 1)
            ->name('order')
            ->label(trans('menumaker::general.order'))
            ->ltr()
            ->inForm()
    !!}

    <h4 class="modal-title">{{ trans('menumaker::general.new_menu_setting') }}</h4>
    <table class="table table-grid-align-middle">
        @foreach($elements as $element)
            <tr>
                <th scope="row" width="200">{{ trans("menumaker::general.elements.$element") }}</th>
                <td style="text-align: left" width="200">
                    {!!
                        widget('combo')
                        ->id($element)
                        ->name('settings['.$element.']')
                        ->value($settings[$element] ?? model('menu')::ITEM_HIDDEN)
                        ->valueField('key')
                         ->captionField('caption')
                        ->options([
                            ['key' => model('menu')::ITEM_HIDDEN, 'caption' => trans('menumaker::general.hidden')],
                            ['key' => model('menu')::ITEM_OPTIONAL, 'caption' => trans('menumaker::general.optional')],
                            ['key' => model('menu')::ITEM_REQUIRED, 'caption' => trans('menumaker::general.required')],
                        ])
                     !!}
                </td>

            </tr>
        @endforeach
    </table>


    <div class="">
        <p class="f17">
            {{trans('menumaker::general.languages')}}
        </p>
        @foreach($locales as $locale)
            {!!
                widget('toggle')
                    ->id('lang-'.$locale)
                    ->name('locales['.$locale.']')
                    ->label(trans('menumaker::general.locale.'.$locale))
                    ->value(1)
            !!}
        @endforeach
    </div>

    @include('manage::forms.buttons-for-modal')
</div>
