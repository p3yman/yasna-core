{!! Widget('modal')->target(route('manage.menus.update',['hash_id'=> $item->hashid] ))->label(trans('menumaker::general.edit')) !!}

<div class="modal-body">


	{!!
    widget('input')
        ->value($item->titleIn(getLocale()))
        ->name('titles['.getLocale().']')
        ->label(trans('menumaker::general.title'))
        ->disabled(isset($disabled) && $disabled ? true : false)
        ->ltr(getLocale() == 'fa' ? false : true)
		->inForm()
!!}
	{!!
		widget('input')
			->value($item->slug)
			->name('slug')
			->label(trans('menumaker::general.slug'))
			->ltr()
			->inForm()
	!!}

	{!!
		widget('input')
			->value($item->order)
			->name('order')
			->label(trans('menumaker::general.order'))
			->ltr()
			->inForm()
	!!}


	<h4 class="modal-title {{ (user()->isDeveloper())?'':'hidden' }}">{{ trans('menumaker::general.edit_menu_setting') }}</h4>
	<table class="table table-grid-align-middle {{ (user()->isDeveloper())?'':'hidden' }}">
		@foreach($elements as $element)
			<tr>
				<th scope="row" width="200">{{ trans("menumaker::general.elements.$element") }}</th>
				<td style="text-align: left" width="200">
					{!!
						widget('combo')
						->id($element)
						->name('settings['.$element.']')
						->value(array_key_exists($element, $settings) ? $settings[$element] : 2)
						->valueField('key')
						->captionField('caption')
						->options([
							['key' => model('menu')::ITEM_HIDDEN, 'caption' => trans('menumaker::general.hidden')],
							['key' => model('menu')::ITEM_OPTIONAL, 'caption' => trans('menumaker::general.optional')],
							['key' => model('menu')::ITEM_REQUIRED, 'caption' => trans('menumaker::general.required')],
						])
					 !!}
				</td>

			</tr>
		@endforeach
	</table>


	<div class="{{ (user()->isDeveloper())?'':'hidden' }}">
		<p class="f17">
			{{trans('menumaker::general.languages')}}
		</p>
		@foreach($locales as $locale)
			{!!
				widget('toggle')
					->id('lang-'.$locale)
					->name('locales['.$locale.']')
					->label(trans('menumaker::general.locale.'.$locale))
					->value(in_array($locale,$enabled_languages) ? 1 : 0)
			!!}
		@endforeach
	</div>


	{!!
		widget('input')
			->type('hidden')
			->name('hashid')
			->value(!empty($item->hashid) ? $item->hashid : hashid(0))
	!!}


	@include('manage::forms.buttons-for-modal')
</div>
