{!! Widget('modal')->target(route('manage.menus.hard_delete',['hash_id'=> $model->hashid]))->label(trans("menumaker::general.destroy")) !!}

<div class="modal-body">
	{!!
        widget('input')
            ->value($model->slug)
            ->name('slug')
            ->label(trans('menumaker::general.slug'))
            ->ltr()
            ->disabled(true)
            ->inForm()
    !!}
</div>

@include('manage::forms.buttons-for-modal', [ 'save_shape' => 'danger', 'save_label' => trans("menumaker::general.delete"), 'save_value' => 'delete' ])
