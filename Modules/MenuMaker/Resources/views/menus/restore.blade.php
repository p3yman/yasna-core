{!! Widget('modal')->target(route('manage.menus.restore',[ 'hash_id'=> $model->hashid ]))->label(trans('manage::forms.button.undelete')) !!}
<div class="modal-body">
    {!!
        widget('input')
            ->value($model->slug)
            ->name('slug')
            ->label(trans('menumaker::general.slug'))
            ->ltr()
            ->disabled(true)
            ->inForm()
    !!}
</div>

<div class="modal-footer">
    {!!
        Widget('button')
        ->type('submit')
        ->value($model->isTrashed()? 'restore' : 'delete')
        ->label(trans("menumaker::general.restore"))
        ->class('btn-primary')
        ->shape($model->isTrashed()? 'success' : 'warning')
     !!}

    {!!
        Widget('button')
        ->label(trans('manage::forms.button.cancel'))
        ->id('btnCancel')
        ->shape('link')
        ->onClick('$(".modal").modal("hide")')
     !!}
</div>


