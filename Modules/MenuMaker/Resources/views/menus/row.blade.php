@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => route('manage.menus.reload', [ 'hash_id' => $model->hashid ]),
])


<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->titleIn(getLocale()),
		'size' => "16" ,
		'class' => "font-yekan text-bold" ,
		'link' =>  !$model->trashed()
			? user()->is_a('developer') ? 'modal:' . route('manage.menus.edit', [ 'hash_id' => $model->hashid ],false) : '': '#',
	])
</td>
<td>
	@include("manage::widgets.grid-text" , [ 'text' => $model->slug, 'size' => "12" ])
</td>
<td>{{ jdate($model->updated_at)->ago() }}</td>
<td>
	<a href="@if(!$model->trashed()) {{ route('manage.menus.links.index', [ 'menu_id' => $model->hashid ]) }} @else # @endif"
	   class="font-yekan text-bold btn btn-link"
	   @if($model->trashed()) onclick="return false;" disabled="disabled" @endif
	   style="font-size: 16px;">{{ trans('menumaker::general.see') }} {{ ad($model->CountAllChildren()) }} {{ trans('menumaker::general.case') }}</a>
</td>

@if(user()->is_a('developer'))
	@include("manage::widgets.grid-actionCol" , [
		"actions" => [
			[
				'pencil',
				trans('manage::forms.button.edit'),
				"modal:" . route('manage.menus.edit', ['hash_id' => $model->hashid],false),
				!$model->trashed()
			], [
				'trash-o',
				trans('manage::forms.button.soft_delete'),
				"modal:" . route('manage.menus.soft_delete', ['hash_id' => $model->hashid], false),
				!$model->trashed()
			],
			[
				'recycle',
				trans('manage::forms.button.undelete'),
				"modal:" . route('manage.menus.restore', ['hash_id' => $model->hashid], false),
				$model->trashed()
			],
			[
				'times',
				trans('manage::forms.button.hard_delete'),
				"modal:" . route('manage.menus.hard_delete', ['hash_id' => $model->hashid], false),
				$model->trashed()
			],
		]
	])
@endif
