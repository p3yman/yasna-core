<?php

//TODO: It's not a good idea to insert closures and conditionals in the config files. This may put the automatic caching and serialization mechanism in trouble. - Taha 97/7/26

return [
     'name'     => 'MenuMaker',
     'settings' => [
          'links'        => \Modules\MenuMaker\Entities\Menu::ITEM_OPTIONAL,
          'icons'        => \Modules\MenuMaker\Entities\Menu::ITEM_OPTIONAL,
          'label'        => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'style'        => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'description'  => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'condition'    => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'target'       => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'classAtr'     => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'idAtr'        => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'color'        => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'html'         => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'onClick'      => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'onDblClick'   => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'onMouseEnter' => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'onMouseLeave' => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
          'onMouseOver'  => \Modules\MenuMaker\Entities\Menu::ITEM_HIDDEN,
     ],
];
