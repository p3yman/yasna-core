<?php

use App\Models\Menu;


if (!function_exists('menumaker')) {
    /**
     *  Shortcut to App\Models\Menu
     *
     * @deprecated
     * @return Menu
     */
    function menumaker()
    {
        return new Menu();
    }
}

if (!function_exists('menu')) {
    /**
     * Shortcut to App\Models\Menu
     *
     * @param null|string $slug_or_hashid
     * @param bool        $with_trashed
     *
     * @return Menu
     */
    function menu($slug_or_hashid = null, $with_trashed = false)
    {
        if (is_null($slug_or_hashid)) {
            return new Menu();
        }

        if (isset($slug_or_hashid) && hashid($slug_or_hashid)) {
            $hashid = $slug_or_hashid;
            return model('menu', $hashid, $with_trashed);
        } else {
            $slug   = $slug_or_hashid;
            $object = new Menu();
            return $object->setPosition($slug);
        }

    }
}
