<?php

namespace Modules\MenuMaker\Http\Controllers\V1;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class MenuController extends YasnaApiController
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = 'menu';

    /**
     * The Builder to Find Items
     *
     * @var Builder|HasMany
     */
    protected $builder;

    /**
     * The Locale in Use
     *
     * @var string
     */
    protected $locale;

    /**
     * The Meta Data of the Response
     *
     * @var array
     */
    protected $meta_data = [];



    /**
     * Returns the site's menus with some given filters.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function list(SimpleYasnaRequest $request)
    {
        $this->resolveLocale($request);
        $this->listGenerateStartingBuilder($request);

        return $this->success($this->mappedArray(), $this->meta_data);
    }



    /**
     * Returns a mapped array of the found menu items.
     *
     * @return array
     */
    protected function mappedArray()
    {
        return $this
             ->builder
             ->get()
             // Filter with locale
             ->filter(function (Menu $menu) {
                 return $menu->isEnabled($this->locale);
             })
             // Map to API array
             ->map(function (Menu $menu) {
                 return $menu->apiArray($this->locale, true);
             })->toArray()
             ;
    }



    /**
     * Resolves the locale.
     *
     * @param SimpleYasnaRequest $request
     */
    protected function resolveLocale(SimpleYasnaRequest $request)
    {
        $requested_locale = $request->input('locale');

        if ($requested_locale and $this->localeIsAcceptable($requested_locale)) {
            $locale = $requested_locale;
        } else {
            $locale = getLocale();
        }

        $this->meta_data['locale'] = $locale;
        $this->locale              = $locale;
    }



    /**
     * Whether the given locale is acceptable in the site.
     *
     * @param string $locale
     *
     * @return bool
     */
    protected function localeIsAcceptable(string $locale)
    {
        $site_locales = get_setting('site_locales');

        return in_array($locale, $site_locales);
    }



    /**
     * Generates the builder for the list action.
     *
     * @param SimpleYasnaRequest $request
     */
    protected function listGenerateStartingBuilder(SimpleYasnaRequest $request)
    {
        $position = $request->position;

        if ($position) {
            $this->meta_data['position'] = $position;

            $this->builder = model($this->model_name, $position)->children();
        } else {
            $this->builder = model($this->model_name)->whereNull('parent_id');
        }

        $this->builder->orderBy('order');
    }
}
