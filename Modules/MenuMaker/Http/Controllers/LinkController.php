<?php

namespace Modules\MenuMaker\Http\Controllers;

use Modules\MenuMaker\Http\Requests\DeleteLinkRequest;
use Modules\MenuMaker\Http\Requests\StoreLinkRequest;
use Modules\MenuMaker\Http\Requests\UpdateLinkRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class LinkController extends YasnaController
{
    protected $base_model  = 'menu';
    protected $view_folder = 'menumaker::';



    /**
     * Show lists of specified menu
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($hash_id)
    {
        $menu  = $this->model($hash_id, true);
        $items = $this->model()->parents()->withTrashed()->get();
        $tabs  = $this->tabs();

        $page      = [
             '0' => ['menus', trans('menumaker::general.main_title'), 'menus'],
             '1' => [$menu->hashid . '/links', $menu->titleIn(getLocale()), 'menus'],
        ];
        $sub_items = collect($menu->children)->sortBy('order');
        return $this->view('links.index', compact('menu', 'items', 'tabs', 'page', 'hash_id', 'sub_items'));
    }



    /**
     * Show modal for submit new link as submenu
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create($hash_id)
    {
        $item              = $this->model($hash_id, true);
        $enabled_languages = $item->enabled_languages;
        $locales           = setting('site_locales')->gain();
        return $this->view('links.create', compact('hash_id', 'item', 'locales', 'enabled_languages'));
    }



    /**
     * Store new link as submenu
     *
     * @param StoreLinkRequest $request
     * @param string           $hash_id
     *
     * @return string
     */
    public function store(StoreLinkRequest $request, $hash_id)
    {
        $model = model('menu')->batchSave($request);

        $success_callback = "divReload('nestable')";
        return $this->jsonAjaxSaveFeedback(true, compact('success_callback'));
    }



    /**
     * Show edit modal for specified link
     *
     * @param string $hash_id
     * @param string $submenu_hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($hash_id, $submenu_hash_id)
    {
        $menu              = $this->model($hash_id, true);
        $item              = $this->model($submenu_hash_id, true);
        $parent_languages  = $item->parent->enabled_languages;
        $enabled_languages = $item->enabled_languages;

        $locales = setting('site_locales')->gain();
        return $this->view('links.edit',
             compact('hash_id', 'menu', 'item', 'locales', 'enabled_languages', 'parent_languages'));
    }



    /**
     * Update the specified menu item
     *
     * @param UpdateLinkRequest $request
     * @param string            $hash_id
     * @param string            $submenu_hash_id
     *
     * @return string
     */
    public function updateLink(UpdateLinkRequest $request, $hash_id, $submenu_hash_id)
    {
        $item             = $this->model($submenu_hash_id, true);
        $ok               = $item->batchSave($request);
        $success_callback = "divReload('nestable')";
        return $this->jsonAjaxSaveFeedback($ok, compact('success_callback'));
    }



    /**
     *  Hard delete the specified menu item with all submenu items
     *
     * @param DeleteLinkRequest $request
     * @param string            $hash_id
     * @param string            $submenu_hash_id
     *
     * @return string
     */
    public function hardDelete(DeleteLinkRequest $request, $hash_id, $submenu_hash_id)
    {
        $item             = $this->model($submenu_hash_id, true);
        $ok               = $item->hardDelete();
        $success_callback = '$.notify("' . trans("menumaker::general.alert.destroyed.success") . '", "success");';
        $success_refresh  = true;
        return $this->jsonAjaxSaveFeedback($ok, compact('success_callback', 'success_refresh'));
    }



    /**
     * Get order of links and update order
     *
     * @param SimpleYasnaRequest $request
     * @param string             $hash_id
     *
     * @return string
     */
    public function order(SimpleYasnaRequest $request, $hash_id)
    {
        $order  = json_decode($request->get('order'), true);
        $status = $request->get('status');
        $this->setOrder($order, $hash_id, $status);
        $success_callback = '$.notify("' . trans("menumaker::general.alert.saved.success") . '", "success");';
        return $this->jsonAjaxSaveFeedback(true, compact('success_callback'));
    }



    /**
     * Update order of links with parentId ,id, children[]
     *
     * @param array  $order
     * @param string $hash_id
     * @param array  $status
     *
     * @return bool
     */
    private function setOrder(array $order, $hash_id, $status)
    {
        static $i = 0;
        foreach ($order as $item) {
            $menu            = model('menu', $item['id']);
            $menu->order     = $i;
            $menu->parent_id = hashid($hash_id);
            $menu->status    = $status[$item['id']];
            $menu->save();
            $i++;
            if (isset($item['children'])) {
                $this->setOrder($item['children'], $item['id'], $status);
            }
        }
        return true;
    }
    


    /**
     * reload the submenus of specified menu item
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function reload($hash_id)
    {
        $menu = $this->model($hash_id, true);
        $list = collect($menu->children)->sortBy('order');
        $id   = 'nestable';

        return $this->view('links.nestable.index', compact('menu', 'id', 'hash_id', 'list'));
    }



    /**
     * Return the array as tabs in menus page
     *
     * @return array
     */
    public function tabs()
    {
        $tabs = $this->model()
                     ->parents()
                     ->orderBy('order')
                     ->get()
                     ->pluck('titles.' . getLocale(), 'hash_id')
                     ->toArray()
        ;
        $tabs = collect($tabs)->map(function ($title, $hash_id) {
            return ['url' => $hash_id . '/links', 'caption' => !is_null($title) ? $title : '---'];
        })->values()->toArray()
        ;
        if (user()->isDeveloper()) {
            array_unshift($tabs, ['url' => '', 'caption' => trans('menumaker::general.all_menus')]);
        }
        return $tabs;
    }
}
