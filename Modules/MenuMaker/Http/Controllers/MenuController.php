<?php

namespace Modules\MenuMaker\Http\Controllers;

use Modules\MenuMaker\Entities\Menu;
use Modules\MenuMaker\Http\Requests\DeleteMenuRequest;
use Modules\MenuMaker\Http\Requests\RestoreMenuRequest;
use Modules\MenuMaker\Http\Requests\StoreMenuRequest;
use Modules\MenuMaker\Http\Requests\UpdateMenuRequest;
use Modules\Yasna\Services\YasnaController;

class MenuController extends YasnaController
{
    protected $base_model  = 'menu';
    protected $view_folder = 'menumaker::';



    /**
     * Show list of menus
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if (!user()->isDeveloper()) {
            if ($this->model()->parents()->count()) {
                $first_menu_id = $this->model()->parents()->first()->hash_id;
                return redirect()->route('manage.menus.links.index', ['hash_id' => $first_menu_id]);
            } else {
                return abort(403);
            }
        }

        $items = $this->model()->parents()->orderBy('order')->withTrashed()->get();
        $tabs  = $this->tabs();
        $page  = [
             '0' => ['menus', trans('menumaker::general.main_title'), 'menus'],
             '1' => ['menus', trans('menumaker::general.all'), 'menus'],
        ];
        return $this->view('menus.index', compact('items', 'tabs', 'page'));
    }



    /**
     * Show modal for submit new menu item
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        $elements = $this->model()->elements;
        $locales  = setting('site_locales')->gain();
        $settings = config('menumaker.settings');
        return $this->view('menus.create', compact('elements', 'locales', 'settings'));
    }



    /**
     * Store new menu item as parent
     *
     * @param StoreMenuRequest $request
     *
     * @return string
     */
    public function store(StoreMenuRequest $request)
    {
        $model           = model($this->base_model);
        $ok              = $model->batchSave($request);
        $success_refresh = true;
        return $this->jsonAjaxSaveFeedback($ok, compact('success_refresh'));
    }



    /**
     * Show edit modal for specified menu item
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($hash_id)
    {
        $item     = model('menu', $hash_id, true);
        $elements = $this->model()->elements;
        $locales  = setting('site_locales')->gain();
        $settings = (isset($item->meta['settings'])) ? (array)$item->meta['settings'] : config('menumaker.settings');
        $enabled_languages = $item->enabled_languages;
        return $this->view('menus.edit', compact('item', 'elements', 'locales', 'settings', 'enabled_languages'));
    }



    /**
     * Update the specified menu item
     *
     * @param UpdateMenuRequest $request
     * @param string            $hash_id
     *
     * @return string
     */
    public function rewrite(UpdateMenuRequest $request, $hash_id)
    {
        $item            = $this->model($hash_id, true);
        $ok              = $item->batchSave($request);
        $success_callback = "rowUpdate('auto','$hash_id')";

        return $this->jsonAjaxSaveFeedback($ok, compact('success_callback'));
    }



    /**
     * Show modal for soft delete the specified menu
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function softDeleteModal($hash_id)
    {
        $model = $this->model($hash_id);
        return $this->view('menus.soft_delete', compact('model'));
    }



    /**
     * Soft delete the specified menu item with all submenu items
     *
     * @param DeleteMenuRequest $request
     * @param string            $hash_id
     *
     * @return string
     * @throws \Exception
     */
    public function softDelete(DeleteMenuRequest $request, $hash_id)
    {
        $item = $this->model($hash_id);
        $item->softDeleteAllChildren();
        $ok               = $item->delete();
        $success_callback = "rowUpdate('auto','$hash_id')";
        return $this->jsonAjaxSaveFeedback($ok, compact('success_callback'));
    }



    /**
     * Show modal for restore the specified menu
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function restoreModal($hash_id)
    {
        $model = model('menu', $hash_id, true);
        return $this->view('menus.restore', compact('model'));
    }



    /**
     * Restore the specified menu item with all submenu items
     *
     * @param RestoreMenuRequest $request
     * @param string             $hash_id
     *
     * @return string
     */
    public function restore(RestoreMenuRequest $request, $hash_id)
    {
        $item = $this->model($hash_id, true);
        $item->restoreAllChildren();
        $ok               = $item->restore();
        $success_callback = "rowUpdate('auto','$hash_id')";
        return $this->jsonAjaxSaveFeedback($ok, compact('success_callback'));
    }



    /**
     * Show modal for hard delete the specified menu
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function hardDeleteModal($hash_id)
    {
        $model = model('menu', $hash_id, true);
        return $this->view('menus.hard_delete', compact('model'));
    }



    /**
     * Hard delete the specified menu item with all submenu items
     *
     * @param DeleteMenuRequest $request
     * @param string            $hash_id
     *
     * @return string
     */
    public function hardDelete(DeleteMenuRequest $request, $hash_id)
    {
        $item = $this->model($hash_id, true);
        $item->hardDeleteAllChildren();
        $ok              = $item->hardDelete();
        $success_refresh = true;
        return $this->jsonAjaxSaveFeedback($ok, compact('success_refresh'));
    }



    /**
     * reload the specified menu item
     *
     * @param string $hash_id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function reload($hash_id)
    {
        $model = model('menu', $hash_id, true);
        return $this->view('menus.row', compact('model'));
    }



    /**
     * Return the array as tabs in menus page
     *
     * @return array
     */
    public function tabs()
    {
        $tabs = $this->model()
                     ->parents()
                     ->orderBy('order')
                     ->get()
                     ->pluck('titles.' . getLocale(), 'hash_id')
                     ->toArray()
        ;
        $tabs = collect($tabs)
             ->map(function ($title, $hash_id) {
                 return ['url' => $hash_id . '/links', 'caption' => !is_null($title) ? $title : '---'];
             })->values()->toArray()
        ;
        array_unshift($tabs, ['url' => 'menus', 'caption' => trans('menumaker::general.all_menus')]);
        return $tabs;
    }

}
