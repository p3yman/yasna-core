<?php

namespace Modules\MenuMaker\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/menu-maker-list
 *                    Menu
 * @apiDescription    List of Menus
 * @apiVersion        1.0.0
 * @apiName           Menu
 * @apiGroup          MenuMaker
 * @apiPermission     None
 * @apiParam {String} [lang] The locale of the menus in the result. Site's locale will be assumed if not presented.
 * @apiParam {String} [position] The slug of a valid position
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "locale": "en",
 *           "position": "main"
 *      },
 *      "results": [
 *           {
 *                "title": "Home",
 *                "link": "home",
 *                "icon": null,
 *                "style": null,
 *                "description": null,
 *                "condition": null,
 *                "target": null,
 *                "classAtr": null,
 *                "idAtr": null,
 *                "color": null,
 *                "html": null,
 *                "label": null,
 *                "onClick": null,
 *                "onDblClick": null,
 *                "onMouseEnter": null,
 *                "onMouseLeave": null,
 *                "onMouseOver": null
 *           },
 *           {
 *                "title": "Home",
 *                "link": "home",
 *                "icon": null,
 *                "style": null,
 *                "description": null,
 *                "condition": null,
 *                "target": null,
 *                "classAtr": null,
 *                "idAtr": null,
 *                "color": null,
 *                "html": null,
 *                "label": null,
 *                "onClick": null,
 *                "onDblClick": null,
 *                "onMouseEnter": null,
 *                "onMouseLeave": null,
 *                "onMouseOver": null
 *           }
 *      ]
 * }
 * @apiErrorExample   Not-Implemented:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-501",
 *      "userMessage": "Not Implemented!",
 *      "errorCode": "endpoint-501",
 *      "moreInfo": "endpoint.moreInfo.endpoint-501",
 *      "errors": []
 * }
 * @method  \Modules\MenuMaker\Http\Controllers\V1\MenuController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Menu";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MenuMaker\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'MenuController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $model            = model('menu');
        $model->titles    = ['en' => 'Home'];
        $model->links     = ['en' => 'home'];
        $model->parent_id = 1;

        return api()->successRespond([
             $model->apiArray('en'),
             $model->apiArray('en'),
        ], [
             "locale"   => "en",
             "position" => "main",
        ]);
    }
}
