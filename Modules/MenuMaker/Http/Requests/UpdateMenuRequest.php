<?php

namespace Modules\MenuMaker\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class UpdateMenuRequest extends YasnaRequest
{
    protected $model_name = "menu";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'slug'   => ['required', 'max:64'],
             'titles' => ['required', 'array', 'array_not_empty', 'max:64'],
             'order'  => ['required', 'numeric', 'min:0', 'max:100'],
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "array_not_empty" => trans_safe("menumaker::validation.array_not_empty"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $elements   = $this->model->elements();
        $attributes = [];
        foreach ($elements as $key => $element) {
            $attributes[$element] = trans("menumaker::general." . $element);
        }
        return $attributes;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['locales'] = array_keys($this->removeNullElements($this->data['locales']));
    }



    /**
     * Remove elements those are null
     *
     * @param $array
     *
     * @return array|null
     */
    private function removeNullElements($array)
    {
        if (is_array($array)) {
            return array_filter($array, function ($var) {
                if ($var != null || $var != 0) {
                    return $var;
                }
            });
        }
        return null;
    }
}
