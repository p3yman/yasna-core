<?php

namespace Modules\MenuMaker\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class StoreMenuRequest extends YasnaRequest
{
    protected $model_name = "menu";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'slug'   => ['required', 'unique:menus', 'max:64'],
             'titles' => ['required', 'array', 'array_not_empty', 'max:64'],
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "array_not_empty" => trans_safe("menumaker::validation.array_not_empty"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['locales'] = array_keys($this->removeNullElements($this->data['locales']));
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $elements   = $this->model->elements();
        $attributes = [];
        foreach ($elements as $key => $element) {
            $attributes[$element] = trans("menumaker::general." . $element);
        }
        return $attributes;
    }



    /**
     * Remove elements those are null
     *
     * @param $array
     *
     * @return array|null
     */
    private function removeNullElements($array)
    {
        if (is_array($array)) {
            return array_filter($array, function ($var) {
                if ($var != null || $var != 0) {
                    return $var;
                }
            });
        }
        return null;
    }

}
