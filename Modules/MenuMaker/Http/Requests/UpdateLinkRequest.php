<?php

namespace Modules\MenuMaker\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class UpdateLinkRequest extends YasnaRequest
{
    protected $model_name = "menu";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $menu     = model('menu', $this->menu_id);
        $elements = $this->model->elements;
        $rules    = [];

        foreach ($elements as $key => $element) {
            if ($menu->itemSettingValue($element) == $this->model::ITEM_REQUIRED) {
                $rules[$element] = "array|array_not_empty";
            }
        }
        $rules['titles'] = "array|required|max:64";

        return $rules;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $elements   = $this->model->elements();
        $attributes = [];
        foreach ($elements as $key => $element) {
            $attributes[$element] = trans("menumaker::general." . $element);
        }
        return $attributes;
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "array_not_empty" => trans_safe("menumaker::validation.array_not_empty"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $elements              = $this->model->elements;
        $request_keys          = array_keys($this->data);
        $this->data['locales'] = array_keys($this->removeNullElements($this->data['locales']));
        $this->data['titles']  = $this->removeNullElements($this->data['titles']);
        if (isset($this->data['links'])) {
            $this->data['links'] = $this->removeNullElements($this->data['links']);
        }
        foreach ($elements as $key => $element) {
            if (in_array($element, $request_keys)) {
                $this->data[$element] = $this->removeNullElements($this->data[$element]);
            }
        }
    }



    /**
     * Remove elements those are null
     *
     * @param $array
     *
     * @return array|null
     */
    private function removeNullElements($array)
    {
        if (is_array($array)) {
            return array_filter($array, function ($var) {
                if ($var != null || $var != 0) {
                    return $var;
                }
            });
        }

        return null;
    }
}
