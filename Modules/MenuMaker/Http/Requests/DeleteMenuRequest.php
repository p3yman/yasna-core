<?php

namespace Modules\MenuMaker\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DeleteMenuRequest extends YasnaRequest
{
    protected $model_name = "menu";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isDeveloper();
    }

}
