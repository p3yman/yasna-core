<?php
/**
 * |--------------------------------------------------------------------------
 *  Define menu route as position
 * |--------------------------------------------------------------------------
 */
Route::group([
     'middleware' => ['web', 'auth', 'is:super'],
     'prefix'     => '/manage',
     'namespace'  => 'Modules\MenuMaker\Http\Controllers',
], function () {
    Route::get('menus/menus', 'MenuController@index');
    Route::get('menus/', 'MenuController@index')->name('manage.menus.index');

    Route::get('menus/create', 'MenuController@create')->name('manage.menus.create')->middleware('is:developer');
    Route::post('menus/', 'MenuController@store')->name('manage.menus.store')->middleware('is:developer');

    Route::get('menus/{hash_id}/edit', 'MenuController@edit')->name('manage.menus.edit');
    Route::post('menus/{hash_id}', 'MenuController@rewrite')->name('manage.menus.update');

    Route::get('menus/{hash_id}/soft_delete', 'MenuController@softDeleteModal')->name('manage.menus.soft_delete');
    Route::post('menus/{hash_id}/soft_delete', 'MenuController@softDelete')->middleware('is:developer');

    Route::get('menus/{hash_id}/restore', 'MenuController@restoreModal')->name('manage.menus.restore');
    Route::post('menus/{hash_id}/restore', 'MenuController@restore')->middleware('is:developer');

    Route::get('menus/{hash_id}/hard_delete', 'MenuController@hardDeleteModal')->name('manage.menus.hard_delete_modal');
    Route::post('menus/{hash_id}/hard_delete', 'MenuController@hardDelete')->name('manage.menus.hard_delete')->middleware('is:developer');

    Route::get('menus/{hash_id}/edit', 'MenuController@edit')->name('manage.menus.edit');

    Route::get('menus/{hash_id}/reload', 'MenuController@reload')->name('manage.menus.reload');
});


/**
 * |--------------------------------------------------------------------------
 *  Define links of menu route as menu and submenu
 * |--------------------------------------------------------------------------
 */
Route::group([
     'middleware' => ['web', 'auth', 'is:super'],
     'prefix'     => '/manage/menus/{hash_id}',
     'namespace'  => 'Modules\MenuMaker\Http\Controllers',
], function () {
    Route::get('links/', 'LinkController@index')->name('manage.menus.links.index');
    Route::get('links/create', 'LinkController@create')->name('manage.menus.links.create');
    Route::post('links/', 'LinkController@store')->name('manage.menus.links.store');

    Route::get('links/{submenu_hash_id}/edit', 'LinkController@edit')->name('manage.menus.links.edit');
    Route::post('links/{submenu_hash_id}', 'LinkController@updateLink')->name('manage.menus.links.update');

    Route::get('links/reload/reload', 'LinkController@reload')->name('manage.menus.links.reload');
    Route::post('links/{submenu_hash_id}/hard_delete', 'LinkController@hardDelete');
    Route::post('links/order/order', 'LinkController@order');
});
