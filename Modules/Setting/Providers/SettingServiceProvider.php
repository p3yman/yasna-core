<?php

namespace Modules\Setting\Providers;

use Modules\Setting\Console\SetSettingCommand;
use Modules\Setting\Http\Endpoints\V1\DestroyEndpoint;
use Modules\Setting\Http\Endpoints\V1\GetEndpoint;
use Modules\Setting\Http\Endpoints\V1\ListEndpoint;
use Modules\Setting\Http\Endpoints\V1\RestoreEndpoint;
use Modules\Setting\Http\Endpoints\V1\SaveEndpoint;
use Modules\Setting\Http\Endpoints\V1\SetEndpoint;
use Modules\Setting\Http\Endpoints\V1\TrashEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class SettingServiceProvider
 *
 * @package Modules\Setting\Providers
 */
class SettingServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
        $this->registerArtisanCommands();
    }



    /**
     * register endpoints
     *
     * @return void
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }
        $this->registerSettingCrudEndpoint();
    }



    /**
     * register setting crud endpoints
     *
     * @return void
     */
    private function registerSettingCrudEndpoint()
    {
        endpoint()->register(SaveEndpoint::class);
        endpoint()->register(TrashEndpoint::class);
        endpoint()->register(DestroyEndpoint::class);
        endpoint()->register(RestoreEndpoint::class);
        endpoint()->register(SetEndpoint::class);
        endpoint()->register(GetEndpoint::class);
        endpoint()->register(ListEndpoint::class);
    }



    /**
     * register artisan commands
     */
    private function registerArtisanCommands()
    {
        $this->addArtisan(SetSettingCommand::class);
    }
}
