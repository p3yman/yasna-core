<?php

if (!function_exists("setting")) {

    /**
     * A shortcut to fire a chain command to receive setting value
     *
     * @param $slug
     *
     * @return \Modules\Setting\Entities\Setting
     */
    function setting($slug = null)
    {
        if ($slug) {
            return \Modules\Setting\Entities\Setting::builder($slug);
        } else {
            return new \Modules\Setting\Entities\Setting();
        }
    }
}


if (!function_exists("get_setting")) {

    /**
     * Easier way to call settings with super-minimal parameters (language=auto, cache=on, default=no etc.)
     *
     * @param $slug
     *
     * @return array|bool|mixed
     */
    function get_setting($slug)
    {
        return setting($slug)->gain();
    }
}
