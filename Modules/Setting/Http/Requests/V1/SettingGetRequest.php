<?php

namespace Modules\Setting\Http\Requests\V1;

use Modules\Setting\Entities\Setting;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * Class PostPinRequest
 *
 * @property Setting $model
 * @property string  $slug
 */
class SettingGetRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Setting";



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        if (!$this->isDiscoverable()) {
            return dev();
        }
        return true;
    }



    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
             "slug" => "required",
        ];
    }



    /**
     * check the api discoverable in model
     *
     * @return bool
     */
    protected function isDiscoverable()
    {
        if (!$this->isset("slug")) {
            return true; //will fail on validation rules
        }

        return boolval(setting($this->slug)->api_discoverable);
    }
}
