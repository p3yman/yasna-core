<?php

namespace Modules\Setting\Http\Requests\V1;

use Illuminate\Validation\Rule;
use Modules\Setting\Entities\Setting;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PinFeatureRequest
 *
 * @property Setting $model
 */
class SettingSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Setting";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $reserved_slugs     = $this->model->reservedSlugs();
        $allowed_categories = $this->model->categoryList();
        $allowed_types      = $this->model->dataTypeList();

        return [
             'title'     => [
                  "required",
                  Rule::unique('settings', 'title')
                      ->ignore($this->model->id, 'id'),
             ],
             'slug'      => [
                  "required",
                  "alpha_dash",
                  "not_in:$reserved_slugs",
                  Rule::unique('settings', 'slug')
                      ->ignore($this->model->id, 'id'),
             ],
             'order'     => "required|numeric",
             'category'  => "required|in:$allowed_categories",
             'data_type' => "required|in:$allowed_types",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'order'            => "ed|numeric",
             'is_localized'     => "boolean",
             'api_discoverable' => "boolean",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'title',
             'slug',
             'order',
             'category',
             'data_type',
             'is_localized',
             'api_discoverable',
             'default_value',
             'custom_value',
             'css_class',
             'hint',
             'meta',
        ];
    }
}
