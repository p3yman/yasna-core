<?php

namespace Modules\Setting\Http\Requests\V1;

use Modules\Setting\Entities\Setting;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PinFeatureRequest
 *
 * @property Setting $model
 */
class SettingSetRequest extends YasnaFormRequest
{
    protected $model_name               = "Setting";
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "value"         => "present" . ($this->model->isLocalized() ? "|array" : ""),
             "default_value" => dev() ? "present" : "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $array = ['value'];

        if (dev()) {
            $array[] = "default_value";
        }

        return $array;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("setting::attributes");
    }



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        if ($this->model->isUpstream()) {
            return user()->isDeveloper();
        }

        return user()->isSuperadmin();
    }
}
