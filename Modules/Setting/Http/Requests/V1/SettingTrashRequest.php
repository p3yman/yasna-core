<?php

namespace Modules\Setting\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class SettingTrashRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Setting";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;
}
