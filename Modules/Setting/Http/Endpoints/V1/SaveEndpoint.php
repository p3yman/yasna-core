<?php

namespace Modules\Setting\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Setting\Http\Controllers\V1\CrudController;

/**
 * @api               {POST}
 *                    /api/modular/v1/setting-save
 *                    Setting Create
 * @apiDescription    Create s new setting
 * @apiVersion        1.0.0
 * @apiName           Setting Create
 * @apiGroup          Setting
 * @apiPermission     Developer
 * @apiParam {string}   id Hashid of the existing setting
 * @apiParam {string}   [slug] The slug for new setting
 * @apiParam {string}   [title] The title for new setting
 * @apiParam {string}   [category] The category name for new setting
 * @apiParam {int}      [order] The number of order
 * @apiParam {string}   [data_type] The datatype of new setting
 * @apiParam {boolean}  is_localized Is localized this setting?
 * @apiParam {boolean}  api_discoverable API discover
 * @apiParam {string}   default_value Set a default value
 * @apiParam {string}   custom_value Set a custom value
 * @apiParam {string}   css_class Set css class
 * @apiParam {string}   hint Set css class
 * @apiParam {string}   meta Set css class
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "eKbrK",
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Error validating data",
 *      "userMessage": "Received data has errors"
 *      "errorCode": 422,
 *      "moreInfo": "setting.moreInfo.422",
 *      "errors": [
 *                  "title": [
 *                      [
 *                      "The title field is required."
 *                    ]
 *               ],
 *                 "slug": [
 *                      [
 *                      "The slug field is required."
 *                     ]
 *              ],
 *                "order": [
 *                      [
 *                      "The order field is required."
 *                    ]
 *               ],
 *                 "category": [
 *                      [
 *                      "The category field is required."
 *                     ]
 *              ],
 *                "data_type": [
 *                      [
 *                      "The data_type field is required."
 *                     ]
 *              ],
 *      ]
 * }
 * @method CrudController controller()
 */
class SaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Setting Create";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Setting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(125),
        ]);
    }
}
