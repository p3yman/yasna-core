<?php

namespace Modules\Setting\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Setting\Http\Controllers\V1\CrudController;

/**
 * @api               {GET}
 *                    /api/modular/v1/setting-get
 *                    Get
 * @apiDescription    Get list of setting data
 * @apiVersion        1.0.0
 * @apiName           Get
 * @apiGroup          Setting
 * @apiPermission     Guest
 * @apiParam {string}   [slug] The slug of existing setting model
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "slug": "site_url",
 *          "authenticated_user" => hashid(user()->id),
 *      },
 *      "results": {
 *          "value": "http://localhost"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Not Found",
 *      "userMessage": "What you want is not found.",
 *      "errorCode": "404",
 *      "moreInfo": "setting.moreInfo.404",
 *      "errors": []
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request (If the current slug is not api-discoverable and user not developer)
 * {
 *      "status": 400,
 *      "developerMessage": "Unauthorized operation",
 *      "userMessage": "There is not enough access.",
 *      "errorCode": "403",
 *      "moreInfo": "setting.moreInfo.403",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class GetEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get the custom value of setting";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Setting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@get';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "result" => [
                  "value"=>"http://localhost"
             ],
        ], [
             "slug" => "site_url",
             "authenticated_user" => hashid(user()->id),
        ]);
    }
}
