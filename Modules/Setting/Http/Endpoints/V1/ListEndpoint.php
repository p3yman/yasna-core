<?php

namespace Modules\Setting\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Setting\Http\Controllers\V1\CrudController;

/**
 * @api               {GET}
 *                    /api/modular/v1/setting-list
 *                    List
 * @apiDescription    List of defined settings.
 * @apiVersion        1.0.0
 * @apiName           List
 * @apiGroup          Setting
 * @apiPermission     Guest
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": [
 *           {
 *               "id": "KXzVA",
 *               "created_at": 1560779246,
 *               "updated_at": null,
 *               "deleted_at": null,
 *               "created_by": {
 *                   "id": "",
 *                   "full_name": ""
 *               },
 *                   "updated_by": {
 *                   "id": "",
 *               "full_name": ""
 *               },
 *               "deleted_by": {
 *                   "id": "",
 *                   "full_name": ""
 *               },
 *               "slug": "instagram_link",
 *               "title": "Instagram Account",
 *               "category": "socials",
 *               "is_localized": 0,
 *               "data_type": "text",
 *               "value": "http://instagram.com/account"
 *           }
 *       }
 * }
 * @method CrudController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Setting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "total" => "29",
        ], [
             "id"           => "KXzVA",
             "created_at"   => 1560779246,
             "updated_at"   => null,
             "deleted_at"   => null,
             "created_by"   => [
                  "id"        => "",
                  "full_name" => "",
             ],
             "updated_by"   => [
                  "id"        => "",
                  "full_name" => "",
             ],
             "deleted_by"   => [
                  "id"        => "",
                  "full_name" => "",
             ],
             "slug"         => "instagram_link",
             "title"        => "Insta Account",
             "category"     => "socials",
             "is_localized" => 0,
             "data_type"    => "text",
             "value"        => "http://instagram.com/account",
        ]);
    }
}
