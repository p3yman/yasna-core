<?php

namespace Modules\Setting\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Setting\Http\Controllers\V1\CrudController;

/**
 * @api               {POST}
 *                    /api/modular/v1/setting-set
 *                    Set
 * @apiDescription    Set value to setting model
 * @apiVersion        1.0.0
 * @apiName           Set
 * @apiGroup          Setting
 * @apiPermission     Superadmin
 * @apiParam {string} id              Hashid of a existing setting model
 * @apiParam {mixed}  value           The value that user want to set in the setting value.
 * @apiParam {mixed}  [default_value] The default value, only for developers
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user" => hashid(1),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 500,
 *      "developerMessage": "Error in data storage",
 *      "userMessage": "Error in data storage.This is a temporary problem.Please try again later.",
 *      "errorCode": "507",
 *      "moreInfo": "endpoint.moreInfo.endpoint-507",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class SetEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Set value in setting model";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // <~~ is checked in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Setting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@set';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "authenticated_user" => hashid(1),
        ]);

    }
}
