<?php

namespace Modules\Setting\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Setting\Http\Controllers\V1\CrudController;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/setting-trash
 *                    Trash
 * @apiDescription    Soft delete the setting
 * @apiVersion        1.0.0
 * @apiName           Trash
 * @apiGroup          Setting
 * @apiPermission     Developer
 * @apiParam {string}   id Hashid of the setting
 * @apiSuccessExample Success-Response: 
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample   
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Content not found!",
 *      "userMessage": "Content not found!",
 *      "errorCode": "404",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class TrashEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Trash";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Setting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@trash';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "authenticated_user" => hashid(12),
        ]);
    }
}
