<?php

namespace Modules\Setting\Http\Controllers\V1;

use App\Models\Setting;
use Modules\Setting\Http\Requests\V1\SettingDestroyRequest;
use Modules\Setting\Http\Requests\V1\SettingGetRequest;
use Modules\Setting\Http\Requests\V1\SettingRestoreRequest;
use Modules\Setting\Http\Requests\V1\SettingSaveRequest;
use Modules\Setting\Http\Requests\V1\SettingSetRequest;
use Modules\Setting\Http\Requests\V1\SettingTrashRequest;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\Yasna\Services\YasnaApiController;

class CrudController extends YasnaApiController
{

    /**
     * create a new setting
     *
     * @param SettingSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(SettingSaveRequest $request)
    {
        $done = $request->model->batchSave($request);

        return $this->modelSaveFeedback($done);
    }



    /**
     * hard delete a setting
     *
     * @param SettingDestroyRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(SettingDestroyRequest $request)
    {
        $done = $request->model->hardDelete();

        return $this->typicalSaveFeedback($done);

    }



    /**
     * perform soft delete action of the setting
     *
     * @param SettingTrashRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function trash(SettingTrashRequest $request)
    {
        $saved = $request->model->delete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * perform undelete action of the setting
     *
     * @param SettingRestoreRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restore(SettingRestoreRequest $request)
    {
        $saved = $request->model->undelete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * set value for existing setting model
     *
     * @param SettingSetRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function set(SettingSetRequest $request)
    {
        $done = true; //<~~ bad idea!

        if ($request->model->isLocalized()) {
            foreach ((array)$request->value as $lang => $value) {
                $done = $request->model->in($lang)->setValue($value);
            }
        } else {
            $done = $request->model->setCustomValue($request->value);
        }

        if ($done and user()->isDeveloper()) {
            $done = $request->model->setDefaultValue($request->default_value);
        }

        return $this->typicalSaveFeedback($done);
    }



    /**
     * get custom value from existing setting model
     *
     * @param SettingGetRequest $request
     *
     * @return array
     */
    public function get(SettingGetRequest $request)
    {
        return $this->success([
             "value" => get_setting($request->slug),
        ], [
             'slug' => $request->slug,
        ]);
    }



    /**
     * get a list of settings
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array
     */
    public function list(SimpleYasnaListRequest $request)
    {
        $builder = model('setting')->newBuilderInstance();

        if (!user()->isSuperadmin()) {
            $builder->where("api_discoverable", 1);
        }

        if(!dev()) {
            $builder->where("category" , "!=", "upstream");
        }

        return $this->getResourcesFromBuilder($builder, $request);
    }
}
