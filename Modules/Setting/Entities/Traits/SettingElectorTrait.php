<?php

namespace Modules\Setting\Entities\Traits;


trait SettingElectorTrait
{
    /**
     * Modifies elector to contain only API discoverable items.
     */
    public function electorForApi()
    {
        $this->elector()->forApi();
    }



    /**
     * Modifies elector based on the specified value for the `api_discoverable` column.
     *
     * @param bool $value
     */
    public function electorApiDiscoverable(bool $value)
    {
        $this->elector()->where('api_discoverable', $value);
    }
}
