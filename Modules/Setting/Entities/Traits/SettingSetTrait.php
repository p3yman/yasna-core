<?php

namespace Modules\Setting\Entities\Traits;

/**
 * Class SettingSetTrait
 *
 * @package Modules\Yasna\Entities\Traits
 * @property $desired_language
 */
trait SettingSetTrait
{
    /**
     * Sets custom value to the already defined models.
     *
     * @param $new_value
     *
     * @return bool
     */
    public function setCustomValue($new_value)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($this->isNotDefined()) {
            return false;
        }

        // calculate desired value
        $new_value = $this->valueFromDataType($new_value);

        /*-----------------------------------------------
        | Process ...
        */
        if ($this->isLocalized()) {
            $array                           = $this->customValueToArray();
            $array[$this->desiredLanguage()] = $new_value;
            $new_value                       = json_encode($array);
        }

        $this->forgetMyCache();

        return $this->batchSaveBoolean([
             "custom_value" => $new_value,
        ]);
    }



    /**
     * @param $new_value
     *
     * @return bool
     */
    public function setValue($new_value)
    {
        return $this->setCustomValue($new_value);
    }



    /**
     * Sets default value to the already defined models.
     *
     * @param $new_value
     *
     * @return bool
     */
    public function setDefaultValue($new_value)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($this->isNotDefined()) {
            return false;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->forgetMyCache();
        return $this->batchSaveBoolean([
             "default_value" => $new_value,
        ]);
    }



    /**
     * Inserts new record in settings table
     *
     * @param $data
     */
    public function new($data)
    {
        $data = array_normalize($data, [
             "slug"          => "",
             "title"         => "",
             "order"         => "21",
             "category"      => "upstream",
             "data_type"     => "text",
             "default_value" => "",
             "hint"          => "",
             "is_localized"  => false,
        ]);

        yasna()->seed('settings', [$data]);
        $return = setting($data['slug'])->noCache()->isDefined();

        return $return;
    }



    /**
     * @param      $slug
     * @param      $new_value
     * @param bool $locale
     * @param bool $set_for_default
     *
     * @return bool
     * @deprecated
     */
    public static function set($slug, $new_value, $locale = false, $set_for_default = false)
    {
        if ($set_for_default) {
            return setting($slug)->setDefaultValue($new_value);
        }

        return setting($slug)->in($locale)->setValue($new_value);
    }



    /**
     * Returns a string representation of the specified value based on the data type.
     *
     * @param string|array|bool|integer $value
     *
     * @return string
     */
    protected function valueFromDataType($value)
    {
        $method_name = camel_case("value from " . $this->data_type);

        if ($this->hasMethod($method_name)) {
            return $this->$method_name($value);
        }

        return $value;
    }



    /**
     * Converts an array value to its string representation.
     *
     * @param array|string $value
     *
     * @return string
     */
    protected static function valueFromArray($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        return implode(HTML_LINE_BREAK, array_values($value));
    }



    /**
     * Converts a boolean value to its string representation.
     *
     * @param bool $bool
     *
     * @return string
     */
    protected static function valueFromBoolean(bool $bool)
    {
        return (string)$bool;
    }
}
