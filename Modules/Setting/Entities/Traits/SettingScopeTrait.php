<?php

namespace Modules\Setting\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;

trait SettingScopeTrait
{
    /**
     * Scope to contain only API discoverable items.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeForApi(Builder $builder)
    {
        return $builder->where('api_discoverable', 1);
    }
}
