<?php

namespace Modules\Setting\Entities\Traits;

trait SettingResourcesTrait
{
    /**
     * boot SettingResourcesTrait
     *
     * @return void
     */
    public static function bootSettingResourcesTrait()
    {
        static::addDirectResources([
             'slug',
             'title',
             'category',
             'is_localized',
             'data_type',
             "order",
             "css_class",
             "hint",
        ]);
    }



    /**
     * get Value resource.
     *
     * @return mixed
     */
    protected function getValueResource()
    {
        return $this->gain();
    }



    /**
     * get Values resource, when in multi-lingual mode.
     *
     * @return array|mixed
     */
    protected function getValuesResource()
    {
        if (!$this->isLocalized()) {
            return $this->gain();
        }

        return json_decode($this->custom_value, true);
    }



    /**
     * get DefaultValue resource.
     *
     * @return mixed
     */
    protected function getDefaultValueResource()
    {
        return $this->defaultValue();
    }
}
