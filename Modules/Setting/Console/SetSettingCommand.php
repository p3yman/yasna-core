<?php

namespace Modules\Setting\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class SetSettingCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:set-setting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'save something in the setting database.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yasna:set-setting 
    {slug : Name of slug that wants to change} 
    {value : Custom value of slug} 
    {lang? : Language parameter is optional (just for slugs that are localized)}';

    protected $slug;
    protected $value;
    protected $lang;



    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->purifyInput();
        $this->saveValue();
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['slug', InputArgument::REQUIRED, 'email'],
             ['value', InputArgument::REQUIRED, 'hello@yasna.team'],
             ['lang', InputArgument::OPTIONAL, 'fa'],
        ];
    }



    /**
     * Purify inputs of the console command.
     *
     * @return void
     */
    protected function purifyInput()
    {
        $this->slug  = $this->argument('slug');
        $this->value = $this->argument('value');
        $this->lang  = $this->argument('lang');

        if (!$this->lang) {
            $this->lang = getLocale();
        }
    }



    /**
     * Set value for the slug
     *
     * @return void
     */
    private function saveValue()
    {
        $feedback = setting($this->slug)->in($this->lang)->setValue($this->value);

        if($feedback) {
            $this->info("New value is set for `$this->slug`.");
        }
        else {
            $this->warn("Nothing set. Maybe because of wrong provided slug!");
        }
    }
}
