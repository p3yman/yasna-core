<?php

namespace Modules\Commenting\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class ListRequest extends YasnaRequest
{
    protected $model_name = "comment";
    protected $responder  = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $type     = $this->getData('type');
        $criteria = 'browse';
        return $this->model->hasManagePermission($type, $criteria);
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'type' => Rule::exists('posttypes', 'slug'),
        ];
    }


}
