<?php

namespace Modules\Commenting\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CommentSaveReplyRequest extends YasnaRequest
{
    protected $model_name = "Comment";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [];

        if ($this->data['new_status']) {
            $rules = [
                 'status' => "in:" . implode(',', model('comment')->availableStatusForProcess()),
            ];
        }

        if ($this->data['new_department'] and model('role')->supportRolesDefined()) {
            $rules['department'] = "required|numeric|min:1";
        }


        if (!$this->data['text'] and !$this->data['new_status'] and !$this->data['new_department']) {
            $rules['action'] = "required";
        }

        return $rules;
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'department.min'  => trans_safe("commenting::feed.department_required"),
             'action.required' => trans_safe("commenting::feed.action_required"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->can('process');
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'department' => "numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if ($this->model->status == $this->data['status']) {
            $this->data['new_status'] = false;
        } else {
            $this->data['new_status'] = $this->data['status'];
        }

        $this->correctDepartment();
        return;
    }



    /**
     * Corrects the department in the request.
     */
    protected function correctDepartment()
    {
        $requested_department = ($this->data['department'] ?? null);

        if ($this->model->current_department_id == $requested_department) {
            $this->data['new_department'] = false;
        } else {
            $this->data['new_department'] = $requested_department;
        }
    }
}
