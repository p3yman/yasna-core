<?php

namespace Modules\Commenting\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CommentSaveActivenessRequest extends YasnaRequest
{
    protected $model_name = "Comment";

    public function authorize()
    {
        if ($this->data['_submit'] == 'undelete') {
            $permit = 'bin';
        } else {
            $permit = 'delete';
        }

        return $this->model->can($permit);
    }
}
