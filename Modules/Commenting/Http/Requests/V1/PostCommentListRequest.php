<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/10/18
 * Time: 12:39 PM
 */

namespace Modules\Commenting\Http\Requests\V1;


use App\Models\Post;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * @property Post $model
 */
class PostCommentListRequest extends YasnaListRequest
{

    /**
     * @inheritdoc
     */
    protected $should_load_model = true;



    /**
     * @inheritdoc
     */
    protected $should_load_model_with_slug = true;



    /**
     * @inheritdoc
     */
    protected $model_name = "Post";



    /**
     * @inheritdoc
     */
    protected $should_load_model_with_hashid = true;



    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->has('commenting') and $this->checkPermissionPost();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'status'         => Rule::in($this->criteriaSlugs()),
             'department'     => Rule::in($this->departmentIds()),
             'with_replies'   => 'boolean',
             'replies_number' => 'integer|min:1',
             'replies_limit'  => 'numeric|min:1',
             'sort'           => Rule::in(['asc', 'desc']),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'department' => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $module = $this->runningModule();

        return [
             'status'         => $module->getTrans('validation.attributes.status'),
             'department'     => $module->getTrans('validation.attributes.department'),
             'with_replies'   => $module->getTrans('validation.attributes.with_replies'),
             'replies_number' => $module->getTrans('validation.attributes.replies_number'),
             'replies_limit'  => $module->getTrans('validation.attributes.replies_limit'),
             'sort'           => $module->getTrans('validation.attributes.sort'),
        ];
    }



    /**
     * Returns a list of departments' IDs.
     *
     * @return array
     */
    private function departmentIds()
    {
        return role()
             ->supportRoles()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Returns a list of acceptable criteria values.
     *
     * @return array
     */
    private function criteriaSlugs()
    {
        return [
             'pending',
             'approved',
             'archive',
             'spam',
             'bin',
        ];
    }



    /**
     * Check out the terms to see comments
     *
     * @return bool
     */
    private function checkPermissionPost()
    {
        return $this->model->isPublished() or $this->model->canEdit();
    }
}
