<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/11/18
 * Time: 11:30 AM
 */

namespace Modules\Commenting\Http\Requests\V1;


use App\Models\Post;
use App\Models\Posttype;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class PostCommentSaveRequest
 * <br>
 * The Request to Save a Comment of a Post
 *
 * @property Post $model
 */
class PostCommentSaveRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = 'post';

    /**
     * Whether a trashed model is acceptable or not.
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Name of the Main Responder
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->postAcceptsComment()
             and
             $this->clientCanComment()
        );
    }



    /**
     * Whether the post accepts comments.
     *
     * @return bool
     */
    protected function postAcceptsComment()
    {
        $model = $this->model;

        return (
             $model->exists
             and
             $model->has('commenting')
             and
             $this->getPosttype()->getCommentingInputValue('receive')
        );
    }



    /**
     * Whether current client can comment.
     *
     * @return bool
     */
    protected function clientCanComment()
    {
        return (
             user()->exists
             or
             $this->getPosttype()->getCommentingInputValue('allow_guests')
        );
    }



    /**
     * Returns the general rules.
     *
     * @return array
     */
    protected function generalRules()
    {
        return [
             'text'                  => 'required',
             'guest_email'           => 'email',
             'guest_mobile'          => 'phone:mobile',
             'initial_department_id' => 'in:' . implode(',', $this->departmentIds()),
             'is_private'            => 'boolean',
        ];
    }



    /**
     * Returns an array of rules based on the related posttype.
     *
     * @return array
     */
    protected function posttypeRules()
    {
        $posttype = $this->getPosttype();
        $rules    = [];

        if ($posttype->not_exists) {
            return $rules;
        }

        $rules['guest_name'] = 'required';

        if ($posttype->commentingFieldIsRequired('subject')) {
            $rules['subject'] = 'required';
        }

        if ($posttype->commentingFieldIsRequired('email')) {
            $rules['guest_email'] = 'required';
        }

        if ($posttype->commentingFieldIsRequired('mobile')) {
            $rules['guest_mobile'] = 'required';
        }

        if ($posttype->commentingFieldIsRequired('department')) {
            $rules['initial_department_id'] = 'required|';
        }

        return $rules;
    }



    /**
     * Returns a list of departments' IDs.
     *
     * @return array
     */
    protected function departmentIds()
    {
        return role()
             ->supportRoles()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Returns the posttype object.
     *
     * @return Posttype
     */
    protected function getPosttype()
    {
        return $this->model->posttype;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->mapFields();
        $this->correctAutomaticFields();
        $this->correctDepartment();
        $this->correctApprovementFields();
    }



    /**
     * Maps fields to some savable names.
     */
    protected function mapFields()
    {
        $map = [
             'name'       => 'guest_name',
             'email'      => 'guest_email',
             'mobile'     => 'guest_mobile',
             'department' => 'initial_department_id',
             'private'    => 'is_private',
        ];

        foreach ($map as $old => $new) {
            if (!isset($this->data[$old])) {
                continue;
            }

            $this->data[$new] = $this->data[$old];
            unset($this->data[$old]);
        }
    }



    /**
     * Corrects automatic fields.
     */
    protected function correctAutomaticFields()
    {
        $this->data['user_id']  = user()->id;
        $this->data['guest_ip'] = $this->ip();
    }



    /**
     * Corrects department fields.
     */
    protected function correctDepartment()
    {
        $posttype = $this->getPosttype();

        // If client can not specify the department.
        if ($posttype->commentingFieldIsDisabled('department')) {
            unset($this->data['initial_department_id']);
        }

        $current_department = ($this->data['initial_department_id'] ?? 0);

        // If a department has been specified.
        if ($current_department) {
            $this->data['current_department_id'] = $current_department;
            return;
        }


        // If the department field has been left empty.
        $default_department = $posttype->getCommentingInputValue('default_department');

        // If there is a default department available
        if ($default_department) {
            $this->data['initial_department_id'] = $default_department;
            $this->data['current_department_id'] = $default_department;
        }
    }



    /**
     * Corrects the approvement fields if needed.
     */
    protected function correctApprovementFields()
    {
        $auto_confirm = $this->getPosttype()->getCommentingInputValue('auto_confirm');

        if (!$auto_confirm) {
            return;
        }

        $this->data['approved_at'] = now();
        $this->data['approved_by'] = user()->id;
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'mobile'     => 'ed',
             'department' => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'text',
             'subject',
             'name',
             'email',
             'mobile',
             'department',
             'private',
        ];
    }



    /**
     * Returns an array of field which could not been saved.
     * <br>
     * This fields has been generated based on the posttype's config.
     *
     * @return array
     */
    public function overflowFields()
    {
        $posttype = $this->getPosttype();
        $fields   = [];

        if ($posttype->commentingFieldIsDisabled('subject')) {
            $fields[] = 'subject';
        }

        if ($posttype->commentingFieldIsDisabled('email')) {
            $fields[] = 'guest_email';
        }

        if ($posttype->commentingFieldIsDisabled('mobile')) {
            $fields[] = 'guest_mobile';
        }


        if (!$posttype->getCommentingInputValue('private_choice')) {
            $fields[] = 'is_private';
        }

        return $fields;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'guest_name'            => trans_safe('validation.attributes.name'),
             'guest_email'           => trans_safe('validation.attributes.email'),
             'guest_mobile'          => trans_safe('validation.attributes.mobile'),
             'initial_department_id' => trans_safe('validation.attributes.department'),
             'is_private'            => $this->runningModule()->getTrans('validation.attributes.private'),
        ];
    }
}
