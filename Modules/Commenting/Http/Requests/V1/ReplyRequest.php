<?php

namespace Modules\Commenting\Http\Requests\V1;

use App\Models\Posttype;
use Modules\Yasna\Services\YasnaRequest;


class ReplyRequest extends YasnaRequest
{
    protected $responder  = 'white-house';
    protected $model_name = "comment";



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'hashid',
             'subject',
             'text',
             'guest_name',
             'guest_email',
             'guest_mobile',
             'department',
             'is_private',
        ];
    }



    /**
     * @inheritdoc
     */
    public function adminsRules()
    {
        return [
             'subject'               => 'required',
             'text'                  => 'required',
             'guest_email'           => 'email',
             'guest_mobile'          => 'phone:mobile',
             'initial_department_id' => 'in:' . implode(',', $this->departmentIds()),
             'is_private'            => 'boolean',
        ];
    }



    /**
     * @inheritdoc
     */
    public function clientsRules()
    {
        if (!user()->exists) {
            return [
                 'guest_email'  => 'required',
                 'guest_mobile' => 'required',
                 'guest_name'   => 'required',
            ];
        } else {
            return [];
        }
    }



    /**
     * Returns a list of departments' IDs.
     *
     * @return array
     */
    protected function departmentIds()
    {
        return role()
             ->supportRoles()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return [
             user()->isAdmin()
             or
             $this->clientCanComment(),
        ];
    }



    /**
     * Whether current client can reply.
     *
     * @return bool
     */
    protected function clientCanComment()
    {
        return (
             user()->exists
             or
             $this->getPosttype()->getCommentingInputValue('users_can_reply')
        );
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->mapFields();
        $this->correctAutomaticFields();
        $this->correctDepartment();
        $this->correctApprovementFields();
    }



    /**
     * Corrects automatic fields.
     */
    protected function correctAutomaticFields()
    {
        $this->data['user_id']     = user()->id;
        $this->data['guest_ip']    = $this->ip();
        $this->data['parent_id']   = $this->model->id;
        $this->data['post_id']     = $this->model->post_id;
        $this->data['posttype_id'] = $this->model->posttype_id;
        $this->data['master_id']   = $this->model->master_id;
        if ($this->model->isFromAdmin()) {
            $this->data['is_from_admin'] = true;
        }
    }



    /**
     * Corrects department fields.
     */
    protected function correctDepartment()
    {
        $posttype = $this->getPosttype();

        // If client can not specify the department.
        if ($posttype->commentingFieldIsDisabled('department')) {
            unset($this->data['initial_department_id']);
        }

        $current_department = ($this->data['initial_department_id'] ?? 0);

        // If a department has been specified.
        if ($current_department) {
            $this->data['current_department_id'] = $current_department;
            return;
        }


        // If the department field has been left empty.
        $default_department = $posttype->getCommentingInputValue('default_department');

        // If there is a default department available
        if ($default_department) {
            $this->data['initial_department_id'] = $default_department;
            $this->data['current_department_id'] = $default_department;
        }
    }



    /**
     * Returns the posttype object.
     *
     * @return Posttype
     */
    protected function getPosttype()
    {
        return $this->model->posttype;
    }



    /**
     * Corrects the approvement fields if needed.
     */
    protected function correctApprovementFields()
    {
        $auto_confirm = $this->getPosttype()->getCommentingInputValue('auto_confirm');

        if (!$auto_confirm) {
            return;
        }

        $this->data['approved_at'] = now();
        $this->data['approved_by'] = user()->id;
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'mobile'     => 'ed',
             'department' => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'guest_name'            => trans_safe('validation.attributes.name'),
             'guest_email'           => trans_safe('validation.attributes.email'),
             'guest_mobile'          => trans_safe('validation.attributes.mobile'),
             'initial_department_id' => trans_safe('validation.attributes.department'),
             'is_private'            => $this->runningModule()->getTrans('validation.attributes.private'),
        ];
    }



    /**
     * Maps fields to some savable names.
     */
    protected function mapFields()
    {
        $map = [
             'department' => 'initial_department_id',
        ];

        foreach ($map as $old => $new) {
            if (!isset($this->data[$old])) {
                continue;
            }

            $this->data[$new] = $this->data[$old];
            unset($this->data[$old]);
        }
    }
}
