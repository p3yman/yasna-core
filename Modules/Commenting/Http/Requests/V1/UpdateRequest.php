<?php

namespace Modules\Commenting\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class UpdateRequest extends YasnaRequest
{
    protected $responder  = 'white-house';
    protected $model_name = "comment";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'guest_mobile' => "phone:mobile",
             'guest_email'  => "email",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'hashid',
             'user_id',
             'post_id',
             'posttype_id',
             'parent_id',
             'is_private',
             'guest_name',
             'guest_email',
             'guest_web',
             'guest_mobile',
             'text',
             'subject',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (auth()->guest() or !$this->entitiesExist()) {
            return false;
        }

        return $this->model->canEdit();

    }



    /**
     * checks that the necessary entities exist
     *
     * @return boolean
     */
    public function entitiesExist()
    {
        if ($this->getData('user_id')) {
            $user_id = $this->getData('user_id');

            return model('user', $user_id)->exists;
        }

        if ($this->getData('parent_id')) {
            $parent_id = $this->getData('parent_id');

            return model('comment', $parent_id)->exists;
        }

        if ($this->getData('posttype_id')) {
            $posttype_id = $this->getData('posttype_id');

            return model('posttype', $posttype_id)->exists;
        }

        if ($this->getData('post_id')) {
            $post_id = $this->getData('post_id');

            return model('post', $post_id)->exists;
        }

        return true;
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'user_id'     => 'dehash',
             'parent_id'   => 'dehash',
             'posttype_id' => 'dehash',
             'post_id'     => 'dehash',
             'mobile'      => 'ed',
        ];
    }
}
