<?php

namespace Modules\Commenting\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CommentSaveSpamRequest extends YasnaRequest
{
    protected $model_name = "Comment" ;

    public function authorize()
    {
        return $this->model->can('process') ;
    }
}
