<?php

namespace Modules\Commenting\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class SaveRequest extends YasnaRequest
{
    protected $model_name = "";
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.
}
