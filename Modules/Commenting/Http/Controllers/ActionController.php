<?php namespace Modules\Commenting\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Commenting\Events\CommentReplied;
use Modules\Commenting\Http\Requests\CommentSaveActivenessRequest;
use Modules\Commenting\Http\Requests\CommentSaveReplyRequest;
use Modules\Commenting\Http\Requests\CommentSaveSpamRequest;
use Modules\Yasna\Services\YasnaController;

class ActionController extends YasnaController
{
    protected $base_model  = "Comment";
    protected $view_folder = "commenting::action";


    public function saveActiveness(CommentSaveActivenessRequest $request)
    {
        if ($request->_submit == 'undelete') {
            $ok = $request->model->undelete();
        } else {
            $ok = $request->model->delete();
        }

        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "rowUpdate('tblComments','$request->hashid')",
        ]);
    }

    public function saveSpam(CommentSaveSpamRequest $request)
    {
        if ($request->_submit == 'not_spam') {
            $ok = $request->model->markAsNotSpam();
        } else {
            $ok = $request->model->markAsSpam();
        }

        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "rowUpdate('tblComments','$request->hashid')",
        ]);
    }

    public function saveReply(CommentSaveReplyRequest $request)
    {
        $model = $request->model;

        $saved = model('comment')->batchSave([
            'user_id'               => user()->id,
            'post_id'               => $model->post_id,
            'posttype_id'           => $model->posttype_id,
            'parent_id'             => $model->id,
            'master_id'             => $model->master->id,
            'is_from_admin'         => 1,
            'text'                  => $request->text,
            'new_status'            => $request->new_status,
            'new_department'        => $request->new_department,
        ]);

        $ok = $saved->exists;

        if ($ok) {
            event(new CommentReplied($saved->fresh()));
        }

        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback' => "rowUpdate('tblComments','$request->id')",
        ]);
    }
}
