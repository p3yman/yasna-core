<?php namespace Modules\Commenting\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class BrowseController extends YasnaController
{
    protected $view_folder = "commenting::browse";
    protected $base_model  = "Comment";
    private $request_type;
    private $request_tab;
    private $switches;
    private $department;
    private $posttype;
    private $support_role;

    public function index($type, $request_tab = 'pending', $department = 'default', $switches = null)
    {
        $this->request_type = $type;
        $this->request_tab  = $request_tab;
        $this->switches     = $switches;
        $this->department   = $department;

        /*-----------------------------------------------
        | Check Permission ...
        */
        if (!$this->authorize()) {
            return $this->abort(403); //TODO: Might be neglected, since it is already checked in tab generations
        }

        /*-----------------------------------------------
        | Posttype ...
        */
        $posttype = $this->posttype();
        if (!$posttype) {
            return $this->abort(410);
        }

        /*-----------------------------------------------
        | Department ...
        */
        $department = $this->department();
        if (!$department) {
            return $this->abort(410);
        }

        /*-----------------------------------------------
        | Tabs ...
        */
        $tabs = $this->tabs();
        if (!array_has($tabs, $request_tab)) {
            return $this->abort(404);
        }

        /*-----------------------------------------------
        | Page ...
        */
        $page = $this->page();

        /*-----------------------------------------------
        | Model ...
        */
        $models = $this->model()->elector([
            'criteria'   => $this->request_tab,
            'department' => $department != 'all' ? $department : null,
            'posttype'   => $posttype->id ? $posttype->id : null,
            'parent_id'  => 0,
        ])
            ->orderBy('created_at', 'desc')
            ->paginate(user()->preference('max_rows_per_page'))
        ;


        /*-----------------------------------------------
        | View ...
        */
        $current_page = "$type/$request_tab/$department";
        return $this->view('index', compact('page', 'request_tab', 'department', 'models', 'tabs', 'current_page'));
    }

    private function authorize()
    {
        return $this->model()->hasManagePermission($this->request_type, $this->request_tab);
    }

    private function posttype()
    {
        if ($this->request_type == 'all') {
            return model('posttype');
        }

        $posttype = posttype($this->request_type);
        if ($posttype and $posttype->exists) {
            return $this->posttype = $posttype;
        }

        return false;
    }

    private function department()
    {
        if ($this->department == 'default') {
            $this->department = $this->posttype->spreadMeta()->default_department;
        }

        if (!$this->department or $this->department = 'all') {
            return $this->department = 'all';
        }

        $model = model('role')->findDepartment($this->department);
        if ($model and $model->exists) {
            $this->support_role = $model;
            return $this->department;
        }

        return false;
    }

    private function tabs()
    {
        $array  = ['all', 'pending', 'archive', 'bin', 'spam'];
        $result = [];

        foreach ($array as $item) {
            if ($this->model()->hasManagePermission($this->request_type, $item)) {
                $result[ $item ] = [
                    'url'         => "$this->request_type/$item/$this->department",
                    'caption'     => trans_safe("commenting::status.$item"),
                    'badge_count' => null,
                ];
            }
        }

        return $result;
    }

    private function page()
    {
        $result[0] = ['comments/browse', trans_safe("commenting::phrases.plural")];
        $result[1] = [$this->posttype->slug, $this->posttype->title];
        $result[2] = [$this->request_tab, trans_safe("commenting::status." . $this->request_tab)];

        if ($this->support_role) {
            $result[3] = [$this->department, $this->support_role->title];
        }

        return $result;
    }
}
