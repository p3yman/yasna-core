<?php

namespace Modules\Commenting\Http\Controllers\V1;

use App\Models\Comment;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Commenting\Http\Requests\V1\PostCommentListRequest;
use Modules\Commenting\Http\Requests\V1\PostCommentSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class PostCommentsController extends YasnaApiController
{
    /**
     * The Default Query Limit
     */
    const DEFAULT_LIMIT = 15;

    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = 'comment';

    /**
     * The Query Builder Object in Use
     *
     * @var Builder
     */
    protected $builder;

    /**
     * The Request Object in User
     *
     * @var PostCommentListRequest
     */
    protected $request;



    /**
     * Returns the list of comments of the specified post, with the specified filters, and in the white-house format.
     *
     * @param PostCommentListRequest $request
     *
     * @return array
     */
    public function list(PostCommentListRequest $request)
    {
        $this->request = $request;

        $this->generateListBuilder();

        if ($request->model->posttype->getMeta('commenting_display') === "") {
            $this->builder->whereNull('id');
        }


        return $this->getResourcesFromBuilder($this->builder, $request);
    }



    /**
     * List All Together
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listFlat()
    {
        $builder = $this->builder;

        $meta = [
             "total" => $builder->count(),
             'post'  => $this->request->model->hashid,
        ];

        $data = $this->mapComments($builder->get());

        return $this->success($data, $meta);
    }



    /**
     * List by Pagination
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listPaginated()
    {
        $builder   = $this->builder;
        $request   = $this->request;
        $paginator = $builder->paginate($request->perPage());
        $result    = $this->mapComments($paginator);
        $meta      = $this->paginatorMetadata($paginator);

        return $this->success($result, $meta);
    }



    /**
     * Returns the comments to be used in the list action.
     *
     * @return void
     */
    protected function generateListBuilder()
    {
        $this->createListBuilder();
        $this->applyQueryRepliesCondition();
    }



    /**
     * Maps a list of comments to a proper format.
     *
     * @param Collection|LengthAwarePaginator $comments
     *
     * @return array
     */
    protected function mapComments($comments)
    {
        return $comments->map(function (Comment $comment) {
            $data = $comment->toListResource();

            $data['replies'] = $this->getCommentChildren($comment);

            return $data;
        })->toArray()
             ;
    }



    /**
     * Returns the children of the specified comment in a proper format.
     *
     * @param Comment $comment
     *
     * @return array
     */
    protected function getCommentChildren(Comment $comment)
    {
        $limit = ($this->request->replies_limit ?: static::DEFAULT_LIMIT);

        $children = $comment
             ->children()
             ->withCriteria($this->guessCriteriaFromRequest())
             ->where('approved_by', '<>', $this->getRefactorRepliesParam())
             ->where('is_private', '<>', $this->getRefactorRepliesParam())
             ->limit($limit)
             ->where('text', '<>', '')
             ->orderBy(...$this->guessOrderParametersFromRequest())
             ->get()
        ;


        return $this->mapComments($children);
    }



    /**
     * Guesses the parameters to the be used in the `orderBy()` method of a query builder based on the request in use.
     *
     * @return array
     */
    protected function guessOrderParametersFromRequest()
    {
        return [
             'created_at',
             ($this->request->sort ?: 'desc'),
        ];
    }



    /**
     * Guesses the criteria to be used while selecting comments
     * based on the request in use.
     *
     * @return string|null
     */
    protected function guessCriteriaFromRequest()
    {
        return ($this->request->status ?: null);
    }



    /**
     * Creates the builder in use for the list action.
     */
    protected function createListBuilder()
    {
        $this->builder = model($this->model_name)
             ->elector($this->generateListElectorData())
             ->orderBy(...$this->guessOrderParametersFromRequest())
        ;
    }



    /**
     * Generates the elector data for the list action.
     *
     * @return array
     */
    protected function generateListElectorData()
    {
        $request = $this->request;

        return [
             'post'               => $request->model->id,
             'parent'             => 0,
             'criteria'           => $this->guessCriteriaFromRequest(),
             'current_department' => ($request->department ?: null),
             'user'               => $request->user,
             'guest_name'         => $request->guest_name,
             'master'             => $request->master,
             'initial_department' => $request->initial_department,
             'from_admin'         => $request->from_admin,
             'private'            => $request->private ?? $this->getParamRefactor(),
             'guest_email'        => $request->guest_email,
             'guest_mobile'       => $request->guest_mobile,
             'guest_web'          => $request->guest_web,
             'subject'            => $request->subject,
             'text'               => $request->text,
             'created_at'         => $request->created_at,
             'deleted_at'         => $request->deleted_at,
             'updated_at'         => $request->updated_at,
             'approved'           => 0,
        ];
    }



    /**
     * Applies the `with_replies` and `replies_number` fields form the request in use.
     */
    protected function applyQueryRepliesCondition()
    {
        $request = $this->request;
        if (!$request->with_replies) {
            return;
        }

        $children_count = ($request->replies_number ?: 0);

        $this
             ->builder
             ->with('children')
             ->has('children', '>', $children_count)
        ;
    }



    /**
     * Saves a comment on a post.
     *
     * @param PostCommentSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(PostCommentSaveRequest $request)
    {
        $saved_model = $request->model->batchSaveComment(
             $request,
             $request->overflowFields()
        );

        return $this->modelSaveFeedback($saved_model);
    }



    /**
     * specifies the private field if it does not exist
     *
     * @return int|null
     */
    public function getParamRefactor()
    {
        if (user()->cannot('commenting')) {
            return 1;
        }
        return null;
    }



    /**
     * Changes the parameter by checking the type of user
     *
     * @return int|null
     */
    protected function getRefactorRepliesParam()
    {
        if (user()->cannot('commenting')) {
            return 0;
        }
        return null;
    }
}
