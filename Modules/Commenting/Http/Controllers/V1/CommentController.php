<?php

namespace Modules\Commenting\Http\Controllers\V1;

use Illuminate\Database\Eloquent\Collection;
use Modules\Commenting\Http\Requests\ListRequest;
use Modules\Commenting\Http\Requests\V1\ReplyRequest;
use Modules\Commenting\Http\Requests\V1\UpdateRequest;
use Modules\Yasna\Services\YasnaApiController;

class CommentController extends YasnaApiController
{

    /**
     * list of all comments
     *
     * @param ListRequest $request
     *
     * @return array
     */
    public function list(ListRequest $request)
    {

        $elector = $this->getElectorArray($request);
        $query   = model("comment")->elector($elector);

        $results = $query->get();

        return $this->success($this->mapOfComments($results));

    }



    /**
     * reply a comment
     *
     * @param ReplyRequest $request
     *
     * @return \Modules\Yasna\Services\YasnaModel
     */
    public function reply(ReplyRequest $request)
    {
        return model('comment')->batchSave($request, ['hashid']);
    }



    /**
     * edit and update comments
     *
     * @param UpdateRequest $request
     *
     * @return array
     */
    public function update(UpdateRequest $request)
    {
        $result = $request->model->batchSave($request);

        return $this->success($result->toArray());
    }



    /**
     * get elector array required for the query builder
     *
     * @param ListRequest $request
     *
     * @return array
     */
    private function getElectorArray(ListRequest $request)
    {
        return [
             "posttypes" => $request->type,
        ];

    }



    /**
     * Return array of comments list
     *
     * @param Collection $comments
     *
     * @return array
     */
    private function mapOfComments($comments): array
    {
        return $comments->map(function ($comment) {
            return $comment->toListResource();
        })->toArray()
             ;
    }
}
