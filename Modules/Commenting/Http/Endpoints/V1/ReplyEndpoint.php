<?php

namespace Modules\Commenting\Http\Endpoints\V1;

use Modules\Commenting\Http\Controllers\V1\CommentController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/commenting-reply
 *                    create a reply for a comment
 * @apiDescription    create a reply for a comment
 * @apiVersion        1.0.0
 * @apiName           create a reply for a comment
 * @apiGroup          Commenting
 * @apiPermission     None
 * @apiParam {String} hashid    Hashid of comment
 * @apiParam {String} subject   Subject for comment
 * @apiParam {String} text      text For comment
 * @apiParam {String} [guest_name]      Guest's name for comment
 * @apiParam {String} [guest_email]     Guest's email for comment
 * @apiParam {String} [guest_mobile]    Guest's mobile for comment
 * @apiParam {String} [department]      Hashid of department for comment
 * @apiParam {boolean} [is_private]     If true= the comment is private
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      },
 *      "results": {
 *              "id": 16,
 *              "user_id": 2,
 *              "post_id": 1,
 *              "posttype_id": 2,
 *              "parent_id": 3,
 *              "master_id": 0,
 *              "initial_department_id": 0,
 *              "current_department_id": 0,
 *              "is_private": false,
 *              "is_from_admin": false,
 *              "guest_name": null,
 *              "guest_email": null,
 *              "guest_mobile": null,
 *              "guest_web": null,
 *              "guest_ip": "127.0.0.1",
 *              "subject": "test subject4444",
 *              "text": "text44444",
 *              "meta": [],
 *              "created_at": "2019-02-04 10:22:18",
 *              "updated_at": "2019-02-04 10:22:18",
 *              "deleted_at": null,
 *              "approved_at": null,
 *              "spammed_at": null,
 *              "created_by": 2,
 *              "updated_by": 0,
 *              "deleted_by": 0,
 *              "approved_by": 0,
 *              "spammed_by": 0
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  CommentController controller()
 */
class ReplyEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Create A Reply For A Comment";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Commenting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CommentController@reply';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return [
             "id"                    => 16,
             "user_id"               => 2,
             "post_id"               => 1,
             "posttype_id"           => 2,
             "parent_id"             => 3,
             "master_id"             => 0,
             "initial_department_id" => 0,
             "current_department_id" => 0,
             "is_private"            => false,
             "is_from_admin"         => false,
             "guest_name"            => null,
             "guest_email"           => null,
             "guest_mobile"          => null,
             "guest_web"             => null,
             "guest_ip"              => "127.0.0.1",
             "subject"               => "test subject4444",
             "text"                  => "text44444",
             "meta"                  => [],
             "created_at"            => "2019-02-04 10:22:18",
             "updated_at"            => "2019-02-04 10:22:18",
             "deleted_at"            => null,
             "approved_at"           => null,
             "spammed_at"            => null,
             "created_by"            => 2,
             "updated_by"            => 0,
             "deleted_by"            => 0,
             "approved_by"           => 0,
             "spammed_by"            => 0,
        ];
    }
}
