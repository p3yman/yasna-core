<?php

namespace Modules\Commenting\Http\Endpoints\V1;

use Modules\Commenting\Http\Controllers\V1\CommentController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/commenting-update
 *                    Create and edit comments
 * @apiDescription    Create and edit comments
 * @apiVersion        1.0.0
 * @apiName           Create and edit comments
 * @apiGroup          Commenting
 * @apiPermission     User
 * @apiParam {String} hashid      hashid of comment
 * @apiParam {String} [text]      content of comment
 * @apiParam {String} [subject]   subject of comment
 * @apiParam {string} [user_id]      hashid of comment
 * @apiParam {string} [post_id]      hashid of post
 * @apiParam {string} [guest_name]   name of guest
 * @apiParam {string} [guest_email]  email of guest
 * @apiParam {string} [guest_mobile] mobile of guest
 * @apiParam {string} [guest_web]    web address of guest
 * @apiParam {int=0,1} [is_private] comment is private or not (boolean)
 * @apiParam {string} [parent_id]      hashid of comment's parent
 * @apiParam {string} [posttype_id]    hashid of posttype
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "id": 41,
 *          "user_id": 1,
 *          "post_id": 1,
 *          "posttype_id": 4,
 *          "parent_id": 0,
 *          "master_id": 0,
 *          "initial_department_id": 0,
 *          "current_department_id": 0,
 *          "is_private": false,
 *          "is_from_admin": true,
 *          "guest_name": "Mostafa Norzade",
 *          "guest_email": "admin@admin.com",
 *          "guest_mobile": "09364588133",
 *          "guest_web": "mosssstaf.com",
 *          "guest_ip": "127.0.0.1",
 *          "subject": "jobs-apply-form-subject",
 *          "text": "This is my text",
 *          "meta": {
 *              "resume": "1550989333-govahi.doc",
 *              "gender": "f",
 *              "marriage": "single",
 *              "bd": "1367",
 *              "address": "Resalat ST .",
 *              "tel": "88745896",
 *              "insurance": "1",
 *              "experience": "1",
 *              "edu_degree": "PHD",
 *              "edu_field": "Information Technology",
 *              "english[reading]": "1",
 *              "english[writing]": "3",
 *              "english[speaking]": "4",
 *              "english[degree]": "1",
 *              "salary": "12 million dollars",
 *              "available_date": "Tomorrow"
 *          },
 *          "created_at": "2019-02-24 09:52:13",
 *          "updated_at": "2019-02-24 17:35:08",
 *          "deleted_at": null,
 *          "approved_at": null,
 *          "spammed_at": null,
 *          "created_by": 1,
 *          "updated_by": 1,
 *          "deleted_by": 0,
 *          "approved_by": 0,
 *          "spammed_by": 0,
 *          "posttype": {
 *              "id": 4,
 *              "slug": "quantity",
 *              "title": "Tehran",
 *              "singular_title": "Tehran",
 *              "icon": "bug",
 *              "order": 11,
 *              "locales": "fa,en,ar,fr",
 *              "template": "album",
 *              "header_title": "",
 *              "optional_meta": null,
 *              "meta": {
 *                  "cached_features": [
 *                  "text",
 *                  "single_view",
 *                  "searchable",
 *                  "slug",
 *                  "manage_sidebar",
 *                  "locales",
 *                  "title2",
 *                  "abstract",
 *                  "commenting"
 *                  ],
 *              },
 *          "created_at": "2019-02-02 16:03:25",
 *          "updated_at": "2019-02-02 16:57:16",
 *          "deleted_at": null,
 *          "converted": 0,
 *          "created_by": 0,
 *          "updated_by": 1,
 *          "deleted_by": 0
 *          }
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  CommentController controller()
 */
class UpdateEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Create And Edit Comments";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Commenting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CommentController@update';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"                    => 41,
             "user_id"               => 1,
             "post_id"               => 1,
             "posttype_id"           => 4,
             "parent_id"             => 0,
             "master_id"             => 0,
             "initial_department_id" => 0,
             "current_department_id" => 0,
             "is_private"            => false,
             "is_from_admin"         => true,
             "guest_name"            => "Mostafa Norzade",
             "guest_email"           => "admin@admin.com",
             "guest_mobile"          => "09364588133",
             "guest_web"             => "mosssstaf.com",
             "guest_ip"              => "127.0.0.1",
             "subject"               => "jobs-apply-form-subject",
             "text"                  => "This is my text",
             "meta"                  => [
                  "resume"            => "1550989333-govahi.doc",
                  "gender"            => "f",
                  "marriage"          => "single",
                  "bd"                => "1367",
                  "address"           => "Resalat ST .",
                  "tel"               => "88745896",
                  "insurance"         => "1",
                  "experience"        => "1",
                  "edu_degree"        => "PHD",
                  "edu_field"         => "Information Technology",
                  "english[reading]"  => "1",
                  "english[writing]"  => "3",
                  "english[speaking]" => "4",
                  "english[degree]"   => "1",
                  "salary"            => "12 million dollars",
                  "available_date"    => "Tomorrow",
             ],
             "created_at"            => "2019-02-24 09:52:13",
             "updated_at"            => "2019-02-24 17:35:08",
             "deleted_at"            => null,
             "approved_at"           => null,
             "spammed_at"            => null,
             "created_by"            => 1,
             "updated_by"            => 1,
             "deleted_by"            => 0,
             "approved_by"           => 0,
             "spammed_by"            => 0,
             "posttype"              => [
                  "id"             => 4,
                  "slug"           => "quantity",
                  "title"          => "Tehran",
                  "singular_title" => "Tehran",
                  "icon"           => "bug",
                  "order"          => 11,
                  "locales"        => "fa,en,ar,fr",
                  "template"       => "album",
                  "header_title"   => "",
                  "optional_meta"  => null,
             ],
        ], [
             "authenticated_user" => "qKXVA",
        ]);
    }
}
