<?php

namespace Modules\Commenting\Http\Endpoints\V1;

use Modules\Commenting\Http\Controllers\V1\PostCommentsController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/commenting-post-comments
 *                    Post Comments
 * @apiDescription    Endpoint for reading comments of a post.
 * @apiVersion        1.0.1
 * @apiName           Post Comments
 * @apiGroup          Commenting
 * @apiPermission     None
 * @apiParam {String}   hashid The hashid of the post
 * @apiParam {String}   [user] The hashid of the user
 * @apiParam {String}   [current_department] The id of the department
 * @apiParam {String}   [guest_name] guest name
 * @apiParam {String}   [guest_email] guest email
 * @apiParam {String}   [guest_mobile] guest mobile
 * @apiParam {String}   [guest_web] guest web
 * @apiParam {String}   [subject] subject of comment
 * @apiParam {String}   [text] text of comment
 * @apiParam {int}      [master] master id
 * @apiParam {int}      [initial_department] initial department id
 * @apiParam {int=0,1}  [from_admin] is from admin or not
 * @apiParam {int=0,1}  [private] private id
 * @apiParam {int}      [created_at] created date
 * @apiParam {int}      [deleted_at] deleted date
 * @apiParam {int}      [updated_at] updated date
 * @apiParam {String=pending,approved,archive,spam,bin} [status] The status of the comments to be shown
 * @apiParam {String} [department] The hashid of a department (support role)
 * @apiParam {Boolean} [with_replies] Whether the returning comments should have replies or not.
 * @apiParam {Integer} [replies_number] The minimum numbers of replies of the comments in the result.
 * @apiParam {Integer} [replies_limit=15] The limit of the replies in the result
 * @apiParam {String=asc,desc} [sort=desc] The Sort Type
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {String} [per_page=15] Indicate the number of results on per page. (available only with `paginated=1`).
 * @apiParam {String} [page=1] Page number of returned result (available only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "total": "2"
 *           "post": "PKEnN"
 *      },
 *      "results": [
 *           {
 *                "id": "PKEnN",
 *                "subject": "Subject",
 *                "text": "Text",
 *                "status": "pending",
 *                "created_at": 1544524504,
 *                "updated_at": 1544524504,
 *                "approved_at": null,
 *                "spammed_at": null,
 *                "deleted_at": null,
 *                "user": {
 *                     "username": null,
 *                     "name": "Jane Doe",
 *                     "avatar": null
 *                }
 *           },
 *           {
 *                "id": "PKEnN",
 *                "subject": "Subject",
 *                "text": "Text",
 *                "status": "pending",
 *                "created_at": 1544524504,
 *                "updated_at": 1544524504,
 *                "approved_at": null,
 *                "spammed_at": null,
 *                "deleted_at": null,
 *                "user": {
 *                     "username": null,
 *                     "name": "Jane Doe",
 *                     "avatar": null
 *                }
 *           },
 *      ]
 * }
 * @apiErrorExample
 * @apiErrorExample   Unauthenticated-Request:
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  PostCommentsController controller()
 */
class PostCommentsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Post Comments";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Commenting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostCommentsController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $model = model('comment');
        $model->fill([
             'subject'    => 'Subject',
             'text'       => 'Text',
             'guest_name' => 'Jane Doe',
             'created_at' => now()->toDateTimeString(),
             'updated_at' => now()->toDateTimeString(),
        ]);

        return api()->successRespond([
             $model->toListResource(),
             $model->toListResource(),
        ], [
             "total" => 2,
             "post"  => hashid(0),
        ]);
    }
}
