<?php

namespace Modules\Commenting\Http\Endpoints\V1;

use Modules\Commenting\Http\Controllers\V1\CommentController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/commenting-list
 *                    list of all posttype's comments
 * @apiDescription    list of all posttype's comments
 * @apiVersion        1.0.0
 * @apiName           list of all posttype's comments
 * @apiGroup          Commenting
 * @apiPermission     Admin
 * @apiParam {String} type      slug of posttype
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "id": "qKXVA",
 *          "subject": "Comment Title",
 *          "text": "This is comment content .",
 *          "status": "pending",
 *          "created_at": null,
 *          "updated_at": 1546950524,
 *          "approved_at": null,
 *          "spammed_at": null,
 *          "deleted_at": null,
 *          "user": {
 *              "username": "Admin",
 *              "name": "Admin",
 *              "avatar": null
 *          }
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  CommentController controller()
 */
class ListEndpoint extends EndpointAbstract
{


    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List Of All Posttype's Comments";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Commenting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CommentController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "authenticated_user" => "qKXVA",
        ], [
             "id"          => "qKXVA",
             "subject"     => "Comment Title",
             "text"        => "This is comment content",
             "status"      => "pending",
             "created_at"  => null,
             "updated_at"  => 1546950524,
             "approved_at" => null,
             "spammed_at"  => null,
             "deleted_at"  => null,
             "user"        => [
                  "username" => "Admin",
                  "name"     => "Admin",
                  "avatar"   => null,
             ],
        ]);
    }
}
