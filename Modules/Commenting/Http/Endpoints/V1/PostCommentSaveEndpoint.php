<?php

namespace Modules\Commenting\Http\Endpoints\V1;

use Modules\Commenting\Http\Controllers\V1\PostCommentsController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/commenting-post-comment-save
 *                    Post Comment Save
 * @apiDescription    Comment on a Post
 * @apiVersion        1.0.0
 * @apiName           Post Comment Save
 * @apiGroup          Commenting
 * @apiPermission     None
 * @apiParam {String} hashid The hashid of the post
 * @apiParam {String} text The text of the comment
 * @apiParam {String} name The name of the commenter
 * @apiParam {String} [subject] The subject of the comment. (Validation and saving depend on the posttype's config)
 * @apiParam {String} [email] The email of the client. (Validation and saving depend on the posttype's config)
 * @apiParam {String} [mobile] The mobile of the client. (Validation and saving depend on the posttype's config)
 * @apiParam {String} [department] The hashid of a department (support role). (Validation and saving depend on the
 *           posttype's config)
 * @apiParam {Boolean} [private=0] If the comment should be save privately.  (May not been saved based on the
 *           posttype's config.)
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Unauthenticated-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  PostCommentsController controller()
 */
class PostCommentSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Post Comment Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Commenting\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostCommentsController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
