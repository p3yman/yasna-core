<?php

/*
|--------------------------------------------------------------------------
| Manage Side
|--------------------------------------------------------------------------
|
*/
Route::group(
    [
        'middleware' => ['web', 'auth', 'is:admin', 'can:commenting'],
        'prefix'     => 'manage/comments/',
        'namespace'  => module('commenting')->controller(),
    ],
    function () {
        Route::get('/browse/{type}/{tab?}/{department?}', 'BrowseController@index');
        Route::get('/update/{hashid}', "BrowseController@update");
        //Route::get('/tab_update/{posttype}/{request_tab?}/{switches?}', 'PostsManageController@tabUpdate');

        Route::get('/act/{model_id}/{action}/{option?}', 'ActionController@singleAction');
        //Route::get('/editor-act/{model_id}/{action}/{option?}', 'PostsEditController@singleAction');
        //Route::get('/check_slug/{id}/{type}/{locale}/{slug?}/p', 'PostsEditController@checkSlug');
        //Route::get('/{posttype}/create/{locale?}/{sisterhood?}', 'PostsEditController@create');
        //Route::get('{posttype}/edit/{post_id}', 'PostsEditController@editor');
        //Route::get('{posttype}/{locale}/search', 'PostsManageController@search');

        Route::group(['prefix' => 'save'], function () {
            Route::post('/', 'ActionController@save')->name('comment-save');
            Route::post('/process', 'ActionController@saveReply')->name('comment-process');
            Route::post('/activeness', 'ActionController@saveActiveness')->name('comment-activeness');
            Route::post('/spam', 'ActionController@saveSpam')->name('comment-spam');
        });
    }
);
