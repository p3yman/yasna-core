<?php

return [
    'name' => 'Commenting',

    'status' => [
        'color' => [
            'pending'  => "warning",
            'trashed'  => "gray",
            'spam'     => "danger",
            'approved' => "success",
        ],

        'icon' => [
            'pending'  => "hourglass-half",
            'trashed'  => "times",
            'spam'     => "ban",
            'approved' => "check",
        ],
    ],
];
