<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0)->index();
            $table->unsignedInteger('post_id')->index();
            $table->unsignedInteger('posttype_id')->index();
            $table->unsignedInteger('parent_id')->index();
            $table->unsignedInteger('master_id')->index(); // <~~ grand grand grand parent :)

            $table->unsignedInteger('initial_department_id')->index();
            $table->unsignedInteger('current_department_id')->index();

            $table->boolean('is_private')->default(0);
            $table->boolean('is_from_admin')->default(0);

            $table->string('guest_name', 200)->nullable()->index();
            $table->string('guest_email', 200)->nullable()->index();
            $table->string('guest_mobile', 200)->nullable()->index();
            $table->string('guest_web', 200)->nullable()->index();
            $table->string('guest_ip', 200)->nullable()->index();

            $table->string('subject')->nullable()->index();
            $table->longText('text')->nullable();
            $table->longText('meta')->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->timestamp('approved_at')->nullable()->index();
            $table->timestamp('spammed_at')->nullable()->index();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);
            $table->unsignedInteger('approved_by')->default(0);
            $table->unsignedInteger('spammed_by')->default(0);

            $table->index('created_at');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
