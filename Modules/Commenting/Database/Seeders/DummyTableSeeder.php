<?php

namespace Modules\Commenting\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\DummyServiceProvider;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->fillCommentsTable(50);
    }

    public function fillCommentsTable($count = 100)
    {
        for ($i = 1; $i <= $count; $i++) {
            $data        = [];
            $user        = rand(0, 1) ? model('user')->inRandomOrder()->first() : model('user');
            $an_admin    = model('user')->inRandomOrder()->first();
            $post        = model('post')->inRandomOrder()->first();
            $master_id   = !rand(0, 5) ? model('comment')->inRandomOrder()->first()->id : 0;
            $department  = array_random(model('role')->supportRoles()->pluck('id')->toArray());
            $created_at  = Carbon::now()->subDays(rand(10, 40));
            $approved_at = rand(0, 1) ? $created_at->addDays(rand(1, 10)) : null;
            $spammed_at  = !rand(0, 9) ? $created_at : null;
            $deleted_at  = !rand(0, 9) ? $created_at : null;

            $data[] = [
                'user_id'               => $user->id,
                'post_id'               => $post->id,
                'posttype_id'           => $post->posttype->id,
                'master_id'             => $master_id,
                'parent_id'             => $master_id,
                'initial_department_id' => rand(0, 3) ? $department : 0,
                'current_department_id' => rand(0, 1) ? $department : 0,
                'is_private'            => rand(0, 10) ? false : true,
                'is_from_admin'         => $user->is_admin(),
                'guest_name'            => $user->exists ? null : DummyServiceProvider::persianName(),
                'guest_email'           => $user->exists ? null : DummyServiceProvider::email(),
                'guest_mobile'          => $user->exists ? null : DummyServiceProvider::mobile(),
                'guest_web'             => $user->exists ? null : DummyServiceProvider::url(),
                'guest_ip'              => $user->exists ? null : DummyServiceProvider::ip(),
                'subject'               => rand(0, 1) ? null : DummyServiceProvider::persianWord(2),
                'text'                  => DummyServiceProvider::persianText(),
                'created_at'            => $created_at,
                'approved_at'           => $approved_at,
                'approved_by'           => $approved_at ? $an_admin->id : 0,
                'spammed_at'            => $spammed_at,
                'spammed_by'            => $spammed_at ? $an_admin->id : 0,
                'deleted_at'            => $deleted_at,
                'deleted_by'            => $deleted_at ? $an_admin->id : 0,
            ];

            yasna()->seed('comments', $data);
        }
    }
}
