<?php

namespace Modules\Commenting\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Cart\Database\Seeders\SettingsTableSeeder;

class CommentingDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(InputsTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
    }
}
