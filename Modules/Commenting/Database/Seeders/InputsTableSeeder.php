<?php

namespace Modules\Commenting\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InputsTableSeeder extends Seeder
{
    private $data  = [];
    private $order = 70;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('inputs', $this->fillData());
    }

    public function fillData()
    {
        $this->addData('receive', 1);
        $this->addData('display', 0);
        $this->addData('intro', null, 'textarea');
        $this->addData('allow_guests', 1);
        $this->addData('auto_confirm');
        $this->addData('default_department', null, 'text');
        $this->addData('enter_department', 'disabled', 'combo');
        $this->addData('enter_subject', 'disabled', 'combo');
        $this->addData('enter_email', 'can', 'combo');
        $this->addData('enter_mobile', 'disabled', 'combo');
        $this->addData('enter_file', 'disabled', 'combo');
        $this->addData('private_choice');
        $this->addData('users_can_reply');
        $this->addData('email_notifications');
        $this->addData('sms_notifications');

        return $this->data;
    }

    public function addData($slug, $default = null, $type = 'boolean', $css = null, $validation = null, $purifier = null)
    {
        $this->data[] = [
            'slug'              => "commenting_$slug",
            'title'             => trans_safe("commenting::settings.$slug"),
            'order'             => $this->order++,
            'type'              => 'upstream',
            'data_type'         => $type,
            'css_class'         => $css,
            'validation_rules'  => $validation,
            'purifier_rules'    => $purifier,
            'default_value'     => $default,
            'default_value_set' => "1",
            'hint'              => trans_safe("commenting::settings.hint.$slug"),
            'meta-options'      => $type == 'combo' ? 'commenting::getConfigCombo' : '',
        ];
    }
}
