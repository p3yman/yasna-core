<?php

namespace Modules\Commenting\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->data());
        $this->relationships();
    }



    /**
     * get the seeding data
     *
     * @return array
     */
    public function data()
    {
        return [
             [
                  'slug'      => 'commenting',
                  'title'     => 'ثبت و دریافت دیدگاه کاربران',
                  'order'     => '11',
                  'icon'      => 'comment-o',
                  'post_meta' => '',
                  'meta-hint' => '',
             ],
        ];
    }



    /**
     * attach relationships
     */
    public function relationships()
    {
        $array = [
             'commenting_receive',
             'commenting_display',
             'commenting_intro',
             'commenting_allow_guests',
             'commenting_auto_confirm',
             'commenting_default_department',
             'commenting_enter_department',
             'commenting_enter_subject',
             'commenting_enter_email',
             'commenting_enter_mobile',
             'commenting_enter_file',
             'commenting_private_choice',
             'commenting_users_can_reply',
             'commenting_email_notifications',
             'commenting_sms_notifications',
        ];

        model('feature', 'commenting')->attachInputs($array);
    }
}
