<?php

namespace Modules\Commenting\Events;

use Illuminate\Queue\SerializesModels;

class CommentReplied
{
    use SerializesModels;
    public $model;
    public $user_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model)
    {
        if (is_string($model) or is_numeric($model)) {
            $model = model('Comment', $model);
        }

        $this->model   = $model;
        $this->user_id = user()->id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
