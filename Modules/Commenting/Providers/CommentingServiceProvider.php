<?php

namespace Modules\Commenting\Providers;

use Modules\Commenting\Events\CommentReplied;
use Modules\Commenting\Http\Endpoints\V1\ListEndpoint;
use Modules\Commenting\Http\Endpoints\V1\PostCommentSaveEndpoint;
use Modules\Commenting\Http\Endpoints\V1\PostCommentsEndpoint;
use Modules\Commenting\Http\Endpoints\V1\ReplyEndpoint;
use Modules\Commenting\Http\Endpoints\V1\UpdateEndpoint;
use Modules\Commenting\Listeners\UpdateCommentOnReply;
use Modules\Yasna\Services\YasnaProvider;

class CommentingServiceProvider extends YasnaProvider
{

    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerSidebar();
        $this->registerListeners();
        $this->registerRoleSampleModules();
        $this->registerModelTraits();
        $this->registerEndpoints();
    }



    /**
     * Registers Listeners
     */
    public function registerListeners()
    {
        $this->listen(CommentReplied::class, UpdateCommentOnReply::class);
    }



    /**
     * Registers Sidebar
     */
    public function registerSidebar()
    {
        service('manage:sidebar')
             ->add('commenting')
             ->blade('commenting::layouts.sidebar')
             ->order(33)
        ;
    }



    /**
     * Gets setting config combo
     *
     * @return array
     */
    public static function getConfigCombo()
    {
        $array  = ['disabled', 'can', 'must'];
        $result = [];

        foreach ($array as $item) {
            $result[] = [
                 'id'    => $item,
                 'title' => trans_safe("commenting::settings.combo.$item"),
            ];
        }

        return $result;
    }



    /**
     * Sidebar Array
     *
     * @return array
     */
    public static function getSidebarArray()
    {
        /*-----------------------------------------------
        | Preparations ...
        */
        $posttypes = model('posttype')->having('commenting')->get();
        $array     = [];
        $printed   = 0;

        /*-----------------------------------------------
        | Explore ...
        */
        foreach ($posttypes as $posttype) {
            $array[] = [
                 'condition' => $condition = $posttype->can('commenting'),
                 'caption'   => $posttype->title,
                 'link'      => "comments/browse/$posttype->slug",
            ];

            //$printed += $condition;
        }

        /*-----------------------------------------------
        | All Together ...
        */
        if ($printed > 1) {
            $array[] = [
                 'caption' => trans_safe("commenting::phrases.all_together"),
                 'link'    => "comments/browse/all",
            ];
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $array;
    }



    /**
     * Module string, to be used as sample for role definitions.
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('comments')
             ->value('edit ,  process ,  publish ,  report ,  delete ,  bin ,')
        ;
    }



    /**
     * Registers Model Traits
     */
    public function registerModelTraits()
    {
        module('yasna')
             ->service('traits')
             ->add('PostCommentingTrait')
             ->trait('Commenting:PostCommentingTrait')
             ->to('Post')
        ;

        $this->addModelTrait('PosttypeCommentingTrait', 'Posttype');
        $this->addModelTrait('PostCommentingResourcesTrait', 'Post');
    }



    /**
     * Registers the endpoints only when the `Endpoints` module is available.
     *
     * @return void
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }

        endpoint()->register(PostCommentsEndpoint::class);
        endpoint()->register(PostCommentSaveEndpoint::class);
        endpoint()->register(ListEndpoint::class);
        endpoint()->register(UpdateEndpoint::class);
        endpoint()->register(ReplyEndpoint::class);
    }
}
