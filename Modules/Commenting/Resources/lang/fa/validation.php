<?php
return [
     "attributes" => [
          "limit"          => "محدودیت تعداد",
          "status"         => "وضعیت",
          "department"     => "دپارتمان",
          "with_replies"   => "به همراه پاسخ‌ها",
          "replies_number" => "تعداد پاسخ‌ها",
          "replies_limit"  => "محدودیت تعداد جواب‌ها",
          "sort"           => "مرتب‌سازی",
          "private"        => "خصوصی",
     ],
];
