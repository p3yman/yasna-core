<?php
return [
     "all"      => "All",
     'pending'  => "Pending",
     'archive'  => "Reviewed",
     'trashed'  => "Trashed",
     'bin'      => "Bin",
     'spam'     => "Spam",
     'approved' => "Approved",
];
