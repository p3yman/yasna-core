<?php
return [
     "attributes" => [
          "limit"          => "Limit",
          "status"         => "Status",
          "department"     => "Department",
          "with_replies"   => "With Replies",
          "replies_number" => "Replies Number",
          "replies_limit"  => "Replies Limit",
          "sort"           => "Sort",
          "private"        => "Private",
     ],
];
