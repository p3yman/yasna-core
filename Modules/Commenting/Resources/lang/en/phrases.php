<?php
return [
     'plural'             => "Comments",
     'singular'           => "Comment",
     'all_together'       => "All Together",
     'sender'             => "Sender",
     'message'            => "Message",
     'by_machine'         => "by Machine",
     'spam'               => "Span",
     'is_spam'            => "This is spam.",
     'not_spam'           => "This is not spam.",
     'action'             => "Action",
     'spam_hint'          => "Spam is an unrelated message that is being sent for commercial purposes, automatically.",
     'ip_info'            => "IP Information",
     'dialogue_mode'      => "Show Dialog",
     'single_mode'        => "Show Single",
     'reply'              => "Reply",
     'refer'              => "Refer / Change Status",
     'reply_text'         => "Reply Message",
     'from_admin'         => "Manager",
     'department_changed' => "Refer to",
     'status_changed'     => "Change Status to",

];
