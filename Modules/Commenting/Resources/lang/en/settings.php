<?php
return [
     'receive'             => "Receive Comments",
     'display'             => "Display Comments",
     'intro'               => "User Guide",
     'allow_guests'        => "Allowing Guests to Leave a Comment",
     'auto_confirm'        => "Auto Confirm",
     'default_department'  => "Default Department",
     'enter_department'    => "Choose Department",
     'enter_subject'       => "Enter Subject",
     'enter_email'         => "Enter Email",
     'enter_mobile'        => "Enter Mobile Number",
     'enter_file'          => "Upload File",
     'private_choice'      => "Private Message Option",
     'users_can_reply'     => "Replying Back Option",
     'email_notifications' => "Email Notifications",
     'sms_notifications'   => "SMS Notifications",

     'hint' => [
          'receive'             => "User can receive comments.",
          'display'             => "Public View for Received Comments",
          'intro'               => "Paragraph Text Shown Beside Commenting Form",
          'allow_guests'        => "Giving Permission for Leaving Comments to Unregistered Users",
          'auto_confirm'        => "Received comments won't need admin's confirmation.",
          'default_department'  => "Where messages with unset department should go.",
          'enter_department'    => "Choosing Department by User",
          'enter_subject'       => "Choosing Comment Title by User",
          'enter_email'         => "Optional or Mandatory Email Address Receipt",
          'enter_mobile'        => "Optional or Mandatory Mobile Number Receipt",
          'enter_file'          => "Optional or Mandatory File Upload",
          'private_choice'      => "Showing \"Private Mode\" Option to Users",
          'users_can_reply'     => "Replying Back Option for Users",
          'email_notifications' => "Drop an email when get replied.",
          'sms_notifications'   => "Text an SMS when get replied.",
     ],

     'combo' => [
          'disabled' => "Disabled",
          'can'      => "Optional",
          'must'     => "Mandatory",
     ],
];
