@include("manage::widgets.grid-date" , [
	'date' => $model->created_at ,
]     )

@include("manage::widgets.grid-text" , [
	'text' => $model->subject,
	'condition' => $model->subject ,
	'class' => "font-yekan text-bold" ,
]     )

@include("manage::widgets.grid-text" , [
	'text' => isset($process) ? $model->text : $model->text_short,
]     )

@include("manage::widgets.grid-text" , [
	'condition' => boolval($new_department_name = $model->new_department_name) ,
	'text' => trans_safe("commenting::phrases.department_changed") . SPACE . $new_department_name,
	'icon' => "arrow-left" ,
	'color' => "primary" ,
]     )

@include("manage::widgets.grid-text" , [
	'condition' => boolval($new_status_name = $model->new_status_name) ,
	'text' => trans_safe("commenting::phrases.status_changed") . SPACE . $new_status_name,
	'icon' => "arrow-left" ,
	'color' => $model->new_status_color ,
]     )

@include("manage::widgets.grid-tiny" , [
	'icon' => "bug",
	'locale' => "en" ,
	'condition' => dev() ,
	'class' => "text-gray" ,
	'text' => $model->id . " | " . $model->hashid ,
]     )
