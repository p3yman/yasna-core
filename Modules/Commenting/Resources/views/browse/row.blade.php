@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
	'refresh_url' => "manage/comments/update/$model->hashid",
])

<td>
	@include("commenting::browse.sender")
</td>
<td class="only-on-desktop">
	@include("commenting::browse.message")
</td>

<td>
	@include("commenting::browse.status")
</td>

@include("commenting::browse.actions")
