@include("manage::widgets.grid-text" , [
	'icon' => $model->from_guest? 'user-o' : 'user' ,
	'class' => "f16 font-yekan text-bold" ,
	'link' => $model->user->manage_link? $model->user->manage_link : null ,
	'color' => $model->is_from_admin? 'green' : null ,
	'text' => $model->user->full_name ,
])

@include("manage::widgets.grid-badge" , [
	'text' => trans_safe("commenting::phrases.from_admin"),
	'condition' => $model->is_from_admin ,
	'icon' => "check-circle-o" ,
	'color' => "default text-green f12" ,
]     )

@include("manage::widgets.grid-text" , [
	'text' => $email = $model->user->email ,
	'size' => "14" ,
	'class' => "text-light font-tahoma text-wide" ,
	'condition' => $model->user->email ,
//	'icon' => "at" ,
	'locale' => "en" ,
])

@include("manage::widgets.grid-text" , [
	'text' => $model->user->mobile ,
	'size' => "12" ,
	'class' => "text-light font-tahoma text-wide" ,
	'condition' => $model->user->mobile ,
	'locale' => "en" ,
//	'icon' => "phone" ,
])


@include("manage::widgets.grid-tiny" , [
	'text' => $model->user->ip ,
	'size' => "10" ,
	'class' => "text-gray font-tahoma text-wide" ,
	'condition' => trim($model->user->ip) ,
	'locale' => "en" ,
	'link' => $model->actionLink('ip') ,
	'icon' => "location-arrow" ,
])

