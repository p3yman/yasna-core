@extends('manage::layouts.template')

@section('content')
	@include('commenting::browse.tabs')


	{{--
	|--------------------------------------------------------------------------
	| Toolbar
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::widgets.toolbar" , [
		'search' => [
			'target' => url("manage/carts/browse/$request_tab/search") ,
			'label' => trans('manage::forms.button.search'),
			'value' => isset($keyword)? $keyword : ''
		] ,
	]     )

	{{--
	|--------------------------------------------------------------------------
	| Grid
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::widgets.grid" , [
		'table_id' => "tblComments" ,
		'row_view' => "commenting::browse.row" ,
		'handle' => "selector" ,
		'operation_heading' => true ,
		'headings' => [
			[trans("commenting::phrases.sender")] ,
			[trans("commenting::phrases.message") , 'only-on-desktop'] ,
			[trans("validation.attributes.status"),200]
		] ,
	]     )



@endsection