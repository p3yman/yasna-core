@include("manage::widgets.grid-actionCol" , [
//	'button_size' => "sm" ,
	"actions" => [
		['pencil' , trans_safe("commenting::phrases.action") , $model->actionLink('process')  , $model->isNotTrashed() and $model->isNotSpam() and $model->can('process')] ,

		['ban' , trans_safe("commenting::phrases.is_spam") , $model->actionLink('spam') , $model->isNotSpam() and $model->can('process') ],
		['undo' , trans_safe("commenting::phrases.not_spam") , $model->actionLink('spam') , $model->isSpam() and $model->can('process') ],

		['trash-o' , trans_safe("manage::forms.button.soft_delete") , $model->actionLink('activeness') , $model->isNotTrashed() and $model->can('delete')],
		['recycle' , trans_safe("manage::forms.button.undelete") , $model->actionLink('activeness') , $model->isTrashed() and $model->can('bin')],
	]
])