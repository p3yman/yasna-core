@include("manage::widgets.grid-text" , [
	'text' => $model->status_text ,
	'size' => "14" ,
	'color' => $model->status_color ,
	'icon' => $model->status_icon ,
]     )


@include("manage::widgets.grid-tiny" , [
	'icon' => $model->by == 'auto' ? 'desktop' : 'user' ,
	'text' => $model->by == 'auto' ? trans_safe("commenting::phrases.by_machine") : trans_safe("manage::forms.general.by") . SPACE . $model->by ,
	'color' => $model->status_color ,
	'condition' => $model->by ,
]     )

{{--
|--------------------------------------------------------------------------
| Normal Action
|--------------------------------------------------------------------------
|
--}}
{!!
widget("button")
	->target($model->actionLink('process'))
	->label('tr:commenting::phrases.action')
	->condition($model->isProcessable() )
	->class('mv10 btn-taha')
!!}

{{--
|--------------------------------------------------------------------------
| Spam Action
|--------------------------------------------------------------------------
|
--}}
{!!
widget("button")
	->target($model->actionLink('spam'))
	->label('tr:commenting::phrases.not_spam')
	->condition($model->isSpam() and $model->can('process') )
	->class('mv10 btn-taha')
!!}

