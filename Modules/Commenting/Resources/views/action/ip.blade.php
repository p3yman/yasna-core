{!! widget("modal")
	->label(trans_safe("commenting::phrases.ip_info") . SPACE . pd($model->user->ip) )
!!}

@php
    $info = ipReader($model->user->ip);
@endphp

@include("commenting::action.ip-row" , [
	'title' => "IP",
	'value' => $model->user->ip  ,
]     )
@include("commenting::action.ip-row" , [
	'title' => "Country",
	'value' => $info->getCountryName() . " - " . $info->getCountryCode()  ,
]     )
@include("commenting::action.ip-row" , [
	'title' => "Region",
	'value' => $info->getRegionName() . " - " . $info->getRegionCode() ,
]     )
@include("commenting::action.ip-row" , [
	'title' => "City",
	'value' => $info->getCity() . " - " . $info->getZipCode()  ,
]     )

