@php
	$process = 'single';
@endphp

{!! widget("modal")
	->target('name:comment-process')
	->label(trans_safe("commenting::phrases.singular"))
	->render()
!!}

{{--
|--------------------------------------------------------------------------
| Explore
|--------------------------------------------------------------------------
|
--}}

<div id="divMessages">
	@foreach($thread = $model->thread as $message)
		@include("commenting::action.message" , [
			'model' => $message ,
			'main' => boolval($model->id == $message->id) ,
		])
	@endforeach
</div>


{{--
|--------------------------------------------------------------------------
| Panel
|--------------------------------------------------------------------------
|
--}}
<div id="divActionPanel" class="noDisplay">
	@include("commenting::action.editor")
</div>

{{--
|--------------------------------------------------------------------------
| Buttons
|--------------------------------------------------------------------------
|
--}}
{!! widget("separator") !!}

<div class="mv10">
	<?php

	widget("button")
		->id('btnSave')
		->shape('success')
		->class('btn-taha mh5')
		->type('submit')
		->label("tr:manage::forms.button.send_and_save")
		->hidden()
		->render()
	;

	widget("button")
		->id('btnReply')
		->label('tr:commenting::phrases.reply')
		->shape('primary')
		->class('btn-taha mh5')
		->onClick("inlineCommentingActions('reply')")
		->render()
	;

	widget("button")
		->id('btnRefer')
		->label('tr:commenting::phrases.refer')
		->shape('info')
		->class('btn-taha mh5')
		->onClick("inlineCommentingActions('refer')")
		->render()
	;

	widget("button")
		->id('btnDialogueMode')
		->onClick("inlineCommentingActions('dialogue-mode')")
		->label("tr:commenting::phrases.dialogue_mode")
		->condition($thread->count() > 1)
		->class('btn-taha mh5')
		->render()
	;

	widget("button")
		->id('btnSingleMode')
		->onClick("inlineCommentingActions('single-mode')")
		->label("tr:commenting::phrases.single_mode")
		->hidden()
		->class('btn-taha mh5')
		->render()
	;

	widget("button")
		->onClick('$(".modal").modal("hide")')
		->label("tr:manage::forms.button.cancel")
		->class('btn-taha mh5')
		->shape('link')
		->render()
	;

	widget('feed')
		->render();

	?>
</div>


{{--
|--------------------------------------------------------------------------
| Script
|--------------------------------------------------------------------------
|
--}}
<script>
   function inlineCommentingActions(mode) {
      let $single_button     = $("#btnSingleMode");
      let $dialogue_button   = $("#btnDialogueMode");
      let $dialogue_messages = $(".-notMain");
      let $messages_panel    = $('#divMessages');
      let $form_panel        = $('#divActionPanel');
      let $reply_button      = $('#btnReply');
      let $refer_button      = $('#btnRefer');
      let $save_button       = $('#btnSave');
      let $reply_text_area   = $("#txtReplyText");

      switch (mode) {
         case 'dialogue-mode' :
            $dialogue_button.hide();
            $single_button.show();
            $save_button.hide();
            $refer_button.show();
            $reply_button.show();
            $form_panel.slideUp('fast');
            $messages_panel.slideDown('fast');
            $dialogue_messages.slideDown('fast');
            break;

         case 'single-mode' :
            $dialogue_button.show();
            $single_button.hide();
            $save_button.hide();
            $refer_button.show();
            $reply_button.show();
            $form_panel.slideUp('fast');
            $messages_panel.slideDown('fast');
            $dialogue_messages.slideUp('fast');
            break;

         case 'reply' :
            $dialogue_button.show();
//            $single_button.show();
            $save_button.show();
            $refer_button.hide();
            $reply_button.hide();
            $reply_text_area.show();
            $form_panel.slideDown();
            $messages_panel.slideDown('fast', function () {
               $reply_text_area.focus();
            });
            $dialogue_messages.slideUp('fast');
//            $messages_panel.slideUp('fast');
            break;
         case 'refer' :
            $dialogue_button.show();
//            $single_button.show();
            $save_button.show();
            $refer_button.hide();
            $reply_button.hide();
            $reply_text_area.hide();
            $form_panel.slideDown();
            $messages_panel.slideDown('fast');
            $dialogue_messages.slideUp('fast');
//            $messages_panel.slideUp('fast');
            break;
      }
   }
</script>
