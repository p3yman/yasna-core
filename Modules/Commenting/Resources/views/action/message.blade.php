@php
	isset($main)?: $main = false;
@endphp

<div class="row w90 mv10 panel panel-success {{ $main? 'bg-gray' : 'noDisplay -notMain'  }} ">
	<div class="panel-body">
		<div class="col-md-3">
			@include("commenting::browse.sender")
		</div>
		<div class="col-md-9">
			@include("commenting::browse.message")
		</div>

	</div>



</div>
