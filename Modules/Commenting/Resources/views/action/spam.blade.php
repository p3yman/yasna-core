@php
    $action = $model->isSpam() ? 'not_spam' : 'is_spam';
@endphp

@include("commenting::action.header" , [
	'route' => "comment-spam" ,
	'title' => trans_safe("commenting::phrases.spam") ,
] )

{!!
widget("note")
	->label('tr:commenting::phrases.spam_hint')
	->class('text-info')
!!}

@include("manage::forms.buttons-for-modal" , [
	'separator' => false ,
	'save_label' => trans_safe("commenting::phrases.$action"),
	'save_value' => $action ,
	'save_shape' => $model->isSpam()? 'success' : 'warning' ,
]     )

