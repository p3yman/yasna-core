<?php
widget("modal")
	->targetIf($route , "name:" . $route)
	->label($title)
	->render()
;

widget('input')
	->value($model->full_title)
	->label('tr:commenting::phrases.singular')
	->disabled()
	->inForm()
	->render()
;

widget('hidden')
	->name('hashid')
	->value($model->hashid)
	->render()
;