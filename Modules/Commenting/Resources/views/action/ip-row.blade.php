<div class="ltr w90 row mv10" style="direction: ltr !important;">
	<div class="col-md-9 ">
		{{ $value }}
	</div>
	<div class="col-md-3 text-bold align-left">
		{{ $title }}
	</div>
</div>