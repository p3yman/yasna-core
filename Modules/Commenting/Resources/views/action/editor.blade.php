{!!
widget("hidden")
	->name('id')
	->value($model->hashid)
!!}

{!!
widget("textarea")
	->name('text')
	->id('txtReplyText')
	->class('atr mv10')
	->size(5)
	->placeholder('tr:commenting::phrases.reply_text')
!!}

{!!
widget("combo")
	->name('status')
	->value('approved')
	->options($model->statusCombo())
	->inForm()
!!}

{!!
widget("combo")
	->name('department')
	->condition(model('role')->supportRolesDefined())
	->value($model->master->current_department_id)
	->options($model->departmentsCombo())
	->blankValueIf( !$model->master->current_department_id , 0)
	->inForm()
!!}
