@php
    $action = $model->isTrashed() ? 'undelete' : 'soft_delete';
    $title = trans_safe("manage::forms.button.$action");
@endphp

@include("commenting::action.header" , [
	'route' => "comment-activeness" ,
	'title' => $title ,
] )

@include("manage::forms.buttons-for-modal" , [
	'separator' => false ,
	'save_label' => $title,
	'save_value' => $action ,
	'save_shape' => $model->isTrashed()? 'success' : 'warning' ,
]     )

