@include('manage::layouts.sidebar-link' , [
	'condition' => user()->can('commenting') ,
	'icon'       => "comments-o",
	'caption' => trans_safe("commenting::phrases.plural") ,
	'link' => 'commenting' ,
	'sub_menus' => Commenting::getSidebarArray() ,
]   )
