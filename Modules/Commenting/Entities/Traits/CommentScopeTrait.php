<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/10/18
 * Time: 3:23 PM
 */

namespace Modules\Commenting\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;

trait CommentScopeTrait
{
    /**
     * The Scope to select comments with the specified criteria.
     *
     * @param Builder $builder
     * @param string  $criteria
     *
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeWithCriteria(Builder $builder, $criteria)
    {
        if (!$criteria) {
            return $builder;
        }

        switch ($criteria) {
            case 'all':
                return $builder->whereNull('spammed_at');

            case 'pending':
                return $builder->whereNull('approved_at')->whereNull('spammed_at');

            case 'approved':
            case 'archive':
                return $builder->whereNotNull('approved_at')->whereNull('spammed_at');

            case 'spam':
                return $builder->whereNotNull('spammed_at');

            case 'bin':
                return $builder->onlyTrashed();

            default:
                return $builder->whereNull('id');

        }
    }
}
