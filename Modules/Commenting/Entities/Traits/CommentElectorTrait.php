<?php namespace Modules\Commenting\Entities\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait CommentElectorTrait
{
    /**
     * Adds a criteria condition to the query.
     *
     * @param string $criteria
     *
     * @return Builder
     */
    protected function electorCriteria($criteria)
    {
        if (!$criteria) {
            $criteria = 'pending';
        }

        return $this
             ->elector()
             ->withCriteria($criteria)
             ;
    }



    /**
     * Adds a condition for the `user_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorUser($id)
    {
        return $this->electorFieldId($id, 'user_id');
    }



    /**
     * Adds a condition for the `post_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorPost($id)
    {
        return $this->electorFieldId($id, 'post_id');
    }



    /**
     * Adds a condition for the `posttype_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorPosttype($id)
    {
        return $this->electorFieldId($id, 'posttype_id');
    }



    /**
     * Adds a posttype slug condition to the query.
     *
     * @param string $slug
     */
    protected function electorPosttypeSlug($slug)
    {
        $posttype_id = posttype($slug)->id;
        $this->electorPosttype($posttype_id);
    }



    /**
     * Adds a condition for the `parent_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorParent($id)
    {
        return $this->electorFieldId($id, 'parent_id');
    }



    /**
     * Adds a condition for the `master_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorMaster($id)
    {
        return $this->electorFieldId($id, 'master_id');
    }



    /**
     * Adds a condition for the `initial_department_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorInitialDepartment($id)
    {
        return $this->elector()->where('initial_department_id', $id);
    }



    /**
     * Adds a condition for the `current_department_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorCurrentDepartment($id)
    {
        return $this->elector()->where('current_department_id', $id);
    }



    /**
     * Adds a condition for the `initial_department_id` column to the query.
     *
     * @param integer $id
     *
     * @return Builder
     */
    protected function electorDepartment($id)
    {
        return $this->electorInitialDepartment($id);
    }



    /**
     * Adds a condition for the `is_from_admin` column to the query.
     *
     * @param boolean $boolean
     *
     * @return Builder
     */
    protected function electorFromAdmin($boolean)
    {
        return $this->electorFieldBoolean($boolean, 'is_from_admin');
    }



    /**
     * Adds a condition for the `is_private` column to the query.
     *
     * @param boolean $boolean
     *
     * @return Builder
     */
    protected function electorPrivate($boolean)
    {
        if (user()->cannot('commenting')) {
            $boolean = 1;
        }
        return $this->electorFieldBoolean($boolean, 'is_private');
    }



    /**
     * Adds a condition for the `guest_name` column to the query.
     *
     * @param string $text
     *
     * @return Builder
     */
    protected function electorGuestName($text)
    {
        return $this->elector()->where('guest_name', $text);
    }



    /**
     * Adds a condition for the `guest_email` column to the query.
     *
     * @param string $text
     *
     * @return Builder
     */
    protected function electorGuestEmail($text)
    {
        return $this->elector()->where('guest_email', $text);
    }



    /**
     * Adds a condition for the `guest_mobile` column to the query.
     *
     * @param string $text
     *
     * @return Builder
     */
    protected function electorGuestMobile($text)
    {
        return $this->elector()->where('guest_mobile', $text);
    }



    /**
     * Adds a condition for the `guest_web` column to the query.
     *
     * @param string $text
     *
     * @return Builder
     */
    protected function electorGuestWeb($text)
    {
        return $this->elector()->where('guest_web', $text);
    }



    /**
     * Adds a condition for the `subject` column to the query.
     *
     * @param string $text
     *
     * @return Builder
     */
    protected function electorSubject($text)
    {
        return $this->elector()->where('subject', $text);
    }



    /**
     * Adds a condition for the `text` column to the query.
     *
     * @param string $text
     *
     * @return Builder
     */
    protected function electorText($text)
    {
        return $this->elector()->where('text', $text);
    }



    /**
     * Adds a condition for the `created_at` column to the query.
     *
     * @param Carbon|string $date
     *
     * @return void
     */
    protected function electorCreatedAt($date)
    {
        $this->elector()->whereDate('created_at', $date);
    }



    /**
     * Adds a condition for the `deleted_at` column to the query.
     *
     * @param Carbon|string $date
     *
     * @return void
     */
    protected function electorDeletedAt($date)
    {
        $this->elector()->whereDate('deleted_at', $date);
    }



    /**
     * Adds a condition for the `updated_at` column to the query.
     *
     * @param Carbon|string $date
     *
     * @return void
     */
    protected function electorUpdatedAt($date)
    {
        $this->elector()->whereDate('updated_at', $date);
    }



    /**
     * Adds a condition for the `approved_by` column to the query.
     *
     * @param integer $id
     *
     * @return void
     */
    protected function electorApproved($id)
    {
        if (user()->cannot('commenting')) {
            $this->elector()->where('approved_by', '<>', $id);
        }
    }
}
