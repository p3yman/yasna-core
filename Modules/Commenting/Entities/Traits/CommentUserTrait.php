<?php namespace Modules\Commenting\Entities\Traits;

trait CommentUserTrait
{
    protected $cached_user = null;

    public function user()
    {
        $this->cached_user = null;
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }

    public function getUserAttribute()
    {
        if (isset($this->cached_user)) {
            return $this->cached_user;
        }

        if (!$this->user_id) {
            return $this->guestUser();
        }

        $this->cached_user = $this->user()->first();
        if ($this->cached_user) {
            return $this->cached_user;
        } else {
            return $this->cached_user = model('user');
        }
    }

    protected function guestUser()
    {
        $user            = model('user');
        $user->full_name = $this->guest_name;
        $user->email     = $this->guest_email;
        $user->mobile    = $this->guest_mobile;
        $user->ip        = $this->guest_ip;
        $user->web       = $this->guest_web;

        return $user;
    }

    public function getFromGuestAttribute()
    {
        return !$this->user_id;
    }

    public function getByAttribute()
    {
        /*-----------------------------------------------
        | Field Discovery ...
        */
        switch ($this->status) {
            case 'spam':
                $field = 'spammed_by';
                break;

            case 'approved':
                $field = 'approved_by';
                break;

            case 'trashed':
                $field = 'deleted_by';
                break;

            default:
                $field = null;
        }

        /*-----------------------------------------------
        | User Discovery ...
        */
        if (!$field) {
            return false;
        }
        if (!$this->$field) {
            return 'auto';
        }
        return $this->getPerson($field)->full_name;
    }
}
