<?php namespace Modules\Commenting\Entities\Traits;

trait CommentPermissionsTrait
{
    public function hasManagePermission($type, $criteria)
    {
        switch ($criteria) {
            case 'bin':
                $permit = 'bin';
                break;

            default:
                $permit = 'browse';
                break;
        }

        return user()->as('admin')->can("commenting-$type.$criteria");
    }



    public function hasnotManagePermission($type, $criteria)
    {
        return !$this->hasManagePermission($type, $criteria);
    }



    /**
     * check permission for current posttype
     *
     * @param string $permit
     *
     * @return bool
     */
    public function can($permit = '*')
    {
        if (!$this->posttype) {
            return false;
        }

        return $this->posttype->can("commenting-$permit");
    }



    /**
     * check if the current online user can update comments
     *
     * @return bool
     */
    public function canEdit(): bool
    {
        return $this->can("edit");
    }



    public function cannot($permit = '*')
    {
        return !$this->can($permit);
    }
}
