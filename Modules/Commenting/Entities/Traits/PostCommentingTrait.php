<?php

namespace Modules\Commenting\Entities\Traits;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

/**
 * Trait PostCommentingTrait (will be added to Post model, via service provider).
 * @method HasMany hasMany($model_name)
 *
 * @package Modules\Commenting\Entities\Traits
 */
trait PostCommentingTrait
{
    /**
     * Laravel standard one-to-many relationship method.
     *
     * @return HasMany
     */
    public function comments()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Comment");
    }



    /**
     * Saves a comment with the given information on this post.
     *
     * @param array|Request $request
     * @param array $overflow_parameters
     *
     * @return bool|Comment
     */
    public function batchSaveComment($request, $overflow_parameters = [])
    {
        if ($this->not_exists) {
            return false;
        }

        if (!is_array($request)) {
            $data = $request->toArray();
        }

        $data['post_id']     = $this->id;
        $data['posttype_id'] = $this->posttype->id;

        return model('comment')->batchSave($data, $overflow_parameters);
    }
}
