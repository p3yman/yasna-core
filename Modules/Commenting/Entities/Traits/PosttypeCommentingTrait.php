<?php

namespace Modules\Commenting\Entities\Traits;

trait PosttypeCommentingTrait
{
    /**
     * Whether the given field is required for comments on this posttype.
     *
     * @param string $field
     *
     * @return bool
     */
    public function commentingFieldIsRequired(string $field): bool
    {
        return ($this->getCommentingFieldsStatus($field) == 'must');
    }



    /**
     * Whether the given field is optional for comments on this posttype.
     *
     * @param string $field
     *
     * @return bool
     */
    public function commentingFieldIsOptional(string $field): bool
    {
        return ($this->getCommentingFieldsStatus($field) == 'can');
    }



    /**
     * Whether the given field is disabled for comments on this posttype.
     *
     * @param string $field
     *
     * @return bool
     */
    public function commentingFieldIsDisabled(string $field): bool
    {
        $input_value = $this->getCommentingFieldsStatus($field);

        return (
             is_null($input_value)
             or
             ($input_value == 'disabled')
        );
    }



    /**
     * Returns the commenting input value for entering a field.
     *
     * @param string $field
     *
     * @return mixed
     */
    protected function getCommentingFieldsStatus(string $field)
    {
        $input = 'enter_' . $field;
        return $this->getCommentingInputValue($input);
    }



    /**
     * Returns the value of a commenting value with
     *
     * @param string $input
     *
     * @return mixed
     */
    public function getCommentingInputValue(string $input)
    {
        return $this->getMeta(
             $this->commentingInputPrefix()
             .
             $input
        );
    }



    /**
     * Returns the prefix of the commenting inputs.
     *
     * @return string
     */
    public function commentingInputPrefix()
    {
        return 'commenting_';
    }
}
