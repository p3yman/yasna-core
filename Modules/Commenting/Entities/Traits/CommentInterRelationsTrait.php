<?php namespace Modules\Commenting\Entities\Traits;

trait CommentInterRelationsTrait
{

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    public function parent()
    {
        return model('comment')->find($this->parent_id);
    }

    public function getParentAttribute()
    {
        return $this->parent();
    }

    public function master()
    {
        if ($this->master_id) {
            return model('comment')->find($this->master_id);
        } else {
            return $this;
        }
    }

    public function getMasterAttribute()
    {
        return $this->master();
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function getChildrenAttribute()
    {
        return $this->children()->orderBy('created_at', 'desc')->get();
    }

    public function slaves()
    {
        return $this->hasMany(static::class, 'master_id');
    }

    public function getSlavesAttribute()
    {
        return $this->slaves()->orderBy('created_at', 'desc')->get();
    }

    public function thread()
    {
        $master = $this->master ;
        return $this->newInstance()->where('id', $master->id)->orWhere('master_id', $master->id) ;
    }

    public function getThreadAttribute()
    {
        return $this->thread()->orderBy('created_at')->get();
    }

    public function threadWithoutAdminReplies()
    {
        return $this->thread()->where('is_from_admin', false) ;
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function isMaster()
    {
        return !$this->isSlave();
    }

    public function isSlave()
    {
        return boolval($this->master_id);
    }

    public function isAChild()
    {
        return boolval($this->parent_id);
    }

    public function isAParent()
    {
        return $this->hasChildren();
    }

    public function hasChildren()
    {
        return boolval($this->children()->count());
    }
}
