<?php

namespace Modules\Commenting\Entities\Traits;

use App\Models\User;
use Carbon\Carbon;

/**
 * @property User $user
 */
trait CommentResourcesTrait
{
    /**
     * boot CommentResourcesTrait
     *
     * @return void
     */
    public static function bootCommentResourcesTrait()
    {
        static::addDirectResources([
             "subject",
             "text",
             "status",
             "approved_at",
             "spammed_at",
        ]);
    }



    /**
     * get Avatar resource.
     *
     * @return string
     */
    protected function getAvatarResource()
    {
        $user = $this->user;

        if (!$user or module('UserAvatar')->isNotActive()) {
            return null;
        }

        return userAvatar($user);
    }



    /**
     * get Username resource.
     *
     * @return string
     */
    protected function getUsernameResource()
    {
        $user = $this->user;

        return $user ? $user->username : null;
    }



    /**
     * get name resource.
     *
     * @return string
     */
    protected function getNameResource()
    {
        $user = $this->user;

        return $user ? $user->full_name : $this->getAttribute("guest_name");
    }

}
