<?php namespace Modules\Commenting\Entities\Traits;

trait CommentStatusTrait
{
    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */

    public function getStatusAttribute()
    {
        if ($this->isSpam()) {
            return 'spam';
        }

        if ($this->trashed()) {
            return 'trashed';
        }

        if ($this->isApproved()) {
            return 'approved';
        }

        return 'pending';
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */


    public function isSpam()
    {
        return boolval($this->spammed_at);
    }

    public function isNotSpam()
    {
        return !$this->isSpam();
    }

    public function isApproved()
    {
        return $this->isNotTrashed() and $this->isNotSpam() and boolval($this->approved_at);
    }

    public function isNotApproved()
    {
        return !$this->isApproved();
    }

    public function isPending()
    {
        return $this->isNotTrashed() and $this->isNotSpam() and !boolval($this->approved_at);
    }

    public function isNotPending()
    {
        return !$this->isPending();
    }

    public function isProcessable()
    {
        return $this->can('process') and $this->isNotTrashed() and $this->isNotSpam();
    }

    public function isNotProcessable()
    {
        return !$this->isProcessable();
    }

    public function availableStatusForProcess()
    {
        return ['pending', 'approved'];
    }
    /*
    |--------------------------------------------------------------------------
    | Setters
    |--------------------------------------------------------------------------
    |
    */
    public function markAsSpam()
    {
        return $this->update([
            'spammed_at' => $this->now(),
            'spammed_by' => user(),
        ]);
    }

    public function markAsNotSpam()
    {
        return $this->update([
            'spammed_at' => null,
            'spammed_by' => 0,
        ]);
    }


    /*
    |--------------------------------------------------------------------------
    | Combo
    |--------------------------------------------------------------------------
    |
    */
    public function statusCombo()
    {
        $available_status = $this->availableStatusForProcess();
        $result           = [];

        foreach ($available_status as $status) {
            $result[] = [
                $status,
                $this->statusText($status),
            ];
        }

        return $result ;
    }
}
