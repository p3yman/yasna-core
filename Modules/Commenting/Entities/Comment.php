<?php namespace Modules\Commenting\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Commenting\Entities\Traits\CommentResourcesTrait;
use Modules\Commenting\Entities\Traits\CommentElectorTrait;
use Modules\Commenting\Entities\Traits\CommentInterRelationsTrait;
use Modules\Commenting\Entities\Traits\CommentPermissionsTrait;
use Modules\Commenting\Entities\Traits\CommentScopeTrait;
use Modules\Commenting\Entities\Traits\CommentStatusTrait;
use Modules\Commenting\Entities\Traits\CommentUserTrait;
use Modules\Yasna\Services\ModelTraits\YasnaStatusTrait;
use Modules\Yasna\Services\YasnaModel;

class Comment extends YasnaModel
{
    use CommentInterRelationsTrait, CommentPermissionsTrait, CommentElectorTrait, CommentUserTrait;
    use YasnaStatusTrait, CommentStatusTrait;
    use CommentResourcesTrait;
    use CommentScopeTrait;
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $casts = [
         'meta'          => "array",
         'user_id'       => "integer",
         'post_id'       => "integer",
         'posttype_id'   => "integer",
         'parent_id'     => "integer",
         'master_id'     => "integer",
         'is_private'    => "boolean",
         'is_from_admin' => "boolean",
    ];



    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    |
    */

    public function post()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Post");
    }



    public function posttype()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Posttype");
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function isFromAdmin()
    {
        return $this->is_from_admin;
    }



    public function isPrivate()
    {
        return $this->is_private;
    }



    public function isPublic()
    {
        return !$this->isPrivate();
    }



    public function isPublished()
    {
        return boolval($this->published_at);
    }



    public function isNotPublished()
    {
        return !$this->isPublished();
    }



    public function actionLink($action)
    {
        return "modal:manage/comments/act/$this->hashid/$action";
    }



    protected function changeMetaFields()
    {
        return [
             'new_status',
             'new_department',
        ];
    }



    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */
    public function getTextShortAttribute()
    {
        return str_limit($this->text, 400);
    }



    public function getFullTitleAttribute()
    {
        $string = $this->user->full_name;
        $string .= " (" . $this->relativeDate() . ")";

        if ($this->subject) {
            $string .= ": " . $this->subject;
        }

        return $string;
    }



    public function getNewDepartmentNameAttribute()
    {
        $id    = intval($this->getMeta('new_department'));
        $model = model('role', $id);
        if ($model->exists) {
            return $model->title;
        } else {
            return null;
        }
    }



    public function getNewStatusNameAttribute()
    {
        $status = $this->getMeta('new_status');
        if ($status) {
            return $this->statusText($status);
        } else {
            return false;
        }
    }



    public function getNewStatusColorAttribute()
    {
        $status = $this->getMeta('new_status');
        if ($status) {
            return $this->statusColor($status);
        } else {
            return false;
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    public function departmentsCombo()
    {
        return model('role')->supportRoles();
    }

}
