<?php namespace Modules\Commenting\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Commenting\Events\CommentReplied;

class UpdateCommentOnReply //implements ShouldQueue
{
    public $tries = 5;
    private $data  = [];
    private $model;
    private $user_id;

    public function handle(CommentReplied $event)
    {
        $this->model   = $event->model->spreadMeta();

        $this->user_id = $event->user_id;
        $this->injectStatusData();
        $this->injectDepartmentData();
        $this->update();
    }

    private function injectStatusData()
    {
        $new_status = $this->model->new_status;


        switch ($new_status) {
            case 'pending':
                $this->data['approved_at'] = null;
                $this->data['approved_by'] = 0;
                break;

            case 'approved':
                $this->data['approved_at'] = $this->model->now();
                $this->data['approved_by'] = $this->user_id;
                break;
        }
    }

    private function injectDepartmentData()
    {
        $new_department = $this->model->new_department;

        if ($new_department) {
            $this->data['current_department_id'] = $new_department;
        }
    }

    private function update()
    {
        if (!count($this->data)) {
            return false;
        }

        return $this->model->threadWithoutAdminReplies()->update($this->data);
    }
}
