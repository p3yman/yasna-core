<?php

namespace Modules\SiteMap\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MapperCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-mapper';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make the uniform mapper in the chosen module.';



    /**
     * get the folder path.
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Mappers");
    }



    /**
     * get stub replacement.
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->class_name,
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Mapper')) {
            return $file_name . "Mapper";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "mapper.stub";
    }
}
