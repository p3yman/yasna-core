<?php

if (!function_exists("mapper")) {
    /**
     * return an static state of the siteMap helper (MapHelper).
     *
     * @return \Modules\SiteMap\Services\MapHelper
     */
    function mapper()
    {
        return \Modules\SiteMap\Services\MapHelper::class;
    }
}

