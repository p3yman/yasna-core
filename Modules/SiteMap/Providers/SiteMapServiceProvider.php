<?php

namespace Modules\SiteMap\Providers;

use Modules\SiteMap\Console\MapperCommand;
use Modules\SiteMap\Schedules\TableUpdaterSchedule;
use Modules\SiteMap\Schedules\XmlGeneratorSchedule;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class SiteMapServiceProvider
 *
 * @package Modules\SiteMap\Providers
 */
class SiteMapServiceProvider extends YasnaProvider
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerSchedules();
        $this->registerArtisan();
    }



    /**
     * register Schedules
     *
     * @return void
     */
    private function registerSchedules()
    {
        $this->addSchedule(TableUpdaterSchedule::class);
        $this->addSchedule(XmlGeneratorSchedule::class);
    }



    /**
     * register artisan command
     *
     * @return void
     */
    private function registerArtisan()
    {
        $this->addArtisan(MapperCommand::class);
    }

}
