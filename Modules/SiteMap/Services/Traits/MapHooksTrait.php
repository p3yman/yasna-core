<?php

namespace Modules\SiteMap\Services\Traits;

trait MapHooksTrait
{
    private static $mappers = [];



    /**
     * register a new mapper by its fully qualified name.
     *
     * @param string $class
     *
     * @return bool
     */
    public static function registerMapper($class)
    {
        if (static::hasMapper($class) !== false) {
            return false;
        }

        static::$mappers[] = $class;

        return true;
    }



    /**
     * remove a registered mapper by its fully qualified name.
     *
     * @param string $class
     *
     * @return bool
     */
    public static function removeMapper($class)
    {

        if (($key = static::hasMapper($class)) === false) {
            return false;
        }

        unset(static::$mappers[$key]);

        return true;
    }



    /**
     * get the mappers array
     *
     * @return array
     */
    public static function getMapperNames()
    {
        return static::$mappers;
    }



    /**
     * check if the specified class is already registered.
     *
     * @param string $class
     *
     * @return bool|int: the key of the array, if exists; false otherwise.
     */
    public static function hasMapper($class)
    {
        return array_search($class, static::$mappers);
    }
}
