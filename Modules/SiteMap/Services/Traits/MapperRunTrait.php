<?php

namespace Modules\SiteMap\Services\Traits;

use App\Models\MapLink;
use Carbon\Carbon;
use Modules\SiteMap\Services\MapHelper;
use Modules\SiteMap\Services\YasnaMapper;
use Modules\Yasna\Services\YasnaModel;

trait MapperRunTrait
{
    /**
     * update links table.
     *
     * @return void
     */
    public static function updateTable()
    {
        foreach (MapHelper::getMapperNames() as $name) {
            /** @var YasnaMapper $mapper */
            $mapper = new $name();

            $mapper->run();
        }
    }



    /**
     * go through the mapper and run it.
     *
     * @return void
     */
    public function run()
    {
        $collection = $this->query(static::getLastRun())->withTrashed()->get();

        /** @var YasnaModel $model */
        foreach ($collection as $model) {
            if ($model->isTrashed()) {
                $this->removeFromMapsTable($model);
            } else {
                $this->addToMapsTable($model);
            }
        }
    }



    /**
     * get the time of which the table was last updated.
     *
     * @return null|Carbon|string
     */
    private static function getLastRun()
    {
        $last = MapLink::where("by_mapper", 1)->orderBy("updated_at", "desc")->first();
        /** @var Carbon $updated */
        $updated = $last ? $last->updated_at : new Carbon("2000-01-01 00:00:00");

        return $updated->subHour(5);
    }



    /**
     * add the model to the map links table.
     *
     * @param YasnaModel $model
     *
     * @return bool
     */
    private function addToMapsTable($model)
    {
        return mapper()::db()
                       ->url($this->getLink($model))
                       ->changeFrequency($this->changeFrequency($model))
                       ->priority($this->priority($model))
                       ->modifiedAt($this->lastModified($model))
                       ->modelId($model->id)
                       ->modelName($model->getClassName())
                       ->byMapper(true)
                       ->insert()
             ;
    }



    /**
     * remove the model from the map links table.
     *
     * @param YasnaModel $model
     *
     * @return bool
     */
    private function removeFromMapsTable($model)
    {
        return mapper()::db()
                       ->modelId($model->id)
                       ->modelName($model->getClassName())
                       ->remove()
             ;

    }
}
