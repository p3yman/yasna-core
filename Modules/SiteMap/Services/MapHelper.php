<?php

namespace Modules\SiteMap\Services;

use Modules\SiteMap\Services\Traits\MapHooksTrait;

/**
 * Class MapHelper
 */
class MapHelper
{
    const ALWAYS  = "always";
    const HOURLY  = "hourly";
    const DAILY   = "daily";
    const WEEKLY  = "weekly";
    const MONTHLY = "monthly";
    const YEARLY  = "yearly";
    const NEVER   = "never";

    use MapHooksTrait;



    /**
     * get an instance of the MapDatabase class to assist treating records by chain of methods.
     *
     * @return MapDatabase
     */
    public static function db()
    {
        return new MapDatabase();
    }



    /**
     * get an array of the valid change frequencies.
     *
     * @return array
     */
    public static function getValidFrequencies()
    {
        return [
             static::ALWAYS,
             static::HOURLY,
             static::DAILY,
             static::WEEKLY,
             static::MONTHLY,
             static::YEARLY,
             static::NEVER,
        ];
    }



    /**
     * regenerate the XML file.
     *
     * @return void
     */
    public static function generate()
    {
        (new MapGenerator())->generate();
    }



    /**
     * update the table, by going through the automatic mappers.
     *
     * @return void
     */
    public static function updateTable()
    {
        YasnaMapper::updateTable();
    }
}
