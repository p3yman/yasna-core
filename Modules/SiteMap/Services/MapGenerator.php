<?php

namespace Modules\SiteMap\Services;

use App\Models\MapLink;
use Illuminate\Filesystem\Filesystem;
use Nwidart\Modules\Support\Stub;

/**
 * Class MapGenerator
 */
class MapGenerator
{
    /**
     * generate the map XML file.
     *
     * @return void
     */
    public function generate()
    {
        $file_system = new Filesystem();
        $path        = public_path("sitemap.xml");

        $file_system->put($path, $this->getContent());
    }



    /**
     * get XML file content
     *
     * @return string
     */
    private function getContent()
    {
        Stub::setBasePath(__DIR__ . DIRECTORY_SEPARATOR . "stubs");

        $stub = new Stub("/sitemap.stub", [
             "LINKS" => $this->getLinks(),
        ]);

        return $stub->render();
    }



    /**
     * get XML links
     *
     * @return string
     */
    private function getLinks()
    {
        $output = "";
        foreach (MapLink::all() as $link) {
            $output .= $this->getOneLink($link);
        }

        return $output;
    }



    /**
     * get one XML link
     *
     * @param MapLink $model
     *
     * @return string
     */
    private function getOneLink($model)
    {
        $output = "";

        $output .= LINE_BREAK . TAB . "<url>";
        $output .= LINE_BREAK . TAB . TAB . "<loc>" . url($model->url) . "</loc>";
        if ($model->modified_at) {
            $output .= LINE_BREAK . TAB . TAB . "<lastmod>" . $model->modified_at->toW3cString() . "</lastmod>";
        }
        $output .= LINE_BREAK . TAB . TAB . "<changefreq>" . $model->change_frequency . "</changefreq>";
        $output .= LINE_BREAK . TAB . TAB . "<priority>" . $model->priority . "</priority>";
        $output .= LINE_BREAK . TAB . "</url>" . LINE_BREAK;

        return $output;
    }
}
