<?php

namespace Modules\SiteMap\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Modules\SiteMap\Services\Traits\MapperRunTrait;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class MapAbstract
 */
abstract class YasnaMapper
{
    use MapperRunTrait;



    /**
     * get the query builder to be executed in order to find the site map links.
     *
     * @param Carbon|string|null $updated_after
     *
     * @return Builder
     */
    abstract protected function query($updated_after);



    /**
     * get the unique link to be placed in the output map.
     *
     * @param YasnaModel $model
     *
     * @return string
     */
    abstract protected function getLink($model);



    /**
     * get the change frequency of the link
     *
     * @param YasnaModel $model
     *
     * @return string
     */
    protected function changeFrequency($model)
    {
        return MapHelper::NEVER;
    }



    /**
     * get the priority of the link
     *
     * @param YasnaModel $model
     *
     * @return float
     */
    protected function priority($model)
    {
        return 0.5;
    }



    /**
     * get the last modification date of the link
     *
     * @param YasnaModel $model
     *
     * @return string
     */
    protected function lastModified($model)
    {
        return $model->updated_at;
    }
}
