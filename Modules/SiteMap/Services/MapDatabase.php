<?php

namespace Modules\SiteMap\Services;

use App\Models\MapLink;
use Carbon\Carbon;

class MapDatabase
{
    /** @var string */
    private $url;

    /** @var string */
    private $model_name = '';

    /** @var int */
    private $model_id = 0;

    /** @var string */
    private $group = '';

    /** @var string */
    private $change_frequency = MapHelper::NEVER;

    /** @var float */
    private $priority = 0.5;

    /** @var Carbon|string|null */
    private $modified_at = null;

    /** @var bool */
    private $by_mapper = false;



    /**
     * set URL of the map link.
     *
     * @param string $url
     *
     * @return $this
     */
    public function url(?string $url)
    {
        $parsed = array_normalize(parse_url($url), [
             "path"  => null,
             "query" => null,
        ]);

        $this->url = $parsed['path'] . ($parsed['query'] ? "?" . $parsed['query'] : null);

        return $this;
    }



    /**
     * set/unset model name of the map record.
     *
     * @param string $model_name
     *
     * @return $this
     */
    public function modelName(string $model_name = '')
    {
        $this->model_name = $model_name;

        return $this;
    }



    /**
     * set/unset model id of the map record.
     *
     * @param int $model_id
     *
     * @return $this
     */
    public function modelId(int $model_id = 0)
    {
        $this->model_id = $model_id;

        return $this;
    }



    /**
     * set/unset group name of the map record.
     *
     * @param string $group
     *
     * @return $this
     */
    public function group(string $group = '')
    {
        $this->group = $group;

        return $this;
    }



    /**
     * set/unset change frequency of the map link.
     *
     * @param string $frequency
     *
     * @return $this
     */
    public function changeFrequency(string $frequency = MapHelper::NEVER)
    {
        if (!in_array($frequency, MapHelper::getValidFrequencies())) {
            $frequency = MapHelper::NEVER;
        }

        $this->change_frequency = $frequency;

        return $this;
    }



    /**
     * set/unset the priority of the map link.
     *
     * @param float $priority
     *
     * @return $this
     */
    public function priority($priority = 0.5)
    {
        $priority = min(1, $priority);
        $priority = max(0, $priority);

        $this->priority = $priority;

        return $this;
    }



    /**
     * set/unset the last modified date of the map link.
     *
     * @param Carbon|string|null $date
     *
     * @return $this
     */
    public function modifiedAt($date = null)
    {
        $this->modified_at = $date;

        return $this;
    }



    /**
     * set/unset the `by_mapper` flag of the map record.
     *
     * @param bool $by_mapper
     *
     * @return $this
     */
    public function byMapper(bool $by_mapper = true)
    {
        $this->by_mapper = $by_mapper;

        return $this;
    }



    /**
     * insert a new or update an existing site map record, using the data given in the chain of methods.
     *
     * @return bool
     */
    public function insert()
    {
        if (!$this->url) {
            return false;
        }

        $record = MapLink::existingOrNew($this->model_name, $this->model_id, $this->url);

        return $record->batchSaveBoolean([
             "model_name"       => $this->model_name,
             "model_id"         => $this->model_id,
             "group"            => $this->group,
             "url"              => $this->url,
             "change_frequency" => $this->change_frequency,
             "priority"         => $this->priority,
             "modified_at"      => $this->modified_at,
             "by_mapper"        => $this->by_mapper,
        ]);
    }



    /**
     * remove an existing site map record, using the selective data given in the chain of methods.
     *
     * @return bool
     */
    public function remove()
    {
        $record = MapLink::existingOrNew($this->model_name, $this->model_id, $this->url);

        return (Bool)$record->delete();
    }
}
