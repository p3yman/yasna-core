<?php

namespace Modules\SiteMap\Schedules;

use Modules\SiteMap\Services\YasnaMapper;
use Modules\Yasna\Services\YasnaSchedule;

class TableUpdaterSchedule extends YasnaSchedule
{
    /**
     * handle the scheduled job.
     *
     * @return void
     */
    protected function job()
    {
        YasnaMapper::updateTable();
    }



    /**
     * @inheritdoc
     */
    protected function frequency()
    {
        return config("site-map.xml_generator_frequency");
    }
}
