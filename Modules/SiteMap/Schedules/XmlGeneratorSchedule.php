<?php

namespace Modules\SiteMap\Schedules;

use Modules\SiteMap\Services\MapGenerator;
use Modules\Yasna\Services\YasnaSchedule;

class XmlGeneratorSchedule extends YasnaSchedule
{
    /**
     * handle the scheduled task
     *
     * @return void
     */
    protected function job()
    {
        (new MapGenerator())->generate();
    }



    /**
     * @inheritdoc
     */
    protected function frequency()
    {
        return config("site-map.xml_generator_frequency");
    }
}
