<?php

namespace Modules\SiteMap\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MapLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        mapper()::db()
                ->url("/")
                ->priority(1)
                ->changeFrequency(mapper()::WEEKLY)
                ->insert()
        ;
    }
}
