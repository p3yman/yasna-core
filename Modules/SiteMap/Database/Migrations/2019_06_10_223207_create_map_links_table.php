<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_links', function (Blueprint $table) {
            $table->increments('id');

            $table->string("model_name")->index();
            $table->unsignedInteger("model_id")->index();

            $table->string("group")->index();
            $table->integer("batch")->index();

            $table->string("url");
            $table->string("change_frequency")->default("weekly");
            $table->float("priority")->default(0.5);

            $table->timestamp("modified_at")->index()->nullable();
            $table->timestamp("placed_at")->index()->nullable();

            $table->boolean("by_mapper")->default(0);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_links');
    }
}
