<?php

return [
     'name'                    => 'SiteMap',
     "table_update_frequency"  => env("SITE_MAP_TABLE_UPDATE_FREQUENCY", "dailyAt:1:00"),
     "xml_generator_frequency" => env("SITE_MAP_XML_GENERATOR_FREQUENCY",  "dailyAt:5:00"),
];
