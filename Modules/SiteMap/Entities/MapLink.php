<?php

namespace Modules\SiteMap\Entities;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class MapLink extends YasnaModel
{
    use SoftDeletes;

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'modified_at'];



    /**
     * tries to find a current record with the given data, and return a new instance if not successful.
     *
     * @param string $model_name
     * @param int    $model_id
     * @param string $url
     *
     * @return \App\Models\MapLink
     */
    public static function existingOrNew($model_name, $model_id, $url)
    {
        /** @var Builder $builder */
        if ($model_name) {
            $builder = MapLink::where("model_name", $model_name)->where("model_id", $model_id);
        } else {
            $builder = MapLink::where("url", $url);
        }

        /** @var \App\Models\MapLink $current */
        $current = $builder->firstOrNew([]);

        return $current;
    }
}
