<?php

return [
     'name'    => 'Attendance',
     'api-key' => '760483978406f1195959ef81a90c91ef',
     //TODO: Make it compatible with the one in settings table.

     'attend-types' => [ //@TODO: Consider other types later.
                         'normal_in',
                         'normal_out',
     ],

     'attend-devices' => [ //@TODO: Consider other devices later.
                           'all',
                           'fingerprint',
                           'rfid',
                           'web_admin',
     ],
];
