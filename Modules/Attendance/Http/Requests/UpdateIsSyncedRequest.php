<?php

namespace Modules\Attendance\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class UpdateIsSyncedRequest extends YasnaRequest
{
    protected $model_name = "attend";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "id_list" => "string", // When synced 'id_list' is not 'required'
        ];
    }



    /**
     * Corrects data
     */
    public function corrections()
    {
        $this->data['id_list'] = str_replace(['[', ']'], '', $this->data['id_list']);
    }
}
