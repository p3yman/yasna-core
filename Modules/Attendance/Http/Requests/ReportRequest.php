<?php

namespace Modules\Attendance\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ReportRequest extends YasnaRequest
{
    protected $model_name = "attend";


    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.



    /**
     * @return array
     */
    public function rules()
    {
        return [
             'user'             => 'string',
             'effected_at_from' => 'date',
             'effected_at_to'   => 'date',
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'effected_at_from' => 'date',
             'effected_at_to'   => 'date',
        ];
    }
}
