<?php

namespace Modules\Attendance\Http\Requests;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class AttendSaveRequest extends YasnaRequest
{
    protected $model_name = "attend";



    /**
     * @return array
     */
    public function creationRules()
    {
        if ($this->model->exists) {
            return [];
        }

        return [
             'user_id' => "required",
        ];
    }



    /**
     * @return array
     */
    public function generalRules()
    {
        return [
             'effected_at' => [
                  'required',
                  'before:' . Carbon::now()->toDateTimeString(),
             ],
             'type'        => [
                  'required',
                  Rule::in(model('attend')->validTypes()),
             ],
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'effected_at' => 'date',
        ];
    }



    /**
     * @return array
     */
    public function attributes()
    {
        return [
             'effected_at' => trans('attendance::browse_attends.effected_at'),
        ];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             'effected_at.before' => ':attribute' . SPACE . trans('attendance::browse_attends.must_be_before_now'),
             'user_id.required'   => trans('attendance::browse_attends.user_not_found'),
        ];
    }



    /**
     * Corrects data.
     */
    public function corrections()
    {
        $this->correctUserId();
        $this->correctDevice();
    }



    /**
     * Corrects user id.
     */
    public function correctUserId()
    {
        if (!array_key_exists('email', $this->data)) {
            return;
        }

        $user = model('user')->where('email', (string)$this->data['email'])->first();

        if ($user) {
            $this->data['user_id'] = $user->id;
        }

        unset($this->data['email']);
    }



    /**
     * Corrects device.
     */
    public function correctDevice()
    {
        if ($this->model->exists) {
            return;
        }
        $this->data['device'] = 'web_admin';
    }
}
