<?php

namespace Modules\Attendance\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;
use Illuminate\Validation\Rules;

class AttendanceRequest extends YasnaRequest
{
    protected $model_name = "attend";
    protected $responder  = 'white-house';
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.



    /**
     * @return array
     */
    public function rules() //@TODO: Right? - Consider changing messages(), if anything here is changed.
    {
        return [
             "is_synced"                => "required|boolean",
             "user_id"                  => "required|integer",
             "effected_at"              => "required|date",
             "type"                     => "required|in:" . $this->allowedTypes(),
             "device"                   => "required|in:" . $this->allowedDevices(),
             "device_template_position" => "required_if:device,fingerprint|integer",
             "device_hash"              => "required_if:device,fingerprint|string",
             "device_accuracy"          => "required_if:device,fingerprint|integer",
             "rfid_unique_id"           => "required_if:device,rfid|string",
        ];
    }



    /**
     * Loads valid devices from model
     *
     * @return string
     */
    private function allowedDevices()
    {
        $array  = model('attend')->validDevices();
        $string = implode(',', $array);

        return $string;
    }



    /**
     * Loads valid types from model
     *
     * @return string
     */
    private function allowedTypes()
    {
        $array  = model('attend')->validTypes();
        $string = implode(',', $array);

        return $string;
    }



    /**
     * Corrects data
     */
    public function corrections()
    {
        if ($this->data['device'] == 'fingerprint') {
            $this->data['rfid_unique_id'] = '0';
        }

        if ($this->data['device'] == 'rfid') {
            $this->data['device_template_position'] = 5000;
            $this->data['device_hash']              = 'No hash for this device.';
            $this->data['device_accuracy']          = -1;
        }
    }
}
