<?php

namespace Modules\Attendance\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class AttendDeleteRequest extends YasnaRequest
{
    protected $model_name = "attend";
}
