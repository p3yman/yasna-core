<?php

namespace Modules\Attendance\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CanSendRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return array|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $attendance_middleware_api_key = get_setting('attendance_api');
        $api_token_in_header           = $request->header('Authorization');

        if ($attendance_middleware_api_key != $api_token_in_header) {
            return api()->errorRespond(1001);
        }
        return $next($request);
    }
}
