<?php

Route::group(
     [
          'middleware' => 'attendanceDeviceToken',
          'prefix'     => 'attendance/api/v1',
          'namespace'  => 'Modules\Attendance\Http\Controllers',
     ],
     function () {
         Route::post('/users/attends', 'AttendanceController@store');
         Route::post('/syncs/cruds', 'AttendanceController@getRemainedIsSyncedZeros');
         Route::post('/updates/is_synceds', 'AttendanceController@updateIsSyncedToOne');
         Route::post('/users/lists', 'AttendanceController@getActiveUsers');
     });

Route::group(
     [
          'middleware' => ['web', 'is:admin'],
          'prefix'     => 'manage/attendance',
          'namespace'  => 'Modules\Attendance\Http\Controllers',
     ],
     function () {
         Route::get('/', 'AttendanceBrowseController@index')->name('attends.browse');
         Route::get('browse/{request_tab?}', 'AttendanceBrowseController@index')->name('attends.browse');
         Route::post('browse/{request_tab?}', 'AttendanceBrowseController@filterResult')->name('attends.filter.result');
         Route::get('update/{hashid}', 'AttendanceBrowseController@updateRow')->name('attends.row.update');
         Route::get('edit/{hashid}', 'AttendanceBrowseController@edit')->name('attends.row.edit');
         Route::post('save', 'AttendanceBrowseController@saveSubmit')->name('attends.row.edit.submit');
         Route::get('delete/{hashid}', 'AttendanceBrowseController@delete')->name('attends.row.delete');
         Route::post('delete', 'AttendanceBrowseController@deleteSubmit')->name('attends.row.delete.submit');
         Route::get('create', 'AttendanceBrowseController@createForm')->name('attends.row.create');
     });
