<?php

namespace Modules\Attendance\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Attendance\Entities\Traits\AttendElectorTrait;
use Modules\Attendance\Http\Requests\AttendDeleteRequest;
use Modules\Attendance\Http\Requests\AttendSaveRequest;
use Modules\Attendance\Http\Requests\ReportRequest;
use Modules\Yasna\Services\YasnaController;

class AttendanceBrowseController extends YasnaController
{
    use AttendElectorTrait;

    protected $base_model  = "attend";
    protected $view_folder = "attendance::browse";
    private   $request_tab;
    //private   $method_is = 'get';
    protected $showing_results = false;
    //private   $request_keyword = null;
    private $filter = null;
    private $filter_request;
    private $user;
    private $device;
    private $effected_at_from;
    private $effected_at_to;



    /**
     * Returns $page parameter in 'index()' method.
     *
     * @return array
     */
    protected function pageInformationArray()
    {
        return $page = [
             '0' => [
                  'attendance/browse',
                  trans('attendance::browse_attends.attendance'),
             ],
             '1' => [
                  $this->request_tab,
                  trans("attendance::tabs.$this->request_tab"),
                  "attendance/browse/$this->request_tab",
             ],
        ];
    }



    /**
     * Prepares models for 'index()' method with the required queries.
     *
     * @return mixed
     */
    private function prepareModels()
    {
        $models = $this->model()->elector([
             'tab' => $this->request_tab,
        ])->orderByDesc('effected_at')->paginate(10)
        ;

        return $models;
    }



    /**
     * Returns view for index blade.
     *
     * @param string $request_tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($request_tab = 'all')
    {
        $this->request_tab = $request_tab;
        $page              = $this->pageInformationArray();
        $models            = $this->prepareModels();
        $method_is         = 'get';

        return $this->view('index', compact('page', 'models', 'request_tab', 'method_is'));
    }



    /**
     * @param ReportRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function filterResult(ReportRequest $request)
    {
        $this->request_tab    = 'filter';
        $request_tab          = $this->request_tab;
        $this->filter_request = $request;

        $page = $this->pageInformationArray();

        $models = $this->model()->elector([
             'tab'              => $this->request_tab,
             'user'             => $request->user ? $request->user : null,
             //'device'           => $this->getAttendDevice($request->device),
             'effected_at_from' => $request->effected_at_from,
             'effected_at_to'   => $request->effected_at_to,
        ])
        ;

        $durations = 0;
        foreach ($models->get() as $key => $model) {
            if ($model->isOutgoing()) {
                continue;
            }

            $durations += $model->workingTimeSeconds();
        }

        $durations       = $durations / 3600; // Duration in hours.
        $working_hours   = (int)$durations;
        $working_minutes = (int)(round($durations - floor($durations), 2) * 60);

        $models    = $models->orderBy('effected_at', 'DESC')->get();
        $method_is = 'post';

        return $this->view('index',
             compact('page', 'models', 'request_tab', 'method_is', 'working_hours', 'working_minutes'));
    }



    /**
     * @param $device
     *
     * @return null
     */
    public function getAttendDevice($device)
    {
        if ($device == 'all') {
            return null;
        }

        $valid_devices = $this->model()->validDevices();
        if (in_array($device, $valid_devices)) {
            return $device;
        } else {
            return null;
        }
    }



    /**
     * Returns 'row' blade.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function updateRow(Request $request) //@TODO: What does it update?
    {
        $model = model('attend', $request->hashid, true);

        if (!$model->exists) {
            return $this->abort(404);
        }

        return view('attendance::layouts.browse.row', compact('model'));
    }



    /**
     * Returns 'edit' blade.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $model = model('attend')->grabHashid($request->hashid);

        if (!$model->exists) {
            return $this->abort(404);
        }

        return view('attendance::browse.edit', compact('model'));
    }



    /**
     * Saves the requested data and updates rows.
     *
     * @param AttendSaveRequest $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function saveSubmit(AttendSaveRequest $request)
    {
        $model = $request->model->batchSave($request);
        $model->update(['is_synced' => 0]);

        return $this->jsonAjaxSaveFeedback($model->exists, [
             'success_message'  => trans('attendance::browse_attends.saved_successfully'),
             'success_callback' => "rowUpdate('attends-table', '$request->model_hashid')",
        ]);
    }



    /**
     * Returns 'delete' blade.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request)
    {
        $model = model('attend')->grabHashid($request->hashid);

        if (!$model->exists) {
            return $this->abort('410');
        }

        return $this->view('delete', compact('model'));
    }



    /**
     * Deletes the requested data and updates rows.
     *
     * @param AttendDeleteRequest $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteSubmit(AttendDeleteRequest $request)
    {
        $model = $request->model;

        if (!$model->exists) {
            return $this->abort('410');
        }

        $model->delete();
        $model->update(['is_synced' => 0]);

        return $this->jsonAjaxSaveFeedback($model->exists, [
             'success_message'  => trans('attendance::browse_attends.deleted_successfully'),
             'success_callback' => "rowUpdate('attends-table', '$model->hashid')",
        ]);
    }



    /**
     * Returns 'edit' blade.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm()
    {
        $model = model('attend');
        return $this->view('edit', compact('model'));
    }
}
