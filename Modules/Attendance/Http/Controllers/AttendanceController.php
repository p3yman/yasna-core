<?php

namespace Modules\Attendance\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Attendance\Http\Requests\AttendanceRequest;
use Modules\Attendance\Http\Requests\UpdateIsSyncedRequest;
use Modules\Yasna\Services\YasnaController;

class AttendanceController extends YasnaController
{
    protected $base_model = "attend";



    /**
     * Store a newly created resource in storage.
     *
     * @param AttendanceRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(AttendanceRequest $request)
    {
        $new_attend_record = model('attend')->batchSave($request);

        return api()->successRespond($new_attend_record);
    }



    /**
     * Get the remained records which their is_synced field is zero.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getRemainedIsSyncedZeros()
    {
        $is_synced_0_records = model('attend')->withTrashed()->where('is_synced', 0)->orderByDesc('id')->get();

        return api()->successRespond($is_synced_0_records);
    }



    /**
     * Update the is_synced value of the received records, to 1.
     *
     * @param UpdateIsSyncedRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateIsSyncedToOne(UpdateIsSyncedRequest $request)
    {
        $id_list_array = explode(', ', $request->id_list);

        foreach ($id_list_array as $single_id) {
            model('attend')->withTrashed()->where('id', $single_id)->update(['is_synced' => 1]);
        }

        return api()->successRespond($request->id_list);
    }



    /**
     * Get active users.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getActiveUsers()
    {
        $users_collection = user()
             ->select('id', 'email', 'name_first', 'name_last', 'code_melli')
             ->where('code_melli', '<>', '')
             ->get()
        ;
        $users            = $users_collection->toArray();
        $admin_ids        = role('manager')->users()->pluck('users.id')->toArray();
        $admins           = $users_collection->whereIn('id', $admin_ids);

        foreach ($admins as $key => $admin) {
            $users[$key]['is_admin'] = 1;
        }

        return api()->successRespond(compact('users'), []);  //@TODO: Remove [] after a fresh pull from dev.
    }
}
