<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attends', function (Blueprint $table) {
            /*-----------------------------------------------
            | Primary Key
            */
            $table->increments('id');
            /*-----------------------------------------------
            | Attendance Info
            */
            $table->boolean('is_synced')->default(0);
            $table->unsignedInteger('user_id')->index();
            $table->timestamp('effected_at')->nullable()->index();
            $table->string('type')->index();
            $table->string('device')->index();
            $table->integer('device_template_position')->default(5000); // 5000 means this 'device' is not fingerprint.
            $table->string('device_hash')->default('No hash for this device.');
            $table->integer('device_accuracy')->default(-1); // accuracy=-1 means this 'device' has no accuracy.
            $table->string('rfid_unique_id')->default(0)->index();
            /*-----------------------------------------------
            | Meta
            */
            $table->longText('meta')->nullable();
            /*-----------------------------------------------
            | Change Logs
            */
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);
            /*-----------------------------------------------
            | Indexes ...
            */
            $table->index('created_at');
            $table->tinyInteger('converted')->default(0)->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attends');
    }
}
