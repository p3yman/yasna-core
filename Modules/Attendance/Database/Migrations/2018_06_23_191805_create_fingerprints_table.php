<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFingerprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fingerprints', function (Blueprint $table) {
            /*-----------------------------------------------
            | Primary Key
            */
            $table->increments('id');
            /*-----------------------------------------------
            | Fingerprint Info
            */
            $table->integer('user_id')->index();
            $table->integer('template_position')->index();
            /*-----------------------------------------------
            | Meta
            */
            $table->longText('meta')->nullable();
            /*-----------------------------------------------
            | Change Logs
            */
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);
            /*-----------------------------------------------
            | Indexes ...
            */
            $table->index('created_at');
            $table->tinyInteger('converted')->default(0)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fingerprints');
    }
}
