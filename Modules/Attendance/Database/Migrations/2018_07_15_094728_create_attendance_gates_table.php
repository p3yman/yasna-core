<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceGatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_gates', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->index();
            $table->string('token');
            $table->string('serial_number')->index();
            $table->string('version');
            $table->timestamp('first_usage_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamp('additional_date')->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_gates');
    }
}
