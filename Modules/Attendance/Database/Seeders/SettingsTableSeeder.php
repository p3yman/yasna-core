<?php

namespace Modules\Attendance\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data[] = [
             'slug'          => 'attendance_api',
             'title'         => trans('attendance::settings_seed.title'),
             'category'      => 'upstream',
             'order'         => '555',
             'data_type'     => 'text',
             'default_value' => config('attendance.api-key'),
             'hint'          => '',
             'css_class'     => '',
             'is_localized'  => '0',
        ];

        yasna()->seed('settings', $data);
    }
}
