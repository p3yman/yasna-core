<?php

namespace Modules\Attendance\Database\Seeders;

use App\Models\Attend;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DummyTableSeeder extends Seeder
{
    private $current_device = null;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->attendsTable();
    }



    /**
     * Inserts dummy data into attends table
     *
     * @param int $total
     */
    public function attendsTable($total = 50)
    {
        $counter = 0;

        while ($counter < $total) {
            $counter++;

            $attend = new Attend;

            $attend->batchSave([
                 'is_synced'                => 0,
                 'user_id'                  => rand(1, 10),
                 'effected_at'              => $this->randomEffectedAtGenerator(),
                 'type'                     => $this->randomTypeSuffixGenerator(),
                 'device'                   => $this->randomDeviceGenerator(),
                 'device_template_position' => $this->randomDeviceTemplatePositionGenerator(),
                 'device_hash'              => $this->randomDeviceHashGenerator(),
                 'device_accuracy'          => $this->randomDeviceAccuracyGenerator(),
                 'rfid_unique_id'           => $this->randomRfidUniqueIdGenerator(),
            ]);
        }
    }



    /**
     * Returns a random timestamp between '2018-05-18 08:01:02' and '2018-6-18 08:01:02'
     *
     * @return string
     */
    public function randomEffectedAtGenerator()
    {
        $int    = rand(1526630462, 1529308862);
        $string = date("Y-m-d H:i:s", $int);

        return (string)$string;
    }



    /**
     * Adds '_in' or '_out' randomly after 'normal'
     *
     * @return string
     */
    public function randomTypeSuffixGenerator()
    {
        $suffix = ['_in', '_out'];

        return 'normal' . $suffix[array_rand($suffix)]; //@TODO: Other types but 'normal' will be added later.
    }



    /**
     * Returns a random item from $device_list
     *
     * @return string
     */
    public function randomDeviceGenerator()
    {
        $device_list          = ['fingerprint', 'rfid', 'web_admin']; //@TODO: Other devices might be added later.
        $this->current_device = (string)$device_list[array_rand($device_list)];

        return $this->current_device;
    }



    /**
     * Returns a random integer between 0 and 999 if the current device is 'fingerprint'
     * and returns 5000 if the current device is NOT 'fingerprint'
     *
     * @return int
     */
    public function randomDeviceTemplatePositionGenerator()
    {
        if ($this->current_device == 'fingerprint') {
            return rand(0, 999);
        } else {
            return 5000;
        }
    }



    /**
     * Returns a random string which its length is 64 if the current device is 'fingerprint'
     * and returns null if the current device is NOT 'fingerprint'
     *
     * @return null|string
     */
    public function randomDeviceHashGenerator()
    {
        if ($this->current_device == 'fingerprint') {
            return md5(uniqid()) . md5(uniqid());
        } else {
            return 'No hash for this device.';
        }
    }



    /**
     * Returns a random integer between 50 and 220 if the current device is 'fingerprint'
     * and returns null if the current device is NOT 'fingerprint'
     *
     * @return int|null
     */
    public function randomDeviceAccuracyGenerator()
    {
        if ($this->current_device == 'fingerprint') {
            return rand(50, 220);
        } else {
            return -1;
        }
    }



    /**
     * Returns a random number with 12 digits in string type if the current device is 'rfid'
     * and returns null if the current device is NOT 'fingerprint'
     *
     * @return null|string
     */
    public function randomRfidUniqueIdGenerator()
    {
        if ($this->current_device == 'rfid') {
            return $this->randomNumberWithSpecificLength(12); //@TODO: Consider changing 12 if other rfid type is used.
        } else {
            return 0;
        }
    }



    /**
     * @param $length
     * Generates a random number with specific $length digits in string type.
     *
     * @return string
     */
    public function randomNumberWithSpecificLength($length)
    {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }
}
