<?php

namespace Modules\Attendance\Providers;

use Modules\Attendance\Http\Middleware\CanSendRequestMiddleware;
use Modules\Yasna\Services\YasnaProvider;

class AttendanceServiceProvider extends YasnaProvider
{

    /**
     * Provider Index
     */
    public function index()
    {
        $this->registerMiddlewares();
        $this->registerAliases();
        $this->registerSidebar();
    }



    /**
     * Registers 'CanSendRequestMiddleware'
     */
    public function registerMiddlewares()
    {
        $this->addMiddleware('attendanceDeviceToken', CanSendRequestMiddleware::class);
    }



    /**
     * Registers aliases.
     */
    public function registerAliases()
    {
        $this->addAlias('Attendance', AttendanceServiceProvider::class);
    }



    /**
     * Returns available types for combo.
     *
     * @return array
     */
    public static function attendsTypesCombo()
    {
        $types = config('attendance.attend-types');
        $combo = [];

        foreach ($types as $type) {
            $combo[] = [
                 'id'      => $type,
                 'caption' => trans("attendance::browse_attends.$type"),
            ];
        }

        return $combo;
    }



    /**
     * Returns available devices for combo.
     *
     * @return array
     */
    public static function attendsDevicesCombo()
    {
        $devices = config('attendance.attend-devices');
        $combo   = [];

        foreach ($devices as $device) {
            $combo[] = [
                 'id'      => $device,
                 'caption' => trans("attendance::browse_attends.$device"),
            ];
        }

        return $combo;
    }



    /**
     * Manage Sidebar
     */
    public function registerSidebar()
    {
        service('manage:sidebar')
             ->add('attendance')
             ->blade('attendance::sidebar.attendance')
             ->order(11)
        ;
    }
}
