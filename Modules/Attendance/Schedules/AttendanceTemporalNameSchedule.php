<?php 

namespace Modules\Attendance\Schedules;

use Modules\Yasna\Services\YasnaSchedule;

class AttendanceTemporalNameSchedule extends YasnaSchedule
{
    //Feel free to incorporate additional() and condition() methods, or even override handle() method as appropriates.
    //@TODO: Register this class, in your module service provider, using ->addSchedule(Folan::class) method, then remove this TODO line.

    /**
     * Scheduled job to be run.
     */
    protected function job()
    {
        //Write the scheduled code here.
    }



    /**
     * Frequency of the Scheduled job.
     *
     * @return string
     */
    protected function frequency()
    {
        return 'dailyAt:22:00';
    }
}
