<?php namespace Modules\Attendance\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessCard extends YasnaModel
{
    use SoftDeletes;



    /**
     * Get the attendance access card that belongs to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * Returns true if the model exists and the length matches.
     * Returns false if the model does not exist or the
     * length does not match.
     *
     * @return bool
     */
    public function hasValidLength()
    {
        $standard_length = 12; //Currently we are using such cards. Consider this line if other cards are used.

        return boolval(strlen($this->unique_id) == $standard_length);
    }
}
