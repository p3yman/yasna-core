<?php namespace Modules\Attendance\Entities;

use Carbon\Carbon;
use Modules\Attendance\Entities\Traits\AttendElectorTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attend extends YasnaModel
{
    use SoftDeletes;
    use AttendElectorTrait;



    /**
     * Get the attend that belongs to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * @return \Illuminate\Database\Eloquent\Model|YasnaModel|null|static
     */
    public function getUserAttribute()
    {
        return ($this->user()->first() ?? model('user'));
    }



    /**
     * Checks whether the record is synced with the device or not.
     *
     * @return bool
     */
    public function isSynced()
    {
        return boolval($this->is_synced);
    }



    /**
     * Checks whether the record is synced with the device or not.
     *
     * @return bool
     */
    public function isNotSynced()
    {
        return !$this->isSynced();
    }



    /**
     * Checks whether an attend type is Incoming or not.
     *
     * @return bool
     */
    public function isIncoming()
    {
        $attend_type = $this->type;
        $check_type  = substr($attend_type, -3);

        return boolval($check_type == '_in');
    }



    /**
     * Checks whether an attend type is Incoming or not.
     *
     * @return bool
     */
    public function isNotIncoming()
    {
        return !$this->isIncoming();
    }



    /**
     * Checks whether an attend type is Outgoing or not.
     *
     * @return bool
     */
    public function isOutgoing()
    {
        $attend_type = $this->type;
        $check_type  = substr($attend_type, -4);

        return boolval($check_type == '_out');
    }



    /**
     * Checks whether an attend type is Outgoing or not.
     *
     * @return bool
     */
    public function isNotOutgoing()
    {
        return !$this->isOutgoing();
    }



    /**
     * Get device title.
     *
     * @return string
     */
    public function getDeviceTitleAttribute()
    {
        return (string)$this->device;
    }



    /**
     * Returns valid devices.
     *
     * @return array
     */
    public function validDevices()
    {
        return ['fingerprint', 'rfid', 'web_admin'];
    }



    /**
     * Returns valid types.
     *
     * @return array
     */
    public function validTypes()
    {
        return ['normal_in', 'normal_out'];
    }



    /**
     * Checks whether the device is 'fingerprint' or not.
     *
     * @return bool
     */
    public function isFingerprint()
    {
        if ($this->getDeviceTitleAttribute() == 'fingerprint') {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Checks whether the device is 'fingerprint' or not.
     *
     * @return bool
     */
    public function isNotFingerprint()
    {
        return !$this->isFingerprint();
    }



    /**
     * Checks whether the device is 'rfid' or not.
     *
     * @return bool
     */
    public function isRfid()
    {
        if ($this->getDeviceTitleAttribute() == 'rfid') {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Checks whether the device is 'rfid' or not.
     *
     * @return bool
     */
    public function isNotRfid()
    {
        return !$this->isRfid();
    }



    /**
     * Checks whether the type is 'normal_in' or not.
     *
     * @return bool
     */
    public function isNormalIn()
    {
        $attend_type = $this->type;

        if ($attend_type == 'normal_in') {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Checks whether the type is 'normal_in' or not.
     *
     * @return bool
     */
    public function isNotNormalIn()
    {
        return !$this->isNormalIn();
    }



    /**
     * Checks whether the type is 'normal_out' or not.
     *
     * @return bool
     */
    public function isNormalOut()
    {
        $attend_type = $this->type;

        if ($attend_type == 'normal_out') {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Checks whether the type is 'normal_out' or not.
     *
     * @return bool
     */
    public function isNotNormalOut()
    {
        return !$this->isNormalOut();
    }



    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTypeTextAttribute()
    {
        return trans("attendance::browse_attends.$this->type");
    }



    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTitle()
    {
        return trans('attendance::browse_attends.full_title', [
             'type' => $this->type_text,
             'name' => $this->user->full_name,
             'date' => ad(echoDate($this->effected_at, 'j F Y')),
             'time' => ad(echoDate($this->effected_at, 'H:i')),
        ]);
    }



    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getTitleAttribute()
    {
        return $this->getTitle();
    }



    /**
     * Gets the pure working time of a single attendance row ($this)
     *
     * @return int
     */
    public function workingTimeSeconds()
    {
        $reverse_attend = $this->reverseAttend();
        if (!$reverse_attend) {
            return 0;
        }

        $time1 = new Carbon($this->effected_at);
        $time2 = new Carbon($reverse_attend->effected_at);

        $working_seconds = $time2->diffInSeconds($time1);

        return $working_seconds;
    }



    /**
     * Returns the corresponding reverse attend of this row.
     *
     * @return \App\Models\Attend|null
     */
    public function reverseAttend()
    {
        $reverse = $this
             ->where('type', $this->getReverseAttendType())
             ->where('user_id', $this->user_id)
             ->where('effected_at', $this->adaptiveSign(), $this->effected_at)
             ->orderBy('effected_at', $this->adaptiveOrder())
             ->first()
        ;

        if ($reverse) {
            $first_date_carbon  = (new Carbon($reverse->effected_at))->toDateString();
            $second_date_carbon = (new Carbon($this->effected_at))->toDateString();

            if ($reverse and $first_date_carbon == $second_date_carbon) {
                return $reverse;
            }
        }

        return null;
    }



    /**
     * Returns adaptive sign(i.e. '>' or '<') according to the 'type' field.
     *
     * @return string
     */
    private function adaptiveSign()
    {
        if ($this->isIncoming()) {
            $sign = '>';
        } else {
            $sign = '<';
        }

        return $sign;
    }



    /**
     * Returns adaptive order(i.e. 'asc' or 'desc') according to the 'type' field.
     *
     * @return string
     */
    private function adaptiveOrder()
    {
        if ($this->isIncoming()) {
            $order = 'asc';
        } else {
            $order = 'desc';
        }

        return $order;
    }



    /**
     * Guess the opposite reverse type, based on the current model ($this)
     *
     * @return string
     */
    private function getReverseAttendType()
    {
        if ($this->isIncoming()) {
            return str_replace('_in', '_out', $this->type);
        } else {
            return str_replace('_out', '_in', $this->type);
        }
    }
}
