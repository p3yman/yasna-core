<?php namespace Modules\Attendance\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class AttendanceGate extends YasnaModel
{
    use SoftDeletes;



    /**
     * Get the attendance gate that belongs to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * Adds $year to the 'first_usage_at' field, stores it in the 'expired_at' field and
     * returns true if the model exists. Returns false if the model does not exist.
     *
     * @param int $year
     *
     * @return bool
     */
    public function setExpiredAt($year)
    {
        if (!$this->exists) {
            return false;
        }

        $first_usage_at = Carbon::parse($this->first_usage_at);
        $expired_at     = $first_usage_at->addYear($year);

        return $this->batchSaveBoolean([
             'expired_at' => $expired_at,
        ]);
    }



    /**
     * Adds $month to the 'expired_at' field, stores it in the 'additional_date' field and
     * returns true if the model exists. Returns false if the model does not exist.
     *
     * @param int $month
     *
     * @return bool
     */
    public function setAdditionalDate($month)
    {
        if (!$this->exists) {
            return false;
        }

        $expired_at      = Carbon::parse($this->expired_at);
        $additional_date = $expired_at->addMonth($month);

        return $this->batchSaveBoolean([
             'additional_date' => $additional_date,
        ]);
    }
}
