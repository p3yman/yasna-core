<?php namespace Modules\Attendance\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fingerprint extends YasnaModel
{
    use SoftDeletes;



    /**
     * Get the fingerprint that belongs to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }



    /**
     * Returns true if the model exists and the 'template_position' is in the fingerprint sensor capacity range.
     * Returns false if the model does not exist or the 'template_position' is not in the fingerprint sensor
     * capacity range.
     *
     * @return bool
     */
    public function hasValidTemplatePosition()
    {
        if (!$this->exists) {
            return false;
        }
        $standard_capacity = 1000; //Currently we are using such fingerprint sensors. Consider this line if other sensors are used.
        $template_position = $this->template_position;

        return ($template_position >= 0 && $template_position < $standard_capacity);
    }
}
