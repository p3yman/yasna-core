<?php

namespace Modules\Attendance\Entities\Traits;

trait AttendElectorTrait
{
    /**
     * Elector method for tabs.
     *
     * @param string $tab
     */
    public function electorTab($tab)
    {
        if ($tab == 'today') {
            $today_begin = now()->startOfDay();
            $today_end   = now()->endOfDay();
            $this->elector()->where('effected_at', '<', $today_end)->where('effected_at', '>', $today_begin);
        }

        if ($tab == 'incoming') {
            $this->elector()->where('type', 'normal_in'); //@TODO: Consider other types later.
        }

        if ($tab == 'outgoing') {
            $this->elector()->where('type', 'normal_out'); //@TODO: Consider other types later.
        }
    }



    /**
     * Elector for user.
     *
     * @param $user_id
     */
    public function electorUser($user_id)
    {
        $this->elector()->where('user_id', $user_id);
    }



    /**
     * Elector for device.
     *
     * @param $device
     */
    public function electorDevice($device)
    {
        $this->elector()->where('device', $device);
    }



    /**
     * Elector for effected_at_from.
     *
     * @param $effected_at_from
     */
    public function electorEffectedAtFrom($effected_at_from)
    {
        $this->elector()->where('effected_at', '>=', $effected_at_from);
    }



    /**
     * Elector for effected_at_to.
     *
     * @param $effected_at_to
     */
    public function electorEffectedAtTo($effected_at_to)
    {
        $this->elector()->where('effected_at', '<=', $effected_at_to);
    }



    /**
     * Elector method for 'is_synced'.
     *
     * @param $is_synced
     */
    public function electorIsSynced($is_synced)
    {
        $this->elector()->where('is_synced', $is_synced);
    }



    /**
     * Elector method for 'user_id'.
     *
     * @param $user_id
     */
    public function electorUserId($user_id)
    {
        $this->elector()->where('user_id', $user_id);
    }



    /**
     * Elector method for 'effected_at'.
     *
     * @param $effected_at
     */
    public function electorEffectedAt($effected_at)
    {
        $this->elector()->where('effected_at', $effected_at);
    }



    /**
     * Elector method for 'type'.
     *
     * @param $type
     */
    public function electorType($type)
    {
        $this->elector()->where('type', $type);
    }
}
