@include("manage::widgets.toolbar" , [
    'title'             => $page[0][1].' / '.$page[1][1],
    'buttons' => [
            [
                'target' => 'modal:manage/attendance/create',
                'type' => "primary",
                'caption' => trans('attendance::browse_attends.add_new_attend'),
                "class" => "btn btn-success btn-sm" ,
                'icon' => "plus-circle",
            ],
	],
    ])
