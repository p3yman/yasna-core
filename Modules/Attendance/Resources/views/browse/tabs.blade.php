@include("manage::widgets.tabs", [
    'current' => $page[1][0],
    'tabs' => [
        [
            'caption' => trans('attendance::browse_attends.all'),
            'url'     => 'all',
        ],
        [
            'caption' => trans('attendance::browse_attends.today'),
            'url'     => 'today',
        ],
        [
            'caption' => trans('attendance::browse_attends.incoming'),
            'url'     => 'incoming',
        ],
        [
            'caption' => trans('attendance::browse_attends.outgoing'),
            'url'     => 'outgoing',
        ],
        [
            'caption' => trans('attendance::browse_attends.filter'),
            'url'     => 'filter',
        ],
        //[
        //    'caption' => trans('attendance::browse_attends.search_results'),
        //    'url'     => 'search_results',
        //],
    ]
  ])