{!!
	widget('form-open')
		->class('contact-form')
		->id('new-angel-form')
		->method('post')
		->target(route('attends.filter.result'))
!!}


{!!
	 widget('combo')
			 ->id('combowidget')
			 ->name('user')
			 ->label(trans('attendance::browse_attends.person_name'))
			 ->inForm()
			 ->blankValue('')
			 ->blankCaption(trans('attendance::browse_attends.all'))
			 ->options(user()->all())
             ->searchable()
			 ->captionField('full_name')
!!}

{!!
	 widget('persian-date-picker')
		->name('effected_at_from')
		 ->label(trans('attendance::browse_attends.from'))
		 ->inForm()
!!}

{!!
	 widget('persian-date-picker')
		->name('effected_at_to')
		 ->label(trans('attendance::browse_attends.to'))
		 ->value($model->effected_at ?? Carbon\Carbon::now()->toDateTimeString())
		 ->inForm()
!!}

{!!
      widget('button')
        ->type('submit')
        ->label(trans('attendance::browse_attends.search'))
        ->value('submit')
        ->shape('default')
        ->class('btn btn-info')
        ->onClick()
 !!}


{!!
	widget('form-close')
!!}