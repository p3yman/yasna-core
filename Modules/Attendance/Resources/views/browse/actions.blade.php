@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['pencil' , trans('manage::forms.button.edit') , "modal:manage/attendance/edit/-hashid-" ],
		['trash-o' , trans('manage::forms.button.soft_delete') , "modal:manage/attendance/delete/-hashid-" , !$model->trashed()] ,
		//['recycle' , trans('manage::forms.button.undelete') , "modal:manage/posts/upstream/feature/activeness/-hashid-" , $model->trashed()],
	]
])
