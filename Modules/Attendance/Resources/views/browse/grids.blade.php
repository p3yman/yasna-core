@include("manage::widgets.grid" , [
		'headings'          => [
			trans('attendance::browse_attends.attend_info'),
		],
		'row_view'          => 'attendance::layouts.browse.row',
		'table_id'          => 'attends-table',
		'handle'            => 'counter',
		'operation_heading' => true,
	])