@extends('manage::layouts.template')

@section('content')

	@include('attendance::browse.toolbar')

	@include('attendance::browse.tabs')

	@if($request_tab == 'filter' && $method_is == 'get')
		@include('attendance::browse.filters')
	@elseif($request_tab == 'filter' && $method_is == 'post')
		@include('attendance::browse.filters')
		<div style="text-align: center; margin:0 auto;">
		{{ trans('attendance::browse_attends.working_hours_summation') }}:
		{{ $working_hours }}
		{{ trans('attendance::browse_attends.hour') }}
		{{ trans('attendance::browse_attends.and') }}
		{{ $working_minutes }}
		{{ trans('attendance::browse_attends.minute') }}
		</div>
	@endif
	@include('attendance::browse.grids')
@endsection
