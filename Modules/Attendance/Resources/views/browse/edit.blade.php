@include('manage::layouts.modal-start' , [
	'form_url' => route("attends.row.edit.submit"),
	'modal_title' => $model->exists
		? trans("attendance::browse_attends.attend_info_edit") . $model->user->full_name
		: trans('attendance::browse_attends.create_new_attend'),
])

<div class='modal-body'>

	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
		widget('input')
			->name('email')
			->condition($model->not_exists)
			->label(trans('attendance::browse_attends.email'))
			->placeholder(trans('attendance::browse_attends.email'))
			->class('form-required')
			->inForm()
			->required()
	!!}

	{!!
		 widget('persian-date-picker')
			->name('effected_at')
		 	->label(trans('attendance::browse_attends.attend_time'))
		 	->value($model->effected_at ?? Carbon\Carbon::now()->toDateTimeString())
		 	->inForm()
		 	->required()
	!!}

	{!!
		 widget('combo')
                 ->id('combowidget')
                 ->name('type')
                 ->label(trans('attendance::browse_attends.type'))
                 ->inForm()
                 ->options(Attendance::attendsTypesCombo())
//                 ->searchable()
                 ->valueField('id')
                 ->captionField('caption')
                 ->value($model->type)
                 ->required()
	!!}

</div>


<div class='modal-footer'>
	@include('manage::forms.buttons-for-modal')
</div>


@include('manage::layouts.modal-end')