@include("manage::layouts.modal-delete" , [
	'form_url' => route('attends.row.delete.submit'),
	'modal_title' => trans('attendance::browse_attends.attend_info_delete'),
	'title_value' => $model->title,
]     )
