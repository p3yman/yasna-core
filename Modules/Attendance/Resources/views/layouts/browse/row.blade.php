@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => route('attends.row.update', ['hashid' => $model->hashid]) ,
])

<td>
	{{ $model->user->full_name }}
	@include("manage::widgets.grid-date" , [
        'date' => $model->effected_at, //@TODO: How about time?
        'default' => "fixed",
        'format' => 'j F Y ' . trans('attendance::browse_attends.hour') . ' H:i',
        'color' => '#1b72e2',
        'size' => 'sm', //@TODO: Ask - Seems not valid, but works.
    ])
	{{ trans('attendance::browse_attends.' . $model->type) }}<br>
	{{ trans('attendance::browse_attends.' . $model->device) }}
</td>

<td>
	@include('attendance::browse.actions')
</td>


@if($model->trashed())
	<script>
        $('tr-{{ $model->hashid }}').addClass('deleted-content')
	</script>
@endif
