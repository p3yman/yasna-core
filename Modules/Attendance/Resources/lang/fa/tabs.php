<?php
return [
     "all"            => "همه",
     "today"          => "امروز",
     "incoming"       => "ورودی",
     "outgoing"       => "خروجی",
     "search"         => "جست‌وجو",
     "filter"         => "فیلتر",
     "search_results" => "نتایج جست‌و‌جو",
];
