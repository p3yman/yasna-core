<?php

namespace Modules\Combo\Database\Seeders;

use App\Models\Combo;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 10; $i++) {
            $this->seedOptions(dummy()::englishWord(), rand(5, 20));
        }
    }



    /**
     * seed a number of options for a given subject
     *
     * @param string $subject
     * @param int    $count
     *
     * @return int
     */
    protected function seedOptions(string $subject, int $count): int
    {
        /** @var Combo $model */
        $model = model("Combo");
        $sed = 0;

        for ($i = 1; $i <= $count; $i++) {
            $sed += $model::seed($subject, dummy()::englishWord(), $this->randomTitles());
        }

        return $sed;
    }



    /**
     * get an array of random titles
     *
     * @return array
     */
    protected function randomTitles(): array
    {
        $result = [];

        if (rand(0, 1)) {
            $result['fa'] = dummy()::persianTitle();
        }

        if (rand(0, 1)) {
            $result['en'] = dummy()::englishWord(3);
        }

        if (rand(0, 1)) {
            $result['fr'] = dummy()::englishWord(3);
        }

        return $result;
    }
}
