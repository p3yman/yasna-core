<?php

namespace Modules\Combo\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

class OptionsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List the Available Options";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Combo\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ComboController@options';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "option-1-key" => "Option 1 Caption",
             "option-2-key" => "Option 2 Caption",
             "option-3-key" => "Option 3 Caption",
        ], [
             "reached" => "OptionsEndpoint",
             "subject" => request()->subject,
             "by"      => "slug",
        ]);
    }
}
