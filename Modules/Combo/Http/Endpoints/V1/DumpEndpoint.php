<?php

namespace Modules\Combo\Http\Endpoints\V1;

use Modules\Combo\Http\Controllers\V1\ComboController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/combo-dump
 *                    Get all combos data
 * @apiDescription    Get all combos data
 * @apiVersion        1.0.0
 * @apiName           Get all combos data
 * @apiGroup          Combo
 * @apiPermission     User
 * @apiParam {String=id,slug,hashid} [by=slug] option slug
 * @apiParam {String} [lang] option language
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "by": "slug",
 *          "lang": null
 *      },
 *      "results": {
 *          "brain-hypoxia": {
 *              "brain-hypoxia-chocking": "chocking",
 *              "brain-hypoxia-hanging": "hanging",
 *              "brain-hypoxia-post-cpr": "cpr",
 *              "brain-hypoxia-drug-poisoning": "poisoning",
 *              "brain-hypoxia-post-convulsion": "convulsion",
 *              "brain-hypoxia-burn": "burn"
 *           },
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method ComboController controller()
 */
class DumpEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get all combos data";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Combo\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ComboController@getAllCombos';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
            "brain-hypoxia" => [
                 "brain-hypoxia-chocking"        => "chocking",
                 "brain-hypoxia-hanging"         => "hanging",
                 "brain-hypoxia-post-cpr"        => "cpr",
                 "brain-hypoxia-drug-poisoning"  => "poisoning",
                 "brain-hypoxia-post-convulsion" => "convulsion",
                 "brain-hypoxia-burn"            => "burn",
            ],
        ]);
    }
}
