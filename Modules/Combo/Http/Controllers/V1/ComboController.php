<?php

namespace Modules\Combo\Http\Controllers\V1;

use App\Models\Combo;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class ComboController extends YasnaApiController
{
    /**
     * proceed "combo-options" endpoint: get combo options of a given subject
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function options(SimpleYasnaRequest $request)
    {
        $by      = $this->discoverMapField($request->by);
        $subject = $request->subject;
        $lang    = $request->lang;

        $result = combo()::listMap($by, $subject, $lang);
        $meta   = compact("subject", "by", "lang");

        return $this->success($result, $meta);
    }



    /**
     * discover the map field
     *
     * @param string $asked
     *
     * @return string
     */
    protected function discoverMapField($asked): string
    {
        $asked      = strtolower($asked);
        $white_list = ['id', 'hashid', 'slug', 'key'];

        if (!in_array($asked, $white_list)) {
            return 'slug';
        }

        return $asked;
    }



    /**
     * pass all combos with subject in array
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function getAllCombos(SimpleYasnaRequest $request)
    {
        $by    = $this->discoverMapField($request->by);
        $lang  = $request->lang;
        $array = [];

        $subjects = combo()->groupBy('subject')->get()->pluck('subject');

        foreach ($subjects as $subject) {
            $array[$subject] = combo()::listMap($by, $subject, $lang);
        }

        $meta = compact("by", "lang");

        return $this->success($array, $meta);
    }


}
