<?php

namespace Modules\Combo\Entities\Traits;

trait ComboChecksTrait
{
    /**
     * check the given slug to be valid and be part of the given subject
     *
     * @param string $slug
     * @param string $subject
     *
     * @return boolean
     */
    public static function checkSubject(?string $slug, string $subject)
    {
        if (!$slug) {
            return false;
        }
        $model = combo()->grabSlug($slug);

        return $model->subject == $subject;
    }
}
