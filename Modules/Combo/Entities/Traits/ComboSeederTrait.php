<?php

namespace Modules\Combo\Entities\Traits;

trait ComboSeederTrait
{
    /**
     * seed something into the table
     *
     * @param string       $subject
     * @param string       $key
     * @param string|array $titles
     * @param int          $order
     *
     * @return int
     */
    public static function seed(string $subject, string $key, $titles, int $order = 1)
    {
        return yasna()::seed("combos", [
             [
                  "slug"        => self::generateSlug($subject, $key),
                  "subject"     => trim($subject),
                  "key"         => trim($key),
                  "meta-titles" => self::arrangeTitles($titles),
                  "order"       => $order,
             ],
        ]);
    }



    /**
     * arrange titles to make sure a qualified array is sent to the database
     *
     * @param string|array $given_titles
     *
     * @return array
     */
    protected static function arrangeTitles($given_titles): array
    {
        if (is_array($given_titles)) {
            return $given_titles;
        }

        return [
             getLocale() => $given_titles,
        ];
    }



    /**
     * generate slug from the given subject and key
     *
     * @param string $subject
     * @param string $key
     *
     * @return string
     */
    protected static function generateSlug(string $subject, string $key): string
    {
        return str_slug("$subject-$key");
    }
}
