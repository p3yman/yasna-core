<?php

namespace Modules\Combo\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class Combo
 * house the methods relative to the subjects
 * @method Builder select(... $fields)
 * @method Builder where($field, $value)
 *
 * @package Modules\Combo\Entities
 */
trait ComboSubjectsTrait
{
    /**
     * get a list of available subjects
     *
     * @return array
     */
    public static function subjects(): array
    {
        return static::select('subject')->groupBy('subject')->pluck('subject')->toArray();
    }



    /**
     * determine if the given subject is available in the table
     *
     * @param string $subject
     *
     * @return bool
     */
    public static function hasSubject(string $subject): bool
    {
        return (bool)static::where("subject", $subject)->count();
    }



    /**
     * determine if the given subject is available in the table
     *
     * @param string $subject
     *
     * @return bool
     */
    public static function hasnotSubject(string $subject): bool
    {
        return !static::hasSubject($subject);
    }
}
