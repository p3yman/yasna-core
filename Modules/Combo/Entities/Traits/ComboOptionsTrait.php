<?php

namespace Modules\Combo\Entities\Traits;

use App\Models\Combo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Combo
 * house the methods relative to the options
 * @method Builder where($field, $value)
 *
 * @package Modules\Combo\Entities
 */
trait ComboOptionsTrait
{
    /**
     * get a list of paired id=>title values of the given subject in the given locale.
     *
     * @param string $subject
     * @param string $locale
     *
     * @return array
     */
    public static function idList($subject, $locale = null): array
    {
        return static::listMap("id", $subject, $locale);
    }



    /**
     * get a list of paired hashid=>title values of the given subject in the given locale.
     *
     * @param string $subject
     * @param string $locale
     *
     * @return array
     */
    public static function hashidList($subject, $locale = null): array
    {
        return static::listMap("hashid", $subject, $locale);
    }



    /**
     * get a list of paired key=>title values of the given subject in the given locale.
     *
     * @param string $subject
     * @param string $locale
     *
     * @return array
     */
    public static function keyList($subject, $locale = null): array
    {
        return static::listMap("key", $subject, $locale);
    }


    /**
     * get a list of paired slug=>title values of the given subject in the given locale.
     *
     * @param string $subject
     * @param string $locale
     *
     * @return array
     */
    public static function slugList($subject, $locale = null): array
    {
        return static::listMap("slug", $subject, $locale);
    }

    /**
     * get a list of paired $map_field=>title values of the given subject in the given locale.
     *
     * @param string $map_field
     * @param string $subject
     * @param string $locale
     *
     * @return array
     */
    public static function listMap(string $map_field, $subject, $locale = null): array
    {
        $locale = static::getLocale($locale);
        $return = [];

        /** @var Combo $combo */
        foreach (static::options($subject) as $combo) {
            $value = $combo->titleIn($locale);
            if ($value) {
                $return[$combo->$map_field] = $value;
            }
        }

        return $return;
    }



    /**
     * get the given locale, provided that the current site locale is returned if nothing was given.
     *
     * @param string $locale
     *
     * @return string
     */
    protected static function getLocale($locale = null): string
    {
        if (!$locale) {
            $locale = getLocale();
        }

        return $locale;
    }



    /**
     * get a collection of options relating to a given subject.
     *
     * @param string $subject
     *
     * @return Collection
     */
    protected static function options($subject)
    {
        return static::where('subject', $subject)->orderBy('order')->get();
    }
}
