<?php

namespace Modules\Combo\Entities\Traits;

use App\Models\Combo;

trait ComboResolveTrait
{
    /**
     * get the title of the combo option
     *
     * @param string $key
     * @param null   $locale
     * @param string $by
     *
     * @return null|string
     */
    public static function resolve(string $key, $locale = null, string $by = "slug")
    {
        $record = static::resolveRecord($key, $by);

        if ($locale) {
            return $record->titleIn($locale);
        }

        return $record->getTitleInCurrentLocale();
    }



    /**
     * check if the combo option exists
     *
     * @param string $key
     * @param null   $locale
     * @param string $by
     *
     * @return bool
     */
    public static function checkExistence(string $key, $locale = null, string $by = "slug")
    {
        return boolval(static::resolve($key, $locale, $by));
    }



    /**
     * resolve record by the given handles
     *
     * @param string $key
     * @param string $by
     *
     * @return Combo
     */
    private static function resolveRecord(string $key, string $by = "slug")
    {
        if ($by == "id") {
            $record = combo()->grabId($key);
        } elseif ($by == "hashid") {
            $record = combo()->grabHashid($key);
        } elseif ($by == "slug") {
            $record = combo()->grabSlug($key);
        } else {
            $record = combo()->grab($key);
        }

        return $record;
    }
}
