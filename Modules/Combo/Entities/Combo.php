<?php 

namespace Modules\Combo\Entities;

use Modules\Combo\Entities\Traits\ComboChecksTrait;
use Modules\Combo\Entities\Traits\ComboOptionsTrait;
use Modules\Combo\Entities\Traits\ComboResolveTrait;
use Modules\Combo\Entities\Traits\ComboSeederTrait;
use Modules\Combo\Entities\Traits\ComboSubjectsTrait;
use Modules\Yasna\Services\ModelTraits\YasnaLocaleTitlesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Combo extends YasnaModel
{
    use SoftDeletes;
    use YasnaLocaleTitlesTrait;

    use ComboSeederTrait;
    use ComboSubjectsTrait;
    use ComboOptionsTrait;
    use ComboResolveTrait;
    use ComboChecksTrait;
}
