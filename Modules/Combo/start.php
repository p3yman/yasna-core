<?php

if (!function_exists("combo")) {
    /**
     * Model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Combo
     */
    function combo($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Combo $model */
        $model = model('Combo', $id_or_hashid, $with_trashed);

        return $model;
    }
}
