<?php

namespace Modules\Combo\Providers;

use Modules\Combo\Http\Endpoints\V1\DumpEndpoint;
use Modules\Combo\Http\Endpoints\V1\OptionsEndpoint;
use Modules\Yasna\Services\YasnaProvider;
use Illuminate\Support\Facades\Validator;
use  \Illuminate\Validation\Validator as ValidatorInstance;

/**
 * Class ComboServiceProvider
 *
 * @package Modules\Combo\Providers
 */
class ComboServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
        $this->registerValidationRules();
    }



    /**
     * register the endpoints only when the `Endpoints` module is available.
     *
     * @return void
     */
    private function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }

        endpoint()->register(OptionsEndpoint::class);
        endpoint()->register(DumpEndpoint::class);
    }



    /**
     * register validation rules.
     *
     * @return void
     */
    private function registerValidationRules()
    {
        Validator::extend("combo", function ($attribute, $value, $parameters, ValidatorInstance $validator) {
            $message = trans("combo::validation.subject", [
                 "name" => $validator->getDisplayableAttribute($attribute),
            ]);

            $validator->setFallbackMessages([$message]);

            return combo()::checkSubject($value, $parameters[0]);
        });
    }
}
