<?php

namespace Modules\UserAvatar\Providers;

use App\Models\User;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class UserAvatarServiceProvider
 *
 * @package Modules\UserAvatar\Providers
 */
class UserAvatarServiceProvider extends YasnaProvider
{



    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->addServices();
        $this->registerUserProfile();
    }



    /**
     * add avatar uploader to any place
     */
    public function addServices()
    {
        $this->userServices();
        $this->manageServices();
    }



    /**
     * add avatar uploader to users module
     */
    private function userServices()
    {
        module('users')
             ->service('browse_headings_handlers')
             ->add()
             ->method("UserAvatar:UserHandleController@browsColumns")
        ;

        module('users')
             ->service('browse_top_blade')
             ->add('filemanager_assets')
             ->blade("useravatar::filemanager_assets")
        ;

    }



    /**
     * add avatar uploader to manage module
     */
    private function manageServices()
    {
        module("manage")
             ->service("account_personal_settings")
             ->add("avatar")
             ->order(1)
             ->blade("useravatar::manage")
        ;

        module('manage')
             ->service('user_info_sidebar')
             ->add('user_info_picture')
             ->blade('useravatar::sidebar.show-avatar')
             ->order(1)
        ;
    }



    /**
     * Registers the avatar for the user's profile.
     */
    protected function registerUserProfile()
    {
        if(!function_exists("userProfile")) {
            return;
        }

        userProfile()->setAvatarParser(
             function (User $user) {
                 return userAvatar($user);
             }
        );
    }
}
