@php
	$uploader1 = fileManager()
		->uploader(posttype('users_avatar'))
		->posttypeConfig('featured_image')
		->resultInputSelector("#avatar_hidden")
		->jsVariableName('avatarUploader')
@endphp

@if($without_assets)
	@php
		$uploader1->withoutGeneralAssets();
	@endphp
@endif
<script>
    var csrfToken = "{{ csrf_token() }}";
</script>
{!! $uploader1->renderCss() !!}
{!! $uploader1->renderJs() !!}
{!! $uploader1->render() !!}