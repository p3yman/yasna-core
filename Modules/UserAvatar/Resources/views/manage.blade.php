<div class="panel panel-default w80 mv30">
	<div class="panel-heading">
		<i class="fa fa-user"></i>
		<span class="mh5">
				{{trans("useravatar::avatar.title")}}
			</span>
	</div>

	<div class="panel-body p10">
		@include("useravatar::index",
			[
		  "image" => userAvatar(user()),
		 "show_delete_button" => userHasAvatar(user()),
		  "user_hashid" => user()->hashid,
		  "value" => userHasAvatar(user()) ? user()->avatar : '',
		]
		)
	</div>
</div>

