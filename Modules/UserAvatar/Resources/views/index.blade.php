@php
	if (isset($image) and  $image){
		$upload_image = $image;
	}else{
		$upload_image = Module::asset('manage:images/user/avatar-default.jpg');
	}

if (empty($without_assets)){
  $without_assets=false;
}
@endphp

<div class="text-center">
	<div class="pv-lg">
		{{-- Avatar Size: 128*128 --}}
		<img id="avatar-img" src="{{ $upload_image }}" alt="Contact"
			 class="center-block img-circle img-responsive img-thumbnail thumb128">
	</div>


	{!!
		widget('button')
		->id('delete-btn')
		->value('delete')
		->label('tr:useravatar::avatar.delete_avatar')
		->icon('trash')
		->class('btn-link')
	 !!}

	<div id="avatar-uploader">
		@include("useravatar::avatar-uploader")
	</div>


	{{
     	widget('hidden')
		->name('_avatar')
		->id('avatar_hidden')
		->value()
	}}

	{{
	 widget('hidden')
	->name('_user_hashid')
	->id('user_hashid')
	->value($user_hashid)
     }}

	<div style="margin-top: 8px">
		{!!
			widget('button')
			->label('tr:useravatar::avatar.accept_new_avatar')
			->id('save-btn')
			->class('hidden btn-success')
			->icon('check')
		 !!}
	</div>
</div>


<script>
	@if($show_delete_button)
    showDeleteBtn();
	@else
    hiddenDeleteBtn();
	@endif
    $("#save-btn").click(function () {
        $.ajax({
            url    : url('/users/save/avatar'),
            method : "GET",
            data   : {
                avatar   : $("#avatar_hidden").val(),
                user_hash: $("#user_hashid").val(),
                _submit  : "save"
            },
            success: function (data) {
                returnedJson = $.parseJSON(data);
                runCallBack(returnedJson.callback);
                hiddenUploadBtn();
                showDeleteBtn();
                $.notify("{{trans('useravatar::avatar.avatar_saved')}}", {status: 'success'});
                changeAvatar(returnedJson.avatar_url);
            },
            error  : function () {
                $.notify("{{trans('useravatar::avatar.avatar_error')}}", {status: 'warning'});
            }
        });
    });


    $("#delete-btn").click(function () {
        $.ajax({
            url    : url('/users/delete/avatar'),
            method : "GET",
            data   : {
                avatar   : $("#avatar_hidden").val(),
                user_hash: $("#user_hashid").val(),
                _submit  : "delete"
            },
            success: function (data) {
                returnedJson = $.parseJSON(data);
                runCallBack(returnedJson.callback);
                hiddenDeleteBtn();
                hiddenUploadBtn();
                $.notify("{{trans('useravatar::avatar.avatar_deleted')}}", {status: 'success'});
                changeAvatar(returnedJson.avatar_url);
            },
            error  : function () {
                $.notify("{{trans('useravatar::avatar.avatar_error')}}", {status: 'warning'});
            }
        });
    });


    //purify avatar hashid
    //avatar hash id by default is a json  this operations is for clear hashid from json
    $(document).ready(function () {
        //remove current file from drop zone
        $(".dropzone").mousedown(function () {
            avatarUploader.removeAllFiles();
        });

        var avatar = $("#avatar_hidden").val();
        setInterval(function () {
            if (avatar !== $("#avatar_hidden").val()) {
                avatar = $("#avatar_hidden").val();
                if (IsJsonString(avatar)) {
                    showUploadBtn();
                    $("#avatar_hidden").val($.parseJSON(avatar)[0]);
                }
            }
        }, 1);

        //default value for avatar hidden input (file manager remove it in load page)
		@if(isset($value))
        setTimeout(function () {
            $("#avatar_hidden").val("{{$value}}");
        }, 100);
		@endif
    });

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }


    function showUploadBtn() {
        $("#save-btn").removeClass('hidden').fadeIn("fast");
    }

    function hiddenUploadBtn() {
        $("#save-btn").addClass('hidden');
    }

    function showDeleteBtn() {
        $("#delete-btn").removeClass('hidden').fadeIn("fast");
    }

    function hiddenDeleteBtn() {
        $("#delete-btn").addClass('hidden');
    }

    /**
     *
     * @param function callback
     **/
    function runCallBack(callback) {
        if (callback !== "")
            eval(callback);
    }

    /**
     * change user avatar in ui and reload uploader
     *
     * @param string avatar_url
     */
    function changeAvatar(avatar_url) {
        avatarUploader.removeAllFiles();
        $("#avatar-img").attr('src', avatar_url);
    }
</script>
