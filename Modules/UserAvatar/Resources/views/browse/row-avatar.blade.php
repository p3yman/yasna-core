<img src="{{ userAvatar($model) }}" alt="Image"
	 class="media-box-object img-circle thumb48"
	 @if($model->canEdit())
	 	style="cursor: pointer"
	 	onclick="masterModal('{{ url('users/avatar/'.$model->hash_id) }}','md')"
		@endif
>

