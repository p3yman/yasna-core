{!! widget('modal')->class('pb-lg') !!}

@include('useravatar::index',[
	"image" =>  userAvatar($hashid),
	"show_delete_button" => userHasAvatar($hashid),
	'user_hashid'=>$hashid,
	"value" => userHasAvatar($hashid) ? $hashid : '',
	'without_assets'=> true
])


@include("manage::widgets101.feed",['container_id'=>"",'container_class'=>'js','container_style'=>''])


<div class="modal-footer pv-lg">
	{!! widget('button')->id('edit-btn')->value('save')->label(trans('users::forms.submit'))->class('btn-success')!!}
</div>


<script>
    $(document).ready(function () {
        $("#edit-btn").click(function () {
            $(".modal").modal("hide");
            rowUpdate('tblUsers', $("#user_hashid").val());
        });
    });
</script>