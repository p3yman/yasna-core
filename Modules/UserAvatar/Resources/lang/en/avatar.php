<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/19/18
 * Time: 11:11 AM
 */

return [
     "title"             => "Change Avatar",
     "accept_new_avatar" => "Accept New Avatar",
     "delete_avatar"     => "Delete Avatar",
     "avatar_saved"      => "New avatar has been saved.",
     "avatar_deleted"    => "Avatar has been removed.",
     "avatar_error"      => "An error occurred.",
     "avatar"            => "Avatar",
];
