<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/19/18
 * Time: 11:11 AM
 */

return [
     "title"             => "تغییر آواتار",
     "accept_new_avatar" => "تأیید عکس آورتار",
     "delete_avatar"     => "حذف عکس",
     "avatar_saved"      => "آواتار ذخیره ‌شد",
     "avatar_deleted"    => "آواتار حذف شد",
     "avatar_error"      => "مشکلی در سرور به‌ وجود آمد",
     "avatar"            => "نمایه",
];