<?php

Route::group([
	'middleware' => 'web',
	'prefix' => 'users',
	'namespace' => 'Modules\UserAvatar\Http\Controllers'
], function () {
    Route::get('save/avatar', 'AvatarController@save');
    Route::get('/avatar', 'AvatarController@dispatch')->name('avatar-operation');
    Route::get('delete/avatar', 'AvatarController@delete');
    Route::get("avatar-uploader",'AvatarController@getUploader');
    Route::get('/avatar/{hashid}', 'AvatarController@avatarModal');
    //test view of uploader don't  remove it
    Route::get('/test',"AvatarController@test");
});
