<?php

namespace Modules\UserAvatar\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class UserHandleController extends YasnaController
{
    /**
     * add avatar col to users grid
     *
     * @param $arguments
     */
    public static function browsColumns($arguments)
    {
        $request_role = $arguments['request_role'];
        //dd($arguments);
        module('users')
             ->service('browse_headings')
             ->add('avatar')
             ->trans("useravatar::avatar.avatar")
             ->blade("useravatar::browse.row-avatar")
             ->width('100')
             ->order(1)
        ;
    }
}
