<?php

namespace Modules\UserAvatar\Http\Controllers;

use Modules\UserAvatar\Http\Requests\AvatarRequest;
use Modules\Yasna\Services\YasnaController;

class AvatarController extends YasnaController
{

    /**
     * save avatar in edit modal
     *
     * @param AvatarRequest $request
     *
     * @return string
     */
    public function save(AvatarRequest $request)
    {
        $avatar_hash = $request->get('avatar');
        $user_hash   = $request->get('user_hash');
        $user        = model('user', $user_hash);
        if (!$user->canEdit()) {
            abort('403');
        }
        $is_saved = $user->batchSave([
             "avatar" => $avatar_hash,
        ]);

        if ($is_saved) {
            fileManager()->file($avatar_hash)->permanentSave();
        }
        //$response['callback']   = "alert('test')";
        $response['avatar_url'] = userAvatar($user_hash);
        return json_encode($response);
    }



    /**
     * delete user avatar image
     *
     * @param AvatarRequest $request
     *
     * @return string
     */
    public function delete(AvatarRequest $request)
    {
        $user_hash   = $request->get('user_hash');
        $user_model  = model('user', $user_hash);
        $avatar_hash = $user_model->avatar;
        if (!$user_model->canEdit()) {
            abort('403');
        }
        $is_saved = $user_model->batchSave([
             "avatar" => "",
        ]);
        if ($is_saved) {
            fileManager()->file($avatar_hash)->delete();
        }

        //$response['callback']="alert('test')";
        $response['avatar_url'] = userAvatar($user_hash);
        return json_encode($response);
    }



    /**
     * test view of avatar uploader
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function test()
    {
        if (!dev()) {
            abort(403);
        }
        $view_data['image']              = userAvatar(user()->hashid);
        $view_data['show_delete_button'] = userHasAvatar(user()->hashid);
        $view_data['user_hashid']        = user()->hashid;
        $view_data["value"]              = userHasAvatar(user()->hashid) ? user()->avatar : '';
        //dd($view_data);
        return view("useravatar::test", $view_data);
    }



    /**
     * Views Avatar Modal
     *
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function avatarModal($hashid)
    {
        $view_data['hashid'] = $hashid;
        $view_data['image']  = userAvatar($hashid);

        return view('useravatar::modal.avatar-edit', $view_data);
    }

}
