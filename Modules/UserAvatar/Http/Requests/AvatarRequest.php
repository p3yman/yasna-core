<?php

namespace Modules\UserAvatar\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class AvatarRequest extends YasnaRequest
{

    /**
     * rules when submit is delete
     *
     * @return array|bool
     */
    public function deleteRules()
    {
        if ($this->data['_submit'] == 'delete') {

            return [
                 'user_hash' => 'required',
            ];

        }
    }



    /**
     * rules when submit is save
     *
     * @return array|bool
     */
    public function editRules()
    {
        if ($this->data['_submit'] == 'save') {
            return [
                 'avatar'    => 'required',
                 'user_hash' => 'required',
            ];
        }
    }



    /**
     * rules message
     *
     * @return array
     */
    public function messages()
    {
        return [
             'avatar.required'    => trans('users::avatar.required'),
             'user_hash.required' => trans('users::avatar.user_hash.required'),
        ];
    }
}
