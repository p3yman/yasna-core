<?php

namespace Modules\UserAvatar\Database\Seeders;

use Illuminate\Database\Seeder;

class PosttypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->makePostType();
        $this->makeUploadConfig();
        $this->makeRelation();
    }



    /**
     * create posttype for upload config
     */
    private function makePostType()
    {
        $data[] = [
             'slug'           => 'users_avatar',
             'title'          => trans('users::avatar.title'),
             'order'          => '0',
             'singular_title' => trans('users::avatar.title'),
             'template'       => 'album',
             'icon'           => 'image',

        ];
        yasna()->seed('posttypes', $data);
    }



    /**
     * create upload config
     */
    private function makeUploadConfig()
    {
        $data[] = [
             'slug'          => 'avatar_upload_configs',
             'title'         => trans('users::avatar.title'),
             'category'      => '',
             'order'         => '20',
             'data_type'     => '',
             'default_value' => $this->readUploadConfigFromJson(),
             'hint'          => '',
             'css_class'     => '',
             'is_localized'  => '0',
        ];
        yasna()->seed('settings', $data);
    }



    /**
     * final operation for add upload config
     */
    private function makeRelation()
    {
        $posttype = posttype('users_avatar');
        posttype('users_avatar')->attachFeatures('featured_image');
        $posttype->attachRequiredInputs();
        $posttype->batchSave([
             'featured_image_upload_configs' => setting('avatar_upload_configs')->id
        ]);
    }



    /**
     * read upload configs from resources with the given file name.
     *
     * @return string
     */
    protected function readUploadConfigFromJson(): string
    {
        $file_path    = implode(DIRECTORY_SEPARATOR, [
             module('UserAvatar')->getPath(),
             'Resources',
             'json',
             "avatar_uploader_config.json",
        ]);
        $file_content = file_get_contents($file_path);
        $json_string  = json_encode(json_decode($file_content, true));

        return $json_string;
    }
}
