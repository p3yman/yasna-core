<?php

namespace Modules\UserAvatar\Database\Seeders;

use Illuminate\Database\Seeder;

class UserAvatarDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();
         $this->call(PosttypeTableSeeder::class);
    }
}
