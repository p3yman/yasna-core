<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}


if (!function_exists('userAvatar')) {
    /**
     * Todo: this is not best idea it must remove to users model
     * get user avatar
     *
     * @param string|\App\Models\User $param
     *
     * @return string
     */
    function userAvatar($param)
    {
        $avatar = is_string($param) ? model('user', $param)->avatar : $param->avatar;
        if (empty($avatar)) {
            $avatar = \Nwidart\Modules\Facades\Module::asset('manage:images/user/avatar-default.jpg');
        } else {
            $avatar = fileManager()->file($avatar)->version('thumb')->getUrl();
        }
        return $avatar;
    }
}

if (!function_exists('userHasAvatar')) {
    /**
     * Todo: this is not best idea it must remove to users model
     * specific that user has avatar
     *
     * @param string|\App\Models\User $param
     *
     * @return bool
     */
    function userHasAvatar($param)
    {
        $avatar = is_string($param) ? model('user', $param)->avatar : $param->avatar;
        if (empty($avatar)) {
            return false;
        } else {
            return true;
        }
    }
}
