<?php

namespace Modules\Ratings\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class ReceiveRequest extends YasnaRequest
{
    protected $responder  = 'white-house';
    protected $model_name = "rating";

    protected $model_not_found = false;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'model_id',
             'model_class_name',
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'model_id'         => 'required',
             'model_class_name' => 'required',
             'model_not_found'  => $this->model_not_found == false ? '' : 'accepted',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'model_not_found.accepted' => trans_safe('ratings::messages.model_not_found'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'model_class_name' => trans('ratings::messages.model'),
             'model_id'         => trans('ratings::messages.model_id'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {

        // Bypass if model_name not exist
        //if ($this->filledField()) {
        //    dd(123);
        //    return true;
        //}

        return true;
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'model_id'         => 'dehash',
             'model_class_name' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        if (!$this->issetModel()) {
            return;
        }

        if (!$this->modelExist() or !$this->issetRatingsTrait()) {
            $this->model_not_found = true;

            return;
        }


        $this->data['id']    = $this->getRequestedModelId($id_or_hashid);
        $this->data['model'] = $this->model = model($this->data['model_class_name'], $this->data['model_id'],
             $this->model_with_trashed);

        $this->refillRequestHashid();
        $this->failIfModelNotExist();
    }



    /**
     * check model_name and model_id is set
     *
     * @return bool
     */
    protected function issetModel(): bool
    {
        return $this->isset('model_class_name') and $this->isset('model_id');
    }



    /**
     * check model is exist
     *
     * @return bool
     */
    protected function modelExist(): bool
    {
        $model_name  = studly_case($this->data['model_class_name']);
        $model_class = MODELS_NAMESPACE . $model_name;

        return class_exists($model_class);
    }



    /**
     * check RatingTrait use in module
     *
     * @return bool
     */
    protected function issetRatingsTrait(): bool
    {
        $model = model($this->data['model_class_name']);

        return method_exists($model, 'rates');
    }



    /**
     * check model is set and model_not_found is false
     *
     * @return bool
     */
    protected function filledField(): bool
    {
        return $this->issetModel() and $this->model_not_found == false;
    }
}
