<?php

namespace Modules\Ratings\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Ratings\Http\Controllers\V1\FrontController;

/**
 * @api               {POST}
 *                    /api/modular/v1/ratings-send
 *                    Send rating for model
 * @apiDescription    Send rating for model
 * @apiVersion        1.0.0
 * @apiName           send rating for model
 * @apiGroup          Ratings
 * @apiPermission     None
 * @apiParam {String} model_class_name  Name of Model
 * @apiParam {String} model_id          Hashid of entity of model
 * @apiParam {Int}    rate              Number for rate
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      },
 *      "results": {
 *              "id"           => 2,
 *              "model"        => "post",
 *              "model_id"     => 12,
 *              "user_id"      => 1,
 *              "user_ip"      => "127.0.0.1",
 *              "rate"         => 3.2,
 *              "rate_percent" => 0,
 *              "created_at"   => "2019-02-06 11:20:08",
 *              "updated_at"   => "2019-02-06 11:20:08",
 *              "deleted_at"   => null,
 *              "created_by"   => 1,
 *              "updated_by"   => 0,
 *              "deleted_by"   => 0,
 *              "meta"         => [],
 *              "converted"    => 0,
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  FrontController controller()
 */
class SendEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Send Rating For Model";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Ratings\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'FrontController@send';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return [
             "id"           => 2,
             "model"        => "post",
             "model_id"     => 12,
             "user_id"      => 1,
             "user_ip"      => "127.0.0.1",
             "rate"         => 3.2,
             "rate_percent" => 0,
             "created_at"   => "2019-02-06 11:20:08",
             "updated_at"   => "2019-02-06 11:20:08",
             "deleted_at"   => null,
             "created_by"   => 1,
             "updated_by"   => 0,
             "deleted_by"   => 0,
             "meta"         => [],
             "converted"    => 0,
        ];
    }
}
