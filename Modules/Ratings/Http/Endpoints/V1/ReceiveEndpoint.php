<?php

namespace Modules\Ratings\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Ratings\Http\Controllers\V1\FrontController;

/**
 * @api               {GET}
 *                    /api/modular/v1/ratings-receive
 *                    Receive rates
 * @apiDescription    Receive rates
 * @apiVersion        1.0.0
 * @apiName           Receive rates
 * @apiGroup          Ratings
 * @apiPermission     None
 * @apiParam {String} model_id          Hashid of entity of model
 * @apiParam {String} model_class_name  Name of Model
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *              "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *              "rate": 2.404,
 *              "model_id": "qKXVA",
 *              "model_class_name": "post"
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  FrontController controller()
 */
class ReceiveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Receive Rates";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Ratings\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'FrontController@receive';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "rate"             => 2.404,
             "model_id"         => "qKXVA",
             "model_class_name" => "post",
        ], [
             "authenticated_user" => user()->hashid,
        ]);
    }
}
