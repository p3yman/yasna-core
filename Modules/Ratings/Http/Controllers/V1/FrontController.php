<?php

namespace Modules\Ratings\Http\Controllers\V1;

use Modules\Ratings\Http\Requests\V1\ReceiveRequest;
use Modules\Ratings\Http\Requests\V1\SendRequest;
use Modules\Yasna\Services\YasnaApiController;

class FrontController extends YasnaApiController
{

    /**
     * send ratings for model
     *
     * @param SendRequest $request
     *
     * @return \Modules\Yasna\Services\YasnaModel
     */
    public function send(SendRequest $request)
    {
        $ip   = $request->user_ip;
        $rate = $request->rate;

        if ($request->user_id) {
            $user = $request->user_id;
            return $request->model->addRate($rate, $user, $ip);
        }
        return $request->model->addRate($rate, $ip);

    }



    /**
     * receive ratings
     *
     * @param ReceiveRequest $request
     *
     * @return array
     */
    public function receive(ReceiveRequest $request)
    {
        return $this->success([
             'rate'             => $request->model->getRatingAverage(),
             'model_id'         => hashid($request->model_id),
             'model_class_name' => $request->model_class_name,
        ]);
    }

}
