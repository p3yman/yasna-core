<?php

return [
     'model_not_found' => 'داده ارسال‌شده اشتباه است.',
     'model'           => 'مدل',
     'rate'            => 'امتیاز',
     'model_id'        => 'شناسه مدل',
];
