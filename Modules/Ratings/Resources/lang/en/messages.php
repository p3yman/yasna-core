<?php

return [
     'model_not_found' => 'The submitted data is invalid.',
     'model'           => 'Model',
     'rate'            => 'Rate',
     'model_id'        => 'Model ID',
];
