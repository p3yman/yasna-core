<?php

namespace Modules\Ratings\Providers;

use Modules\Ratings\Http\Endpoints\V1\ReceiveEndpoint;
use Modules\Ratings\Http\Endpoints\V1\SendEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class RatingsServiceProvider
 *
 * @package Modules\Ratings\Providers
 */
class RatingsServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();

        //module('yasna')->service('traits')
        //               ->add()->trait('Ratings:TestTrait')->to('user')
        //;
    }



    /**
     * Registers the endpoints only when the `Endpoint` module is available.
     *
     * @return void
     */
    public function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(SendEndpoint::class);
        endpoint()->register(ReceiveEndpoint::class);
    }
}
