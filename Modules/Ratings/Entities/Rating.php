<?php

namespace Modules\Ratings\Entities;

use App\Models\User;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rating extends YasnaModel
{
    use SoftDeletes;
}
