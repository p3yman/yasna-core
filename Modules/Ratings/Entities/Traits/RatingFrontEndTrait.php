<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/26/18
 * Time: 11:07 AM
 */

namespace Modules\Ratings\Entities\Traits;


trait RatingFrontEndTrait
{
    /**
     * get stars of a model
     *
     * @return float
     */
    public function getStars()
    {
        return $this->getRatingAverage();
    }



    /**
     * get count of specific rate for example if you wanna know that how many person rated 5 to this model
     *
     * @param float $rate
     *
     * @return int
     */
    public function getCountOfSpecificRate($rate)
    {
        return $this->rates()->where('rate', $rate)->count();
    }



    /**
     * get percent of average
     *
     * @return int|float
     */
    public function getRatingAveragePercent()
    {
        return $this->getRatePercent($this->getRatingAverage());
    }



    /**
     * get percent of average
     *
     * @param date $start_time
     * @param date $end_time
     *
     * @return int|float
     */
    public function getRatingAveragePercentWithin($start_time, $end_time)
    {
        return $this->getRatePercent($this->getRatingAverageWithin($start_time, $end_time));
    }
}