<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/21/18
 * Time: 4:57 PM
 */

namespace Modules\Ratings\Entities\Traits;


trait  RatingDefaultValueTrait
{
    /**
     * specify max value of rate
     *
     * @return int
     */
    public function maxRateValue()
    {
        return 5;
    }



    /**
     * specify that user guest can rate it or no
     *
     * @return bool
     */
    public function acceptGuest()
    {
        return true;
    }



    /**
     * specify that what roles can rate it
     *
     * @return string
     */
    public function acceptRoles()
    {
        return "all";
    }
}