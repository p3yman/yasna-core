<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/25/18
 * Time: 1:01 PM
 */

namespace Modules\Ratings\Entities\Traits;

use App\Models\User;
use Mockery\Exception;
use Modules\Ratings\Entities\Rating;

trait AddRateTrait
{
    /**
     * Add rate to a model
     *
     * @param     int    $rate value of your rate
     * @param     string $user it can be user id or a valid ip
     * @param     string $ip   user ip (optional) when you pass id you can pass ip with this parameter
     *
     * @return Rating
     */
    public function addRate($rate, $user, $ip = null)
    {
        if ($rate > $this->maxRateValue()) {
            throw new Exception("value of rate is bigger than max value");
        }

        $user_obj = model('user', $user);
        if ($user_obj->exists) {
            return $this->addRateWithRegisteredUser($rate, $user_obj, $ip);
        } else {
            return $this->addRateWithGuestUser($rate, $user);
        }
    }



    /**
     * add rate to model with registered user
     *
     * @param    int    $rate
     * @param    User   $user
     * @param    string $ip (optional)
     *
     * @return Rating
     */
    private function addRateWithRegisteredUser($rate, $user, $ip = null)
    {
        $model_name = $this->modelName();

        if ($this->acceptRoles() != "all") {
            $this->checkUserRoles($user);
        }

        $rating_obj = $this->rates()
                           ->where('user_id', $user->id)
                           ->first()
        ;
        if (!$rating_obj) {
            $rating_obj = model('rating');
        }

        if (isset($ip)) {
            $this->isValidIP($ip);
        }

        $is_saved = $rating_obj->batchSave([
             'model'        => $model_name,
             'model_id'     => $this->id,
             'user_id'      => $user->id,
             'user_ip'      => $ip,
             'rate'         => $rate,
             'rate_percent' => $this->getRatePercent($rate),
        ]);


        return $is_saved;
    }



    /**
     * check user roles with accept roles
     *
     * @param User $user
     *
     * @return  boolean
     */
    private function checkUserRoles($user)
    {
        if (!$user->isAnyOf($this->acceptRoles())) {
            throw new Exception('This user cant rate to this model');
        }
        return true;
    }



    /**
     * add rate to model with guest user
     *
     * @param int    $rate
     * @param string $ip
     *
     * @return Rating
     */
    private function addRateWithGuestUser($rate, $ip)
    {
        if (!$this->acceptGuest()) {
            throw new Exception("Guests Doesn't allow to rate");
        }

        $model_name = $this->modelName();

        $this->isValidIP($ip);

        $rating_obj = $this->rates()
                           ->where('user_ip', $ip)
                           ->whereNull("user_id")
                           ->first()
        ;
        if (!$rating_obj) {
            $rating_obj = model('rating');
        }

        $is_saved = $rating_obj->batchSave([
             'model'    => $model_name,
             'model_id' => $this->id,
             'user_id'  => null,
             'user_ip'  => $ip,
             'rate'     => $rate,
             'rate_percent' => $this->getRatePercent($rate),
        ]);
        return $is_saved;
    }



    /**
     * get percent of rate
     *
     * @param float $rate
     *
     * @return float|int
     */
    private function getRatePercent($rate)
    {
        return 100 * $rate / $this->maxRateValue();
    }



    /**
     * check that string is a valid ip
     *
     * @param string $ip IP address
     *
     * @return bool
     */
    private function isValidIP($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            return true;
        } else {
            throw new Exception("IP is invalid");
        }
    }



    /**
     * get name of model
     *
     * @return string
     */
    private function modelName()
    {
        return class_basename(get_class($this));
    }
}