<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/21/18
 * Time: 4:12 PM
 */

namespace Modules\Ratings\Entities\Traits;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Modules\Ratings\Entities\Rating;

trait RatingTrait
{
    use RatingDefaultValueTrait;
    use AddRateTrait;
    use RatingFrontEndTrait;



    /**
     * @return Builder
     */
    public function rates()
    {
        return model('rating')->where('model', $this->modelName())->where('model_id', $this->id);
    }



    /**
     * @param    string $id_or_ip ip or id of a user raters
     * @param bool      $in_gustes
     *
     * @return Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getRatedBy($id_or_ip, $in_gustes = false)
    {
        if ($in_gustes) {
            return $this->rates()->whereNull('user_id')->where('user_ip', $id_or_ip)->first();
        }

        if (model('user')->where('id', $id_or_ip)->exists()) {

            return $this->rates()->where('user_id', $id_or_ip)->first();

        } else {
            return $this->rates()->where('user_ip', $id_or_ip)->first();
        }
    }



    /**
     * specify that specific user rated to this model or no
     *
     * @param  string $id_or_id
     * @param bool    $in_gustes
     *
     * @return bool
     */
    public function isRatedBy($id_or_id, $in_gustes = false)
    {
        if ($this->getRatedBy($id_or_id, $in_gustes)) {
            return true;
        }

        return false;
    }



    /**
     * return builder of user raters
     *
     * @return Builder
     */
    public function usersRated()
    {
        $ids = $this->rates()->groupBy('user_id')->pluck('user_id')->toArray();
        return model('user')->whereIn('id', $ids);
    }



    /**
     * return total of submitted rates
     *
     * @return int
     */
    public function countRates()
    {
        return $this->rates()->count();
    }



    /**
     * return total of submitted rates within this dates
     *
     * @param date $start_time
     * @param date $end_time
     *
     * @return int
     */
    public function countRatesWithin($start_time, $end_time)
    {
        $start = Carbon::parse($start_time);
        $end   = Carbon::parse($end_time);
        return $this->rates()
                    ->where('updated_at', ">=", ($start->hour == 0) ? $start->startOfDay() : $start->toDateTimeString())
                    ->where('updated_at', "<=", ($end->hour == 0) ? $end->endOfDay() : $end->toDateTimeString())
                    ->count()
             ;
    }



    /**
     * evaluate average of all rating of a model
     *
     * @return float|int
     */
    public function getRatingAverage()
    {
        $all_rates = $this->rates()->get();
        return $this->getAverage($all_rates);
    }



    /**
     * Get average of rates of a model within this dates
     *
     * @param date $start_time
     * @param date $end_time
     *
     * @return float|int
     */
    public function getRatingAverageWithin($start_time, $end_time)
    {
        $start     = Carbon::parse($start_time);
        $end       = Carbon::parse($end_time);
        $all_rates = $this->rates()
                          ->where('updated_at', ">=",
                               ($start->hour == 0) ? $start->startOfDay() : $start->toDateTimeString())
                          ->where('updated_at', "<=", ($end->hour == 0) ? $end->endOfDay() : $end->toDateTimeString())
                          ->get()
        ;

        return $this->getAverage($all_rates);
    }



    /**
     * evaluate average of rating
     *
     * @param Rating $rates
     *
     * @return float|int
     */
    private function getAverage($rates)
    {
        $total = 0;
        foreach ($rates as $rate) {
            $total += $rate->rate;
        }
        if ($total == 0) {
            return 0;
        }
        return $total / $rates->count();
    }
}