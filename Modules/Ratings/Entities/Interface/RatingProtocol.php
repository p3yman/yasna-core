<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/25/18
 * Time: 4:00 PM
 */

namespace Modules\Ratings\Entities;


interface RatingProtocol
{
    /**
     * specify max value of rate
     *
     * @return int
     */
    function maxRateValue(): int;



    /**
     * specify that rate accept guests users or no
     *
     * @return bool
     */
    function acceptGuest(): bool;



    /**
     * specify roles that can rate
     *
     * @return array
     */
    function acceptRoles(): array;
}