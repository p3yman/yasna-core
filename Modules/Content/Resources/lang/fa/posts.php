<?php
return [
     "grid_title" => "مطالب",

     'tab' => [
          "published" => "منتشرشده",
          "scheduled" => "صف انتشار",
          "pending"   => "منتظر تأیید",
          "my_posts"  => "نوشته‌های من",
          "my_drafts" => "پیش‌نویس‌های من",
     ],

];
