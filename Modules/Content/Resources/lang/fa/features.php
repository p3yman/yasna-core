<?php

return [
     "title"               => "عنوان",
     "text"                => "متن اصلی",
     "synopsis"            => "متن خلاصه",
     "pin"                 => "سنجاق کردن",
     "second_title"        => "عنوان دوم",
     "searchable"          => "جست‌وجوپذیر",
     "slug"                => "نامک",
     "panel"               => "ظهور در پنل ادمین",
     "custom_publish_date" => "تنظیم تاریخ انتشار",
     "image"               => "تصویر شاخص",
     "labels"              => "دسته‌بندی",
     "locales"             => "زبان‌ها",
];
