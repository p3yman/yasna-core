<?php
return [
     "grid_title" => "Posts",

     'tab' => [
          "published" => "Published",
          "scheduled" => "Scheduled",
          "pending"   => "Pending",
          "my_posts"  => "My Posts",
          "my_drafts" => "My Drafts",
     ],
];
