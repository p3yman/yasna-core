<?php

return [
     "text"                => "Main Text",
     "synopsis"            => "Abstract Text",
     "pin"                 => "Pin",
     "second_title"        => "Second Title",
     "searchable"          => "Searchable",
     "slug"                => "Slug",
     "panel"               => "Show in Admin Panel",
     "custom_publish_date" => "Custom Publish Date",
     "image"               => "Featured Image",
     "labels"              => "Labels",
];
