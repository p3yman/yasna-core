<?php

if (!function_exists("content")) {
    /**
     * get an instance of the ContentPost
     *
     * @return \Modules\Content\Services\ContentHelper
     */
    function content()
    {
        return new \Modules\Content\Services\ContentHelper();
    }
}
