<?php

namespace Modules\Content\Services;

use App\Models\ContentPost;
use App\Models\ContentType;
use Modules\Content\Http\Grids\V1\PostsGrid;
use Modules\Content\Http\Requests\V1\PostSaveRequest;
use Modules\Forms\Services\FormBinder;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

abstract class FeatureAbstract
{
    /**
     * get the feature slug
     *
     * @return string
     */
    abstract public function slug();



    /**
     * get the Persian title of the feature
     *
     * @return string
     */
    abstract public function persianTitle();



    /**
     * get the form binder, required to build the post editor, main area
     *
     * @return FormBinder|null
     */
    public function editorMainFormBinder()
    {
        return null;
    }



    /**
     * get the form binder, required to build the post editor, side area
     *
     * @return FormBinder|null
     */
    public function editorSideFormBinder()
    {
        return null;
    }



    /**
     * get the form binder, required to build the type settings
     *
     * @return FormBinder|null
     */
    public function settingFormBinder()
    {
        return null;
    }



    /**
     * get the array of the validation rules
     *
     * @return array
     */
    public function validationRules()
    {
        return [];
    }



    /**
     * get the responsible request file to handle the feature, when the post is being saved.
     * This will run merge the following methods: `fillableFields()`, `correctionsBeforePurifier()`, `corrections()`,
     * `mutators()`, `purifier()`, `rules()`, `attributes()` and `messages()`.
     * The followings are not merged, nor executed: `correctionsBeforeLoadingModel()` and `authorize()`
     *
     * @param PostSaveRequest $request
     *
     * @return YasnaFormRequest|null
     */
    public function postValidator()
    {
        return null;
        // override this to take the benefits of a full-featured request class.
    }



    /**
     * get the responsible request file to handle the feature, when the type setting is being saved.
     * This will run merge the following methods: `fillableFields()`, `correctionsBeforePurifier()`, `corrections()`,
     * `mutators()`, `purifier()`, `rules()`, `attributes()` and `messages()`.
     * The followings are not merged, nor executed: `correctionsBeforeLoadingModel()` and `authorize()`
     *
     * @param PostSaveRequest $request
     *
     * @return YasnaFormRequest|null
     */
    public function settingValidator()
    {
        return null;
        // override this to take the benefits of a full-featured request class.
    }



    /**
     * actions after completion of the post save
     *
     * @param ContentPost     $post
     * @param PostSaveRequest $request
     *
     * @return void
     */
    public function afterSave(ContentPost $post, PostSaveRequest $request)
    {
        // override this to make something happen after the post has been saved.

        return;
    }



    /**
     * get the attributes (fields or meta) required to be retrieved in the resources having this feature
     *
     * @return array
     */
    public function attributes()
    {
        return [];
    }



    /**
     * get the single resource provided by this feature
     *
     * @param ContentPost $post
     *
     * @return array
     */
    public function toSingleResource(ContentPost $post)
    {
        return [];
    }



    /**
     * get the list resource provided by this feature
     *
     * @param ContentPost $post
     *
     * @return array
     */
    public function toListResource(ContentPost $post)
    {
        return [];
    }



    /**
     * get the fetch resource provided by this feature
     *
     * @param ContentPost $post
     *
     * @return array
     */
    public function toFetchResource(ContentPost $post)
    {
        return $this->toSingleResource($post);
    }



    /**
     * add grid items
     *
     * @return void
     */
    public function gridItems()
    {
        // override this to add grid items (toolbar button, mass action, column, etc.)
    }
}
