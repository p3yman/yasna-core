<?php

namespace Modules\Content\Services;


use App\Models\ContentPost;

class ContentHelper
{
    const FEATURES_HOOK  = "content::features";
    const TEMPLATES_HOOK = "content::templates";



    /**
     * get a post instance
     *
     * @param int  $identifier
     * @param bool $with_disabled
     *
     * @return ContentPost
     */
    public function post($identifier = 0, $with_disabled = false)
    {
        /** @var ContentPost $model */
        $model = model("content-post", $identifier, $with_disabled);

        return $model;
    }



    /**
     * get an instance of the requested type from the singleton instance
     *
     * @param $handle
     *
     * @return \App\Models\ContentType
     */
    public function type($handle)
    {
        return $this->types()->find($handle);
    }



    /**
     * get the singleton instance of the types
     *
     * @return ContentTypeSingleton
     */
    public function types()
    {
        return new ContentTypeSingleton();
    }



    /**
     * register a content feature
     *
     * @param string $class
     *
     * @return void
     */
    public function registerFeature(string $class)
    {
        /** @var FeatureAbstract $instance */
        $instance = new $class();

        $slug  = $instance->slug();
        $title = $instance->persianTitle();

        hook(static::FEATURES_HOOK)
             ->add($slug)
             ->class($class)
             ->title($title)
        ;
    }



    /**
     * get a list of all the registered content features
     *
     * @return array
     */
    public function registeredFeatures()
    {
        return hook(static::FEATURES_HOOK)->get();
    }



    /**
     * register a content template
     *
     * @param array|string $names
     *
     * @return void
     */
    public function registerTemplate($names)
    {
        $names = (array)$names;

        foreach ($names as $name) {
            hook(static::TEMPLATES_HOOK)->add($name);
        }
    }



    /**
     * get a list of all the registered content templates
     *
     * @return array
     */
    public function registeredTemplates()
    {
        return hook(static::TEMPLATES_HOOK)->getKeys();
    }
}
