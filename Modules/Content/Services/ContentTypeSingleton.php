<?php

namespace Modules\Content\Services;


use App\Models\ContentType;
use Illuminate\Database\Eloquent\Collection;

class ContentTypeSingleton
{
    /**
     * keep the singleton static instance of the types
     *
     * @var Collection|null
     */
    protected static $singleton = null;

    /**
     * keep the dynamic collection of the instance, built on the singleton instance
     *
     * @var Collection|mixed
     */
    protected $payload;



    /**
     * ContentTypeSingleton constructor.
     */
    public function __construct()
    {
        $this->loadSingleton();
        $this->payload = clone static::$singleton;
    }



    /**
     * magic call method, to proxy all Eloquent Collection methods (at the end of this object's chain of conditions)
     *
     * @param string $method_name
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method_name, $parameters)
    {
        $result = $this->payload->$method_name(... $parameters);

        if (!is_object($result) or get_class($result) != 'Illuminate\Database\Eloquent\Collection') {
            return $result;
        }

        $this->payload = $result;

        return $this;
    }



    /**
     * get the collection
     *
     * @return Collection
     */
    public function get()
    {
        return $this->payload;
    }



    /**
     * filter the collection by feature(s)
     *
     * @param string|array $features
     */
    public function having($features)
    {
        $features = (array)$features;

        foreach ($this->payload as $key => $item) {
            foreach ($features as $feature) {
                if (!in_array($feature, $item->features)) {
                    $this->payload->forget($key);
                }
            }
        }

        return $this;
    }



    /**
     * filter the collection by NOT-HAVING feature(s)
     *
     * @param string|array $features
     */
    public function notHaving($features)
    {
        $features = (array)$features;

        foreach ($this->payload as $key => $item) {
            foreach ($features as $feature) {
                if (in_array($feature, $item->features)) {
                    $this->payload->forget($key);
                }
            }
        }

        return $this;
    }



    /**
     * find a record by its id, hashid or slug
     *
     * @param string|int $identifier
     *
     * @return ContentType
     */
    public function find($identifier)
    {
        if (is_numeric($identifier)) {
            return $this->findById($identifier);
        }

        $id = hashid($identifier);
        if ($id) {
            return $this->findById($id);
        }

        return $this->findBySlug($identifier);
    }



    /**
     * find a record by its slug
     *
     * @param string $slug
     *
     * @return ContentType
     */
    public function findBySlug(string $slug)
    {
        $instance = $this->payload->where('slug', $slug)->first();

        return $this->getSafeInstance($instance);
    }



    /**
     * find a record by its hashid
     *
     * @param string $hashid
     *
     * @return ContentType
     */
    public function findByHashid(string $hashid)
    {
        if (is_numeric($hashid)) {
            return $this->getSafeInstance(null);
        }

        return $this->findById(hashid($hashid));
    }



    /**
     * find a record by its id
     *
     * @param int $id
     *
     * @return ContentType
     */
    public function findById(int $id)
    {
        $instance = $this->payload->where("id", $id)->first();

        return $this->getSafeInstance($instance);
    }



    /**
     * get a safe instance of the type
     *
     * @param null|ContentType $instance
     *
     * @return ContentType
     */
    protected function getSafeInstance($instance)
    {
        if (!$instance) {
            return model("content-type");
        }

        return $instance;
    }



    /**
     * set static singleton
     *
     * @return void
     */
    protected function loadSingleton()
    {
        if (!is_null(static::$singleton)) {
            return;
        }

        static::$singleton = ContentType::all()->map(function (ContentType $type) {
            $type->features = $type->getFeaturesAttribute();
            return $type;
        })
        ;
    }

}
