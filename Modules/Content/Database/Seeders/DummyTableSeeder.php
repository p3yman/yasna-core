<?php

namespace Modules\Content\Database\Seeders;

use App\Models\ContentPost;
use App\Models\ContentType;
use Illuminate\Database\Seeder;
use Modules\Content\Services\ContentHelper;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedTypes();
        $this->seedPosts();
    }



    /**
     * seed some random types
     *
     * @param int $total
     */
    private function seedTypes($total = 10)
    {
        $features  = array_keys(content()->registeredFeatures());
        $templates = content()->registeredTemplates();

        for ($i = 1; $i <= $total; $i++) {
            shuffle($features);

            $titles = [
                 "fa" => dummy()::persianWord() . SPACE . dummy()::persianWord(),
                 "en" => dummy()::englishWord(),
            ];

            $data = [
                 "titles"   => $titles,
                 "slug"     => str_slug($titles['fa']),
                 "locales"  => "fa,en",
                 "features" => $features[0] . "," . $features[1] . "," . $features[2],
                 "template" => array_random($templates),
            ];

            model("content-type")->batchSave($data);
        }
    }



    /**
     * seed some random posts
     *
     * @param int $total
     */
    private function seedPosts($total = 10)
    {
        foreach (content()->types()->get() as $type) {
            $this->seedPostsOfOneType($type, $total);
        }
    }



    /**
     * seed posts in one type
     *
     * @param ContentType $type
     * @param int         $total
     */
    private function seedPostsOfOneType(ContentType $type, $total)
    {
        for ($i = 1; $i <= $total; $i++) {
            $sisterhood = ContentPost::generateSisterhood();

            foreach ($type->getLocalesArray() as $locale) {
                if (!rand(0, 2)) {
                    continue;
                }

                $this->seedOnePost($type, $locale, $sisterhood);
            }
        }
    }



    /**
     * seed one post
     *
     * @param ContentType $type
     * @param string      $locale
     * @param string      $sisterhood
     */
    private function seedOnePost(ContentType $type, string $locale, string $sisterhood)
    {
        $rtl      = in_array($locale, ['fa', 'ar']);
        $title    = $rtl ? dummy()::persianWord(2) : dummy()::englishWord(2);
        $synopsis = $rtl ? dummy()::persianText() : dummy()::englishText();
        $text     = $rtl ? dummy()::persianText(2) : dummy()::englishText(2);

        $data = [
             "slug"       => $type->has('slug') ? str_slug($title) : "",
             "sisterhood" => $sisterhood,
             "locale"     => $locale,
             "type_id"    => $type->id,
             "master_id"  => 0,
             "user_id"    => user()->inRandomOrder()->first()->id,
             "title"      => $title,
             "synopsis"   => $type->has('synopsis') ? $synopsis : null,
             "text"       => $type->has("text") ? $text : null,
        ];

        content()->post()->batchSave($data);
    }
}
