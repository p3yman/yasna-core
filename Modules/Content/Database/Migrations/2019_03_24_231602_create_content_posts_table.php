<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_posts', function (Blueprint $table) {

            /*-----------------------------------------------
            | Identifiers ...
            */
            $table->increments('id');
            $table->string("slug")->nullable()->index();
            $table->string("sisterhood")->index();
            $table->string('locale', 2)->default('fa')->index();
            $table->boolean('is_draft')->default(1);

            /*-----------------------------------------------
            | Relations ...
            */
            $table->unsignedInteger("type_id")->index();
            $table->unsignedBigInteger("master_id")->default(0)->index();

            $table->unsignedBigInteger("user_id")->default(0)->index();
            $table->unsignedBigInteger("organization_id")->default(0)->index();

            $table->unsignedInteger('replacement_id')->default(0)->index();

            /*-----------------------------------------------
            | Post Things ...
            */
            $table->string("title")->index();
            $table->string("title2")->index();

            $table->longText('synopsis')->nullable();
            $table->longText('text')->nullable();

            $table->string('template')->nullable();

            $table->timestamp('event_starts_at')->nullable();
            $table->timestamp('event_ends_at')->nullable();

            $table->timestamp('register_starts_at')->nullable();
            $table->timestamp('register_ends_at')->nullable();

            /*-----------------------------------------------
            | Flags and timestamps ...
            */
            $table->timestamps();

            $table->timestamp("pinned_at")->nullable()->index();
            $table->unsignedInteger("pinned_by")->default(0);
            $table->tinyInteger("pinned_order")->default(1);

            $table->timestamp('published_at')->index()->nullable();
            $table->unsignedInteger('published_by')->default(0);

            $table->timestamp('moderated_at')->nullable();
            $table->unsignedInteger('moderated_by')->default(0);

            /*-----------------------------------------------
            | Rest ...
            */
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_posts');
    }
}
