<?php

namespace Modules\Content\Providers;


use App\Models\ContentType;

class ContentHooksHandler
{
    /**
     * handle content-based permits to the relevant hook.
     *
     * @return void
     */
    public static function handlePermits()
    {
        /** @var ContentType $type */
        foreach (content()->types()->get() as $type) {
            foreach ($type->getLocalesArray() as $locale) {
                hook("persons::permits")
                     ->add("content-$type->slug.$locale")
                     ->title($type->titleIn($locale))
                ;
            }
        }
    }
}
