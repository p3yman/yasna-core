<?php

namespace Modules\Content\Providers;

use Modules\Content\Console\MakeFeatureCommand;
use Modules\Content\Http\ContentFeatures\CustomPublishedAtFeature;
use Modules\Content\Http\ContentFeatures\LabelsFeature;
use Modules\Content\Http\ContentFeatures\LocalesFeature;
use Modules\Content\Http\ContentFeatures\PanelFeature;
use Modules\Content\Http\ContentFeatures\SampleFeature;
use Modules\Content\Http\ContentFeatures\SearchableFeature;
use Modules\Content\Http\ContentFeatures\SecondTitleFeature;
use Modules\Content\Http\ContentFeatures\SlugFeature;
use Modules\Content\Http\ContentFeatures\SynopsisFeature;
use Modules\Content\Http\ContentFeatures\PinFeature;
use Modules\Content\Http\ContentFeatures\TextFeature;
use Modules\Content\Http\ContentFeatures\ImageFeature;
use Modules\Content\Http\Endpoints\V1\FeaturesEndpoint;
use Modules\Content\Http\Endpoints\V1\PostDestroyEndpoint;
use Modules\Content\Http\Endpoints\V1\PostGridEndpoint;
use Modules\Content\Http\Endpoints\V1\PostPinEndpoint;
use Modules\Content\Http\Endpoints\V1\PostRestoreEndpoint;
use Modules\Content\Http\Endpoints\V1\PostListEndpoint;
use Modules\Content\Http\Endpoints\V1\PostSingleEndpoint;
use Modules\Content\Http\Endpoints\V1\PostTrashEndpoint;
use Modules\Content\Http\Endpoints\V1\PostFetchEndpoint;
use Modules\Content\Http\Endpoints\V1\PostSaveEndpoint;
use Modules\Content\Http\Endpoints\V1\PostUnpinEndpoint;
use Modules\Content\Http\Endpoints\V1\TemplatesEndpoint;
use Modules\Content\Http\Endpoints\V1\TypeDestroyEndpoint;
use Modules\Content\Http\Endpoints\V1\TypeFetchEndpoint;
use Modules\Content\Http\Endpoints\V1\TypeGridEndpoint;
use Modules\Content\Http\Endpoints\V1\TypeListEndpoint;
use Modules\Content\Http\Endpoints\V1\TypeRestoreEndpoint;
use Modules\Content\Http\Endpoints\V1\TypeSaveEndpoint;
use Modules\Content\Http\Endpoints\V1\TypeTrashEndpoint;
use Modules\Content\Http\Panel\PanelLayout;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class ContentServiceProvider
 *
 * @package Modules\Content\Providers
 */
class ContentServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerHooks();
        $this->registerFeatures();
        $this->registerTemplates();
        $this->registerEndpoints();
        $this->registerPanelLayout();
        $this->registerArtisans();
        //$this->registerPermitHooks();
    }



    /**
     * register hooks
     *
     * @return void
     */
    private function registerHooks()
    {
        hook(content()::FEATURES_HOOK)->register("Content Features", [
             "class" => "the FQ name of the feature class",
             "title" => "the Persian title of the feature",
        ]);

        hook(content()::TEMPLATES_HOOK)->register("Content Templates", []);
    }



    /**
     * register features
     *
     * @return void
     */
    private function registerFeatures()
    {
        content()->registerFeature(TextFeature::class);
        content()->registerFeature(SynopsisFeature::class);
        content()->registerFeature(PinFeature::class);
        content()->registerFeature(SecondTitleFeature::class);
        content()->registerFeature(PanelFeature::class);
        content()->registerFeature(SearchableFeature::class);
        content()->registerFeature(SlugFeature::class);
        content()->registerFeature(CustomPublishedAtFeature::class);
        content()->registerFeature(ImageFeature::class);
        content()->registerFeature(LabelsFeature::class);
        content()->registerFeature(LocalesFeature::class);

        if (debugMode()) {
            content()->registerFeature(SampleFeature::class);
        }
    }



    /**
     * register templates
     *
     * @return void
     */
    private function registerTemplates()
    {
        content()->registerTemplate([
             "text",
             "faq",
        ]);
    }



    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }

        $this->registerGeneralEndpoints();
        $this->registerPostCrudEndpoints();
        $this->registerTypeCurdEndpoints();
    }



    /**
     * register GeneralEndpoints
     *
     * @return void
     */
    private function registerGeneralEndpoints()
    {
        endpoint()->register(FeaturesEndpoint::class);
        endpoint()->register(TemplatesEndpoint::class);
    }



    /**
     * register post crud endpoints
     *
     * @return void
     */
    private function registerPostCrudEndpoints()
    {
        endpoint()->register(PostFetchEndpoint::class);
        endpoint()->register(PostSaveEndpoint::class);
        endpoint()->register(PostTrashEndpoint::class);
        endpoint()->register(PostDestroyEndpoint::class);
        endpoint()->register(PostRestoreEndpoint::class);
        endpoint()->register(PostPinEndpoint::class);
        endpoint()->register(PostUnpinEndpoint::class);
        endpoint()->register(PostListEndpoint::class);
        endpoint()->register(PostSingleEndpoint::class);
        endpoint()->register(PostGridEndpoint::class);
    }



    /**
     * register Type Curd Endpoints
     *
     * @return void
     */
    private function registerTypeCurdEndpoints()
    {
        endpoint()->register(TypeSaveEndpoint::class);
        endpoint()->register(TypeListEndpoint::class);
        endpoint()->register(TypeGridEndpoint::class);
        endpoint()->register(TypeFetchEndpoint::class);
        endpoint()->register(TypeTrashEndpoint::class);
        endpoint()->register(TypeRestoreEndpoint::class);
        endpoint()->register(TypeDestroyEndpoint::class);
    }



    /**
     * register PanelLayout
     *
     * @return void
     */
    private function registerPanelLayout()
    {
        if ($this->cannotUseModule("panel")) {
            return;
        }

        panel()->layout()->register(PanelLayout::class);
    }



    /**
     * register artisans
     *
     * @return void
     */
    private function registerArtisans()
    {
        $this->addArtisan(MakeFeatureCommand::class);
    }



    /**
     * register PermitHooks
     *
     * @return void
     */
    private function registerPermitHooks()
    {
        hook("persons::permitsHandler")
             ->add("content")
             ->class(ContentHooksHandler::class)
             ->method("handlePermits")
        ;
    }

}
