<?php

namespace Modules\Content\Entities;

use Modules\Content\Entities\Traits\PostElectorTrait;
use Modules\Content\Entities\Traits\PostFeaturesTrait;
use Modules\Content\Entities\Traits\PostHelpersTrait;
use Modules\Content\Entities\Traits\PostPermissionsTrait;
use Modules\Content\Entities\Traits\PostPinTrait;
use Modules\Content\Entities\Traits\PostResourceTrait;
use Modules\Content\Entities\Traits\PostSisterhoodTrait;
use Modules\Content\Entities\Traits\PostStatusTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Uploader\Services\Entity\FilesTrait;

class ContentPost extends YasnaModel
{
    use SoftDeletes;
    use PostSisterhoodTrait;
    use PostStatusTrait;
    use PostPermissionsTrait;
    use PostResourceTrait;
    use PostElectorTrait;
    use PostPinTrait;
    use PostFeaturesTrait;
    use FilesTrait;


    /**
     * get a new instance of the model, with correct type and brand-new sisterhood
     *
     * @param string|int  $type
     * @param string|null $sisterhood
     *
     * @return \App\Models\ContentPost
     */
    public static function newRecordInstance($type, $sisterhood = null)
    {
        //TODO: Use parent::newInstance(), resolve type and generate sisterhood if required
    }



    /**
     * get the ContentType instance, the post is attached to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "ContentType");
    }



    /**
     * get a safe instance of the type
     *
     * @return \App\Models\ContentType
     */
    public function getTypeModelAttribute()
    {
        if ($this->type) {
            return $this->type;
        }

        return model('content-type');
    }

}
