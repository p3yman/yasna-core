<?php

namespace Modules\Content\Entities;

use Modules\Content\Entities\Traits\TypeFeaturesTrait;
use Modules\Content\Entities\Traits\TypeFormsTrait;
use Modules\Content\Entities\Traits\TypePermissionsTrait;
use Modules\Content\Entities\Traits\TypeResourcesTrait;
use Modules\Yasna\Services\ModelTraits\YasnaLocaleTitlesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentType extends YasnaModel
{
    use SoftDeletes;
    use YasnaLocaleTitlesTrait;
    use TypePermissionsTrait;
    use TypeFeaturesTrait;
    use TypeFormsTrait;
    use TypeResourcesTrait;



    /**
     * get a list of reserved slugs, forbidden to be used on the types
     *
     * @return array
     */
    public static function getReservedSlugs()
    {
        return [
             "root",
             "admin",
             "upstream",
        ];
    }



    /**
     * get the posts attached to the current type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(MODELS_NAMESPACE . "ContentPost", "type_id");
    }



    /**
     * get main meta fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "features",
             "icon",
        ];
    }
}
