<?php

namespace Modules\Content\Entities\Traits;

use App\Models\ContentType;

/**
 * Class ContentType
 *
 * @property ContentType $type_model
 */
trait PostPermissionsTrait
{
    /**
     * check if the user can create a post of the specified type
     *
     * @return bool
     */
    public function canCreate()
    {
        return $this->type_model->can('create');
    }



    /**
     * TODO: check if the user can edit the post in question
     *
     * @return bool
     */
    public function canEdit()
    {
        return user()->exists;
    }



    /**
     * check if the user can create a new post or can edit an existing one
     *
     * @return bool
     */
    public function canCreateOrEdit()
    {
        if ($this->exists) {
            return $this->canEdit();
        }

        return $this->canCreate();
    }



    /**
     * check if the user can delete a post
     *
     * @return bool
     */
    public function canDelete()
    {
        if (!$this->exists) {
            return true;
        }

        if ($this->type_model->can('delete')) {
            return true;
        }

        if ($this->isDraft() and $this->isOwner()) {
            return true;
        }

        return false;
    }



    /**
     * TODO: check if the user can undelete or permanent delete of the post in question
     *
     * @return bool
     */
    public function canRestore()
    {
        return true;
    }



    /**
     * TODO: check if the user can browse the posts of the posttype in question
     *
     * @return bool
     */
    public function canBrowse()
    {
        return true;
    }



    /**
     * check if the user can view the post in question
     *
     * @return bool
     */
    public function canView()
    {
        if($this->canEdit()) {
            return true;
        }

        return $this->isPublished();
    }
}
