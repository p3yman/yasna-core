<?php

namespace Modules\Content\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class ContentPost
 *
 * @property string $sisterhood
 * @method Builder where($attribute, $value)
 */
trait PostSisterhoodTrait
{
    /**
     * generate a random sisterhood based on the unix timestamp
     *
     * @return string
     */
    public static function generateSisterhood()
    {
        return hashid(time(), 'main') . str_random(10);
    }



    /**
     * get the Builder instance of the post sisters, including herself
     *
     * @return Builder
     */
    public function sisters()
    {
        return static::where('sisterhood', $this->sisterhood);
    }



    /**
     * get the Collection of the post sisters, including herself
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getSistersAttribute()
    {
        return $this->sisters()->get();
    }

}
