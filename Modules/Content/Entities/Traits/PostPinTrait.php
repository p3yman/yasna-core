<?php


namespace Modules\Content\Entities\Traits;


trait PostPinTrait
{

    /**
     * Set date and id to pin a post
     *
     * @return bool
     */
    public function pin()
    {
        if (!$this->type_model->has("pin")) {
            return false;
        }

        if ($this->pinned_at != null) {
            return false;
        }

        $data['pinned_at'] = $this->now();
        $data['pinned_by'] = user()->id;

        return $this->update($data);
    }



    /**
     * unSet date and id to unPin a post
     *
     * @return bool
     */
    public function unPin()
    {
        if (!$this->type_model->has("pin")) {
            return false;
        }

        if ($this->pinned_at == null) {
            return false;
        }

        $data['pinned_at'] = null;
        $data['pinned_by'] = 0;

        return $this->update($data);
    }



    /**
     * check the post is pinned
     *
     * @return bool
     */
    public function isPinned()
    {
        return isset($this->pinned_at);
    }

}
