<?php

namespace Modules\Content\Entities\Traits;

use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

trait TypeFormsTrait
{
    /**
     * get the form binder, required to build the type settings
     *
     * @return FormBinder
     */
    public function settingForm()
    {
        $binder = new FormBinder();

        /** @var FeatureAbstract $feature */
        foreach ($this->featureInstancesArray() as $feature) {
            $form = $feature->settingFormBinder();

            if ($form) {
                $binder->mergeWith($form);
            }
        }

        return $binder;

    }



    /**
     * get editor form
     *
     * @return array
     */
    public function editorFormArray()
    {
        return [
             "main" => $this->editorMainForm()->toArray(),
             "side" => $this->editorSideForm()->toArray(),
        ];
    }



    /**
     * get the form binder, required to build the post editor, main area
     *
     * @return FormBinder
     */
    public function editorMainForm()
    {
        $binder = new FormBinder();
        
        // Add title to all forms
        $binder->add("text")
               ->name("title")
               ->placeholder(trans('content::features.title'))
               ->cssClass('lg')
               ->noLabel()
               ->order(10);

        /** @var FeatureAbstract $feature */
        foreach ($this->featureInstancesArray() as $feature) {
            $form = $feature->editorMainFormBinder();

            if ($form) {
                $binder->mergeWith($form);
            }
        }

        return $binder;
    }



    /**
     * get the form binder, required to build the post editor, side area
     *
     * @return FormBinder
     */
    public function editorSideForm()
    {
        $binder = new FormBinder();

        /** @var FeatureAbstract $feature */
        foreach ($this->featureInstancesArray() as $feature) {
            $form = $feature->editorSideFormBinder();

            if ($form) {
                $binder->mergeWith($form);
            }
        }

        return $binder;
    }

}
