<?php

namespace Modules\Content\Entities\Traits;

trait TypeResourcesTrait
{

    /**
     * boot TypeResourcesTrait
     *
     * @return void
     */
    public static function bootTypeResourcesTrait()
    {
        static::addDirectResources('*');
    }
}
