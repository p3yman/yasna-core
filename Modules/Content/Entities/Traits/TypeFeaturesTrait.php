<?php

namespace Modules\Content\Entities\Traits;

trait TypeFeaturesTrait
{
    /**
     * get array of the features slug
     *
     * @return array
     */
    public function getFeaturesAttribute()
    {
        $registered = array_keys(content()->registeredFeatures());
        $array      = explode_not_empty(",", $this->getMeta("features"));

        foreach ($array as $key => $item) {
            if (!in_array($item, $registered)) {
                unset($array[$key]);
            }
        }

        return $array;
    }



    /**
     * get a key-value list of features and their associated classes
     *
     * @return array
     */
    public function featureClassesArray()
    {
        $registered = content()->registeredFeatures();
        $features   = $this->getFeaturesAttribute();
        $array      = [];

        foreach ($features as $feature) {
            $array[$feature] = $registered[$feature]['class'];
        }

        return $array;
    }



    /**
     * get an array of the feature instances
     *
     * @return array
     */
    public function featureInstancesArray()
    {
        $array = [];
        foreach ($this->featureClassesArray() as $feature) {
            $array[] = new $feature();
        }

        return $array;
    }



    /**
     * check if the type has a certain feature
     *
     * @param string $feature
     *
     * @return bool
     */
    public function has(string $feature)
    {
        return in_array($feature, $this->getFeaturesAttribute());
    }



    /**
     * check if the type has NOT a certain feature
     *
     * @param string $feature
     *
     * @return bool
     */
    public function hasnot(string $feature)
    {
        return !$this->has($feature);
    }
}
