<?php

namespace Modules\Content\Entities\Traits;

use App\Models\ContentPost;
use App\Models\ContentType;
use Modules\Content\Services\FeatureAbstract;

/**
 * @property ContentType $type_model
 * @property string      $title
 * @property string      $published_at
 * @property string      $moderated_at
 * @property int         $published_by
 * @property int         $moderated_by
 * @property int         $user_id
 */
trait PostResourceTrait
{
    /**
     * boot PostResourceTrait
     *
     * @return void
     */
    public static function bootPostResourceTrait()
    {
        static::addDirectResources([
             "title",
             "sisterhood",
             "user_id",
             "published_at",
             "published_by",
             "moderated_at",
             "moderated_by",
             "locale",
        ]);
    }



    /**
     * get the feature-based single resources
     *
     * @return array
     */
    //protected function toSingleFeaturedResource()
    //{
    //    /** @var ContentPost $model */
    //    $model = $this;
    //    $array = [];
    //
    //    /** @var FeatureAbstract $feature */
    //    foreach ($this->type_model->featureInstancesArray() as $feature) {
    //        $array = array_merge($array, $feature->toSingleResource($model));
    //    }
    //
    //    return $array;
    //}
    //
    //
    //
    ///**
    // * get the feature-based list resources
    // *
    // * @return array
    // */
    //protected function toListFeaturedResource()
    //{
    //    /** @var ContentPost $model */
    //    $model = $this;
    //    $array = [];
    //
    //    /** @var FeatureAbstract $feature */
    //    foreach ($this->type_model->featureInstancesArray() as $feature) {
    //        $array = array_merge($array, $feature->toListResource($model));
    //    }
    //
    //    return $array;
    //}
    //
    //
    //
    ///**
    // * get the feature-based list resources
    // *
    // * @return array
    // */
    //public function toFetchFeaturedResource()
    //{
    //    /** @var ContentPost $model */
    //    $model = $this;
    //    $array = [];
    //
    //    /** @var FeatureAbstract $feature */
    //    foreach ($this->type_model->featureInstancesArray() as $feature) {
    //        $array = array_merge($array, $feature->toFetchResource($model));
    //    }
    //
    //    return $array;
    //}



    /**
     * get Type resource.
     *
     * @return array
     */
    protected function getTypeResource()
    {
        return $this->type_model->toResource(["slug"]);
    }

}
