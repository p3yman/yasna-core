<?php


namespace Modules\Content\Entities\Traits;


use Carbon\Carbon;

trait PostElectorTrait
{
    /**
     * get posts that have active search feature
     *
     * @param string $keyword
     */
    public function electorSearch($keyword)
    {
        $searchable_id = content()->types()->having('searchable')->pluck('id')->toArray();


        $this->elector()->whereIn("type_id", $searchable_id)->where(function ($query) use ($keyword) {
            $query->where('title', $keyword)
                  ->orWhere('title', 'like', '%' . $keyword . '%')
                  ->orWhere('title2', $keyword)
                  ->orWhere('title2', 'like', '%' . $keyword . '%')
                  ->orWhere('slug', $keyword)
                  ->orWhere('slug', 'like', '%' . $keyword . '%')
            ;

        })
        ;
    }



    /**
     * put pinned condition on elector
     */
    protected function electorPinned()
    {
        $types = content()->types()->having('pin')->pluck('id')->toArray();

        $this->elector()->whereIn("type_id", $types)->whereNotNull('pinned_at');
    }



    /**
     * put type_id(s) condition on elector with the given id(s) or hashid(s)
     *
     * @param string|int|array $ids
     */
    protected function electorTypesId($ids)
    {
        $type_ids = $this->convertHashidToIdArray($ids);

        $this->elector()->whereIn('type_id', $type_ids);
    }



    /**
     * put the type(s) condition on elector
     *
     * @param array|string $types
     */
    protected function electorTypes($types)
    {
        $array_slugs = (array)$types;

        $types_id = content()->types()->whereIn('slug', $array_slugs)->pluck('id')->toArray();

        $this->electorTypesId($types_id);

    }



    /**
     * put the feature(s) condition on elector
     *
     * @param array|string $features
     */
    protected function electorFeatures($features)
    {
        $features = (array)$features;

        $types_id = content()->types()->having($features)->pluck('id')->toArray();

        $this->electorTypesId($types_id);
    }



    /**
     * put the locale(s) condition on elector
     *
     * @param array|string $locales
     */
    protected function electorLocales($locales)
    {
        $array_locales = (array)$locales;

        if (is_string($locales) and ($locales == 'all')) {
            return;
        }

        $this->elector()->whereIn("locale", $array_locales);
    }



    /**
     * put the id(s) condition on elector with the given id(s) or hashid(s)
     *
     * @param array|string|int $ids
     */
    protected function electorIds($ids)
    {
        $id_num = $this->convertHashidToIdArray($ids);

        $this->elector()->whereIn("id", $id_num);
    }



    /**
     * put the time limit until $created_date on elector
     *
     * @param string $create_date
     */
    protected function electorDateUntil($create_date)
    {
        $this->elector()->whereDate('created_at', '<=', $create_date);
    }



    /**
     * put the time limit from $created_date on elector
     *
     * @param string $create_date
     */
    protected function electorDateFrom($create_date)
    {
        $this->elector()->whereDate('created_at', '>=', $create_date);
    }



    /**
     * put the owner(s) condition whit user_id(s) on elector
     *
     * @param array|string $ids
     */
    protected function electorOwners($ids)
    {
        $owner_ids = $this->convertHashidToIdArray($ids);

        $this->elector()->whereIn('user_id', $owner_ids);
    }



    /**
     * put the post creator(s) condition on elector
     *
     * @param array|string|int $ids
     */
    protected function electorPostsCreators($ids)
    {
        $ids_num = $this->convertHashidToIdArray($ids);

        $this->elector()->whereIn('created_by', $ids_num);
    }



    /**
     * put the published time limit from $date on elector
     *
     * @param string|Carbon $date
     */
    protected function electorPublishedFrom($date)
    {
        $this->elector()->whereDate('published_at', '>=', $date);
    }



    /**
     * put the published time limit at $date on elector
     *
     * @param string|Carbon $date
     */
    protected function electorPublishedAt($date)
    {
        $this->elector()->whereDate('published_at', $date);
    }



    /**
     * put the published time limit until $date on elector
     *
     * @param $date
     */
    protected function electorPublishedUntil($date)
    {
        $this->elector()->whereDate('published_at', '<=', $date);
    }



    /**
     * put the elector based on the requested tab (useful in the grid-view)
     *
     * @param $tab
     *
     * @return void
     */
    protected function electorTab($tab)
    {
        switch ($tab) {
            case "published":
                $this->electorCriteriaPublished();
                break;

            case "scheduled":
                $this->elector()->whereDate('published_at', '<=', $this->now())->where('published_by', '>', 0);
                break;

            case "pending":
                $this->elector()->where("moderated_by", 0);
                break;

            case "my_posts":
                $this->elector()->where("is_draft", 0)->where("user_id", user()->id);
                break;

            case "my_drafts":
                $this->elector()->where("is_draft", 1)->where("user_id", user()->id);
                break;

            case "trash":
                $this->elector()->onlyTrashed();
                break;

            default:
                $this->elector()->whereRaw('1=0');
        }
    }



    /**
     * Adds `published` condition to the criteria
     */
    protected function electorCriteriaPublished()
    {
        $this->elector()->whereDate('published_at', '<=', $this->now())->where('published_by', '>', 0);
    }



    /**
     * put the event starts time limit from $start_date on elector
     *
     * @param string|Carbon $start_date
     */
    protected function electorEventStartsFrom($start_date)
    {
        $this->elector()->whereDate('event_starts_at', '>=', $start_date);
    }



    /**
     * put the event starts time limit at $start_date on elector
     *
     * @param string|Carbon $start_date
     */
    protected function electorEventStartsAt($start_date)
    {
        $this->elector()->whereDate('event_starts_at', $start_date);
    }



    /**
     * put the event starts time limit until $start_date on elector
     *
     * @param string|Carbon $start_date
     */
    protected function electorEventStartsUntil($start_date)
    {
        $this->elector()->whereDate('event_starts_at', '<=', $start_date);
    }



    /**
     * put the event ends time limit from $end_date on elector
     *
     * @param string|Carbon $end_date
     */
    protected function electorEventEndsFrom($end_date)
    {
        $this->elector()->whereDate('event_ends_at', '>=', $end_date);
    }



    /**
     * put the event ends time limit at $end_date on elector
     *
     * @param string|Carbon $end_date
     */
    protected function electorEventEndsAt($end_date)
    {
        $this->elector()->whereDate('event_ends_at', $end_date);
    }



    /**
     * put the event ends time limit until $end_date on elector
     *
     * @param string|Carbon $end_date
     */
    protected function electorEventEndsUntil($end_date)
    {
        $this->elector()->whereDate('event_ends_at', '<=', $end_date);
    }



    /**
     * put the register starts time limit from $start_date on elector
     *
     * @param string|Carbon $start_date
     */
    protected function electorRegisterStartsFrom($start_date)
    {
        $this->elector()->whereDate('register_starts_at', '>=', $start_date);
    }



    /**
     * put the register starts time limit at $start_date on elector
     *
     * @param string|Carbon $start_date
     */
    protected function electorRegisterStartsAt($start_date)
    {
        $this->elector()->whereDate('register_starts_at', $start_date);
    }



    /**
     * put the register starts time limit until $start_date on elector
     *
     * @param string|Carbon $start_date
     */
    protected function electorRegisterStartsUntil($start_date)
    {
        $this->elector()->whereDate('register_starts_at', '<=', $start_date);
    }



    /**
     * put the register ends time limit from $end_date on elector
     *
     * @param string|Carbon $end_date
     */
    protected function electorRegisterEndsFrom($end_date)
    {
        $this->elector()->whereDate('register_ends_at', '>=', $end_date);
    }



    /**
     * put the register ends time limit at $end_date on elector
     *
     * @param string|Carbon $end_date
     */
    protected function electorRegisterEndsAt($end_date)
    {
        $this->elector()->whereDate('register_ends_at', $end_date);
    }



    /**
     * put the register ends time limit until $end_date on elector
     *
     * @param string|Carbon $end_date
     */
    protected function electorRegisterEndsUntil($end_date)
    {
        $this->elector()->whereDate('register_ends_at', '<=', $end_date);
    }



    /**
     * get the array or strings of ids or hashids and convert to array
     *
     * @param string|array|int $data
     *
     * @return array
     */
    private function convertHashidToIdArray($data)
    {
        $array_ids = (array)$data;

        foreach ($array_ids as $id) {
            /** @var array $id_num */
            $id_num[] = hashid_number($id);
        }
        return $id_num;
    }
}
