<?php namespace Modules\Content\Entities\Traits;

trait PostStatusTrait
{

    /**
     * Check post published
     *
     * @return bool
     */
    public function isPublished()
    {
        return (!$this->trashed() and $this->published_by and $this->published_at and $this->published_at <= now());
    }



    /**
     * Check post approved
     *
     * @return bool
     */
    public function isApproved()
    {
        return boolval($this->published_by);
    }



    /**
     * Check post unpublished
     *
     * @return bool
     */
    public function isNotApproved()
    {
        return !$this->isApproved();
    }



    /**
     * Check post in case of draft
     *
     * @return bool
     */
    public function isDraft()
    {
        return $this->is_draft;
    }



    /**
     * Check  the non-draft post
     *
     * @return bool
     */
    public function isNotDraft()
    {
        return !$this->is_draft;
    }



    /**
     * check owner post
     *
     * @return bool
     */
    public function isOwner()
    {
        if (!$this->exists) {
            return true;
        }

        return user()->id == $this->user_id;
    }
}
