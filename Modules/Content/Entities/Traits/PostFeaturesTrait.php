<?php

namespace Modules\Content\Entities\Traits;

use App\Models\ContentType;

/**
 * @property ContentType $type_model
 */
trait PostFeaturesTrait
{
    /**
     * get array of the features slug
     *
     * @return array
     */
    public function getFeaturesAttribute()
    {
        return $this->type_model->getFeaturesAttribute();
    }



    /**
     * get a key-value list of features and their associated classes
     *
     * @return array
     */
    public function featureClassesArray()
    {
        return $this->type_model->featureClassesArray();
    }



    /**
     * get an array of the feature instances
     *
     * @return array
     */
    public function featureInstancesArray()
    {
        return $this->type_model->featureInstancesArray();
    }



    /**
     * check if the type has a certain feature
     *
     * @param string $feature
     *
     * @return bool
     */
    public function has(string $feature)
    {
        return $this->type_model->has($feature);
    }



    /**
     * check if the type has NOT a certain feature
     *
     * @param string $feature
     *
     * @return bool
     */
    public function hasnot(string $feature)
    {
        return $this->type_model->hasnot($feature);
    }

}
