<?php

namespace Modules\Content\Entities\Traits;

trait TypePermissionsTrait
{
    /**
     * determine if the current user can perform a specific action on the attached posts
     *
     * @param string $action
     * @param string $role
     *
     * @return bool
     */
    public function can(string $action = "*", string $role = "admin"): bool
    {
        if(!$this->exists) {
            return false;
        }

        return user()->as($role)->can("posts-" . $this->slug . "." . $action);
    }



    /**
     * determine if the current user cannot perform a specific action on the attached posts
     *
     * @param string $action
     * @param string $role
     *
     * @return bool
     */
    public function cannot(string $action = "*", string $role = "admin"): bool
    {
        return !$this->can($action, $role);
    }
}
