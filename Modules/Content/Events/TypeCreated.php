<?php 

namespace Modules\Content\Events;

use Modules\Yasna\Services\YasnaEvent;

class TypeCreated extends YasnaEvent
{
    public $model_name = "content-type";
}
