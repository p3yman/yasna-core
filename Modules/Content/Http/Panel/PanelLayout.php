<?php

namespace Modules\Content\Http\Panel;

use Modules\Panel\Services\PanelMasterLayout;

/**
 * class PanelLayout
 * define the admin panel layout items, related to the Content module
 */
class PanelLayout extends PanelMasterLayout
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $types = content()->types()->having("panel")->sortby('order')->get();

        foreach ($types as $type) {
            $this->addSidebarItem("content-$type->slug", 1)
                 ->icon($type->icon)
                 ->order(31)
                 ->link(panel()->link()->name("content-browse")) //TODO: Link to be checked when front-end got ready.
            ;

        }
    }
}
