<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\GeneralController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-templates
 *                    Templates
 * @apiDescription    List of all available registered content features
 * @apiVersion        1.0.0
 * @apiName           Templates
 * @apiGroup          Content
 * @apiPermission     Guest
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *         "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *         "text",
 *         "faq"
 *      }
 * }
 * @method GeneralController controller()
 */
class TemplatesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Templates";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'GeneralController@getTemplates';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "authenticated_user" => "folan",
        ], [
             "text",
             "faq",
        ]);
    }
}
