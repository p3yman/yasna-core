<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\PostController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-post-fetch
 *                    Post Fetch
 * @apiDescription    Post Fetch
 * @apiVersion        1.0.0
 * @apiName           Post Fetch
 * @apiGroup          Content
 * @apiPermission     Admin
 * @apiParam {string} [id] hashid of the post in question
 * @apiParam {string} [type] hashid of the type, in case of new post and absence of `id`
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          TODO: Fill this!
 *      },
 *      "results": {
 *          TODO: Fill this!
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  PostController controller()
 */
class PostFetchEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Post Fetch";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostController@fetch';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "reached" => "PostFetchEndpoint",
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
