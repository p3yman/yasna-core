<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\PostController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-post-single
 *                    Post Single
 * @apiDescription    The Single Presentation of a Post
 * @apiVersion        1.0.0
 * @apiName           Post Single
 * @apiGroup          Content
 * @apiPermission     Guest
 * @apiParam {string}   id  Hashid of a content post
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *               "status": 200,
 *                "metadata": {
 *                  "post_id": "qKXVA",
 *                  "authenticated_user": "QKgJx"
 *                  },
 *                  "results": {
 *                          "id": "qKXVA",
 *                          "created_at": 1557676545,
 *                          "updated_at": 1557676545,
 *                          "deleted_at": "",
 *                          "created_by": {
 *                                  "id": "",
 *                                  "full_name": ""
 *                          },
 *                          "updated_by": {
 *                                  "id": "",
 *                                  "full_name": ""
 *                           },
 *                          "deleted_by": {
 *                                  "id": "",
 *                                  "full_name": ""
 *                          },
 *                          "title": " post title",
 *                          "type": {
 *                                  "id": "qKXVA",
 *                                  "slug": "kharj-piam"
 *                          },
 *                          "published_at": 1557675702,
 *                          "published_by": {
 *                                  "id": "qKXVA",
 *                          "full_name": "folan folani"
 *                          }
 *                  }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Content not found!",
 *      "userMessage": "Content not found!",
 *      "errorCode": "404",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method PostController controller()
 */
class PostSingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Post Single";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // will be checked in the request layer
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostController@single';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "results" => [
                  "id"           => "qKXVA",
                  "created_at"   => 1557676545,
                  "updated_at"   => 1557676545,
                  "deleted_at"   => "",
                  "created_by"   => [
                       "id"        => "",
                       "full_name" => "",
                  ],
                  "updated_by"   => [
                       "id"        => "",
                       "full_name" => "",
                  ],
                  "deleted_by"   => [
                       "id"        => "",
                       "full_name" => "",
                  ],
                  "title"        => dummy()::persianTitle(),
                  "type"         => [
                       "id"   => "qKXVA",
                       "slug" => "kharj-piam",
                  ],
                  "published_at" => 1557675702,
                  "published_by" => [
                       "id"        => "qKXVA",
                       "full_name" => dummy()::persianName(),
                  ],
             ],
        ], [
             "post_id" => "qKXVA",
             "authenticated_user"=> hashid(user()->id),
        ]);
    }
}
