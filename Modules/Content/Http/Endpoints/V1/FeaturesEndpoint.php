<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\GeneralController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-features
 *                    Features
 * @apiDescription    List of all available registered content features
 * @apiVersion        1.0.0
 * @apiName           Features
 * @apiGroup          Content
 * @apiPermission     Guest
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "status": 200,
 *     "metadata": {
 *         "authenticated_user": "PKEnN"
 *     },
 *     "results": {
 *         "text": {
 *             "key": "text",
 *             "order": 11,
 *             "condition": true,
 *             "class": "Modules\\Content\\Http\\ContentFeatures\\TextFeature",
 *             "title": "Main Text"
 *         },
 *         "abstract": {
 *             "key": "abstract",
 *             "order": 11,
 *             "condition": true,
 *             "class": "Modules\\Content\\Http\\ContentFeatures\\AbstractFeature",
 *             "title": "Abstract Text"
 *         },
 *         "pin": {
 *             "key": "pin",
 *             "order": 11,
 *             "condition": true,
 *             "class": "Modules\\Content\\Http\\ContentFeatures\\PinFeature",
 *             "title": "Pin"
 *         }
 *     }
 * }
 * @method GeneralController controller()
 */
class FeaturesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Features";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'GeneralController@getFeatures';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "authenticated_user" => "folan",
        ], [
             "text"     => [
                  "key"       => "text",
                  "order"     => 11,
                  "condition" => true,
                  "class"     => 'Modules\Content\Http\ContentFeatures\TextFeature',
                  "title"     => "Main Text",
             ],
             "abstract" => [
                  "key"       => "abstract",
                  "order"     => 11,
                  "condition" => true,
                  "class"     => 'Modules\Content\Http\ContentFeatures\AbstractFeature',
                  "title"     => "Abstract Text",
             ],
             "pin"      => [
                  "key"       => "pin",
                  "order"     => 11,
                  "condition" => true,
                  "class"     => 'Modules\Content\Http\ContentFeatures\PinFeature',
                  "title"     => "Pin",
             ],
        ]);
    }
}
