<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\PostController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-post-list
 *                    List of Posts
 * @apiDescription    Get List of Posts with Hahsid or Slug of Content Type
 * @apiVersion        1.0.0
 * @apiName           List of Posts
 * @apiGroup          Content
 * @apiPermission     Guest
 * @apiParam {string}   types           Hashid or slug of a content type
 * @apiParam {String}   [foo]           whatever field with electors already set can be passed.
 * @apiParam {int=0,1}  [paginated=0]   Set the result must be paginated or not
 * @apiParam {int}      [per_page=15]   Set the number of results on each request
 * @apiParam {int}      [page=1]        Set the page number on pagination
 * @apiParam {int}      [limit=100]     Set limit to be applied on the number of posts to be retrieved
 * @apiParam {string}   [sort=id]       Set the order field
 * @apiParam {string=desc,asc} [order=desc] Set the order ordination
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *            "total": 1,
 *            "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "results": [
 *                           {
 *                           "id": "BxWEN",
 *                           "sisterhood": "Jz4ErMWnRlA0B2uPb",
 *                           "locale": "fa",
 *                           "is_draft": 1,
 *                           "type_id": "PNOLA",
 *                           "master_id": 0,
 *                           "user_id": "qKXVA",
 *                           "organization_id": 0,
 *                           "replacement_id": 0,
 *                           "title": "persian title",
 *                           "created_at": 1556085245,
 *                           "updated_at": 1556085245,
 *                           "published_at": null,
 *                           "published_by": 0,
 *                           "moderated_at": null,
 *                           "moderated_by": 0,
 *                           "deleted_at": null,
 *                           "created_by": 0,
 *                           "updated_by": 0,
 *                           "deleted_by": 0,
 *                           "slug": "jabz-kortana",
 *                           "text": "persian text"
 *                    }
 *               ]
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Content not found!",
 *      "userMessage": "Content not found!",
 *      "errorCode": "404",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method PostController controller()
 */
class PostListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List of Posts";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // may have conditions imposed in the request layer
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "total"              => 13,
             "authenticated_user" => "qKXVA",
        ], [
             "results" => [
                  [
                       "id"              => "BxWEN",
                       "sisterhood"      => "Jz4ErMWnRlA0B2uPb",
                       "locale"          => "fa",
                       "is_draft"        => 1,
                       "type_id"         => "PNOLA",
                       "master_id"       => 0,
                       "user_id"         => "qKXVA",
                       "organization_id" => 0,
                       "replacement_id"  => 0,
                       "title"           => dummy()::persianTitle(),
                       "created_at"      => 1556085245,
                       "updated_at"      => 1556085245,
                       "published_at"    => null,
                       "published_by"    => 0,
                       "moderated_at"    => null,
                       "moderated_by"    => 0,
                       "deleted_at"      => null,
                       "created_by"      => 0,
                       "updated_by"      => 0,
                       "deleted_by"      => 0,
                       "slug"            => "jabz-kortana",
                       "text"            => dummy()::persianText(),
                  ],
             ],
        ]);
    }
}
