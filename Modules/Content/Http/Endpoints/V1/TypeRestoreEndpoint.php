<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\TypeController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {PUT}
 *                    /api/modular/v1/content-type-restore
 *                    Content Type Restore (Undelete)
 * @apiDescription    undelete action for the upstream
 * @apiVersion        1.0.0
 * @apiName           Content Type Restore (Undelete)
 * @apiGroup          Content
 * @apiPermission     Developer
 * @apiParam {string} id       . hashid of the type
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "QKgJx"
 *      },
 *      "results": {
 *          done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method TypeController controller()
 */
class TypeRestoreEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Content Type Restore (Undelete)";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_PUT;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TypeController@restore';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "metadata" => hashid(12123213),
        ]);
    }
}
