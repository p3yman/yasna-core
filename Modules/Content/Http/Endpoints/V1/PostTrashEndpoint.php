<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\PostController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/content-post-trash
 *                    Post Trash
 * @apiDescription    delete a post in the admin panel
 * @apiVersion        1.0.0
 * @apiName           Post Delete
 * @apiGroup          Content
 * @apiPermission     Admin
 * @apiParam {String} hashid The hashid of the content
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": 1
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method PostController controller()
 */
class PostTrashEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Post Trash";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostController@trash';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "metadata"           => hashid(1),
             "authenticated_user" => hashid(1),
        ]);
    }
}
