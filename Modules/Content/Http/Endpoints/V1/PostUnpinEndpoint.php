<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\PostController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/content-post-unpin
 *                    Postun Pin
 * @apiDescription    Content post unpin
 * @apiVersion        1.0.0
 * @apiName           Postun Pin
 * @apiGroup          Content
 * @apiPermission     Admin
 * @apiParam {string}   hashid Hashid of the post
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 500,
 *      "developerMessage": "Error in data storage",
 *      "userMessage": "Error in data storage.This is a temporary problem.Please try again later.",
 *      "errorCode": "507",
 *      "moreInfo": "endpoint.moreInfo.endpoint-507",
 *      "errors": []
 * }
 * @method PostController controller()
 */
class PostUnpinEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Postun Pin";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostController@unPin';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "authenticated_user" => hashid(1),
        ]);

    }
}
