<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\TypeController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/content-type-save
 *                    Type Save
 * @apiDescription    Content type upstream save
 * @apiVersion        1.0.0
 * @apiName           Type Save
 * @apiGroup          Content
 * @apiPermission     Developer
 * @apiParam {string} [id]     . hashid of the type, (create mode, in case of absence)
 * @apiParam {string} slug     . slug of the type
 * @apiParam {int}    order    . order of the type to appear in the menus etc.
 * @apiParam {string} template . the default template of the type
 * @apiParam {string} locales  . the comma-separated list of languages supported by the type
 * @apiParam {array}  titles   . the array of titles for each locale
 * @apiParam {array}  features . the features of the type
 * @apiParam {string} icon     . the icon (from a package of the front-end choice. We don't give a shit to this.)
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "QKgJx"
 *      },
 *      "results": {
 *          done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  TypeController controller()
 */
class TypeSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Type Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TypeController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "metadata" => hashid(12123213),
        ]);
    }
}
