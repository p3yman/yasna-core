<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\TypeController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-type-list
 *                    Type List
 * @apiDescription    Content type upstream list
 * @apiVersion        1.0.0
 * @apiName           Type List
 * @apiGroup          Content
 * @apiPermission     Developer
 * @apiParam {Integer=0,1} [trash]       Specifies that the results must look into the trashed data only (default 0).
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer}     [page=1]      Specifies the page number of results (only with `paginated=1`).
 * @apiParam {Integer}     [per_page=15] Specifies the number of results on each page (only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 11,
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "id": "qKXVA",
 *          "slug": "folan",
 *          "order": 12,
 *          "locales": "fa,en",
 *          "template": "text",
 *          "created_at": 1553634267,
 *          "updated_at": 1554708902,
 *          "deleted_at": 1554711945,
 *          "created_by": null,
 *          "updated_by": "qKXVA",
 *          "deleted_by": null,
 *          "converted": 0,
 *          "titles": {
 *              "fr": "Bonjoure",
 *              "en": "Hello"
 *          },
 *          "features": [
 *              "synopsis",
 *              "text"
 *          ]
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  TypeController controller()
 */
class TypeListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Type List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TypeController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"         => "qKXVA",
             "slug"       => "folan",
             "order"      => 12,
             "locales"    => "fa,en",
             "template"   => "text",
             "created_at" => 1553634267,
             "updated_at" => 1554708902,
             "deleted_at" => 1554712289,
             "created_by" => null,
             "updated_by" => "qKXVA",
             "deleted_by" => null,
             "converted"  => 0,
             "features"   => [
                  "synopsis",
                  "text",
             ],
             "titles"     => [
                  "fa" => dummy()::persianTitle(),
                  "en" => "First Roozbeh",
             ],
        ], [
             "total" => "1",
        ]);
    }
}
