<?php

namespace Modules\Content\Http\Endpoints\V1;

use Modules\Content\Http\Controllers\V1\PostController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/content-post-grid
 *                    Post Grid
 * @apiDescription    Post Grid View
 * @apiVersion        1.0.0
 * @apiName           Post Grid
 * @apiGroup          Content
 * @apiPermission     Admin
 * @apiParam {string} tab       the grid tab
 * @apiParam {string} type      the content type (slug or hashid)
 * @apiParam {string} [search]  search keyword (+ any other thing available in the electors)
 * @apiSuccessExample Success-Response: TODO: This is the default save feedback. Change if your work is different
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method PostController controller()
 */
class PostGridEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Post Grid";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // <~~ will be checked in the request layer
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Content\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostController@getGrid';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1,100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
