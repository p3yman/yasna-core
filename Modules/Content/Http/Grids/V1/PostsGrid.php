<?php

namespace Modules\Content\Http\Grids\V1;

use App\Models\ContentType;
use Modules\Content\Http\Requests\V1\PostGridRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Panel\Services\PanelGridLayout;
use Modules\Forms\Services\FormBinder;
use Modules\Yasna\Services\YasnaModel;

/**
 * @property PostGridRequest $request
 */
class PostsGrid extends PanelGridLayout
{
    /** @var ContentType */
    private $type;

    /** @var array */
    private $features;



    /**
     * @inheritdoc
     */
    public function getHeaderTitle(): string
    {
        return $this->type->title;
    }



    /**
     * @inheritdoc
     */
    public function getHeaderIcon(): string
    {
        $icon = $this->type->icon;

        if (!$icon) {
            $icon = "md-card-bulleted-outline";
        }

        return $icon;
    }



    /**
     * @inheritdoc
     */
    public function hasSearch(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function columns(): void
    {
        $this->add('title', 11)
             ->title(trans("validation.attributes.title"))
             ->icon("md-bandage")
             ->sortable()
             ->method("titleColumn")
        ;
    }



    /**
     * @inheritdoc
     */
    public function buttons(): void
    {
        $this->add("new", 1)
             ->title(trans("content::types.create"))
             ->link('') //TODO: We need a front-end route.
        ;
    }



    /**
     * @inheritdoc
     */
    public function tabs(): void
    {

        // TODO: Links are to be added!

        $this->add("published", 1)
             ->title(trans("content::posts.tab.published"))
             ->icon("md-seal")
             ->link()
        ;

        $this->add("scheduled", 1)
             ->title(trans("content::posts.tab.scheduled"))
             ->icon("md-clock-start")
             ->link()
        ;

        $this->add("pending", 1)
             ->title(trans("content::posts.tab.pending"))
             ->icon("md-progress-check")
             ->link()
        ;

        $this->add("my_posts", 1)
             ->title(trans("content::posts.tab.my_posts"))
             ->icon("md-emoticon-excited-outline")
             ->link()
        ;

        $this->add("my_drafts", 1)
             ->title(trans("content::posts.tab.my_drafts"))
             ->icon("md-test-tube")
             ->link()
        ;

        $this->add("bin", 99)
             ->icon("md-trash-can")
             ->title(trans("panel::action.bin"))
             ->link()
        ;
    }



    /**
     * @inheritdoc
     */
    public function massActions(): void
    {
        $this->add("trash", 1)
             ->title(trans("panel::action.trash"))
             ->icon("")
             ->link()
        ;
    }



    /**
     * bind the title column of the grid
     *
     * @param YasnaModel $model
     * @param FormBinder $binder
     *
     * @return FormBinder
     */
    public function titleColumn($model, $binder): FormBinder
    {
        //TODO: Follow the below example to make your columns. (You may need to replace the whole method.)

        $binder->add("title")->text($model->title);

        return $binder;
    }



    /**
     * @inheritdoc
     */
    protected function boot()
    {
        $this->type     = $this->request->model;
        $this->features = $this->type->featureInstancesArray();

        $this->loadFeaturedBaseGridItems();
    }



    /**
     * load featured-base grid items
     *
     * @return void
     */
    private function loadFeaturedBaseGridItems()
    {
        /** @var FeatureAbstract $feature */
        foreach ($this->features as $feature) {
            $feature->gridItems();
        }

    }
}
