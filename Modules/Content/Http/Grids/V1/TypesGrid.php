<?php

namespace Modules\Content\Http\Grids\V1;


use App\Models\ContentType;
use Modules\Forms\Services\FormBinder;
use Modules\Panel\Services\PanelGridLayout;

class TypesGrid extends PanelGridLayout
{
    /**
     * @inheritdoc
     */
    public function getHeaderTitle(): string
    {
        return trans("content::types.title");
    }



    /**
     * @inheritdoc
     */
    public function getHeaderIcon(): string
    {
        return "md-clipboard-text-outline";
    }



    /**
     * @inheritdoc
     */
    public function hasSearch(): bool
    {
        return false;
    }



    /**
     * @inheritdoc
     */
    public function columns(): void
    {
        $this->add('title', 1)
             ->title(trans("validation.attributes.title"))
             ->method("titleColumn")
        ;

        $this->add("features", 2)
             ->title(trans("content::types.features"))
             ->method("featuresColumn")
        ;

        $this->add("actions", 9)
             ->title(trans("validation.attributes.action"))
             ->method("actionColumn")
        ;
    }



    /**
     * @inheritdoc
     */
    public function buttons(): void
    {
        $this->add("new", 1)
             ->title(trans("content::types.create"))
             ->link() //@TODO: To be Completed when link interface is prepared.
        ;
    }



    /**
     * @inheritdoc
     */
    public function tabs(): void
    {
        $this->add("actives", 1)
             ->title()
             ->title(trans("panel::action.actives"))
             ->active()
             ->link() //@TODO: To be Completed when link interface is prepared.
        ;

        $this->add("bin", 99)
             ->title()
             ->title(trans("panel::action.bin"))
             ->link() //@TODO: To be Completed when link interface is prepared.
        ;
    }



    /**
     * @inheritdoc
     */
    public function massActions(): void
    {
        $this->add("trash", 1)
             ->title()
             ->title(trans("panel::action.trash"))
             ->link() //@TODO: To be Completed when link interface is prepared.
        ;
    }



    /**
     * bind the title column of the grid
     *
     * @param ContentType $model
     * @param FormBinder  $binder
     *
     * @return FormBinder
     */
    public function titleColumn($model, $binder): FormBinder
    {
        $binder->add("title")->text($model->title);

        return $binder;
    }



    /**
     * bind the features column of the grid
     *
     * @param ContentType $model
     * @param FormBinder  $binder
     *
     * @return FormBinder
     */
    public function featuresColumn($model, $binder): FormBinder
    {
        //TODO: To be completed!
        //TODO: Story #5312 - We need some more attempts for adding image feature in grid's output.

        return $binder;
    }



    /**
     * bind the actions column of the grid
     *
     * @param ContentType $model
     * @param FormBinder  $binder
     *
     * @return FormBinder
     */
    public function actionColumn($model, $binder): FormBinder
    {
        //TODO: To be completed!
        return $binder;
    }
}
