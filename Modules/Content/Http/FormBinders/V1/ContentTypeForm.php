<?php

namespace Modules\Content\Http\FormBinders\V1;

use Modules\Forms\Services\FormBinder;

class ContentTypeForm extends FormBinder
{
    /**
     * @inheritdoc
     */
    public function components()
    {
        $this->add("hidden")
             ->name("id")
        ;

        $this->add("text")
             ->name("slug")
             ->label(trans("validation.attributes.slug"))
        ;

        $this->add("divider");
        $this->addTitleComponents();
        $this->add("divider");

        $this->add("number")
             ->name("order")
             ->label(trans("validation.attributes.order"))
        ;

        $this->add("select")
             ->name("template")
             ->label(trans("validation.attributes.template"))
             ->options($this->getTemplates())
        ;

        $this->add("select")
             ->name("features")
             ->label(trans("content::types.features"))
             ->options($this->getFeatures())
             ->multi()
             ->allowEmpty()
        ;

        $this->add("text")
             ->name("icon")
             ->label(trans("validation.attributes.icon"))
        ;
    }



    /**
     * add title components
     *
     * @return void
     */
    private function addTitleComponents()
    {
        $locales = get_setting("site_locales");

        foreach ($locales as $locale) {
            $this->add("text")
                 ->name("titles.$locale")
                 ->label(trans("validation.attributes.title") . SPACE . trans("yasna::lang.$locale"))
            ;
        }

    }



    /**
     * get features options in the correct format
     *
     * @return array
     */
    private function getFeatures(): array
    {
        return array_values(array_map(
             function ($item) {
                 return [
                      "value" => $item['key'],
                      "label" => $item['title'],
                 ];
             },
             content()->registeredFeatures()
        ));
    }



    /**
     * get templates options in the correct format
     *
     * @return array
     */
    private function getTemplates(): array
    {
        return array_map(
             function ($item) {
                 return [
                      "value" => $item,
                      "label" => title_case($item),
                 ];
             },
             content()->registeredTemplates()
        );
    }
}
