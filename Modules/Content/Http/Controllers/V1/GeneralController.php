<?php

namespace Modules\Content\Http\Controllers\V1;

use Modules\Yasna\Services\YasnaApiController;

class GeneralController extends YasnaApiController
{

    /**
     * get a list of registered features
     *
     * @return array
     */
    public function getFeatures()
    {
        return $this->success(content()->registeredFeatures());
    }



    /**
     * get a list of registered features
     *
     * @return array
     */
    public function getTemplates()
    {
        return $this->success(content()->registeredTemplates());
    }
}
