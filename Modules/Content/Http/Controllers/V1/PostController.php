<?php

namespace Modules\Content\Http\Controllers\V1;

use Modules\Content\Http\Grids\V1\PostsGrid;
use Modules\Content\Http\Requests\V1\GetPostsListRequest;
use Modules\Content\Http\Requests\V1\PostDestroyRequest;
use Modules\Content\Http\Requests\V1\PostGridRequest;
use Modules\Content\Http\Requests\V1\PostPinRequest;
use Modules\Content\Http\Requests\V1\PostRestoreRequest;
use Modules\Content\Http\Requests\V1\PostSingleRequest;
use Modules\Content\Http\Requests\V1\PostTrashRequest;
use Modules\Content\Http\Requests\V1\PostFetchRequest;
use Modules\Content\Http\Requests\V1\PostSaveRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Yasna\Services\YasnaApiController;

class PostController extends YasnaApiController
{
    /**
     * fetch a post to arrange for a editor panel
     *
     * @param PostFetchRequest $request
     *
     * @return array
     */
    public function fetch(PostFetchRequest $request)
    {
        $type = $request->model->type_model;
        $data = $request->model->toResource(); // <~~ It's better to eliminate the resources, without asking front-end.
        $meta = [
             "form" => $type->editorFormArray(),
        ];

        return $this->success($data, $meta);
    }



    /**
     * save a post
     *
     * @param PostSaveRequest $request
     *
     * @return array
     */
    public function save(PostSaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request);

        /** @var FeatureAbstract $feature */
        foreach ($request->features as $feature) {
            $feature->afterSave($saved_model, $request);
        }

        return $this->modelSaveFeedback($saved_model);
    }



    /**
     * delete a post
     *
     * @param PostTrashRequest $request
     *
     * @return array
     */
    public function trash(PostTrashRequest $request)
    {
        $model = $request->model->delete();

        return $this->typicalSaveFeedback($model);

    }



    /**
     * hard delete a post
     *
     * @param PostDestroyRequest $request
     *
     * @return array
     */
    public function destroy(PostDestroyRequest $request)
    {
        $model = $request->model->hardDelete();

        return $this->typicalSaveFeedback($model);
    }



    /**
     * perform undelete action of the post
     *
     * @param PostRestoreRequest $request
     *
     * @return array
     */
    public function restore(PostRestoreRequest $request)
    {
        $saved = $request->model->undelete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * perform pin action of the post
     *
     * @param PostPinRequest $request
     *
     * @return array
     */
    public function pin(PostPinRequest $request)
    {
        $pinned = $request->model->pin();

        return $this->typicalSaveFeedback($pinned);
    }



    /**
     * perform pin action of the post
     *
     * @param PostPinRequest $request
     *
     * @return array
     */
    public function unPin(PostPinRequest $request)
    {
        $un_pinned = $request->model->unPin();

        return $this->typicalSaveFeedback($un_pinned);
    }



    /**
     * get a list of posts, filtered by query strings
     *
     * @param GetPostsListRequest $request
     *
     * @return array
     */
    public function list(GetPostsListRequest $request)
    {
        $builder = content()->post()->elector($request->toArray());

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get the single of post
     *
     * @param PostSingleRequest $request
     *
     * @return array
     */
    public function single(PostSingleRequest $request)
    {
        $post_single = $request->model->toSingleResource();

        return $this->success($post_single, ['post_id' => $request->id]);
    }



    /**
     * get a grid-view of the list of posts
     *
     * @param PostGridRequest $request
     *
     * @return array
     */
    public function getGrid(PostGridRequest $request)
    {
        $builder = content()
             ->post()
             ->elector($request->toArray())
             ->where("type_id", $request->model->id)
        ;

        return (new PostsGrid($builder, $request))->toArray();
    }
}
