<?php

namespace Modules\Content\Http\Controllers\V1;

use App\Models\ContentType;
use Modules\Content\Events\TypeCreated;
use Modules\Content\Events\TypeUpdated;
use Modules\Content\Http\FormBinders\V1\ContentTypeForm;
use Modules\Content\Http\Grids\V1\TypesGrid;
use Modules\Content\Http\Requests\V1\TypeDestroyRequest;
use Modules\Content\Http\Requests\V1\TypeFetchRequest;
use Modules\Content\Http\Requests\V1\TypeRestoreRequest;
use Modules\Content\Http\Requests\V1\TypeSaveRequest;
use Modules\Content\Http\Requests\V1\TypeTrashRequest;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\Yasna\Services\YasnaApiController;

class TypeController extends YasnaApiController
{
    /**
     * get an upstream list of content types
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array
     */
    public function list(SimpleYasnaListRequest $request)
    {
        $builder = model('content-type')->newBuilderInstance();

        if ($request->trash) {
            $builder = $builder->onlyTrashed();
        }

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get a grid-view of the list of content types
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array
     */
    public function grid(SimpleYasnaListRequest $request)
    {
        $builder = model('content-type')->newBuilderInstance();

        return (new TypesGrid($builder, $request))->toArray();
    }



    /**
     * fetch a content type to be edited
     *
     * @param TypeFetchRequest $request
     *
     * @return array
     */
    public function fetch(TypeFetchRequest $request)
    {
        return $this->success(
             $request->model->toFetchResource(),
             [
                  "form" => (new ContentTypeForm())->toArray(),
             ]
        );
    }



    /**
     * perform upstream save action of the type
     *
     * @param TypeSaveRequest $request
     *
     * @return array
     */
    public function save(TypeSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        $this->raiseSaveEvents($saved, $request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * perform soft delete action of the type
     *
     * @param TypeTrashRequest $request
     *
     * @return array
     */
    public function trash(TypeTrashRequest $request)
    {
        $saved = $request->model->delete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * perform undelete action of the type
     *
     * @param TypeRestoreRequest $request
     *
     * @return array
     */
    public function restore(TypeRestoreRequest $request)
    {
        $saved = $request->model->undelete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * perform permanent delete action of the type
     *
     * @param TypeDestroyRequest $request
     *
     * @return array
     */
    public function destroy(TypeDestroyRequest $request)
    {
        $saved = $request->model->hardDelete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * raise events after a successful save
     *
     * @param ContentType     $saved
     * @param TypeSaveRequest $request
     *
     * @return void
     */
    private function raiseSaveEvents(ContentType $saved, TypeSaveRequest $request)
    {
        if (!$saved->exists) {
            return;
        }

        if ($request->createMode()) {
            event(new TypeCreated($saved));
        } else {
            event(new TypeUpdated($saved, $request->model));
        }
    }
}
