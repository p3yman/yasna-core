<?php

namespace Modules\Content\Http\ContentFeatures;

use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class LocalesFeature extends FeatureAbstract
{
    /**
     * @inheritdoc
     */
    public function slug()
    {
        return "locales";
    }



    /**
     * @inheritdoc
     */
    public function persianTitle()
    {
        return trans("content::features.locales");
    }
    
    /**
     * @inheritdoc
     */
    public function editorSideFormBinder()
    {
        $form = new FormBinder();
    
        // @TODO: Make options dynamic
        $form->add("locales")
             ->title(trans("content::features.locales"))
             ->current('fa')
             ->options([
                 'fa' => 'QxVsc',
                 'en' => null,
                 'ar' => 'XVsWs',
             ])
        ;
    
        return $form;
    }
}
