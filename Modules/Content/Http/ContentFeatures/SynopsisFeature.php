<?php

namespace Modules\Content\Http\ContentFeatures;

use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class SynopsisFeature extends FeatureAbstract
{
    /**
     * @inheritdoc
     */
    public function slug()
    {
        return "synopsis";
    }



    /**
     * @inheritdoc
     */
    public function persianTitle()
    {
        return trans("content::features.synopsis");
    }



    /**
     * @inheritdoc
     */
    public function editorMainFormBinder()
    {
        $form = new FormBinder();
    
        $form->add("panel")
             ->title(trans('content::features.synopsis'))
             ->name("synopsis-panel")
             ->children([
                 component("textarea")->name("synopsis")->noLabel()->cssClass("d-f mb0")->rows(5)
             ]);
        
        return $form;
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "synopsis",
        ];
    }

}
