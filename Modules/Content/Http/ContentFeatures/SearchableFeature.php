<?php


namespace Modules\Content\Http\ContentFeatures;


use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class SearchableFeature extends FeatureAbstract
{

    /**
     * @inheritDoc
     */
    public function slug()
    {
        return "searchable";
    }



    /**
     * @inheritDoc
     */
    public function persianTitle()
    {
        return trans("content::features.searchable");
    }


}