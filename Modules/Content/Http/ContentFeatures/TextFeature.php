<?php

namespace Modules\Content\Http\ContentFeatures;

use Modules\Content\Http\Requests\V1\TextFeatureRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class TextFeature extends FeatureAbstract
{
    /**
     * @inheritdoc
     */
    public function slug()
    {
        return "text";
    }



    /**
     * @inheritdoc
     */
    public function persianTitle()
    {
        return trans("content::features.text");
    }



    /**
     * @inheritdoc
     */
    public function editorMainFormBinder()
    {
        $form = new FormBinder();

        $form->add("editor")->name("text")->order(12);

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "text",
        ];
    }



    /**
     * @inheritdoc
     */
    public function postValidator()
    {
        return TextFeatureRequest::class;
    }
}
