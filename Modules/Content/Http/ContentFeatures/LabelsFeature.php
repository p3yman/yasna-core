<?php


namespace Modules\Content\Http\ContentFeatures;

use App\Models\ContentPost;
use Modules\Content\Http\Requests\V1\LabelsFeatureRequest;
use Modules\Content\Http\Requests\V1\PostSaveRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class LabelsFeature extends FeatureAbstract
{

    /**
     * @inheritDoc
     */
    public function slug()
    {
        return "labels";
    }



    /**
     * @inheritDoc
     */
    public function persianTitle()
    {
        return trans('content::features.label');
    }



    /**
     * @inheritdoc
     */
    public function editorMainFormBinder()
    {
        $form = new FormBinder();

        $form->add("content-labels")
             ->name("_labels")
             ->order(16)
             ->hasSettingButton($this->ifAuthorized())
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "labels",
        ];
    }



    /**
     * @inheritdoc
     */
    public function postValidator()
    {
        return LabelsFeatureRequest::class;
    }



    /**
     * @inheritdoc
     */
    public function afterSave(ContentPost $post, PostSaveRequest $request)
    {
        $post->detachAllLabels();

        $labels = $request->input('_labels');

        foreach ((array) $labels as $label) {

            $model = model('label')->grabHashid($label);

            if ($model->exists) {
                $post->attachLabel($model->slug);
            }

        }

        return;

    }



    /**
     * check authorization create new label
     *
     * @return bool
     */
    public function ifAuthorized()
    {
        $contenttype = model('ContentType');

        return $contenttype->canConfigureLabels();
    }



    /**
     * @inheritdoc
     */
    public function toSingleResource(ContentPost $post)
    {
        $collections = $post->type->getAllLabelsRecursively()->get();
        $result      = $post::getMappedFromCollection($collections);

        return [
             "Labels_list" => $result,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toListResource(ContentPost $post)
    {
        $collections = $post->type->getAllLabelsRecursively()->get();
        $result      = $post::getMappedFromCollection($collections);

        return [
             "Labels_list" => $result,
        ];
    }

}
