<?php

namespace Modules\Content\Http\ContentFeatures;

use App\Models\ContentPost;
use Modules\Content\Http\Requests\V1\PinFeatureRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class PinFeature extends FeatureAbstract
{
    /**
     * @inheritdoc
     */
    public function slug()
    {
        return "pin";
    }



    /**
     * @inheritdoc
     */
    public function persianTitle()
    {
        return trans("content::features.pin");
    }



    /**
     * @inheritDoc
     */
    public function editorSideFormBinder()
    {
        $form = new FormBinder();
    
        $form->add("panel")
             ->name("pin-panel")
             ->children([
                 component('checkbox')->name('pin')->cssClass("mb0")->label(trans("content::features.pin")),
             ]);

        return $form;
    }



    /**
     * @inheritDoc
     */
    public function postValidator()
    {
        return PinFeatureRequest::class;
    }



    /**
     * @inheritdoc
     */
    public function toSingleResource(ContentPost $post)
    {
        return [
             "pinned_at" => $post->pinned_at ? carbon()::parse($post->pinned_at)->getTimestamp() : "",
             "pinned_by" => user()::quickFind($post->pinned_by)->toMinimalResource(),
        ];
    }



    /**
     * @inheritdoc
     */
    public function toListResource(ContentPost $post)
    {
        return [
             "pinned_at" => $post->pinned_at ? carbon()::parse($post->pinned_at)->getTimestamp() : "",
        ];
    }
}
