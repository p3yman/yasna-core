<?php


namespace Modules\Content\Http\ContentFeatures;


use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class SecondTitleFeature extends FeatureAbstract
{

    /**
     * @inheritDoc
     */
    public function slug()
    {
        return "title2";
    }



    /**
     * @inheritDoc
     */
    public function persianTitle()
    {
        return trans('content::features.second_title');
    }



    /**
     * @inheritdoc
     */
    public function editorMainFormBinder()
    {
        $form = new FormBinder();

        $form->add("text")->name("title2")->label(trans('content::features.second_title'))->order(10);

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "title2",
        ];
    }
}