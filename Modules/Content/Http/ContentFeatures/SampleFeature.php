<?php


namespace Modules\Content\Http\ContentFeatures;


use App\Models\ContentType;
use Modules\Content\Http\Grids\V1\PostsGrid;
use Modules\Content\Http\Requests\V1\ImageFeatureRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class SampleFeature extends FeatureAbstract
{

    /**
     * @inheritDoc
     */
    public function slug()
    {
        return "sample";
    }



    /**
     * @inheritDoc
     */
    public function persianTitle()
    {
        return "SAMPLE";
    }



    /**
     * @inheritdoc
     */
    public function editorMainFormBinder()
    {
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
        ];
    }



    /**
     * @inheritdoc
     */
    public function postValidator()
    {
        return ImageFeatureRequest::class;
    }



    /**
     * @inheritdoc
     */
    public function gridItems()
    {
        PostsGrid::addColumn('sample', 1)
                 ->title("SAMPLE")
                 ->icon("md-image-filter-vintage")
                 ->method("sampleColumn")
                 ->inClass(static::class)
        ;

        PostsGrid::addButton("sample", 10)
                 ->title("SAMPLE")
                 ->link(123123)
        ;

        PostsGrid::addMassAction("sample", 10)
                 ->title("SAMPLE")
                 ->icon("md-feather")
                 ->link()
        ;
    }



    /**
     * bind the price column of the grid
     *
     * @param ContentType $model
     * @param FormBinder  $binder
     *
     * @return FormBinder
     */
    public static function sampleColumn($model, $binder): FormBinder
    {
        $binder->add("title")->text("SAMPLE THING!");

        return $binder;
    }
}
