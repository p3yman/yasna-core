<?php

namespace Modules\Content\Http\ContentFeatures;

use Modules\Content\Services\FeatureAbstract;

class PanelFeature extends FeatureAbstract
{
    /**
     * @inheritdoc
     */
    public function slug()
    {
        return "panel";
    }



    /**
     * @inheritdoc
     */
    public function persianTitle()
    {
        return trans("content::features.panel");
    }
}
