<?php


namespace Modules\Content\Http\ContentFeatures;


use App\Models\ContentPost;
use App\Models\ContentType;
use Modules\Content\Http\Grids\V1\PostsGrid;
use Modules\Content\Http\Requests\V1\ImageFeatureRequest;
use Modules\Content\Http\Requests\V1\PostSaveRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class ImageFeature extends FeatureAbstract
{

    //TODO: We don't have image feature detail in post fetch response. we can have some resource-related magics here!

    /**
     * @inheritDoc
     */
    public function slug()
    {
        return "image";
    }



    /**
     * @inheritDoc
     */
    public function persianTitle()
    {
        return trans('content::features.image');
    }



    /**
     * @inheritdoc
     */
    public function editorSideFormBinder()
    {
        $form = new FormBinder();
    
        $form->add("panel")
             ->title(trans('content::features.image'))
             ->name("image-panel")
             ->children([
                 component("uploader")->name("_image")->maxFiles(1)->order(15)->noLabel()->cssClass('mb0')
             ]);
        
        return $form;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "image",
        ];
    }



    /**
     * @inheritdoc
     */
    public function postValidator()
    {
        return ImageFeatureRequest::class;
    }



    /**
     * @inheritdoc
     */
    public function afterSave(ContentPost $post, PostSaveRequest $request)
    {
        $fileId = null;
        if ($request->input('_image')) {
            $fileId = hashid($request->input('_image'));
        }
        if ($fileId) {
            uploader()->file($fileId, true)->attachTo($post);
        }
        return;
    }



    /**
     * @inheritdoc
     */
    public function gridItems()
    {
        PostsGrid::addColumn('image', 1)
                 ->title(trans("validation.attributes.title"))
                 ->icon("md-image-filter-vintage")
                 ->method("imageColumn")
                 ->inClass(static::class)
        ;
    }



    /**
     * bind the price column of the grid
     *
     * @param ContentType $model
     * @param FormBinder  $binder
     *
     * @return FormBinder
     */
    public static function imageColumn($model, $binder): FormBinder
    {
        $binder->add("title")->text("Image to be displayed here!");

        //TODO: to be replaced by image component, once the component gets ready.

        return $binder;
    }
}
