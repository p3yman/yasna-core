<?php


namespace Modules\Content\Http\ContentFeatures;


use Modules\Content\Http\Requests\V1\CustomPublishFeatureRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class CustomPublishedAtFeature extends FeatureAbstract
{

    /**
     * @inheritDoc
     */
    public function slug()
    {
        return "custom-published-at";
    }



    /**
     * @inheritDoc
     */
    public function persianTitle()
    {
        return trans('content::features.custom_publish_date');
    }



    /**
     * @inheritdoc
     */
    public function editorSideFormBinder()
    {
        $form = new FormBinder();
    
        $form->add("panel")
             ->title(trans('content::features.custom_publish_date'))
             ->name("custom_publish_date-panel")
             ->children([
                 component("datetime")->name("published_at")->order(14)->noLabel()->cssClass("mb0")->rows(5)
             ]);

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "published_at",
        ];
    }


    /**
     * @inheritdoc
     */
    public function postValidator()
    {
        return CustomPublishFeatureRequest::class;
    }
}
