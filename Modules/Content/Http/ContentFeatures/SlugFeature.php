<?php

namespace Modules\Content\Http\ContentFeatures;

use Modules\Content\Http\Requests\V1\SlugFeatureRequest;
use Modules\Content\Services\FeatureAbstract;
use Modules\Forms\Services\FormBinder;

class SlugFeature extends FeatureAbstract
{
    /**
     * @inheritdoc
     */
    public function slug()
    {
        return "slug";
    }



    /**
     * @inheritdoc
     */
    public function persianTitle()
    {
        return trans("content::features.slug");
    }



    /**
     * @inheritdoc
     */
    public function editorMainFormBinder()
    {
        $form = new FormBinder();

        $form->add("slug")->name("slug");

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "slug",
        ];
    }



    /**
     * @inheritdoc
     */
    public function postValidator()
    {
        return SlugFeatureRequest::class;
    }
}
