<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentType;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class TypeSaveRequest
 *
 * @property ContentType $model
 */
class TypeSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "content-type";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id             = $this->model->id;
        $reserved_slugs = implode(",", ($this->model)::getReservedSlugs());
        $templates      = implode(",", content()->registeredTemplates());

        return [
             'slug'     => "required|alpha_dash|forbidden_chars:_|not_in:$reserved_slugs|unique:content_types,slug,$id,id",
             "order"    => "numeric|required|min:1",
             "template" => "required|in:$templates",
             "titles"   => "array|required",
             "features" => "array",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "slug",
             "order",
             "template",
             "titles",
             "features",
             "icon",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->unsetEmptyTitles();
        $this->setLocales();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->setData("features", implode(",", (array)$this->getData("features")));
    }



    /**
     * unset empty titles
     *
     * @return void
     */
    private function unsetEmptyTitles()
    {
        $titles = $this->getData("titles");

        foreach ($titles as $locale => $title) {
            if (!trim($title)) {
                unset($titles[$locale]);
            }
        }

        $this->setData("titles", $titles);
    }



    /**
     * set locales attribute from what received in the titles array
     *
     * @return void
     */
    private function setLocales()
    {
        $locales = implode(",", array_keys($this->getData("titles")));

        $this->setData("locales", $locales);
    }
}
