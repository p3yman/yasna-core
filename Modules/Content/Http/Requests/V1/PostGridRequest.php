<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentType;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * @property ContentType $model
 */
class PostGridRequest extends YasnaListRequest
{
    protected $model_name                  = "content-type";
    protected $model_slug_attribute        = "type";
    protected $should_allow_create_mode    = false;
    protected $should_load_model           = true;
    protected $should_load_model_with_slug = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->can("browse");
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "tab" => "in:published,scheduled,pending,my_posts,my_drafts,trash",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if (!$this->isset("tab")) {
            $this->setData("tab", "published");
        }
    }
}
