<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentPost;
use Modules\Content\Services\FeatureAbstract;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PostSaveRequest
 *
 * @property ContentPost $model
 */
class PostSaveRequest extends YasnaFormRequest
{

    /** @var array */
    public $features = [];

    /** @var array */
    protected $validators = [];

    /**
     * @inheritdoc
     */
    protected $model_name = "content-post";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $array = [
             "type",
             "sisterhood",
        ];

        /** @var FeatureAbstract $feature */
        foreach ($this->features as $feature) {
            $array = array_merge($array, (array)$feature->attributes());
        }

        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $array = array_merge($array, $validator->fillableFields());
        }

        return $array;
    }



    /**
     * @inheritdoc
     */
    public function correctionsBeforePurifier()
    {
        $this->resolveModelInCreateMode();
        $this->resolveFeatureInstances();
        $this->resolveFeaturedValidators();
        $this->injectionGuard();

        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $validator->setDataArray($this->getDataArray());
            $validator->correctionsBeforePurifier();
            $this->setDataArray($validator->getDataArray());
        }
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $validator->setDataArray($this->getDataArray());
            $validator->corrections();
            $this->setDataArray($validator->getDataArray());
        }
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $validator->setDataArray($this->getDataArray());
            $validator->mutators();
            $this->setDataArray($validator->getDataArray());
        }

        $this->setTypeId();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        $array = [];

        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $array = array_merge($array, $validator->purifier());
        }

        return $array;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $array = [];

        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $array = $this->mergeRules($array, $validator->rules());
        }

        return $array;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $array = [];

        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $array = array_merge($array, $validator->attributes());
        }

        return $array;
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        $array = [];

        /** @var YasnaFormRequest $validator */
        foreach ($this->validators as $validator) {
            $array = array_merge($array, $validator->messages());
        }

        return $array;
    }



    /**
     * resolve model in create mode. Invalid types are automatically handled in the authorize method.
     *
     * @return void
     */
    private function resolveModelInCreateMode()
    {
        $given_type = $this->unsetData("type");

        if ($this->editMode()) {
            return;
        }

        $type = content()->types()->findByHashid((string)$given_type);

        $this->model->type_id = $type->id;
    }



    /**
     * resolve feature instances
     *
     * @return void
     */
    private function resolveFeatureInstances()
    {
        $this->features = $this->model->type_model->featureInstancesArray();
    }



    /**
     * resolve an array of the featured validators
     *
     * @return void
     */
    private function resolveFeaturedValidators()
    {
        $array = [];

        /** @var FeatureAbstract $feature */
        foreach ($this->features as $feature) {
            $validator = $feature->postValidator();

            if ($validator) {
                /** @var YasnaFormRequest $instance */
                $instance = new $validator();
                $instance->setModel($this->model);

                $array[] = $instance;
            }
        }

        $this->validators = $array;
    }



    /**
     * set `type_id` to be used in the batchSave method
     */
    private function setTypeId()
    {
        $this->setData('type_id', $this->model->type_id);
    }
}
