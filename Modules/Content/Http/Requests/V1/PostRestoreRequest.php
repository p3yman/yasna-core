<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentPost;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PostDeleteRequest
 *
 * @property ContentPost $model
 */
class PostRestoreRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "content-post";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     */
    protected $should_load_trashed_models_only = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }
}
