<?php

namespace Modules\Content\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class LabelsFeatureRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Label";



    /**
     * @inheritdoc
     */
    public function Rules()
    {
        return [
             '_labels' => 'array',
        ];
    }
}
