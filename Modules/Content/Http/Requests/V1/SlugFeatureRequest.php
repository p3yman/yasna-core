<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentPost;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


/**
 * Class PostSaveRequest
 *
 * @property ContentPost $model
 */
class SlugFeatureRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $type_id = $this->model->type_model->id;

        return [
             'slug' => Rule::unique('content_posts', 'slug')
                           ->where('type_id', $type_id)
                           ->where('locale', getLocale())
                           ->ignore($this->model->id, 'id'),
        ];
    }
}
