<?php

namespace Modules\Content\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class TextFeatureRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }
    
    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
            "text",
        ];
    }
}
