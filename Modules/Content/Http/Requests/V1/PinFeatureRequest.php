<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentPost;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PinFeatureRequest
 *
 * @property ContentPost $model
 */
class PinFeatureRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "content-post";



    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
             'pin' => 'boolean',
        ];
    }



    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             "pin",
        ];
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {
        $this->setPinData();
    }



    /**
     * set pin data for new model
     */
    protected function setPinData()
    {

        if (!$this->isset('pin')) {
            return;
        }

        $pin   = (bool)$this->getData('pin');
        $model = (bool)$this->model->isPinned();
        $this->unsetData('pin');


        if (!$pin and $model) {
            $this->setData('pinned_at', null);
            $this->setData('pinned_by', 0);
            return;
        }

        if (!$pin and !$model) {
            return;
        }

        if ($pin and $model) {
            return;
        }

        if ($pin and !$model) {
            $this->setData('pinned_at', now());
            $this->setData('pinned_by', user()->id);
            return;
        }

    }
}
