<?php

namespace Modules\Content\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class TypeRestoreRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "content-type";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     */
    protected $should_load_trashed_models_only = true;
}
