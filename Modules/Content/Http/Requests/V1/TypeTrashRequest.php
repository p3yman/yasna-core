<?php

namespace Modules\Content\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class TypeTrashRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "content-type";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;
}
