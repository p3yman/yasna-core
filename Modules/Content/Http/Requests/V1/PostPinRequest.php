<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentPost;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PostPinRequest
 *
 * @property ContentPost $model
 */
class PostPinRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "content_post";



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return $this->model->canEdit();
    }
}
