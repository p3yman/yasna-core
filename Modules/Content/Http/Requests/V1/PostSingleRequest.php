<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentPost;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * @property ContentPost $model
 */
class PostSingleRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "ContentPost";

    /**
     * @inheritdoc
     */
    protected $should_load_model = true;

    /**
     * @inheritdoc
     */
    protected $should_load_model_with_hashid = true;



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return $this->model->canView();
    }



    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
             'id' => 'required',
        ];
    }
}
