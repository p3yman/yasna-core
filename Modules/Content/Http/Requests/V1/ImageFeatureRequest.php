<?php

namespace Modules\Content\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class ImageFeatureRequest extends YasnaFormRequest
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        //TODO: It'll be better to use this db fetch in ImageFeature in spite of a new fetch at it's afterSave method.

        $rule    = '';
        $file_id = hashid($this->getData('_image'));
        if (!$file_id) {
            // Passed file id must exist.
            $rule = 'accepted';
        }
        $file_mime = uploader()->file($file_id, true)->getOriginal('mime_type');
        if (preg_match("/image/i", $file_mime) != 1) {
            // Mime type must match.
            $rule = 'accepted';
        }

        return [
             '_image' => $rule,
        ];
    }



    /**
     * @inheritDoc
     */
    public function purifier()
    {
        return [];
    }

}
