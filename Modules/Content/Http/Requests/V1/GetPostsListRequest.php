<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentType;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;


class GetPostsListRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "ContentPost";

    /**
     * @inheritdoc
     */
    protected $should_load_model = true;

    /**
     * @inheritdoc
     */
    protected $should_load_model_with_hashid = true;

    /**
     * @inheritdoc
     */
    protected $should_load_model_with_slug = true;

    /**
     * instance of content-type model
     *
     * @var ContentType
     */
    private $model_instance;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "type"  => "required",
             "limit" => "numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function getModelInstance()
    {
        return $this->getInstanceOfModel();
    }



    /**
     * @inheritdoc
     */
    public function modelRequested(): bool
    {
        return true;
    }



    /**
     * @inheritDoc
     */
    public function corrections()
    {
        $this->limitCriteria();
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {
        $this->applyCurrentLocale();
        $this->setSlugInTypes();
    }



    /**
     * limit criteria to the published ones, when the user has not enough privileges.
     */
    private function limitCriteria()
    {
        /** @var ContentType $model */
        $model = $this->model;

        if (!user()->exists or $model->cannot("browse")) {
            $this->setData("criteria", "published");
        }
    }



    /**
     * apply current locale if necessary
     */
    private function applyCurrentLocale()
    {
        if ($this->getData("locales") == "*") {
            $this->unsetData("locales");
            return;
        }

        if ($this->isset("locales") or $this->isset("locales")) {
            return;
        }

        $this->setData("locales", getLocale());
    }



    /**
     * get instance of content-type model
     *
     * @return ContentType
     * @throws \Illuminate\Validation\ValidationException
     */
    private function getInstanceOfModel()
    {

        $model = $this->model_instance = $this->findContentType();

        if (!$model) {
            return $this->failIfModelNotFound();
        }

        /** @var content-type $model */
        return $model;
    }



    /**
     * find the content type model
     *
     * @return ContentType
     * @throws \Illuminate\Validation\ValidationException
     */
    private function findContentType()
    {
        if (!$this->isset('type')) {
            return $this->failIfModelNotFound();
        }

        $type = $this->getData('type');

        if (is_numeric($type)) {
            return $this->failIfModelNotFound();
        }

        return model('content-type')
             ->where('slug', $type)
             ->orWhere('id', hashid($type))
             ->first()
             ;
    }



    /**
     * supersede the model's slug to request
     */
    private function setSlugInTypes()
    {
        $model = $this->model_instance;

        $this->unsetData('type');

        $this->setData('types', $model->slug);
    }

}
