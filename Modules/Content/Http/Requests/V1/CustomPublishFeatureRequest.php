<?php

namespace Modules\Content\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class CustomPublishFeatureRequest extends YasnaFormRequest
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
             'published_at' => 'date',
        ];
    }



    /**
     * @inheritDoc
     */
    public function corrections()
    {
        $published_at = $this->model->published_at;

        if ($this->isset('published_at') and is_null($published_at)) {
            $this->setData('published_at', date(now()));
        }

    }



    /**
     * @inheritDoc
     */
    public function purifier()
    {
        return [
             'published_at' => 'date',
        ];
    }

}
