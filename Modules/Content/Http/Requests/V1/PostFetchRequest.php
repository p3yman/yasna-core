<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentPost;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PostFetchRequest
 *
 * @property ContentPost $model
 */
class PostFetchRequest extends YasnaFormRequest
{
    protected $model_name               = "content-post";
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'type',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveModelInCreateMode();
    }



    /**
     * resolve model in create mode
     *
     * @return void
     */
    private function resolveModelInCreateMode()
    {
        if ($this->editMode()) {
            return;
        }

        $type = content()->types()->findByHashid((string)$this->getData("type"));

        $this->model->type_id    = $type->id;
        $this->model->sisterhood = ContentPost::generateSisterhood();
        // Invalid types are automatically handled in the authorize method.
    }
}
