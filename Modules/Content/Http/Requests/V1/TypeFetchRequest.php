<?php

namespace Modules\Content\Http\Requests\V1;

use App\Models\ContentType;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


/**
 * Class TypeSaveRequest
 *
 * @property ContentType $model
 */
class TypeFetchRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "content-type";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;
}
