<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/17/18
 * Time: 10:34 AM
 */

namespace Modules\EhdaEcg\Services\Controller;

use Illuminate\Routing\Controller;
use Modules\EhdaEcg\Services\Module\CurrentModuleHelper;
use Modules\EhdaEcg\Services\Module\ModuleHandler;

abstract class ModuleControllerAbstract extends Controller
{
    protected $module;



    public function __construct()
    {
        CurrentModuleHelper::assign($this->getModule());
    }



    /**
     * @return string
     */
    public function getModuleAlias()
    {
        return $this->getModule()->getAlias();
    }



    /**
     * @return ModuleHandler
     */
    public function getModule()
    {
        return ($this->module) ?: ($this->module = ModuleHandler::create(get_class($this)));
    }



    /**
     * Get the evaluated view contents for the given view in this current module.
     *
     * @param  string $view
     * @param  array  $data
     * @param  array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function template(...$parameters)
    {
        return $this->safeTemplate(...$parameters);
    }



    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string $view
     * @param  array  $data
     * @param  array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function safeTemplate(...$parameters)
    {
        if (is_string($parameters[0]) and !str_contains($parameters[0], '::')) {
            $parameters[0] = $this->getModuleAlias() . '::' . $parameters[0];
        }

        return view(...$parameters);
    }
}
