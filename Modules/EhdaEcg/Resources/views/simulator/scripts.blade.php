@section('end-of-body')
	@if($dev)
		{{ Html::script(CurrentModule::asset('js/simulator-constants-dev.min.js')) }}
	@else
		{{ Html::script(CurrentModule::asset('js/simulator-constants.min.js')) }}
	@endif
	{{ Html::script(CurrentModule::asset('js/simulator.min.js')) }}
@append