<div class="tab-content">
    @include(CurrentModule::bladePath('simulator.management-panel-page-treatments-tab-fluid'))
    @include(CurrentModule::bladePath('simulator.management-panel-page-treatments-tab-vasopressor'))
    @include(CurrentModule::bladePath('simulator.management-panel-page-treatments-tab-ventilator'))
    @include(CurrentModule::bladePath('simulator.management-panel-page-treatments-tab-temperature'))
    @include(CurrentModule::bladePath('simulator.management-panel-page-treatments-tab-medications'))
</div>