@extends(CurrentModule::bladePath('layouts.plane'))

@php $dev = $dev ?? false @endphp

@section('head')
    <title>ECG Simulator</title>
    {{ Html::style(CurrentModule::asset('css/simulator.min.css')) }}
@append

@section('body')
    <div class="container-main">
        <div class="container-inner monitor">
            <div class="col-lg-2 col-md-3 col-xs-12 monitor-column-1">
                @include(CurrentModule::bladePath('simulator.vital-singns'))
            </div>
            <div class="col-lg-10 col-md-9 col-xs-12 monitor-column-2">
                @include(CurrentModule::bladePath('simulator.ecg-preview'))
                @include(CurrentModule::bladePath('simulator.shocker-box'))
                @include(CurrentModule::bladePath('simulator.management-panel'))
                @include(CurrentModule::bladePath('simulator.second-preview'))
            </div>
        </div>
    </div>

    @include(CurrentModule::bladePath('simulator.loading-window-cover'))
@append

@section('end-of-body')
    @include(CurrentModule::bladePath('simulator.scripts'))
@append