<div class="monitor-case-management-panel">
    <div class="monitor-case-management-panel-inner">
        @include(CurrentModule::bladePath('simulator.management-panel-page-start'))

        @include(CurrentModule::bladePath('simulator.management-panel-page-question'))

        @include(CurrentModule::bladePath('simulator.management-panel-page-more-info'))

        @include(CurrentModule::bladePath('simulator.management-panel-page-exams'))

        @include(CurrentModule::bladePath('simulator.management-panel-page-treatments'))
    </div>
</div>