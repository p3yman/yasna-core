@php
	$files_url_tmp = CurrentModule::action('FileController@getFile', [
		'folder_name' => '_',
		'file_name' => '_'
	]);
	$files_url = str_replace('/_/_', '', $files_url_tmp);
@endphp
<script>
    let MODULE_URL = '{{ CurrentModule::getCurrentModule()->getPath() }}';
    let FILES_URL  = '{{ $files_url }}';
    let SOUNDS_URL = '{{ CurrentModule::asset('sound/') }}';
</script>

{{ Html::script(CurrentModule::asset('lib/jquery-3.3.1/jquery-3.3.1.min.js')) }}
{{ Html::script(CurrentModule::asset('lib/bootstrap/js/bootstrap.min.js')) }}
{{ Html::script(CurrentModule::asset('lib/low-lag/low-lag.min.js')) }}
{{ Html::script(CurrentModule::asset('lib/low-lag/soundmanager2.min.js')) }}
{{ Html::script(CurrentModule::asset('js/tools.min.js')) }}