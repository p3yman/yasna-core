<!DOCTYPE html>
<html lang="{{ getLocale() }}">
@include(CurrentModule::bladePath('layouts.head'))
<body>

@yield('body')
@include(CurrentModule::bladePath('layouts.scripts'))
@yield('end-of-body')
</body>
</html>