<?php

namespace Modules\EhdaEcg\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\EhdaEcg\Services\Controller\EhdaEcgControllerAbstract;

class EhdaEcgController extends EhdaEcgControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function copy()
    {
        return $this->template('copy.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function simulator()
    {
        return $this->template('simulator.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function simulatorDev()
    {
        return $this->template('simulator.main', ['dev' => true]);
    }
}
