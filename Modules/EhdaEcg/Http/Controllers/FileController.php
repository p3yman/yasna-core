<?php

namespace Modules\EhdaEcg\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as FilesFacades;
use Modules\EhdaEcg\Services\Controller\EhdaEcgControllerAbstract;
use Symfony\Component\HttpFoundation\File\File;

class FileController extends EhdaEcgControllerAbstract
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getFile(Request $request)
    {
        $file_pathname = $this->findFilePathname($request);
        if (FilesFacades::exists($file_pathname)) {
            $file = new File($file_pathname);
            return response(file_get_contents($file_pathname), 200, [
                 'Content-Type' => $file->getMimeType(),
            ]);
        }

        return $this->abort(404);
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    protected function findFilePathname(Request $request)
    {
        return implode(DIRECTORY_SEPARATOR, [
             $this->getModule()->getPath(),
             'Resources',
             'files',
             $request->folder_name,
             $request->file_name,
        ]);
    }
}
