<?php

Route::group(['middleware' => 'web', 'prefix' => 'ehdaecg', 'namespace' => 'Modules\EhdaEcg\Http\Controllers'],
     function () {
         Route::get('/', 'EhdaEcgController@index');
     });


Route::group(['prefix' => 'ecg', 'middleware' => 'web', 'namespace' => 'Modules\EhdaEcg\Http\Controllers'],
     function () {
         // Implementation of `skillstat`
         Route::get('copy', 'EhdaEcgController@copy')->middleware(['auth', 'is:developer']);

         Route::get('simulator', 'EhdaEcgController@simulator');
         Route::get('simulator/dev', 'EhdaEcgController@simulatorDev')->middleware(['auth', 'is:developer']);


         Route::get('file/{folder_name}/{file_name}', 'FileController@getFile');
     }
);

Route::group(['prefix' => 'china', 'middleware' => 'web', 'namespace' => 'Modules\EhdaEcg\Http\Controllers'],
    function () {
        Route::get('ecg', 'EhdaEcgController@simulator');
    }
);
