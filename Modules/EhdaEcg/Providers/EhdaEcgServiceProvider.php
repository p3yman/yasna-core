<?php

namespace Modules\EhdaEcg\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\EhdaEcg\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Services\YasnaProvider;

class EhdaEcgServiceProvider extends YasnaProvider
{

    /**
     * Add all dependencies for this module
     *
     * @return void
     */
    public function index()
    {
        $this->addAllAliases();
    }



    /**
     * @return $this
     */
    protected function addAllAliases()
    {
        $aliases = ['CurrentModule' => CurrentModuleHelper::class];
        foreach ($aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }

        return $this;
    }
}
