<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/7/18
 * Time: 11:15 AM
 */

namespace Modules\Remark\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class EntityModelScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder $builder
     * @param  Model   $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $class        = get_class($model);
        $entity_model = $class::$entity_model;

        $builder->where('entity_model', $entity_model);
    }
}
