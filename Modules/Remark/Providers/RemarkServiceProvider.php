<?php

namespace Modules\Remark\Providers;

use Modules\Remark\Events\RemarkChangedFlag;
use Modules\Remark\Listeners\MakeChangeLogWhenRemarkFlagChanged;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class RemarkServiceProvider
 *
 * @package Modules\Remark\Providers
 */
class RemarkServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEventListeners();
        $this->registerModelTraits();
    }



    /**
     * Register event listeners.
     */
    protected function registerEventListeners()
    {
        $this->listen(RemarkChangedFlag::class, MakeChangeLogWhenRemarkFlagChanged::class);
    }



    /**
     * Registers model traits.
     */
    protected function registerModelTraits()
    {
        $this->addModelTrait('UserRemarkTrait', 'User');
    }
}
