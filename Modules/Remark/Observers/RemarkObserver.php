<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 10:30 AM
 */

namespace Modules\Remark\Observers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Remark\Entities\Remark;
use Modules\Remark\Events\RemarkAdded;
use Modules\Remark\Events\RemarkChangedFlag;
use Modules\Remark\Exceptions\EntityModelInvalidException;
use Modules\Remark\Exceptions\EntityModelMissingException;
use Modules\Remark\Exceptions\EntityModelNotFoundException;
use Modules\Remark\Exceptions\EntityNotFoundException;
use Modules\Remark\Exceptions\ImpossibleFlagException;

class RemarkObserver
{
    /**
     * Get the linking entity.
     *
     * @param Remark $model
     *
     * @return Model
     * @throws EntityNotFoundException
     */
    protected function entity(Remark $model)
    {
        $entity = $model->getFoundEntity();

        if ($entity) {
            return $entity;
        } else {
            throw new EntityNotFoundException;
        }
    }



    /**
     * Set the default flag if needed.
     *
     * @param Remark $model
     *
     * @throws EntityNotFoundException
     * @return void
     */
    protected function handleDefaultFlag(Remark $model)
    {
        if ($model->flag) {
            return;
        }

        $entity  = $this->entity($model);
        $guessed = $entity->guessDefaultRemarkFlag();

        if ($guessed) {
            $model->flag = $guessed;
        }
    }



    /**
     * Set the default lang if needed.
     *
     * @param Remark $model
     *
     * @throws EntityNotFoundException
     * @return void
     */
    protected function handleDefaultLang(Remark $model)
    {
        if ($model->lang) {
            return;
        }

        $entity  = $this->entity($model);
        $guessed = $entity->guessDefaultRemarkLang();

        if ($guessed) {
            $model->lang = $guessed;
        }
    }



    /**
     * Set the default type.
     *
     * @param Remark $model
     *
     * @throws EntityNotFoundException
     * @return void
     */
    protected function handleDefaultType(Remark $model)
    {
        if ($model->type) {
            return;
        }

        $entity  = $this->entity($model);
        $guessed = $entity->guessDefaultRemarkType();

        if ($guessed) {
            $model->type = $guessed;
        }
    }



    /**
     * Fill the `original_flag` and `flagged_at` fields while creating new remark.
     *
     * @param Remark $model
     *
     * @return void
     */
    protected function handleNewFlagInfoWhileCreating(Remark $model)
    {
        if ($model->flag) {
            $model->original_flag = $model->flag;
            $model->flagged_at    = Carbon::now()->toDateTimeString();
        }
    }



    /**
     * Do automatic tasks before creating a remark.
     *
     * @param Remark $model
     *
     * @throws EntityModelMissingException
     * @throws EntityModelNotFoundException
     * @throws EntityModelInvalidException
     * @throws EntityNotFoundException
     * @return void
     */
    public function creating(Remark $model)
    {
        if ($model->user_id == 0) {
            $model->user_id = user()->id;
        }

        $this->handleDefaultFlag($model);
        $this->handleDefaultLang($model);
        $this->handleDefaultType($model);
        $this->handleNewFlagInfoWhileCreating($model);
    }



    /**
     * Do automatic tasks after creating a remark.
     *
     * @param Remark $model
     */
    public function created(Remark $model)
    {
        if ($model->isDefaultType()) {
            event(new RemarkAdded($model));
        }
    }



    /**
     * Check entity model and throw exception if needed.
     *
     * @param string|null $entity_model
     *
     * @throws EntityModelMissingException
     * @throws EntityModelNotFoundException
     * @throws EntityModelInvalidException
     * @return void
     */
    protected function checkEntityModel($entity_model)
    {
        // Check if the entity model ha been filled
        if (!$entity_model) {
            throw new EntityModelMissingException;
        }

        // Check the existence of the model class.
        try {
            new $entity_model();
        } catch (\Throwable $ex) {
            throw new EntityModelNotFoundException($ex);
        }

        // Check if the class is in the models folder.
        if (!starts_with($entity_model, MODELS_NAMESPACE)) {
            throw new EntityModelInvalidException($entity_model);
        }
    }



    /**
     * Check things about the entity model and entity id.
     *
     * @param Remark $model
     *
     * @throws EntityModelInvalidException
     * @throws EntityModelMissingException
     * @throws EntityModelNotFoundException
     * @throws EntityNotFoundException
     * @return  void
     */
    protected function checkEntityThings(Remark $model)
    {
        $this->checkEntityModel($model->entity_model);
        $this->entity($model);
    }



    /**
     * Return possible flags depending on the entity.
     *
     * @param Remark $model
     *
     * @return array
     * @throws EntityNotFoundException
     */
    protected function getPossibleFlags(Remark $model)
    {
        $entity = $this->entity($model);

        return $entity->guessPossibleRemarkFlags();
    }



    /**
     * Check if the flag can be saved or not.
     *
     * @param Remark $model
     *
     * @throws EntityNotFoundException
     * @throws ImpossibleFlagException
     */
    protected function checkGivenFlag(Remark $model)
    {
        $flag = $model->flag;
        if (!$flag) {
            return;
        }

        $entity         = $this->entity($model);
        $possible_flags = $entity->guessPossibleRemarkFlags();

        if (!in_array($flag, $possible_flags)) {
            throw new ImpossibleFlagException($flag);
        }
    }



    /**
     * Do automatic tasks before saving a remark.
     *
     * @param Remark $model
     *
     * @throws EntityModelMissingException
     * @throws EntityModelNotFoundException
     * @throws EntityModelInvalidException
     * @throws EntityNotFoundException
     */
    public function saving(Remark $model)
    {
        $this->checkEntityThings($model);
        $this->checkGivenFlag($model);
    }



    /**
     * Do automatic tasks before changing the flag a remark.
     *
     * @param Remark $model
     *
     * @return void
     */
    public function flagChanged(Remark $model)
    {
        event(new RemarkChangedFlag($model));
    }
}
