<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/6/18
 * Time: 1:48 PM
 */

namespace Modules\Remark\Services\Traits;


use Modules\Remark\Scopes\EntityModelScope;

trait RemarkAliasModelTrait
{
    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable()
    {
        if ($this->module()->isNotInitialized()) {
            return parent::getTable();
        } else {
            return model('remark')->getTable();
        }
    }



    /**
     * The "booting" method of the model.
     * <br>
     * With the `Modules\Tickets\Scopes\EntityModelScope` scope,
     * only remarks attached to ticket types will be selected.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        if (property_exists(static::class, 'entity_model')) {
            static::addGlobalScope(new EntityModelScope);
        }
    }
}
