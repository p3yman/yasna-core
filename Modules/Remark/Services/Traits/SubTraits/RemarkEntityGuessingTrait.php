<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 4:01 PM
 */

namespace Modules\Remark\Services\Traits\SubTraits;

trait RemarkEntityGuessingTrait
{
    /**
     * Guess the default flag for remarks of this model.
     *
     * @return string|null
     */
    public function guessDefaultRemarkFlag()
    {
        // Return the default remark flag if the default flag method specified in the hosting model
        $default_flag_method = 'defaultRemarkFlag';
        if (method_exists($this, $default_flag_method)) {
            return $this->$default_flag_method();
        }

        // Return the default remark flag if the default flag property specified in the hosting model
        $default_flag_property = 'default_remark_flag';
        if (property_exists($this, $default_flag_property)) {
            return $this->$default_flag_property;
        }

        return null;
    }



    /**
     * Guess the default lang for remarks of this model.
     *
     * @return string|null
     */
    public function guessDefaultRemarkLang()
    {
        // Return the default remark lang if the default lang method specified in the hosting model
        $default_lang_method = 'defaultRemarkLang';
        if (method_exists($this, $default_lang_method)) {
            return $this->$default_lang_method();
        }

        // Return the default remark lang if the default lang property specified in the hosting model
        $default_lang_property = 'default_remark_lang';
        if (property_exists($this, $default_lang_property)) {
            return $this->$default_lang_property;
        }

        return null;
    }



    /**
     * Guess the default type for remarks of this model.
     *
     * @return string|null
     */
    public function guessDefaultRemarkType()
    {
        // Return the default remark type if the default type method specified in the hosting model
        $default_type_method = 'defaultRemarkType';
        if (method_exists($this, $default_type_method)) {
            return $this->$default_type_method();
        }

        // Return the default remark type if the default type property specified in the hosting model
        $default_type_property = 'default_remark_type';
        if (property_exists($this, $default_type_property)) {
            return $this->$default_type_property;
        }

        // Return the default remark type of all remarks.
        return 'default';
    }



    /**
     * Guess all the possible flags for remarks of this model.
     *
     * @return array|null
     */
    public function guessPossibleRemarkFlags()
    {
        // Return the default the possible flags if the flags method specified in the hosting model
        $flags_method = 'remarkPossibleFlags';
        if (method_exists($this, $flags_method)) {
            return $this->$flags_method();
        }

        // Return the default the possible flags if the flags property specified in the hosting model
        $flags_property = 'remark_possible_flags';
        if (property_exists($this, $flags_property)) {
            return $this->$flags_property;
        }

        return [];
    }



    /**
     * Guess all the model which is pointing to the remarks items.
     * <br>
     * By default all remarks are accessible as instances of the `Remark` model.
     *
     * @return string
     */
    public function guessRemarkModel()
    {
        // Return the alias model's name if the related model has been specified.
        $alias_model_method = 'remarkAliasModel';
        if (method_exists($this, $alias_model_method)) {
            return $this->$alias_model_method();
        }

        // Return the alias model's name if the related property has been specified.
        $alias_model_property = 'remark_alias_model';
        if (property_exists($this, $alias_model_property)) {
            return $this->$alias_model_property;
        }

        return 'remark';
    }
}
