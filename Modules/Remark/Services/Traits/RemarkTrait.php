<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 12:38 PM
 */

namespace Modules\Remark\Services\Traits;

use App\Models\Remark;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Remark\Services\Traits\SubTraits\RemarkEntityGuessingTrait;
use Modules\Yasna\Services\YasnaModel;

trait RemarkTrait
{
    use RemarkEntityGuessingTrait;



    /**
     * Return the full name space of the pointing model.
     *
     * @return string
     */
    protected function getRemarkModelClass()
    {
        return get_class(model($this->guessRemarkModel()));
    }



    /**
     * Attach a remark to this model.
     *
     * @param array $remark_data The data to be stored as the new remark.
     *
     * @return Remark|YasnaModel
     */
    public function addRemark($remark_data = [])
    {
        $remark_data['entity_id']    = $this->id;
        $remark_data['entity_model'] = get_class($this);

        return model($this->guessRemarkModel())->batchSave($remark_data);
    }



    /**
     * Laravel one-to-many relationship method with conditions for flag and lang if specified.
     *
     * @param string|null $flag
     * @param string|null $lang
     *
     * @return HasMany
     */
    public function remarks($flag = null, $lang = null, $type = null)
    {
        // Class of the current model.
        $class = get_class($this);

        // Generate the main builder
        $builder = $this->hasMany($this->getRemarkModelClass(), 'entity_id')
                        ->where('entity_model', $class)
        ;

        // Apply flag condition if needed
        if ($flag) {
            $builder->where('flag', $flag);
        }


        // Apply lang condition if needed
        if ($lang) {
            $builder->where('lang', $lang);
        }


        // Apply type condition if needed
        if ($type) {
            $builder->where('type', $type);
        }

        // Return the final builder
        return $builder;
    }



    /**
     * Check if this model has any remark with the given flag and lang if specified.
     *
     * @param string|null $flag
     * @param string|null $lang
     *
     * @return bool
     */
    public function hasRemarks($flag = null, $lang = null, $type = null)
    {
        return boolval($this->remarks($flag, $lang, $type)->count());
    }



    /**
     * Laravel one-to-many relationship method to find remarks in the given lang.
     *
     * @param string $lang
     *
     * @return HasMany
     */
    public function remarksIn(string $lang)
    {
        return $this->remarks(null, $lang);
    }



    /**
     * Check if this model has any remark in the given lang.
     *
     * @param string $lang
     *
     * @return HasMany
     */
    public function hasRemarksIn(string $lang)
    {
        return boolval($this->remarksIn($lang)->count());
    }



    /**
     * Laravel one-to-many relationship method to find remarks as the given flag.
     *
     * @param string $flag
     *
     * @return HasMany
     */
    public function remarksAs(string $flag)
    {
        return $this->remarks($flag);
    }



    /**
     * Check if this model has any remark as the given flag.
     *
     * @param string $flag
     *
     * @return bool
     */
    public function hasRemarksAs(string $flag)
    {
        return boolval($this->remarksAs($flag)->count());
    }



    /**
     * Laravel one-to-many relationship method to find remarks with the given type.
     *
     * @param string $type
     *
     * @return HasMany
     */
    public function remarksWithType(string $type)
    {
        return $this->remarks(null, null, $type);
    }



    /**
     * Check if this model has any remark with the given type.
     *
     * @param string $type
     *
     * @return bool
     */
    public function hasRemarksWithType(string $type)
    {
        return boolval($this->remarksWithType($type)->count());
    }



    /**
     * Laravel one-to-many relationship method to find remarks with the default type.
     * <br>
     * The default type could be defined by the hosting model or `default` will be used.
     *
     * @param string $type
     *
     * @return HasMany
     */
    public function remarksWithDefaultType()
    {
        $default_type = $this->guessDefaultRemarkType();

        return $this->remarksWithType($default_type);
    }



    /**
     * Check if this model has any remark with the default type.
     * <br>
     * The default type could be defined by the hosting model or `default` will be used.
     *
     * @param string $type
     *
     * @return bool
     */
    public function hasRemarksWithDefaultType()
    {
        return boolval($this->remarksWithDefaultType()->count());
    }
}
