<?php 

namespace Modules\Remark\Events;

use Modules\Yasna\Services\YasnaEvent;

class RemarkAdded extends YasnaEvent
{
    public $model_name = 'remark';
}
