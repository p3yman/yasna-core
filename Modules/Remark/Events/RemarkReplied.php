<?php

namespace Modules\Remark\Events;

use Modules\Remark\Entities\Remark;
use Modules\Yasna\Services\YasnaEvent;

class RemarkReplied extends YasnaEvent
{
    public $model_name = 'remark';

    /**
     * @var Remark $reply The model of the flag-changing remark.
     */
    public $reply;



    /**
     * RemarkChangedFlag constructor.
     *
     * @param Remark $model
     * @param Remark $reply
     */
    public function __construct(Remark $model, Remark $reply)
    {
        $this->reply = $reply;

        parent::__construct($model);
    }
}
