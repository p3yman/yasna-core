<?php

namespace Modules\Remark\Events;

use Modules\Yasna\Services\YasnaEvent;

class RemarkChangedFlag extends YasnaEvent
{
    public $model_name = 'remark';
}
