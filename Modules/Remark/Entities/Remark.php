<?php

namespace Modules\Remark\Entities;

use Modules\Label\Entities\Traits\LabelsTrait;
use Modules\Remark\Entities\Traits\RemarkElectorsTrait;
use Modules\Remark\Entities\Traits\RemarkEntityRelationTrait;
use Modules\Remark\Entities\Traits\RemarkFilesTrait;
use Modules\Remark\Entities\Traits\RemarkInformationTrait;
use Modules\Remark\Entities\Traits\RemarkMapTrait;
use Modules\Remark\Entities\Traits\RemarkModificationTrait;
use Modules\Remark\Entities\Traits\RemarkNestingTrait;
use Modules\Remark\Entities\Traits\RemarkObserverTrait;
use Modules\Remark\Entities\Traits\RemarkScopesTrait;
use Modules\Remark\Entities\Traits\RemarkUserTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Remark extends YasnaModel
{
    use SoftDeletes;
    use RemarkModificationTrait;
    use RemarkNestingTrait;
    use RemarkEntityRelationTrait;
    use RemarkInformationTrait;
    use RemarkScopesTrait;
    use RemarkElectorsTrait;
    use RemarkObserverTrait;
    use RemarkFilesTrait;
    use RemarkMapTrait;
    use RemarkUserTrait;
    use LabelsTrait;


    /**
     * @var array List of guarded fields.
     */
    protected $guarded = ["id"];

    /**
     * @var array List of additive observers.
     */
    protected $observables = ['flagChanging', 'flagChanged'];
}
