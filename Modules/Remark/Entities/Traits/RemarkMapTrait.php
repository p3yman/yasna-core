<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/15/18
 * Time: 3:17 PM
 */

namespace Modules\Remark\Entities\Traits;


trait RemarkMapTrait
{
    /**
     * Returns and array version of ticket to in used for API.
     *
     * @return array
     */
    public function getApiArray()
    {
        $owner  = $this->owner;
        $parent = $this->parent ?? static::instance();
        $master = $this->master ?? static::instance();

        return [
             'id'          => $this->hashid,
             'title'       => $this->title,
             'text'        => $this->text,
             'flag'        => $this->flag,
             'lang'        => $this->lang,
             'type'        => $this->type,
             'owner_id'    => ($owner->exists) ? $owner->hashid : '',
             'owner_name'  => ($owner->exists) ? $owner->full_name : '',
             'parent_id'   => $parent->exists ? $parent->hashid : '',
             'master_id'   => $master->exists ? $master->hashid : '',
             'attachments' => array_map(function ($hashid) {
                 return fileManager()->file($hashid)->getUrl();
             }, $this->attachments),
             'created_at'  => $this->created_at->toIso8601String(),
             'updated_at'  => $this->updated_at->toIso8601String(),
        ];
    }



    /**
     * Accessor for the `getApiArray()` Method
     *
     * @return array
     */
    public function getApiArrayAttribute()
    {
        return $this->getApiArray();
    }
}
