<?php 

namespace Modules\Remark\Entities\Traits;

use App\Models\Remark;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserRemarkTrait
{
    /**
     * Standard One-to-Many Relationship between users and remarks.
     *
     * @return HasMany
     */
    public function remarks()
    {
        return $this->hasMany(Remark::class);
    }
}
