<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/5/18
 * Time: 10:17 AM
 */

namespace Modules\Remark\Entities\Traits;

use Carbon\Carbon;
use Modules\Remark\Entities\Remark;
use Modules\Remark\Events\RemarkChangedFlag;
use Modules\Remark\Exceptions\ImpossibleFlagException;

trait RemarkModificationTrait
{
    /**
     * Change the flag to the given flag.
     *
     * @param string $flag
     *
     * @return Remark
     * @throws ImpossibleFlagException
     */
    public function markAs(string $flag)
    {
        if ($flag == $this->flag) {
            return $this;
        }

        $saved = $this->batchSave([
             'flag'       => $flag,
             'flagged_at' => Carbon::now()->toDateTimeString(),
        ]);

        if ($saved->exists) {
            $this->refresh();
            $this->fireModelEvent('flagChanged');
        }

        return $this;
    }



    /**
     * Change the value of the given field to the given value.
     *
     * @param string $field
     * @param string $value
     *
     * @return \App\Models\Remark
     */
    public function changeField(string $field, string $value)
    {
        return $this->batchSave([
             $field => $value,
        ]);
    }



    /**
     * Change the `title`.
     *
     * @param string $title
     *
     * @return \App\Models\Remark
     */
    public function changeTitle(string $title)
    {
        return $this->changeField('title', $title);
    }



    /**
     * Change the `text`.
     *
     * @param string $text
     *
     * @return \App\Models\Remark
     */
    public function changeText(string $text)
    {
        return $this->changeField('text', $text);
    }



    /**
     * Change the `guest_name_first`.
     *
     * @param string $name
     *
     * @return \App\Models\Remark
     */
    public function changeGuestFirstName(string $name)
    {
        return $this->changeField('guest_name_first', $name);
    }



    /**
     * Change the `guest_name_first`.
     *
     * @param string $name
     *
     * @return \App\Models\Remark
     */
    public function changeGuestNameFirst(string $name)
    {
        return $this->changeGuestFirstName($name);
    }



    /**
     * Change the `guest_name_last`.
     *
     * @param string $name
     *
     * @return \App\Models\Remark
     */
    public function changeGuestLastName(string $name)
    {
        return $this->changeField('guest_name_last', $name);
    }



    /**
     * Change the `guest_name_last`.
     *
     * @param string $name
     *
     * @return \App\Models\Remark
     */
    public function changeGuestNameLast(string $name)
    {
        return $this->changeGuestLastName($name);
    }



    /**
     * Change the `guest_email`.
     *
     * @param string $email
     *
     * @return \App\Models\Remark
     */
    public function changeGuestEmail(string $email)
    {
        return $this->changeField('guest_email', $email);
    }



    /**
     * Change the `guest_mobile`.
     *
     * @param string $mobile
     *
     * @return \App\Models\Remark
     */
    public function changeGuestMobile(string $mobile)
    {
        return $this->changeField('guest_mobile', $mobile);
    }



    /**
     * Change the `guest_web`.
     *
     * @param string $web
     *
     * @return \App\Models\Remark
     */
    public function changeGuestWeb(string $web)
    {
        return $this->changeField('guest_web', $web);
    }



    /**
     * Change the `guest_ip`.
     *
     * @param string $ip
     *
     * @return \App\Models\Remark
     */
    public function changeGuestIp(string $ip)
    {
        return $this->changeField('guest_ip', $ip);
    }
}
