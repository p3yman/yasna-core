<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/14/18
 * Time: 11:54 AM
 */

namespace Modules\Remark\Entities\Traits;


trait RemarkFilesTrait
{
    /**
     * Returns an array of meta fields for files.
     *
     * @return array
     */
    protected function fileMetaFields()
    {
        return ['attachments'];
    }



    /**
     * Returns attachments.
     *
     * @return array
     */
    protected function getAttachments()
    {
        return ($this->getMeta('attachments') ?? []);
    }



    /**
     * Accessor for the `getAttachments()` Method.
     *
     * @return array
     */
    protected function getAttachmentsAttribute()
    {
        return $this->getAttachments();
    }
}
