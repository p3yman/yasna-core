<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/6/18
 * Time: 5:20 PM
 */

namespace Modules\Remark\Entities\Traits;


use Modules\Yasna\Services\YasnaModel;

trait RemarkElectorsTrait
{
    /**
     * Entity elector for remarks.
     *
     * @param string $entities
     */
    public function electorEntity($entities)
    {
        if (!is_array($entities)) {
            $entities = [$entities];
        }

        $this->elector()->where(function ($query) use ($entities) {
            foreach ($entities as $entity) {
                $entity_model = get_class($entity);
                $entity_id    = $entity->id;

                $query->orWhere(compact('entity_model', 'entity_id'));
            }
        })
        ;
    }



    /**
     * Flag elector for remarks.
     *
     * @param string $tags
     */
    public function electorFlag($tags)
    {
        if (!is_array($tags)) {
            $tags = [$tags];
        }

        $this->elector()->whereIn('flag', $tags);
    }



    /**
     * Type elector for remarks.
     *
     * @param string $types
     */
    public function electorType($types)
    {
        if (!is_array($types)) {
            $types = [$types];
        }

        $this->elector()->whereIn('type', $types);
    }



    /**
     * Language elector for remarks.
     *
     * @param string $languages
     */
    public function electorLang($languages)
    {
        if (!is_array($languages)) {
            $languages = [$languages];
        }

        $this->elector()->whereIn('lang', $languages);
    }
}
