<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/16/18
 * Time: 1:23 PM
 */

namespace Modules\Remark\Entities\Traits;


use App\Models\User;

trait RemarkUserTrait
{
    /**
     * Returns the owner of the remark.
     *
     * @return \Modules\Yasna\Services\YasnaModel
     */
    public function getOwner()
    {
        return ($this->user ?: model('user'));
    }



    /**
     * Accessor for the `getOwner()` Method
     *
     * @return \Modules\Yasna\Services\YasnaModel
     */
    public function getOwnerAttribute()
    {
        return $this->getOwner();
    }



    /**
     * Checks if the remark is owned by the specified user.
     *
     * @param User $user
     *
     * @return bool
     */
    public function ownedBy(User $user)
    {
        if ($user->not_exists) {
            return false;
        }

        return ($this->user_id == $user->id);
    }



    /**
     * Checks if the remark is owned by the logged in user.
     *
     * @return bool
     */
    public function ownedByClient()
    {
        return $this->ownedBy(user());
    }
}
