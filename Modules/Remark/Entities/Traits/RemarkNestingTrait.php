<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/5/18
 * Time: 10:19 AM
 */

namespace Modules\Remark\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Remark\Entities\Remark;
use Modules\Remark\Events\RemarkReplied;

trait RemarkNestingTrait
{
    /**
     * Retrieve the reference and change the specified column with its id.
     *
     * @param \App\Models\Remark|string|int $reference
     * @param string                        $column
     *
     * @return \App\Models\Remark
     */
    protected function changeSelfReferenceFields($reference, string $column)
    {
        $class = get_class($this);

        if (!($reference instanceof $class)) {
            $reference = $this->grab($reference);
        }

        return $this->changeField($column, $reference->id);
    }



    /**
     * Change the `parent_id` field.
     *
     * @param \App\Models\Remark|string|int $parent
     *
     * @return \App\Models\Remark
     */
    public function changeParent($parent)
    {
        return $this->changeSelfReferenceFields($parent, 'parent_id');
    }



    /**
     * Change the `parent_id` field.
     *
     * @param \App\Models\Remark|string|int $master
     *
     * @return \App\Models\Remark
     */
    public function changeMaster($master)
    {
        return $this->changeSelfReferenceFields($master, 'master_id');
    }



    /**
     * Return query builder to be used while selecting the parent remark.
     *
     * @return Builder
     */
    public function parent()
    {
        return $this->where('id', $this->parent_id);
    }



    /**
     * Return the parent remark.
     *
     * @return Builder|Model|null
     */
    public function getParentAttribute()
    {
        return $this->parent()->first();
    }



    /**
     * Return query builder to be used while selecting the master remark.
     *
     * @return Builder
     */
    public function master()
    {
        return $this->where('id', $this->master_id);
    }



    /**
     * Return the master remark.
     *
     * @return Builder|Model|null
     */
    public function getMasterAttribute()
    {
        return $this->master()->first();
    }



    /**
     * Return query builder to be used while selecting the replies.
     *
     * @return Builder
     */
    public function replies()
    {
        return $this->where('parent_id', $this->id);
    }



    /**
     * Return a collection of the replies.
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getRepliesAttribute()
    {
        return $this->replies()->get();
    }



    /**
     * Return query builder to be used while selecting all the remarks depending on their `master_id` columns.
     *
     * @return Builder
     */
    public function deepReplies()
    {
        return $this->where('master_id', $this->id);
    }



    /**
     * Return a collection of the replies depending on their `master_id` columns.
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getDeepRepliesAttribute()
    {
        return $this->deepReplies()->get();
    }



    /**
     * Add a reply to this remark.
     *
     * @param array $data
     *
     * @return Remark
     */
    public function addReply(array $data = [])
    {
        $data['parent_id']    = $this->id;
        $data['master_id']    = ($this->master_id ?: $this->parent_id ?: $this->id);
        $data['entity_model'] = $this->entity_model;
        $data['entity_id']    = $this->entity_id;

        $reply = (new static)->batchSave($data);

        if ($reply->exists and $reply->isDefaultType()) {
            event(new RemarkReplied($this, $reply));
        }

        return $reply;
    }
}
