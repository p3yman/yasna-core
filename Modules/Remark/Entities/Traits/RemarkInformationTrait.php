<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/7/18
 * Time: 1:30 PM
 */

namespace Modules\Remark\Entities\Traits;


use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait RemarkInformationTrait
{
    /**
     * The standard laravel one-to-many relationship.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }



    /**
     * Get the name of the sender to be shown.
     *
     * @return string
     */
    public function getSenderNameAttribute()
    {
        $user = $this->user;
        if ($user) {
            return $user->full_name;
        } else {
            return trim($this->guest_name_first . ' ' . $this->guest_name_last);
        }
    }



    /**
     * Check if the type of the ticket is `default` or not.
     *
     * @return bool
     */
    public function isDefaultType()
    {
        return ($this->type == 'default');
    }



    /**
     * Accessor for the `isDefaultType()` method.
     *
     * @return bool
     */
    public function getIsDefaultTypeAttribute()
    {
        return $this->isDefaultType();
    }



    /*
     * Check if the type of the ticket is `changing-flag` or not.
     *
     * @return bool
     */
    public function isChangingFlag()
    {
        return ($this->type == 'changing-flag');
    }



    /**
     * Accessor for the `isChangingFlag()` method.
     *
     * @return bool
     */
    public function getIsChangingFlagAttribute()
    {
        return $this->isChangingFlag();
    }
}
