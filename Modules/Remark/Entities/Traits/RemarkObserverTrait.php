<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/7/18
 * Time: 4:23 PM
 */

namespace Modules\Remark\Entities\Traits;


use Modules\Remark\Observers\RemarkObserver;

trait RemarkObserverTrait
{
    /**
     * The "booting" tasks of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::observe(RemarkObserver::class);
    }

}
