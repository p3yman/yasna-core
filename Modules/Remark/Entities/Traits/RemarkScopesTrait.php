<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/5/18
 * Time: 10:57 AM
 */

namespace Modules\Remark\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait RemarkScopesTrait
{
    /**
     * Scope a query to only include remarks with the given lang.
     *
     * @param Builder $query
     * @param string  $lang
     *
     * @return Builder
     */
    public function scopeIn(Builder $query, string $lang)
    {
        return $query->where('lang', $lang);
    }



    /**
     * Scope a query to only include remarks with the given lang.
     *
     * @param Builder $query
     * @param string  $lang
     *
     * @return Builder
     */
    public function scopeInLang(Builder $query, string $lang)
    {
        return $this->scopeIn($query, $lang);
    }



    /**
     * Scope a query to only include remarks as the given flag.
     *
     * @param Builder $query
     * @param string  $flag
     *
     * @return Builder
     */
    public function scopeAs(Builder $query, string $flag)
    {
        return $query->where('flag', $flag);
    }



    /**
     * Scope a query to only include remarks as the given flag.
     *
     * @param Builder $query
     * @param string  $flag
     *
     * @return Builder
     */
    public function scopeWithFlag(Builder $query, string $flag)
    {
        return $this->scopeAs($query, $flag);
    }



    /**
     * Scope a query to only include remarks with the given type.
     *
     * @param Builder $query
     * @param string  $flag
     *
     * @return Builder
     */
    public function scopeWithType(Builder $query, string $type)
    {
        return $query->where('type', $type);
    }



    /**
     * Scopes a query to only include parents remarks.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeParentsOnly(Builder $query)
    {
        return $query->where('parent_id', 0);
    }



    /**
     * Scopes a query to only include parents remarks.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeParents(Builder $query)
    {
        return $this->scopeParentsOnly($query);
    }



    /**
     * Scopes a query to only include master remarks.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeMastersOnly(Builder $query)
    {
        return $query
             ->parentsOnly()
             ->where('master_id', 0)
             ;
    }



    /**
     * Scopes a query to only include master remarks.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeMasters(Builder $query)
    {
        return $this->scopeMastersOnly($query);
    }
}
