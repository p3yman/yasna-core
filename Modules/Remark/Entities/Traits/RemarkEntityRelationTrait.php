<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/5/18
 * Time: 10:21 AM
 */

namespace Modules\Remark\Entities\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait RemarkEntityRelationTrait
{
    /**
     * @var Model|null The entity of this object.
     */
    protected $found_entity;



    /**
     * Laravel one-to-many relationship method.
     *
     * @return BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo($this->entity_model, 'entity_id');
    }



    /**
     * Run a query and retrieve the entity.
     *
     * @return Model
     */
    protected function findEntity()
    {
        $entity_model = $this->entity_model;
        $entity_id    = $this->entity_id;

        try {
            return $entity_model::find($entity_id);
        } catch (\Exception $exception) {
            return null;
        }
    }



    /**
     * Run a query an reset the $entity property with its result.
     *
     * @return void
     */
    public function refreshEntity()
    {
        $this->found_entity = $this->findEntity();
    }



    /**
     * Return the entity model object.
     *
     * @return Model|null
     */
    public function getFoundEntity()
    {
        if (!$this->found_entity) {
            $this->refreshEntity();
        }

        return $this->found_entity;
    }
}
