<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 4:33 PM
 */

namespace Modules\Remark\Exceptions;

use Exception;

class ImpossibleFlagException extends Exception
{
    /**
     * InaccessibleFlagException constructor.
     *
     * @param string $flag
     */
    public function __construct(string $flag)
    {
        $message = "Flag '$flag' can not be saved.";

        parent::__construct($message);
    }
}
