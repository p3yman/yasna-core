<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 1:32 PM
 */

namespace Modules\Remark\Exceptions;

use Exception;

class EntityNotFoundException extends Exception
{
    /**
     * @var string The Customized Message of the Exception.
     */
    protected $message = 'Entity not found.';
}
