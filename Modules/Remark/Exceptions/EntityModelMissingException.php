<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 10:43 AM
 */

namespace Modules\Remark\Exceptions;

use Exception;

class EntityModelMissingException extends Exception
{
    /**
     * @var string Customized message which will be shown in the exception.
     */
    protected $message = "Missing entity model.";
}
