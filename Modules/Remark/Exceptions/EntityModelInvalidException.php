<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 11:35 AM
 */

namespace Modules\Remark\Exceptions;

use Exception;

class EntityModelInvalidException extends Exception
{
    /**
     * EntityModelInvalidException constructor.
     *
     * @param string $class
     */
    public function __construct(string $class)
    {
        $message = "Class '$class' is not a valid model.";

        parent::__construct($message);
    }
}
