<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/4/18
 * Time: 11:00 AM
 */

namespace Modules\Remark\Exceptions;

use Symfony\Component\Debug\Exception\FatalThrowableError;
use Throwable;

class EntityModelNotFoundException extends FatalThrowableError
{
    /**
     * EntityModelNotFoundException constructor.
     *
     * @param Throwable $previous
     */
    public function __construct(Throwable $previous)
    {
        parent::__construct($previous);
    }
}
