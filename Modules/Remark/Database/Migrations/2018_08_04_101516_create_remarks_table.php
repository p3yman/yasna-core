<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remarks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_model')->index();
            $table->unsignedInteger('entity_id')->index();

            $table->unsignedInteger('user_id')->default(0)->index();
            $table->unsignedInteger('parent_id')->index();
            $table->unsignedInteger('master_id')->index()->comment('The id of the first remark in the chain.');


            $table->string('type')->index();

            $table->string('lang')->nullable()->index();

            $table->string('original_flag')->nullable()->index();
            $table->string('flag')->nullable()->index();
            $table->dateTime('flagged_at')->nullable()->index();

            $table->string('guest_name_first')->nullable()->index();
            $table->string('guest_name_last')->nullable()->index();
            $table->string('guest_email')->nullable()->index();
            $table->string('guest_mobile')->nullable()->index();
            $table->string('guest_web')->nullable()->index();
            $table->string('guest_ip')->nullable()->index();

            $table->string('title')->nullable()->index();
            $table->longText('text')->nullable();

            $table->timestamps();

            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remarks');
    }
}
