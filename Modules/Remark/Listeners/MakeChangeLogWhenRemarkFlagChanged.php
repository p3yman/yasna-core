<?php

namespace Modules\Remark\Listeners;

use Modules\Remark\Events\RemarkChangedFlag;

class MakeChangeLogWhenRemarkFlagChanged
{
    /**
     * Handle the event.
     *
     * @param \Modules\Remark\Events\RemarkChangedFlag $event
     *
     * @return void
     */
    public function handle(RemarkChangedFlag $event)
    {
        $copy_keys   = ['lang', 'flag'];
        $model       = $event->model;
        $copy_fields = array_only($model->toArray(), $copy_keys);
        $new_fields  = [
             'type'  => 'changing-flag',
             'title' => 'Flag changed to "' . $model->flag . '"',
        ];
        $fields      = array_merge($copy_fields, $new_fields);


        $model->addReply($fields);
    }
}
