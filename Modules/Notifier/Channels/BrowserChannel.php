<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 12:28 PM
 */

namespace Modules\Notifier\Channels;


use App\Models\Notifier;
use Illuminate\Notifications\Notification;
use Modules\Notifier\Channels\Traits\BrowserOneSignalTrait;
use Modules\Notifier\Exceptions\NotificationFailed;

class BrowserChannel
{
    protected $notifier;



    /**
     * Sends the notification via browser.
     *
     * @param mixed        $notifiable
     * @param Notification $notification
     *
     * @throws NotificationFailed
     */
    public function send($notifiable, Notification $notification)
    {
        $handler = app('notifier.browser');

        if ($handler->isNotActive()) {
            $this->throwErrorIfInvalidDriver();
        }

        $handler->getDefaultDriverInstance()->send($notifiable, $notification);
    }



    /**
     * Throws error if notification fails.
     *
     * @throws NotificationFailed
     */
    protected function throwErrorIfInvalidDriver()
    {
        $diver = $this->getDefaultDriver();

        throw NotificationFailed::submissionError("Invalid Driver ($diver)");
    }



    /**
     * Returns the default browser driver.
     *
     * @return string
     */
    protected function getDefaultDriver()
    {
        return app('notifier.browser')->getDefaultDriver();
    }
}
