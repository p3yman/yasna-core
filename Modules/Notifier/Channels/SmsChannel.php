<?php

namespace Modules\Notifier\Channels;

use App\Models\User;
use Illuminate\Notifications\Notification;
use Modules\Notifier\Exceptions\NotificationFailed;

abstract class SmsChannel
{
    protected $config;



    /**
     * send sms via any of the defined drivers.
     *
     * @param User         $notifiable
     * @param Notification $notification
     */
    public function send($notifiable, $notification)
    {
        if (!notifier()::getDefaultDriverOf('sms')) { // <~~ TODO: Not a very good idea!
            return;
        }

        $this->config = $this->loadConfig();
        $destination  = $this->getDestination($notifiable);
        $message      = $this->getMessage($notifiable, $notification);
        $response     = $this->curl($message, $destination);

        $this->throwErrorIfFailed($response);
    }



    /**
     * throw error if notification fails.
     *
     * @param string $response
     */
    protected function throwErrorIfFailed($response)
    {
        if (is_json($response)) {
            $response_array = json_decode($response, true);
            if (isset($response_array['status'])) {
                throw NotificationFailed::submissionError($response_array['status']);
            }

            return;
        }

        throw NotificationFailed::connectionError();
    }



    /**
     * trigger sms drivers to send the message.
     *
     * @param string $message
     * @param string $destination
     */
    abstract public function curl(string $message, string $destination);



    /**
     * load sms drivers config
     *
     * @return array
     */
    abstract public function loadConfig(): array;



    /**
     * check if the config is enough to send an sms
     *
     * @return bool
     */
    protected function isConfigEnough(): bool
    {
        $config = $this->config;

        foreach ($config as $key => $value) {
            if (!$value) {
                return false;
            }
        }

        return true;
    }



    /**
     * get phone number from the notifier instance.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function getDestination($notifiable): string
    {
        return $notifiable->mobile;
    }



    /**
     * get message from the notification instance.
     *
     * @param User         $notifiable
     * @param Notification $notification
     *
     * @return string
     */
    abstract public function getMessage($notifiable, $notification): string;

}
