<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/27/18
 * Time: 5:47 PM
 */

namespace Modules\Notifier\Services\Database;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class DataProvider implements Arrayable, Jsonable
{
    protected $group;
    protected $title   = null;
    protected $content = null;
    protected $link    = null;



    /**
     * Sets the group property with a valid notification group based on the given group.
     *
     * @param string $group_name
     *
     * @return $this
     */
    public function group(string $group_name)
    {
        $this->group = notifier()->database()->findGroup($group_name);

        return $this;
    }



    /**
     * Sets the content property.
     *
     * @param string $content
     *
     * @return $this
     */
    public function content(string $content)
    {
        $this->content = $content;

        return $this;
    }



    /**
     * Sets the title property.
     *
     * @param string $title
     *
     * @return $this
     */
    public function title(string $title)
    {
        $this->title = $title;

        return $this;
    }



    /**
     * Sets the title property.
     *
     * @param string $message
     *
     * @return $this
     * @deprecated 2018-12-5
     */
    public function message(string $message)
    {
        return $this->title($message);
    }



    /**
     * Sets the link property.
     *
     * @param string $link
     *
     * @return $this
     */
    public function link(string $link)
    {
        $this->link = $link;

        return $this;
    }



    /**
     * Returns the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
             'group'   => $this->group->getName(),
             'title'   => $this->title,
             'content' => $this->content,
             'link'    => $this->link,
        ];
    }



    /**
     * Converts the object to its JSON representation.
     *
     * @param int $options
     *
     * @return string
     */
    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }
}
