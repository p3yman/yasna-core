<?php

namespace Modules\Notifier\Services\Sms;

use Illuminate\Notifications\Notification;
use Modules\Notifier\Channels\SmsChannel;
use Modules\Yasna\Entities\User;


class Ahra extends SmsChannel
{


    /**
     * load Ahra config
     *
     * @return array
     */
    public function loadConfig(): array
    {
        $notifier = notifier()::locate("sms", 'ahra');

        return [
             "domain"       => $notifier->getData("domain"),
             "user"         => $notifier->getData("user"),
             "password"     => $notifier->getData("password"),
             "from"         => $notifier->getData("from"),
             "receivetype"  => $notifier->getData("receivetype"),
             "scheduledate" => $notifier->getData("scheduledate"),
             "url"          => $notifier->getData("url"),
        ];
    }



    /**
     * trigger Ahra system to send the message.
     *
     * @param string $message
     * @param string $destination
     *
     * @return mixed
     */
    public function curl(string $message, string $destination)
    {
        $fields  = $this->setParameters($message, $destination);
        $headers = $this->setHeaders();

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $this->config['url'] . '?' . $fields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl, CURLOPT_TIMEOUT, 3);
        $response = curl_exec($curl);

        curl_close($curl);

        return $response;

    }



    /**
     * setup POST headers
     *
     * @return array
     */
    public function setHeaders()
    {
        $headers[] = 'Content-Type: application/json';

        return $headers;
    }



    /**
     * setup the POST parameters
     *
     * @param string $message
     * @param string $destination
     *
     * @return false|string
     */
    public function setParameters($message, $destination)
    {
        return http_build_query([
             'domain'       => $this->config['domain'],
             'user'         => $this->config['user'],
             'password'     => $this->config['password'],
             'from'         => $this->config['from'],
             "receivetype"  => $this->config['receivetype'],
             "scheduledate" => $this->config['scheduledate'],
             'to'           => $destination,
             'message'      => $message,
        ]);
    }



    /**
     * get message from the notification instance.
     *
     * @param User         $notifiable
     * @param Notification $notification
     *
     * @return string
     */
    public function getMessage($notifiable, $notification): string
    {
        return trim($notification->toSms($notifiable));
    }

}
