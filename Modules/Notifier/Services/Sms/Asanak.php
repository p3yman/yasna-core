<?php


namespace Modules\Notifier\Services\Sms;

use Illuminate\Notifications\Notification;
use Modules\Notifier\Channels\SmsChannel;
use Modules\Yasna\Entities\User;

class Asanak extends SmsChannel
{


    /**
     * load asanak config
     *
     * @return array
     */
    public function loadConfig(): array
    {
        $notifier = notifier()::locate("sms", 'asanak');

        return [
             "username" => $notifier->getData("username"),
             "password" => $notifier->getData("password"),
             "source"   => $notifier->getData("source"),
             "url"      => $notifier->getData("url"),
        ];
    }



    /**
     * trigger Asanak system to send message.
     *
     * @param string $message
     * @param string $destination
     *
     * @return mixed
     */
    public function curl(string $message, string $destination)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Accept: application/json']);
        curl_setopt($curl, CURLOPT_URL, $this->config['url']);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS,
             http_build_query([
                  'username'    => $this->config['username'],
                  'password'    => $this->config['password'],
                  'source'      => $this->config['source'],
                  'destination' => $destination,
                  'message'     => $message,
             ]));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }



    /**
     * get message from the notification instance.
     *
     * @param User             $notifiable
     * @param Notification $notification
     *
     * @return string
     */
    public function getMessage($notifiable, $notification): string
    {
        return urlencode(trim($notification->toSms($notifiable)));
    }

}
