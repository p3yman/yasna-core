<?php

namespace Modules\Notifier\Services\Sms;

use Illuminate\Notifications\Notification;
use Modules\Notifier\Channels\SmsChannel;
use Modules\Yasna\Entities\User;


class Faraz extends SmsChannel
{


    /**
     * load faraz config
     *
     * @return array
     */
    public function loadConfig(): array
    {
        $notifier = notifier()::locate("sms", 'faraz');

        return [
             "op"    => $notifier->getData("op"),
             "uname" => $notifier->getData("uname"),
             "pass"  => $notifier->getData("pass"),
             "from"  => $notifier->getData("from"),
             "time"  => $notifier->getData("time"),
             "url"   => $notifier->getData("url"),
        ];
    }



    /**
     * trigger faraz system to send the message.
     *
     * @param string $message
     * @param string $destination
     *
     * @return mixed
     */
    public function curl(string $message, string $destination)
    {
        $param = $this->setParameters($message, $destination);

        $handler = curl_init($this->config['url']);
        curl_setopt($handler, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($handler, CURLOPT_POSTFIELDS, $param);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
        $response2 = curl_exec($handler);

        $response2 = json_decode($response2);
        $res_data  = $response2[1];

        return json_encode($res_data);

    }



    /**
     * setup POST headers
     *
     * @return array
     */
    public function setHeaders()
    {
        $headers[] = 'Content-Type: application/json';

        return $headers;
    }



    /**
     * setup the POST parameters
     *
     * @param string $message
     * @param string $destination
     *
     * @return false|string
     */
    public function setParameters($message, $destination)
    {
        return http_build_query([
             'op'      => $this->config['op'],
             'uname'   => $this->config['uname'],
             'pass'    => $this->config['pass'],
             'from'    => $this->config['from'],
             'to'      => json_encode($destination),
             'time'    => $this->config['time'],
             'message' => $message,
        ]);
    }



    /**
     * get message from the notification instance.
     *
     * @param User         $notifiable
     * @param Notification $notification
     *
     * @return string
     */
    public function getMessage($notifiable, $notification): string
    {
        return trim($notification->toSms($notifiable));
    }

}
