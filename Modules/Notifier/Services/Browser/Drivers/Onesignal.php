<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/20/19
 * Time: 11:32 AM
 */

namespace Modules\Notifier\Services\Browser\Drivers;


use App\Models\User;
use Illuminate\Notifications\Notification;
use Modules\Notifier\Services\Browser\Drivers\Onesignal\OneSignalCaller;
use Modules\Notifier\Services\Browser\Drivers\Template\DriverAbstract;
use NotificationChannels\OneSignal\Exceptions\CouldNotSendNotification;
use ReflectionException;

final class Onesignal extends DriverAbstract
{
    /**
     * @inheritdoc
     * @throws ReflectionException
     */
    public function isActive(): bool
    {
        $app_id       = $this->getDriverModelDataField('app_id');
        $rest_api_key = $this->getDriverModelDataField('rest_api_key');

        return ($app_id and $rest_api_key);
    }



    /**
     * @inheritdoc
     * @throws CouldNotSendNotification
     */
    public function send($notifiable, Notification $notification)
    {
        /** @var OneSignalCaller $caller */
        $caller = app()->make(OneSignalCaller::class);

        return $caller->send($notifiable, $notification);
    }



    /**
     * @inheritdoc
     */
    public function routeNotificationForUser(User $user)
    {
        return [
             'tags' => [
                  'key'      => 'user_id',
                  'relation' => '',
                  'value'    => $user->hashid,
             ],
        ];
    }
}
