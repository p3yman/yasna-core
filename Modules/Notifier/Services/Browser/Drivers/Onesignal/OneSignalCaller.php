<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 3:34 PM
 */

namespace Modules\Notifier\Services\Browser\Drivers\Onesignal;


use Berkayk\OneSignal\OneSignalClient;
use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\Exceptions\CouldNotSendNotification;
use Psr\Http\Message\ResponseInterface;

class OneSignalCaller
{
    /** @var OneSignalClient */
    protected $one_signal;



    /**
     * OneSignalCaller constructor.
     *
     * @param OneSignalClient $one_signal
     */
    public function __construct(OneSignalClient $one_signal)
    {
        $this->one_signal = $one_signal;
    }



    /**
     * Send the given notification.
     *
     * @param mixed        $notifiable
     * @param Notification $notification
     *
     * @throws CouldNotSendNotification
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$user_id = $notifiable->routeNotificationFor('browser')) {
            return;
        }

        /** @var ResponseInterface $response */
        $response = $this->one_signal->sendNotificationCustom(
             $this->payload($notifiable, $notification, $user_id)
        );

        if ($response->getStatusCode() !== 200) {
            throw CouldNotSendNotification::serviceRespondedWithAnError($response);
        }
    }



    /**
     * Returns the payload of the OneSignal notification.
     *
     * @param mixed        $notifiable
     * @param Notification $notification
     * @param array        $user_ids
     *
     * @return array
     */
    protected function payload($notifiable, $notification, $user_ids)
    {
        return OneSignalPayloadFactory::make($notifiable, $notification, $user_ids);
    }
}
