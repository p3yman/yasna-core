<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 2:26 PM
 */

namespace Modules\Notifier\Services\Browser\Drivers\Onesignal;

use Illuminate\Notifications\Notification;
use NotificationChannels\OneSignal\OneSignalPayloadFactory as BaseFactory;

class OneSignalPayloadFactory extends BaseFactory
{
    /**
     * Makes a onesignal notification payload.
     *
     * @param mixed        $notifiable
     * @param Notification $notification
     * @param mixed        $targeting
     *
     * @return array
     */
    public static function make($notifiable, Notification $notification, $targeting): array
    {
        $payload = $notification->toBrowser($notifiable)->toArray();

        if (static::isTargetingEmail($targeting)) {
            $payload['filters'] = collect([['field' => 'email', 'value' => $targeting['email']]]);
        } elseif (static::isTargetingTags($targeting)) {
            $payload['tags'] = collect([$targeting['tags']]);
        } else {
            $payload['include_player_ids'] = collect($targeting);
        }

        return $payload;
    }
}
