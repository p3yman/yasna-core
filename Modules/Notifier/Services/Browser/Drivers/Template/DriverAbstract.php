<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/20/19
 * Time: 11:33 AM
 */

namespace Modules\Notifier\Services\Browser\Drivers\Template;

use App\Models\Notifier;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Notifications\Notification;
use Illuminate\View\View;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use ReflectionClass;
use ReflectionException;

/**
 * Class DriverAbstract
 * <br>
 * This class will be extended by the drivers classes.
 */
abstract class DriverAbstract
{
    use ModuleRecognitionsTrait;

    /**
     * The instance of the driver's model
     *
     * @var Notifier
     */
    protected $driver_model;



    /**
     * Fetches the specified field form the data of the driver's model and returns it.
     *
     * @param string $field
     *
     * @return string|null
     * @throws ReflectionException
     */
    public function getDriverModelDataField(string $field)
    {
        $data = $this->getDriverModelData();

        return ($data[$field] ?? null);
    }



    /**
     * Returns the data meta of the driver's model.
     *
     * @return array
     * @throws ReflectionException
     */
    protected function getDriverModelData(): array
    {
        $data = ($this->getDriverModel()->getMeta('data') ?? null);

        return ($data and is_array($data))
             ? $data
             : [];
    }



    /**
     * Returns the driver model.
     *
     * @return Notifier
     * @throws ReflectionException
     */
    public function getDriverModel()
    {
        if (!$this->driver_model) {
            $this->driver_model = notifier()::locate('browser', $this->getDriverSlug());
        }

        return $this->driver_model;
    }



    /**
     * Returns the view response of the general view.
     *
     * @return Factory|View
     * @throws ReflectionException
     */
    public function getGeneralView()
    {
        return view($this->getGeneralViewPath(), ['driver' => $this]);
    }



    /**
     * Returns the path of the general view.
     *
     * @return string
     * @throws ReflectionException
     */
    protected function getGeneralViewPath(): string
    {
        return $this->runningModule()->getBladePath('browser.drivers.' . $this->getDriverSlug());
    }



    /**
     * Returns the slug of the related driver.
     *
     * @return string
     * @throws ReflectionException
     */
    protected function getDriverSlug(): string
    {
        $class_name = (new ReflectionClass($this))->getShortName();

        return str_slug($class_name);
    }



    /**
     * If the driver is active.
     *
     * @return bool
     */
    abstract public function isActive(): bool;



    /**
     * If the driver is not active.
     *
     * @return bool
     */
    public function isNotActive(): bool
    {
        return !$this->isActive();
    }



    /**
     * Sends the notification throw the channel driver.
     *
     * @param mixed        $notifiable
     * @param Notification $notification
     */
    abstract public function send($notifiable, Notification $notification);



    /**
     * Returns the route notification for a user notifier.
     *
     * @param User $user
     *
     * @return mixed
     */
    abstract public function routeNotificationForUser(User $user);
}
