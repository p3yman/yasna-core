<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 5:21 PM
 */

namespace Modules\Notifier\Services\Browser;


use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Modules\Notifier\Services\Browser\Drivers\Template\DriverAbstract;

class BrowserNotificationHandler
{
    /**
     * The Instance of the Resolver
     *
     * @var DriverResolver
     */
    protected $driver_resolver;



    /**
     * If the browser notification has an active status.
     *
     * @return bool
     */
    public function isActive(): bool
    {
        $resolver = $this->getDefaultDriverResolver();
        return (
             $resolver->isResolvable()
             and
             $resolver->getDriverInstance()->isActive()
        );
    }



    /**
     * If the browser notification has not an active status.
     *
     * @return bool
     */
    public function isNotActive(): bool
    {
        return !$this->isActive();
    }



    /**
     * Returns the general view of the notification browser.
     * <br>
     * _This view will be appended to some of the responses._
     *
     * @return Factory|View
     */
    public function getGeneralView()
    {
        return $this->getDefaultDriverInstance()->getGeneralView();
    }



    /**
     * Returns the instance of the default driver's class.
     *
     * @return DriverAbstract
     */
    public function getDefaultDriverInstance()
    {
        return $this->getDefaultDriverResolver()->getDriverInstance();
    }



    /**
     * Returns the resolver to resolve the default driver.
     *
     * @return DriverResolver
     */
    protected function getDefaultDriverResolver()
    {
        if (!$this->driver_resolver) {
            $this->driver_resolver = new DriverResolver($this->getDefaultDriver());
        }

        return $this->driver_resolver;
    }



    /**
     * Returns he slug of the default driver.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return notifier()::getDefaultDriverOf('browser');
    }
}
