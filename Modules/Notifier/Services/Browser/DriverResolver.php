<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 5:22 PM
 */

namespace Modules\Notifier\Services\Browser;


use Modules\Notifier\Services\Browser\Drivers\Template\DriverAbstract;

class DriverResolver
{
    /**
     * The Name of the Driver
     *
     * @var string
     */
    protected $driver_name;

    /**
     * The Driver's Instance
     *
     * @var DriverAbstract
     */
    protected $driver;



    /**
     * DriverResolver constructor.
     *
     * @param string $driver_name
     */
    public function __construct(string $driver_name)
    {
        $this->driver_name = $driver_name;
    }



    /**
     * Whether the driver may be resolved.
     *
     * @return bool
     */
    public function isResolvable()
    {
        return (class_exists($this->getDriverClass()));
    }



    /**
     * Returns the name of the driver class.
     *
     * @return string
     */
    protected function getDriverClass()
    {
        return implode('\\', [
             $this->getDriversNameSpace(),
             studly_case($this->driver_name),
        ]);
    }



    /**
     * Returns the namespace of the drivers.
     *
     * @return string
     */
    protected function getDriversNameSpace()
    {
        return implode('\\', [
             __NAMESPACE__,
             'Drivers',
        ]);
    }



    /**
     * Returns the instance of the driver's class.
     *
     * @return DriverAbstract
     */
    public function getDriverInstance()
    {
        if (!$this->driver) {
            $class        = $this->getDriverClass();
            $this->driver = new $class();
        }

        return $this->driver;
    }
}
