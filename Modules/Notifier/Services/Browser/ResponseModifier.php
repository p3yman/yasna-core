<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/22/19
 * Time: 1:22 PM
 */

namespace Modules\Notifier\Services\Browser;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ResponseModifier
{
    /**
     * @var Response
     */
    protected $response;

    /**
     * @var Request
     */
    protected $request;



    /**
     * ResponseModifier constructor.
     *
     * @param Response $response
     * @param Request  $request
     */
    public function __construct($response, Request $request)
    {
        $this->response = $response;
        $this->request  = $request;
    }



    /**
     * Modifies the response and returns the new version.
     *
     * @return Response
     */
    public function getModifiedVersion()
    {
        if ($this->responseIsNotModifiable()) {
            return $this->response;
        }

        if ($this->requestIsAjax()) {
            return $this->response;
        }

        $this->appendAdditiveCode();

        return $this->response;
    }



    /**
     * If the response is modifiable.
     *
     * @return bool
     */
    protected function responseIsModifiable()
    {
        return ($this->response instanceof Response);
    }



    /**
     * If the response is not modifiable.
     *
     * @return bool
     */
    protected function responseIsNotModifiable()
    {
        return !$this->responseIsModifiable();
    }



    /**
     * If the request is AJAX.
     *
     * @return bool
     */
    protected function requestIsAjax()
    {
        return $this->request->ajax();
    }



    /**
     * Appends the additive code to the response's content.
     */
    protected function appendAdditiveCode()
    {
        $this->response->setContent($this->getFinalContent());
    }



    /**
     * Returns the final content (after adding the additive content).
     *
     * @return string
     */
    protected function getFinalContent()
    {
        $content          = $this->response->getContent();
        $additive_content = $this->getAdditiveCode();

        $pos = strripos($content, '</body>');
        if (false !== $pos) {
            return substr($content, 0, $pos) . $additive_content . substr($content, $pos);
        } else {
            return $content . $additive_content;
        }
    }



    /**
     * Returns the additive code.
     *
     * @return View
     */
    protected function getAdditiveCode()
    {
        return app('notifier.browser')->getGeneralView();
    }
}
