<?php


namespace Modules\Notifier\Http\Forms;


use Modules\Forms\Services\FormBinder;

class CreateDriverForm extends FormBinder
{
    /**
     * @inheritDoc
     */
    public function components()
    {
        $this->add("text")
             ->name("channel")
             ->label(trans('notifier::forms.channel_name'))
             ->order(11)
        ;

        $this->add("text")
             ->name("title")
             ->label(trans('notifier::forms.driver_title'))
             ->order(12)
        ;

        $this->add("text")
             ->name("driver")
             ->label(trans('notifier::forms.driver_name'))
             ->order(13)
        ;

        $this->add("text")
             ->name("data")
             ->label(trans('notifier::forms.fields_name'))
             ->order(14)
        ;


    }

}
