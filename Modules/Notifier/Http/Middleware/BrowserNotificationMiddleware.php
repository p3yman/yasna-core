<?php

namespace Modules\Notifier\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Notifier\Services\Browser\ResponseModifier;

class BrowserNotificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if (app('notifier.browser')->isNotActive()) {
            return $response;
        }

        $response_modifier = new ResponseModifier($response, $request);

        return $response_modifier->getModifiedVersion();
    }
}
