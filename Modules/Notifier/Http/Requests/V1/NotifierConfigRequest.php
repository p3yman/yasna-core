<?php

namespace Modules\Notifier\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class NotifierConfigRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Notifier";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     */
    protected $should_load_model_with_slug = true;


    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritDoc
     */
    protected function fillableFields()
    {
        return [
             'slug',
        ];
    }



    /**
     * @inheritDoc
     */
    public function corrections()
    {
        $this->setDefaultDriver($this->slug);
        $this->unsetIllegalFields();

    }



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        if ($this->model->available_for_admins) {
            return user()->isSuperadmin();
        }
        return dev();
    }



    /**
     * unset the illegal fields
     */
    protected function unsetIllegalFields()
    {
        $request_array = $this->toArray();
        $allow_fields  = $this->model->getMeta();

        $illegal_fields = array_diff_key($request_array, $allow_fields['data']);

        foreach ($illegal_fields as $key => $illegal_field) {
            $this->unsetData($key);
        }
    }



    /**
     * set default driver of channel
     *
     * @param string $slug
     *
     * @return bool
     */
    private function setDefaultDriver($slug)
    {
        $array = explode(':', $slug);
        notifier()::setDefaultDriverOf($array[0], $array[1]);
        return true;
    }

}
