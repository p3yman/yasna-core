<?php

namespace Modules\Notifier\Http\Requests\V1;

use App\Models\Notifier;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class NotifierSaveRequest
 *
 * @property Notifier $model
 */
class NotifierSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Notifier";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'channel' => 'required',
             'title'   => 'required',
             'driver'  => 'required',
             'data'    => 'required',
             'slug'    => Rule::unique('notifiers', 'slug')
                              ->where('channel', $this->channel)
                              ->where('driver', $this->driver)
                              ->ignore($this->model->id, 'id'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'slug.unique' => trans('notifier::validation.driver_exist'),
        ];
    }



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return dev();
    }



    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'title',
             'channel',
             'driver',
             'data',
        ];
    }



    /**
     * @inheritDoc
     */
    public function corrections()
    {
        $this->correctSlug();

    }



    /**
     * correct slug
     */
    private function correctSlug()
    {
        $chanel = $this->getData('channel');
        $driver = $this->getData('driver');

        $slug = $chanel . ":" . $driver;

        $this->setData('slug', $slug);
    }


}
