<?php

namespace Modules\Notifier\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class NotifierRestoreRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Notifier";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     */
    protected $should_load_trashed_models_only = true;



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return dev();
    }
}
