<?php

namespace Modules\Notifier\Http\Requests\V1;

use Modules\Notifier\Entities\Notifier;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * class NotifierGetDriverRequest
 *
 * @property Notifier $model
 */
class NotifierGetDriverRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Notifier";

    /**
     * @inheritdoc
     */
    protected $should_load_model = true;

    /**
     * @inheritdoc
     */
    protected $should_load_model_with_hashid = true;



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        if ($this->model->available_for_admins) {
            return user()->isSuperadmin();
        }
        return dev();
    }
}
