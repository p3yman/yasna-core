<?php

namespace Modules\Notifier\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class NotifierTrashRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Notifier";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return dev();
    }
}
