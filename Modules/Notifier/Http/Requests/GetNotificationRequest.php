<?php

namespace Modules\Notifier\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class GetNotificationRequest extends YasnaRequest
{
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "limit" => "int|min:0",
             "page"  => "int|min:1",
        ];
    }



    /**
     * Return the value which must skipped for getting the correct result for
     * the requested page with specified limit.
     *
     * @return int
     */
    public function getSkipValue(): int
    {
        $page  = $this->getPageValue();
        $limit = $this->getLimitValue();

        return ($page - 1) * $limit;
    }



    /**
     * Return the number of requested notification.
     *
     * @return int
     */
    public function getTakeValue(): int
    {
        return $this->getLimitValue();
    }



    /**
     * Return requested page number: The page of notification which must be
     * showed same in a pagination situation.
     *
     * @return int
     */
    private function getPageValue(): int
    {
        return intval($this->input('page', 1));
    }



    /**
     * Return the number of notification which must be returned.
     *
     * @return int
     */
    private function getLimitValue(): int
    {
        return intval($this->input('limit', 15));
    }
}
