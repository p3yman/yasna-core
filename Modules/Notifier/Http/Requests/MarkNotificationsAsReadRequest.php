<?php

namespace Modules\Notifier\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class MarkNotificationsAsReadRequest extends YasnaRequest
{
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "notifications"     => "array",
             "notifications.*"   => "size:36|string",
             "all_notifications" => "in:0,1",
        ];
    }
}
