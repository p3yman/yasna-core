<?php

namespace Modules\Notifier\Http\Controllers\V1;

use Modules\Notifier\Http\Requests\GetNotificationRequest;
use Modules\Notifier\Http\Requests\MarkNotificationsAsReadRequest;
use Modules\Yasna\Services\YasnaApiController;

class NotificationController extends YasnaApiController
{
    /**
     * Return a list of the user notifications.
     *
     * @param GetNotificationRequest $request
     *
     * @return array
     */
    public function listAllNotifications(GetNotificationRequest $request)
    {
        $skip = $request->getSkipValue();
        $take = $request->getTakeValue();

        $data = user()
             ->notifications()
             ->orderBy('created_at', 'desc')
             ->skip($skip)
             ->take($take)
             ->get()
             ->map(function ($notification) {
                 $group = notifier()->database()->getGroup($notification);

                 return [
                      "id"      => $notification->id,
                      "link"    => notifier()->database()->getNotificationUrl($notification),
                      "isRead"  => $notification->read(),
                      "title"   => $notification->data['content'] ?? '',
                      "date"    => $notification->created_at->timestamp,
                      "caption" => $notification->data['message'] ?? null,
                      "type"    => $group->getIcon(),
                 ];
             })
             ->toArray()
        ;

        return $this->success($data);
    }



    /**
     * Set a set of notifications or all of them as read
     *
     * @param MarkNotificationsAsReadRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function markNotificationsAsRead(
         MarkNotificationsAsReadRequest $request
    ) {
        $all_notifications    = $request->input("all_notifications", false);
        $notifications_id_set = $request->input('notifications', []);

        if (!boolval($all_notifications) && empty($notifications_id_set)) {
            return $this->clientError(400);
        }

        if (boolval($all_notifications)) {
            return $this->markAllNotificationsAsRead();
        }

        return $this->markTheSetOfNotificationsAsRead($notifications_id_set);
    }



    /**
     * Mark all notifications of current logged in user as read
     *
     * @return array
     */
    private function markAllNotificationsAsRead(): array
    {
        if (user()->unreadNotifications()->count()) {
            user()->unreadNotifications()->update(['read_at' => now()]);
        }

        return $this->success([
             "done" => true,
        ]);
    }



    /**
     * Update a set of given notifications as read
     *
     * @param array $notifications
     *
     * @return array
     */
    private function markTheSetOfNotificationsAsRead(
         array $notifications
    ): array {
        if (user()->unreadNotifications()->count() && !empty($notifications)) {
            user()->unreadNotifications()
                  ->whereIn('id', $notifications)
                  ->update(['read_at' => now()])
            ;
        }

        return $this->success([
             "done" => true,
        ]);
    }
}
