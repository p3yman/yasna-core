<?php

namespace Modules\Notifier\Http\Controllers\V1;

use Modules\Forms\Services\FormBinder;
use Modules\Notifier\Http\Forms\CreateDriverForm;
use Modules\Notifier\Http\Requests\V1\NotifierConfigRequest;
use Modules\Notifier\Http\Requests\V1\NotifierFormConfigRequest;
use Modules\Notifier\Http\Requests\V1\NotifierRestoreRequest;
use Modules\Notifier\Http\Requests\V1\NotifierGetDriverRequest;
use Modules\Notifier\Http\Requests\V1\NotifierTrashRequest;
use Modules\Notifier\Http\Requests\V1\NotifierDestroyRequest;
use Modules\Notifier\Http\Requests\V1\NotifierSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class NotifierController extends YasnaApiController
{

    /**
     * add new driver
     *
     * @param NotifierSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(NotifierSaveRequest $request)
    {

        $request_array = $request->toArray();
        $fields_name   = ['data' => $this->getFields($request->data)];

        $done = $request->model->batchSave(array_merge($request_array, $fields_name));

        return $this->modelSaveFeedback($done);
    }



    /**
     * perform soft delete action of the driver
     *
     * @param NotifierTrashRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function trash(NotifierTrashRequest $request)
    {
        $done = $request->model->delete();

        return $this->typicalSaveFeedback($done);

    }



    /**
     * perform hard delete action of the driver
     *
     * @param NotifierDestroyRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(NotifierDestroyRequest $request)
    {
        $done = $request->model->hardDelete();

        return $this->typicalSaveFeedback($done);
    }



    /**
     * perform undelete action of the soft deleted driver
     *
     * @param NotifierRestoreRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restore(NotifierRestoreRequest $request)
    {
        $done = $request->model->undelete();

        return $this->typicalSaveFeedback($done);
    }



    /**
     * displays driver information
     *
     * @param NotifierGetDriverRequest $request
     *
     * @return array
     */
    public function getDriverInfo(NotifierGetDriverRequest $request)
    {
        $driver_info = $request->model->toResource();

        return $this->success($driver_info, [
             "id" => $request->id,
        ]);
    }



    /**
     * config the available driver
     *
     * @param NotifierConfigRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function config(NotifierConfigRequest $request)
    {
        $request_data = $request->toArray();
        $meta = $request->model->getMeta();

        $array_fields = array_replace($meta['data'] , $request_data);

        $done = $request->model->batchSave([
             'data' => $array_fields,
        ]);

        return $this->modelSaveFeedback($done);
    }



    /**
     * call the driver register form
     *
     * @return array
     */
    public function createDriverForm()
    {
        $form = (new CreateDriverForm());

        return $this->success([], ["form" => $form->toArray()]);
    }



    /**
     * call the driver config form
     *
     * @param NotifierFormConfigRequest $request
     *
     * @return array
     */
    public function configForm(NotifierFormConfigRequest $request)
    {
        $meta = $request->model->getMeta();
        $form = new FormBinder();

        foreach ($meta['data'] as $key => $value) {
            $form->add('text')
                ->name($key)
                ->label($key)
                ->value($value ?: '');
        }

        return $this->success([], ["form" => $form->toArray()]);
    }



    /**
     * get correct data style to save in driver list
     *
     * @param string     $fields
     * @param array|null $original_arr
     *
     * @return array
     */
    private function getFields($fields, $original_arr = null)
    {
        $arr  = explode(',', $fields);
        $data = [];
        foreach ($arr as $item) {
            $item_trimmed = trim($item);
            if (!isset($original_arr[$item_trimmed])) {
                $data[$item_trimmed] = null;
            } else {
                $data[$item_trimmed] = $original_arr[$item_trimmed];
            }
        }
        return $data;
    }
}
