<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Notifier\Http\Controllers\V1\NotifierController;

/**
 * @api               {GET}
 *                    /api/modular/v1/notifier-create-form
 *                    Create Form
 * @apiDescription    Driver building form
 * @apiVersion        1.0.0
 * @apiName           Create Form
 * @apiGroup          Notifier
 * @apiPermission     Developer
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "form": [
 *              {
 *                     "component_name": "text",
 *                     "order": 11,
 *                      "id": "",
 *                      "name": "channel",
 *                      "css": "",
 *                       "css_class": "",
 *                       "width": "",
 *                       "col": "",
 *                       "default": "",
 *                       "value": "",
 *                       "placeholder": "",
 *                       "validation": "",
 *                       "label": "Channel Name",
 *                       "label_desc": "",
 *                       "desc": "",
 *                       "clone": "",
 *                       "size": "",
 *                       "char_limit": "",
 *                       "prepend": "",
 *                       "append": "",
 *                       "disabled": false,
 *                       "readonly": false
 *      },
 *      "results": []
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "",
 *      "userMessage": "",
 *      "errorCode": "",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method NotifierController controller()
 */
class CreateFormEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Create Driver Form";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotifierController@createDriverForm';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "status"   => 200,
             "metadata" => [
                  "form" => [
                       [
                            "component_name" => "text",
                            "order"          => 11,
                            "id"             => "",
                            "name"           => "channel",
                            "css"            => "",
                            "css_class"      => "",
                            "width"          => "",
                            "col"            => "",
                            "default"        => "",
                            "value"          => "",
                            "placeholder"    => "",
                            "validation"     => "",
                            "label"          => "Channel Name",
                            "label_desc"     => "",
                            "desc"           => "",
                            "clone"          => "",
                            "size"           => "",
                            "char_limit"     => "",
                            "prepend"        => "",
                            "append"         => "",
                            "disabled"       => false,
                            "readonly"       => false,
                       ],
                  ],
                  [
                       "results" => [],
                  ],
             ],
        ]);
    }
}
