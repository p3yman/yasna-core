<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use DateInterval;
use DateTime;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {get} /api/modular/v1/notifier-get-notifications
 *                    Notifications List
 * @apiDescription    Get notifications list of the current authenticated
 *                    user
 * @apiVersion        1.0.0
 * @apiName           List Notifications
 * @apiGroup          Notifications
 * @apiParam {Integer} [page=1] Page of notifications to show.
 * @apiParam {Integer} [limit=15] Set a limit on number of requested
 *           notifications.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": [
 *          {
 *              "class": "p-lg",
 *              "id": "fe0e15ea-41eb-480b-81ee-a4eec6495065",
 *              "link": null,
 *              "is_read": false,
 *              "title": "",
 *              "date": 1542525176,
 *              "content": null,
 *              "color": "warning",
 *              "icon": "bell"
 *           }
 *      ]
 * }
 * @apiErrorExample   Failed-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 */
class GetNotificationsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get Notifications";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotificationController@listAllNotifications';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond(
             [
                  $this->mockNotificationBuilder(
                       '0b75afa7-90c5-450d-8f15-b2c3da287421',
                       'default',
                       'انقضای فلان حکم نزدیک است',
                       'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
                       (new DateTime(now()))->sub(new DateInterval('P1D'))
                                            ->getTimestamp(),
                       false,
                       "http://yasna.team/noti/12"
                  ),
                  $this->mockNotificationBuilder(
                       'e1ebf56e-0713-4551-93b1-ea937fb97ae0',
                       'default',
                       'انقضای فلان حکم نزدیک است',
                       'لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.',
                       (new DateTime(now()))->getTimestamp(),
                       false,
                       "http://yasna.team/noti/134"
                  ),
             ]
        );
    }



    /**
     * Create a mock notification sample
     *
     * @param mixed $id
     * @param mixed $type
     * @param mixed $title
     * @param mixed $caption
     * @param mixed $date
     * @param mixed $is_read
     * @param mixed $link
     *
     * @return array
     */
    private function mockNotificationBuilder(
         $id,
         $type,
         $title,
         $caption,
         $date,
         $is_read,
         $link
    ) {
        return [
             "id"      => $id,
             "type"    => $type,
             "title"   => $title,
             "caption" => $caption,
             "date"    => $date,
             "isRead"  => $is_read,
             "link"    => $link,
        ];
    }
}
