<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Notifier\Http\Controllers\V1\NotifierController;

/**
 * @api               {POST}
 *                    /api/modular/v1/notifier-save
 *                    Create new notifier driver
 * @apiDescription    Create new notifier driver
 * @apiVersion        1.0.0
 * @apiName           Create new notifier driver
 * @apiGroup          Notifier
 * @apiPermission     Developer
 * @apiParam {string}   id Hashid of existing notifier (This field is for edit mode)
 * @apiParam {string}   channel_name Channel name
 * @apiParam {string}   driver_title Driver title
 * @apiParam {string}   driver_name Driver name
 * @apiParam {string}   data Fields required to configure a driver(comma-separated)
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" : hashid(folan),
 *          "authenticated_user": hashid(user_id)
 *      },
 *      "results": {
 *          "done" : "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method NotifierController controller()
 */
class SaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Create new notifier driver";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();

    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotifierController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid"             => hashid(rand(1)),
             "authenticated_user" => hashid(user()->id),
        ]);
    }
}
