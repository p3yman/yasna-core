<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Notifier\Http\Controllers\V1\NotifierController;

/**
 * @api               {POST}
 *                    /api/modular/v1/notifier-driver-config
 *                    Driver Config
 * @apiDescription    Config the existing driver
 * @apiVersion        1.0.0
 * @apiName           Driver Config
 * @apiGroup          Notifier
 * @apiPermission     Super Admin
 * @apiParam {string}   slug Slug of the driver
 * @apiParam {string}   [fields name] The name of the driver configurations fields
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *          "authenticated_user"=> "hashid(user()->id)"
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample   
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Content not found!",
 *      "userMessage": "Content not found!",
 *      "errorCode": "404",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method NotifierController controller()
 */
class DriverConfigEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Driver Config";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotifierController@config';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(12),
             "authenticated_user"=> hashid(user()->id),
        ]);
        
    }
}
