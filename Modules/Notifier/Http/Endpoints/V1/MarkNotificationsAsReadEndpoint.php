<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {post}
 *                    /api/modular/v1/notifier-mark-notifications-as-read
 *                    Mark as Read
 * @apiDescription    Set a collection of notifications or all of them as
 *                    read for current authenticated user
 * @apiVersion        1.0.0
 * @apiName           Mark Notifications as Read
 * @apiGroup          Notifications
 * @apiParam {Integer=0,1} [all_notifications=0] Mark
 *           all notifications of the current authenticated user as read.
 *           `1` means true and `0` is false.
 * @apiParam {String[]} [notifications=null] A set of given
 *           notifications to mark them as read.
 * @apiParamExample   Selected Notifications:
 * {
 *      "notifications": [
 *          "0b75afa7-90c5-450d-8f15-b2c3da287421",
 *          "e1ebf56e-0713-4551-93b1-ea937fb97ae0"
 *      ]
 * }
 * @apiParamExample   All Notifications:
 * {
 *      "all_notifications": 1
 * }
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": {
 *          "done": true
 *      }
 * }
 * @apiErrorExample   Unauthenticated-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   Not Given Params Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "notifier::developerMessages.400",
 *      "userMessage": "notifier::userMessages.400",
 *      "errorCode": 400,
 *      "moreInfo": "notifier.moreInfo.400",
 *      "errors": []
 * }
 */
class MarkNotificationsAsReadEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Mark Notifications As Read";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotificationController@markNotificationsAsRead';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => true,
        ]);
    }
}
