<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Notifier\Http\Controllers\V1\NotifierController;

/**
 * @api               {GET}
 *                    /api/modular/v1/notifier-driver-get
 *                    Driver Get
 * @apiDescription    Get single driver information
 * @apiVersion        1.0.0
 * @apiName           Driver Get
 * @apiGroup          Notifier
 * @apiPermission     Admin
 * @apiParam {string}   id  Hashid of the existing driver
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id" => "qKXVA",
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *                    "id": "qKXVA",
 *                    "slug": "mail:smtp",
 *                    "title": "Smtp Mail Notifier",
 *                    "channel": "mail",
 *                    "driver": "smtp",
 *                    "available_for_admins": 0,
 *                    "created_at": 1556513355,
 *                    "updated_at": 1557296160,
 *                    "deleted_at": 1557296160,
 *                    "created_by": null,
 *                    "updated_by": null,
 *                    "deleted_by": null,
 *                    "converted": 0,
 *                    "data": {
 *                              "username": "",
 *                              "password": "",
 *                              "host": "smtp.yasna.team",
 *                              "port": "587",
 *                              "from-address": "no-reply@yasna.team",
 *                              "from-name": "Yasnateam Notifier",
 *                              "encryption": null
 *                    }
 *      }
 * }
 * @apiErrorExample   
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method NotifierController controller()
 */
class DriverGetEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Driver Get";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotifierController@getDriverInfo';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'status'   => 200,
             'metadata' => [
                  "id"=> "qKXVA",
                  "authenticated_user"=> hashid(user()->id),
             ],
             "result" => [
                       "id"=> "qKXVA",
                       "slug"=> "mail:smtp",
                       "title"=> "Smtp Mail Notifier",
                       "channel"=> "mail",
                       "driver"=> "smtp",
                       "available_for_admins"=> 0,
                       "created_at"=> 1556513355,
                       "updated_at"=> 1557297135,
                       "deleted_at"=> 1557297135,
                       "created_by"=> null,
                       "updated_by"=> null,
                       "deleted_by"=> null,
                       "converted"=> 0,
                       "data"=> [
                            "username"=> "",
                            "password"=> "",
                            "host"=> "smtp.yasna.team",
                            "port"=> "587",
                            "from-address"=> "no-reply@yasna.team",
                            "from-name"=> "Yasnateam Notifier",
                            "encryption"=> null
                       ],
             ]
        ]);
    }
}
