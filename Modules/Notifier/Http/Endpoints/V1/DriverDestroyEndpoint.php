<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Notifier\Http\Controllers\V1\NotifierController;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/notifier-driver-destroy
 *                    Driver Destroy
 * @apiDescription    Hard Delete a driver
 * @apiVersion        1.0.0
 * @apiName           Driver Destroy
 * @apiGroup          Notifier
 * @apiPermission     Developer
 * @apiParam {string}   id Hashid of the soft deleted notifier model
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": hashid(user()->id)
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Content not found!",
 *      "userMessage": "Content not found!",
 *      "errorCode": "404",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method NotifierController controller()
 */
class DriverDestroyEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Driver Destroy";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotifierController@destroy';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "authenticated_user"=> hashid(user()->id)
        ]);

    }
}
