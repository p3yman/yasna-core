<?php

namespace Modules\Notifier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Notifier\Http\Controllers\V1\NotifierController;

/**
 * @api               {GET}
 *                    /api/modular/v1/notifier-config-form
 *                    Config Form
 * @apiDescription    Create driver config form.
 * @apiVersion        1.0.0
 * @apiName           Config Form
 * @apiGroup          Notifier
 * @apiPermission     Super Admin
 * @apiParam {string}   hashid  hashid of the existing driver
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "form": [
 *              {
 *                     "component_name": "text",
 *                     "order": 11,
 *                      "id": "",
 *                      "name": "username",
 *                      "css": "",
 *                       "css_class": "",
 *                       "width": "",
 *                       "col": "",
 *                       "default": "",
 *                       "value": "",
 *                       "placeholder": "",
 *                       "validation": "",
 *                       "label": "username",
 *                       "label_desc": "",
 *                       "desc": "",
 *                       "clone": "",
 *                       "size": "",
 *                       "char_limit": "",
 *                       "prepend": "",
 *                       "append": "",
 *                       "disabled": false,
 *                       "readonly": false
 *      },
 *      "results": []
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unauthorized operation",
 *      "userMessage": "There is not enough access.",
 *      "errorCode": "403",
 *      "moreInfo": "notifier.moreInfo.403",
 *      "errors": []
 * }
 * @method NotifierController controller()
 */
class ConfigFormEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Config Form";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Notifier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'NotifierController@configForm';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "status"   => 200,
             "metadata" => [
                  "form" => [
                       [
                            "component_name" => "text",
                            "order"          => 11,
                            "id"             => "",
                            "name"           => "username",
                            "css"            => "",
                            "css_class"      => "",
                            "width"          => "",
                            "col"            => "",
                            "default"        => "",
                            "value"          => "",
                            "placeholder"    => "",
                            "validation"     => "",
                            "label"          => "username",
                            "label_desc"     => "",
                            "desc"           => "",
                            "clone"          => "",
                            "size"           => "",
                            "char_limit"     => "",
                            "prepend"        => "",
                            "append"         => "",
                            "disabled"       => false,
                            "readonly"       => false,
                       ],
                  ],
                  "authenticated_user"=> "qKXVA",
                  [
                       "results" => [],
                  ],
             ],
        ]);
    }
}
