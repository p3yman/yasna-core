<?php

namespace Modules\Notifier\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MakeNotification extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes a ready-to-use notification class.';

    /**
     * keep the stub file name
     *
     * @var string
     */
    protected $stub_name = "notification";



    /**
     * get the target folder os path
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('Notifications');
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Notification')) {
            return $file_name . "Notification";
        }
        return $file_name;
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             'MODULE' => $this->module_name,
             'CLASS'  => $this->class_name,
        ];
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "notification.stub";
    }
}
