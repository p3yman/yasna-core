<body>

</body>
@if (app('notifier.browser')->isNotActive())
	@php return @endphp
@endif

<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
<script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(function () {
        OneSignal.init({
            appId: "{{ $driver->getDriverModelDataField('app_id') }}",
        });

        OneSignal.push(['registerForPushNotifications']);
    });


    OneSignal.push(function () {
        OneSignal.sendTags({
            user_id: "{{ user()->hashid }}"
        });
    })
</script>
