<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 3:52 PM
 */

namespace Modules\Notifier\Entities\Traits;


trait UserRouteNotificationsTrait
{
    /**
     * Returns the route notification for the browser channel.
     *
     * @return mixed
     */
    public function routeNotificationForBrowser()
    {
        $handler = app('notifier.browser');

        if ($handler->isNotActive()) {
            return null;
        }


        return $handler
             ->getDefaultDriverInstance()
             ->routeNotificationForUser($this)
             ;
    }
}
