<?php

namespace Modules\Notifier\Entities\Traits;

trait MainModelResourceTrait
{
    /**
     * boot MainModelResourceTrait
     *
     * @return void
     */
    public static function bootMainModelResourceTrait()
    {
        static::addDirectResources('*');
    }
}
