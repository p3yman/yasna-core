<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/20/19
 * Time: 11:45 AM
 */

namespace Modules\Notifier\Providers;


use Berkayk\OneSignal\OneSignalClient;
use Modules\Notifier\Exceptions\NotificationFailed;
use Modules\Notifier\Http\Middleware\BrowserNotificationMiddleware;
use Modules\Notifier\Services\Browser\BrowserNotificationHandler;
use Modules\Notifier\Services\Browser\Drivers\Onesignal\OneSignalCaller;
use NotificationChannels\OneSignal\Exceptions\InvalidConfiguration;

trait BrowserTrait
{
    /**
     * Registers all the needs of the browser notifications.
     */
    protected function registerBrowser()
    {
        $this->registerBrowserSingleton();
        $this->registerBrowserMiddleware();

        $this->registerOneSignal();
    }



    /**
     * Registers a signleton as `notifier.browser` to handle the processes for the browser notification.
     */
    protected function registerBrowserSingleton()
    {
        $this->app->singleton('notifier.browser', function () {
            return new BrowserNotificationHandler();
        });
    }



    /**
     * Registers the alias for the browser middleware.
     */
    protected function registerBrowserMiddleware()
    {
        $this->addMiddleware('browser_notification', BrowserNotificationMiddleware::class);
    }



    /**
     * Registers the needs for the onesignal driver.
     */
    protected function registerOneSignal()
    {
        $this->registerOneSignalClientMaker();
    }



    /**
     * Register the maker for the onesignal client.
     */
    protected function registerOneSignalClientMaker()
    {
        $this->app->when(OneSignalCaller::class)
                  ->needs(OneSignalClient::class)
                  ->give(function () {
                      $driver       = notifier()::locate('browser', 'onesignal');
                      $app_id       = $driver->getData('app_id');
                      $rest_api_key = $driver->getData('rest_api_key');

                      if (!$app_id or !$rest_api_key) {
                          throw NotificationFailed::submissionError("Not enough configuration for Onesignal driver");
                      }

                      return new OneSignalClient(
                           $app_id,
                           $rest_api_key,
                           ''
                      );
                  })
        ;
    }
}
