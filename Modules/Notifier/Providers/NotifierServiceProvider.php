<?php

namespace Modules\Notifier\Providers;

use Illuminate\Routing\Router;
use Modules\Notifier\Console\MakeNotification;
use Modules\Notifier\Http\Endpoints\V1\ConfigFormEndpoint;
use Modules\Notifier\Http\Endpoints\V1\CreateFormEndpoint;
use Modules\Notifier\Http\Endpoints\V1\DriverConfigEndpoint;
use Modules\Notifier\Http\Endpoints\V1\DriverDestroyEndpoint;
use Modules\Notifier\Http\Endpoints\V1\DriverGetEndpoint;
use Modules\Notifier\Http\Endpoints\V1\DriverRestoreEndpoint;
use Modules\Notifier\Http\Endpoints\V1\DriverTrashEndpoint;
use Modules\Notifier\Http\Endpoints\V1\SaveEndpoint;
use Modules\Notifier\Http\Endpoints\V1\GetNotificationsEndpoint as GetNotificationsEndpointV1;
use Modules\Notifier\Http\Endpoints\V1\MarkNotificationsAsReadEndpoint as MarkNotificationsAsReadEndpointV1;
use Modules\Notifier\Http\Middleware\NotificationMiddleware;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class NotifierServiceProvider
 *
 * @package Modules\Notifier\Providers
 */
class NotifierServiceProvider extends YasnaProvider
{
    use BrowserTrait;



    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerModelTraits();
        $this->registerArtisanCommands();
        $this->registerSidebar();
        $this->registerServices();
        $this->registerAPIEndpoints();
        $this->registerNotificationGroups();
        $this->registerBrowser();
    }



    /**
     * @inheritdoc
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $this->registerAutoMiddleware($router);
    }



    /**
     * register model traits
     */
    protected function registerModelTraits()
    {
        module('yasna')->service('traits')->add("UserNotifyTrait")->trait('Notifier:UserNotifyTrait')->to('User');

        $this->addModelTrait('UserRouteNotificationsTrait', 'User');
    }



    /**
     * register artisan commands
     */
    protected function registerArtisanCommands()
    {
        $this->addArtisan(MakeNotification::class);
    }



    /**
     * registers sidebar nested link under setting
     */
    protected function registerSidebar()
    {
        service('manage:settings_sidebar')
             ->add('notifier-setting')
             ->link('notifier')
             ->trans("notifier::general.notifiers")
             ->order(4)
             ->condition(function () {
                 return user()->isSuperadmin();
             })
        ;
    }



    /**
     * Registers Services
     */
    protected function registerServices()
    {
        $this
             ->module()
             ->register("notification_groups", "Groups of notifications to be used in the notifications' preview.")
        ;
    }



    /**
     * Adds the notification middleware to the `web` group.
     *
     * @param Router $router
     */
    protected function registerAutoMiddleware(Router $router)
    {
        $router->pushMiddlewareToGroup('web', NotificationMiddleware::class);
    }



    /**
     * Register all endpoints provided by this module
     */
    protected function registerAPIEndpoints()
    {
        if ($this->cannotUseModule("Endpoint")) {
            return;
        }

        endpoint()->register(GetNotificationsEndpointV1::class);
        endpoint()->register(MarkNotificationsAsReadEndpointV1::class);
        endpoint()->register(SaveEndpoint::class);
        endpoint()->register(DriverTrashEndpoint::class);
        endpoint()->register(DriverDestroyEndpoint::class);
        endpoint()->register(DriverRestoreEndpoint::class);
        endpoint()->register(DriverGetEndpoint::class);
        endpoint()->register(DriverConfigEndpoint::class);
        endpoint()->register(CreateFormEndpoint::class);
        endpoint()->register(ConfigFormEndpoint::class);
    }



    /**
     * register basic notification groups
     */
    private function registerNotificationGroups()
    {
        notifier()->database()->registerGroup("warnings", [
             "color" => "orange",
             "icon"  => "warning",
        ])
        ;

        notifier()->database()->registerGroup("dangers", [
             "color" => "red",
             "icon"  => "times",
        ])
        ;

        notifier()->database()->registerGroup("successes", [
             "color" => "success",
             "icon"  => "check",
        ])
        ;
    }
}
