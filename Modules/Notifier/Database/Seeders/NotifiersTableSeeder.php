<?php

namespace Modules\Notifier\Database\Seeders;

use Illuminate\Database\Seeder;

class NotifiersTableSeeder extends Seeder
{
    protected $data = [];



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->prepareDataForSmtp();
        $this->prepareDataForAsanak();
        $this->prepareDataForOneSignal();
        $this->prepareDataForAhra();
        $this->prepareDataForFaraz();

        yasna()::seed("notifiers", $this->data);
    }



    /**
     * Prepare $this->data for Asanak SMS driver
     *
     * @return void
     */
    protected function prepareDataForAsanak()
    {
        $this->data[] = [
             "channel"              => $channel = "sms",
             "driver"               => $driver = "asanak",
             "slug"                 => notifier()::generateSlug($channel, $driver),
             "title"                => notifier()::generateTitle($channel, $driver),
             "available_for_admins" => "0",
             "meta-data"            => [
                  "username" => env("ASANAK_SMS_USERNAME"),
                  "password" => env("ASANAK_SMS_PASSWORD"),
                  "source"   => env("ASANAK_SMS_SOURCE"),
                  "url"      => env("ASANAK_SMS_URL"),
             ],
        ];
    }



    /**
     * Prepare $this->data for SMTP driver
     *
     * @return void
     */
    protected function prepareDataForSmtp()
    {
        $this->data[] = [
             "channel"              => $channel = "mail",
             "driver"               => $driver = "smtp",
             "slug"                 => notifier()::generateSlug($channel, $driver),
             "title"                => notifier()::generateTitle($channel, $driver),
             "available_for_admins" => "0",
             "meta-data"            => [
                  "username"     => env("MAIL_USERNAME"),
                  "password"     => env("MAIL_PASSWORD"),
                  "host"         => "smtp.yasna.team",
                  "port"         => "587",
                  "from-address" => "no-reply@yasna.team",
                  "from-name"    => "Yasnateam Notifier",
                  "encryption"   => null,
                  //"sendmail"       => "",
                  //"markdown-theme" => "",
                  //"markdown-paths" => "",
             ],
        ];
    }



    /**
     * Prepares $this->data for OneSignal driver
     */
    protected function prepareDataForOneSignal()
    {
        $this->data[] = [
             "channel"              => $channel = "browser",
             "driver"               => $driver = "onesignal",
             "slug"                 => notifier()::generateSlug($channel, $driver),
             "title"                => notifier()::generateTitle($channel, $driver),
             "available_for_admins" => "0",
             "meta-data"            => [
                  "app_id"       => '',
                  "rest_api_key" => '',
             ],
        ];
    }



    /**
     * Prepare $this->data for ahra SMS driver
     *
     * @return void
     */
    protected function prepareDataForAhra()
    {
        $this->data[] = [
             "channel"              => $channel = "sms",
             "driver"               => $driver = "ahra",
             "slug"                 => notifier()::generateSlug($channel, $driver),
             "title"                => notifier()::generateTitle($channel, $driver),
             "available_for_admins" => "0",
             "meta-data"            => [
                  "domain"       => env("AHRA_SMS_DOMAIN"),
                  "user"         => env("AHRA_SMS_USER"),
                  "password"     => env("AHRA_SMS_PASSWORD"),
                  "from"         => env("AHRA_SMS_FROM"),
                  "receivetype"  => env("AHRA_SMS_RECEIVETYPE"),
                  "scheduledate" => env("AHRA_SMS_SCHEDULEDATE"),
                  "url"          => env("AHRA_SMS_URL"),
             ],
        ];
    }



    /**
     * Prepare $this->data for faraz SMS driver
     *
     * @return void
     */
    protected function prepareDataForFaraz()
    {
        $this->data[] = [
             "channel"              => $channel = "sms",
             "driver"               => $driver = "faraz",
             "slug"                 => notifier()::generateSlug($channel, $driver),
             "title"                => notifier()::generateTitle($channel, $driver),
             "available_for_admins" => "0",
             "meta-data"            => [
                  "op"    => "send",
                  "uname" => null,
                  "pass"  => null,
                  "from"  => null,
                  "time"  => null,
                  "url"   => "37.130.202.188/services.jspd",
             ],
        ];
    }
}
