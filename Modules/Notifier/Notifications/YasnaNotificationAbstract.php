<?php

namespace Modules\Notifier\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Notifier\Channels\BrowserChannel;
use Modules\Notifier\Services\Sms\Ahra;
use Modules\Notifier\Services\Sms\Asanak;
use Modules\Notifier\Services\Sms\Faraz;

class YasnaNotificationAbstract extends Notification
{
    use Queueable;



    /**
     * Yasna SMS channel for easier calls
     *
     * @return mixed
     */
    final public function yasnaSmsChannel()
    {
        return $this->returnClass();
    }



    /**
     * Yasna mail channel for easier calls
     *
     * @return string
     */
    final public function yasnaMailChannel()
    {
        return 'mail';
    }



    /**
     * Yasna database channel for easier calls
     *
     * @return string
     */
    final public function yasnaDatabaseChannel()
    {
        return 'database';
    }



    /**
     * Yasna push channel for easier calls
     *
     * @return string
     */
    final public function yasnaBrowserChannel()
    {
        return BrowserChannel::class;
    }



    /**
     * return sms driver
     *
     * @return string
     */
    protected function returnClass()
    {
        $class = notifier()::locateDefaultDriverOf('sms')['driver'];
        if ($class === 'ahra') {
            return Ahra::class;
        } elseif ($class === 'asanak') {
            return Asanak::class;
        } elseif ($class === 'faraz') {
            return Faraz::class;
        }
        return '';
    }
}
