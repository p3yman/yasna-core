<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 12:36 PM
 */

namespace Modules\Notifier\Messages;


use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use Modules\Notifier\Messages\Parts\BrowserButton;
use Modules\Notifier\Messages\Parts\BrowserWebButton;

class BrowserMessage implements Arrayable
{
    /** @var string */
    protected $body;

    /** @var string */
    protected $subject;

    /** @var string */
    protected $url;

    /** @var string */
    protected $icon;

    /** @var array */
    protected $data = [];

    /** @var array */
    protected $buttons = [];

    /** @var array */
    protected $web_buttons = [];

    /** @var array */
    protected $extra_parameters = [];



    /**
     * DataProvider constructor.
     *
     * @param string $body
     */
    public function __construct($body = '')
    {
        $this->body = $body;
    }



    /**
     * Set the message body.
     *
     * @param string $value
     *
     * @return $this
     */
    public function body($value)
    {
        $this->body = $value;

        return $this;
    }



    /**
     * Sets the message icon.
     *
     * @param string $value
     *
     * @return $this
     */
    public function icon($value)
    {
        $this->icon = $value;

        return $this;
    }



    /**
     * Sets the message subject.
     *
     * @param string $value
     *
     * @return $this
     */
    public function subject($value)
    {
        $this->subject = $value;

        return $this;
    }



    /**
     * Set the message url.
     *
     * @param string $value
     *
     * @return $this
     */
    public function url($value)
    {
        $this->url = $value;

        return $this;
    }



    /**
     * Set additional data.
     *
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }



    /**
     * Set additional parameters.
     *
     * @param string $key
     * @param string $value
     *
     * @return $this
     */
    public function setParameter($key, $value)
    {
        $this->extra_parameters[$key] = $value;

        return $this;
    }



    /**
     * Add a web button to the message.
     *
     * @param BrowserWebButton $button
     *
     * @return $this
     */
    public function webButton(BrowserWebButton $button)
    {
        $this->web_buttons[] = $button->toArray();

        return $this;
    }



    /**
     * Add a native button to the message.
     *
     * @param BrowserButton $button
     *
     * @return $this
     */
    public function button(BrowserButton $button)
    {
        $this->buttons[] = $button->toArray();

        return $this;
    }



    /**
     * Set an image to all possible attachment variables.
     *
     * @param string $image_url
     *
     * @return $this
     */
    public function setImageAttachments($image_url)
    {
        $this->extra_parameters['ios_attachments']['id1'] = $image_url;
        $this->extra_parameters['big_picture']            = $image_url;
        $this->extra_parameters['adm_big_picture']        = $image_url;
        $this->extra_parameters['chrome_big_picture']     = $image_url;

        return $this;
    }



    /**
     * Returns the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $message = [
             'contents'        => [
                  'en' => $this->body,
             ],
             'headings'        => [
                  'en' => $this->subject,
             ],
             'url'             => $this->url,
             'buttons'         => $this->buttons,
             'web_buttons'     => $this->web_buttons,
             'chrome_web_icon' => $this->icon,
             'chrome_icon'     => $this->icon,
             'adm_small_icon'  => $this->icon,
             'small_icon'      => $this->icon,
        ];

        foreach ($this->extra_parameters as $key => $value) {
            Arr::set($message, $key, $value);
        }

        foreach ($this->data as $data => $value) {
            Arr::set($message, 'data.' . $data, $value);
        }

        return $message;
    }

}
