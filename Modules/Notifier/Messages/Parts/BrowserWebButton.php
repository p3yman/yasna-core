<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/16/19
 * Time: 12:37 PM
 */

namespace Modules\Notifier\Messages\Parts;


use Illuminate\Contracts\Support\Arrayable;

class BrowserWebButton implements Arrayable
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $text;

    /** @var string */
    protected $icon;

    /** @var string */
    protected $url;



    /**
     * BrowserWebButton constructor.
     *
     * @param string $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }



    /**
     * Sets the button icon.
     *
     * @param string $value
     *
     * @return $this
     */
    public function icon($value)
    {
        $this->icon = $value;

        return $this;
    }



    /**
     * Sets the button text.
     *
     * @param string $value
     *
     * @return $this
     */
    public function text($value)
    {
        $this->text = $value;

        return $this;
    }



    /**
     * Sets the button url.
     *
     * @param string $value
     *
     * @return $this
     */
    public function url($value)
    {
        $this->url = $value;

        return $this;
    }



    /**
     * Returns the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
             'id'   => $this->id,
             'text' => $this->text,
             'icon' => $this->icon,
             'url'  => $this->url,
        ];
    }

}
