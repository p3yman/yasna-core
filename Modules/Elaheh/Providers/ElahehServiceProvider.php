<?php

namespace Modules\Elaheh\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class ElahehServiceProvider
 *
 * @package Modules\Elaheh\Providers
 */
class ElahehServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        // Call your methods here!
    }



    /**
     * get site sections
     *
     * @return array
     */
    public static function siteSections(): array
    {
        return trans("elaheh::sections");
    }



    /**
     * get the site section trans of the requested key
     *
     * @param string $key
     *
     * @return string
     */
    public static function siteSection(string $key): string
    {
        return trans("elaheh::sections.$key");
    }



    /**
     * get the text corresponding to the requested post slug from `small-things` posttype
     *
     * @param string $slug
     *
     * @return string
     */
    public static function smallThing(string $slug): string
    {
        return post($slug)->abstract ?? $slug;

    }
}
