<?php

namespace Modules\Elaheh\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ContactFormRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "name"    => "required",
             "email"   => "required|email",
             "subject" => "required",
             "text"    => "required",
             "mobile"  => "phone:mobile",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "mobile" => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    protected function getRedirectUrl()
    {
        $url = $this->redirector->getUrlGenerator();

        return $url->previous() . "#contact-section";
    }
}
