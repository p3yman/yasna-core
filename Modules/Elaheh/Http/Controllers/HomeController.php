<?php

namespace Modules\Elaheh\Http\Controllers;

use Modules\Elaheh\Http\Requests\ContactFormRequest;
use Modules\Yasna\Services\YasnaController;

class HomeController extends YasnaController
{
    protected $view_folder = "elaheh::home";



    /**
     * show site home page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->view("index");
    }



    /**
     * save contact submitted form
     *
     * @param ContactFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveContactForm(ContactFormRequest $request)
    {
        $saved = comment()->batchSaveBoolean([
             "post_id"      => post("homepage")->id,
             "posttype_id"  => posttype("homepage")->id,
             "guest_name"   => $request->name,
             "guest_mobile" => $request->mobile,
             "guest_email"  => $request->email,
             "subject"      => $request->subject,
             "text"         => $request->text,
        ]);

        if ($saved) {
            return $this->redirectWithSuccessMessage();
        }

        // @TODO: Redirect if failed.
    }



    /**
     * redirect back with a success message
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectWithSuccessMessage()
    {
        $url     = app('url')->previous() . '#contact-section';
        $message = trans_safe("elaheh::contact.success");

        session()->put('success', $message);

        return redirect()->to($url);
    }
}
