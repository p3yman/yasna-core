<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => 'Modules\Elaheh\Http\Controllers',
], function () {
    Route::get('/', 'HomeController@index');
    Route::post('contact', 'HomeController@saveContactForm')->name("contact");
});
