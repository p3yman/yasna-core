<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{ get_setting("site_title") }}</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                @foreach(Elaheh::siteSections() as $section => $trans)
                    <li><a href="#{{$section}}-section">{{$trans}}</a></li>
                @endforeach
            </ul>
        </div>

    </div>
</div>
