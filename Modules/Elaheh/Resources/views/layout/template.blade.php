<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="fa"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="fa"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="fa"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="fa">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="{{ get_setting("site_description") }}">
    <meta name="author" content="">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>{{ get_setting("site_title") }}</title>

    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    {!! Html::style(Module::asset("Elaheh:css/bootstrap.css")) !!}
    {!! Html::style(Module::asset("Elaheh:css/bootstrap.rtl.min.css")) !!}
    {!! Html::style(Module::asset("Elaheh:fonts/sahel/stylesheet.css")) !!}
    {!! Html::style(Module::asset("Elaheh:css/font-awesome.min.css")) !!}
    {!! Html::style(Module::asset("Elaheh:css/style.css")) !!}

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

@yield('content')

{!! Html::script(Module::asset('Elaheh:js/jquery.js')) !!}
{!! Html::script(Module::asset('Elaheh:js/bootstrap.min.js')) !!}
{!! Html::script(Module::asset('Elaheh:js/scrollReveal.js')) !!}
{!! Html::script(Module::asset('Elaheh:js/custom.js')) !!}

</body>
</html>
