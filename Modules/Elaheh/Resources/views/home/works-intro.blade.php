<div class="row main-top-margin text-center" data-scrollreveal="enter top and move 100px, wait 0.3s">
    <div class="col-md-8 col-md-offset-2 ">
        <h1>{{ Elaheh::siteSection("works") }}</h1>
        <h4>{{ Elaheh::smallThing("what-i-do") }}</h4>
    </div>
</div>
