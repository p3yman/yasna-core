<div class="row main-low-margin">
    <div class="col-md-10  col-md-offset-1 ">
        @if(($loop->iteration)%2 > 0)
            @include("elaheh::home.works-portfolio-image")
        @endif

        <div class="col-md-8 " data-scrollreveal="enter right and move 100px, wait 0.4s">
            <h4><strong class="color-red">{{$post->title}}</strong></h4>
            <p style="margin-bottom: 40px;text-align: justify">
                {!! $post->text !!}
            </p>
            @if($post->spreadMeta()->button_label)
                <a href="{{$post->button_link}}" class="btn btn-success">{{ $post->button_label }}</a>
            @endif
        </div>

        @if(($loop->iteration)%2 == 0)
            @include("elaheh::home.works-portfolio-image")
        @endif
    </div>

</div>
