@php
    $post = post("about");
@endphp
<div id="about-section" >
    <div class="container" >
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 " data-scrollreveal="enter top and move 100px, wait 0.3s">
                <h1>{{ Elaheh::siteSection("about") }}</h1>
                <h4>
                    {{ Elaheh::smallThing("who-i-am") }}
                </h4>
            </div>
        </div>
        <!-- ./ Main Heading-->
        <div class="row main-low-margin text-center">
            <div class="col-md-3 " data-scrollreveal="enter left and move 100px, wait 0.4s">
                {!!
                    fileManager()
                        ->file($post->featured_image)
                        ->elementWidth('180')
                        ->elementHeight('180')
                        ->elementClass("img-circle")
                        ->show()
                !!}
                <h4><strong>{{ get_setting("site_title") }}</strong> </h4>
                <p>
                    {{ $post->abstract }}
                </p>
                <p>
                    @include("elaheh::home.about-socials")
                </p>

            </div>

            <div class="col-md-7 col-sm-7 col-md-offset-1  text-justify" data-scrollreveal="enter right and move 100px, wait 0.4s">
                <h3>{{ $post->title2 }}</h3>
                <p>
                    {!! $post->text !!}
                </p>
            </div>
        </div>
        <!-- ./ Row Content-->
    </div>
</div>
