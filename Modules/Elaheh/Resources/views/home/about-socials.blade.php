@if($link = get_setting("email")[0])
    <a href="mailto:{{$link}}">
        <img src="{{ Module::asset("Elaheh:img/social-mail.png") }}" style="width: 32px" />
    </a>
@endif

@if($link = get_setting("linkedin_account"))
    <a href="{{$link}}">
        <img src="{{ Module::asset("Elaheh:img/social-linkedin.png") }}" style="width: 32px" />
    </a>
@endif
