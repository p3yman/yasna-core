@php($post = post("contact-details"))

<div class="col-md-6  " data-scrollreveal="enter left and move 100px, wait 0.4s">
    <h3>{{ $post->title }}</h3>
    <hr />
    <p>
        {!! nl2br($post->abstract) !!}
    </p>

</div>
