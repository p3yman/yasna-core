@php($post = post("homepage"))

<div id="header-section" style="background: url({{ fileManager()->file($post->featured_image)->getUrl() }}) no-repeat center top">
    <div class="container">
        <div class="row centered">
            <div class="col-md-8 col-md-offset-2">
                <h1><strong>{{ $post->title }}</strong>

                </h1>
                <br/> <br/> <br/>
                <h2>{{ $post->title2 }}</h2>
                <br/>
                <a href="#about-section"> <i class="fa fa-angle-double-down fa-5x down-icon"></i> </a>
            </div>
        </div>

    </div>
</div>
