<div class="col-md-6  " data-scrollreveal="enter right and move 100px, wait 0.4s">
    <h3>{{ trans_safe("elaheh::contact.title") }}</h3>
    <hr/>

    {!! Form::open([
        'url' => route("contact") ,
        'method' => 'post' ,
    ]) !!}

    <div class="row">
        <div class="col-md-6 ">
            <div class="form-group">
                <input type="text" value="{{old('name')}}" name="name" class="form-control" required="required"
                       placeholder="{{ trans_safe("elaheh::contact.name") }}">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" name="email" value="{{old('email')}}" class="form-control" required="required"
                       placeholder="{{ trans_safe("elaheh::contact.email") }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 ">
            <div class="form-group">
                <input type="text" name="subject" value="{{old('subject')}}" class="form-control" required="required"
                       placeholder="{{ trans_safe("elaheh::contact.subject") }}">
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="form-group">
                <input type="text" name="mobile" value="{{old('mobile')}}" class="form-control"
                       placeholder="{{ trans_safe("elaheh::contact.mobile") }}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="form-group">
                <textarea name="text" id="message" required="required" class="form-control"
                          rows="3" placeholder="{{ trans_safe("elaheh::contact.text") }}">{{old('text')}}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">{{ trans_safe("elaheh::contact.submit") }}</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if ($success = session()->pull("success" , null))
        <div class="alert alert-success">
            {{ $success }}
        </div>
    @endif
</div>
