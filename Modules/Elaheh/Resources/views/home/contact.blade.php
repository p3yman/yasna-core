<div id="contact-section">
    <div class="container" >
        <div class="row main-top-margin text-center">
            <div class="col-md-8 col-md-offset-2 " data-scrollreveal="enter top and move 100px, wait 0.3s">
                <h1>{{ Elaheh::siteSection("contact") }}</h1>
                <h4>{{ Elaheh::smallThing("contact-me") }}</h4>
            </div>
        </div>
        <!-- ./ Main Heading-->
        <div class="row">
            <div class="col-md-12  col-sm-12 ">
                @include("elaheh::home.contact-details")
                @include("elaheh::home.contact-form")
            </div>
        </div>
        <!-- ./ Row Content-->
    </div>
</div>
