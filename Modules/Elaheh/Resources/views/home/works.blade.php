<div id="works-section" style="padding-bottom: 50px">
    <div class="container" >

        @include("elaheh::home.works-intro")

        @foreach( posttype("portfolio")->posts()->get() as $post)
            <hr />
            @include("elaheh::home.works-portfolio")
        @endforeach
    </div>
</div>
