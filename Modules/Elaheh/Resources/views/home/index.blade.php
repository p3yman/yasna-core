@extends("elaheh::layout.template")

@section("content")
    @include("elaheh::layout.navbar")

    @include("elaheh::home.header")
    @include("elaheh::home.about")
    @include("elaheh::home.works")
    @include("elaheh::home.contact")
@endsection
