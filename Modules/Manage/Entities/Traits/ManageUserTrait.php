<?php

namespace Modules\Manage\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;

trait ManageUserTrait
{
    /**
     * @param null $default
     *
     * @return string
     */
    public function adminTheme($default = null)
    {
        $default_theme = (get_setting('manage-default-theme') ?: $default);
        return $this->preference('admin_theme', $default_theme);
    }



    /**
     * Returns a limited version of the `notifications()` relationship.
     *
     * @param int $limit
     *
     * @return MorphMany
     */
    public function lastNotifications(int $limit = 10)
    {
        return $this->notifications()->limit($limit);
    }



    /**
     * get the admin locale selected by the user, or the default one if not selected
     *
     * @return string
     */
    public function getAdminLocale()
    {
        $locales = manage()::availableLocales();
        $default = $locales[0];
        if (!$default) {
            $default = "fa";
        }

        $result = $this->preference('admin_locale', $default);

        if(!in_array($result,$locales)) {
            $result = $default;
        }

        return $result;
    }



    /**
     * set the admin locale to the user's preferences
     *
     * @param $locale
     *
     * @return bool
     */
    public function setAdminLocale($locale)
    {
        if(!in_array($locale,manage()::availableLocales())) {
            return false;
        }

        return $this->setPreference("admin_locale", $locale);
    }
}
