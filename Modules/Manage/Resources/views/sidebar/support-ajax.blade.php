<li class="sidebar-subnav-header">{{ trans("manage::support.title") }}</li>

@if (yasnaSupport()->isAvailable())

	@php $submenus = yasnaSupport()->sidebarSubmenus() @endphp

	@if (!empty($submenus))

		@foreach($submenus as $sub_menu)

			<li>
				<a href="{{ url ("manage/". $sub_menu['link']) }}">
					@if(isset($badge))
						<div class="pull-right label label-{{ $badge_color or "success" }}">{{ pd($badge_count) }}</div>
					@endif
					<i class="fa fa-{{ $sub_menu['icon'] or 'dot-circle-o' }} fa-fw" style="width: 20px"></i>
					&nbsp;<span>{{ $sub_menu['caption'] }}&nbsp;</span>
				</a>
			</li>

		@endforeach

		@php return @endphp
	@endif

@endif


<li>
	<a style="color: #aaaaaa;">
		<i class="fa fa-exclamation-triangle fa-fw" style="width: 20px"></i>
		&nbsp;<span>{{ $__module->getTrans('support.unavailable') }}&nbsp;</span>
	</a>
</li>
