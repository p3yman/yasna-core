@include("manage::layouts.sidebar-link" , [
	'icon' => "life-ring",
	'caption' => trans("manage::support.title") ,
	'link' => "support" ,
	"sub_menus" => [
		[
			  'link'    => '#',
			  'caption' => ' ',
		]
	],
	'permission' => 'super',
	'id' => 'support-sidebar'
])

@section('html_header')
	<script>
        $(document).ready(function () {
            $('#support-sidebar').click(function () {
                if (supportToBeLoaded()) {
                    $.ajax({
                        url       : "{{ route('manage.support.sidebar') }}",
                        beforeSend: function () {
                            setLoadingSupportContent();
                        },
                        success   : function (rs) {
                            let sidebar_item = $('#support-sidebar');
                            sidebar_item.find('ul').html(rs);
                            setReadySupportContent();
                            sidebar_item.addClass('js-loaded-content');
                        }
                    });
                }
            });
        });


        function supportToBeLoaded() {
            let sidebar_item = $('#support-sidebar');

            return (
                !sidebar_item.hasClass('js-loaded-content') &&
                !sidebar_item.hasClass('whirl') &&
                (sidebar_item.children('a[data-toggle="collapse"]').attr('aria-expanded') !== 'true')
            );
        }


        function setLoadingSupportContent() {
            let ul = $('#support-sidebar').find('ul');
            ul.addClass('whirl');
            ul.addClass('line');
        }


        function setReadySupportContent() {
            let ul = $('#support-sidebar').find('ul');
            ul.removeClass('whirl');
            ul.removeClass('line');
        }
	</script>
@endsection
