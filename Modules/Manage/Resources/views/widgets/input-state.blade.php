{!!
widget("combo")
	->name($name)
	->options(model('state')->combo())
	->searchable()
	->value($value)
	->label($label)
	->inForm()
!!}

{!!
widget("note")
	->label("tr:yasna::states.combo_hint")
	->condition($name != "default_value")
!!}
