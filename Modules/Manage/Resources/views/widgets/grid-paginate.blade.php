<div class="paginate">
    @if(method_exists($models , 'render'))
		@php $models->appends(request()->all()); @endphp
        {!! $models->render() !!}
    @endif
</div>
