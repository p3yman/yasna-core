<?php
$lang = getLocale();
?>

@if($lang === 'fa')
	@include('manage::widgets.grid-date-fa')
@elseif($lang ==='en')
	@include('manage::widgets.grid-date-en')
@endif
