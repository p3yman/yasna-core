<div class="p">
    <h4 class="text-muted text-thin font-yekan">{{ trans_safe("manage::settings.locale.title") }}</h4>

    {!! widget("form-open")->target(route("manage-save-admin-locale"))->class("js") !!}

    {!!
    widget("combo")
    	->name("admin_locale")
    	->options(manage()::availableLocalesCombo())
    	->onChange("$('#btnAdminLocaleSave').slideDown('fast')")
    	->value(user()->getAdminLocale())
    !!}

    {!!
    widget("button")
    	->label("tr:manage::forms.button.save")
    	->class("w100")
    	->shape("success")
    	->type("submit")
    	->hidden()
    	->id("btnAdminLocaleSave")
    !!}

    {!! widget("feed") !!}
    {!! widget("form-close") !!}
</div>
