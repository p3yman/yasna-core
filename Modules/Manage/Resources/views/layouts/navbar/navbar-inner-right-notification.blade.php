@php
	$notifications_count = user()->unreadNotifications()->count();
	$interval = get_setting('notifications_intervals');
@endphp

<a href="#" data-interval="{{ $interval }}">
	@if ($notifications_count)
		<em class="icon-bell"></em>
		<div class="label label-danger">{{ pd($notifications_count) }}</div>
	@else
		<em class="fa fa-bell-slash-o"></em>
	@endif
</a>
