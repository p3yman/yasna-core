<?php

namespace Modules\Manage\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\View;
use Modules\Yasna\Console\YasnaMakerCommand;
use Nwidart\Modules\Support\Stub;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class MakeModal extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-modal {full_blade_address}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes an empty Manage modal blade.';

    /**
     * keep the address requested by the user
     *
     * @var null
     */
    protected $address;

    /**
     * Array of folders name
     *
     * @var array
     */
    protected $folders_array;

    /**
     * The modal blade name
     *
     * @var string
     */
    protected $blade_name;

    /**
     * The address of module view folder
     *
     * @var string
     */
    protected $base_folder;

    /**
     * keep the stub file name
     *
     * @var string
     */
    protected $stub_name = "modal";



    /**
     * validate the address format
     *
     * @return bool
     */
    protected function purifier()
    {

        $this->receiveAddress();
        $this->parseAddress();
        if (!str_contains($this->address, '::')) {
            $this->error("Invalid Address Format. Use Module::folder.blade pattern.");
            die();
        }

        if (View::exists($this->address)) {
            $this->error("The blade already exists!");
            die();
        }

        if (module()->isNotDefined($this->module_name)) {
            $this->error("Module '$this->module_name' is not defined.");
            die();
        }

        return true;
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['blade-address', InputArgument::REQUIRED, 'Manage::example.modal'],
        ];
    }



    /**
     * parse the entered address by user
     *
     * @return void
     */
    public function parseAddress()
    {
        $this->module_name       = studly_case(str_before($this->address, "::"));
        $folder_and_blade_string = str_after($this->address, "::");
        $folder_and_blade_array  = explode(".", $folder_and_blade_string);
        $this->blade_name        = array_last($folder_and_blade_array);
        $this->class_name        = $this->blade_name . '.blade';
        $this->folders_array     = array_remove($folder_and_blade_array, $this->blade_name);
    }



    /**
     * Receive command argument as the blade address
     */
    public function receiveAddress()
    {
        $this->address = $this->argument('blade-address');
    }



    /**
     * Make folders of folder array
     */
    protected function makeFolders()
    {
        foreach ($this->folders_array as $folder) {
            $this->base_folder .= DIRECTORY_SEPARATOR . $folder;
            $this->makeFolder();
        }
    }



    /**
     * get the target folder path
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return module($this->module_name)
                  ->getPath("Resources" . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR)
             . implode(DIRECTORY_SEPARATOR, $this->folders_array);
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        return $this->class_name;
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [];
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "modal.stub";
    }
}
