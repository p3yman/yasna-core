<?php

namespace Modules\Manage\Http\Controllers;

use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class PreferencesController extends YasnaController
{
    /**
     * save admin locale in the online-user's preferences
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function saveAdminLocale(SimpleYasnaRequest $request)
    {
        $saved = user()->setAdminLocale($request->admin_locale);

        return $this->jsonAjaxSaveFeedback($saved , [
             "success_refresh" => true,
        ]);
    }
}
