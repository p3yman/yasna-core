<?php

Route::group([
    'middleware' => 'web', 
    'namespace'  => module('Manage')->getControllersNamespace(), 
    'prefix'     => 'manage'
], function () {
    Route::post('save-admin-locale' , 'PreferencesController@saveAdminLocale')->name("manage-save-admin-locale");
});
