<?php

namespace Modules\Manage\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManageLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $uri   = app()->request->getRequestUri();
        $array = explode_not_empty("/", $uri);

        if (isset($array[0]) and strtolower($array[0]) == "manage") {
            app()->setLocale(user()->getAdminLocale());
        }

        return $next($request);
    }
}
