<?php

namespace Modules\Posts\Providers;

use Faker\Test\Provider\Collection;
use Modules\Posts\Http\Endpoints\V1\ListEndpoint;
use Modules\Posts\Http\Endpoints\V1\SingleEndpoint;
use Modules\Posts\Http\Endpoints\V1\TypesEndpoint;
use Modules\Posts\Mappers\PostsMapper;
use Modules\Posts\Schedules\PostScheduledPublished;
use Modules\Yasna\Services\YasnaProvider;
use Modules\Posts\Entities\Input;

class PostsServiceProvider extends YasnaProvider
{


    /**
     * Provider Index
     */
    public function index()
    {
        $this->registerAssets();
        $this->registerServices();
        $this->registerUpstream();
        $this->registerDownstream();
        $this->registerSidebar();
        $this->registerPostHandlers();
        $this->registerPermitTabs();
        $this->registerAliases();
        $this->registerWidgets();
        $this->registerCreateHandlers();
        $this->registerNotificationHandlers();
        $this->registerRoleSampleModules();
        $this->registerModelTraits();
        $this->registerCronJobs();
        $this->registerEndpoints();
        $this->registerMappers();
    }



    /**
     * register cron jobs
     */
    public function registerCronJobs()
    {
        $this->addSchedule(PostScheduledPublished::class);
    }



    /**
     * Model Traits
     */
    public function registerModelTraits()
    {
        module('yasna')
             ->service('traits')
             ->add('events-to-user')
             ->trait('Posts:UserEventTrait')
             ->to('User')
        ;

        module('yasna')
             ->service('traits')
             ->add('user-relations')
             ->trait('Posts:UserPostTrait')
             ->to("User")
        ;

        $this->addModelTrait("PosttypeResourcesTrait", "Posttype");
        $this->addModelTrait("PosttypeResourcesTrait", "Posttype");
    }



    /**
     * Manage Dashboard Widgets
     */
    public function registerWidgets()
    {
        module('manage')
             ->service('widgets_handler')
             ->add('posts')
             ->method('posts:HandleController@widgets')
        ;
    }



    /**
     * Module string, to be used as sample for role definitions.
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('posts')
             ->value('browse ,  create ,  edit ,  publish ,  report ,  delete ,  bin ,  settings')
        ;
    }



    /**
     * Manage Notification Handler
     */
    public function registerNotificationHandlers()
    {
        module('manage')
             ->service('nav_notification_handler')
             ->add('pending-posts')
             ->method('posts:HandleController@pendingNotifications')
        ;
    }



    /**
     * Manage Create Handler
     */
    public function registerCreateHandlers()
    {
        module('manage')
             ->service('nav_create_handler')
             ->add('posts')
             ->method('posts:HandleController@createButtons')
        ;
    }



    /**
     * Services
     */
    public function registerServices()
    {
        module("posts")
             ->register("mass_action_handlers",
                  "Methods, responsible to generate 'Mass Actions' button, above posts browse view.")
             ->register("postak_mass_action_handlers",
                  "Methods, responsible to generate 'Mass Actions' button, above postaks browse view.")
             ->register("mass_actions", "Array of the 'Mass Actions' button, above posts browse view. (On Demand).")
             ->register("postak_mass_actions",
                  "Array of the 'Mass Actions' button, above postaks browse view. (On Demand).")
             ->register("save_gates", "Methods, responsible to filter submitted post data, before the storage.")
             ->register('after_save', "Methods, to be executed after the post has been saved.")
             ->register('row_action_handlers',
                  "Methods, responsible to generate the 'Actions' button, on each row of posts browse view.")
             ->register('row_actions', "Array of the 'Actions' button, on each row of posts browse view (On Demand).")
             ->register('browse_headings_handlers', "Methods, responsible to generate the posts browse columns.")
             ->register('browse_headings',
                  "Array of the blades, responsible to show the content of each browse column (On Demand).")
             ->register('editor_handlers', "Responsible methods to be loaded in the post editor.")
             ->register('editor_main', 'Builds the main area of the editor (On Demand).')
             ->register('editor_side', 'Builds the side area of the editor (On Demand).')
             //->register('editor_modal', 'Builds the modal area of the editor (On Demand).')
             ->register('editor_misc', 'Builds the MISC panel, on the side of the editor (On Demand).')
             ->register('browse_head_blade', 'Blades to be appended to the head in all browse pages.')
             ->register('browse_filters_handlers', "Responsible methods to be loaded in post's list page")
             ->register('browse_filters', "Add filters to the head in all browse pages's tabs")
        ;
    }



    /**
     * Upstream Settings
     */
    public function registerUpstream()
    {
        service('manage:upstream_tabs')
             ->add('posttypes')
             ->link('posttypes')
             ->trans("posts::types.title")
             ->method('posts:PosttypeUpstreamController@index')
             ->order(26)
        ;
        service('manage:upstream_tabs')
             ->add('features')
             ->link('features')
             ->trans("posts::features.title")
             ->method('posts:UpstreamController@featureIndex')
             ->order(27)
        ;
        service('manage:upstream_tabs')
             ->add('inputs')
             ->link('inputs')
             ->trans("posts::inputs.title")
             ->method('posts:UpstreamController@inputIndex')
             ->order(28)
        ;
    }



    /**
     * Downstream Settings
     */
    public function registerDownstream()
    {
        module('manage')
             ->service('downstream')
             ->add('posttypes')
             ->link('posttypes')
             ->trans("posts::types.title")
             ->method('posts:DownstreamController@posttypeIndex')
             ->order(11)
        ;
    }



    /**
     * Sidebar Menu
     */
    public function registerSidebar()
    {
        service('manage:sidebar')->add('slides')->blade('posts::layouts.sidebar_slideshow')->order(32);
        service('manage:sidebar')->add('posts')->blade('posts::layouts.sidebar')->order(33);
        service('manage:sidebar')->add('minimal_posts')->blade('posts::layouts.sidebar_minimal_post')->order(34);
    }



    /**
     * Post Handlers
     */
    public function registerPostHandlers()
    {
        service('posts:mass_action_handlers')->add()->method('posts:HandleController@browseMassActions');
        service('posts:postak_mass_action_handlers')->add()->method('posts:HandleController@browsePostakMassActions');
        service('posts:browse_headings_handlers')->add()->method('posts:HandleController@browseColumns');
        service('posts:row_action_handlers')->add()->method('posts:HandleController@browseRowActions');
        service('posts:editor_handlers')->add()->method('posts:HandleController@editor');
        service('posts:save_gates')->add()->method('posts:HandleController@saveGate');
        service('posts:browse_filters_handlers')->add()->method('posts:HandleController@browseFilters');
    }



    /**
     * Permit Tabs in User Privileges
     */
    public function registerPermitTabs()
    {
        module("users")
             ->service("permit_tabs")
             ->add('posts')
             ->trans("posts::general.content_management")
             ->blade("posts::users.permits")
             ->order(21)
        ;
    }



    /**
     * Manage Assets
     */
    public function registerAssets()
    {
        module('yasna')
             ->service('traits')
             ->add('DomainPosts')
             ->trait('Yasna:DomainPostsTrait')
             ->to('Domain')
        ;
    }



    /**
     * Aliases
     */
    protected function registerAliases()
    {
        $this->addAlias('PostsModule', PostsServiceProvider::class);
    }



    /**
     * Registers endpoints only if the endpoint module is active.
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(SingleEndpoint::class);
        endpoint()->register(TypesEndpoint::class);
        endpoint()->register(ListEndpoint::class);

    }



    /**
     * @return mixed
     */
    public static function allInputs()
    {
        return Input::orderBy('order')->orderBy('title')->get();
    }



    /**
     * get Posttype collection for the sidebar in grouped mode
     *
     * @return Collection
     */
    public static function sidebarPosttypesGrouped()
    {
        return posttypes()
             ->having("manage_sidebar")
             ->groupBy('header_title')
             ->sortBy('order')
             ->get()
             ;
    }



    /**
     * get posttypes suitable for the sidebar
     *
     * @param bool $header_title
     *
     * @return Collection
     */
    public static function sidebarPosttypes($header_title = false)
    {
        $collection = posttypes()->having("manage_sidebar");

        if ($header_title) {
            $collection = $collection->where("header_title", $header_title);
        }

        return $collection->sortBy('order')->get();
    }



    /**
     * Return all minimal post under the name of a unified group
     *
     * @return Collection
     */
    public static function sidebarMinimalPosttypes()
    {
        return posttypes()->having("minimal_post")->sortBy('order')->get();
    }



    /**
     * register site mappers.
     *
     * @return void
     */
    private function registerMappers()
    {
        if ($this->cannotUseModule("SiteMap")) {
            return;
        }

        mapper()::registerMapper(PostsMapper::class);
    }
}
