<?php namespace Modules\Posts\Events;

use Modules\Yasna\Services\YasnaEvent;

class PostDeleted extends YasnaEvent
{
    public $model_name = "Post";
}
