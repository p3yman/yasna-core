<?php namespace Modules\Posts\Events;

use Modules\Yasna\Services\YasnaEvent;

class PostUnpublished extends YasnaEvent
{
    public $model_name = "Post";
}
