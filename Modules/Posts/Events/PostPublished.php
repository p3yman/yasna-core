<?php namespace Modules\Posts\Events;

use Modules\Yasna\Services\YasnaEvent;

class PostPublished extends YasnaEvent
{
    public $model_name = "Post";
}
