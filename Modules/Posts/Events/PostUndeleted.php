<?php namespace Modules\Posts\Events;

use Modules\Yasna\Services\YasnaEvent;

class PostUndeleted extends YasnaEvent
{
    public $model_name = "Post";
}
