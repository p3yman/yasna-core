<?php

namespace Modules\Posts\Events;

use Illuminate\Queue\SerializesModels;

class PosttypeUpdated
{
    use SerializesModels;
    public $posttype ;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($posttype)
    {
        if (is_numeric($posttype) or is_string($posttype)) {
            $posttype = model('posttype', $posttype) ;
        }
        $this->posttype = $posttype;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
