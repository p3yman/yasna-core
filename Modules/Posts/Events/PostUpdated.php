<?php namespace Modules\Posts\Events;

use Modules\Yasna\Services\YasnaEvent;

class PostUpdated extends YasnaEvent
{
    public $model_name = "Post";
}
