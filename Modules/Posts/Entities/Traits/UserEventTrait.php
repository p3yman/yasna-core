<?php namespace Modules\Posts\Entities\Traits;

trait UserEventTrait
{
    /**
     * Standard Laravel Relation Method
     *
     * @return mixed
     */
    public function event()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Post", "from_event_id");
    }
}
