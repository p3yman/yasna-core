<?php

namespace Modules\Posts\Entities\Traits;

trait PosttypeResourcesTrait
{
    /**
     * boot PosttypeResourcesTrait
     *
     * @return void
     */
    public static function bootPosttypeResourcesTrait()
    {
        static::addDirectResources([
             "title",
             "slug",
             "icon",
             "template",
             "header_title",
             "locales",
        ]);
    }
}
