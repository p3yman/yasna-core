<?php

namespace Modules\Posts\Entities\Traits;

use Illuminate\Database\Eloquent\Collection;

trait PostAuthorTrait
{

    /**
     * get a collection of all creators of the posts
     *
     * @return Collection
     */
    public static function getAllAuthors()
    {
        $field    = static::getAuthorField();
        $creators = post()->groupBy($field)->pluck($field)->toArray();

        return user()->whereIn("id", $creators)->orderBy('name_last')->get();
    }



    /**
     * get the name of the field holding author id
     *
     * @return string
     */
    public static function getAuthorField()
    {
        return "created_by";
    }
}
