<?php namespace Modules\Posts\Entities\Traits;

trait PosttypeLocalesTrait
{
    public static $site_locale      = false;
    private $locale_meta_name = "locale_titles";



    /**
     * @return array: meta fields, related to the multi-locale system
     */
    public function titlesMetaFields()
    {
        return [
             $this->locale_meta_name,
        ];
    }



    /**
     * @return string
     */
    private function siteLocale($bypass_cached_data = false)
    {
        if (self::$site_locale and !$bypass_cached_data) {
            return self::$site_locale;
        }

        self::$site_locale = $this->getSiteLocaleFromSettings();

        return self::$site_locale;
    }



    /**
     * @return string;
     */
    private function getSiteLocaleFromSettings()
    {
        $setting_value = get_setting('site_locales');

        if (!$setting_value or !is_array($setting_value)) {
            return 'fa';
        }

        return $setting_value[0];
    }



    public function test($locale)
    {
        self::$site_locale = $locale;
    }



    /**
     * Provides a safe default-language array, for the times no language is set.
     *
     * @return array
     */
    private function siteDefaultLocalesArray()
    {
        return (array)$this->siteLocale();
    }



    /**
     * @return array
     */
    public function getLocalesArrayAttribute()
    {
        return $this->localesArray();
    }



    /**
     * @return array
     */
    public function localesArray()
    {
        $array = explode_not_empty(',', $this->locales);
        if (sizeof($array)) {
            return $array;
        }

        return $this->siteDefaultLocalesArray();
    }



    /**
     * @return int
     */
    public function localesCount()
    {
        return count($this->localesArray());
    }



    /**
     * @return int
     */
    public function getLocalesCountAttribute()
    {
        return $this->localesCount();
    }



    /**
     * @return mixed
     */
    public function defaultLocale()
    {
        return $this->localesArray()[0];
    }



    /**
     * @return mixed
     */
    public function getDefaultLocaleAttribute()
    {
        return $this->defaultLocale();
    }



    /**
     * @param bool $locale
     *
     * @return string
     */
    public function titleIn($locale = false)
    {
        if (!$locale) {
            $locale = $this->defaultLocale();
        }

        if ($locale == $this->defaultLocale()) {
            return $this->title;
        }

        $titles = $this->getMeta($this->locale_meta_name);

        return ($titles["title-$locale"] ?? null);
    }



    /**
     * @param bool $locale
     *
     * @return string
     */
    public function singularTitleIn($locale = false)
    {
        if (!$locale) {
            $locale = $this->defaultLocale();
        }

        if ($locale == $this->defaultLocale()) {
            return $this->singular_title;
        }

        $titles = $this->getMeta($this->locale_meta_name);

        return ($titles["singular_title-$locale"] ?? null);
    }



    public function localesCombo()
    {
        $array = [];

        foreach ($this->localesArray() as $locale) {
            $array[] = [
                 $locale,
                 trans_safe("manage::forms.lang.$locale"),
            ];
        }

        return $array;
    }
}
