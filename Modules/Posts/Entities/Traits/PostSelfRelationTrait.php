<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/28/18
 * Time: 5:21 PM
 */

namespace Modules\Posts\Entities\Traits;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait PostSelfRelationTrait
{
    /**
     * Returns a builder to select posts related to this post.
     *
     * @return BelongsToMany
     */
    public function relateds()
    {
        return $this->belongsToMany(static::class, 'post_relations', 'post_id', 'related_post_id');
    }



    /**
     * Selects and orders the posts related to this post based on the given arguments.
     *
     * @param int  $limit
     * @param bool $random
     *
     * @return Collection
     */
    public function limitedRelateds(int $limit = 10, bool $random = false)
    {
        $builder = $this->relateds();

        return $this->limitPostRelation($builder, $limit, $random);
    }



    /**
     * Returns an array of ids of the posts related to this post.
     *
     * @return array
     */
    public function relatedsIds()
    {
        return $this
             ->relateds()
             ->select('post_id as id')
             ->get()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Returns an array of hashids of the posts related to this post.
     *
     * @return array
     */
    public function relatedsHashids()
    {
        return $this
             ->relateds()
             ->select('post_id as id')
             ->get()
             ->pluck('hashid')
             ->toArray()
             ;
    }



    /**
     * Returns a builder to select posts which this post is related to them.
     *
     * @return BelongsToMany
     */
    public function relatedTo()
    {
        return $this->belongsToMany(static::class, 'post_relations', 'related_post_id', 'post_id');
    }



    /**
     * Accessor for the `relatedTo()` Relation
     *
     * @return Collection
     */
    public function getRelatedToAttribute()
    {
        return $this->getRelationValue('relatedTo');
    }



    /**
     * Selects and orders the posts which this post is related to them.
     *
     * @param int  $limit
     * @param bool $random
     *
     * @return Collection
     */
    public function limitedRelatedTo(int $limit = 10, bool $random = false)
    {
        $builder = $this->relatedTo();

        return $this->limitPostRelation($builder, $limit, $random);
    }



    /**
     * Returns an array of ids of the posts which this post is related to them.
     *
     * @return array
     */
    public function relatedToIds()
    {
        return $this
             ->relatedTo()
             ->select('post_id as id')
             ->get()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Returns an array of hashids of the posts which this post is related to them.
     *
     * @return array
     */
    public function relatedToHashids()
    {
        return $this
             ->relatedTo()
             ->select('post_id as id')
             ->get()
             ->pluck('hashid')
             ->toArray()
             ;
    }



    /**
     * Selects and orders the given relation based on the given arguments.
     *
     * @param BelongsToMany $builder
     * @param int           $limit
     * @param bool          $random
     *
     * @return Collection
     */
    protected function limitPostRelation(BelongsToMany $builder, int $limit, bool $random)
    {
        $builder->limit($limit);

        if ($random) {
            $builder->inRandomOrder();
        } else {
            $builder->orderByDesc('post_relations.created_at');
        }

        return $builder->get();
    }



    /**
     * Modifies the elector to contain posts related to the specified post id
     *
     * @param int|string|array $post_id
     */
    public function electorRelatedTo($post_id)
    {
        $ids = (array) $post_id;
        $this->elector()->whereIn('id', function ($query) use($ids) {
            $query
                 ->select('posts2.id')
                 ->from('posts as posts2')
                 ->join('post_relations', function ($join_query) use ($ids) {
                     $join_query->whereColumn('post_relations.related_post_id', 'posts2.id');
                     $join_query->whereIn('post_relations.post_id', $ids);
                })
            ;
        })
        ;
    }
}
