<?php namespace Modules\Posts\Entities\Traits;

trait PostStatusTrait
{
    public function getStatusAttribute()
    {
        if (!$this->exists) {
            return 'unsaved';
        }

        if ($this->trashed()) {
            return 'deleted';
        }

        if ($this->isPublished()) {
            return 'published';
        }

        if ($this->isScheduled() and $this->published_by) {
            return 'scheduled';
        }

        if ($this->isDraft()) {
            return 'draft';
        }

        if ($this->isPending()) {
            return 'pending';
        }

        return 'unknown';
    }



    public function isPublished()
    {
        return (!$this->trashed() and $this->published_by and $this->published_at and $this->published_at <= now());
    }



    public function isScheduled()
    {
        return ($this->published_at and $this->published_at > now());
    }



    public function isApproved()
    {
        return boolval($this->published_by);
    }



    public function isNotApproved()
    {
        return !$this->isApproved();
    }



    public function isRejected()
    {
        return boolval($this->moderated_by and !$this->published_by);
    }



    public function isDraft()
    {
        return $this->is_draft;
    }



    public function isPending()
    {
        return (!$this->isDraft() and !$this->published_by);
    }
}
