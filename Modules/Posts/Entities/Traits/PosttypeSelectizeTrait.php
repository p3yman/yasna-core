<?php
namespace Modules\Posts\Entities\Traits;

use Illuminate\Support\Collection;
use App\Models\Feature;

trait PosttypeSelectizeTrait
{
    /**
     * Gets a selectize-suitable list of available features, to be attached
     *
     * @return array
     */
    public function featuresSelectizeList(): array
    {
        $array = [];
        foreach ($this->usableFeatures() as $feature) {
            $array[] = [
                 "id"    => $feature->id,
                 "title" => $feature->title,
            ];
        }

        return $array;
    }



    /**
     * Gets a selectize-suitable string of attached features
     *
     * @return string
     */
    public function featuresSelectizeValue(): string
    {
        return implode(',', $this->features()->get()->pluck('id')->toArray());
    }



    /**
     * Gets a selectize-suitable list of available inputs, to be attached
     *
     * @return array
     */
    public function inputsSelectizeList(): array
    {
        $array = [];
        foreach (model('input')->all() as $input) {
            $array[] = [
                 "id"    => $input->id,
                 "title" => $input->title,
            ];
        }

        return $array;
    }



    /**
     * Gets a selectize-suitable string of attached inputs
     *
     * @return string
     */
    public function inputsSelectizeValue(): string
    {
        return implode(',', $this->inputs()->get()->pluck('id')->toArray());
    }
}
