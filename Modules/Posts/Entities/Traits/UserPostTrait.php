<?php namespace Modules\Posts\Entities\Traits;

trait UserPostTrait
{
    /**
     * shortcut to postsCreatedBy
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->postsCreatedBy();
    }



    /**
     * posts, created by the user in question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsCreatedBy()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Post", 'created_by');
    }



    /**
     * posts, updated by the user in question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsUpdatedBy()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Post", 'updated_by');
    }



    /**
     * posts, deleted by the user in question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsDeletedBy()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Post", 'deleted_by');
    }



    /**
     * posts, published by the user in question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsPublishedBy()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Post", 'published_by');
    }



    /**
     * posts, owned by the user in question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsOwnedBy()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Post", 'owned_by');
    }



    /**
     * posts, moderated by the user in question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsModeratedBy()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Post", 'moderated_by');
    }



    /**
     * posts, pinned by the user in question
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsPinnedBy()
    {
        return $this->hasMany(MODELS_NAMESPACE . "Post", 'pinned_by');
    }
}
