<?php namespace Modules\Posts\Entities\Traits;

trait PostInputTrait
{
    public function inputs()
    {
        return $this->posttype()->inputs()->where('type', 'editor');
    }



    public function getInputsAttribute()
    {
        return $this->inputs()->get();
    }
}
