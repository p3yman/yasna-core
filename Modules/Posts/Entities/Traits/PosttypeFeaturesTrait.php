<?php
namespace Modules\Posts\Entities\Traits;

use Illuminate\Support\Collection;
use App\Models\Feature;

trait PosttypeFeaturesTrait
{
    protected $features_cached = false;

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */
    public function features()
    {
        return $this->belongsToMany(MODELS_NAMESPACE .'Feature');
    }

    /*
    |--------------------------------------------------------------------------
    | Selectors
    |--------------------------------------------------------------------------
    |
    */
    public static function having($features)
    {
        return self::selector([
            'features' => $features,
        ]);
    }

    /**
     * @param $features
     *
     * @return mixed
     * @deprecated (replaced with having() )
     */
    public static function withFeature($features)
    {
        return self::having($features);
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function hasFeature($feature_slug, $neglect_cache = false)
    {
        return boolval(in_array($feature_slug, $this->featuresArray($neglect_cache)));
    }

    public function has($feature_slug, $neglect_cache = false)
    {
        return $this->hasFeature($feature_slug, $neglect_cache);
    }

    public function hasnot($feature_slug, $neglect_cache = false)
    {
        return !$this->hasFeature($feature_slug, $neglect_cache);
    }

    public function hasAllOf($features, $neglect_cache = false)
    {
        foreach ($features as $feature) {
            if ($this->hasnot($feature, $neglect_cache)) {
                return false;
            }
        }

        return true;
    }

    public function hasAnyOf($features, $neglect_cache = false)
    {
        foreach ($features as $feature) {
            if ($this->has($feature, $neglect_cache)) {
                return true;
            }
        }

        return false;
    }

    public function featuresArray($neglect_cache = false)
    {
        if ($neglect_cache) {
            return $this->features()->pluck('slug')->toArray();
        } else {
            return (array) $this->getMeta('cached_features');
        }
    }

    public function getFeaturesArrayAttribute()
    {
        return $this->featuresArray();
    }


    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */

    public function syncFeatures($requested_features)
    {
        $this->features()->sync($requested_features);
    }

    public function attachFeatures($slugs)
    {
        $slugs    = (array)$slugs;
        $id_array = [];

        foreach ($slugs as $slug) {
            $feature = model('feature')->grabSlug($slug);
            if ($feature and $feature->id) {
                $id_array[] = $feature->id;
            }
        }

        return $this->features()->syncWithoutDetaching($id_array);
    }

    public function cacheFeatures()
    {
        $array = $this->featuresArray(true);
        return $this->updateOneMeta('cached_features', $array);
    }

    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    /**
     * Provides a collection of all active features, used for combo boxes.
     * @return Collection
     */
    public function usableFeatures()
    {
        return Feature::orderBy('order')->orderBy('title')->get();
    }
}
