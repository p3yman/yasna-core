<?php namespace Modules\Posts\Entities\Traits;

trait PosttypeMetaTrait
{
    /**
     * Main meta fields
     *
     * @return array
     */
    public function mainMetaFields(): array
    {
        return [
             'cached_features',
        ];
    }



    /**
     * Dynamic meta fields
     *
     * @return array
     */
    public function dynamicMetaFields(): array
    {
        return $this->inputs()->whereIn('type', ['downstream', 'upstream'])->pluck('slug')->toArray();
    }
}
