<?php

namespace Modules\Posts\Entities\Traits;

use App\Models\Posttype;

/**
 * Class Post
 *
 * @property string $type
 * @package Modules\Posts\Entities\Traits
 */
trait PostPosttypeTrait
{

    /**
     * get the parent posttype of the record
     *
     * @return \App\Models\Posttype
     */
    public function posttype()
    {
        return posttype($this->type);
    }



    /**
     * get the parent posttype of the record
     *
     * @return \App\Models\Posttype
     */
    public function getPosttypeAttribute()
    {
        return $this->posttype();
    }



    /**
     * set the posttype of the model (useful for new instances)
     *
     * @param Posttype $model
     */
    public function setPosttype($model)
    {
        $this->type = $model->slug;
    }



    /**
     * get the encrypted posttype slugs
     *
     * @return string
     */
    public function getEncryptedTypeAttribute()
    {
        return $this->posttype()->encrypted_slug;
    }



    /**
     * check if the posttype has the provided feature
     *
     * @param string $feature
     *
     * @return bool
     */
    public function has($feature)
    {
        return $this->posttype()->has($feature);
    }



    /**
     * check if the posttype has not the provided feature
     *
     * @param string $feature
     *
     * @return bool
     */
    public function hasnot($feature)
    {
        return !$this->has($feature);
    }



    /**
     * check if the posttype has any of the provided features
     *
     * @param array $features
     *
     * @return bool
     */
    public function hasAnyOf($features)
    {
        return $this->posttype()->hasAnyOf($features);
    }



    /**
     * check if the posttype has all of the provided features
     *
     * @param array $features
     *
     * @return bool
     */
    public function hasAllOf($features)
    {
        return $this->posttype()->hasAllOf($features);
    }
}
