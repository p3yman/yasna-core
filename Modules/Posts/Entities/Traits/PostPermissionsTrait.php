<?php
namespace Modules\Posts\Entities\Traits;

trait PostPermissionsTrait
{
    public function fullPermitString($permit = '*')
    {
        $full_permit = "posts-$this->type.$permit";

        if ($this->has('locales') and !str_contains($permit, '.')) {
            $full_permit .= ".$this->locale";
        }

        return $full_permit;
    }



    public function accessibleDomains()
    {
        $array = ['manager'];

        if ($this->has('domains')) {
            $array[] = model('role')->domain_role_prefix . '-' .$this->domain_slug;
        }

        return $array;
    }



    public function can($permit = '*')
    {
        return user()
             ->as($this->accessibleDomains())
             ->can($this->fullPermitString($permit))
             ;
    }



    public function cannot($permit)
    {
        return !$this->can($permit);
    }



    public function canPublish()
    {
        if ($this->trashed()) {
            return false;
        }

        return $this->can('publish');
    }



    public function canEdit()
    {
        if (!$this->exists or $this->trashed()) {
            return false;
        }

        if ($this->isOwner() and $this->isNotApproved() and $this->can('create')) {
            return true;
        }

        return $this->can('edit');
    }



    public function canDelete()
    {
        if (!$this->exists) {
            return true;
        }

        if ($this->isOwner() and $this->isNotApproved() and $this->can('create')) {
            return true;
        }

        return $this->can('delete');
    }



    public function canBin()
    {
        if (!$this->exists) {
            return false;
        }

        if ($this->isOwner() and $this->isNotApproved() and $this->can('create')) {
            return true;
        }

        return $this->can('bin');
    }



    public function canRestore()
    {
        return $this->canBin();
    }



    public function canDestroy()
    {
        return $this->canBin();
    }



    public function canCreate()
    {
        return $this->can('create');
    }



    public function canCreateOrEdit()
    {
        if ($this->id) {
            return $this->canEdit();
        } else {
            return $this->canCreate();
        }
    }



    public function canReflect()
    {
        return boolval(user()->domainsCount($this->fullPermitString('create')) > 1);
    }



    public static function checkManagePermission($posttype, $criteria)
    {
        switch ($criteria) {
            case 'search':
                $permit = '*';
                break;

            case 'pending':
            case 'drafts':
                $permit = '*';
                break;

            case 'my_posts':
            case 'my_drafts':
                $permit = 'create';
                break;

            case 'bin':
                $permit = '*';
                break;

            default:
                $permit = '*';
        }

        return user()->as('admin')->can("posts-$posttype.$permit");
    }



    /**
     * A combo of all the domains, a user can create post in
     */
    public function domainsCombo()
    {
        $required_permission = "create";

        return user()->domainsQuery($this->fullPermitString($required_permission))->get();
    }
}
