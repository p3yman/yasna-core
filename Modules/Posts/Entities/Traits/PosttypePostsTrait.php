<?php // Attached to Posttype Model
namespace Modules\Posts\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class PosttypePostsTrait
 *
 * @package Modules\Posts\Entities\Traits
 * @property $slug
 */
trait PosttypePostsTrait
{
    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */

    /**
     * @return Builder
     */
    public function posts()
    {
        return model('post')->where('type', $this->slug);
    }



    /**
     * @param int $number
     *
     * @return Collection
     */
    public function recent($number = 5)
    {
        return $this->recentIn($number, $this->localesAllowed('browse'));
    }



    /**
     * @param int    $number
     * @param string $locale
     *
     * @return Collection
     */
    public function recentIn($number = 5, $locale = 'auto')
    {
        $elector_switches = [
             'type'     => $this->slug,
             'criteria' => "published",
             'locale'   => $locale,
        ];

        return model('post')
             ->elector($elector_switches)
             ->orderBy('published_at', 'desc')
             ->limit($number)
             ->get()
             ;
    }



    /**
     * @return Builder
     */
    public function pendings()
    {
        $elector_switches = [
             'type'     => $this->slug,
             'criteria' => "pending",
             'locale'   => $this->localesAllowed('publish'),
        ];

        return model('post')->elector($elector_switches);
    }
}
