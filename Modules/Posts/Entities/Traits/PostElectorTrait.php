<?php namespace Modules\Posts\Entities\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class PostElectorTrait
 *
 * @package Modules\Posts\Entities\Traits
 * @method Builder elector()
 * @method mixed getElector(string $parameter_name)
 */
trait PostElectorTrait
{
    /**
     * For a single posttype, with safe handling of accidentally array inputs.
     * Detects 'features:folan' patterns and diverts them to the correct method, for compatibility reasons.
     *
     * @param $type
     */
    protected function electorType($type)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (is_array($type)) {
            $this->electorTypes($type);
            return;
        }

        if ($type == 'all') {
            return;
        }

        /*-----------------------------------------------
        | Compatibility Bypass ...
        */
        if (str_contains($type, 'features:')) {
            $this->electorFeatures(str_after($type, "features:"));
            return;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->elector()->where('type', $type);
    }



    /**
     * For an array of posttypes, with safe handling of accidentally string inputs.
     *
     * @param $types
     */
    protected function electorTypes($types)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!is_array($types)) {
            $this->electorType($types);
            return;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->elector()->whereIn('type', $types);
    }



    /**
     * A mirror to $this->electorFeatures()
     *
     * @param $feature
     */
    protected function electorFeature($feature)
    {
        $this->electorFeatures($feature);
    }



    /**
     * for a single/array/comma-separated list of features
     *
     * @param $features
     */
    protected function electorFeatures($features)
    {
        /*-----------------------------------------------
        | Array Conversion ...
        */
        if (is_string($features)) {
            $features = explode_not_empty(',', strtolower($features));
        }

        /*-----------------------------------------------
        | Find Types ...
        */
        $types = model("Posttype")->having($features)->get()->pluck('slug')->toArray();

        /*-----------------------------------------------
        | Process ...
        */
        $this->electorType($types);
    }



    /**
     * For a single locale, with safe handling of accidentally array inputs and comma-separated strings.
     *
     * @param $locale
     */
    protected function electorLocale($locale)
    {
        /*-----------------------------------------------
        | Bypass ..
        */
        if (is_array($locale)) {
            $this->electorLocales($locale);
            return;
        }

        if ($locale == 'all') {
            return;
        }

        if (str_contains($locale, ',')) {
            $this->electorLocales(explode_not_empty(',', $locale));
            return;
        }

        /*-----------------------------------------------
        | Auto Detection ...
        */
        if ($locale == 'auto') {
            $locale = getLocale();
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->elector()->where('locale', $locale);
    }



    /**
     * For an array of locales, with safe handling of accidentally string inputs.
     *
     * @param $locales
     */
    protected function electorLocales($locales)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!is_array($locales)) {
            $this->electorLocale($locales);
            return;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->elector()->whereIn('locale', $locales);
    }



    /**
     * A mirror to $this->electorDomains()
     *
     * @param $domain
     */
    protected function electorDomain($domain)
    {
        $this->electorDomains($domain);
    }



    /**
     * For considering reflecting domains
     * a single domain, an array of domains, and special 'all'/'auto' to select automatically.
     * Both id and slug are supported.
     *
     * @param string|array $domains
     */
    protected function electorDomains($domains)
    {
        $domains = (array)$this->automaticallyFindAllowedDomainsIfNecessary($domains);

        /*-----------------------------------------------
        | Bypass  ...
        */
        if (!count($domains)) {
            return;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $this->elector()->whereHas('domains', function ($query) use ($domains) {
            $query->whereIn('domains.id', $domains)->orWhereIn('domains.slug', $domains);
        })
        ;
    }



    /**
     * a mirror to $this->electorMainDomains()
     *
     * @param $domain
     */
    protected function electorMainDomain($domain)
    {
        $this->electorMainDomains($domain);
    }



    /**
     * For considering reflecting domains
     * a single domain, an array of domains, and special 'all'/'auto' to select automatically.
     * Both id and slug are supported.
     *
     * @param $domains
     */
    protected function electorMainDomains($domains)
    {
        $domains = (array)$this->automaticallyFindAllowedDomainsIfNecessary($domains);
        $domains = $this->automaticallyReplaceIdsWithSlugsIfNecessary($domains);


        /*-----------------------------------------------
        | Bypass  ...
        */
        if (!count($domains)) {
            return;
        }


        /*-----------------------------------------------
        | Process ...
        */
        $this->elector()->whereIn('domain_id', $domains);
    }



    /**
     * Automatically finds allowed domains, if $domains is 'all' or 'auto'
     *
     * @param string|array $domains
     *
     * @return array
     */
    private function automaticallyFindAllowedDomainsIfNecessary($domains)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!is_string($domains) or ($domains != 'all' and $domains != 'auto')) {
            return $domains;
        }

        /*-----------------------------------------------
        | If A Manager ...
        */
        if (user()->is_a('manager')) {
            return [];
        }

        /*-----------------------------------------------
        | Normal Situations ...
        */
        return user()->domainsArray("posts-" . $this->getElector('type'));
    }



    /**
     * Automatically replaces domain slugs with the corresponding ids
     *
     * @param array $domains
     *
     * @return array
     */
    private function automaticallyReplaceIdsWithSlugsIfNecessary(array $domains)
    {
        foreach ($domains as $key => $domain) {
            if (!is_numeric($domain)) {
                $domains[$key] = model('domain')->slugToId($domain);
            }
        }

        return $domains;
    }



    /**
     * Adds `all_with_trashed` condition to the criteria
     */
    protected function electorCriteriaAllWithTrashed()
    {
        $this->electorCriteriaWithTrashed();
    }



    /**
     * Removes all criteria from the elector
     */
    protected function electorCriteriaAll()
    {
        // intentionally left blank
    }



    /**
     * Adds `with_trashed` condition to the criteria
     */
    protected function electorCriteriaWithTrashed()
    {
        $this->elector()->withTrashed();
    }



    /**
     * for posts after @form
     *
     * @param string|Carbon $from
     */
    public function electorPublishedFrom($from)
    {
        $this->elector()->whereDate('created_at', '>=', $from);
    }



    /**
     * for posts before @to
     *
     * @param string|Carbon $to
     */
    public function electorPublishedTo($to)
    {
        $this->elector()->whereDate('created_at', '<=', $to);
    }



    /**
     * Adds `published` condition to the criteria
     */
    protected function electorCriteriaPublished()
    {
        $this->elector()->whereDate('published_at', '<=', $this->now())->where('published_by', '>', 0);
    }



    /**
     * Adds `scheduled` condition to the criteria
     */
    protected function electorCriteriaScheduled()
    {
        $this->elector()->whereDate('published_at', '>', $this->now())->where('published_by', '>', 0);
    }



    /**
     * Adds `pending` condition to the criteria
     */
    protected function electorCriteriaPending()
    {
        $this->elector()->where('published_by', '0')->where('is_draft', 0);
    }



    /**
     * Adds `drafts` condition to the criteria
     */
    protected function electorCriteriaDrafts()
    {
        $this->elector()->where('is_draft', 1)->where('published_by', 0);
    }



    /**
     * Adds `rejected` condition to the criteria
     */
    protected function electorCriteriaRejected()
    {
        $this->elector()->where('moderated_by', '>', '0')->where('published_by', 0);
    }



    /**
     * Adds `my_posts` condition to the criteria
     */
    protected function electorCriteriaMyPosts()
    {
        $this->elector()->where('owned_by', user()->id);
    }



    /**
     * Adds `my_drafts` condition to the criteria
     */
    protected function electorCriteriaMyDrafts()
    {
        $this->elector()->where('owned_by', user()->id)->where('is_draft', true)->where('published_by', '0');
    }



    /**
     * Adds `bin` condition to the criteria
     */
    protected function electorCriteriaBin()
    {
        $this->elector()->onlyTrashed();
    }



    /**
     * @param int $user_id
     */
    protected function electorOwner($user_id)
    {
        $this->elector()->where('owned_by', $user_id);
    }



    /**
     * @param $event_starts
     */
    protected function electorEventStarts($event_starts)
    {
        $this->elector()->whereDate('event_starts_at', '<=', $event_starts);
    }



    /**
     * @param $event_ends
     */
    protected function electorEventEnds($event_ends)
    {
        $this->elector()->whereDate('event_ends_at', '>=', $event_ends);
    }



    /**
     * @param $register_starts
     */
    protected function electorRegisterStarts($register_starts)
    {
        $this->elector()->whereDate('event_starts_at', '<=', $register_starts);
    }



    /**
     * @param $register_ends
     */
    protected function electorRegisterEnds($register_ends)
    {
        $this->elector()->whereDate('event_starts_at', '>=', $register_ends);
    }



    /**
     * @return array
     */
    protected function mainSearchableFields()
    {
        return [
             'title',
             'slug',
        ];
    }



    /**
     * @param array $parameters
     * Compatibility Method
     *
     * @return Builder
     * @deprecated
     */
    public static function selector($parameters = [])
    {
        return model('post')->elector($parameters);
    }



    /**
     * get all creator posts's
     *
     * @param string|int $id
     */
    public function electorCreator($id)
    {
        if (!$id) {
            return;
        }

        if (is_string($id)) {
            $id = hashid($id);
        }

        $this->elector()->where('created_by', $id);
    }



    /**
     * get all posts after $date
     *
     * @param Carbon|string $date
     */
    public function electorDateFrom($date)
    {
        if (!is_numeric($date) or !$date or strlen($date) < 10) {
            return;
        }

        if (strlen($date) > 10) {
            $date = $this->convertTime($date);
        }

        try {
            $from = $this->createCarbonObject($date);
        } catch (\Exception $exception) {
            return;
        };

        $this->elector()->whereDate('created_at', '>=', $from);
    }



    /**
     * get all posts before $date
     *
     * @param Carbon|string $date
     */
    public function electorDateTo($date)
    {
        if (!is_numeric($date) or !$date or strlen($date) < 10) {
            return;
        }

        if (strlen($date) > 10) {
            $date = $this->convertTime($date);
        }

        try {
            $from = $this->createCarbonObject($date);
        } catch (\Exception $exception) {
            return;
        };

        $this->elector()->whereDate('created_at', '<=', $from);
    }



    /**
     * get all posts with the given ids or hashids (array or comma-separated lists)
     *
     * @param array|string $ids
     */
    public function electorIds($ids)
    {
        $array_ids = $ids;
        if (is_string($ids)) {
            $array_ids = explode(",", $ids);
        }

        foreach ($array_ids as $id) {
            /** @var array $id_num */
            $id_num[] = hashid_number($id);
        }

        $this->elector()->whereIn("id", $id_num);
    }



    /**
     * convert time of milliseconds to seconds
     *
     * @param Carbon|string $date
     *
     * @return float
     */
    public function convertTime($date)
    {
        return floor($date / 1000);
    }



    /**
     * create carbon object and convert to timeString
     *
     * @param Carbon|string $date
     *
     * @return string
     */
    public function createCarbonObject($date)
    {
        return Carbon::createFromTimestamp($date)->toDateTimeString();
    }

}
