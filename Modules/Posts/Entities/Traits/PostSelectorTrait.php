<?php
namespace Modules\Posts\Entities\Traits;

use App\Models\Posttype;
use Carbon\Carbon;

/**
 * Class PostSelectorTrait
 *
 * @package Modules\Posts\Entities\Traits
 * @deprecated
 */
trait PostSelectorTrait
{
    public static function selector($parameters = [])
    {

        /*-----------------------------------------------
        | Normalize ...
        */
        $switch = array_normalize($parameters, service("posts:model_selector")->paired('default'));
        $table  = self::where('id', '>', '0');


        /*-----------------------------------------------
        | Process ...
        */
        foreach (service("posts:model_selector")->read() as $service) {
            if (isset($service['method'])) {
                $method_name = $service['method'];
                $table       = self::$method_name($switch, $table);
            }
        }


        /*-----------------------------------------------
        | Return ...
        */

        return $table;
    }



    public static function selector_native($switch, $table)
    {
        /*-----------------------------------------------
        | Simple Things ...
        */
        if ($switch['slug']) {
            $table = $table->where('slug', $switch['slug']);
        }
        if ($switch['id']) {
            $table = $table->where('id', $switch['id']);
        }

        /*-----------------------------------------------
        | Process Owner ...
        */
        if ($switch['owner'] > 0) {
            $table = $table->where('owned_by', $switch['owner']);
        }

        /*-----------------------------------------------
        | Process Dates ...
        */
        if ($switch['event_starts']) {
            $table = $table->whereDate('event_starts_at', '<=', $switch['event_starts']);
        }
        if ($switch['event_ends']) {
            $table = $table->whereDate('event_ends_at', '>=', $switch['event_ends']);
        }
        if ($switch['register_starts']) {
            $table = $table->whereDate('register_starts', '<=', $switch['register_starts']);
        }
        if ($switch['register_ends']) {
            $table = $table->whereDate('register_ends', '>=', $switch['register_ends']);
        }


        return $table;
    }



    public static function selector_locale($switch, $table)
    {
        $locale = $switch['locale'];
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!$locale or (is_string($locale) and in_array($locale, ['all', null]))) {
            return $table;
        }

        /*-----------------------------------------------
        | Auto ...
        */
        if (is_string($locale) and $locale == 'auto') {
            $locale = getLocale();
        }

        /*-----------------------------------------------
        | Process and Feedback...
        */
        $locale = (array)$locale;
        return $table->whereIn('locale', $locale);
    }



    public static function selector_type($switch, $table)
    {
        $type = $switch['type'];

        /*-----------------------------------------------
        | Bypass if 'all' types are requested ...
        */
        if ($type == 'all') {
            return $table;
        }


        /*-----------------------------------------------
        | If some features (comma-separated) are requested ...
        */
        if (!is_array($type) and str_contains($type, 'features:')) {
            $feature = explode(',', strtolower(str_after($type, ':')));
            $type    = Posttype::having($feature)->get()->pluck('slug')->toArray(); //<~~ an array of posttypes
        }

        /*-----------------------------------------------
        | When an array of selected posttypes are requested ...
        */
        if (is_array($type)) {
            $table->whereIn('type', $type);
        } /*-----------------------------------------------
        | When an specific type is requested ...
        */
        else {
            $table = $table->where('type', $type);
        }


        return $table;
    }



    public static function selector_criteria($switch, $table)
    {
        $now = Carbon::now()->toDateTimeString();
        switch ($switch['criteria']) {
            case 'all':
                break;

            case 'all_with_trashed':
                $table = $table->withTrashed();
                break;

            case 'published':
                $table = $table->whereDate('published_at', '<=', $now)->where('published_by', '>', '0');
                break;

            case 'scheduled':
                $table = $table->whereDate('published_at', '>', $now)->where('published_by', '>', '0');
                break;

            case 'pending':
                $table = $table->where('published_by', '0')->where('is_draft', 0);
                break;

            case 'drafts':
                $table = $table->where('is_draft', 1)->where('published_by', '0');
                break;

            case 'rejected':
                $table = $table->where('moderated_by', '>', '0')->where('published_by', '0');
                break;

            case 'my_posts':
                $table = $table->where('owned_by', user()->id);
                break;

            case 'my_drafts':
                $table = $table->where('owned_by', user()->id)->where('is_draft', true)->where('published_by', '0');
                break;

            case 'bin':
                $table = $table->onlyTrashed();
                break;

            default:
                $table = $table->where('id', '0');
                break;

        }


        return $table;
    }



    public static function selector_search($switch, $table)
    {
        if ($switch['search']) {
            $table = $table->whereRaw(self::searchRawQuery($switch['search'])); //@TODO: Danger of raw query! <~~
        }

        return $table;
    }



    public static function selector_domains($switch, $table)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!isset($switch['domains']) or !$switch['domains']) {
            return $table;
        }

        /*-----------------------------------------------
        | Process ...
        */
        if ($switch['domains'] == 'auto') {
            if (user()->is_a('manager')) {
                $switch['domains'] = null;
            } else {
                $switch['domains'] = user()->domainsArray("posts-" . $switch['type']);
            }
        }

        $domains = (array)$switch['domains'];

        if (count($domains)) {
            $table->whereHas('domains', function ($query) use ($domains) {
                $query->whereIn('domains.id', $domains)->orWhereIn('domains.slug', $domains);
            });
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $table;
    }



    public static function selector_main_domain($switch, $table)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!isset($switch['main_domain']) or !$switch['main_domain']) {
            return $table;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $domain_id = $switch['main_domain'];

        if ($domain_id == 'auto') {
            if (user()->is_a('manager')) {
                $domain_id = null;
            } else {
                $domain_id = user()->domainsArray("posts-" . $switch['type']);
            }
        }

        $domains = (array)$domain_id;

        /*-----------------------------------------------
        | Slugs to Ids ...
        */
        foreach ($domains as $key => $domain) {
            if (!is_numeric($domain)) {
                $domains[$key] = model('domain')->slugToId($domain);
            }
        }


        /*-----------------------------------------------
        | Query ...
        */
        if (count($domains)) {
            $table->whereIn('domain_id', $domains);
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $table;
    }
}
