<?php
namespace Modules\Posts\Entities\Traits;

/**
 * @method where($field, $value)
 * @method grabId($id)
 * @property $copy_of
 * @property $cached_parent
 */
trait PostInterrelationTrait
{
    /**
     * @return $this
     */
    public function copies()
    {
        return $this->where('copy_of', $this->id);
    }



    /**
     * @return $this
     */
    public function original()
    {
        if ($this->isNotCopy()) {
            return $this->refresh();
        }

        return $this->grabId($this->copy_of);
    }



    /**
     * @return $this
     */
    public function parent()
    {
        if ($this->cached_parent) {
            return $this->cached_parent;
        }

        if ($this->isCopy()) {
            return $this->grabId($this->copy_of);
        }

        return $this;
    }



    /**
     * @return $this
     */
    public function getParentAttribute()
    {
        return $this->parent();
    }



    /**
     * @return bool
     */
    public function isCopy()
    {
        return boolval($this->copy_of);
    }



    /**
     * @return bool
     */
    public function isNotCopy()
    {
        return !$this->isCopy();
    }
}
