<?php namespace Modules\Posts\Entities\Traits;

trait PostDomainsTrait
{
    /**
     * One-to-Many relation for the domain_id field (which is the main domain)
     *
     * @return mixed
     */
    public function domain()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "Domain");
    }



    /**
     * @return string : Main Domain Name
     */
    public function domainName()
    {
        if ($this->hasnot('domains') or !$this->domain_id) {
            return null;
        }

        return $this->domain->title;
    }



    /**
     * @return string : Main Domain Name
     */
    public function getDomainNameAttribute()
    {
        return $this->domainName();
    }



    /**
     * @return string : Main Domain Slug
     */
    public function domainSlug()
    {
        if ($this->hasnot('domains')) {
            return null;
        }

        if (!$this->domain_id) {
            return $this->randomAllowedDomain();
        }

        return $this->domain->slug;
    }



    /**
     * @return string : Main Domain Slug
     */
    public function getDomainSlugAttribute()
    {
        return $this->domainSlug();
    }



    /**
     * @return string
     */
    private function randomAllowedDomain()
    {
        $array = user()->domainsArray();
        if (count($array)) {
            return $array[0];
        }

        return null;
    }



    /**
     * Many-to-Many Relation for the reflective domains
     *
     * @return mixed
     */
    public function domains()
    {
        return $this
             ->belongsToMany(MODELS_NAMESPACE . "Domain")
             ->withPivot('is_main')
             ->withTimestamps()
             ;
    }



    /**
     * A mirror method to be used instead of $this->domains() to be more readable.
     *
     * @return mixed
     */
    public function domainReflections()
    {
        return $this->domains();
    }



    /**
     * @return array : ids of the reflecting domains
     */
    public function domainReflectionIds()
    {
        return $this->domainReflections()->get()->pluck('id')->toArray();
    }



    /**
     * @return mixed
     */
    public function getDomainReflectionsAttribute()
    {
        return $this->domainReflections()->get();
    }



    /**
     * @return integer
     */
    public function domainReflectionCount()
    {
        return $this->domainReflections()->count();
    }



    /**
     * Attaches a domain to the post model, separating the main and reflecting ones.
     *
     * @param      $domain_id
     * @param bool $is_main
     *
     * @return mixed
     */
    public function reflect($domain_id, $is_main = false)
    {
        return $this->domainReflections()->attach($domain_id, [
             "is_main" => $is_main,
        ])
             ;
    }



    /**
     * Syncs many-to-many relations, without touching the one created by main domain
     *
     * @param $domain_ids
     *
     * @return mixed
     */
    public function updateReflections($domain_ids)
    {
        return $this->domainReflections()->wherePivot('is_main', 0)->sync($domain_ids);
    }



    /**
     * updates many-to-many reflecting domain for the main domain_id, by automatic detaching and reattaching domains.
     *
     * @param $domain_id
     */
    public function updateMainReflection($domain_id)
    {
        $this->domainReflections()->wherePivot('is_main', 1)->detach();
        $this->domainReflections()->attach($domain_id, [
             "is_main" => "1",
        ])
        ;
    }
}
