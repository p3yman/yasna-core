<?php namespace Modules\Posts\Entities\Traits;

trait PostAttachmentsTrait
{
    public function getAttachmentHashidsArrayAttribute()
    {
        $array  = $this->spreadMeta()->files;
        $result = [];

        foreach ($array as $item) {
            $result[] = $item['src'];
        }

        return $result;
    }



    public function getViewableAlbumAttribute()
    {
        $this->spreadMeta();
        $album = $this->post_photos;
        if ($album and is_array($album) and count($album)) {
            foreach ($album as &$image) {
                $image['src'] = url($image['src']);
            }

            return $album;
        }

        return [];
    }



    public function getViewableAlbumThumbnailsAttribute()
    {
        $album = $this->viewable_album;
        if ($album and is_array($album) and $album) {
            foreach ($album as &$image) {
                $image['src'] = str_replace_last('/', '/thumbs/', $image['src']);
            }

            return $album;
        }

        return [];
    }



    public static function savePhotos($data)
    {
        $resultant_array = [];
        unset($data['_photo_src_NEW']);

        foreach ($data as $field => $value) {
            if (str_contains($field, '_photo_src_')) {
                $label_field = str_replace('src', 'label', $field);
                $link_field  = str_replace('src', 'link', $field);
                array_push($resultant_array, [
                     'src'   => str_replace(url('') . '/', null, $value),
                     'label' => $data[$label_field],
                     'link'  => $data[$link_field],
                ]);
            }
        }

        return $resultant_array;
    }
}
