<?php namespace Modules\Posts\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait PostLocalesTrait
{
    /**
     * @return Builder
     */
    public function sisters()
    {
        return self::where('sisterhood', $this->sisterhood);
    }



    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getSistersAttribute()
    {
        return $this->sisters()->get();
    }



    /**
     * @param $locale
     *
     * @return Model|$this
     */
    public function in($locale)
    {
        if ($locale == $this->locale) {
            return $this;
        }

        $model = $this->sisters()->where('locale', $locale)->first();
        if ($model) {
            return $model;
        } else {
            $model             = new self();
            $model->locale     = $locale;
            $model->type       = $this->type;
            $model->sisterhood = $this->sisterhood;

            return $model;
        }
    }



    /**
     * @return string
     */
    public function getOtherLocalesAttribute()
    {
        $array = $this->posttype->locales_array;
        $key   = array_search($this->locale, $array);
        array_forget($array, $key);

        return $array;
    }



    /**
     * generate a random sisterhood based on the unix timestamp
     *
     * @return string
     */
    public static function generateSisterhood()
    {
        return hashid(time(), 'main') . str_random(5);
    }
}
