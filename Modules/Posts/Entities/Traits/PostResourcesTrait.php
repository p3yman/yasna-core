<?php

namespace Modules\Posts\Entities\Traits;

trait PostResourcesTrait
{

    /**
     * boot Post Resources Trait
     *
     * @return void
     */
    public static function bootPostResourcesTrait()
    {
        static::addDirectResources('*');
    }
}
