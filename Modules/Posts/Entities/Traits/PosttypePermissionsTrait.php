<?php // Attached to Posttype Model
namespace Modules\Posts\Entities\Traits;

trait PosttypePermissionsTrait
{
    public function can($permit = '*', $as = 'admin')
    {
        return user()->as($as)->can("posts-" . $this->slug . '.' . $permit);
    }

    public function cannot($permit, $as = 'admin')
    {
        return !$this->can($permit, $as);
    }

    public function localesAllowed($for = '*')
    {
        $locales = $this->locales_array;
        $result  = [];

        /*-----------------------------------------------
        | Special Cases ...
        */
        if ($for=='*' or $this->hasnot('locales')) {
            return $this->can($for)? $locales : [] ;
        }

        /*-----------------------------------------------
        | Ordinary Cases ...
        */
        foreach ($locales as $locale) {
            if ($this->can("$for.$locale")) {
                $result[] = $locale;
            }
        }

        return $result ;
    }
}
