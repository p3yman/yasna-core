<?php namespace Modules\Posts\Entities\Traits;

trait PostMetaTrait
{
    public function mainMetaFields()
    {
        return [
             'moderate_note',
        ];
    }



    public function dynamicMetaFields()
    {
        $array = [];
        foreach ($this->inputs as $input) {
            $array[] = $input->slug;
        }

        return $array;
    }
}
