<?php namespace Modules\Posts\Entities\Traits;

trait PostRegisterTrait
{
    public function registers()
    {
        return model('user')->where('from_event_id', $this->id);
    }



    public function getRegistersAttribute()
    {
        return $this->registers()->get();
    }
}
