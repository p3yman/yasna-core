<?php
namespace Modules\Posts\Entities\Traits;

trait PostLinksTrait
{
    /**
     * @deprecated
     * @return mixed
     */
    public function getSiteLinkAttribute()
    {
        return $this->direct_url;
    }



    public function getPreviewLinkAttribute()
    {
        //@TODO: Preview Link
    }



    public function getBrowseLinkAttribute()
    {
        return $this->browseLink('all', "post=$this->hashid");
    }



    public function getCreateLinkAttribute()
    {
        return url("manage/posts/" . $this->type . "/create/" . $this->locale . "/" . $this->sisterhood);
    }



    public function getEditLinkAttribute()
    {
        return url("manage/posts/" . $this->type . "/edit/" . $this->hash_id);
    }



    public function getShortUrlAttribute()
    {
        return route('post.single.very-short', [
             'identifier' => config('prefix.routes.post.short') . $this->hash_id,
        ]);
        //		return url_locale(config('prefix.routes.post.short') . $this->hash_id);
    }



    public function browseLink($tab = 'all', $additive = null)
    {
        return url("manage/posts/$this->type/$tab/$additive");
    }
}
