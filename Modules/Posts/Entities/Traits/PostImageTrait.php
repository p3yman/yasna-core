<?php namespace Modules\Posts\Entities\Traits;

use Modules\Filemanager\Providers\UploadServiceProvider;

trait PostImageTrait
{
    public function getImageAttribute()
    {
        $image_url = $this->spreadMeta()->featured_image;
        if (!$image_url) {
            $image_url = 'assets/images/close.png';
        }

        return asset($image_url);
    }



    public function getViewableFeaturedImageAttribute()
    {
        $this->spreadMeta();
        if ($this->featured_image) {
            return url($this->featured_image);
        } else {
            if ($typeImage = $this->posttype->spreadMeta()->default_featured_image) {
                return url($typeImage);
            }
        }

        return '';
    }



    public function getViewableFeaturedImageThumbnailAttribute()
    {
        $image = $this->viewable_featured_image;

        return UploadServiceProvider::getThumb($image);
    }
}
