<?php
namespace Modules\Posts\Entities\Traits;

trait PosttypeInputsTrait
{
    protected $inputs_cached = false;

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */
    public function inputs()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . "Input");
    }

    public function editorInputs()
    {
        return $this->inputs()->where('type', 'editor')->orderBy('order');
    }

    public function getEditorInputsAttribute()
    {
        return $this->editorInputs()->get();
    }

    public function settingInputs()
    {
        $table = posttype($this->id)->inputs;

        if (user()->isDeveloper()) {
            $table = $table->whereIn('type', ['downstream', 'upstream']);
        } else {
            $table = $table->where('type', 'downstream');
        }
        $table = $table->sortBy('order');

        return $table;
    }

    public function getSettingInputsAttribute()
    {
        return $this->settingInputs();
    }


    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */
    public function attachRequiredInputs()
    {
        return $this->inputs()->syncWithoutDetaching($this->inputsRequiredByFeatures()->get()->pluck('id')->toArray());
    }

    public function syncInputs($requested_inputs)
    {
        if (is_string($requested_inputs)) {
            $requested_inputs = array_filter(explode(',', $requested_inputs));
        }

        return $this->inputs()->sync($requested_inputs);
    }

    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function hasInput($input_slug)
    {
        $inputs = $this->inputs->pluck('slug')->toArray();

        return boolval(in_array($input_slug, $inputs));
    }

    public function inputsRequiredByFeatures()
    {
        $models = model('input')::orderBy('order')->whereHas('features', function ($query) {
            $query->whereIn('features.id', $this->features()->get()->pluck('id')->toArray());
        })
        ;

        return $models;
    }
}
