<?php

namespace Modules\Posts\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use App\Models\Input;
use Modules\Yasna\Services\YasnaModel;

class Feature extends YasnaModel
{
    use SoftDeletes;
    public    $reserved_slugs = 'root,super,all,dev,developer,admin';
    protected $inputs_cached  = false;
    protected $guarded        = ['id'];



    public function mainMetaFields()
    {
        return [
             'hint',
        ];
    }



    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */

    public function posttypes()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Posttype');
    }



    public function inputs()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Input');
    }



    /**
     * @return bool|Collection
     * Provides a way to make sure the above relationship runs, one time only!
     */
    public function getInputsAttribute()
    {
        if ($this->inputs_cached === false) {
            $this->inputs_cached = $this->inputs()->get();
        }

        return $this->inputs_cached;
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function hasInput($input_slug)
    {
        $inputs = $this->inputs->pluck('slug')->toArray();

        return boolval(in_array($input_slug, $inputs));
    }



    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */
    public function syncInputs($requested_inputs)
    {
        if (is_string($requested_inputs)) {
            $requested_inputs = array_filter(explode(',', $requested_inputs));
        }

        return $this->inputs()->sync($requested_inputs);
    }



    public function attachInputs($slugs)
    {
        $slugs    = (array)$slugs;
        $id_array = [];

        foreach ($slugs as $slug) {
            $input = model("input", $slug);
            if ($input and $input->id) {
                $id_array[] = $input->id;
            }
        }

        return $this->inputs()->syncWithoutDetaching($id_array);
    }



    /*
    |--------------------------------------------------------------------------
    | Assessor and Mutators
    |--------------------------------------------------------------------------
    |
    */
    public function getTitleAndHintAttribute()
    {
        if ($this->spreadMeta()->hint) {
            return $this->title . ': ' . $this->hint;
        } else {
            return $this->title;
        }
    }



    public function getSeederAttribute()
    {
        $output = null;
        $array  = [
             'slug'      => $this->slug,
             'title'     => $this->title,
             'order'     => $this->order,
             'icon'      => $this->icon,
             'post_meta' => $this->post_meta,
             'meta-hint' => $this->spreadMeta()->hint,
        ];

        foreach ($array as $key => $item) {
            $output .= "'$key' => '$item' ," . LINE_BREAK;
        }

        return $output;
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    public static function getTitle($slug = null)
    {
        //@TODO: IMPORTANT NOTE: Name 'title' was not a good idea for this method. Usages must be found and repaired.

        $model = static::grabSlug($slug);
        if ($model and $model->exists) {
            return $model->title;
        } else {
            return null;
        }
    }
}
