<?php

namespace Modules\Posts\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Lang;
use Modules\Yasna\Services\YasnaModel;

class Input extends YasnaModel
{
    use SoftDeletes;
    protected static $available_data_types = [
         'text',
         'textarea',
         'boolean',
         'date',
         'photo',
         'array',
         'file',
         'combo',
         'color-picker',
    ];
    protected static $available_types      = ['upstream', 'downstream', 'editor'];
    public $reserved_slugs       = 'root,super,all,dev,developer,admin';
    protected $guarded              = ['id'];


    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posttypes()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . "Posttype");
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function features()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . "Feature");
    }



    /*
    |--------------------------------------------------------------------------
    | Assessors
    |--------------------------------------------------------------------------
    |
    */
    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return " $this->title ($this->slug) ";
    }



    /**
     * @return string
     */
    public function getTypeTitleAttribute()
    {
        return trans("posts::inputs.type.$this->type");
    }



    /**
     * @return string
     */
    public function getDataTypeTitleAttribute()
    {
        return trans("manage::forms.data_type.$this->data_type");
    }



    /**
     * @return string
     */
    public function getRequiredAttribute()
    {
        return boolval(str_contains(strtolower($this->validation_rules), 'required'));
    }



    /**
     * @return string
     */
    public function getSeederAttribute()
    {
        $output = null;
        $array  = [
             'slug'              => $this->slug,
             'title'             => $this->title,
             'order'             => $this->order,
             'type'              => $this->type,
             'data_type'         => $this->data_type,
             'css_class'         => $this->css_class,
             'validation_rules'  => $this->validation_rules,
             'purifier_rules'    => $this->purifier_rules,
             'default_value'     => $this->default_value,
             'default_value_set' => $this->default_value_set,
             'hint'              => $this->hint,
             'meta-options'      => $this->meta('options'),
        ];

        foreach ($array as $key => $item) {
            $output .= "'$key' => '$item' ," . LINE_BREAK;
        }

        return $output;
    }



    /**
     * @return array
     */
    public function getOptionsArrayAttribute()
    {
        $options = $this->getMeta('options');

        /*-----------------------------------------------
        | Bypass ...
        */
        if ($this->data_type != 'combo') {
            return [];
        }

        /*-----------------------------------------------
        | If a method is given...
        | Format: ModuleName::methodName
        */
        if (str_contains($options, "::")) {
            return module(str_before($options, "::"))->provider(str_after($options, "::"));
        }


        /*-----------------------------------------------
        | Primary Explosion ...
        */
        $array1 = array_filter(explode("\r\n", $options));

        /*-----------------------------------------------
        | Further Inside ...
        */
        $final = [];
        foreach ($array1 as $option) {
            $id    = trim(str_before($option, ":"));
            $title = trim(str_after($option, ":"));

            if ($id == '-') {
                $id    = "";
                $title = "";
            } elseif ($title == "trans") {
                $title = trans("validation.attributes.$id");
            }

            $final[] = compact('id', 'title');
        }

        return $final;
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    /**
     * @return array
     */
    public function mainMetaFields()
    {
        return ['options'];
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    /**
     * @return array
     */
    public static function dataTypes()
    {
        $return = [];
        foreach (self::$available_data_types as $data_type) {
            $trans = "manage::forms.data_type.$data_type";
            if (Lang::has($trans)) {
                $caption = trans($trans);
            } else {
                $caption = $data_type;
            }
            array_push($return, [$data_type, $caption]);
        }

        return $return;
    }



    public static function inputTypes()
    {
        $return = [];
        foreach (self::$available_types as $type) {
            $trans = "posts::inputs.type.$type";
            if (Lang::has($trans)) {
                $caption = trans($trans);
            } else {
                $caption = $type;
            }
            $return[] = [$type, $caption];
        }

        return $return;
    }
}
