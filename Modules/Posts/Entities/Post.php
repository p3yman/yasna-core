<?php

namespace Modules\Posts\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Posts\Entities\Traits\PostAttachmentsTrait;
use Modules\Posts\Entities\Traits\PostAuthorTrait;
use Modules\Posts\Entities\Traits\PostDomainsTrait;
use Modules\Posts\Entities\Traits\PostElectorTrait;
use Modules\Posts\Entities\Traits\PostImageTrait;
use Modules\Posts\Entities\Traits\PostInputTrait;
use Modules\Posts\Entities\Traits\PostInterrelationTrait;
use Modules\Posts\Entities\Traits\PostLinksTrait;
use Modules\Posts\Entities\Traits\PostLocalesTrait;
use Modules\Posts\Entities\Traits\PostMetaTrait;
use Modules\Posts\Entities\Traits\PostPermissionsTrait;
use Modules\Posts\Entities\Traits\PostPosttypeTrait;
use Modules\Posts\Entities\Traits\PostRegisterTrait;
use Modules\Posts\Entities\Traits\PostResourcesTrait;
use Modules\Posts\Entities\Traits\PostSelfRelationTrait;
use Modules\Posts\Entities\Traits\PostStatusTrait;
use Modules\Yasna\Services\YasnaModel;

class Post extends YasnaModel
{
    use SoftDeletes;
    use PostDomainsTrait;
    use PostMetaTrait;
    use PostInputTrait;
    use PostPosttypeTrait;
    use PostPermissionsTrait;
    use PostLinksTrait;
    use PostInterrelationTrait;
    use PostLocalesTrait;
    use PostRegisterTrait;
    use PostImageTrait;
    use PostAttachmentsTrait;
    //use PostSelectorTrait;
    use PostElectorTrait;
    use PostStatusTrait;
    use PostSelfRelationTrait;
    use PostResourcesTrait;
    use PostAuthorTrait;

    public static    $reserved_slugs = "none,without";
    protected static $search_fields  = ['title', 'slug'];
    protected        $guarded        = ['id'];
    protected        $casts          = [
         'is_draft'   => "boolean",
         'is_limited' => "boolean",
         'meta'       => "array",
    ];



    /*
    |--------------------------------------------------------------------------
    | Assessors
    |--------------------------------------------------------------------------
    |
    */
    public function getSafeTitleAttribute()
    {
        if ($this->has('long_title')) {
            $this->title = $this->long_title;
        }

        return str_limit($this->title, 200);
    }



    public function getEditorMoodAttribute()
    {
        if (!$this->exists) {
            $mood = 'new';
        } elseif ($this->copy_of) {
            $mood = 'copy';
        } else {
            $mood = 'original';
        }

        return $mood;
    }



    public function getNormalizedSlugAttribute()
    {
        return self::normalizeSlug($this->id, $this->type, $this->locale, $this->slug);
    }



    public function getPinnedAttribute()
    {
        return boolval($this->pinned_at);
    }


    //	public function getAbstractAttribute($original_value)
    //	{
    //		if($original_value) {
    //			return $original_value;
    //		}
    //
    //		return str_limit($this->text, 200);
    //
    //	}


    public function getTemplateAttribute($original_value)
    {
        if ($original_value and $this->has('template_choice')) {
            return $original_value;
        } else {
            return $this->posttype->template;
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    public function isOwner()
    {
        if (!$this->exists) {
            return true;
        }

        return user()->id == $this->owned_by;
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */


    public static function normalizeSlug($post_id, $post_type, $post_locale, $slug)
    {
        //preparations...
        $found_a_unique_slug = false;
        $tries               = 1;

        //Invalid Patterns...
        if (!$slug) {
            return '';
        }

        $slug = str_slug(str_limit($slug, 100));

        //General Corrections...
        if (str_contains("01234567890-_", $slug[0])) {
            $slug = "p" . $slug;
        }

        $purified_original_slug = $slug;
        if (in_array($slug, explode(',', self::$reserved_slugs))) {
            $tries++;
            $slug .= "-" . strval($tries);
        }


        //loop...
        while (!$found_a_unique_slug) {
            $used = self::where('id', '!=', $post_id)
                        ->where('type', $post_type)
                        ->where('locale', $post_locale)
                        ->where('slug', $slug)
                        ->where('copy_of', 0)
                        ->withTrashed()
                        ->count()
            ;
            if ($used) {
                $tries++;
                $slug = $purified_original_slug . "-" . strval($tries);
            } else {
                $found_a_unique_slug = true;
            }
        }

        //return...
        return $slug;
    }



    public function unpublish()
    {
        return $this->update([
             'published_at' => null,
             'published_by' => 0,
             'moderated_by' => 0,
             'moderated_at' => null,
        ]);
    }



    public function deleteWithCopies()
    {
        $this->copies()->delete();
        return $this->delete();
    }
}
