<?php

namespace Modules\Posts\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Posts\Entities\Traits\PosttypeSelectizeTrait;
use Modules\Posts\Entities\Traits\PosttypeFeaturesTrait;
use Modules\Posts\Entities\Traits\PosttypeInputsTrait;
use Modules\Posts\Entities\Traits\PosttypeLocalesTrait;
use Modules\Posts\Entities\Traits\PosttypeMetaTrait;
use Modules\Posts\Entities\Traits\PosttypePermissionsTrait;
use Modules\Posts\Entities\Traits\PosttypePostsTrait;
use Modules\Yasna\Services\YasnaModel;

class Posttype extends YasnaModel
{
    use SoftDeletes;
    use PosttypeFeaturesTrait;
    use PosttypeSelectizeTrait;
    use PosttypeInputsTrait;
    use PosttypePostsTrait;
    use PosttypePermissionsTrait;
    use PosttypeMetaTrait;
    use PosttypeLocalesTrait;

    public    $available_templates = ['album', 'post', 'postak', 'product', 'slideshow', 'dialogue', 'faq', 'special'];
    public    $reserved_slugs      = 'root,admin,upstream';
    protected $guarded             = ['id'];



    /*
    |--------------------------------------------------------------------------
    | Accessors and Mutators
    |--------------------------------------------------------------------------
    |
    */

    public function getEncryptedSlugAttribute()
    {
        return Crypt::encrypt($this->slug);
    }



    public function getDefaultLocaleAttribute()
    {
        return $this->locales_array[0];
    }



    public function getSeederAttribute()
    {
        $output = null;
        $array  = [
             'slug'           => $this->slug,
             'title'          => $this->title,
             'order'          => $this->order,
             'singular_title' => $this->singular_title,
             'template'       => $this->template,
             'icon'           => $this->icon,
        ];

        foreach ($array as $key => $item) {
            $output .= "'$key' => '$item' ," . LINE_BREAK;
        }

        return $output;
    }



    public function getCreateTitleAttribute()
    {
        return trans('manage::forms.button.add_to') . ' ' . $this->title;
    }



    public function getCreateLinkAttribute()
    {
        return url("manage/posts/$this->slug/create/all");
    }



    /*
    |--------------------------------------------------------------------------
    | Selectors
    |--------------------------------------------------------------------------
    |
    */
    public static function getAll()
    {
        return self::orderBy('title')->get();
    }



    public static function selector($parameters = [])
    {
        $switch = array_normalize($parameters, [
             'locales'      => false, // <~~ Supports Arrays
             'template'     => false,
             'header_title' => false,
             'features'     => false, // <~~ Supports Arrays
             'criteria'     => false,
             'converted'    => false,
        ]);

        $table = self::where('id', '>', '0');

        /*-----------------------------------------------
        | Simple Things ...
        */
        if ($switch['template'] !== false) {
            $table->where('template', $switch['template']);
        }
        if ($switch['header_title'] !== false) {
            $table->where('header_title', $switch['header_title']);
        }
        if ($switch['converted']) {
            $table->where('converted', boolval($switch['converted']));
        }

        /*-----------------------------------------------
        | features ...
        */
        if ($switch['features'] !== false and $switch['features'] != 'all') {
            $features = (array)$switch['features'];
            $table->whereHas('features', function ($query) use ($features) {
                $query->whereIn('features.slug', $features);
            });
        }

        /*-----------------------------------------------
        | Locales ...
        */
        if ($switch['locales'] !== false) {
            $table->where(function ($query) use ($switch) {
                $query->where('id', '0');

                foreach ($switch['locales'] as $locale) {
                    $query->orWhere('locales', 'like', "%$locale%");
                }
            });
        }

        /*-----------------------------------------------
        | Criteria ...
        */
        if ($switch['criteria'] !== false) {
            switch ($switch['criteria']) {
                case 'all':
                    break;

                case 'all_with_trashed':
                case 'with_trashed':
                    $table = $table->withTrashed();
                    break;

                case 'bin':
                case 'only_trashed':
                    $table = $table->onlyTrashed();
                    break;

                default:
                    $table = $table->where('id', '0');
                    break;

            }
        }


        /*-----------------------------------------------
        | Return ...
        */

        return $table;
    }



    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */
    /**
     * Syncs posttype attachments, by managing its features, inputs, and the relations in between.
     *
     * @param string $features
     * @param string $inputs
     * @param string $old_slug
     *
     * @return void
     */
    public function syncAttachments($features, $inputs, $old_slug)
    {
        $this->syncFeatures(explode_not_empty(',', $features));
        $this->syncInputs(explode_not_empty(',', $inputs));
        $this->attachRequiredInputs();
        $this->updateSlugUsages($old_slug);
    }



    public function updateSlugUsages($old_slug)
    {
        $new_slug = $this->slug;
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($old_slug == $new_slug) {
            return true;
        }

        /*-----------------------------------------------
        | Action ...
        */
        $posts_updated = Post::where('type', $old_slug)->update([
             'type' => $new_slug,
        ])
        ;

        /*-----------------------------------------------
        | Return ...
        */

        return $posts_updated;
    }



    public function normalizeRequestLocale($request_locale)
    {
        if (!$request_locale or !in_array($request_locale, $this->locales_array)) {
            return $this->locales_array[0];
        } else {
            return $request_locale;
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */
    public function templatesCombo()
    {
        $array = [];
        foreach ($this->available_templates as $template) {
            array_push($array, [
                 $template,
                 trans("posts::types.templates.$template"),
            ]);
        }

        return $array;
    }
}
