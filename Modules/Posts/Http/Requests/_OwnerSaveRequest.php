<?php

namespace Modules\Posts\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;

class _OwnerSaveRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            $this->field => "required",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $value       = parent::all();
        $this->field = config("auth.providers.users.field_name");

        $purified = ValidationServiceProvider::purifier($value, [
            $this->field => "ed|lower",
        ]);

        return $purified;
    }
}
