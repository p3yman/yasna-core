<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PostDeleteRequest extends YasnaRequest
{
    protected $model_name = "Post";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canDelete();
    }
}
