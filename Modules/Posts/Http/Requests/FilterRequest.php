<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;


class FilterRequest extends YasnaRequest
{
    protected $model_name = "";



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->filterRequest();
        $this->sortValidation();
    }



    /**
     * unset key have start with underline from request array
     *
     * @return void
     */
    public function filterRequest()
    {
        foreach ($this->data as $key => $value) {
            if (starts_with($key, "_")) {
                $this->unsetData($key);
            }
        }
    }



    /**
     * sort validation
     *
     * @return bool
     */
    public function sortValidation()
    {
        if ($this->sort) {
            $sort = $this->sort;
            if ($sort != 'asc' or $sort != 'desc') {
                return false;
            }
        }
    }

}
