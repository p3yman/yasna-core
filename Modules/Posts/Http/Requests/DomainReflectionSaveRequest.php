<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DomainReflectionSaveRequest extends YasnaRequest
{
    protected $model_name = "Post";



    public function authorize()
    {
        return $this->model->canEdit();
    }



    public function corrections()
    {
        $this->data['domains'] = $this->selectedDomains();
    }



    public function selectedDomains()
    {
        $array = [];

        foreach ($this->data['domains'] as $key => $value) {
            if ($value == 'on') {
                $id = hashid($key);
                if ($this->model->domain_id != $id) {
                    $array[] = $id;
                }
            }
        }

        return $array;
    }
}
