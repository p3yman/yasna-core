<?php

namespace Modules\Posts\Http\Requests\V1;

use App\Models\Posttype;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;


class PostsListRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $should_load_model = true;

    /**
     * @inheritdoc
     */
    protected $should_load_model_with_slug = true;

    /**
     * @inheritdoc
     */
    protected $model_name = "Post";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "type"  => "required",
             "limit" => "numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function getModelInstance()
    {
        return posttype($this->getData("type"));
    }



    /**
     * @inheritdoc
     */
    public function modelRequested(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->limitCriteria();
        $this->applyCurrentLocale();
    }



    /**
     * limit criteria to the published ones, when the user has not enough privileges.
     */
    private function limitCriteria()
    {
        /** @var Posttype $model */
        $model = $this->model;

        if (!user()->exists or $model->cannot("browse")) {
            $this->setData("criteria", "published");
        }
    }



    /**
     * apply current locale if necessary
     */
    private function applyCurrentLocale()
    {
        if($this->getData("locale") == "*") {
            $this->unsetData("locale");
            return;
        }

        if($this->isset("locale") or $this->isset("locales")) {
            return;
        }

        $this->setData("locale", getLocale());
    }
}
