<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/18/18
 * Time: 10:02 AM
 */

namespace Modules\Posts\Http\Requests\V1;


use App\Models\Post;
use Modules\Yasna\Services\YasnaRequest;


/**
 * Class PostSingleRequest
 *
 * @property Post $model
 */
class PostSingleRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = 'post';

    /** @var string */
    protected $responder = 'white-house';

    /** @var int */
    protected $error_code_when_authorization_fails = 404;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return ($this->modelExists() and $this->modelIsViewable());
    }



    /**
     * If the requested model exists.
     *
     * @return bool
     */
    protected function modelExists()
    {
        return $this->model->exists;
    }



    /**
     * If the logged in user is allowed to view the requested model.
     *
     * @return bool
     */
    protected function modelIsViewable()
    {
        $model = $this->model;

        return (
             $model->isPublished()
             or
             $model->can('browse')
        );
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        if (!$this->model_name) {
            return;
        }

        $identifer           = $this->getRequestedModelId($id_or_hashid) ?: $this->getData('slug');
        $this->data['id']    = $identifer;
        $this->data['model'] = $this->model = model($this->model_name, $this->data['id'], $this->model_with_trashed);
        $this->refillRequestHashid();
        $this->failIfModelNotExist();
    }
}
