<?php

namespace Modules\Posts\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Input;
use Modules\Yasna\Providers\ValidationServiceProvider;

class PostSaveRequest2 extends FormRequest
{
    protected $inputs;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /*-----------------------------------------------
        | Preparations ...
        */
        $input = $this->all();
        $rules = [];

        /*-----------------------------------------------
        | Normal Rules ...
        */
        switch ($input['_submit']) {
            case 'delete':
            case 'delete_original':
            case 'unpublish':
                break;

            case 'publish':
            case 'approval':
            case 'save':
                $rules = [
                    'title' => "required",
                ];
                if (isset($input['_schedule']) and $input['_schedule']) {
                    $rules = array_merge($rules, [
                        'publish_date'   => "required|date",
                        'publish_hour'   => "required|numeric|min:0|max:23",
                        'publish_minute' => "required|numeric|min:0|max:59",
                    ]);
                }

                break;

            case 'reject':
                $rules = [
                    'title' => "required",
                ];
                break;

        }

        /*-----------------------------------------------
        | Meta Rules ...
        */
        foreach ($this->inputs as $input) {
            $rules[ $input->slug ] = $input->validation_rules;
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function all($keys = null)
    {
        /*-----------------------------------------------
        | Preparations ...
        */
        $value        = parent::all();

        $hashids = $this->data['_meta_inputs'];
        if (is_string($hashids) and str_contains($hashids, ',')) {
            $hashids = array_filter(explode(',', $hashids));
        }

        $ids           = (array)hashid($hashids);
        $this->_inputs = model("input")->whereIn("id", $ids)->get();

        /*-----------------------------------------------
        | Normal Rules ...
        */
        $rules = [
            'id'             => "decrypt",
            'type'           => "decrypt",
            'publish_minute' => "ed",
            'publish_hour'   => "ed",
            'slug'           => "lower",
        ];

        /*-----------------------------------------------
        | Meta Rules ...
        */
        foreach ($this->inputs as $input) {
            $rules[ $input->slug ] = $input->purifier_rules;
        }

        /*-----------------------------------------------
        | Return ...
        */
        $purified = ValidationServiceProvider::purifier($value, $rules);

        return $purified;
    }
}
