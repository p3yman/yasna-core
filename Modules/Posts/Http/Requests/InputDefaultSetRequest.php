<?php

namespace Modules\Posts\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Posts\Entities\Input;
use Modules\Yasna\Providers\ValidationServiceProvider;

class InputDefaultSetRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'default_value' => $this->input->validation_rules ,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $value = parent::all() ;
        $this->input = model('input',$value['slug'], [
            'with_trashed' => true ,
        ]) ;
        $purified = ValidationServiceProvider::purifier($value, [
            'default_value' => $this->input->purifier_rules ,
        ]);
        return $purified ;
    }
}
