<?php

namespace Modules\Posts\Http\Requests;

use App\Models\Posttype;
use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\YasnaRequest;

class PostSaveRequest extends YasnaRequest
{
    /**
     * keep the instance of the posttype
     *
     * @var Posttype
     */
    public $_posttype;

    /**
     * keep the collection of editor inputs
     *
     * @var Collection
     */
    protected $inputs;

    /**
     * @inheritdoc
     */
    protected $model_name = "Post";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $authorizing_method = "authorize" . studly_case($this->_submit);

        if ($this->model->type != $this->_posttype->slug) {
            return false;
        }

        if (method_exists($this, $authorizing_method)) {
            return $this->$authorizing_method();
        } else {
            return false;
        }
    }



    /**
     * check whether the user is authorized to publish the post
     *
     * @return bool
     */
    protected function authorizePublish()
    {
        return $this->authorizeSave() and $this->model->canPublish();
    }



    /**
     * check whether the user is authorized to unpublish the post
     *
     * @return bool
     */
    protected function authorizeUnpublish()
    {
        return $this->model->canPublish();
    }



    /**
     * check whether the user is authorized to save the post
     *
     * @return bool
     */
    protected function authorizeSave()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * check whether the user is authorized to delete the post
     *
     * @return bool
     */
    protected function authorizeDelete()
    {
        return $this->model->canDelete();
    }



    /**
     * check whether the user is authorized to delete the original post
     *
     * @return bool
     */
    protected function authorizeDeleteOriginal()
    {
        return $this->model->original()->canDelete();
    }



    /**
     * check whether the user is authorized to approve the pending post
     *
     * @return bool
     */
    protected function authorizeApproval()
    {
        return $this->authorizeSave();
    }



    /**
     * check whether the user is authorized to reject the pending post
     *
     * @return bool
     */
    protected function authorizeReject()
    {
        return $this->authorizePublish();
    }



    /**
     * get validation rules about titles
     *
     * @return array
     */
    protected function titlesRules()
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (in_array($this->_submit, ['delete', 'delete_original', 'unpublish'])) {
            return [];
        }

        /*-----------------------------------------------
        | Rule ...
        */
        return [
             "title" => "required",
        ];
    }



    /**
     * get validation rules about schedules
     *
     * @return array
     */
    protected function scheduleRules()
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (not_in_array($this->_submit, ['publish', 'approval', 'save'])) {
            return [];
        }
        if (!isset($this->data['_schedule']) or !$this->input(['_schedule'])) {
            return [];
        }

        /*-----------------------------------------------
        | Rule ...
        */
        return [
             'published_at' => "required|date",
        ];
    }



    /**
     * get validation rules about dynamic inputs
     *
     * @return array
     */
    protected function dynamicInputsRules()
    {
        $rules = [];

        foreach ($this->inputs as $input) {
            $rules[$input->slug] = $input->validation_rules;
        }

        return $rules;
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        $constant_rules = $this->purifierFixedRules();
        $dynamic_rules  = $this->purifierDynamicRules();

        return array_merge($constant_rules, $dynamic_rules);
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $array1 = trans("validation.attributes");
        $array2 = [];

        foreach ($this->inputs as $input) {
            $array2[$input->slug] = $input->title;
        }

        return array_merge($array1, $array2);
    }



    /**
     * purifier for dynamic rules
     *
     * @return array
     */
    private function purifierDynamicRules()
    {
        $array = [];

        foreach ($this->inputs as $input) {
            $array[$input->slug] = $input->purifier_rules;
        }

        return $array;
    }



    /**
     * purifier for fixed rules
     *
     * @return array
     */
    private function purifierFixedRules()
    {
        return [
             'publish_minute'     => "ed",
             'publish_hour'       => "ed",
             'slug'               => "lower",
             'published_at'       => "date",
             'event_starts_at'    => "date",
             'event_ends_at'      => "date",
             'register_starts_at' => "date",
             'register_ends_at'   => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctSchedule();
    }



    /**
     * Corrects the schedule information.
     */
    protected function correctSchedule()
    {
        if (isset($this->data['_schedule']) and $this->input(['_schedule'])) {
            return;
        }

        $this->unsetData('published_at');
    }



    /**
     * @inheritdoc
     */
    public function correctionsBeforePurifier()
    {
        $this->discoverRequestPosttype();
        $this->enrichNewModels();
        $this->resolveAllowedInputs();
    }



    /**
     * resolve a collection of allowed inputs
     */
    private function resolveAllowedInputs()
    {
        $this->inputs = $this->_posttype->editorInputs()->get();
    }



    /**
     * enrich the new model instances to house posttype and locale handles
     */
    private function enrichNewModels()
    {
        if ($this->model->exists) {
            return;
        }

        $this->model->type   = $this->_posttype->slug;
        $this->model->locale = $this->data['locale'];
    }



    /**
     * discover requested posttype
     */
    private function discoverRequestPosttype()
    {
        $this->data['type'] = decrypt($this->data['type']);
        $this->_posttype    = posttype($this->data['type']);
    }
}
