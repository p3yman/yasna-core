<?php

namespace Modules\Posts\Http\Requests;

use Modules\Posts\Entities\Post;
use Modules\Yasna\Services\YasnaRequest;

class PostakRequest extends YasnaRequest
{
    protected $model_name = "Post";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'slug'            => 'string',
             'postak_hashid'   => 'array|min:1',
             'postak_hashid.*' => 'string|nullable',
             'postak_title'    => 'array|min:1',
             'postak_title.*'  => 'string|nullable',
             'postak_text'     => 'array',
             'postak_text.*'   => 'string|nullable',
        ];
    }



    /**
     * Return a post model based on current locale as the main post model
     *
     * @param string|null $locale
     *
     * @return Post
     */
    public function mainPostModel(?string $locale): ?Post
    {
        // get the locale
        if (is_null($locale)) {
            $locale = getLocale();
        }

        // get information of post based on locale
        $information = $this->getLocalePost($locale);

        // return post model
        return $this->getPostModel($information);
    }



    /**
     * Return all sister models of a given set of locales. The logic for selecting the sister locales is based on this
     * fact that we suppose the current locale of the user as the main locale and other than it, for us is a sister
     * locale.
     *
     * @param string $main_locale
     * @param array  $locales
     *
     * @return array
     */
    public function sisterPostModels(string $main_locale, array $locales): array
    {
        // get sisters post information
        $sisters_information = $this->getSistersInformation($main_locale, $locales);

        // get sister models
        $models = $this->getSisterModels($sisters_information);

        // and return result
        return $models;
    }



    /**
     * Return information of a post on a specified locale
     *
     * @param $locale
     *
     * @return array
     */
    public function getLocalePost($locale): array
    {
        // if there is no title, there will be no post
        if (!$this->input("postak_title.{$locale}")) {
            return [];
        }

        return [
             'slug'   => $this->get('slug'),
             'hashid' => $this->input("postak_hashid.{$locale}"),
             'title'  => $this->input("postak_title.{$locale}"),
             'text'   => $this->input("postak_text.{$locale}"),
             'locale' => $locale,
        ];
    }



    /**
     * Set values of a post model based on an information array
     *
     * @param Post  $post
     * @param array $information
     */
    public function updatePostValues(Post $post, array $information)
    {
        $information = $this->informationNormalizer($information);

        $post->slug   = $information['slug'];
        $post->title  = $information['title'];
        $post->text   = $information['text'];
        $post->locale = $information['locale'];
    }



    /**
     * Return a post model corresponded to a given set of information
     *
     * @param array $information
     *
     * @return Post
     */
    private function getPostModel(array $information): ?Post
    {
        // first check if information is provided
        if (empty($information)) {
            return null;
        }

        // normalize information
        $information = $this->informationNormalizer($information);

        // get a post model based on hashid
        $post = model('post', $information['hashid']);

        // insert/update information of post
        $this->updatePostValues($post, $information);

        // return model
        return $post;
    }



    /**
     * Generate normalized information array of a post from a given information array
     *
     * @param $information
     *
     * @return array
     */
    private function informationNormalizer($information): array
    {
        $default = [
             'hashid' => null,
             'slug'   => null,
             'title'  => null,
             'text'   => null,
             'locale' => getLocale(),
        ];

        return array_normalize($information, $default);
    }



    /**
     * Return information array of sisters of current locale
     *
     * @param string $main_locale
     * @param array  $locales
     *
     * @return array
     */
    private function getSistersInformation(string $main_locale, array $locales): array
    {
        // get sister locales
        $sister_locales = array_diff($locales, [$main_locale]);

        // generate information array for each locale and return it
        return array_map(
             function ($sister_locale) {
                 return $this->getLocalePost($sister_locale);
             },
             $sister_locales
        );
    }



    /**
     * Return an array of sister models corresponded to a given set of information
     *
     * @param array $information
     *
     * @return array
     */
    private function getSisterModels(array $information): array
    {
        return array_map(
             function ($sister_information) {
                 // return post model
                 return $this->getPostModel($sister_information);
             },
             $information
        );
    }
}
