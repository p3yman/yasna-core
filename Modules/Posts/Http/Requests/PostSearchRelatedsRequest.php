<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PostSearchRelatedsRequest extends YasnaRequest
{
    protected $model_name         = 'Post';
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'search' => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $post = $this->model;

        return ($post->exists and $post->has('related_posts'));
    }
}
