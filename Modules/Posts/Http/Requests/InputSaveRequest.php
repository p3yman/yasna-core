<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class InputSaveRequest extends YasnaRequest
{
    protected $model_name = "Input";



    public function rules()
    {
        $id             = $this->data['id'];
        $reserved_slugs = $this->model->reserved_slugs;

        return [
             'slug'    => "required|alpha_dash|not_in:$reserved_slugs|unique:inputs,slug,$id,id",
             'title'   => "required|unique:roles,title,$id,id",
             'order'   => "required|numeric|min:1",
             'options' => "required_if:data_type,combo",
        ];
    }



    public function purifier()
    {
        return [
             'slug'  => "lower|ed",
             'order' => "ed",
        ];
    }
}
