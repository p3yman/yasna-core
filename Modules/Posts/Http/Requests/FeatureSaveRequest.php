<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class FeatureSaveRequest extends YasnaRequest
{
    protected $model_name = "Feature";



    public function rules()
    {
        $id             = $this->data['id'];
        $reserved_slugs = model('Feature')->reserved_slugs;

        return [
             'slug'  => "required|alpha_dash|not_in:$reserved_slugs|unique:roles,slug,$id,id",
             'title' => "required|unique:roles,title,$id,id",
             'order' => "required|numeric|min:1",
        ];
    }



    public function purifier()
    {
        return [
             'slug'  => "lower|ed",
             'order' => "ed",
        ];
    }



    public function corrections()
    {
        $this->data['icon'] = str_after($this->data['icon'], 'fa-');
    }
}
