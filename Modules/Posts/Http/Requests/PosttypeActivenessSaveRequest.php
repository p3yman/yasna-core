<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PosttypeActivenessSaveRequest extends YasnaRequest
{
    protected $model_name = "Posttype";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return !boolval($this->model->slug == 'root');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "_submit" => "in:delete,restore",
        ];
    }
}
