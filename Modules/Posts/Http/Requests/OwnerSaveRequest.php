<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class OwnerSaveRequest extends YasnaRequest
{
    public $username_field;
    public $user;
    protected $model_name = "Post";



    public function authorize()
    {
        return $this->model->canEdit();
    }



    public function MainRules()
    {
        return [
             $this->username_field => 'required',
        ];
    }



    public function rulesForUser()
    {
        if (!$this->user->exists) {
            return [
                "user" => "required" ,
            ];
        }

        return [];
    }



    public function messages()
    {
        return [
             "user.required" => trans_safe("users::forms.nobody_found") ,
        ];
    }


    public function purifier()
    {
        $this->username_field = config("auth.providers.users.field_name");

        return [
             $this->username_field => "ed|lower",
        ];
    }



    public function corrections()
    {
        $field_name = $this->username_field;
        $this->user = user()->findByUsername($this->data[$field_name]);
    }
}
