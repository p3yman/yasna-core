<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PosttypeSaveRequest extends YasnaRequest
{
    protected $model_name = "Posttype";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id                  = $this->data['id'];
        $reserved_slugs      = $this->model->reserved_slugs;
        $available_templates = $this->model->available_templates;

        return [
             'title'          => "required|unique:posttypes,title,$id",
             'singular_title' => 'required',
             'slug'           => "required|alpha_dash|forbidden_chars:_|not_in:$reserved_slugs|unique:posttypes,slug,$id,id",
             'template'       => "required|in:" . implode(',', $available_templates),
             'icon'           => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'slug'     => 'lower',
             'template' => 'lower',
             'icon'     => 'lower',
             'order'    => "ed",
        ];
    }
}
