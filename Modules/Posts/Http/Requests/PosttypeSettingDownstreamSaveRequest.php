<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PosttypeSettingDownstreamSaveRequest extends YasnaRequest
{
    protected $model_name = "Posttype";
    protected $inputs;



    public function authorize()
    {
        return $this->model->can('setting');
    }



    public function purifier()
    {
        $this->inputs = $this->model->setting_inputs;
        $rules        = [];

        foreach ($this->inputs as $input) {
            session()->put('test', $input->id);
            $rules[$input->slug] = $input->purifier_rules;
            if ($input->data_type == 'boolean') {
                $rules[$input->slug] = 'bool';
            }
        }

        return $rules;
    }



    public function rules()
    {
        $rules = [];

        foreach ($this->inputs as $input) {
            $rules[$input->slug] = $input->validation_rules . '';
        }

        return $rules;
    }
}
