<?php

namespace Modules\Posts\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class PosttypeTitlesSaveRequest extends YasnaRequest
{
    protected $model_name = "Posttype";



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['locale_titles'] = $this->generateLocaleTitlesArray();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'title'          => 'required',
             'singular_title' => 'required',
        ];
    }



    /**
     * Generates Locale Titles Array
     *
     * @return array
     */
    public function generateLocaleTitlesArray()
    {
        $data         = $this->data;
        $result_array = [];

        foreach ($this->model->locales_array as $locale) {
            if ($locale == $this->model->defaultLocale()) {
                continue;
            }

            $result_array["title-$locale"]          = $data["_title_in_$locale"];
            $result_array["singular_title-$locale"] = $data["_singular_title_in_$locale"];
        }

        return $result_array;
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'title',
             'singular_title',
        ];
    }
}
