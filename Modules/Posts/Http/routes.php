<?php

/*
|--------------------------------------------------------------------------
| Upstream Settings
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:developer'],
          'prefix'     => 'manage/posts/upstream',
          'namespace'  => 'Modules\Posts\Http\Controllers',
     ],
     function () {
         Route::get('/posttype/{action}/{hash_id?}', 'PosttypeUpstreamController@action');
         Route::get('/feature/{action}/{hash_id?}', 'UpstreamController@featureAction');
         Route::get('/input/{action}/{hash_id?}', 'UpstreamController@inputAction');


         Route::group(['prefix' => 'save'], function () {
             Route::post('/posttype', 'PosttypeUpstreamController@save');
             Route::post('/posttype-activeness', 'PosttypeUpstreamController@saveActiveness');
             Route::post('/posttype-titles', 'PosttypeUpstreamController@saveTitles');

             Route::post('/feature', 'UpstreamController@featureSave');
             Route::post('/feature-activeness', 'UpstreamController@featureActiveness');

             Route::post('/input', 'UpstreamController@inputSave');
             Route::post('/input-default', 'UpstreamController@inputDefault');
             Route::post('/input-activeness', 'UpstreamController@inputActiveness');
         });
     }
);

/*
|--------------------------------------------------------------------------
| Downstream
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth'], //@TODO: permission
          'prefix'     => 'manage/posts/downstream',
          'namespace'  => 'Modules\Posts\Http\Controllers',
     ],
     function () {
         Route::get('/posttype/{action}/{hash_id?}', 'DownstreamController@posttypeAction');


         Route::group(['prefix' => 'save'], function () {
             Route::post('/posttype', 'DownstreamController@posttypeSave')->name('posttype-downstream-save');
         });
     }
);


/*
|--------------------------------------------------------------------------
| Manage Side
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin'],
          'prefix'     => 'manage/posts/',
          'namespace'  => module('posts')->controller(),
     ],
     function () {
         Route::get('/filter', 'PostsManageController@postFilter');

         Route::get('/update/{item_id}', 'PostsManageController@update');
         //Route::get('/tab_update/{posttype}/{request_tab?}/{switches?}', 'PostsManageController@tabUpdate');

         Route::get('/act/{model_id}/{action}/{option?}', 'PostsManageController@singleAction');
         Route::get('/editor-act/{model_id}/{action}/{option?}', 'PostsEditController@singleAction');
         Route::get('/check_slug/{id}/{type}/{locale}/{slug?}/p', 'PostsEditController@checkSlug');

         Route::get('/{posttype}', 'PostsManageController@browse');
         Route::get('/{posttype}/create/{locale?}/{sisterhood?}', 'PostsEditController@create');
         Route::get('{posttype}/edit/{post_id}', 'PostsEditController@editor');
         Route::get('{hashid}/relateds/search', 'PostsEditController@searchRelateds')->name('posts.relateds.search');
         Route::get('{posttype}/{locale}/search', 'PostsManageController@search');
         Route::get('/{posttype}/{request_tab?}/{switches?}', 'PostsManageController@browse');

         Route::group(['prefix' => 'save'], function () {
             Route::post('/', 'PostsSaveController@save')->name('post-save');
             Route::post('/delete', 'PostsDeleteController@delete')->name('post-save-delete');
             Route::post('/undelete', 'PostsDeleteController@undelete')->name('post-save-undelete');
             Route::post('/destroy', 'PostsDeleteController@destroy')->name('post-save-destroy');
             Route::post('/clone', 'PostsManageController@saveClone')->name('post-save-clone');
             //Route::post('/clone', 'PostsManageController@saveLocales')->name('post-save-clone');
             Route::post('/delete-mass', 'PostsDeleteController@massDelete')->name('post-save-delete-mass');
             Route::post('/undelete-mass', 'PostsDeleteController@massUndelete')->name('post-save-undelete-mass');
             Route::post('/destroyMass', 'PostsDeleteController@massDestroy')->name('post-save-destroy-mass');
             Route::post('/owner', 'PostsManageController@saveOwner')->name('post-save-owner');
             Route::post('/pin', 'PostsManageController@savePin')->name('post-save-pin');
             Route::post('/domain-reflections', 'PostsManageController@saveDomainReflections')
                  ->name('post-domain-reflections')
             ;

         });
     }
);


/*
|--------------------------------------------------------------------------
| Minimal Posts
|--------------------------------------------------------------------------
|
*/
Route::group(
     [
          'middleware' => ['web', 'auth', 'is:admin'],
          'prefix'     => 'manage/postaks/',
          'namespace'  => 'Modules\Posts\Http\Controllers',
     ],
     function () {
         Route::get('/update/{hashid}', 'PostaksManageController@update')->name('posts.postak.update');
         Route::get('/{posttype}', 'PostaksManageController@browse')->name('posts.postak.browse');
         Route::get('/{posttype}/create', 'PostaksEditController@create')->name('posts.postak.create');
         Route::get('/{posttype}/edit/{hashid}', 'PostaksEditController@edit')->name('posts.postak.edit');
         Route::post('/{posttype}/save', 'PostaksSaveController@save')->name('posts.postak.save');
         Route::get('/{posttype}/delete/{hashid}', 'PostaksDeleteController@deleteAction')
              ->name('posts.postak.delete_action')
         ;
         Route::post('/{posttype}/delete/{hashid}', 'PostaksDeleteController@delete')->name('posts.postak.delete');
         Route::get('/{posttype}/restore/{hashid}', 'PostaksDeleteController@undeleteAction')
              ->name('posts.postak.undelete_action')
         ;
         Route::post('/{posttype}/restore/{hashid}', 'PostaksDeleteController@undelete')->name('posts.postak.undelete');
         Route::get('/{posttype}/destroy/{hashid}', 'PostaksDeleteController@destroyAction')
              ->name('posts.postak.destroy_action')
         ;
         Route::post('/{posttype}/destroy/{hashid}', 'PostaksDeleteController@destroy')->name('posts.postak.destroy');
         Route::get('{posttype}/{locale}/search', 'PostaksManageController@search');
         Route::get('/mass/{action}/{option?}', 'PostaksManageController@massAction');
         Route::post('/delete-mass', 'PostaksDeleteController@massDelete')->name('posts.postak.mass_delete');
         Route::post('/undelete-mass', 'PostaksDeleteController@massUndelete')->name('posts.postak.mass_undelete');
         Route::post('/destroyMass', 'PostaksDeleteController@massDestroy')->name('posts.postak.mass_destroy');
         Route::get('/{posttype}/{request_tab?}/{switches?}', 'PostaksManageController@browse');
     }
);
