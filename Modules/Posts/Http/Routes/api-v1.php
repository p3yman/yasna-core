<?php

Route::group([
    'middleware' => 'web', 
    'namespace'  => module('Posts')->getControllersNamespace("Api\\V1"),
    'prefix'     => 'api/v1/posts'
], function () {
    Route::get('query' , 'PostsController@getPosts');
    Route::get('/{hashid}' , 'PostsController@getPost');
});
