<?php

Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'namespace'  => module('Posts')->getControllersNamespace(),
     'prefix'     => 'manage/slideshows',
], function () {
    Route::get("/{posttype}", "SlideshowsController@showPage");
    Route::post('/save', "SlideshowsController@saveSlides");
    Route::post('/delete', "SlideshowsController@deleteSlide");
    Route::post('/image/delete', "SlideshowsController@deleteImage");
});

Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'namespace'  => module('Posts')->getControllersNamespace(),
     'prefix'     => '/slideshows',
], function () {
    Route::get("/date-picker","SlideshowsController@getDatePicker");
});
