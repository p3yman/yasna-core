<?php

namespace Modules\Posts\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/posts-types
 *                    Post Type
 * @apiDescription    Get all posttype and posttype with feature name
 * @apiVersion        1.0.0
 * @apiName           Post Type
 * @apiGroup          Posts
 * @apiPermission     none
 * @apiParam {String}  [feature]  name of the posttype feature
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *          "id": "qKXVA",
 *          "title": "my_Slideshow_Name",
 *          "slug": "slides",
 *          "icon": "bug",
 *          "locales": null,
 *          "template": "slideshow",
 *          "header_title": "",
 *          "created_at": 1545552635,
 *          "updated_at": 1545552829,
 *          "deleted_at": null,
 *          "updated_by": "qKXVA",
 *          "created_by": "qKXVA",
 *          "deleted_by": null
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Posts\Http\Controllers\V1\PostsController controller()
 */
class TypesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get All PostTypes";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Posts\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostsController@postTypes';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"           => "qKXVA",
             "title"        => "mySlideShowName",
             "slug"         => "slides",
             "icon"         => "bug",
             "locales"      => null,
             "template"     => "slideshow",
             "header_title" => "",
             "created_at"   => 1545552635,
             "updated_at"   => 1545552829,
             "deleted_at"   => null,
             "updated_by"   => "qKXVA",
             "created_by"   => "qKXVA",
             "deleted_by"   => null,
        ], [
             "authenticated_user" => "PKEnN",
        ]);
    }
}
