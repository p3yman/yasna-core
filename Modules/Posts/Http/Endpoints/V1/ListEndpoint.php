<?php

namespace Modules\Posts\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Posts\Http\Controllers\V1\PostsController;

/**
 * @api               {GET}
 *                    /api/modular/v1/posts-list
 *                    Posts List
 * @apiDescription    A list of posts with every setting required to display
 * @apiVersion        1.0.0
 * @apiName           Posts List
 * @apiGroup          Posts
 * @apiPermission     none
 * @apiParam {String}   [foo]           whatever field with electors already set can be passed.
 * @apiParam {int=0,1}  [paginated=0]   Set the result must be paginated or not
 * @apiParam {int}      [per_page=15]   Set the number of results on each request
 * @apiParam {int}      [page=1]        Set the page number on pagination
 * @apiParam {int}      [limit=100]     Set limit to be applied on the number of posts to be retrieved
 * @apiParam {string}   [sort=id]       Set the order field
 * @apiParam {string=desc,asc} [order=desc] Set the order ordination
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "posttype": {
 *              "slideshow_auto": "0",
 *              "slideshow_pause": "0",
 *              "slideshow_speed": "20000",
 *              "slideshow_timeout": "200"
 *          },
 *          "total": 2,
 *          "per_page": 1,
 *          "last_page": 2,
 *          "current_page": 1,
 *          "next_page_url":
 *          "http://localhost:8000/api/modular/v1/posts-posts-list?type=slides&paginated=1&per_page=1&page=2",
 *          "previous_page_url": null,
 *          "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *          "id": "NDgwK",
 *          "slug": "dummy-simplicity",
 *          "type": "there",
 *          "title": "This is my title",
 *          "title2": "",
 *          "long_title": null,
 *          "is_draft": false,
 *          "locale": "fr",
 *          "copy_of": 0,
 *          "sisterhood": "8r9GVQqZaMl9",
 *          "abstract": "This is my abstract",
 *          "text": "This is my text",
 *          "template": "album",
 *          "featured_image": {
 *                  "0": "QKgJx",
 *                  "id": "QKgJx",
 *                  "original":
 *                  "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_original.jpeg",
 *                  "thumb":
 *                  "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_thumb.jpeg",
 *                  "posttype-DAvQN-thumb":
 *                  "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_posttype-DAvQN-thumb.jpeg"
 *          },
 *          "attached_files": null,
 *          "event_starts_at": null,
 *          "event_ends_at": null,
 *          "register_starts_at": null,
 *          "register_ends_at": null,
 *          "pinned_at": null,
 *          "pinned_by": 0,
 *          "meta": null,
 *          "created_at": 1547536150,
 *          "updated_at": 1547619894,
 *          "deleted_at": null,
 *          "published_at": 1547536150,
 *          "created_by": "qKXVA",
 *          "updated_by": "qKXVA",
 *          "deleted_by": "PKEnN",
 *          "published_by": 1,
 *          "owned_by": 1,
 *          "moderated_by": 1,
 *          "moderated_at": "2019-01-15 10:39:10",
 *          "converted": 0,
 *          "domain_id": null,
 *          "order": null,
 *          "moderate_note": null,
 *          "post_files": {
 *              "1": {
 *                  "label": "This is my file label",
 *                  "link": "www.x.com",
 *                  "src": "oxljN"
 *                  }
 *          },
 *          attachments": [
 *              {
 *                  "label": "This is my file label",
 *                  "link": "www.x.com",
 *                  "file": {
 *                      "id": "oxljN",
 *                      "original":
 *                      "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_original.png",
 *                      "thumb":
 *                      "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_thumb.png",
 *                      "posttype-DAvQN-thumb":
 *                      "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_posttype-DAvQN-thumb.png"
 *                  }
 *              }
 *          ]
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  PostsController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Posts List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // may have conditions imposed in the request layer
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Posts\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostsController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "id"         => "DAvQN",
                  "slug"       => "dummy-cedric",
                  "type"       => "marcus",
                  "title"      => dummy()::persianTitle(),
                  "title2"     => "",
                  "long_title" => null,
                  "is_draft"   => false,
                  "locale"     => "fa",
                  "copy_of"    => 0,
                  "sisterhood" => "97J77p0V5nOG",
                  "text"       => dummy()::persianText(),
             ],
        ], [
             "total"              => 4,
             "count"              => 4,
             "per_page"           => 15,
             "last_page"          => 1,
             "current_page"       => 1,
             "next_page_url"      => null,
             "previous_page_url"  => null,
             "authenticated_user" => "qKXVA",
        ]);
    }
}
