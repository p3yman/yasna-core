<?php

namespace Modules\Posts\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Posts\Http\Controllers\V1\PostsController;

/**
 * @api               {GET}
 *                    /api/modular/v1/posts-single
 *                    Post Single
 * @apiDescription    The Single Presentation of a Post
 * @apiVersion        1.0.1
 * @apiName           Post Single
 * @apiGroup          Posts
 * @apiPermission     none
 * @apiParam {String} [id] The Hashid of the Post.
 * @apiParam {String} [slug] The Slug of the Post. This filled will be assumed as the identifier of the post in the
 *           absence of the `id`.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *          "id": "NDgwK",
 *          "title": "This is post title",
 *          "text": "This is post content",
 *          "created_at": 1547536150,
 *          "created_by": {
 *                  "id": "qKXVA",
 *                  "name_first": "Mostafa",
 *                  "name_last": "Norzade",
 *                  "full_name": "Mostafa Norzade"
 *          },
 *          "published_at": 1547536150,
 *          "deleted_at": null,
 *          "featured_image": {
 *                  "id": "QKgJx",
 *                  "original":
 *                  "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_original.jpeg",
 *                  "thumb":
 *                  "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_thumb.jpeg",
 *                  "posttype-DAvQN-thumb":
 *                  "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_posttype-DAvQN-thumb.jpeg"
 *          },
 *          "attachments": [
 *              {
 *                  "label": "This is file's title",
 *                  "link": "www.x.com",
 *                  "file": {
 *                          "id": "oxljN",
 *                          "original":
 *                          "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_original.png",
 *                          "thumb":
 *                          "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_thumb.png",
 *                          "posttype-DAvQN-thumb":
 *                          "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_posttype-DAvQN-thumb.png"
 *                  }
 *              }
 *          ]
 *      }
 * }
 * @apiErrorExample   Not Found
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Not Found!",
 *      "userMessage": "The requested resource could not be found but may be available in the future.",
 *      "errorCode": 404,
 *      "moreInfo": "posts.moreInfo.404",
 *      "errors": []
 * }
 * @method  PostsController controller()
 */
class SingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Post Single";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Posts\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostsController@single';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"             => "NDgwK",
             "title"          => "This is post title",
             "text"           => "ُThis is post content",
             "created_at"     => 1547536150,
             "created_by"     => [
                  "id"         => "qKXVA",
                  "name_first" => "Mostafa",
                  "name_last"  => "Norzade",
                  "full_name"  => "Mostafa norzade",
             ],
             "published_at"   => 1547536150,
             "deleted_at"     => null,
             "featured_image" => [
                  "id"                   => "QKgJx",
                  "original"             => "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_original.jpeg",
                  "thumb"                => "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_thumb.jpeg",
                  "posttype-DAvQN-thumb" => "http://localhost:8000/uploads/default/image/1547537498_wcT8lSW72JNqB4WhCEPfHugoxSsdQK_posttype-DAvQN-thumb.jpeg",
             ],
             "attachments"    => [
                  [
                       "label" => "This is file's title",
                       "link"  => "www.x.com",
                       "file"  => [
                            "id"                   => "oxljN",
                            "original"             => "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_original.png",
                            "thumb"                => "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_thumb.png",
                            "posttype-DAvQN-thumb" => "http://localhost:8000/uploads/default/image/1547630172_y4DXN6pnXRcUMM2gbGIhQkHL2WNPgq_posttype-DAvQN-thumb.png",
                       ],
                  ],
             ],
        ], [
             "authenticated_user" => hashid(1),
        ]);
    }
}
