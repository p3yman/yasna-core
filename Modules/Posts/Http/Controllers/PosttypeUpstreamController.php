<?php

namespace Modules\Posts\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Modules\Posts\Events\PosttypeUpdated;
use Modules\Posts\Http\Requests\PosttypeActivenessSaveRequest;
use Modules\Posts\Http\Requests\PosttypeSaveRequest;
use Modules\Posts\Http\Requests\PosttypeTitlesSaveRequest;
use Modules\Yasna\Services\YasnaController;

class PosttypeUpstreamController extends YasnaController
{
    protected $base_model  = "Posttype";
    protected $view_folder = "posts::upstream.posttypes";



    /**
     * Called by service environment to form the index page of posttypes upstream tab.
     *
     * @param $search_request
     *
     * @return array
     */
    public static function index($search_request)
    {
        return [
             "keyword"   => $search_request->keyword,
             "models"    => static::indexModels($search_request->keyword),
             "view_file" => "posts::upstream.posttypes.index",
        ];
    }



    /**
     * Gets a collection of models to be shown in index page.
     *
     * @param $search_keyword
     *
     * @return Collection
     */
    private static function indexModels($search_keyword)
    {
        if ($search_keyword) {
            return posttype()
                 ->withTrashed()
                 ->where('title', 'like', "%$search_keyword%")
                 ->orWhere('singular_title', 'like', "%$search_keyword%")
                 ->orWhere('slug', 'like', "%$search_keyword%")
                 ->orderBy('order')
                 ->orderBy('title')
                 ->paginate(user()->preference('max_rows_per_page'))
                 ;
        }

        return posttype()
             ->withTrashed()
             ->orderBy('order')
             ->orderBy('title')
             ->paginate(user()->preference('max_rows_per_page'))
             ;
    }



    /**
     * Prepares the model and returns the appropriate blade, by considering the twin parameters.
     *
     * @param      $action
     * @param null $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function action($action, $hashid = null)
    {
        if (!$this->isActionAllowed($action)) {
            return $this->abort(404);
        }

        $model = posttype($hashid, true);
        if ($model->isNotSet() and $action != 'create') {
            return $this->abort(410);
        }

        if ($action == 'create') {
            $action = 'edit';
        }

        return $this->view($action, compact('model'));
    }



    /**
     * Determines if an action is valid.
     *
     * @param $action
     *
     * @return bool
     */
    private function isActionAllowed($action)
    {
        return in_array($action, [
             'edit',
             'create',
             'activeness',
             'row',
             'titles',
             'seeder',
        ]);
    }



    /**
     * Saved the edit (/creation) form.
     *
     * @param PosttypeSaveRequest $request
     *
     * @return string
     */
    public function save(PosttypeSaveRequest $request)
    {
        $old_slug    = $request->model->slug;
        $saved_model = $request->model->batchSave($request);

        if ($saved_model->exists) {
            event(new PosttypeUpdated($saved_model));
            $saved_model->syncAttachments($request->_features, $request->_inputs, $request->model->slug);
            $saved_model->updateSlugUsages($old_slug);
            $saved_model->cacheFeatures();
        }

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             'success_callback' => "rowUpdate('tblPosttypes','$request->hashid')",
        ]);
    }



    /**
     * Saves Locale Titles
     *
     * @param PosttypeTitlesSaveRequest $request
     *
     * @return string
     */
    public function saveTitles(PosttypeTitlesSaveRequest $request)
    {
        $saved = $request->model->batchSaveBoolean($request);

        return $this->jsonAjaxSaveFeedback($saved, [
             'success_callback' => "rowUpdate('tblPosttypes','$request->hashid')",
        ]);
    }



    /**
     * Saves the delete/restore requests.
     *
     * @param PosttypeActivenessSaveRequest $request
     *
     * @return string
     */
    public function saveActiveness(PosttypeActivenessSaveRequest $request)
    {
        if ($request->_submit == 'delete') {
            $done = $request->model->delete();
        } else {
            $done = $request->model->restore();
        }

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback' => "rowUpdate('tblPosttypes','$request->hashid')",
        ]);
    }
}
