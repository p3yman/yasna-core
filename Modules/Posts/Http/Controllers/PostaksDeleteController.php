<?php

namespace Modules\Posts\Http\Controllers;

use Modules\Posts\Entities\Post;
use Modules\Posts\Events\PostDeleted;
use Modules\Posts\Events\PostUndeleted;
use Modules\Yasna\Services\YasnaController;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest as Request;

class PostaksDeleteController extends YasnaController
{
    protected $view_folder = "posts::postaks";
    protected $row_view    = "browse-row";
    protected $base_model  = "post";



    /**
     * Show a confirm modal to delete a postak
     *
     * @param string $type_slug Slug of post-type
     * @param string $hashid    Hashid of postak model
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($type_slug, $hashid)
    {
        // get the posttype and check that it's a valid one
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has deleting permission
        if ($posttype->cannot('delete')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // get the postak and check that it's a valid one
        $model = model('post', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('posts::postak.post_not_found'));
        }

        // return confirm modal
        return $this->view('delete', compact('model'));
    }



    /**
     * Get a model and delete it
     *
     * @param string $type_slug
     * @param string $hashid
     *
     * @return string
     */
    public function delete($type_slug, $hashid)
    {
        // get the posttype and check that it's a valid one
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has deleting permission
        if ($posttype->cannot('delete')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // get the postak and check that it's a valid one
        $model = model('post', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('posts::postak.post_not_found'));
        }

        $title = trans('posts::postak.post_deleted', ['name' => $model->title]);

        if ($model->canDelete()) {
            // delete model and its sisters
            $this->deletePostAndSisters($model);
        }

        // return confirm modal
        return $this->jsonAjaxSaveFeedback(
             $title,
             [
                  'success_refresh' => true,
             ]
        );
    }



    /**
     * Show a confirm modal to delete a postak
     *
     * @param string $type_slug Slug of post-type
     * @param string $hashid    Hashid of postak model
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function undeleteAction($type_slug, $hashid)
    {
        // get the posttype and check that it's a valid one
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has deleting permission
        if ($posttype->cannot('bin')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // get the postak and check that it's a valid one
        $model = model('post', $hashid, true);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('posts::postak.post_not_found'));
        }

        // return confirm modal
        return $this->view('undelete', compact('model'));
    }



    /**
     * Get a trashed model and restore it
     *
     * @param string $type_slug Slug of Posttype
     * @param string $hashid    Hashid of Post
     *
     * @return string
     */
    public function undelete($type_slug, $hashid)
    {
        // get the posttype and check that it's a valid one
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has deleting permission
        if ($posttype->cannot('bin')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // get the postak and check that it's a valid one
        $model = model('post', $hashid, true);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('posts::postak.post_not_found'));
        }

        $title = trans('posts::postak.post_restored', ['name' => $model->title]);

        if ($model->canDelete()) {
            // restore model and its sisters
            $this->restorePostAndSisters($model);
        }

        // return confirm modal
        return $this->jsonAjaxSaveFeedback(
             $title,
             [
                  'success_refresh' => true,
             ]
        );
    }



    /**
     * Show a confirm modal to delete a postak permanently
     *
     * @param string $type_slug Slug of post-type
     * @param string $hashid    Hashid of postak model
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function destroyAction($type_slug, $hashid)
    {
        // get the posttype and check that it's a valid one
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has deleting permission
        if ($posttype->cannot('bin')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // get the postak and check that it's a valid one
        $model = model('post', $hashid, true);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('posts::postak.post_not_found'));
        }

        // return confirm modal
        return $this->view('destroy', compact('model'));
    }



    /**
     * delete selected posts
     *
     * @param Request $request
     *
     * @return string
     */
    public function massDelete(Request $request)
    {
        $posts      = $this->getSelectedPosts($request->ids);
        $total_done = 0;

        foreach ($posts as $post) {
            if ($post->canDelete()) {
                $done       = $this->deletePostAndSisters($post);
                $total_done += (int)$done;
                $this->dispatchDeleteEvent($post, $done);
            }
        }

        return $this->jsonAjaxSaveFeedback(
             $total_done,
             [
                  'success_refresh' => true,
                  'success_message' => trans(
                       "manage::forms.feed.mass_done",
                       [
                            "count" => pd($total_done),
                       ]
                  ),
             ]
        );
    }



    /**
     * undelete selected posts
     *
     * @param Request $request
     *
     * @return string
     */
    public function massUndelete(Request $request)
    {
        $posts      = $this->getSelectedPosts($request->ids);
        $total_done = 0;

        foreach ($posts as $post) {
            if ($post->canDelete()) {
                $done       = $this->restorePostAndSisters($post);
                $total_done += (int)$done;
                $this->dispatchUndeleteEvent($post, $done);
            }
        }

        return $this->jsonAjaxSaveFeedback(
             $total_done,
             [
                  'success_refresh' => true,
                  'success_message' => trans(
                       "manage::forms.feed.mass_done",
                       [
                            "count" => pd($total_done),
                       ]
                  ),
             ]
        );
    }



    /**
     * permanently destroy selected posts
     *
     * @param Request $request
     *
     * @return string
     */
    public function massDestroy(Request $request)
    {
        $posts      = $this->getSelectedPosts($request->ids);
        $total_done = 0;

        foreach ($posts as $post) {
            if ($post->canDelete()) {
                $done       = $this->destroyPostAndSisters($post);
                $total_done += $done;
            }
        }

        return $this->jsonAjaxSaveFeedback(
             $total_done,
             [
                  'success_refresh' => true,
                  'success_message' => trans(
                       "manage::forms.feed.mass_done",
                       [
                            "count" => pd($total_done),
                       ]
                  ),
             ]
        );
    }



    /**
     * Get a trashed model and destroy it permanently
     *
     * @param string $type_slug Slug of posttype
     * @param string $hashid    Hashid of post
     *
     * @return string
     */
    public function destroy($type_slug, $hashid)
    {
        // get the posttype and check that it's a valid one
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has deleting permission
        if ($posttype->cannot('bin')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // get the postak and check that it's a valid one
        $model = model('post', $hashid, true);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('posts::postak.post_not_found'));
        }

        $title = trans('posts::postak.post_destroyed', ['name' => $model->title]);

        if ($model->canDelete()) {
            // restore model and its sisters
            $this->destroyPostAndSisters($model);
        }

        // return confirm modal
        return $this->jsonAjaxSaveFeedback(
             $title,
             [
                  'success_refresh' => true,
             ]
        );
    }



    /**
     * Delete a model and all its sister
     *
     * @param Post $model
     *
     * @return bool
     */
    private function deletePostAndSisters(Post $model)
    {
        if ($model->delete()) {
            $model->sisters->each(
                 function ($post) {
                     $post->delete();
                 }
            );

            return true;
        }

        return false;
    }



    /**
     * Restore a model and all its sister
     *
     * @param Post $model
     *
     * @return bool
     */
    private function restorePostAndSisters(Post $model)
    {
        if ($model->restore()) {
            $model->sisters()->withTrashed()->each(
                 function ($post) {
                     $post->restore();
                 }
            )
            ;

            return true;
        }

        return false;
    }



    /**
     * Destroy a model and all its sister
     *
     * @param Post $model
     */
    private function destroyPostAndSisters(Post $model)
    {
        $model->sisters()->withTrashed()->each(
             function ($post) {
                 $post->hardDelete();
             }
        )
        ;

        $model->hardDelete();
    }



    /**
     * get a collection of posts selected by the user
     *
     * @param string $hashids
     *
     * @return Collection
     */
    private function getSelectedPosts($hashids)
    {
        $hashid_array = explode_not_empty(',', $hashids);
        $id_array     = hashid($hashid_array);

        return post()->withTrashed()->whereIn("id", $id_array)->get();
    }



    /**
     * dispatch undelete event if undelete action is successful
     *
     * @param Post $model
     * @param bool $is_done
     *
     * @return void
     */
    private function dispatchUndeleteEvent($model, $is_done)
    {
        if ($is_done) {
            event(new PostUndeleted($model));
        }
    }



    /**
     * dispatch delete event if delete action is successful
     *
     * @param Post $model
     * @param bool $is_done
     *
     * @return void
     */
    private function dispatchDeleteEvent($model, $is_done)
    {
        if ($is_done) {
            event(new PostDeleted($model));
        }
    }
}
