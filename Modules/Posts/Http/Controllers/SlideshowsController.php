<?php

namespace Modules\Posts\Http\Controllers;

use App\Models\Posttype;
use Illuminate\Support\Carbon;
use Modules\Posts\Entities\Post;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;
use Nwidart\Modules\Collection;

class SlideshowsController extends YasnaController
{
    /**
     * @var Posttype
     */
    private $posttype;



    /**
     * show slideshow view
     *
     * @param string $hashid
     *
     * @return string
     */
    public function showPage($hashid)
    {
        $this->registerAssets();
        $view_data['posttype'] = posttype($hashid);

        if (!$view_data['posttype']->can('edit')) {
            return $this->abort(403);
        }

        $view_data['page']  = $this->setPage($view_data['posttype']);
        $view_data['posts'] = $this->fetchPosts($view_data['posttype']->slug);

        return view('posts::slideshow.main', $view_data);
    }



    /**
     * get date-picker widget
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDatePicker()
    {
        return view("posts::slideshow.date-picker");
    }



    /**
     * save slider data
     *
     * @param SimpleYasnaRequest $request
     */
    public function saveSlides(SimpleYasnaRequest $request)
    {
        $data           = json_decode($request->data, true);
        $hashid         = $request->posttype;
        $this->posttype = posttype($hashid);

        if (!$this->posttype->can('edit')) {
            abort(403);
        }

        $this->saveSetting($data['settings']);
        $this->saveSlidersData($data['slides']);
    }



    /**
     * hard delete slide
     *
     * @param SimpleYasnaRequest $request
     */
    public function deleteSlide(SimpleYasnaRequest $request)
    {
        if (!post($request->post_id)->can('delete')) {
            abort(403);
        }

        $id = str_replace("key_", "", $request->post_id);
        post()->withTrashed()->whereId($id)->forceDelete();
    }



    /**
     *  delete image of a slider
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \App\Models\Post
     */
    public function deleteImage(SimpleYasnaRequest $request)
    {
        $id    = str_replace("key_", "", $request->post_id);
        $model = post($id);
        $ok    = $model->batchSave(['featured_image' => '']);
        return $ok;
    }



    /**
     * return slides of posttype
     *
     * @param string $posttype this is slug of posttype
     *
     * @return Collection
     */
    private function fetchPosts($posttype)
    {
        return post()
             ->withTrashed()
             ->where('type', $posttype)
             ->orderBy('deleted_at', 'asc')
             ->orderBy('order', 'asc')
             ->get()
             ;
    }



    /**
     * save slider setting
     *
     * @param array $data
     *
     * @return Posttype
     */
    private function saveSetting($data)
    {
        $ok = $this->posttype->batchSave([
             'slideshow_auto'    => $data['auto'],
             'slideshow_pause'   => $data['pause'],
             'slideshow_speed'   => $data['speed'],
             'slideshow_timeout' => $data['timeout'],
        ]);

        return $ok;
    }



    /**
     * save all slides as post
     *
     * @param array $slides
     */
    private function saveSlidersData($slides)
    {
        $order = 1;
        foreach ($slides as $id => $data) {
            $save_arr = $this->makeSaveArray($data, $order);

            $post = $this->savePost($save_arr, $id);

            $this->setAvailability($data['view_slide'], $post);

            $order++;
        }
    }



    /**
     * check availability and it disables or enables posts
     *
     * @param string $view_slide
     * @param Post   $post
     */
    private function setAvailability($view_slide, $post)
    {
        if ($view_slide == "0") {
            $post->delete();
        } elseif (!empty($post->deleted_at)) {
            $post->restore();
        }
    }



    /**
     * make save array for save slide
     *
     * @param array $data
     * @param int   $order
     *
     * @return array
     */
    private function makeSaveArray($data, $order)
    {
        $publish_at = $this->getTimeFromUnix($data['publish_at']);

        return [
             'type'               => $this->posttype->slug,
             'title'              => $data['slide_title'],
             'title2'             => $data['slide_subtitle'],
             'long_title'         => $data['slide_description'],
             'featured_image'     => $data['image_hash'],
             'view_slide'         => $data['view_slide'],
             'button_title'       => $data['button_title'],
             'button_link'        => $data['button_link'],
             'button_target'      => $data['button_target'],
             'slide_position'     => $data['position'],
             'slide_theme'        => $data['theme'],
             'slide_auto_publish' => (empty($data['publish_at'])) ? false : $data['auto_publish'],
             'published_at'       => ($data['view_slide']) ? ($data['publish_at']) ? $publish_at : now() : null,
             'published_by'       => user()->id,
             'order'              => $order,
        ];
    }



    /**
     * save slide as a post
     *
     * @param array  $save_array
     * @param string $id
     *
     * @return \App\Models\Post
     */
    private function savePost($save_array, $id)
    {
        $id    = str_replace("key_", "", $id);
        $model = post()->withTrashed()->whereId($id)->first();

        //create new slide
        if (is_null($model)) {
            $model       = post();
            $model->type = $this->posttype->slug;
            return $model->batchSave($save_array);
        }

        //update slide
        return $model->batchSave($save_array);
    }



    /**
     * get formal date format from unix time
     *
     * @param string $time
     *
     * @return bool|string
     */
    private function getTimeFromUnix($time)
    {
        //return pre value without change
        if (!$this->isValidTimeStamp($time)) {
            return $time;
        }

        $time = substr($time, 0, strlen($time) - 3);
        $time = Carbon::parse(date('Y-m-d H:i:s', $time))->toDateTimeString();
        return $time;
    }



    /**
     * check that is valid timestamp
     *
     * @param string $timestamp
     *
     * @return bool
     */
    public function isValidTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === $timestamp)
             && ($timestamp <= PHP_INT_MAX)
             && ($timestamp >= ~PHP_INT_MAX);
    }



    /**
     * register assets
     */
    private function registerAssets()
    {
        module('manage')
             ->service('template_assets')
             ->add('slideshow-css')
             ->link("posts:css/slideshow-editor.min.css")
             ->order(46)
        ;


        module('manage')
             ->service('template_bottom_assets')
             ->add('rslider-js')
             ->link("posts:libs/responsiveslides.js")
             ->order(47)
        ;
        module('manage')
             ->service('template_bottom_assets')
             ->add('sortable-js')
             ->link("posts:libs/Sortable.min.js")
             ->order(45)
        ;

        module('manage')
             ->service('template_bottom_assets')
             ->add('slideshow-js')
             ->link("posts:js/slideshow-editor.min.js")
             ->order(48)
        ;
    }



    /**
     * @param Posttype $posttype
     *
     * @return array
     */
    private function setPage($posttype)
    {
        return [
             ["slideshows/" . $posttype->hashid, $posttype->title],
        ];
    }
}
