<?php

namespace Modules\Posts\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Manage\Traits\ManageControllerTrait;
use Modules\Posts\Http\Requests\_OwnerSaveRequest;
use Modules\Posts\Http\Requests\DomainReflectionSaveRequest;
use Modules\Posts\Http\Requests\FilterRequest;
use Modules\Posts\Http\Requests\OwnerSaveRequest;

class PostsManageController extends Controller
{
    protected $view_folder = "posts::manage";
    protected $base_model;
    protected $model_name  = "post";
    private   $has_filter  = false;


    use ManageControllerTrait;



    public function __construct()
    {
        $this->base_model = new Post();
    }



    public function browse($type, $request_tab = 'published', $switches = null)
    {
        /*-----------------------------------------------
        | Check Permission ...
        */
        if (!Post::checkManagePermission($type, $request_tab)) {
            return $this->abort(403);
        }
        /*-----------------------------------------------
        | Reveal Posttype ...
        */
        $posttype = posttype($type);
        if (!$posttype->exists) {
            return $this->abort(404);
        }

        /*-----------------------------------------------
        | Break Switches ...
        */
        $switches = array_normalize(array_maker($switches), [
             'locale' => $posttype->localesAllowed('browse'),
             'post'   => null,
        ]);

        $locale = $switches['locale'];
        if (!in_array($locale, $posttype->locales_array)) {
            $locale = 'all';
        }

        /*-----------------------------------------------
        | Page Browse ...
        */
        $page = [
             '0' => ["posts/$posttype->slug", $posttype->title, "posts/$posttype->slug"],
             '1' => [$request_tab, trans("posts::criteria.$request_tab"), "posts/$posttype->slug/$request_tab"],
        ];

        /*-----------------------------------------------
        | Model ...
        */
        $elector_switches = [
             'type'        => $type,
             'locale'      => $locale,
             'criteria'    => $request_tab,
             'hashid'      => $switches['post'],
             'main_domain' => $posttype->hasnot('domains') ? null : 'auto',
        ];


        if (in_array($request_tab, ['pending', 'bin']) and user()->as('admin')->cant("post-$type.publish")) {
            $elector_switches['owner'] = user()->id;
        }

        $models = model('Post')
             ->elector($elector_switches)
             ->orderBy('pinned_at', 'desc')
             ->orderBy('created_at', 'desc')
             ->paginate(user()->preference('max_rows_per_page'))
        ;

        $this->setHasFilter();
        $has_filter = $this->has_filter;

        /* @TODO it's a good idea to fill this var with a planned value or remove it at all from line 102 */
        $db = null;
        /*-----------------------------------------------
        | View ...
        */
        $arguments = compact('page', 'models', 'db', 'locale', 'posttype', 'has_filter');

        module('posts')->service('mass_action_handlers')->handle($arguments);
        module('posts')->service('browse_headings_handlers')->handle($arguments);

        return view($this->view_folder . ".browse", $arguments);
    }



    /**
     * passed the filtered posts to browse blade
     *
     * @param string|null   $switches
     * @param FilterRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function postFilter($switches = null, FilterRequest $request)
    {

        $type        = $request->type;
        $request_tab = $request->current_tab;

        /*-----------------------------------------------
        | Check Permission ...
        */
        if (!Post::checkManagePermission($type, $request_tab)) {
            return $this->abort(403);
        }
        /*-----------------------------------------------
        | Reveal Posttype ...
        */
        $posttype = posttype($type);
        if (!$posttype->exists) {
            return $this->abort(404);
        }

        /*-----------------------------------------------
        | Break Switches ...
        */
        $switches = array_normalize(array_maker($switches), [
             'locale' => $posttype->localesAllowed('browse'),
             'post'   => null,
        ]);

        $locale = $switches['locale'];
        if (!in_array($locale, $posttype->locales_array)) {
            $locale = 'all';
        }

        /*-----------------------------------------------
        | Page Browse ...
        */
        $page = [
             '0' => ["posts/$posttype->slug", $posttype->title, "posts/$posttype->slug"],
             '1' => [
                  $request_tab,
                  trans("posts::criteria.$request_tab") . ' ' . trans("posts::filter.filtered"),
                  "posts/$posttype->slug/$request_tab",
             ],
        ];

        /*-----------------------------------------------
        | Model ...
        */
        $elector_switches = [
             'type'        => $type,
             'locale'      => $locale,
             'criteria'    => $request_tab,
             'hashid'      => $switches['post'],
             'main_domain' => $posttype->hasnot('domains') ? null : 'auto',
        ];
        $request_elector  = $request->toArray();
        $elector          = array_merge($request_elector, $elector_switches);

        if (in_array($request_tab, ['pending', 'bin']) and user()->as('admin')->cant("post-$type.publish")) {
            $elector_switches['owner'] = user()->id;
        }

        $models = model('Post')
             ->elector($elector)
             ->orderBy('published_at', $request->sort)
             ->orderBy('created_at', $request->sort)
             ->paginate(user()->preference('max_rows_per_page'))
        ;

        $this->setHasFilter();
        $has_filter = $this->has_filter;

        /*-----------------------------------------------
        | View ...
        */
        $arguments = compact('page', 'models', 'db', 'locale', 'posttype', 'has_filter');

        module('posts')->service('mass_action_handlers')->handle($arguments);
        module('posts')->service('browse_headings_handlers')->handle($arguments);

        return view($this->view_folder . ".browse", $arguments);
    }



    /**
     * this is for show filter box in ui when user uses filter in reload page
     */
    private function setHasFilter()
    {
        $filter_data = request()->except('type', 'current_tab', '_date_from', '_date_to', 'sort', 'page');

        if (empty($filter_data)) {
            return;
        }

        $this->has_filter = true;
    }



    public function search($type, $locale, Request $request)
    {
        /*-----------------------------------------------
        | Check Permission ...
        */
        if (!Post::checkManagePermission($type, 'search')) {
            return view('errors.403');
        }

        /*-----------------------------------------------
        | Revealing the Posttype...
        */
        $posttype = posttype($type);
        if (!$posttype) {
            return view('errors.404');
        }

        /*-----------------------------------------------
        | Locale ...
        */
        if (!in_array($locale, $posttype->locales_array)) {
            $locale = 'all';
        }

        /*-----------------------------------------------
        | Page Browse ...
        */
        $page = [
             '0' => ["posts/$posttype->slug", $posttype->title, "posts/$posttype->slug"],
             '1' => [
                  "$locale/search",
                  trans('manage::forms.button.search_for') . " $request->keyword ",
                  "posts/$type/$locale/",
             ],
        ];

        /*-----------------------------------------------
        | Only Panel ...
        */
        if (strlen($request->keyword) < 3) {
            $page[1][1] = trans('manage::forms.button.search');

            return view($this->view_folder . ".search", compact('page', 'models', 'db', 'locale', 'posttype'));
        }

        /*-----------------------------------------------
        | Model ...
        */
        $elector_switches = [
             'type'     => $type,
             'locale'   => $locale,
             'criteria' => 'all',
             'search'   => $keyword = $request->keyword,
        ];

        $models = model('post')
             ->elector($elector_switches)
             ->orderBy('created_at', 'desc')
             ->paginate(user()->preference('max_rows_per_page'))
        ;

        /*-----------------------------------------------
        | View ...
        */

        $this->setHasFilter();
        $has_filter = $this->has_filter;
        $arguments = compact('page', 'models', 'db', 'locale', 'posttype', 'keyword', 'has_filter');

        module('posts')->service('mass_action_handlers')->handle($arguments);
        module('posts')->service('browse_headings_handlers')->handle($arguments);


        return view($this->view_folder . ".browse", $arguments);
    }



    public function savePin(Request $request)
    {
        /*
        |--------------------------------------------------------------------------
        | Model Retreive
        |--------------------------------------------------------------------------
        |
        */

        $model = Post::findByHashid($request->hashid);
        if (!$model or !$model->id or $model->hasnot('pin')) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }
        if (!$model->canPublish()) {
            return $this->jsonFeedback(trans('validation.http.Error503'));
        }


        /*
        |--------------------------------------------------------------------------
        | Action
        |--------------------------------------------------------------------------
        |
        */
        if ($request->_submit == 'put') {
            Post::where('type', $model->type)->withTrashed()->update([
                 'pinned_at' => null,
                 'pinned_by' => "0",
            ])
            ;
            $ok = $model->update([
                 'pinned_at' => Carbon::now()->toDateTimeString(),
                 'pinned_by' => user()->id,
            ]);
        } elseif ($request->_submit == 'remove') {
            $ok = $model->update([
                 'pinned_at' => null,
                 'pinned_by' => "0",
            ]);
        } else {
            $ok = false;
        }

        /*
        |--------------------------------------------------------------------------
        | Feedback
        |--------------------------------------------------------------------------
        |
        */

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_refresh' => "1",
             //'success_callback' => "rowUpdate('tblPosts' , '$request->id')",
        ]);
    }



    public function saveOwner(OwnerSaveRequest $request)
    {
        $ok = $request->model->batchSaveBoolean([
             "owned_by" => $request->user->id,
        ]);

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('tblPosts' , '$request->hashid');divReload('divOwnerPanel')",
        ]);
    }



    public function _saveOwner(_OwnerSaveRequest $request)
    {
        $field = config("auth.providers.users.field_name");
        /*
        |--------------------------------------------------------------------------
        | Model Retreive
        |--------------------------------------------------------------------------
        |
        */
        $model = Post::findByHashid($request->hashid);
        if (!$model or !$model->id) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }
        if (!$model->canEdit()) {
            return $this->jsonFeedback(trans('validation.http.Error503'));
        }

        /*
        |--------------------------------------------------------------------------
        | User Retrieve
        |--------------------------------------------------------------------------
        |
        */
        $user = user()->grab($request->$field, $field);
        if (!$user or !$user->exists) {
            return $this->jsonFeedback(trans("yasna::people.nobody_found"));
        }
        if ($user->as('admin')->cannot("posts-$model->type")) {
            return $this->jsonFeedback(trans("yasna::people.invalid_permission"));
        }

        /*
        |--------------------------------------------------------------------------
        | Save
        |--------------------------------------------------------------------------
        |
        */
        $ok = Post::store([
             'id'       => $model->id,
             'owned_by' => $user->id,
        ]);

        /*
        |--------------------------------------------------------------------------
        | Feedback
        |--------------------------------------------------------------------------
        |
        */

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('tblPosts' , '$request->id');divReload('divOwnerPanel')",
        ]);
    }



    public function saveClone(Request $request)
    {
        $data = $request->toArray();

        /*
        |--------------------------------------------------------------------------
        | Model Retreive
        |--------------------------------------------------------------------------
        |
        */
        $model = Post::findByHashid($request->hashid);
        if (!$model or !$model->id) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }
        if (!$model->can('create')) {
            return $this->jsonFeedback(trans('validation.http.Error503'));
        }

        /*
        |--------------------------------------------------------------------------
        | Validation
        |--------------------------------------------------------------------------
        |
        */
        if ($model->has('locales')) {
            if (!in_array($data['locale'], $model->posttype->locales_array)) {
                return $this->jsonFeedback();
            }
            if ($data['is_sister']) {
                $sister = $model->in($data['locale']);
                if ($sister->exists) {
                    return $this->jsonFeedback(trans('posts::general.translation_already_made'));
                }
            }
        } else {
            //(to be safe)
            $data['is_sister'] = 0;
            $data['locale']    = $model->locale;
        }

        /*
        |--------------------------------------------------------------------------
        | Make Clone
        |--------------------------------------------------------------------------
        |
        */
        $new_model = $model->replicate();
        if ($model->has('locales')) {
            $new_model->locale = $data['locale'];
        }
        if (!$data['is_sister']) {
            $new_model->sisterhood = hashid(time(), 'main');
        }
        $new_model->title        = $request->new_title;
        $new_model->slug         = $new_model->normalized_slug;
        $new_model->is_draft     = 1;
        $new_model->copy_of      = 0;
        $new_model->published_at = null;
        $new_model->moderated_at = null;
        $new_model->published_by = 0;
        $new_model->moderated_by = 0;
        $new_model->owned_by     = user()->id;
        $new_model->created_by   = user()->id;

        $new_model->save();

        /*-----------------------------------------------
        | Feedback ...
        */

        return $this->jsonAjaxSaveFeedback($new_model->id > 0, [
             'success_callback' => "rowUpdate('tblPosts','$model->hashid')",
             'success_redirect' => $new_model->edit_link,
        ]);
    }



    public function saveDomainReflections(DomainReflectionSaveRequest $request)
    {
        $hashid = $request->model->hashid;
        $request->model->updateReflections($request->domains);

        return $this->jsonAjaxSaveFeedback(true, [
             'success_callback' => "rowUpdate('tblPosts','$hashid')",
             'success_refresh'  => "",
        ]);
    }
}
