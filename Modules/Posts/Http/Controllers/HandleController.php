<?php

namespace Modules\Posts\Http\Controllers;

use Illuminate\Routing\Controller;

class HandleController extends Controller
{
    /**
     * form the content of the create buttons, on the manage top-bar
     */
    public static function createButtons()
    {
        $posttypes = posttypes()->having("manage_sidebar")->sortBy('order')->get();

        foreach ($posttypes as $posttype) {
            module('manage')
                 ->service('nav_create')
                 ->add("post-$posttype->slug")
                 ->caption($posttype->create_title)
                 ->condition($posttype->can('create'))
                 ->link($posttype->create_link)
                 ->icon($posttype->icon)
                 ->color('info')
            ;
        }
    }



    /**
     * register dashboard widgets of the posts module
     */
    public static function widgets()
    {
        static::registerPosts();
    }



    /**
     * register widgets for individual posts
     */
    public static function registerPosts()
    {
        foreach (posttypes()->having('dashboard')->get() as $posttype) {
            $posttype->spreadMeta();
            module('manage')
                 ->service("widgets")
                 ->add("post-recent-$posttype->slug")
                 ->color('info')
                 ->icon($posttype->icon)
                 ->caption(trans_safe("posts::settings.recent", [
                      'title' => $posttype->title,
                 ]))
                 ->blade('posts::dashboard.recent')
            ;
        }
    }



    /**
     * Pending Notifications on the top bar
     */
    public static function pendingNotifications()
    {
        foreach (posttypes()->having("manage_sidebar")->get() as $posttype) {
            $count = $posttype->pendings()->count();
            if ($count) {
                $posttype->spreadMeta();
                module('manage')
                     ->service("nav_notification")
                     ->add("pending-posts-$posttype->slug")
                     ->caption($posttype->title)
                     ->condition($posttype->can('publish'))
                     ->link("url:manage/posts/$posttype->slug/pending")
                     ->icon($posttype->icon)
                     ->color('info')
                     ->set('count', $count)
                     ->comment(trans_safe("posts::criteria.pending"))
                ;
            }
        }
    }



    public static function editor($model)
    {
        self::editorMain($model);
        self::editorSide($model);
        self::editorMisc($model);
        self::afterSave();
    }



    private static function afterSave()
    {
        module('posts')
             ->service('after_save')
             ->add('domains')
             ->method('posts:PostsEditController@saveRelatedDomains')
        ;
        module('posts')
             ->service('after_save')
             ->add('relateds')
             ->method('posts:PostsEditController@saveRelateds')
        ;
    }



    private static function editorMain($model)
    {
        $service_name = "posts:editor_main";
        $view_folder  = "posts::editor";

        /*-----------------------------------------------
        | Hidden Things ...
        */

        service($service_name)
             ->add('delete')
             ->blade("$view_folder.delete")
             ->order(10)
        ;

        service($service_name)
             ->add('unpublish')
             ->blade("$view_folder.unpublish")
             ->order(10)
        ;

        service($service_name)
             ->add('refer_back')
             ->blade("$view_folder.refer_back")
             ->order(10)
        ;

        /*-----------------------------------------------
        | Visible Things ...
        */

        service($service_name)
             ->add('title')
             ->blade("$view_folder.title")
             ->order(11)
        ;

        service($service_name)
             ->add('moderator_note')
             ->blade("$view_folder.moderator_note")
             ->order(11)
        ;

        service($service_name)
             ->add('change_warning')
             ->blade("$view_folder.change_warning")
             ->order(10)
        ;

        service($service_name)
             ->add('text')
             ->blade("$view_folder.text")
             ->order(21)
        ;

        service($service_name)
             ->add('event')
             ->blade("$view_folder.event")
             ->condition($model->has('event'))
             ->order(31)
        ;

        service($service_name)
             ->add('register')
             ->condition($model->has('register'))
             ->blade("$view_folder.register")
             ->order(31)
        ;

        //service($service_name)
        //	->add('album')
        //	->blade("$view_folder.album")
        //	->order(31)
        //;

        service($service_name)
             ->add('relateds')
             ->blade("$view_folder.related_posts")
             ->order(90)
             ->condition($model->has('related_posts'))
        ;

        service($service_name)
             ->add('meta')
             ->blade("$view_folder.meta")
             ->order(91)
        ;
    }



    private static function editorSide($model)
    {
        $service_name = "posts:editor_side";
        $view_folder  = "posts::editor";

        service($service_name)
             ->add('publish')
             ->blade("$view_folder.publish")
             ->order(11)
        ;

        service($service_name)
             ->add('schedule')
             ->blade("$view_folder.schedule")
             ->order(12)
        ;

        service($service_name)
             ->add('locales')
             ->blade("$view_folder.locales")
             ->order(21)
        ;

        //service($service_name)
        //	->add('featured_image')
        //	->blade("$view_folder.featured_image")
        //	->order(31)
        //;

        service($service_name)
             ->add('misc')
             ->blade("$view_folder.misc")
             ->order(999)
        ;
    }



    private static function editorMisc($model)
    {
        $service_name = "posts:editor_misc";
        $view_folder  = "posts::editor";

        service($service_name)
             ->add('slug')
             ->blade("$view_folder.slug")
             ->order(11)
             ->condition($model->has('slug'))
        ;

        service($service_name)
             ->add('owner')
             ->blade("$view_folder.owner")
             ->order(999)
        ;

        service($service_name)
             ->add('template')
             ->blade("$view_folder.template")
             ->order(12)
             ->condition($model->has('template_choice'))
        ;

        service($service_name)
             ->add('domain_id')
             ->blade("$view_folder.domain_main")
             ->order(21)
             ->condition($model->has('domains'))
        ;
    }



    public static function saveGate($model, $data)
    {

        /*-----------------------------------------------
        | owned_by ...
        */
        if (!$model->exists) {
            $data['owned_by'] = user()->id;
        }


        /*-----------------------------------------------
        | Slug ...
        */
        if ($model->isNotCopy()) {
            if ($model->has('slug')) {
                $model->slug  = $data['slug'];
                $data['slug'] = $model->normalized_slug;
            } else {
                $data['slug'] = null;
            }
        }

        /*-----------------------------------------------
        | Long Title ...
        */
        if ($model->has('long_title')) {
            $data['long_title'] = $data['title'];
            $data['title']      = str_limit($data['title'], 100);
        }

        /*-----------------------------------------------
        | Published_at [Schedule] ...
        */

        /*-----------------------------------------------
        | Events ...
        */
        if (!isset($data['event_starts_at']) or !$data['event_starts_at']) {
            $data['event_starts_at'] = null;
        }
        if (!isset($data['event_ends_at']) or !$data['event_ends_at']) {
            $data['event_ends_at'] = null;
        }
        if (!isset($data['register_starts_at']) or !$data['register_starts_at']) {
            $data['register_starts_at'] = null;
        }
        if (!isset($data['register_ends_at']) or !$data['register_ends_at']) {
            $data['register_ends_at'] = null;
        }


        /*-----------------------------------------------
        | Language ...
        */
        if (!in_array($data['locale'], $model->posttype->locales_array)) {
            return false;
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $data;
    }



    public static function browseMassActions($arguments)
    {
        $page_slug = $arguments['page'][1][0];

        /*-----------------------------------------------
        | Delete ...
        */
        service('posts:mass_actions')
             ->add('delete')
             ->icon('trash-o')
             ->trans("manage::forms.button.soft_delete")
             ->link("modal:manage/posts/act/mass/delete")
             ->order(97)->condition($page_slug != 'bin')
        ;

        /*-----------------------------------------------
        | Undelete ...
        */
        service('posts:mass_actions')
             ->add('undelete')
             ->icon('recycle')
             ->trans("manage::forms.button.undelete")
             ->link("modal:manage/posts/act/mass/undelete")
             ->order(98)
             ->condition($page_slug == 'bin')
        ;

        /*-----------------------------------------------
        | Destroy ...
        */
        service('posts:mass_actions')
             ->add('destroy')
             ->icon('times')
             ->trans("manage::forms.button.hard_delete")
             ->link("modal:manage/posts/act/mass/destroy")
             ->order(99)
             ->condition($page_slug == 'bin')
        ;
    }



    /**
     * Add mass action to postaks browse page
     *
     * @param $arguments
     */
    public static function browsePostakMassActions($arguments)
    {
        $page_slug = $arguments['page'][1][0];

        /*-----------------------------------------------
        | Delete ...
        */
        service('posts:postak_mass_actions')
             ->add('delete')
             ->icon('trash-o')
             ->trans("manage::forms.button.soft_delete")
             ->link("modal:manage/postaks/mass/delete")
             ->order(97)->condition($page_slug != 'bin')
        ;

        /*-----------------------------------------------
        | Undelete ...
        */
        service('posts:postak_mass_actions')
             ->add('undelete')
             ->icon('recycle')
             ->trans("manage::forms.button.undelete")
             ->link("modal:manage/postaks/mass/undelete")
             ->order(98)
             ->condition($page_slug == 'bin')
        ;

        /*-----------------------------------------------
        | Destroy ...
        */
        service('posts:postak_mass_actions')
             ->add('destroy')
             ->icon('times')
             ->trans("manage::forms.button.hard_delete")
             ->link("modal:manage/postaks/mass/destroy")
             ->order(99)
             ->condition($page_slug == 'bin')
        ;
    }



    public static function browseColumns($arguments)
    {
        $posttype = $arguments['posttype'];

        /*-----------------------------------------------
        | Properties ...
        */
        service('posts:browse_headings')
             ->add('properties')
             ->trans('validation.attributes.properties')
             ->blade('posts::manage.browse-properties')
             ->order(11)
        ;

        /*-----------------------------------------------
        | Featured_Image ...
        */
        service('posts:browse_headings')
             ->add('featured_image')
             ->trans('validation.attributes.featured_image')
             ->blade('posts::manage.browse-image')
             ->width(200)
             ->condition($posttype->has('featured_image'))
             ->order(10)
        ;

        /*-----------------------------------------------
        | Domain ...
        */
        service('posts:browse_headings')
             ->add('domains')
             ->trans('yasna::domains.domain')
             ->blade('posts::manage.browse-domain')
             ->condition($posttype->has('domains'))
             ->order(21)
        ;

        ///*-----------------------------------------------
        //| Browse Headings (Table Grids) ...
        //*/
        //service('posts:browse_headings')
        //     ->add('feedback')
        //     ->trans('posts::features.feedback')
        //     ->blade('')
        //     ->width(200)
        //     ->condition($posttype->has('feedback'))
        //     ->order(31)
        //;
    }



    public static function browseRowActions($arguments)
    {
        $model   = $arguments['model'];
        $service = 'posts:row_actions';

        /*-----------------------------------------------
        | Link View ...
        */
        service($service)
             ->add('view_in_site')
             ->icon('eye')
             ->trans('posts::general.view_in_site')
             ->link("urlN:$model->direct_url")
             ->condition($model->isPublished() and $model->has('single_view'))
             ->order(11)
        ;

        /*-----------------------------------------------
        | Preview ...
        */
        service($service)
             ->add('preview')
             ->icon('eye-slash')
             ->trans('posts::general.preview')
             ->link("urlN:$model->preview_link")
             ->condition(!$model->isPublished() and $model->has('single_view'))
             ->order(12)
        ;

        /*-----------------------------------------------
        | Edit ...
        */
        service($service)
             ->add('edit')
             ->icon("pencil")
             ->trans("manage::forms.button.edit")
             ->link("url:manage/posts/$model->type/edit/-hashid-")
             ->condition($model->canEdit())
        ;

        /*-----------------------------------------------
        | Post Owner ...
        */
        service($service)
             ->add("owner")
             ->icon("user-o")
             ->trans("posts::general.post_owner")
             ->link("modal:manage/posts/act/-hashid-/owner/1")
             ->condition($model->isActive() and $model->canPublish())
        ;


        /*-----------------------------------------------
        | Clone ...
        */
        service($service)
             ->add('clone')
             ->icon("clone")
             ->trans("posts::general.clone")
             ->link("modal:manage/posts/act/-hashid-/clone")
             ->condition($model->isActive() and $model->can('create'))
        ;

        /*-----------------------------------------------
        | Locales ...
        */
        service($service)
             ->add('locales')
             ->icon("globe")
             ->trans("posts::general.locales")
             ->link("modal:manage/posts/act/-hashid-/locales/")
             ->condition($model->isActive() and $model->has('locales'))
        ;


        /*-----------------------------------------------
        | Pins ...
        */
        service($service)
             ->add('pin')
             ->icon("thumb-tack")
             ->trans($model->pinned ? "posts::pin.remove_command" : "posts::pin.put_command")
             ->link("modal:manage/posts/act/-hashid-/pin")
             ->condition($model->isActive() and $model->has('pin') and $model->can('publish'))
        ;

        /*-----------------------------------------------
        | delete ...
        */
        service($service)
             ->add('delete')
             ->icon("trash-o")
             ->trans("manage::forms.button.soft_delete")
             ->link("modal:manage/posts/act/-hashid-/delete")
             ->condition($model->canDelete() and !$model->trashed())
        ;


        /*-----------------------------------------------
        | undelete ...
        */
        service($service)
             ->add('restore')
             ->icon("recycle")
             ->trans("manage::forms.button.undelete")
             ->link("modal:manage/posts/act/-hashid-/undelete")
             ->condition($model->canDelete() and $model->trashed())
        ;


        /*-----------------------------------------------
        | destroy ...
        */
        service($service)
             ->add('destroy')
             ->icon("times")
             ->trans("manage::forms.button.hard_delete")
             ->link("modal:manage/posts/act/-hashid-/destroy")
             ->condition($model->canDelete() and $model->trashed())
        ;
    }



    /**
     * add post's creator filter to browse page
     */
    public static function browseFilters()
    {
        $service_name = 'posts:browse_filters';

        service($service_name)
             ->add('creators')
             ->blade('posts::manage.list.filters.creators')
             ->order(1)
        ;

        service($service_name)
             ->add('date_from')
             ->blade('posts::manage.list.filters.date-from')
             ->order(1)
        ;

        service($service_name)
             ->add('date_to')
             ->blade('posts::manage.list.filters.date-to')
             ->order(1)
        ;

        service($service_name)
             ->add('sort')
             ->blade('posts::manage.list.filters.sort')
             ->order(1)
        ;
    }
}
