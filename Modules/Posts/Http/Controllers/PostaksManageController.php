<?php

namespace Modules\Posts\Http\Controllers;

use Modules\Yasna\Services\YasnaController;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest as Request;

class PostaksManageController extends YasnaController
{
    protected $view_folder = "posts::postaks";
    protected $row_view    = "browse-row";
    protected $base_model  = "post";
    protected $model_name  = "post";



    /**
     * Return list of all posts related to a minimal post-type
     *
     * @param        $type
     * @param string $request_tab
     * @param null   $switches
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function browse($type, $request_tab = 'published', $switches = null)
    {
        /*-----------------------------------------------
        | Check Permission ...
        */
        if (!model('post')::checkManagePermission($type, $request_tab)) {
            return $this->abort(403);
        }

        /*-----------------------------------------------
        | Reveal Posttype ...
        */
        $posttype = model('posttype', $type);
        if (!$posttype->exists) {
            return $this->abort(404);
        }

        /*-----------------------------------------------
        | Break Switches ...
        */
        $switches = array_normalize(
             array_maker($switches),
             [
                  'locale' => $posttype->localesAllowed('browse'),
                  'post'   => null,
             ]
        );

        $locale = $switches['locale'];
        if (!in_array($locale, $posttype->locales_array)) {
            $locale = 'all';
        }

        /*-----------------------------------------------
        | Page Browse ...
        */
        $page = [
             '0' => ["postaks/$posttype->slug", $posttype->title, "postaks/$posttype->slug"],
             '1' => [$request_tab, trans("posts::criteria.$request_tab"), "postaks/$posttype->slug/$request_tab"],
        ];

        /*-----------------------------------------------
        | Model ...
        */
        $selector_switches = [
             'type'        => $type,
             'locale'      => $locale,
             'criteria'    => $request_tab,
             'hashid'      => $switches['post'],
             'main_domain' => $posttype->hasnot('domains') ? null : 'auto',
        ];

        if (in_array($request_tab, ['bin']) and user()->as('admin')->cant("post-$type.publish")) {
            $selector_switches['owner'] = user()->id;
        }

        $models = model('Post')
             ->selector($selector_switches)
             ->groupBy('sisterhood')
             ->orderBy('pinned_at', 'desc')
             ->orderBy('created_at', 'desc')
             ->paginate(user()->preference('max_rows_per_page'))
        ;

        $headings = [
             trans('posts::postak.post_title'),
             trans('posts::postak.available_locales'),
             trans('posts::postak.operations'),
        ];

        /*-----------------------------------------------
        | View ...
        */
        $arguments = compact('page', 'models', 'db', 'locale', 'posttype', 'headings');

        module('posts')->service('postak_mass_action_handlers')->handle($arguments);

        return $this->view("browse", $arguments);
    }



    /**
     * Do a search on minimal posts
     *
     * @param string  $type
     * @param string  $locale
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function search($type, $locale, Request $request)
    {
        /*-----------------------------------------------
        | Check Permission ...
        */
        if (!model('post')::checkManagePermission($type, 'search')) {
            return view('errors.403');
        }

        /*-----------------------------------------------
        | Revealing the Posttype...
        */
        $posttype = model('posttype', $type);
        if (!$posttype) {
            return view('errors.404');
        }

        /*-----------------------------------------------
        | Locale ...
        */
        if (!in_array($locale, $posttype->locales_array)) {
            $locale = 'all';
        }

        /*-----------------------------------------------
        | Page Browse ...
        */
        $page = [
             '0' => ["postaks/$posttype->slug", $posttype->title, "postaks/$posttype->slug"],
             '1' => [
                  "$locale/search",
                  trans('manage::forms.button.search_for') . " $request->keyword ",
                  "postaks/$type/$locale/",
             ],
        ];

        /*-----------------------------------------------
        | Only Panel ...
        */
        if (strlen($request->keyword) < 3) {
            $page[1][1] = trans('manage::forms.button.search');

            return $this->view("search", compact('page', 'models', 'db', 'locale', 'posttype'));
        }

        /*-----------------------------------------------
        | Model ...
        */
        $elector_switches = [
             'type'     => $type,
             'locale'   => $locale,
             'criteria' => 'all',
             'search'   => $keyword = $request->keyword,
        ];

        $models = model('post')
             ->elector($elector_switches)
             ->groupBy('sisterhood')
             ->orderBy('created_at', 'desc')
             ->paginate(user()->preference('max_rows_per_page'))
        ;

        $headings = [
             trans('posts::postak.post_title'),
             trans('posts::postak.operations'),
        ];

        /*-----------------------------------------------
        | View ...
        */
        $arguments = compact('page', 'models', 'db', 'locale', 'posttype', 'keyword', 'headings');

        module('posts')->service('postak_mass_action_handlers')->handle($arguments);


        return $this->view("browse", $arguments);
    }



    public function massAction($view_file, ...$option)
    {
        return $this->view($view_file . '-mass', compact('option'));
    }
}
