<?php

namespace Modules\Posts\Http\Controllers;

use App\Models\Post;
use Illuminate\Routing\Controller;
use Modules\Manage\Traits\ManageControllerTrait;
use Modules\Posts\Http\Requests\PostSaveRequest;
use Carbon\Carbon;
use Modules\Posts\Http\Requests\PostSearchRelatedsRequest;

class PostsEditController extends Controller
{
    use ManageControllerTrait;

    protected static $st_view_folder = "posts::editor";
    protected        $view_folder;
    protected        $base_model;



    public function __construct()
    {
        $this->view_folder = self::$st_view_folder;
        $this->base_model  = new Post();
    }



    /*
    |--------------------------------------------------------------------------
    | Main Methods (create, editor, save)
    |--------------------------------------------------------------------------
    |
    */
    public function editor($type_slug, $hashid)
    {
        /*-----------------------------------------------
        | Model ...
        */
        $model = Post::findByHashid($hashid);
        if (!$model or !$model->id or $model->type != $type_slug or !$model->posttype->exists) {
            return $this->abort(410, true);
        }
        if (!$model->spreadMeta()->canEdit()) {
            return $this->abort(403, true);
        }

        /*-----------------------------------------------
        | Page ...
        */
        $page = [
             '0' => ["posts/$type_slug", $model->posttype->title, "posts/$type_slug"],
             '1' => [
                  "posts/$type_slug/edit/$hashid",
                  trans('manage::forms.button.edit'),
                  "posts/$type_slug/edit/$hashid",
             ],
        ];

        /*-----------------------------------------------
        | View ...
        */

        return $this->safeView($this->view_folder . ".index", compact('model', 'page'));
    }



    public function create($type_slug, $locale = null, $sisterhood = null)
    {
        /*-----------------------------------------------
        | Permission ...
        */
        $permit_string = "post-$type_slug.create";
        if ($locale != 'all') {
            $permit_string .= ".$locale";
        }
        if (user()->as('admin')->cannot($permit_string)) {
            return $this->abort(403);
        }

        /*-----------------------------------------------
        | Model ...
        */
        $model           = new Post();
        $model->type     = $type_slug;
        $model->owned_by = user()->id;

        if (true) {
            if (!$locale or $locale == 'all') {
                $model->locale = $model->posttype->locales_array[0];
            } elseif (!in_array($locale, $model->posttype->locales_array)) {
                return $this->abort(410);
            } else {
                $model->locale = $locale;
            }
        }

        if ($sisterhood) {
            $model->sisterhood = $sisterhood;
        } else {
            $model->sisterhood = post()::generateSisterhood();
        }

        if (!$model->posttype->exists) {
            return view('errors.410');
        }

        /*-----------------------------------------------
        | Page ...
        */
        $page = [
             '0' => ["posts/$type_slug", $model->posttype->title, "posts/$type_slug"],
             '1' => ["posts/$type_slug/create", trans('manage::forms.button.add'), "posts/$type_slug/create"],
        ];

        /*-----------------------------------------------
        | View ...
        */

        return $this->safeView($this->view_folder . ".index", compact('model', 'page'));
    }



    public static function saveRelatedDomains($model)
    {
        $model->updateMainReflection($model->domain_id);
    }



    /*
    |--------------------------------------------------------------------------
    | Partial Services
    |--------------------------------------------------------------------------
    |
    */
    public function checkSlug($hashid, $post_type, $post_locale, $suggested_slug = null)
    {
        //dd($hashid, $post_type,$post_locale, $suggested_slug);
        $suggested_slug = trim($suggested_slug);
        if ($suggested_slug) {
            $approved_slug = Post::normalizeSlug($hashid, decrypt($post_type), $post_locale, $suggested_slug);
        } else {
            $approved_slug = '';
        }

        return $this->safeView("posts::editor.slug-feedback", compact('suggested_slug', 'approved_slug'));
    }



    /**
     * Searches the specified query in the posts of the specified posttype.
     *
     * @param PostSearchRelatedsRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchRelateds(PostSearchRelatedsRequest $request)
    {
        $post = $request->model;

        return response()->json(
             post()->elector([
                  'type'     => $post->type,
                  'search'   => $request->search,
                  'criteria' => 'published',
             ])->select(['id', 'title'])
                   ->orderByDesc('published_at')
                   ->where('id', '<>', $post->id)
                   ->limit(10)
                   ->get()
                   ->map(function (Post $post) {
                       return $post->only(['hashid', 'title']);
                   })
                   ->toArray()
        );
    }



    /**
     * Saves the related posts after saving a post.
     *
     * @param Post  $model
     * @param array $data
     */
    public static function saveRelateds(Post $model, array $data)
    {
        $input = ($data['_relateds'] ?? null);

        if (!$input) {
            $model->relateds()->detach();
            return;
        }

        $hashids = explode_not_empty(',', $input);
        $ids     = array_map('hashid', $hashids);

        $model->relateds()->sync($ids);
    }
}
