<?php

namespace Modules\Posts\Http\Controllers\V1;

use App\Models\Post;
use Modules\Posts\Http\Requests\AllPostTypesRequest;
use Modules\Posts\Http\Requests\V1\PostSingleRequest;
use Modules\Posts\Http\Requests\V1\PostsListRequest;
use Modules\Yasna\Services\YasnaApiController;

class PostsController extends YasnaApiController
{


    /**
     * Returns the single representation of the requested post.
     *
     * @param PostSingleRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function single(PostSingleRequest $request)
    {
        /** @var Post $model */
        $model = $request->model;

        if ($model->not_exists) {
            return $this->clientError(404);
        }

        return $this->success($model->in(getLocale())->toSingleResource());
    }



    /**
     * get a list of posts, filtered by query strings
     *
     * @param PostsListRequest $request
     *
     * @return array
     */
    public function list(PostsListRequest $request)
    {
        $builder = post()->elector($request->toArray());

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get all posttypes with feature name
     *
     * @param AllPostTypesRequest $request
     *
     * @return array
     */
    public function postTypes(AllPostTypesRequest $request)
    {

        if ($request['feature']) {
            $posttype = Posttype()->having($request['feature'])->get();
            $result   = $this->addToMapPosttype($posttype);

            return $this->success($result);
        }


        $posttype = Posttype()->get();
        $result   = $this->addToMapPosttype($posttype);

        return $this->success($result);

    }



    /**
     * add map on posttype query
     *
     * @param $posttype
     *
     * @return mixed
     */
    public function addToMapPosttype($posttype)
    {
        $result = $posttype->map(function ($posttype) {
            return $posttype->toListResource();
        })->toArray()
        ;

        return $result;

    }



    /**
     * Complete the elector array for the list action with the default values.
     *
     * @param array $elector
     *
     * @return array
     */
    protected function completeListElector(array $elector)
    {
        $defaults = $this->defaultListElectors();

        foreach ($defaults as $default_key => $default_value) {
            if (isset($elector[$default_key])) {
                continue;
            }

            $elector[$default_key] = $default_value;
        }

        return $elector;
    }



    /**
     * Returns an array of the default electors which should be applied on the list result by default.
     *
     * @return array
     */
    protected function defaultListElectors()
    {
        return [
             'locales' => getLocale(),
        ];
    }



    /**
     * Return array of posts list
     *
     * @param $posts
     *
     * @return array
     */
    private function mapOfPosts($posts): array
    {
        return $posts->map(function ($post) {
            return $post->toListResource();
        })->toArray()
             ;
    }



    /**
     * get an appropriate meta data to be dispatched back to the front-end
     *
     * @param array $electors
     *
     * @return array
     */
    private function getMetadata(array $electors): array
    {
        $array = [];
        if (isset($electors['type'])) {
            $posttype = posttype($electors['type']);
            $posttype->spreadMeta();
            $array['posttype'] = $posttype->only([
                 'slideshow_auto',
                 'slideshow_pause',
                 'slideshow_speed',
                 'slideshow_timeout',
            ]);
        }

        return $array;
    }
}
