<?php

namespace Modules\Posts\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Manage\Traits\ManageControllerTrait;
use App\Models\Posttype;
use Modules\Posts\Events\PosttypeUpdated;
use Modules\Posts\Http\Requests\PosttypeDownstreamSaveRequest;
use Modules\Posts\Http\Requests\PosttypeSettingDownstreamSaveRequest;

class DownstreamController extends Controller
{
    use ManageControllerTrait;



    /*
    |--------------------------------------------------------------------------
    | Posttypes
    |--------------------------------------------------------------------------
    |
    */

    public static function posttypeIndex()
    {
        /*-----------------------------------------------
        | View File ...
        */
        $result['view_file'] = "posts::downstream.posttypes";

        /*-----------------------------------------------
        | Model ...
        */
        $result['models'] = Posttype::orderBy('order')->orderBy('title')->get();

        /*-----------------------------------------------
        | Return ...
        */

        return $result;
    }



    public function posttypeAction($action, $hashid = false)
    {
        /*-----------------------------------------------
        | Action Check ...
        */
        if (!in_array($action, ['setting',])) {
            return $this->abort(404, true);
        }

        /*-----------------------------------------------
        | Model ...
        */
        $model = Posttype::findByHashid($hashid);
        if (!$model or !$model->id) {
            return $this->abort(410, true);
        }
        if ($model->cannot('setting')) {
            return $this->abort(403, true);
        }
        $model->spreadMeta();

        /*-----------------------------------------------
        | View ...
        */
        $view_file = "posts::downstream.posttypes-$action";


        return view($view_file, compact('model'));
    }



    public function posttypeSave(PosttypeSettingDownstreamSaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request);
        if ($saved_model->exists) {
            event(new PosttypeUpdated($saved_model));
        }

        return $this->jsonAjaxSaveFeedback($saved_model->exists);
    }
}
