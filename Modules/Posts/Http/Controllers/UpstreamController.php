<?php

namespace Modules\Posts\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feature;
use App\Models\Input;
use App\Models\Posttype;
use Modules\Posts\Events\PosttypeUpdated;
use Modules\Posts\Http\Requests\FeatureSaveRequest;
use Modules\Posts\Http\Requests\InputDefaultSetRequest;
use Modules\Posts\Http\Requests\InputSaveRequest;
use Modules\Posts\Http\Requests\PosttypeSaveRequest;
use Modules\Posts\Http\Requests\PosttypeTitlesSaveRequest;
use Modules\Yasna\Services\YasnaController;

class UpstreamController extends YasnaController
{


    /*
    |--------------------------------------------------------------------------
    | Features
    |--------------------------------------------------------------------------
    |
    */

    public static function featureIndex($search_request)
    {
        /*-----------------------------------------------
        | View File ...
        */
        $result['view_file'] = "posts::upstream.features";

        /*-----------------------------------------------
        | Search Request ...
        */
        $keyword = $search_request->keyword;

        /*-----------------------------------------------
        | Model ...
        */
        if ($keyword) {
            $result['models']  = Feature::withTrashed()
                                        ->where('title', 'like', "%$keyword%")
                                        ->orWhere('slug', 'like', "%$keyword%")
                                        ->orderBy('order')
                                        ->orderBy('title')
                                        ->paginate(user()->preference('max_rows_per_page'))
            ;
            $result['keyword'] = $keyword;
        } else {
            $result['models'] = Feature::withTrashed()
                                       ->orderBy('order')
                                       ->orderBy('title')
                                       ->paginate(user()->preference('max_rows_per_page'))
            ;
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $result;
    }



    public function featureAction($action, $hashid = false)
    {
        /*-----------------------------------------------
        | Action Check ...
        */
        if (!in_array($action, ['edit', 'create', 'activeness', 'row', 'seeder'])) {
            return $this->abort(404, true);
        }

        /*-----------------------------------------------
        | Model ...
        */
        if (in_array($action, ['create'])) {
            $model = new Feature();
        } else {
            $model = Feature::findByHashid($hashid, true, [
                 'with_trashed' => true,
            ]);
            if (!$model or !$model->id) {
                return $this->abort(410, true);
            }
        }


        /*-----------------------------------------------
        | View ...
        */
        if ($action == 'create') {
            $view_file = "posts::upstream.features-edit";
        } else {
            $view_file = "posts::upstream.features-$action";
        }


        return view($view_file, compact('model'));
    }



    public function featureActiveness(Request $request)
    {
        switch ($request->toArray()['_submit']) {
            case 'delete':
                $model = Feature::find($request->id);
                if (!$model) {
                    return $this->jsonFeedback(trans('manage::forms.general.sorry'));
                }
                $ok = Feature::where('id', $request->id)->delete();
                break;

            case 'restore':
                $ok = Feature::withTrashed()->where('id', $request->id)->restore();
                break;

            default:
                $ok = false;
        }

        $hashid = hashid($request->id);
        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('tblFeatures','$hashid')",
        ]);
    }



    public function featureSave(FeatureSaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request);
        $saved_model->syncInputs($request->_inputs);

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             'success_callback' => "rowUpdate('tblFeatures','$request->hashid')",
        ]);
    }



    /*
    |--------------------------------------------------------------------------
    | Inputs
    |--------------------------------------------------------------------------
    |
    */
    public static function inputIndex($search_request)
    {
        /*-----------------------------------------------
        | View File ...
        */
        $result['view_file'] = "posts::upstream.inputs";

        /*-----------------------------------------------
        | Search Request ...
        */
        $keyword = $search_request->keyword;

        /*-----------------------------------------------
        | Model ...
        */
        if ($keyword) {
            $result['models']  = Input::withTrashed()
                                      ->where('title', 'like', "%$keyword%")
                                      ->orWhere('slug', 'like', "%$keyword%")
                                      ->orderBy('order')
                                      ->orderBy('title')
                                      ->paginate(user()->preference('max_rows_per_page'))
            ;
            $result['keyword'] = $keyword;
        } else {
            $result['models'] = Input::withTrashed()
                                     ->orderBy('order')
                                     ->orderBy('title')
                                     ->paginate(user()->preference('max_rows_per_page'))
            ;
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $result;
    }



    public function inputAction($action, $hashid = false)
    {
        /*-----------------------------------------------
        | Action Check ...
        */
        if (!in_array($action, ['edit', 'create', 'activeness', 'row', 'selector-one', 'default', 'seeder'])) {
            return $this->abort(404, true);
        }

        /*-----------------------------------------------
        | Model ...
        */
        if (in_array($action, ['create'])) {
            $model = new Input();
        } elseif ($action == 'selector-one') {
            $model = Input::find($hashid);
            if (!$model or !$model->id) {
                return $this->abort(410, true);
            }
        } else {
            $model = Input::findByHashid($hashid, true, [
                 'with_trashed' => true,
            ]);
            if (!$model or !$model->id) {
                return $this->abort(410, true);
            }
        }


        /*-----------------------------------------------
        | View ...
        */
        if ($action == 'create') {
            $view_file = "posts::upstream.inputs-edit";
        } else {
            $view_file = "posts::upstream.inputs-$action";
        }


        return view($view_file, compact('model'));
    }



    public function inputActiveness(Request $request)
    {
        switch ($request->toArray()['_submit']) {
            case 'delete':
                $model = Input::find($request->id);
                if (!$model) {
                    return $this->jsonFeedback(trans('manage::forms.general.sorry'));
                }
                $ok = Input::where('id', $request->id)->delete();
                break;

            case 'restore':
                $ok = Input::withTrashed()->where('id', $request->id)->restore();
                break;

            default:
                $ok = false;
        }

        $hashid = hashid($request->id);
        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('tblInputs','$hashid')",
        ]);
    }



    public function inputSave(InputSaveRequest $request)
    {
        $saved = $request->model->batchSaveBoolean($request);

        return $this->jsonAjaxSaveFeedback($saved, [
             'success_callback' => "rowUpdate('tblInputs','$request->hashid')",
        ]);
    }



    public function inputDefault(InputDefaultSetRequest $request)
    {
        $ok     = model("input" , $request->slug, [
             'with_trashed' => true,
        ])->update([
             'default_value' => $request->default_value,
        ])
        ;
        $hashid = hashid($request->id);

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('tblInputs','$hashid')",
        ]);
    }
}
