<?php

namespace Modules\Posts\Http\Controllers;

use Carbon\Carbon;
use Modules\Posts\Entities\Post;
use Modules\Posts\Entities\Posttype;
use Modules\Posts\Http\Requests\PostakRequest;
use Modules\Yasna\Services\YasnaController;

class PostaksSaveController extends YasnaController
{
    protected $view_folder = "posts::postaks";
    protected $row_view    = "browse-row";
    protected $base_model  = "post";



    /**
     * Save information of a minimal post and its sister
     *
     * @param PostakRequest $request
     * @param string        $type_slug
     *
     * @return string
     */
    public function save(PostakRequest $request, $type_slug)
    {
        // get the posttype and check that it's a valid one
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // get the main post
        $main = $this->getMainPost($request, $posttype);

        // detect it's a partial update or a new data entry
        $should_refresh = !$main->exists;

        // checking permissions
        if (
            // if it should refresh, it means it's a new entry and user must have creating permission
            ($should_refresh && $posttype->cannot('create'))
            // otherwise if user tries to edit an post, it should not required to update but must have edit permission
            || (!$should_refresh && $posttype->cannot('edit'))
        ) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // save main post model
        $this->savePostModel($main, $posttype, '');

        // get other posts as a sisterhood and save them
        $sisters = $request->sisterPostModels($main->locale, $posttype->locales_array);
        $this->saveSisterModels($sisters, $posttype, $main->sisterhood);

        // return information
        return $this->jsonAjaxSaveFeedback(
             $main->exists,
             [
                  'success_callback' => "rowUpdate('tblPostaks', '$main->hashid')",
                  'success_refresh'  => $should_refresh,
             ]
        );
    }



    /**
     * Save a post model
     *
     * @param Post     $post
     * @param Posttype $posttype
     * @param string   $sisterhood
     */
    private function savePostModel(?Post $post, Posttype $posttype, string $sisterhood)
    {
        // if there is no post model, don't continue saving process
        if (is_null($post)) {
            return;
        }

        // set posttype information
        $this->setPostInformationFromPosttype($post, $posttype);

        // set sisterhood of the main post if not exist
        $this->ensureSisterhood($post, $sisterhood);

        // save the main post
        $post->save();
    }



    /**
     * Set information of post which is related to its posttype
     *
     * @param Post     $post
     * @param Posttype $posttype
     */
    private function setPostInformationFromPosttype(Post $post, Posttype $posttype)
    {
        $post->type         = $posttype->slug;
        $post->template     = $posttype->template;
        $post->is_draft     = 0;
        $post->published_at = $this->now();
        $post->published_by = user()->id;
        $post->owned_by     = user()->id;
        $post->created_by   = user()->id;
    }



    /**
     * Set sisterhood of the post, if it doesn't present
     *
     * @param Post   $post
     * @param string $sisterhood
     */
    private function ensureSisterhood(Post $post, string $sisterhood = '')
    {
        if (!$post->sisterhood) {
            $post->sisterhood = $sisterhood ? $sisterhood : post()::generateSisterhood();
        }
    }



    /**
     * Save a set of sister models
     *
     * @param array    $sister_models
     * @param Posttype $posttype
     * @param string   $sisterhood
     */
    private function saveSisterModels(array $sister_models, Posttype $posttype, string $sisterhood)
    {
        array_map(
             function ($sister_model) use ($posttype, $sisterhood) {
                 $this->savePostModel($sister_model, $posttype, $sisterhood);
             },
             $sister_models
        );
    }



    /**
     * Get the main post model from the request. The main post will be consider as the first locale which
     * has the non-empty title.
     *
     * @param PostakRequest $request
     * @param Posttype      $posttype
     *
     * @return Post|null
     */
    private function getMainPost(PostakRequest $request, PostType $posttype): ?Post
    {
        // iterate on all locales to get the first locale with non-empty
        // title ans consider it as the main post
        foreach ($posttype->locales_array as $locale) {
            $post = $request->mainPostModel($locale);
            if (!is_null($post)) {
                return $post;
            }
        }

        return null;
    }
}
