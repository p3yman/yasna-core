<?php namespace Modules\Posts\Http\Controllers;

use Modules\Posts\Http\Requests\PostSaveRequest;
use Modules\Yasna\Services\YasnaController;

class PostsSaveController extends YasnaController
{
    protected $base_model                = "Post";
    private $data;
    private $request;
    private $requested_model;
    private $saving_model;
    private $saved_model;
    private $should_delete_after_save  = false;
    private $should_refresh_after_save = false;
    private $has_error                 = false;
    private $redirect_after_save       = false;
    private $event_after_save          = false;
    private $debug_mode                = false; // <~~ Keep it off



    /**
     * PostsSaveController constructor.
     */
    public function __construct()
    {
        if ($this->debug_mode) {
            chalk()->clear();
        }
    }



    /**
     * This is the main method called by all the post editor events.
     * saveFolan is the next called method, where Folan is the requested action.
     *
     * @param PostSaveRequest $request
     *
     * @return string
     */
    public function save(PostSaveRequest $request)
    {
        $this->setClassProperties($request);

        $save_method = "save" . studly_case($request->_submit);
        if ($this->hasMethod($save_method)) {
            return $this->$save_method();
        }

        return $this->jsonFeedback("Method $save_method Not Found!");
    }



    /**
     * Initiates the public properties of this class, with default values.
     *
     * @param $request
     */
    public function setClassProperties($request)
    {
        $this->request         = $request;
        $this->data            = $request->toArray();
        $this->requested_model = $request->model;

        $this->setSavingModel($this->requested_model);
    }



    /**
     * Sets $has_error property, to be used as a flag of error happening.
     *
     * @return bool
     */
    public function setErrorFlag()
    {
        return $this->has_error = true;
    }



    /**
     * Responsible for deletion of the post, itself
     *
     * @return string:json
     */
    protected function saveDelete()
    {
        $deleted = $this->requested_model->delete();
        $this->setRedirectAfterSave("last-browse");
        $this->setEvent('Deleted');
        $this->saved_model = post($this->requested_model->id, true);
        return $this->generateFeedback($deleted);
    }



    /**
     * Responsible for deletion of the post parent, when it is a copy of something else.
     *
     * @return string:json
     */
    protected function saveDeleteOriginal()
    {
        $deleted = $this->requested_model->deleteWithCopies();
        $this->setRedirectAfterSave('last-browse');
        $this->setEvent('Deleted');
        return $this->generateFeedback($deleted);
    }



    /**
     * Responsible when a publish command is received
     *
     * @return string:json
     */
    protected function savePublish()
    {
        return $this->saveGeneral();
    }



    /**
     * Responsible when an unpublish command is received
     *
     * @return string:json
     */
    protected function saveUnpublish()
    {
        $this->setEvent('Unpublished');
        $done = $this->requested_model->unpublish();
        $this->saved_model = $this->requested_model->refresh();
        return $this->generateFeedback($done);
    }



    /**
     * Responsible when a save-draft command is received.
     *
     * @return string:json
     */
    protected function saveSave()
    {
        return $this->saveGeneral();
    }



    /**
     * Responsible when a send-to-editor command is received
     *
     * @return string:json
     */
    protected function saveApproval()
    {
        return $this->saveGeneral();
    }



    /**
     * Responsible when a rejection command is received.
     *
     * @return string:json
     */
    protected function saveReject()
    {
        return $this->saveGeneral();
    }



    /**
     * Responsible for saving most of the commands, called from the designated responsible method, above
     *
     * @return string:json
     */
    protected function saveGeneral()
    {
        /*-----------------------------------------------
        | Data Purification ...
        */
        $this->saveGates();
        $this->preparations();

        /*-----------------------------------------------
        | Save to Database ...
        */
        if ($this->has_error) {
            $this->addToChalk('Error!');
            return $this->jsonFeedback();
        }

        $this->addToChalk($this->data);
        $this->saved_model = $this->saving_model->batchSave($this->data);
        $this->addToChalk('Saved! :)');

        /*-----------------------------------------------
        | Actions After Save ...
        */
        if ($this->saved_model->exists) {
            $this->destroyRequestModelIfNecessary();
            $this->afterSaveServices();
        }

        /*-----------------------------------------------
        | Feedback ...
        */
        return $this->generateFeedback();
    }



    /**
     * Explores through the registered "save_gates" services and calls them one by one.
     *
     * @return bool
     */
    protected function saveGates()
    {
        $gates = module('posts')->service('save_gates')->read();

        foreach ($gates as $gate) {
            $this->data = $gate['method']($this->saving_model, $this->data);
            if (!$this->data) { // <~~ Interrupts when some error is detected.
                return $this->setErrorFlag();
            }
        }

        return true;
    }



    /**
     * Prepares the models and variables, based on the request command and the current post status
     * Automatically calls methods, named "prepare"+[command_name]+"For"+[current_post_status]
     *
     * @return void
     */
    protected function preparations()
    {
        $method = "prepare"
             . studly_case($this->request->_submit)
             . 'For'
             . studly_case($this->requested_model->editor_mood);

        if ($this->hasNotMethod($method)) {
            $this->setErrorFlag();
            return;
        }

        $this->$method();
    }



    /**
     * Prepares for publish command on a new post.
     */
    protected function preparePublishForNew()
    {
        $this->fillPublishColumns();
        $this->fillModerateColumns();
        $this->setIsDraftColumn();
        $this->setModerateNoteColumn();
        $this->setRedirectAfterSave('editor');
        $this->setEvent('Published');
    }



    /**
     * Prepares for publish command on an existing original post.
     */
    protected function preparePublishForOriginal()
    {
        $this->setIsDraftColumn();
        $this->setModerateNoteColumn();
        $this->setEvent('Updated');

        if ($this->requested_model->isNotApproved()) {
            $this->fillPublishColumns();
            $this->fillModerateColumns();
            $this->setEvent('Published');
        }
    }



    /**
     * Prepares for publish command on an existing copied version of a post, which is going to replace the original.
     */
    protected function preparePublishForCopy()
    {
        $this->setSavingModel($this->requested_model->original());
        $this->fillModerateColumns();
        $this->setIsDraftColumn();
        $this->setModerateNoteColumn();
        $this->setRedirectAfterSave('editor');
        $this->setDeleteAfterSave();
        $this->setEvent('Updated');
    }



    /**
     * Prepares for send-to-approval command on a new post.
     */
    protected function prepareApprovalForNew()
    {
        $this->setIsDraftColumn();
        $this->setModerateNoteColumn();
        $this->setRedirectAfterSave('editor');
    }



    /**
     * Prepares for send-to-approval command on a copied version of a post.
     */
    protected function prepareApprovalForCopy()
    {
        $this->setIsDraftColumn();
        $this->setModerateNoteColumn();
    }



    /**
     * Prepares for send-to-approval command on an existing post.
     */
    protected function prepareApprovalForOriginal()
    {
        $this->setIsDraftColumn();
        $this->setModerateNoteColumn();

        if ($this->requested_model->isApproved()) {
            $this->setOwnerColumn();
            $this->setCopyOfColumn($this->requested_model->id);
            $this->setSavingModel();
            $this->setRedirectAfterSave('editor');
        }
    }



    /**
     * Prepares for save-draft command on a new post.
     */
    protected function prepareSaveForNew()
    {
        $this->setIsDraftColumn(true);
        $this->setModerateNoteColumn();
        $this->setRedirectAfterSave('editor');
    }



    /**
     * Prepares for save-draft command on a copied version of a post.
     */
    protected function prepareSaveForCopy()
    {
        $this->setIsDraftColumn(true);
        $this->setModerateNoteColumn();
    }



    /**
     * Prepares for save-draft command, on an existing post.
     */
    protected function prepareSaveForOriginal()
    {
        $this->setIsDraftColumn(true);
        $this->setModerateNoteColumn();

        if ($this->requested_model->isApproved()) {
            $this->setOwnerColumn();
            $this->setSavingModel();
            $this->setCopyOfColumn($this->requested_model->id);
            $this->setRedirectAfterSave('editor');
        }
    }



    /**
     * Prepares for rejection command, on an existing post.
     */
    protected function prepareRejectForOriginal()
    {
        $this->setIsDraftColumn(true);
        $this->fillModerateColumns();

        if ($this->requested_model->isApproved()) {
            $this->setErrorFlag();
        } else {
            $this->setRedirectAfterSave('pending');
        }
    }



    /**
     * Prepares for rejection command, on a copied version which is submitted to replace a published post.
     */
    protected function prepareRejectForCopy()
    {
        $this->setIsDraftColumn(true);
        $this->fillModerateColumns();
        $this->setRedirectAfterSave('pending');
    }



    /**
     * Destroys the request model, if $should_delete_after_save property has been set in one of the preparation methods.
     */
    protected function destroyRequestModelIfNecessary()
    {
        if ($this->should_delete_after_save) {
            $this->requested_model->hardDelete();
        }
    }



    /**
     * Explores the registered 'after_save' services, set by the modules, and runs them one by pne.
     */
    protected function afterSaveServices()
    {
        service("posts:editor_handlers")->handle($this->saved_model);
        $gates = service("posts:after_save")->read();

        foreach ($gates as $gate) {
            $gate['method']($this->saved_model, $this->data);
        }
    }



    /**
     * Raises an appropriate methods, based on the $event_after_save property, set by the above preparation methods.
     */
    protected function eventRaiser()
    {
        if (!$this->event_after_save) {
            return;
        }
        $namespace       = module('posts')->namespace('Events\\Post');
        $fully_qualified = $namespace . $this->event_after_save;

        event(new $fully_qualified($this->saved_model));
    }



    /**
     * Generates an standard json feedback, according the the result of save method, and preparations done beforehand.
     *
     * @param string $is_success
     *
     * @return string:json
     */
    protected function generateFeedback($is_success = 'auto')
    {
        if ($is_success === 'auto') {
            $is_success = $this->saved_model->exists;
        }

        if ($is_success) {
            $this->eventRaiser();
        }

        return $this->jsonAjaxSaveFeedback($is_success, [
             'success_redirect' => $this->generateRedirectUrl(),
             'success_callback' => $this->generateSuccessCallback(),
             'success_refresh'  => $this->should_refresh_after_save,
        ]);
    }



    /**
     * Generates a redirect necessary, if necessary, based on the data set by the preparation methods.
     *
     * @return null|string
     */
    private function generateRedirectUrl()
    {
        switch ($this->redirect_after_save) {
            case 'editor':
                return $this->saved_model->edit_link;
            case 'pending':
                return $this->saved_model->browseLink('pending');
            case 'last-browse':
                return $this->requested_model->browseLink();
            default:
                return null;

        }
    }



    /**
     * Generates a js callback function, if necessary, based on the data set by the preparation methods.
     *
     * @return null|string
     */
    private function generateSuccessCallback()
    {
        if ($this->redirect_after_save) {
            return null;
        }

        return "divReload('divPublishPanel')";
    }



    /**
     * Fills the published_at and published_by entries of $this->data, by the current time and online user data
     * respectively.
     */
    private function fillPublishColumns()
    {
        $this->data['published_by'] = user()->id;
        if (!isset($this->data['published_at'])) {
            $this->data['published_at'] = $this->now();
        }
    }



    /**
     * Fills the owned_by entry of $this->data, by the online user id, or any specific one passed by the argument.
     *
     * @param string $user_id : online user, if not given
     */
    public function setOwnerColumn($user_id = 'auto')
    {
        if ($user_id == 'auto') {
            $user_id = user()->id;
        }

        $this->data['owned_by'] = $user_id;
    }



    /**
     * Fills the `copy_of` entry of $this->data, by the given argument.
     *
     * @param $post_id
     */
    public function setCopyOfColumn($post_id)
    {
        $this->data['copy_of'] = $post_id;
    }



    /**
     * Fills the `moderated_at` and `moderated_by` entries of $this->data, by the current time and online user data
     * respectively.
     */
    private function fillModerateColumns()
    {
        $this->data['moderated_by'] = user()->id;
        $this->data['moderated_at'] = $this->now();
    }



    /**
     * Sets `is_draft` entry of $this->data, by the given value.
     *
     * @param bool $value
     */
    private function setIsDraftColumn($value = false)
    {
        $this->data['is_draft'] = $value;
    }



    /**
     * Sets `moderate_note` entry of $this->data, by the given value.
     *
     * @param string $note
     */
    private function setModerateNoteColumn($note = null)
    {
        $this->data['moderate_note'] = $note;
    }



    /**
     * Sets the model being saved, by the given argument. A new Post instance is set, if null is given.
     *
     * @param null $model
     */
    private function setSavingModel($model = null)
    {
        if ($model) {
            $this->saving_model = $model;
        } else {
            $this->saving_model         = model('Post');
            $this->saving_model->type   = $this->requested_model->type;
            $this->saving_model->locale = $this->requested_model->locale;
        }
    }



    /**
     * Sets $this->should_delete_after_save flag, by the given value.
     *
     * @param bool $value
     */
    private function setDeleteAfterSave($value = true)
    {
        $this->should_delete_after_save = $value;
    }



    /**
     * Sets $this->should_refresh_after_save flag, by the given value.
     *
     * @param bool $value
     */
    private function setRefreshAfterSave($value = true)
    {
        $this->should_refresh_after_save = $value;
    }



    /**
     * Sets $this->redirect_after_save flag, by the given value.
     *
     * @param null $target
     */
    private function setRedirectAfterSave($target = null)
    {
        $this->redirect_after_save = $target;
    }



    /**
     * Sets the event to be raised after a successful save, by a given value.
     *
     * @param bool|string $event_name
     */
    private function setEvent($event_name = false)
    {
        $this->event_after_save = $event_name;
    }



    /**
     * Adds something to Chalk, if $this->debug_mode flag is set.
     *
     * @link  https://github.com/mhrezaei/yasna-core/wiki/C.-Chalk
     *
     * @param $data
     */
    private function addToChalk($data)
    {
        if ($this->debug_mode) {
            chalk()->add($data);
        }
    }
}
