<?php

namespace Modules\Posts\Http\Controllers\Api\V1;

use App\Models\Post;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class PostsController extends YasnaApiController
{
    /**
     * get a number of posts
     *
     * @return array
     */
    public function getPosts(SimpleYasnaRequest $request): array
    {
        $posts = post()->elector($request->toArray())->limit(10)->get()->toArray();
        //TODO: Enrich this query with taking actions to the given $page, $limit and $order

        return $this->success($posts , $request->toArray());
    }



    /**
     * get a single post
     *
     * @var string $hashid
     * @return array
     */
    public function getPost(string $hashid): array
    {
        $post = $this->findPost($hashid);

        return $this->success($post->toArray());
    }



    /**
     * find a post and throws a standard api 400 error when not found
     *
     * @param $hashid
     *
     * @return Post
     */
    protected function findPost($hashid)
    {
        $post = post()->grabHashid($hashid);

        if (!$post->exists) {
            api()->clientErrorRespond(410);
            // TODO: Throw a white-house-compatible exception
        }

        return $post;
    }
}
