<?php

namespace Modules\Posts\Http\Controllers;

use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Modules\Posts\Events\PostDeleted;
use Modules\Posts\Events\PostUndeleted;
use Modules\Posts\Http\Requests\PostDeleteRequest;
use Illuminate\Http\Request;
use Modules\Yasna\Services\YasnaController;

class PostsDeleteController extends YasnaController
{
    protected $base_model = "Post";



    /**
     * delete a single post
     *
     * @param PostDeleteRequest $request
     *
     * @return string
     */
    public function delete(PostDeleteRequest $request)
    {
        $deleted = $request->model->delete();
        $this->dispatchDeleteEvent($request->model, $deleted);

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "rowHide('tblPosts' , '$request->hashid')",
        ]);
    }



    /**
     * undelete a single post
     *
     * @param PostDeleteRequest $request
     *
     * @return string
     */
    public function undelete(PostDeleteRequest $request)
    {
        $restored = $request->model->undelete();
        $this->dispatchUndeleteEvent($request->model, $restored);

        return $this->jsonAjaxSaveFeedback($restored, [
             'success_callback' => "rowHide('tblPosts' , '$request->hashid')",
        ]);
    }



    /**
     * permanently destroy a single post
     *
     * @param PostDeleteRequest $request
     *
     * @return string
     */
    public function destroy(PostDeleteRequest $request)
    {
        $destroyed = $request->model->hardDelete();

        return $this->jsonAjaxSaveFeedback($destroyed, [
             'success_callback' => "rowHide('tblPosts' , '$request->hashid')",
        ]);
    }



    /**
     * delete selected posts
     *
     * @param Request $request
     *
     * @return string
     */
    public function massDelete(Request $request)
    {
        $posts      = $this->getSelectedPosts($request->ids);
        $total_done = 0;

        foreach ($posts as $post) {
            if ($post->canDelete()) {
                $done = $post->delete();
                $total_done += $done;
                $this->dispatchDeleteEvent($post, $done);
            }
        }

        return $this->jsonAjaxSaveFeedback($total_done, [
             'success_refresh' => true,
             'success_message' => trans("manage::forms.feed.mass_done", [
                  "count" => pd($total_done),
             ]),
        ]);
    }



    /**
     * undelete selected posts
     *
     * @param Request $request
     *
     * @return string
     */
    public function massUndelete(Request $request)
    {
        $posts      = $this->getSelectedPosts($request->ids);
        $total_done = 0;

        foreach ($posts as $post) {
            if ($post->canDelete()) {
                $done = $post->undelete();
                $total_done += $done;
                $this->dispatchUndeleteEvent($post, $done);
            }
        }

        return $this->jsonAjaxSaveFeedback($total_done, [
             'success_refresh' => true,
             'success_message' => trans("manage::forms.feed.mass_done", [
                  "count" => pd($total_done),
             ]),
        ]);
    }



    /**
     * permanently destroy selected posts
     *
     * @param Request $request
     *
     * @return string
     */
    public function massDestroy(Request $request)
    {
        $posts      = $this->getSelectedPosts($request->ids);
        $total_done = 0;

        foreach ($posts as $post) {
            if ($post->canDelete()) {
                $done = $post->hardDelete();
                $total_done += $done;
            }
        }

        return $this->jsonAjaxSaveFeedback($total_done, [
             'success_refresh' => true,
             'success_message' => trans("manage::forms.feed.mass_done", [
                  "count" => pd($total_done),
             ]),
        ]);
    }



    /**
     * get a collection of posts selected by the user
     *
     * @param string $hashids
     *
     * @return Collection
     */
    private function getSelectedPosts($hashids)
    {
        $hashid_array = explode_not_empty(',', $hashids);
        $id_array     = hashid($hashid_array);

        return post()->withTrashed()->whereIn("id", $id_array)->get();
    }



    /**
     * dispatch delete event if delete action is successful
     *
     * @param Post $model
     * @param bool $is_done
     *
     * @return void
     */
    private function dispatchDeleteEvent($model, $is_done)
    {
        if ($is_done) {
            event(new PostDeleted($model));
        }
    }



    /**
     * dispatch undelete event if undelete action is successful
     *
     * @param Post $model
     * @param bool $is_done
     *
     * @return void
     */
    private function dispatchUndeleteEvent($model, $is_done)
    {
        if ($is_done) {
            event(new PostUndeleted($model));
        }
    }
}
