<?php

namespace Modules\Posts\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class PostaksEditController extends YasnaController
{
    protected $view_folder = "posts::postaks";
    protected $base_model  = "post";



    /**
     * Show modal of creating a post
     *
     * @param string $type_slug Slug of posttype which want to add a post to it
     *
     * @return string|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create($type_slug)
    {
        $posttype = model('posttype', $type_slug);
        // check post-type exists
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has creating permission
        if ($posttype->cannot('create')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // first create model instance and initialize it
        $model           = model('post');
        $model->type     = $type_slug;
        $model->owned_by = user()->id;

        // create view variables
        $title_label = trans('posts::postak.add_post_title', ['type' => $posttype->title]);

        // finally return results
        return $this->view("form", compact('title_label', 'model', 'posttype'));
    }



    /**
     * Show modal of creating a post
     *
     * @param string $type_slug Slug of posttype which want to add a post to it
     *
     * @return string|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($type_slug, $hashid)
    {
        // check post-type exists
        $posttype = model('posttype', $type_slug);
        if (!$posttype->exists) {
            return $this->jsonFeedback(trans('posts::postak.posttype_not_found'));
        }

        // check user has editing permission
        if ($posttype->cannot('edit')) {
            return $this->jsonFeedback(trans('posts::postak.unauthorized_action'));
        }

        // check post exists
        $model = model('post', $hashid);
        if (!$model->exists) {
            return $this->jsonFeedback(trans('posts::postak.post_not_found'));
        }

        // create view variables
        $title_label = trans('posts::postak.edit_post_title', ['name' => $model->title]);

        // finally return results
        return $this->view("form", compact('title_label', 'model', 'posttype'));
    }
}
