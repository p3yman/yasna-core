<?php

namespace Modules\Posts\Mappers;

use App\Models\Post;
use Modules\SiteMap\Services\YasnaMapper;

class PostsMapper extends YasnaMapper
{
    /**
     * @inheritdoc
     */
    protected function query($updated_after)
    {
        $elector = [
             "criteria" => "published",
        ];

        return post()->elector($elector)->whereDate('updated_at', '>=', $updated_after);
    }



    /**
     * @inheritdoc
     */
    protected function getLink($model)
    {
        return $model->direct_link;
    }
}
