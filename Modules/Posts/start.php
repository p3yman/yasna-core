<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("post")) {
    /**
     * Model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Post
     */
    function post($id_or_hashid = 0, $with_trashed = false)
    {
        return model('Post', $id_or_hashid, $with_trashed);
    }
}

if (!function_exists("posttype")) {

    /**
     * Model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Posttype
     */
    function posttype($id_or_hashid = 0, $with_trashed = false)
    {
        if($id_or_hashid and !$with_trashed) {
            return posttypes()->find($id_or_hashid);
        }

        return model('Posttype', $id_or_hashid, $with_trashed);
    }
}

if (!function_exists("posttypes")) {
    /**
     * get an instance of Posttypes singleton (which in turn extends from standard eloquent collection)
     *
     * @return \Modules\Posts\Services\PosttypesSingleton
     */
    function posttypes()
    {
        return new \Modules\Posts\Services\PosttypesSingleton();
    }
}

if (!function_exists("array_super_merge")) {

    function array_super_merge(... $arrays) //TODO: Move this to Yasna Helpers
    {
        $result = [];
        foreach ($arrays as $array) {
            $result = array_merge($result, $array);
        }

        return $result;
    }
}
