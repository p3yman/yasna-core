
function postsInit() {
}

function postFormChange() {
	let $flag = $('#txtChangeWarning');
	if ($flag.val() == '1') {
		forms_log('change detected!');
		$('#divChangeWarningPanel .panel').slideDown('fast');
		$flag.val('0');
	}
	$('#txtChangeDetected').val('1');
}


function postToggleTitle2() {
	let $txtTitle2 = $('#txtTitle2');

	$('#lblTitle2,#txtTitle2-container').toggle();
	if ($txtTitle2.is(':visible')) {
		$txtTitle2.focus();
	}
	else {
		$txtTitle2.val('');
	}
}

function postToggleSchedule($mood) {
	let $schedule = $("#divSchedule");
	let $link = $("#lnkSchedule");
	let $date = $("#txtPublishDate");
	let $flag = $("#txtScheduleFlag");

	if (!$mood) {
		forms_log("mood=" + $mood);
		if ($($schedule).is(':visible'))
			$mood = 'hide';
		else
			$mood = 'show';
	}

	switch ($mood) {
		case 'show' :
			$schedule.slideDown('fast');
			$flag.val('1');
			$link.hide();
			$date.focus();
			break;

		case 'hide' :
			$schedule.slideUp('fast');
			$link.show();
			$date.val('');
			$flag.val('');
			$("#cmbPublishDate").val('08:00');
			$('.selectpicker').selectpicker('refresh');
			break;

	}

	return $mood;

}

function postSlugCheck() {
	let $divFeedback = $("#divSlugFeedback");
	$divFeedback.html('...').addClass('loading');

	forms_log('[' + $("#txtSlug").val() + ']');

	$.ajax({
		url  : url("manage/posts/check_slug/" + $("#txtId").val() + "/" + $("#txtType").val() + "/" + $("#txtLocale").val() + '' + '/' + $("#txtSlug").val() + ' /' + 'p'),
		cache: false
	})
		.done(function (html) {
			$($divFeedback).html(html);
			$($divFeedback).removeClass('loading');
		});

}

function postsAction($command, $model_id) {
	forms_log('action: ' + $command);
	switch ($command) {
		case 'adjust_publish_time' :
			postToggleSchedule('show');
			break;

		case 'refer_back' :
			$('#divReferBack').slideDown('fast' , function() {
				setTimeout(function() {
					$('#txtModerateNote').focus() ;
				} , 100 )
			}) ;
			break;

		case 'delete':
			$("#divDelete").slideDown("fast") ;
			break;

		case 'unpublish':
			$('#divUnpublish').slideDown('fast') ;
			break;

		case 'send_for_approval' :
			$("#btnApproval").click();
			break;

		case 'refer_to' :
			masterModal(url('manage/posts/act/' + $model_id + '/owner/1'));
			break;

	}
}
