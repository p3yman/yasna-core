<?php

namespace Modules\Posts\Services;

use App\Models\Posttype;
use Illuminate\Database\Eloquent\Collection;

class PosttypesSingleton
{
    /**
     * keep the singleton static instance of the posttypes
     *
     * @var Collection|null
     */
    protected static $singleton = null;

    /**
     * keep the dynamic collection of the instance, built on the singleton instance
     *
     * @var Collection|mixed
     */
    protected $payload;



    /**
     * PosttypesSingleton constructor.
     */
    public function __construct()
    {
        $this->loadSingleton();
        $this->payload = clone static::$singleton;
    }



    /**
     * magic call method, to proxy all Eloquent Collection methods (at the end of this object's chain of conditions)
     *
     * @param string $method_name
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method_name, $parameters)
    {
        $result = $this->payload->$method_name(... $parameters);

        if (!is_object($result) or get_class($result) != 'Illuminate\Database\Eloquent\Collection') {
            return $result;
        }

        $this->payload = $result;

        return $this;
    }



    /**
     * get the collection
     *
     * @return Collection
     */
    public function get()
    {
        return $this->payload;
    }



    /**
     * filter the collection by feature(s)
     *
     * @param string|array $features
     *
     * @return $this
     */
    public function having($features)
    {
        $features = (array)$features;

        foreach ($this->payload as $key => $item) {
            if (!$item->features->whereIn('slug', $features)->count()) {
                $this->payload->forget($key);
            }
        }

        return $this;
    }



    /**
     * filter the collection by the absence of feature(s)
     *
     * @param string|array $features
     *
     * @return $this
     */
    public function notHaving($features)
    {
        $features = (array)$features;

        foreach ($this->payload as $key => $item) {
            if ($item->features->whereIn('slug', $features)->count()) {
                $this->payload->forget($key);
            }
        }

        return $this;
    }



    /**
     * find a record by its id, hashid or slug
     *
     * @param string|int $identifier
     *
     * @return Posttype
     */
    public function find($identifier)
    {
        if (is_numeric($identifier)) {
            return $this->findById($identifier);
        }

        if ($id = hashid($identifier)) {
            return $this->findById($id);
        }

        return $this->locateBySlug($identifier);
    }



    /**
     * find a record by its slug
     *
     * @param string $slug
     *
     * @return Posttype
     */
    public function locateBySlug(string $slug)
    {
        $instance = $this->payload->where('slug', $slug)->first();

        return $this->getSafeInstance($instance);
    }



    /**
     * find a record by its hashid
     *
     * @param string $hashid
     *
     * @return Posttype
     */
    public function findByHashid(string $hashid)
    {
        if (is_numeric($hashid)) {
            return $this->getSafeInstance(null);
        }

        return $this->findById(hashid($hashid));
    }



    /**
     * find a record by its id
     *
     * @param int $id
     *
     * @return Posttype
     */
    public function findById(int $id)
    {
        $instance = $this->payload->where("id", $id)->first();

        return $this->getSafeInstance($instance);
    }



    /**
     * get a safe instance of the Posttype
     *
     * @param null|Posttype $instance
     *
     * @return Posttype
     */
    protected function getSafeInstance($instance)
    {
        if (!$instance) {
            return new Posttype();
        }

        return $instance;
    }



    /**
     * set static singleton
     *
     * @return void
     */
    protected function loadSingleton()
    {
        if (is_null(static::$singleton)) {
            static::$singleton = Posttype::with(['features', 'inputs'])->get();
        }
    }
}
