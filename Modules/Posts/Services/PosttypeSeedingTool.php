<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/14/18
 * Time: 3:14 PM
 */

namespace Modules\Posts\Services;


use App\Models\Posttype;

class PosttypeSeedingTool
{
    /**
     * Seeds data to the `posttypes` table.
     *
     * @param array $data
     */
    public function seedPosttypes(array $data)
    {
        yasna()->seed('posttypes', array_column($data, 'posttype'));

        foreach ($data as $datum) {
            $slug     = $datum['posttype']['slug'];
            $posttype = model('posttype', $slug);

            $this->seedSinglePosttypeInformation($posttype, $datum);
        }
    }



    /**
     * Seeds the information of a single posttype.
     *
     * @param Posttype $posttype
     * @param array    $posttype_information
     */
    protected function seedSinglePosttypeInformation(Posttype $posttype, array $posttype_information)
    {
        $features = ($posttype_information['features'] ?? []);
        $inputs   = ($posttype_information['inputs'] ?? []);

        $this->seedPosttypeFeatures($posttype, $features);
        $this->seedPosttypeInputs($posttype, $inputs);
    }



    /**
     * Seeds features and required inputs for the specified posttype.
     *
     * @param Posttype $posttype
     * @param array    $features
     */
    protected function seedPosttypeFeatures(Posttype $posttype, $features = [])
    {
        $posttype->attachFeatures($features);
        $posttype->attachRequiredInputs();
        $posttype->cacheFeatures();
    }



    /**
     * Seeds inputs for the specified posttype.
     *
     * @param Posttype $posttype
     * @param array    $inputs_slugs
     */
    protected function seedPosttypeInputs(Posttype $posttype, $inputs_slugs = [])
    {
        $inputs     = model('input')->whereIn('slug', $inputs_slugs)->get();
        $inputs_ids = $inputs->pluck('id')->toArray();

        $posttype->inputs()->syncWithoutDetaching($inputs_ids);
    }
}
