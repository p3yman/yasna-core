<?php
return [
     'features' => [
          "title2"            => "Second Title",
          "long_title"        => "Long Title",
          "text"              => "Text",
          "abstract"          => "Abstract",
          "pin"               => "Pin",
          "rss"               => "RSS",
          "domains"           => "Domains Support",
          "single_view"       => "Single View",
          'list_view'         => "List View",
          'searchable'        => "Searchable",
          'schedule'          => "Scheduling",
          'event'             => "Event",
          'register'          => "User Register",
          'visibility_choice' => "Visibility Choice",
          'template_choice'   => "Template Choice",
          'slug'              => "Slug Choice",
          'manage_sidebar'    => "Manage Sidebar",
          'locales'           => "Multilingual",
          'dashboard'         => "Dashboard Widget",
          'minimal_post'      => 'Minimal',
          "api_discoverable"  => "API Discoverable",
          "related_posts"     => "Related Posts",
          "slideshow"         => "Slide Show",
     ],
];
