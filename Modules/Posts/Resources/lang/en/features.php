<?php
return [
     'title'                => "Post Features",
     'plural'               => "Features",
     'singular'             => "Feature",
     'feedback'             => "Feedback",
     'creation_warning'     => "Do you know that newly created features should be used in the code?",
     'slug_edition_warning' => "Do you know changing the slug might break the code?",

     'event_settings'    => "Event Settings",
     'register_settings' => "Registration Settings",
     'register'          => "Register",

     'locales' => 'Languages',

     'related_posts' => 'Related Posts',
];
