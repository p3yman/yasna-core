<?php
return [
     'title'                    => "Posttypes",
     'plural'                   => "Types",
     'singular'                 => "Type",
     'full_singular'            => "Posttype",
     'locales_hint'             => "Separate languages with comma (,). If the field is left blank, Persian language will be activated.",
     'locale_titles'            => "Title in Other Languages",
     'input_selector_notice'    => "Required fields will be added automatically, based on features; But for security's sake, useless fields won't be deleted, automatically.",
     'header_title_placeholder' => "Public Content",

     'templates' => [
          'post'      => "Post",
          'album'     => "Album",
          'slideshow' => "Slide Show",
          'dialogue'  => "Dialogue",
          'faq'       => "FAQ",
          'product'   => "Shop Product",
          'event'     => "Event",
          'special'   => "Special",
          'postak'    => "Postak",
     ],

];
