<?php
return [
     'title'                 => "Inputs",
     'plural'                => "Inputs",
     'singular'              => "Input",
     'additional'            => "Independent Inputs",
     'selector_placeholder'  => "Choose new input from this list.",
     'slug_edition_warning'  => "Do you know changing the slug might break the code?",
     'creation_warning'      => "Remember, in case you add a new validation, it should be introduced in Laravel \"validation\" file.",
     'default_value'         => "Default Value",
     'without_default_value' => "Without Default Value",
     'default_value_setting' => "Set Default Value",
     'options_hint'          => "Write each options in a new line. First write the value, and if desired write Persian value after \":\".",

     'type' => [
          'upstream'   => "Available to Developers",
          'downstream' => "Available to Admins",
          'editor'     => "Available in Post Editor",
     ],
];
