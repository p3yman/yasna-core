<?php
return [
    'all'       => "All",
    'published' => "Published",
    'scheduled' => "Scheduled",
    'pending'   => "Pending",
    'drafts'    => "Drafts",
    'my_posts'  => "My Posts",
    'my_drafts' => "My Drafts",
    'bin'       => "Bin",
    'approved'  => "Approved",
];
