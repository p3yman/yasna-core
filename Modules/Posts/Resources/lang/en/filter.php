<?php
return [
     'submit_filters' => 'Submit',
     'remove_filters' => 'Remove',
     'blank_item'     => 'Choose',
     'creators'       => 'Post creators',
     'filtered'       => 'Filtered',
     'date_from'      => 'From',
     'date_to'        => 'To',
     'asc'            => 'Ascending',
     'desc'           => 'Descending',
     'sort'           => 'Sort',
     'filters'        => 'Filters',
];
