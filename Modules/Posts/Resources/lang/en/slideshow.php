<?php
return [
     "title"          => "Slide Show Wizard",
     "settings"       => "Settings",
     "post_title"     => "Slide Show Title",
     "add_slide"      => "Add Slide",
     "preview"        => "Preview",
     "new_slide"      => "New Slide",
     "save_slideshow" => "Save Slide Show",
     "deactivated"    => "(Deactivated)",
     "notice"         => "The interface you see here is different from actual slide show in template.",


     "view_slide"    => "View slide in slide show.",
     "main_title"    => "Title",
     "subtitle"      => "Subtitle",
     "description"   => "Description",
     "button"        => "Button",
     "button_title"  => "Button Title",
     "button_link"   => "Link",
     "button_target" => "Open link in new window",
     "position"      => "Position",
     "theme"         => "Text Color",
     "publish"       => "Publish Method",
     "auto_publish"  => "Publish automatically at specified time.",
     "publish_at"    => "Publish Date",

     "auto_change"    => "Auto Change",
     "speed"          => "Speed in Millisecond",
     "timeout"        => "Change Interval in Millisecond",
     "pause_on_hover" => "Pause when mouse enters.",

     "success_save" => "Successfully saved",
     "error_save"   => "Server error",

     "top-right"     => "Top-Right",
     "top-center"    => "Top-Center",
     "top-left"      => "Top-Left",

     "bottom-right"  => "Bottom-Right",
     "bottom-center" => "Bottom-Center",
     "bottom-left"   => "Bottom-Left",

     "center-right"  => "Center-Right",
     "center-center" => "Center-Center",
     "center-left"   => "Center-Left",

     "dark"          => "Dark",
     "light"         => "Light",

];
