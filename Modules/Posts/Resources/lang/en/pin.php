<?php
return [
    'put_command'    => "Pin the Post",
    'remove_command' => "Unpin Post",
    'description'    => "Pinned post will be shown on top of other posts.",
    'put_alert'      => "Current pinned post will be unpinned.",
    'remove_alert'   => "No pinned post will remain.",
    'pinned'         => "Pinned",
];
