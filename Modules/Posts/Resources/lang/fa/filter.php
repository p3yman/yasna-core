<?php
return [
     'submit_filters' => 'اعمال فیلترها',
     'remove_filters' => 'حذف فیلترها',
     'blank_item'     => 'انتخاب کنید',
     'creators'       => 'نویسنده‌ها',
     'filtered'       => '(فیلترشده)‌',
     'date_from'      => 'شروع زمان‌',
     'date_to'        => 'پایان زمان‌',
     'asc'            => 'صعودی',
     'desc'           => 'نزولی',
     'sort'           => 'مرتب‌سازی',
     'filters'        => 'فیلترها',
];
