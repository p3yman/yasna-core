<?php
return [
    'all'       => "همه",
    'published' => "منتشرشده‌ها",
    'scheduled' => "صف انتشار",
    'pending'   => "منتظر تأیید",
    'drafts'    => "پیش‌نویس‌ها",
    'my_posts'  => "نوشته‌های من",
    'my_drafts' => "پیش‌نویس‌های من",
    'bin'       => "زباله‌دان",
    'approved'  => "تأییدشده‌ها",
];
