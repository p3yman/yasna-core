<div class="panel">
	<div class="panel-body">
		<div class="clearfix" id="js_slideShowData">
			{!!
		    widget('input')
		    ->label('tr:posts::slideshow.post_title')
		    ->inForm()
		    ->name('')
		    ->value($posttype->title)
		    ->disabled()
		 !!}
		</div>

		<div class="mv30 clearfix">
			{!!
			    widget('button')
			    ->label('tr:posts::slideshow.preview')
			    ->class('btn-primary pull-right mh js_showPreview')
			    ->icon('eye')
			 !!}
			{!!
			    widget('button')
			    ->label('tr:posts::slideshow.add_slide')
			    ->class('btn-success pull-right mh js_addNewSlide')
			    ->icon('plus-circle')
			 !!}
		</div>

		<hr>

		<div class="js_slidesContainer">
			@foreach($posts as $post)
				@include('posts::slideshow.templates.slide-panel',[
					"id" => "key_" . $post->id ,
					"view_slide" => !empty($post->deleted_at)?"0":"1" ,
					"title" => $post->title ,
					"subtitle" => $post->title2 ,
					"description" => $post->long_title ,
					"image_hash" => $post->featured_image,
					"image"		   => fileManager()->file($post->featured_image)->getUrl() ,
					"button_title" => $post->getMeta('button_title') ,
					"button_link"   => $post->getMeta('button_link') ,
					"button_target" => $post->getMeta('button_target') ,
					"position" => $post->getMeta('slide_position') ,
					"theme" => $post->getMeta('slide_theme') ,
					"auto_publish" => $post->getMeta('slide_auto_publish') ,
					"publish_at" => $post->published_at ,
					'post'  => $post
				])
			@endforeach

		</div>
	</div>
</div>
