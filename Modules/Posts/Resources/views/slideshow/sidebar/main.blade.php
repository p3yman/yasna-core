<div class="panel text-center">
	<div class="panel-body">
		{!!
			widget('button')
			->id('js_saveSlideShow')
			->class('btn-success')
			->label('tr:posts::slideshow.save_slideshow')
		 !!}
	</div>
</div>

<div class="panel" id="js_slideShowSetting">
	<div class="panel-heading">
		{{ trans('posts::slideshow.settings') }}
	</div>

	<div class="panel-body">
		{!!
				widget('checkbox')
				->name('auto')
				->label('tr:posts::slideshow.auto_change')
				->value($posttype->getMeta('slideshow_auto'))
			 !!}

		{!!
			widget('checkbox')
			->name('pause')
			->label('tr:posts::slideshow.pause_on_hover')
			->value($posttype->getMeta('slideshow_pause'))
		 !!}

		{!!
			widget('input')
			->name('speed')
			->label('tr:posts::slideshow.speed')
			->type('number')
			->value($posttype->getMeta('slideshow_speed'))
		 !!}

		{!!
			widget('input')
			->name('timeout')
			->label('tr:posts::slideshow.timeout')
			->type('number')
			->value($posttype->getMeta('slideshow_timeout'))
		 !!}

	</div>
</div>
