<script>

    $(document).on("click", ".panel-heading a", function () {
        $(".btn_featured_image").fileManagerModal();
    });

    /**
     * set img after user upload image
     *
     * @param  img_id
     * @param  url
     */
    function setImageInImg(img_id, url) {
        $("#" + img_id).attr('src', url);
    }

    function successMessage() {
        setTimeout(function () {
            $.notify("{{trans_safe('posts::slideshow.success_save')}}", {status: 'success'});
        }, 100);
    }

    function errorMessage() {
        setTimeout(function () {
            $.notify("{{trans_safe('posts::slideshow.error_save')}}", {status: 'danger'});
        }, 100);
    }

    $("#js_saveSlideShow").click(function () {
        $(this).addClass('disabled')
    })


    function deleteImage(ele,id, default_image) {
        if (id == 'temp'){
            $("#image-" + id).attr('src', default_image);
            return;
        }

        $(ele).addClass('disabled');
        $.ajax({
            url    : url('manage/slideshows/image/delete'),
            type   : "json",
            method : "POST",
            data   : {
                "_token" : $("#form_token").val(),
                post_id  : id
            },
            success: function () {
                successMessage();
                $(ele).removeClass('disabled');
                $('#panel-' + id).find('[name=image_hash]').val('');
                $("#image-" + id).attr('src', default_image);
            },
            error  : function () {
                errorMessage();
                $(ele).removeClass('disabled');
            }
        })
    }
</script>
