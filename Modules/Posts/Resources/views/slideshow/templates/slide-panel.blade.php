<div class="js_slideItem panel panel-primary" id='{!! $id or "@{{id}}" !!}'>
	<div class="panel-heading clearfix">
		<h4 class="m0 mt-sm pull-left">
			<i class="fa fa-bars mr js_slideHandle" style="display:inline-block;"></i>
			<a href='#panel-{!! $id or "@{{id}}" !!}' data-title-getter data-toggle="collapse">
				@if(empty($title))
					@if(isset($title))
						...
					@else
						{{	trans('posts::slideshow.new_slide')}}
					@endif
				@else
					{{$title}}
				@endif
				{{--{{ $title or trans('posts::slideshow.new_slide') }}--}}
			</a>
			<span class="deactivated text-muted f14 {{ (isset($view_slide) and !$view_slide) ? "": "hidden"  }}">
				{{ trans('posts::slideshow.deactivated') }}
			</span>
		</h4>

		<div class="pull-right">
			@if($posttype->can('delete'))
				@if(!empty($post->deleted_at))
					<button class="btn btn-danger btn-sm js_removeSlide">
						<i class="fa fa-trash-o m0"></i>
					</button>
				@endif
			@endif
			<button class="btn btn-primary btn-sm js_previewer">
				<i class="fa fa-eye m0"></i>
				<i class="fa fa-eye-slash m0"></i>
			</button>
		</div>
	</div>

	<div id='panel-{!! $id or "@{{id}}" !!}' class="panel-collapse collapse">
		<div class="panel-body">
			<div class="js_slideFormEditor">
				<div class="row">
					<div class="col-sm-3 text-center">

					<span style="position: relative; display: inline-block;">
						<a href="#fileManage">
							@php($image_id="image-hash-".uniqid())
							{!!
								widget('hidden')
								->id($image_id)
								->name('image_hash')
								->value($image_hash ?? '')
							 !!}
							<img src="{{ $image or Module::asset('posts:images/img-placeholder.png') }}" alt="Image"
								 class="img-responsive img-thumbnail js_slideImage btn_featured_image"
								 id="image-{!! $id or "@{{id}}" !!}"
								 data-file-manager-input="{{$image_id}}"
								 data-file-manager-preview="div-featured-image-inside"
								 data-file-manager-callback="setImageInImg('image-{!! $id or "@{{id}}" !!}','__url__')"
								 data-file-manager-file-type="image"
								 data-file-manager-output-type="hashid"
								 data-file-manager-default-folder="posttype___{{ $posttype->hashid }}"
							>
						</a>

						<button class="btn btn-danger btn-sm" style="position: absolute; top: 0; left: 0;"
								onclick="deleteImage(this,'{!! $id or 'temp' !!}','{{Module::asset('posts:images/img-placeholder.png')}}')">
							<i class="fa fa-times m0"></i>
						</button>

					</span>

					</div>
					<div class="col-sm-9">
						<fieldset>
							{!!
							    widget('checkbox')
							    ->label('tr:posts::slideshow.view_slide')
							    ->name('view_slide')
							    ->class('js_slideDeactivated')
							    ->value(isset($view_slide)? $view_slide : true)
							 !!}
						</fieldset>

						<div class="clearfix mt-xl">
							{!!
								widget('input')
								->label('tr:posts::slideshow.main_title')
								->class('js_titleSetter')
								->name('slide_title')
								->value($title ?? "")
							 !!}
							{!!
								widget('input')
								->label('tr:posts::slideshow.subtitle')
								->name('slide_subtitle')
								->value( $subtitle ?? "")
							 !!}
							{!!
								widget('textarea')
								->name('slide_description')
								->label('tr:posts::slideshow.description')
								->value($description ?? "")
							 !!}
						</div>

						<div class="clearfix mt-xl">
							<legend>
								{{ trans('posts::slideshow.button') }}
							</legend>
							{!!
								widget('input')
								->name('button_title')
								->label('tr:posts::slideshow.button_title')
								->value($button_title ?? "")
							 !!}

							{!!
								widget('input')
								->name('button_link')
								->label('tr:posts::slideshow.button_link')
								->value($button_link ?? '')
							 !!}

							{!!
								widget('checkbox')
								->name('button_target')
								->label('tr:posts::slideshow.button_target')
								->value( $button_target ?? 0)
							 !!}
						</div>

						<div class="clearfix mt-xl">
							<legend>
								{{ trans('posts::slideshow.position') }}
							</legend>

							{!!
								widget('combo')
								->options([
									[
										"value" => "top-right" ,
										"caption" => trans("posts::slideshow.top-right") ,
									],
									[
										"value" => "top-center" ,
										"caption" => trans("posts::slideshow.top-center") ,
									],
									[
										"value" => "top-left" ,
										"caption" => trans("posts::slideshow.top-left"),
									],
									[
										"value" => "bottom-right" ,
										"caption" => trans("posts::slideshow.bottom-right") ,
									],
									[
										"value" => "bottom-center" ,
										"caption" => trans("posts::slideshow.bottom-center") ,
									],
									[
										"value" => "bottom-left" ,
										"caption" => trans("posts::slideshow.bottom-left") ,
									],

											[
										"value" => "center-right" ,
										"caption" => trans("posts::slideshow.center-right") ,
									],
									[
										"value" => "center-center" ,
										"caption" => trans("posts::slideshow.center-center") ,
									],
									[
										"value" => "center-left" ,
										"caption" => trans("posts::slideshow.center-left") ,
									],
								])
								->name('position')
								->captionField('caption')
								->valueField('value')
								->value($position ?? '')
							 !!}
						</div>

						<div class="clearfix mt-xl">
							<legend>
								{{ trans('posts::slideshow.theme') }}
							</legend>

							{!!
								widget('combo')
								->options([
									[
										"value" => "" ,
										"caption" => "" ,
									],
									[
										"value" => "dark" ,
										"caption" => trans('posts::slideshow.dark')
									],
									[
										"value" => "light" ,
										"caption" => trans('posts::slideshow.light') ,
									]
								])
								->name('theme')
								->captionField('caption')
								->valueField('value')
								->value($theme ?? '')
							 !!}
						</div>

						<div class="clearfix mt-xl">
							<legend>
								{{ trans('posts::slideshow.publish') }}
							</legend>

							{!!
							    widget('checkbox')
							    ->name('auto_publish')
							    ->value($auto_publish ?? "")
							    ->class('js_autoPublish')
							    ->label('tr:posts::slideshow.auto_publish')
							 !!}

							<div class="js_publishAt {{ (isset($auto_publish) and $auto_publish) ? "": "hidden"  }}"
								 data-src="{{url('/slideshows/date-picker/')}}"
								 id="slide-date-{!! $id or "@{{id}}" !!}"
							>
								{{--@dd($publish_at)--}}
								{!!
									widget('persianDatePicker')
									->name('publish_at')
									->label('tr:posts::slideshow.publish_at')
									->value($publish_at ?? "")
								 !!}
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="js_slidePreview"></div>
		</div>
	</div>
</div>
