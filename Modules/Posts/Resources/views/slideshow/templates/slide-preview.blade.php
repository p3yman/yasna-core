<div class="slideshows">
	<img src="@{{image}}" alt="Image">

	<div class="content @{{position}} @{{theme}}">
		<div class="title">@{{title}}</div>
		<div class="subtitle">@{{subtitle}}</div>
		<div class="description">@{{description}}</div>
		<a href="@{{button_link}}" target="@{{button_target}}">@{{button_title}}</a>
	</div>
</div>
