@extends('manage::layouts.template')

@section('content')
	<div class="row">
		<div class="col-md-9 col-sm-8">
			@include('posts::slideshow.editor.main')
		</div>
		<div class="col-md-3 col-sm-4">
			@include('posts::slideshow.sidebar.main')
		</div>
	</div>

	{!!
		widget('hidden')
		->id('js_slideshowData')
		->name('slideshow_data')
		->value('')
	 !!}

	{!!
	widget('hidden')
	->id('form_token')
	->name('form_token')
	->value(csrf_token())
     !!}

	{!!
		widget('hidden')
		->id('posttype-id')
		->name('posttype')
		->value($posttype->hashid)
	!!}

	<script id="js_slideTemplate" type="negar/template">
		 @include('posts::slideshow.templates.slide-panel')
	</script>
	<script id="js_slidePreviewTemplate" type="negar/template">
	 	@include('posts::slideshow.templates.slide-preview')
	</script>

	@include('posts::slideshow.templates.actions-js')
@endsection

@section('html_footer')
	@include('posts::slideshow.modal')
@append

