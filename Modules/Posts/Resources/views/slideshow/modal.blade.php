<div id="js_previewModal" class="modal fade previewModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			{!!
				widget('modal')
		        	->label('tr:posts::slideshow.preview')
			 !!}
			<div class="modal-body" >
				<div role="alert" class="alert alert-warning">
					{{ trans('posts::slideshow.notice') }}
				</div>
				<div id="js_generalPreview"></div>
			</div>
		</div>
	</div>
</div>
