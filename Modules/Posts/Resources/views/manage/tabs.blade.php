@include("manage::widgets.tabs" , [
	'current' =>  $page[1][0] ,
	'tabs' => [
		["published" , trans('posts::criteria.published')],
		["scheduled" , trans('posts::criteria.scheduled')],
		["pending" , trans('posts::criteria.pending')],
		["my_posts" , trans('posts::criteria.my_posts') , 0 , $posttype->can('create')],
		["my_drafts" , trans('posts::criteria.my_drafts'), 0 , $posttype->can('create')],
		["bin" , trans('posts::criteria.bin') ],
		["$locale/search" , trans('manage::forms.button.search')],
	] ,
])
