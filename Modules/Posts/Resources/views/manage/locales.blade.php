@include("manage::layouts.modal-start" , [
	'form_url' => '#' ,
	'modal_title' => trans("posts::general.locales"),
])

<div class="modal-body">


	{{--
	|--------------------------------------------------------------------------
	| Readonly fields
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'hashid' ,
		'value' => $model->hashid,
	])

	@include('manage::forms.input' , [
		'name' => '',
		'label' => trans('validation.attributes.title'),
		'value' => $model->title ,
		'extra' => 'disabled' ,
	])


	{{--
	|--------------------------------------------------------------------------
	| Insider
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.group-start")
		<div style="width: 50%">
			@include("posts::editor.locales-inside")
		</div>

	@include("manage::forms.group-end")


</div>

@include("manage::layouts.modal-end")