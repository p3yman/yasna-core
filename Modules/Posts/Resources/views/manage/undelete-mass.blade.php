@include("manage::layouts.modal-mass" , [
	'form_url' => route('post-save-undelete-mass') ,
	'modal_title' => trans("manage::forms.button.undelete") ,
	'save_label' => trans("manage::forms.button.undelete") ,
	'save_shape' => "success" ,
]   )
