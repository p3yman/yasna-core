@include("manage::layouts.modal-start" , [
	'form_url' => route('post-save-pin') ,
	'modal_title' => $model->id? trans('manage::forms.button.edit') : trans('manage::forms.button.add'),
])

<div class="modal-body">

	{{--
	|--------------------------------------------------------------------------
	| id and Inputs
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'hashid' ,
		'value' => $model->hashid,
	])

	@include('manage::forms.input' , [
		'name' => '',
		'label' => trans('validation.attributes.title'),
		'value' => $model->title ,
		'extra' => 'disabled' ,
	])

	{{--
	|--------------------------------------------------------------------------
	| Notices
	|--------------------------------------------------------------------------
	|
	--}}
	@include('manage::forms.group-start')

	@include("manage::forms.note" , [
		'text' => trans("posts::pin.description"),
		'shape' => "info" ,
		'condition' => !$model->pinned ,
	]     )

	@include("manage::forms.note" , [
		'text' => trans("posts::pin.put_alert"),
		'shape' => "warning" ,
		'condition' => !$model->pinned ,
	]     )

	@include("manage::forms.note" , [
		'text' => trans("posts::pin.remove_alert") ,
		'shape' => "warning" ,
		'condition' => $model->pinned ,
	]     )

	@include('manage::forms.group-end')


	{{--
	|--------------------------------------------------------------------------
	| Button
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::forms.buttons-for-modal" , [
		'save_label' => $model->pinned?  trans("posts::pin.remove_command") : trans('posts::pin.put_command') ,
		'save_shape' => $model->pinned? 'warning' : 'primary' ,
		'save_value' => $model->pinned? 'remove' : 'put' ,
	] )
</div>

@include("manage::layouts.modal-end")