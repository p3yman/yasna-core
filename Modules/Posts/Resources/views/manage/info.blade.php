@include("manage::layouts.modal-start" , [
	'form_url' => '#' ,
	'modal_title' => trans("posts::general.info"),
])

<div class="modal-body">


	{{--
	|--------------------------------------------------------------------------
	| Header
	|--------------------------------------------------------------------------
	|
	--}}
	<div class="w90">
		@include("manage::widgets.grid-text" , [
			'text' => $model->title,
			'icon' => $model->posttype->icon ,
			'size' => "20" ,
			'class' => "font-yekan" ,
		]     )


		@include("manage::widgets.grid-text" , [
			'condition' => $model->has('title2') and $model->title2 ,
			'text' => $model->title2 ,
			'class' => "font-yekan" ,
			'size' => "15" ,
		]      )

		@include("posts::manage.badges")
	</div>

	{{--
	|--------------------------------------------------------------------------
	| People and Dates
	|--------------------------------------------------------------------------
	|
	--}}
	<div class="w90">
		@include("manage::widgets.grid-date" , [
			'text' => trans('manage::forms.general.created_at').': ',
			'text2' => trans('manage::forms.general.by').' '.$model->creator->full_name,
			'date' => $model->created_at,
		])

		@include("manage::widgets.grid-tiny" , [
			'text' => trans('posts::general.post_owner').': '.$model->getPerson('owned_by')->full_name,
			'link' => "modal:manage/posts/act/-hashid-/owner" ,
			'icon' => "user-o" ,
		]     )
		@include("manage::widgets.grid-date" , [
			'text' => trans('posts::general.publish').': ',
			'text2' => trans('manage::forms.general.by').' '.$model->publisher->full_name,
			'date' => $model->published_at,
			'condition' => $model->isPublished(),
		])

		@include("manage::widgets.grid-date" , [
			'text' => trans('manage::forms.general.deleted_at').': ',
			'text2' => trans('manage::forms.general.by').' '.$model->deleter->full_name,
			'date' => $model->deleted_at,
			'condition' => $model->trashed(),
			'color' => "danger",
		])

		{{--
		|--------------------------------------------------------------------------
		| Developer
		|--------------------------------------------------------------------------
		|
		--}}
			@include("manage::widgets.grid-tiny" , [
				'text' => "$model->id|$model->hashid" ,
				'color' => "gray" ,
				'icon' => "bug",
				'class' => "font-tahoma" ,
				'locale' => "en" ,
				'condition' => user()->isDeveloper() ,
			]     )
	</div>




	{{--
	|--------------------------------------------------------------------------
	| Buttons
	|--------------------------------------------------------------------------
	|
	--}}

	<div class="modal-footer">

		@if($model->canEdit())
			<a href="{{ $model->edit_link }}" class="btn btn-default w30">
				{{ trans('manage::forms.button.edit')   }}
			</a>
		@endif
		@if(!$model->isPublished() and $model->has('single_view'))
			<a href="{{ $model->preview_link }}" target="_blank" class="btn btn-default w30">
				{{ trans('posts::general.preview') }}
			</a>
		@endif
		@if($model->isPublished() and $model->has('single_view'))
			<a href="{{ $model->site_link }}" target="_blank" class="btn btn-default w30">
				{{ trans('posts::general.view_in_site') }}
			</a>
		@endif

	</div>
</div>


@include("manage::layouts.modal-end")