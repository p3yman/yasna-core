@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
	'refresh_url' => "manage/posts/update/$model->hashid",
])

@if(!isset($posttype))
	@php
		service('posts:browse_headings_handlers')->handle([
			'posttype' => $model->posttype ,
		])
	@endphp
@endif

@foreach( service('posts:browse_headings')->read() as $heading)
	<td>
		@include($heading['blade'])
	</td>
@endforeach


@include("posts::manage.browse-actions")