{{--
|--------------------------------------------------------------------------
| Copy Detector
|--------------------------------------------------------------------------
|
--}}

@if($model->isCopy())
    @php
        $model->title = '['.trans('posts::general.copy').'] '.$model->title;
    @endphp
@endif



{{--
|--------------------------------------------------------------------------
| Title
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-text" , [
	'text' => $model->title,
	'link' => "modal:manage/posts/act/$model->hashid/info" ,
	'size' => "14" ,
	'class' => "font-yekan" ,
])

{{--
|--------------------------------------------------------------------------
| Event (& Register) Duration
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-text" , [
	'text' => trans('manage::forms.button.duration' , [
		'date1' => echoDate($model->event_starts_at , 'j F Y'),
		'date2' => echoDate($model->event_ends_at , 'j F Y'),
	]),
	'condition' => $model->has('event'),
	'color' => "info",
	'size' => "10",
	'icon' => "clock-o",
])

@include("manage::widgets.grid-text" , [
	'text' => trans("posts::features.register") . SPACE . trans('manage::forms.button.duration' , [
		'date1' => echoDate($model->register_starts_at , 'j F Y'),
		'date2' => echoDate($model->register_ends_at , 'j F Y'),
	]),
	'condition' => $model->has('register'),
	'color' => "info",
	'size' => "10",
	'icon' => "clock-o",
])


{{--
|--------------------------------------------------------------------------
| Badges
|--------------------------------------------------------------------------
|
--}}

@include("posts::manage.badges")


{{--
|--------------------------------------------------------------------------
| Timestamps and Owner
|--------------------------------------------------------------------------
|
--}}

{{--@include("manage::widgets.grid-date" , [--}}
	{{--'text' => trans('manage::forms.general.created_at').': ',--}}
	{{--'text2' => trans('manage::forms.general.by').' '.$model->creator->full_name,--}}
	{{--'date' => $model->created_at,--}}
{{--])--}}

@include("manage::widgets.grid-tiny" , [
	'text' => trans('posts::general.post_owner').': '.$model->getPerson('owned_by')->full_name,
	'link' => "modal:manage/posts/act/-hashid-/owner" ,
	'icon' => "user-o" ,
]     )

{{--@include("manage::widgets.grid-date" , [--}}
	{{--'text' => trans('posts::general.publish').': ',--}}
	{{--'text2' => trans('manage::forms.general.by').' '.$model->publisher->full_name,--}}
	{{--'date' => $model->published_at,--}}
	{{--'condition' => $model->isPublished(),--}}
{{--])--}}

{{--@include("manage::widgets.grid-date" , [--}}
	{{--'text' => trans('manage::forms.general.deleted_at').': ',--}}
	{{--'text2' => trans('manage::forms.general.by').' '.$model->deleter->full_name,--}}
	{{--'date' => $model->deleted_at,--}}
	{{--'condition' => $model->trashed(),--}}
	{{--'color' => "danger",--}}
{{--])--}}


{{--
|--------------------------------------------------------------------------
| Developer
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-tiny" , [
	'text' => "$model->id|$model->hashid" ,
	'color' => "gray" ,
	'icon' => "bug",
	'class' => "font-tahoma" ,
	'locale' => "en" ,
	'condition' => user()->isDeveloper() ,
]     )


