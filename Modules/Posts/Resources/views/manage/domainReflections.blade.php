{!!
widget("modal")
	->label('tr:yasna::domains.reflections')
	->target('name:post-domain-reflections')
!!}

{!!
widget("hidden")
	->name('id')
	->value($model->hashid)
!!}

<div class="modal-body">
	{!!
	widget("input")
		->disabled()
		->inForm()
		->label("tr:validation.attributes.title")
		->value($model->title)
	!!}

	{!!
	widget("input")
		->disabled()
		->inForm()
		->label("tr:yasna::domains.main")
		->value($model->domain_name)
	!!}

	{!!
	widget("group")
		->start()
		->label('tr:yasna::domains.reflection_in')
	!!}

	@include("posts::manage.domainReflections-list")

	{!!
	widget("group")
		->stop()
	!!}

</div>

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		'separator' => false,
	]      )
	
</div>