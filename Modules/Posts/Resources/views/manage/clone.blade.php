@include("manage::layouts.modal-start" , [
	'form_url' => route('post-save-clone') ,
	'modal_title' => trans("posts::general.clone"),
])

<div class="modal-body">


	{{--
	|--------------------------------------------------------------------------
	| Readonly fields
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'hashid' ,
		'value' => $model->hashid,
	])

	@include('manage::forms.input' , [
		'name' => '',
		'label' => trans('validation.attributes.title'),
		'value' => $model->title ,
		'extra' => 'disabled' ,
	])

	{{--
	|--------------------------------------------------------------------------
	| New Title
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.input" , [
		'name' => "new_title",
		'class' => "form-required form-default atr" ,
	]     )


	{{--
	|--------------------------------------------------------------------------
	| Locale Fields
	|--------------------------------------------------------------------------
	| Applicable if and only if the posttype has locales feature!
	--}}
	@include("manage::forms.select" , [
		'condition' => $model->has('locales'),
		'name' => "locale",
		'options' => $model->posttype->localesCombo(),
		'value_field' => "0",
		'caption_field' => "1",
		'value' => $model->locale,
	])

	@include("manage::forms.toggle-form" , [
		'name' => "is_sister",
		'label' => trans('posts::general.clone_is_a_sister'),
		'data_on' => trans("manage::forms.logic.yes") ,
		'data_off' => trans("manage::forms.logic.no") ,
		'value' => "0",
		'condition' => $model->has('locales'),
	])


	{{--
	|--------------------------------------------------------------------------
	| Button
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::forms.buttons-for-modal" , [
		'save_label' => trans('posts::general.make_a_clone_and_get_me_there') ,
	])
</div>

@include("manage::layouts.modal-end")