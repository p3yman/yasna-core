@include("manage::layouts.modal-start" , [
	'form_url' => route('post-save-owner') ,
	'modal_title' => trans("posts::general.change_post_owner"),
])

<div class="modal-body">


	{{--
	|--------------------------------------------------------------------------
	| Readonly fields
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'hashid' ,
		'value' => $model->hashid,
	])

	@include('manage::forms.input' , [
		'name' => '',
		'label' => trans('validation.attributes.title'),
		'value' => $model->title ,
		'extra' => 'disabled' ,
	])


	{{--
	|--------------------------------------------------------------------------
	| Informative Fields
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.grid-text" , [
		'label' => trans('posts::general.post_creator') ,
		'text' => $model->creator->full_name,
		'icon' => "user-o" ,
		'link' => "urlN:".$model->creator->profile_link,
	]     )
	@include("manage::forms.grid-text" , [
		'fake' => $owner = $model->getPerson('owned_by') ,
		'label' => trans('posts::general.post_owner') ,
		'text' => $owner->full_name,
		'icon' => "user-o" ,
		'link' => "urlN:".$owner->profile_link,
	]     )

	{{--
	|--------------------------------------------------------------------------
	| Main Field
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.sep")

	@include("manage::forms.input" , [
		'name' => $field_name = config("auth.providers.users.field_name"),
		'label' => trans("posts::general.new_post_owner") ,
		'class' => "form-default form-required ltr" ,
		'hint' => trans("validation.attributes.$field_name") . SPACE . trans("posts::general.new_post_owner"),
	] )


	{{--
	|--------------------------------------------------------------------------
	| Button
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::forms.buttons-for-modal")
</div>

@include("manage::layouts.modal-end")