@php($current_domains = $model->domainReflectionIds())

@foreach($model->domainsCombo() as $domain )

	{!!
	widget("toggle")
		->name("domains[$domain->hashid]")
		->label($domain->title)
		->containerClass('mv10')
		->value( in_array($domain->id , $current_domains) )
		->condition($domain->id != $model->domain_id)
	!!}

@endforeach
