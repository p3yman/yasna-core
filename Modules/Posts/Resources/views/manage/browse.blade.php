@extends('manage::layouts.template')

@section('html_header')
	@foreach(service('posts:browse_head_blade')->read() as $item)
		@include($item['blade'])
	@endforeach
@append

@section('content')
	@include("posts::manage.tabs")

	{{--
	|--------------------------------------------------------------------------
	| Toolbar
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::widgets.toolbar" , [
		'mass_actions' => service('posts:mass_actions')->indexed('icon' , 'caption' , 'link') ,
		'buttons' => [
			[
				'type' => "default" ,
				'caption' => trans('posts::filter.filters') ,
				'icon' => "filter" ,
				'link'=> "$('#divPostFilters').slideToggle('fast')",
			],
			[
				'target' => url("manage/posts/$posttype->slug/create/$locale"),
				'type' => "success",
				'caption' => trans('manage::forms.button.add_to').' '.$posttype->title ,
				'icon' => "plus-circle",
				'condition' => $posttype->can('create') ,
			],
			[
				'target' => "modal:manage/posts/downstream/posttype/setting/$posttype->hashid/" ,
				'type' => "primary" ,
				'caption' => trans('manage::settings.config') ,
				'icon' => "gear" ,
				'condition' => $posttype->can('setting') and $posttype->setting_inputs->count() ,
			],

		],
		'search' => [
			'target' => url("manage/posts/$posttype->slug/$locale/search"),
			'label' => trans('manage::forms.button.search'),
			'value' => isset($keyword)? $keyword : ''
		],
	])


	@include("posts::manage.list.filter")

	{{--
	|--------------------------------------------------------------------------
	| Grid
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::widgets.grid" , [
		'table_id' => "tblPosts",
		'row_view' => "posts::manage.browse-row",
		'handle' => "selector",
		'operation_heading' => true ,
		'headings' => service('posts:browse_headings')->indexed('caption' , 'width' , 'condition' , 'blade') ,
	])


	<script>
		@if($has_filter == true)
			$('#divPostFilters').show();
		@endif
	</script>

@endsection

