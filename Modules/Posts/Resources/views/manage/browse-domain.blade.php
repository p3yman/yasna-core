@include("manage::widgets.grid-text" , [
	'text' => $model->domain_name,
	'size' => "12" ,
])

@include("manage::widgets.grid-text" , [
	"fake" => $count = $model->domainReflectionCount() - 1,
	"condition" => $model->domain_id > 0 and ($count or $model->canReflect()) ,
	'link' => "modal:manage/posts/act/$model->hashid/domainReflections" ,
	"size" => "11" ,
	"color" => "darkgray" ,
	"text" => $count? trans_safe("yasna::domains.n_reflections" , [
		"n" => $count
	]) : trans_safe("yasna::domains.no_reflection") ,
])
