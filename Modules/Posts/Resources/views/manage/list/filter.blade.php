@php
	service("posts:browse_filters_handlers")->handle();
@endphp

<div id="divPostFilters" class="panel noDisplay">
	<div class="tforms">
		<form action="{{url('manage/posts/filter')}}" method="get" id="report-form" class="form-horizontal">

			{{ widget('hidden')->name('type')->value($posttype->slug) }}
			{{ widget('hidden')->name('current_tab')->value($page[1][0]) }}

			<div class="panel-body">
				<div class="row">
					@foreach(module('posts')->service('browse_filters')->read() as $service)
						@include($service['blade'])
					@endforeach
				</div>
			</div>
			<div class="panel-footer pv-lg">
				{!! widget('button')->id('submit')->type('submit')->label(trans('posts::filter.submit_filters'))->class('btn-primary')!!}
				{!! widget('button')->id('reset')->label(trans('posts::filter.remove_filters'))->class('btn-warning')->onClick('resetWareFilterForm()')!!}
			</div>
		</form>
	</div>
</div>

