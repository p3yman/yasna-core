<div class="col-md-2">
	{!!
		   widget('combo')
			 ->name('sort')
			 ->options([
					[
					 	"caption" => trans("posts::filter.desc"),
					 	"value" =>" desc",
					],
					[
						"caption" => trans("posts::filter.asc"),
						"value" =>" asc",
					],

			])
			 ->label('tr:posts::filter.sort')
			 ->valueField('value')
			 ->captionField('caption')
	!!}
</div>
