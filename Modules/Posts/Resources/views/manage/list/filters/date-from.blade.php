<div class="col-md-2">
	{!!
		   widget('persian_date_picker')
			 ->name('date_from')
			 ->searchable()
			 ->value(
			 	request()->get('date_from')
			 		? carbon()::createFromTimestamp(request()->get('date_from') / 1000)->toDateTimeString()
			 		: null
			)
			 ->label('tr:posts::filter.date_from')
	!!}
</div>
