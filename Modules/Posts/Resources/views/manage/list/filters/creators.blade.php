<div class="col-md-2">
	{!!
		   widget('combo')
			 ->name('creator')
			 ->options(post()::getAllAuthors())
			 ->valueField('hashid')
			 ->captionField('full_name')
			 ->searchable()
			 ->blankValue('')
			 ->value(request()->get('creator'))
			 ->blankCaption(trans('posts::filter.blank_item'))
			 ->label('tr:posts::filter.creators')
	!!}
</div>
