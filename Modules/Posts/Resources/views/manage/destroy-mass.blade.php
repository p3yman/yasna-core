@include("manage::layouts.modal-mass" , [
	'form_url' => route('post-save-destroy-mass') ,
	'modal_title' => trans("manage::forms.button.hard_delete") ,
	'save_label' => trans("manage::forms.button.sure_hard_delete") ,
	'note_text' => trans('manage::forms.feed.hard_delete_notice') ,
	'note_shape' => 'danger' ,
	'save_shape' => "danger" ,
]   )
