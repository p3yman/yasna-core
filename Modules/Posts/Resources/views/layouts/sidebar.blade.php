@if(get_setting('fold_posts_on_manage_sidebar'))
	@include("posts::layouts.sidebar_folded", [
	   "groups" => PostsModule::sidebarPosttypesGrouped(),
	])
@else
	@include("posts::layouts.sidebar_unfolded", [
	   "posttypes" => PostsModule::sidebarPosttypes(),
	])
@endif
