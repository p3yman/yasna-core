@php($posttypes = posttype()->whereHas('features',function($q){ return $q->where('slug','slideshow'); })->get())

@foreach($posttypes as $posttype)
	@if($posttype->can('edit'))
		@php($subs[]=[
				'link'   => "slideshows/$posttype->hashid",
				'caption'=> $posttype->title,
				'icon'   => 'image'
		])
	@endif
@endforeach

@if(!empty($subs))
	@include('manage::layouts.sidebar-link' , [
		 'condition' => count($posttypes) ,
		 'icon'       => "image",
		 'caption' => trans('posts::seeder.features.slideshow') ,
		 'link' => "posts-group" , //<~~ Doesn't really do anything!
		 'sub_menus' => $subs,
	]   )
@endif