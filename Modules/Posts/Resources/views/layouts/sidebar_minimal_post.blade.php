@php
	$sub_menus = [];
@endphp

@foreach(PostsModule::sidebarMinimalPosttypes() as $posttype)
	@if($posttype->can())
		@php
			$sub_menus[] = [
				'link'    => "postaks/{$posttype->slug}",
				'caption' => $posttype->title,
				'icon'    => $posttype->icon,
			];
		@endphp
	@endif
@endforeach

@include('manage::layouts.sidebar-link', [
	'condition'  => count($sub_menus),
	'icon'       => "file-o",
	'caption'    => trans_choice("posts::postak.titles", count($sub_menus)),
	'link'       => "postsak", //<~~ Doesn't really do anything!
	'sub_menus'  => $sub_menus,
])
