@foreach($posttypes as $posttype)

	@include('manage::layouts.sidebar-link' , [
		'icon'       => $posttype->icon,
		'caption'    => $posttype->title,
		'link'       => "posts/" . $posttype->slug,
		'condition'  => $posttype->can() ,
		'permission' => "posts-" . $posttype->slug,
	])

@endforeach
