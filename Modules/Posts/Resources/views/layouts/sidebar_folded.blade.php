@foreach($groups as $group)
    @php
        $sub_menus = [];
    @endphp

    @foreach($group as $posttype)
        @if($posttype->can())
            @php
                $sub_menus[] = [
                     'link' => "posts/$posttype->slug" ,
                     'caption' => $posttype->title ,
                     'icon' => $posttype->icon ,
                ] ;
            @endphp
        @endif
    @endforeach

    @include('manage::layouts.sidebar-link' , [
         'condition' => count($sub_menus) ,
         'icon'       => "paragraph",
         'caption' => $group->first()->header_title ? $group->first()->header_title : trans("posts::types.header_title_placeholder") ,
         'link' => "posts-group" , //<~~ Doesn't really do anything!
         'sub_menus' => $sub_menus ,
    ]   )
@endforeach
