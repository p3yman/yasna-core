@include("manage::layouts.modal-start" , [
	'form_url' => url('manage/posts/upstream/save/feature'),
	'modal_title' => $model->id? trans('manage::forms.button.edit') : trans('manage::forms.button.add'),
])

<div class="modal-body">

	{{--
	|--------------------------------------------------------------------------
	| Normal Fields
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'id' ,
		'value' => $model->spreadMeta()->id,
	])

	@include("manage::forms.note" , [
		'text' => trans("posts::features.creation_warning"),
		'condition' => !$model->id ,
		'shape' => "warning" ,
	]     )

	@include('manage::forms.input' , [
		'id' => "txtSlug" ,
		'name' =>	'slug',
		'class' =>	'form-required ltr form-default',
		'value' =>	$model ,
		'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.english-only'),
		'on_change' => "inlineSlugChanged()" ,
	])

	@include("manage::forms.note" , [
		'class' => "noDisplay" ,
		'text' => trans("posts::features.slug_edition_warning"),
		'condition' => $model->id ,
		'shape' => "warning" ,
		'id' => "divSlugEditionWarning" ,
	]     )

	@include('manage::forms.input' , [
		'name' =>	'title',
		'value' =>	$model->title,
		'class' => 'form-required' ,
		'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.persian-only'),
	])

	@include('manage::forms.input' , [
		'name' => "order",
		'class' => "form-required",
		'value' => $model->exists? $model->order : 22,
	])

	{!!
	widget("iconPicker")
		->name('icon')
		->required()
		->value("fa-".$model->icon)
		->hint("tr:validation.hint.icon_hint")
		->inForm()
	!!}

	@include("manage::widgets.input-textarea" , [
		'name' => "hint",
		'value' => $model->hint,
		'class' =>	'form-autoSize',
		'rows' => "3",
	])

	{{--
	|--------------------------------------------------------------------------
	| Fields Attachments
	|--------------------------------------------------------------------------
	|
	--}}

	@include("posts::upstream.inputs-selector" , [
		'separator' => true,
		'value' => $model->inputs ,
	]     )

</div>
<div class="modal-footer">

{{--
	|--------------------------------------------------------------------------
	| Buttons
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.buttons-for-modal")


</div>

@include("manage::layouts.modal-end")

<script>
	function inlineSlugChanged() {
		let $input = $('#txtSlug');
		let $warning = $('#divSlugEditionWarning');

		if ($input.val() == '{{$model->slug}}') {
			$warning.slideUp('fast');
		}
		else {
			$warning.slideDown('fast');
		}
	}
</script>