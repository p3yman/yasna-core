@include("manage::layouts.modal-start" , [
	'form_url' => url('manage/posts/upstream/save/input-default'),
	'modal_title' => trans("posts::inputs.default_value_setting"),
])

<div class="modal-body">

	{{--
	|--------------------------------------------------------------------------
	| Disabled and Hidden Fields
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'slug' ,
		'value' => $model->slug,
	])

	@include('manage::forms.input' , [
		'name' =>	'title',
		'value' =>	$model,
		'disabled' => true ,
	])

	@include('manage::forms.input' , [
		'name' =>	'validation_rules',
		'class' =>	'ltr ',
		'hint' => "Laravel Validation Rules" ,
		'value' =>	$model->validation_rules ,
		'disabled' => true ,
])
	@include('manage::forms.input' , [
		'name' =>	'purifier_rules',
		'class' =>	'ltr ',
		'hint' => "Taha Purification Rules" ,
		'value' =>	$model->purifier_rules ,
		'disabled' => true ,
	])

	@include("manage::forms.sep")

	{{--
	|--------------------------------------------------------------------------
	| Main Part
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::widgets.input-$model->data_type" , [
		'value' => $model->default_value ,
		'name' => "default_value",
		'hint' => $model->hint ,
		'class' => $model->css_class ,
	])


	{{--
	|--------------------------------------------------------------------------
	| Buttons
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::forms.buttons-for-modal")


</div>

@include("manage::layouts.modal-end")