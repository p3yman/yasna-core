@extends('manage::layouts.template')

@section('content')
	@include("manage::upstream.tabs")

	@include("manage::widgets.toolbar" , [
	'buttons' => [
		[
			'target' => "modal:manage/posts/upstream/input/create/0",
			'type' => "success",
			'caption' => trans('manage::forms.button.add'),
			'icon' => "plus-circle",
		],
	],
	'search' => [
		'target' => url('manage/upstream/inputs/') ,
		'label' => trans('manage::forms.button.search') ,
		'value' => isset($keyword)? $keyword : '' ,
	],
])

	@include("manage::widgets.grid" , [
		'table_id' => "tblInputs",
		'row_view' => "posts::upstream.inputs-row",
//		'handle' => "counter",
		'headings' => [
			['#',60],
			trans('validation.attributes.title'),
			trans("validation.attributes.type"),
			[trans('validation.attributes.usage'),300],
			trans("validation.attributes.status"),
			trans('manage::forms.button.action'),
		],
	])

@endsection