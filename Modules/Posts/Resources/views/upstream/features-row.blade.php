@include('manage::widgets.grid-rowHeader', [
	'refresh_url' => "manage/posts/upstream/feature/row/$model->hashid"
])

{{--
|--------------------------------------------------------------------------
| Icon
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->order,
		'size' => "12" ,
		'div_class' => "mv10" ,
		'class' => "font-yekan text-bold" ,
	])
</td>
<td>
	<i class="fa fa-{{$model->icon}} f30 m5 text-info"></i>
</td>

{{--
|--------------------------------------------------------------------------
| Title and Slug
|--------------------------------------------------------------------------
|
--}}

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'size' => "16" ,
		'class' => "font-yekan text-bold" ,
		'link' =>  "modal:manage/posts/upstream/feature/edit/-hashid-" ,
	])
	@include("manage::widgets.grid-tiny" , [
		'text' => "$model->id|$model->slug" ,
		'color' => "gray" ,
		'icon' => "bug",
		'class' => "font-tahoma" ,
		'locale' => "en" ,
	]     )

</td>

{{--
|--------------------------------------------------------------------------
| Status
|--------------------------------------------------------------------------
|
--}}
<td>
	<div class="mv10">
		@include("manage::widgets.grid-badge" , [
			'condition' => $model->trashed() ,
			'icon' => "times",
			'text' => trans("manage::forms.status_text.deleted"),
			'link' => "modal:manage/posts/upstream/feature/activeness/-hashid-" ,
			'color' => 'danger',
		])
	</div>
</td>
<td>

	@include("manage::widgets.grid-text" , [
		'text' => $model->posttypes()->count() . ' ' .trans("posts::types.full_singular"),
		'icon' => "code-fork" ,
		'div_class' => "mv5" ,
	]     )

	@include("manage::widgets.grid-text" , [
		'text' => $model->inputs()->count(). ' ' . trans("posts::inputs.singular"),
		'icon' => "server" ,
	]     )

</td>

{{--
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['pencil' , trans('manage::forms.button.edit') , "modal:manage/posts/upstream/feature/edit/-hashid-" ],
		['wheelchair-alt' , trans('manage::forms.button.seeder_cheat') , "modal:manage/posts/upstream/feature/seeder/-hashid-"],
		['trash-o' , trans('manage::forms.button.soft_delete') , "modal:manage/posts/upstream/feature/activeness/-hashid-" , !$model->trashed()] ,
		['recycle' , trans('manage::forms.button.undelete') , "modal:manage/posts/upstream/feature/activeness/-hashid-" , $model->trashed()],
	]
])
