{{--To be used, inside the modal editors of features-edit, posttypes-edit etc.--}}
@php
	$text_value = '';
@endphp


{{--
|--------------------------------------------------------------------------
| Separator
|--------------------------------------------------------------------------
|
--}}
@if(isset($separator) and $separator)
	@include("manage::forms.sep" , [
		'label' => isset($label)? $label : trans("posts::inputs.additional") ,
		'class' => "font-yekan text-bold" ,
	])
@endif



{{--
|--------------------------------------------------------------------------
| Notice
|--------------------------------------------------------------------------
|
--}}
@if(isset($notice) and $notice)
	@include("manage::forms.note" , [
		'text' => $notice ,
		'condition' => isset($condition)? $condition : true ,
		'shape' => isset($notice_shape)? $notice_shape : 'info' ,
	]     )
@endif

<div id="divInputSelector-container" class="w100 mv5">



	{{--
	|--------------------------------------------------------------------------
	| Browse Current Inputs
	|--------------------------------------------------------------------------
	|
	--}}
	<div id="divInputSelector-list" data-src="manage/posts/upstream/input/selector-one/" data-type="append">

		@foreach($value as $input)
			@php
				$text_value .= ','. $input->id . ',';
			@endphp

			@include("posts::upstream.inputs-selector-one")

		@endforeach
	</div>


	{{--
	|--------------------------------------------------------------------------
	| New Input
	|--------------------------------------------------------------------------
	|
	--}}
	<div class="w100 p10 panel panel-yellow panel-footer row">
		<div class="col-sm-10">
			@include('manage::forms.select_self' , [
				'name' =>	'_new_input',
				'id' => "cmbInputSelector" ,
				'options' => PostsModule::allInputs() ,
				'caption_field' => "full_name" ,
				'value' => '' ,
				'blank_value' => '0' ,
				'blank_label' => trans("posts::inputs.selector_placeholder") ,
				'search' => true ,
			])
		</div>
		<div class="col-sm-2">
			@include("manage::forms.button" , [
				'class' => "w100",
				'label' => trans("manage::forms.button.add") ,
				'link' => "inputSelectorAdd()" ,
			]     )
		</div>
	</div>


	{{--
	|--------------------------------------------------------------------------
	| Hidden Field
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.hidden" , [
		'id' => "txtInputSelector",
		'name' => isset($name)? $name : '_inputs' ,
		'value' => $text_value ,
		'class' => "ltr" ,
	]     )

</div>

<script>
	function inputSelectorAdd() {
		let $input = $('#txtInputSelector');
		let $combo = $('#cmbInputSelector');
		let new_id = $combo.val();
		let additive = "," + new_id + ",";

		if ($input.val().search(additive) < 0) {
			$input.val($input.val() + additive);
			divReload("divInputSelector-list", new_id);
		}

		$combo.selectpicker('val', '0');
	}

	function inputSelectorRemove(input_id) {
		let $div = $("#divInputSelector-" + input_id);
		let $input = $('#txtInputSelector');
		let removal = "," + input_id + ",";

		$input.val($input.val().replaceAll(removal, ''));
		$div.slideUp('fast').remove();

	}
</script>
