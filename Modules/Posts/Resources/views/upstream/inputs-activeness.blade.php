@include("manage::layouts.modal-start" , [
	'form_url' => url('manage/posts/upstream/save/input-activeness'),
	'modal_title' => trans('manage::forms.button.change_status'),
	'no_validation' => true ,
])
<div class='modal-body'>

	@include('manage::forms.hidden' , [
		'name' => 'id' ,
		'value' => $model->spreadMeta()->id,
	])

	@include("manage::forms.input" , [
		'name' => "",
		'value' => $model->title ,
		'label' => trans('validation.attributes.title') ,
		'disabled' => true ,
	]     )
	
	@include("manage::forms.buttons-for-modal" , [
		'save_label' => $model->trashed()? trans('manage::forms.button.undelete') : trans('manage::forms.button.soft_delete'),
		'save_shape' => $model->trashed() ? 'success' : 'danger' ,
		'save_value' => $model->trashed()? 'restore' : 'delete'  ,
	]     )

	@include('manage::forms.closer')

</div>
@include("manage::layouts.modal-end")