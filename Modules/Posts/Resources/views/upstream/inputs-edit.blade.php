@include("manage::layouts.modal-start" , [
	'form_url' => url('manage/posts/upstream/save/input'),
	'modal_title' => $model->id? trans('manage::forms.button.edit') : trans('manage::forms.button.add'),
])

<div class="modal-body">

	{{--
	|--------------------------------------------------------------------------
	| Normal Fields
	|--------------------------------------------------------------------------
	|
	--}}

	@include('manage::forms.hidden' , [
		'name' => 'id' ,
		'value' => $model->spreadMeta()->id,
	])

	@include('manage::forms.input' , [
		'id' => "txtSlug" ,
		'name' =>	'slug',
		'class' =>	'form-required ltr form-default',
		'value' =>	$model ,
		'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.english-only'),
		'on_change' => "inlineSlugChanged()" ,
	])

	@include("manage::forms.note" , [
		'class' => "noDisplay" ,
		'text' => $model->id ? trans("posts::inputs.slug_edition_warning") : trans("posts::inputs.creation_warning"),
//		'condition' => $model->id ,
		'shape' => "warning" ,
		'id' => "divSlugEditionWarning" ,
	]     )

	@include('manage::forms.input' , [
		'name' =>	'title',
		'value' =>	$model,
		'class' => 'form-required' ,
		'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.persian-only'),
	])

	@include('manage::forms.input' , [
		'name' => "order",
		'class' => "form-required",
		'value' => $model->exists? $model->order : 22,
	])

	@include('manage::forms.select' , [
		'name' => 'type' ,
		'class' => 'form-required',
		'options' => $model::inputTypes() ,
		'caption_field' => '1' ,
		'value_field' => '0' ,
		'value' => $model->type ,
	])

	@include('manage::forms.select' , [
		'name' => 'data_type' ,
		'id' => "cmbDataType" ,
		'class' => 'form-required',
		'options' => $model::dataTypes() ,
		'caption_field' => '1' ,
		'value_field' => '0' ,
		'on_change' => "inlineTypeChanged()" ,
		'value' => $model->data_type ,
	])

	@include('manage::forms.textarea' , [
		'name' => "options",
		'id' => "txtOptions" ,
		'value' => $model->options ,
		'hint' => trans("posts::inputs.options_hint") ,
		'class' => "form-autoSize ltr" ,
		'div_class' => $model->data_type == 'combo' ? '' : 'noDisplay' ,
	]      )
	

	@include('manage::forms.input' , [
		'name' =>	'hint',
		'value' =>	$model,
		'class' => '' ,
	])


	@include('manage::forms.input' , [
		'name' =>	'css_class',
		'class' =>	'ltr ',
		'value' =>	$model->css_class ,
	])
	@include('manage::forms.input' , [
		'name' =>	'validation_rules',
		'class' =>	'ltr ',
		'hint' => "Laravel Validation Rules" ,
		'value' =>	$model->validation_rules ,
	])
	@include('manage::forms.input' , [
		'name' =>	'purifier_rules',
		'class' =>	'ltr ',
		'hint' => "Taha Purification Rules" ,
		'value' =>	$model->purifier_rules ,
	])
</div>
<div class="modal-footer">

	{{--
	|--------------------------------------------------------------------------
	| Buttons
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.buttons-for-modal")


</div>

@include("manage::layouts.modal-end")

<script>
	function inlineSlugChanged() {
		let $input = $('#txtSlug');
		let $warning = $('#divSlugEditionWarning');

		if ($input.val() == '{{$model->slug}}') {
			$warning.slideUp('fast');
		}
		else {
			$warning.slideDown('fast');
		}
	}

	function inlineTypeChanged() {
		let $combo = $('#cmbDataType') ;
		let $input = $('#txtOptions') ;

		if($combo.val() == 'combo') {
			$input.parent().parent().slideDown('fast') ;
			$input.focus() ;
		}
		else {
			$input.parent().parent().slideUp('fast') ;
		}
	}
</script>