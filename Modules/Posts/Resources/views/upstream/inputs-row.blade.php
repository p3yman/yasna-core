@include('manage::widgets.grid-rowHeader', [
	'refresh_url' => "manage/posts/upstream/input/row/$model->hashid"
])

{{--
|--------------------------------------------------------------------------
| Icon
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->order,
		'size' => "12" ,
		'div_class' => "mv10" ,
		'class' => $model->trashed()? "font-yekan deleted-content" : "font-yekan text-bold" ,
	])
</td>

{{--
|--------------------------------------------------------------------------
| Title and Slug
|--------------------------------------------------------------------------
|
--}}

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'size' => "16" ,
		'class' => "font-yekan text-bold" ,
		'link' =>  "modal:manage/posts/upstream/input/edit/-hashid-" ,
	])
	@include("manage::widgets.grid-tiny" , [
		'text' => "$model->id|$model->slug" ,
		'color' => "gray" ,
		'icon' => "bug",
		'class' => "font-tahoma" ,
		'locale' => "en" ,
	]     )

</td>

{{--
|--------------------------------------------------------------------------
| Type
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->data_type_title,
	]     )
	@include("manage::widgets.grid-text" , [
		'text' => $model->type_title,
	]     )
</td>

{{--
|--------------------------------------------------------------------------
| Usages
|--------------------------------------------------------------------------
|
--}}

<td>

	@include("manage::widgets.grid-text" , [
		'text' => $model->features()->count(). ' ' . trans("posts::features.singular"),
	]     )

	@include("manage::widgets.grid-text" , [
		'text' => $model->posttypes()->count() . ' ' .trans("posts::types.full_singular"),
		'div_class' => "mv5" ,
	]     )

</td>


{{--
|--------------------------------------------------------------------------
| Status
|--------------------------------------------------------------------------
|
--}}
<td>
	<div class="mv10">
		@include("manage::widgets.grid-badge" , [
			'condition' => $model->trashed() ,
			'icon' => "times",
			'text' => trans("manage::forms.status_text.deleted"),
			'link' => "modal:manage/posts/upstream/input/activeness/-hashid-" ,
			'color' => 'danger',
		])

		@include("manage::widgets.grid-badge" , [
			'condition' => !$model->trashed() and !$model->default_value_set ,
			'icon' => "flag" ,
			'text' => trans("posts::inputs.without_default_value") ,
			'link' => "modal:manage/posts/upstream/input/default/-hashid-" ,
			'color' => "warning" ,
		]    )

		@include("manage::widgets.grid-badge" , [
			'condition' => !$model->trashed() and $model->default_value_set ,
			'icon' => "flag" ,
			'text' => trans("posts::inputs.default_value_setting") ,
			'link' => "modal:manage/posts/upstream/input/default/-hashid-" ,
			'color' => "default" ,
		]    )

	</div>
</td>


{{--
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['pencil' , trans('manage::forms.button.edit') , "modal:manage/posts/upstream/input/edit/-hashid-" ],
		['flag' , trans("posts::inputs.default_value_setting") , "modal:manage/posts/upstream/input/default/-hashid-" , !$model->trashed()] ,
		['wheelchair-alt' , trans('manage::forms.button.seeder_cheat') , "modal:manage/posts/upstream/input/seeder/-hashid-"],
		['trash-o' , trans('manage::forms.button.soft_delete') , "modal:manage/posts/upstream/input/activeness/-hashid-" , !$model->trashed()] ,
		['recycle' , trans('manage::forms.button.undelete') , "modal:manage/posts/upstream/input/activeness/-hashid-" , $model->trashed()],
	]
])
