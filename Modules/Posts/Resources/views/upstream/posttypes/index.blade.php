@extends('manage::layouts.template')

@section('content')
	@include("manage::upstream.tabs")

	@include("manage::widgets.toolbar" , [
	'buttons' => [
		[
			'target' => "modal:manage/posts/upstream/posttype/create/0",
			'type' => "success",
			'caption' => trans('manage::forms.button.add'),
			'icon' => "plus-circle",
		],
	],
	'search' => [
		'target' => url('manage/upstream/posttypes/') ,
		'label' => trans('manage::forms.button.search') ,
		'value' => isset($keyword)? $keyword : '' ,
	],
])


	@include("manage::widgets.grid" , [
		'table_id' => "tblPosttypes",
		'row_view' => "posts::upstream.posttypes.row",
		'handle' => "counter",
		'headings' => [
			[trans('validation.attributes.title'),60],
			'',
			['',40],
			['',40],
			['',40],
			'',
			[trans('validation.attributes.status'),300],
			trans('manage::forms.button.action'),
		],
	])

@endsection