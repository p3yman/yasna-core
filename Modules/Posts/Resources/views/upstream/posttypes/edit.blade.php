@include("manage::layouts.modal-start" , [
	'form_url' => url('manage/posts/upstream/save/posttype'),
	'modal_title' => $model->id? trans('manage::forms.button.edit') : trans('manage::forms.button.add'),
])

<div class="modal-body">
    @include('manage::forms.hidden' , [
         'name' => 'id' ,
         'value' => $model->spreadMeta()->id,
    ])

    @include('manage::forms.input' , [
         'id' => "txtSlug" ,
         'name' =>	'slug',
         'class' =>	'form-required ltr form-default',
         'value' =>	$model ,
         'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.english-only'),
    ])

    @include('manage::forms.input' , [
         'name' =>	'title',
         'value' =>	$model,
         'class' => 'form-required' ,
         'hint' =>	trans('validation.hint.unique').' | '.trans('validation.hint.persian-only'),
    ])

    @include('manage::forms.input' , [
         'name' =>	'singular_title',
         'value' =>	$model,
         'class' => 'form-required' ,
         'hint' =>	trans('validation.hint.persian-only'),
    ])

    @include('manage::forms.input' , [
         'name' =>	'icon',
         'class' =>	'ltr form-required',
         'value' =>	$model->icon ,
         'hint' =>	trans('validation.hint.icon_hint'),
    ])

    @include("manage::forms.select" , [
         'name' => "template",
         'class' => "form-required",
         'options' => $model->templatesCombo(),
         'caption_field' => "1",
         'value_field' => "0",
         'value' => $model->template,
    ])


    @include('manage::forms.input' , [
         'name' => "order",
         'class' => "",
         'value' => $model,
    ])

    @include('manage::forms.input' , [
         'name' =>	'header_title',
         'value' =>	$model->header_title,
         'hint' =>	trans('validation.hint.persian-only'),
    ])

    @include('manage::forms.input' , [
         'name' => "locales",
         'value' => $model->locales,
         'hint' => trans('posts::types.locales_hint'),
         'class' => "ltr",
    ])

    {{--
    |--------------------------------------------------------------------------
    | Features
    |--------------------------------------------------------------------------
    |
    --}}
    {!!
    widget("selectize")
         ->name('_features')
         ->inForm()
         ->value( $model->featuresSelectizeValue() )
         ->options($model->featuresSelectizeList())
         ->valueField('id')
         ->captionField('title')
         ->label("tr:posts::features.plural")
    !!}

    {{--
    |--------------------------------------------------------------------------
    | Inputs
    |--------------------------------------------------------------------------
    |
    --}}
    {!!
    widget("selectize")
         ->name('_inputs')
         ->inForm()
         ->value( $model->inputsSelectizeValue() )
         ->options($model->inputsSelectizeList())
         ->valueField('id')
         ->captionField('title')
         ->label("tr:posts::inputs.plural")
         ->help('tr:posts::types.input_selector_notice')
    !!}


</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal" )
</div>

@include("manage::layouts.modal-end")