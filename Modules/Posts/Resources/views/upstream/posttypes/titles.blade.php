@include("manage::layouts.modal-start" , [
	'form_url' => url('manage/posts/upstream/save/posttype-titles'),
	'modal_title' => trans('manage::settings.title_in_other_locales'),
	'no_validation' => true ,
])

@php
	$default_locale = $model->defaultLocale();
@endphp
<div class='modal-body'>

	@include('manage::forms.hidden' , [
		'name' => 'id' ,
		'value' => $model->spreadMeta()->id,
	])

	@include("manage::forms.input" , [
		'name' => "",
		'value' => $model->slug ,
		'label' => trans('validation.attributes.slug') ,
		'disabled' => true ,
	]     )

	@foreach($model->locales_array as $locale)
		@include("manage::forms.sep" , [
			'label' => trans("manage::forms.lang.$locale"),
			'class' => "text-danger font-yekan f16" ,
		]     )

		@include('manage::forms.input' , [
			'name' =>	($locale == $default_locale) ? 'title' : "_title_in_$locale",
			'label' => trans('validation.attributes.title') ,
			'value' =>	$model->titleIn($locale),
			'class' =>  in_array($locale , ['fa' , 'ar']) ? 'form-required' : 'form-required ltr' ,
			'hint' =>	($locale == $default_locale) ? trans('validation.hint.unique').' | '.trans('validation.hint.persian-only') : '',
		])
		@include('manage::forms.input' , [
			'name' =>	($locale == $default_locale) ? 'singular_title' : "_singular_title_in_$locale",
			'label' => trans('validation.attributes.singular_title') ,
			'value' =>	$model->singularTitleIn($locale),
			'class' =>  in_array($locale , ['fa' , 'ar']) ? 'form-required' : 'form-required ltr' ,
			'hint' =>	($locale == $default_locale) ? trans('validation.hint.persian-only') : '',
		])
	@endforeach

	@include("manage::forms.buttons-for-modal")
	{{--@include('manage::forms.closer')--}}

</div>
@include("manage::layouts.modal-end")
