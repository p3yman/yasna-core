@include('manage::widgets.grid-rowHeader', [
	'handle' => "counter" ,
	'refresh_url' => "manage/posts/upstream/posttype/row/$model->hashid"
])

{{--
|--------------------------------------------------------------------------
| Icon
|--------------------------------------------------------------------------
|
--}}
<td>
	<i class="fa fa-{{$model->icon}} f30 m5 text-info"></i>
</td>

{{--
|--------------------------------------------------------------------------
| Title and Slug
|--------------------------------------------------------------------------
|
--}}

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'size' => "16" ,
		'class' => "font-yekan text-bold" ,
		'link' =>  "modal:manage/posts/upstream/posttype/edit/-hashid-" ,
	])
	@include("manage::widgets.grid-tiny" , [
		'text' => "$model->id|$model->slug" ,
		'color' => "gray" ,
		'icon' => "bug",
		'class' => "font-tahoma" ,
		'locale' => "en" ,
	]     )

</td>

{{--
|--------------------------------------------------------------------------
| Edit Buttons
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => trans('manage::forms.button.edit'),
		'class' => "btn btn-default" ,
		'link' =>  "modal:manage/posts/upstream/posttype/edit/-hashid-" ,
	])
</td>


{{--
|--------------------------------------------------------------------------
| Config Buttons
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => trans('manage::settings.config'),
		'class' => "btn btn-default" ,
		'link' =>  "modal:manage/posts/downstream/posttype/setting/-hashid-" ,
	])
</td>

{{--
|--------------------------------------------------------------------------
| Titles Buttons
|--------------------------------------------------------------------------
|
--}}
<td>
	@include("manage::widgets.grid-text" , [
		'text' => trans('manage::settings.title_in_other_locales'),
		'class' => "btn btn-default" ,
		'link' =>  "modal:manage/posts/upstream/posttype/titles/-hashid-" ,
//		'condition' => $model->has('locales'),
	])
</td>



{{--
|--------------------------------------------------------------------------
| Status
|--------------------------------------------------------------------------
|
--}}
<td>
	<div class="mv10">
		@include("manage::widgets.grid-badge" , [
			'condition' => $model->trashed() ,
			'icon' => "times",
			'text' => trans("manage::forms.status_text.deleted"),
			'link' => "modal:manage/posts/upstream/posttype/activeness/-hashid-" ,
			'color' => 'danger',
		])
	</div>
</td>


<td>

	@include("manage::widgets.grid-text" , [
		'text' => $model->features()->count() . ' ' .trans("posts::features.singular"),
		'icon' => "code-fork" ,
		'div_class' => "mv5" ,
	]     )

	@include("manage::widgets.grid-text" , [
		'text' => $model->inputs()->count(). ' ' . trans("posts::inputs.singular"),
		'icon' => "server" ,
	]     )

</td>

{{--
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		['pencil' , trans('manage::forms.button.edit') , "modal:manage/posts/upstream/posttype/edit/-hashid-" ],
		['sliders' , trans('manage::settings.config') , "modal:manage/posts/downstream/posttype/setting/-hashid-/"],
		['taxi' , trans('manage::settings.title_in_other_locales') , "modal:manage/posts/upstream/posttype/titles/-hashid-" , $model->has('locales')],
		['wheelchair-alt' , trans('manage::forms.button.seeder_cheat') , "modal:manage/posts/upstream/posttype/seeder/-hashid-"],
		['trash-o' , trans('manage::forms.button.soft_delete') , "modal:manage/posts/upstream/posttype/activeness/-hashid-" , !$model->trashed() and $model->slug != 'root'] ,
		['recycle' , trans('manage::forms.button.undelete') , "modal:manage/posts/upstream/posttype/activeness/-hashid-" , $model->trashed()],
	]
])
