@if(!isset($input))
	@php
		$input = $model;
	@endphp
@endif
<div id="divInputSelector-{{$input->id}}" class="w100 panel panel-default p10"
	 onmousemove="$(this).children().children('span.-delete').show()" onmouseout="$(this).children().children('.-delete').hide()"
>

	<div class="w100">
		{{ $input->full_name }}
	</div>

	<div class="w100 text-left font-tahoma text-darkgray ltr">
		@if($input->validation_rules)
			{{ $input->validation_rules }}
		@else
			<i>(no validation rules)</i>
		@endif

		<span class="pull-right btn btn-link btn-xs text-red noDisplay -delete" onclick="inputSelectorRemove('{{$input->id}}')">
				{{ trans("manage::forms.button.delete") }}
		</span>
	</div>

</div>
