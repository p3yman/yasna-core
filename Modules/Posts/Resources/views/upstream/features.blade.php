@extends('manage::layouts.template')

@section('content')
	@include("manage::upstream.tabs")

	@include("manage::widgets.toolbar" , [
	'buttons' => [
		[
			'target' => "modal:manage/posts/upstream/feature/create/0",
			'type' => "success",
			'caption' => trans('manage::forms.button.add'),
			'icon' => "plus-circle",
		],
	],
	'search' => [
		'target' => url('manage/upstream/features/') ,
		'label' => trans('manage::forms.button.search') ,
		'value' => isset($keyword)? $keyword : '' ,
	],
])

	@include("manage::widgets.grid" , [
		'table_id' => "tblFeatures",
		'row_view' => "posts::upstream.features-row",
//		'handle' => "counter",
		'headings' => [
			'#',
			[trans('validation.attributes.title'),60],
			'',
			'',
			[trans('validation.attributes.status'),300],
			trans('manage::forms.button.action'),
		],
	])

@endsection