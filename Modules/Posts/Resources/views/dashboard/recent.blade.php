@php
	$posttype = posttype( str_after($widget['key'] , 'post-recent-') );
	$models = $posttype->spreadMeta()->recent();
@endphp

<table class="table table-striped">
	@include("manage::widgets.grid-null")
	
	@foreach($models as $model)
		<tr>
			<td class="text-center">
				{{ pd($loop->iteration) }}
			</td>
			<td>
				@include("manage::widgets.grid-text" , [
					'text' => $model->title ,
					'link' => "modal:manage/posts/act/$model->hashid/info" ,
				])
			</td>
			<td>
				@include("manage::widgets.grid-date" , [
					'date' => $model->published_at,
				])
			</td>
		</tr>
	@endforeach
	
	<tr>
		<td class="text-center">
			<i class="fa fa-ellipsis-h "></i>
		</td>
		<td colspan="2">
			@include("manage::widgets.grid-text" , [
				'text' => "(".trans_safe("posts::general.browse_all").")" ,
				'link' => "url:manage/posts/$posttype->slug" ,
			])

		</td>
	</tr>

</table>
