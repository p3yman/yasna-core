@extends('manage::layouts.template')

@section('content')
	@include("manage::downstream.tabs")
	@include("manage::widgets.toolbar")

    <div class="container container-md">
        <div class="row-masonry row-masonry-xl-3 row-masonry-lg-2">
		   @php
			   $printed = false ;
		   @endphp
            @foreach($models as $model)

                @include('manage::widgets.panel-link-icon',[
                    'title' => $model->title,
                    'icon' => $model->icon ,
                    'color' => "",
                    'link' => "modal:manage/posts/downstream/posttype/setting/-hashid-/" ,
                    'condition' => $model->can('setting') ,
                ])

            @endforeach
        </div>
    </div>

	@if(!$printed)
		<div class="null">
			{{ trans('manage::forms.feed.nothing') }}
		</div>
	@endif

@endsection
