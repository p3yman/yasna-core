@include("manage::layouts.modal-start" , [
	'form_url' => route('posttype-downstream-save') ,
	'modal_title' => $model->title,
])


<div class="modal-body">
	@php
		$inputs = $model->setting_inputs;
	@endphp

	@include('manage::forms.hidden' , [
		'name' => 'hashid' ,
		'value' => $model->hashid,
	])

	{{--
	|--------------------------------------------------------------------------
	| No Content
	|--------------------------------------------------------------------------
	|
	--}}


	@if(!$count = $inputs->count())
		<div class="null">
			{{ trans('manage::forms.feed.nothing') }}
		</div>
	@endif




	{{--
	|--------------------------------------------------------------------------
	| Browse
	|--------------------------------------------------------------------------
	|
	--}}
	@foreach($inputs as $input)

		{{--{!!--}}
		{{--widget($input->data_type)--}}
			{{--->name($slug = $input->slug)--}}
			{{--->label($input->title)--}}
			{{--->value( $model->$slug ? $model->$slug : $input->default_value )--}}
			{{--->class( $input->css_class )--}}
			{{--->options( $input->options_array )--}}
			{{--->help( $input->hint )--}}
			{{--->addonIf( user()->isDeveloper()  , $slug)--}}
			{{--->inForm()--}}
		{{--!!}--}}

		@include("manage::widgets.input-".$input->data_type , [
			'name' => $slug = $input->slug ,
			'label' => $input->title ,
			'value' => $model->$slug ,//? $model->$slug : $input->default_value ,
			'class' => $input->css_class,
			'options' => $input->options_array ,
			'hint' => $input->hint ,
		])

	@endforeach



	{{--
	|--------------------------------------------------------------------------
	| Footer
	|--------------------------------------------------------------------------
	|
	--}}

</div>

@include("manage::forms.buttons-for-modal" , [
	'condition' => $count ,
])
@include("manage::layouts.modal-end")
