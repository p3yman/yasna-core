{!!
widget("combo")
	->name('domain_id')
	->options($domains = $model->domainsCombo())
	->label('tr:yasna::domains.domain')
	->value($model->domain_id)
!!}