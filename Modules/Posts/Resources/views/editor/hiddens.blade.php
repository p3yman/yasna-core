<div class="noDisplay">
	{{--
	|--------------------------------------------------------------------------
	| General
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.hiddens" , ['fields' => [
//		['hashid' , encrypt($model->id) , 'txtId'] ,
//		['id' , encrypt($model->id) , 'txtId'] ,
		['id' , $model->hashid , 'txtId'],
		['type' , encrypt($model->type) , 'txtType'],
		['sisterhood' , $model->sisterhood] ,
	]])


	{{--
	|--------------------------------------------------------------------------
	| Hidden "approval" button
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::forms.button" , [
		'name' => "_submit",
		'type' => "submit",
		'value' => "approval",
		'id' => "btnApproval",
		'label' => "Approval",
	])

</div>