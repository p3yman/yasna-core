@if($model->has('locales'))
	<div class="panel panel-default post-locales">
		<div class="panel-heading">
			{{ trans("posts::features.locales") }}
		</div>

		<div class="panel-body p0">

			@include("posts::editor.locales-inside")

		</div>

	</div>
@endif

@include("manage::forms.hidden" , [
	'name' => "locale",
	'value' => $model->locale,
	'id' => "txtLocale",
])