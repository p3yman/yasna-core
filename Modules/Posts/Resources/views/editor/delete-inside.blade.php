{{--
|--------------------------------------------------------------------------
| In 'create' mode where nothing is saved yet!
|--------------------------------------------------------------------------
|
--}}

@if(!$model->exists)
	<div class="text-danger f14 p20">
		{{ trans('posts::general.delete_alert_for_unsaved_post') }}
	</div>

	<div class="text-center m20">
		<a href="{{ url("manage/posts/$model->type") }}" class="btn btn-lg btn-danger ph45">{{ trans('manage::forms.button.sure_delete') }}</a>
	</div>
	<div class="text-center m20">
		<button type="button" class="btn btn-link" onclick='$("#divDelete").slideUp("fast")'>{{ trans("manage::forms.button.cancel") }}</button>
	</div>
@endif


{{--
|--------------------------------------------------------------------------
| If it's a copied version
|--------------------------------------------------------------------------
|
--}}

@if($model->isCopy())

	<div class="text-danger f14 p20">
			{{ trans('posts::general.delete_alert_for_copies') }}
	</div>

	<div class="text-center m20">
		<button type="submit" name="_submit" value="delete" class="btn btn-lg btn-warning ph45">{{ trans('posts::general.delete_this_copy') }}</button>
	</div>
	<div class="text-center m20">
		<button type="submit" name="_submit" value="delete_original" class="btn btn-lg btn-danger ph45">{{ trans('posts::general.delete_original_post') }}</button>
	</div>
	<div class="text-center m20">
		<button class="btn btn-link" type="button"  onclick='$("#divDelete").slideUp("fast")'>{{ trans("manage::forms.button.cancel") }}</button>
	</div>
@endif


{{--
|--------------------------------------------------------------------------
| Other Situations
|--------------------------------------------------------------------------
| Different notes when the post is already published and when not
--}}

@if($model->exists and !$model->isCopy())
		<div class="text-danger f14 p20">
			@if($model->isPublished())
				{{ trans('posts::general.delete_alert_for_published_post') }}
			@else
				{{ trans("posts::general.delete_alert_for_draft_posts") }}
			@endif
		</div>

	<div class="text-center m20">
		<button type="submit" name="_submit" value="delete" class="btn btn-lg btn-danger ph45">{{ trans('manage::forms.button.sure_delete') }}</button>
	</div>
	<div class="text-center m20">
		<button class="btn btn-link" type="button"  onclick='$("#divDelete").slideUp("fast")'>{{ trans("manage::forms.button.cancel") }}</button>
	</div>
@endif
