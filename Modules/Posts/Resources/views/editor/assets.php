<?php
service("Manage:template_assets")
    //->add()
    //->link("Posts:js/editor.js")
    //->order(90)

    ->add()
    ->link("posts:css/postEditorStyles.min.css")
    ->order(91)

    ->add()
    ->link("posts:js/editor.js")
    ->order(91);

service("posts:editor_handlers")->handle($model) ;
