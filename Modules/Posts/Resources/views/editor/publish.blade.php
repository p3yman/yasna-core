<div class="refresh">{{ url("manage/posts/editor-act/$model->hashid/publish") }}</div>
<div class="panel panel-default text-center">
	{{--<div class="panel-heading text-right" ondblclick="divReload('divPublishPanel')">{{ trans('posts::general.save_and_publish') }}</div>--}}
	<div class="panel-body bg-ultralight w100">

		{{--
		|--------------------------------------------------------------------------
		| Change Detectors
		|--------------------------------------------------------------------------
		|
		--}}
		<input type="hidden" id="txtChangeWarning" value="{{$model->isApproved()? '1' : '0'}}">
		<input type="hidden" id="txtChangeDetected" value="0">

		{{--
		|--------------------------------------------------------------------------
		| Copy Identicator
		|--------------------------------------------------------------------------
		|
		--}}
		@php
			$parent = $model->parent;
		@endphp
		@if($model->isCopy())
			<div class="alert alert-danger">
				{{ trans('posts::general.copy_of') }}
				@if($parent->isPublished())
					&nbsp;{{ trans('posts::general.published_post') }}

				@elseif($parent->isApproved())
					&nbsp;{{ trans('posts::general.approved_post') }}
				@endif
				<div class="mv5">
					<a href="{{ url($model->parent->edit_link) }}" target="_blank">{{ $model->parent->title }}</a>
				</div>
			</div>
		@endif


		{{--
		|--------------------------------------------------------------------------
		| Current Status
		|--------------------------------------------------------------------------
		|
		--}}
		@php
			$status_color = manageStatus($model->status , 'color');
		@endphp

		<div class="text-center alert alert-{{ $status_color }}">
			{{ manageStatus($model->status) }}
		</div>

		{{--
		|--------------------------------------------------------------------------
		| Main Publish Button
		|--------------------------------------------------------------------------
		|
		--}}
		<div class="btn-group">

			{{-- Main Button --}}
			@if($model->canPublish() and $model->isApproved())
				<button type="submit" name="_submit" value="publish" class="btn btn-taha btn-primary">{{ trans('posts::general.update_button') }}</button>
			@elseif($model->canPublish() and !$model->isApproved())
				<button type="submit" name="_submit" value="publish" class="btn btn-taha btn-primary">{{ trans('posts::general.publish') }}</button>
			@elseif($model->isApproved() and !$model->canPublish())
				<button type="submit" name="_submit" disabled value="approval" class="btn btn-taha btn-primary">{{ trans('posts::general.update_button') }}</button>
			@else
				<button type="submit" name="_submit" value="approval" class="btn btn-taha btn-primary">{{ trans('posts::general.send_for_approval') }}</button>
			@endif

			{{-- Carret --}}
			<button type="button" class="btn btn-primary dropdown-toggle minWidthAuto" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<span class="caret"></span>
				<span class="sr-only">other_save_options</span>
			</button>


			{{-- Small Buttons --}}
			<ul class="dropdown-menu">
				@include("posts::editor.publish-buttons" , [ 'buttons' => [
					[
						'command' => "adjust_publish_time",
						'__condition' => $schedule_apperaed = boolval($model->has('schedule') and !$model->isPublished() and !$model->isScheduled() and !$parent->isPublished()),
						'condition' => $schedule_apperaed = boolval($model->has('schedule') and !$model->isPublished()),
						'id' => 'lnkSchedule',
					],
					[
						'command' => "send_for_approval",
						'condition' => !$model->isApproved() or !$model->canPublish(),
					],
					[
						'command' => "-",
						'condition' => $schedule_apperaed ,
					],
					[
						'command' => "refer_back",
						'condition' => $model->canPublish() and !$model->isOwner() and !$model->isApproved(),
					],
					[
						'command' => "refer_to",
						'condition' => $model->exists and $model->canPublish() and !$model->isApproved(),
					],
					[
						'command' => "unpublish",
						'condition' => $model->canPublish() and $model->isPublished(),
					],
					[
						'command' => "delete",
						'condition' => $model->canDelete(),
					],
					[
						'command' => "-",
						'condition' => $model->has('history_system'),
					],
//					[
//						'command' => "history",
//						'condition' => $model->has('history_system'),
//					],

				]])
			</ul>

		</div>


		{{--
		|--------------------------------------------------------------------------
		| Small Buttons
		|--------------------------------------------------------------------------
		|
		--}}
		@if($model->canPublish() and !$model->isOwner() and !$model->isApproved())
			<div class="mv10">
				<button type="button" class="btn btn-link btn-xs " onclick="postsAction('refer_back' , '{{$model->id}}')">
					<span class="text-danger">{{trans('posts::general.refer_back')}}</span>
				</button>
			</div>
		@endif

		@if(true)
			<div class="mv10">
				<button type="submit" name="_submit" value="save" class="btn btn-link btn-xs">{{ trans('posts::general.save_draft') }}</button>
			</div>
		@endif

		@if($model->has('preview'))
			<div class="mv10">
				<a href="{{$model->preview_link}}" target="_blank" class="btn btn-xs btn-link">{{ trans('posts::general.preview') }}</a>
			</div>
		@endif



	</div>

</div>
