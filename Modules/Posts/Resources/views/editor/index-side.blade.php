@foreach( service('posts:editor_side')->read() as $service)
	<div id="div{{ studly_case($service['key'])  }}Panel">
		@include($service['blade'])
	</div>
@endforeach