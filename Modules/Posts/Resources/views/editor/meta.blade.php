@php
    $inputs = $model->posttype->editorInputs();
    $hashids = '';
@endphp

@if($inputs->count())
	<div class="panel panel-default mv20">
		<div class="panel-body">

			@foreach($inputs->get() as $input)

				@include("manage::widgets.input-".$input->data_type , [
					'name' => $slug = $input->slug ,
					'label' => $input->title ,
					'value' => $model->$slug ,
					'class' => $input->css_class,
					'hint' => $input->hint ,
					'options' => $input->options_array ,
				])

				@php
					$hashids .= ",$input->hashid";
				@endphp

			@endforeach


		</div>
	</div>
@endif

@include("manage::forms.hidden" , [
	'name' => "_meta_inputs",
	'value' => $hashids ,
]     )
