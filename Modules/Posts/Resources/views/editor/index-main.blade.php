@foreach( service('posts:editor_main')->read() as $service)
	<div id="div{{ studly_case($service['key'])  }}Panel">
		@include($service['blade'])
	</div>
@endforeach