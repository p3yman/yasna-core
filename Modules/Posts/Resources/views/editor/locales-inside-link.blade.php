@if($sister->exists)


	@include("manage::widgets.grid-text" , [
		'text' => trans("manage::forms.status_text.$sister->status"),
		'link' => "urlN:" . ($sister->canEdit() ?  $sister->edit_link : ''),
		'color' => trans("manage::forms.status_color.".$sister->status),
		'size'=> '11',
		'div_style' => "margin-bottom: 0px",
	])




@else


	@if($model->can("create.$locale"))
		<a class="btn btn-default w100 btn-sm" style="font-size: 10px" href="{{ $sister->create_link }}">{{ trans('validation.attributes.create') }}</a>
	@else
		<span class="text-gray f10">{{ trans("manage::forms.status_text.so_far_absent") }}</span>
	@endif



@endif