@if($model->has('schedule'))
	<div id="divSchedule" class="panel panel-default text-center {{ $model->isScheduled()? '' : 'noDisplay' }}">
		<div class="panel-heading text-right">
			<span class="pull-left">
				<label class="fa fa-times text-gray clickable" onclick="postToggleSchedule('hide')"></label>
			</span>
			{{ trans('posts::general.adjust_publish_time') }}
		</div>
		<div class="panel-body">

			{{--
			|--------------------------------------------------------------------------
			| Hidden Set Input
			|--------------------------------------------------------------------------
			|
			--}}
			@include("manage::forms.hidden" , [
				'name' => "_schedule",
				'id' => "txtScheduleFlag",
				'value' => $model->isScheduled(),
			])

			{!!
				widget('persian-date-picker')
					->name('published_at')
					->value($model->published_at ?: now()->toDateTimeString())
			!!}

		</div>

	</div>
@endif
