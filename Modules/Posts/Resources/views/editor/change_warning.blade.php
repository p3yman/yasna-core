@if($model->isPublished())
	<div class="panel panel-danger noDisplay">
		<div class="panel-heading">
			<span class="fa fa-exclamation-triangle f14"></span>
			{{ trans('manage::forms.general.be_aware') }}
		</div>


		<div class="panel-body bg-ultralight ">

			<div class="text-danger f14 p20">
				@if($model->canPublish())
					{{ trans("posts::general.copy_suggestion_when_can_publish") }}
				@else
					{{ trans("posts::general.copy_suggestion_when_cannot_publish") }}
				@endif
			</div>

			<div class="text-center">
				<button type="submit" name="_submit" value="save" class="btn btn-default ph45 ">{{ trans('posts::general.save_draft') }}</button>
			</div>

		</div>


	</div>
@endif