@extends('manage::layouts.template')

@include("posts::editor.assets")


@section('content')

	@include('manage::forms.opener' , [
		'id' => 'frmEditor',
		'url' => route('post-save'),
		'class' => 'js',
		'onchange' => "postFormChange()" ,
		'no_validation' => "1" ,
	])

	@include("manage::forms.feed")
	@include("posts::editor.hiddens")


	<div class="row w100" style="margin-bottom: 50px">
		<div class="col-md-9">
			@include("posts::editor.index-main")
		</div>
		<div class="col-md-3">
			@include("posts::editor.index-side")
		</div>
	</div>


	{{--@include("posts::editor.modals")--}}
	@include("manage::forms.closer")

	<script>postsInit()</script>

@endsection

