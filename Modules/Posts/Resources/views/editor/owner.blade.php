<span class="refresh">
	{{ url("manage/posts/editor-act/$model->hashid/owner") }}
</span>
@include("manage::widgets.grid-tiny" , [
	'text' => trans('posts::general.post_owner').': '.$model->getPerson('owned_by')->full_name,
	'link' => $model->id? "modal:manage/posts/act/-hashid-/owner" : '',
	'icon' => "user-o" ,
	'div_class' => "mv10" ,
]     )
