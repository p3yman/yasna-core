<div id="divUnpublish" class="panel panel-warning noDisplay">
	<div class="panel-heading">
		<span class="fa fa-exclamation-triangle f14"></span>
		<span class="mh10">
				{{ trans('posts::general.unpublish') }}
			</span>
	</div>


	<div class="panel-body bg-ultralight ">


		<div class="text-warning f14 p20">
			{{ trans('posts::general.unpublish_warning') }}
		</div>

		<div class="text-center m20">
			<button type="submit" name="_submit" onclick="$('#divUnpublish').slideUp('fast')" value="unpublish" class="btn btn-lg btn-warning ph45">{{ trans('posts::general.sure_unpublish') }}</button>
		</div>
		<div class="text-center m20">
			<button class="btn btn-link" type="button"  onclick="$('#divUnpublish').slideUp('fast')">{{ trans("manage::forms.button.cancel") }}</button>
		</div>

	</div>

</div>