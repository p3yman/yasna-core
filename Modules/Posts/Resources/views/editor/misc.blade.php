@php
	$services = service("posts:editor_misc")->read();
@endphp
<div class="panel panel-default">
	<div class="panel-heading">
		{{ trans('manage::forms.general.misc') }}
	</div>

	<div class="panel-body">

		@foreach(service("posts:editor_misc")->read() as $service)
			<div id="div{{ studly_case($service['key'])  }}Panel">
				@include($service['blade'])
			</div>

			@if(!$loop->last)
				<hr>
			@endif
		@endforeach

	</div>
</div>
