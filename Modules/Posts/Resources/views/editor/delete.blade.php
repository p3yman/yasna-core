	<div id="divDelete" class="panel panel-danger noDisplay">
		<div class="panel-heading">
			<span class="fa fa-exclamation-triangle f14"></span>
			<span class="mh10">
				{{ trans('manage::forms.button.soft_delete') }}
			</span>
		</div>


		<div class="panel-body bg-ultralight ">
			@include("posts::editor.delete-inside")
		</div>
	</div>
