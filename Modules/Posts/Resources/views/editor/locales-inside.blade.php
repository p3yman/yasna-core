{{-- Current lang --}}
<div class="current-locale">
    <div class="flex-row">

        <div class="flex-col">
            <img src="{{ Module::asset("manage:images/lang-$model->locale.png") }}" style="width: 15px">
            <span class="title">{{ trans("manage::forms.lang.$model->locale") }}</span>
        </div>

        <div class="flex-col">
            <span class="badge bg-success">
                <i class="fa fa-check"></i>
                {{ trans('posts::general.this_page') }}
            </span>
        </div>

    </div>
</div>

<div class="other-locales @if( count($model->posttype->locales_array) < 2 ) empty @endif">

    @foreach($model->posttype->locales_array as $locale)
        @php($sister = $model->in($locale))

        @if( $locale != $model->locale )

            <div class="locale-item">

                <div class="flex-row">

                    <div class="flex-col">
                        <img src="{{ Module::asset("manage:images/lang-$locale.png") }}" style="width: 15px">
                        <span class="title">{{ trans("manage::forms.lang.$locale") }}</span>
                    </div>

                    <div class="flex-col">
                        @include("posts::editor.locales-inside-link")
                    </div>

                </div>

            </div>

        @endif

    @endforeach

</div>