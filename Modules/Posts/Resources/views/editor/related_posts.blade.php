@php
	$relateds = $model->relateds()->get();
	$options = $relateds->map(function ($post) {
		return $post->only(['hashid', 'title']);
	})->toArray();
	$value = implode(',', array_column($options, 'hashid'));
@endphp

{!!
	widget('selectize')
		->name('_relateds')
		->ajaxSource(route('posts.relateds.search', ['hashid' => $model->hashid]))
		->searchField('title')
		->valueField('hashid')
		->captionField('title')
		->options($options)
		->value($value)
		->label($__module->getTrans('features.related_posts'))
!!}
