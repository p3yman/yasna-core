@include("manage::forms.select_self" , [
	'name' => "template",
	'top_label' => trans("validation.attributes.template") ,
	'options' => $model->posttype->templatesCombo(),
	'caption_field' => "1",
	'value_field' => "0",
	'value' => $model->template,
])
