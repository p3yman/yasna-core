<div id="divReferBack" class="panel panel-warning noDisplay">
	<div class="panel-heading">
		<span class="fa fa-exclamation-triangle f14"></span>
		<span class="mh10">
				{{ trans('posts::general.refer_back') }}
			</span>
	</div>


	<div class="panel-body bg-ultralight ">

		<div class="w80" style="max-width: 600px">
			@include("manage::forms.textarea" , [
				'name' => "moderate_note",
				'label' => trans('validation.attributes.moderate_note'),
				'id' => "txtModerateNote",
				'value' => $model->moderate_note ,
				'class' => "form_required",
			])

			@include('manage::forms.group-start')

				@include('manage::forms.button' , [
					'label' => trans('posts::general.refer_back'),
					'name' => "_submit" ,
					'type' => "submit" ,
					'value' => "reject" ,
					'id' => "btnReject" ,
					'shape' => 'warning',
					'link' => 'postsAction("submit_reject")',
				])

			@include('manage::forms.button' , [
				'label' => trans('manage::forms.button.cancel'),
				'shape' => 'link',
				'link' => "$('#divReferBack').slideUp('fast')",
			])

			@include('manage::forms.group-end')

		</div>

	</div>

</div>

