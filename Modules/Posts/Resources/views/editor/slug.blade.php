@include("manage::forms.input-self" , [
	'name' => "slug",
	'top_label' => trans('validation.attributes.slug'),
	'top_label_style' => "" ,
	'value' => $model->slug,
	'class' => "ltr text-center",
	'placeholder' => "like-this",
	'on_blur' => "postSlugCheck()",
	'id' => "txtSlug",
])
<div id="divSlugFeedback"></div>
