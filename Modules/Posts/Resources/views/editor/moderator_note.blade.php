@if($model->isRejected() and $model->moderate_note)
	<div class="panel panel-danger">
		<div class="panel-heading">
			<span class="fa fa-sticky-note f14"></span>
			{{ trans('validation.attributes.moderate_note') }}
		</div>


		<div class="panel-body bg-warning">
			@include("manage::widgets.grid-date" , [
				'date' => $model->moderated_at,
				'text2' => trans('manage::forms.general.by').' '.$model->getPerson('moderated_by')->full_name ,
				'class' => "f10" ,
			]     )
			@include("manage::widgets.grid-text" , [
				'text' => $model->moderate_note,
				'size' => "14" ,
				'div_class' => "mv10" ,
			]     )

		</div>


	</div>
@endif