<div class="panel panel-primary mv20">
	<div class="panel-heading">
		<i class="fa fa-user-plus mh5"></i>
		{{ trans("posts::features.register_settings") }}
	</div>

	<div class="panel-body bg-ultralight">

		{{--
		|--------------------------------------------------------------------------
		| Package and Availability
		|--------------------------------------------------------------------------
		|
		--}}


		<div class="row">
			<div class="col-md-6">

				{{-- Starts_at --}}
				{!!
					widget('persian-date-picker')
						->name('register_starts_at')
						->label(trans('validation.attributes.starts_at'))
						->value($model->register_starts_at)
				!!}

			</div>
			<div class="col-md-6">

				{!!
					widget('persian-date-picker')
						->name('register_ends_at')
						->label(trans('validation.attributes.ends_at'))
						->value($model->register_ends_at)
				!!}

			</div>
		</div>

	</div>
</div>
