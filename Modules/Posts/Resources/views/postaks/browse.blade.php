@extends('manage::layouts.template')

@section('content')
	@include("posts::postaks.tabs")

	{{--
	|--------------------------------------------------------------------------
	| Toolbar
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::widgets.toolbar" , [
		'mass_actions' => service('posts:postak_mass_actions')->indexed('icon' , 'caption' , 'link') ,
		'buttons' => [
			[
				'target' => 'modal:' . url("manage/postaks/$posttype->slug/create"),
				'type' => "success",
				'caption' => trans('manage::forms.button.add_to').' '.$posttype->title ,
				'icon' => "plus-circle",
				'condition' => $posttype->can('create') ,
			],
		],
		'search' => [
			'target' => url("manage/postaks/$posttype->slug/$locale/search"),
			'label' => trans('manage::forms.button.search'),
			'value' => isset($keyword)? $keyword : ''
		],
	])



	{{--
	|--------------------------------------------------------------------------
	| Grid
	|--------------------------------------------------------------------------
	|
	--}}
	@include("manage::widgets.grid" , [
		'table_id' => "tblPostaks",
		'row_view' => "posts::postaks.browse-row",
		'handle' => "selector",
	])

@endsection

