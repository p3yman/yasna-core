@include("manage::widgets.grid-badges" , [$badges = [
	[
		'text' => trans("manage::forms.button.edit") ,
		'color' => "default" ,
		'icon' => "pen" ,
		'link' => $model->edit_link ,
		'condition' => user()->isDeveloper() ,
	],
	[
		'text' => trans("posts::pin.pinned") ,
		'color' => "purple text-white" ,
		'icon' => "thumb-tack" ,
		'link' => $model->canPublish() ? "modal:manage/posts/act/-hashid-/pin" : '',
		'condition' => $model->has('pin') and $model->pinned,
	],

	[
		'text' => trans('posts::general.copy'),
		'color' => "orange",
		'icon' => "pencil",
		'link' => "urlN:".$model->parent->browse_link ,
		'condition' => $model->isCopy(),
	],
	[
		'text' => trans("manage::forms.status_text.$model->status"),
		'color' => config("manage.status.color.$model->status"),
		'icon' => config("manage.status.icon.$model->status"),
	],
	[
		'text' => trans('posts::general.rejected'),
		'color' => "danger",
		'icon' => "undo",
		'condition' => $model->isRejected(),
	],
	[
		'text' => trans("manage::forms.lang.$model->locale"),
		'condition' => $model->has('locales'),
		'link' => "modal:manage/posts/act/-hashid-/locales/" ,
		'icon' => "globe",
	]
]])
