@extends('manage::layouts.template')

@section('content')
	@include("posts::postaks.tabs")

	{{--
	|--------------------------------------------------------------------------
	| Toolbar
	|--------------------------------------------------------------------------
	|
	--}}

	@include("manage::widgets.toolbar")

	<div class="panel panel-default m20">

		@include('manage::forms.opener',[
			'url' => "manage/postaks/$posttype->slug/$locale/search" ,
			'class' => 'js-' ,
			'method' => 'get',
		])

		<br>

		@include('manage::forms.hiddens' , ['fields' => [
			['searched' , 1],
		]])

		@include('manage::forms.input' , [
			'name' => 'keyword',
			'class' => 'form-required form-default'
		])

		@include('manage::forms.group-start')

		@include('manage::forms.button' , [
			'label' => trans('manage::forms.button.search'),
			'shape' => 'success',
			'type' => 'submit' ,
		])

		@include('manage::forms.group-end')

		@include('manage::forms.feed' , [])

		@include('manage::forms.closer')
	</div>



	{{--
	|--------------------------------------------------------------------------
	| Panel
	|--------------------------------------------------------------------------
	|
	--}}

@endsection

