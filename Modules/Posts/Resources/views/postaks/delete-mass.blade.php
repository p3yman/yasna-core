@include("manage::layouts.modal-mass" , [
	'form_url' => route('posts.postak.mass_delete') ,
	'modal_title' => trans("manage::forms.button.soft_delete") ,
	'save_label' => trans("manage::forms.button.soft_delete") ,
	'save_shape' => "danger" ,
]   )
