@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
	'refresh_url' => route('posts.postak.update', ['hashid' => $model->hashid], false),
])

<td>
	@if ($model->canEdit())
		<a href="javascript:void(0)" onclick="masterModal('{{ route('posts.postak.edit', ['post_type' => $model->type, 'hashid' => $model->hashid], false) }}')">{{ $model->title }}</a>
	@else
		{{ $model->title }}
	@endif

	@include("manage::widgets.grid-tiny" , [
		'text' => trans('posts::general.post_owner').': '.$model->getPerson('owned_by')->full_name,
		'link' => "modal:manage/posts/act/-hashid-/owner" ,
		'icon' => "user-o" ,
	])

	@include("manage::widgets.grid-tiny" , [
		'text' => "$model->id|$model->hashid" ,
		'color' => "gray" ,
		'icon' => "bug",
		'class' => "font-tahoma" ,
		'locale' => "en" ,
		'condition' => user()->isDeveloper() ,
	])
</td>

<td>
	@foreach($model->sisters as $sister)
		<img src="{{ Module::asset("manage:images/lang-{$sister->locale}.png") }}" style="width: 25px">
	@endforeach
</td>


@include("posts::postaks.browse-actions")
