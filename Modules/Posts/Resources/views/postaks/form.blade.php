<?php
/* @var string $title_label */
/* @var \Modules\Posts\Entities\Post $model */
/* @var \Modules\Posts\Entities\Posttype $posttype */
?>

{!! widget('modal')->target(route('posts.postak.save', ['post_type' => $model->type], false))->label($title_label) !!}

<div class='modal-body'>
	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
		widget("input")
			->name('slug')
			->label("tr:posts::postak.post_slug")
			->value($model->slug)
			->condition($posttype->hasFeature('slug'))
			->inForm()
			->required()
	!!}

	@include('posts::postaks.locales')

	@include('manage::forms.buttons-for-modal' , [])
</div>
