@include("manage::widgets.grid-actionCol"  , [
	'actions' => [
		['edit', trans('posts::postak.edit'), 'modal:' . route('posts.postak.edit', ['hashid' => $model->hashid, 'posttype' => $model->type], false), $model->canEdit()],
		['trash', trans('posts::postak.move_to_trash'), 'modal:' . route('posts.postak.delete_action', ['hashid' => $model->hashid, 'posttype' => $model->type], false), !$model->trashed() && $model->canDelete()],
		['recycle', trans('posts::postak.restore'), 'modal:' . route('posts.postak.undelete_action', ['hashid' => $model->hashid, 'posttype' => $model->type], false), $model->trashed() && $model->canBin()],
		['trash', trans('posts::postak.delete_permanently'), 'modal:' . route('posts.postak.destroy_action', ['hashid' => $model->hashid, 'posttype' => $model->type], false), $model->trashed() && $model->canBin()],
	]
])
