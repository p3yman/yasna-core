<?php
/* @var string $title_label */
/* @var \Modules\Posts\Entities\Post $model */
/* @var \Modules\Posts\Entities\Post $sister */
/* @var \Modules\Posts\Entities\Posttype $posttype */
?>

<ul class="nav nav-tabs">
	@foreach($model->posttype->locales_array as $locale)
		<li{{ $loop->first ? ' class=active' : '' }}>
			<a data-toggle="tab" href="#{{ $locale }}">
				<img src="{{ Module::asset("manage:images/lang-$locale.png") }}" style="width: 15px">
				{{ trans("manage::forms.lang.$locale") }}
			</a>
		</li>
	@endforeach
</ul>

<div class="tab-content">
@foreach($model->posttype->locales_array as $locale)
	@php($sister = $model->in($locale))

	<div id="{{ $locale }}" class="tab-pane fade{{ $loop->first ? ' in active' : '' }}">
		{!!
			widget('hidden')
				->name("postak_hashid[$locale]")
				->value($sister->hashid)
		!!}

		{!!
			widget('input')
				->name("postak_title[$locale]")
				->value($sister->title)
				->label('tr:posts::postak.post_title')
				->inForm()
		!!}

		{!!
			widget('textarea')
				->name("postak_text[$locale]")
				->value($sister->text)
				->label('tr:posts::postak.post_text')
				->inForm()
		!!}
	</div>
@endforeach
</div>
