@include("manage::layouts.modal-mass" , [
	'form_url' => route('posts.postak.mass_undelete') ,
	'modal_title' => trans("manage::forms.button.undelete") ,
	'save_label' => trans("manage::forms.button.undelete") ,
	'save_shape' => "success" ,
]   )
