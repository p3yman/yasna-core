@include("manage::widgets.tabs", [
	'current' =>  $page[1][0],
	'tabs' => [
		["published" , trans('posts::criteria.published')],
		["bin" , trans('posts::criteria.bin') ],
		["$locale/search" , trans('manage::forms.button.search')],
	],
])
