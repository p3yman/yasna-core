@if(isset($modules['posts']))
    @foreach(model('posttype')::getAll() as $posttype)
        @include("users::permits.module" , [
             'title' => $posttype->title,
             'module' => "posts-$posttype->slug" ,
             'permits' => $modules['posts'] ,
             'locales' => $posttype->locales_array,
        ]     )
    @endforeach
@endif
