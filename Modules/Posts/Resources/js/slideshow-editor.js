/**
 * SlideShow Editor
 * -------------------------
 * Created by Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2018-09-30
 */

jQuery(function ($) {

    $(document).ready(function () {
        let slidesContainer = document.querySelector('.js_slidesContainer');

        // Make slide items sortable
        Sortable.create(slidesContainer, {
            animation: 150,
            draggable: '.js_slideItem',
            handle   : '.js_slideHandle',
            sort     : true
        });

        // Show / Hide Preview
        $(document).on('click', '.js_previewer', function () {
            let panel   = $(this).closest('.panel');
            let panelId = panel.attr('id');

            if (!panel.hasClass('show_preview')) {
                generateSlidePreview(panelId);
            }

            panel.toggleClass('show_preview');
        });

        // Add new slide
        $(document).on('click', '.js_addNewSlide', addNewSlide);

        // Remove slide
        $(document).on('click', '.js_removeSlide', function () {
            let slide = $(this).closest('.js_slideItem');
            deleteSlide(slide);
        });

        // Dynamic panel title setter
        $(document).on('input', '.js_titleSetter', function () {
            let heading = $(this).closest('.js_slideItem').find('[data-title-getter]');
            if ($(this).val().trim()) {
                heading.text($(this).val());
            } else {
                heading.text("...");
            }
        });

        // View slides activation status in title
        $(document).on('change', '.js_slideDeactivated', function () {
            let sign = $(this).closest('.js_slideItem').find('.deactivated');

            if ($(this).is(':checked')) {
                sign.addClass('hidden');
            } else {
                sign.removeClass('hidden');
            }
        });

        // Show / Hide date picker when needed
        $(document).on('change', '.js_autoPublish', function () {
            let datePicker = $(this).closest('.checkbox').siblings('.js_publishAt');

            if ($(this).is(':checked')) {
                datePicker.removeClass('hidden');
            } else {
                datePicker.addClass('hidden');
            }
        });

        // Show general preview
        $(document).on('click', '.js_showPreview', function () {
            generateSlider();
            $('#js_previewModal').modal("show");
        });

        // Save data
        $(document).on('click', '#js_saveSlideShow', function () {
            $.ajax({
                url    : url('manage/slideshows/save'),
                type   : "json",
                method : "POST",
                data   : {
                    "_token": $("#form_token").val(),
                    posttype: $("#posttype-id").val(),
                    data    : JSON.stringify(collectData())
                },
                success: function () {
                    successMessage();
                    location.reload();
                },
                error  : function () {
                    errorMessage();
                    $("#js_saveSlideShow").removeClass('disabled')
                }
            })

        })
    });


    /**
     * Generates slider based on form information
     */
    function generateSlider() {
        let data      = collectData();
        let slideList = "";
        let sliderEl;
        let sliderId  = generateId('slider_');

        $.each(data.slides, function (id, slideData) {
            if (+slideData.view_slide) {
                let slideElement = createSlideTemplate(slideData);
                slideList += '<li>' + slideElement + '</li>';
            }
        });

        sliderEl = '<ul class="rslides" id=' + sliderId + '>' + slideList.trim() + '</ul>';


        $('#js_generalPreview').empty().html(sliderEl);

        $('.rslides').responsiveSlides(generateSliderOptions(data.settings));
    }


    /**
     * Generate slider options
     *
     * @param settings
     * @return {{auto: boolean, speed: number, timeout: number, pause: boolean, prevText: string, nextText: string, nav: boolean, pager: boolean}}
     */
    function generateSliderOptions(settings) {
        return {
            auto    : Boolean(+settings.auto),             // Boolean: Animate automatically, true or false
            speed   : +settings.speed || 500,            // Integer: Speed of the transition, in milliseconds
            timeout : +settings.timeout || 4000,          // Integer: Time between slide transitions, in milliseconds
            pause   : Boolean(+settings.pause),
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>',
            nav     : true,
            pager   : true
        }
    }


    /**
     * Generates Slide Preview based on entered data
     *
     * @param panelId
     */
    function generateSlidePreview(panelId) {
        let data = collectFormData('#' + panelId);
        let el   = createSlideTemplate(data);

        $('#' + panelId).find('.js_slidePreview').html(el)
    }


    /**
     * Creates slide html element
     *
     * @param data
     * @return element {*}
     */
    function createSlideTemplate(data) {
        let temp = $.trim($('#js_slidePreviewTemplate').html());
        return temp
            .replace(/{{image}}/ig, data.image)
            .replace(/{{title}}/ig, data.slide_title || "")
            .replace(/{{subtitle}}/ig, data.slide_subtitle || "")
            .replace(/{{description}}/ig, data.slide_description || "")
            .replace(/{{button_link}}/ig, data.button_link || "")
            .replace(/{{button_title}}/ig, data.button_title || "")
            .replace(/{{button_target}}/ig, (Number(data.button_target)) ? "_blank" : "")
            .replace(/{{position}}/ig, data.position || "")
            .replace(/{{theme}}/ig, data.theme || "")
            ;
    }


    /**
     * Collects page data
     *
     * @return {{settings: {}, slideShow: {}, slides: {}}}
     */
    function collectData() {
        let data       = {
            settings : {},
            slideShow: {},
            slides   : {}
        };
        data.settings  = collectFormData('#js_slideShowSetting');
        data.slideShow = collectFormData('#js_slideShowData');
        data.slides    = collectSlidesData('.js_slidesContainer');
        console.log(data);
        return data;
    }


    /**
     * Collects slides data
     *
     * @param selector
     */
    function collectSlidesData(selector) {
        let slidesData = {};

        $(selector).find('.js_slideItem').each(function (i, slide) {
            let id         = $(slide).attr('id');
            slidesData[id] = {};
            slidesData[id] = collectFormData('#' + id);
        });

        return slidesData;
    }


    /**
     * Set image src
     *
     * @param selector
     * @return {*|jQuery}
     */
    function getSlideImage(selector) {
        return $(selector).find('.js_slideImage').attr('src');
    }


    /**
     * Collects form data
     *
     * @param selector
     */
    function collectFormData(selector) {
        let data     = {};
        let formJson = $(selector).find(':input').serializeArray();

        $(formJson).each(function (i, input) {
            data[input.name] = input.value;
        });

        if (getSlideImage(selector)) {
            data['image'] = getSlideImage(selector);
        }

        return data;
    }


    /**
     * Remove slide
     *
     * @param el
     */
    function deleteSlide(el) {

        $('.js_removeSlide').addClass('disabled');


        $.ajax({
            url    : url('manage/slideshows/delete'),
            type   : "json",
            method : "POST",
            data   : {
                "_token" : $("#form_token").val(),
                post_id  : el.attr('id')
            },
            success: function () {
                successMessage();
                   el.remove();
                $('.js_removeSlide').removeClass('disabled')
            },
            error  : function () {
                errorMessage();
                $('.js_removeSlide').removeClass('disabled')
            }
        })
    }


    /**
     * Add created slide to slides list
     */
    function addNewSlide() {
        let id      = generateId();
        let element = createSlide(id);
        $('.js_slidesContainer').append(element);
        divReload('slide-date-'+id);
    }


    /**
     * Create a slide element with given id
     *
     * @param id
     * @returns {*}
     */
    function createSlide(id) {
        let temp = $.trim($('#js_slideTemplate').html());
        return temp.replace(/{{id}}/ig, id)
    }


    /**
     * Generates random string id
     *
     * @param prefix
     * @param postfix
     * @returns {string}
     */
    function generateId(prefix, postfix) {
        prefix  = prefix || "";
        postfix = postfix || "";

        let hashId = Date.now().toString(36);

        return prefix + hashId + postfix;
    }
}); //End Of siaf!
