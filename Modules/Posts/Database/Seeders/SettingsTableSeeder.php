<?php

namespace Modules\Posts\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class SettingsTableSeeder extends Seeder
{
    private $main_data = [] ;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->mainData());
    }

    protected function mainData()
    {
        $this->addToMainData('fold_posts_on_manage_sidebar', 'upstream', 'boolean');
        return $this->main_data ;
    }

    public function addToMainData($slug, $category, $data_type, $default_value = 0, $order = 91)
    {
        if (setting()->hasnot($slug)) {
            $this->main_data[] = [
                'slug' => $slug ,
                 'title' => trans("posts::settings.seeder.$slug")  ,
                 'category' => $category ,
                 'order' => $order ,
                 'data_type' => $data_type ,
                 'default_value' => $default_value ,
            ];
            return true;
        } else {
            return false ;
        }
    }
}
