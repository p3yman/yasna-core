<?php

namespace Modules\Posts\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Yasna\Providers\YasnaServiceProvider;

class InputsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
             [
                  'slug'              => "max_per_page",
                  'title'             => "گنجایش صفحه‌های فهرست",
                  'type'              => "downstream",
                  'order'             => "11",
                  'data_type'         => "text",
                  'css_class'         => "form-required",
                  'validation_rules'  => "sometimes|required|numeric|min:5|max:100",
                  'purifier_rules'    => "ed",
                  'default_value'     => "10",
                  'default_value_set' => "1",
                  'hint'              => "تعداد نوشته‌ها در هر صفحه. بهترین حالت: بین ۱۰ و ۳۰",
             ],
             [
                  'slug'              => "fresh_time_duration",
                  'title'             => "مدت تازگی",
                  'type'              => "downstream",
                  'order'             => "12",
                  'data_type'         => "text",
                  'validation_rules'  => "numeric|min:0|max:10|",
                  'purifier_rules'    => "ed",
                  'default_value'     => "7",
                  'default_value_set' => "1",
                  'hint'              => "مدت زمانی که یک نوشته، علامت «جدید» بر خود می‌گیرد. بین صفر تا ۱۰ روز.",
             ],
             [
                  'slug'              => "gallery_thumb_size",
                  'title'             => "طول چکیده",
                  'type'              => "upstream",
                  'order'             => "43",
                  'data_type'         => "text",
                  'validation_rules'  => "numeric|min:150|max:1000",
                  'purifier_rules'    => "ed",
                  'default_value'     => "500",
                  'default_value_set' => "1",
                  'hint'              => "گنجایش متن چکیده بر حسب کاراکتر",
             ],
             [
                  'slug'          => "slideshow_auto",
                  'title'         => trans('posts::slideshow.auto_change'),
                  'type'          => "upstream",
                  'order'         => "44",
                  'data_type'     => "boolean",
                  'default_value' => false,
             ],
             [
                  'slug'          => "slideshow_pause",
                  'title'         => trans('posts::slideshow.pause_on_hover'),
                  'type'          => "upstream",
                  'order'         => "45",
                  'data_type'     => "boolean",
                  'default_value' => false,
             ],
             [
                  'slug'             => "slideshow_speed",
                  'title'            => trans('posts::slideshow.speed'),
                  'type'             => "upstream",
                  'order'            => "46",
                  'data_type'        => "text",
                  'validation_rules' => "numeric",
             ],
             [
                  'slug'             => "slideshow_timeout",
                  'title'            => trans('posts::slideshow.timeout'),
                  'type'             => "upstream",
                  'order'            => "47",
                  'data_type'        => "text",
                  'validation_rules' => "numeric",
             ],
             [
                  'slug'          => 'view_slide',
                  'title'         => trans('posts::slideshow.view_slide'),
                  'type'          => "editor",
                  'order'         => "48",
                  'data_type'     => "boolean",
                  'default_value' => true,
             ],
             [
                  'slug'      => "button_title",
                  'title'     => trans('posts::slideshow.button_title'),
                  'type'      => "editor",
                  'order'     => "49",
                  'data_type' => "text",
             ],
             [
                  'slug'      => "button_link",
                  'title'     => trans('posts::slideshow.button_link'),
                  'type'      => "editor",
                  'order'     => "50",
                  'data_type' => "text",
             ],
             [
                  'slug'          => "button_target",
                  'title'         => trans('posts::slideshow.button_target'),
                  'type'          => "editor",
                  'order'         => "51",
                  'data_type'     => "boolean",
                  'default_value' => false,
             ],
             [
                  'slug'      => "slide_position",
                  'title'     => trans('posts::slideshow.position'),
                  'type'      => "editor",
                  'order'     => "52",
                  'data_type' => "text",
             ],
             [
                  'slug'      => "slide_theme",
                  'title'     => trans('posts::slideshow.theme'),
                  'type'      => "editor",
                  'order'     => "53",
                  'data_type' => "text",
             ],
             [
                  'slug'          => "slide_auto_publish",
                  'title'         => trans('posts::slideshow.auto_publish'),
                  'type'          => "editor",
                  'order'         => "54",
                  'data_type'     => "boolean",
                  'default_value' => false,
             ],
             [
                  'slug'      => "slide_publish_at",
                  'title'     => trans('posts::slideshow.publish_at'),
                  'type'      => "editor",
                  'order'     => "55",
                  'data_type' => "text",
             ],
        ];


        yasna()->seed('inputs', $data);
    }
}
