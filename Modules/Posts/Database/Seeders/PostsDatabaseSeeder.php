<?php

namespace Modules\Posts\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PostsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(InputsTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
        //$this->call(PosttypesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
