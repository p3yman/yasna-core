<?php

namespace Modules\Posts\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Feature;
use Modules\Yasna\Providers\YasnaServiceProvider;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->getData());
        $this->applyRelationships();
    }



    /**
     * apply relationships
     *
     * @return void
     */
    protected function applyRelationships()
    {
        $data = [
             'abstract'  => "abstract_length",
             'slideshow' => "
                     slideshow_auto, 
                     slideshow_speed,
                     slideshow_timeout,
                     slideshow_pause,
                     view_slide,
                     button_title,
                     button_link,
                     button_target,
                     slide_position,
                     slide_theme,
                     slide_auto_publish,
                     slide_publish_at,
                ",
        ];

        foreach ($data as $key => $slugs) {
            $model = model("feature", $key);
            if (!$model or !$model->id) {
                continue;
            }
            $model->attachInputs(explode(',', $slugs));
        }
    }



    /**
     * make a single data item
     *
     * @param string $slug
     * @param string $icon
     * @param string $fields
     * @param string $meta
     * @param int    $order
     *
     * @return array
     */
    protected function makeItem(string $slug, string $icon, $fields = null, $meta = null, int $order = 11): array
    {
        return [
             "order"       => $order,
             "slug"        => $slug,
             "title"       => trans("posts::seeder.features.$slug"),
             //           "meta-hint"   => trans("posts::seeder.feature-hints.$slug"),
             "icon"        => $icon,
             "post_fields" => $fields,
             "post_meta"   => $meta,
        ];
    }



    /**
     * get seeder data
     *
     * @return array
     */
    protected function getData()
    {
        return [
             $this->makeItem('title2', 'subscript', 'title2'),
             $this->makeItem('long_title', 'text-height', 'long_title'),
             $this->makeItem('text', 'list', 'text'),
             $this->makeItem('text', 'list', 'text'),
             $this->makeItem('abstract', 'compress', 'abstract'),
             $this->makeItem('pin', 'thumb-tack', 'pinned_at,pinned_by'),
             $this->makeItem('rss', 'rss'),
             $this->makeItem('domains', 'code-fork'),
             $this->makeItem('single_view', 'laptop'),
             $this->makeItem('list_view', 'bars'),
             $this->makeItem('searchable', 'search'),
             $this->makeItem('schedule', 'search', '', 'original_published_at'),
             $this->makeItem('event', 'calendar', 'event_starts_at,event_ends_at'),
             $this->makeItem('register', 'user-plus', 'register_starts_at,register_ends_at'),
             //$this->makeItem('visibility_choice', 'shield', ''),
             $this->makeItem('template_choice', 'th-large', 'template'),
             $this->makeItem('slug', 'hashtag', 'slug'),
             $this->makeItem('manage_sidebar', 'puzzle-piece'),
             $this->makeItem('locales', 'globe'),
             $this->makeItem('dashboard', 'window-restore'),
             $this->makeItem('minimal_post', 'file-o'),
             $this->makeItem('api_discoverable', 'rocket'),
             $this->makeItem('related_posts', 'link'),
             $this->makeItem('slideshow', 'image'),
        ];
    }
}
