<?php

namespace Modules\Posts\Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Posttype;
use Modules\Yasna\Providers\YasnaServiceProvider;

class PosttypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('posttypes', $this->mainData());
        $this->relationships() ;
    }

    protected function mainData()
    {
        $data = [
            [
                'slug'           => "pages",
                'title'          => "برگه‌ها",
                'singular_title' => "برگه",
                'template'       => "post",
                'icon'           => "file-o",
                'order'          => "2",
            ],
        ];


        return $data ;
    }

    protected function relationships($data = false)
    {
        if (!$data) {
            $data = [
                'pages' => "text,single_view,searchable,slug,manage_sidebar" ,
            ];
        }

        foreach ($data as $key => $slugs) {
            $model = posttype($key) ;
            if (!$model or !$model->id) {
                continue ;
            }
            $model->attachFeatures(explode(',', $slugs)) ;
            $model->attachRequiredInputs() ;
        }
    }
}
