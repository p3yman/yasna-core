<?php

namespace Modules\Posts\Database\Seeders;

use App\Models\Post;
use Illuminate\Database\Seeder;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createDummyPosttypes();
        $this->createDummyPosts();
    }



    /**
     * create dummy posttypes with standard languages and features
     *
     * @param int $count
     *
     * @return void
     */
    protected function createDummyPosttypes($count = 5)
    {
        $features = "text,single_view,searchable,slug,manage_sidebar,locales,slug,title2,abstract";
        $locales  = implode(",", $this->getSomeLocales());

        for ($i = 1; $i <= $count; $i++) {

            try {
                $posttype = posttype()->batchSave([
                     "slug"           => dummy()::slug(),
                     "title"          => $title = dummy()::persianWord(),
                     "singular_title" => $title,
                     "icon"           => "bug",
                     "locales"        => $locales,
                ]);
            } catch (\Exception $e) {
                continue;
            }

            $posttype->attachFeatures(explode(",", $features));
            $posttype->cacheFeatures();
        }
    }



    /**
     * create dummy posts
     *
     * @param int $count
     *
     * @return void
     */
    protected function createDummyPosts($count = 100)
    {
        for ($i = 1; $i <= $count; $i++) {
            $post = $this->createOneDummyPost();
            $this->createOneSister($post);
        }
    }



    /**
     * @param string $sisterhood
     * @param string $locale
     *
     * @return Post
     */
    protected function createOneDummyPost($sisterhood = null, $locale = null)
    {
        $deleted   = !boolval(rand(0, 10));
        $published = boolval(rand(0, 1));
        $posttype  = posttype()->inRandomOrder()->first();

        if (!$sisterhood) {
            $sisterhood = post()::generateSisterhood();
        }
        if (!$locale) {
            $locale = array_random($posttype->locales_array);
        }

        $post = post()->batchSave([
             "slug"         => "dummy-" . dummy()::slug(),
             "type"         => $posttype->slug,
             "title"        => dummy()::persianTitle(),
             "title2"       => !boolval(rand(0, 5)) ? dummy()::persianTitle() : "",
             "locale"       => $locale,
             "sisterhood"   => $sisterhood,
             "abstract"     => dummy()::persianWord(rand(10, 50)),
             "text"         => dummy()::persianText(rand(1, 10)),
             "deleted_at"   => $deleted ? now() : null,
             "is_draft"     => rand(0, 1),
             "deleted_by"   => $deleted ? user()->inRandomOrder()->first()->id : 0,
             "published_at" => $published ? now() : null,
             "published_by" => $published ? user()->inRandomOrder()->first()->id : 0,
             "created_by"   => user()->inRandomOrder()->first()->id,
             "owned_by"     => user()->inRandomOrder()->first()->id,
             "moderated_by" => $published ? user()->inRandomOrder()->first()->id : 0,
             "moderated_at" => $published ? now() : null,
        ]);

        return $post;
    }



    /**
     * create a random sister for the given post
     *
     * @param Post $post
     */
    protected function createOneSister($post)
    {
        $locales = array_remove($this->getSomeLocales(), $post->locale);
        return $this->createOneDummyPost($post->sisterhood, array_random($locales));
    }



    /**
     * get a sample array of locales to be used in creator functions
     *
     * @return array
     */
    protected function getSomeLocales()
    {
        return [
             'fa',
             'en',
             'ar',
             'fr',
        ];
    }
}
