<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->string('type');
            $table->integer('order')->default(11)->index() ;
            $table->string('data_type');

            $table->string('css_class')->nullable() ;
            $table->string('validation_rules')->nullable() ;
            $table->string('purifier_rules')->nullable() ;
            $table->string('hint')->nullable() ;
            $table->text('default_value')->nullable() ;
            $table->boolean('default_value_set')->default(0);
            $table->longText('meta')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->boolean('converted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inputs');
    }
}
