<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInputPosttypeRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_posttype', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('input_id')->index();
            $table->unsignedInteger('posttype_id')->index();
            $table->timestamps();

            $table->foreign('posttype_id')->references('id')->on('posttypes')->onDelete('cascade');
            $table->foreign('input_id')->references('id')->on('inputs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_posttype');
    }
}
