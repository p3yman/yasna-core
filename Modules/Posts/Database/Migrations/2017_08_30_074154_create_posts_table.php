<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');

            /*-----------------------------------------------
            | Basic Fields ...
            */
            $table->string('slug')->nullable()->index();
            $table->string('type')->index();
            $table->string('title')->index();
            $table->string('title2')->nullable();
            $table->text('long_title')->nullable() ;

            $table->boolean('is_draft')->default(1);

            $table->string('locale', 2)->default('fa')->index();
            $table->unsignedInteger('copy_of')->default(0)->index();
            $table->string('sisterhood', 30)->default('')->index();
            //			$table->string('domains', '200')->nullable();

            $table->longText('abstract')->nullable();
            $table->longText('text')->nullable();

            /*-----------------------------------------------
            | Feature-Based Fields ...
            */
            $table->string('template')->nullable();

            $table->string('featured_image')->nullable();
            $table->text('attached_files')->nullable();

            $table->timestamp('event_starts_at')->nullable();
            $table->timestamp('event_ends_at')->nullable();

            $table->timestamp('register_starts_at')->nullable();
            $table->timestamp('register_ends_at')->nullable();

            $table->timestamp('pinned_at')->nullable()->index();
            $table->unsignedInteger('pinned_by')->default(0);

            /*-----------------------------------------------
            | General Meta ...
            */
            $table->longText('meta')->nullable();


            /*-----------------------------------------------
            | Timestamps ...
            */
            $table->timestamps();
            $table->softDeletes();
            $table->timestamp('published_at')->nullable();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);
            $table->unsignedInteger('published_by')->default(0);

            $table->unsignedInteger('owned_by')->default(0)->index();
            $table->unsignedInteger('moderated_by')->default(0)->index();
            $table->timestamp('moderated_at')->nullable();

            $table->index('created_at');

            /*-----------------------------------------------
            | Reserved for Later Use ...
            */
            $table->boolean('converted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
