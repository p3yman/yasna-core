<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomainPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_post', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('post_id')->index();
            $table->unsignedInteger('domain_id')->index();

            $table->boolean('is_main')->default(false)->index();

            $table->timestamps();

            $table->foreign('domain_id')->references('id')->on('domains')->onDelete('cascade');
            ;
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            ;
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_post');
    }
}
