<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosttypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->string('title');
            $table->string('singular_title') ;
            $table->string('icon')->nullable() ;
            $table->tinyInteger('order')->default(11);
            $table->string('locales')->nullable() ;

            $table->string('template');
            $table->string('header_title')->nullable(); //<~~ necessary for grouping of menus

            $table->text('optional_meta')->nullable();
            $table->longText('meta')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->boolean('converted')->default(0);

            $table->index(['order', 'title']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posttypes');
    }
}
