<?php

namespace Modules\Posts\Schedules;

use Illuminate\Database\Eloquent\Collection;
use Modules\Posts\Events\PostPublished;
use Modules\Yasna\Services\YasnaSchedule;

class PostScheduledPublished extends YasnaSchedule
{

    /**
     * run the job's logic
     */
    protected function job()
    {
        foreach ($this->recentlyPublishedPosts() as $post) {
            event(new PostPublished($post));
        }
    }



    /**
     * get a collection of recently published posts
     *
     * @return Collection
     */
    protected function recentlyPublishedPosts()
    {
        $elector = [
             "criteria" => "published",
        ];

        return post()->elector($elector)->whereDate("published_at", '>', now()->subDay())->get();
    }
}
