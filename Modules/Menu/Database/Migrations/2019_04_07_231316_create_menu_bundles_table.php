<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_bundles', function (Blueprint $table) {
            $table->increments('id');

            $table->string("slug")->index();
            $table->string("title")->index();
            $table->string('locale')->nullable();

            $table->timestamps();

            $table->timestamp("published_at")->nullable();
            $table->unsignedInteger("published_by")->default(0);

            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_bundles');
    }
}
