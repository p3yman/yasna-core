<?php

namespace Modules\Menu\Entities\Traits;

trait BundleResourceTrait
{
    /**
     * boot BundleResourceTrait
     *
     * @return void
     */
    public static function bootBundleResourceTrait()
    {
        static::addDirectResources('*');
    }
    
}
