<?php

namespace Modules\Menu\Entities\Traits;

trait BundlePermitTrait
{
    /**
     * check if the current user can perform a particular action on the menu bundles
     *
     * @param string $action
     *
     * @return bool
     */
    public function can(string $action): bool
    {
        return user()->can("menu.$action");
    }



    /**
     * check if the current user CANNOT perform a particular action on the menu bundles
     *
     * @param string $action
     *
     * @return bool
     */
    public function cannot(string $action): bool
    {
        return !$this->can($action);
    }



    /**
     * check if the user can create a new menu bundle
     *
     * @return bool
     */
    public function canCreate()
    {
        return $this->can("create");
    }



    /**
     * check if the user can edit the menu bundle in question
     *
     * @return bool
     */
    public function canEdit()
    {
        return $this->can("edit");
    }



    /**
     * check if the user can create a new bundle or can edit an existing one
     *
     * @return bool
     */
    public function canCreateOrEdit()
    {
        if ($this->exists) {
            return $this->canEdit();
        }

        return $this->canCreate();
    }



    /**
     * check if the user can delete the bundle in question
     *
     * @return bool
     */
    public function canDelete()
    {
        return $this->can("delete");
    }



    /**
     * check if the user can undelete or permanent delete of the bundle in question
     *
     * @return bool
     */
    public function canRestore()
    {
        return $this->can("bin");
    }



    /**
     * /**
     * check if the online user can view the bundle in question
     *
     * @return bool
     */
    public function canView()
    {
        if ($this->isPublished()) {
            return true;
        }

        return $this->can("view");
    }

}
