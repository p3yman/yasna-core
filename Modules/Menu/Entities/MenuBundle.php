<?php

namespace Modules\Menu\Entities;

use Modules\Menu\Entities\Traits\BundlePermitTrait;
use Modules\Menu\Entities\Traits\BundleResourceTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuBundle extends YasnaModel
{
    use BundlePermitTrait;
    use BundleResourceTrait;
    use SoftDeletes;



    /**
     * check if the bundle is published
     *
     * @return bool
     */
    public function isPublished()
    {
        return boolval($this->published_at);
    }



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "items",
        ];
    }
}
