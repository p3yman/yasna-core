<?php

namespace Modules\Menu\Providers;

use Modules\Menu\Http\Endpoints\V1\DestroyEndpoint;
use Modules\Menu\Http\Endpoints\V1\ListEndpoint;
use Modules\Menu\Http\Endpoints\V1\RestoreEndpoint;
use Modules\Menu\Http\Endpoints\V1\SaveEndpoint;
use Modules\Menu\Http\Endpoints\V1\SingleEndpoint;
use Modules\Menu\Http\Endpoints\V1\TrashEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class MenuServiceProvider
 *
 * @package Modules\Menu\Providers
 */
class MenuServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerUserPermissions();
        $this->registerEndpoints();
    }



    /**
     * register UserPermissions
     *
     * @return void
     */
    private function registerUserPermissions()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('menu')
             ->value('browse ,  create ,  edit ,  delete ,  bin')
        ;
    }



    /**
     * register Endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }

        endpoint()->register(ListEndpoint::class);
        endpoint()->register(SingleEndpoint::class);
        endpoint()->register(SaveEndpoint::class);
        endpoint()->register(TrashEndpoint::class);
        endpoint()->register(RestoreEndpoint::class);
        endpoint()->register(DestroyEndpoint::class);
    }

}
