<?php

namespace Modules\Menu\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/menu-single
 *                    Menu Single View
 * @apiDescription    Menu Single View
 * @apiVersion        1.0.0
 * @apiName           Menu Single View
 * @apiGroup          Menu
 * @apiPermission     Guest
 * @apiParam {string} [id] hashid of the endpoint
 * @apiParam {string} [slug] slug of the endpoint, in the absence of the `id`
 * @apiSuccessExample Success-Response: TODO: This is the default save feedback. Change if your work is different
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-404",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-404",
 *      "moreInfo": "endpoint.moreInfo.endpoint-404",
 *      "errors": []
 * }
 */
class SingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Menu Single View";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Menu\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@single';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
