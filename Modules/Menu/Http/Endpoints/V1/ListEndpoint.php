<?php

namespace Modules\Menu\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Menu\Http\Controllers\V1\CrudController;

/**
 * @api               {GET}
 *                    /api/modular/v1/menu-list
 *                    Menu List
 * @apiDescription    List of all available menus (will filter published_at if not privileged enough)
 * @apiVersion        1.0.0
 * @apiName           Menu List
 * @apiGroup          Menu
 * @apiPermission     Guest
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          TODO: Fill this!
 *      },
 *      "results": {
 *          TODO: Fill this!
 *      }
 * }
 * @method  CrudController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Menu List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Menu\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "reached" => "ListEndpoint",
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
