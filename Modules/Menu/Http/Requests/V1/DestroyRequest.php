<?php

namespace Modules\Menu\Http\Requests\V1;

use App\Models\MenuBundle;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class DestroyRequest
 *
 * @property MenuBundle $model
 */
class DestroyRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "menu-bundle";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canRestore();
    }
}
