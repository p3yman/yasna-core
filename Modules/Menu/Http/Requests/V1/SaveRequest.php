<?php

namespace Modules\Menu\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use App\Models\MenuBundle;


/**
 * Class SingleRequest
 *
 * @property MenuBundle $model
 */
class SaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "menu-bundle";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id = $this->model->id;

        return [
             'slug'  => "required|alpha_dash|unique:menu_bundles,slug,$id,id",
             "title" => "string|required",
             "items" => "array",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "slug",
             "title",
             "items",
        ];
    }
}
