<?php

namespace Modules\Menu\Http\Controllers\V1;

use App\Models\MenuBundle;
use Illuminate\Database\Eloquent\Builder;
use Modules\Menu\Http\Requests\V1\DestroyRequest;
use Modules\Menu\Http\Requests\V1\RestoreRequest;
use Modules\Menu\Http\Requests\V1\SaveRequest;
use Modules\Menu\Http\Requests\V1\SingleRequest;
use Modules\Menu\Http\Requests\V1\TrashRequest;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\Yasna\Services\YasnaApiController;

class CrudController extends YasnaApiController
{
    /**
     * list the available menus
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array
     */
    public function list(SimpleYasnaListRequest $request)
    {
        /** @var MenuBundle $model */
        $model = model("menu-bundle");

        /** @var Builder $builder */
        $builder = $model->withTrashed();

        // @TODO
//        if (!$model->canBrowse()) {
//            $builder = $builder->whereNotNull("published_at");
//        }
//        $builder = $builder->whereNotNull("published_at");

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get a single menu item
     *
     * @param SingleRequest $request
     *
     * @return array
     */
    public function single(SingleRequest $request)
    {
        return $this->success($request->model->toSingleResource());
    }



    /**
     * save a model (called for both: create and edit)
     *
     * @param SaveRequest $request
     *
     * @return array
     */
    public function save(SaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * perform soft delete action of the bundle
     *
     * @param TrashRequest $request
     *
     * @return array
     */
    public function trash(TrashRequest $request)
    {
        $saved = $request->model->delete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * perform an undelete action of the bundle
     *
     * @param RestoreRequest $request
     *
     * @return array
     */
    public function restore(RestoreRequest $request)
    {
        $saved = $request->model->undelete();

        return $this->typicalSaveFeedback($saved);
    }



    /**
     * perform a permanent delete action of the bundle
     *
     * @param DestroyRequest $request
     *
     * @return array
     */
    public function destroy(DestroyRequest $request)
    {
        $saved = $request->model->hardDelete();

        return $this->typicalSaveFeedback($saved);
    }
}
