<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class DividerComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "type"  => "divider",
             "title" => "", // Can accept any html content
             "size"  => "", // Divider space. Accept: xs, sm, md, lg, xlg
             "color" => "", // Divider color class name.

             "dashed"    => false, // Divider line dashed type.
             "dotted"    => false, // Divider line dotted type.
             "inverted"  => false, // Divider line inverted type.
             "hidden"    => false, // Divider line hidden type.
             "only_icon" => false, // Divider with icon only.

        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
        ];
    }

}
