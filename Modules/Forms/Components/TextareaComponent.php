<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class TextareaComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "cols"      => "", // Textarea Columns Number
             "rows"      => "", // Textarea Rows Number
             "no_resize" => false, // Set true if no-resize is desired
        ]);
    }

}
