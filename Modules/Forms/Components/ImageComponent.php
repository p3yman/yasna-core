<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class ImageComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "src"  => "",
        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return array_merge(parent::requiredAttributes(), [
             "src",
        ]);
    }

}
