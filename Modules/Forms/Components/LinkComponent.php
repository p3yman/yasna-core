<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class LinkComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "url"        => "",
             "title"      => "",
             "icon"       => "",
             "new_window" => false,
             "params"     => [],
             "queries"    => [],
             "options"    => [],
        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [];
    }
}
