<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class PanelComponent extends ComponentAbstract
{

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "title"       => "", // Can accept any html content
             "icon"        => "",
             "collapsible" => false,
             "color"       => "",
             "size"        => "",
        ]);
    }



    /**
     * @inheritdoc
     */
    public function hasChildren()
    {
        return true;
    }

}
