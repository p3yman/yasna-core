<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class RowComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function hasChildren()
    {
        return true;
    }

}
