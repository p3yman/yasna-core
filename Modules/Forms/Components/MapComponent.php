<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class MapComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "center_lat"       => 0,
             "center_lng"       => 0,
             "zoom"             => 14,
             "height"           => 400,
             "api_key"          => "",
        ]);
    }

}
