<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class CheckboxListComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "options"  => [], // Checkbox options provided in a key => value pairs
             "switch"   => false, // Load switch UI for checkbox
             "circular" => false, // Load circular switch UI for checkbox
             "inline"   => false, // Checkbox Inline Style
             "alt"      => false, // Checkbox Alt Style
             "size"     => "", // Checkbox Alt Style
             "color"    => "", // Checkbox Alt Style
             "no_label" => false, // Checkbox No-label style
        ]);
    }

}
