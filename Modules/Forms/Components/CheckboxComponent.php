<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class CheckboxComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "on_text"  => "Yes", // Checkbox ON Text
             "off_text" => "No", // Checkbox OFF Text
             "true"     => 1, // Checkbox ON Value
             "false"    => 0, // Checkbox OFF Value
             "switch"   => false, // Load switch UI for checkbox
             "circular" => false, // Load circular switch UI for checkbox
             "inline"   => false, // Checkbox Inline Style
             "alt"      => false, // Checkbox Alt Style
             "size"     => "", // Checkbox Alt Style
             "color"    => "", // Checkbox Alt Style
             "no_label" => false, // Checkbox No-label style
        ]);
    }

}
