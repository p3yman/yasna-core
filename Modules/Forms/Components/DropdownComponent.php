<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class DropdownComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "text"  => "",
             "color" => "",
             "size"  => "",
        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
        ];
    }



    /**
     * @inheritdoc
     */
    public function hasChildren()
    {
        return true;
    }
}
