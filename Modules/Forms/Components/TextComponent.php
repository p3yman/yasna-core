<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class TextComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return parent::attributes();
    }

}
