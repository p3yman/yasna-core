<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class RadioComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "options"  => [], // Radio options provided in a key => value pairs
             "switch"   => false, // Load switch UI for Radio
             "circular" => false, // Load circular switch UI for Radio
             "inline"   => false, // Radio Inline Style
             "alt"      => false, // Radio Alt Style
             "size"     => "", // Radio Alt Style
             "color"    => "", // Radio Alt Style
             "no_label" => false, // Radio No-label style

        ]);
    }

}
