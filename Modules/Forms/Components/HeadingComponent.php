<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class HeadingComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "title"    => "", // Can accept any html content
             "size"     => 1, // Size of heading from 1 to 6 to show h1 to h6
        ]);
    }

}
