<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class RangeComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "step"     => 1,
             "min"      => "",
             "max"      => "",
             "prefix"   => "",
             "suffix"   => "",
        ]);
    }

}
