<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class DateComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "input_format" => 'jYYYY/jM/jD', // Input Format view
             "format"       => 'X', // Input format value
             "max"          => null, // Date Max
             "min"          => null, // Date Min
        ]);
    }

}
