<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class RecaptchaComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "invisible" => false,
             "compact"   => false,
        ]);
    }

}
