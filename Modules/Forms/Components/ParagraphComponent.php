<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class ParagraphComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "text"  => "",
             "color" => "",
             "size"  => "",
        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return array_merge(parent::requiredAttributes(), [
             "text",
        ]);
    }

}
