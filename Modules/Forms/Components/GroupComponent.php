<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class GroupComponent extends ComponentAbstract
{

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "clonable" => false,
        ]);
    }



    /**
     * @inheritdoc
     */
    public function hasChildren()
    {
        return true;
    }

}
