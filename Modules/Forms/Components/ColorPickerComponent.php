<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class ColorPickerComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            "type"      => "chrome", // Type of color picker. Can be one of these options: chrome, material, compact, swatches, slider, sketch, photoshop
            "options"   => [], // Array of colors in one of common formats: '#f00', '#00ff00', '#00ff0055', 'rgb(201, 76, 76)', 'rgba(0,0,255,1)', 'hsl(89, 43%, 51%)', 'hsla(89, 43%, 51%, 0.6)'
        ]);
    }

}
