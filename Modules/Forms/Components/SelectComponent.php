<?php

namespace Modules\Forms\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class SelectComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "options"     => [], // Array of select options containing a value and a label.
             "multi"       => false, // Set true if multi-select is desired.
             "allow_empty" => false, // Allow select to be empty.
             "is_loading"  => false, // Select Loading Mode

             "label_field" => "label", // Option's Label Field Name
             "value_field" => "value", // Option's Value Field Name

             "group_select" => false, // Set true if group-select view is desired
             "group_label"  => null, // Group Label Field Name
             "group_values" => null, // Group Label Field Name

        ]);
    }

}
