<?php

namespace Modules\Forms\Services;

trait FormBinderOperationsTrait
{
    /**
     * merge a given form (components and validator) into this object
     *
     * @param FormBinder $form
     *
     * @return FormBinder|$this
     */
    public function mergeWith(FormBinder $form)
    {
        $this->components = array_merge($this->getComponents(), $form->getComponents());
        //TODO: Merge Validators too!

        return $this;
    }



    /**
     * get components, raw
     *
     * @return array
     */
    public function getComponents()
    {
        return $this->components;
    }
}
