<?php

namespace Modules\Forms\Services\Traits;

use Modules\Forms\Exceptions\ComponentNotDefinedException;
use Modules\Yasna\Services\ModuleHelper;

trait ComponentServicesTrait
{
    private static $service_order = 1;



    /**
     * register a component
     *
     * @param string $name
     * @param string $class
     *
     * @return void
     */
    public static function register(string $name, string $class)
    {
        static::service()
              ->add(kebab_case($name))
              ->order(static::$service_order++)
              ->class($class)
        ;
    }



    /**
     * get a list of registered components, paired with their attached class
     *
     * @return array
     */
    public static function list()
    {
        return static::service()->paired("class", "key");
    }



    /**
     * get the fully-qualified class name of a given component
     *
     * @param string $component_name
     *
     * @return null|string
     * @throws ComponentNotDefinedException
     */
    public static function getClass(string $component_name)
    {
        $array = static::list();

        if (!isset($array[kebab_case($component_name)])) {
            return null;
        }

        return $array[kebab_case($component_name)];
    }



    /**
     * get the instance of the current service
     *
     * @return ModuleHelper
     */
    private static function service()
    {
        return module('Forms')->service("components");
    }
}
