<?php

namespace Modules\Forms\Services\Traits;

use Modules\Forms\Services\FormComponent;

trait ComponentChildrenTrait
{
    protected $children = [];



    /**
     * specify if the component can have children
     *
     * @return bool
     */
    public function hasChildren()
    {
        return false;
        //override this method to change the behaviour
    }



    /**
     * add a child to the component
     *
     * @param string        $key
     * @param FormComponent $component
     *
     * @return $this;
     */
    public function addChild(string $key, FormComponent $component)
    {
        if (!$this->hasChildren()) {
            return $this;
        }

        $this->children[$key] = $component; // <~~ TODO: Should throw an error if already exist

        return $this;
    }



    /**
     * conditionally add a child to the component
     *
     * @param bool          $condition
     * @param string        $key
     * @param FormComponent $component
     *
     * @return $this;
     */
    public function addChildIf(bool $condition, string $key, FormComponent $component)
    {
        if (!$condition) {
            return $this;
        }

        return $this->addChild($key, $component);
    }



    /**
     * edit a child of the component, by its key
     *
     * @param string        $key
     * @param FormComponent $component
     *
     * @return $this;
     */
    public function editChild(string $key, FormComponent $component)
    {
        if (!$this->hasChildren()) {
            return $this;
        }

        $this->children[$key] = $component; // <~~ TODO: Should throw an error if not exist

        return $this;
    }



    /**
     * conditionally edit a child of the component, by its key
     *
     * @param bool          $condition
     * @param string        $key
     * @param FormComponent $component
     *
     * @return $this;
     */
    public function editChildIf(bool $condition, string $key, FormComponent $component)
    {
        if (!$condition) {
            return $this;
        }

        return $this->editChild($key, $component);
    }



    /**
     * remove a child of the component, by its key
     *
     * @param string $key
     *
     * @return $this;
     */
    public function removeChild(string $key)
    {
        if (!$this->hasChildren()) {
            return $this;
        }

        unset($this->children[$key]);

        return $this;
    }



    /**
     * conditionally remove a child of the component, by its key
     *
     * @param bool   $condition
     * @param string $key
     *
     * @return $this;
     */
    public function removeChildIf(bool $condition, string $key, FormComponent $component)
    {
        if (!$condition) {
            return $this;
        }

        return $this->removeChild($key);
    }



    /**
     * set all children at once
     *
     * @param array $children
     *
     * @return $this
     */
    public function children(array $children)
    {
        foreach ($children as $key => $child) {
            $this->addChild((string)$key, $child);
        }

        return $this;
    }



    /**
     * conditionally set all children at once
     *
     * @param bool  $condition
     * @param array $children
     *
     * @return $this
     */
    public function childrenIf(bool $condition, array $children)
    {
        if (!$condition) {
            return $this;
        }

        return $this->children($children);
    }
}
