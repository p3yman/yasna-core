<?php

namespace Modules\Forms\Services\Traits;

use Modules\Forms\Exceptions\ComponentOptionMissedException;

trait ComponentRenderTrait
{
    /**
     * convert the object to a key-value array
     *
     * @return array
     */
    public function toArray()
    {
        $array['component_name'] = $this->component_name;

        foreach ($this->allAttributes() as $attribute => $default) {
            $this->throwExceptionInAbsenceOfRequiredAttributes($attribute);

            $value = $this->renderDefaultValues($attribute, $default);
            $value = $this->renderObjectValues($value);
            $value = $this->renderArrayOfObjectValues($value);

            $array[$attribute] = $value;
        }

        $array = $this->renderPayloads($array);

        return $array;
    }



    /**
     * render object values to create standard arrays
     *
     * @param mixed $value
     *
     * @return mixed
     */
    private function renderObjectValues($value)
    {
        if (!is_object($value)) {
            return $value;
        }

        try {
            $value = $value->toArray();
        } catch (\Exception $e) {
            // $value = (array)$value; // This could be an alternative, if the automatic php cast didn't work well.
        };

        return $value;
    }



    /**
     * render object values bound into arrays to create standard arrays
     *
     * @param mixed $array
     *
     * @return array
     */
    private function renderArrayOfObjectValues($array)
    {
        if (!is_array($array)) {
            return $array;
        }

        foreach ($array as $key => $item) {
            $array[$key] = $this->renderObjectValues($item);
        }

        return $array;
    }



    /**
     * render the default values and return it, in case of missing
     *
     * @param string $attribute
     * @param mixed  $default
     *
     * @return mixed
     */
    private function renderDefaultValues(string $attribute, $default)
    {
        if (isset($this->$attribute)) {
            return $this->$attribute;
        }

        return $default;
    }



    /**
     * throw exception in the absence of a required attribute
     *
     * @param string $attribute
     *
     * @throws ComponentOptionMissedException
     */
    private function throwExceptionInAbsenceOfRequiredAttributes(string $attribute)
    {
        if (!isset($this->$attribute) and in_array($attribute, $this->requiredAttributes())) {
            throw new ComponentOptionMissedException($this->component_name, $attribute);
        }
    }



    /**
     * add an encrypted custom payload to the result array
     *
     * @param array $array
     *
     * @return array
     */
    private function renderPayloads(array $array): array
    {
        $payload = $this->getPayload($array);

        if ($payload) {
            $array['payload'] = encrypt($payload);
        }

        return $array;
    }



    /**
     * get all attributes
     *
     * @return array
     */
    private function allAttributes()
    {
        return array_merge($this->structuralAttributes(), $this->attributes());
    }



    /**
     * get structural attributes
     *
     * @return array
     */
    private function structuralAttributes()
    {
        $base = [
             "order" => 11,
        ];

        if($this->hasChildren()) {
            $base['children'] = [];
        }

        return $base;
    }

}
