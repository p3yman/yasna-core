<?php

namespace Modules\Forms\Services\BasicComponents;

class InputAbstract extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "default"     => "",
             "value"       => "",
             "placeholder" => "",
             "validation"  => "",
             "label"       => "",
             "label_desc"  => "",
             "desc"        => "",
             "clone"       => "",
             "size"        => "",
             "char_limit"  => "",
             "prepend"     => "",
             "append"      => "",
             "disabled"    => false,
             "readonly"    => false,
             "no_label"    => false,
        ]);
    }

}
