<?php

namespace Modules\Forms\Services\BasicComponents;

use Modules\Forms\Services\FormComponent;

class ComponentAbstract extends FormComponent
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "id"        => "",
             "name"      => "",
             "css"       => "",
             "css_class" => "",
             "width"     => "", // Width in percent
             "col"       => "", // From 1 to 12 to use in grid
        ];
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
        ];
    }
}
