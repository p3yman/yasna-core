<?php

namespace Modules\Forms\Services;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Forms\Contracts\ComponentInterface;
use Modules\Forms\Exceptions\ComponentNotDefinedException;
use Modules\Forms\Services\Traits\ComponentChildrenTrait;
use Modules\Forms\Services\Traits\ComponentRenderTrait;
use Modules\Forms\Services\Traits\ComponentServicesTrait;

abstract class FormComponent implements Arrayable, ComponentInterface
{
    use ComponentServicesTrait;
    use ComponentRenderTrait;
    use ComponentChildrenTrait;

    /** @var string */
    public $component_name;



    /**
     * build an instance of a component, extended from this class
     *
     * @param string $component_name
     *
     * @return FormComponent
     */
    public static function builder(string $component_name)
    {
        $class = static::getClass($component_name);

        if (!$class) {
            throw new ComponentNotDefinedException($component_name);
        }

        $class                 = new $class;
        $class->component_name = $component_name;

        return $class;
    }



    /**
     * magic method for general setters and getters
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $property = snake_case($name);

        /*-----------------------------------------------
        | Getters ...
        */
        if (starts_with($name, "get")) {
            $property = snake_case(str_after($name, "get"));
            return $this->$property;
        }

        /*-----------------------------------------------
        | conditioned call: widget()->folanIf( $condition , $other_arguments) ...
        */
        if (ends_with($name, "If")) {
            $property  = snake_case(str_before($name, "If"));
            $condition = array_shift($arguments);

            if (!boolval($condition)) {
                return $this;
            }
        }

        /*-----------------------------------------------
        | Boolean Things ...
        */
        if (!isset($arguments[0])) {
            $arguments[0] = true;
        }

        /*-----------------------------------------------
        | Finally ...
        */
        if (in_array($property, array_keys($this->allAttributes()))) {
            $this->$property = $arguments[0];
        }

        return $this;
    }



    /**
     * get custom payload to be encrypted
     *
     * @param array $array
     *
     * @return mixed
     */
    protected function getPayload(array $array)
    {
        return null; //override this to return your payloads
    }
}
