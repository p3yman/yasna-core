<?php

namespace Modules\Forms\Services;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Yasna\Services\GeneralTraits\Serializable;

class FormBinder implements Arrayable
{
    use Serializable;
    use FormBinderOperationsTrait;

    /** @var array  */
    protected $components = [];



    /**
     * FormBinder constructor.
     */
    public function __construct()
    {
        $this->components();
    }
    /**
     * get binder name
     *
     * @return string|null
     */
    public function name()
    {
        return null;
    }



    /**
     * get binder endpoint
     *
     * @return string|null
     */
    public function endpoint()
    {
        return null;
    }



    /**
     * get binder http method
     *
     * @return string
     */
    public function method()
    {
        return EndpointAbstract::HTTP_PUT;
    }



    /**
     * get the request class responsible to handle the request
     *
     * @return string|null
     */
    public function validator()
    {
        return null;
    }



    /**
     * get the array equivalence of the binder
     *
     * @return array
     */
    public function toArray()
    {
        $result = [];

        /** @var FormComponent $component */
        foreach ($this->components as $component) {
            $result[] = $component->toArray();
        }

        return $result;
    }



    /**
     * get the list of binder components
     *
     * @return void
     */
    public function components()
    {
        // add components here.
    }



    /**
     * add a component
     *
     * @param string $component_name
     *
     * @return FormComponent
     */
    public function add(string $component_name)
    {
        $this->components[] = component($component_name);

        return array_last($this->components);
    }
}
