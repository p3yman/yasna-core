<?php

namespace Modules\Forms\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MakeComponentCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-component';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make the uniform component in the chosen module.';



    /**
     * get the folder path.
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Components");
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        return studly_case($this->argument("class_name")) . "Component";
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "component.stub";
    }
}
