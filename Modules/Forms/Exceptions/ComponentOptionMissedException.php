<?php

namespace Modules\Forms\Exceptions;

use Exception;

class ComponentOptionMissedException extends Exception
{

    /**
     * ComponentOptionMissedException constructor.
     *
     * @param string $component
     * @param string $option
     */
    public function __construct(string $component, string $option)
    {
        parent::__construct("Option `$option` has to be provided for `$component` component.");
    }
}
