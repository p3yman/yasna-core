<?php

namespace Modules\Forms\Exceptions;

use Exception;

class ComponentNotDefinedException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct("`$name` is not a valid registered component.");
    }
}
