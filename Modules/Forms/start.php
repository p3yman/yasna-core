<?php

if (!function_exists("component")) {
    /**
     * build an instance of a component, extended from this class
     *
     * @param string|null $component_name
     *
     * @return \Modules\Forms\Services\FormComponent
     */
    function component(string $component_name = null)
    {
        if (!$component_name) {
            return \Modules\Forms\Services\FormComponent::class;
        }

        return \Modules\Forms\Services\FormComponent::builder($component_name);
    }
}



if (!function_exists("binder")) {
    /**
     * build an instance of a component, extended from this class
     *
     * @param string|null $component_name
     *
     * @return \Modules\Forms\Services\FormBinder
     */
    function binder()
    {
        return new \Modules\Forms\Services\FormBinder();
    }
}
