<?php

namespace Modules\Forms\Contracts;


interface ComponentInterface
{
    /**
     * get an array of available attributes to be passed to the component ([$key => $default_value] format)
     *
     * @return array
     */
    public function attributes();



    /**
     * get an array of required attributes that must be present in the component render-time
     *
     * @return array
     */
    public function requiredAttributes();
}
