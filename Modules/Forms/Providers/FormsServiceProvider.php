<?php

namespace Modules\Forms\Providers;

use Modules\Forms\Components\BadgeComponent;
use Modules\Forms\Components\CheckboxComponent;
use Modules\Forms\Components\CheckboxListComponent;
use Modules\Forms\Components\ColorPickerComponent;
use Modules\Forms\Components\DateComponent;
use Modules\Forms\Components\DateTimeComponent;
use Modules\Forms\Components\DividerComponent;
use Modules\Forms\Components\DropdownComponent;
use Modules\Forms\Components\EditorComponent;
use Modules\Forms\Components\EmailComponent;
use Modules\Forms\Components\GroupComponent;
use Modules\Forms\Components\HeadingComponent;
use Modules\Forms\Components\HiddenComponent;
use Modules\Forms\Components\ImageComponent;
use Modules\Forms\Components\LinkComponent;
use Modules\Forms\Components\LocalesComponent;
use Modules\Forms\Components\MapComponent;
use Modules\Forms\Components\NumberComponent;
use Modules\Forms\Components\PanelComponent;
use Modules\Forms\Components\ParagraphComponent;
use Modules\Forms\Components\PasswordComponent;
use Modules\Forms\Components\RadioComponent;
use Modules\Forms\Components\RadioListComponent;
use Modules\Forms\Components\RangeComponent;
use Modules\Forms\Components\RecaptchaComponent;
use Modules\Forms\Components\RowComponent;
use Modules\Forms\Components\SelectAdvancedComponent;
use Modules\Forms\Components\SelectComponent;
use Modules\Forms\Components\SlugComponent;
use Modules\Forms\Components\TextareaComponent;
use Modules\Forms\Components\TextComponent;
use Modules\Forms\Components\TimeComponent;
use Modules\Forms\Components\TitleComponent;
use Modules\Forms\Components\UrlComponent;
use Modules\Forms\Console\MakeComponentCommand;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class FormsServiceProvider
 *
 * @package Modules\Forms\Providers
 */
class FormsServiceProvider extends YasnaProvider
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerServices();
        $this->registerComponents();
        $this->registerArtisans();
    }



    /**
     * register services
     */
    private function registerServices()
    {
        module("forms")->register("components", "keep the list of registered form components");
    }



    /**
     * register components
     */
    private function registerComponents()
    {
        component()::register("panel", PanelComponent::class);
        component()::register("row", RowComponent::class);
        component()::register("group", GroupComponent::class);
        component()::register("text", TextComponent::class);
        component()::register("textarea", TextareaComponent::class);
        component()::register("checkbox", CheckboxComponent::class);
        component()::register("checkbox-list", CheckboxListComponent::class);
        component()::register("radio", RadioComponent::class);
        component()::register("color-picker", ColorPickerComponent::class);
        component()::register("date", DateComponent::class);
        component()::register("datetime", DateTimeComponent::class);
        component()::register("divider", DividerComponent::class);
        component()::register("editor", EditorComponent::class);
        component()::register("email", EmailComponent::class);
        component()::register("heading", HeadingComponent::class);
        component()::register("hidden", HiddenComponent::class);
        component()::register("map", MapComponent::class);
        component()::register("number", NumberComponent::class);
        component()::register("password", PasswordComponent::class);
        component()::register("range", RangeComponent::class);
        component()::register("select", SelectComponent::class);
        component()::register("time", TimeComponent::class);
        component()::register("url", UrlComponent::class);
        component()::register("link", LinkComponent::class);
        component()::register("badge", BadgeComponent::class);
        component()::register("title", TitleComponent::class);
        component()::register("paragraph", ParagraphComponent::class);
        component()::register("dropdown", DropdownComponent::class);
        component()::register("image", ImageComponent::class);
        component()::register("slug", SlugComponent::class);
        component()::register("locales", LocalesComponent::class);
        component()::register("recaptcha", RecaptchaComponent::class);
    }



    /**
     * register artisans
     *
     * @return void
     */
    public function registerArtisans()
    {
        $this->addArtisan(MakeComponentCommand::class);
    }
}
