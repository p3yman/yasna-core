<?php

return [
     'help-100' => 'The required help article doesn\'t exists.',
     'help-101' => 'Error on deleting the help article!',
     'help-102' => 'Error on updating the help article!',
     'help-103' => 'An error occurred on creating the help article.',
];
