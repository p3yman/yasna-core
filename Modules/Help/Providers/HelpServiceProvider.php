<?php

namespace Modules\Help\Providers;

use Modules\Help\Http\Endpoints\V1\DeleteEndpoint as DeleteEndpointV1;
use Modules\Help\Http\Endpoints\V1\ListEndpoint as ListEndpointV1;
use Modules\Help\Http\Endpoints\V1\SingleEndpoint as SingleEndpointV1;
use Modules\Help\Http\Endpoints\V1\UpdateEndpoint as UpdateEndpointV1;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class HelpServiceProvider
 *
 * @package Modules\Help\Providers
 */
class HelpServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
    }



    /**
     * register endpoints
     */
    private function registerEndpoints()
    {
        endpoint()->register(SingleEndpointV1::class);
        endpoint()->register(ListEndpointV1::class);
        endpoint()->register(UpdateEndpointV1::class);
        endpoint()->register(DeleteEndpointV1::class);
    }
}
