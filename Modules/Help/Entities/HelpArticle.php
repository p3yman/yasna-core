<?php

namespace Modules\Help\Entities;

use Modules\Uploader\Services\Entity\FilesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class HelpArticle
 *
 * @package Modules\Help\Entities
 * @property string         $slug
 * @property string         $title
 * @property string         $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class HelpArticle extends YasnaModel
{
    use SoftDeletes;
    use FilesTrait;
}
