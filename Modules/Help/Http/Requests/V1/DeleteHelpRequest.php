<?php

namespace Modules\Help\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class DeleteHelpRequest extends YasnaRequest
{
    protected $responder  = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'slug' => 'required|string',
        ];
    }
}
