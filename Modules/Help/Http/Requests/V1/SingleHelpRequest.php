<?php

namespace Modules\Help\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class SingleHelpRequest extends YasnaRequest
{
    protected $model_name = "help_article";
    protected $responder  = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'hashid' => 'required_without:slug|string',
             'slug'   => 'required_without:hashid|string',
        ];
    }



    /**
     * Tries to fetch with slug. If a record has been found it returns true, otherwise it returns false.
     *
     * @return bool
     */
    public function fetchModelWithSlug(): bool
    {
        $this->model = model('help_article', $this->input('slug', 0));

        return $this->model->exists;
    }
}
