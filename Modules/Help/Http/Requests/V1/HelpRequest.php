<?php

namespace Modules\Help\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class HelpRequest extends YasnaRequest
{
    protected $model_name = "help_article";
    protected $responder  = "white-house";

    private $files_hashid = [];



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'hashid'  => 'string',
             'slug'    => 'required|string|max:120',
             'title'   => 'required|string|max:120',
             'content' => 'string',
             'files'   => 'array|min:1',
             'files.*' => 'string',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if ($this->filled('files')) {
            $this->files_hashid = (array)$this->input('files', []);
            unset($this->data['files']);
        }
    }



    /**
     * Fetch model with the given slug.
     */
    public function fetchModelWithSlug()
    {
        $this->model = model($this->model_name, $this->input('slug', 0));
    }



    /**
     * Return attached files.
     *
     * @return array|null
     */
    public function fetchFiles(): array
    {
        return $this->files_hashid;
    }
}
