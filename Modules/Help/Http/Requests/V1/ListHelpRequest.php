<?php

namespace Modules\Help\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class ListHelpRequest extends YasnaRequest
{
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'paginated' => 'int|in:0,1',
             'per_page'  => 'int|min:1|max:50',
             'page'      => 'int|min:1',
        ];
    }
}
