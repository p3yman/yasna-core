<?php

namespace Modules\Help\Http\Controllers\V1;

use Modules\Help\Entities\HelpArticle;
use Modules\Help\Http\Requests\V1\HelpRequest;
use Modules\Yasna\Services\YasnaApiController;

class HelpController extends YasnaApiController
{
    /**
     * Create/Update an help article.
     *
     * @param HelpRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(HelpRequest $request)
    {
        // if request can automatically fetch the model, just update it
        if (
             $request->model->exists or
             (
                  $request->fetchModelWithSlug() and $request->model->exists
             )
        ) {
            return $this->updateHelpArticleModel($request);
        }

        // otherwise create the resource
        return $this->createHelpArticleModel($request);
    }



    /**
     * Update an help article.
     *
     * @param HelpRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    private function updateHelpArticleModel(HelpRequest $request)
    {
        $response = $this->saveModel($request);
        if (!empty($response)) {
            return $response;
        }

        return $this->clientError('help-102');
    }



    /**
     * Create a help article.
     *
     * @param HelpRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    private function createHelpArticleModel(HelpRequest $request)
    {
        $response = $this->saveModel($request);
        if (!empty($response)) {
            return $response;
        }

        return $this->clientError('help-103');
    }



    /**
     * Save a model with values from a given request.
     *
     * @param HelpRequest $request
     *
     * @return array
     */
    private function saveModel(HelpRequest $request): array
    {
        $model = $request->model->batchSave($request);
        if ($model->exists) {
            if (($files = $request->fetchFiles()) != null) {
                $this->attachFiles($model, $files);
            }
            $this->detachFiles($model, $files);

            return $this->success($this->responseModel($model));
        }

        return [];
    }



    /**
     * Generate a response suitable array.
     *
     * @param HelpArticle $model
     *
     * @return array
     */
    private function responseModel(HelpArticle $model): array
    {
        $arr = [
             'id'         => $model->hashid,
             'slug'       => $model->slug,
             'title'      => $model->title,
             'content'    => $model->content,
             'files'      => $this->getFiles($model),
             'created_at' => $model->created_at->timestamp,
        ];

        if ($model->updated_at) {
            $arr['updated_at'] = $model->updated_at->timestamp;
        }

        return $arr;
    }



    /**
     * Return list files for a given HelpArticle model.
     *
     * @param HelpArticle $model
     *
     * @return array
     */
    private function getFiles(HelpArticle $model): array
    {
        if (is_null($model->files)) {
            return [];
        }

        // to have the latest changes, re-fetch files.
        return $model->files()
                     ->get()
                     ->map(function ($file) {
                         return $file->apiArray();
                     })
                     ->toArray()
             ;
    }



    /**
     * Attach uploaded files to a given help article.
     *
     * @param HelpArticle $model
     * @param array       $files
     */
    private function attachFiles(HelpArticle $model, array $files)
    {
        // attach new attachments
        foreach ($files as $file) {
            uploader()->file($file)->attachTo($model);
        }
    }



    /**
     * Detach uploaded files from a given help article.
     *
     * @param HelpArticle $model
     * @param array       $files
     */
    private function detachFiles(HelpArticle $model, array $files)
    {
        // detach removed attachments
        $removed_files = $this->getRemovedAttachments($model, $files);
        foreach ($removed_files as $file) {
            uploader()->file($file)->detachFromEntity();
        }
    }



    /**
     * Get removed files.
     *
     * @param HelpArticle $model
     * @param array       $files
     *
     * @return array
     */
    private function getRemovedAttachments(HelpArticle $model, array $files): array
    {
        if (is_null($model->files)) {
            return [];
        }

        $current = $model->files
             ->map(function ($file) {
                 return $file->hashid;
             })
             ->toArray()
        ;

        return array_diff($current, $files);
    }
}
