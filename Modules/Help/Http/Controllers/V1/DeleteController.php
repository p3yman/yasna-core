<?php

namespace Modules\Help\Http\Controllers\V1;

use Modules\Help\Http\Requests\V1\DeleteHelpRequest;
use Modules\Yasna\Services\YasnaApiController;

class DeleteController extends YasnaApiController
{
    /**
     * Delete a given article.
     *
     * @param DeleteHelpRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(DeleteHelpRequest $request)
    {
        $model = model('help_article', $request->input('slug'));
        if (!$model->exists) {
            return $this->clientError('help-100');
        }

        if ($model->delete()) {
            return $this->success(["done" => true]);
        }

        return $this->clientError('help-101');
    }
}
