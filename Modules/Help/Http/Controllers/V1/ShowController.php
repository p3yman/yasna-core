<?php

namespace Modules\Help\Http\Controllers\V1;

use Illuminate\Database\Eloquent\Collection;
use Modules\Help\Entities\HelpArticle;
use Modules\Help\Http\Requests\V1\ListHelpRequest;
use Modules\Help\Http\Requests\V1\SingleHelpRequest;
use Modules\Yasna\Services\YasnaApiController;

class ShowController extends YasnaApiController
{
    /**
     * List all help articles.
     *
     * @param ListHelpRequest $request
     *
     * @return array
     */
    public function list(ListHelpRequest $request)
    {
        $models = model('help_article');

        if ($request->isPaginated()) {
            $paginated = $models->paginate($request->perPage());
            $meta      = $this->paginatorMetadata($paginated);

            return $this->success($this->listViewOfArticles($paginated), $meta);
        }

        $results = $models->get();

        return $this->success($this->listViewOfArticles($results));
    }



    /**
     * Get a single view of a requested help article.
     *
     * @param SingleHelpRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function single(SingleHelpRequest $request)
    {
        if ($request->model->exists or $request->fetchModelWithSlug()) {
            return $this->success($this->detailViewOfArticle($request->model));
        }

        return $this->clientError('help-100');
    }



    /**
     * Return array of list view suitable articles.
     *
     * @param Collection $articles
     *
     * @return array
     */
    private function listViewOfArticles($articles): array
    {
        return $articles->map(function ($article) {
            return $this->listViewOfArticle($article);
        })
                        ->toArray()
             ;
    }



    /**
     * Return a list suitable of help articles.
     *
     * @param HelpArticle $article
     *
     * @return array
     */
    private function listViewOfArticle(HelpArticle $article): array
    {
        return [
             'hashid'     => $article->hashid,
             'slug'       => $article->slug,
             'title'      => $article->title,
             'created_at' => $article->created_at->timestamp,
             'updated_at' => $article->updated_at->timestamp,
        ];
    }



    /**
     * Return a detail view of help article.
     *
     * @param HelpArticle $article
     *
     * @return array
     */
    private function detailViewOfArticle(HelpArticle $article): array
    {
        return [
             'hashid'     => $article->hashid,
             'slug'       => $article->slug,
             'title'      => $article->title,
             'content'    => $article->content,
             'files'      => $this->getFiles($article),
             'created_at' => $article->created_at->timestamp,
             'updated_at' => $article->updated_at->timestamp,
        ];
    }



    /**
     * Return list of files of a given help article.
     *
     * @param HelpArticle $article
     *
     * @return array
     */
    private function getFiles(HelpArticle $article): array
    {
        return $article->files
             ->map(function ($file) {
                 return $file->apiArray();
             })
             ->toArray()
             ;
    }
}
