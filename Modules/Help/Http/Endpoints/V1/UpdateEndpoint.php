<?php

namespace Modules\Help\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/help-update
 *                    Create or Update a help article
 * @apiDescription    Create or update a help article. If given slug exists try to update, otherwise create a new one.
 * @apiVersion        1.0.0
 * @apiName           Update a help
 * @apiGroup          Help
 * @apiPermission     Developer
 * @apiParam {String}   hashid Hashid of content which must be updated. On update if we first use this parameter for
 *           detect DB record and if that was absent, we use `slug`. So at least on updating slug, it's necessary to
 *           put this argument on your request.
 * @apiParam {String}   slug Slug of the help article.
 * @apiParam {String}   title Title of the help article.
 * @apiParam {String}   content Content of the help article.
 * @apiParam {String[]} files   Array of Hashid of uploaded files.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": {
 *          "id": "QKgJx",
 *          "slug": "Meysam",
 *          "title": "سلام چطوری؟",
 *          "content": "آره خوبمممم.",
 *          "files": [
 *              {
 *                  "hashid": "QKgJx",
 *                  "link":
 *                  "http://ycms.mpg/uploader/file/QKgJx/original/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i.jpg",
 *                  "versions": {
 *                      "thumb":
 *                      "http://ycms.mpg/uploader/file/QKgJx/thumb/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i-thumb.jpg"
 *                  }
 *              }
 *          ],
 *          "created_at": 1543395680,
 *          "updated_at": 1543395753
 * }
 * @apiErrorExample   Unauthorized access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 */
class UpdateEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Update a help";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Help\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'HelpController@index';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "status"   => 200,
             "metadata" => null,
             "results"  => [
                  "id"         => "QKgJx",
                  "slug"       => "Meysam",
                  "title"      => "سلام چطوری؟",
                  "content"    => "آره خوبمممم.",
                  "files"      => [
                       [
                            "hashid"   => "QKgJx",
                            "link"     => "http://ycms.mpg/uploader/file/QKgJx/original/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i.jpg",
                            "versions" => [
                                 "thumb" => "http://ycms.mpg/uploader/file/QKgJx/thumb/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i-thumb.jpg",
                            ],
                       ],
                  ],
                  "created_at" => 1543395680,
                  "updated_at" => 1543395753,
             ],
        ]);
    }
}
