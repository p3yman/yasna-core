<?php

namespace Modules\Help\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/help-single
 *                    Help article single view
 * @apiDescription    Get a single view of a help article
 * @apiVersion        1.0.0
 * @apiName           Single
 * @apiGroup          Help
 * @apiPermission     User
 * @apiParam {String} hashid Hashid value of the help article.
 * @apiParam {String} slug Slug of the help article. It's required if no `hashid` has been set on the request.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": {
 *      "hashid": "oxljN",
 *      "slug": "Meysaa",
 *      "title": "سلام چطوری؟",
 *      "content": "آره خوبمممم.",
 *      "files": [
 *          {
 *              "hashid": "QKgJx",
 *              "link":
 *              "http://ycms.mpg/uploader/file/QKgJx/original/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i.jpg",
 *              "versions": {
 *                  "thumb":
 *                  "http://ycms.mpg/uploader/file/QKgJx/thumb/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i-thumb.jpg"
 *              }
 *          }
 *      ],
 *      "created_at": 1543400240,
 *      "updated_at": 1543403494
 * }
 * @apiErrorExample   Unauthorized access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 */
class SingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Help Single";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Help\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ShowController@single';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "hashid"     => "oxljN",
             "slug"       => "Meysaa",
             "title"      => "سلام چطوری؟",
             "content"    => "آره خوبمممم.",
             "files"      => [
                  [
                       "hashid"   => "QKgJx",
                       "link"     => "http://ycms.mpg/uploader/file/QKgJx/original/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i.jpg",
                       "versions" => [
                            "thumb" => "http://ycms.mpg/uploader/file/QKgJx/thumb/1543397452_wrHXZZwVZUWDaDeUlhy6KqHFJnEvp232e1mTJC1i-thumb.jpg",
                       ],
                  ],
             ],
             "created_at" => 1543400240,
             "updated_at" => 1543403494,
        ]);
    }
}
