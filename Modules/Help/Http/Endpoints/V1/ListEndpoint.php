<?php

namespace Modules\Help\Http\Endpoints\V1;

use Carbon\Carbon;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/help-list
 *                    List of help articles
 * @apiDescription    List of all help articles.
 * @apiVersion        1.0.0
 * @apiName           List of help articles
 * @apiGroup          Help
 * @apiPermission     Developer
 * @apiParam {int=0,1}  [paginated=0] Set the result must be paginated or not.
 * @apiParam {int} [per_page=15] Set the number of results on each request (Only available with `paginated`).
 * @apiParam {int} [page=1] Set the page number on pagination (Only available with `paginated`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": [
 *          {
 *              "hashid": "QKgJx",
 *              "slug": "Meysam",
 *              "title": "سلام چطوری؟",
 *              "created_at": 1543395680,
 *              "updated_at": 1543395753
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthorized access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List of helps";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Help\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ShowController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {

        $response = [];

        for ($i = 0; $i < 10; $i++) {
            $response[] = $this->mockHelpBuilder(
                 rand(),
                 dummy()::persianWord(rand(3, 7)),
                 str_slug(dummy()::englishWord(rand(3, 7))),
                 Carbon::now()->subDays(rand(4, 7))->timestamp,
                 Carbon::now()->subDays(rand(1, 3))->timestamp
            );
        }

        return api()->successRespond($response);
    }



    /**
     * Create a mock help sample
     *
     * @param mixed $id
     * @param mixed $title
     * @param mixed $slug
     * @param int   $created_at
     * @param int   $updated_at
     *
     * @return array
     */
    private function mockHelpBuilder($id, $title, $slug, $created_at, $updated_at)
    {
        return [
             "id"         => $id,
             "title"      => $title,
             "slug"       => $slug,
             "created_at" => $created_at,
             "updated_at" => $updated_at,
        ];
    }
}
