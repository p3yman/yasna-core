<?php

namespace Modules\Help\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {Delete}
 *                    /api/modular/v1/help-delete
 *                    Delete a help
 * @apiDescription    Delete a requested article.
 * @apiVersion        1.0.0
 * @apiName           Delete a help article
 * @apiGroup          Help
 * @apiPermission     Developer
 * @apiParam {String} hashid Hashid of an article which must be deleted.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": {
 *          "done": true
 *      }
 * }
 * @apiErrorExample   Unauthorized access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   Error on deleting:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "help::developerMessages.help-101",
 *      "userMessage": "Error on deleting the help article!",
 *      "errorCode": "help-101",
 *      "moreInfo": "help.moreInfo.help-101",
 *      "errors": []
 * }
 * @apiErrorExample   Article not exist:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "help::developerMessages.help-100",
 *      "userMessage": "The required help article doesn't exists.",
 *      "errorCode": "help-100",
 *      "moreInfo": "help.moreInfo.help-100",
 *      "errors": []
 * }
 */
class DeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Delete a help article";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Help\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'DeleteController@delete';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => true,
        ]);
    }
}
