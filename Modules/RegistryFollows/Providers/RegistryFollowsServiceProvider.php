<?php

namespace Modules\RegistryFollows\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class RegistryFollowsServiceProvider
 *
 * @package Modules\RegistryFollows\Providers
 */
class RegistryFollowsServiceProvider extends YasnaProvider
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerModelTraits();
    }



    /**
     * register ModelTraits
     *
     * @return void
     */
    private function registerModelTraits()
    {
        $this->addModelTrait("SickFollowsTrait", "RegistrySick");
    }

}
