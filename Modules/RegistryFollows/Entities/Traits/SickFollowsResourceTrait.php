<?php

namespace Modules\RegistryFollows\Entities\Traits;

use Carbon\Carbon;

// TODO: electors for filter by file and receipt numbers are to be added, once the criteria is advised.

trait SickFollowsResourceTrait
{
    /**
     * define liver_admitted_at elector to filter data entered after a certain date in the liver waiting list.
     *
     * @param int|string|Carbon $value
     *
     * @return void
     */
    protected function electorLiverAdmittedAfter($value): void
    {
        if (is_int($value)) {
            $value = carbon()::createFromTimestamp($value);
        }

        $this->elector()->whereDate("liver_admitted_at", ">=", $value);
    }



    /**
     * define liver_admitted_at elector to filter data entered before a certain date in the liver waiting list.
     *
     * @param int|string|Carbon $value
     *
     * @return void
     */
    protected function electorLiverAdmittedBefore($value): void
    {
        if (is_int($value)) {
            $value = carbon()::createFromTimestamp($value);
        }

        $this->elector()->whereDate("liver_admitted_at", "<=", $value);
    }



    /**
     * define liver_received_at elector to filter data entered after a certain date in the liver waiting list.
     *
     * @param int|string|Carbon $value
     *
     * @return void
     */
    protected function electorLiverReceivedAfter($value): void
    {
        if (is_int($value)) {
            $value = carbon()::createFromTimestamp($value);
        }

        $this->elector()->whereDate("liver_received_at", ">=", $value);
    }



    /**
     * define liver_received_at elector to filter data entered before a certain date in the liver waiting list.
     *
     * @param int|string|Carbon $value
     *
     * @return void
     */
    protected function electorLiverReceivedBefore($value): void
    {
        if (is_int($value)) {
            $value = carbon()::createFromTimestamp($value);
        }

        $this->elector()->whereDate("liver_received_at", "<=", $value);
    }

}
