<?php

return [
     'liver-surgery-type' => [
          "whole"     => "Whole Liver",
          "partial"   => "Partial Liver, Remainder not tx or Living tx",
          "split"     => "Split Liver",
          "whole-p"   => "Whole Liver with Pancreas",
          "partial-p" => "Partial Liver with Pancreas",
          "split-p"   => "Split Liver with Pancreas",
     ],

     'liver-solution-type' => [
          "uw"       => "UW",
          "custodil" => "کاستادیول",
          "others"   => "سایر",
     ],

     'liver-se' => [
          "arrhythmia"  => "آریتمی",
          "bleeding"    => "خونریزی",
          "electrolyte" => "اختلال الکترولیتی",
          "perfusion"   => "سندروم ری پرفیوژن",
          "hypoxia"     => "هیپوکسی طولانی",
          "hypotension" => "هیپوتانسیون طولانی",
          "hf"          => "ایست قلبی",
          "func"        => "عدم کارکرد اولیه ارگان",
          "hp"          => "نارسایی قلب",
     ],

     "liver-se-treat" => [
          "medicine" => "دارویی",
          "surgery"  => "جراحی",
     ],

     "liver-parenchyma" => [
          "normal"    => "نرمال",
          "abnormal"  => "غیرنرمال",
          "cirrhosis" => "سیروز",
     ],

     'spleen-status' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
     ],

     'thrombosis-dev' => [
          "smv"     => "SMV",
          "splenic" => "Splenic Vein",
     ],

     'thrombosis-current' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
     ],

     'thrombosis-loc' => [
          "major" => "Mailn Branch Major",
          "intra" => "Intrahepatic Branch",
          "rl"    => "Right or Left Branch",
     ],

     'liver-artery' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
          "ni"       => "مشاهده نشد",
     ],

     'liver-vein' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
          "ni"       => "مشاهده نشد",
     ],

     'liver-inferior' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
          "ni"       => "مشاهده نشد",
     ],

     'bile-status' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
     ],

     'pancreas-status' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
          "ni"       => "مشاهده نشد",
     ],

     'kidney-status' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
          "ni"       => "مشاهده نشد",
     ],

     'adrenal-glands' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
          "ni"       => "مشاهده نشد",
     ],

     'lymphadenopathy' => [
          "normal"   => "نرمال",
          "abnormal" => "غیرنرمال",
          "ni"       => "مشاهده نشد",
     ],

     'liver-tumor-cons' => [
          "homogeneous"  => "هموژن",
          "heterozygote" => "هتروژن",
     ],

     'liver-tumor-loc' => [
          "1"  => "سگمان ۱",
          "2"  => "سگمان ۲",
          "3"  => "سگمان ۳",
          "4a" => "سگمان ۴آ",
          "4b" => "سگمان ۴ب",
          "5"  => "سگمان ۵",
          "6"  => "سگمان ۶",
          "7"  => "سگمان ۷",
          "8"  => "سگمان ۸",
     ],

     'cyclosporine' => [
          "neural"   => "نئورال",
          "iminoral" => "ایمینورال",
          "others"   => "سایر",
     ],

     'tacrolimus' => [
          "pro"    => "پروگراف",
          "co"     => "کوگراف",
          "others" => "سایر",
     ],

     'mycophenolate' => [
          "cellcept"     => "سلسپت",
          "mycophenolic" => "مایفورتیک",
          "suprimon"     => "سوپریمون",
          "others"       => "سایر",
     ],

     'immun' => [
          "me"     => "متیل پردنیزولون",
          "cy"     => "سیکلوسپورین",
          "ta"     => "تاکرولیموس",
          "mi"     => "مایکوفنولات",
          "atg"    => "ATG",
          "il"     => "Il-2 Receptor Inhibitor",
          "others" => "سایر",
     ],
];
