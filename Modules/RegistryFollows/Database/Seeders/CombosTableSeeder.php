<?php

namespace Modules\RegistryFollows\Database\Seeders;

use Illuminate\Database\Seeder;

class CombosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = trans("registry-follows::combo");

        foreach ($list as $subject => $array) {
            $this->seedArray($subject, $array);
        }
    }



    /**
     * seed the array
     *
     * @param string $subject
     * @param array  $array
     *
     * @return void
     */
    private function seedArray(string $subject, array $array)
    {
        $order = 1;

        foreach ($array as $key => $title) {
            combo()::seed($subject, $key, $title, $order);
            $order++;
        }
    }
}
