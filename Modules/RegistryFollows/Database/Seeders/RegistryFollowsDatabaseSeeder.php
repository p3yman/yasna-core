<?php

namespace Modules\RegistryFollows\Database\Seeders;

use Illuminate\Database\Seeder;

class RegistryFollowsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CombosTableSeeder::class);
    }
}
