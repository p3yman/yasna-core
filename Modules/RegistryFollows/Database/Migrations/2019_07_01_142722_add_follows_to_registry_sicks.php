<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFollowsToRegistrySicks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registry_sicks', function (Blueprint $table) {
            $this->generalInfo($table);
            $this->medicalInfo($table);
            $this->transInfo($table);
            $this->sideEffectsInfo($table);
            $this->clinicalFunctionInfo($table);
            $this->testsInfo($table);
            $this->ultrasonicInfo($table);
            $this->drugsInfo($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Let's talk about love!
    }



    /**
     * migrate general info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function generalInfo(Blueprint $table)
    {
        $table->unsignedInteger("liver_doctor_id")->default(0);
        $table->unsignedInteger("liver_surgeon_id")->default(0);
        $table->unsignedInteger("liver_received_id")->default(0);
        $table->string("liver_received_manual_code")->nullable();
    }



    /**
     * migrate medicalInfo
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function medicalInfo(Blueprint $table)
    {
        $table->timestamp("liver_admitted_at")->nullable();
        $table->timestamp("liver_discharged_at")->nullable();
    }



    /**
     * migrate trans info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function transInfo(Blueprint $table)
    {
        $table->string("liver_surgery_type", 50)->nullable();
        $table->mediumText("liver_split_type")->nullable();
        $table->string("liver_solution_type", 50)->nullable();
        $table->integer("liver_solution_cc")->nullable();
        $table->mediumText("liver_solution_other")->nullable();

        $table->timestamp("liver_aortic_clamped_at")->nullable();
        $table->timestamp("liver_deiced_at")->nullable();
        $table->timestamp("liver_anastomed_at")->nullable();
        $table->timestamp("liver_perfused_at")->nullable();

        $table->boolean("liver_used_extra_vessels")->default(0);
        $table->mediumText("liver_vessel_type")->nullable();
    }



    /**
     * migrate side effects
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function sideEffectsInfo(Blueprint $table)
    {
        $table->boolean("liver_se_arrhythmia")->default(0);
        $table->string("liver_se_arrhythmia_treat", 40)->nullable();
        $table->mediumText("liver_se_arrhythmia_text")->nullable();

        $table->boolean("liver_se_bleeding")->default(0);
        $table->string("liver_se_bleeding_treat", 40)->nullable();
        $table->mediumText("liver_se_bleeding_text")->nullable();

        $table->boolean("liver_se_electrolyte")->default(0);
        $table->string("liver_se_electrolyte_treat", 40)->nullable();
        $table->mediumText("liver_se_electrolyte_text")->nullable();

        $table->boolean("liver_se_perfusion")->default(0);
        $table->string("liver_se_perfusion_treat", 40)->nullable();
        $table->mediumText("liver_se_perfusion_text")->nullable();

        $table->boolean("liver_se_hypoxia")->default(0);
        $table->string("liver_se_hypoxia_treat", 40)->nullable();
        $table->mediumText("liver_se_hypoxia_text")->nullable();

        $table->boolean("liver_se_hypotension")->default(0);
        $table->string("liver_se_hypotension_treat", 40)->nullable();
        $table->mediumText("liver_se_hypotension_text")->nullable();

        $table->boolean("liver_se_hf")->default(0);
        $table->string("liver_se_hf_treat", 40)->nullable();
        $table->mediumText("liver_se_hf_text")->nullable();

        $table->boolean("liver_se_func")->default(0);
        $table->string("liver_se_func_treat", 40)->nullable();
        $table->mediumText("liver_se_func_text")->nullable();

        $table->boolean("liver_se_hp")->default(0);
        $table->string("liver_se_hp_treat", 40)->nullable();
        $table->mediumText("liver_se_hp_text")->nullable();
        $table->string("liver_se_hp_severity")->nullable();
        $table->string("liver_se_hp_arrangement")->nullable();

        $table->text("liver_se_other")->nullable();
    }



    /**
     * migrate clinical function info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function clinicalFunctionInfo(Blueprint $table)
    {
        $table->boolean("liver_failed")->default(0);
        $table->text("liver_failed_details")->nullable();
        $table->text("liver_trans_problems")->nullable();
    }



    /**
     * migrate tests
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function testsInfo(Blueprint $table)
    {
        $table->float("test_bili_d")->nullable();
        $table->float("test_alt")->nullable();
        $table->float("test_ast")->nullable();
        $table->float("test_cbc")->nullable();
        $table->float("test_alk_p")->nullable();
        $table->float("test_pt")->nullable();
        $table->float("test_bun")->nullable();
        $table->float("test_sr")->nullable();

        $table->boolean("hbc_ab_has")->default(0);
        $table->boolean("hbc_ab_need")->default(0);
        $table->mediumText("hbc_ab_plan")->nullable();

        $table->boolean("hbs_ag_has")->default(0);
        $table->boolean("hbs_ag_need")->default(0);
        $table->mediumText("hbs_ag_plan")->nullable();

        $table->boolean("hbs_ab_has")->default(0);
        $table->boolean("hbs_ab_need")->default(0);
        $table->mediumText("hbs_ab_plan")->nullable();

        $table->boolean("hcv_ab_has")->default(0);
        $table->boolean("hcv_ab_need")->default(0);
        $table->mediumText("hcv_ab_plan")->nullable();

        $table->boolean("hiv_ab_has")->default(0);
        $table->boolean("hiv_ab_need")->default(0);
        $table->mediumText("hiv_ab_plan")->nullable();

        $table->boolean("hcv_nat_has")->default(0);
        $table->boolean("hcv_nat_need")->default(0);
        $table->mediumText("hcv_nat_plan")->nullable();

        $table->boolean("hiv_nat_has")->default(0);
        $table->boolean("hiv_nat_need")->default(0);
        $table->mediumText("hiv_nat_plan")->nullable();

        $table->boolean("hbv_nat_has")->default(0);
        $table->boolean("hbv_nat_need")->default(0);
        $table->mediumText("hbv_nat_plan")->nullable();

        $table->boolean("cmb_igm_has")->default(0);
        $table->boolean("cmb_igm_need")->default(0);
        $table->mediumText("cmb_igm_plan")->nullable();

        $table->boolean("cmb_igg_has")->default(0);
        $table->boolean("cmb_igg_need")->default(0);
        $table->mediumText("cmb_igg_plan")->nullable();

        $table->boolean("ebv_igm_has")->default(0);
        $table->boolean("ebv_igm_need")->default(0);
        $table->mediumText("ebv_igm_plan")->nullable();

        $table->boolean("ebv_igg_has")->default(0);
        $table->boolean("ebv_igg_need")->default(0);
        $table->mediumText("ebv_igg_plan")->nullable();
    }



    /**
     * migrate ultrasonic info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function ultrasonicInfo(Blueprint $table)
    {
        $table->string("liver_size", 30)->nullable();
        $table->float("liver_mm")->nullable();
        $table->string("liver_parenchyma", 30)->nullable();
        $table->mediumText("liver_parenchyma_desc")->nullable();

        $table->float("spleen_mm")->nullable();
        $table->string("spleen_status", 30)->nullable();
        $table->mediumText("spleen_desc")->nullable();

        $table->boolean("thrombosis")->nullable();
        $table->float("thrombosis_mm")->nullable();
        $table->float("thrombosis_speed")->nullable();
        $table->string("thrombosis_dev", 30)->nullable();
        $table->string("thrombosis_current", 30)->nullable();
        $table->string("thrombosis_loc", 30)->nullable();

        $table->string("liver_artery", 30)->nullable();
        $table->mediumText("liver_artery_text")->nullable();
        $table->string("liver_vein", 30)->nullable();
        $table->mediumText("liver_vein_text")->nullable();
        $table->string("liver_inferior", 30)->nullable();
        $table->mediumText("liver_inferior_text")->nullable();

        $table->boolean("collateral")->nullable();
        $table->string("collateral_loc")->nullable();

        $table->float("bile_dia")->nullable();
        $table->string("bile_status", 30)->nullable();
        $table->mediumText("bile_text")->nullable();

        $table->boolean("gallbladder")->nullable();
        $table->boolean("gallstone")->nullable();
        $table->boolean("biliary_slip")->nullable();

        $table->boolean("ascites")->nullable();
        $table->string("ascites-status", 30)->nullable();

        $table->string("pancreas_status")->nullable();
        $table->mediumText("pancreas_index")->nullable();

        $table->string("adrenal_glands", 30)->nullable();
        $table->mediumText("adrenal_glands_text")->nullable();

        $table->string("lymphadenopathy", 30)->nullable();
        $table->mediumText("lymphadenopathy_text")->nullable();

        $table->string("kidney_right_status", 30)->nullable();
        $table->float("kidney_right_mm", 30)->nullable();
        $table->mediumText("kidney_right_text")->nullable();
        $table->string("kidney_left_status", 30)->nullable();
        $table->float("kidney_left_mm", 30)->nullable();
        $table->mediumText("kidney_left_text")->nullable();

        $table->boolean("liver_tumor")->nullable();
        $table->string("liver_tumor_cons", 35)->nullable();
        $table->tinyInteger("liver_tumor_count")->nullable();
        $table->float("liver_tumor_mm")->nullable();
        $table->string("liver_tumor_loc", 35)->nullable();

        $table->text("liver_ultrasonic_text")->nullable();
    }



    /**
     * migrate Drugs
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function drugsInfo(Blueprint $table)
    {
        $table->string("cyclosporine", 30)->nullable();
        $table->mediumText("cyclosporine_dose")->nullable();
        $table->mediumText("cyclosporine_other")->nullable();

        $table->string("tacrolimus", 20)->nullable();
        $table->mediumText("tacrolimus_dose")->nullable();
        $table->mediumText("tacrolimus_other")->nullable();

        $table->mediumText("sirolimus_dose")->nullable();
        $table->mediumText("everolimus_dose")->nullable();
        $table->mediumText("prednisolone_dose")->nullable();

        $table->string("mycophenolate", 30)->nullable();
        $table->mediumText("mycophenolate_dose")->nullable();
        $table->mediumText("mycophenolate_other")->nullable();

        $table->string("immun", 20)->nullable();
        $table->mediumText("immun_other")->nullable();
        $table->mediumText("immun_me_dose")->nullable();
        $table->timestamp("immun_me_at")->nullable();
        $table->mediumText("immun_cy_dose")->nullable();
        $table->timestamp("immun_cy_at")->nullable();
        $table->mediumText("immun_ta_dose")->nullable();
        $table->timestamp("immun_ta_at")->nullable();
        $table->mediumText("immun_mi_dose")->nullable();
        $table->timestamp("immun_mi_at")->nullable();
        $table->mediumText("immun_atg_dose")->nullable();
        $table->timestamp("immun_atg_at")->nullable();
        $table->mediumText("immun_il_dose")->nullable();
        $table->timestamp("immun_il_at")->nullable();
    }

}
