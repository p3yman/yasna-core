<?php

namespace Modules\Socialite\Providers;

use Modules\Socialite\Http\Middleware\CheckProviderMiddleware;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class SocialiteServiceProvider
 *
 * @package Modules\Socialite\Providers
 */
class SocialiteServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerMiddlewares();
        $this->registerServices();
    }



    /**
     * Registers middlewares.
     */
    protected function registerMiddlewares()
    {
        $this->addMiddleware('check-provider', CheckProviderMiddleware::class);
    }



    /**
     * Registers services.
     */
    protected function registerServices()
    {
        module($this->moduleName())
             ->register(socialite()->providersService(), "Providers which will be used.")
             ->register(socialite()->callbackService(), "Methods, to be executed after the OAuth authentication.")
        ;
    }



    /**
     * @inheritdoc
     */
    public function afterBoot()
    {
        $this->fillConfigs();
    }



    /**
     * Fills configs.
     */
    protected function fillConfigs()
    {
        $this->fillCallbacksInConfig();
        $this->fillGeneralServicesConfigs();
    }



    /**
     * Fills the callback urls of services.
     */
    protected function fillCallbacksInConfig()
    {
        $config_key = 'services';
        $services   = $this->module()->getConfig($config_key);

        foreach ($services as $provider => $data) {
            if (!array_key_exists('redirect', $data) or $data['redirect']) {
                continue;
            }

            $this->module()->setConfig([
                 "$config_key.$provider.redirect" => route('socialite.oauth.callback', [
                      'provider' => $provider,
                 ]),
            ])
            ;
        }
    }



    /**
     * Fills the general services config with the services config from this module.
     */
    protected function fillGeneralServicesConfigs()
    {
        $services = $this->module()->getConfig('services');

        foreach ($services as $provider => $data) {
            config(["services.$provider" => $data]);
        }
    }
}
