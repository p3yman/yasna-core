<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 11:14 AM
 */

namespace Modules\Socialite\Services;

use Modules\Socialite\Services\Traits\SocialiteCallbackTrait;
use Modules\Socialite\Services\Traits\SocialiteModuleTrait;
use Modules\Socialite\Services\Traits\SocialiteProvidersTrait;

/**
 * Class Socialite
 * _This class could be used to access features of the `Socialite` module._
 *
 * @package Modules\Socialite\Services
 */
class Socialite
{
    use SocialiteModuleTrait;
    use SocialiteProvidersTrait;
    use SocialiteCallbackTrait;

}
