<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 11:56 AM
 */

namespace Modules\Socialite\Services\Structure;

use Laravel\Socialite\Two\User;

/**
 * <h1>Interface ProviderInterface</h1>
 * <p>
 * All providers' classes should implement this interface.
 * </p>
 *
 * @package Modules\Socialite\Services\Structure
 */
interface ProviderInterface
{
    /**
     * Does the callback based on the provider with the given socialite user.
     *
     * @param User $user
     *
     * @return mixed
     */
    public function callback(User $user);



    /**
     * Returns the name of the provider in the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public function nameIn(string $locale): string;



    /**
     * Returns the name of the provider in the current locale.
     *
     * @return string
     */
    public function name(): string;
}
