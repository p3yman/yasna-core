<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 11:55 AM
 */

namespace Modules\Socialite\Services\Structure;

use App\Models\User;
use Exception;
use Laravel\Socialite\One\User as SocialiteUser1;
use Laravel\Socialite\Two\User as SocialiteUser2;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

/**
 * <h1>Class ProviderAbstract</h1>
 * <p>
 * All providers' classes should extend this class.
 * </p>
 *
 * @package Modules\Socialite\Services\Structure
 */
abstract class ProviderAbstract implements ProviderInterface
{
    use ModuleRecognitionsTrait;



    /**
     * Finds the User model from the database based on the given socialite user.
     *
     * @param SocialiteUser1|SocialiteUser2 $user
     *
     * @return User
     */
    public function findUserModelWithSocialiteModel($user)
    {
        $username_field = user()->usernameField();

        try {
            $has_username = boolval($user->$username_field);
        } catch (Exception $exception) {
            $has_username = false;
        }

        if ($has_username) {
            return User::firstOrNew([
                 $username_field => $user->$username_field,
            ]);
        } else {
            return model('user');
        }
    }



    /**
     * Does the default callback for providers.
     *
     * @param SocialiteUser1|SocialiteUser2 $user
     *
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function callback($user)
    {
        $user_model = $this->findUserModelWithSocialiteModel($user);

        $user_model->save();

        auth()->login($user_model);

        return redirect()->route('home');
    }



    /**
     * Returns the name of the provider in the current locale.
     *
     * @return string
     */
    public function name(): string
    {
        return $this->nameIn(getLocale());
    }
}
