<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 11:30 AM
 */

namespace Modules\Socialite\Services\Traits;


use Modules\Socialite\Exceptions\UndefinedProviderException;
use Modules\Socialite\Services\Structure\ProviderInterface;

trait SocialiteProvidersTrait
{
    /**
     * Returns the providers service's name.
     *
     * @return string
     */
    public static function providersServiceName()
    {
        return 'using_providers';
    }



    /**
     * Returns the providers service's name.
     *
     * @return string
     */
    public static function providersService()
    {
        return static::providersServiceName();
    }



    /**
     * Makes the socialite module to use the given provider.
     *
     * @param string $provider
     *
     * @throws UndefinedProviderException
     */
    public static function useProvider(string $provider)
    {
        if (socialite()->providersIsNotDefined($provider)) {
            throw new UndefinedProviderException($provider);
        }


        $order = static::providersServiceInstance()->count() + 1;

        module(static::moduleName())
             ->service(socialite()->providersService())
             ->add($provider)
             ->class($provider)
             ->order($order)
        ;
    }



    /**
     * Returns the providers service's identifier.
     *
     * @return string
     */
    protected static function providersServiceIdentifier()
    {
        return static::moduleName() . ":" . socialite()->providersService();
    }



    /**
     * Returns the providers service's instance.
     *
     * @return \Modules\Yasna\Services\ModuleHelper
     */
    protected static function providersServiceInstance()
    {
        return service(static::providersServiceIdentifier());
    }



    /**
     * Returns the items registered in for the providers service.
     *
     * @return array|mixed
     */
    public static function registeredProvidersService()
    {
        return static::providersServiceInstance()->read();
    }



    /**
     * Returns all handleable providers.
     *
     * @return array
     */
    public static function allProviders(): array
    {
        $folder_items   = scandir(static::providersDirectory());
        $provider_files = array_filter($folder_items, function ($item) {
            return ends_with($item, '.php');
        });
        $providers      = array_map(function ($file) {
            $class_name = str_before($file, '.php');

            return kebab_case($class_name);
        }, $provider_files);

        return array_values($providers);
    }



    /**
     * Weather the given provider is defined.
     *
     * @param string $provider
     *
     * @return bool
     */
    public static function providerIsDefined(string $provider): bool
    {
        return in_array($provider, static::allProviders());
    }



    /**
     * Weather the given provider is not defined.
     *
     * @param string $provider
     *
     * @return bool
     */
    public static function providersIsNotDefined(string $provider): bool
    {
        return !static::providerIsDefined($provider);
    }



    /**
     * Returns registered providers.
     *
     * @return array
     */
    public static function registeredProviders(): array
    {
        return array_column(static::registeredProvidersService(), 'class');
    }



    /**
     * Returns all registered provider which are handleable.
     *
     * @return array
     */
    public static function availableProviders(): array
    {
        return array_intersect(static::allProviders(), static::registeredProviders());
    }



    /**
     * Returns a list of providers and their info.
     *
     * @return array
     */
    public static function providers(): array
    {
        $providers      = socialite()->availableProviders();
        $providers_info = array_map(function ($provider) {
            $instance = socialite()->providerInstance($provider);
            return [
                 'name' => $instance->name(),
                 'link' => route('socialite.oauth', ['provider' => $provider]),
            ];
        }, $providers);

        return array_combine($providers, $providers_info);
    }



    /**
     * Weather the given provider is available.
     *
     * @param string $provider
     *
     * @return bool
     */
    public static function providerIsAvailable(string $provider): bool
    {
        return in_array($provider, static::availableProviders());
    }



    /**
     * Weather the given provider is not available.
     *
     * @param string $provider
     *
     * @return bool
     */
    public static function providerIsNotAvailable(string $provider): bool
    {
        return !static::providerIsAvailable($provider);
    }



    /**
     * Returns the part of providers' path.
     *
     * @return array
     */
    protected static function providersPathParts()
    {
        return [
             'Modules',
             'Socialite',
             'Services',
             'Providers',
        ];
    }



    /**
     * Returns the namespace of providers.
     *
     * @return string
     */
    protected static function providersNameSpace()
    {
        $separator = '\\';
        return $separator . implode($separator, static::providersPathParts()) . $separator;
    }



    /**
     * Returns the directory of providers.
     *
     * @return string
     */
    protected static function providersDirectory()
    {
        return base_path(implode(DIRECTORY_SEPARATOR, static::providersPathParts())) . DIRECTORY_SEPARATOR;
    }



    /**
     * Returns the instance of the provider with the given provider name.
     *
     * @param string $provider
     *
     * @return ProviderInterface
     */
    public static function providerInstance(string $provider)
    {
        $class = static::providersNameSpace() . studly_case($provider);
        return new $class;
    }
}
