<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 2:45 PM
 */

namespace Modules\Socialite\Services\Traits;


use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

trait SocialiteModuleTrait
{
    use ModuleRecognitionsTrait;



    /**
     * Returns the module object for this class.
     *
     * @return \Modules\Yasna\Services\ModuleHelper
     */
    public static function module()
    {
        return socialite()->runningModule();
    }



    /**
     * Returns the name of the module for this class.
     *
     * @return string
     */
    public static function moduleName()
    {
        return socialite()->runningModuleName();
    }
}
