<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 2:22 PM
 */

namespace Modules\Socialite\Services\Traits;

use Laravel\Socialite\One\User as SocialiteUser1;
use Laravel\Socialite\Two\User as SocialiteUser2;

trait SocialiteCallbackTrait
{
    /**
     * Returns the name of the after OAuth service.
     *
     * @return string
     */
    public static function callbackServiceName(): string
    {
        return 'after_oauth';
    }



    /**
     * Returns the name of the after OAuth service.
     *
     * @return string
     */
    public static function callbackService(): string
    {
        return static::callbackServiceName();
    }



    /**
     * Returns the name of the callback method
     * which should be registered in after OAuth service
     *
     * @return string
     */
    public static function afterOAuthCallbackName(): string
    {
        return 'callback';
    }



    /**
     * Registers after OAuth method.
     *
     * @param string $method
     * @param string $name
     *
     * @return void
     */
    public static function registerOAuthCallback(string $method): void
    {
        $order = static::afterOAuthServiceInstance()->count() + 1;
        $name  = snake_case(preg_replace("/[^A-Za-z0-9 ]/", ' ', $method));

        module(static::moduleName())
             ->service(socialite()->callbackService())
             ->add($name)
             ->method($method)
             ->order($order)
        ;
    }



    /**
     * Returns the identifier to find after OAuth service.
     *
     * @return string
     */
    protected static function afterOAuthServiceIdentifier(): string
    {
        return static::moduleName() . ":" . socialite()->callbackService();
    }



    /**
     * Returns the after OAuth service.
     *
     * @return \Modules\Yasna\Services\ModuleHelper
     */
    protected static function afterOAuthServiceInstance()
    {
        return service(static::afterOAuthServiceIdentifier());
    }



    /**
     * Returns the registered items for the after OAuth service.
     *
     * @return array
     */
    public static function registeredAfterOAuth()
    {
        return static::afterOAuthServiceInstance()->read();
    }



    /**
     * Handles the callback with the specified socialite user.
     *
     * @param SocialiteUser1|SocialiteUser2 $user
     * @param string                        $provider
     *
     * @return mixed
     */
    public static function callback($user, string $provider)
    {
        $methods        = socialite()->registeredAfterOAuth();
        $simple_methods = array_except($methods, static::afterOAuthCallbackName());

        foreach ($simple_methods as $method) {
            static::callOAuthCallbackMethod($method, $user);
        }

        $callback = ($methods[static::afterOAuthCallbackName()] ?? null);

        if ($callback) {
            return static::callOAuthCallbackMethod($callback, $user);
        }

        return static::callProviderCallback($user, $provider);
    }



    /**
     * Runs the callback method of the specified provider with the specified socialite user.
     *
     * @param SocialiteUser1|SocialiteUser2 $user
     * @param string                        $provider
     *
     * @return mixed
     */
    public static function callProviderCallback($user, string $provider)
    {
        return static::providerInstance($provider)->callback($user);
    }



    /**
     * Calls the OAuth callback method with the specified socialite user.
     * _The method has been registered with the after OAuth service._
     *
     * @param array                         $method
     * @param SocialiteUser1|SocialiteUser2 $user
     *
     * @return mixed
     */
    protected static function callOAuthCallbackMethod(array $method_info, $user)
    {
        $method       = $method_info['method'];
        $class        = str_before($method, '@');
        $class_method = str_after($method, '@');

        return $class::$class_method($user);
    }
}
