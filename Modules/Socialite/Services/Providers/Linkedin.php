<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/25/18
 * Time: 10:59 AM
 */

namespace Modules\Socialite\Services\Providers;


use Modules\Socialite\Services\Structure\ProviderAbstract;

class Linkedin extends ProviderAbstract
{
    /**
     * Returns the name of the provider in the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public function nameIn(string $locale): string
    {
        return $this->runningModule()->getTrans('services.linkedin', [], $locale);
    }
}
