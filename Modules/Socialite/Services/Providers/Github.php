<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 12:02 PM
 */

namespace Modules\Socialite\Services\Providers;

use Laravel\Socialite\Two\User;
use Modules\Socialite\Services\Structure\ProviderAbstract;

class Github extends ProviderAbstract
{
    /**
     * Returns the name of the provider in the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public function nameIn(string $locale): string
    {
        return $this->runningModule()->getTrans('services.github', [], $locale);
    }
}
