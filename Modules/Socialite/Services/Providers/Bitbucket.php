<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/25/18
 * Time: 12:26 PM
 */

namespace Modules\Socialite\Services\Providers;


use Modules\Socialite\Services\Structure\ProviderAbstract;

class Bitbucket extends ProviderAbstract
{
    /**
     * Returns the name of the provider in the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public function nameIn(string $locale): string
    {
        return $this->runningModule()->getTrans('services.bitbucket', [], $locale);
    }
}
