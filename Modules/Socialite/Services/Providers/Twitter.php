<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/1/18
 * Time: 4:00 PM
 */

namespace Modules\Socialite\Services\Providers;


use Modules\Socialite\Services\Structure\ProviderAbstract;

class Twitter extends ProviderAbstract
{
    /**
     * Returns the name of the provider in the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public function nameIn(string $locale): string
    {
        return $this->runningModule()->getTrans('services.twitter', [], $locale);
    }
}
