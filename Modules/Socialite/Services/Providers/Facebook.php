<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 4:51 PM
 */

namespace Modules\Socialite\Services\Providers;


use Modules\Socialite\Services\Structure\ProviderAbstract;

class Facebook extends ProviderAbstract
{
    /**
     * Returns the name of the provider in the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public function nameIn(string $locale): string
    {
        return $this->runningModule()->getTrans('services.facebook', [], $locale);
    }
}
