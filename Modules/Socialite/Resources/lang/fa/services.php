<?php
return [
     "github"    => "گیت‌هاب",
     "facebook"  => "فیسبوک",
     "twitter"   => "توییتر",
     "linkedin"  => "لینکدین",
     "google"    => "گوگل",
     "bitbucket" => "بیتباکت",
];
