<?php
return [
     "github"    => "GitHub",
     "facebook"  => "Facebook",
     "twitter"   => "Twitter",
     "linkedin"  => "LinkedIn",
     "google"    => "Google",
     "bitbucket" => "Bitbucket",
];
