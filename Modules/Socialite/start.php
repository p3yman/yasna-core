<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}


if (!function_exists('socialite')) {

    /**
     * Returns new instance of the `Modules\Socialite\Services\Socialite` class.
     * _This helper could be used to access features of the `Socialite` module._
     *
     * @return \Modules\Socialite\Services\Socialite
     */
    function socialite()
    {
        return new \Modules\Socialite\Services\Socialite();
    }
}
