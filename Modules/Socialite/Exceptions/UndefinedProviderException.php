<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/24/18
 * Time: 5:43 PM
 */

namespace Modules\Socialite\Exceptions;

use Exception;

class UndefinedProviderException extends Exception
{
    /**
     * UndefinedProviderException constructor.
     *
     * @param string $provider
     */
    public function __construct(string $provider)
    {
        $message = "Undefined provider: '$provider'.";

        parent::__construct($message);
    }
}
