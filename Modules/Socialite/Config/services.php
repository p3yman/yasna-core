<?php

return [
     'github' => [
          'client_id'     => env('GITHUB_CLIENT_ID'),
          'client_secret' => env('GITHUB_CLIENT_SECRET'),
          'redirect'      => '', // This field is being filled dynamically with code.
     ],

     'facebook' => [
          'client_id'     => env('FACEBOOK_CLIENT_ID'),
          'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
          'redirect'      => '', // This field is being filled dynamically with code.
     ],

     'twitter' => [
          'client_id'     => env('TWITTER_CLIENT_ID'),
          'client_secret' => env('TWITTER_CLIENT_SECRET'),
          'redirect'      => '', // This field is being filled dynamically with code.
     ],

     'linkedin' => [
          'client_id'     => env('LINKEDIN_CLIENT_ID'),
          'client_secret' => env('LINKEDIN_CLIENT_SECRET'),
          'redirect'      => '', // This field is being filled dynamically with code.
     ],

     'google' => [
          'client_id'     => env('GOOGLE_CLIENT_ID'),
          'client_secret' => env('GOOGLE_CLIENT_SECRET'),
          'redirect'      => '', // This field is being filled dynamically with code.
     ],

     'bitbucket' => [
          'client_id'     => env('BITBUCKET_CLIENT_ID'),
          'client_secret' => env('BITBUCKET_CLIENT_SECRET'),
          'redirect'      => '', // This field is being filled dynamically with code.
     ],
];
