<?php

namespace Modules\Socialite\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckProviderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (socialite()->providerIsNotAvailable($request->provider)) {
            abort(404);
        }

        return $next($request);
    }
}
