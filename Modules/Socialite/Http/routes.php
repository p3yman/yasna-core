<?php

Route::group([
     'middleware' => ['web', 'check-provider'],
     'namespace'  => 'Modules\Socialite\Http\Controllers',
], function () {

    Route::get('oauth/{provider}', 'OAthController@redirectToProvider')
         ->name('socialite.oauth')
    ;
    Route::get('oauth/{provider}/callback', 'OAthController@handleProviderCallback')
         ->name('socialite.oauth.callback')
    ;

});
