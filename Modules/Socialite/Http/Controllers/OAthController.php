<?php

namespace Modules\Socialite\Http\Controllers;

use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

use Laravel\Socialite\Facades\Socialite;

class OAthController extends YasnaController
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(SimpleYasnaRequest $request)
    {
        return Socialite::driver($request->provider)->redirect();
    }



    /**
     * Obtain the user information from GitHub.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(SimpleYasnaRequest $request)
    {
        $user = Socialite::driver($request->provider)->user();

        return socialite()->callback($user, $request->provider);
    }
}
