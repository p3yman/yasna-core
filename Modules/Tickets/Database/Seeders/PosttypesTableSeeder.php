<?php

namespace Modules\Tickets\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Filemanager\Services\Seeder\PosttypeSeedingTool;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PosttypesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        (new PosttypeSeedingTool)->seedUploadConfigs($this->uploadConfigsData());
        (new PosttypeSeedingTool)->seedPosttypes($this->posttypesData());
    }



    /**
     * Returns an array of upload configs data.
     *
     * @return array
     */
    protected function uploadConfigsData()
    {
        return [
             [
                  'slug'          => 'tickets-attachments',
                  'title'         => $this->runningModule()->getTrans('general.attachments.of.plural', [
                       'title' => $this->runningModule()->getTrans('general.tickets'),
                  ]),
                  'category'      => '',
                  'order'         => '20',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "attachments.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
        ];
    }



    /**
     * Returns an array of posttypes data.
     *
     * @return array
     */
    protected function posttypesData()
    {
        return [
             [
                  'posttype'       => [
                       'slug'           => "tickets",
                       'title'          => $this->runningModule()->getTrans('general.tickets'),
                       'singular_title' => $this->runningModule()->getTrans('general.ticket'),
                       'template'       => "special",
                       'icon'           => "commenting",
                       'order'          => "2",
                       'locales'        => 'fa',
                  ],
                  'features'       => [
                       'attachments',
                  ],
                  'upload-configs' => [
                       'attachments_upload_configs' => 'tickets-attachments',
                  ],
             ],
        ];
    }

}
