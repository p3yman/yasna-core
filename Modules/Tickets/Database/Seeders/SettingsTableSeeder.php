<?php

namespace Modules\Tickets\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns an array of the data to be seeded.
     *
     * @return array
     */
    protected function data(): array
    {
        $module = $this->runningModule();

        return [
             [
                  'slug'          => 'ticket_notification_channels',
                  'title'         => $module->getTrans('seeder.notification-channels'),
                  'category'      => 'upstream',
                  'order'         => '50',
                  'data_type'     => 'array',
                  'default_value' => implode(HTML_LINE_BREAK, $module->getConfig('notifications.channels')),
                  'hint'          => $module->getTrans('seeder.notification-channels-help'),
                  'css_class'     => 'ltr',
                  'is_localized'  => '0',
             ],
        ];
    }
}
