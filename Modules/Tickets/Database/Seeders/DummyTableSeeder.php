<?php

namespace Modules\Tickets\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class DummyTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedTicketType();
        $this->seedTickets();
    }



    /**
     * Seeds the dummy ticket type.
     */
    protected function seedTicketType()
    {
        yasna()->seed('ticket_types', [
             [
                  'slug'   => $this->dummyTicketTypeSlug(),
                  'titles' => json_encode([
                       'fa' => $this->runningModule()->getTrans('seeder.dummy', [], 'fa'),
                       'en' => $this->runningModule()->getTrans('seeder.dummy', [], 'en'),
                  ]),
             ],
        ]);
    }



    /**
     * Seeds the dummy tickets.
     */
    protected function seedTickets()
    {
        $locales = get_setting('site_locales');

        foreach ($locales as $locale) {
            $this->seedTicketsInLocale($locale);
        }
    }



    /**
     * Seeds a dummy ticket and one answer of it in the given locale.
     *
     * @param string $locale
     */
    protected function seedTicketsInLocale(string $locale)
    {
        $main_ticket_data['title']  = $this->runningModule()->getTrans('seeder.dummy-ticket', [], $locale);
        $main_ticket_data['text']   = $this->runningModule()->getTrans('seeder.text-of', [
             'title' => $main_ticket_data['title'],
        ], $locale)
        ;
        $reply_ticket_data['title'] = $this->runningModule()->getTrans('seeder.answer-of', [
             'title' => $main_ticket_data['title'],
        ], $locale)
        ;
        $reply_ticket_data['text']  = $this->runningModule()->getTrans('seeder.text-of', [
             'title' => $reply_ticket_data['title'],
        ], $locale)
        ;

        $main_ticket = ticket($this->dummyTicketTypeSlug())
             ->addRemark($main_ticket_data + ['lang' => $locale]);

        $main_ticket->addReply($reply_ticket_data + ['lang' => $locale]);

    }



    /**
     * Returns the slug of the dummy ticket type.
     *
     * @return string
     */
    protected function dummyTicketTypeSlug()
    {
        return 'dummy';
    }
}
