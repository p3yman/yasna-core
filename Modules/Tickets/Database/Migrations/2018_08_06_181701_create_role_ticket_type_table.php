<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleTicketTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_ticket_type', function(Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('ticket_type_id')->index();
            $table->foreign('ticket_type_id')->references('id')->on('ticket_types');

            $table->unsignedInteger('role_id')->index();
            $table->foreign('role_id')->references('id')->on('roles');
            
            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_ticket_type');
    }
}
