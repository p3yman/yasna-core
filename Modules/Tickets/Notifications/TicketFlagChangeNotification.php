<?php

namespace Modules\Tickets\Notifications;

use App\Models\Ticket;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Tickets\Notifications\Traits\DynamicChannelsTrait;

class TicketFlagChangeNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use DynamicChannelsTrait;

    public $action_ticket;



    /**
     * TicketFlagChangeNotification constructor.
     *
     * @param Ticket $action_ticket
     */
    public function __construct($action_ticket)
    {
        $this->action_ticket = $action_ticket;
    }



    /**
     * Returns the mail representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $locale = $notifiable->lang;
        app()->setLocale($locale);

        $template = isLangRtl($locale) ? 'email-rtl' : 'email-ltr';
        $module   = $this->runningModule();


        return (new MailMessage)
             ->template($module->getBladePath("notification.$template"))
             ->subject($module->getTrans('notifications.mail.flag-changing-subject'))
             ->greeting($module->getTrans('notifications.mail.greeting'))
             ->line($this->notificationText($notifiable))
             ->salutation($module->getTrans(
                  'notifications.mail.salutation',
                  ['site_title' => get_setting('site_title')]
             ))
             ;
    }



    /**
     * Return the sms representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        return $this->notificationText($notifiable);
    }



    /**
     * Returns the general text of this notification.
     *
     * @param Ticket $notifiable
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    protected function notificationText($notifiable)
    {
        $title  = $notifiable->title;
        $locale = $notifiable->lang;
        $flag   = $this->action_ticket->flag;
        $person = $this->action_ticket->creator;

        if ($person->exists) {
            $name = $person->full_name;

            return trans(
                 "tickets::notifications.sms.flag-changing-by.$flag",
                 compact('name', 'title'),
                 $locale
            );
        } else {
            return trans(
                 "tickets::notifications.sms.flag-changing.$flag",
                 compact('title'),
                 $locale
            );
        }
    }



    /**
     * Return the array representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'title'       => $notifiable->title,
             'text'        => $this->toSms($notifiable),
             'destination' => $notifiable->routeNotificationForSms(),
        ];
    }
}
