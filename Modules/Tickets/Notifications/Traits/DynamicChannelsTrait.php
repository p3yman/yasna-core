<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/13/19
 * Time: 1:08 PM
 */

namespace Modules\Tickets\Notifications\Traits;


use App\Models\Ticket;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

trait DynamicChannelsTrait
{
    use ModuleRecognitionsTrait;



    /**
     * Return the notification's delivery channels.
     *
     * @param Ticket $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return collect($this->dynamicChannels())
             ->map(function (string $channel_slug) {
                 return $this->mapChannelSlug($channel_slug);
             })->filter()
             ->toArray()
             ;
    }



    /**
     * Maps the specified channel to its real value and returns it.
     *
     * @param string $channel
     *
     * @return string|null
     */
    protected function mapChannelSlug(string $channel)
    {
        $map = $this->channelsMap();

        return ($map[$channel] ?? null);
    }



    /**
     * Returns a map array to map the channel slugs to their real values.
     *
     * @return array
     */
    protected function channelsMap(): array
    {
        return [
             'sms'  => $this->yasnaSmsChannel(),
             'mail' => $this->yasnaMailChannel(),
        ];
    }



    /**
     * Returns an array of channels based on the current state of the site's settings.
     *
     * @return array
     */
    protected function dynamicChannels()
    {
        return ($this->settingChannels() ?? $this->defaultChannels());
    }



    /**
     * Returns an array of channels from the settings or `null` if the setting is not available.
     *
     * @return array|null
     */
    protected function settingChannels()
    {
        $setting = get_setting('ticket_notification_channels');

        return is_array($setting)
             ? $setting
             : null;
    }



    /**
     * Returns an array of the default channels from the configs
     *
     * @return array
     */
    protected function defaultChannels(): array
    {
        return $this->runningModule()->getConfig('notifications.channels');
    }
}
