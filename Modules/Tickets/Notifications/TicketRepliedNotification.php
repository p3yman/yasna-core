<?php

namespace Modules\Tickets\Notifications;

use App\Models\Ticket;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Tickets\Notifications\Traits\DynamicChannelsTrait;

class TicketRepliedNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use DynamicChannelsTrait;

    public $action_ticket;



    /**
     * TicketFlagChangeNotification constructor.
     *
     * @param Ticket $action_ticket
     */
    public function __construct($action_ticket)
    {
        $this->action_ticket = $action_ticket;
    }



    /**
     * Returns the mail representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $locale = $notifiable->lang;
        app()->setLocale($locale);

        $template = isLangRtl($locale) ? 'email-rtl' : 'email-ltr';
        $module   = $this->runningModule();
        $lines    = $this->notificationLines($notifiable);


        return (new MailMessage)
             ->template($module->getBladePath("notification.$template"))
             ->subject($module->getTrans('notifications.mail.reply-subject'))
             ->greeting($module->getTrans('notifications.mail.greeting'))
             ->line($lines[0] . ':')
             ->line($lines[1])
             ->salutation($module->getTrans(
                  'notifications.mail.salutation',
                  ['site_title' => get_setting('site_title')]
             ))
             ;
    }



    /**
     * Return the sms representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $lines = $this->notificationLines($notifiable);

        return $lines[0]
             . ':'
             . "\n\r"
             . $lines[1];
    }



    /**
     * Returns the lines of this notification's content.
     *
     * @param Ticket $notifiable
     *
     * @return array
     */
    protected function notificationLines($notifiable)
    {
        $title  = $notifiable->title;
        $locale = $notifiable->lang;
        $reply_text = $this->action_ticket->text;
        $person     = $this->action_ticket->creator;

        if ($person->exists) {
            $name = $person->full_name;

            $first_line = trans(
                 "tickets::notifications.sms.replied-by",
                 compact('name', 'title'),
                 $locale
            );
        } else {
            $first_line = trans(
                 "tickets::notifications.sms.replied",
                 compact('title'),
                 $locale
            );
        }

        return [
             $first_line,
             $reply_text,
        ];
    }



    /**
     * Return the array representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'title'       => $notifiable->title,
             'text'        => $this->toSms($notifiable),
             'destination' => $notifiable->routeNotificationForSms(),
        ];
    }
}
