<?php

namespace Modules\Tickets\Notifications;

use App\Models\Ticket;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Tickets\Notifications\Traits\DynamicChannelsTrait;

class TicketCreationNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use DynamicChannelsTrait;



    /**
     * Returns the mail representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $locale = $notifiable->lang;
        app()->setLocale($locale);

        $template = isLangRtl($locale) ? 'email-rtl' : 'email-ltr';
        $module   = $this->runningModule();


        return (new MailMessage)
             ->template($module->getBladePath("notification.$template"))
             ->subject($module->getTrans('notifications.mail.creation-subject'))
             ->greeting($module->getTrans('notifications.mail.greeting'))
             ->line($module->getTrans(
                  'notifications.mail.creation-content.line1',
                  ['title' => $notifiable->title]
             ))
             ->salutation($module->getTrans(
                  'notifications.mail.salutation',
                  ['site_title' => get_setting('site_title')]
             ))
             ;
    }



    /**
     * Return the sms representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        return trans('tickets::notifications.sms.creation', ['title' => $notifiable->title], $notifiable->lang);
    }



    /**
     * Return the array representation of the notification.
     *
     * @param Ticket $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'title'       => $notifiable->title,
             'text'        => $this->toSms($notifiable),
             'destination' => $notifiable->routeNotificationForSms(),
        ];
    }
}
