<?php

namespace Modules\Tickets\Entities\Traits;


use App\Models\TicketType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserTicketTrait
{
    /**
     * Standard One-to-Many Relationship between tickets and users.
     *
     * @return HasMany
     */
    public function tickets()
    {
        return $this->remarks()
                    ->setModel(model('ticket'))
                    ->where('entity_model', TicketType::class)
             ;
    }



    /**
     * Standard One-to-Many Relationship to access master tickets of the user.
     *
     * @return HasMany
     */
    public function masterTickets()
    {
        return $this->tickets()->topItems();
    }



    /**
     * Accessor for the `masterTickets()` Method
     *
     * @return Collection
     */
    public function getMasterTicketsAttribute()
    {
        return $this->getRelationValue('masterTickets');
    }
}
