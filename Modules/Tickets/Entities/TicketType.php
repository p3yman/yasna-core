<?php

namespace Modules\Tickets\Entities;

use App\Models\Role;
use App\Models\User;
use Modules\Remark\Services\Traits\RemarkTrait;
use Modules\Tickets\Providers\TicketsServiceProvider;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketType extends YasnaModel
{
    use SoftDeletes;
    use RemarkTrait;

    /**
     * The list of guarded attributes.
     *
     * @var array
     */
    protected $guarded = ["id"];

    /**
     * The list of casting attributes.
     *
     * @var array
     */
    protected $casts = ['titles' => 'array'];



    /**
     * The list of all possible flags for remarks/tickets.
     *
     * @return array
     */
    public function remarkPossibleFlags()
    {
        return TicketsServiceProvider::flags();
    }



    /**
     * Return the default language for the remark/tickets.
     *
     * @return string
     */
    public function defaultRemarkLang()
    {
        return getLocale();
    }



    /**
     * Return the default flag for the remark/tickets.
     *
     * @return string
     */
    public function defaultRemarkFlag()
    {
        return 'new';
    }



    /**
     * The model which will be using as an alias of the default `Remark` model.
     *
     * @return string
     */
    public function remarkAliasModel()
    {
        return 'ticket';
    }



    /**
     * Return the title if the given locale.
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function titleIn(string $locale)
    {
        return ($this->titles[$locale] ?? null);
    }



    /**
     * Return the title in the current locale.
     *
     * @return null|string
     */
    public function titleInCurrentLocale()
    {
        return $this->titleIn(getLocale());
    }



    /**
     * Accessor of the `titleInCurrentLocale()` method.
     *
     * @return null|string
     */
    public function getTitleAttribute()
    {
        return $this->titleInCurrentLocale();
    }



    /**
     * The standard laravel many-to-many relationship between `ticket_types` and `roles` table.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }



    /**
     * Check if this ticket type has the pointed role as its supporter.
     *
     * @param int $role_id
     *
     * @return bool
     */
    public function hasRole($role_id)
    {
        return boolval($this->roles()->where('roles.id', $role_id)->count());
    }



    /**
     * Searches in a given role, including 'all', 'no', 'auto' and 'support' wildcards.
     *
     * @param string|array $given_role
     */
    protected function electorRole($given_role)
    {
        /*
         * Bypass
         */
        if (is_null($given_role)) {
            return;
        }

        /*
         * Wildcards
         */
        if ($given_role == 'auto') {
            $given_role = user()->rolesArray();
        } elseif ($given_role == 'all') {
            $given_role = role()->supportRoles()->pluck('slug')->toArray();
        } elseif ($given_role == 'no') {
            $this->elector()->has('roles', '=', 0);
        } elseif (!is_array($given_role)) {
            $given_role = (array)$given_role;
        }

        /*
         * Safety
         */
        if (!is_array($given_role)) {
            return;
        }

        /*
         * Final Process
         */
        $this->elector()->whereHas('roles', function ($query) use ($given_role) {
            $query->whereIn('roles.slug', $given_role);

            return $query;
        })
        ;
    }



    /**
     * Whether this ticket type is accessible for the specified user.
     *
     * @param User $user
     *
     * @return bool
     */
    public function isAccessibleFor(User $user)
    {
        return boolval(
                  static::instance()
                        ->elector(['role' => $user->rolesArray()])
                        ->whereSlug($this->slug)
                        ->count()
             ) or $user->isDeveloper();
    }



    /**
     * Whether this ticket type is accessible for the logged in user.
     *
     * @return bool
     */
    public function isAccessible()
    {
        return $this->isAccessibleFor(user());
    }



    /**
     * Whether any ticket type is accessible for the specified user.
     *
     * @param User $user
     *
     * @return bool
     */
    public static function anyAccessibleFor(User $user)
    {
        return boolval(
                  static::instance()
                        ->elector(['role' => $user->rolesArray()])
                        ->count()
             ) or $user->isDeveloper();
    }



    /**
     * Whether any ticket type is accessible for the logged in user.
     *
     * @return bool
     */
    public static function anyAccessible()
    {
        return static::anyAccessibleFor(user());
    }



    /**
     * Check if any ticket is stored under this type.
     *
     * @return bool
     */
    public function hasAnyTicket()
    {
        return boolval($this->remarks()->count());
    }



    /**
     * Check if no ticket is stored under this type.
     *
     * @return bool
     */
    public function hasNotAnyTicket()
    {
        return !$this->hasAnyTicket();
    }



    /**
     * check if this ticket type is ready to be hard deleted
     *
     * @return bool
     */
    public function isReadyToHardDelete()
    {
        return ($this->trashed() and $this->hasNotAnyTicket());
    }



    /**
     * check if this ticket type is not ready to be hard deleted
     *
     * @return bool
     */
    public function isNotReadyToHardDelete()
    {
        return !$this->isReadyToHardDelete();
    }



    /**
     * Returns and array version of ticket type to in used for API.
     *
     * @return array
     */
    public function getApiArray()
    {
        return [
             'slug'       => $this->slug,
             'title'      => $this->title,
        ];
    }



    /**
     * Accessor for the `getApiArray()` Method
     *
     * @return array
     */
    public function getApiArrayAttribute()
    {
        return $this->getApiArray();
    }
}
