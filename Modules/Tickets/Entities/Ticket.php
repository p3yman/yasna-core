<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/6/18
 * Time: 1:39 PM
 */

namespace Modules\Tickets\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Modules\Remark\Entities\Remark;
use Modules\Remark\Services\Traits\RemarkAliasModelTrait;
use App\Models\TicketType;
use Modules\Tickets\Providers\TicketsServiceProvider;

/**
 * Model Ticket
 * <br>
 * This method is an alias of the default `Remark` model.
 *
 * @package Modules\Tickets\Entities
 */
class Ticket extends Remark
{
    use RemarkAliasModelTrait;
    use Notifiable;

    /**
     * @var string Full namespace of the entity model.
     */
    public static $entity_model = TicketType::class;

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
         'entity_model' => TicketType::class,
    ];


    /*
     * There is a problem with this model
     * because it is potentially extending from the `YasnaModel` class
     * but that parent is not specifically specified in this file.
     * So the `yasna:init` command will not recognize it as a new model.
     * And we have write 'extends YasnaModel' to trick it.
     *
     * ToDo: Remove this comments when all models has been converted to the `YasnaModel` type.
    */

    /**
     * Elector to select tickets form the given ticket type.
     *
     * @param string|array $types
     */
    public function electorTicketType($types)
    {
        /*
         * Bypass
         */
        if (!$types or ($types == 'all')) {
            return;
        }

        /*
         * Wildcards
         */
        if (!is_array($types)) {
            $types = (array)$types;
        }

        /*
         * Safety
         */
        if (!is_array($types) or !count($types)) {
            return;
        }

        $type_models = [];
        foreach ($types as $type) {
            $type_models[] = model('ticket-type', $type);
        }


        $this->elector()->whereIn('entity_id', collect($type_models)->pluck('id')->toArray());
    }



    /**
     * Accessor to get the trans of the flag.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getFlagTitleAttribute()
    {
        return TicketsServiceProvider::getFlagTitle($this->flag);
    }



    /**
     * Accessor to get the color of the flag.
     *
     * @return string|null
     */
    public function getFlagColorAttribute()
    {
        return TicketsServiceProvider::getFlagColor($this->flag);
    }



    /**
     * Accessor to get the icon of the flag.
     *
     * @return string|null
     */
    public function getFlagIconAttribute()
    {
        return TicketsServiceProvider::getFlagIcon($this->flag);
    }



    /**
     * Return a text of the flag which is ready to show.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|mixed|null|string
     */
    public function getUsableText()
    {
        if ($this->type == 'changing-flag') {
            return trans("tickets::flags.changing.$this->flag");
        }

        if ($this->type == 'changing-ticket-type') {
            return trans("tickets::general.type-changed-to", [
                 'type' => $this->getFoundEntity()->title,
            ]);
        }

        return $this->text;
    }



    /**
     * Accessor for the `getUsableText()` method.
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|mixed|null|string
     */
    public function getUsableTextAttribute()
    {
        return $this->getUsableText();
    }



    /**
     * Return an string that is defines the type of the ticket in the timeline.
     *
     * @return string
     */
    public function getTimelineType()
    {
        if (in_array($this->type, ['changing-flag', 'changing-ticket-type'])) {
            if ($this->flag == 'done') {
                return 'closing';
            } else {
                return 'modifying';
            }
        }

        return 'default';
    }



    /**
     * Accessor for the `getTimelineType()` method.
     *
     * @return string
     */
    public function getTimelineTypeAttribute()
    {
        return $this->getTimelineType();
    }



    /**
     * Return the icon to show the ticket in timeline.
     *
     * @return string
     */
    public function getTimelineIcon()
    {
        $map = [
             'default'   => 'comments',
             'closing'   => 'times',
             'modifying' => 'share',
        ];

        return $map[$this->timeline_type];
    }



    /**
     * Accessor for the `getTimelineIcon()` method.
     *
     * @return string
     */
    public function getTimelineIconAttribute()
    {
        return $this->getTimelineIcon();
    }



    /**
     * Return the color to show the ticket in timeline.
     *
     * @return string
     */
    public function getTimelineColor()
    {
        $map = [
             'default'   => 'info',
             'closing'   => 'danger',
             'modifying' => 'primary',
        ];

        return $map[$this->timeline_type];
    }



    /**
     * Accessor for the `getTimelineColor()` method.
     *
     * @return string
     */
    public function getTimelineColorAttribute()
    {
        return $this->getTimelineColor();
    }



    /**
     * Check if the ticket is finalized or not.
     *
     * @return bool
     */
    public function isFinalized()
    {
        return ($this->flag == 'done');
    }



    /**
     * Accessor for the `isFinalized()` method.
     *
     * @return bool
     */
    public function getIsFinalizedAttribute()
    {
        return $this->isFinalized();
    }



    /**
     * Check if the ticket is not finalized.
     *
     * @return bool
     */
    public function isNotFinalized()
    {
        return !$this->isFinalized();
    }



    /**
     * Accessor for the `isNotFinalized()` method.
     *
     * @return bool
     */
    public function getIsNotFinalizedAttribute()
    {
        return $this->isNotFinalized();
    }



    /**
     * Check if the ticket is accessible for the logged in user or not.
     *
     * @return mixed
     */
    public function isAccessible()
    {
        return $this->entity->isAccessible();
    }



    /**
     * Find and return the last flag of the ticket before being marked as done.
     *
     * @return mixed|null
     */
    public function lastFlagBeforeDone()
    {
        if ($this->flag != 'done') {
            return null;
        }

        $last_flag_change = $this->deepReplies()
                                 ->orderByDesc('created_at')
                                 ->where('flag', '<>', 'done')
                                 ->first()
        ;

        if ($last_flag_change) {
            return $last_flag_change->flag;
        } else {
            return $this->original_flag;
        }
    }



    /**
     * Return the route notification for sms notifications.
     *
     * @return string|null
     */
    public function routeNotificationForSms()
    {
        $user = $this->user;

        if ($user) {
            return $user->mobile;
        }

        return $this->guest_mobile;
    }



    /**
     * Return the route notification for email notifications.
     *
     * @return string|null
     */
    public function routeNotificationForMail()
    {
        $user = $this->user;

        if ($user) {
            return $user->email;
        }

        return $this->guest_email;
    }



    /**
     * Return the mobile number to be used while sending the sms notifications.
     *
     * @return null|string
     */
    public function getMobileAttribute()
    {
        return $this->routeNotificationForSms();
    }



    /**
     * get last flag of ticket
     *
     * @return string
     */
    public function getLastFlag()
    {
        return $this->where('parent_id', $this->id)
                    ->where('type', 'changing-flag')
                    ->orderBy('id', 'desc')
                    ->first()->flag
             ;
    }



    /**
     * get last update time of ticket
     *
     * @return string
     */
    public function getLastUpdateTime()
    {
        $ticket = $this->where('parent_id', $this->id)->orderBy('id', 'desc')->first();
        if ($ticket) {
            return $ticket->updated_at;
        } else {
            return $this->updated_at;
        }
    }



    /**
     * Standard One-to-Many relationship with the ticket types.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticketType()
    {
        return $this->entity();
    }



    /**
     * Accessor for the `ticketType()` Relation
     *
     * @return TicketType|null
     */
    public function getTicketTypeAttribute()
    {
        return $this->getRelationValue('ticketType');
    }



    /**
     * Checks if the ticket type of this ticket is accessible for the specified user.
     *
     * @param User $user
     *
     * @return bool
     */
    public function ticketTypeIsAccessibleFor(User $user)
    {
        $type = $this->ticket_type;

        return ($type and $type->isAccessibleFor($user));
    }



    /**
     * Weather this ticket is viewable for the specified user.
     *
     * @param User $user
     *
     * @return bool
     */
    public function isViewableFor(User $user)
    {
        return ($this->ownedBy($user) or $this->ticketTypeIsAccessibleFor($user));
    }



    /**
     * Weather this ticket is not viewable for the specified user.
     *
     * @param User $user
     *
     * @return bool
     */
    public function isNotViewableFor(User $user)
    {
        return !$this->isViewableFor($user);
    }



    /**
     * Weather this ticket is viewable for the current client.
     *
     * @return bool
     */
    public function isViewableForClient()
    {
        return $this->isViewableFor(user());
    }



    /**
     * Weather this ticket is not viewable for the current client.
     *
     * @return bool
     */
    public function isNotViewableForClient()
    {
        return !$this->isViewableForClient();
    }



    /**
     * Scopes the query to the master tickets with the default type.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeTopItems(Builder $query)
    {
        return $query
             ->withType(ticket()->guessDefaultRemarkType())
             ->mastersOnly()
             ;

    }
}
