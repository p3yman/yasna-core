<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/16/18
 * Time: 6:04 PM
 */

namespace Modules\Tickets\Http\Controllers\Api;


use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class TypesController extends YasnaApiController
{
    /**
     * Returns a response containing a list of ticket types.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return response()->json(
             $this->success(
                  ticket()
                       ->get()
                       ->pluck('api_array')
                       ->toArray()
             )
        );
    }



    /**
     * Returns a response containing information about the specified ticket type.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function single(SimpleYasnaRequest $request)
    {
        $type = ticket()->grabSlug($request->slug);

        if ($type->not_exists) {
            return $this->clientError(1005);
        }
        return response()->json(
             $this->success([$type->api_array])
        );
    }
}
