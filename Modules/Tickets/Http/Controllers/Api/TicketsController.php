<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/15/18
 * Time: 2:54 PM
 */

namespace Modules\Tickets\Http\Controllers\Api;


use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Modules\Tickets\Http\Requests\Api\TicketChangeFlagRequest;
use Modules\Tickets\Http\Requests\Api\TicketDeleteRequest;
use Modules\Tickets\Http\Requests\Api\TicketReplyRequest;
use Modules\Tickets\Http\Requests\Api\TicketSaveRequest;
use Modules\Tickets\Http\Requests\Api\TicketSingleActionRequest;
use Modules\Tickets\Providers\TicketsServiceProvider;
use Modules\Tickets\Providers\TicketTypeServiceProvider;
use Modules\Tickets\Services\Api\Paginator;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;
use Modules\Yasna\Services\YasnaRequest;
use Symfony\Component\HttpFoundation\JsonResponse;

class TicketsController extends YasnaApiController
{
    /**
     * Returns tickets for the logged in user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $builder = user()
             ->masterTickets()
             ->where('entity_id', TicketTypeServiceProvider::getRequestTicketType()->id)
        ;

        return response()->json(
             (new Paginator($builder))
                  ->itemsName('tickets')
                  ->itemsMap(function (Ticket $ticket) {
                      return $ticket->api_array;
                  })
                  ->toArray()
        );
    }



    /**
     * Saves a ticket with the given data.
     *
     * @param TicketSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(TicketSaveRequest $request)
    {
        $data        = $this->saveData($request);
        $ticket_type = TicketTypeServiceProvider::getRequestTicketType();
        $ticket      = $ticket_type->addRemark($data);

        return response()->json(
             $this->success([$ticket->api_array])
        );
    }



    /**
     * Uploads files while saving new ticket and returns the hashids.
     *
     * @param TicketSaveRequest $request
     *
     * @return array
     */
    protected function saveUploadFiles(TicketSaveRequest $request)
    {
        $uploaded        = [];
        $attachment_keys = $request->getAttachmentKeys();

        foreach ($attachment_keys as $key) {
            $file     = $request->file($key);
            $uploader = TicketsServiceProvider::getVirtualUploader($file);
            $hashid   = $uploader->upload();

            if (fileManager()->file($hashid)->permanentSave()) {
                $uploaded[] = $hashid;
            }
        }

        return $uploaded;
    }



    /**
     * Returns the saving data for a new ticket.
     *
     * @param TicketSaveRequest $request
     *
     * @return array
     */
    protected function saveData(TicketSaveRequest $request)
    {
        $uploaded          = $this->saveUploadFiles($request);
        $data              = array_merge($request->only($request->generalFillableFields()), [
             'attachments' => $uploaded,
        ]);
        $data['entity_id'] = TicketTypeServiceProvider::getRequestTicketType()->id;

        return $data;
    }



    /**
     * Returns the information of a specified ticket.
     *
     * @param TicketSingleActionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function single(TicketSingleActionRequest $request)
    {
        $ticket = $this->findTicketByHashid($request->hashid);

        return response()->json(
             $this->success([$ticket->api_array])
        );
    }



    /**
     * Returns a full information of the flag of the requested ticket.
     *
     * @param TicketSingleActionRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function flag(TicketSingleActionRequest $request)
    {
        $ticket = $this->findTicketByHashid($request->hashid);

        return response()->json(
             $this->success([TicketsServiceProvider::getFlagApiInfo($ticket->flag)])
        );
    }



    /**
     * Marks the specified ticket as the given flag.
     *
     * @param TicketChangeFlagRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeFlag(TicketChangeFlagRequest $request)
    {
        $ticket = $request->model;

        $ticket->markAs($request->flag);

        return response()->json(
             $this->success([$ticket->api_array])
        );
    }



    /**
     * Returns a response containing all the remarks from the timeline of the requested ticket.
     *
     * @param TicketSingleActionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function timeline(TicketSingleActionRequest $request)
    {
        $ticket  = $this->findTicketByHashid($request->hashid);
        $builder = $ticket->deepReplies();

        return response()->json(
             (new Paginator($builder))
                  ->itemsMap(function (Ticket $ticket) {
                      return $ticket->api_array;
                  })
                  ->toArray()
        );
    }



    /**
     * Returns a response containing replies of the requested ticket.
     *
     * @param TicketSingleActionRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function replies(TicketSingleActionRequest $request)
    {
        $ticket  = $this->findTicketByHashid($request->hashid);
        $builder = $ticket
             ->replies()
             ->withType(ticket()->guessDefaultRemarkType())
        ;

        return response()->json(
             (new Paginator($builder))
                  ->itemsName('replies')
                  ->itemsMap(function (Ticket $ticket) {
                      return $ticket->api_array;
                  })
                  ->toArray()
        );
    }



    /**
     * Saves a reply for the requested ticket.
     *
     * @param TicketReplyRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function reply(TicketReplyRequest $request)
    {
        $ticket = $request->model;
        $data   = $this->saveData($request);
        $reply  = $ticket->addReply($data);

        return response()->json(
             $this->success([$reply->api_array])
        );
    }



    /**
     * Deletes the requested ticket.
     *
     * @param TicketDeleteRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(TicketDeleteRequest $request)
    {
        $ticket = $request->model;

        $ticket->delete();

        $response = array_merge($ticket->api_array, [
             'deleted_at' => Carbon::parse($ticket->deleted_at)->toIso8601String(),
        ]);

        return response()->json(
             $this->success([$response])
        );
    }



    /**
     * Finds a top ticket with its hashid.
     *
     * @param string $hashid
     *
     * @return Ticket
     */
    protected function findTicketByHashid(string $hashid)
    {
        return model('ticket')
             ->topItems()
             ->where('entity_id', TicketTypeServiceProvider::getRequestTicketType()->id)
             ->grabHashid($hashid)
             ;
    }
}
