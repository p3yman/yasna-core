<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/16/18
 * Time: 3:49 PM
 */

namespace Modules\Tickets\Http\Controllers\Api;


use Modules\Tickets\Providers\TicketsServiceProvider;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class FlagsController extends YasnaApiController
{
    /**
     * Returns a response containing flags with their information.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return response()->json($this->success($this->ticketsInfoList()));
    }



    /**
     * Returns a list of flags with their information.
     *
     * @return array
     */
    protected function ticketsInfoList()
    {
        return array_map(function ($type_slug) {
            return TicketsServiceProvider::getFlagApiInfo($type_slug);
        }, ticket()->remarkPossibleFlags());
    }



    /**
     * Returns a response containing the information of the requested flag.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function single(SimpleYasnaRequest $request)
    {
        $slug = $request->slug;

        if ($this->flagIsNotValid($slug)) {
            return $this->clientError(1004);
        }

        return response()->json(
             $this->success([TicketsServiceProvider::getFlagApiInfo($slug)])
        );
    }



    /**
     * Whether the flag is a valid flag.
     *
     * @param string $slug
     *
     * @return bool
     */
    protected function flagIsValid(string $slug)
    {
        return in_array($slug, ticket()->remarkPossibleFlags());
    }



    /**
     * Whether the flag is not a valid flag.
     *
     * @param string $slug
     *
     * @return bool
     */
    protected function flagIsNotValid(string $slug)
    {
        return !$this->flagIsValid($slug);
    }
}
