<?php

namespace Modules\Tickets\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Yasna\Services\YasnaController;

class TicketsBrowseController extends YasnaController
{
    protected $base_model  = "ticket";
    protected $view_folder = "tickets::tickets.browse";

    protected $request_type;
    protected $request_tab;
    protected $ticket_type_model;
    protected $page = [];
    protected $builder;
    protected $models;
    protected $view_arguments;



    /**
     * Check if the specified type is valid.
     *
     * @return bool
     */
    protected function typeIsValid()
    {
        if ($this->request_type == 'all') {
            return true;
        } else {
            $this->ticket_type_model = model('ticket-type')->grabSlug($this->request_type);

            return $this->ticket_type_model->exists;
        }
    }



    /**
     * Check if the specified type is not valid.
     *
     * @return bool
     */
    protected function typeIsNotValid()
    {
        return !$this->typeIsValid();
    }



    /**
     * Check if the logged in user has access to the requested page.
     *
     * @return bool
     */
    protected function hasPermission()
    {
        if ($this->request_type == 'all') {
            return model('ticket-type')->anyAccessible();
        } else {
            return $this->ticket_type_model->isAccessible();
        }
    }



    /**
     * Check if the logged in user has not access to the requested page.
     *
     * @return bool
     */
    protected function hasNotPermission()
    {
        return !$this->hasPermission();
    }



    /**
     * The URI for tabs.
     *
     * @return string
     */
    protected function routeUri()
    {
        $url = route('tickets.browse', [
             'type' => '__TICKET_TYPE_',
             'flag' => '__FLAG__',
        ], false);

        return str_after($url, 'manage/');
    }



    /**
     * Generate the link of each tab.
     *
     * @param string $type
     * @param string $flag
     *
     * @return string
     */
    protected function generatePageLink($type, $flag = '')
    {
        $uri      = $this->routeUri();
        $replaced = str_replace(['__TICKET_TYPE_', '__FLAG__'], [$type, $flag], $uri);

        return rtrim($replaced, '/');
    }



    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    protected function buildPageInfo()
    {
        $this->page[0] = [
             $url = $this->generatePageLink($this->request_type),
             ($this->ticket_type_model ? $this->ticket_type_model->title : trans('tickets::general.all-tickets')),
             $url,
        ];

        $this->page[1] = [
             $this->request_tab,
             trans("tickets::flags.$this->request_tab"),
             $url,
             $this->generatePageLink($this->request_type, $this->request_tab),
        ];
    }



    /**
     * Make the query builder to select tickets.
     */
    protected function makeQueryBuilder()
    {
        $this->builder = model('ticket')
             ->elector([
                  'ticket_type' => $this->request_type,
                  'type'        => 'default',
                  'flag'        => $this->request_tab,
             ])->where('parent_id', 0)
             ->where('master_id', 0)
             ->orderByDesc('created_at')
        ;

    }



    /**
     * Make the collection from the generated query builder.
     */
    protected function makeCollection()
    {
        $this->models = $this->builder->simplePaginate(20);
    }



    /**
     * Builds view arguments and stores in $this->view_arguments
     */
    protected function buildViewArguments()
    {
        $this->view_arguments = [
             "page"               => $this->page,
             "models"             => $this->models,
             "ticket_type"        => $this->ticket_type_model,
             "ticket_type_string" => $this->request_type,
             "request_type"       => $this->request_tab,
             "controller"         => $this,
             "has_permission"     => $this->hasPermission(),
        ];
    }



    /**
     * Return the default tab to be used when the tab has not been specified.
     *
     * @return string
     */
    protected function defaultTab()
    {
        return 'new';
    }



    /**
     * Browse the tickets.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function browse(Request $request)
    {
        $this->request_type = $request->type;
        $this->request_tab  = ($request->flag ?? $this->defaultTab());

        // Check existence of the requested ticket type.
        if ($this->typeIsNotValid()) {
            return $this->abort(404);
        }

        // Check if the logged in user can preview this page.
        if ($this->hasNotPermission()) {
            return $this->abort(403);
        }

        $this->buildPageInfo();
        $this->makeQueryBuilder();
        $this->makeCollection();
        $this->buildViewArguments();


        return $this->view('index', $this->view_arguments);
    }
}
