<?php

namespace Modules\Tickets\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class TicketsController extends YasnaController
{
    /**
     * Displays Single page of a ticket
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single()
    {
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("tickets:js/tickets.min.js")
             ->order(101)
        ;
        return $this->view('tickets::single.main');
    }
}
