<?php

namespace Modules\Tickets\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Tickets\Http\Requests\NotificationChannelsRequest;
use Modules\Tickets\Http\Requests\TicketTypeDeleteRequest;
use Modules\Tickets\Http\Requests\TicketTypeHardDeleteRequest;
use Modules\Tickets\Http\Requests\TicketTypeSaveRequest;
use Modules\Tickets\Http\Requests\TicketTypeSaveSupportersRequest;
use Modules\Yasna\Services\YasnaController;

class TicketTypesController extends YasnaController
{
    /**
     * The base model.
     *
     * @var string
     */
    protected $base_model = "ticket-type";
    /**
     * The prefix of the view files in this controller.
     *
     * @var string
     */
    protected $view_folder = "tickets::ticket-types";



    /**
     * Fine the models to be rendered in the browse grid.
     *
     * @return Collection
     */
    protected static function findModels()
    {
        return model("ticket-type")
             ->orderBy('created_at', 'desc')
             ->withTrashed()
             ->simplePaginate(20)
             ;
    }



    /**
     * The method which will be using in the related downstream tab.
     *
     * @return array
     */
    public static function downstream()
    {
        // View File
        $variables['view_file'] = "tickets::ticket-types.downstream";

        // Models
        $variables['models'] = static::findModels();


        return $variables;
    }



    /**
     * The root form of the create method.
     * <br>
     * This method will be triggered the edit method for a new model object.
     *
     * @param string $model_hashid
     * @param array  ...$options
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createRootForm($model_hashid, ...$options)
    {
        return $this->singleAction($model_hashid, 'edit', ...$options);
    }



    /**
     * Save a ticket type.
     *
     * @param TicketTypeSaveRequest $request
     *
     * @return string
     */
    public function save(TicketTypeSaveRequest $request)
    {
        $old_model = $request->model;
        $editing   = $old_model->exists;
        $model     = $old_model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($model->exists, [
             "success_callback" => ($editing ? "rowUpdate('auto','$model->hashid')" : "divReload('browse-grid')"),
        ]);
    }



    /**
     * Return the grid for browsing the ticket types.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function browse()
    {
        $models = static::findModels();

        return $this->safeView($this->view_folder . ".browse", compact('models'));
    }



    /**
     * Delete or undelete a ticket type.
     *
     * @param TicketTypeDeleteRequest $request
     *
     * @return string
     */
    public function deleteOrUndelete(TicketTypeDeleteRequest $request)
    {
        if ($request->_submit == 'delete') {
            $ok = $request->model->delete();
        } else {
            $ok = $request->model->restore();
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('auto','$request->model_hashid')",
        ]);
    }



    /**
     * Hard delete the specified ticket type.
     *
     * @param TicketTypeHardDeleteRequest $request
     *
     * @return string
     */
    public function hardDelete(TicketTypeHardDeleteRequest $request)
    {
        if ($this->isNotReadyToHardDelete()) {
            return $this->abort(403);
        }

        $ok = $request->model->hardDelete();

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "divReload('browse-grid')",
        ]);
    }



    /**
     * Save the roles which are supporters of the specified ticket type.
     *
     * @param TicketTypeSaveSupportersRequest $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function saveSupporters(TicketTypeSaveSupportersRequest $request)
    {
        $model = $request->model;

        // Check the model existence
        if ($model->not_exists) {
            return $this->abort(410);
        }

        // Find the IDs of roles to be saved.
        $role_ids = $this->saveSupportersIds($request);

        // Save selected roles
        try {
            $model->roles()->sync($role_ids);
            $saved = true;
        } catch (QueryException $exception) {
            // This exception occurs when some IDs is not valid.
            $saved = false;
        }

        // Return the result.
        return $this->jsonAjaxSaveFeedback($saved, [
             'success_message'  => trans('tickets::general.supporters-save-succeeded'),
             'success_callback' => "rowUpdate('auto', '$model->hashid')",

             'danger_message' => trans('tickets::general.supporters-save-failed'),
        ]);
    }



    /**
     * Find the IDs from the given hashids of roles.
     *
     * @param Request $request
     *
     * @return array
     */
    protected function saveSupportersIds(Request $request)
    {
        $requested_data = ($request->roles ?? []);
        $ids            = [];

        foreach ($requested_data as $hashid => $value) {
            if ($value) {
                $ids[] = hashid($hashid);
            }
        }

        return $ids;
    }



    /**
     * Sets the setting of the notification channels.
     *
     * @param NotificationChannelsRequest $request
     *
     * @return string
     */
    public function notificationChannels(NotificationChannelsRequest $request)
    {
        $setting_value = collect($request->channels)
             ->filter()
             ->keys()
             ->implode(HTML_LINE_BREAK)
        ;

        $ok = setting('ticket_notification_channels')->setCustomValue($setting_value);

        return $this->jsonAjaxSaveFeedback($ok);
    }
}
