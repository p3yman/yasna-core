<?php

namespace Modules\Tickets\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class TicketsSidebarController extends YasnaController
{
    /**
     * @var array All retrieved submenus.
     */
    protected $all_submenus = [];



    /**
     * Return a collection of all tickets type which the logged in user can access them.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    protected function ticketTypesCollection()
    {
        return model('ticket-type')->get();
    }



    /**
     * Return the URI of the routes of each submenu.
     *
     * @return string
     */
    protected function routeUri()
    {
        $url = route('tickets.browse', ['type' => '__TICKET_TYPE_'], false);

        return str_after($url, 'manage/');
    }



    /**
     * Add submenu for each ticket type.
     */
    protected function ticketTypesOneByOne()
    {
        $link_uri = $this->routeUri();

        foreach ($this->ticketTypesCollection() as $ticket_type) {
            if (!$ticket_type->isAccessible()) {
                continue;
            }


            $this->all_submenus[$ticket_type->slug] = [
                 'caption' => $ticket_type->title,
                 'link'    => str_replace('__TICKET_TYPE_', $ticket_type->slug, $link_uri),
            ];
        }
    }



    /**
     * Add the submenu to show all tickets of all accessible submenus.
     */
    protected function allTickets()
    {
        if (!model('ticket-type')->anyAccessible()) {
            return;
        }

        $link_uri = $this->routeUri();

        $this->all_submenus['all'] = [
             'caption' => trans('tickets::general.all-tickets'),
             'link'    => str_replace('__TICKET_TYPE_', 'all', $link_uri),
        ];
    }



    /**
     * Render tickets submenus.
     *
     * @return array
     */
    public function render()
    {
        $this->ticketTypesOneByOne();
        $this->allTickets();

        return $this->all_submenus;
    }
}
