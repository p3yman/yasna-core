<?php

namespace Modules\Tickets\Http\Controllers;

use App\Models\Ticket;
use Modules\Tickets\Entities\TicketType;
use Modules\Tickets\Http\Requests\TicketActionRequest;
use Modules\Tickets\Http\Requests\TicketCreateRequest;
use Modules\Tickets\Http\Requests\TicketReplyRequest;
use Modules\Tickets\Http\Requests\TicketSaveRequest;
use Modules\Tickets\Providers\TicketsServiceProvider;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class TicketsActionController extends YasnaController
{
    protected $base_model  = "ticket";
    protected $view_folder = "tickets::tickets";



    /**
     * Return the URL to preview the given ticket.
     *
     * @param Ticket $model
     *
     * @return string
     */
    protected function previewUrl(Ticket $model)
    {
        return route('tickets.single-action', [
             'model_id' => $model->hashid,
             'action'   => 'preview',
        ]);
    }



    /**
     * return come javascript codes to reload the ticket modal.
     *
     * @param Ticket $model
     *
     * @return string
     */
    protected function reloadModalCallback(Ticket $model)
    {
        $url = $this->previewUrl($model);

        return <<<JS
masterModal("$url");
JS
             ;
    }



    /**
     * Change the the ticket type of the requested ticket.
     *
     * @param TicketActionRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function changeTicketType(TicketActionRequest $request)
    {
        $model       = $request->model;
        $ticket_type = model('ticket-type')->grabHashid($request->ticket_type);

        if ($model->not_exists or $ticket_type->not_exists) {
            return $this->abort(404);
        }

        $model->batchSave([
             'entity_id' => $ticket_type->id,
        ]);

        $model->addReply([
             'type' => 'changing-ticket-type',
        ]);

        return [
             'message'  => trans('tickets::general.department-changed'),
             'callback' => $this->reloadModalCallback($model),
        ];
    }



    /**
     * Change the flag of the requested ticket.
     *
     * @param TicketActionRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function changeFlag(TicketActionRequest $request)
    {
        $model = $request->model;

        if ($model->not_exists) {
            return $this->abort(404);
        }

        $model->markAs($request->flag);

        return [
             'message'  => trans('tickets::general.priority-changed'),
             'callback' => $this->reloadModalCallback($model),
        ];
    }



    /**
     * Reopen the requested ticket.
     *
     * @param TicketActionRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function reopen(TicketActionRequest $request)
    {
        $model = $request->model;

        if ($model->not_exists) {
            return $this->abort(404);
        }

        $flag = $model->lastFlagBeforeDone();
        $model->markAs($flag);

        return [
             'message'  => trans('tickets::general.opened'),
             'callback' => $this->reloadModalCallback($model),
        ];
    }



    /**
     * Mark the requested ticket as new.
     *
     * @param TicketActionRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function markAsNew(TicketActionRequest $request)
    {
        $model = $request->model;

        if ($model->not_exists) {
            return $this->abort(404);
        }

        $model->markAs('new');

        return [
             'message'  => trans('tickets::general.flag-change-to-new'),
             'callback' => $this->reloadModalCallback($model),
        ];
    }



    /**
     * Close the requested ticket.
     *
     * @param TicketActionRequest $request
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function close(TicketActionRequest $request)
    {
        $model = $request->model;

        if ($model->not_exists) {
            return $this->abort(404);
        }

        $model->markAs('done');

        return [
             'message'  => trans('tickets::general.closed'),
             'callback' => $this->reloadModalCallback($model),
        ];
    }



    /**
     * Save a reply to a ticket.
     *
     * @param TicketReplyRequest $request
     *
     * @return string
     */
    public function reply(TicketReplyRequest $request)
    {
        $data                = $request->only(['text']);
        $data['attachments'] = $this->replySaveAttachments($request);
        $new_model           = $request->model->addReply($data);

        return $this->jsonAjaxSaveFeedback($new_model->exists, [
             'success_message'    => trans('tickets::general.reply-saved'),
             'success_callback'   => 'reloadTimeline()',
             'success_modalClose' => false,
        ]);
    }



    /**
     * Saves and filters attachments array.
     *
     * @param TicketReplyRequest $request
     *
     * @return array|null
     */
    protected function replySaveAttachments(TicketReplyRequest $request)
    {
        $attachments = $request->attachments;
        if (!$attachments) {
            return null;
        }

        foreach ($attachments as $key => $hashid) {
            if (fileManager()->file($hashid)->permanentSave()) {
                continue;
            }

            unset($attachments[$key]);
        }


        return $attachments;
    }



    /**
     * Renders a preview of attachments for reply.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function attachment(SimpleYasnaRequest $request)
    {
        $hashid     = $request->hashid;
        $number     = $request->number;
        $with_input = true;

        return $this->view('attachment-item', compact('hashid', 'number', 'with_input'));
    }



    /**
     * Show a modal for filling information of a ticket for a customer
     *
     * @param TicketCreateRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create(TicketCreateRequest $request)
    {
        $ticket_types = model('ticket_type')
             ->get()
             ->pluck('api_array')
             ->toArray()
        ;
        // TODO: make this field more general to support more identifier as username
        $usernameField = user()->usernameField();
        $priorities    = TicketsServiceProvider::prioritiesCombo();

        return $this->view('create',
             compact('ticket_types', 'usernameField', 'priorities')
        );
    }



    /**
     * Save a given request as a ticket from a customer
     *
     * @param TicketSaveRequest $request
     *
     * @return string
     */
    public function save(TicketSaveRequest $request)
    {
        // get data of ticket
        $data = $this->prepareDataForCreateTicket($request);

        // if the user is invalid show an error
        if (is_null($data)) {
            return $this->jsonFeedback(trans('tickets::general.user_not_found'));
        }

        /* @var TicketType $ticket */
        $ticket = model('ticket_type')->grabSlug($request->ticket_type);

        // save ticket
        $remark = $ticket->addRemark($data);

        // create the link of timeline modal which must be showed after success saving
        $url = route('tickets.single-action', [
             'model_id' => $remark->hashid,
             'action'   => 'preview',
        ]);

        return $this->jsonAjaxSaveFeedback($remark->exists, [
             'success_message'    => trans('tickets::general.opened'),
             'success_callback'   => sprintf('masterModal("%s")', $url),
             'success_modalClose' => false,
        ]);
    }



    /**
     * Prepare an array of data which must be used to save a ticket from the
     * management panel
     *
     * @param TicketSaveRequest $request
     *
     * @return array
     */
    private function prepareDataForCreateTicket(TicketSaveRequest $request
    ): ?array {
        $user = user()->grab(
             $request->username,
             user()->usernameField()
        );

        if (!$user->exists) {
            return null;
        }

        $data                = $request->only([
             'text',
             'title',
        ]);
        $data['attachments'] = $this->replySaveAttachments($request);
        $data['user_id']     = $user->id;
        $data['type']        = 'default';
        $data['flag']        = $request->priority;

        return $data;
    }
}
