<?php

namespace Modules\Tickets\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Tickets\Providers\TicketTypeServiceProvider;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class CheckTicketTypeMiddleware
{
    use ModuleRecognitionsTrait;



    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $type_slug = $request->type;

        if ($this->typeIsNotValid($type_slug)) {
            return api()
                 ->inModule($this->runningModuleName())
                 ->clientErrorRespond(1005)
                 ;
        }

        return $next($request);
    }



    /**
     * Whether the type is valid.
     *
     * @param string $type_slug
     *
     * @return bool
     */
    protected function typeIsValid(string $type_slug)
    {
        $type = TicketTypeServiceProvider::findAndKeep($type_slug);

        TicketTypeServiceProvider::setRequestTicketType($type);

        return $type->exists;
    }



    /**
     * Whether the type is not valid.
     *
     * @param string $type_slug
     *
     * @return bool
     */
    protected function typeIsNotValid(string $type_slug)
    {
        return !$this->typeIsValid($type_slug);
    }
}
