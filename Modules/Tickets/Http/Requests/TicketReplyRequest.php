<?php

namespace Modules\Tickets\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketReplyRequest extends YasnaRequest
{
    protected $model_name = "ticket";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return ($this->model->exists and $this->model->isAccessible());
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'text'        => 'required',
             'attachments' => 'array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'attachments' => $this->runningModule()->getTrans('general.attachments.title.plural')
        ];
    }
}
