<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/16/18
 * Time: 4:28 PM
 */

namespace Modules\Tickets\Http\Requests\Api;


use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class TicketChangeFlagRequest extends YasnaRequest
{
    protected $model_name = 'ticket';
    protected $responder  = 'white-house';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'hashid' => [
                  'required',
                  Rule::in(
                       $this->topTicketsIds()
                  ),
             ],
             'flag'   => [
                  'required',
                  Rule::in(ticket()->remarkPossibleFlags()),
             ],
        ];
    }



    /**
     * Returns a list of top tickets' ids.
     *
     * @return array
     */
    protected function topTicketsIds()
    {
        return model('ticket')
             ->topItems()
             ->get()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'hashid' => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['flag'];
    }



    /**
     * @inheritdoc
     */
    public function guardedFields()
    {
        return ['id'];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        return ($model->exists and $model->isViewableForClient());
    }
}
