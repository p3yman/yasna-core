<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/15/18
 * Time: 4:20 PM
 */

namespace Modules\Tickets\Http\Requests\Api;

use Closure;
use Illuminate\Validation\Rule;
use Modules\Tickets\Providers\TicketsServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class TicketSaveRequest extends YasnaRequest
{
    protected $responder = 'white-house';
    const ATTACHMENTS_PREFIX = 'attachments_';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(
             $this->generaRules(),
             $this->attachmentsRules()
        );
    }



    /**
     * Returns the general rules.
     *
     * @return array
     */
    public function generaRules()
    {
        return [
             'title' => 'required',
             'text'  => 'required',
        ];
    }



    /**
     * Returns the rules for attachments
     *
     * @return array
     */
    public function attachmentsRules()
    {
        return array_fill_keys($this->getAttachmentKeys(), $this->attachmentValidationRule());
    }



    /**
     * Returns the validation rule for attachments.
     *
     * @return Closure
     */
    protected function attachmentValidationRule()
    {
        return function ($attribute, $value, $fail) {
            $uploaded_file = $this->file($attribute);
            if (!$uploaded_file) {
                $fail($this->notFileErrorMessage($attribute));
                return false;
            }

            $uploader = TicketsServiceProvider::getVirtualUploader($uploaded_file);
            $is_valid = $uploader->isValid();

            if (!$is_valid) {
                $fail(implode(' ', $uploader->getErrors()));
            }

            return $is_valid;
        };
    }



    /**
     * Returns the keys of attachments.
     *
     * @return array
     */
    public function getAttachmentKeys()
    {
        return array_values(array_filter(array_keys($this->all()), function ($key) {
            return starts_with($key, static::ATTACHMENTS_PREFIX);
        }));
    }



    /**
     * Returns the error for an attachment which is not a file.
     *
     * @param string $attribute
     *
     * @return string
     */
    protected function notFileErrorMessage(string $attribute)
    {
        $number = str_after($attribute, static::ATTACHMENTS_PREFIX);

        return $this
             ->runningModule()
             ->getTrans('general.attachments.not-a-file', [
                  'number' => ad($number),
             ])
             ;
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_merge($this->generalFillableFields(), $this->attachmentFillableFields());
    }



    /**
     * Returns the general fillable fields.
     *
     * @return array
     */
    public function generalFillableFields()
    {
        return ['title', 'text'];
    }



    /**
     * Returns the fillable fields based on attachments.
     *
     * @return array
     */
    protected function attachmentFillableFields()
    {
        return $this->getAttachmentKeys();
    }
}
