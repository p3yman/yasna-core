<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/17/18
 * Time: 10:30 AM
 */

namespace Modules\Tickets\Http\Requests\Api;


use Modules\Tickets\Providers\TicketTypeServiceProvider;

class TicketDeleteRequest extends TicketSingleActionRequest
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->not_exists or $model->ownedByClient());
    }



    /**
     * @inheritdoc
     */
    protected function findTicketById(string $id)
    {
        return model('ticket')
             ->where('entity_id', TicketTypeServiceProvider::getRequestTicketType()->id)
             ->grabId($id)
             ;
    }
}
