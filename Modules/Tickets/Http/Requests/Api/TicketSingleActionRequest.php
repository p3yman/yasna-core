<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/17/18
 * Time: 9:35 AM
 */

namespace Modules\Tickets\Http\Requests\Api;


use App\Models\TicketType;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Modules\Tickets\Entities\Ticket;
use Modules\Tickets\Providers\TicketTypeServiceProvider;
use Modules\Yasna\Services\YasnaRequest;
use Symfony\Component\HttpFoundation\JsonResponse;

class TicketSingleActionRequest extends YasnaRequest
{
    protected $model_name = 'ticket';
    protected $responder  = 'white-house';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'id' => [
                  'required',
                  Rule::exists($this->model->getTable()),
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->not_exists or $model->isViewableForClient());
    }



    /**
     * @inheritdoc
     */
    protected function failedValidation(Validator $validator)
    {
        if ($validator->errors()->has('id')) {
            $this->throwNotExistenceError($validator);
        }

        parent::failedValidation($validator);
    }



    /**
     * Throws a proper error about not existence of the ticket.
     *
     * @param Validator $validator
     *
     * @return void
     * @throws ValidationException
     */
    protected function throwNotExistenceError(Validator $validator)
    {
        $content  = $this->notExistenceErrorResponse();
        $response = new JsonResponse($content, 400);

        throw (new ValidationException($validator, $response));
    }



    /**
     * Returns an array containing a proper response for not existence of the requested ticket.
     *
     * @return array
     */
    protected function notExistenceErrorResponse()
    {
        return api()
             ->inModule($this->runningModuleName())
             ->errorArray(1003)
             ;
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        $id          = $this->getRequestedModelId($id_or_hashid);
        $this->model = $this->findTicketById($id);

        if ($this->model->not_exists) {
            return;
        }

        $this->data['id']    = $id;
        $this->data['model'] = $this->model;
        $this->refillRequestHashid();
    }



    /**
     * Finds a top ticket with its id.
     *
     * @param string $id
     *
     * @return Ticket
     */
    protected function findTicketById(string $id)
    {
        return model('ticket')
             ->topItems()
             ->where('entity_id', TicketTypeServiceProvider::getRequestTicketType()->id)
             ->grabId($id)
             ;
    }
}
