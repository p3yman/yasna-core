<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/16/18
 * Time: 5:45 PM
 */

namespace Modules\Tickets\Http\Requests\Api;


use App\Models\Ticket;
use Illuminate\Validation\Rule;
use Modules\Tickets\Providers\TicketTypeServiceProvider;

class TicketReplyRequest extends TicketSaveRequest
{
    protected $model_name = 'ticket';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->findTicketByHashid($this->hashid);

        return ($model->exists and $model->isViewableForClient());
    }



    /**
     * Finds a ticket with its hashid.
     *
     * @param string $hashid
     *
     * @return Ticket
     */
    protected function findTicketByHashid(string $hashid)
    {
        return model('ticket')
             ->withType(ticket()->guessDefaultRemarkType())
             ->where('entity_id', TicketTypeServiceProvider::getRequestTicketType()->id)
             ->grabHashid($hashid)
             ;
    }
}
