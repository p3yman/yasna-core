<?php

namespace Modules\Tickets\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketCreateRequest extends YasnaRequest
{
    protected $model_name = "ticket-type";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return model($this->model_name, $this->ticket_type)->isAccessible();
    }
}
