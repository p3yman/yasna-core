<?php

namespace Modules\Tickets\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class NotificationChannelsRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return !is_null(get_setting('ticket_notification_channels'));
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'channels' => 'required|array',
        ];
    }
}
