<?php

namespace Modules\Tickets\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketTypeDeleteRequest extends YasnaRequest
{
    protected $model_name = "ticket-type";
}
