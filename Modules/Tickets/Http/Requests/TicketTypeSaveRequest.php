<?php

namespace Modules\Tickets\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class TicketTypeSaveRequest extends YasnaRequest
{
    protected $model_name = "ticket-type";



    /**
     * Correct the titles.
     */
    public function correctTitle()
    {
        $title = ($this->data['titles'] ?? []);

        $this->data['titles'] = array_filter($title, function ($item) {
            return boolval($item);
        });
    }



    /**
     * Correct the data.
     */
    public function corrections()
    {
        $this->correctTitle();
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'titles.min' => trans('tickets::validation.at_least_one_required'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'titles' => trans('tickets::validation.attributes.titles'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'slug'   => [
                  'required',
                  Rule::unique('ticket_types')
                      ->whereNot('id', $this->id),
             ],
             'titles' => 'required|array|min:1',
        ];
    }
}
