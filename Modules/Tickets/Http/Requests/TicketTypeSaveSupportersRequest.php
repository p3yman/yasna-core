<?php

namespace Modules\Tickets\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketTypeSaveSupportersRequest extends YasnaRequest
{
    protected $model_name = "ticket-type";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'roles' => 'required|array',
        ];
    }
}
