<?php

namespace Modules\Tickets\Http\Requests;

class TicketSaveRequest extends TicketReplyRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'ticket_type' => 'required|string',
             'text'        => 'string',
             'attachments' => 'array',
             'username'    => 'required|string',
             'priority'    => 'string|required',
             'title'       => 'required|string',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return model("ticket_type")->isAccessible();
    }
}
