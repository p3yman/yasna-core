<?php

namespace Modules\Tickets\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketActionRequest extends YasnaRequest
{
    protected $model_name = "ticket";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return ($this->model->exists and $this->model->isAccessible());
    }
}
