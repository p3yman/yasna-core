<?php

namespace Modules\Tickets\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketTypeHardDeleteRequest extends YasnaRequest
{
    protected $model_name = "ticket-type";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->hasNotAnyTicket();
    }
}
