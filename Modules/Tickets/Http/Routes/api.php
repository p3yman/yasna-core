<?php
Route::group([
     'middleware' => ['api', 'WhiteHouseMiddleware'],
     'namespace'  => module('Tickets')->getControllersNamespace() . '\Api',
     'prefix'     => 'tickets/api/v1',
], function () {


    Route::group(['prefix' => 'types'], function () {
        Route::get('/', 'TypesController@list')->name('tickets.api.types.list');
        Route::get('{slug}', 'TypesController@single')->name('tickets.api.types.single');
    });

    Route::group(['prefix' => '{type}/tickets', 'middleware' => 'check-ticket-type'], function () {
        Route::get('/', 'TicketsController@list')->name('tickets.api.list');
        Route::post('/', 'TicketsController@save')->name('tickets.api.save');


        // Actions on a Single Ticket
        Route::group(['prefix' => '{hashid}'], function () {
            Route::get('/', 'TicketsController@single')->name('tickets.api.single');
            Route::delete('/', 'TicketsController@delete')->name('tickets.api.delete');

            Route::get('timeline', 'TicketsController@timeline')->name('tickets.api.single.timeline');

            Route::get('flag', 'TicketsController@flag')->name('tickets.api.single.flag');
            Route::post('flag', 'TicketsController@changeFlag')->name('tickets.api.single.change-flag');

            Route::group(['prefix' => 'replies'], function () {
                Route::get('/', 'TicketsController@replies')->name('tickets.api.single.replies');
                Route::post('/', 'TicketsController@reply')->name('tickets.api.single.reply');
            });
        });
    });

    // Actions About Flags
    Route::group(['prefix' => 'flags'], function () {
        Route::get('/', 'FlagsController@list')->name('tickets.api.flags');
        Route::get('{slug}', 'FlagsController@single')->name('tickets.api.flags.single');
    });
});
