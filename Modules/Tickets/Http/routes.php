<?php

Route::group([
     'middleware' => 'web',
     'prefix'     => 'manage/tickets',
     'namespace'  => 'Modules\Tickets\Http\Controllers',
], function () {
    Route::get('/single', 'TicketsController@single');
});

Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'prefix'     => 'manage/tickets/types',
     'namespace'  => 'Modules\Tickets\Http\Controllers',
], function () {
    Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'TicketTypesController@singleAction')
         ->name('tickets.types.single-action')
    ;

    Route::get('browse', 'TicketTypesController@browse')->name('tickets.types.browse');

    Route::post('save', 'TicketTypesController@save')->name('tickets.types.save');
    Route::post('delete', 'TicketTypesController@deleteOrUndelete')->name('tickets.types.delete');
    Route::post('notification-channels', 'TicketTypesController@notificationChannels')
         ->name('tickets.types.notification-channels')
    ;
});

Route::group([
     'middleware' => ['web', 'auth', 'is:super'],
     'prefix'     => 'manage/tickets/types',
     'namespace'  => 'Modules\Tickets\Http\Controllers',
], function () {
    Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'TicketTypesController@singleAction')
         ->name('tickets.types.single-action')
    ;

    Route::get('browse', 'TicketTypesController@browse')->name('tickets.types.browse');

    Route::post('save', 'TicketTypesController@save')->name('tickets.types.save');
    Route::post('delete', 'TicketTypesController@deleteOrUndelete')->name('tickets.types.delete');
    Route::post('hard-delete', 'TicketTypesController@hardDelete')->name('tickets.types.hard-delete');
    Route::post('supporters', 'TicketTypesController@saveSupporters')->name('tickets.types.supporters');
});

Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'prefix'     => 'manage/tickets',
     'namespace'  => 'Modules\Tickets\Http\Controllers',
], function () {

    Route::get('browse/{type}/{flag?}', 'TicketsBrowseController@browse')->name('tickets.browse');

    Route::get('browse/act/{model_id}/{action}/{option0?}/{option1?}/{option2?}',
         'TicketsBrowseController@singleAction')
         ->name('tickets.browse.single-action')
    ;

    Route::get('create/{ticket_type}', 'TicketsActionController@create')
         ->name('tickets.create')
    ;
    Route::post('save', 'TicketsActionController@save')
         ->name('tickets.save')
    ;

    Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'TicketsActionController@singleAction')
         ->name('tickets.single-action')
    ;

    Route::post('change-ticket-type', 'TicketsActionController@changeTicketType')->name('tickets.change-ticket-type');
    Route::post('change-flag', 'TicketsActionController@changeFlag')->name('tickets.change-flag');
    Route::post('mark-as-new', 'TicketsActionController@markAsNew')->name('tickets.mark-as-new');
    Route::post('reopen', 'TicketsActionController@reopen')->name('tickets.reopen');
    Route::post('close', 'TicketsActionController@close')->name('tickets.close');

    Route::post('reply', 'TicketsActionController@reply')->name('tickets.reply');

    Route::get('attachment/{number}/{hashid}', 'TicketsActionController@attachment')->name('tickets.attachment');
});
