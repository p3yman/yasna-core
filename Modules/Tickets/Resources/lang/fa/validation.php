<?php
return [
     "attributes" => [
          "titles" => 'عنوان‌ها',
     ],

     "at_least_one_required" => "حداقل یکی از :attribute الزامی است.",
];
