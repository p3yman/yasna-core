<?php
return [
     "locales" => [
          "fa" => "فارسی",
          "en" => "انگلیسی",
     ],

     "form" => [
          "title"                          => "عنوان",
          "message"                        => "پیام",
          "title_in"                       => "عنوان :locale",
          "username_field"                 => [
               "email"      => "ایمیل مخاطب",
               "username"   => "نام کاربری مخاطب",
               "code_melli" => "کد ملی مخاطب",
               "mobile"     => "تلفن همراه مخاطب",
          ],
          "username_field_hint_email"      => "ایمیلی که با آن مخاطب در سیستم ثبت شده است را وارد نمائید.",
          "username_field_hint_username"   => "نام کاربری‌ای که با آن مخاطب در سیستم ثبت شده است را وارد نمائید.",
          "username_field_hint_code_melli" => "کد ملی ثبت‌شده از مخاطب در سیستم را وارد نمائید.",
          "username_field_hint_mobile"     => "شماره‌ی تلفن همراهی از مخاطب که برای وی در سیستم ثبت شده است را وارد نمائید.",
     ],

     "attach" => "ضمیمه",
     "send"   => "ارسال",

     "user_not_found" => "کاربری با این مشخصات یافت نشد.",

     "close_ticket"              => "بستن تیکت",
     "open_ticket"               => "گشایش تیکت",
     "create_ticket"             => "ایجاد تیکت",
     "create_ticket_label"       => "ایجاد تیکت جدید برای مشتری",
     "reopen"                    => "بازگشایی تیکت",
     "change_department"         => "تغییر دپارتمان",
     "change_priority"           => "تغییر اولویت",
     "mark_as_unread"            => "تغییر به خوانده‌نشده",
     "answer"                    => "پاسخ دادن",
     "tickets"                   => "تیکت‌ها",
     "ticket"                    => "تیکت",
     "ticket-type"               => "گونه‌ی تیکت",
     "ticket-types"              => "گونه‌های تیکت‌ها",
     "edit-ticket-type"          => "ویرایش نوع تیکیت «:title»",
     "new-ticket-type"           => "افزودن نوع تیکت",
     "supporters"                => "پشتیبان‌ها",
     "supporters-of"             => "پشتیبان‌های :title",
     "supporters-save-succeeded" => "تغییرات در پشتیبان‌ها ذخیره شد.",
     "supporters-save-failed"    => "ذخیره‌ی تغییرات در پیشتیبان‌ها با خطا مواجه شد.",
     "all-tickets"               => "همه‌ی تیکت‌ها",
     "priority"                  => "اولویت",
     "labels"                    => "برچسب‌ها",
     "preview"                   => "نمایش",
     "department-changed"        => "دپارتمان تغییر کرد.",
     "priority-changed"          => "اولیت تغییر کرد.",
     "closed"                    => "تیکت بسته شد.",
     "opened"                    => "تیکت باز شد.",
     "flag-change-to-new"        => "تیکت به عنوان «خوانده‌نشده» نشانه‌گذاری شد.",
     "reply-saved"               => "پاسخ ذخیره شد.",
     "ticker-information"        => "جزئیات تیکت",
     "change-ticket-type"        => "تغییر نوع",
     "type-changed-to"           => "نوع تیکت به «:type» تغییر کرد.",

     "attachments" => [
          "title"      => [
               "plural"   => "فایل‌های ضمیمه",
               "singular" => "فایل ضمیمه",
          ],
          "of"         => [
               "plural"   => "فایل‌های ضمیمه‌ی :title",
               "singular" => "فایل ضمیمه‌ی :title",
          ],
          "not-a-file" => "ضمیمه‌ی شماره‌ی :number باید یک فایل باشد.",
     ],

     "notification-channels" => [
          "title" => [
               "plural" => "کانال‌های ارسال اعلان",
          ],

          "channels" => [
               "sms"  => "پیامک",
               "mail" => "ایمیل",
          ],
     ],
];
