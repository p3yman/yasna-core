<?php
return [
     "dummy"        => "ساختگی",
     "dummy-ticket" => "تیکت ساختگی",
     "text-of"      => "متن :title",
     "answer-of"    => "پاسخ :title",

     "notification-channels"      => "کانال‌های اعلان تیکت",
     "notification-channels-help" => "کانال‌هایی که بعد از ثبت تیکت از طریق آن‌ها به کاربر اطلاع‌رسانی می‌شود.",
];
