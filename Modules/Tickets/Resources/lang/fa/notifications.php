<?php
return [
     "sms" => [
          "creation" => "تیکت شما با عنوان «:title» ثبت شد. پاسخ پشتیبانی برای شما ارسال خواهد شد.",

          "flag-changing" => [
               "new"             => "تیکت «:title» به عنوان خوانده‌نشده نشانه‌گزاری شد.",
               "high-priority"   => "وضعیت تیکت «:title» به «فوری» تغییر پیدا کرد.",
               "medium-priority" => "وضعیت تیکت «:title» به «معمولی» تغییر پیدا کرد.",
               "low-priority"    => "وضعیت تیکت «:title» به «کم‌اهمیت» تغییر پیدا کرد.",
               "dome"            => "تیکت «:title» بسته شد.",
          ],

          "flag-changing-by" => [
               "new"             => ":name تیکت «:title» را به عنوان خوانده‌نشده نشانه‌گزاری کرد.",
               "high-priority"   => ":name وضعیت تیکت «:title» را به «فوری» تغییر داد.",
               "medium-priority" => ":name وضعیت تیکت «:title» را به «معمولی» تغییر داد.",
               "low-priority"    => ":name وضعیت تیکت «:title» را به «کم‌اهمیت» تغییر داد.",
               "dome"            => ":name تیکت «:title» را بست.",
          ],

          "replied" => "تیکت «:title» پاسخ داده شد",

          "replied-by" => ":name به تیکت «:title» پاسخ داد",
     ],

     "mail" => [
          "greeting"   => "سلام،",
          "salutation" => "با احترام، :site_title",

          "creation-subject" => "اعلان ثبت تیکت",
          "creation-content" => [
               "line1" => "تیکت شما با عنوان «:title» ثبت شد. پاسخ پشتیبانی برای شما ارسال خواهد شد.",
          ],

          "flag-changing-subject" => "اعلان تغییر وضعیت تیکت",

          "reply-subject" => "اعلان پاسخ تیکت",
     ],
];
