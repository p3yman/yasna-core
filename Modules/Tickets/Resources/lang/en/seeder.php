<?php
return [
     "dummy"        => "Dummy",
     "dummy-ticket" => "Dummy Ticket",
     "text-of"      => "Text of :title",
     "answer-of"    => "Answer of :title",

     "notification-channels"      => "Ticket Notification Channels",
     "notification-channels-help" => "The channels which submitting tickets will be notified through them.",
];
