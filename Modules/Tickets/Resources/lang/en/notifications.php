<?php
return [
     "sms" => [
          "creation" => "Your ticket with title \":title\" has been submitted. Supporters' answered will be sent to you.",

          "flag-changing" => [
               "new"             => "The ticket with title \":title\" has been marked as new.",
               "high-priority"   => "The ticket with title \":title\" has been changed to High-Priority status.",
               "medium-priority" => "The ticket with title \":title\" has been changed to Medium-Priority status.",
               "low-priority"    => "The ticket with title \":title\" has been changed to Low-Priority status.",
               "dome"            => "The ticket with title \":title\" has been closed.",
          ],

          "flag-changing-by" => [
               "new"             => "The ticket with title \":title\" has been marked as new by :name.",
               "high-priority"   => "The ticket with title \":title\" has been changed to High-Priority status by :name.",
               "medium-priority" => "The ticket with title \":title\" has been changed to Medium-Priority status by :name.",
               "low-priority"    => "The ticket with title \":title\" has been changed to Low-Priority status by :name.",
               "dome"            => "The ticket with title \":title\" has been closed by :name.",
          ],

          "replied" => "The ticket with title \":title\" has been replied",

          "replied-by" => ":name replied the ticket with title \":title\"",
     ],

     "mail" => [
          "greeting"   => "Hello!",
          "salutation" => "Regards, :site_title",

          "creation-subject" => "Ticket Submission Notification",
          "creation-content" => [
               "line1" => "Your ticket with title \":title\" has been submitted. Supports reply will be sent to you.",
          ],

          "flag-changing-subject" => "Ticket Process Notification",

          "reply-subject" => "Ticket Reply Notification",
     ],
];
