<?php
return [
     "1001" => "The action is unauthorized.",
     "1002" => "The given data was invalid.",
     "1003" => "Ticket not found.",
     "1004" => "Flag not found.",
     "1005" => "Ticket type not found.",
];
