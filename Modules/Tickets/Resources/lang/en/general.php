<?php
return [
     "locales" => [
          "fa" => "English",
          "en" => "Persian",
     ],

     "form" => [
          "title"                          => "Title",
          "message"                        => "Message",
          "title_in"                       => "Title in :locale",
          "username_field"                 => [
               "email"      => "Client Email",
               "username"   => "Client Username",
               "code_melli" => "Client National ID",
               "mobile"     => "Client Cell Phone Number",
          ],
          "username_field_hint_email"      => "Enter the e-mail that the client have been registered on the system by it.",
          "username_field_hint_username"   => "Enter the username of the client that is registered for him on the system.",
          "username_field_hint_code_melli" => "Enter the national ID of the client which is registered on the system for him.",
          "username_field_hint_mobile"     => "Enter the cell phone number of the client which is registered on the system.",

     ],

     "attach" => "Attach",
     "send"   => "Send",

     "user_not_found" => "No user has been found on the system.",

     "close_ticket"              => "Close ticket",
     "open_ticket"               => "Open ticket",
     "create_ticket"             => "Create Ticket",
     "create_ticket_label"       => "Create a New Ticket for Customer",
     "reopen"                    => "Reopen ticket",
     "change_department"         => "Change fepartment",
     "change_priority"           => "Chane priority",
     "mark_as_unread"            => "Mark as unread",
     "answer"                    => "Answer",
     "tickets"                   => "Tickets",
     "ticket"                    => "Ticket",
     "ticket-type"               => "Ticket Type",
     "ticket-types"              => "Ticket Types",
     "edit-ticket-type"          => "Edit Ticket \":title\" Type ",
     "new-ticket-type"           => "New Ticket Type",
     "supporters"                => "Supporters",
     "supporters-of"             => "Supporters of :title",
     "supporters-save-succeeded" => "Changes in supporters has been saved.",
     "supporters-save-failed"    => "Saving changes in supporters has failed due to an error.",
     "all-tickets"               => "All Tickets",
     "priority"                  => "Priority",
     "labels"                    => "Labels",
     "preview"                   => "Preview",
     "department-changed"        => "Department has been changed.",
     "priority-changed"          => "Priority has been changed.",
     "closed"                    => "Ticket has been closed.",
     "opened"                    => "Ticket has been opened.",
     "flag-change-to-new"        => "Ticket has been marked as unread.",
     "reply-saved"               => "Reply has been saved.",
     "ticker-information"        => "Ticket Information",
     "change-ticket-type"        => "Change Ticket Type",
     "type-changed-to"           => "Ticket type has been changed to \":type\".",

     "attachments" => [
          "title"      => [
               "plural"   => "Attachments",
               "singular" => "Attachment",
          ],
          "of"         => [
               "plural"   => "Attachments of :title",
               "singular" => "Attachment of :title",
          ],
          "not-a-file" => "The attachment number :number should be a file.",
     ],

     "notification-channels" => [
          "title" => [
               "plural" => "Notification Channels",
          ],

          "channels" => [
               "sms"   => "SMS",
               "email" => "Email",
          ],
     ],
];
