<?php
return [
     "attributes" => [
          "titles" => 'Titles',
     ],

     "at_least_one_required" => "At least one of the :attribute is required.",
];
