<?php
return [
     "new"             => "New",
     "high-priority"   => "High Priority",
     "medium-priority" => "Medium Priority",
     "low-priority"    => "Low Priority",
     "done"            => "Done",

     "changing" => [
          "new"             => "Ticket has been marked as unread.",
          "high-priority"   => "Ticket has been changed to \"High Priority\"",
          "medium-priority" => "Ticket has been changed to \"Medium Priority\"",
          "low-priority"    => "Ticket has been changed to \"Low Priority\"",
          "done"            => "Ticket has been changed to closed.",
     ],
];
