<div class="panel panel-default">
    <div class="panel-body p-xl">
        <h3 class="mt0 mb-lg">
            {{ $ticket['title'] }}
        </h3>
        <span class="bg-red label pt0">{{ $ticket['priority'] }}</span>
    </div>
    <div class="panel-footer">

        <button class="btn btn-primary btn-sm mv-sm" onclick="goToCommentBox()">
            <i class="fa fa-reply"></i>
            {{ trans('tickets::general.answer') }}
        </button>

        <div class="dropdown inline mv-sm">
            <button type="button" class="btn btn-info btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-building"></i>
                {{ trans('tickets::general.change_department') }}
                <span class="caret ml"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                @foreach($departments as $department)
                    <li>
                        <a href="{{ $department['link'] }}">{{ $department['title'] }}</a>
                    </li>
                @endforeach
            </ul>
        </div>

        <div class="dropdown inline mv-sm">
            <button type="button" class="btn btn-info btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-sort-amount-up"></i>
                {{ trans('tickets::general.change_priority') }}
                <span class="caret ml"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                @foreach($priorities as $priority)
                    <li>
                        <a href="{{ $priority['link'] }}">
                            {{ $priority['title'] }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>

        <button class="btn btn-green btn-sm mv-sm">
            <i class="fa fa-bookmark"></i>
            {{ trans('tickets::general.mark_as_unread') }}
        </button>

        @if($ticket_status === "open")
            <button class="btn btn-danger btn-sm mv-sm">
                <i class="fa fa-ban"></i>
                {{ trans('tickets::general.close_ticket') }}
            </button>
        @elseif($ticket_status === "close")
            <button class="btn btn-success btn-sm mv-sm">
                <i class="fa fa-unlock"></i>
                {{ trans('tickets::general.open_ticket') }}
            </button>
        @endif

    </div>
</div>