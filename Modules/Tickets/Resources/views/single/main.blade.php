@extends('manage::layouts.template')

@php
    $events = [
        [
            "badge" => [
                "color" => "info" ,
            ] ,
            "author" => [
                "image" => Module::asset('manage:images/user/02.jpg') ,
                "name" => "طاها کامکار" ,
            ] ,
            "time" => "۲ دقیقه پیش" ,
            "content" => [
                "type" => "comment" ,
                "title" => "یه عنوان الکی" ,
                "body" => "
                    <p>Av 123 St - Floor 2
                    <br>
                    <small>Pellentesque ut diam velit, eget porttitor risus. Nullam posuere euismod volutpat.</small>
                </p>
                " ,
                "attached_files" => [
                    [
                        "link" => "#" ,
                    ],
                    [
                        "link" => "#" ,
                    ],
                    [
                        "link" => "#" ,
                    ],
                ] ,
                "labels" => [
                    [
                        "color" => "danger" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                    [
                        "color" => "info" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                    [
                        "color" => "" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                ] ,
            ] ,
        ],
        [
            "badge" => [
                "color" => "primary" ,
                "icon" => "building" ,
            ] ,
            "author" => [
                "image" => Module::asset('manage:images/user/01.jpg') ,
                "name" => "نگار جمالی‌فرد" ,
            ] ,
            "time" => "۳ دقیقه پیش" ,
            "content" => [
                "type" => "message" ,
                "body" => "<p>تیکت به یک دپارتمان دیگر منتقل شد.</p>" ,
            ] ,
        ],
        [
            "badge" => [
                "color" => "info" ,
            ] ,
            "author" => [
                "image" => Module::asset('manage:images/user/02.jpg') ,
                "name" => "طاها کامکار" ,
            ] ,
            "time" => "۲ دقیقه پیش" ,
            "content" => [
                "type" => "comment" ,
                "title" => "یه عنوان الکی" ,
                "body" => "
                    <p>Av 123 St - Floor 2
                    <br>
                    <small>Pellentesque ut diam velit, eget porttitor risus. Nullam posuere euismod volutpat.</small>
                </p>
                " ,
                "attached_files" => [
                    [
                        "link" => "#" ,
                    ],
                    [
                        "link" => "#" ,
                    ],
                    [
                        "link" => "#" ,
                    ],
                ] ,
                "labels" => [
                    [
                        "color" => "danger" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                    [
                        "color" => "info" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                    [
                        "color" => "" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                ] ,
            ] ,
        ],
        [
            "badge" => [
                "color" => "info" ,
            ] ,
            "author" => [
                "image" => Module::asset('manage:images/user/03.jpg') ,
                "name" => "مسئول مربوطه" ,
            ] ,
            "time" => "۲ دقیقه پیش" ,
            "content" => [
                "type" => "comment" ,
                "title" => "یه عنوان الکی" ,
                "body" => "
                    <p>با سلام
                    <br>
                    نمی‌دونم.
                </p>
                " ,
                "attached_files" => [
                    [
                        "link" => "#" ,
                    ],
                ] ,
                "labels" => [
                    [
                        "color" => "danger" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                    [
                        "color" => "info" ,
                        "text" => "دپارتمان فروش" ,
                    ]
                ] ,
            ] ,
        ],
        [
            "badge" => [
                "color" => "danger" ,
                "icon" => "times" ,
            ] ,
            "author" => [
                "image" => Module::asset('manage:images/user/01.jpg') ,
                "name" => "نگار جمالی‌فرد" ,
            ] ,
            "time" => "۳ دقیقه پیش" ,
            "content" => [
                "type" => "message" ,
                "body" => "<p>تیکت به یک دپارتمان دیگر منتقل شد.</p>" ,
            ] ,
        ],
        [
            "badge" => [
                "color" => "info" ,
            ] ,
            "author" => [
                "image" => Module::asset('manage:images/user/02.jpg') ,
                "name" => "طاها کامکار" ,
            ] ,
            "time" => "۲ دقیقه پیش" ,
            "content" => [
                "type" => "comment" ,
                "title" => "یه عنوان الکی" ,
                "body" => "
                    <p>Av 123 St - Floor 2
                    <br>
                    <small>Pellentesque ut diam velit, eget porttitor risus. Nullam posuere euismod volutpat.</small>
                </p>
                " ,
                "attached_files" => [
                    [
                        "link" => "#" ,
                    ],
                    [
                        "link" => "#" ,
                    ],
                    [
                        "link" => "#" ,
                    ],
                ] ,
                "labels" => [
                    [
                        "color" => "danger" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                    [
                        "color" => "info" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                    [
                        "color" => "" ,
                        "text" => "دپارتمان فروش" ,
                    ],
                ] ,
            ] ,
        ]
    ];

    $ticket_status = "open";

    $ticket = [
        "title" => "مشکل با پردازش سبدهای خرید در پنل Manage" ,
        "priority" => "بسیار مهم" ,
        "department" => "پشتیبانی" ,
    ];

    $departments = [
        [
            "link" => "#" ,
            "title" => "فروش" ,
        ],
        [
            "link" => "#" ,
            "title" => "فروش" ,
        ],
    ];

    $priorities = [
        [
            "link" => "#" ,
            "title" => "مهم" ,
        ],
        [
            "link" => "#" ,
            "title" => "بی‌اهمیت" ,
        ],
    ];
@endphp

@section('content')
    <div class="tickets-timeline">

        {{-- Ticket header --}}
        @include('tickets::single.header')

        <ul class="timeline">

            @foreach($events as $event)
                @include('tickets::single.dialog-box',[
                    "badge" => $event['badge'] ,
                    "author" => $event['author'] ,
                    "time" => $event['time'] ,
                    "content" => $event['content'] ,
                ])

                {{-- DO NOT REMOVE THIS--}}
                <li style="display: block; width: 100%; visibility: hidden;"></li>

            @endforeach

            @if($ticket_status === "open")
                @include('tickets::single.dialog-box',[
                    "reversed" => false,
                    "id" => "timeline_comment_box" ,
                    "badge" => [
                        "color" => "" ,
                        "icon" => "plus" ,
                    ],
                    "author" => [
                        "image" => Module::asset('manage:images/user/03.jpg') ,
                        "name" => "حضرت برنامه‌نویس" ,
                    ] ,
                    "time" => "" ,
                    "content" => [
                        "type" => 'editor' ,
                    ] ,
                ])

                <li class="timeline-end">
                    <div class="timeline-badge">
                        <em class="fas fa-ellipsis-v" style="line-height: 35px;"></em>
                    </div>
                </li>
            @elseif($ticket_status === "close")
                <li class="timeline-end">
                    <div class="timeline-badge danger">
                        <em class="fas fa-ban" style="line-height: 35px;"></em>
                    </div>
                </li>
        @endif


        <!-- END timeline item-->
        </ul>

    </div>


@endsection