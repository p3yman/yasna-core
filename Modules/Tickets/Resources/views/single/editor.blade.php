{!!
    widget('form-open')
    ->method('')
 !!}

{!!
    widget('input')
    ->name('title')
    ->label('tr:tickets::general.form.title')
    ->inForm()
 !!}

{!!
    widget('textarea')
    ->class('tickets_tinymce_editor')
    ->label('tr:tickets::general.form.message')
    ->inForm()
 !!}

<div class="form-group">
	<label class="control-label col-sm-2">
		{{ trans('tickets::general.attach') }}
	</label>
	<div class="col-sm-10">
		{{--TODO: Place to put dropzone--}}
	</div>
</div>

{{--TODO: Uploaded files should be viewed in this list--}}
@if(isset($attached_files) and count($attached_files))
	<div class="p10 bg-gray-lighter well mt-lg">
		<ul>
			@foreach($attached_files as $attached_file)
				<li>
					<a href="{{ $attached_file['link']}}">
						{{ trans('tickets::general.attach'). " " . ad($loop->iteration) }}
					</a>
				</li>
			@endforeach
		</ul>
	</div>

@endif

<div class="form-group">
	{!!
		widget('button')
		->type('submit')
		->shape('success')
		->class('btn-taha pull-right mh')
		->label('tr:tickets::general.send')
	 !!}
</div>


{!!
    widget('form-close')
 !!}
