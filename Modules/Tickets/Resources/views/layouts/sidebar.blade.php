@include("manage::layouts.sidebar-link" , [
	'icon' => "comment",
	'caption' => trans("tickets::general.tickets") ,
	'link' => "tickets" ,
	'sub_menus' => $sub_menus = module('tickets')->TicketsSidebarController()->render() ,
	'condition' => count($sub_menus),
])
