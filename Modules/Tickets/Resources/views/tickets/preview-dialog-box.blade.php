@php
	$timeline_class = "";
	$popover_class = "left";

@endphp

<!-- START timeline item-->
<li class="timeline-inverted" id="{{ $id or "" }}">
	<div class="timeline-badge {{ $badge['color'] or "primary" }}">
		<em class="fa fa-{{ $badge['icon'] or "comments" }}"
			style="line-height: 35px;"></em>
	</div>
	<div class="timeline-panel">
		<div class="popover right">
			<div class="popover-title">
				<div class="author">
					<img src="{{ $author['image'] }}" class="img-thumbnail img-circle thumb32 mr-sm">
					<strong>{{ $author['name'] }}</strong>
					<small class="pull-right text-muted mt-sm">
						@include('manage::widgets.grid-date', [
							'date' => $time,
							'condition' => boolval($time),
						])
					</small>
				</div>
			</div>
			<div class="arrow"></div>
			<div class="popover-content">
				@if($content['type'] === "comment")

					@include('tickets::tickets.preview-dialog-content',[
						"title" => $content['title'] ,
						"body" => $content['body'] ,
						"attached_files" => ($content['attached_files'] ?? []) ,
						"labels" => ($content['labels'] ?? []) ,
					])

				@elseif($content['type'] === "message")

					{!! $content['body'] !!}

				@elseif($content['type'] === "editor")

					@include('tickets::tickets.preview-editor')

				@endif
			</div>
		</div>
	</div>
</li>
<!-- END timeline item-->
