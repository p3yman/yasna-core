{!!
	widget('modal')
    	->label(trans_safe('tickets::general.ticker-information'))
!!}

@php
	$ticket_status = ($model->is_finalized ? "close" : "open");

	$current_labels = $model->labels;
	$current_labels_ids = $current_labels->pluck('id')->toArray();
	$current_ticket_type = $model->entity;
@endphp


<div class="modal-body">
	<div class="tickets-timeline">
		@include('tickets::tickets.preview-header')

		@include('tickets::tickets.preview-timeline')
	</div>
</div>


<div class="modal-footer">
	{!!
		widget('button')
			->label(trans('manage::forms.button.cancel'))
			->id('btnCancel')
			->shape('link')
			->onClick('$(".modal").modal("hide")')
	!!}
</div>

<script>
    var model_key = "{{ $model->hashid }}";
</script>
