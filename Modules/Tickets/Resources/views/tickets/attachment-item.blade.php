@php $link = fileManager()->file($hashid)->getDownloadUrl() @endphp


@if ($link)
	<li>
		<a href="{{ $link }}" target="_blank">
			{{ trans('tickets::general.attach') }}
			{{ ad($number) }}
		</a>
	</li>

	@if ($with_input ?? false)
		{!! widget('hidden')->name('attachments[]')->value($hashid) !!}
	@endif

@endif
