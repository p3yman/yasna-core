<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'class' => "font-yekan" ,
		'size' => "14" ,
		'link' => $action_urls['preview']
	])


	@include("manage::widgets.grid-badge" , [
		'text' => $model->entity->title,
		'class' => "font-yekan" ,
		'size' => "10" ,
		'color' => 'green',
		'icon' => 'phone',
	])


	@include("manage::widgets.grid-text" , [
		'text' => $model->sender_name,
		'class' => "font-yekan" ,
		'size' => "12" ,
	])


	@include("manage::widgets.grid-text" , [
		'text' => $model->id . '|' . $model->hashid,
		'size' => "10" ,
		'condition' => user()->isDeveloper(),
		'color' => 'gray'
	])

	@include('manage::widgets.grid-date', [
		'date' => $model->created_at
	])


</td>
