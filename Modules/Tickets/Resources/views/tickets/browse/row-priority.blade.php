<td>
	@include('manage::widgets.grid-badge', [
		'text' => $model->flag_title,
		'color' => $model->flag_color,
		'icon'=> $model->flag_icon,
	])
</td>
