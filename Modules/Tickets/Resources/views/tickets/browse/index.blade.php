@extends('manage::layouts.template')

@section('content')
	@include('tickets::tickets.browse.tabs')
	@include('tickets::tickets.browse.toolbar')
	@include('tickets::tickets.browse.grid')
@append

@section('html_header')
	<script>
        var urls = {
            ticketTypeChange: "{{ route('tickets.change-ticket-type') }}",
            flagChange      : "{{ route('tickets.change-flag') }}",
            markAsNew       : "{{ route('tickets.mark-as-new') }}",
            reopen          : "{{ route('tickets.reopen') }}",
            close           : "{{ route('tickets.close') }}",
        };
	</script>

	{!! Html::script(Module::asset('tickets:js/timer.min.js')) !!}
	{!! Html::script(Module::asset('tickets:js/tickets.min.js')) !!}

	<meta name="csrf-token" content="{{ csrf_token() }}">

	{!! fileManager()->uploader()->renderGeneralCss() !!}
@append

@section('html_footer')
	{!! fileManager()->uploader()->renderGeneralJs() !!}

	<script>
		@php
			$attachment_url = route('tickets.attachment', [
				'hashid' => '__HASHID__',
				'number' => '__NUMBER__',
			])
		@endphp

        function attachmentsBox() {
            return $('#js-attachments');
        }


        function attachmentItems() {
            return attachmentsBox().children('li');
        }

        function nextAttachmentNumber() {
            let next = (parseInt(attachmentsBox().data('number')) + 1);
            attachmentsBox().data('number', next);
            return next;
        }


        function handleAttachmentsBox() {
            let box_back = attachmentsBox().parent();
            if (attachmentItems().length) {
                box_back.show();
            } else {
                box_back.hide();
            }
        }

        function addAttachmentPreview(hashid) {
            $.ajax({
                url     : "{{ $attachment_url }}"
                    .replace('__HASHID__', hashid)
                    .replace('__NUMBER__', nextAttachmentNumber()),
                success : function (rs) {
                    attachmentsBox().append(rs);
                },
                complete: function () {
                    handleAttachmentsBox();
                }
            });

        }

        function removeFilePreview(file) {
            let uploader_var = eval($(file.previewElement.closest('.dropzone')).data('varName'));
            uploader_var.removeFile(file);
        }


        function fileUploaded(file) {
            let json   = JSON.parse(file.xhr.response);
            let hashid = json.file;

            if (hashid) {
                addAttachmentPreview(hashid);
                removeFilePreview(file);
            }
        }
	</script>
@append
