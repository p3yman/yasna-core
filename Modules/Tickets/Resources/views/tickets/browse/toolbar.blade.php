@include("manage::widgets.toolbar" , [
	'buttons' => [
        [
            "target"    => "modal:" . route('tickets.create', ['ticket_type' => $ticket_type_string]),
            "caption"   => trans("tickets::general.create_ticket"),
            "icon"      => "plus",
            "type"      => "success",
            "condition" => $has_permission,
        ],
    ],
])
