@include("manage::widgets.grid" , [
	'table_id' => "tbl-tickets" ,
	'row_view' => module('tickets')->viewName('tickets.browse.row') ,
	'handle' => "counter" ,
	'operation_heading' => true ,
	'headings' => [
		trans('validation.attributes.title'),
		trans('tickets::general.priority'),
		trans('tickets::general.labels'),
	],
])
