@php
	$action_urls = TicketTools::browseActionUrls($model);
@endphp

@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => $action_urls['refresh'],
])


@include('tickets::tickets.browse.row-title')
@include('tickets::tickets.browse.row-priority')
@include('tickets::tickets.browse.row-labels')


@include("manage::widgets.grid-actionCol", [
	'actions' => [
		[
			'eye',
			trans('tickets::general.preview'),
			$action_urls['preview']
		],
	]
])
