@php $labels = $model->labels @endphp

<td>
	@foreach($labels as $label)
		@include("manage::widgets.grid-badge" , [
			'text' => $label->titleIn(getLocale()),
			'class' => "font-yekan" ,
			'size' => "10" ,
			'color' => 'info',
			'icon' => 'tag',
		])
	@endforeach
</td>
