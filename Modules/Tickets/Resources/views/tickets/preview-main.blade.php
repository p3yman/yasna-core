@extends('manage::layouts.template')

@section('content')
	<div class="tickets-timeline">

		@include('tickets::tickets.preview-header')

		<ul class="timeline">

			@foreach($events as $event)
				@include('tickets::tickets.preview-dialog-box',[
					"reversed" => ($loop->iteration % 2 === 0) ,
					"badge" => $event['badge'] ,
					"author" => $event['author'] ,
					"time" => $event['time'] ,
					"content" => $event['content'] ,
				])

				{{-- DO NOT REMOVE THIS--}}
				<li style="display: block; width: 100%; visibility: hidden;"></li>

			@endforeach

				@if($ticket_status === "open")
					@include('tickets::tickets.preview-dialog-box',[
						"reversed" => false,
						"id" => "timeline_comment_box" ,
						"badge" => [
							"color" => "" ,
							"icon" => "plus" ,
						],
						"author" => [
							"image" => Module::asset('manage:images/user/03.jpg') ,
							"name" => "حضرت برنامه‌نویس" ,
						] ,
						"time" => "" ,
						"content" => [
							"type" => 'editor' ,
						] ,
					])

					<li class="timeline-end">
						<div class="timeline-badge">
							<em class="fas fa-ellipsis-v" style="line-height: 35px;"></em>
						</div>
					</li>
				@elseif($ticket_status === "close")
					<li class="timeline-end">
						<div class="timeline-badge danger">
							<em class="fas fa-ban" style="line-height: 35px;"></em>
						</div>
					</li>
				@endif


			<!-- END timeline item-->
		</ul>
	</div>


@endsection
