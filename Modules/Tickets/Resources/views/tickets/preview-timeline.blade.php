<ul class="timeline" id="timeline">
	<span class="refresh">
		{{ TicketTools::browseActionUrl($model, 'timeline') }}
	</span>

	@include('tickets::tickets.preview-item',[
		'model' => $model,
		'labels' => ($current_labels ?? null),
	])

	@php $replies = $model->deep_replies @endphp

	@foreach($replies as $reply)
		@include('tickets::tickets.preview-item', [
			'model' => $reply,
		])
	@endforeach

	@if($model->is_not_finalized)
		@include('tickets::tickets.preview-dialog-box',[
			"reversed" => false,
			"id" => "timeline_comment_box" ,
			"badge" => [
				"color" => "" ,
				"icon" => "plus" ,
			],
			"author" => [
				"image" => Module::asset('manage:images/user/avatar-default.jpg') ,
				"name" => user()->full_name ,
			] ,
			"time" => "" ,
			"content" => [
				"type" => 'editor' ,
			] ,
		])

		<li class="timeline-end">
			<div class="timeline-badge">
				<em class="fas fa-ellipsis-v" style="line-height: 35px;"></em>
			</div>
		</li>
	@elseif($model->is_finalized)
		<li class="timeline-end">
			<div class="timeline-badge danger">
				<em class="fas fa-ban" style="line-height: 35px;"></em>
			</div>
		</li>
	@endif


	{{--<script>--}}
        {{--initReplyEditor();--}}
	{{--</script>--}}

	<!-- END timeline item-->
</ul>
