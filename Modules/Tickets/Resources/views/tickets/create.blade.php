{!!
	widget('modal')
    	->label('tr:tickets::general.create_ticket_label')
    	->target(route('tickets.save'))
		->setExtra('enctype', 'multipart/form-data')
!!}

<div class="modal-body">


	{!!
		widget('input')
		->name('username')
		->label('tr:tickets::general.form.username_field.' . $usernameField)
		->help('tr:tickets::general.form.username_field_hint_' . $usernameField)
		->inForm()
		->required()
	 !!}

	{!!
		widget('combo')
			->inForm()
			->label('tr:tickets::general.ticket-type')
			->name('ticket_type')
			->options($ticket_types)
			->valueField('slug')
			->captionField('title')
	!!}

	{!!
		widget('combo')
			->inForm()
			->label('tr:tickets::general.priority')
			->name('priority')
			->options($priorities)
			->valueField('value')
			->captionField('title')
	!!}

	{!!
		widget('input')
		->name('title')
		->label('tr:tickets::general.form.title')
		->inForm()
		->required()
	 !!}

	{!!
		widget('textarea')
			->name('text')
			->class('tickets_tinymce_editor')
			->label('tr:tickets::general.form.message')
			->inForm()
	 !!}

	{!! widget('separator') !!}

	{!!
		TicketTools::getTicketUploader()
			->withAssets()
			 ->withoutAjaxRemove()
			 ->onEachUploadComplete('fileUploaded')
			->render()
	!!}

	<div class="p10 bg-gray-lighter well mt-lg" style="display: none;">
		<ul id="js-attachments" data-number="0">
		</ul>
	</div>

</div>
<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
</div>


<script type="text/javascript">
    initReplyEditor()
</script>
