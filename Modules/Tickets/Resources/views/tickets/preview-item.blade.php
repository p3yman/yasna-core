@php
	$labels_collection = ($labels ??  $model->labels);
@endphp

@include('tickets::tickets.preview-dialog-box',[
		"author" => [
			"name" => $model->sender_name,
			"image" => Module::asset('manage:images/user/avatar-default.jpg')
		],
		"badge" => [
			"icon" => $model->timeline_icon,
			"color" => $model->timeline_color,
		],
		"time" => $model->created_at ,
		"content" => [
			"type" => "comment" ,
			"title" => '' ,
			"body" => $model->usable_text ,
			"labels" => $labels_collection->map(function ($label) {
				return [
					'text' => $label->titleIn(getLocale())
				];
			})->toArray(),
			'attached_files' => array_map(function ($hashid) {
				return [
					'link' => fileManager()->file($hashid)->getDownloadUrl()
				];
			}, $model->attachments)
		] ,
])

{{-- DO NOT REMOVE THIS--}}
<li style="display: block; width: 100%; visibility: hidden;"></li>
