<div class="panel panel-default">
	<div class="panel-body p-xl">
		<h3 class="mt0 mb-lg">
			{{ $model->title }}
		</h3>
		<p>
			<span class="bg-purple label pt0">{{ $model->flag_title }}</span>
		</p>
		<p>
			<span class="bg-info label pt0">{{ $current_ticket_type->title }}</span>
		</p>
	</div>
	<div class="panel-footer">
		@if ($model->is_not_finalized)
			<button class="btn btn-primary m5" onclick="goToCommentBox()">
				<i class="fa fa-reply"></i>
				{{ trans('tickets::general.answer') }}
			</button>

			@php $available_ticket_types = TicketTools::availableTicketTypes() @endphp
			@if ($available_ticket_types->count() > 1)
				<div class="btn-group m5">
					<button type="button" class="btn btn-info ">
						<i class="fa fa-building"></i>
						{{ trans('tickets::general.change-ticket-type') }}
					</button>
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						@foreach($available_ticket_types as $ticket_type)
							@if ($ticket_type == $current_ticket_type)
								@continue
							@endif
							<li>
								<a href="{{ '#' }}" class="ticket-type-selector"
								   data-key="{{ $ticket_type->hashid }}">
									{{ $ticket_type->title }}
								</a>
							</li>
						@endforeach
					</ul>
				</div>
			@endif


			<div class="btn-group m5">
				<button class="btn btn-purple">
					<i class="fa fa-sort-amount-up"></i>
					{{ trans('tickets::general.change_priority') }}
				</button>
				<button type="button" class="btn btn-purple dropdown-toggle" data-toggle="dropdown">
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					@php $flags = TicketTools::priorities() @endphp
					@foreach($flags as $flag)
						@if ($flag === $model->flag)
							@continue
						@endif
						<li>
							<a href="#" data-key="{{ $flag }}" class="flag-selector">
								{{ TicketTools::getFlagTitle($flag) }}
							</a>
						</li>
					@endforeach
				</ul>
			</div>

			@if ($model->flag != 'new')
				<button class="btn btn-green m5" id="btn-mark-as-new">
					<i class="fa fa-bookmark"></i>
					{{ trans('tickets::general.mark_as_unread') }}
				</button>
			@endif

			<button class="btn btn-danger m5" id="btn-close">
				<i class="fa fa-ban"></i>
				{{ trans('tickets::general.close_ticket') }}
			</button>
		@else
			<button class="btn btn-success m5" id="btn-reopen">
				<i class="fa fa-unlock"></i>
				{{ trans('tickets::general.reopen') }}
			</button>
		@endif
	</div>
</div>
