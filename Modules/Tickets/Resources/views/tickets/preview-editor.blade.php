{!!
    widget('form-open')
    	->target(route('tickets.reply'))
    	->class('js')
!!}

{!!
	widget('hidden')
		->name('hashid')
		->value($model->hashid)
!!}

{!!
    widget('textarea')
		->name('text')
		->class('tickets_tinymce_editor')
		->label('tr:tickets::general.form.message')
		->inForm()
!!}

<div class="form-group">
	<label class="control-label col-sm-2">
		{{ trans('tickets::general.attach') }}
	</label>
	<div class="col-sm-10">
		{!!
			TicketTools::getTicketUploader()
				->withAssets()
             	->withoutAjaxRemove()
             	->onEachUploadComplete('fileUploaded')
				->render()
		!!}
	</div>
</div>

<div class="p10 bg-gray-lighter well mt-lg" style="display: none;">
	<ul id="js-attachments" data-number="0">
	</ul>
</div>


{!! widget('feed') !!}

<div class="form-group">
	{!!
		widget('button')
		->type('submit')
		->class('btn-success btn-taha pull-right mh')
		->label('tr:tickets::general.send')
	 !!}
</div>


{!!
    widget('form-close')
!!}
