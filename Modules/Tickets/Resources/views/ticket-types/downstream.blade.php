@extends('manage::layouts.template')


@section('content')
	@include("manage::downstream.tabs")

	@include("manage::widgets.toolbar" , [
		'buttons' => [
				[
					'target' => 'modal:' . route('tickets.types.single-action', [
						'model_id' => hashid(0),
						'action' => 'notification-channels'
					], false),
					'type' => "info",
					'caption' => $__module->getTrans('general.notification-channels.title.plural'),
					'icon' => "bell",
					'condition' => !is_null(get_setting('ticket_notification_channels')),
				],
				[
					'target' => 'modal:' . route('tickets.types.single-action', [
						'model_id' => hashid(0),
						'action' => 'create'
					], false),
					'type' => "success",
					'caption' => trans('manage::forms.button.add_to')
						. ' '
						. trans('tickets::general.ticket-types'),
					'icon' => "plus-circle",
					'condition' => user()->isSuperadmin(),
				],
			],
		])

	@include("tickets::ticket-types.browse")
@endsection
