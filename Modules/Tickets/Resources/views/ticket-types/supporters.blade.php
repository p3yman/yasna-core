{!!
	widget('modal')
		->label(trans('tickets::general.supporters-of', ['title' => $model->title]))
    	->target(route('tickets.types.supporters'))
!!}


<div class="modal-body">
	{!! widget('hidden')->name('id')->value($model->hashid) !!}

	@php $support_roles = TicketTypeTools::acceptableRoles() @endphp
	@foreach($support_roles as $role)
		{!!
			widget('checkbox')
				->inForm()
				->name("roles[$role->hashid]")
				->value($model->hasRole($role->id) ? 1 : 0)
				->label($role->title)
		!!}
	@endforeach

</div>


<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
