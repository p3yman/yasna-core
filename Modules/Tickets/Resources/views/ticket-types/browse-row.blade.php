@php
	$action_urls = TicketTypeTools::browseActionUrls($model);
@endphp

@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => $action_urls['refresh'],
])

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'class' => "font-yekan" ,
		'size' => "14",
		'link' => $action_urls['edit']
	])


	<div class="mv10 f10 text-grey">
		{{ $model->creator->full_name }}
	</div>

	@include('manage::widgets.grid-date', [
		'date' => $model->created_at
	])
</td>

@include('tickets::ticket-types.browse-row-supporters')


@include("manage::widgets.grid-actionCol"  , [
	'actions' => [
		[
			'pencil',
			trans('manage::forms.button.edit'),
			$action_urls['edit']
		],
		[
			'phone-square',
			trans('tickets::general.supporters'),
			$action_urls['supporters']
		],
		[
			'comments',
			trans('tickets::general.tickets'),
			$action_urls['tickets'],
			!$model->trashed()
		],
		[
			'trash-o',
			trans('manage::forms.button.soft_delete'),
			$action_urls['delete'],
			!$model->trashed()
		],
		[
			'recycle',
			trans('manage::forms.button.undelete'),
			$action_urls['undelete'],
			$model->trashed()
		],
		[
			'times',
			trans('manage::forms.button.hard_delete'),
			$action_urls['hard-delete'],
			($model->isReadyToHardDelete())
		],
	] ,
])
