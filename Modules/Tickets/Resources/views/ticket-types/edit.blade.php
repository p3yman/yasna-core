{!!
	widget('modal')
    	->labelIf($model->exists, trans('tickets::general.edit-ticket-type', ['title' => $model->title]))
    	->labelIf(!$model->exists, trans('tickets::general.new-ticket-type'))
    	->target(route('tickets.types.save'))
!!}


<div class="modal-body">
	{!! widget('hidden')->name('id')->value($model->hashid) !!}

	{!!
		widget('input')
			->inForm()
			->name('slug')
			->label(trans('validation.attributes.slug'))
			->value($model->slug)
	!!}


	@php $site_locales = get_setting('site_locales') @endphp
	@foreach($site_locales as $locale)
		@php $locale_title = trans("tickets::general.locales.$locale") @endphp
		{!!
			widget('input')
				->inForm()
				->name("titles[$locale]")
				->label(trans('tickets::general.form.title_in', ['locale' => $locale_title]))
				->value($model->titleIn($locale))
		!!}
	@endforeach
</div>

<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
