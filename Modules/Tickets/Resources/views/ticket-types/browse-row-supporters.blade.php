<td>
	@php $roles = $model->roles @endphp

	@foreach($roles as $role)
		@include('manage::widgets.grid-badge', [
			'text' => $role->title,
			'icon' => 'headphones'
		])
	@endforeach
</td>
