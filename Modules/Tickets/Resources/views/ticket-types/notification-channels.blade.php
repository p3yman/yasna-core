{!!
	widget('modal')
		->label($__module->getTrans('general.notification-channels.title.plural'))
		->target(route('tickets.types.notification-channels'))
!!}


<div class="modal-body">
	@php
		$available_channels = $__module->getConfig('notifications.channels');
		$setting_channels = get_setting('ticket_notification_channels');
		$active_channels = is_array($setting_channels) ? $setting_channels : [];
	@endphp

	@foreach($available_channels as $channel)
		{!!
			widget('checkbox')
				->label($__module->getTrans("general.notification-channels.channels.$channel"))
				->name("channels[$channel]")
				->value(in_array($channel, $active_channels))
		!!}
	@endforeach
</div>


<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
