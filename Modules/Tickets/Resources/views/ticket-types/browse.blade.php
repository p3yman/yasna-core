<div class="row" id="browse-grid">
	<span class="hidden refresh">{{ route('tickets.types.browse') }}</span>

	<div class="col-xs-12">
		@include("manage::widgets.grid" , [
			'table_id' => "tbl-accounts-online",
			'table_class' => 'tbl-accounts',
			'row_view' => "tickets::ticket-types.browse-row",
			'handle' => "counter",
			'models' => $models,
			'headings' => [
				trans('validation.attributes.title'),
				trans('tickets::general.supporters'),
				trans('manage::forms.button.action'),
			],
		])
	</div>
</div>
