<?php

namespace Modules\Tickets\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Remark\Events\RemarkReplied;
use Modules\Tickets\Notifications\TicketFlagChangeNotification;
use Modules\Tickets\Providers\TicketsServiceProvider;

class SendNotificationWhenTicketFlagChanged
{
    /**
     * Handle the event.
     *
     * @param RemarkReplied $event
     *
     * @return void
     */
    public function handle(RemarkReplied $event)
    {
        $model = $event->model;
        $reply = $event->reply;

        if (TicketsServiceProvider::isNotTicket($model)) {
            return;
        }

        if (!$model->routeNotificationForSms()) {
            return;
        }

        if ($reply->isChangingFlag()) {
            $event->model->notify(new TicketFlagChangeNotification($reply));
        }
    }
}
