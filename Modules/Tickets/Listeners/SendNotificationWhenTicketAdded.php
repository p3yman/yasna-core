<?php

namespace Modules\Tickets\Listeners;

use Modules\Remark\Events\RemarkAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Tickets\Notifications\TicketCreationNotification;
use Modules\Tickets\Providers\TicketsServiceProvider;

class SendNotificationWhenTicketAdded
{
    /**
     * Handle the event.
     *
     * @param RemarkAdded $event
     *
     * @return void
     */
    public function handle(RemarkAdded $event)
    {
        $model = $event->model;

        if (TicketsServiceProvider::isTicket($model) and $model->routeNotificationForSms()) {
            $event->model->notify(new TicketCreationNotification);
        }
    }
}
