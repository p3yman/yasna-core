<?php

namespace Modules\Tickets\Listeners;

use Modules\Remark\Events\RemarkReplied;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Tickets\Notifications\TicketRepliedNotification;
use Modules\Tickets\Providers\TicketsServiceProvider;

class SendNotificationWhenTicketReplied
{
    /**
     * Handle the event.
     *
     * @param RemarkReplied $event
     * @return void
     */
    public function handle(RemarkReplied $event)
    {
        $model = $event->model;
        $reply = $event->reply;

        if (TicketsServiceProvider::isNotTicket($model)) {
            return;
        }

        if (!$model->routeNotificationForSms()) {
            return;
        }

        if ($reply->isDefaultType()) {
            $event->model->notify(new TicketRepliedNotification($reply));
        }
    }
}
