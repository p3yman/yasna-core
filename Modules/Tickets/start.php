<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists('ticket')) {
    /**
     * Returns an instance of the `TicketType` model with the given identifier.
     *
     * @param string|int $type         The identifier of the ticket type
     * @param bool       $with_trashed Whether to search in trashed items.
     *
     * @return App\Models\TicketType
     */
    function ticket($type = 0, $with_trashed = false)
    {
        return model('ticket-type', $type, $with_trashed);
    }
}
