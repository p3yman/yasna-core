<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/22/18
 * Time: 2:10 PM
 */

namespace Modules\Tickets\Services\Api;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator as BasePaginator;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class Paginator
{
    use ModuleRecognitionsTrait;

    const LIMIT_MIN = 5;
    const LIMIT_MAX = 100;
    const PAGE_MIN  = 1;

    /**
     * The Main Builder to be paginated
     *
     * @var Builder|HasMany|BelongsTo|BelongsToMany
     */
    protected $builder;
    /**
     * Current Page
     *
     * @var int
     */
    protected $page = 1;
    /**
     * Query Limit
     *
     * @var int
     */
    protected $limit = 20;
    /**
     * The Handling Request
     *
     * @var Request
     */
    protected $request;

    /**
     * The Name of the Items Field
     *
     * @var string
     */
    protected $items_name = 'items';
    /**
     * The Closure to Map Result Items
     *
     * @var Closure
     */
    protected $items_map;

    /**
     * The Name of the Page Field in The Request
     *
     * @var string
     */
    protected $page_name = 'page';
    /**
     * The Name of the Limit Field in The Request
     *
     * @var string
     */
    protected $limit_name = 'limit';



    /**
     * Paginator constructor.
     *
     * @param Builder|HasMany|BelongsTo|BelongsToMany $builder
     */
    public function __construct($builder)
    {
        $this->builder = $builder;

        $this->setDefaultRequest();
        $this->guessPage();
        $this->guessLimit();
    }



    /**
     * Sets the default request as the request property.
     */
    protected function setDefaultRequest()
    {
        $this->request(request());
    }



    /**
     * Sets the given request as the request property.
     *
     * @param Request $request
     *
     * @return $this
     */
    public function request(Request $request)
    {
        $this->request = $request;

        return $this;
    }



    /**
     * Guesses the page property.
     */
    protected function guessPage()
    {
        $request_page = $this->getRequestPage();
        if ($request_page and $this->pageIsAcceptable($request_page)) {
            $this->page = intval($request_page);
        }
    }



    /**
     * Returns the page field from the request.
     *
     * @return string|null
     */
    protected function getRequestPage()
    {
        $page_name = $this->getPageName();

        return $this->getRequest()->$page_name;
    }



    /**
     * Returns the name of the page field in the request.
     *
     * @return string
     */
    protected function getPageName()
    {
        return $this->page_name;
    }



    /**
     * Checks if the given value can be a valid page.
     *
     * @param string|int $page
     *
     * @return bool
     */
    public function pageIsAcceptable($page)
    {
        return ($this->isInt($page) and ($page >= $this->getPageMinimum()));
    }



    /**
     * Returns the minimum of the page.
     *
     * @return int
     */
    protected function getPageMinimum()
    {
        return static::PAGE_MIN;
    }



    /**
     * Returns the page property.
     *
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }



    /**
     * Sets the given value as the page property.
     *
     * @param string|int $page
     *
     * @return $this
     */
    public function page($page)
    {
        if ($this->pageIsAcceptable($page)) {
            $this->page = $page;
        }

        return $this;
    }



    /**
     * Guesses the limit property.
     */
    protected function guessLimit()
    {
        $request_limit = $this->getRequest()->limit;
        if ($request_limit and $this->limitIsAcceptable($request_limit)) {
            $this->limit = intval($request_limit);
        }
    }



    /**
     * Returns the limit value from the request.
     *
     * @return string|null
     */
    protected function getRequestLimit()
    {
        $limit_name = $this->getLimitName();

        return $this->getRequest()->$limit_name;
    }



    /**
     * Returns the name of the limit field in the request.
     *
     * @return string
     */
    protected function getLimitName()
    {
        return $this->limit_name;
    }



    /**
     * Checks if the given limit can be a valid limit.
     *
     * @param string|int $limit
     *
     * @return bool
     */
    public function limitIsAcceptable($limit)
    {
        return (
             $this->isInt($limit) and
             ($limit >= $this->getLimitMinimum()) and
             ($limit <= $this->getLimitMaximum())
        );
    }



    /**
     * Returns the minimum limit.
     *
     * @return int
     */
    protected function getLimitMinimum()
    {
        return static::LIMIT_MIN;
    }



    /**
     * Returns the maximum limit.
     *
     * @return int
     */
    protected function getLimitMaximum()
    {
        return static::LIMIT_MAX;
    }



    /**
     * Returns the limit property.
     *
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }



    /**
     * Sets the given limit as the limit property.
     *
     * @param string|int $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        if ($this->limitIsAcceptable($limit)) {
            $this->limit = $limit;
        }

        return $this;
    }



    /**
     * Returns the request property.
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }



    /**
     * Returns the name of items field in the result.
     *
     * @param string $name
     *
     * @return $this
     */
    public function itemsName(string $name)
    {
        $this->items_name = $name;

        return $this;
    }



    /**
     * Returns the items name property.
     *
     * @return string
     */
    public function getItemsName()
    {
        return $this->items_name;
    }



    /**
     * Sets the given closure as the map of the items.
     *
     * @param Closure $map
     *
     * @return $this
     */
    public function itemsMap(Closure $map)
    {
        $this->items_map = $map;

        return $this;
    }



    /**
     * Returns the map of the items.
     *
     * @return Closure
     */
    public function getItemsMap()
    {
        return $this->items_map
             ?: function ($item) {
                 return $item->toArray();
             };
    }



    /**
     * Checks if the given value is a valid integer number.
     *
     * @param string|int $value
     *
     * @return bool
     */
    public static function isInt($value)
    {
        return (is_numeric($value) and (intval($value) == $value));
    }



    /**
     * Resolves current page.
     */
    protected function resolveCurrentPage()
    {
        BasePaginator::currentPageResolver(function () {
            return $this->page;
        });
    }



    /**
     * Returns query array to be appended to the request.
     *
     * @return array
     */
    protected function queryArray()
    {
        return $this->getRequest()->only($this->getLimitName());
    }



    /**
     * Returns the main paginator object.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function getPaginatorObject()
    {
        $this->resolveCurrentPage();
        return $this
             ->builder
             ->paginate($this->limit)
             ->appends($this->queryArray())
             ;
    }



    /**
     * Returns as array version of this object.
     *
     * @return array
     */
    public function toArray()
    {
        $paginator = $this->getPaginatorObject();

        $meta = [
             'total' => $paginator->total(),
             'limit' => $paginator->perPage(),
             'count' => $paginator->getCollection()->count(),
             'links' => [
                  'first'    => $paginator->url(1),
                  'previous' => $paginator->previousPageUrl(),
                  'current'  => $paginator->url($paginator->currentPage()),
                  'next'     => $paginator->nextPageUrl(),
                  'last'     => $paginator->url($paginator->lastPage()),
             ],
             'pages' => [
                  'first'    => 1,
                  'previous' => $paginator->onFirstPage() ? null : ($paginator->currentPage() - 1),
                  'current'  => $paginator->currentPage(),
                  'next'     => $paginator->hasMorePages() ? ($paginator->currentPage() + 1) : null,
                  'last'     => $paginator->lastPage(),
                  'count'    => $paginator->lastPage(),
                  'is_first' => $paginator->onFirstPage(),
                  'is_last'  => !$paginator->hasMorePages(),
                  'has_more' => $paginator->hasMorePages(),
             ],
        ];
        $data = $paginator->getCollection()
                          ->map($this->getItemsMap())
                          ->toArray()
        ;

        return api()
             ->inModule($this->runningModuleName())
             ->successArray($data, $meta)
             ;

    }



    /**
     * Returns a JSON version of this object.
     *
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }
}
