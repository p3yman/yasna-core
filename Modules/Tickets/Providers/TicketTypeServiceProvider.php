<?php

namespace Modules\Tickets\Providers;

use App\Models\TicketType;
use Illuminate\Support\ServiceProvider;

class TicketTypeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    /**
     * The Kept Ticket Types
     *
     * @var array
     */
    protected static $kept_ticket_types = [];

    /**
     * The Ticket Type of the Request
     *
     * @var TicketType|null
     */
    protected static $request_ticket_type;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Return the URLs to be used for actions of ticket types' browsing.
     *
     * @param TicketType $type
     *
     * @return array
     */
    public static function browseActionUrls(TicketType $type)
    {
        return [
             'refresh' => route('tickets.types.single-action', [
                  'model_id' => $type->hashid,
                  'action'   => 'browse-row',
             ]),

             'edit' => 'modal:' . route('tickets.types.single-action', [
                       'model_id' => $type->hashid,
                       'action'   => 'edit',
                  ], false),

             'supporters' => 'modal:' . route('tickets.types.single-action', [
                       'model_id' => $type->hashid,
                       'action'   => 'supporters',
                  ], false),

             'delete' => 'modal:' . route('tickets.types.single-action', [
                       'model_id' => $type->hashid,
                       'action'   => 'delete',
                       'option0'  => 'delete',
                  ], false),

             'undelete' => 'modal:' . route('tickets.types.single-action', [
                       'model_id' => $type->hashid,
                       'action'   => 'delete',
                       'option0'  => 'undelete',
                  ], false),

             'hard-delete' => 'modal:' . route('tickets.types.single-action', [
                       'model_id' => $type->hashid,
                       'action'   => 'hard-delete',
                  ], false),

             'tickets' => 'url:' . route('tickets.browse', ['type' => $type->slug], false),
        ];
    }



    /**
     * Return all the roles which can be assigned as supporter of a ticket type.
     *
     * @return mixed
     */
    public static function acceptableRoles()
    {
        return model('role')->supportRoles();
    }



    /**
     * Finds a ticket type with its slug and keeps it for this request.
     *
     * @param string $slug
     *
     * @return TicketType
     */
    public static function findAndKeep(string $slug)
    {
        if (!array_key_exists($slug, static::$kept_ticket_types)) {
            static::$kept_ticket_types[$slug] = ticket()->grabSlug($slug);
        }

        return static::$kept_ticket_types[$slug];
    }



    /**
     * Sets the given ticket type as the request's ticket type.
     *
     * @param TicketType $ticket_type
     */
    public static function setRequestTicketType(TicketType $ticket_type)
    {
        static::$request_ticket_type = $ticket_type;
    }



    /**
     * Returns the ticket type of the request.
     *
     * @return TicketType|null
     */
    public static function getRequestTicketType()
    {
        return static::$request_ticket_type;
    }
}
