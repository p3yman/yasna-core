<?php

namespace Modules\Tickets\Providers;

use App\Models\Ticket;
use App\Models\TicketType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Modules\Remark\Entities\Remark;
use Modules\Remark\Events\RemarkAdded;
use Modules\Remark\Events\RemarkReplied;
use Modules\Tickets\Http\Middleware\CheckTicketTypeMiddleware;
use Modules\Tickets\Listeners\SendNotificationWhenTicketAdded;
use Modules\Tickets\Listeners\SendNotificationWhenTicketFlagChanged;
use Modules\Tickets\Listeners\SendNotificationWhenTicketReplied;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class TicketsServiceProvider
 *
 * @package Modules\Tickets\Providers
 */
class TicketsServiceProvider extends YasnaProvider
{


    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerDownstream();
        $this->registerProviders();
        $this->registerAliases();
        $this->registerSidebar();
        $this->registerEventListener();
        $this->registerModelTraits();
        $this->registerMiddlewares();
    }



    /**
     * Register the downstream part.
     *
     * @return void
     */
    protected function registerDownstream()
    {
        module('manage')
             ->service('downstream')
             ->add('tickets')
             ->link('ticket-types')
             ->trans('tickets::general.ticket-types')
             ->method('tickets:TicketTypesController@downstream')
             ->condition(function () {
                 return user()->isSuperadmin();
             })
             ->order(30)
        ;
    }



    /**
     * Register providers
     */
    protected function registerProviders()
    {
        $this->addProvider(TicketTypeServiceProvider::class);
    }



    /**
     * Register Aliases
     */
    protected function registerAliases()
    {
        $this->addAlias('TicketTypeTools', TicketTypeServiceProvider::class);
        $this->addAlias('TicketTools', TicketsServiceProvider::class);
    }



    /**
     * Register sidebar.
     */
    protected function registerSidebar()
    {
        module('manage')
             ->service('sidebar')
             ->add('tickets')
             ->blade('tickets::layouts.sidebar')
             ->order(44)
        ;
    }



    /**
     * Register event listeners.
     */
    protected function registerEventListener()
    {
        $this->listen(RemarkAdded::class, SendNotificationWhenTicketAdded::class);
        $this->listen(RemarkReplied::class, SendNotificationWhenTicketFlagChanged::class);
        $this->listen(RemarkReplied::class, SendNotificationWhenTicketReplied::class);
    }



    /**
     * Registers model traits.
     */
    protected function registerModelTraits()
    {
        $this->addModelTrait('UserTicketTrait', 'User');
    }



    /**
     * Registers middlewares.
     */
    protected function registerMiddlewares()
    {
        $this->addMiddleware('check-ticket-type', CheckTicketTypeMiddleware::class);
    }



    /**
     * Generate the URLs for actions of the given ticket.
     *
     * @param Ticket $ticket
     *
     * @return array
     */
    public static function browseActionUrls(Ticket $ticket)
    {
        return [
             'refresh' => route('tickets.browse.single-action', [
                  'model_id' => $ticket->hashid,
                  'action'   => 'row',
             ]),

             'preview' => 'modal:' . route('tickets.single-action', [
                       'model_id' => $ticket->hashid,
                       'action'   => 'preview',
                  ], false),

             'timeline' => route('tickets.single-action', [
                  'model_id' => $ticket->hashid,
                  'action'   => 'preview-timeline',
             ]),
        ];
    }



    /**
     * Generate the URLs for the specified action of the given ticket.
     *
     * @param Ticket $ticket
     * @param string $action
     *
     * @return string|null
     */
    public static function browseActionUrl(Ticket $ticket, string $action)
    {
        $urls = static::browseActionUrls($ticket);

        return ($urls[$action] ?? null);
    }



    /**
     * Return all available flags of tickets.
     *
     * @return array
     */
    public static function flags()
    {
        return ['new', 'high-priority', 'medium-priority', 'low-priority', 'done'];
    }



    /**
     * Return all available priorities of tickets.
     * <br >
     * The priorities are some items from the flags.
     *
     * @return array
     */
    public static function priorities()
    {
        $flags = static::flags();

        return array_filter($flags, function ($flag) {
            return !in_array($flag, ['new', 'done']);
        });
    }



    /**
     * Return a ready to use array of priorities for combo widget
     *
     * @return array
     */
    public static function prioritiesCombo()
    {
        return array_map(
             function ($priority) {
                 return [
                      'title' => static::getFlagTitle($priority),
                      'value' => $priority,
                 ];
             },
             static::priorities()
        );
    }



    /**
     * Return a map to find a color from a flag.
     *
     * @return array
     */
    public static function flagColors()
    {
        return [
             'new'             => 'primary',
             'high-priority'   => 'danger',
             'medium-priority' => 'warning',
             'low-priority'    => 'info',
             'done'            => 'success',
        ];
    }



    /**
     * Return the color of the given flag.
     *
     * @param string $flag
     *
     * @return string|null
     */
    public static function getFlagColor(string $flag)
    {
        $flag_colors = static::flagColors();
        return ($flag_colors[$flag] ?? null);
    }



    /**
     * Return a map to find an icon from a flag.
     *
     * @return array
     */
    public static function flagIcons()
    {
        return [
             'new'             => 'folder-open',
             'high-priority'   => 'arrow-circle-up',
             'medium-priority' => 'dot-circle',
             'low-priority'    => 'arrow-circle-down',
             'done'            => 'thumbs-up',
        ];
    }



    /**
     * Return the icon of the given flag.
     *
     * @param string $flag
     *
     * @return string|null
     */
    public static function getFlagIcon(string $flag)
    {
        $flag_icons = static::flagIcons();
        return ($flag_icons[$flag] ?? null);
    }



    /**
     * Return the title of the given flag.
     *
     * @param string $flag
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function getFlagTitle(string $flag)
    {
        return trans("tickets::flags.$flag");
    }



    /**
     * Returns an array of information about the given flag for api.
     *
     * @param string $flag
     *
     * @return array
     */
    public static function getFlagApiInfo(string $flag)
    {
        return [
             'slug'  => $flag,
             'title' => static::getFlagTitle($flag),
             'color' => static::getFlagColor($flag),
             'icon'  => static::getFlagIcon($flag),
        ];
    }



    /**
     * The tabs for the browse page of tickets.
     *
     * @return array
     */
    public static function browseTabs()
    {
        $flags = static::flags();
        $tabs  = [];

        foreach ($flags as $flag) {
            $tabs[] = [$flag, static::getFlagTitle($flag)];
        }

        return $tabs;
    }



    /**
     * Return all available labels for tickets.
     *
     * @return Collection
     */
    public static function availableLabels()
    {
        return model('ticket')->definedLabels()->get();
    }



    /**
     * return all ticket types in a collection.
     *
     * @return Collection
     */
    public static function availableTicketTypes()
    {
        return model('ticket-type')->get();
    }



    /**
     * Check if the given remark is a ticket or not.
     *
     * @param Remark $remark
     *
     * @return bool
     */
    public static function isTicket(Remark $remark)
    {
        return ($remark instanceof Ticket);
    }



    /**
     * Check if the given remark is not a ticket.
     *
     * @param Remark $remark
     *
     * @return bool
     */
    public static function isNotTicket(Remark $remark)
    {
        return !static::isTicket($remark);
    }



    /**
     * Returns the uploader
     *
     * @return \Modules\Filemanager\Services\Tools\Uploader\UploaderAbstract
     */
    public static function getTicketUploader()
    {
        return fileManager()
             ->uploader('tickets')
             ->posttypeConfig('attachments')
             ;
    }



    /**
     * Returns an instance of the `Modules\Filemanager\Services\Tools\VirtualUploader\Uploader` class
     * to upload the given file.
     *
     * @param UploadedFile $file
     *
     * @return \Modules\Filemanager\Services\Tools\VirtualUploader\Uploader
     */
    public static function getVirtualUploader(UploadedFile $file)
    {
        return fileManager()
             ->virtualUploader($file)
             ->setPosttypeConfig(posttype('tickets'), 'attachments')
             ;
    }
}
