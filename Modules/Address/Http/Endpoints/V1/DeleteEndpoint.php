<?php

namespace Modules\Address\Http\Endpoints\V1;

use Modules\Address\Http\Controllers\V1\UpdateController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/address-delete
 *                    Delete endpoint
 * @apiDescription    Delete user's addresses
 * @apiVersion        1.0.0
 * @apiName           Delete User's Addresses
 * @apiGroup          Address
 * @apiPermission     User,SuperAdmin
 * @apiParam {String} hashid hashid of address
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => "RNplN",
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  UpdateController controller()
 */
class DeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Delete User's Addresses";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Address\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UpdateController@deleteAddress';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => "RNplN",
        ]);
    }
}
