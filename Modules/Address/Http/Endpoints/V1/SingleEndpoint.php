<?php

namespace Modules\Address\Http\Endpoints\V1;

use Modules\Address\Http\Controllers\V1\SingleController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/address-single
 *                    Single Address
 * @apiDescription    Single Address
 * @apiVersion        1.0.0
 * @apiName           Single Address
 * @apiGroup          Address
 * @apiPermission     User,SuperAdmin
 * @apiParam {String} hashid        Hashid of address
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *            "authenticated_user": "PNOLA"
 *      },
 *      "results": {
 *           "id": 9,
 *          "user_id": 3,
 *          "address": "Resalat street",
 *          "postal_code": "2132311321321",
 *          "location": "xxx",
 *          "telephone": "09364588132",
 *          "province_id": 322132,
 *          "city_id": 12,
 *          "receiver": "0",
 *          "verified": 0,
 *          "default": 0,
 *          "meta": [],
 *          "created_at": "2018-12-30 15:39:40",
 *          "updated_at": "2018-12-30 15:39:40",
 *          "deleted_at": null,
 *          "created_by": 2,
 *          "updated_by": 0,
 *          "deleted_by": 0
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  SingleController controller()
 */
class SingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Single Address";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Address\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SingleController@singleAddress';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "authenticated_user" => "PNOLA",
        ], [
             "id"          => "pxkYN",
             "user_id"     => "PNOLA",
             "address"     => "Resalat street",
             "postal_code" => "2132311321321",
             "location"    => "xxx",
             "telephone"   => "09364588132",
             "province_id" => "BmovV",
             "city_id"     => 12,
             "receiver"    => "0",
             "verified"    => 0,
             "created_at"  => 1546171780,
             "updated_at"  => 1546171780,
             "deleted_at"  => null,
             "updated_by"  => "PKEnN",
             "created_by"  => "QKgJx",
             "deleted_by"  => null,
        ]);
    }
}
