<?php

namespace Modules\Address\Http\Endpoints\V1;

use Modules\Address\Http\Controllers\V1\UpdateController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/address-save
 *                    Create and Update user's addresses
 * @apiDescription    Create and Update user's addresses
 * @apiVersion        1.0.0
 * @apiName           Create and Update user's addresses
 * @apiGroup          Address
 * @apiPermission     User,SuperAdmin
 * @apiParam {String}       [user_id=0] Hashid of user
 * @apiParam {String}       [hashid]  Hashid of address
 * @apiParam {longText}     [address]  description of address
 * @apiParam {String}       [postal_code] valid postal code of address
 * @apiParam {String}       [location]  location of address
 * @apiParam {String}       [telephone]  telephone of address
 * @apiParam {integer}      [province_id]  Hashid of address
 * @apiParam {integer}      [city_id]  city of address
 * @apiParam {String}       [receiver]  receiver's name of address
 * @apiParam {boolean}      [verified=false]  Hashid of address
 * @apiParam {boolean}      [default=false]  Hashid of address
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  UpdateController controller()
 *          controller
 */
class SaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Create And Update User's Addresses";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Address\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UpdateController@updateAddress';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
