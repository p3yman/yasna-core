<?php

namespace Modules\Address\Http\Endpoints\V1;

use Modules\Address\Http\Controllers\V1\ListController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/address-list
 *                    Get all user's addresses
 * @apiDescription    get all user's addresses
 * @apiVersion        1.0.0
 * @apiName           Get all user's addresses
 * @apiGroup          Address
 * @apiPermission     User,SuperAdmin
 * @apiParam {String} [user]    Hashid of user
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "id": "kxPdx",
 *          "user_id": "qKXVA",
 *          "address": "Resalat street - maryam street",
 *          "postal_code": null,
 *          "location": null,
 *          "telephone": null,
 *          "province_id": null,
 *          "city_id": null,
 *          "receiver": null,
 *          "verified": 0,
 *          "created_at": 1546162985,
 *          "updated_at": 1546162985,
 *          "deleted_at": null,
 *          "updated_by": "PKEnN",
 *          "created_by": "qKXVA",
 *          "deleted_by": null
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  ListController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get All User's Addresses";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Address\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ListController@addressList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "authenticated_user" => "qKXVA",
        ],
         [
              "id"          => "DAvQN",
              "user_id"     => "qKXVA",
              "address"     => "Resalat street",
              "postal_code" => null,
              "location"    => null,
              "telephone"   => null,
              "province_id" => null,
              "city_id"     => null,
              "receiver"    => null,
              "verified"    => 0,
              "created_at"  => 1546159790,
              "updated_at"  => 1546161918,
              "deleted_at"  => null,
              "updated_by"  => "qKXVA",
              "created_by"  => "qKXVA",
              "deleted_by"  => null,
         ]);

    }
}
