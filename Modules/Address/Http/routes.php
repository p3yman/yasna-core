<?php

Route::group([
    'middleware' => 'web',
    'prefix'     => 'address',
    'namespace'  => 'Modules\Address\Http\Controllers'
], function () {
//    Route::get('/', 'AddressController@index');

    Route::post('save', 'AddressController@save')->name('address.save');
    Route::get('remove/{hashid}', 'AddressController@remove')->name('address.remove');
    Route::get('make-default/{hashid}', 'AddressController@makeDefault')->name('address.makeDefault');
});
