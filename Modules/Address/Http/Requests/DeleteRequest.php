<?php

namespace Modules\Address\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DeleteRequest extends YasnaRequest
{
    protected $model_name         = "address";
    protected $responder          = "white-house";
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (auth()->guest() or $this->model->not_exists) {
            return false;
        }

        return $this->model->canDelete();
    }

}
