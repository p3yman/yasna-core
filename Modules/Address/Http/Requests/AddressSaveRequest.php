<?php

namespace Modules\Address\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class AddressSaveRequest extends YasnaRequest
{
    protected $model_name = 'address';



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'id'          => [
                  Rule::exists('addresses', 'id', function ($query) {
                      $query->where('user_id', user()->id);
                  }),
             ],
             'receiver'    => 'required|string',
             'city_id'     => [
                  'required',
                  Rule::exists('states', 'id')->where(function ($query) {
                      $query->where('parent_id', '<>', 0);
                  }),
             ],
             'address'     => 'required',
             'telephone'   => 'required|phone',
             'postal_code' => 'required|postal_code',
        ];
    }



    /**
     * Correct data
     */
    public function corrections()
    {
        if (!$this->data['id']) {
            unset($this->data['id']);
        }

        $this->data['user_id'] = user()->id;
    }



    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return boolval(user()->exists);
    }



    /**
     * The purification map.
     *
     * @return array
     */
    public function purifier()
    {
        return [
             'id'          => 'dehash',
             'postal_code' => 'ed',
             'location'    => 'ed',
             'telephone'   => 'ed',
             'province_id' => 'ed',
             'city_id'     => 'ed',
        ];
    }
}
