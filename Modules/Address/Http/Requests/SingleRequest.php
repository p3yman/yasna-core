<?php

namespace Modules\Address\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class SingleRequest extends YasnaRequest
{
    protected $model_name = "address";
    protected $responder  = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (auth()->guest()) {
            return false;
        }

        if ($this->model->exists) {
            return $this->model->canView();
        }

        return false;
    }


}
