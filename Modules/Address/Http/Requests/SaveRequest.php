<?php

namespace Modules\Address\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class SaveRequest extends YasnaRequest
{
    public    $user;
    protected $model_name = "address";
    protected $responder  = "white-house";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "postal_code" => "string|postal_code",
         ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'address',
             'postal_code',
             'location',
             'telephone',
             'province_id',
             'city_id',
             'receiver',
             'user_id',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (auth()->guest()) {
            return false;
        }

        if ($this->model->exists) {
            return $this->model->canEdit();
        }

        $user_id = $this->data['user_id'];
        return $this->model->canCreateFor($user_id);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctUser();
    }



    /**
     * Fill user property with correct user
     */
    protected function correctUser()
    {
        if ($this->model->exists) {
            $this->setUserId($this->model->user_id);
            return;
        }


        $specifier_user_id = ($this->data['user_id'] ?? null);

        if ($specifier_user_id) {
            return;
        }

        $this->setUserId(user()->id);
    }



    /**
     * set current user_id in model user_id
     *
     * @param $user_id
     */
    protected function setUserId($user_id)
    {
        $this->data['user_id'] = hashid($user_id);
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'user_id' => 'dehash',
             'postal_code' => 'ed',
        ];
    }


}
