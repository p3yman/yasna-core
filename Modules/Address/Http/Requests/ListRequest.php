<?php

namespace Modules\Address\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class ListRequest extends YasnaRequest
{
    public    $user;
    protected $model_name = "address";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (
             ($this->filled('user') and user()->isSuperadmin())
             or (!$this->filled('user') and user()->exists)
        ) {
            return true;
        }

        return false;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctUser();
    }



    /**
     * Fill user property with correct user
     */
    protected function correctUser()
    {
        if ($this->filled('user')) {
            $this->user = model('user', $this->input('user'));
            return;
        }

        $this->user = user();
    }
}
