<?php

namespace Modules\Address\Http\Controllers\V1;

use Modules\Address\Http\Requests\DeleteRequest;
use Modules\Address\Http\Requests\SaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class UpdateController extends YasnaApiController
{


    /**
     * create and update users's addresses
     *
     * @param SaveRequest $request
     */
    public function updateAddress(SaveRequest $request)
    {
        $request->model->batchSave($request, ['user']);
    }



    /**
     * delete user's addresses
     *
     * @param DeleteRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAddress(DeleteRequest $request)
    {
        if ($request->model->delete()) {
            return $this->success([
                 'done' => 1,
            ]);
        }

        return $this->clientError("address-101");
    }
}
