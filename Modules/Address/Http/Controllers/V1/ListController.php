<?php

namespace Modules\Address\Http\Controllers\V1;

use Illuminate\Support\Facades\Auth;
use Modules\Address\Http\Requests\ListRequest;
use Modules\Yasna\Services\YasnaApiController;

class ListController extends YasnaApiController
{

    /**
     * get all user's addresses
     *
     * @param ListRequest $request
     *
     * @return array
     */
    public function addressList(ListRequest $request)
    {

        $addresses = model('address')
             ->where('user_id', $request->user->id)
             ->get()
        ;

        $result = $this->addToMapAddress($addresses);

        return $this->success($result);
    }



    /**
     * add map on addresses query
     *
     * @param $addresses
     *
     * @return array
     */
    public function addToMapAddress($addresses)
    {
        $result = $addresses->map(function ($addresses) {
            return $addresses->toListResource();
        })->toArray()
        ;

        return $result;

    }

}
