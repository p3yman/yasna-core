<?php

namespace Modules\Address\Http\Controllers\V1;

use Modules\Address\Http\Requests\SingleRequest;
use Modules\Yasna\Services\YasnaApiController;

class SingleController extends YasnaApiController
{

    /**
     * get all user's addresses
     *
     * @param SingleRequest $request
     *
     * @return array
     */
    public function singleAddress(SingleRequest $request)
    {
        $address = $request->model->toSingleResource();

        return $this->success($address);
    }


}
