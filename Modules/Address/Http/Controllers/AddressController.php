<?php

namespace Modules\Address\Http\Controllers;

use App\Models\Address;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Address\Http\Requests\AddressSaveRequest;

class AddressController extends Controller
{
    use AddressControllerTrait;



    /**
     * Save an address for the logged in user.
     *
     * @param AddressSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(AddressSaveRequest $request)
    {
        // clear user default address
        user()->addresses()
              ->where('default', 1)
              ->update(['default' => 0])
        ;

        $request->merge([
             "province_id" => $this->findProvinceId($request->city_id),
        ]);

        $saved_address = $request->model->batchSave($request);

        // set the new address as default
        $saved_address->update(['default' => 1]);

        $success_callback = 'addressAddedCallback';
        return $this->jsonAjaxSaveFeedback($saved_address->exists, [
             "success_message"      => trans("address::base.message.success.save"),
             "success_callback"     => "if(typeof $success_callback == 'function'){ $success_callback('$saved_address->hashid') }",
             "success_form_reset"   => 1,
             "success_feed_timeout" => 1000,
        ]);
    }



    /**
     * Remove an address.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request)
    {
        $address = model('address', $request->hashid);

        if ($address and $address->exists and $address->user_id = user()->id) {
            $address->delete();
            return \response()->json(['success' => true]);
        } else {
            return \response()->json(['success' => false], 500);
        }
    }



    /**
     * Set an address as default address of the current user
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function makeDefault(Request $request)
    {
        $address = Address::findByHashid($request->hashid);

        if ($address and $address->exists and ($address->user_id == user()->id)) {
            user()->addresses()
                  ->where('default', 1)
                  ->update(['default' => 0])
            ;

            $address->update(['default' => 1]);
            return \response()->json(['success' => true]);
        }

        return \response()->json(['success' => false], 500);
    }



    /**
     * Find the province of the given city.
     *
     * @param int|string $city_id
     *
     * @return int|string
     */
    protected function findProvinceId($city_id)
    {
        return model('state')
             ->firstOrNew([
                  'id' => $city_id,
                  ['parent_id', '<>', 0],
             ])->parent_id
             ;
    }
}
