<?php

namespace Modules\Address\Entities\Traits;

trait ResourcesTrait
{
    /**
     * boot ResourcesTrait
     *
     * @return void
     */
    public static function bootResourcesTrait()
    {
        static::addDirectResources([
             "user_id",
             "address",
             "postal_code",
             "location",
             "telephone",
             "province_id",
             "city_id",
             "receiver",
             "verified",
        ]);
    }
}
