<?php

namespace Modules\Address\Entities\Traits;

use App\Models\Address;

trait AddressUserTrait
{
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }
}
