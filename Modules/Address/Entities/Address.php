<?php

namespace Modules\Address\Entities;

use App\Models\State;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Address\Entities\Traits\ResourcesTrait;
use Modules\Address\Observers\AddressObserver;
use Modules\Yasna\Services\YasnaModel;

/**
 * Class Address
 *
 * @property int $user_id
 */
class Address extends YasnaModel
{
    use SoftDeletes;
    use ResourcesTrait;

    protected $guarded = ['id'];

    private $cached_city;
    private $cached_province;
    private $text_array = [];



    /**
     * One-to-Many Relationship for the City
     *
     * @param bool $force_fresh
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city($force_fresh = false)
    {
        if (!$this->cached_city or $force_fresh) {
            $this->cached_city = $this->belongsTo(State::class, 'city_id');
        }

        return $this->cached_city;
    }



    /**
     * One-to-Many Relationship for the Province
     *
     * @param bool $force_fresh
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province($force_fresh = false)
    {
        if (!$this->cached_province or $force_fresh) {
            $this->cached_province = $this->belongsTo(State::class, 'province_id');
        }

        return $this->cached_province;
    }



    /**
     * Returns the text array to be shown to clients.
     *
     * @param bool $force_fresh
     *
     * @return array
     */
    private function textArray($force_fresh = false)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (count($this->text_array) and !$force_fresh) {
            return $this->text_array;
        }

        $this->text_array = [];

        /*-----------------------------------------------
        | City Name ...
        */
        if ($this->city and $this->city->exists()) {
            $this->text_array[] = $this->city->full_name;
        } elseif ($this->province and $this->province->exists()) {
            $this->text_array[] = $this->province->full_name;
        }

        /*-----------------------------------------------
        | Address Text ...
        */
        $this->text_array[] = $this->address;

        /*-----------------------------------------------
        | Postal Code ...
        */
        if ($this->postal_code) {
            $this->text_array[] = trans_safe("address::phrases.postal_code") . SPACE . $this->postal_code;
        }

        /*-----------------------------------------------
        | Telephone ...
        */
        if ($this->telephone) {
            $this->text_array[] = trans_safe("address::phrases.telephone") . SPACE . $this->telephone;
        }

        /*-----------------------------------------------
        | Receiver ...
        */
        $this->text_array[] = $this->receiver;

        /*-----------------------------------------------
        | Return ...
        */
        return $this->text_array;
    }



    /**
     * Returns a text version of the address.
     *
     * @return string
     */
    public function fullText()
    {
        return implode(trans_safe("manage::forms.general.comma") . SPACE, $this->textArray());
    }



    /**
     * Accessor fot the `fullText()` Method
     *
     * @return string
     */
    public function getFullTextAttribute()
    {
        return $this->fullText();
    }



    /**
     * check if the current online user can perform edit action
     *
     * @return bool
     */
    public function canEdit(): bool
    {
        if (user()->id == $this->user_id) {
            return true;
        }

        $user = user($this->user_id);

        return $user->canDo('edit');
    }



    /**
     * check if the current user can create address
     *
     * @return bool
     */
    public function canCreate(): bool
    {
        return user()->exists;
    }



    /**
     * check if the current user cann't create address
     *
     * @return bool
     */
    public function cannotCreate(): bool
    {
        return !$this->canCreate();
    }



    /**
     * check current user create address for which user
     *
     * @param int $user_id
     *
     * @return bool
     */
    public function canCreateFor(int $user_id): bool
    {
        if ($this->cannotCreate()) {
            return false;
        }

        if ($user_id == user()->id) {
            return true;
        }

        $user = user($user_id);
        return $user->canDo('create');
    }



    /**
     * check current user cann't create address for another user
     *
     * @param int $user_id
     *
     * @return bool
     */
    public function cannotCreateFor(int $user_id): bool
    {
        return !$this->canCreateFor($user_id);
    }



    /**
     * check if the model can be deleted by the current user
     *
     * @return bool
     */
    public function canDelete()
    {
        if ($this->user_id == user()->id) {
            return true;
        }

        $user = user($this->user_id);
        return $user->canDo('delete');
    }



    /**
     * check if the current online user can view address
     *
     * @return bool
     */
    public function canView(): bool
    {
        if (user()->id == $this->user_id) {
            return true;
        }

        $user = user($this->user_id);

        return $user->canDo('view');
    }



    /**
     * @inheritdoc
     */
    public static function boot()
    {
        parent::boot();

        static::observe(AddressObserver::class);
    }
}
