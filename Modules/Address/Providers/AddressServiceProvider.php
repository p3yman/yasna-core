<?php

namespace Modules\Address\Providers;

use Modules\Address\Http\Endpoints\V1\ListEndpoint;
use Modules\Address\Http\Endpoints\V1\DeleteEndpoint;
use Modules\Address\Http\Endpoints\V1\SaveEndpoint;
use Modules\Address\Http\Endpoints\V1\SingleEndpoint;
use Modules\Yasna\Services\YasnaProvider;

class AddressServiceProvider extends YasnaProvider
{
    public function index()
    {
        $this->registerModelTraits();
        $this->registerEndpoints();
    }

    protected function registerModelTraits()
    {
        module("yasna")
            ->service("traits")
            ->add()->trait("Address:AddressUserTrait")->to("User");
    }



    /**
     * register endpoints
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(ListEndpoint::class);
        endpoint()->register(SaveEndpoint::class);
        endpoint()->register(DeleteEndpoint::class);
        endpoint()->register(SingleEndpoint::class);
    }
}
