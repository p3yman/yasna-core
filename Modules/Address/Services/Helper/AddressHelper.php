<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 7/29/18
 * Time: 5:07 PM
 */

namespace Modules\Address\Services\Helper;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class AddressHelper
{
    /**
     * @var Builder The builder.
     */
    protected $builder;



    /**
     * Set a condition for the user.
     *
     * @param User|string|int $user_identifier
     *
     * @return $this
     */
    public function user($user_identifier)
    {
        if ($user_identifier instanceof User) {
            $user = $user_identifier;
        } else {
            $user = model('user', $user_identifier);
        }

        $this->where('user_id', $user->id);

        return $this;
    }



    /**
     * Generate the builder.
     *
     * @return void
     */
    protected function generateBuilder()
    {
        $this->builder = model('address')
             ->select();
    }



    /**
     * AddressHelper constructor.
     *
     * @param User|string|int|null $user
     */
    public function __construct($user = null)
    {
        $this->generateBuilder();

        if (!is_null($user)) {
            $this->user($user);
        }
    }



    /**
     * AddressHelper call magic method
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->builder->$method(...$parameters);
    }
}
