<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')
                ->default(0)
                ->index()
                ->foreign('users');

            $table->longText('address')->nullable();

            $table->string('postal_code')->nullable();
            $table->string('location')->nullable(); // Latitude and Longitude (in comma separated format)
            $table->string('telephone')->nullable();

            $table->unsignedInteger('province_id')
                ->nullable()
                ->foreign('states')
                ->onDelete('NO ACTION');
            $table->unsignedInteger('city_id')
                ->nullable()
                ->foreign('states')
                ->onDelete('NO ACTION');

            $table->string('receiver')->nullable();

            $table->boolean('verified')->default(false); // True if this address is verified
            $table->boolean('default')->default(false); // True if this address is user's default address


            $table->longText('meta')->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0);
            $table->unsignedInteger('deleted_by')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
