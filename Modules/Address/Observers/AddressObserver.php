<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/16/18
 * Time: 1:39 PM
 */

namespace Modules\Address\Observers;


use App\Models\Address;

class AddressObserver
{
    /**
     * Handles the `province_id` and the `city_id` fields while saving an address.
     *
     * @param Address $model
     */
    protected function handleProvinceWhileSaving(Address $model)
    {
        if ($model->province_id) {
            return;
        }

        if (!$model->city_id) {
            return;
        }
        $city = model('state')->grabId($model->city_id);

        if ($city->isNotCity()) {
            $model->city_id = null;
            return;
        }

        $province = $city->province;

        $model->province_id = $province->id;
    }



    /**
     * Observes the saving event.
     *
     * @param Address $model
     */
    public function saving(Address $model)
    {
        $this->handleProvinceWhileSaving($model);
    }



    /**
     * Handles the `user_id` field while creating a new address.
     *
     * @param Address $model
     */
    protected function handleUserIdWhileCreating(Address $model)
    {
        if ($model->user_id) {
            return;
        }

        if (auth()->guest()) {
            return;
        }

        $model->user_id = user()->id;
    }



    /**
     * Observes the creating event.
     *
     * @param Address $model
     */
    public function creating(Address $model)
    {
        $this->handleUserIdWhileCreating($model);
    }
}
