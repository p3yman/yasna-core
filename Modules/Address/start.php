<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("addresses")) {
    /**
     * Find addresses for all or the given user
     *
     * @param \App\Models\User|string|int|null $user
     *
     * @return \Modules\Address\Services\Helper\AddressHelper
     */
    function addresses($user = null)
    {
        return new \Modules\Address\Services\Helper\AddressHelper($user);
    }
}