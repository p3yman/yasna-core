<?php

namespace Modules\Endpoint\Providers;

use Modules\Endpoint\Console\MakeEndpointCommand;
use Modules\Endpoint\Http\Endpoints\V1\ListEndpoint;
use Modules\Endpoint\Http\Endpoints\V1\LoginCheckEndpoint;
use Modules\Endpoint\Http\Endpoints\V1\ModelDestroyEndpoint;
use Modules\Endpoint\Http\Endpoints\V1\ModelSingleEndpoint;
use Modules\Endpoint\Http\Endpoints\V1\ModelListEndpoint;
use Modules\Endpoint\Http\Endpoints\V1\ModelRestoreEndpoint;
use Modules\Endpoint\Http\Endpoints\V1\RefreshTokenEndpoint;
use Modules\Endpoint\Http\Endpoints\V1\ModelTrashEndpoint;
use Modules\Endpoint\Http\Middleware\EndpointLocaleMiddleware;
use Modules\Endpoint\Http\Middleware\EndpointTokenMiddleware;
use Modules\Endpoint\Http\Middleware\MockLoginMiddleware;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class EndpointServiceProvider
 *
 * @package Modules\Endpoint\Providers
 */
class EndpointServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerServices();
        $this->registerEndpoints();
        $this->registerArtisanCommands();
        $this->registerMiddlewares();
    }



    /**
     * register middleware alias
     */
    private function registerMiddlewares()
    {
        $this->app['router']->prependMiddlewareToGroup('api', MockLoginMiddleware::class);

        $this->addMiddleware("endpointToken", EndpointTokenMiddleware::class);
        $this->addMiddleware("endpointLocale", EndpointLocaleMiddleware::class);
    }



    /**
     * register services
     */
    private function registerServices()
    {
        module($this->moduleName())
             ->register(endpoint()::SERVICE_NAME, "keeps the endpoint router classes");
    }



    /**
     * register artisan commands
     */
    private function registerArtisanCommands()
    {
        $this->addArtisan(MakeEndpointCommand::class);
    }



    /**
     * register the in-house endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        endpoint()->register(ListEndpoint::class);
        endpoint()->register(LoginCheckEndpoint::class);
        endpoint()->register(RefreshTokenEndpoint::class);

        endpoint()->register(ModelTrashEndpoint::class);
        endpoint()->register(ModelRestoreEndpoint::class);
        endpoint()->register(ModelDestroyEndpoint::class);
        endpoint()->register(ModelSingleEndpoint::class);
        endpoint()->register(ModelListEndpoint::class);
    }

}
