<?php

namespace Modules\Endpoint\Services;

use Illuminate\Support\Facades\Request;
use Modules\Endpoint\Http\Middleware\MockLoginMiddleware;
use Exception;

trait EndpointMiddlewaresTrait
{
    /**
     * keep an array of registered middlewares
     *
     * @var array
     */
    private $middlewares = [];

    /**
     * keep an instance of the Request, received from the last-executed middleware
     *
     * @var Request
     */
    private $new_request;


    /**
     * keep the middleware error message (if any)
     *
     * @var null|string
     */
    private $middleware_error_message = null;



    /**
     * register all middlewares
     *
     * @return void
     */
    protected function registerMiddlewares()
    {
        // Override this method in your endpoints to set all the middlewares you need.
    }



    /**
     * set a middleware in the middlewares stack
     *
     * @param string $class
     * @param array  $options
     *
     * @return void
     */
    protected function setMiddleware(string $class, ... $options)
    {
        $this->middlewares[] = [
             "class"   => $class,
             "options" => $options,
        ];
    }



    /**
     * process middlewares
     */
    private function processMiddlewares()
    {
        $this->registerMiddlewares();
        $this->setMiddleware(MockLoginMiddleware::class);

        try {
            $this->cycleThroughMiddlewares();
        } catch (Exception $e) {
            $this->middleware_error_message = $e->getMessage();
        }
    }



    /**
     * determine if error occurred while processing middlewares
     *
     * @return bool
     */
    private function hasMiddlewareError()
    {
        return boolval(($this->middleware_error_message));
    }



    /**
     * dispatch the middleware error
     *
     * @return mixed
     */
    private function dispatchMiddlewareError()
    {
        return api()->withErrors([$this->middleware_error_message])->clientErrorRespond('endpoint-600');
    }



    /**
     * cycle  through the registered middlewares
     */
    private function cycleThroughMiddlewares()
    {
        $request = request();

        foreach ($this->getAllApplicableMiddlewares() as $middleware) {
            $request = $this->callMiddleware($middleware, $request);
        }
    }



    /**
     * get a combined list of all applicable middlewares
     *
     * @return array
     */
    private function getAllApplicableMiddlewares()
    {
        return array_merge(
             endpoint()->getGlobalMiddlewares(),
             $this->middlewares
        );
    }



    /**
     * call a single middleware
     *
     * @param array   $middleware
     * @param Request $request
     *
     * @return Request
     */
    private function callMiddleware(array $middleware, $request)
    {
        $class_name = $middleware['class'];
        $options    = $middleware['options'];
        $instance   = new $class_name();

        $instance->handle($request, $new_request = function ($request) {
            $this->new_request = $request;
        }, ... $options);

        return $this->new_request;
    }

}
