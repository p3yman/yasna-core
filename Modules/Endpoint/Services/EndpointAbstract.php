<?php

namespace Modules\Endpoint\Services;


use App\Models\User;
use Modules\Yasna\Services\GeneralTraits\ClassRecognitionsTrait;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

abstract class EndpointAbstract implements EndpointInterface
{
    use ClassRecognitionsTrait;
    use ModuleRecognitionsTrait;

    const HTTP_GET     = "GET";
    const HTTP_PUT     = "PUT";
    const HTTP_POST    = "POST";
    const HTTP_PATCH   = "PATCH";
    const HTTP_DELETE  = "DELETE";
    const HTTP_OPTIONS = "OPTIONS";

    /**
     * keep the instance of the online/mocked user
     *
     * @var User
     */
    protected $user;



    /**
     * Endpoint constructor.
     */
    public function __construct()
    {
        $this->user = user();
    }



    /**
     * check if the Endpoint is generally suitable to proceed
     *
     * @return bool
     */
    public function canProceed()
    {
        if (!$this->hasPermit()) {
            if (!request()->error) {
                request()->error = 403;
            }
            return false;
        }
        if (!$this->isAllowed()) {
            request()->error = 405;
            return false;
        }
        if (!$this->controllerExists() and !request()->mock) {
            request()->error = 501;
            return false;
        }

        return true;
    }



    /**
     * check if the Endpoint is NOT generally suitable to proceed
     *
     * @return bool
     */
    public function cannotProceed()
    {
        return !$this->canProceed();
    }



    /**
     * check if the request http method matches the one Endpoint is for
     *
     * @return bool
     */
    public function isAllowed()
    {
        return ($this->getValidMethod() == request()->getMethod());
    }



    /**
     * check if the controller class and method exists
     *
     * @return bool
     */
    public function controllerExists()
    {
        $class  = str_before($this->controller(), "@");
        $method = str_after($this->controller(), "@");

        return method_exists($class, $method);
    }



    /**
     * get the version of the Endpoint, based on the namespace
     *
     * @return int
     */
    public static function getVersion(): int
    {
        $namespace = static::reflector()->getNamespaceName();
        $last_part = array_last(explode('\\', $namespace));

        return (int)str_after($last_part, "V");
    }



    /**
     * get a suitable title to be used on the service environment
     *
     * @return string
     */
    public static function getServiceTitle(): string
    {
        $module  = static::getModuleName();
        $version = static::getVersion();
        $name    = static::getEndpointName();

        return endpoint()->makeServiceName($module, $name, $version);
    }



    /**
     * get endpoint name, out of the called class name
     *
     * @return string
     */
    public static function getEndpointName(): string
    {
        return str_before(static::calledClassName(), "Endpoint");
    }



    /**
     * get the modular url of the endpoint
     *
     * @return string
     */
    public static function getModularUrl(): string
    {
        $version = "v" . static::getVersion();
        $module  = kebab_case(static::getModuleName());
        $name    = kebab_case(static::getEndpointName());

        return url("api/modular/$version/$module-$name");
    }



    /**
     * proceed the Endpoint
     *
     * @return mixed
     */
    public function proceed()
    {
        //if($this->hasMiddlewareError()) {
        //    return $this->dispatchMiddlewareError();
        //}

        return app()->call($this->controller());
    }



    /**
     * get the fully qualified name of the controller
     *
     * @return string
     */
    protected function controller()
    {
        return $this->getControllerNamespace() . "\\" . $this->getController();
    }
}
