<?php

namespace Modules\Endpoint\Services;

use Modules\Yasna\Services\ModuleHelper;

trait HelperServicesTrait
{
    /**
     * register a new endpoint class (to be called from the service provider layer)

     *
*@param string|EndpointAbstract $class (fully qualified string)
     */
    public function register(string $class)
    {
        /** @var EndpointAbstract $instance */
        $instance = new $class();

        $this->service()
             ->add($class::getServiceTitle())
             ->caption($class::getTitle())
             ->order(static::$order++)
             ->set('version', $class::getVersion())
             ->set('module', $class::getModuleName())
             ->set('method', $instance->getValidMethod())
             ->set('url', $class::getModularUrl())
             ->class($class)
        ;
    }



    /**
     * get registered endpoints
     *
     * @param string     $module
     * @param int|string $version
     *
     * @return array
     */
    public function list($module = null, $version = null)
    {
        $filter = $this->makeFilter($module, $version);

        return $this->service()->read($filter);
    }



    /**
     * get a registered services as a paired array of key-title values
     *
     * @param string     $module
     * @param int|string $version
     *
     * @return array
     */
    public function getTitles($module = null, $version = null)
    {
        $filter = $this->makeFilter($module, $version);

        return $this->service()->paired("caption", "key", $filter);
    }



    /**
     * get a registered services as a paired array of key-class values
     *
     * @param string     $module
     * @param int|string $version
     *
     * @return array
     */
    public function getClasses($module = null, $version = null)
    {
        $filter = $this->makeFilter($module, $version);
        return $this->service()->paired("class", "key", $filter);
    }



    /**
     * get the fully qualified class of a given endpoint name
     *
     * @param string $endpoint_name
     * @param int    $version
     *
     * @return bool|EndpointAbstract
     */
    public function getEndpointClass(string $endpoint_name, $version)
    {
        $module_name   = studly_case(str_before($endpoint_name, "-"));
        $endpoint_name = strtolower(str_after($endpoint_name, "-"));
        $class_name    = studly_case($endpoint_name);
        $service_name  = $this->makeServiceName($module_name, $class_name, $version);

        if ($this->hasnot($service_name)) {
            return false;
        }

        $class = $this->getClasses()[$service_name];

        if (!class_exists($class)) {
            return false;
        }

        return $class;
    }



    /**
     * get the object instance of the given endpoint name
     *
     * @param string $endpoint_name
     * @param int    $version
     *
     * @return bool|EndpointAbstract
     */
    public function getEndpointInstance($endpoint_name, $version)
    {
        $class = $this->getEndpointClass($endpoint_name, $version);
        if (!$class) {
            return false;
        }

        $object = new $class();

        return $object;
    }



    /**
     * check if an endpoint is registered
     *
     * @param string $class_name
     *
     * @return bool
     */
    public function has($endpoint_name)
    {
        return $this->service()->has($endpoint_name);
    }



    /**
     * check if an endpoint is not registered
     *
     * @param string $endpoint_name
     *
     * @return bool
     */
    public function hasnot($endpoint_name)
    {
        return !$this->has($endpoint_name);
    }



    /**
     * make and return a typical service name, with the given values
     *
     * @param string $module_name
     * @param string $service_name
     * @param int    $version
     *
     * @return string
     */
    public function makeServiceName($module_name, $service_name, $version)
    {
        return kebab_case($module_name . $service_name) . "-v" . $version;
    }



    /**
     * get the instance of the current service
     *
     * @return ModuleHelper
     */
    protected function service()
    {
        return module("Endpoint")->service(static::SERVICE_NAME);
    }



    /**
     * make and return a filter suitable for the service environment
     *
     * @param string     $module
     * @param int|string $version
     *
     * @return array
     */
    protected function makeFilter($module, $version)
    {
        $filter = [];

        if (!is_null($version)) {
            $filter['version'] = $version;
        }
        if (!is_null($module)) {
            $filter['module'] = studly_case($module);
        }

        return $filter;
    }

}
