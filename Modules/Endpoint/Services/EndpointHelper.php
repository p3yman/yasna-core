<?php

namespace Modules\Endpoint\Services;


use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class EndpointHelper
{
    const SERVICE_NAME = "endpoints";

    use ModuleRecognitionsTrait;
    use HelperServicesTrait;
    use HelperVersionsTrait;
    use HelperMiddlewaresTrait;

    /**
     * keep the order of registration, useful for rules and groups
     *
     * @var int
     */
    protected static $order = 1;
}
