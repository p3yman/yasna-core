<?php

namespace Modules\Endpoint\Services;

trait HelperMiddlewaresTrait
{
    /**
     * keep an array of global middlewares to be applied on all endpoints
     *
     * @var array
     */
    protected static $global_middlewares = [];



    /**
     * set a middleware in the global-middlewares stack
     *
     * @param string $class
     * @param array  $options
     *
     * @return void
     */
    public function setGlobalMiddleware(string $class, ... $options)
    {
        static::$global_middlewares[] = [
             "class"   => $class,
             "options" => $options,
        ];
    }



    /**
     * get the array of global middlewares
     *
     * @return array
     */
    public function getGlobalMiddlewares()
    {
        return static::$global_middlewares;
    }
}
