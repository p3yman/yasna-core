<?php

namespace Modules\Endpoint\Services;

trait HelperVersionsTrait
{
    /**
     * keep the map data
     *
     * @var array
     */
    protected static $map;



    /**
     * get the raw map data
     *
     * @return array
     */
    public function getRawMap()
    {
        return static::$map;
    }



    /**
     * map all modules into an array
     *
     * @param int   $app_version
     * @param array $modules_versions
     */
    public function mapModulesVersion(int $app_version, array $modules_versions)
    {
        static::$map[$app_version] = $modules_versions;
    }



    /**
     * convert app version to the modular one
     *
     * @param int    $app_version
     * @param string $module_name
     *
     * @return int
     */
    public function getModularVersion(int $app_version, string $module_name)
    {
        if (!isset(static::$map[$app_version][$module_name])) {
            return 0;
        }

        return (int)static::$map[$app_version][$module_name];
    }



    /**
     * convert app version to the modular one (latest is used)
     *
     * @param int    $app_version
     * @param string $module_name
     *
     * @return int
     */
    public function getAppVersion(int $module_version, string $module_name)
    {
        $versions = $this->getRawMap();
        $result   = 0;

        foreach ($versions as $key => $version) {
            if (isset($version[$module_name]) and $version[$module_name] == $module_version) {
                $result = $key;
            }
        }

        return $result;
    }
}
