<?php

namespace Modules\Endpoint\Services;


interface EndpointInterface
{
    /**
     * get a human readable title for the Endpoint
     *
     * @return string
     */
    public static function getTitle(): string;



    /**
     * determine if the current user has access to this Endpoint
     *
     * @return bool
     */
    public function hasPermit(): bool;



    /**
     * get the valid http method of the Endpoint.
     *
     * @return string
     */
    public function getValidMethod(): string;



    /**
     * get the Endpoint namespace
     *
     * @return string
     */
    public function getControllerNamespace(): string;



    /**
     * get the Endpoint method, just like the Laravel's "Controller@method"
     *
     * @return string
     */
    public function getController(): string;



    /**
     * get a mock result, just the way the final controller would create
     *
     * @return mixed
     */
    public function getMockResult();
}
