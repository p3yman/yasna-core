<?php

return [
     "endpoint-401" => "Token has expired.",
     "endpoint-403" => "Forbidden!",
     "endpoint-404" => "Not Found!",
     "endpoint-405" => "Bad Method Call!",
     "endpoint-501" => "Not Implemented!",
     "endpoint-410" => "Gone!",
     "endpoint-600" => "Onion Termination!"
];
