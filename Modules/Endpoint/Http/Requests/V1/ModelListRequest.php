<?php

namespace Modules\Endpoint\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\RequestPaginationTrait;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class ModelListRequest extends YasnaFormRequest
{
    use RequestPaginationTrait;

    protected $should_load_model         = false;
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return ($this->model)::canList();
    }



    /**
     * get the validation rule for `model` attribute.
     *
     * @return array
     */
    public function modelRules()
    {
        $model = studly_case($this->getData("model"));
        if (!class_exists(MODELS_NAMESPACE . $model)) {
            return [
                 "model" => "required|invalid",
            ];
        }

        $this->model = model($model);

        return [];
    }



    /**
     * get the list validation rules.
     *
     * @return array
     */
    public function listRules()
    {
        return [
             "paginated" => "boolean",
             "per_page"  => "integer",
             "order"     => "in:desc,asc,rand",
             "including" => "required|array",
             "excluding" => "array",
             "count"     => "boolean",
        ];
    }
}
