<?php

namespace Modules\Endpoint\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class ModelTrashRequest extends YasnaFormRequest
{
    protected $automatic_injection_guard = false; // <~~ no mass assignment ahead!



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists and $this->model->canTrash();
    }



    /**
     * @inheritdoc
     */
    public function correctionsBeforeLoadingModel()
    {
        $model = studly_case($this->getData("model"));

        if (class_exists(MODELS_NAMESPACE . $model)) {
            $this->model_name = $model;
        }
    }
}
