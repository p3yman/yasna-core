<?php

namespace Modules\Endpoint\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class ModelSingleRequest extends YasnaFormRequest
{
    protected $automatic_injection_guard  = false; // <~~ no mass assignment ahead!
    protected $should_load_trashed_models = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->model->isTrashed()) {
            return $this->model->exists and $this->model->canView() and $this->model->canViewTrashed();
        }

        return $this->model->exists and $this->model->canView();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "including" => "required|array",
             "excluding" => "array",
        ];
    }



    /**
     * @inheritdoc
     */
    public function correctionsBeforeLoadingModel()
    {
        $model = studly_case($this->getData("model"));

        if (class_exists(MODELS_NAMESPACE . $model)) {
            $this->model_name = $model;
        }

        if ($this->getData("with_trashed")) {
            $this->should_load_trashed_models = true;
        }
    }
}
