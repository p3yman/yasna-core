<?php

namespace Modules\Endpoint\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

class LoginCheckEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Login Check";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Endpoint\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'GeneralServiceController@loginCheck';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "ok" => "0",
        ], [
             "given_token" => request()->token,
             "hashid"      => hashid(0),
             "username"    => 'folan',
        ]);
    }
}
