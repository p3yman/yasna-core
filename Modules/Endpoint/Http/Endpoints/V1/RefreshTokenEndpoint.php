<?php

namespace Modules\Endpoint\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/endpoint-refresh-token
 *                    Refresh Token
 * @apiDescription    Refresh a given token
 * @apiVersion        1.0.0
 * @apiName           Refresh Token
 * @apiGroup          Endpoint
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "new_token":
 *          "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95Y21zLm1wZ1wvYXBpXC92MVwvZW5kcG9pbnQtcmVmcmVzaC10b2tlbiIsImlhdCI6MTU0MzY3NTM3MiwiZXhwIjoxNTQ4NDc2MDMwLCJuYmYiOjE1NDQ4NzYwMzAsImp0aSI6ImJ6UzRNcjM3T0xBd2FYMlciLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.f8H7k_dLpI2hiDvo23CFFryoUetotBTc7pcxRKu6rvE",
 *          "hashid": "qKXVA",
 *          "username": "3040055437"
 *      },
 *          "results": {
 *          "ok": 1
 *      }
 * }
 * @apiErrorExample   Unauthenticated Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Endpoint\Http\Controllers\V1\GeneralServiceController controller()
 */
class RefreshTokenEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Refresh Token";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Endpoint\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'GeneralServiceController@refreshToken';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond(
             [
                  "ok" => 1,
             ],
             [
                  "new_token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95Y21zLm1wZ1wvYXBpXC92MVwvZW5kcG9pbnQtcmVmcmVzaC10b2tlbiIsImlhdCI6MTU0MzY3NTM3MiwiZXhwIjoxNTQ4NDc2MDMwLCJuYmYiOjE1NDQ4NzYwMzAsImp0aSI6ImJ6UzRNcjM3T0xBd2FYMlciLCJzdWIiOjEsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.f8H7k_dLpI2hiDvo23CFFryoUetotBTc7pcxRKu6rvE",
                  "hashid"    => "qKXVA",
                  "username"  => "5646545645",
             ]
        );
    }
}
