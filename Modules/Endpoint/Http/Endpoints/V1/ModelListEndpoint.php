<?php

namespace Modules\Endpoint\Http\Endpoints\V1;

use Modules\Endpoint\Http\Controllers\V1\CrudController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/endpoint-model-list
 *                    Model List
 * @apiDescription    get a list of the models with the requested resources
 * @apiVersion        1.0.0
 * @apiName           Model List
 * @apiGroup          Endpoint
 * @apiPermission     Anybody who can pass the `canList()` condition of the model.
 * @apiParam {string}  model        name of the model in question
 * @apiParam {array}   including    list of resources to be included in the response
 * @apiParam {array}   [excluding]  list of resources to be excluded from the response
 * @apiParam {boolean} [paginated]  determine if the list should be paginated
 * @apiParam {integer} [per_page]   determine the number of items per page, in paginated mode
 * @apiParam {integer} [page]       determine the page number, in paginated mode
 * @apiParam {string}  [order]      determine the sort type (asc/desc/rand)
 * @apiParam {string}  [sort]       determine the field of which the result is to be ordered against
 * @apiParam {boolean} [count]      determine if the only thing matters is the total number of the results
 * @apiParam {mixed}   [_elector_]  any number of electors are supported
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "model": "User",
 *          "total": 1
 *      },
 *      "results": {
 *          {
 *              "id": "hA5h1d",
 *              "folan": "jafar",
 *          }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class ModelListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Model List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // <~~ The real auth is checked in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Endpoint\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "id"    => hashid(rand(1, 100)),
                  "folan" => "jafar",
             ],
        ], [
             "model" => "User",
             "total" => "1",
        ]);
    }
}
