<?php

namespace Modules\Endpoint\Http\Endpoints\V1;

use Modules\Endpoint\Http\Controllers\V1\CrudController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/endpoint-model-destroy
 *                    Model Destroy
 * @apiDescription    permanently delete a soft-deleted model
 * @apiVersion        1.0.0
 * @apiName           Model Destroy
 * @apiGroup          Endpoint
 * @apiPermission     Anybody who can pass the `canDestroy()` condition of the model.
 * @apiParam {string} model  name of the model in question
 * @apiParam {string} id     hashid of the model in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class ModelDestroyEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Model Destroy";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // <~~ The real auth is checked in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Endpoint\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@destroy';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "id" => hashid(rand(1, 100)),
        ]);
    }
}
