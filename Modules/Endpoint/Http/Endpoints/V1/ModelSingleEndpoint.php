<?php

namespace Modules\Endpoint\Http\Endpoints\V1;

use Modules\Endpoint\Http\Controllers\V1\CrudController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/endpoint-model-single
 *                    Model Single
 * @apiDescription    fetch a model with the requested resources
 * @apiVersion        1.0.0
 * @apiName           Model Single
 * @apiGroup          Endpoint
 * @apiPermission     Anybody who can pass the `canView()` condition of the model.
 * @apiParam {string}  model          name of the model in question
 * @apiParam {string}  id             hashid of the model in question
 * @apiParam {array}   including      list of resources to be included in the response
 * @apiParam {array}   [excluding]    list of resources to be excluded from the response
 * @apiParam {boolean} [with_trashed] require the trashed model to be available to the user
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "model": "User"
 *      },
 *      "results": {
 *          "id": "hA5h1d",
 *          "folan": "jafar",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class ModelSingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Model Single";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // <~~ The real auth is checked in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Endpoint\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@single';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"    => hashid(rand(1, 100)),
             "folan" => "jafar",
        ], [
             "model" => "User",
        ]);
    }
}
