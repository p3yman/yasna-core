<?php

namespace Modules\Endpoint\Http\Endpoints\V1;

use Modules\Endpoint\Http\Controllers\V1\ListController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/endpoint-list
 *                    Endpoint List
 * @apiDescription    lists the available endpoints
 * @apiVersion        1.0.0
 * @apiName           Endpoint List
 * @apiGroup          Endpoint
 * @apiPermission     guest (only in Debug Mode)
 * @apiParam {String} [module_name] The name which must be searched.
 * @apiParam {int}    [version_number] The hashid of the city to be filtered against.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "module_name": "folan",
 *          "version_number": "2"
 *      },
 *      "results": {
 *          "endpoint-list-v1": {
 *          "default": false,
 *          "key": "endpoint-list-v1",
 *          "order": 1,
 *          "permit": null,
 *          "condition": true,
 *          "comment": null,
 *          "caption": "List Endpoints",
 *          "version": 1,
 *          "module": "Endpoint",
 *          "class": "Modules\\Endpoint\\Http\\Endpoints\\V1\\ListEndpoint"
 *          },
 *          "endpoint-login-check-v1": {
 *          "default": false,
 *          "key": "endpoint-login-check-v1",
 *          "order": 2,
 *          "permit": null,
 *          "condition": true,
 *          "comment": null,
 *          "caption": "Login Check",
 *          "version": 1,
 *          "module": "Endpoint",
 *          "class": "Modules\\Endpoint\\Http\\Endpoints\\V1\\LoginCheckEndpoint"
 * },
 *      }
 * }
 * @apiErrorExample   Unauthenticated-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  ListController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List Endpoints";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return debugMode();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Endpoint\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ListController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "registry-get-user-profile-fields-v1" => [
                  "default"   => false,
                  "key"       => "registry-get-user-profile-fields-v1",
                  "order"     => 30,
                  "permit"    => null,
                  "condition" => true,
                  "comment"   => null,
                  "caption"   => "Get User Profile Fields",
                  "version"   => 1,
                  "module"    => "Registry",
                  "class"     => 'Modules\Registry\Http\Endpoints\V1\GetUserProfileFieldsEndpoint',
             ],
             "registry-user-validation-rules-v1"   => [
                  "default"   => false,
                  "key"       => "registry-user-validation-rules-v1",
                  "order"     => 31,
                  "permit"    => null,
                  "condition" => true,
                  "comment"   => null,
                  "caption"   => "User Validation Rules",
                  "version"   => 1,
                  "module"    => "Registry",
                  "class"     => 'Modules\Registry\Http\Endpoints\V1\UserValidationRulesEndpoint',
             ],
        ], [
             "version_number" => request()->version_number,
             "module_name"    => request()->module_name,
        ]);
    }
}
