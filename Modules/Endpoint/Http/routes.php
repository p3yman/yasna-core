<?php

Route::group([
     'middleware' => ['api', 'endpointToken', 'endpointLocale'],
     'prefix'     => '',
     'namespace'  => 'Modules\Endpoint\Http\Controllers',
], function () {

    Route::any("api/modular/{version}/{endpoint}/ping", "EndpointController@modulePing")->name("endpoint-modular-ping");
    Route::any("api/{version}/{endpoint}/ping", "EndpointController@appPing")->name("endpoint-app-ping");
    Route::any("api/modular/{version}/{endpoint}", "EndpointController@modularResolve")->name("endpoint-modular");
    Route::any("api/{version}/{endpoint}", "EndpointController@appResolve")->name('endpoint-app');

});
