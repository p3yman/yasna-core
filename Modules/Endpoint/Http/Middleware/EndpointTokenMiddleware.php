<?php

namespace Modules\Endpoint\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class EndpointTokenMiddleware extends BaseMiddleware
{
    const JWT_TOKEN_EXPIRED_MESSAGE = "Token has expired";

    /** @var JWTException */
    private $exception = null;



    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->authenticate($request);

        if (!user()->exists and $this->exception) {
            if ($this->exception->getMessage() == static::JWT_TOKEN_EXPIRED_MESSAGE) {
                request()->error = 401;
            }
        }

        return $next($request);
    }



    /**
     * @inheritdoc
     */
    public function authenticate(Request $request)
    {
        $this->checkForToken($request);

        try {
            $this->auth->parseToken()->authenticate();
        } catch (JWTException $e) {
            $this->exception = $e;
        }

    }



    /**
     * @inheritdoc
     */
    public function checkForToken(Request $request)
    {
        $this->auth->parser()->setRequest($request)->hasToken();
    }

}
