<?php

namespace Modules\Endpoint\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MockLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (!debugMode() or !request()->__user) {
            return $next($request);
        }

        $user = user(request()->__user);
        if ($user->exists) {
            login($user->id);
            return $next($request);
        }

        Auth::guard()->logout();
        return $next($request);
    }
}
