<?php

namespace Modules\Endpoint\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EndpointLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $locales    = (array) get_setting('site_locales');
        $get_locale = $request->header('locale');

        if (in_array($get_locale, $locales)) {
            app()->setLocale($get_locale);
        }

        return $next($request);
    }
}
