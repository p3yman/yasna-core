<?php

namespace Modules\Endpoint\Http\Controllers;

use Modules\Yasna\Services\YasnaApiController;


class EndpointController extends YasnaApiController
{
    /**
     * resolve the http endpoint request within the modules
     *
     * @param string $version
     * @param string $endpoint
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function modularResolve(string $version_string, string $endpoint)
    {
        $version = $this->getRequestedVersion($version_string);
        $class   = endpoint()->getEndpointInstance($endpoint, $version);

        if (!$class or $class->cannotProceed()) {
            return $this->responseError();
        }

        if (request()->mock) {
            return $class->getMockResult();
        }

        return $class->proceed();
    }



    /**
     * resolve the http endpoint request within the app
     *
     * @param string $version
     * @param string $endpoint
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function appResolve(string $version_string, string $endpoint)
    {
        $app_version    = $this->getRequestedVersion($version_string);
        $module_name    = $this->discoverModuleName($endpoint);
        $module_version = endpoint()->getModularVersion($app_version, $module_name);
        $class          = endpoint()->getEndpointInstance($endpoint, $module_version);

        if (!$class or $class->cannotProceed()) {
            return $this->responseError();
        }

        if (request()->mock) {
            return $class->getMockResult();
        }

        return $class->proceed();
    }



    /**
     * process the modular ping request
     *
     * @param string $version_string
     * @param string $endpoint
     *
     * @return array
     */
    public function modulePing(string $version_string, string $endpoint)
    {
        $module_version = $this->getRequestedVersion($version_string);
        $module_name    = $this->discoverModuleName($endpoint);
        $app_version    = endpoint()->getAppVersion($module_version, $module_name);
        $class          = endpoint()->getEndpointInstance($endpoint, $module_version);

        if (!$class) {
            return $this->returnPingNotFound($module_name, $endpoint, $module_version);
        }

        $method = $class->getValidMethod();

        if ($class->cannotProceed()) {
            return $this->returnPingNotAvailable($module_name, $endpoint, $module_version, $app_version, $method);
        }

        return $this->returnPingSuccess($module_name, $endpoint, $module_version, $app_version, $method);
    }



    /**
     * process the app ping request
     *
     * @param string $version_string
     * @param string $endpoint
     *
     * @return array
     */
    public function appPing(string $version_string, string $endpoint)
    {
        $app_version    = $this->getRequestedVersion($version_string);
        $module_name    = $this->discoverModuleName($endpoint);
        $module_version = endpoint()->getModularVersion($app_version, $module_name);
        $class          = endpoint()->getEndpointInstance($endpoint, $module_version);

        if (!$class) {
            return $this->returnPingNotFound($module_name, $endpoint, $app_version);
        }

        $method = $class->getValidMethod();

        if ($class->cannotProceed()) {
            return $this->returnPingNotAvailable($module_name, $endpoint, $module_version, $app_version, $method);
        }

        return $this->returnPingSuccess($module_name, $endpoint, $module_version, $app_version, $method);
    }



    /**
     * return a ping success result
     *
     * @param string $module_name
     * @param string $endpoint_name
     * @param int    $version_number
     * @param int    $app_version_number
     * @param string $method
     *
     * @return array
     */
    protected function returnPingSuccess($module_name, $endpoint_name, $version_number, $app_version_number, $method)
    {
        return $this->returnPing(true, [
             "module_name"        => $module_name,
             "endpoint_name"      => $endpoint_name,
             "version_number"     => $version_number,
             "app_version_number" => $app_version_number,
             "method"             => $method,
        ]);
    }



    /**
     * return a ping not-found result
     *
     * @param string $module_name
     * @param string $endpoint_name
     * @param int    $version_number
     *
     * @return array
     */
    protected function returnPingNotFound($module_name, $endpoint_name, $version_number)
    {
        return $this->returnPing(false, [
             "module_name"       => $module_name,
             "endpoint_name"     => $endpoint_name,
             "requested_version" => $version_number,
             "method"            => 'UNKNOWN',
        ]);
    }



    /**
     * return a ping not-allowed result
     *
     * @param string $module_name
     * @param string $endpoint_name
     * @param int    $version_number
     * @param int    $app_version_number
     * @param string $method
     *
     * @return array
     */
    protected function returnPingNotAvailable(
         $module_name,
         $endpoint_name,
         $version_number,
         $app_version_number,
         $method
    ) {
        return $this->returnPing(false, [
             "module_name"        => $module_name,
             "endpoint_name"      => $endpoint_name,
             "version_number"     => $version_number,
             "app_version_number" => $app_version_number,
             "method"             => $method,
        ]);

    }



    /**
     * return ping result
     *
     * @param bool  $ok
     * @param array $metadata
     *
     * @return array
     */
    protected function returnPing(bool $ok, array $metadata)
    {
        $result = [
             "ok" => (int)$ok,
        ];

        if (!debugMode()) {
            $metadata = [];
        }

        return $this->success($result, $metadata);
    }



    /**
     * get a standard WhiteHouse error
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function responseError()
    {
        $code = request()->error? request()->error : 404;

        if($code == 401) {
            return api()->clientErrorRespond("endpoint-401");
        }

        if (!debugMode()) {
            return api()->clientErrorRespond("endpoint-404");
        }

        return api()->clientErrorRespond("endpoint-$code");
    }



    /**
     * get the requested version number
     *
     * @param string $string
     *
     * @return int
     */
    protected function getRequestedVersion(string $string): int
    {
        $string = strtolower($string);

        //if (!preg_match("/v([1-9])/", $string)) {
        //    return 0;
        //}

        return (int)str_after($string, "v");
    }



    /**
     * discover the module name out of the endpoint string
     *
     * @param string $endpoint
     *
     * @return string
     */
    protected function discoverModuleName(string $endpoint)
    {
        $array = explode("-", $endpoint);

        $module_name = $array[0];

        if (module($module_name)->isValid()) {
            return $array[0];
        }

        return studly_case("$array[0]-$array[1]");
    }
}
