<?php

namespace Modules\Endpoint\Http\Controllers\V1;


use Modules\Yasna\Services\YasnaApiController;
use Tymon\JWTAuth\Facades\JWTAuth;

class GeneralServiceController extends YasnaApiController
{
    /**
     * check whether a user is logged in and responds as appropriate
     *
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function loginCheck()
    {
        if (user()->id == 0) {
            return $this->clientError("endpoint-403");
        }

        $result = [
             "ok" => user()->id,
        ];

        $metadata = [
             "given_token" => request()->token,
             "hashid"      => user()->id ? user()->hashid : null,
             "username"    => user()->username,
        ];

        return $this->success($result, $metadata);
    }



    /**
     * Refresh a user token
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshToken()
    {
        $user = user();
        if (!$user->exists) {
            return $this->clientError("endpoint-403");
        }

        $token = JWTAuth::parseToken()->refresh(true);

        $result = [
             "ok" => user()->id,
        ];

        $metadata = [
             "new_token" => $token,
             "hashid"    => user()->hashid,
             "username"  => user()->username,
        ];

        return $this->success($result, $metadata);
    }
}
