<?php

namespace Modules\Endpoint\Http\Controllers\V1;

use Modules\Yasna\Services\YasnaApiController;

class ListController extends YasnaApiController
{
    /**
     * get the list of registered endpoints
     *
     * @return array
     */
    public function list()
    {
        $result = endpoint()->list(request()->module_name, request()->version_number);

        return $this->success($result, [
             "module_name"    => request()->module_name,
             "version_number" => request()->version_number,
        ]);
    }


}
