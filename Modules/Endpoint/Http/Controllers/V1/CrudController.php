<?php

namespace Modules\Endpoint\Http\Controllers\V1;

use Modules\Endpoint\Http\Requests\V1\ModelDestroyRequest;
use Modules\Endpoint\Http\Requests\V1\ModelSingleRequest;
use Modules\Endpoint\Http\Requests\V1\ModelListRequest;
use Modules\Endpoint\Http\Requests\V1\ModelRestoreRequest;
use Modules\Endpoint\Http\Requests\V1\ModelTrashRequest;
use Modules\Yasna\Services\YasnaApiController;

class CrudController extends YasnaApiController
{
    /**
     * fetch the model with the requested resources.
     *
     * @param ModelSingleRequest $request
     *
     * @return array
     */
    public function single(ModelSingleRequest $request)
    {
        if ($request->including == ["*"]) {
            $request->including = null;
        }

        $result = $request->model->toResource($request->including, $request->excluding);
        $meta   = [
             "model" => $request->model->getClassName(),
        ];

        return $this->success($result, $meta);
    }



    /**
     * get the list-view of the requested model.
     *
     * @param ModelListRequest $request
     *
     * @return array
     */
    public function list(ModelListRequest $request)
    {
        if ($request->including == ["*"]) {
            $request->including = null;
        }

        $electors = array_merge($request->toArray(), ($request->model)::dominantElectors());
        $builder  = $request->model->elector($electors);

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * perform soft-delete action on the given model.
     *
     * @param ModelTrashRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function trash(ModelTrashRequest $request)
    {
        $done = $request->model->delete();

        return $this->typicalSaveFeedback($done, [
             "id" => $request->model->hashid,
        ]);
    }



    /**
     * perform restore action on the given soft-deleted model.
     *
     * @param ModelRestoreRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restore(ModelRestoreRequest $request)
    {
        $done = $request->model->undelete();

        return $this->typicalSaveFeedback($done, [
             "id" => $request->model->hashid,
        ]);
    }



    /**
     * perform permanent delete action on the given soft-deleted model.
     *
     * @param ModelDestroyRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(ModelDestroyRequest $request)
    {
        $done = $request->model->hardDelete();

        return $this->typicalSaveFeedback($done, [
             "id" => $request->model->hashid,
        ]);
    }
}
