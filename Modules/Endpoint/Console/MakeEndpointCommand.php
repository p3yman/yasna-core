<?php

namespace Modules\Endpoint\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MakeEndpointCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-endpoint';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make an endpoint class.';

    /**
     * keep the value of the desired version
     *
     * @var int
     */
    protected $version;

    /**
     * keep the human-readable title
     *
     * @var string
     */
    protected $title;

    /**
     * keep the http method name
     *
     * @var string
     */
    protected $method;
    


    /**
     * Execute the console command.
     */
    public function customHandle()
    {
        $this->askVersion();
        $this->askTitle();
        $this->askMethod();
    }



    /**
     * ask the HTTP method
     */
    protected function askMethod()
    {
        $allowed_methods = ["GET", "PUT", "POST", "PATCH", "DELETE", "OPTIONS"];

        $this->method = $this->choice("HTTP Method?", $allowed_methods, 0);
    }



    /**
     * ask a title via a dialogue
     */
    protected function askTitle()
    {
        $suggest = $this->discoverSuitableTitle();
        $title   = $this->ask("An English Human-Readable Title (Probably '$suggest')?");

        if (!$title) {
            $title = $suggest;
        }

        $this->title = $title;
    }



    /**
     * discover a suitable title
     *
     * @return string
     */
    protected function discoverSuitableTitle()
    {
        $kebab = kebab_case(str_before($this->class_name, "Endpoint"));

        return title_case(str_replace("-", " ", $kebab));
    }



    /**
     * Ask the requested version via a dialogue
     */
    protected function askVersion()
    {
        $suggest = $this->discoverCurrentVersion();
        $version = $this->ask("Desired Version (Probably $suggest)?");

        if (is_null($version)) {
            $version = $suggest;
        }

        while (!is_numeric($version) or $version < 1) {
            $version = $this->ask("Something greater than 1 please.");
            if (is_null($version)) {
                $version = $suggest;
            }
        }

        $this->version = $version;
    }



    /**
     * discover a possible version based on the file system
     *
     * @return int
     */
    protected function discoverCurrentVersion(): int
    {
        $parent_path = $this->getFolderPath();
        if (!file_exists(($parent_path))) {
            return 1;
        }

        $subs = scandir($parent_path);
        $max  = 0;
        foreach ($subs as $sub) {
            $version = (int)str_after($sub, "V");
            $max     = max($version, $max);
        }

        return $max;
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE"        => $this->module_name,
             "CLASS"         => $this->class_name,
             "VERSION"       => "V" . $this->version,
             "VERSION_LOWER" => "v" . $this->version,
             "TITLE"         => $this->title,
             "ENDPOINT"      => str_replace("-endpoint", null, kebab_case($this->module_name . $this->class_name)),
             "VERSION_NO"    => $this->version,
             "METHOD"        => $this->method,
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        $parent_path = $this->module->getPath("Http" . DIRECTORY_SEPARATOR . "Endpoints");
        if ($this->version != null) {
            return $parent_path . DIRECTORY_SEPARATOR . "V" . $this->version;
        }
        return $parent_path;
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Endpoint')) {
            return $file_name . "Endpoint";
        }
        return $file_name;
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "endpoint.stub";
    }
}
