<?php

namespace Modules\Yasnateam\Notifications;

use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class TicketReplySendSmsNotification extends YasnaNotificationAbstract
{
    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * Send messages to support team members when the ticket is answered
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        return trans_safe('yasnateam::notification.reply_ticket');
    }

}
