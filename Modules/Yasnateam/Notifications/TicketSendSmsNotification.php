<?php

namespace Modules\Yasnateam\Notifications;

use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Morilog\Jalali\Facades\jDate;

class TicketSendSmsNotification extends YasnaNotificationAbstract
{

    protected $ticket;



    /**
     * TicketSendSmsNotification constructor.
     *
     * @param $ticket
     */
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * Send messages to support team members when the ticket is sent
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $data = $date = pd(jDate::forge($this->ticket->model->created_at)->format('Y/m/j - H:i'));

        return trans_safe('yasnateam::notification.send_ticket',
             ['user' => $this->ticket->model->user->full_name, 'date' => $data],
             $notifiable->lang);
    }

}
