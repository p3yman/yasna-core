<?php

namespace Modules\Yasnateam\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Remark\Events\RemarkAdded;
use Modules\Yasnateam\Notifications\TicketSendSmsNotification;

class SendSmsWhenTicketAdd implements ShouldQueue
{
    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param RemarkAdded $event
     */
    public function handle(RemarkAdded $event)
    {
        $support_roles = role()::supportRoles()->pluck('slug')->toArray();
        $users         = user()->elector(['role' => $support_roles])->get();
        foreach ($users as $user) {
            $user->notify(new TicketSendSmsNotification($event));
        }
    }

}
