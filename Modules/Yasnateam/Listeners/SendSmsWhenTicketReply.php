<?php

namespace Modules\Yasnateam\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Remark\Events\RemarkReplied;
use Modules\Yasnateam\Notifications\TicketReplySendSmsNotification;

class SendSmsWhenTicketReply implements ShouldQueue
{
    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param RemarkReplied $event
     */
    public function handle(RemarkReplied $event)
    {
        $user = user($event->model->user->id);
        $user->notify(new TicketReplySendSmsNotification());

    }
}
