<?php

return [
     'name' => 'Yasnateam',

     'default-locales' => [
          'fa',
          'en',
     ],

     "group-icon" => [
          "star"            => "star",
          "crime"           => "times",
          "delay-warning"   => "warning",
          "general-warning" => "warning",
          "award"           => "trophy",
     ],

     'group-color' => [
          "star"            => "green",
          "crime"           => "danger",
          "delay-warning"   => "orange",
          "general-warning" => "yellow",
          "award"           => "pink",
     ],
];
