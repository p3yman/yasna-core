<?php

/*
|--------------------------------------------------------------------------
| Front Side
|--------------------------------------------------------------------------
|
*/

Route::group([
     'middleware' => ['web'],
     'namespace'  => 'Modules\Yasnateam\Http\Controllers',
], function () {

	// Home
	Route::get('/', 'YasnateamNewFrontController@index')->name('home');

	// Yasna Web 3 Landing
	Route::get('yasna-web', 'YasnateamNewFrontController@yasnaweb')->name('yasnaweb');

	// Page
	Route::get('page/{slug}', 'YasnateamNewFrontController@page')->name('page');

	// Contact page
	Route::get('contact-us', 'YasnateamNewFrontController@contact')->name('contact');

	// FAQs
	Route::get('faq', 'YasnateamNewFrontController@faq')->name('faq');

	// Services
	Route::get('services/{service}', 'YasnateamNewFrontController@services')->name('services');

	// Blog
	Route::get('blog', 'YasnateamNewFrontController@blog')->name('blog');
	Route::get('blog/tag/{slug?}', 'YasnateamNewFrontController@blogTag')->name('blog-tag');
	Route::get('blog/{category}', 'YasnateamNewFrontController@blogCategory')->name('blog-category');
	Route::get('blog/{category}/{id}/{slug?}', 'YasnateamNewFrontController@blogSingle')->name('blog-single');

	// Jobs
	Route::get('jobs', 'YasnateamNewFrontController@jobs')->name('jobs');
	Route::get('jobs/apply', 'YasnateamNewFrontController@jobsApplyForm')->name('jobs-apply-form');
	Route::post('jobs/apply/send', 'YasnateamNewFrontController@sendJobsApplyForm')->name('send-jobs-apply-form');
	Route::get('jobs/{id}', 'YasnateamNewFrontController@jobsSingle')->name('jobs-single');

});


/*
|--------------------------------------------------------------------------
| Rewards
|--------------------------------------------------------------------------
|
*/
Route::group([
     "middleware" => "web",
     "namespace"  => module('yasnateam')->getControllersNamespace(),
     "prefix"     => "manage/rewards/downstream",
], function () {
    Route::get('/update/{item_id}', 'RewardsDownstreamController@update');
    Route::get('/create', 'RewardsDownstreamController@create');
    Route::get('/edit/{hashid}', 'RewardsDownstreamController@edit');

    Route::post('/', 'RewardsDownstreamController@save')->name("downstream-rewards-save");
});

Route::group([
     "middleware" => "web",
     "namespace"  => 'Modules\Yasnateam\Http\Controllers',
     "prefix"     => "manage/",
], function () {
    Route::get('questionnaire', 'QuestionnaireController@grid');
    Route::get('questionnaire/key-pair/{hash_id}', 'QuestionnaireController@showKeyPair');
});

Route::group([
     "middleware" => "web",
     "namespace"  => module('yasnateam')->getControllersNamespace(),
     "prefix"     => "manage/rewards",
], function () {
    Route::get('/update/{item_id}', 'RewardsBrowseController@update');
    Route::get('/act/{model_id}/{action}/{option?}', 'RewardsActionController@singleAction');

    //Route::get('/browse/user/{user_hashid}/', 'RewardsBrowseController@index');
    //Route::get('/browse/reason/{reason_hashid}/', 'RewardsBrowseController@index');
    //Route::get('/browse/group/{group_slug}/', 'RewardsBrowseController@index');
    Route::get('/browse', 'RewardsBrowseController@index');
    Route::get('/', 'RewardsBrowseController@index');

    Route::get('/create', 'RewardsActionController@create');
    Route::get('/edit/{hashid}', 'RewardsActionController@editor');

    Route::group([
         "prefix" => "/save",
    ], function () {
        Route::post('/', 'RewardsActionController@save')->name("rewards-save");
        Route::post('/delete', 'RewardsActionController@delete')->name("rewards-delete");
        Route::post('/forgive', 'RewardsActionController@forgive')->name("rewards-forgive");
    });
});


Route::group([
     "middleware" => "web",
     "namespace"  => module('yasnateam')->getControllersNamespace(),
     "prefix"     => "manage/resumes",
], function () {
    Route::get('/', 'ResumeBrowseController@index');
    Route::get('/{hashid}/show', 'ResumeBrowseController@show');
    Route::get('/{hashid}/delete_confirm', 'ResumeBrowseController@deleteConfirm');
    Route::post('/{hashid}/delete', 'ResumeBrowseController@delete')->name('delete-resume');
    Route::get('/{hashid}/download', 'ResumeBrowseController@downloadResume');
});
