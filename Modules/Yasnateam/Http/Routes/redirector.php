<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Yasnateam')->getControllersNamespace(),
     'prefix'     => '',
], function () {
    Route::get('vue' , 'RedirectController@vueWorkshop');
    Route::get('scrum' , 'RedirectController@scrumWorkshop');
});
