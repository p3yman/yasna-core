<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Yasnateam')->getControllersNamespace(),
     'prefix'     => 'yasnateam',
], function () {
    Route::post('submit-contact-form', 'FrontContactFormController@submit')->name("yasnateam-submit-contact-form");
});
