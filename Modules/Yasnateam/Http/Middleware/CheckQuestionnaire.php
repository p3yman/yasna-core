<?php

namespace Modules\Yasnateam\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckQuestionnaire
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $fields = collect($request->get('form_data'))->pluck('name');

        $array_of_invalid_fields = array_diff($fields->toArray(), $this->availableFields());

        if (count($array_of_invalid_fields)) {
            die(trans('yasnateam::validation.change_field'));
        }
        return $next($request);
    }



    /**
     * list of available fields of questionnaire form
     *
     * @return array of questionnaire fields
     */
    private function availableFields()
    {
        return [
             'company_name',
             'foundation_year',
             'email',
             'phone_number',
             'address',
             'site_url',
             'organization_intro',
             'superiority',
             'has_color',
             'has_logo',
             'customers_gender',
             'customer_age_from',
             'customer_age_to',
             'customer_edu_from',
             'customer_edu_to',
             'nationality',
             'language',
             'content_time',
             'competitors_sites',
             'competitors_sites_features',
             'competitors_sites_disadvantages',
             'competitors_has-site',
             'your_site_advantages',
             'requirement_reasons',
             'comprehend_features',
             'slogan',
             'budget',
             'example',
             'mobile_app',
             'email_service',
             'keywords',
             'red_line',

             'social_media-no',
             'social_media_facebook',
             'social_media_instagram',
             'social_media_twitter',
             'social_media_linkedin',
             'social_media_others',
             'social_media_management',

             'site_locale_fa',
             'site_locale_en',
             'site_locale_ar',
             'site_locale_ar',
             'site_locale_tr',
             'site_locale_az',
             'site_locale_fr',
             'site_locale_es',
             'site_locale_es',
             'site_locale_it',
             'site_locale_zh',
             'site_locale_ur',
             'site_locale_ps',
             'site_locale_others',

             'general_options_nested_menu',
             'general_options_chat',
             'general_options_search',
             'general_options_donation',
             'general_options_ads',
             'general_options_sub_domain',
             'general_options_survey',
             'general_options_survey',
             'general_options_slide_show',
             'general_options_statistics',
             'general_options_users_observation',
             'general_options_video_streaming',
             'general_options_users_registration',
             'general_options_others',

             'static_pages_contact_us',
             'static_pages_about_us',
             'static_pages_google_maps',
             'static_pages_manage_chart',
             'static_pages_manager_message',
             'static_pages_manager_message',
             'static_pages_error',
             'static_pages_others',

             "dynamic_pages_news",
             "dynamic_pages_events",
             " dynamic_pages_expo",
             " dynamic_pages_expo",
             "dynamic_pages_blog",
             "dynamic_pages_photo_gallery",
             "dynamic_pages_video_gallery",
             "dynamic_pages_video_gallery",
             "dynamic_pages_categories",
             "dynamic_pages_rss",
             "dynamic_pages_rss",
             "dynamic_pages_auto_publish",
             " dynamic_pages_auto_publish",
             "dynamic_pages_tags",
             "dynamic_pages_share_to_social_networks",
             "dynamic_pages_share_to_social_networks",
             "dynamic_pages_commenting",
             "dynamic_pages_commenting",
             "dynamic_pages_others",
             "dynamic_pages_expo",

             'shop_online_payment',
             'shop_offline_payment',
             'shop_group_discount',
             'shop_product_discount',
             'shop_package_management',
             'shop_inventory_management',
             'shop_order_tracking',
             'shop_prices_archives',
             'shop_discount_archives',
             'shop_currency_sale',
             'shop_credit_sale',
             'shop_installment_sale',
             'shop_unavailable_product_request',
             'shop_customers_club',
             'shop_sales_report',
             'shop_shop_accounting',
             'shop_physical_shop_integrity',
             'shop_physical_shop_integrity',
             'shop_others',

             'manage_dashboard_two_step_verification',
             'manage_dashboard_access_levels',
             'manage_dashboard_users_activity_report',
             'manage_dashboard_sending_sms_email',
             'manage_dashboard_sending_sms_email',
             'manage_dashboard_menu_maker',
             'manage_dashboard_manage_user_roles',
        ];
    }
}
