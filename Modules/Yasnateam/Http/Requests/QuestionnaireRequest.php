<?php

namespace Modules\Yasnateam\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class QuestionnaireRequest extends YasnaRequest
{
    protected $model_name = "";


    /**
     * @return array
     */
    public function rules()
    {
        return [
             'name_first' => 'required|string',
             'name_last'  => 'required|string',
             'email'      => 'required|email',
             'mobile'     => 'required|phone',
        ];
    }
}
