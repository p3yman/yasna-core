<?php

namespace Modules\Yasnateam\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;


class JobsFormRequest extends YasnaRequest
{
    protected $model_name = "";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name'             => 'required|persian:60',
             'gender'           => 'required',
             'marriage'         => 'required',
             'bd'               => 'required|numeric|min:1350|max:1450',
             'address'          => 'required|persian:60',
             'tel'              => 'required',
             'mobile'           => 'required|phone:mobile',
             'email'            => 'required|email',
             'edu_degree'       => 'required',
             'edu_field'        => 'required',
             'english.reading'  => 'required',
             'english.writing'  => 'required',
             'english.speaking' => 'required',
             'salary'           => 'required',
             'available_date'   => 'required',
             'resume'           => 'required|file|mimes:pdf,docx,doc',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'bd'               => trans('yasnateam::job.bd'),
             'edu_degree'       => trans('yasnateam::job.edu_degree'),
             'english.reading'  => trans('yasnateam::job.english_reading'),
             'english.writing'  => trans('yasnateam::job.english_writing'),
             'english.speaking' => trans('yasnateam::job.english_speaking'),
             'salary'           => trans('yasnateam::job.salary'),
             'available_date'   => trans('yasnateam::job.available_date'),
             'resume'           => trans('yasnateam::job.name_resume'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name'     => 'pd',
             'gender'   => 'ed',
             'marriage' => 'ed',
             'address'  => 'pd',
             'email'    => 'ed',
             'bd'       => 'ed',
             'mobile'   => 'ed',
             'tel'      => 'ed',
        ];
    }
}
