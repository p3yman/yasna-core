<?php

namespace Modules\Yasnateam\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RewardsDeleteRequest extends YasnaRequest
{
    protected $model_name = "Reward";



    /**
     * @return mixed
     */
    public function authorize()
    {
        return $this->model->canDelete();
    }
}
