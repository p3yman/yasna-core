<?php

namespace Modules\Yasnateam\Http\Requests;

use App\Models\Reward;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class RewardsForgiveRequest
 *
 * @package Modules\Yasnateam\Http\Requests
 * @property Reward $model
 */
class RewardsForgiveRequest extends YasnaRequest
{
    protected $model_name = "Reward";



    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->model->canForgive();
    }
}
