<?php

namespace Modules\Yasnateam\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class FrontContactRequest extends YasnaRequest
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "name"    => "required",
             "email"   => "required|email",
             "message" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "phone" => "ed",
        ];
    }
}
