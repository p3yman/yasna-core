<?php

namespace Modules\Yasnateam\Http\Requests;

use App\Models\Reward;
use App\Models\RewardReason;
use Carbon\Carbon;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class RewardsSaveRequest
 *
 * @package Modules\Yasnateam\Http\Requests
 * @property RewardReason $reason_model
 * @property Reward       $model
 */
class RewardsSaveRequest extends YasnaRequest
{
    protected $model_name = "Reward";
    private $reason_model;



    /**
     * @return bool
     */
    public function authorize()
    {
        if ($this->model->exists) {
            return $this->model->canEdit();
        }
        
        return $this->model->canCreate();
    }



    /**
     * @return array
     */
    public function mainRules()
    {
        return [
             "effected_at" => "required",
             "user_id"     => "required",
             "reason_id"   => "required",
        ];
    }



    /**
     * @return array
     */
    public function weightRules()
    {
        if ($this->reason_model->isStar() or $this->reason_model->isAward()) {
            return [
                 "weight" => "required|numeric|min:1",
            ];
        }

        $this->data['weight'] = 1;
        return [];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             "weight" => "ed",
        ];
    }



    /**
     * Corrections
     */
    public function corrections()
    {
        $this->correctEffectedTimestamp();
        $this->loadReasonModel();
    }



    /**
     * Loads Reason Model
     */
    private function loadReasonModel()
    {
        $this->reason_model = model('reward-reason', $this->data['reason_id']);

        if ($this->reason_model->not_exists) {
            $this->data['reason_id'] = null; //<~~ This will cause error in rules method
        }

        $this->data['group'] = $this->reason_model->group;
    }



    /**
     * Corrects `effected_at`
     */
    private function correctEffectedTimestamp()
    {
        $timestamp = $this->data['effected_at'] / 1000;
        $carbon    = Carbon::createFromTimestamp($timestamp);

        $this->data['effected_at'] = $carbon->toDateTimeString();
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             "user_id.required"   => trans_safe("yasnateam::rewards.name-required"),
             "reason_id.required" => trans_safe("yasnateam::rewards.subject-required"),
        ];
    }
}
