<?php

namespace Modules\Yasnateam\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RewardsDownstreamSaveRequest extends YasnaRequest
{
    protected $model_name = "reward-reason";



    /**
     * @return bool
     */
    public function authorize()
    {
        return user()->isSuperadmin();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
             "group" => "required",
             "text"  => "required",
        ];
    }
}
