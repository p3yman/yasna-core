<?php

namespace Modules\Yasnateam\Http\Controllers;

use App\Models\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Modules\Yasna\Services\YasnaController;
use Modules\Yasnateam\Http\Requests\QuestionnaireRequest;
use Illuminate\Support\Collection;

class QuestionnaireController extends YasnaController
{
    protected $base_model  = "Questionnaire";
    protected $view_folder = "";
    private $model_obj;
    private $request;



    /**
     * action of first form of questionnaire
     *
     * @param QuestionnaireRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function inquire(QuestionnaireRequest $request)
    {
        $quesion         = new Questionnaire();
        $form_id         = uniqid();
        $quesion->name   = $request->get('name_first');
        $quesion->family = $request->get('name_last');
        $quesion->email  = $request->get('email');
        $quesion->mobile = $request->get('mobile');
        if ($request->has('domain')) {
            $quesion->domain = $request->get('domain');
        }
        $quesion->form_id = $form_id;
        $quesion->save();
        $request->session()->put('form_id', $form_id);
        return redirect()->route('survey', ['locale' => App::getLocale()]);
    }



    /**
     * action of questionnaire ajax
     *
     * @param Request $request
     */
    public function survey(Request $request)
    {
        $this->request   = $request;
        $form_data       = $this->request->get('form_data');
        $this->model_obj = model('QuestionnaireKeyPair');
        $this->checkFormId();
        foreach ($form_data as $datum) {
            if (isset($datum['value'])) {
                $this->saveField($datum['name'], $datum['value']);
            }
        }
    }



    /**
     * if form_id is empty this method fill it and set session and set empty row in questionnaire table
     */
    private function checkFormId()
    {
        $form_id = $this->request->get('form_id');
        if (empty($form_id)) {
            $form_id                = uniqid();
            $this->request->form_id = $form_id;
            $this->request->session()->put('form_id', $form_id);
            $tbl          = model('questionnaire');
            $tbl->form_id = $form_id;
            $tbl->save();
        }
    }



    /**
     * save fields key pair
     *
     * @param $name
     * @param $value
     */
    private function saveField($name, $value)
    {
        $form_id = $this->request->form_id;
        $check   = $this->model_obj->where('form_id', $form_id)->where('name', $name);
        if ($check->exists()) {
            if ($check->first()->value != $value) {
                $check->update([
                     'value' => $value,
                ]);
            }
        } else {
            $this->model_obj->batchSave([
                 'form_id' => $form_id,
                 'name'    => $name,
                 'value'   => $value,
            ]);
        }
    }



    /**
     * show list of persons who send questionnaire form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function grid()
    {
        $view_data['models'] = model('questionnaire')->all();
        return view('yasnateam::yasna.questionnaire-grid.index', $view_data);
    }



    /**
     *  show questionnaire key pair
     *
     * @param $hashid
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showKeyPair($hashid)
    {
        $view_data['key_pairs'] = model('questionnaire', $hashid)->keyPairs()->get();
        $view_data['socials']   = $this->getSocials($view_data['key_pairs']);
        $view_data['locales']   = $this->getLocales($view_data['key_pairs']);
        $view_data['generals']  = $this->getGenerals($view_data['key_pairs']);
        $view_data['statics']   = $this->getStaticPages($view_data['key_pairs']);
        $view_data['dynamics']  = $this->getDynamicPages($view_data['key_pairs']);
        $view_data['shops']     = $this->getShop($view_data['key_pairs']);
        $view_data['manages']   = $this->getManages($view_data['key_pairs']);


        //dd($view_data['key_pairs']->where('name','customers_gender')->first()->value);
        return view('yasnateam::yasna.questionnaire-grid.modal', $view_data);
    }



    /**
     * filter collection to get locales
     *
     * @param Collection $collect
     *
     * @return collection
     */
    private function getLocales(Collection $collect)
    {
        return $collect->filter(function ($item) {
            return str_contains($item->name, 'locale') and ($item->value != '0');
        });
    }



    /**
     * filter collection to get socials
     *
     * @param Collection $collect
     *
     * @return collection
     */
    private function getSocials(Collection $collect)
    {
        return $collect->filter(function ($item) {
            return str_contains($item->name, 'social') and ($item->value != '0') and !str_contains($item->name,
                      'social_media_management');
        });
    }



    /**
     * filter collection to get generals
     *
     * @param Collection $collect
     *
     * @return collection
     */
    private function getGenerals(Collection $collect)
    {
        return $collect->filter(function ($item) {
            return str_contains($item->name, 'general_options') and ($item->value != '0');
        });
    }



    /**
     * filter collection to get Static Pages
     *
     * @param Collection $collect
     *
     * @return collection
     */
    private function getStaticPages(Collection $collect)
    {
        return $collect->filter(function ($item) {
            return str_contains($item->name, 'static_pages') and ($item->value != '0');
        });
    }



    /**
     * filter collection to get Dynamic Pages
     *
     * @param Collection $collect
     *
     * @return collection
     */
    private function getDynamicPages(Collection $collect)
    {
        return $collect->filter(function ($item) {
            return str_contains($item->name, 'dynamic_pages') and ($item->value != '0');
        });
    }



    /**
     * filter collection to get shop
     *
     * @param Collection $collect
     *
     * @return collection
     */
    private function getShop(Collection $collect)
    {
        return $collect->filter(function ($item) {
            return str_contains($item->name, 'shop_') and ($item->value != '0');
        });
    }



    /**
     * filter collection to get manage
     *
     * @param Collection $collect
     *
     * @return collection
     */
    private function getManages(Collection $collect)
    {
        return $collect->filter(function ($item) {
            return str_contains($item->name, 'manage_dashboard') and ($item->value != '0');
        });
    }
}
