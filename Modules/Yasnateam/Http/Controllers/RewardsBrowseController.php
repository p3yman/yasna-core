<?php namespace Modules\Yasnateam\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class RewardsBrowseController extends YasnaController
{
    protected $base_model  = "Reward";
    protected $view_folder = "yasnateam::rewards.browse";

    private $page;
    private $view_arguments;
    private $models;

    private $specified_user        = null;
    private $specified_reason      = null;
    private $specified_group       = null;
    private $specified_settlement  = false;
    private $specified_forgiveness = false;



    /**
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if (user()->as('admin')->cannot('rewards')) {
            return $this->abort(403);
        }

        $this->buildPageInfo();
        $this->queryBuilder();
        $this->buildViewArguments();

        return $this->view('index', $this->view_arguments);
    }



    /**
     * Query
     */
    private function queryBuilder()
    {
        $elector_array = [
             "user"        => $this->specified_user,
             "reason"      => $this->specified_reason,
             "group"       => $this->specified_group,
             "settlement"  => $this->specified_settlement,
             "forgiveness" => $this->specified_forgiveness,
        ];

        $this->models = model("Reward")
             ->elector($elector_array)
             ->orderBy('effected_at', 'DESC')
             ->paginate(20)
        ;
    }



    /**
     * View Arguments
     */
    private function buildViewArguments()
    {
        $this->view_arguments = [
             "page"   => $this->page,
             "models" => $this->models,
        ];
    }



    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    private function buildPageInfo()
    {
        $browse_url = "rewards/browse";

        $this->page[0] = [
             $url = $browse_url,
             trans_safe("yasnateam::rewards.title"),
             $browse_url,
        ];
    }
}
