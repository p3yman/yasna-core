<?php namespace Modules\Yasnateam\Http\Controllers;

use Modules\Yasna\Services\YasnaController;
use Modules\Yasnateam\Http\Requests\RewardsDeleteRequest;
use Modules\Yasnateam\Http\Requests\RewardsForgiveRequest;
use Modules\Yasnateam\Http\Requests\RewardsSaveRequest;

class RewardsActionController extends YasnaController
{
    protected $base_model  = "Reward";
    protected $view_folder = "yasnateam::rewards.action";



    /**
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        if (user()->as('admin')->cannot('rewards.create')) {
            return $this->abort(403);
        }

        $model              = model('Reward');
        $model->effected_at = $this->now();
        $model->weight      = 1;

        return $this->view('editor', compact('model'));
    }



    /**
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editor($hashid)
    {
        $model = $this->model($hashid)->spreadMeta();

        if ($model->cannotEdit()) {
            return $this->abort(403);
        }

        return $this->view('editor', compact('model'));
    }



    /**
     * @param RewardsSaveRequest $request
     *
     * @return string
     */
    public function save(RewardsSaveRequest $request)
    {
        $saved = $request->model->batchSaveBoolean($request);

        return $this->jsonAjaxSaveFeedback($saved, [
             'success_callback' => "rowUpdate('tblRewards','$request->hashid')",
        ]);
    }



    /**
     * @param RewardsDeleteRequest $request
     *
     * @return string
     */
    public function delete(RewardsDeleteRequest $request)
    {
        $deleted = $request->model->delete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "rowHide('tblRewards','$request->hashid')",
        ]);
    }



    /**
     * @param RewardsForgiveRequest $request
     *
     * @return string
     */
    public function forgive(RewardsForgiveRequest $request)
    {
        $forgiven = $request->model->forgive();

        return $this->jsonAjaxSaveFeedback($forgiven, [
             'success_callback' => "rowUpdate('tblRewards','$request->hashid')",
        ]);
    }
}
