<?php

namespace Modules\Yasnateam\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Modules\Yasna\Services\YasnaController;

class PostHandlerController extends YasnaController
{
    /**
     * Post Editor Handlers
     *
     * @param Post $model
     */
    public static function editor(Post $model)
    {
        static::customersEditor($model);
    }



    /**
     * Posts Save Gaters
     *
     * @param Post  $model
     * @param array $data
     *
     * @return array
     */
    public static function saveGate(Post $model, array $data)
    {
        $data = static::customerSaveGate($model, $data);

        return $data;
    }



    /**
     * Customer Posts Editor Handlers
     *
     * @param Post $model
     */
    protected static function customersEditor(Post $model)
    {
        if ($model->hasnot('customer')) {
            return;
        }

        service("posts:editor_main")
             ->add('customer')
             ->blade(module('yasnateam')->getBladePath('manage.posts.editor.customer'))
             ->order(50)
        ;

    }



    /**
     * Customer Posts Save Gates
     *
     * @param Post  $model
     * @param array $data
     *
     * @return array
     */
    protected static function customerSaveGate(Post $model, array $data)
    {
        if (array_key_exists('project_date', $data) and $data['project_date']) {
            $seconds              = $data['project_date'] / 1000;
            $data['project_date'] = Carbon::createFromTimestamp($seconds)->toDateString();
        }

        if (array_key_exists('project_version', $data) and $data['project_version']) {
            $data['project_version'] = ed($data['project_version']);
        }

        return $data;
    }


}
