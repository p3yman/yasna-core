<?php

namespace Modules\Yasnateam\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class RedirectController extends YasnaController
{
    /**
     * redirect to the vue workshop.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function vueWorkshop()
    {
        return redirect("https://evand.com/events/vuejs-nuxtjs-workshop-2");
    }



    /**
     * redirect to the scrum workshop.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function scrumWorkshop()
    {
        return redirect("https://evand.com/events/yasna-scrum-workshop");
    }
}
