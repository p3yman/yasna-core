<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 21/01/2018
 * Time: 06:15 PM
 */

namespace Modules\Yasnateam\Http\Controllers\Traits;

use App\Models\Post;
use App\Models\Posttype;
use Modules\Yasnateam\Services\Template\YasnateamTemplateHandler as Template;

trait YasnateamTemplateTrait
{
    private $posttypeSlugs = [
        'links' => 'links',
    ];

    protected function template(...$parameters)
    {
        $this->readSettings()
            ->readRelatedLinks()
            ->readPosttypes();
        return parent::template(...$parameters);
    }

    protected function darkHeaderNeeded()
    {
        Template::setDarkHeader(true);

        return $this;
    }

    protected function readSettings()
    {
        return $this->readSiteTitleSetting()
            ->readSocialLinks()
            ->readContactInfo()
            ->readSiteLanguages()
            ->readYasnaTeamTitle();
    }

    protected function readSiteTitleSetting()
    {
        if ($siteTitle = get_setting('site_title')) {
            Template::setSiteTitle($siteTitle);
            Template::appendToPageTitle($siteTitle);
        }
        return $this;
    }

    protected function readContactInfo()
    {
        if ($address = get_setting('address')) {
            Template::mergeWithContactInfo(['address' => $address]);
        }
        if (($telephone = get_setting('telephone')) and is_array($telephone) and count($telephone)) {
            Template::mergeWithContactInfo(['telephone' => $telephone]);
        }
        if (($email = get_setting('email')) and is_array($email) and count($email)) {
            Template::mergeWithContactInfo(['email' => $email]);
        }
        return $this;
    }

    protected function readSocialLinks()
    {
        if ($instagram = get_setting('instagram_link')) {
            Template::appendToSocialLinks([
                'link'   => $instagram,
                'font-o' => 'instagram',
            ]);
        }
        if ($twittier = get_setting('twitter_link')) {
            Template::appendToSocialLinks([
                'link'   => $twittier,
                'font-o' => 'twitter',
            ]);
        }
        if ($github = get_setting('github_link')) {
            Template::appendToSocialLinks([
                'link'   => $github,
                'font-o' => 'github',
            ]);
        }
        if ($fb = get_setting('facebook_link')) {
            Template::appendToSocialLinks([
                'link'   => $fb,
                'font-o' => 'facebook-square',
            ]);
        }
        if ($telegram = get_setting('telegram_link')) {
            Template::appendToSocialLinks([
                'link'   => $telegram,
                'font-o' => 'telegram',
            ]);
        }
        if ($rss = get_setting('rss_link')) {
            Template::appendToSocialLinks([
                'link'   => $rss,
                'font-o' => 'rss',
            ]);
        }
        if ($linkedin = get_setting('linkedin_account')) {
            Template::appendToSocialLinks([
                'link'   => $linkedin,
                'font-o' => 'linkedin',
            ]);
        }
        if ($virgool = get_setting('blog_link')) {
            Template::appendToSocialLinks([
                'link'   => $virgool,
                'font-o' => 'bold',
            ]);
        }
        return $this;
    }

    protected function readSiteLanguages()
    {
        Template::setSiteLocales(get_setting('site_locales') ?: config('yasnateam.default-locales'));
        return $this;
    }

    protected function readYasnaTeamTitle()
    {
        Template::setYasnaTitle(
            get_setting('yasnaـgroupـtitle') ?: trans('yasnateam::yasna.template.yasna_group')
        );
        return $this;
    }

    protected function readRelatedLinks()
    {
        $posttype = posttype($typeSlug = $this->posttypeSlugs['links']);
        if ($posttype->exists) {
            if (
                ($category1 = $posttype->categories()->where('slug', 'category1')->first()) and
                ($posts1 = Post::selector(['type' => $typeSlug, 'category' => $category1->slug])
                    ->limit(6)
                    ->orderBy('published_at', 'desc')
                    ->get()) and
                $posts1->count()
            ) {
                Template::mergeWithRelatedLinks([
                    'group1' => [
                        'category' => $category1,
                        'links'    => $posts1,
                    ],
                ]);
            }
            if (
                ($category2 = $posttype->categories()->where('slug', 'category2')->first()) and
                ($posts2 = Post::selector(['type' => $typeSlug, 'category' => $category2->slug])
                    ->limit(6)
                    ->orderBy('published_at', 'desc')
                    ->get()) and
                $posts2->count()
            ) {
                Template::mergeWithRelatedLinks([
                    'group2' => [
                        'category' => $category2,
                        'links'    => $posts2,
                    ],
                ]);
            }
        }
        return $this;
    }

    protected function readPosttypes()
    {
        Template::mergeWithPosttypes(['blog' => posttype('blog')]);

        return $this;
    }
}
