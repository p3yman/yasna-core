<?php

namespace Modules\Yasnateam\Http\Controllers;

use Modules\Yasna\Services\YasnaController;
use Modules\Yasnateam\Http\Controllers\Traits\YasnateamTemplateTrait;
use Modules\Yasnateam\Http\Requests\JobsFormRequest;
use Nwidart\Modules\Facades\Module;
use Modules\Yasnateam\Services\Template\YasnateamTemplateHandler as Template;

class YasnateamNewFrontController extends YasnaController
{
    use YasnateamTemplateTrait;

    protected $base_model = "comment";

    protected $view_folder = "yasnateam::yasna-new";



    /**
     * YasnateamNewFrontController constructor.
     */
    public function __construct()
    {
        if ($siteTitle = get_setting('site_title')) {
            Template::appendToPageTitle($siteTitle);
        }
    }



    /**
     * Displays index page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $customers = model('post')
             ->elector(['type' => 'customers'])
             ->limit(12)
             ->get()
        ;

        $team_members = model('post')
             ->elector(['type' => 'ourteam'])
             ->limit(12)
             ->get()
        ;

        $technologies = model('post')
             ->elector(['type' => 'technologies'])
             ->get()
        ;

        $statistics = model('post')
             ->elector(['type' => 'achievements'])
             ->get()
        ;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.home'));

        return $this->view('index.index', compact('customers', 'team_members', 'technologies', 'statistics'));
    }



    /**
     * Displays YasnaWeb 3 page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function yasnaweb()
    {
        $modules = model('post')
             ->elector(['type' => 'modules'])
             ->get()
        ;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.yasnaweb_banner.subtitle'));

        return $this->view('yasnaweb.yasnaweb', compact('modules'));
    }



    /**
     * Displays static page
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function page($slug)
    {
        $page = model('post')
             ->elector(['type' => 'pages'])
             ->whereSlug($slug)
             ->firstOrFail()
        ;

        // Set page title
        Template::appendToPageTitle($page->title);

        return $this->view('pages.page', compact('page'));
    }



    /**
     * Displays contact us page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function contact()
    {
        return (new FrontContactFormController())->form();
    }



    /**
     * Displays faq page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function faq()
    {
        $faqs = model('post')
             ->elector(['type' => 'faq'])
             ->get()
        ;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.faq'));

        return $this->view('pages.faq', compact('faqs'));
    }



    /**
     * Displays services landing
     *
     * @param string $service
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function services($service)
    {
        $services = ['website', 'mobile', 'ui-ux', 'seo', 'digital-marketing', 'e-commerce'];

        if (!in_array($service, $services)) {
            $this->abort(404);
        }

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.services.base') . trans('yasnateam::general.services.' . $service));

        return $this->view('services.service', compact('service'));
    }



    /**
     * Displays blog posts list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function blog()
    {
        $posts = model('post')
             ->elector(['type' => 'blog'])
             ->paginate(6)
        ;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.blog'));

        return $this->view('blog.blog', compact('posts'));
    }



    /**
     * Displays blog posts list by category
     *
     * @param string $category
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function blogCategory($category)
    {
        $category = model('category')
             ->whereSlug($category)
             ->first()
        ;

        $posts = $category
             ->posts()
             ->wheretype('blog')
             ->paginate(6)
        ;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.blog_inner') . $category->title);

        return $this->view('blog.blog', compact('posts', 'category'));
    }



    /**
     * Displays blog posts list by tag
     *
     * @param string $tag
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function blogTag($tag)
    {
        $tag = model('tag')
             ->whereSlug($tag)
             ->first()
        ;

        $posts = $tag
             ->posts()
             ->wheretype('blog')
             ->paginate(6)
        ;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.blog_inner') . $tag->slug);

        return $this->view('blog.blog', compact('posts', 'tag'));
    }



    /**
     * Displays blog single post
     *
     * @param string $category
     * @param int    $id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function blogSingle($category, $id)
    {
        $post     = model('post')->find($id);
        $category = model('category')->whereSlug($category)->first();
        $tags     = $post->tags;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.blog_inner') . $post->title);

        return $this->view('blog.blog-single', compact('post', 'category', 'tags'));
    }



    /**
     * Displays active jobs list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function jobs()
    {
        $jobs = model('post')
             ->elector(['type' => 'jobs'])
             ->get()
        ;

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.jobs'));

        return $this->view('jobs.jobs', compact('jobs'));
    }



    /**
     * Displays job single
     *
     * @param int $id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function jobsSingle($id)
    {
        $job = model('post')->find($id);

        // Set page title
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.jobs_inner') . $job->title);

        return $this->view('jobs.job-single', compact('job'));
    }



    /**
     * Displays job apply form
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function jobsApplyForm()
    {
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.jobs_apply'));

        return $this->view('jobs.job-apply');
    }



    /**
     * Submit job apply form
     *
     * @param JobsFormRequest $request
     *
     * @return string
     */
    public function sendJobsApplyForm(JobsFormRequest $request)
    {
        $post     = post("jobs-apply-form");
        $posttype = $post->posttype();

        if (!$post->exists or !$posttype->exists) {
            return $this->jsonFeedback(trans("yasnateam::job.feed_danger"));
        }

        $orginal_name = $request->resume->getClientOriginalName();
        $file_name    = time() . '-' . $orginal_name;

        $request->resume->move(public_path('/modules/yasnateam/resume'), $file_name);

        $saved = $this->model()->batchSave([
             "user_id"           => user()->id,
             "post_id"           => $post->id,
             "posttype_id"       => $posttype->id,
             "is_from_admin"     => user()->isAdmin(),
             "guest_name"        => $request->name,
             "guest_email"       => $request->email,
             "guest_mobile"      => $request->mobile,
             "guest_ip"          => $request->ip(),
             "subject"           => "jobs-apply-form-subject",
             "text"              => $request->courses,
             "resume"            => $file_name,
             'gender'            => $request->gender,
             'marriage'          => $request->marriage,
             'bd'                => $request->bd,
             'address'           => $request->address,
             'tel'               => $request->tel,
             'insurance'         => $request->insurance,
             'experience'        => $request->experience,
             'edu_degree'        => $request->edu_degree,
             'edu_field'         => $request->edu_field,
             'english[reading]'  => $request->english['reading'],
             'english[writing]'  => $request->english['writing'],
             'english[speaking]' => $request->english['speaking'],
             'english[degree]'   => $request->english['degree'],
             'salary'            => $request->salary,
             'available_date'    => $request->available_date,
        ])
        ;

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             'success_message'    => trans("yasnateam::job.feed_success"),
             'danger_message'     => trans("yasnateam::job.feed_danger"),
             "success_form_reset" => true,
        ]);
    }
}
