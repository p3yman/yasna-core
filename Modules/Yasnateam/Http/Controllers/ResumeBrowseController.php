<?php

namespace Modules\Yasnateam\Http\Controllers;

use Symfony\Component\HttpFoundation\File\File;
use Modules\Yasna\Services\YasnaController;

class ResumeBrowseController extends YasnaController
{
    protected $base_model  = "Comment";
    protected $view_folder = "yasnateam::yasna-new.resume";

    private $models;
    private $page;
    private $view_arguments;



    /**
     * show list of all resumes
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if (!user()->as('admin')->can('resumes')) {
            return $this->abort(404);
        }

        $this->buildPageInfo();
        $this->queryBuilder();
        $this->buildViewArguments();

        return $this->view('index', $this->view_arguments);
    }



    /**
     * Query for resumes
     */
    private function queryBuilder()
    {
        $elector_array = [
             "subject" => "jobs-apply-form-subject",
        ];

        $this->models = model("Comment")
             ->elector($elector_array)
             ->orderBy('created_at', 'DESC')
             ->paginate(15)
        ;
    }



    /**
     * View Arguments
     */
    private function buildViewArguments()
    {
        $this->view_arguments = [
             "page"   => $this->page,
             "models" => $this->models,
        ];
    }



    /**
     * Builds page info, to be used in manage top-bar, and stores the result in $this->page.
     */
    private function buildPageInfo()
    {
        $browse_url = "resumes";

        $this->page[0] = [
             $url = $browse_url,
             trans_safe("yasnateam::job.resumes"),
             $browse_url,
        ];
    }



    /**
     * Show single resume modal
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function show($hashid)
    {
        $model = model('comment', $hashid);

        return $this->view('show', compact("model"));
    }



    /**
     * Return content of confirm modal for deciding about deleting a resume
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteConfirm($hashid)
    {
        $model       = model('comment', $hashid);
        $title_label = trans('yasnateam::job.delete_job_form', ['name' => $model->guest_name]);

        if (!$model->exists) {
            return $this->jsonFeedback(trans('yasnateam::job.job_not_found'));
        }

        return $this->view('delete', compact('model', 'title_label'));
    }



    /**
     * Trash a resume
     *
     * @param string $hashid
     *
     * @return string | \Symfony\Component\HttpFoundation\Response
     */
    public function delete($hashid)
    {
        $model = model('Comment', $hashid);
        $name  = $model->guest_name;

        if (!$model->exists) {
            return $this->abort('410');
        }

        $deleted = $model->delete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_message' => trans('yasnateam::job.job_has_been_deleted_successfully', ['name' => $name]),
             'success_refresh' => 1,
        ]);
    }



    /**
     * Download resume
     *
     * @param string $hashid
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function downloadResume($hashid)
    {
        $comment = model('comment', $hashid);
        if (!$comment->exists) {
            return $this->abort('404');
        }

        $file_name = $comment->meta['resume'];
        $file_path = "Assets" . DIRECTORY_SEPARATOR . "resume" . DIRECTORY_SEPARATOR . $file_name;
        $full_path = module('yasnateam')->getPath($file_path);

        try {
            $file = new File($full_path);
        } catch (\Exception $e) {
            return $this->abort(410);
        }

        if (!$file) {
            return $this->abort(404);
        }

        $headers = [
             'Content-Type: ' . $file->getMimeType(),
        ];

        return response()->download($file->getPathname(), $file->getFilename(), $headers);
    }
}
