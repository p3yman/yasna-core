<?php

namespace Modules\Yasnateam\Http\Controllers;

use Modules\Yasna\Services\YasnaController;
use Modules\Yasnateam\Http\Requests\RewardsDownstreamSaveRequest;

class RewardsDownstreamController extends YasnaController
{
    protected $base_model  = "reward-reason";
    protected $view_folder = "yasnateam::rewards.downstream";
    protected $row_view    = 'row';



    /**
     * Browse Index, called from service environment
     *
     * @return array
     */
    public static function index()
    {
        return [
             "view_file" => "yasnateam::rewards.downstream.index",
             "models"    => rewardReason()->orderBy('group')->orderBy('text')->paginate(50),
        ];
    }



    /**
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        $model = rewardReason();

        return $this->view('editor', compact('model'));
    }



    /**
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($hashid)
    {
        $model = rewardReason($hashid);

        if ($model->not_exists) {
            return $this->abort(410);
        }

        return $this->view('editor', compact('model'));
    }



    /**
     * @param RewardsDownstreamSaveRequest $request
     *
     * @return string
     */
    public function save(RewardsDownstreamSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($saved, [
             'success_callback' => "rowUpdate('tblRewards','$request->hashid')",
        ]);
    }
}
