<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 11:24 AM
 */

namespace Modules\Yasnateam\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Modules\Yasnateam\Http\Controllers\Traits\FeedbackControllerTrait;
use Modules\Yasnateam\Http\Controllers\Traits\YasnateamTemplateTrait;

abstract class YasnateamFrontControllerAbstract extends YasnateamControllerAbstract
{
    use FeedbackControllerTrait {
        abort as protected traitAbort;
    }
    use YasnateamTemplateTrait;

    protected function abort($errorCode, $minimal = false, $data = null)
    {
        if (is_null($data)) {
            $data = $this->template(
                'errors' . ($minimal ? '.m' : '') . '.' . $errorCode,
                $this->variables
            );
        }
        return $this->traitAbort($errorCode, $minimal, $data);
    }
}
