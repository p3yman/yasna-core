<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 11:18 AM
 */

namespace Modules\Yasnateam\Http\Controllers;

abstract class YasnateamControllerAbstract extends ModuleControllerAbstract
{
    protected static $moduleAlias = 'yasnateam';

    protected $variables = [];
}
