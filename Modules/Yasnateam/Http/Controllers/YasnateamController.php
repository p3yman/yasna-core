<?php

namespace Modules\Yasnateam\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class YasnateamController extends YasnateamFrontControllerAbstract
{
    // general data needed in every page
    public $switches = [];

    protected $posttypesSlugs = [
         'blog' => 'blog',
    ];



    /**
     * YasnateamController constructor.
     */
    public function __construct()
    {
        Paginator::defaultView('yasnateam::yasna.pagination.default');
    }



    /**
     * @return mixed
     */
    public function index()
    {
        return $this
             ->selectSlideShow()
             ->selectIntroData()
             ->selectInformationPost()
             ->selectCustomers()
             ->selectWhyUs()
             ->selectAboutPost()
             ->selectProfileCards()
             ->selectLocationInfo()
             ->loadAchievements()
             ->loadTechnologies()
             ->template('yasna.index.index', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function selectSlideShow()
    {
        $this->variables['slideshow'] = model('post')->elector(['type' => 'slideshows'])
                                                     ->limit(10)
                                                     ->orderBy('published_at', 'desc')
                                                     ->get()
        ;
        return $this;
    }



    /**
     * @return $this
     */
    protected function selectIntroData()
    {
        $posttype = posttype('services');
        $posts    = model('post')
             ->elector(['type' => $posttype->slug])
             ->orderBy('published_at', 'desc')
             ->limit(6)
             ->get()
        ;

        if ($posttype and $posttype->exists and $posts and $posts->count()) {
            $this->variables['introData'] = [
                 'main_heading' => $posttype->titleIn(getLocale()),
                 'intro'        => $posts,
            ];
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectInformationPost()
    {
        $this->variables['informationPost'] = model('post')
             ->elector([
                  'type' => 'yasnaweb-info',
                  'slug' => 'yasnaweb-information',
             ])
             ->first()
        ;
        return $this;
    }



    /**
     * @return $this
     */
    protected function selectCustomers()
    {
        $posttype = posttype('customers');
        $posts    = model('post')
             ->elector(['type' => $posttype->slug])
             ->orderBy('published_at', 'desc')
             ->limit(12)
             ->get()
        ;
        if ($posttype and $posttype->exists and $posts and $posts->count()) {
            $this->variables['logobar'] = [
                 'main_heading' => $posttype->titleIn(getLocale()),
                 'customers'    => $posts,
            ];
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectWhyUs()
    {
        $posttype = posttype('whyus');
        $posts    = model('post')
             ->elector(['type' => $posttype->slug])
             ->orderBy('published_at', 'desc')
             ->limit(6)
             ->get()
        ;
        if ($posttype and $posttype->exists and $posts and $posts->count()) {
            $this->variables['whyus'] = [
                 'main_heading' => $posttype->titleIn(getLocale()),
                 'reasons'      => $posts,
            ];
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectAboutPost()
    {
        // about us
        $this->variables['aboutPost'] = model('post')
             ->elector([
                  'type' => 'pages',
                  'slug' => 'meet-our-team',
             ])
             ->first()
        ;
        return $this;
    }



    /**
     * @return $this
     */
    protected function selectProfileCards()
    {
        //profile cards data
        $this->variables['profileCards'] = model('post')
             ->elector(['type' => 'ourteam'])
             ->limit(12)
             ->inRandomOrder()
             ->get()
        ;

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectLocationInfo()
    {
        $location = get_setting('location');
        if ($location and is_array($location) and (count($location) == 2)) {
            $this->variables['location'] = $location;
        }
        return $this;
    }



    /**
     * @return $this
     */
    public function blogPosts(Request $request)
    {
        return $this
             ->darkHeaderNeeded()
             ->renderBlogListResult($request)
             ;
    }



    /**
     * @return $this
     */
    protected function selectBlogCategories()
    {
        if ($this->blogPosttypeExists()) {
            $this->variables['categories'] = $this->variables['posttype']
                 ->categories()
                 ->select('categories.*')
                 ->join('category_post', 'categories.id', 'category_post.category_id')
                 ->join('posts', 'category_post.post_id', 'posts.id')
                 ->whereNull('posts.deleted_at')
                 ->whereNotNull('posts.published_at')
                 ->where('posts.locale', getLocale())
                 ->groupBy('categories.id')
                 ->get()
            ;
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function renderBlogListResult(Request $request)
    {
        if ($this->blogPosttypeExists() and $this->blogListCategoryIsValid($request)) {
            return $this
                 ->blogListGuessCurrentCategory($request)
                 ->selectBlogPosts()
                 ->template('yasnateam::yasna.blog-posts.list-view.0blog-list-view', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return $this
     */
    protected function blogPosttypeExists()
    {
        if (!array_key_exists('posttype', $this->variables)) {
            $this->variables['posttype'] = posttype($this->posttypesSlugs['blog']);
        }

        return ($this->variables['posttype'] and $this->variables['posttype']->exists);
    }



    /**
     * @return $this
     */
    protected function blogListCategoryIsValid(Request $request)
    {
        $this->selectBlogCategories();
        return is_null($request->category) or
             ($this->variables['categories']->pluck('slug')->search($request->category) !== false);
    }



    /**
     * @return $this
     */
    protected function blogListGuessCurrentCategory(Request $request)
    {
        $this->variables['currentCategory'] = is_null($request->category)
             ? $this->variables['categories']->first()
             : $this->variables['categories']->where('slug', $request->category)->first();

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectBlogPosts()
    {
        $this->variables['posts'] = model('post')->elector([
             'type'     => $this->variables['posttype']->slug,
             'category' => $this->variables['currentCategory'] ? $this->variables['currentCategory']->slug : null,
        ])
                                                 ->orderBy('published_at', 'desc')
                                                 ->paginate(12)
        ;
        return $this;
    }



    /**
     * @return $this
     */
    public function article(Request $request)
    {
        return $this
             ->findBlogPost($request->post, $request->category)
             ->darkHeaderNeeded()
             ->renderBlogSingleResult()
             ;
    }



    /**
     * @return $this
     */
    protected function findBlogPost($hashid, $category)
    {
        $encrypted = hashid_decrypt($hashid, 'ids');
        if ($encrypted and is_array($encrypted) and count($encrypted)) {
            $this->variables['post'] = model('post')->elector([
                 'type'     => $this->posttypesSlugs['blog'],
                 'id'       => $encrypted[0],
                 'category' => $category,
            ])->first()
            ;
        } else {
            $this->variables['post'] = null;
        }
        $this->variables['currentCategory'] = '';
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderBlogSingleResult()
    {
        if ($this->variables['post']) {
            return $this->selectBlogCategories()
                        ->template('yasna.blog-posts.single.0single', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return $this
     */
    protected function loadAchievements()
    {
        $this->variables['achievements'] = model('Posttype', 'achievements')
             ->posts()
             ->limit(6)
             ->get()
        ;

        return $this;
    }



    /**
     * @return $this
     */
    protected function loadTechnologies()
    {
        $technologies = model('posttype', 'technologies');
        if ($technologies->exists) {
            $this->variables['technologies'] = $technologies;
        }

        return $this;
    }



    /**
     * @return $this
     */
    public function test(Request $request)
    {
        dd($request->toArray());
    }



    /**
     * @return mixed
     */
    public function inquire()
    {
        $view_data['REFERER'] = $this->setRefferer();

        return $this->darkHeaderNeeded()
                    ->template('yasna.blog-posts.domain.main', $view_data)
             ;
    }



    /**
     * @return mixed
     */
    public function survey()
    {
        return $this->darkHeaderNeeded()
                    ->template('yasna.blog-posts.survey.main')
             ;
    }



    /**
     * @param Request $request
     *
     * @return int
     */
    public function surveyFrom(Request $request)
    {
        return 1;
    }



    /**
     * try to find HTTP_REFERER url and check that it be from other domain
     *
     * @return $_SERVER['HTTP_REFERER']
     */
    private function setRefferer()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (!str_contains($_SERVER['HTTP_REFERER'], url('/'))) {
                return $_SERVER['HTTP_REFERER'];
            }
        }
        return null;
    }
}
