<?php

namespace Modules\Yasnateam\Http\Controllers;

use Modules\Yasna\Services\YasnaController;
use Modules\Yasnateam\Http\Requests\FrontContactRequest;
use Modules\Yasnateam\Services\Template\YasnateamTemplateHandler as Template;

class FrontContactFormController extends YasnaController
{
    protected $base_model  = "comment";
    protected $view_folder = "yasnateam::yasna-new.pages.contact";



    /**
     * show the whole page, including the form
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function form()
    {
        Template::appendToPageTitle(trans('yasnateam::general.page_titles.contact'));

        return $this->view();
    }



    /**
     * submit the contact form
     *
     * @param FrontContactRequest $request
     *
     * @return string
     */
    public function submit(FrontContactRequest $request)
    {
        $post     = post("contact-form");
        $posttype = $post->posttype();

        if (!$post->exists or !$posttype->exists) {
            return $this->jsonFeedback(trans("yasnateam::contact.feed_danger"));
        }

        $saved = $this->model()->batchSave([
             "user_id"       => user()->id,
             "post_id"       => $post->id,
             "posttype_id"   => $posttype->id,
             "is_from_admin" => user()->isAdmin(),
             "guest_name"    => $request->name,
             "guest_email"   => $request->email,
             "guest_mobile"  => $request->phone,
             "guest_ip"      => $request->ip(),
             "subject"       => $request->subject,
             "text"          => $request->message,
        ])
        ;

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             'success_message'    => trans("yasnateam::contact.feed_success"),
             'danger_message'     => trans("yasnateam::contact.feed_danger"),
             "success_form_reset" => true,
        ]);
    }
}
