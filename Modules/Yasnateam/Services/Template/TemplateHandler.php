<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 12:17 PM
 */

namespace Modules\Yasnateam\Services\Template;

class TemplateHandler
{
    private static $methodPrefixes = [
        'appending' => 'appendTo',
        'merging'   => 'mergeWith',
        'imploding' => 'implode',
        'assigning' => 'set',
    ];
    protected static $__calledMethod = null;

    public static function __callStatic($name, $arguments)
    {
        static::$__calledMethod = $name;

        if (starts_with($name, self::$methodPrefixes['appending'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['appending']));
            return self::appendToProperty($property, $arguments);
        } elseif (starts_with($name, self::$methodPrefixes['merging'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['merging']));
            return self::mergeWithProperty($property, ...$arguments);
        } elseif (starts_with($name, self::$methodPrefixes['imploding'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['imploding']));
            return self::implodeProperty($property, $arguments);
        } elseif (starts_with($name, self::$methodPrefixes['assigning'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['assigning']));
            return self::setProperty($property, $arguments);
        } else {
            if (property_exists(static::class, $name)) {
                return static::$$name;
            }
        }

        return self::undefinedMethodException();
    }

    protected static function appendToProperty($property, $appending)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge(static::$$property, $appending);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function mergeWithProperty($property, $merging)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge(static::$$property, $merging);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function setProperty($property, $parameters)
    {
        if (property_exists(static::class, $property)) {
            if (count($parameters)) {
                static::$$property = $parameters[0];
            } else {
                static::tooFewArgumentsException(1);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function implodeProperty($property, $parameters)
    {
        if (property_exists(static::class, $property) and is_array($propertyVal = static::$$property)) {
            if (count($parameters)) {
                return implode($parameters[0], $propertyVal);
            } else {
                static::tooFewArgumentsException(1);
            }
        }

        self::undefinedMethodException();
    }

    private static function undefinedMethodException()
    {
        trigger_error(
            'Call to undefined method ' . __CLASS__ . '::' . static::$__calledMethod . '()', E_USER_ERROR
        );
    }

    private static function tooFewArgumentsException($passedArgumentsCount)
    {
        trigger_error(
            'Too few arguments to function '
            . __CLASS__ . '::' .
            static::$__calledMethod
            . ', '
            . $passedArgumentsCount
            . ' passed', E_USER_ERROR
        );
    }
}
