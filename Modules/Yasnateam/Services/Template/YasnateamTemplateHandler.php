<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 12:22 PM
 */

namespace Modules\Yasnateam\Services\Template;

use Nwidart\Modules\Facades\Module;

class YasnateamTemplateHandler extends TemplateHandler
{
    protected static $siteTitle;
    protected static $pageTitle    = [];
    protected static $darkHeader;
    protected static $contactInfo  = [];
    protected static $relatedLinks = [];
    protected static $socialLinks  = [];
    protected static $siteLocales  = [];
    protected static $yasnaTitle;
    protected static $posttypes    = [];

    protected static $siteLogoUrl;

    protected static $moduleName = 'Yasnateam';

    public static function siteLogoUrl()
    {
        if (is_null(static::$siteLogoUrl)) {
            if (
                ($settingLogo = setting(self::darkHeader() ? 'site_logo_tiny' : 'site_logo')->gain()) and
                ($logoFile = doc($settingLogo)->getUrl())
            ) {
                static::$siteLogoUrl = $logoFile;
            } else {
                static::$siteLogoUrl = Module::asset(
                    static::$moduleName
                    . ":images/"
                    . (self::darkHeader() ? 'logo-white' : 'logo')
                    . ".png"
                );
            }
        }

        return static::$siteLogoUrl;
    }
}
