<?php

namespace Modules\Yasnateam\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Filemanager\Services\Seeder\PosttypeSeedingTool;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PosttypesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PosttypeSeedingTool)->seedPosttypes($this->posttypesData());
    }



    /**
     * Returns an array of posttypes data.
     *
     * @return array
     */
    protected function posttypesData()
    {
        $module = $this->runningModule();
        return [
             [
                  'posttype' => [
                       'slug'           => 'customers',
                       'title'          => $module->getTrans('seeder.posttypes.customers.plural'),
                       'singular_title' => $module->getTrans('seeder.posttypes.customers.singular'),
                       'template'       => "special",
                       'icon'           => "handshake-o",
                       'order'          => "2",
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'customer',
                       'slug',
                       'manage_sidebar',
                  ],
             ],
        ];
    }
}
