<?php

namespace Modules\Yasnateam\Database\Seeders;

use Illuminate\Database\Seeder;

class ContactFormsFeatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedPosttypes();
        $this->seedPosttypeFeatures();
        $this->seedPosts();
    }



    /**
     * seed the necessary posttype(s)
     */
    protected function seedPosttypes()
    {
        yasna()::seed("posttypes", [
             [
                  'slug'           => 'commenting',
                  'title'          => trans("yasnateam::seeders.commenting_plural"),
                  'order'          => '12',
                  'singular_title' => trans("yasnateam::seeders.commenting_singular"),
                  'template'       => 'special',
                  'icon'           => 'comment',
             ],
        ]);
    }



    /**
     * seed posttype features
     */
    protected function seedPosttypeFeatures()
    {
        posttype("commenting")->attachFeatures("commenting");

    }



    /**
     * seed posts
     */
    protected function seedPosts()
    {
        yasna()->seed("posts", [
             [
                  "slug"         => "contact-form",
                  "type"         => "commenting",
                  "title"        => trans("yasnateam::seeders.contact_us"),
                  "locale"       => "fa",
                  "sisterhood"   => post()::generateSisterhood(),
                  "published_at" => now()->toDateTimeString(),
             ],
        ]);
    }
}
