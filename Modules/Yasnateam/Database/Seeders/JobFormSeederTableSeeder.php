<?php

namespace Modules\Yasnateam\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class JobFormSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedPost();
    }


    /**
     * seed post for job form
     */
    protected function seedPost()
    {
        yasna()->seed("posts", [
             [
                  "slug"         => "jobs-apply-form",
                  "type"         => "commenting",
                  "title"        => trans("yasnateam::general.page_titles.jobs"),
                  "locale"       => "fa",
                  "sisterhood"   => post()::generateSisterhood(),
                  "published_at" => now()->toDateTimeString(),
             ],
        ]);
    }
}
