<?php

namespace Modules\Yasnateam\Database\Seeders;

use Illuminate\Database\Seeder;

class YasnateamDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ContactFormsFeatureSeeder::class);
        $this->call(JobFormSeederTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
        $this->call(PosttypesTableSeeder::class);
        $this->call(MapLinksTableSeeder::class);
    }
}
