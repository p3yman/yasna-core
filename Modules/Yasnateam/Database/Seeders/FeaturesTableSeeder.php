<?php

namespace Modules\Yasnateam\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class FeaturesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->data());
    }



    /**
     * Returns the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'slug'      => 'customer',
                  'title'     => $this->runningModule()->getTrans('seeder.features.customer'),
                  'order'     => '70',
                  'icon'      => 'handshake-o',
                  'post_meta' => '',
                  'meta-hint' => '',
             ],
        ];
    }
}
