<?php

namespace Modules\Yasnateam\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MapLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        mapper()::db()->url("jobs")->changeFrequency(mapper()::MONTHLY)->priority(0.3)->insert();
        mapper()::db()->url("contact-us")->changeFrequency(mapper()::NEVER)->priority(0.2)->insert();
        mapper()::db()->url("yasna-web")->changeFrequency(mapper()::MONTHLY)->priority(1)->insert();
        mapper()::db()->url("services/website")->changeFrequency(mapper()::MONTHLY)->priority(0.9)->insert();
        mapper()::db()->url("services/mobile")->changeFrequency(mapper()::MONTHLY)->priority(0.9)->insert();
        mapper()::db()->url("services/e-commerce")->changeFrequency(mapper()::MONTHLY)->priority(0.9)->insert();
        mapper()::db()->url("services/seo")->changeFrequency(mapper()::MONTHLY)->priority(0.9)->insert();
        mapper()::db()->url("services/ui-ux")->changeFrequency(mapper()::MONTHLY)->priority(0.9)->insert();
        mapper()::db()->url("services/digital-marketing")->changeFrequency(mapper()::MONTHLY)->priority(0.9)->insert();
    }
}
