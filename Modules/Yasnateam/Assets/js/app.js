jQuery(document).ready(function ($) {

    // AOS
    AOS.init({
        once: true,
        duration: 550,
    });

    // Clients
    $('.clients').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        rtl: true,
        nextArrow: '<button type="button" class="slick-next icon-arrow-left"></button>',
        prevArrow: '<button type="button" class="slick-prev icon-arrow-right"></button>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    // FAQs
    if( $('.faqs').length ){
        $('.faqs .item:first-child').addClass('active').find('.content').show();
    }
    $('.faqs .item a.head').on('click', function (e) {
        e.preventDefault();

        var item = $(this).parent();

        if( $(item).hasClass('active') ){
            return false;
        }

        $('.faqs .item').removeClass('active').find('.content').slideUp();
        $(item).addClass('active').find('.content').slideDown();
    })

    // Technologies
    $('.technologies').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        rtl: true,
        nextArrow: '<button type="button" class="slick-next icon-arrow-left"></button>',
        prevArrow: '<button type="button" class="slick-prev icon-arrow-right"></button>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    // Members height
    $('.members-info .item').matchHeight({
        byRow: false,
    });

    var members_owl = $('.members').owlCarousel({
        autoplay: true,
        autoplayTimeout: 20000,
        smartSpeed: 500,
        rtl: true,
        center: true,
        dots: false,
        items: 3,
        loop: true,
        margin: 10,
        nav:true,
        responsive:{
            0:{
                items:1,
            },
            768:{
                items:3,
            },
        },
        navText: ["<i class='icon-arrow-left'></i>", "<i class='icon-arrow-right'></i>"],
        afterMove: function (elem) {
            var current = this.currentItem;
            var src = elem.find(".owl-item").eq(current).find("img").attr('src');
            console.log('Image current is ' + src);
        }
    });
    members_owl.on('changed.owl.carousel', function(property) {
        var current = property.item.index;
        var target = $(property.target).find(".owl-item").eq(current).find('.item').data('target');
        $('.members-info .item').slideUp('fast');
        $(target).slideDown('fast');
    })
    members_owl.find('.owl-item').each( function( index ) {
        $(this).attr( 'data-position', index );
    });

    // Match height
    $('.inner-slick .item').matchHeight();

    // Tabs
    var start = false;
    if( $('.tabs').length ) {
        var tabs = $('.tabs');
        $('.tabs .titles a').on('click', function (e) {
            e.preventDefault();

            if ($(this).hasClass('active')) {
                return false;
            }

            var index = $(this).index();
            $(tabs).find('.titles a').removeClass('active');
            $(this).addClass('active');
            $(tabs).find('.contents .content').slideUp();
            $(tabs).find('.contents .content').eq(index).slideDown();
        });

        var tabs = $('.tabs');
        var targetOffset = $(tabs).offset().top;
        var $w = $(window).scroll(function () {
            if ($w.scrollTop() + 200 > targetOffset && start === false) {

                start = true;

            }
        });

        // Tabs height
        $('.tabs .contents .content').matchHeight({
            byRow: false,
        });

        function interval(tabs) {

            setInterval(function () {
                var active = $(tabs).find('a.active');
                var nextTab = active.index() < $(tabs).find('a').length - 1 ? active.next() : $(tabs).find('a').first();
                nextTab.click();
            }, 5000);

        }
    }

    // Map
    if( $('#map').length ){
        var map_location = [ $('#map').data('lat'), $('#map').data('lng')];
        var map = L.map('map').setView(map_location, 16);
        map.scrollWheelZoom.disable();
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        L.marker(map_location).addTo(map);
    }

    // Smooth scroll
    $('a[href*="#"]')
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                }
            }
        });

});
