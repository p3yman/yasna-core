@php($title = $reason->title . ($model->weight > 1 ? " (". pd($model->weight . " x)") : ''))

@include("manage::widgets.grid-text" , [
	'text' => $title,
	'size' => "12" ,
])
