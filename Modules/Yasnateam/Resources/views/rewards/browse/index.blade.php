@extends('manage::layouts.template')

@section('content')

	@include("yasnateam::rewards.browse.toolbar")
	@include("yasnateam::rewards.browse.grid")

@endsection