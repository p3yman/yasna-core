@include('manage::widgets.grid-rowHeader' , [
	"fake" => $reason = $model->reason ,
	'refresh_url' => "manage/rewards/update/$model->hashid",
])

<td class="text-center">
	@include("yasnateam::rewards.browse.icon")
</td>

<td>
	@include("yasnateam::rewards.browse.name")
</td>

<td>
	@include("yasnateam::rewards.browse.badges")
</td>

@include("yasnateam::rewards.browse.actions")

