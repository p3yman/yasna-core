@include("manage::widgets.toolbar" , [
	'buttons' => [
            [
                'target' => 'modal:manage/rewards/create',
                'type' => "primary",
                'caption' => trans('manage::forms.button.add'),
                "class" => "btn-taha" ,
                'icon' => "plus-circle",
                "condition" => user()->as('admin')->can('rewards.create') ,
            ],
	] ,
	'title' => trans_safe("yasnateam::rewards.title")
])
