@include("manage::widgets.grid-actionCol" , [
	'button_size' => "sm" ,
	"actions" => [
		['pencil' , trans_safe("manage::forms.button.edit") , "modal:manage/rewards/edit/-hashid-" , $model->canEdit()],
		['trash' , trans_safe("manage::forms.button.delete") , "modal:manage/rewards/act/-hashid-/delete" , $model->canDelete()],
	]
])