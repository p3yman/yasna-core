@include("manage::widgets.grid-date" , [
	"text" => trans_safe("manage::forms.general.by") . " " . $model->creator->full_name ,
	'date' => $model->effected_at,
])
