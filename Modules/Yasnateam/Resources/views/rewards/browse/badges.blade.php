@include("manage::widgets.grid-badge" , [
	"text" => trans_safe("yasnateam::rewards.forgivable") ,
	"class" => "text-success" ,
	"link" => $model->canForgive()? "modal:manage/rewards/act/-hashid-/forgive" : "NO" ,
	"condition" => $model->isForgivable() ,
])

@include("manage::widgets.grid-badge" , [
	"text" => trans_safe("yasnateam::rewards.forgiven") ,
	"color" => "success" ,
	"condition" => $model->isForgiven() ,
])