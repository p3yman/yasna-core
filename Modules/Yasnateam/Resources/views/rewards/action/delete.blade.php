@include("manage::layouts.modal-delete" , [
	'form_url' => route('rewards-delete'),
	"fake" => $action = 'delete' ,
	'modal_title' => trans( $action == 'delete' ? "manage::forms.button.soft_delete" : "manage::forms.button.undelete" ) ,
	'title_value' => $model->title ,
	'title_label' => trans("validation.attributes.subject") ,
	'save_label' => trans ( $action == 'delete' ? "manage::forms.button.soft_delete" : "manage::forms.button.undelete" ),
	'save_value' => $action ,
	'save_shape' => $action == 'delete' ? 'danger' : 'primary' ,
]     )