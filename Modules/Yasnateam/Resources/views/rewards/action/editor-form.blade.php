{!!
widget("hidden")
	->name('id')
	->value($model->hashid)
!!}

{!!
widget("persian-date-picker")
	->name('effected_at')
	->label("tr:validation.attributes.date")
	->value($model->effected_at)
	->inForm()
	->required()
!!}


{!!
widget("combo")
	->name('user_id')
	->label("tr:validation.attributes.name_first")
	->searchable()
	->options(role('manager')->users()->get())
	->value($model->user_id)
	->captionField('full_name')
	->inForm()
	->required()
	->blankValue('')
!!}

{!!
widget("combo")
	->name('reason_id')
	->searchable()
	->options(model('RewardReason')->orderBy('group')->get())
	->value($model->reason_id)
	->inForm()
	->required()
	->blankValue('')
	->label("tr:validation.attributes.subject")
!!}

{!!
widget("text")
	->name('weight')
	->value($model->weight)
	->inForm()
	->required()
	->label("tr:yasnateam::rewards.weight")
!!}

{!!
widget("textarea")
	->name('description')
	->value($model->description)
	->inForm()
!!}
