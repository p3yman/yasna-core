@include("manage::layouts.modal-delete" , [
	'form_url' => route('rewards-forgive'),
	'modal_title' => trans_safe("yasnateam::rewards.forgive") ,
	'title_value' => $model->title ,
	'title_label' => trans("validation.attributes.subject") ,
	'save_label' => trans_safe("yasnateam::rewards.forgive"),
	'save_shape' => 'success' ,
]     )