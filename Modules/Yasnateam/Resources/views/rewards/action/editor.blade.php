{!!
widget("modal")
	->target("name:rewards-save")
	->validation(false)
	->label("tr:yasnateam::rewards." . ($model->exists? "edit" : "create"))
!!}

<div class='modal-body'>
	@include("yasnateam::rewards.action.editor-form")
</div>

<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>