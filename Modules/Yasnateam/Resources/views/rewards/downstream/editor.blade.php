{!!
widget("modal")
	->target("name:downstream-rewards-save")
	->validation(false)
	->label("tr:yasnateam::rewards." . ($model->exists? "edit" : "create"))
!!}

<div class='modal-body' style="min-height: 200px">
    @include("yasnateam::rewards.downstream.editor-form")
</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>