@include("manage::widgets.grid" , [
    'headings'          => [
        '',
        trans_safe("validation.attributes.properties")
    ],
    'row_view'          => "yasnateam::rewards.downstream.row",
    'table_id'          => 'tblRewards',
    'handle'            => 'counter',
    'operation_heading' => true,
])