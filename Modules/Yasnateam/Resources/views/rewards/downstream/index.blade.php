@extends('manage::layouts.template')

@section('content')
    @include("manage::downstream.tabs")

    @include("yasnateam::rewards.downstream.toolbar")
    @include("yasnateam::rewards.downstream.grid")
@endsection