@include("manage::widgets.toolbar" , [
    'title'             => trans_safe("yasnateam::rewards.title"),
    'buttons'           => [
        [
	    'target'  => 'modal:manage/rewards/downstream/create',
	    'type'    => 'primary',
	    'caption' => trans_safe("yasnateam::rewards.create"),
	    'icon'    => 'plus',
	    "class" => "btn-taha" ,
        ],
    ],
])