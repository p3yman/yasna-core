{!!
widget("hidden")
	->name('id')
	->value($model->hashid)
!!}

{!!
widget("combo")
	->name('group')
	->options(rewardReason()->groupCombo())
	->value($model->group)
	->inForm()
	->required()
	->blankValue('')
	->label("tr:validation.attributes.subject")
!!}

{!!
widget("text")
	->name('text')
	->value($model->text)
	->inForm()
	->required()
!!}