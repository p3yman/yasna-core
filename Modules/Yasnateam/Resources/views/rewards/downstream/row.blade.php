@include('manage::widgets.grid-row-handle', [
    'refresh_url' => url("manage/rewards/downstream/update/$model->hashid"),
    'handle'      => 'counter',
])

<td>
    @include("yasnateam::rewards.browse.icon" , [
        "reason" => $model ,
    ])
</td>


<td>
    @include("manage::widgets.grid-text" , [
         'text' => $model->title,
         'size' => "16" ,
         'class' => "font-yekan text-bold mv10" ,
    ])
</td>


@include("manage::widgets.grid-actionCol" , [
    "actions" => [
        ['pencil', trans_safe("manage::forms.button.edit"), 'modal:manage/rewards/downstream/edit/-hashid-'],
    ],
])
