<div class="row" style="margin-top: 15px">

	<div class="col-lg-6">
		{!!
			widget('persian-date-picker')
				->name('project_date')
				->label($__module->getTrans('customers.project-date'))
				->inForm()
				->value($model->project_date ?? '')
				->onlyDatePicker()
		!!}
	</div>

	<div class="col-lg-6">
		{!!
			widget('text')
				->name('project_version')
				->label($__module->getTrans('customers.project-version'))
				->inForm()
				->value($model->project_version ?? '')
		!!}
	</div>

</div>

