<div class="container">

    <div class="call-to-action {{ isset($color) ? $color : '' }}" data-aos="fade-up">

        <div class="right" data-aos="slide-right">
            <h1 >{{ $title }}</h1>
            <a href="{{ $link }}" class="button lg w-arrow">{{ trans('yasnateam::general.read_more') }}<i class="icon-arrow-left"></i></a>
        </div>

        <div class="left">
            <div class="icons" data-aos="zoom-in" data-aos-delay="200">
                <div class="icon green xlg"><img src="images/icons/puzzles.svg"></div>
                <div class="icon blue xlg"><img src="images/icons/rocket.svg"></div>
                <div class="icon red xlg"><img src="images/icons/shield.svg"></div>
            </div>
        </div>

    </div>

</div>