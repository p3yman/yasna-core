<div class="item" data-aos="fade-up">
    <div class="img">
        <div class="icon {{ $color }} lg">
            @if( isset($icon) )
                <img src="{{ Module::asset('yasnateam:images/icons/'.$icon.'.svg') }}">
            @else
                <span>{{ $number }}</span>
            @endif
        </div>


    </div>
    <div class="content">
        <div class="part-title secondary">{{ $title }}</div>
        <p>{{ $content }}</p>
    </div>
</div>