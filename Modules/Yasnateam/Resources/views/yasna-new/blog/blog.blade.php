@extends('yasnateam::yasna-new.layouts.main')

@php

	$logo_hashid = get_setting('site_logo');
		$site_logo = fileManager()
						->file($logo_hashid)
						->getUrl();

@endphp

@section('header-links')
    <meta property="og:title" content="{{ Template::implodePageTitle(' :: ') }}"/>
    <meta property="og:url" content="{{ route('blog') }}" />
    <meta property="og:image" content="{{ $site_logo }}"/>
    <meta property="og:description" content="{{ trans('yasnateam::general.page_description.blog') }}"/>
@endsection

@section('inner-body')

    <div class="container">

        <div class="paper-wrapper">

            <div class="paper">

                @if( isset($category) && $category->exists )

                    <div class="sub-part-title" data-aos="zoom-in-up"><a href="{{ route('blog') }}">{{ trans('yasnateam::menu.main.blog') }}</a></div>
                    <div class="part-title" data-aos="zoom-in-up">{{ $category->titleIn('fa') }}</div>

                @elseif( isset($tag) && $tag->exists )

                    <div class="sub-part-title" data-aos="zoom-in-up"><a href="{{ route('blog') }}">{{ trans('yasnateam::menu.main.blog') }}</a></div>
                    <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::general.tag') }}: {{ $tag->slug }}</div>

                @else

                    <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::menu.main.blog') }}</div>

                @endif

                <div class="row">

                    <div class="col-sm-10 col-center">

                        @foreach( $posts as $post )

                            @php( $class = $loop->index % 2 == 0 ? '' : 'even' )

                            @include('yasnateam::yasna-new.blog.blog-item', ['class'=>$class])

                        @endforeach

                        {{-- Pagination --}}
                        <div class="pagination-wrapper">
                            {!! $posts->links() !!}
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@stop
