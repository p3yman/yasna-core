<div class="blog-item {{ $class }}" data-aos="zoom-in-up">

    <img src="{{ postImage($post->featured_image)->posttype( posttype('blog') )->config('featured_image')->version('300x225')->getUrl() }}">

    <div class="content">

        @php( $category = model('category', $post->default_category) )
        @if( $category->exists )
            <a href="{{ route('blog-category', $category->slug) }}" class="h3 d-ib mb10">
                {{ $category->titleIn(getLocale()) }}
            </a>
        @endif

        <h1>{{ $post->title }}</h1>

        <p>{{ $post->abstract }}</p>

        <a href="{{ route('blog-single', ['category'=>$category->slug, 'id'=>$post->id, 'slug'=>$post->slug]) }}" class="button lg w-arrow">
            {{ trans('yasnateam::general.read_more') }}
            <i class="icon-arrow-left"></i>
        </a>

    </div>
</div>