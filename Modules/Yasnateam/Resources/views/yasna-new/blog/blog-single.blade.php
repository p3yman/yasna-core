@extends('yasnateam::yasna-new.layouts.main')

@section('header-links')
    <meta property="og:title" content="{{ $post->title }}"/>
    <meta property="og:url" content="{{ route('blog-single', ['category'=>$category->slug, 'id'=>$post->id, 'slug'=>$post->slug]) }}" />
    <meta property="og:image" content="{{ postImage($post->featured_image)->posttype( posttype('blog') )->config('featured_image')->version('cover')->getUrl() }}" />
    <meta property="og:description" content="{{ $post->abstract }}"/>
@endsection

@section('inner-body')

    <div class="container">

        <div class="paper-wrapper">

            <div class="paper">

                <div class="single-page" data-aos="zoom-in-up">

                    @if( $post->featured_image )
                        <div class="page-cover" data-aos="zoom-in-up">
                            <img src="{{ postImage($post->featured_image)->posttype( posttype('blog') )->config('featured_image')->version('cover')->getUrl() }}">
                        </div>
                    @endif

                    <div class="part-title" data-aos="zoom-in-up">
                        
                        {{-- Post title --}}
                        {{ $post->title }}

                        <div class="page-meta">

                            <div class="item">{{ pd(jDate::forge($post->published_at)->format('j F Y')) }}</div>

                            <div class="item">{{ array_get($post, 'meta.author') }}</div>

                            @if( $category->exists )
                                <div class="item"><a href="{{ route('blog-category', $category->slug) }}">{{ $category->titleIn('fa') }}</a></div>
                            @endif

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-8 col-center">

                            <div class="page-content" data-aos="zoom-in-up">

                                {!! $post->text !!}

                            </div>

                            <div class="page-footer" data-aos="zoom-in-up">

                                @if( $tags->count() )
                                    <div class="tags">
                                        @foreach( $tags as $tag )
                                            <a href="{{ route('blog-tag', $tag->slug) }}">{{ $tag->slug }}</a>
                                        @endforeach
                                    </div>
                                @endif

                                <div class="socials">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('blog-single', ['id'=>$post->id, 'slug'=>$post->slug]) }}" class="icon-fb"></a>
                                    <a href="https://twitter.com/home?status={{ route('blog-single', ['id'=>$post->id, 'slug'=>$post->slug]) }}" class="icon-tw"></a>
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('blog-single', ['id'=>$post->id, 'slug'=>$post->slug]) }}&title={{ $post->title }}&summary={{ $post->abstract }}" class="icon-li"></a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@stop
