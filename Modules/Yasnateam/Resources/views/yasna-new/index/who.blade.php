<section id="who">

    <div class="container">

        <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::menu.main.who_we_are') }}</div>

        <div id="team">
            <div class="row">
                <div class="col-sm-6">
                    <div class="pl20">
                        <img src="{{ Module::asset('yasnateam:images/team/team-norooz-98.jpg') }}" class="responsive" data-aos="fade">
                    </div>
                </div>
                <div class="col-sm-6">
                    <h2 class="title" data-aos="fade-right">{{ trans('yasnateam::general.yasna_team') }}</h2>
                    <div class="content" data-aos="fade-right" data-aos-delay="100">
                        {!! trans('yasnateam::general.team_intro') !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="part-title secondary" data-aos="zoom-in-right">{{ trans('yasnateam::general.team.stats') }}</div>
        <div id="statistics" data-aos="zoom-in-up">
            @foreach( $statistics as $statistic )
                <div class="item">
                    <div class="number">{{ pd($statistic->abstract) }}</div>
                    <div class="title">{{ $statistic->title }}</div>
                    <div class="subtitle">{{ strtoupper($statistic->title2) }}</div>
                </div>
            @endforeach
        </div>

        <div class="part-title secondary" data-aos="zoom-in-right">{{ trans('yasnateam::general.team.what_we_know') }}</div>
        <div class="inner-slick technologies mb50"  data-aos="zoom-in-up">
            @foreach( $technologies as $technology )
                <div class="item technology"><a href="{{ $technology->getMeta('link') }}"><img src="{{ postImage($technology->featured_image)->posttype( posttype('technologies') )->config('featured_image')->version('90')->getUrl() }}" target="_blank"></a></div>
            @endforeach
        </div>

        <div class="part-title secondary" data-aos="zoom-in-right">{{ trans('yasnateam::general.team.members') }}</div>
        <div class="owl-carousel members mb50"  data-aos="zoom-in-up">
            @foreach( $team_members as $team_member )
                <div class="item member" data-target="#member-{{ $team_member->id }}"><img src="{{ postImage($team_member->featured_image)->posttype( posttype('ourteam') )->config('featured_image')->version('326x197')->getUrl() }}"></div>
            @endforeach
        </div>
        <div class="members-info" data-aos="zoom-in-up">
            @foreach( $team_members as $team_member )
                <div class="item" id="member-{{ $team_member->id }}">
                    <div class="row">
                        <div class="col-sm-4">
                            <h2>{{ $team_member->title }}</h2>
                            <div class="position">{{ $team_member->getMeta('post') }}</div>
                            <div class="socials">
                                @if( $team_member->getMeta('personal_website') != '' )
                                    <a href="{{ $team_member->getMeta('personal_website') }}" class="icon-home" target="_blank"></a>
                                @endif
                                @if( $team_member->getMeta('twitter_account') != '' )
                                    <a href="https://twitter.com/{{ $team_member->getMeta('twitter_account') }}" class="icon-tw" target="_blank"></a>
                                @endif
                                @if( $team_member->getMeta('linkedin_account') != '' )
                                    <a href="https://www.linkedin.com/in/{{ $team_member->getMeta('linkedin_account') }}" class="icon-li" target="_blank"></a>
                                @endif
                                @if( $team_member->getMeta('github_account') != '' )
                                    <a href="https://github.com/{{ $team_member->getMeta('github_account') }}" class="icon-gh" target="_blank"></a>
                                @endif
                                @if( $team_member->getMeta('facebook_account') != '' )
                                    <a href="https://www.facebook.com/{{ $team_member->getMeta('facebook_account') }}" class="icon-fb" target="_blank"></a>
                                @endif
                                @if( $team_member->getMeta('telegram_account') != '' )
                                    <a href="https://t.me/{{ $team_member->getMeta('telegram_account') }}" class="icon-tl" target="_blank"></a>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-8">
                            {!! $team_member->text !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


    </div>

</section>
