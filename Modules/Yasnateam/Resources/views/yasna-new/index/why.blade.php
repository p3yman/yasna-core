<section id="why">

    <div class="container">

        <div class="part-title" data-aos="zoom-in-up">چرا ما؟</div>

        <div class="row">
            <div class="col-sm-4">
                <div class="why-item" data-aos="flip-up">
                    <div class="icon blue"><img src="{{ Module::asset('yasnateam:images/icons/update.svg') }}"></div>
                    <h3>{{ trans('yasnateam::general.why_us.update.title') }}</h3>
                    <p>{{ trans('yasnateam::general.why_us.update.text') }}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="why-item" data-aos="flip-up" data-aos-delay="100">
                    <div class="icon red"><img src="{{ Module::asset('yasnateam:images/icons/shield.svg') }}"></div>
                    <h3>{{ trans('yasnateam::general.why_us.security.title') }}</h3>
                    <p>{{ trans('yasnateam::general.why_us.security.text') }}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="why-item" data-aos="flip-up" data-aos-delay="150">
                    <div class="icon green"><img src="{{ Module::asset('yasnateam:images/icons/puzzles.svg') }}"></div>
                    <h3>{{ trans('yasnateam::general.why_us.perfect.title') }}</h3>
                    <p>{{ trans('yasnateam::general.why_us.perfect.text') }}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="why-item" data-aos="flip-up" data-aos-delay="200">
                    <div class="icon purple"><img src="{{ Module::asset('yasnateam:images/icons/key.svg') }}"></div>
                    <h3>{{ trans('yasnateam::general.why_us.analyze.title') }}</h3>
                    <p>{{ trans('yasnateam::general.why_us.analyze.text') }}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="why-item" data-aos="flip-up" data-aos-delay="250">
                    <div class="icon teal"><img src="{{ Module::asset('yasnateam:images/icons/support.svg') }}"></div>
                    <h3>{{ trans('yasnateam::general.why_us.support.title') }}</h3>
                    <p>{{ trans('yasnateam::general.why_us.support.text') }}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="why-item" data-aos="flip-up" data-aos-delay="300">
                    <div class="icon orange"><img src="{{ Module::asset('yasnateam:images/icons/documents.svg') }}"></div>
                    <h3>{{ trans('yasnateam::general.why_us.documents.title') }}</h3>
                    <p>{{ trans('yasnateam::general.why_us.documents.text') }}</p>
                </div>
            </div>
        </div>

    </div>

</section>