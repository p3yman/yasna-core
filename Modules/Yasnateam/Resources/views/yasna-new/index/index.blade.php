@extends('yasnateam::yasna-new.layouts.main')

@php
	$logo_hashid = get_setting('site_logo');
		$site_logo = fileManager()
						->file($logo_hashid)
						->getUrl();

@endphp

@section('header-links')
    <meta property="og:title" content="{{ Template::implodePageTitle(' :: ') }}"/>
    <meta property="og:url" content="{{ route('home') }}" />
    <meta property="og:image" content="{{ $site_logo }}"/>
    <meta property="og:description" content="{{ trans('yasnateam::general.page_description.home') }}"/>
@endsection

@section('header-big-title')

    <div class="row">

        <div class="col-sm-6 mt30">
            <div class="home-title">
                <h3 data-aos="fade-up">{!! trans('yasnateam::yasnaweb.home-title.subtitle') !!}</h3>
                <h1 data-aos="fade-up">{!! trans('yasnateam::yasnaweb.home-title.title') !!}</h1>
                <span  data-aos="fade-up">
                    <a href="{{ route('yasnaweb') }}" class="button lg w-arrow">
                        {{ trans('yasnateam::yasnaweb.home-title.button') }}<i class="icon-arrow-left"></i>
                    </a>
                </span>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="ta-l">
                <img src="{{ Module::asset('yasnateam:images/home-head-img.png') }}" width="265" class="responsive" data-aos="fade-up">
            </div>
        </div>

    </div>

@endsection

@section('inner-body')

    {{-- What we do --}}
    @include('yasnateam::yasna-new.index.what')

    {{-- Yasnaweb 3 banner --}}
    @include('yasnateam::yasna-new.index.yasnaweb-banner')

    {{-- What we've done --}}
    @include('yasnateam::yasna-new.index.projects')

    {{-- Why us --}}
    @include('yasnateam::yasna-new.index.why')

    {{-- Who we are --}}
    @include('yasnateam::yasna-new.index.who')

    {{-- Where --}}
    @include('yasnateam::yasna-new.index.where')

@stop
