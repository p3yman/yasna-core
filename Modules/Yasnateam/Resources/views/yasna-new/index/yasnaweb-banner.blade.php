<section id="yasnaweb-banner">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">
                <img src="{{ Module::asset('yasnateam:images/yasnaweb-mockup-banner.png') }}" data-aos="fade-down" data-aos-duration="800">
            </div>

            <div class="col-sm-6">
                <div class="subtitle" data-aos="fade-right">{{ trans('yasnateam::general.yasnaweb_banner.subtitle') }}</div>
                <div class="title" data-aos="fade-right" data-aos-delay="100">{{ trans('yasnateam::general.yasnaweb_banner.title') }}</div>

                <span data-aos="fade-right" data-aos-delay="150">
                    <a href="{{ route('yasnaweb') }}" class="button lg w-arrow animated-arrow">
                        {{ trans('yasnateam::general.yasnaweb_banner.button_text') }}
                        <i class="icon-arrow-left"></i>
                    </a>
                </span>
            </div>

        </div>

    </div>

</section>