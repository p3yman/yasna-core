<section id="where">

    <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::menu.main.where_we_are') }}</div>

    <div class="container">

        <div id="map" data-lat="{{ get_setting('location')[0] }}" data-lng="{{ get_setting('location')[1] }}"></div>

        <div class="contact-info">
            <div class="row">
                <div class="col-sm-4" data-aos="fade-up">
                    <span class="icon gray xs"><img src="{{ Module::asset('yasnateam:images/icons/map.svg') }}"></span>
                    <div class="text">{{ get_setting('address') }}</div>
                </div>
                <div class="col-sm-4" data-aos="fade-up" data-aos-delay="100">
                    <span class="icon gray xs"><img src="{{ Module::asset('yasnateam:images/icons/phone.svg') }}"></span>
                    <div class="text phone">{{ pd( implode(' - ', get_setting('telephone')) ) }}</div>
                </div>
                <div class="col-sm-4" data-aos="fade-up" data-aos-delay="150">
                    <span class="icon gray xs"><img src="{{ Module::asset('yasnateam:images/icons/mail.svg') }}"></span>
                    <div class="text email"><a href="mailto:{{ array_first(get_setting('email')) }}">{{ array_first(get_setting('email')) }}</a></div>
                </div>
            </div>
        </div>

    </div>

</section>
