<div class="container">

    <div class="paper-wrapper">

        <div class="paper">

            <section id="what">

                <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::menu.main.what_we_do') }}</div>

                <div class="what-items">

                    <div class="what-row">

                        <div class="item" data-aos="flip-up">

                            <img src="{{ Module::asset('yasnateam:images/what-website.svg') }}" height="96">

                            <h3>{{ trans('yasnateam::general.services.website') }}</h3>

                            <a href="{{ route('services', 'website') }}" class="button w-arrow animated-arrow">
                                {{ trans('yasnateam::general.know_more') }}
                                <i class="icon-arrow-left"></i>
                            </a>

                        </div>

                        <div class="item" data-aos="flip-up" data-aos-delay="100">

                            <img src="{{ Module::asset('yasnateam:images/what-mobile.svg') }}" height="96">

                            <h3>{{ trans('yasnateam::general.services.mobile') }}</h3>

                            <a href="{{ route('services', 'mobile') }}" class="button w-arrow animated-arrow">
                                {{ trans('yasnateam::general.know_more') }}
                                <i class="icon-arrow-left"></i>
                            </a>
                        </div>

                        <div class="item" data-aos="flip-up" data-aos-delay="150">

                            <img src="{{ Module::asset('yasnateam:images/what-e-commerce.svg') }}" height="96">

                            <h3>{{ trans('yasnateam::general.services.e-commerce') }}</h3>

                            <a href="{{ route('services', 'e-commerce') }}" class="button w-arrow animated-arrow">
                                {{ trans('yasnateam::general.know_more') }}
                                <i class="icon-arrow-left"></i>
                            </a>
                        </div>

                    </div>

                    <div class="what-row">

                        <div class="item" data-aos="flip-up" data-aos-delay="200">

                            <img src="{{ Module::asset('yasnateam:images/what-seo.svg') }}" height="96">

                            <h3>{{ trans('yasnateam::general.services.seo') }}</h3>

                            <a href="{{ route('services', 'seo') }}" class="button w-arrow animated-arrow">
                                {{ trans('yasnateam::general.know_more') }}
                                <i class="icon-arrow-left"></i>
                            </a>
                        </div>

                        <div class="item" data-aos="flip-up" data-aos-delay="250">

                            <img src="{{ Module::asset('yasnateam:images/what-ui-ux.svg') }}" height="96">

                            <h3>{{ trans('yasnateam::general.services.ui-ux') }}</h3>

                            <a href="{{ route('services', 'ui-ux') }}" class="button w-arrow animated-arrow">
                                {{ trans('yasnateam::general.know_more') }}
                                <i class="icon-arrow-left"></i>
                            </a>
                        </div>

                        <div class="item" data-aos="flip-up" data-aos-delay="300">

                            <img src="{{ Module::asset('yasnateam:images/what-digital-marketing.svg') }}" height="96">

                            <h3>{{ trans('yasnateam::general.services.digital-marketing') }}</h3>

                            <a href="{{ route('services', 'digital-marketing') }}" class="button w-arrow animated-arrow">
                                {{ trans('yasnateam::general.know_more') }}
                                <i class="icon-arrow-left"></i>
                            </a>
                        </div>

                    </div>

                </div>

            </section>

        </div>

    </div>

</div>