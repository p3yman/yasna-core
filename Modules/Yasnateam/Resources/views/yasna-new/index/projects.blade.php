<section id="projects">

    <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::menu.main.what_we_done') }}</div>

    <div class="container">

        <div class="inner-slick clients mb50" data-aos="zoom-in-up">

            @foreach( $customers as $customer )

                <div class="item client simptip-position-bottom simptip-smooth simptip-fade" data-tooltip="{{ $customer->title }}">
                    <a href="{{ $customer->getMeta('link') }}" target="_blank">
                        <img src="{{ postImage($customer->featured_image)->posttype( posttype('customers') )->config('featured_image')->version('120')->getUrl() }}">
                    </a>
                </div>

            @endforeach

        </div>

    </div>

</section>