@extends('yasnateam::yasna-new.layouts.main')

@section('header-big-title')
    @include("yasnateam::yasna-new.services.$service.header-big-title")
@stop

@section('inner-body')

    @include("yasnateam::yasna-new.services.$service.$service")

@stop