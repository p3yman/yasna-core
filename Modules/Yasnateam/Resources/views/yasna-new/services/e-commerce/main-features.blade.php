<div class="container">

    <div class="paper-wrapper">

        <div class="paper">

            <div class="part-title" data-aos="zoom-in-up"><div class="title">{{ trans('yasnateam::service-ecommerce.features.title') }}</div></div>

            <div class="features-small">

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ecommerce.features.easy.title'),
                    'icon'      => 'mobile-easy',
                    'color'     => 'green',
                    'content'   => trans('yasnateam::service-ecommerce.features.easy.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ecommerce.features.seo.title'),
                    'icon'      => 'seo-in',
                    'color'     => 'blue',
                    'content'   => trans('yasnateam::service-ecommerce.features.seo.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ecommerce.features.extend.title'),
                    'icon'      => 'development',
                    'color'     => 'red',
                    'content'   => trans('yasnateam::service-ecommerce.features.extend.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ecommerce.features.payment.title'),
                    'icon'      => 'mobile-payment',
                    'color'     => 'orange',
                    'content'   => trans('yasnateam::service-ecommerce.features.payment.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ecommerce.features.dashboard.title'),
                    'icon'      => 'dashboard',
                    'color'     => 'purple',
                    'content'   => trans('yasnateam::service-ecommerce.features.dashboard.content')
                ])

            </div>

        </div>

    </div>

</div>