{{-- Main features --}}
@include("yasnateam::yasna-new.services.e-commerce.main-features")

{{-- Inner banner --}}
@include("yasnateam::yasna-new.services.e-commerce.inner-banner")

{{-- Steps --}}
@include("yasnateam::yasna-new.services.e-commerce.steps")