<div class="container">

    <div class="paper-wrapper">

        <div class="paper">

            <div class="part-title" data-aos="zoom-in-up"><div class="title">{{ trans("yasnateam::service-seo.ways.title") }}</div></div>

            <div class="features-small">

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-seo.ways.seo-on.title'),
                    'icon'      => 'seo-in',
                    'color'     => 'blue',
                    'content'   => trans('yasnateam::service-seo.ways.seo-on.content')
                ])

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-seo.ways.seo-off.title'),
                    'icon'      => 'seo-out',
                    'color'     => 'green',
                    'content'   => trans('yasnateam::service-seo.ways.seo-off.content')
                ])

                <div class="part-title secondary" data-aos="zoom-in-up"><div class="title">{{ trans('yasnateam::service-seo.ways.seo-url.title') }}</div></div>
                {!! trans('yasnateam::service-seo.ways.seo-url.content') !!}

            </div>

        </div>

    </div>

</div>