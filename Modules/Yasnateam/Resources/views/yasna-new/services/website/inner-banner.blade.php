<div class="inner-banner">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">

                <div class="img">
                    <img src="{{ Module::asset('yasnateam:images/samples/server.png') }}" data-aos="zoom-in-left" data-aos-duration="1000">
                </div>

            </div>

            <div class="col-sm-6">

                <div class="part-title" data-aos="fade-left" data-aos-easing="ease-in-quart">{{ trans('yasnateam::service-website.banner.title') }}</div>

                <p data-aos="fade-left" data-aos-delay="150">{{ trans('yasnateam::service-website.banner.content') }}</p>

            </div>

        </div>

    </div>

</div>