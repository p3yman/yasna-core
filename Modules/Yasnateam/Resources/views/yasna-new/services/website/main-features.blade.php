<div class="container">

    <div class="paper-wrapper">

        <div class="paper">

            <div class="part-title" data-aos="zoom-in-up"><div class="title">{{ trans('yasnateam::service-website.features.title') }}</div></div>

            <div class="features-small">
                
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-website.features.security.title'),
                    'icon'      => 'shield',
                    'color'     => 'green',
                    'content'   => trans('yasnateam::service-website.features.security.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-website.features.ui.title'),
                    'icon'      => 'sitemap',
                    'color'     => 'teal',
                    'content'   => trans('yasnateam::service-website.features.ui.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-website.features.dashboard.title'),
                    'icon'      => 'dashboard',
                    'color'     => 'red',
                    'content'   => trans('yasnateam::service-website.features.dashboard.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-website.features.seo.title'),
                    'icon'      => 'search',
                    'color'     => 'blue',
                    'content'   => trans('yasnateam::service-website.features.seo.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-website.features.extend.title'),
                    'icon'      => 'development',
                    'color'     => 'purple',
                    'content'   => trans('yasnateam::service-website.features.extend.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-website.features.competitors.title'),
                    'icon'      => 'search-users',
                    'color'     => 'orange',
                    'content'   => trans('yasnateam::service-website.features.competitors.content')
                ])

            </div>

        </div>

    </div>

</div>