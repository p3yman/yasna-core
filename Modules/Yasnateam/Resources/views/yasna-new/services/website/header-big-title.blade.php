<h1 data-aos="fade-up" class="">{{ trans('yasnateam::service-website.big-title.title') }}</h1>
<div class="row">
    <div class="col-sm-8 col-center">
        <div class="text " data-aos="fade-up" data-aos-delay="150">
            {!! trans('yasnateam::service-website.big-title.content') !!}
        </div>
    </div>
</div>