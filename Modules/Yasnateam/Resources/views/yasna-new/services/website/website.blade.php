{{-- Main features --}}
@include("yasnateam::yasna-new.services.website.main-features")

{{-- Big features --}}
@include("yasnateam::yasna-new.services.website.big-features")

{{-- Inner banner --}}
@include("yasnateam::yasna-new.services.website.inner-banner")

{{-- Steps --}}
@include("yasnateam::yasna-new.services.website.steps")