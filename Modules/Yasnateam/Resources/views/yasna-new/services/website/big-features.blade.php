<div class="features-big">

    <div class="item">
        <div class="container">
            <div class="content">
                <h1 data-aos="fade-right">{{ trans('yasnateam::service-website.features.modular.title') }}</h1>
                <p data-aos="fade-right" data-aos-delay="150">{{ trans('yasnateam::service-website.features.modular.content') }}</p>
            </div>
            <div class="img">
                <img src="{{ Module::asset('yasnateam:images/samples/feature-big-2.png') }}" data-aos="flip-left" data-aos-delay="250">
            </div>
        </div>
    </div>

    <div class="item">
        <div class="container">
            <div class="img">
                <img src="{{ Module::asset('yasnateam:images/landings/shopping.png') }}" data-aos="flip-right" data-aos-delay="250">
            </div>
            <div class="content">
                <h1 data-aos="fade-left">{{ trans('yasnateam::service-website.features.e-commerce.title') }}</h1>
                <p data-aos="fade-left" data-aos-delay="150">{{ trans('yasnateam::service-website.features.e-commerce.content') }}</p>
            </div>
        </div>
    </div>

</div>
