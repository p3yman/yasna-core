<div class="features-big">

    <div class="item">
        <div class="container">
            <div class="content">
                <h1 data-aos="fade-right">{{ trans('yasnateam::service-ui.features.importance.title') }}</h1>
                {!! trans('yasnateam::service-ui.features.importance.content') !!}
            </div>
            <div class="img">
                <img src="{{ Module::asset('yasnateam:images/landings/design.png') }}" data-aos="flip-left" data-aos-delay="250">
            </div>
        </div>
    </div>

</div>
