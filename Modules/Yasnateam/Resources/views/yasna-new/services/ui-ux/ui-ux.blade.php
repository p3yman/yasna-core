{{-- Main features --}}
@include("yasnateam::yasna-new.services.ui-ux.main-features")

{{-- Big features --}}
@include("yasnateam::yasna-new.services.ui-ux.big-features")

{{-- Steps --}}
@include("yasnateam::yasna-new.services.ui-ux.steps")