<div class="container">

    <div class="paper-wrapper">

        <div class="paper">

            <div class="part-title" data-aos="zoom-in-up"><div class="title">{{ trans("yasnateam::service-ui.features.title") }}</div></div>

            <div class="features-small">

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ui.features.responsive.title'),
                    'icon'      => 'mobile-website',
                    'color'     => 'green',
                    'content'   => trans('yasnateam::service-ui.features.responsive.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ui.features.usability.title'),
                    'icon'      => 'user-check',
                    'color'     => 'blue',
                    'content'   => trans('yasnateam::service-ui.features.usability.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-ui.features.colors.title'),
                    'icon'      => 'colors',
                    'color'     => 'red',
                    'content'   => trans('yasnateam::service-ui.features.colors.content')
                ])

            </div>

        </div>

    </div>

</div>