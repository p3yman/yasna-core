<div class="features-steps">

    <div class="container">

        <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::service-ui.steps.title') }}</div>
        <p>{{ trans('yasnateam::service-ui.steps.content') }}</p>

        <div class="steps-wrapper tabs">

            <div class="titles">
                <a href="#" class="step-title active" data-aos="zoom-in-up">
                    <div class="icon gray xxs"><span>{{ ad( 1 ) }}</span></div>
                    <h3>{{ trans('yasnateam::service-ui.steps.1.title') }}</h3>
                </a>
                <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="">
                    <div class="icon red xxs"><span>{{ ad( 2 ) }}</span></div>
                    <h3>{{ trans('yasnateam::service-ui.steps.2.title') }}</h3>
                </a>
                <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="">
                    <div class="icon orange xxs"><span>{{ ad( 3 ) }}</span></div>
                    <h3>{{ trans('yasnateam::service-ui.steps.3.title') }}</h3>
                </a>
                <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="">
                    <div class="icon yellow xxs"><span>{{ ad( 4 ) }}</span></div>
                    <h3>{{ trans('yasnateam::service-ui.steps.4.title') }}</h3>
                </a>
                <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="">
                    <div class="icon teal xxs"><span>{{ ad( 5 ) }}</span></div>
                    <h3>{{ trans('yasnateam::service-ui.steps.5.title') }}</h3>
                </a>
            </div>

            <div class="contents">
                <div class="content">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/landings/step-0.png') }}" width="200">
                    </div>
                    <div class="text">
                        <p>{{ trans('yasnateam::service-ui.steps.1.content') }}</p>
                    </div>
                </div>
                <div class="content">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/step-1.png') }}" width="200">
                    </div>
                    <div class="text">
                        <p>{{ trans('yasnateam::service-ui.steps.2.content') }}</p>
                    </div>
                </div>
                <div class="content">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/step-5.png') }}" width="200">
                    </div>
                    <div class="text">
                        <p>{{ trans('yasnateam::service-ui.steps.3.content') }}</p>
                    </div>
                </div>
                <div class="content">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/step-2.png') }}" width="200">
                    </div>
                    <div class="text">
                        <p>{{ trans('yasnateam::service-ui.steps.4.content') }}</p>
                    </div>
                </div>
                <div class="content">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/step-6.png') }}" width="200">
                    </div>
                    <div class="text">
                        <p>{{ trans('yasnateam::service-ui.steps.5.content') }}</p>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>