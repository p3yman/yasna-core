<div class="container">

    <div class="paper-wrapper">

        <div class="paper">

            <div class="part-title" data-aos="zoom-in-up"><div class="title">{{ trans('yasnateam::service-mobile.features.title') }}</div></div>

            <div class="features-small">

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-mobile.features.easy.title'),
                    'icon'      => 'mobile-easy',
                    'color'     => 'teal',
                    'content'   => trans('yasnateam::service-mobile.features.easy.content')
                ])

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-mobile.features.connect.title'),
                    'icon'      => 'mobile-website',
                    'color'     => 'red',
                    'content'   => trans('yasnateam::service-mobile.features.connect.content')
                ])

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-mobile.features.extend.title'),
                    'icon'      => 'mobile-development',
                    'color'     => 'blue',
                    'content'   => trans('yasnateam::service-mobile.features.extend.content')
                ])

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-mobile.features.security.title'),
                    'icon'      => 'shield',
                    'color'     => 'green',
                    'content'   => trans('yasnateam::service-mobile.features.security.content')
                ])

            </div>

        </div>

    </div>

</div>