{{-- Main features --}}
@include("yasnateam::yasna-new.services.mobile.main-features")

{{-- Big features --}}
@include("yasnateam::yasna-new.services.mobile.big-features")

{{-- Steps --}}
@include("yasnateam::yasna-new.services.mobile.steps")