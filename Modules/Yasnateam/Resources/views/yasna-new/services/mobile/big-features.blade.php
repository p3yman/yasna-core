<div class="features-big">

    <div class="item">
        <div class="container">
            <div class="content">
                <h1 data-aos="fade-right">{{ trans('yasnateam::service-mobile.features.payment.title') }}</h1>
                {!! trans('yasnateam::service-mobile.features.payment.content') !!}
            </div>
            <div class="img">
                <img src="{{ Module::asset('yasnateam:images/landings/mobile-shopping.png') }}" style="max-width: 440px;" data-aos="flip-left" data-aos-delay="250">
            </div>
        </div>
    </div>

</div>
