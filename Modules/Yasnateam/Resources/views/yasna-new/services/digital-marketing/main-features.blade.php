<div class="container">

    <div class="paper-wrapper">

        <div class="paper">

            <div class="part-title" data-aos="zoom-in-up"><div class="title">{{ trans("yasnateam::service-marketing.steps-title") }}</div></div>

            <div class="features-small">

                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.1.title'),
                    'icon'      => 'analytics',
                    'color'     => 'red',
                    'content'   => trans('yasnateam::service-marketing.steps.1.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.2.title'),
                    'icon'      => 'search-users',
                    'color'     => 'green',
                    'content'   => trans('yasnateam::service-marketing.steps.2.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.3.title'),
                    'icon'      => 'strategies',
                    'color'     => 'blue',
                    'content'   => trans('yasnateam::service-marketing.steps.3.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.4.title'),
                    'icon'      => 'website',
                    'color'     => 'yellow',
                    'content'   => trans('yasnateam::service-marketing.steps.4.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.5.title'),
                    'icon'      => 'typewriter',
                    'color'     => 'orange',
                    'content'   => trans('yasnateam::service-marketing.steps.5.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.6.title'),
                    'icon'      => 'network',
                    'color'     => 'teal',
                    'content'   => trans('yasnateam::service-marketing.steps.6.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.7.title'),
                    'icon'      => 'seo-in',
                    'color'     => 'purple',
                    'content'   => trans('yasnateam::service-marketing.steps.7.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.8.title'),
                    'icon'      => 'promotion',
                    'color'     => 'green',
                    'content'   => trans('yasnateam::service-marketing.steps.8.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.9.title'),
                    'icon'      => 'statistics',
                    'color'     => 'blue',
                    'content'   => trans('yasnateam::service-marketing.steps.9.content')
                ])
                @include('yasnateam::yasna-new.partials.feature-small', [
                    'title'     => trans('yasnateam::service-marketing.steps.10.title'),
                    'icon'      => 'strategy',
                    'color'     => 'red',
                    'content'   => trans('yasnateam::service-marketing.steps.10.content')
                ])

            </div>

        </div>

    </div>

</div>
