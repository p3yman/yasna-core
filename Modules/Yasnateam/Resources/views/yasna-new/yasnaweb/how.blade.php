<div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::yasnaweb.modules.title') }}</div>
<div class="container">

    <div class="row">
        <div class="col-sm-8 col-center" data-aos="fade-up">
            <p class="how-intro">{{ trans('yasnateam::yasnaweb.modules.content') }}</p>
        </div>
    </div>

    <div class="modules">

        @foreach($modules->chunk(4) as $row)
            <div class="row">
                @php( $i = 0 )
                @foreach($row as $module)
                    <div class="col-sm-3">
                        <div class="item" data-aos="flip-left" data-aos-delay="{{ $i*50 }}">
                            <div class="icon {{ $module->getMeta('icon_color') }} lg"><img src="{{ Module::asset('yasnateam:images/modules/'.$module->getMeta('icon').'.svg') }}"></div>
                            <h3>{{ $module->title }}</h3>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach

    </div>

</div>