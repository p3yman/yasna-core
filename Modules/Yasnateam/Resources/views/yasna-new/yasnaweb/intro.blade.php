<div class="paper-wrapper">

    <div class="paper">

        <div class="mockup"><img src="{{ Module::asset('yasnateam:images/yasnaweb-mockup-big.png') }}" class="fluid" style="border-radius: 10px 10px 0 0" data-aos="fade"></div>

        <div class="part-title" data-aos="zoom-in-up">
            {{ trans('yasnateam::yasnaweb.intro.title') }}
        </div>

        <div class="row">
            <div class="col-sm-8 col-center">
                <div class="intro-text">
                    {!! trans('yasnateam::yasnaweb.intro.content') !!}
                </div>
            </div>
        </div>

    </div>

</div>