<div class="container">
    <div class="big-title">
        <h1 data-aos="fade-up">
            <img src="{{ Module::asset('yasnateam:images/yasnaweb-logo.png') }}" width="146" class="ml30">
            {{ trans('yasnateam::yasnaweb.big-title.title') }}
        </h1>
        <div class="row">
            <div class="col-sm-8 col-center">
                <div class="text" data-aos="fade-up" data-aos-delay="150">
                    {!! trans('yasnateam::yasnaweb.big-title.content') !!}
                </div>
            </div>
        </div>
    </div>
</div>