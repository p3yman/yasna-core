<section id="yasnaweb-banner" class="custom-module">

    <div class="container">

        <div class="row">

            <div class="icon light xlg" data-aos="fade-up" data-aos-delay="200"><img src="{{ Module::asset('yasnateam:images/icon-question.svg') }}"></div>

            <div class="col-sm-5">
            </div>

            <div class="col-sm-7">
                <div class="subtitle mb5" data-aos="fade-left">{{ trans('yasnateam::yasnaweb.custom-modules.subtitle') }}</div>
                <div class="title" data-aos="fade-left" data-aos-delay="100">{{ trans('yasnateam::yasnaweb.custom-modules.title') }}</div>

                <a href="{{ route('contact') }}" class="button lg w-arrow" data-aos="fade-left" data-aos-delay="150">{{ trans('yasnateam::yasnaweb.custom-modules.button') }}<span class="icon-arrow-left"></span></a>
            </div>

        </div>

    </div>

</section>