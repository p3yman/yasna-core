<div class="features-small outside">

    <div class="container">

        <div class="item" data-aos="fade-up">
            <div class="img">
                <div class="icon blue lg"><img src="{{ Module::asset('yasnateam:images/icons/network.svg') }}"></div>
            </div>
            <div class="content">
                <div class="part-title secondary">{{ trans('yasnateam::yasnaweb.features.multi-agency.title') }}</div>
                {!! trans('yasnateam::yasnaweb.features.multi-agency.content') !!}
            </div>
        </div>

        <div class="item" data-aos="fade-up">
            <div class="img">
                <div class="icon red lg"><img src="{{ Module::asset('yasnateam:images/icons/multi-site.svg') }}"></div>
            </div>
            <div class="content">
                <div class="part-title secondary">{{ trans('yasnateam::yasnaweb.features.multi-site.title') }}</div>
                {!! trans('yasnateam::yasnaweb.features.multi-site.content') !!}
            </div>
        </div>

    </div>

</div>