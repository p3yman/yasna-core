@extends('yasnateam::yasna-new.layouts.main')

@section('header-big-title')
    @include('yasnateam::yasna-new.yasnaweb.header-big-title')
@stop

@section('inner-body')

    <div class="container">

        {{-- Intro --}}
        @include('yasnateam::yasna-new.yasnaweb.intro')

        {{-- Features --}}
        @include('yasnateam::yasna-new.yasnaweb.features')

    </div>

    {{-- Automatic updates banner --}}
    @include('yasnateam::yasna-new.yasnaweb.updates-banner')

    {{-- Multisite features --}}
    @include('yasnateam::yasna-new.yasnaweb.multisite')

    {{-- Call to action --}}
    @include('yasnateam::yasna-new.yasnaweb.call-to-action')

    {{-- How: introduce modules --}}
    @include('yasnateam::yasna-new.yasnaweb.how')

    {{-- Custom modules --}}
    @include('yasnateam::yasna-new.yasnaweb.custom-module')

@stop