<div class="inner-banner gray">

    <div class="container">

        <div class="row">

            <div class="col-sm-6">

                <div class="img">
                    <img src="{{ Module::asset('yasnateam:images/shield.png') }}" data-aos="zoom-in-left" data-aos-duration="1000">
                </div>

            </div>

            <div class="col-sm-6">

                <div class="part-title" data-aos="fade-left" data-aos-easing="ease-in-quart">{{ trans('yasnateam::yasnaweb.update-banner.title') }}</div>

                <p data-aos="fade-left" data-aos-delay="150">{{ trans('yasnateam::yasnaweb.update-banner.content') }}</p>

            </div>

        </div>

    </div>

</div>