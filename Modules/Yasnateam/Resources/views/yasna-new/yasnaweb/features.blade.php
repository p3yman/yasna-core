<div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::yasnaweb.features.title') }}</div>
<div class="options-wrapper tabs">

    <div class="titles">
        <a href="#" class="step-title active" data-aos="zoom-in-up">
            <div class="icon red"><img src="{{ Module::asset('yasnateam:images/icons/shield.svg') }}"></div>
            <h3>{{ trans('yasnateam::yasnaweb.features.security.title') }}</h3>
        </a>
        <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="100">
            <div class="icon blue"><img src="{{ Module::asset('yasnateam:images/icons/dashboard.svg') }}"></div>
            <h3>{{ trans('yasnateam::yasnaweb.features.dashboard.title') }}</h3>
        </a>
        <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="150">
            <div class="icon green"><img src="{{ Module::asset('yasnateam:images/icons/key-2.svg') }}"></div>
            <h3>{{ trans('yasnateam::yasnaweb.features.roles.title') }}</h3>
        </a>
        <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="200">
            <div class="icon orange"><img src="{{ Module::asset('yasnateam:images/icons/trans.svg') }}"></div>
            <h3>{{ trans('yasnateam::yasnaweb.features.multilingual.title') }}</h3>
        </a>
        <a href="#" class="step-title" data-aos="zoom-in-up" data-aos-delay="250">
            <div class="icon purple"><img src="{{ Module::asset('yasnateam:images/icons/files.svg') }}"></div>
            <h3>{{ trans('yasnateam::yasnaweb.features.filemanager.title') }}</h3>
        </a>
    </div>

    <div class="contents">
        <div class="content">
            <div class="row">
                <div class="col-sm-7">
                    <div class="text">
                        <div class="part-title secondary xs-show mt0">{{ trans('yasnateam::yasnaweb.features.security.title') }}</div>
                        {!! trans('yasnateam::yasnaweb.features.security.content') !!}
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/samples/project-sample.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-sm-7">
                    <div class="text">
                        <div class="part-title secondary xs-show mt0">{{ trans('yasnateam::yasnaweb.features.dashboard.title') }}</div>
                        {!! trans('yasnateam::yasnaweb.features.dashboard.content') !!}
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/samples/project-sample.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-sm-7">
                    <div class="text">
                        <div class="part-title secondary xs-show mt0">{{ trans('yasnateam::yasnaweb.features.roles.title') }}</div>
                        {!! trans('yasnateam::yasnaweb.features.roles.content') !!}
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/samples/project-sample.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-sm-7">
                    <div class="text">
                        <div class="part-title secondary xs-show mt0">{{ trans('yasnateam::yasnaweb.features.multilingual.title') }}</div>
                        {!! trans('yasnateam::yasnaweb.features.multilingual.content') !!}
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/samples/project-sample.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-sm-7">
                    <div class="text">
                        <div class="part-title secondary xs-show mt0">{{ trans('yasnateam::yasnaweb.features.filemanager.title') }}</div>
                        {!! trans('yasnateam::yasnaweb.features.filemanager.content') !!}
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="img">
                        <img src="{{ Module::asset('yasnateam:images/samples/project-sample.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
