<div class="container">

    <div class="call-to-action" data-aos="fade-up">

        <div class="right" data-aos="slide-right">
            <h1 >{{ trans('yasnateam::general.call_to_action.start_project.title') }}</h1>
            <a href="{{ route('contact') }}" class="button lg w-arrow">
                {{ trans('yasnateam::general.call_to_action.start_project.button_text') }}
                <i class="icon-arrow-left"></i>
            </a>
        </div>

        <div class="left">
            <div class="icons" data-aos="zoom-in" data-aos-delay="200">
                <div class="icon green xlg"><img src="{{ Module::asset('yasnateam:images/icons/puzzles.svg') }}"></div>
                <div class="icon blue xlg"><img src="{{ Module::asset('yasnateam:images/icons/rocket.svg') }}"></div>
                <div class="icon red xlg"><img src="{{ Module::asset('yasnateam:images/icons/shield.svg') }}"></div>
            </div>
        </div>

    </div>

</div>