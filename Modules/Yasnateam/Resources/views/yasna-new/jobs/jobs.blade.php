@extends('yasnateam::yasna-new.layouts.main')

@section('inner-body')

    <div class="container">

        <div class="paper-wrapper">

            <div class="paper">

                <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::general.page_titles.jobs') }}</div>

                <div class="row">

                    <div class="col-sm-8 col-center">

                        <div class="desc mb30">
                            <p>{!! get_setting('jobs_desc') !!}</p>
                        </div>

                        <div class="sub-part-title mb30" data-aos="zoom-in-up">{{ trans('yasnateam::general.page_titles.jobs_active') }}</div>

                        @foreach( $jobs as $job )

                            @include('yasnateam::yasna-new.jobs.job-item')

                        @endforeach

                        <div class="d-ib w-100 mt50"></div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@stop
