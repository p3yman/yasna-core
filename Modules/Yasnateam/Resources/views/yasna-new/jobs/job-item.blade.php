<div class="blog-item" style="width: 100%;" data-aos="zoom-in-up">

    <div class="content">

        <a href="{{ route('jobs-single', $job->id) }}" class="h2">{{ $job->title }}</a>

    </div>
</div>