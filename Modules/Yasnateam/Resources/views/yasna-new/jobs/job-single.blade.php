@extends('yasnateam::yasna-new.layouts.main')

@section('inner-body')

    <div class="container">

        <div class="paper-wrapper">

            <div class="paper">

                <div class="single-page" data-aos="zoom-in-up">

                    <div class="sub-part-title" data-aos="zoom-in-up">
                        <a href="{{ route('jobs') }}" class="h3">{{ trans('yasnateam::general.page_titles.jobs') }}</a>
                    </div>

                    <div class="part-title" data-aos="zoom-in-up">

                        {{-- Post title --}}
                        {{ $job->title }}

                    </div>

                    <div class="row">

                        <div class="col-sm-8 col-center">

                            <div class="page-content" data-aos="zoom-in-up">

                                {!! $job->text !!}

                                <div class="ta-c mt50">
                                    <a href="{{ $job->getMeta('link') }}" class="button blue lg w-arrow" target="_blank">
                                        {{ trans('yasnateam::general.send_resume') }}
                                        <i class="icon-arrow-left"></i>
                                    </a>
                                </div>

                            </div>

                            <div class="page-footer" data-aos="zoom-in-up">

                                <div class="socials">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('blog-single', ['id'=>$job->id, 'slug'=>$job->slug]) }}" class="icon-fb"></a>
                                    <a href="https://twitter.com/home?status={{ route('blog-single', ['id'=>$job->id, 'slug'=>$job->slug]) }}" class="icon-tw"></a>
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('blog-single', ['id'=>$job->id, 'slug'=>$job->slug]) }}&title={{ $job->title }}&summary={{ $job->abstract }}" class="icon-li"></a>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@stop