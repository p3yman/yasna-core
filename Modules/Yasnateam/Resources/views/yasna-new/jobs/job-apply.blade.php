@extends('yasnateam::yasna-new.layouts.main')

@php

	$logo_hashid = get_setting('site_logo');
		$site_logo = fileManager()
						->file($logo_hashid)
						->getUrl();

@endphp

@section('header-links')
    <meta property="og:title" content="{{ Template::implodePageTitle(' :: ') }}"/>
    <meta property="og:url" content="{{ route('jobs-apply-form') }}" />
    <meta property="og:image" content="{{ $site_logo }}"/>
    <meta property="og:description" content="{{ trans('yasnateam::general.page_description.jobs_apply') }}"/>
@endsection

@section('inner-body')

    <div class="container">
        <div class="paper-wrapper">
            <div class="paper">

                <div class="sub-part-title" data-aos="zoom-in-up">
                    <a href="{{ route('jobs') }}" class="h3">{{ trans('yasnateam::general.page_titles.jobs') }}</a>
                </div>

                <div class="part-title" data-aos="zoom-in-up">
                    {{ trans('yasnateam::general.page_titles.jobs_apply') }}
                </div>

                <div class="row">
                    <div class="col-sm-8 col-center">
                        <div class="page-content">
                            <p data-aos="zoom-in-up" class="ta-c">{{ trans('yasnateam::job.intro') }}</p>

                            <div class="contact-form">

                                <form id="frmContact" action="{{ route("send-jobs-apply-form") }}" method="post" class="js" enctype="multipart/form-data">
                                    @csrf

                                    <h4 class="form-part-title">{{ trans('yasnateam::job.parts.personal') }}</h4>
                                    <div class="row mb10">

                                        <div class="col-sm-4">
                                            <div class="field mb10">
                                                <label for="name">{{ trans('yasnateam::job.name') }}</label>
                                                <input type="text" name="name" id="name">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="field mb10">
                                                <label for="email">{{ trans('yasnateam::job.gender') }}</label>
                                                <div class="radio alt inline">
                                                    <input id="gender_m" type="radio" name="gender" value="m" checked="">
                                                    <label for="gender_m"> {{ trans('yasnateam::job.gender_m') }} </label>
                                                </div>
                                                <div class="radio alt inline">
                                                    <input id="gender_f" type="radio" name="gender" value="f" checked="">
                                                    <label for="gender_f"> {{ trans('yasnateam::job.gender_f') }} </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="field mb10">
                                                <label for="marriage">{{ trans('yasnateam::job.marriage') }}</label>
                                                <div class="radio alt inline">
                                                    <input id="married" type="radio" name="marriage" value="married" checked="">
                                                    <label for="married"> {{ trans('yasnateam::job.married') }} </label>
                                                </div>
                                                <div class="radio alt inline">
                                                    <input id="single" type="radio" name="marriage" value="single" checked="">
                                                    <label for="single"> {{ trans('yasnateam::job.single') }} </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row mb10">

                                        <div class="col-sm-4">
                                            <div class="field mb10">
                                                <label for="bd">{{ trans('yasnateam::job.bd') }}</label>
                                                <input type="text" name="bd" id="bd" placeholder="{{ trans('yasnateam::job.bd_example') }}">

                                            </div>
                                        </div>

                                        <div class="col-sm-8">
                                            <div class="field mb10">
                                                <label for="address">{{ trans('yasnateam::job.address') }}</label>
                                                <input type="text" name="address" id="address">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row mb10">

                                        <div class="col-sm-4">
                                            <div class="field mb10">
                                                <label for="tel">{{ trans('yasnateam::job.tel') }}</label>
                                                <input type="text" name="tel" id="tel">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="field mb10">
                                                <label for="mobile">{{ trans('yasnateam::job.mobile') }}</label>
                                                <input type="text" name="mobile" id="mobile">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="field mb10">
                                                <label for="email">{{ trans('yasnateam::job.email') }}</label>
                                                <input type="email" name="email" id="email">
                                            </div>
                                        </div>

                                    </div>

                                    <h4 class="form-part-title">{{ trans('yasnateam::job.parts.experience') }}</h4>
                                    <div class="row mb20">

                                        <div class="col-sm-6">
                                            <div class="field mb10">
                                                <div class="checkbox">
                                                    <input id="insurance" type="checkbox" name="insurance" value="1">
                                                    <label for="insurance"> {{ trans('yasnateam::job.insurance') }} </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="field mb10">
                                                <div class="checkbox">
                                                    <input id="experience" type="checkbox" name="experience" value="1">
                                                    <label for="experience"> {{ trans('yasnateam::job.experience') }} </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <h4 class="form-part-title">{{ trans('yasnateam::job.parts.edu') }}</h4>
                                    <div class="row mb10">

                                        <div class="col-sm-6">
                                            <div class="field mb10">
                                                <div class="field mb10">
                                                    <label for="edu_degree">{{ trans('yasnateam::job.edu_degree') }}</label>
                                                    <input type="text" name="edu_degree" id="edu_degree">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="field mb10">
                                                <div class="field mb10">
                                                    <label for="edu_field">{{ trans('yasnateam::job.edu_field') }}</label>
                                                    <input type="text" name="edu_field" id="edu_field">
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <h4 class="form-part-title">{{ trans('yasnateam::job.parts.english') }}</h4>
                                    <div class="field mb10">
                                        <label>{{ trans('yasnateam::job.english_reading') }}</label>
                                        <div class="radio alt inline">
                                            <input id="english_reading_1" type="radio" name="english[reading]" value="1">
                                            <label for="english_reading_1"> {{ trans('yasnateam::job.rate_1') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_reading_2" type="radio" name="english[reading]" value="2">
                                            <label for="english_reading_2"> {{ trans('yasnateam::job.rate_2') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_reading_3" type="radio" name="english[reading]" value="3">
                                            <label for="english_reading_3"> {{ trans('yasnateam::job.rate_3') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_reading_4" type="radio" name="english[reading]" value="4">
                                            <label for="english_reading_4"> {{ trans('yasnateam::job.rate_4') }} </label>
                                        </div>
                                    </div>
                                    <div class="field mb10">
                                        <label>{{ trans('yasnateam::job.english_writing') }}</label>
                                        <div class="radio alt inline">
                                            <input id="english_writing_1" type="radio" name="english[writing]" value="1">
                                            <label for="english_writing_1"> {{ trans('yasnateam::job.rate_1') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_writing_2" type="radio" name="english[writing]" value="2">
                                            <label for="english_writing_2"> {{ trans('yasnateam::job.rate_2') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_writing_3" type="radio" name="english[writing]" value="3">
                                            <label for="english_writing_3"> {{ trans('yasnateam::job.rate_3') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_writing_4" type="radio" name="english[writing]" value="4">
                                            <label for="english_writing_4"> {{ trans('yasnateam::job.rate_4') }} </label>
                                        </div>
                                    </div>
                                    <div class="field mb10">
                                        <label>{{ trans('yasnateam::job.english_speaking') }}</label>
                                        <div class="radio alt inline">
                                            <input id="english_speaking_1" type="radio" name="english[speaking]" value="1">
                                            <label for="english_speaking_1"> {{ trans('yasnateam::job.rate_1') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_speaking_2" type="radio" name="english[speaking]" value="2">
                                            <label for="english_speaking_2"> {{ trans('yasnateam::job.rate_2') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_speaking_3" type="radio" name="english[speaking]" value="3">
                                            <label for="english_speaking_3"> {{ trans('yasnateam::job.rate_3') }} </label>
                                        </div>
                                        <div class="radio alt inline">
                                            <input id="english_speaking_4" type="radio" name="english[speaking]" value="4">
                                            <label for="english_speaking_4"> {{ trans('yasnateam::job.rate_4') }} </label>
                                        </div>
                                    </div>
                                    <div class="field mt10 mb30">
                                        <div class="checkbox">
                                            <input id="english_degree" type="checkbox" name="english[degree]" value="1">
                                            <label for="english_degree"> {{ trans('yasnateam::job.english_degree') }} </label>
                                        </div>
                                    </div>

                                    <h4 class="form-part-title">{{ trans('yasnateam::job.parts.courses') }}</h4>
                                    <div class="field mb30">
                                        <label for="courses">{{ trans('yasnateam::job.courses') }}</label>
                                        <textarea name="courses" id="courses" rows="5"></textarea>
                                    </div>

                                    <h4 class="form-part-title">{{ trans('yasnateam::job.parts.others') }}</h4>
                                    <div class="row mb10">
                                        <div class="col-sm-6">
                                            <div class="field mb10">
                                                <label for="salary">{{ trans('yasnateam::job.salary') }}</label>
                                                <input type="text" name="salary" id="salary">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="field mb10">
                                                <label for="available_date">{{ trans('yasnateam::job.available_date') }}</label>
                                                <input type="text" name="available_date" id="available_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field mb10">
                                        <label for="resume">{{ trans('yasnateam::job.resume') }}</label>
                                        <input type="file" name="resume">
                                    </div>

                                    <div class="action ta-l">
                                        <button class="button blue">{{ trans('yasnateam::job.send') }}</button>
                                    </div>

                                    @include("manage::forms.feed", [
                                        "success_class" => "alert success",
                                        "danger_class"  => "alert danger",
                                    ])

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('footer-assets')
    {!! Html::script(Module::asset('manage:libs/jquery.form.min.js')) !!}
    {!! Html::script(Module::asset("manage:libs/bootstrap-toggle/bootstrap-toggle.min.js")) !!} {{-- I'm truely sorry for this, Peyman. Please don't mention it very loud. --}}
    {!! Html::script(Module::asset("manage:js/forms.js")) !!}
@endsection
