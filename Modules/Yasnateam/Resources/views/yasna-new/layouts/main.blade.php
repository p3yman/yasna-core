<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />

    <title>{{ Template::implodePageTitle(' :: ') }}</title>

    @if( isLangRtl() )
        {!! Html::style(Module::asset('yasnateam:css/front-style.min.css')) !!}
    @else
        {!! Html::style(Module::asset('yasnateam:css/front-style-ltr.min.css')) !!}
    @endif
    {!! Html::style(Module::asset('yasnateam:css/more.min.css')) !!}
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>

    @yield('header-links')

</head>
<body>

    <header id="main-header">

        <div class="container">

            <div class="header-wrapper">

                <a href="{{ route('home') }}" id="logo" data-aos="flip-left">
                    <img src="{{ Module::asset('yasnateam:images/logo.png') }}" width="200" alt="{{ trans('yasnateam::genral.yasnateam') }}">
                </a>

                <div id="menu">
                    <ul>
                        <li><a href="{{ Route::currentRouteName() != 'home' ? route('home') . '/' : '' }}#what" data-aos="fade-down" data-aos-delay="100">{{ trans('yasnateam::menu.main.what_we_do') }}</a></li>
                        <li><a href="{{ Route::currentRouteName() != 'home' ? route('home') . '/' : '' }}#projects" data-aos="fade-down" data-aos-delay="150">{{ trans('yasnateam::menu.main.what_we_done') }}</a></li>
                        <li><a href="{{ Route::currentRouteName() != 'home' ? route('home') . '/' : '' }}#why" data-aos="fade-down" data-aos-delay="200">{{ trans('yasnateam::menu.main.why_us') }}</a></li>
                        <li><a href="{{ Route::currentRouteName() != 'home' ? route('home') . '/' : '' }}#who" data-aos="fade-down" data-aos-delay="250">{{ trans('yasnateam::menu.main.who_we_are') }}</a></li>
                        <li><a href="{{ Route::currentRouteName() != 'home' ? route('home') . '/' : '' }}#where" data-aos="fade-down" data-aos-delay="300">{{ trans('yasnateam::menu.main.where_we_are') }}</a></li>
                        <li><a href="{{ route('blog') }}" data-aos="fade-down" data-aos-delay="350">{{ trans('yasnateam::menu.main.blog') }}</a></li>
                        <li><a href="{{ route('jobs') }}" data-aos="fade-down" data-aos-delay="400">{{ trans('yasnateam::menu.footer.jobs') }}</a></li>
                    </ul>
                </div>

            </div>

        </div>

        <div class="divider"><img src="{{ Module::asset('yasnateam:images/divider-white.svg') }}"></div>

        <div class="bg-shapes">
            <span class="shape-1"></span>
            <span class="shape-2"></span>
            <span class="shape-3"></span>
        </div>

        {{-- Header big title --}}
        <div class="container">
            @if (trim($__env->yieldContent('header-big-title')))
                <div class="big-title">
                    @yield('header-big-title')
                </div>
            @endif
        </div>

    </header>


    {{-- Inner body --}}
    @yield('inner-body')


    {{-- Footer contact --}}
    @if( Route::currentRouteName() != 'home' && Route::currentRouteName() != 'contact' )
        <section id="where" class="inner">

            <div class="container">

                <div class="contact-info">
                    <div class="row">
                        <div class="col-sm-4" data-aos="fade-up">
                            <span class="icon gray xs"><img src="{{ Module::asset('yasnateam:images/icons/map.svg') }}"></span>
                            <div class="text">{{ get_setting('address') }}</div>
                        </div>
                        <div class="col-sm-4" data-aos="fade-up" data-aos-delay="100">
                            <span class="icon gray xs"><img src="{{ Module::asset('yasnateam:images/icons/phone.svg') }}"></span>
                            <div class="text phone">{{ pd( implode(' - ', get_setting('telephone')) ) }}</div>
                        </div>
                        <div class="col-sm-4" data-aos="fade-up" data-aos-delay="150">
                            <span class="icon gray xs"><img src="{{ Module::asset('yasnateam:images/icons/mail.svg') }}"></span>
                            <div class="text email"><a href="mailto:{{ array_first(get_setting('email')) }}">{{ array_first(get_setting('email')) }}</a></div>
                        </div>
                    </div>
                </div>

            </div>

        </section>
    @endif

    <footer id="main-footer">

        <div class="content">

            <div class="container">

                <a href="#" id="logo-footer" data-aos="fade-right">
                    <img src="{{ Module::asset('yasnateam:images/logo-footer.png') }}" width="156">
                </a>

                <div class="left" data-aos="fade-left">
                    <ul class="footer-menu">
                        <li><a href="{{ route('blog') }}">{{ trans('yasnateam::menu.footer.blog') }}</a></li>
                        <li><a href="{{ route('contact') }}">{{ trans('yasnateam::menu.footer.contact') }}</a></li>
                        <li><a href="{{ route('yasnaweb') }}">{{ trans('yasnateam::menu.footer.yasnaweb') }}</a></li>
                        <li><a href="{{ route('jobs') }}">{{ trans('yasnateam::menu.footer.jobs') }}</a></li>
                    </ul>
                    <div class="socials">
                        <a href="{{ get_setting('instagram_link') }}" class="icon-in"></a>
                        <a href="{{ get_setting('linkedin_account') }}" class="icon-li"></a>
                    </div>
                </div>

            </div>

        </div>

        <div class="copyright">{{ trans('yasnateam::general.copyright') }}</div>

    </footer>

    {!! Html::script(Module::asset('yasnateam:js/vendor/jquery.js')) !!}
    {!! Html::script(Module::asset('yasnateam:js/vendor/jquery.matchHeight.js')) !!}
    {!! Html::script(Module::asset('yasnateam:js/vendor/slick.js')) !!}
    {!! Html::script(Module::asset('yasnateam:js/vendor/owl.js')) !!}
    {!! Html::script(Module::asset('yasnateam:js/vendor/aos.js')) !!}
    <script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js" integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA==" crossorigin=""></script>
    {!! Html::script(Module::asset('yasnateam:js/app.js')) !!}

    @yield('footer-assets')

</body>
</html>
