@extends('yasnateam::yasna-new.layouts.main')

@section('inner-body')

    <div class="container">
        <div class="paper-wrapper">
            <div class="paper">

                <div class="single-page" data-aos="zoom-in-up">

                    @if( $page->featured_image )
                        <div class="page-cover" data-aos="zoom-in-up">
                            <img src="{{ postImage($page->featured_image)->getUrl() }}" class="fluid">
                        </div>
                    @endif

                    <div class="part-title" data-aos="zoom-in-up">
                        {{ $page->title }}
                    </div>

                    <div class="row">

                        <div class="col-sm-8 col-center">

                            <div class="page-content" data-aos="zoom-in-up">

                                {!! $page->text !!}

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>

@stop