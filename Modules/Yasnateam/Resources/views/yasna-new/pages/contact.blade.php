@extends('yasnateam::yasna-new.layouts.main')

@php

	$logo_hashid = get_setting('site_logo');
		$site_logo = fileManager()
						->file($logo_hashid)
						->getUrl();

@endphp

@section('header-links')
	<meta property="og:title" content="{{ Template::implodePageTitle(' :: ') }}"/>
	<meta property="og:url" content="{{ route('contact') }}"/>
	<meta property="og:image" content="{{ $site_logo }}"/>
	<meta property="og:description" content="{{ trans('yasnateam::general.page_description.contact') }}"/>
@endsection

@section('inner-body')

	<div class="container">
		<div class="paper-wrapper">
			<div class="paper">
				<div class="part-title"
					 data-aos="zoom-in-up">{{ trans('yasnateam::general.page_titles.contact') }}</div>
				<div class="row">
					<div class="col-sm-8 col-center">
						<div class="page-content">
							<p data-aos="zoom-in-up" class="ta-c">{{ trans('yasnateam::contact.intro') }}</p>

							<div class="contact-form">

								<form id="frmContact" action="{{ route("yasnateam-submit-contact-form") }}"
									  method="post" class="js">
									@csrf

									<div class="row">

										<div class="col-sm-4">
											<div class="field mb10">
												<label for="name">{{ trans('yasnateam::contact.name') }}</label>
												<input type="text" name="name">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="field mb10">
												<label for="email">{{ trans('yasnateam::contact.email') }}</label>
												<input type="email" name="email">
											</div>
										</div>

										<div class="col-sm-4">
											<div class="field mb10">
												<label for="phone">{{ trans('yasnateam::contact.phone') }}</label>
												<input type="text" name="phone">
											</div>
										</div>

									</div>

									<div class="field mb10">
										<label for="subject">{{ trans('yasnateam::contact.subject') }}</label>
										<input type="text" name="subject">
									</div>

									<div class="field mb10">
										<label for="message">{{ trans('yasnateam::contact.message') }}</label>
										<textarea name="message" id="message" rows="5"></textarea>
									</div>

									<div class="action ta-l">
										<button class="button blue">{{ trans('yasnateam::contact.send') }}</button>
									</div>

									@include("manage::forms.feed", [
										"success_class" => "alert success",
										"danger_class"  => "alert danger",
									])

								</form>

							</div>

							<div class="contact-info">
								<div class="row">
									<div class="col-sm-4" data-aos="fade-up">
										<span class="icon gray xs"><img
													src="{{ Module::asset('yasnateam:images/icons/map.svg') }}"
													class="m0"></span>
										<div class="text">{{ get_setting('address') }}</div>
									</div>
									<div class="col-sm-4" data-aos="fade-up" data-aos-delay="100">
										<span class="icon gray xs"><img
													src="{{ Module::asset('yasnateam:images/icons/phone.svg') }}"
													class="m0"></span>
										<div class="text phone">{{ pd( implode(' - ', get_setting('telephone')) ) }}</div>
									</div>
									<div class="col-sm-4" data-aos="fade-up" data-aos-delay="150">
										<span class="icon gray xs"><img
													src="{{ Module::asset('yasnateam:images/icons/mail.svg') }}"
													class="m0"></span>
										<div class="text email"><a
													href="mailto:{{ array_first(get_setting('email')) }}">{{ array_first(get_setting('email')) }}</a>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="map" class="mb50" data-lat="{{ get_setting('location')[0] }}"
			 data-lng="{{ get_setting('location')[1] }}"></div>
	</div>

@stop

@section('footer-assets')
	{!! Html::script(Module::asset('manage:libs/jquery.form.min.js')) !!}
	{!! Html::script(Module::asset("manage:libs/bootstrap-toggle/bootstrap-toggle.min.js")) !!} {{-- I'm truely sorry for this, Peyman. Please don't mention it very loud. --}}
	{!! Html::script(Module::asset("manage:js/forms.js")) !!}
@endsection
