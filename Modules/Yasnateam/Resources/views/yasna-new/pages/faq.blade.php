@extends('yasnateam::yasna-new.layouts.main')

@php

	$logo_hashid = get_setting('site_logo');
		$site_logo = fileManager()
						->file($logo_hashid)
						->getUrl();

@endphp

@section('header-links')
    <meta property="og:title" content="{{ Template::implodePageTitle(' :: ') }}"/>
    <meta property="og:url" content="{{ route('faq') }}" />
    <meta property="og:image" content="{{ $site_logo }}"/>
    <meta property="og:description" content="{{ trans('yasnateam::general.page_description.faq') }}"/>
@endsection

@section('inner-body')

    <div class="container">

        <div class="paper-wrapper">

            <div class="paper">

                <div class="part-title" data-aos="zoom-in-up">{{ trans('yasnateam::general.page_titles.faq') }}</div>

                <div class="row">

                    <div class="col-sm-8 col-center">

                        <div class="page-content">

                            <div class="faqs">

                                @foreach( $faqs as $faq )
                                    <div class="item" data-aos="zoom-in-up">
                                        <a href="#" class="head">{{ $faq->title }}<div class="toggle"></div></a>
                                        <div class="content">
                                            <p>{{ $faq->abstract }}</p>
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="container">

        <div class="call-to-action" data-aos="fade-up">

            <div class="right" data-aos="slide-right">
                <h1 >{{ trans('yasnateam::general.call_to_action.faq.title') }}</h1>
                <a href="{{ route('contact') }}" class="button lg w-arrow">{{ trans('yasnateam::general.call_to_action.faq.button_text') }}<i class="icon-arrow-left"></i></a>
            </div>

            <div class="left">
                <div class="icons" data-aos="zoom-in">
                    <div class="icon yellow xlg"><img src="{{ Module::asset('yasnateam:images/icons/puzzles.svg') }}"></div>
                    <div class="icon green xlg"><img src="{{ Module::asset('yasnateam:images/icons/support.svg') }}"></div>
                    <div class="icon red xlg"><img src="{{ Module::asset('yasnateam:images/icons/documents.svg') }}"></div>
                </div>
            </div>

        </div>

    </div>

@stop
