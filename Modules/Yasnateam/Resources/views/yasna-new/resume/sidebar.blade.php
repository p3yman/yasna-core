@include("manage::layouts.sidebar-link" , [
	'icon' => "file-alt",
	'caption' => trans("yasnateam::job.resumes") ,
	'link' => "resumes" ,
	'permission' => 'resumes',
])

