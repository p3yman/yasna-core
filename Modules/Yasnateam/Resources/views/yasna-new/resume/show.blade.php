<div class="p30">
	<table class="table table-striped bordered border-top">
		<tbody>
			<tr>
				<td class="col-4">{{ trans("yasnateam::job.name_and_family") }} :</td>
				<td>{{ $model->guest_name }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.gender") }} :</td>
				<td>
					@if($model->meta['gender'] == 'm')
						{{ trans("yasnateam::job.gender_m") }}
					@elseif($model->meta['gender'] == 'f')
						{{ trans("yasnateam::job.gender_f") }}
					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.marriage") }} :</td>
				<td>
					@if($model->meta['marriage'] == 'married')
						{{ trans("yasnateam::job.married") }}
					@elseif($model->meta['marriage'] == 'single')
						{{ trans("yasnateam::job.single") }}
					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.bd") }} :</td>
				<td>{{ $model->meta['bd'] }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.address") }} :</td>
				<td>{{ $model->meta['address'] }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.tel") }} :</td>
				<td>{{ $model->meta['tel'] }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.mobile") }} :</td>
				<td>{{ $model->guest_mobile }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.email") }} :</td>
				<td>{{ $model->guest_email }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.insurance") }} :</td>
				<td>
					@if($model->meta['insurance'] == 1)
						{{ trans("yasnateam::job.yes") }}
					@elseif($model->meta['insurance'] == 0)
						{{ trans("yasnateam::job.no") }}
					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.experience") }} :</td>
				<td>
					@if($model->meta['experience'] == 1)
						{{ trans("yasnateam::job.yes") }}
					@elseif($model->meta['experience'] == 0)
						{{ trans("yasnateam::job.no") }}
					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.edu_degree") }} :</td>
				<td>{{ $model->meta['edu_degree'] }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.edu_field") }} :</td>
				<td>{{ $model->meta['edu_field'] }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.parts.english") }} ( {{ trans("yasnateam::job.english_reading") }} ) :</td>
				<td>
					@if($model->meta['english[reading]'] == 1)
						{{ trans("yasnateam::job.rate_1") }}
					@elseif($model->meta['english[reading]'] == 2)
						{{ trans("yasnateam::job.rate_2") }}
					@elseif($model->meta['english[reading]'] == 3)
						{{ trans("yasnateam::job.rate_3") }}
					@elseif($model->meta['english[reading]'] == 4)
						{{ trans("yasnateam::job.rate_4") }}
					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.parts.english") }} ( {{ trans("yasnateam::job.english_writing") }} ) :</td>
				<td>
					@if($model->meta['english[writing]'] == 1)
						{{ trans("yasnateam::job.rate_1") }}
					@elseif($model->meta['english[writing]'] == 2)
						{{ trans("yasnateam::job.rate_2") }}
					@elseif($model->meta['english[writing]'] == 3)
						{{ trans("yasnateam::job.rate_3") }}
					@elseif($model->meta['english[writing]'] == 4)
						{{ trans("yasnateam::job.rate_4") }}
					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.parts.english") }} ( {{ trans("yasnateam::job.english_speaking") }} ) :</td>
				<td>
					@if($model->meta['english[speaking]'] == 1)
						{{ trans("yasnateam::job.rate_1") }}
					@elseif($model->meta['english[speaking]'] == 2)
						{{ trans("yasnateam::job.rate_2") }}
					@elseif($model->meta['english[speaking]'] == 3)
						{{ trans("yasnateam::job.rate_3") }}
					@elseif($model->meta['english[speaking]'] == 4)
						{{ trans("yasnateam::job.rate_4") }}
					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.english_degree") }} :</td>
				<td>
					@if($model->meta['english[degree]'] == 0)
						{{ trans("yasnateam::job.no") }}
					@elseif($model->meta['english[degree]'] == 1)
						{{ trans("yasnateam::job.yes") }}

					@endif
				</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.courses") }} :</td>
				<td>{{ $model->text }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.salary") }} :</td>
				<td>{{ $model->meta['salary'] }}</td>
			</tr>
			<tr>
				<td>{{ trans("yasnateam::job.available_date") }} :</td>
				<td>{{ $model->meta['available_date'] }}</td>
			</tr>
		</tbody>
	</table>
</div>
