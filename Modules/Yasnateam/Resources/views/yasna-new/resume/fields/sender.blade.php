@include("manage::widgets.grid-text" , [
	'icon' => 'user' ,
	'class' => "f16 font-yekan text-bold" ,
	'color' => $model->is_from_admin? 'green' : null ,
	'text' => $model->guest_name ,
])

@include("manage::widgets.grid-text" , [
	'text' => trans("yasnateam::job.birthday") . SPACE . $model->bd ,
	'size' => "12" ,
	'class' => "text-light font-tahoma text-wide" ,
	'condition' => $model->bd ,
])

@include("manage::widgets.grid-date" , [
	'date' => $model->created_at ,
	'class' => "text-light text-wide" ,
	'size' => '10',
])

@include("manage::widgets.grid-tiny" , [
	'icon' => "bug",
	'locale' => "en" ,
	'condition' => dev() ,
	'class' => "text-gray" ,
	'text' => $model->id . " | " . $model->hashid ,
]     )


