@include("manage::widgets.grid-text" , [
	'text' => $model->guest_mobile ,
	'size' => "12" ,
	'class' => "text-light font-tahoma text-wide" ,
	'condition' => $model->guest_mobile ,
	'icon' => "mobile" ,
])

@include("manage::widgets.grid-text" , [
	'text' => $model->guest_email ,
	'size' => "12" ,
	'class' => "text-light font-tahoma text-wide" ,
	'condition' => $model->guest_email ,
	'locale' => "en" ,
	'icon' => "at" ,
])

@include("manage::widgets.grid-text" , [
	'text' => $model->tel ,
	'size' => "12" ,
	'class' => "text-light font-tahoma text-wide" ,
	'condition' => $model->tel ,
	'icon' => "phone" ,
])
