@include("manage::widgets.grid-actionCol" , [
	'button_size' => "sm" ,
	"actions" => [
		['eye' , trans_safe("yasnateam::job.view") , "modal:manage/resumes/-hashid-/show", user()->can('resumes.browse') ],
		['trash' , trans_safe("manage::forms.button.soft_delete") , "modal:manage/resumes/-hashid-/delete_confirm", user()->can('resumes.delete')],
	]
])
