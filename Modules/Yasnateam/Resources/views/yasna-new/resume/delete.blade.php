@include("manage::layouts.modal-delete" , [
	'form_url' => route('delete-resume', ['module_id' => $model->hashid]),
	'modal_title' => trans('yasnateam::job.delete'),
	'title_value' => $model->guest_name,
])
