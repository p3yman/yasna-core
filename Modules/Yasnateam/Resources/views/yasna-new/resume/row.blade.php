@include('manage::widgets.grid-rowHeader' , [
	'handle' => "selector" ,
])

<td>
	@include("yasnateam::yasna-new.resume.fields.sender")
</td>
<td class="only-on-desktop">
	@include("yasnateam::yasna-new.resume.fields.contact")
</td>
<td class="only-on-desktop">
	@include("yasnateam::yasna-new.resume.fields.download_resume")
</td>
@include("yasnateam::yasna-new.resume.fields.action" , [
	"refresh_action" => false,
])
