@include("manage::widgets.grid" , [
	'table_id' => "tblResumes" ,
	'row_view' => "yasnateam::yasna-new.resume.row" ,
	'handle' => "selector" ,
	'operation_heading' => true ,
	'headings' => [
		trans('yasnateam::job.sender'),
		trans('yasnateam::job.contact'),
		trans('yasnateam::job.download_resume'),
	] ,
]     )
