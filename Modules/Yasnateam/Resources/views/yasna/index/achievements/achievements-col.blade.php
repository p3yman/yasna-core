<div class="achievement-col">
	<div class="achievement-content">
		<div class="achievement-counter">
			{{ ad($text) }}
		</div>
		<div class="achievement-titles">
			<div class="achievement-main-title">
				{{ $mainTitle }}
			</div>
			@if(isLangRtl())
			<div class="achievement-subtitle">
				{{ $subtitle }}
			</div>
			@endif
		</div>
	</div>
</div>