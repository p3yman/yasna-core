<section class="achievements hiddenDiv" id="achievements">
	<div class="container-fluid">
		<div class="achievements-outer">
			@foreach($achievements as $achievement)
				@include('yasnateam::yasna.index.achievements.achievements-col',[
					"text" => $achievement->abstract ,
					"mainTitle" => $achievement->title ,
					"subtitle" => $achievement->title2 ,
				])
			@endforeach
		</div>
	</div>
</section>