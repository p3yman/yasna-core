@php

$techs = [
	[
		"img" => Module::asset('yasnateam:images/logo/logo8.png'),
		"alt" => "logo" ,
		"title" => "PHP" ,
	],
	[
		"img" => Module::asset('yasnateam:images/logo/logo8.png'),
		"alt" => "logo" ,
		"title" => "PHP" ,
	],
	[
		"img" => Module::asset('yasnateam:images/logo/logo8.png'),
		"alt" => "logo" ,
		"title" => "PHP" ,
	],
	[
		"img" => Module::asset('yasnateam:images/logo/logo8.png'),
		"alt" => "logo" ,
		"title" => "PHP" ,
	],
	[
		"img" => Module::asset('yasnateam:images/logo/logo8.png'),
		"alt" => "logo" ,
		"title" => "PHP" ,
	],
	[
		"img" => Module::asset('yasnateam:images/logo/logo8.png'),
		"alt" => "logo" ,
		"title" => "PHP" ,
	],
	[
		"img" => Module::asset('yasnateam:images/logo/logo8.png'),
		"alt" => "logo" ,
		"title" => "PHP" ,
	],
];

@endphp

<div class="technologies-outer">
	<div class="technologies-slider owl-carousel">
		@foreach($technologies->posts()->inRandomOrder()->get() as  $tech)
			@php $tech->spreadMeta() @endphp
			@include('yasnateam::yasna.index.technologies.slide',[
				"img" => postImage($tech->featured_image)->getUrl()  ,
				"alt" => $tech->title,
				"title" => $tech->title ,
				"link" => $tech->link ,
			])
		@endforeach
	</div>
</div>

@section('endbody-script')
	<script>
		$(document).ready(function () {

            $('.technologies-slider').owlCarousel({
				@if(isLangRtl())
                rtl:true,
				@endif
                nav:false,
                dots: false,
                autoplay: true,
                autoplayTimeout:3500,
                autoplaySpeed: 3000,
                autoplayHoverPause: false,
				loop:true,
				items: 6,
                responsive:{
                    0:{
                        items:2,
                    },
                    600:{
                        items:3,
                    },
                    1000:{
                        items:5,
                    },
                    1440:{
                        items:6,
                    }
                }
			})
        })
	</script>
@append