{{-- Technologies --}}

<section class="technologies hiddenDiv" id="technologies">
	<div class="container-fluid">
		{{-- accepts a heading text--}}
		@include('yasnateam::yasna.frame.heading-main',["text"=> $technologies->title])

		@include('yasnateam::yasna.index.technologies.slider')

	</div>
</section>