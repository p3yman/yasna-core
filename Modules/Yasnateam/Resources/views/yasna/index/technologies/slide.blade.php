<div class="technologies-slide">
	<div class="tech-logo">
		<a href="{{ $link }}" target="_blank">
			<img src="{{ $img }}" alt="{{ $alt }}">
		</a>
	</div>
	<div class="tech-title">
		{{ $title }}
	</div>
</div>