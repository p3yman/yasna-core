{{--  index page  --}}

{{-- site essential frame extend --}}
@extends('yasnateam::yasna.frame.general-assets')

{{--page title--}}
@section('page_title' , Template::siteTitle())

{{--page essencial head content--}}
@section('header-links')
    {{--slider plugin stylesheets --}}
    {!! Html::style(Module::asset('yasnateam:libs/owlcarousel/dist/assets/owl.carousel.min.css')) !!}
    {!! Html::style(Module::asset('yasnateam:libs/owlcarousel/dist/assets/owl.theme.default.min.css')) !!}
    
    {{-- slider custom stylesheets --}}
    {!! Html::style(Module::asset('yasnateam:css/slider-style.css')) !!}
    {!! Html::style(Module::asset('yasnateam:css/btn-style.css')) !!}
@append


{{--
page inner body
header and contents of the page
--}}
@section('inner-body')
    
    @include('yasnateam::yasna.header.0header')
    
    {{--index page sections--}}
    @if($slideshow->count())
        @include('yasnateam::yasna.index.home.0home')
    @endif
    
    @isset($introData)
        @include('yasnateam::yasna.index.intro.0intro')
    @endisset
    
    @if($informationPost and $informationPost->exists)
        @include('yasnateam::yasna.index.services.0services')
    @endif

    @isset($logobar)
        @include('yasnateam::yasna.index.customers.0customers')
    @endisset
    
    @isset($whyus)
        @include('yasnateam::yasna.index.whyus.0whyus')
    @endisset

    @if (($aboutPost and $aboutPost->exists) or $profileCards->count())
        @include('yasnateam::yasna.index.about.0about')
    @endif

    @isset($achievements)
        @include('yasnateam::yasna.index.achievements.0achievements')
    @endisset

    @isset($technologies)
        @include('yasnateam::yasna.index.technologies.0technologies')
    @endisset

@endsection

{{--page end body scripts --}}
@section('endbody-script')
    @include('yasnateam::yasna.json-ld.index')
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9V7ArENsbs678XLPRJplwzdcVEgO0UBI&callback=initMap">
    </script>
    {!! Html::script(Module::asset('yasnateam:libs/owlcarousel/dist/owl.carousel.min.js')) !!}
    @if(isLangRtl())
        {!! Html::script(Module::asset('yasnateam:js/slider-custom-rtl.js')) !!}
    @else
        {!! Html::script(Module::asset('yasnateam:js/slider-custom.js')) !!}
    @endif
    {!! Html::script(Module::asset('yasnateam:js/btn.js')) !!}

    {!! Html::script(Module::asset('yasnateam:libs/particlesjs/particles.js')) !!}
    {!! Html::script(Module::asset('yasnateam:libs/particlesjs/demo/js/app.js')) !!}
@append

