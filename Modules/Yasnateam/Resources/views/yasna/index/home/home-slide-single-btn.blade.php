{{--Single Slide Type btn--}}

<div class="slide-owl mobile-scroll">
    <img src="{{ postImage($slide->featured_image)->getUrl() }}" alt="{{ $slide['title2'] }}">
    <div class="btn-wrapper">
        <button class="circle pulse-btn"><i class="fa fa-plus" aria-hidden="true"></i></button>
        <div class="popup pop-msg">
            <div class="popup-content light-bg">
                <h1 class="cap-heading pop-heading">{{ $slide['title2'] }}</h1>
                <p class="cap-p">{{ $slide['abstract'] }}</p>
                <a class="pop-link" href="{{ $slide['link'] }}">{{ $slide['btn_title'] }}</a>
            </div>
        </div>
    </div>
</div>