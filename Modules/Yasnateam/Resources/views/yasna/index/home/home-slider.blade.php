{{--Fullscreen Slider--}}

<div class="owl-carousel big-slider owl-theme">
    {{-- Creating each slide based on its type--}}
   @foreach($slideshow as $slide)
        @php $slide->spreadMeta() @endphp
        @if($slide['slideshow-type'] == 'normal')
            @include('yasnateam::yasna.index.home.home-slide-single')
        @elseif($slide['slideshow-type'] == 'btn')
            @include('yasnateam::yasna.index.home.home-slide-single-btn')
        @endif

   @endforeach


</div>