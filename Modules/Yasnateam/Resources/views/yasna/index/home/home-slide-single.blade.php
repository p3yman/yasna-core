{{--Single Slides: Type Normal--}}

<div class="slide-owl {{ $slide['slideshow-position-detail'] or '' }} {{ $slide['slideshow-content-color'] or '' }}">
    {{-- slide image --}}
    @include('yasnateam::yasna.frame.widgets.image',[
        'src'=> postImage($slide->featured_image)->getUrl(),
        'alt'=> $slide['title2']
    ])
    {{-- slide caption: gets title, headline, link --}}
   @include('yasnateam::yasna.index.home.home-slide-caption',[
        'title'=> $slide['title2'],
        'headline'=> $slide['abstract'], // a short sentence (optional)
        'href'=> $slide['link'], //link of slide (optional)
        'link_text'=> $slide['btn_title'] //text of the button
   ])

</div>