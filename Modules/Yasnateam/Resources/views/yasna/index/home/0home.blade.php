{{--Home Section--}}
{{--contains a fullscreen slider--}}
<section class="home">
    {{-- home page content--}}
    @include('yasnateam::yasna.index.home.home-slider')
</section>