<div class="row">


    @foreach($reasons as $reason)
        @php $reason->spreadMeta() @endphp
        <div class="col-md-4">
            <div class="whyus-container">
                <div class="whyus-img">

                    @if($reason->featured_image)
                        @include('yasnateam::yasna.frame.widgets.image',[
                            'src'=> postImage($reason->featured_image)->getUrl()
                        ])
                    @else
                        <span class="icon">
                            <i class="fa fa-check-square-o" aria-hidden="true"></i>
                        </span>
                    @endif

                </div>
                <div class="whyus-text">
                    <h2>
                        {{ $reason->title }}
                    </h2>
                    <div style="text-align: justify;margin-top: 40px">{!! $reason->text !!}</div>
                </div>
            </div>
        </div>
    @endforeach


</div>