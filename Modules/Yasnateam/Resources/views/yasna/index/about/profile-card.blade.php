
{{--profile card--}}

<li class="card" style="cursor: pointer">
    <header class="card-image">
        {{--profile image thumbnail--}}
        <img src="{{ doc($card->featured_image)->getUrl() }}">
    </header>
    {{--member name--}}
    <h1 class="profile-name">{{ $card->title }}</h1>
    {{--member job title--}}
    <h4 class="profile-title">{{ $card->post }}</h4>
    {{--member social links--}}
    <div class="profile-links">

                @if(strlen($card->personal_website))
                    {{--accepts a link to social and a fontawesome icon name--}}
                    @include('yasnateam::yasna.frame.social-link',
                    [
                        'link' => $card->personal_website,
                        'class' => 'home',
                    ])
                @endif

                @if(strlen($card->github_account))
                    {{--accepts a link to social and a fontawesome icon name--}}
                    @include('yasnateam::yasna.frame.social-link',
                    [
                        'link' => "https://github.com/".$card->github_account,
                        'class' => 'github',
                    ])
                @endif

                @if(strlen($card->twitter_account))
                    {{--accepts a link to social and a fontawesome icon name--}}
                    @include('yasnateam::yasna.frame.social-link',
                    [
                        'link' => "https://www.twitter.com/".$card->twitter_account,
                        'class' => 'twitter',
                    ])
                @endif


            @if(strlen($card->telegram_account))
                {{--accepts a link to social and a fontawesome icon name--}}
                @include('yasnateam::yasna.frame.social-link',
                [
                    'link' => "https://t.me/".$card->telegram_account,
                    'class' => 'telegram',
                ])
            @endif

            @if(strlen($card->facebook_account))
                {{--accepts a link to social and a fontawesome icon name--}}
                @include('yasnateam::yasna.frame.social-link',
                [
                    'link' => "https://www.facebook.com/".$card->facebook_account,
                    'class' => 'facebook',
                ])
            @endif

            @if(strlen($card->linkedin_account))
                {{--accepts a link to social and a fontawesome icon name--}}
                @include('yasnateam::yasna.frame.social-link',
                [
                    'link' => "https://www.linkedin.com/in/".$card->linkedin_account,
                    'class' => 'linkedin',
                ])
            @endif


    </div>
    {{--display is none - will be appended to "about-story" by js--}}
    <div class="profile-data">
        {{-- large profile image --}}
        <img src="{{ postImage($card->featured_image)->getUrl() }}">
        {{-- story of the member--}}
        <div class="member-story">
            {!! $card->text !!}
        </div>
    </div>
</li>