
{{--profile cards list--}}

<div class="row">
    <div class="cards-container">
        <ul class="cards owl-carousel owl-theme">
            {{-- creates a card for each group member--}}
            @foreach($profileCards as $card)
                @php $card->spreadMeta() @endphp
                @include('yasnateam::yasna.index.about.profile-card')
            @endforeach

        </ul>
    </div>
</div>