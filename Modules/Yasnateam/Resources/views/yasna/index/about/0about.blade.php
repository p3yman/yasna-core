{{-- About section --}}

<section class="about hiddenDiv" id="about">
    <div class="container-fluid">
        {{-- Section main heading --}}
        
        {{-- accepts a heading text--}}
        @include('yasnateam::yasna.frame.heading-main',["text"=> $aboutPost->title])
        
        {{-- About story section --}}
        @include('yasnateam::yasna.index.about.about-story')
        
        @if ($profileCards->count())
            {{-- About profile card slider --}}
            @include('yasnateam::yasna.index.about.about-profiles')
        @endif
    </div>
</section>