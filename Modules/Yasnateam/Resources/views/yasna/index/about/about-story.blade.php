{{-- About section story --}}

<div class="row story-outer">
    <div class="story-container" @if(!$aboutPost->exists) style="display: none" @endif>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    {{-- About story large image --}}
                    {{--accepts image src--}}
                    <div class="about-image-lg">
                        @include('yasnateam::yasna.frame.widgets.image',[
                            'src'=> doc($aboutPost->featured_image)->getUrl()
                        ])
                    </div>
                </div>
                <div class="col-lg-6">
                    {{-- About story default content --}}
                    <div class="story-content">
                        <button class="back-to-yasna btn-default" style="display: none;">
                            {{ trans('yasnateam::yasna.template.show_yasna') }}
                        </button>
                        {{--accepts subtitle text--}}
                        @include('yasnateam::yasna.frame.eyebrow',[
                            'text'=> $aboutPost->title2,
                            'span_class'=> "job-title"
                        ])
                        {{--echoing main title text--}}
                        <h2 class="about-name-lg name">{{ Template::siteTitle() }}</h2>
                        {{--echoing story text--}}
                        <div class="story description">
                            {!! $aboutPost->text or "" !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
