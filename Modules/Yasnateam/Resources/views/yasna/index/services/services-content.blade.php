{{--service detail text--}}

<div class="text">
    {{--subtitle: small text shown on top of title--}}
    @include('yasnateam::yasna.frame.eyebrow',[
        'text'=> $informationPost->title2
    ])
    {{-- title or motto of the service--}}
    @include('yasnateam::yasna.frame.heading-h2',[
        'text'=>$informationPost->title
    ])
    {{-- short description of the service --}}
    {{--@include('yasnateam::yasna.frame.paragraph',[
        'text'=> $services['text'],
        'class'=>"paragraph"
    ])--}}
    <div class="paragraph">
        {!! $informationPost->text  !!}
    </div>
    {{--link to complete introduction of the service --}}
    @include('yasnateam::yasna.frame.button-atag',[
        "href"=> $informationPost->link,
        "class"=> $informationPost->btn_class,
        "text" => $informationPost->btn_title
    ])

</div>