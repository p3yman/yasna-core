{{-- media inner--}}

<div class="slider-container">
    {{--media--}}
    @include('yasnateam::yasna.index.services.service-small-slider')

    {{--hover effect--}}
    <div class="hover-slide light-bg">
        <p>
            @include('yasnateam::yasna.frame.button-atag',[
                "href"=> $informationPost->link ?: '#',
                "class"=>"center-btn",
                "text" => $informationPost->btn_title
            ])
        </p>
    </div>
</div>