{{-- service media container --}}

<div class="browser-window">
    {{-- browser top view ( design related )--}}
    @include('yasnateam::yasna.frame.browser-top')
    {{--media--}}
    @include('yasnateam::yasna.index.services.service-slider')
</div>