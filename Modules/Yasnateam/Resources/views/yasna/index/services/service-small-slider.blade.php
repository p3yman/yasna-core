{{--small slider--}}

<div class="small-slider owl-carousel">
        {{--slider images--}}
    @foreach($informationPost->files as $img)
        <div class="small-slide">
            @include('yasnateam::yasna.frame.widgets.image',[
                "src"=> doc($img['src'])->getUrl()
            ])
        </div>
    @endforeach

</div>