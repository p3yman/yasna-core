{{-- logo slider row --}}

<div class="logo-row">
    <ul class="logo-container owl-carousel">
        {{-- gets data of each logo slide --}}
        {{-- accepts: tooltip title, logo image, customer's name, customer's product description, link to the product--}}
        @foreach($logobar['customers'] as $logo)
            @php $logo->spreadMeta() @endphp
            @include('yasnateam::yasna.index.customers.logobar-single',[
//                'tooltip_title'=> $logo->title,
                'logo_src' => postImage($logo->featured_image)->getUrl(),
                'product_src'=> postImage($logo->second_featured_image)->getUrl(),
                'product_name'=> $logo->title,
                'product_data'=> $logo->text,
                'product_link'=> $logo->link, //optional
                'product_link_title'=> $logo->btn_title //optional

            ])
        @endforeach
    </ul>
</div>