{{-- customers inner content --}}

<div class="row">
    <div class="col-sm-12">
        {{-- customers logo slider --}}
        @include('yasnateam::yasna.index.customers.logobar-slider')
    </div>
</div>
<div class="row">
    {{-- customer's details: tags will be filled with js --}}
    @include('yasnateam::yasna.index.customers.customers-details')
</div>