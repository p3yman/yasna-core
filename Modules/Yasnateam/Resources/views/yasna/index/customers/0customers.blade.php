{{-- Coustomers Section --}}
{{-- intoroduces customers--}}
<section class="customers hiddenDiv">
    <div class="container-fluid">
        {{-- Customers main heading --}}
        @include('yasnateam::yasna.frame.heading-main',[
            "text"=> $logobar['main_heading']
        ])
        {{-- customers inner content --}}
        @include('yasnateam::yasna.index.customers.customers')
    </div>
</section>