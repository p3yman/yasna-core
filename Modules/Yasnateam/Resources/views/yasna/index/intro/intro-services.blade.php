<div class="row intro-cards">
    {{--into section main heading--}}
    {{--accepts main heading and class--}}
    @include('yasnateam::yasna.frame.heading-main',[
         "text"=> $services['main_heading'],
         "class"=>'dark-bg'
    ])

    @foreach($services['intro'] as $service)
        @php $service->spreadMeta() @endphp
        <div class="col-md-4 col-sm-6">
            <div class="intro-card">
                <a href="#">
                    <div class="intro-card-img">
                        @include('yasnateam::yasna.frame.widgets.image',[
                            'src'=> doc($service->featured_image)->getUrl(),
                        ])
                    </div>
                    <div class="intro-text">
                        <h2>
                            {{ $service->title or ""}}
                        </h2>
                        <p>
                            {{ $service->abstract or ""}}
                        </p>
                    </div>
                </a>
            </div>
        </div>
    @endforeach
</div>