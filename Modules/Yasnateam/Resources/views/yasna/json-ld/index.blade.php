<script type="application/ld+json">
    {
        "@context" : "http://schema.org",
        "@type": "Organization",
        "url": "{{ url('/') }}",
        "name": "YasnaTeam",
        "logo": "{{ "https:". Module::asset('yasnateam:images/logo/yasnateam.png') }}",
        "description":"Software Development Company",
        "foundingDate": "2005",
        "founders": [
			{
				"@type": "Person",
				"name": "Mohsen Rezaei"
			},
			{
				"@type": "Person",
				"name": "Mohammad Hadi Rezaei"
			},
			{
				"@type": "Person",
				"name": "Taha Kamkar"
			}
		],
			"owns":{
			"@type": "Product",
				"name": "YasnaWeb"
		},
			"sameAs": [
			"https://github.com/yasnateam",
			"https://linkedin.com/company/yasnateam",
			"https://instagram.com/yasnateam"
		],
			"contactPoint": [
			{
				"@type": "ContactPoint",
				"telephone": "+98-21-77929890",
				"contactType": "customer support",
				"availableLanguage": "Persian"
			},
			{
				"@type": "ContactPoint",
				"telephone": "+98-21-22505661",
				"contactType": "customer support",
				"availableLanguage": "Persian"
			}
		],
			"email": "hello@yasna.team",
			"address":{
			"@type": "PostalAddress",
				"addressLocality": "Tehran,Tehran Province",
				"postalCode": "164-666-8352",
				"streetAddress": "No. 12, Block 376, Dr Ayat St",
				"addressCountry": "Iran"
		}
    }

</script>
