{!! widget('modal') !!}
@php($blank_char="-")
<div class="modal-body p40">
	<div class="list-group">
		<div class="row list-group-item ">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.company_name')}}
				</h4>
				<p>
					{{$key_pairs->where('name','company_name')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		<div class="row list-group-item ">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.foundation_year')}}
				</h4>
				<p>
					{{$key_pairs->where('name','foundation_year')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		<div class="row list-group-item ">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.email')}}
				</h4>
				<p>
					{{$key_pairs->where('name','email')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.phone_number')}}
				</h4>
				<p>
					{{$key_pairs->where('name','phone_number')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.address')}}
				</h4>
				<p>
					{{$key_pairs->where('name','address')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.site_url')}}
				</h4>
				<p>
					{{$key_pairs->where('name','site_url')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.organization_intro')}}
				</h4>
				<p>
					{{$key_pairs->where('name','organization_intro')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.your_superiority')}}
				</h4>
				<p>
					{{$key_pairs->where('name','superiority')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		@if(!is_null($key_pairs->where('name','has_color')->first()))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.has_color')}}
					</h4>
					<p>
						@if($key_pairs->where('name','has_color')->first()->value==1)
							{{trans('yasnateam::questionnaire.modal.yes')}}
						@else
							{{$blank_char}}
						@endif
					</p>
				</div>
			</div>

		@endif

		@if(!is_null($key_pairs->where('name','has_logo')->first()))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.has_logo')}}
					</h4>
					<p>
						@if($key_pairs->where('name','has_logo')->first()->value==1)
							{{trans('yasnateam::questionnaire.modal.yes')}}
						@else
							{{$blank_char}}
						@endif
					</p>
				</div>
			</div>
		@endif

		@if($key_pairs->where('name','customers_gender')->count() > 0)

			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::questionnaire.modal.gender')}}
					</h4>
					<p>
						{!! trans('yasnateam::yasna.survey.customers_gender.'.str_replace(' ','_',$key_pairs->where('name','customers_gender')->first()->value))  !!}
					</p>
				</div>
			</div>
		@endif

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.age')}}
				</h4>
				<p>
					<i>{{$key_pairs->where('name','customer_age_from')->first()->value or $blank_char}}</i>
					<i>{{trans('yasnateam::questionnaire.till')}}</i>
					<i>{{$key_pairs->where('name','customer_age_to')->first()->value or $blank_char}}</i>

				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.education')}}
				</h4>
				<p>
					<i>{{$key_pairs->where('name','customer_edu_from')->first()->value or $blank_char}}</i>
					<i>{{trans('yasnateam::questionnaire.till')}}</i>
					<i>{{$key_pairs->where('name','customer_edu_to')->first()->value or $blank_char}}</i>

				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.nationality')}}
				</h4>
				<p>
					{{$key_pairs->where('name','nationality')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.language')}}
				</h4>
				<p>
					{{$key_pairs->where('name','language')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		@if($key_pairs->where('name','competitors_has-site')->count() > 0)
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.has_site')}}
					</h4>
					<p>
						@if($key_pairs->where('name','competitors_has-site')->first()->value=='yes')
							{{trans('yasnateam::questionnaire.modal.yes')}}
						@elseif($key_pairs->where('name','competitors_has-site')->first()->value=='no')
							{{trans('yasnateam::questionnaire.modal.no')}}
						@else
							{{$blank_char}}
						@endif
					</p>
				</div>
			</div>
		@endif

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.competitors_sites')}}
				</h4>
				<p>
					{{$key_pairs->where('name','competitors_sites')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.competitors_sites_features')}}
				</h4>
				<p>
					{{$key_pairs->where('name','competitors_sites_features')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.your_site_advantages')}}
				</h4>
				<p>
					{{$key_pairs->where('name','your_site_advantages')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.budget')}}
				</h4>
				<p>
					{{$key_pairs->where('name','budget')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.example')}}
				</h4>
				<p>
					{{$key_pairs->where('name','example')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		@if(!is_null($key_pairs->where('name','mobile_app')->first()))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.mobile_app')}}
					</h4>
					<p>
						@if($key_pairs->where('name','mobile_app')->first()->value==1)
							{{trans('yasnateam::questionnaire.modal.yes')}}
						@else
							{{$blank_char}}
						@endif
					</p>
				</div>
			</div>
		@endif

		@if(!is_null($key_pairs->where('name','email_service')->first()))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.email_service')}}
					</h4>
					<p>
						@if($key_pairs->where('name','email_service')->first()->value==1)
							{{trans('yasnateam::questionnaire.modal.yes')}}
						@else
							{{$blank_char}}
						@endif
					</p>
				</div>
			</div>
		@endif

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.keywords')}}
				</h4>
				<p>
					{{$key_pairs->where('name','keywords')->first()->value or $blank_char}}
				</p>
			</div>
		</div>

		<div class="row list-group-item">
			<div class="col-md-12">
				<h4>
					{{trans('yasnateam::yasna.survey.red_lines')}}
				</h4>
				<p>
					{{$key_pairs->where('name','red_line')->first()->value or $blank_char}}
				</p>
			</div>
		</div>


		@if($key_pairs->where('name','content_time')->count() > 0)
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.content_time.title')}}
					</h4>
					<p>
						{!! trans('yasnateam::yasna.survey.content_time.'.str_replace(' ','_',$key_pairs->where('name','content_time')->first()->value))  !!}
					</p>
				</div>
			</div>
		@endif

		@if(!empty($socials))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.social_media.title')}}
					</h4>

					<ol>
						@foreach($socials as $social)
							@if(str_contains($social->name,'other'))
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$social->name)}}
									: {{$key_pairs->where('name','social_media_others')->first()->value or ""}}
								</li>
							@else
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$social->name)}}
								</li>
							@endif
						@endforeach
					</ol>

				</div>
			</div>
		@endif

		@if(!is_null($key_pairs->where('name','social_media_management')->first()))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.social_media_management')}}
					</h4>
					<p>
						@if($key_pairs->where('name','social_media_management')->first()->value==1)
							{{trans('yasnateam::questionnaire.modal.yes')}}
						@else
							{{$blank_char}}
						@endif
					</p>
				</div>
			</div>
		@endif

		@if(!empty($locales))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.title.site_locale')}}
					</h4>

					<ol>
						@foreach($locales as $locale)
							@if(str_contains($locale->name,'other') and !empty($locale->value))
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$locale->name)}}
									: {{$locale->value}}
								</li>
							@else
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$locale->name)}}
								</li>
							@endif
						@endforeach
					</ol>

				</div>
			</div>
		@endif

		@if(!empty($generals))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.title.general_options')}}
					</h4>

					<ol>
						@foreach($generals as $general)
							@if(str_contains($general->name,'other') and !empty($general->value))
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$general->name)}}
									: {{$general->value}}
								</li>
							@else
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$general->name)}}
								</li>
							@endif
						@endforeach
					</ol>

				</div>
			</div>
		@endif

		@if(!empty($statics))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.title.static_pages')}}
					</h4>

					<ol>
						@foreach($statics as $static)
							@if(str_contains($static->name,'other') and !empty($static->value))
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$static->name)}}
									: {{$static->value}}
								</li>
							@else
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$static->name)}}
								</li>
							@endif
						@endforeach
					</ol>

				</div>
			</div>
		@endif


		@if(!empty($dynamics))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.title.dynamic_pages')}}
					</h4>

					<ol>
						@foreach($dynamics as $dynamic)
							@if(str_contains($dynamic->name,'other') and !empty($dynamic->value))
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$dynamic->name)}}
									: {{$dynamic->value}}
								</li>
							@else
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$dynamic->name)}}
								</li>
							@endif
						@endforeach
					</ol>

				</div>
			</div>
		@endif

		@if(!empty($shops))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.title.shop')}}
					</h4>

					<ol>
						@foreach($shops as $shop)
							@if(str_contains($shop->name,'other') and !empty($shop->value))
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$shop->name)}}
									: {{$shop->value}}
								</li>
							@else
								<li class="list-item">
									{{trans('yasnateam::questionnaire.modal.'.$shop->name)}}
								</li>
							@endif
						@endforeach
					</ol>

				</div>
			</div>
		@endif

		@if(!empty($manages))
			<div class="row list-group-item">
				<div class="col-md-12">
					<h4>
						{{trans('yasnateam::yasna.survey.title.manage_dashboard')}}
					</h4>

					<ol>
						@foreach($manages as $manage)
							<li class="list-item">
								{{trans('yasnateam::questionnaire.modal.'.$manage->name)}}
							</li>
						@endforeach
					</ol>

				</div>
			</div>
		@endif
	</div>
</div>
