<td>{{$model->id}}</td>
<td>{{$model->name}}</td>
<td>{{$model->family}}</td>
<td>{{$model->email}}</td>
<td>{{pd($model->mobile)}}</td>
<td>{{echoDate($model->created_at)}}</td>
@if( $model->keyPairs()->exists())
	<td>
		<button class="btn btn-primary"
				onclick="masterModal('{{url('manage/questionnaire/key-pair/'.$model->hash_id)}}')"><i
					class='fa fa-eye'></i></button>
	</td>
@else
	<td>-</td>
@endif