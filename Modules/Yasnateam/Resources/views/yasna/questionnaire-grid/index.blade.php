@extends('manage::layouts.template')

@section('content')
	<div class="row">
		<div class="col-md-12">
			@include("manage::widgets.grid" , [
				'table_id' => "questionnaire" ,
				'row_view' => "yasnateam::yasna.questionnaire-grid.row" ,
				'headings' => [
				trans('yasnateam::questionnaire.id'),
				trans('yasnateam::questionnaire.name'),
				trans('yasnateam::questionnaire.family'),
				trans('yasnateam::questionnaire.email'),
				trans('yasnateam::questionnaire.mobile'),
				trans('yasnateam::questionnaire.created_at'),
				trans('yasnateam::questionnaire.key-pair'),
				] ,
			]     )

		</div>
	</div>
@stop