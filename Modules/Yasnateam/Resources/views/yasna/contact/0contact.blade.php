{{-- contact section --}}

<section class="contact" id="contact">
    <div class="container">
        @include('yasnateam::yasna.contact.contact-inner')
    </div>
</section>