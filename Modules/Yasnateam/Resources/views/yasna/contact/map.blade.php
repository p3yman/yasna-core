@isset($location)
    {{-- contact section map container--}}
    <div class="row">
        {{--<div class="map-container" id="map"></div>--}}
        <div class="contact-map map-container map" id="map" data-map-zoom="19" data-map-latitude="{{ $location[0] }}"
             data-map-longitude="{{ $location[1] }}"></div>
    </div>
@endisset