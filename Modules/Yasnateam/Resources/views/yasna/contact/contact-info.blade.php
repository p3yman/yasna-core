<h4 class="list-title logo-footer">
    {{ Template::siteTitle() }}
</h4>
@if (count($contactInfo = Template::contactInfo()))
    <ul class="contact-info">
        @if (array_key_exists('address', $contactInfo))
            <li class="info">
                <span class="info-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                {{ $contactInfo['address'] }}
            </li>
        @endif
        @if (array_key_exists('telephone', $contactInfo))
            <li class="info">
                <span class="info-icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                @foreach($contactInfo['telephone'] as $number)
                    @if ($loop->index)
                        ,
                    @endif
                    <a href="tel:{{ $number }}">{{ ad($number) }}</a>
                @endforeach
            </li>
        @endif
        @if (array_key_exists('email', $contactInfo))
            <li class="info">
                <span class="info-icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                @foreach($contactInfo['email'] as $email)
                    @if ($loop->index)
                        ,
                    @endif
                    <a href="mailto:{{ $email }}">{{ ad($email) }}</a>
                @endforeach
            
            </li>
        @endif
    </ul>
@endif
