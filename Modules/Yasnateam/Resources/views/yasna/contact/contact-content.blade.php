<div class="row footer-info">
    <div class="col-md-3 col-sm-4 col-xs-12">
        @include('yasnateam::yasna.contact.contact-info')
    </div>
    @php $relatedLinks = $relatedLinks = Template::relatedLinks() @endphp
    <div class="col-md-3 col-sm-4 col-xs-6">
        @if (array_key_exists('group1', $relatedLinks))
            @include('yasnateam::yasna.contact.contact-link-list',[
                "title"=> $relatedLinks['group1']['category']->titleIn(getLocale()),
                "links"=> $relatedLinks['group1']['links']
            ])
        @endif
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">
        @if (array_key_exists('group2', $relatedLinks))
            @include('yasnateam::yasna.contact.contact-link-list',[
                "title"=> $relatedLinks['group2']['category']->titleIn(getLocale()),
                "links"=> $relatedLinks['group2']['links']
            ])
        @endif
    </div>
    <div class="col-md-3 col-sm-12">
        @if(count(get_setting('site_locales')) > 1)
            @include('yasnateam::yasna.frame.button-atag',[
                'class'=>'white-border lang-btn',
                'text'=>"language",
                'icon'=>'globe'
            ])
        @endif
    </div>
</div>
