{{--contact inner--}}

{{--includes contanct heading and map only on index page--}}
@if(YasnaTeam::isIndex())
    @include('yasnateam::yasna.frame.heading-main',[
            "text" => trans('yasnateam::yasna.template.contact_us'),
            "class" => "dark-bg"
        ])
    @include('yasnateam::yasna.contact.map')
@endif


@include('yasnateam::yasna.contact.contact-content')