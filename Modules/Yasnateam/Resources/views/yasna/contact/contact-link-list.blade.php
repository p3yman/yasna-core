<h4 class="list-title">{{ $title }}</h4>
<ul class="related-links">
    @foreach($links as $link)
        <li>
            <a href="{{ $link->meta('link') }}">{{ $link->title }}</a>
        </li>
    @endforeach
</ul>