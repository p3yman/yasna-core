<aside>
    <div class="cover-container">
          <span class="close-btn">
            <i class="fa fa-times" aria-hidden="true"></i>
          </span>
        <div class="list-container">
            <h1 class="aside-title heading main">
                {{ trans("yasnateam::yasna.template.languages") }}
            </h1>
            @include('yasnateam::yasna.lang.language-list')
        </div>
    </div>
</aside>