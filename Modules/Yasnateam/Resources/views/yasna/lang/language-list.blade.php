<ul class="lang-list">
    @foreach(Template::siteLocales() as $language)
        <li @if($isCurrent = ($language === getLocale())) class="active-lang"
            @endif data-lang="{{ $langTitle =  YasnaTeam::getLanguageTitle($language) }}">
            <a @if(!$isCurrent) href="{{ YasnaTeamRouteTools::action('YasnateamController@index', ['lang' => $language]) }}" @endif>
                {{ $langTitle }}
            </a>
        </li>
    @endforeach

</ul>