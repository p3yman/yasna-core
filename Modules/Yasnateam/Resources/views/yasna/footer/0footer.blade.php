<footer>
    <div class="container">
        <div class="row border-line">
            <div class="col-md-6 col-md-push-6">
                @include('yasnateam::yasna.footer.footer-social-links',[
                    "links"=> Template::socialLinks()
                ])
            </div>
            <div class="col-md-6 col-md-pull-6">
                @include('yasnateam::yasna.frame.paragraph',[
                    "text"=> trans('yasnateam::yasna.template.credit', ['owner' => Template::yasnaTitle()]),
                    'class'=>'credit'
                ])
            </div>
        </div>
    </div>
</footer>