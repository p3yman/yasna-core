<div class="panel">
	<div class="panel-body">
		<div class="description" style="margin: 10px 0 40px; text-align: justify">
			{{ $desc }}
		</div>
		{!!
		    widget('form-open')->target(url('save/inquire'))
		 !!}


		@if(!is_null($REFERER))
			{{--{!!--}}
			{{--widget('input')--}}
			{{--->label(trans('yasnateam::questionnaire.domain'))--}}
			{{--->placeholder(trans('yasnateam::questionnaire.domain'))--}}
			{{--->name("domain")--}}
			{{--->value(old('domain'))--}}
			{{--->required()--}}
			{{--->value($REFERER)--}}
			{{--->attr('readonly')--}}
			{{--->inForm()--}}
			{{--!!}--}}
			<div id="widget-input-75871593-container" class="form-group " style="">



				<label for="name_last" class="col-sm-2 control-label ">
					{{trans('yasnateam::questionnaire.domain')}}
				</label>
				<div class="col-sm-10">
					<input type="text" readonly class="form-control" value="{{$REFERER}}" name="domain">

				</div>

			</div>
		@endif

		{!!
		    widget('input')
		    ->label('tr:validation.attributes.name_first')
		    ->placeholder('tr:validation.attributes_placeholder.name_first')
		    ->name("name_first")
		    ->value(old('name_first'))
		    ->required()
		    ->inForm()
		 !!}

		{!!
		    widget('input')
		    ->label('tr:validation.attributes.name_last')
		    ->placeholder('tr:validation.attributes_placeholder.name_last')
		    ->name("name_last")
		  	->value(old('name_last'))
		   ->required()
		    ->inForm()
		 !!}

		{!!
		    widget('input')
		    ->label('tr:validation.attributes.email')
		    ->placeholder('tr:validation.attributes_placeholder.email')
		    ->name("email")
		    		    ->required()
		  	->value(old('email'))
		    ->inForm()
		 !!}


		{!!
		    widget('input')
		    ->label('tr:validation.attributes.mobile')
		    ->placeholder('tr:validation.attributes_placeholder.mobile')
		    ->name("mobile")
		   	->value(old('mobile'))
		   ->required()
		    ->inForm()
		 !!}

		<div class="row">
			<div class="col-xs-12">
				{!!
					widget('button')
					->type('submit')
					->label('tr:yasnateam::yasna.form.send')
					->class('pull-right btn-info')
				 !!}
			</div>
		</div>
		<div class="row mt-10" style="margin-top: 10px">
			@foreach($errors->all() as $error)
				<div class="alert alert-danger" role="alert">
					{{$error}}
				</div>
			@endforeach
		</div>
		{!!
		    widget('feed')
		 !!}

		{!!
		    widget('form-close')
		 !!}
		<br><br><br>
	</div>
</div>