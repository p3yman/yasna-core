{{-- list view page of blog --}}

{{--
extending blog frame, gets page main title and hero image
containing header and footer
--}}
@extends('yasnateam::yasna.blog-posts.frame.blogposts',[
    "main_title"=> $posttype->titleIn(getLocale()),
    "hero_img"=> doc($posttype->spreadMeta()->list_header_image)->getUrl() ?: Module::asset('yasnateam:images/slide2.jpg')
])


{{-- styesheets of list-view blog page --}}
@section('header-links')
    {{ Html::style(Module::asset('yasnateam:css/list.min.css')) }}
@endsection

@if ($currentCategory)
    @php
        Template::appendToPageTitle($currentCategory->titleIn(getLocale()));
    @endphp
@endif

{{-- post's specific title --}}
@section('blog-post-title')
    {{ $posttype->titleIn(getLocale()) }}
@endsection

{{--blog page inner content--}}
@section('blog-content')
    @if($posts->count())
        {{-- content of a list view blog page --}}
        @include('yasnateam::yasna.blog-posts.list-view.blog-post-list')
        {{--bottom page pagination--}}
        @include('yasnateam::yasna.blog-posts.list-view.blog-page-navigation')
    @else
        @include('yasnateam::yasna.blog-posts.frame.posts-alert',[
            "type" => "danger" ,
            "message" => trans('yasnateam::general.message.no-post-found') ,
        ])
    @endif

@endsection


