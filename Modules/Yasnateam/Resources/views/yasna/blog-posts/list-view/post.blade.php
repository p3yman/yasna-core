{{--blog posts list view _ single post--}}

<div class="post">
    <a href="{{ $post_link or "" }}">
        <div class="thumbnail-img">
            {{--thumbnail image of the post --}}
            @include('yasnateam::yasna.frame.widgets.image',[
                'src'=> $thumbnail_img,
                'alt'=> ""
            ])
        </div>
    </a>
    {{-- post text details --}}
    <div class="post-details">
        {{-- post category --}}
        @if ($category)
            <h4 data-category="{{ $category or "" }}" class="eyebrow category">
                <a @if($category_link) href="{{ $category_link }}" @endif>{{ $category or "" }}</a>
            </h4>
        @endif
        {{-- post title --}}
        <a href="{{ $post_link or "" }}">
            <h3>{{ $title or "" }}</h3>
        </a>
        {{--post abstract--}}
        <p>
            {{ $abstract or "" }}
        </p>
        
        <div class="posts-publication-meta">
            {{-- author image --}}
            @include('yasnateam::yasna.frame.widgets.image',[
                'class'=>"post-author",
                'src'=>$author_img,
                'alt'=>''
            ])
            {{-- author name and post date--}}
            <div class="post-info">
                <p class="author-name">{{ $author_name or "" }}</p>
                <p class="date">{{ $date or "" }}</p>
            </div>
        </div>
    </div>
</div>
