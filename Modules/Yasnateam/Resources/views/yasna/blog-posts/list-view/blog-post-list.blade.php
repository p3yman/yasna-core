{{--blog posts list view--}}

<div class="row">
    {{-- creates blog post abstract cards --}}
    @foreach($posts as $post)
        {{--checkes cards column view--}}
        @php $col_class = ($post->spreadMeta()->columns_number == 2 ) ? "col-lg-12" : "col-lg-6" @endphp
        
        {{--blog post cards accept: post link, post thumbnail image, post category, post category link,
       post title, post abstract, author image src, author name, date--}}
        <div class="{{ $col_class }}">
            @include('yasnateam::yasna.blog-posts.list-view.post',[
                'post_link'=>$post->direct_url ?: '#',
                'thumbnail_img'=> doc($post->featured_image)->getUrl(),
                'category'=> ($defaultCategory = $post->guessDefaultCategory())->titleIn(getLocale()),
                'category_link'=> YasnaTeamRouteTools::actionLocale('YasnateamController@blogPosts', [
                    'category' => $defaultCategory->slug,
                ]),
                'title'=> $post->title,
                'abstract'=> $post->abstract,
                'author_img'=> ($postCreator = $post->creator) ? $postCreator->gravatar : '',
                'author_name'=> $postCreator ? $postCreator->full_name : "",
                'date'=> ad(echoDate($post->published_at, 'd F Y'))
            ])
        </div>
    
    @endforeach
    
    {{--@include('yasna.blog-posts.frame.posts.no-post',[
        'massage' => "There is no post to view."
    ])--}}

</div>