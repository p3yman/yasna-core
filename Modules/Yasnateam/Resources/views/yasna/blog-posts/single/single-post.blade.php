{{-- single page article content --}}
<div class="row">
    <div class="col-xs-12">

        {{-- single page inner content--}}
        @include('yasnateam::yasna.blog-posts.single.content')

    </div>
</div>