{{-- Single page for blog posts --}}

{{--
extending blog frame, gets page main title and hero image
containing header and footer
--}}
@extends('yasnateam::yasna.blog-posts.frame.blogposts',[
    "main_title"=> $post->title,
    "hero_img"=> doc($post->featured_image)->getUrl()
])

{{-- post specific title --}}
@section('blog-post-title')
    {{ $post->title }}
@endsection

@php
    Template::appendToPageTitle($post->guessDefaultCategory()->titleIn(getLocale()));
    Template::appendToPageTitle($post->title);
@endphp


{{--blog page inner contents--}}
@section('blog-content')

    @include('yasnateam::yasna.blog-posts.single.single-post')

@endsection