{{-- article content of single page blade--}}

<div class="content">
    
    {{--article author name and date--}}
    @include('yasnateam::yasna.blog-posts.single.data-author',[
        'date' => ad(echoDate($post->published_at, 'd F Y')),
        'name'=> $post->creator->full_name ,

    ])
    
    {{-- article texts --}}
    {!! $post->text !!}
    
    {{--sharing options for the post--}}
    @include('yasnateam::yasna.blog-posts.single.sharing',[
        "title" => $post->title,
        "url" => request()->url()
    ])

</div>