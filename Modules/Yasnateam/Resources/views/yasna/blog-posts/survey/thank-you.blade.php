<div class="final-page">
	<div class="final-message">
		{{ trans('yasnateam::yasna.survey.thanks') }}
	</div>
	<div class="image">
		<img src="{{ Module::asset('yasnateam:images/thanks.jpg') }}" alt="thank you">
	</div>
</div>