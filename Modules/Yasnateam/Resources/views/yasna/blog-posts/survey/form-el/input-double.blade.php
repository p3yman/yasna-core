<div class="from-group">
	<label for="{{ $id[0] or "" }}" class="control-label" style="margin-top: 10px;">
		{{ $label }}
	</label>
	<div class="row">
		<div class="col-xs-6 col-500">
			<input type="text" class="form-control form-watch" name="{{ $name[0] }}" id="{{ $id[0] or "" }}" placeholder="{{ $placeholder[0] }}">
		</div>
		<div class="col-xs-6 col-500">
			<label for="{{ $id[1] or "" }}" class="col-sm-12 hidden">
				{{ $label }}
			</label>
			<input type="text" class="form-control form-watch" name="{{ $name[1] }}" id="{{ $id[1] or "" }}" placeholder="{{ $placeholder[1] }}">
		</div>
	</div>
</div>