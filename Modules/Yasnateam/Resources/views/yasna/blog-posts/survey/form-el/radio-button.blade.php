@php
	if (isLangRtl()){
		$class = "rtl-fix";
	}

	if(isset($is_checked) and $is_checked){
		$checked= "checked";
	}else{
		$checked = "";
	}

@endphp

<label class="{{ $class or "" }}">
	<input name="{{ $name }}" type="radio" {{ $checked }} class="form-watch" value="{{ $value or 1 }}">
	<span>{{ $label }}</span>
</label>