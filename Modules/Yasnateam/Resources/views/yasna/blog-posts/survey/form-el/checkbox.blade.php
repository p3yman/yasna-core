@php
	if (isLangRtl()){
		$class = "rtl-fix";
	}
@endphp
<input type="hidden" value="0" name="{{$name}}">
@if(isset($block) and $block)
	<p>
		<label class="{{ $class or "" }}">
			<input type="checkbox" name="{{ $name }}" class="form-watch" value="{{ $value or 1 }}"/>
			<span>{{ $label }}</span>
		</label>
	</p>
@else
	<label class="{{ $class or "" }}">
		<input type="checkbox" name="{{ $name }}" class="form-watch" value="{{ $value or 1 }}"/>
		<span>{{ $label }}</span>
	</label>
@endif