@extends('yasnateam::yasna.blog-posts.frame.blogposts')

@php
	$main_title = "دامنه‌ها"
@endphp

@section('blog-post-title')
	{{ "فرم ثبت دامنه" }}
@endsection


{{--blog page inner contents--}}
@section('blog-content')

	@include('yasnateam::yasna.blog-posts.survey.wizard')

@endsection

@section('header-links')
	<script>
        function baseUrl() {
            return "{{ url('/') }}";
        }
	</script>
@append	

@section('endbody-script')
	{{--{!! Html::script(Module::asset('yasnateam:libs/vue/vue.js')) !!}--}}
	{!! Html::script(Module::asset('yasnateam:libs/vue/vue.min.js')) !!}
	
	{!! Html::script(Module::asset('yasnateam:js/timer.min.js')) !!}
	{!! Html::script(Module::asset('yasnateam:js/survey-form.js')) !!}

@endsection	