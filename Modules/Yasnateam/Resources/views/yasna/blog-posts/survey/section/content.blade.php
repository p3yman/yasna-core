@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.your_customers") }}
	</h3>

	<div class="form-fields">
		{!!
			widget('input')
			->name('keywords')
			->label('tr:yasnateam::yasna.survey.keywords')
			->class('form-watch')
		 !!}

		{!!
			widget('input')
			->name('red_line')
			->label('tr:yasnateam::yasna.survey.red_lines')
			->class('form-watch')
		 !!}

		<hr>

		<div>
			<label for="" class="control-label" style="margin-bottom: 10px;">
				{{ trans($transPath . "content_time.title")."..." }}
			</label>
			<div class="row">
				<div class="col-sm-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.radio-button",[
						"name" => "content_time" ,
						"label" => trans('yasnateam::yasna.survey.content_time.less_than_5_hour') ,
						"value" => "less_than_5_hour" , 
					])
				</div>
				<div class="col-sm-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.radio-button",[
						"name" => "content_time" ,
						"label" => trans('yasnateam::yasna.survey.content_time.more_than_5_hour') ,
						"value" => "more_than_5_hour" ,
					])
				</div>
				<div class="col-sm-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.radio-button",[
						"name" => "content_time" ,
						"label" => trans('yasnateam::yasna.survey.content_time.outsourcing') ,
						"value" => "outsourcing" ,
					])
				</div>
			</div>
		</div>

		<hr>

		<div>
			<label for="" class="control-label" style="margin-bottom: 10px;">
				{{ trans($transPath . "social_media.title")."..." }}
			</label>
			<div class="row">
				<div class="col-md-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "social_media-no" ,
						"label" => trans('yasnateam::yasna.survey.social_media.no') ,
						"value" => "no" ,
					])
				</div>
				<div class="col-md-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "social_media_facebook" ,
						"label" => trans('yasnateam::yasna.survey.social_media.facebook') ,
						"value" => "facebook" ,
					])
				</div>
				<div class="col-md-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "social_media_instagram" ,
						"label" => trans('yasnateam::yasna.survey.social_media.instagram') ,
						"value" => "instagram" ,
					])
				</div>
				<div class="col-md-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "social_media_twitter" ,
						"label" => trans('yasnateam::yasna.survey.social_media.twitter') ,
						"value" => "twitter" ,
					])
				</div>
				<div class="col-md-4">
					@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "social_media_linkedin" ,
						"label" => trans('yasnateam::yasna.survey.social_media.linkedin') ,
						"value" => "linkedin" ,
					])
				</div>
				<div class="col-md-4">
					{!!
					    widget('input')
					    ->name('social_media_others')
					    ->placeholder('tr:yasnateam::yasna.survey.social_media.others')
					     ->class('form-watch')
					 !!}
				</div>

			</div>
		</div>

		<hr>

		@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "social_media_management" ,
						"label" => trans('yasnateam::yasna.survey.social_media_management') ,
						"block" => true ,
					])
	</div>
</div>