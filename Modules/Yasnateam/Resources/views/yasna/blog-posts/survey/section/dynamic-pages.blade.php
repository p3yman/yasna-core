@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.dynamic_pages") }}
	</h3>

	<div class="form-fields">
		<div class="row">
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_news" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.news') ,
						"value" => "news" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_events" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.events') ,
						"value" => "events" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_expo" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.expo') ,
						"value" => "expo" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_blog" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.blog') ,
						"value" => "blog" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_photo_gallery" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.photo_gallery') ,
						"value" => "photo_gallery" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_video_gallery" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.video_gallery') ,
						"value" => "video_gallery" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_categories" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.categories') ,
						"value" => "categories" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_rss" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.rss') ,
						"value" => "rss" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_auto_publish" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.auto_publish') ,
						"value" => "auto_publish" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_tags" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.tags') ,
						"value" => "tags" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_share_to_social_networks" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.share_to_social_networks') ,
						"value" => "share_to_social_networks" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "dynamic_pages_commenting" ,
						"label" => trans('yasnateam::yasna.survey.dynamic_pages.commenting') ,
						"value" => "commenting" ,
					])
			</div>
		</div>

		<br>

		{!!
		    widget('textarea')
		    ->label(trans($transPath."dynamic_pages.others"))
		    ->name('dynamic_pages_others')
		        ->class('form-watch')
		 !!}
	</div>
</div>