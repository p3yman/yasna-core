<div class="form-section">
	<h3 class="section-header">
		{{ trans('yasnateam::yasna.survey.title.site_locale') }}
	</h3>

	<div class="form-fields">
		<div class="row">
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_fa" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.fa') ,
						"value" => "fa" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_en" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.en') ,
						"value" => "en" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_ar" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.ar') ,
						"value" => "ar" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_ar" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.ar') ,
						"value" => "ar" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_tr" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.tr') ,
						"value" => "tr" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_az" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.az') ,
						"value" => "az" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_fr" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.fr') ,
						"value" => "fr" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_es" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.es') ,
						"value" => "es" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_it" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.it') ,
						"value" => "it" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_zh" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.zh') ,
						"value" => "zh" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_ur" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.ur') ,
						"value" => "ur" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "site_locale_ps" ,
						"label" => trans('yasnateam::yasna.survey.site_locale.ps') ,
						"value" => "ps" ,
					])
			</div>
		</div>
		{!!
		    widget('input')
		    ->name('site_locale_others')
		    ->label('tr:yasnateam::yasna.survey.site_locale.others')
		    				    ->class('form-watch')

		 !!}
	</div>
</div>