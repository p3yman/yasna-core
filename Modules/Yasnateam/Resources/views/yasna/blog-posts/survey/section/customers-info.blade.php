@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.your_customers") }}
	</h3>

	<div class="form-fields">
		<div class="row">
			<div class="col-sm-4">
				@include('yasnateam::yasna.blog-posts.survey.form-el.radio-button',[
					"name" => "customers_gender" ,
					"label" => trans($transPath. "customers_gender.women"),
					"value" => "women" ,
				])
			</div>
			<div class="col-sm-4">
				@include('yasnateam::yasna.blog-posts.survey.form-el.radio-button',[
					"name" => "customers_gender" ,
					"label" => trans($transPath. "customers_gender.men") ,
					"value" => "men" ,
				])
			</div>
			<div class="col-sm-4">
				@include('yasnateam::yasna.blog-posts.survey.form-el.radio-button',[
					"name" => "customers_gender" ,
					"label" => trans($transPath. "customers_gender.no_difference") ,
					"is_checked" => true ,
					"value" => "no difference" ,
				])
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				@include('yasnateam::yasna.blog-posts.survey.form-el.input-double',[
					"label" => trans($transPath."age") ,
					"id" => ["customer_age_from","customer_age_to"] ,
					"name" => ["customer_age_from","customer_age_to"] ,
					"placeholder" => [
						trans($transPath."from"),
						trans($transPath."to")
					] ,
				])
			</div>
			<div class="col-md-6">
				@include('yasnateam::yasna.blog-posts.survey.form-el.input-double',[
					"label" => trans($transPath."education") ,
					"id" => ["customer_edu_from","customer_edu_to"] ,
					"name" => ["customer_edu_from","customer_edu_to"] ,
					"placeholder" => [
						trans($transPath."from"),
						trans($transPath."to")
					] ,
				])
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				{!!
				    widget('text')
				    ->label(trans($transPath."nationality"))
				    ->name('nationality')
				    ->class('form-watch')
				 !!}
			</div>
			<div class="col-md-6">
				{!!
				    widget('text')
				    ->label(trans($transPath."language"))
				    ->name('language')
				    ->class('form-watch')
				 !!}
			</div>
		</div>

	</div>
</div>