@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.your_organization") }}
	</h3>

	<div class="form-fields">
		{!!
			widget('textarea')
			->name('organization_intro')
			->label('tr:yasnateam::yasna.survey.organization_intro')
			->class('form-watch')
			->style('margin-top: 10px; resize: none;')
		 !!}

		{!!
			widget('textarea')
			->name('superiority')
			->label('tr:yasnateam::yasna.survey.your_superiority')
			->class('form-watch')
			->style('margin-top: 10px; resize: none;')
		 !!}

		<br>

		@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
			"name" => "has_color" ,
			"label" => trans('yasnateam::yasna.survey.has_color') ,
			"block" => true ,
		])

		@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
			"name" => "has_logo" ,
			"label" => trans('yasnateam::yasna.survey.has_logo') ,
			"block" => true ,
		])

	</div>
</div>