@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.your_competitors") }}
	</h3>

	<div class="form-fields">
		<div class="row" style="margin-bottom: 10px;">
			<label class="form-label ">{{ trans($transPath. "has_site"). "..." }}</label>
			@include('yasnateam::yasna.blog-posts.survey.form-el.radio-button',[
					"label" => trans($transPath. "yes") ,
					"name" => "competitors_has-site" ,
					"value" => "yes" ,
			])
			@include('yasnateam::yasna.blog-posts.survey.form-el.radio-button',[
					"label" => trans($transPath. "no") ,
					"name" => "competitors_has-site" ,
					"value" => "no" ,
			])
		</div>

		{!!
			widget('textarea')
			->name('competitors_sites')
			->label('tr:yasnateam::yasna.survey.competitors_sites')
			->class('form-watch')
			->style('margin-top: 10px; resize: none;')
		 !!}

		{!!
			widget('textarea')
			->name('competitors_sites_features')
			->label('tr:yasnateam::yasna.survey.competitors_sites_features')
			->class('form-watch')
			->style('margin-top: 10px; resize: none;')
		 !!}

		{!!
			widget('textarea')
			->name('competitors_sites_disadvantages')
			->label('tr:yasnateam::yasna.survey.competitors_sites_disadvantages')
			->class('form-watch')
			->style('margin-top: 10px; resize: none;')
		 !!}

		{!!
			widget('textarea')
			->name('your_site_advantages')
			->label('tr:yasnateam::yasna.survey.your_site_advantages')
			->class('form-watch')
			->style('margin-top: 10px; resize: none;')
		 !!}

	</div>
</div>