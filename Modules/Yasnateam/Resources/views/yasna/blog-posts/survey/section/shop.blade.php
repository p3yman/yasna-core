@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.shop") }}
	</h3>

	<div class="form-fields">
		<div class="row">
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_online_payment" ,
						"label" => trans('yasnateam::yasna.survey.shop.online_payment') ,
						"value" => "online_payment" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_offline_payment" ,
						"label" => trans('yasnateam::yasna.survey.shop.offline_payment') ,
						"value" => "offline_payment" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_group_discount" ,
						"label" => trans('yasnateam::yasna.survey.shop.group_discount') ,
						"value" => "group_discount" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_product_discount" ,
						"label" => trans('yasnateam::yasna.survey.shop.product_discount') ,
						"value" => "product_discount" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_package_management" ,
						"label" => trans('yasnateam::yasna.survey.shop.package_management') ,
						"value" => "package_management" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_inventory_management" ,
						"label" => trans('yasnateam::yasna.survey.shop.inventory_management') ,
						"value" => "inventory_management" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_order_tracking" ,
						"label" => trans('yasnateam::yasna.survey.shop.order_tracking') ,
						"value" => "order_tracking" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_prices_archives" ,
						"label" => trans('yasnateam::yasna.survey.shop.prices_archives') ,
						"value" => "prices_archives" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_discount_archives" ,
						"label" => trans('yasnateam::yasna.survey.shop.discount_archives') ,
						"value" => "discount_archives" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_currency_sale" ,
						"label" => trans('yasnateam::yasna.survey.shop.currency_sale') ,
						"value" => "currency_sale" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_credit_sale" ,
						"label" => trans('yasnateam::yasna.survey.shop.credit_sale') ,
						"value" => "credit_sale" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_installment_sale" ,
						"label" => trans('yasnateam::yasna.survey.shop.installment_sale') ,
						"value" => "installment_sale" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_unavailable_product_request" ,
						"label" => trans('yasnateam::yasna.survey.shop.unavailable_product_request') ,
						"value" => "unavailable_product_request" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_customers_club" ,
						"label" => trans('yasnateam::yasna.survey.shop.customers_club') ,
						"value" => "customers_club" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_sales_report" ,
						"label" => trans('yasnateam::yasna.survey.shop.sales_report') ,
						"value" => "sales_report" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_shop_accounting" ,
						"label" => trans('yasnateam::yasna.survey.shop.shop_accounting') ,
						"value" => "shop_accounting" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "shop_physical_shop_integrity" ,
						"label" => trans('yasnateam::yasna.survey.shop.physical_shop_integrity') ,
						"value" => "physical_shop_integrity" ,
					])
			</div>
		</div>

		<br>

		{!!
		    widget('textarea')
		    ->label(trans('yasnateam::yasna.survey.shop.others'))
		    ->name('shop_others')
		    ->class('form-watch')
		 !!}
	</div>

</div>