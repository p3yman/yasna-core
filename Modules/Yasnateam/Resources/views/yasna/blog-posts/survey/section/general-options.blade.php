@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.general_options") }}
	</h3>

	<div class="form-fields">
		<div class="row">
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_users_registration" ,
						"label" => trans('yasnateam::yasna.survey.general_options.users_registration') ,
						"value" => "users_registration" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_nested_menu" ,
						"label" => trans('yasnateam::yasna.survey.general_options.nested_menu') ,
						"value" => "nested_menu" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_chat" ,
						"label" => trans('yasnateam::yasna.survey.general_options.chat') ,
						"value" => "chat" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_search" ,
						"label" => trans('yasnateam::yasna.survey.general_options.search') ,
						"value" => "search" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_donation" ,
						"label" => trans('yasnateam::yasna.survey.general_options.donation') ,
						"value" => "donation" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_ads" ,
						"label" => trans('yasnateam::yasna.survey.general_options.ads') ,
						"value" => "ads" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_sub_domain" ,
						"label" => trans('yasnateam::yasna.survey.general_options.sub_domain') ,
						"value" => "sub_domain" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_survey" ,
						"label" => trans('yasnateam::yasna.survey.general_options.survey') ,
						"value" => "survey" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_slide_show" ,
						"label" => trans('yasnateam::yasna.survey.general_options.slide_show') ,
						"value" => "slide_show" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_statistics" ,
						"label" => trans('yasnateam::yasna.survey.general_options.statistics') ,
						"value" => "statistics" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_users_observation" ,
						"label" => trans('yasnateam::yasna.survey.general_options.users_observation') ,
						"value" => "users_observation" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "general_options_video_streaming" ,
						"label" => trans('yasnateam::yasna.survey.general_options.video_streaming') ,
						"value" => "video_streaming" ,
					])
			</div>
		</div>

		<br>

		{!!
		    widget('textarea')
		    ->label(trans('yasnateam::yasna.survey.general_options.others'))
		    ->name('general_options_others')
		    				    ->class('form-watch')

		 !!}
	</div>

</div>