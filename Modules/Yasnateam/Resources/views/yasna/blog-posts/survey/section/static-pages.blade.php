@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.static_pages") }}
	</h3>

	<div class="form-fields">
		<div class="row">
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "static_pages_contact_us" ,
						"label" => trans('yasnateam::yasna.survey.static_pages.contact_us') ,
						"value" => "contact_us" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "static_pages_about_us" ,
						"label" => trans('yasnateam::yasna.survey.static_pages.about_us') ,
						"value" => "about_us" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "static_pages_google_maps" ,
						"label" => trans('yasnateam::yasna.survey.static_pages.google_maps') ,
						"value" => "google_maps" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "static_pages_manage_chart" ,
						"label" => trans('yasnateam::yasna.survey.static_pages.manage_chart') ,
						"value" => "manage_chart" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "static_pages_manager_message" ,
						"label" => trans('yasnateam::yasna.survey.static_pages.manager_message') ,
						"value" => "manager_message" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "static_pages_error" ,
						"label" => trans('yasnateam::yasna.survey.static_pages.error') ,
						"value" => "error" ,
					])
			</div>
		</div>

		<br>

		{!!
		    widget('textarea')
		    ->label(trans($transPath."static_pages.others"))
		    ->name('static_pages_others')
		        ->class('form-watch')
		 !!}
	</div>
</div>