@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.manage_dashboard") }}
	</h3>

	<div class="form-fields">
		<div class="row">
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "manage_dashboard_two_step_verification" ,
						"label" => trans('yasnateam::yasna.survey.manage_dashboard.two_step_verification') ,
						"value" => "two_step_verification" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "manage_dashboard_access_levels" ,
						"label" => trans('yasnateam::yasna.survey.manage_dashboard.access_levels') ,
						"value" => "access_levels" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "manage_dashboard_users_activity_report" ,
						"label" => trans('yasnateam::yasna.survey.manage_dashboard.users_activity_report') ,
						"value" => "users_activity_report" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "manage_dashboard_sending_sms_email" ,
						"label" => trans('yasnateam::yasna.survey.manage_dashboard.sending_sms_email') ,
						"value" => "sending_sms_email" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "manage_dashboard_menu_maker" ,
						"label" => trans('yasnateam::yasna.survey.manage_dashboard.menu_maker') ,
						"value" => "menu_maker" ,
					])
			</div>
			<div class="col-sm-4">
				@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
						"name" => "manage_dashboard_manage_user_roles" ,
						"label" => trans('yasnateam::yasna.survey.manage_dashboard.manage_user_roles') ,
						"value" => "manage_user_roles" ,
					])
			</div>
		</div>

	</div>

</div>