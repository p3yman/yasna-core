@php
	$transPath = "yasnateam::yasna.survey."
@endphp

<div class="form-section">
	<h3 class="section-header">
		{{ trans($transPath."title.your_expectation") }}
	</h3>

	<div class="form-fields">
		{!!
			widget('input')
			->name('requirement_reasons')
			->label('tr:yasnateam::yasna.survey.why_you_need_site.label')
			->help('tr:yasnateam::yasna.survey.why_you_need_site.hint')
			->class('form-watch')
		 !!}

		{!!
			widget('input')
			->name('comprehend_features')
			->label('tr:yasnateam::yasna.survey.comprehend_features')
			->class('form-watch')
		 !!}

		{!!
			widget('input')
			->name('slogan')
			->label('tr:yasnateam::yasna.survey.slogan')
			->class('form-watch')
		 !!}

		{!!
			widget('input')
			->name('budget')
			->label('tr:yasnateam::yasna.survey.budget')
			->class('form-watch')
		 !!}

		{!!
			widget('textarea')
			->name('example')
			->label('tr:yasnateam::yasna.survey.example')
			->class('form-watch')
		 !!}

		<br>

		@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
			"name" => "mobile_app" ,
			"label" => trans('yasnateam::yasna.survey.mobile_app') ,
			"block" => true ,
		])

		@include("yasnateam::yasna.blog-posts.survey.form-el.checkbox",[
			"name" => "email_service" ,
			"label" => trans('yasnateam::yasna.survey.email_service') ,
			"block" => true ,
		])
	</div>
</div>