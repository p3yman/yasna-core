<div class="form-section">
	<h3 class="section-header">
		{{ trans('yasnateam::yasna.survey.title.general_info') }}
	</h3>

	<div class="form-fields">
		{!!
			widget('input')
			->name('company_name')
			->inForm()
			->label('tr:yasnateam::yasna.survey.company_name')
			->class('form-watch')
		 !!}


		{!!
			widget('input')
			->name('foundation_year')
			->inForm()
			->label("tr:yasnateam::yasna.survey.foundation_year")
			->class('form-watch')
		 !!}


		{!!
			widget('input')
			->name('email')
			->inForm()
			->label("tr:yasnateam::yasna.survey.email")
			->class('form-watch')
		 !!}

		{!!
			widget('input')
			->name('phone_number')
			->inForm()
			->label("tr:yasnateam::yasna.survey.phone_number")
			->class('form-watch')
		 !!}

		{!!
			widget('input')
			->name('address')
			->inForm()
			->label('tr:yasnateam::yasna.survey.address')
			->class('form-watch')
		 !!}

		{!!
			widget('input')
			->name('site_url')
			->inForm()
			->label('tr:yasnateam::yasna.survey.site_url')
			->class('form-watch')
		 !!}

	</div>

</div>