@php
	$folderPath= 'yasnateam::yasna.blog-posts.survey.section.';
	$formId = csrf_token();

	$btnTexts = json_encode(trans('yasnateam::yasna.wizardControls'));

@endphp

<div class="survey-form hidden">
	{!!
	    widget('hidden')
	    ->value($formId)
	    ->id('csrf')
	 !!}

	{!!
	widget('hidden')
	->value(session('form_id') )
	->id('formId')
    !!}
	<Wizard v-show="!end" v-on:wizardend="endit" :btn-texts="{{ $btnTexts }}">
		<wizard-section id="step1" :selected="true">
			@include($folderPath . "general-info")
		</wizard-section>
		<wizard-section id="step2">
			@include($folderPath . "your-organization")
		</wizard-section>
		<wizard-section id="step3">
			@include($folderPath . "customers-info")
		</wizard-section>
		<wizard-section id="step4">
			@include($folderPath . "competitors")
		</wizard-section>
		<wizard-section id="step5">
			@include($folderPath . "your-expectation")
		</wizard-section>
		<wizard-section id="step6">
			@include($folderPath . "content")
		</wizard-section>
		<wizard-section id="step7">
			@include($folderPath . "site-locale")
		</wizard-section>
		<wizard-section id="step8">
			@include($folderPath . "general-options")
		</wizard-section>
		<wizard-section id="step9">
			@include($folderPath . "static-pages")
		</wizard-section>
		<wizard-section id="step10">
			@include($folderPath . "dynamic-pages")
		</wizard-section>
		<wizard-section id="step11">
			@include($folderPath . "shop")
		</wizard-section>
		<wizard-section id="step12">
			@include($folderPath . "manage")
		</wizard-section>
	</Wizard>

	<div class="wizard-end" v-show="end">
		@include("yasnateam::yasna.blog-posts.survey.thank-you")
	</div>
</div>