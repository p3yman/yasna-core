{{--Heading ( inner page type )--}}

<section class="{{ Template::darkHeader() ? "has-dark-bg" : "" }} generic-hero">

    {{-- $main_title and $hero_img sets when extending blogpost.blade--}}
    @include('yasnateam::yasna.blog-posts.frame.heading.heading-inner',[
        "image_src"=> (isset($hero_img))? $hero_img : false,
        "page_title" => $main_title
    ])

</section>