{{--inner heading --}}

{{--hero image is optional--}}
@if( isset($image_src) or $image_src)
	@include('yasnateam::yasna.frame.widgets.image',[
		'src'=>$image_src,
		'class'=>"cover-img"
	])
@endif

@include('yasnateam::yasna.blog-posts.frame.heading.heading-title',[
    'title'=>$page_title
])
