<div class="row">
    <div class="col-md-12">
        <div class="alert-container">
            @include('yasnateam::yasna.frame.alert',[
            "type" => $type ,
            "message" => $message ,
        ])
        </div>
    </div>
</div>