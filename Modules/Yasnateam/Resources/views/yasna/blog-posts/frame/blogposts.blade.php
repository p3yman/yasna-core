{{-- general assets of the site --}}
@extends('yasnateam::yasna.frame.general-assets')


{{-- blog pages general stylesheet--}}
@section('header-links')
    {{ Html::style(Module::asset('yasnateam:css/blog.min.css')) }}
@append

{{--blog body inner sections--}}
@section('inner-body')

    {{--general heading of site: accepts a class for styling--}}
    @include('yasnateam::yasna.header.0header')

    {{--blog headings: accepts a class for styling--}}
    @include('yasnateam::yasna.blog-posts.frame.heading.0heading')


    {{--navbar of blog pages--}}
    @include('yasnateam::yasna.blog-posts.frame.navbar.0navbar')

    {{-- blog contents--}}
    @include('yasnateam::yasna.blog-posts.frame.posts.0blog')

@endsection


@section('endbody-script')
    {!! Html::script (Module::asset('yasnateam:js/blog.js')) !!}
@append