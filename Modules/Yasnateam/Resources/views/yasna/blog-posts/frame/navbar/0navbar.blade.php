{{--navigation--}}
@if (isset($categories) and $categories->count())
    {{--laptop view nav--}}
    <nav class="nav-tab">
        <ul class="container clearfix">
            @foreach($categories as $category)
                @php
                    $categoryListUrl = YasnaTeamRouteTools::actionLocale('YasnateamController@blogPosts', [
                        'category' => $category->slug,
                    ]);
                    $categoryTitle = $category->titleIn(getLocale());
                    $isDefault = ($currentCategory and ($category->slug == $currentCategory->slug));
                @endphp
                <li class="subnav  @if($isDefault) is-active @endif">
                    <a href="{{ $categoryListUrl }}">{{ $categoryTitle }}</a>
                </li>
            @section('tablet-nav-options')
                <option value="{{ $category->slug }}" data-href="{{ $categoryListUrl }}"
                        @if($isDefault) selected @endif>
                    {{ $categoryTitle }}
                </option>
            @append
            @endforeach
        </ul>
    </nav>
    
    {{--tablet view nav--}}
    <nav class="select-nav is-hidden">
        <select class="select-subnav">
            @yield('tablet-nav-options')
        </select>
    </nav>
@endif
