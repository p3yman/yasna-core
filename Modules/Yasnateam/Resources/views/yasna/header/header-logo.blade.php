{{--header logo--}}

<a href="{{ YasnaTeamRouteTools::actionLocale('YasnateamController@index') }}" class="logo">
    {{--logo image--}}
    @if($logoUrl = Template::siteLogoUrl())
        <span class="logo-icon">
            @include('yasnateam::yasna.frame.widgets.image',[
                'src'=> $logoUrl
            ])
        </span>
    @endif

    {{--site title--}}
    @if($siteTitle = Template::siteTitle())
        <span class="title">{{ $siteTitle }}</span>
    @endif

</a>