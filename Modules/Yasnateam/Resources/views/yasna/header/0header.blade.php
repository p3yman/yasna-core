{{--site header--}}

<header id="fixed-header" class="header-style {{ Template::darkHeader() ? "has-dark-bg" : "" }}">
    
    <div class="container-fluid">
        <div class="header-content">
            @include('yasnateam::yasna.header.header-inner')
        </div>
    </div>

</header>

