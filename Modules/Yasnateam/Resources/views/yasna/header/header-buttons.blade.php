{{--header buttons--}}

@php
	$isIndex = YasnaTeam::isIndex();
@endphp
<div class="{{ Template::darkHeader() ? 'dark-bg' : '' }} header-btns">
	@if (!$isIndex)
		@include('yasnateam::yasna.frame.button-atag',[
			"href"=> YasnaTeamRouteTools::actionLocale('YasnateamController@index'),
			"class"=>"btn-login",
			"text"=>trans('yasnateam::yasna.template.home')
		])
	@endif

	@if (get_setting('blog_link'))
		@include('yasnateam::yasna.frame.button-atag',[
			"href"=> get_setting('blog_link'),
			"class"=>"btn-login",
			"text"=>trans('yasnateam::yasna.template.blog')
		])
	@endif

	@include('yasnateam::yasna.frame.button-atag', [
		"href"=> ($isIndex ? '' : YasnaTeamRouteTools::actionLocale('YasnateamController@index')) . '#about',
		"class"=>"btn-login",
		"text"=>trans('yasnateam::yasna.template.about_us'),
		"target" => "" ,
	])

	@include('yasnateam::yasna.frame.button-atag',[
		"href"=> "#contact",
		"class"=>"start-btn",
		"text"=>trans('yasnateam::yasna.template.contact_us'),
		"target" => "" ,
	])

</div>
