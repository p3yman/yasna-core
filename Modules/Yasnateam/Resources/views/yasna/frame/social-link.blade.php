
{{--social links--}}

<a class="link-icon" href="{{ $link or ""}}" target="_blank">
    <i class="fa fa-{{ $class or ""}}" aria-hidden="true"></i>
</a>