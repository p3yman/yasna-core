@php
	$contact_info = Template::contactInfo();
@endphp

<div class="message">
	<h5 class="header">
		{{ trans('yasnateam::yasna.dialog.title') }}
	</h5>

	@if(isset($contact_info['telephone']) and $contact_info['telephone'])
	<div class="contact-info ltr">
		<i class="fa fa-phone"></i>

		@foreach($contact_info['telephone'] as $telephone)
			<a class="info"  href="{{ "tel:".$telephone }}">
				{{ pd($telephone) }}
			</a>
			@if($loop->iteration !== $loop->count)
				<span> | </span>
			@endif
		@endforeach
	</div>
	@endif


	@if(isset($contact_info['email']) and $contact_info['email'])
	<div class="contact-info ltr">
		<i class="fa fa-envelope"></i>
		@foreach($contact_info['email'] as $email)
			<a class="info" href="{{ "mailto:".$email }}">
				{{ $email }}
			</a>
			@if($loop->iteration !== $loop->count)
				<span> | </span>
			@endif
		@endforeach
	</div>
	@endif

	@if(isset($contact_info['address']) and $contact_info['address'])
	<div class="contact-info">
		<i class="fa fa-map-marker"></i>
		<span class="info">
			{{ $contact_info['address'] }}
		</span>
	</div>
	@endif

	{{--<div class="contact-info call-to-action">--}}
		{{--<a href="#">--}}
			{{--{{ trans('yasnateam::yasna.dialog.order') }}--}}
		{{--</a>--}}
	{{--</div>--}}
</div>