@extends('yasnateam::yasna.frame.0base')

{{-- html head general assets --}}


{{--header general assets--}}
@section('header_assets')
    
    <!-- Bootstrap -->
    {!! Html::style(Module::asset('yasnateam:libs/bootstrap/bootstrap.min.css')) !!}
    
    @if(isLangRtl())
        <!-- Bootstrap rtl -->
        {!! Html::style(Module::asset('yasnateam:libs/bootstrap/bootstrap.rtl.min.css')) !!}
    @endif
    
    <!-- Fonts -->
    {!! Html::style(Module::asset('yasnateam:fonts/opensans/stylesheet.css')) !!}
    
    @if(isLangRtl())
        {!! Html::style(Module::asset('yasnateam:css/fontiran.css')) !!}
    @endif
    {!! Html::style(Module::asset('yasnateam:libs/font-awesome/css/font-awesome.min.css')) !!}
    
    
    <!-- General style -->
    {!! Html::style(Module::asset('yasnateam:css/style.min.css')) !!}

    {{--Other header links--}}
    @yield('header-links')
    
    {{--custom rtl stylesheet--}}
    {{--always should be the last stylesheet--}}
    @if(isLangRtl())
        {!! Html::style(Module::asset('yasnateam:css/typography.rtl.css')) !!}
    @endif
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@endsection



{{-- end body general scripts --}}
@section('footer_assets')
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    {!! Html::script(Module::asset('yasnateam:js/jquery-3.2.1.min.js')) !!}
    
    <!-- Bootstrap -->
    {!! Html::script(Module::asset('yasnateam:libs/bootstrap/bootstrap.min.js')) !!}
    
    
    {{--other scripts --}}

    @yield('endbody-script')

    {{--main script--}}
    {{--always should be the last--}}
    {!! Html::script(Module::asset('yasnateam:js/tools.js')) !!}
    {!! Html::script(Module::asset('yasnateam:js/custom.js')) !!}
    {!! Html::script(Module::asset('yasnateam:js/dialog.js')) !!}


@endsection


{{-- Body Joint sections and tags in whole project--}}
@section('body')
    <div class="wrapper">
        {{--Other body sections--}}
        @yield('inner-body')
        
        {{--contact section - Modifies based on the page route--}}
        @include('yasnateam::yasna.contact.0contact')
        
        {{--footer section - contains social links and credit --}}
        @include('yasnateam::yasna.footer.0footer')
        
        {{--changing language modal window--}}
        @include('yasnateam::yasna.lang.0lang')

        {{--Sticky Dialog--}}
        @include('yasnateam::yasna.frame.sticky-dialog.dialog')
    </div>
@endsection
