<div class="alert alert-{{ $type or 'danger' }}">
    {{ $message or "" }}
</div>