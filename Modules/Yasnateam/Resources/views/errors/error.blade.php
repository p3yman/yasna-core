@extends('yasnateam::yasna.frame.0base')

@section('header_assets')
    @if(isLangRtl())
        <!-- Fonts -->
        {!! Html::style(Module::asset('yasnateam:css/fontiran.css')) !!}
    @else
        <!-- Fonts -->
        {!! Html::style(Module::asset('yasnateam:fonts/opensans/stylesheet.css')) !!}
    @endif
    {!! Html::style(Module::asset('yasnateam:libs/font-awesome/css/font-awesome.min.css')) !!}
    {!! Html::style(Module::asset('yasnateam:css/error-page.min.css')) !!}
@stop

@section('body')
    <div class="error-container">
        <nav class="navbar">
            @if($logoUrl = Template::siteLogoUrl())
                <div class="logo">
                    <a href="{{  YasnaTeamRouteTools::actionLocale('YasnateamController@index') }}">
                        <img src="{{ $logoUrl }}" alt="logo">
                    </a>
                </div>
            @endif
        </nav>
        <canvas id="canvas"></canvas>
        <div class="inner-container">
            <h1>{{ $errorCode }}</h1>
            <div class="message">
                {{ $errorMsg }}
            </div>
            <hr>
            
            <div class="links">
                <a href="{{ YasnaTeamRouteTools::actionLocale('YasnateamController@index') }}">
                    {{ trans('yasnateam::yasna.template.home') }}
                </a>
                @if (($posttypes = Template::posttypes()) and array_key_exists('blog', $posttypes) and $posttypes['blog']->exists)
                    <span> | </span>
                    <a href="{{ YasnaTeamRouteTools::actionLocale('YasnateamController@blogPosts') }}">
                        {{ trans('yasnateam::yasna.template.blog') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
    <script>
        // stolen TV noise code
        var canvas = document.getElementById('canvas'),
            ctx = canvas.getContext('2d');

        function resize() {
            canvas.width = window.innerWidth;
            canvas.height = window.innerHeight;
        }

        resize();
        window.onresize = resize;

        function noise(ctx) {
            var w = ctx.canvas.width,
                h = ctx.canvas.height,
                idata = ctx.createImageData(w, h),
                buffer32 = new Uint32Array(idata.data.buffer),
                len = buffer32.length,
                i = 0;

            for (; i < len;)
                buffer32[i++] = ((255 * Math.random()) | 0) << 24;

            ctx.putImageData(idata, 0, 0);
        }

        var toggle = true;

        // added toggle to get 30 FPS instead of 60 FPS
        (function loop() {
            toggle = !toggle;
            if (toggle) {
                requestAnimationFrame(loop);
                return;
            }
            noise(ctx);
            requestAnimationFrame(loop);
        })();
    </script>
@stop