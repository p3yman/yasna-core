@extends('yasnateam::errors.error')

@php
    $errorCode = 404;
    $errorMsg = trans('yasnateam::general.message.page-not-found');
@endphp