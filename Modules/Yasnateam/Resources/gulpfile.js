/*-----------------------------------------------------------------
- Yasna Gulp Workflow
-----------------------------------------------------------------*/

// Load Plugins
//---------------------------------------------------------------
var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    cssnano         = require('gulp-cssnano'),
    autoprefixer    = require('gulp-autoprefixer'),
    rename          = require('gulp-rename'),
    newer           = require('gulp-newer'),
    csscomb         = require('gulp-csscomb'),
    rtlcss          = require('gulp-rtlcss');


// Directories
//---------------------------------------------------------------
var src_dir         = './',
    dist_dir        = '../Assets/',

    css = {
        in:     src_dir + 'sass/**/*.scss',
        out:    dist_dir + 'css/',
    },

    js = {
        in:     src_dir + 'js/**/*.js',
        out:    dist_dir + 'js/',
    };

// Tasks
//---------------------------------------------------------------
gulp.task('styles', function() {
    return gulp.src(css.in)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer("last 5 version"))
        .pipe(sourcemaps.write())
        .pipe( gulp.dest( css.out ) )
        // .pipe(rtlcss())
        // .pipe(rename({ suffix: '-rtl' }))
        // .pipe( gulp.dest( css.out ) );
});

gulp.task('scripts', function () {
    return gulp.src(js.in)
        .pipe(newer(js.out))
        .pipe(gulp.dest(js.out));
});

gulp.task('default', ['styles', 'scripts'], function() {

    gulp.watch( css.in, ['styles']  );
    gulp.watch( js.in, ['scripts'] );

});