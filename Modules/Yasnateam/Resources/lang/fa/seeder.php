<?php
return [
     "features" => [
          "customer" => "مشتری",
     ],

     "posttypes" => [
          "customers" => [
               "plural"   => "مشتری‌ها",
               "singular" => "مشتری",
          ],
     ],
];
