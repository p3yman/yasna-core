<?php
return [
	'main' => [
		"what_we_do"   => "چه می‌کنیم؟",
		"what_we_done" => "چه کرده‌ایم؟",
		"why_us"       => "چرا ما؟",
		"who_we_are"   => "که هستیم؟",
		"where_we_are" => "کجا هستیم؟",
		"blog"         => "بلاگ",
	],
	
	'footer' => [
		"blog"     => "بلاگ",
		"about"    => "درباره ما",
		"contact"  => "تماس با ما",
		"yasnaweb" => "یسناوب",
		"faq"      => "پرسش‌های متداول",
		"jobs"     => "فرصت‌های شغلی",
	],
];
