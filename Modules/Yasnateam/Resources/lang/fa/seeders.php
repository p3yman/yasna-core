<?php
return [
     "commenting_plural"   => "پیام‌گیرها",
     "commenting_singular" => "پیام‌گیر",
     "contact_us"          => "تماس با ما",
];
