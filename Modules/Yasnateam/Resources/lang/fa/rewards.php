<?php
return [
     "title"            => "نامه اعمال",
     "create"           => "جدید",
     "edit"             => "ویرایش",
     "star"             => "ستاره",
     "delay-warning"    => "کارت نارنجی",
     "general-warning"  => "کارت زرد",
     "crime"            => "جریمه",
     "award"            => "جایزه",
     "weight"           => "وزن",
     "name-required"    => "نام شخص را از فهرست انتخاب کنید.",
     "subject-required" => "موضوع را از فهرست انتخاب کنید.",
     "forgivable"       => "قابل بخشش",
     "forgive"          => "بخشش :)",
     "forgiven"         => "بخشیده‌شده",
];
