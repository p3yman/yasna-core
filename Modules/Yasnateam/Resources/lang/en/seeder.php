<?php
return [
     "features" => [
          "customer" => "Customer",
     ],

     "posttypes" => [
          "customers" => [
               "plural"   => "Customers",
               "singular" => "Customer",
          ],
     ],
];
