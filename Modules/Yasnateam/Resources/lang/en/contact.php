<?php
return [
	'intro' => 'Contact us. We will respond.',
	
	'name'    => 'Name',
	'mobile'  => 'Mobile',
	'phone'   => 'Phone',
	'email'   => 'Email',
	'subject' => 'Subject',
	'message' => 'Your message',
	
	'send' => 'Send',

     "feed_success" => "Your message has been submitted.",
     "feed_danger"  => "Failed to send the message! Please try again later.",

];
