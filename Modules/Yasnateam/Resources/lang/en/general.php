<?php

return [
    "language" => [
        "en" => "English",
        "fa" => "Persian",
    ],

    "message" => [
        "page-not-found" => "Page not found.",
        "no-post-found"  => "No content found.",
    ],

    "page_description" => [
         "home"        => "",
         "faq"         => "",
         "contact"     => "",
         "blog"        => "",
         "jobs_apply"  => "",
    ],
];
