<?php
return [

     'template' => [
          'login'         => 'Login',
          "get_started"   => "Get Started",
          'read_more'     => 'Read More',
          'view_project'  => 'Veiw Project',
          'view_product'  => 'view product',
          "contact_us"    => "Contact Us",
          "about_us"      => "About Us",
          "yasna_group"   => "Yansa Group",
          "related_links" => "related links",
          "credit"        => "copyright 2017 :owner",
          "languages"     => "languages",
          "by"            => "by",
          "ondate"        => "on",
          "services"      => 'Services',
          "our_customers" => "Our customers",
          "why_us"        => 'why us?',
          "blog"          => 'blog',
          "home"          => 'home',
          "show_yasna"    => '#YasnaTeam',
          "achievements"  => [
               "experience"  => [
                    "additive"   => " years",
                    "main_title" => "Experience",
                    "subtitle"   => "Experience",
               ],
               "development" => [
                    "additive"   => "+",
                    "main_title" => "Development team",
                    "subtitle"   => "Development team",
               ],
               "design"      => [
                    "additive"   => "",
                    "main_title" => "Design team",
                    "subtitle"   => "Design team",
               ],
               "projects"    => [
                    "additive"   => "+",
                    "main_title" => "Projects",
                    "subtitle"   => "Projects",
               ],
               "customers"   => [
                    "additive"   => "+",
                    "main_title" => "Customers",
                    "subtitle"   => "Customers",
               ],
          ],
          "what_we_use"   => "Technologies we use",
     ],

     "form" => [
          "send" => "send",
     ],

     "dialog" => [
          "title" => "Contact Us",
          "order" => "Order",
     ],

];
