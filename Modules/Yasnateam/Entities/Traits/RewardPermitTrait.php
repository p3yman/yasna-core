<?php namespace Modules\Yasnateam\Entities\Traits;

trait RewardPermitTrait
{
    /**
     * @return bool
     */
    public function canEditTemporarily()
    {
        return (user()->id == $this->created_by) and $this->created_at > now()->subMinutes(5);
    }



    /**
     * @return bool
     */
    public function canEdit()
    {
        return $this->hasPermit('edit') or $this->canEditTemporarily();
    }



    /**
     * @return bool
     */
    public function cannotEdit()
    {
        return !$this->canEdit();
    }



    /**
     * @return bool
     */
    public function canDelete()
    {
        return $this->hasPermit('delete') or $this->canEditTemporarily();
    }



    /**
     * @return bool
     */
    public function cannotDelete()
    {
        return !$this->canDelete();
    }



    /**
     * @return bool
     */
    public function canCreate()
    {
        return $this->hasPermit('create');
    }



    /**
     * @return bool
     */
    public function cannotCreate()
    {
        return !$this->canCreate();
    }



    /**
     * @return bool
     */
    public function canForgive()
    {
        return $this->canCreate();
    }



    /**
     * @return bool
     */
    public function cannotForgive()
    {
        return !$this->canForgive();
    }



    /**
     * @param $permit
     *
     * @return bool
     */
    public function hasPermit($permit)
    {
        return user()->as('admin')->can("rewards.$permit");
    }



    /**
     * @param $permit
     *
     * @return bool
     */
    public function hasnotPermit($permit)
    {
        return !$this->hasPermit($permit);
    }



    /**
     * @return bool
     */
    public function forgive()
    {
        if ($this->isForgivable()) {
            return $this->batchSaveBoolean([
                 "is_forgiven" => "1",
            ]);
        }

        return false;
    }
}
