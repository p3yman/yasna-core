<?php namespace Modules\Yasnateam\Entities\Traits;

trait RewardElectorTrait
{
    /**
     * @param $id
     */
    public function electorUser($id)
    {
        $this->electorFieldId($id, 'user_id');
    }



    /**
     * @param $id
     */
    public function electorReason($id)
    {
        $this->electorFieldId($id, 'reason_id');
    }



    /**
     * @param $group_slug
     */
    public function electorGroup($group_slug)
    {
        $this->electorFieldValue('group', $group_slug);
    }



    /**
     * @param $is_settled
     */
    public function electorSettlement($is_settled)
    {
        $this->electorFieldValue('is_settled', $is_settled);
    }



    /**
     * @param $is_forgiven
     */
    public function electorForgiveness($is_forgiven)
    {
        $this->electorFieldValue('is_forgiven', $is_forgiven);
    }
}
