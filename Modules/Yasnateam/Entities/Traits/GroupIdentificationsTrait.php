<?php namespace Modules\Yasnateam\Entities\Traits;

trait GroupIdentificationsTrait
{
    /**
     * @return bool
     */
    public function isStar()
    {
        return boolval($this->group == 'star');
    }



    /**
     * @return bool
     */
    public function isWarning()
    {
        return str_contains($this->group, 'warning');
    }



    /**
     * check if the model is of award type
     *
     * @return bool
     */
    public function isAward()
    {
        return boolval($this->group == 'award');
    }
}
