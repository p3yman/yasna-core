<?php namespace Modules\Yasnateam\Entities\Traits;

/**
 * Class RewardForgivenessTrait
 *
 * @package Modules\Yasnateam\Entities\Traits
 * @property     $effected_at
 * @property int $user_id
 * @property int $is_forgiven
 */
trait RewardForgivenessTrait
{
    /**
     * @return bool
     */
    public function isForgivable()
    {
        return $this->isNotForgiven() and
             $this->isWarning() and
             $this->isTimeBarred() and
             $this->isNotRepeatedAgain();
    }



    /**
     * @return bool
     */
    private function isTimeBarred()
    {
        return boolval($this->effected_at < now()->subWeek());
    }



    /**
     * @return bool
     */
    private function isRepeatedAgain()
    {
        $repeats = $this
             ->where('user_id', $this->user_id)
             ->where('effected_at', '>', $this->effected_at)
             ->where('is_settled', false)
             ->where('is_forgiven', false)
             ->where('group', $this->group)
             ->count()
        ;

        return boolval($repeats);
    }



    /**
     * @return bool
     */
    private function isNotRepeatedAgain()
    {
        return !$this->isRepeatedAgain();
    }



    /**
     * @return bool
     */
    public function isForgiven()
    {
        return boolval($this->is_forgiven);
    }



    /**
     * @return bool
     */
    public function isNotForgiven()
    {
        return !$this->isForgiven();
    }
}
