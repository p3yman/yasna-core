<?php namespace Modules\Yasnateam\Entities\Traits;

use App\Models\Reward;
use App\Models\RewardReason;

/**
 * Class RewardReasonTrait
 *
 * @package Modules\Yasnateam\Entities\Traits
 * @property RewardReason $reason
 * @property string       $group
 */
trait RewardReasonTrait
{
    private $cached_reason = false;
    use GroupIdentificationsTrait;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reason()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "RewardReason");
    }



    /**
     * @return bool|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getReasonAttribute()
    {
        if ($this->cached_reason) {
            return $this->cached_reason;
        }

        return $this->reason()->first();
    }



    /**
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->reason->title . " (" . $this->creator->full_name . ") ";
    }



    /**
     * @return mixed
     */
    public function refreshGroupCache()
    {
        $this->suppressMeta();
        $this->group = $this->reason->group;
        return $this->save();
    }



    /**
     * @return int
     */
    public function refreshGroupCacheForAllRecords()
    {
        /**
         * @var  Reward $record
         */
        $total = 0;

        foreach ($this->build()->withTrashed()->get() as $record) {
            $total += $record->refreshGroupCache();
        }

        return $total;
    }
}
