<?php

namespace Modules\Yasnateam\Entities\Traits;

trait CommentYasnateamTrait
{
    /**
     * Returns an array of meta fields for file.
     *
     * @return array
     */
    protected function fileMetaFields()
    {
        return [
             'resume',
             'gender',
             'marriage',
             'bd',
             'address',
             'tel',
             'insurance',
             'experience',
             'edu_degree',
             'edu_field',
             'english[reading]',
             'english[writing]',
             'english[speaking]',
             'english[degree]',
             'salary',
             'available_date',
        ];
    }



    /**
     * Elector for subject
     *
     * @param  string $subject
     */
    public function electorSubject($subject)
    {
        $this->elector($subject, 'jobs-apply-form-subject');
    }
}
