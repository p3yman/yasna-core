<?php

namespace Modules\Yasnateam\Entities\Traits;

trait PostCustomerTrait
{
    /**
     * The Meta Fields for the Customer Posts
     *
     * @return array
     */
    public function customerMetaFields()
    {
        if ($this->hasnot('customer')) {
            return [];
        }

        return ['project_date', 'project_version'];
    }
}
