<?php

namespace Modules\Yasnateam\Entities\Traits;

trait YasnateamPostTrait
{
    /**
     * get direct url of the model.
     *
     * @return string
     */
    public function getDirectUrlAttribute()
    {
        switch ($this->type) {
            case "blog":
                return "blog/internal/$this->id";

            case "jobs":
                return "jobs/$this->id";
        }

        return null;
    }



    /**
     * get the direct link of the model.
     *
     * @return string
     */
    public function getDirectLinkAttribute()
    {
        return $this->getDirectUrlAttribute();
    }
}
