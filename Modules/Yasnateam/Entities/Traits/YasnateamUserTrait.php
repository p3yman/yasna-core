<?php

namespace Modules\Yasnateam\Entities\Traits;

use App\Models\Post;
use App\Models\Receipt;
use Nwidart\Modules\Facades\Module;

trait YasnateamUserTrait
{
    public function gravatar(string $default = null, $size = 100)
    {
        $default = $default ?: Module::asset('yasnateam:images/user-3.png');
        return $this->email
             ? "https://www.gravatar.com/avatar/"
             . md5(strtolower(trim($this->email)))
             . "?d="
             . urlencode(trim($default, '/'))
             . "&s="
             . $size
             : $default;
    }



    public function getGravatarAttribute()
    {
        return $this->gravatar();
    }

}
