<?php namespace Modules\Yasnateam\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasnateam\Entities\Traits\GroupIdentificationsTrait;

class RewardReason extends YasnaModel
{
    use SoftDeletes;
    use GroupIdentificationsTrait;

    protected $guarded = ["id"];



    /**
     * @return array
     */
    public function availableGroups()
    {
        return [
             'star',
             'delay-warning',
             'general-warning',
             'award',
        ];
    }



    /**
     * @return array
     */
    public function warningGroups()
    {
        $array = [];

        foreach ($this->availableGroups() as $group) {
            if (str_contains($group, 'warning')) {
                $array[] = $group;
            }
        }

        return $array;
    }



    /**
     * @param null $group_slug
     *
     * @return string
     */
    public function groupName($group_slug = null)
    {
        if (!$group_slug) {
            $group_slug = $this->group;
        }

        return trans_safe("yasnateam::rewards.$group_slug");
    }



    /**
     * @param null $group_slug
     */
    public function groupIcon($group_slug = null)
    {
        if (!$group_slug) {
            $group_slug = $this->group;
        }

        return config("yasnateam.group-icon.$group_slug");
    }



    /**
     * @param null $group_slug
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function groupColor($group_slug = null)
    {
        if (!$group_slug) {
            $group_slug = $this->group;
        }

        return config("yasnateam.group-color.$group_slug");
    }



    /**
     * @return array
     */
    public function groupCombo()
    {
        $array = [];

        foreach ($this->availableGroups() as $group) {
            $array[] = [
                 $group,
                 $this->groupName($group),
            ];
        }

        return $array;
    }



    /**
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->groupName() . ": " . $this->text;
    }
}
