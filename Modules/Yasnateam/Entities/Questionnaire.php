<?php

namespace Modules\Yasnateam\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Questionnaire extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];



    /**
     * Relationship with QuestionnaireKeyPair (one to many)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keyPairs()
    {
        return $this->hasMany(\App\Models\QuestionnaireKeyPair::class, 'form_id', 'form_id');
    }
}
