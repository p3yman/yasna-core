<?php namespace Modules\Yasnateam\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasnateam\Entities\Traits\RewardForgivenessTrait;
use Modules\Yasnateam\Entities\Traits\RewardPermitTrait;
use Modules\Yasnateam\Entities\Traits\RewardReasonTrait;
use Modules\Yasnateam\Entities\Traits\RewardElectorTrait;

class Reward extends YasnaModel
{
    use SoftDeletes;
    use RewardForgivenessTrait;
    use RewardReasonTrait;
    use RewardPermitTrait;
    use RewardElectorTrait;

    protected $guarded = ["id"];



    /**
     * @return array
     */
    protected function mainMetaFields()
    {
        return [
             'description',
        ];
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . "User");
    }
}
