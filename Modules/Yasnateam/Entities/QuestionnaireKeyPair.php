<?php

namespace Modules\Yasnateam\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionnaireKeyPair extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];
    protected $table   = "questionnaire_key_pair";



    /**
     * Relationship with Questionnaire (one to many inverse)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function questionnaire()
    {
        return $this->belongsTo(\App\Models\Questionnaire::class, 'form_id', 'form_id');
    }
}
