<?php

namespace Modules\Yasnateam\Providers;

use Modules\Remark\Events\RemarkAdded;
use Modules\Remark\Events\RemarkReplied;
use Modules\Yasna\Services\YasnaProvider;
use Modules\Yasnateam\Listeners\SendSmsWhenTicketAdd;
use Modules\Yasnateam\Listeners\SendSmsWhenTicketReply;
use Modules\Yasnateam\Services\Template\YasnateamTemplateHandler;

class YasnateamServiceProvider extends YasnaProvider
{


    /**
     * Boot the application events.
     *
     * @return void
     */
    public function index()
    {
        $this->addAliases();
        $this->registerModelTraits();
        $this->registerManageSidebar();
        $this->registerRoleSampleModules();
        $this->registerDownstream();
        $this->sideBar();
        $this->registerPostHandlers();
        $this->registerPostSaveGates();
        $this->registerListeners();
    }



    /**
     * Downstream Settings
     */
    private function registerDownstream()
    {
        module('manage')
             ->service('downstream')
             ->add('rewards')
             ->link('rewards')
             ->caption("trans:yasnateam::rewards.title")
             ->method('yasnateam:RewardsDownstreamController@index')
             ->order(11)
        ;
    }



    /**
     * Module string, to be used as sample for role definitions.
     */
    private function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('rewards')
             ->value('browse ,  create ,  edit ,  delete ,  bin ,  settings')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('resumes')
             ->value('browse ,  delete')
        ;
    }



    /**
     * Aliases
     */
    protected function addAliases()
    {
        $this->addAlias("Template", YasnateamTemplateHandler::class);
        $this->addAlias("YasnaTeam", self::class);
        $this->addAlias("YasnaTeamRouteTools", RouteServiceProvider::class);
    }



    /**
     * Model Traits
     */
    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait("Yasnateam:YasnateamUserTrait")->to("User")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait("Yasnateam:YasnateamPostTrait")->to("Post")
        ;

        $this->addModelTrait('PostCustomerTrait', 'Post');
        $this->addModelTrait('CommentYasnateamTrait', 'Comment');
    }



    /**
     * Manage Side Bar
     */
    private function registerManageSidebar()
    {
        service('manage:sidebar')
             ->add('rewards')
             ->blade('yasnateam::sidebar')
             ->order(1)
        ;

        service('manage:sidebar')
             ->add('resume')
             ->blade('yasnateam::yasna-new.resume.sidebar')
             ->order(1)
        ;
    }



    /**
     * @return bool
     */
    public static function isIndex()
    {
        return strpos(request()->route()->getActionName(), 'YasnateamController@index') !== false;
    }



    /**
     * @param $lang
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function getLanguageTitle($lang)
    {
        return trans("yasnateam::general.language.$lang", [], $lang);
    }



    /**
     * set sidebar menu for questionnaire grid
     */
    public function sideBar()
    {
        service('manage:sidebar')
             ->add('questionnaire')
             ->blade('yasnateam::yasna.questionnaire-grid.sidebar')
             ->order(10)
        ;
    }



    /**
     * Registers the handlers for the posts.
     */
    protected function registerPostHandlers()
    {
        service('posts:editor_handlers')
             ->add('yasnateam')
             ->method('yasnateam:PostHandlerController@editor')
        ;
    }



    /**
     * Registers the save gates for the posts.
     */
    protected function registerPostSaveGates()
    {
        service('posts:save_gates')
             ->add('yasnateam')
             ->method('yasnateam:PostHandlerController@saveGate')
        ;
    }



    /**
     * register listeners of this module.
     */
    protected function registerListeners()
    {
        $this->listen(RemarkAdded::class, SendSmsWhenTicketAdd::class);
        $this->listen(RemarkReplied::class, SendSmsWhenTicketReply::class);
    }

}
