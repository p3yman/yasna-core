<?php

namespace Modules\Yasnateam\Providers;

use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected static $controllersNamespace = '\Modules\Yasnateam\Http\Controllers\\';

    public static $postDirectUrlPrefix = 'P';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function action(string $action, ...$otherParameters)
    {
        return action(self::$controllersNamespace . $action, ...$otherParameters);
    }

    public static function actionLocale(string $action, ...$otherParameters)
    {
        return action_locale(self::$controllersNamespace . $action, ...$otherParameters);
    }
}
