<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}


if (!function_exists('isIndex')) {
    /**
     * Checks if on index page
     *
     * @return boolean
     */
    function isIndex()
    {
        $route = app('request')->route()->getAction();

        return strpos($route['controller'], 'YasnateamController@index');
    }
}

if (!function_exists('templateData')) {
    function templateData()
    {
        $switches = [];

        // login btn href
        $switches['login'] = url('/login');

        return $switches;
    }
}


if (!function_exists('postImage')) {

    /**
     * @param $hashid
     *
     * @return \Modules\Filemanager\Services\Tools\Doc
     */
    function postImage($hashid)
    {
        return fileManager()->file($hashid);
    }
}

if (!function_exists('rewardReason')) {

    /**
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return \App\Models\RewardReason
     */
    function rewardReason($id = 0, $with_trashed = false)
    {
        return model('reward-reason', $id, $with_trashed);
    }
}


if (!function_exists('reward')) {

    /**
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return \App\Models\Reward
     */
    function reward($id = 0, $with_trashed = false)
    {
        return model('reward', $id, $with_trashed);
    }
}