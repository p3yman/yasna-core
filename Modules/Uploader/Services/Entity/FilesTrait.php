<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/26/18
 * Time: 4:07 PM
 */

namespace Modules\Uploader\Services\Entity;


use App\Models\UploaderFile;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Uploader\Services\Finder\FileFinder;


/**
 * Trait FilesTrait
 * <br>
 * The trait can be added to any model to work with its files.
 */
trait FilesTrait
{
    /**
     * Standard One-to-Many Relation with Files
     *
     * @param string|array $groups
     *
     * @return HasMany
     */
    public function files($groups = [])
    {
        $builder = $this->hasMany(UploaderFile::class, 'model_id')
                        ->where('model_class', $this->reflector()->getName())
        ;

        // apply groups if specified
        if (!empty($groups)) {
            $builder->withGroups($groups);
        }

        return $builder;
    }



    /**
     * Standard One-to-Many Relation with Files with the Specified Group
     *
     * @param string|array $groups
     *
     * @return HasMany
     */
    public function filesAs($groups)
    {
        return $this->files($groups);
    }



    /**
     * Attaches the given file to this model.
     *
     * @param UploaderFile|array|string|int $files
     * @param string|null                   $group_name
     *
     * @return int
     */
    public function attachFile($files, ?string $group_name = null): int
    {
        // find the ids of the existed files.
        $finder = new FileFinder($files);
        $ids    = $finder->getInstances()->pluck('id');

        return uploader()
             ->file()
             ->whereIn('id', $ids)
             ->attachTo($this, $group_name)
             ;
    }



    /**
     * Attaches the given file to this model with the specified group.
     *
     * @param UploaderFile|array|string|int $files
     * @param string                        $group_name
     *
     * @return int
     */
    public function attachFileAs(UploaderFile $files, string $group_name): int
    {
        return $this->attachFile($files, $group_name);
    }
}
