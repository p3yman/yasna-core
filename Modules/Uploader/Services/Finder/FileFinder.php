<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/19/19
 * Time: 1:41 PM
 */

namespace Modules\Uploader\Services\Finder;

use App\Models\UploaderFile;
use Illuminate\Support\Collection;

/**
 * Class FileFinder
 * <br>
 * A tool to find instances of a group of file with any type of identifiers.
 */
class FileFinder
{
    /**
     * @var Collection
     */
    protected $instances;



    /**
     * FileFinder constructor.
     *
     * @param UploaderFile|array|string|int ...$files
     */
    public function __construct(...$files)
    {
        $this->setInstances($files);
    }



    /**
     * Fills the `$instances` property with a collection of files instances.
     *
     * @param array $instances
     */
    protected function setInstances(array $instances)
    {
        $this->instances = collect($instances)
             ->flatten()
             ->map(function ($file_identifier) {
                 $instance = $this->getFileInstance($file_identifier);
                 if ($instance->not_exists) {
                     return null;
                 }

                 return $instance;
             })
             ->filter()
        ;
    }



    /**
     * Returns the file instance based on the specified identifier.
     *
     * @param UploaderFile $identifier
     *
     * @return UploaderFile
     */
    protected function getFileInstance($identifier)
    {
        $model_class = $this->getModelClass();
        if ($identifier instanceof $model_class) {
            return $identifier;
        }

        return uploader()->file($identifier);
    }



    /**
     * Returns the full namespace of the instances class.
     *
     * @return string
     */
    protected function getModelClass()
    {
        return get_class(uploader()->file());
    }



    /**
     * Returns the value of the `$instance` property.
     *
     * @return Collection
     */
    public function getInstances()
    {
        return $this->instances;
    }
}
