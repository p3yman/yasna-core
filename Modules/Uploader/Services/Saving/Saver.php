<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/21/18
 * Time: 12:25 PM
 */

namespace Modules\Uploader\Services\Saving;


use App\Models\UploaderFile;
use Illuminate\Http\UploadedFile;
use Modules\Uploader\Services\Saving\Traits\SaverImageTrait;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Symfony\Component\HttpFoundation\File\File as FileFoundation;

class Saver
{
    use ModuleRecognitionsTrait;
    use SaverImageTrait;

    /**
     * The File to be processed
     *
     * @var UploadedFile
     */
    protected $file;

    /**
     * The Saved File's Instance
     *
     * @var FileFoundation
     */
    protected $saved_file;

    /**
     * The Data to be Stored in the Database
     *
     * @var array
     */
    protected $data = [];
    /**
     * An Array of Versions
     *
     * @var array
     */
    protected $versions = [];
    /**
     * Whether the file should be uploaded in the private directory or not.
     *
     * @var bool
     */
    protected $private = true;

    /**
     * The Separator of the Name Parts.
     */
    const FILE_NAME_SEPARATOR = '_';

    /**
     * The file configs.
     */
    public $configs = [];


    /**
     * The Separator of the Parts of the Name of the Versions
     */
    const FILE_VERSION_PARTs_SEPARATOR = '-';



    /**
     * Saver constructor.
     *
     * @param UploadedFile $file
     * @param bool         $private
     * @param array        $configs
     */
    public function __construct(UploadedFile $file, bool $private = true, $configs = [])
    {
        $this->file    = $file;
        $this->private = $private;
        $this->configs = $configs;
    }



    /**
     * Does the store process.
     *
     * @return UploaderFile
     */
    public function save()
    {
        $family_name = $this->generateName();
        $extension   = $this->file->getClientOriginalExtension();
        $file_name   = $family_name . '.' . $extension;

        $this->data = [
             'original_name' => $this->file->getClientOriginalName(),
             'extension'     => $this->file->getClientOriginalExtension(),
             'mime_type'     => $this->file->getMimeType(),
             'family_name'   => $family_name,
             'file_name'     => $file_name,
             'is_private'    => $this->private,
        ];


        $this->uploadFile();
        $this->callVersionSavers();

        return uploader()->fileModel()->batchSave($this->saveData());
    }



    /**
     * Uploads the file.
     */
    protected function uploadFile()
    {
        $directory = $this->getUploadDirectory();
        $real_name = $this->data['family_name'];
        $extension = $this->data['extension'];
        $file_name = $real_name . '.' . $extension;
        $pathname  = $directory . DIRECTORY_SEPARATOR . $file_name;

        $this->file->move($directory, $file_name);

        $this->saved_file = new FileFoundation($pathname);
    }



    /**
     * Generates the new file's name.
     *
     * @return string
     */
    protected function generateName()
    {
        return time()
             . static::FILE_NAME_SEPARATOR
             . str_random(40);
    }



    /**
     * Returns the upload directory.
     *
     * @return string
     */
    protected function getUploadDirectory()
    {
        return uploader()->uploadDirectory($this->private);
    }



    /**
     * Calls teh version save methods.
     */
    protected function callVersionSavers()
    {
        $version_saving_methods = $this->versionSavingMethods();

        foreach ($version_saving_methods as $method) {
            $this->$method();
        }
    }



    /**
     * Returns an array of version saver methods.
     *
     * @return array
     */
    protected function versionSavingMethods()
    {
        return array_filter($this->getAllClassMethods(), function (string $method_name) {
            return (
                 starts_with($method_name, 'save')
                 and ends_with($method_name, 'Versions')
            );
        });
    }



    /**
     * Returns an array of all methods of this class.
     *
     * @return array
     */
    protected function getAllClassMethods()
    {
        return get_class_methods($this);
    }



    /**
     * Returns the array to be saved in the `files` table.
     *
     * @return array
     */
    protected function saveData()
    {
        $data = $this->data;

        $data['versions'] = $this->versions;

        return $data;
    }
}
