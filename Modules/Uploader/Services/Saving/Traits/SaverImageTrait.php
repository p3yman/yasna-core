<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/21/18
 * Time: 12:32 PM
 */

namespace Modules\Uploader\Services\Saving\Traits;


use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image as ImageFacade;
use Intervention\Image\Image;

trait SaverImageTrait
{
    /**
     * The Main Image Instance to Work With
     *
     * @var Image
     */
    protected $image;

    /**
     * The Image Instance which is processing
     *
     * @var Image
     */
    protected $processing_image;



    /**
     * Saves the versions of a saving image file.
     */
    protected function saveImageVersions()
    {
        if ($this->isNotImage()) {
            return;
        }

        try {
            $this->image = ImageFacade::make($this->saved_file->getPathname());
        } catch (NotReadableException $exception) {
            return;
        }
        $this->createNeededVersions();

    }



    /**
     * Checks if the saved file is an image.
     *
     * @return bool
     */
    protected function isImage()
    {
        return starts_with($this->saved_file->getMimeType(), 'image');
    }



    /**
     * Checks if the saved file is not an image.
     *
     * @return bool
     */
    protected function isNotImage()
    {
        return !$this->isImage();
    }



    /**
     * Creates the needed versions of a saved image file.
     */
    protected function createNeededVersions()
    {
        $versions_info = $this->getImageVersionsArray();

        foreach ($versions_info as $version_info) {
            $title  = $version_info['title'];
            $width  = ($version_info['width'] ?? null);
            $height = ($version_info['height'] ?? null);

            $this->createNewVersionOfImage($title, $width, $height);
        }
    }



    /**
     * Creates a new version of a saved image file with the specified title and dimensions.
     *
     * @param string    $title
     * @param int|float $width
     * @param int|float $height
     */
    protected function createNewVersionOfImage(string $title, $width, $height)
    {
        $this->processing_image = clone $this->image;

        if ($this->safeResizeProcessingImage($width, $height)) {
            $this->saveProcessingImage($title);
        }
    }



    /**
     * Saves the processing image with the specified title.
     *
     * @param string $title
     */
    protected function saveProcessingImage(string $title)
    {
        $directory         = $this->image->dirname;
        $extension         = $this->image->extension;
        $original_name     = $this->data['family_name'];
        $version_name      = $original_name . static::FILE_VERSION_PARTs_SEPARATOR . $title;
        $version_file_name = $version_name . '.' . $extension;
        $pathname          = $directory . DIRECTORY_SEPARATOR . $version_file_name;

        $this->processing_image->save($pathname);

        $this->versions[$title] = $version_file_name;
    }



    /**
     * Resizes the processing image with the specified dimensions in a safe model.
     * The safety of the resize means that the resized file will be in a proper dimensions and look fine.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return $this|bool
     */
    public function safeResizeProcessingImage($width, $height)
    {
        if ($width or $height) {
            if ($this->cropImageNeededForDimensions($width, $height)) {
                $this->cropProcessingImage($width, $height);
            }

            $this->resizeProcessingImage($width, $height);
            return $this;
        } else {
            return false;
        }
    }



    /**
     * Checks if the resizing to the specified dimensions needs a primitive crop.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return bool
     */
    protected function cropImageNeededForDimensions($width, $height)
    {
        return ($width and $height);
    }



    /**
     * Crops the processing image to be ready for resizing to the specified dimensions.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return $this
     */
    protected function cropProcessingImage($width, $height)
    {
        list($crop_width, $crop_height) = array_values($this->safeCropImageDimensions($width, $height));

        $this->processing_image->crop($crop_width, $crop_height);

        return $this;
    }



    /**
     * Returns an array of the dimensions for a safe crop before resizing to the specified dimensions.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return array
     */
    protected function safeCropImageDimensions($width, $height)
    {
        $original_width  = $this->image->width();
        $original_height = $this->image->height();

        if ($this->canCropImageWidthBased($width, $height)) {
            $dimensions['width']  = $original_width;
            $dimensions['height'] = floor(($dimensions['width'] * $height) / $width);
        } else {
            $dimensions['height'] = $original_height;
            $dimensions['width']  = floor(($dimensions['height'] * $width) / $height);
        }

        // Sort $dimensions
        return array_replace(array_flip(['width', 'height']), $dimensions);
    }



    /**
     * Checks if the image can be cropped based on the width dimension.
     * If false, the image should be cropped based on the height dimension.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return bool
     */
    protected function canCropImageWidthBased($width, $height)
    {
        $original_width  = $this->image->width();
        $original_height = $this->image->height();
        $ratioHeight     = $height * $original_width / $width;

        if ($ratioHeight <= $original_height) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Resizes the processing image to the specified dimensions.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return $this
     */
    public function resizeProcessingImage($width, $height)
    {
        $resize_width  = $this->resizeProperWidthForProcessingImage($width, $height);
        $resize_height = $this->resizeProperHeightForProcessingImage($width, $height);

        $this->processing_image->resize($resize_width, $resize_height);
        return $this;
    }



    /**
     * Returns a proper width size to resize the image to the specified dimensions.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return int
     */
    protected function resizeProperWidthForProcessingImage($width, $height)
    {
        if ($width) {
            return $width;
        }

        return ((integer)floor(($height * $this->processing_image->width()) / $this->processing_image->height()));
    }



    /**
     * Returns a proper height size to resize the image to the specified dimensions.
     *
     * @param int|float $width
     * @param int|float $height
     *
     * @return int
     */
    protected function resizeProperHeightForProcessingImage($width, $height)
    {
        if ($height) {
            return $height;
        }

        return ((integer)floor(($width * $this->processing_image->height()) / $this->processing_image->width()));
    }



    /**
     * Returns an array of information about versions to be saved of an image file.
     *
     * @return array
     */
    protected function getImageVersionsArray()
    {
        if (isset($this->configs['versions'])) {

            $versions       = [];
            $versions_array = $this->configs['versions'];

            foreach ($versions_array as $key => $version) {
                $versions[$key]['title']  = $this->getDetailVersion($version)[1];
                $versions[$key]['with']   = $this->getDetailVersion($version)[2];
                $versions[$key]['height'] = $this->getDetailVersion($version)[3];
            }
            return array_merge($this->getImageThumb(), $versions);
        } else {
            return $this->getImageThumb();
        }

    }



    /**
     * Returns an array of information about versions to saved of an image thumb.
     *
     * @return array
     */
    protected function getImageThumb()
    {
        return [
             [
                  'title' => 'thumb',
                  'width' => 300,
             ],
        ];
    }



    /**
     * get detail versions
     *
     * @param string $version
     *
     * @return array
     */
    public function getDetailVersion($version)
    {
        preg_match('/(.*):([0-9]+)\*([0-9]+)/', $version, $matches);

        return $matches;
    }
}
