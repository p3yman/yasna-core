<?php

return [
     'name' => 'Uploader',

     'upload-directory-public'    => env('UPLOADER_FILES_DIRECTORY_PUBLIC', public_path('files')),
     'upload-directory-private'   => env('UPLOADER_FILES_DIRECTORY_PRIVATE', public_path('files')),
     "default-max-file-size"      => env('UPLOADER_DEFAULT_MAX_FILE_SIZE', public_path('files')),
     "default-allowed-extensions" => ['jpg', 'jpeg', 'png', 'gif', 'mp3', 'mpga', 'mp4', 'pdf',],
     "default-shared-link-file-serve" => env('UPLOADER_DEFAULT_SHARED_FILE_SERVE', public_path('files'), false),
];
