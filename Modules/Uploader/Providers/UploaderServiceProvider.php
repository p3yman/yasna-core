<?php

namespace Modules\Uploader\Providers;

use App\Models\UploaderFile;
use Illuminate\Database\Eloquent\Collection;
use Modules\Uploader\Components\UploaderComponent;
use Modules\Uploader\Http\Endpoints\V1\ReadEndpoint;
use Modules\Uploader\Http\Endpoints\V1\UploadEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class UploaderServiceProvider
 *
 * @package Modules\Uploader\Providers
 */
class UploaderServiceProvider extends YasnaProvider
{


    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
        $this->registerAssets();
        $this->registerComponents();
    }



    /**
     * Registers the endpoints.
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        endpoint()->register(UploadEndpoint::class);
        endpoint()->register(ReadEndpoint::class);
    }



    /**
     * Returns the proper upload directory based on the specified privacy.
     *
     * @param bool $private
     *
     * @return string
     */
    public static function uploadDirectory(bool $private = true)
    {
        if ($private) {
            return uploader()->module()->getConfig('upload-directory-private');
        } else {
            return uploader()->module()->getConfig('upload-directory-public');
        }
    }



    /**
     * Returns an instance of the proper files' model.
     *
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return UploaderFile
     */
    public static function file($id = 0, $with_trashed = false)
    {
        return model('uploader-file', $id, $with_trashed);
    }



    /**
     * Returns a collection of the required files
     *
     * @param array $ids
     * @param bool  $with_trashed
     *
     * @return Collection
     */
    public static function files(array $ids, bool $with_trashed = false)
    {
        $files = model('uploader-file');
        if ($with_trashed) {
            $files->withTrashed();
        }

        return $files->whereIn('id', hashid($ids))
                     ->get()
                     ->map(function (UploaderFile $file) {
                         $file->hashid   = $file->hashid;
                         $file->link     = $file->getLink();
                         $file->versions = $file->getMeta("versions"); //TODO: Full thumb links must be provided.

                         return $file;
                     })
             ;
    }



    /**
     * get an uploader-suitable array of files
     *
     * @param array $hashids
     * @param bool  $with_trashed
     *
     * @return array
     */
    public static function filesArray(array $hashids, bool $with_trashed = false): array
    {
        return static::files($hashids, $with_trashed)->map(function (UploaderFile $file) {
            return $file->toSingleResource();
        })->toArray()
             ;
    }



    /**
     * Returns the links of the files with the given hashids.
     *
     * @param array|string $hashids
     * @param bool         $with_trashed
     *
     * @return array
     */
    public static function filesLinks($hashids, bool $with_trashed = false)
    {
        $hashids = (array)$hashids;
        $files   = static::files($hashids, $with_trashed);
        $links   = [];

        foreach ($files as $file) {
            $links[$file->hashid] = $file->getLink();
        }

        return $links;
    }



    /**
     * Returns an instance of the proper files' model.
     *
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return UploaderFile
     */
    public static function fileModel($id = 0, $with_trashed = false)
    {
        return static::file($id, $with_trashed);
    }



    /**
     * Register module assets
     */
    public function registerAssets()
    {
        if ($this->canNotUseModule('manage')) {
            return;
        }

        module('manage')
             ->service('template_assets')
             ->add('uploader-css')
             ->link("uploader:css/uploader.min.css")
             ->order(44)
        ;

        module('manage')
             ->service('template_assets')
             ->add('dropzone-js')
             ->link("uploader:libs/dropzone.js")
             ->order(43)
        ;

        module('manage')
             ->service('template_bottom_assets')
             ->add('uploader-custom-js')
             ->link("uploader:js/uploader.js")
             ->order(44)
        ;
    }



    /**
     * register form components
     *
     * @return void
     */
    private function registerComponents()
    {

        if ($this->cannotUseModule("Forms")) {
            return;
        }

        component()::register("Uploader", UploaderComponent::class);
    }
}
