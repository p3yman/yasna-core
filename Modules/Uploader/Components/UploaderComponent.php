<?php

namespace Modules\Uploader\Components;

use Modules\Forms\Services\BasicComponents\InputAbstract;

class UploaderComponent extends InputAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {

        return array_merge(parent::attributes(), [
             "min_files"      => null, // Minimum File Numbers
             "max_files"      => null, // Maximum File Numbers
             "min_size"       => null, // Minimum File Size
             "max_size"       => null, // Maximum File Size
             "accepted_files" => null, // File Mime Types
             "headers"        => "", // Upload Headers
             "files"          => [], // Uploaded Files
             "versions"       => [], // File Versions
        ]);
    }



    /**
     * @inheritdoc
     */
    public function getPayload(array $attributes)
    {
        $array = [
             "min_size"       => $attributes['min_size'],
             "max_size"       => $attributes['max_size'],
             "accepted_files" => $attributes['accepted_files'],
             "versions"       => $attributes['versions'],
        ];

        return $array;
    }

}
