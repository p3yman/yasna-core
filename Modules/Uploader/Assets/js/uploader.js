/**
 * Uploader Custom Js
 * -------------------------
 * Created Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2019-02-10
 */

let UploaderDropzone = function (options) {

    this.myDropzone        = null;
    this.options           = options;
    this.dropzoneContainer = $("#dropzone_body_" + this.options.id);
    this.previewTemplate   = $.trim($("#preview_template_" + this.options.id).html());
    this.filesContainer    = $('#files_' + this.options.id);
    this.fileAdder         = $('#dropzone_wrapper_' + options.id + ' .add-file');
    this.filesCount        = this.options.filesCount;


    /**
     * Checks that uploaded files count won't exceed files limit
     * @private
     */
    this._checkFileAdderVisibility = function () {

        if (this.options.maxFiles === null || this.filesCount < this.options.maxFiles) {
            this.fileAdder.show();
        } else {
            this.fileAdder.hide();
        }
    };


    /**
     * Constructor method
     *
     * @param $
     * @param document
     * @private
     */
    this._constructor = function ($, document) {
        let options   = this.options;
        let self      = this;
        let container = '#dropzone_wrapper_' + options.id;

        this._checkFileAdderVisibility();

        this.myDropzone = new Dropzone('#' + options.id, {
            url           : options.url,
            thumbnailWidth: 150,
            addRemoveLinks: true,
            paramName     : 'upload',
            maxFiles      : 1,

            init: function () {
                // Dropzone sending event
                this.on('sending', function (file, xhr, formData) {
                    formData.append('_token', options.csrf);
                });

                // Dropzone success event
                this.on('success', function (file, response) {
                    self.putPreview(response.results);
                    self.hideDropzone();
                    self.myDropzone.removeFile(file);
                    ++self.filesCount;
                    self._checkFileAdderVisibility();
                });

                // Dropzone error event
                this.on('error', function (file, message, xhr) {
                    $('#' + options.id + ' .dz-error-message span').text(message.errors.upload[0]);
                });
            }
        });

        // Show dropzone
        $(document).on('click', container + ' .js_btn_toggle_dropzone', function () {
            let target = $(this).attr('data-id');

            $('#dropzone_body_' + target).slideDown();
        });

        // Remove preview file
        $(document).on('click', container + ' .js_remove_perveiw', function (e) {
            e.preventDefault();
            let target = $(this).attr('data-id');

            $('#' + target).remove();
            --self.filesCount;
            self._checkFileAdderVisibility();
        });
    };
    this._constructor(jQuery, document);


    /**
     * Hide dropzone area
     */
    this.hideDropzone = function () {
        this.dropzoneContainer.slideUp();
    };


    /**
     * Show dropzone area
     */
    this.showDropzone = function () {
        this.dropzoneContainer.slideDown();
    };


    /**
     * Show thumnail prview of uploaded file
     * @param data
     */
    this.putPreview = function (data) {
        let el  = this.createPreviewEl(data);
        let $el = this.processPreview(el);
        this.appendPreview($el);
    };


    /**
     * Creates html element of uploaded file
     */
    this.createPreviewEl = function (data) {
        let el = this.previewTemplate.replace(/{{hashid}}/ig, data.hashid)
            .replace(/{{link}}/ig, data.link)
            .replace(/{{extension}}/ig, data.extension);

        if (typeof data.versions !== "undefined" && typeof data.versions.thumb !== "undefined") {
            el = el.replace(/{{thumb}}/ig, data.versions.thumb)
                .replace(/{{thumbClass}}/ig, '')
                .replace(/{{svgClass}}/ig, 'noDisplay');
        } else {
            el = el.replace(/{{thumb}}/ig, '')
                .replace(/{{thumbClass}}/ig, 'noDisplay')
                .replace(/{{svgClass}}/ig, '');
        }

        return el
    };


    /**
     * Process html element before appending
     * @param el
     * @return {jQuery|HTMLElement}
     */
    this.processPreview = function (el) {
        if (!$(el).attr('data-has-thumb')) {
            $(el).find('img').css('display', 'none');
            $(el).find('icon').css('display', '');
        }

        return $(el);
    };


    /**
     * Append jQuery element of preveiw to uploader
     * @param $el
     */
    this.appendPreview = function ($el) {
        this.filesContainer.append($el);
    };


};


