<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploaderFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploader_files', function (Blueprint $table) {
            $table->increments('id');

            $table->string('model_class')->nullable()->index();
            $table->unsignedInteger('model_id')->default(0)->index();

            $table->string('original_name')->index();
            $table->string('extension')->index();
            $table->string('mime_type')->index();
            $table->string('group_name')->index();
            $table->string('file_name')->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploader_files');
    }
}
