<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupToUploaderFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('uploader_files', function (Blueprint $table) {
            $table->string('group')
                  ->after('model_id')
                  ->nullable()
            ;
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('uploader_files', function (Blueprint $table) {
            $table->dropColumn('group');
        });
    }
}
