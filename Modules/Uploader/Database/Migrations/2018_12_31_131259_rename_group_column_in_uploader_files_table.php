<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameGroupColumnInUploaderFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('uploader_files', function (Blueprint $table) {
            $table->renameColumn('group_name', 'family_name');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('uploader_files', function (Blueprint $table) {
            $table->renameColumn('family_name', 'group_name');
        });
    }
}
