<?php

if (!function_exists('uploader')) {

    /**
     * Returns a new instance of the `Modules\Uploader\Providers\UploaderServiceProvider` Class.
     *
     * @return \Modules\Uploader\Providers\UploaderServiceProvider
     */
    function uploader()
    {
        return new \Modules\Uploader\Providers\UploaderServiceProvider(app());
    }
}
