<?php

namespace Modules\Uploader\Entities\Traits;

/**
 * @property string $family_name
 * @property string $extension
 * @property bool   $is_private
 */
trait FileStoringTrait
{
    /**
     * Returns the pathname of the specified version or the main version.
     *
     * @param string|null $version
     *
     * @return string
     */
    public function getPathname(?string $version = null)
    {
        $directory = uploader()->uploadDirectory($this->is_private);

        return $directory . DIRECTORY_SEPARATOR . $this->getFileName($version);
    }



    /**
     * get the file name with its extension.
     *
     * @param null|string $version
     *
     * @return string
     */
    public function getFileName(?string $version = null)
    {
        if ($version and $this->hasNotVersion($version)) {
            $version = null;
        }

        $postfix = $this->getVersionPostfix($version);

        return $this->family_name . $postfix . '.' . $this->extension;

    }
}
