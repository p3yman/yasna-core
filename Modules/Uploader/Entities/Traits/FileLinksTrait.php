<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/31/19
 * Time: 2:30 PM
 */

namespace Modules\Uploader\Entities\Traits;


trait FileLinksTrait
{
    /**
     * Returns the link to the original version.
     *
     * @return string
     */
    public function getLink()
    {
        if ($this->isPublic()) {
            return $this->getPublicUrl();
        }

        return $this->getVersionLink($this->originalVersionKey());
    }



    /**
     * Returns the link to the given version.
     *
     * @param string $version
     *
     * @return string
     */
    public function getVersionLink(string $version)
    {
        if ($this->isPublic() and !config('uploader.default-shared-link-file-serve')) {
            $path = $this->getPublicUrl($version);
            return $path ? $path : $this->getPublicUrl();
        }

        return route('uploader.file.read', [
             'hashid'  => $this->hashid,
             'version' => $version,
             'name'    => $this->getAttribute("file_name"),
        ]);
    }



    /**
     * get public url of the file.
     *
     * @param null|string $version
     *
     * @return null|string
     */
    public function getPublicUrl($version = null)
    {
        if ($this->isPrivate()) {
            return null;
        }

        return url("uploader/". $this->getFileName($version));
    }



    /**
     * identify if the file is marked as private.
     *
     * @return bool
     */
    public function isPrivate()
    {
        return boolval($this->getAttribute("is_private"));
    }



    /**
     * identify if the file is NOT marked as private.
     *
     * @return bool
     */
    public function isPublic()
    {
        return !$this->isPrivate();
    }
}
