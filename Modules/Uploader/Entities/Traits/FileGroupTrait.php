<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/31/18
 * Time: 3:59 PM
 */

namespace Modules\Uploader\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;

trait FileGroupTrait
{
    /**
     * Attaches this file to the given entity object with the specified group.
     *
     * @param YasnaModel $model
     * @param string     $group_name
     *
     * @return bool
     */
    public function attachToEntityAs(YasnaModel $model, string $group_name): bool
    {
        return $this->attachTo($model, $group_name);
    }



    /**
     * Scope for selecting file by their groups.
     *
     * @param Builder      $builder
     * @param array|string ...$groups
     *
     * @return Builder
     */
    public function scopeWithGroups(Builder $builder, ...$groups)
    {
        $groups = array_flatten($groups);

        return $builder->whereIn('group', $groups);
    }



    /**
     * Scope for selecting file by their groups.
     *
     * @param Builder      $builder
     * @param array|string ...$groups
     *
     * @return Builder
     */
    public function scopeWithGroup(Builder $builder, ... $groups)
    {
        return $this->scopeWithGroups($builder, ...$groups);
    }
}
