<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/31/18
 * Time: 3:52 PM
 */

namespace Modules\Uploader\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;

trait FileEntityTrait
{

    /**
     * Attaches this file to the given entity object.
     *
     * @param YasnaModel  $model
     * @param string|null $group_name
     *
     * @return bool
     */
    public function attachTo(YasnaModel $model, ?string $group_name = null): bool
    {
        $update_data = $this->getAttachmentsFields($model, $group_name);

        if (empty($update_data)) {
            return false;
        }

        return $this->batchSaveBoolean($update_data);
    }



    /**
     * A scope to attach files in a query to an entity.
     *
     * @param Builder     $builder
     * @param YasnaModel  $model
     * @param string|null $group_name
     *
     * @return int
     */
    public function scopeAttachTo(Builder $builder, YasnaModel $model, string $group_name = null): int
    {
        $update_data = $this->getAttachmentsFields($model, $group_name);

        if (empty($update_data)) {
            return 0;
        }


        return $builder->update($update_data);
    }



    /**
     * Returns an array of the fields to be updated while attaching file(s) to an entity.
     *
     * @param YasnaModel  $model
     * @param string|null $group_name
     *
     * @return array
     */
    protected function getAttachmentsFields(YasnaModel $model, string $group_name = null)
    {
        if ($model->not_exists) {
            return [];
        }

        $model_class = $model->reflector()->getName();
        $model_id    = $model->id;
        $group       = ($group_name ?: null);

        return compact('model_class', 'model_id', 'group');
    }



    /**
     * Detaches the file from any saved entity.
     *
     * @return bool
     */
    public function detachFromEntity()
    {
        if ($this->not_exists) {
            return false;
        }

        return $this->batchSaveBoolean([
             'model_class' => null,
             'model_id'    => 0,
             'group'       => null,
        ]);
    }



    /**
     * The One-to-Many Relationship with any type of entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function entity()
    {
        return $this->belongsTo($this->model_class, 'model_id');
    }



    /**
     * Returns the entity object if exists.
     *
     * @return YasnaModel|null
     */
    public function getEntityAttribute()
    {
        if ($this->model_class) {
            return $this->getRelationValue('entity');
        } else {
            return null;
        }
    }



    /**
     * Checks if this file has been attached to any entity.
     *
     * @return bool
     */
    public function hasEntity()
    {
        return boolval($this->entity);
    }



    /**
     * Checks if this file has not been attached to any entity.
     *
     * @return bool
     */
    public function hasNotEntity()
    {
        return !$this->hasEntity();
    }

}
