<?php

namespace Modules\Uploader\Entities\Traits;

trait FileResourcesTrait
{
    /**
     * boot FileResourcesTrait
     *
     * @return void
     */
    public static function bootFileResourcesTrait()
    {
        static::addDirectResources([
             "mime_type",
             "extension",
        ]);
    }



    /**
     * get Link resource.
     *
     * @return string
     */
    protected function getLinkResource()
    {
        return $this->getLink();
    }



    /**
     * get Hashid resource.
     *
     * @return string
     */
    protected function getHashidResource()
    {
        return $this->hashid;
    }



    /**
     * get Versions resource.
     *
     * @return array
     */
    protected function getVersionsResource()
    {
        $result   = [];
        $versions = $this->versionsArray();

        foreach ($versions as $version => $version_name) {
            $result[$version] = $this->getVersionLink($version);
        }

        return $result;
    }



    /**
     * Returns an array version of the file model.
     *
     * @return array
     * @deprecated 2018-12-31
     */
    public function apiArray()
    {
        return $this->toSingleResource();
    }



    /**
     * Accessor for the `apiArray()` Method
     *
     * @return array
     * @deprecated 2018-12-31
     */
    public function getApiArrayAttribute()
    {
        return $this->toSingleResource();
    }
}
