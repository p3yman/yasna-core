<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/31/18
 * Time: 3:57 PM
 */

namespace Modules\Uploader\Entities\Traits;


use Modules\Uploader\Services\Saving\Saver;

trait FileVersionsTrait
{
    /**
     * Returns an array of the meta fields to hold and manage the versions.
     *
     * @return array
     */
    public function versionsMetaFields()
    {
        return [
             'versions',
        ];
    }



    /**
     * Returns the postfix to generate the version name.
     *
     * @param string|null $version
     *
     * @return string
     */
    public function getVersionPostfix(?string $version)
    {
        if ($version) {
            return Saver::FILE_VERSION_PARTs_SEPARATOR . $version;
        }

        return '';
    }



    /**
     * Checks if this file has the specified version.
     *
     * @param string $version
     *
     * @return bool
     */
    public function hasVersion(string $version)
    {
        return array_key_exists($version, $this->versions_array);
    }



    /**
     * Checks if this file has not the specified version.
     *
     * @param string $version
     *
     * @return bool
     */
    public function hasNotVersion(string $version)
    {
        return !$this->hasVersion($version);
    }



    /**
     * Returns an array of version.
     *
     * @return array
     */
    public function versionsArray()
    {
        return ($this->getMeta('versions') ?? []);
    }



    /**
     * Accessor for the `versionsArray()` Method.
     *
     * @return array
     */
    public function getVersionsArrayAttribute()
    {
        return $this->versionsArray();
    }



    /**
     * Returns the key to specify the original version.
     *
     * @return string
     */
    public static function originalVersionKey()
    {
        return 'original';
    }
}
