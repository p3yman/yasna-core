<?php

namespace Modules\Uploader\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Uploader\Entities\Traits\FileEntityTrait;
use Modules\Uploader\Entities\Traits\FileGroupTrait;
use Modules\Uploader\Entities\Traits\FileResourcesTrait;
use Modules\Uploader\Entities\Traits\FileStoringTrait;
use Modules\Uploader\Entities\Traits\FileLinksTrait;
use Modules\Uploader\Entities\Traits\FileVersionsTrait;
use Modules\Yasna\Services\YasnaModel;

class UploaderFile extends YasnaModel
{
    use SoftDeletes;
    use FileEntityTrait;
    use FileVersionsTrait;
    use FileGroupTrait;
    use FileStoringTrait;
    use FileResourcesTrait;
    use FileLinksTrait;
}
