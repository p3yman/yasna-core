<?php
return [
     "add_file" => "Add File",
     "title"    => "Drop files here or click to upload.",
     'required' => 'Required',
];
