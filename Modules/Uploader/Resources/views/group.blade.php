<div id="{{ $container_id or "" }}" class="form-group {{ $container_class or "" }}"
	 style="{{ $container_style or "" }}" {{ $container_extra or "" }}>

	{{--
	|--------------------------------------------------------------------------
	| Label
	|--------------------------------------------------------------------------
	|
	--}}

	<label for="{{ $name or "" }}" class="col-sm-2 control-label {{ $label_class or "" }}">
		{{ $label or "" }}

		@if($required)
			<span class="fa fa-star required-sign" title="{{trans('uploader::dropzone.required')}}"></span>
		@endif

	</label>


	{{--
	|--------------------------------------------------------------------------
	| Group
	|--------------------------------------------------------------------------
	|
	--}}
	<div class="col-sm-10">
		{{ $slot }}
	</div>


</div>
