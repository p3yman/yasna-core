<a target="_blank" id="{!! $hashid or "@{{hashid}}" !!}" href="{!! $link or "@{{link}}" !!}"
   class="file-preview"
   title="{!! $extension or "@{{extension}}" !!}">

	<span class="remove js_remove_perveiw" data-id="{!! $hashid or "@{{hashid}}" !!}">&times;</span>

	@if(isset($hashid) and $hashid)
		<img class="img {{ (isset($src) and $src) ? '' : 'noDisplay' }}" src=" {!! $src or  "@{{thumb}}" !!}">
		<img class="icon {{ (isset($src) and $src) ? 'noDisplay' : '' }}" src="{{ Module::asset('uploader:images/file.svg') }}">
	@else
		<img class="img {!!"@{{thumbClass}}" !!}" src=" {!! "@{{thumb}}" !!}">
		<img class="icon {!!"@{{svgClass}}" !!}" src="{{ Module::asset('uploader:images/file.svg') }}">
	@endif

	{!!
		widget('hidden')
		->name(isset($name) ? $name . "[]" :  "dropzone[]")
		->class(isset($id) ? "input_" . $id : "input_@{{id}}")
		->value($hashid ?? "@{{hashid}}" )
	!!}
</a>


