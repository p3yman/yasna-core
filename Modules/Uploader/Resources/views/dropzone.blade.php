@php
	if(isset($files) and is_array($files)) {
		$files = uploader()->filesArray($files);
	} else {
		$files = [];
     }

	$id = $id ?? "widget-dropzone-" . rand(1000, 9999) . rand(1000, 9999);

@endphp

<div class="dropzone-wrapper" id="{{ "dropzone_wrapper_" . $id }}">
	@if(isset($in_form) and $in_form)

		@component('uploader::group',[
			"container_id" => $container_id ?? "" ,
			"container_class" => $container_class ?? "" ,
			"container_style" => $container_style ?? "" ,
			"container_extra" => $container_extra ?? "" ,
			"name" => $name or "dropzone" ,
			"required" => $required ?? false ,
			"label_class" => $label_class ?? "" ,
			"label" => $label ?? "" ,
		])

			@include("uploader::dropzone_inside")

		@endcomponent

	@else

		@isset($label)
			<label for="{{ $name or "" }}" class="control-label text-gray mt10 {{ $label_class or "" }}">
				{{ $label or "" }}...

				@if(isset($required) and $required)
					<span class="fa fa-star required-sign" title="{{trans('uploader::dropzone.required')}}"></span>
				@endif

			</label>
		@endisset

		@include("uploader::dropzone_inside")

	@endif
</div>
