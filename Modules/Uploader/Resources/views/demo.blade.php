@extends('manage::layouts.template')

@php
	$files = [
		[
			"hashid" => "xaVWK" ,
			"mime_type" => "image/jpeg" , 
			"extension" => "jpeg" ,
			"link"=>"http://localhost/yasna-core/public/uploader/file/xaVWK/original/1550040344_Gv86FQOp911nWoR99VGhmr0BbHB7HGtJb3k0TBFU.jpeg",
			"versions" => [
				"thumb" => "http://localhost/yasna-core/public/uploader/file/xaVWK/thumb/1550040344_Gv86FQOp911nWoR99VGhmr0BbHB7HGtJb3k0TBFU.jpeg" ,
			] ,
		],

		[
			"hashid" => "KEGnA" ,
			"mime_type" => "application/pdf" ,
			"extension" => "pdf" ,
			"link"=>"http://localhost/yasna-core/public/uploader/file/KEGnA/original/1550040930_SDQeoD8IFUElB9yXlGkuRenX3ybZcDXlnLWqoK9A.pdf",
		],


	];

@endphp

@section('content')
	{!! widget('form-open') !!}

	@include('uploader::dropzone',[
		"in_form" => true ,
		"label" => "آپلودر فایل" ,
		"name" => "dropzone" ,
		"required" => false ,
		"disabled" => false ,
		"id" => "dr1" ,
		"max_file" => 2 ,
		"files" => $files ,
	])

	<hr>

	@include('uploader::dropzone',[
		"in_form" => true ,
		"label" => "ارسال پرونده" ,
		"name" => "dropzone2" ,
		"required" => true ,
		"files" => [] ,
	])

	{!! widget('form-close') !!}
@endsection
