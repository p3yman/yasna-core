<div class="previews">

	<div class="files" id="files_{{ $id  }}">
		@if(isset($files) and count($files))
			@foreach($files as $file)
				@include('uploader::file_template',[
					"hashid" => $file['hashid'] ,
					"link" => $file['link'] ,
					"src" => isset($file['versions']) ? $file['versions']['thumb'] : '' ,
					"extension" => $file['extension'] ,
				])
			@endforeach
		@endif
	</div>

	<div class="file-preview add-file" style="display: none;">
		{!!
			widget('button')
			->label('tr:uploader::dropzone.add_file')
			->class('btn-primary btn-lg js_btn_toggle_dropzone')
			->extraArray(["data-id"=> $id])
			->disabled($disabled ?? false)
		 !!}
	</div>
</div>


<div class="dropzone_toggling_container" style="display: none" id="dropzone_body_{{ $id  }}">
	<div class="dropzone_view uploader_dropzone" id="{{ $id }}">
		<div class="dz-message">
			<h4>{{ trans('uploader::dropzone.title') }}</h4>
		</div>
	</div>
</div>

<script id="preview_template_{{$id}}" type="dropzone/template">
	@include('uploader::file_template')
</script>


<script>
    $(document).ready(function(){
        let uploader = new UploaderDropzone({
			url: "{{ route('uploader.upload') }}",
			id: "{{ $id }}" ,
			csrf: "{{ csrf_token() }}",
			maxFiles: {{ $max_file ?? 'null' }},
			filesCount: {{ isset($files) ? count($files) : 0 }},
		});

        // console.log(uploader);
    });

</script>
