<?php

namespace Modules\Uploader\Http\Endpoints\V1;

use Illuminate\Support\Facades\File as FileFacade;
use Modules\Endpoint\Services\EndpointAbstract;
use Symfony\Component\HttpFoundation\File\File as FileFoundation;

/**
 * @api               {get} /api/modular/v1/uploader-read
 *                    Read File
 * @apiDescription    Read a file with its hashid.
 *                    This endpoint returns the file as the content in case of success.
 * @apiVersion        1.0.0
 * @apiName           Read File
 * @apiGroup          Uploader
 * @apiParam {String} [hashid] The Hashid of the File to Be Read
 * @apiParam {String} [version] If the version does not exist, the original size will be displayed
 * @apiPermission     developer
 * @apiErrorExample   Failed-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *      "headers": {},
 *      "original": {
 *           "status": 400,
 *           "developerMessage": "Invalid version.",
 *           "userMessage": "uploader::userMessages.uploader-2002",
 *           "errorCode": "uploader-2002",
 *           "moreInfo": "uploader.moreInfo.uploader-2002",
 *           "errors": []
 *      },
 *      "exception": null
 * }
 */
class ReadEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Read File";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Uploader\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ReadController@read';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $thumb_requested = (request()->get('version') == 'thumb');
        $path_parts      = [
             $this->runningModule()->getPath(),
             'Assets',
             'images',
             'mock',
        ];

        $base_path = implode(DIRECTORY_SEPARATOR, $path_parts) . DIRECTORY_SEPARATOR;

        if ($thumb_requested) {
            $path = $base_path . '01-thumb.jpg';
        } else {
            $path = $base_path . '01.jpg';
        }

        $file    = new FileFoundation($path);
        $content = FileFacade::get($file->getPathname());

        return response($content)->header('Content-Type', $file->getMimeType());
    }
}
