<?php

namespace Modules\Uploader\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {post} /api/modular/v1/uploader-upload
 *                    Upload File
 * @apiDescription    Upload a file.
 * @apiVersion        1.0.0
 * @apiName           Upload File
 * @apiGroup          Uploader
 * @apiParam {File} [upload] The file to be uploaded
 * @apiPermission     None
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": [
 *           {
 *              "hashid": "pxkYN",
 *              "link": "https://yasna.domain/uploader/file/PKEnN/original/file.jpg",
 *              "versions": {
 *                    "thumb": "https://yasna.domain/uploader/file/PKEnN/thumb/thumb.jpg"
 *               }
 *           }
 *      ]
 * }
 * @apiErrorExample   Failed-Response:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "uploader::developerMessages.1002",
 *      "userMessage": "uploader::userMessages.1002",
 *      "errorCode": 1002,
 *      "moreInfo": "uploader.moreInfo.1002",
 *      "errors": {
 *           "upload": [
 *              [
 *                 "The File must be a file of type: jpg, jpeg, png, gif, mp3, mp4, pdf."
 *              ]
 *           ]
 *       }
 * }
 */
class   UploadEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Upload File";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Uploader\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UploadController@upload';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $file            = uploader()->file();
        $file->file_name = 'file.jpg';
        $file->setMeta('versions', [
             'thumb' => 'thumb.jpg',
        ]);

        return api()->successRespond($file->api_array);
    }
}
