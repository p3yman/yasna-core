<?php

namespace Modules\Uploader\Http\Requests;

use App\Models\UploaderFile;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * The Request for Reading Files
 *
 * @property UploaderFile $model
 */
class ReadRequest extends YasnaFormRequest
{
    protected $model_name = "uploader-file";

    /**
     * @inheritdoc
     */
    protected $should_load_model = true;


    /**
     * @inheritdoc
     */
    protected $should_load_model_with_hashid = true;


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'version' => Rule::in($this->acceptableVersions()),
        ];
    }



    /**
     * @inheritdoc
     */
    public function correctionsBeforeLoadingModel()
    {
        if (!$this->isset('hashid') and request()->hashid) {
            $this->setData("hashid", request()->hashid);
        }
    }



    /**
     * Returns an array of acceptable versions based on the requested file.
     *
     * @return array
     */
    protected function acceptableVersions()
    {
        return array_keys($this->model->versionsArray());
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['version'];
    }
}
