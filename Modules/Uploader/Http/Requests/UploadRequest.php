<?php

namespace Modules\Uploader\Http\Requests;

use Exception;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

class UploadRequest extends YasnaFormRequest
{
    /**
     * keep the decrypted payload content of the configs
     *
     * @var array
     */
    public $configs = [];

    /**
     * @inheritdoc
     */
    protected $automatic_purification_rules = "";

    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;

    /**
     * @var bool
     */
    protected $invalid_payload = false;

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'upload'  => [
                  'required',
                  'mimes:' . implode(',', $this->acceptableExtensions()),
                  'max:' . $this->maxAcceptableFileSize(),
                  'min:' . $this->minAcceptableFileSize(),
             ],
             "payload" => $this->invalid_payload ? "invalid" : "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'upload' => $this->runningModule()->getTrans('file.file'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->loadConfigs();
    }



    /**
     * Returns an array of all the acceptable extensions.
     *
     * @return array
     */
    private function acceptableExtensions()
    {
        if (isset($this->configs['accepted_files'])) {
            return explode_not_empty(",", $this->configs['accepted_files']);
        }

        return config("uploader.default-allowed-extensions");
    }



    /**
     * Returns the max file size in bytes.
     *
     * @return float|int
     */
    private function maxAcceptableFileSize()
    {
        if (isset($this->configs['max_size'])) {
            $mb = $this->configs['max_size'];
        } else {
            $mb = dev() ? 100 : config("uploader.default-max-file-size");
        }

        return 1024 * $mb;
    }



    /**
     * Returns the min file size in bytes.
     *
     * @return float|int
     */
    private function minAcceptableFileSize()
    {
        if (isset($this->configs['min_size'])) {
            $mb = $this->configs['min_size'];
        } else {
            $mb = 0;
        }

        return 1024 * $mb;
    }



    /**
     * load the encrypted payload array inside the upload request
     *
     * @return void
     */
    private function loadConfigs()
    {
        $attribute = "payload";

        if (!$this->isset($attribute)) {
            return;
        }

        $payload = $this->getData("payload");

        try {
            $this->configs = (array)decrypt($payload);
        } catch (Exception $exception) {
            $this->invalid_payload = true;
        }
    }
}
