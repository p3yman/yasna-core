<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Uploader')->getControllersNamespace(),
     'prefix'     => 'uploader',
], function () {
    Route::get('file/{hashid}/{version}/{name}', 'ReadFileController@read')->name('uploader.file.read');
});
