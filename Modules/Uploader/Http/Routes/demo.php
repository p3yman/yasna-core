<?php

Route::group([
     'middleware' => ['web', 'auth', 'is:developer'],
     'namespace'  => module('Uploader')->getControllersNamespace(),
     'prefix'     => 'manage/uploader',
], function () {
    Route::get('demo', 'DemoController@index')->name("uploader-demo");
});
