<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Uploader')->getControllersNamespace() . '\V1',
     'prefix'     => 'uploader',
], function () {
    Route::post('upload', 'UploadController@upload')->name('uploader.upload');
});
