<?php

namespace Modules\Uploader\Http\Controllers;

use App\Models\UploaderFile;
use Illuminate\Support\Facades\File as FileFacade;
use Modules\Uploader\Http\Requests\ReadRequest;
use Modules\Yasna\Services\YasnaController;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File as FileFoundation;

class ReadFileController extends YasnaController
{
    /**
     * Reads the specified file in the specified version.
     *
     * @param ReadRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function read(ReadRequest $request)
    {
        $model   = $request->model;
        $version = ($request->version == $model->originalVersionKey())
             ? null :
             $request->version;

        return $this->getFileResponse($model, $version);
    }



    /**
     * Returns the proper response for the specified file in the given version.
     *
     * @param UploaderFile $model
     * @param string|null  $version
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getFileResponse(UploaderFile $model, ?string $version)
    {
        try {
            $file = new FileFoundation($model->getPathname($version));
        } catch (FileNotFoundException $exception) {
            return $this->abort(404, true, true);
        }

        return response()->file($file->getPathname());
    }
}
