<?php

namespace Modules\Uploader\Http\Controllers\V1;

use Modules\Uploader\Http\Requests\UploadRequest;
use Modules\Uploader\Services\Saving\Saver;
use Modules\Yasna\Services\YasnaApiController;

class UploadController extends YasnaApiController
{
    /**
     * Saves the requested file.
     *
     * @param UploadRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(UploadRequest $request)
    {
        $private_upload = boolval($request->private);
        $saver = new Saver($request->file('upload'),$private_upload, $request->configs);

        $file = $saver->save();

        return response()->json(
             $this->success(
                  $file->toSingleResource()
             )
        );
    }
}
