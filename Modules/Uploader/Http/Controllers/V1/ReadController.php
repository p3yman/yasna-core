<?php

namespace Modules\Uploader\Http\Controllers\V1;

use App\Models\UploaderFile;
use Illuminate\Support\Facades\File as FileFacade;
use Modules\Uploader\Http\Requests\ReadRequest;
use Modules\Yasna\Services\YasnaApiController;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File as FileFoundation;

class ReadController extends YasnaApiController
{
    /**
     * Reads the specified file in the specified version.
     *
     * @param ReadRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(ReadRequest $request)
    {
        $model   = $request->model;
        $version = $request->get('version');

        if ($version and $model->hasNotVersion($version)) {
            return response()->json(
                 $this->clientError('uploader-2002')
            );
        }

        return $this->getFileResponse($model, $version);
    }



    /**
     * Returns the proper response for the specified file in the given version.
     *
     * @param UploaderFile $model
     * @param string|null  $version
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getFileResponse(UploaderFile $model, ?string $version)
    {
        try {
            $file = new FileFoundation($model->getPathname($version));
        } catch (FileNotFoundException $exception) {
            return response()->json(
                 $this->serverError('uploader-2001')
            );
        }

        $content = FileFacade::get($file->getPathname());

        return response($content)->header('Content-Type', $file->getMimeType());
    }
}
