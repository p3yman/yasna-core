<?php 

namespace Modules\Uploader\Http\Controllers;

use Modules\Yasna\Services\YasnaController;

class DemoController extends YasnaController
{
    /**
     * Uploader demo page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->view('uploader::demo');
    }
}
