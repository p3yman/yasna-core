<?php

namespace Modules\RegistryWaitings\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\RegistryWaitings\Http\Controllers\V1\SicksController;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/registry-waitings-sick-delete
 *                    Sick Delete
 * @apiDescription
 * @apiVersion        1.0.0
 * @apiName           Sick Delete
 * @apiGroup          RegistryWaitings
 * @apiPermission     User TODO: to be updated when specified by the customer!
 * @apiParam {string} [id]  hashid of the model in question, in edit mode.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method SicksController controller()
 */
class SickDeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Sick Delete";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists; // <~~ to be handled in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\RegistryWaitings\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SicksController@delete';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "id" => hashid(rand(1, 100)),
        ]);
    }
}
