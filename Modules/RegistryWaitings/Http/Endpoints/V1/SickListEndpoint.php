<?php

namespace Modules\RegistryWaitings\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\RegistryWaitings\Http\Controllers\V1\SicksController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-waitings-sick-list
 *                    list view of the sick persons
 * @apiDescription
 * @apiVersion        1.0.0
 * @apiName           list view of the sick persons
 * @apiGroup          RegistryWaitings
 * @apiPermission     User TODO: to be updated when specified by the customer!
 * @apiParam {string}  [name_first]             filter against first name
 * @apiParam {string}  [name_last]              filter against last name
 * @apiParam {string}  [code_melli]             filter against code_melli
 * @apiParam {string}  [blood]                  filter against blood type and blood rh
 * @apiParam {boolean} [liver_priority]         filter against liver_priority
 * @apiParam {int}     [age]                    filter against age
 * @apiParam {int}     [liver_entered_at_from]  determine the lower limit of the liver_entered_at
 * @apiParam {int}     [liver_entered_at_from]  determine the upper limit of the liver_entered_at
 * @apiParam {string}  [country_id]             filter against the country hashid
 * @apiParam {string}  [province_id]            filter against the province hashid
 * @apiParam {string}  [city_id]                filter against the city hashid
 * @apiParam {boolean} [paginated]              determine if the result should be paginated.
 * @apiParam {int}     [page]                   determine the page number, if the result is paginated.
 * @apiParam {int}     [per_page]               determine the number of items per page, if the result is paginated.
 * @apiParam {string}  [sort]                   determine the sort field.
 * @apiParam {string}  [order]                  determine the order type: desc/asc.
 * @apiParam {array}   [including]              specify the requested list of resources.
 * @apiParam {array}   [excluding]              specify the excluded list of resources.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total" => 1,
 *      },
 *      "results": {
 *          {
 *              "id": "qKXVA",
 *              "height": 185,
 *              "weight": 65,
 *              "measured_at": 1561787907
 *          }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method SicksController controller()
 */
class SickListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "list view of the sick persons";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists; // <~~ to be handled in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\RegistryWaitings\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SicksController@getList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "id"          => "qKXVA",
                  "height"      => 185,
                  "weight"      => 65,
                  "measured_at" => 1561787907,
             ],
        ], [
             "total" => "1",
        ]);
    }
}
