<?php

namespace Modules\RegistryWaitings\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\RegistryWaitings\Http\Controllers\V1\SicksController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-waitings-sick-history
 *                    History of a selected data of a sick person
 * @apiDescription
 * @apiVersion        1.0.0
 * @apiName           History of a selected data of a sick person
 * @apiGroup          RegistryWaitings
 * @apiPermission     User TODO: to be updated when specified by the customer!
 * @apiParam {string} id          hashid of the model in question
 * @apiParam {string} attribute   the attribute of which the history is requested
 * @apiParam {array}  [including] the list of resources to be included in the result
 * @apiParam {array}  [excluding] the list of resources to be excluded from the result
 * @apiSuccessExample Success-Response
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hAshId",
 *          "attribute":"height",
 *          "total":1
 *      },
 *      "results": {
 *          {
 *              "time": 1561791602,
 *              "height": "185"
 *          }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method SicksController controller()
 */
class SickHistoryEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "History of a selected data of a sick person";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists; // <~~ to be handled in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\RegistryWaitings\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SicksController@getHistory';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "time"               => now()->subDay(1),
                  request()->attribute => rand(1, 100),
             ],
             [
                  "time"               => time(),
                  request()->attribute => rand(1, 100),
             ],
        ], [
             "id"        => "Ha5HiD",
             "attribute" => request()->attribute,
             "total"     => 2,
        ]);
    }
}
