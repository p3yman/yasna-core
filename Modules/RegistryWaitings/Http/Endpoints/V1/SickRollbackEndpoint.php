<?php

namespace Modules\RegistryWaitings\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\RegistryWaitings\Http\Controllers\V1\SicksController;

/**
 * @api               {PATCH}
 *                    /api/modular/v1/registry-waitings-sick-rollback
 *                    Sick Rollback
 * @apiDescription    Rollback a case to the latest revision.
 * @apiVersion        1.0.0
 * @apiName           Sick Rollback
 * @apiGroup          RegistryWaitings
 * @apiPermission     User TODO: to be updated when specified by the customer!
 * @apiParam {string} [id]  hashid of the model in question, in edit mode.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method SicksController controller()
 */
class SickRollbackEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Sick Rollback";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists; // <~~ to be handled in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_PATCH;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\RegistryWaitings\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SicksController@rollback';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "id" => hashid(rand(1, 100)),
        ]);
    }
}
