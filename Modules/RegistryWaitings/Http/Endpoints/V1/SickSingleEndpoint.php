<?php

namespace Modules\RegistryWaitings\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\RegistryWaitings\Http\Controllers\V1\SicksController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-waitings-sick-single
 *                    Single View of a sick person
 * @apiDescription
 * @apiVersion        1.0.0
 * @apiName           Single View of a sick person
 * @apiGroup          RegistryWaitings
 * @apiPermission     User TODO: to be updated when specified by the customer!
 * @apiParam {string} id  hashid of the model in question
 * @apiParam {array}  including  list if the including resources
 * @apiParam {array}  excluding  list if the excluding resources
 * @apiParam {int}    batch      batch number, if specific history batch is required.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "qKXVA",
 *          "including": [
 *              "medical_resource_group",
 *              "_info_resource_group"
 *          ],
 *          "excluding": null,
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "id": "qKXVA",
 *          "height": 185,
 *          "weight": 65,
 *          "measured_at": 1561787907
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method SicksController controller()
 */
class SickSingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Single View of a sick person";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists; // <~~ to be handled in the request layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\RegistryWaitings\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SicksController@getSingle';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"          => "qKXVA",
             "height"      => 185,
             "weight"      => 65,
             "measured_at" => 1561787907,
        ], [
             "id"        => "qKXVA",
             "including" => [
                  "medical_resource_group",
                  "_info_resource_group",
             ],
             "excluding" => null,
        ]);
    }
}
