<?php

namespace Modules\RegistryWaitings\Http\Controllers\V1;

use Modules\RegistryWaitings\Http\Requests\V1\SickDeleteRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickLiverClinicalSaveRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickLiverGeneralSaveRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickHistoryRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickListRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickLiverMedicalSaveRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickRollbackRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickSingleRequest;
use Modules\RegistryWaitings\Http\Requests\V1\SickLiverStateSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class SicksController extends YasnaApiController
{
    /**
     * get a single-view resource.
     *
     * @param SickSingleRequest $request
     *
     * @return array
     */
    public function getSingle(SickSingleRequest $request)
    {
        $model = $request->model;

        if ($request->batch) {
            $model->revert($request->batch, true);
        }

        return $this->success(
             $request->model->toResource($request->including, (array)$request->excluding),
             [
                  "id"        => $request->model_hashid,
                  "including" => $request->including,
                  "excluding" => $request->excluding,
             ]
        );
    }



    /**
     * get a list-view set of resources.
     *
     * @param SickListRequest $request
     *
     * @return array
     */
    public function getList(SickListRequest $request)
    {
        $builder = model("registry-sick")->elector($request->toArray());

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get history of a selected item.
     *
     * @param SickHistoryRequest $request
     *
     * @return array
     */
    public function getHistory(SickHistoryRequest $request)
    {
        $result = $request->model->getHistoryOf($request->attribute, $request->including, $request->exluding);
        $meta   = [
             "id"        => $request->model_hashid,
             "attribute" => $request->attribute,
             "including" => $request->including,
             "excluding" => $request->excluding,
        ];

        return $this->success($result, $meta);
    }



    /**
     * perform soft delete.
     *
     * @param SickDeleteRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(SickDeleteRequest $request)
    {
        $deleted = $request->model->delete();

        return $this->typicalSaveFeedback($deleted, [
             "id" => $request->model_hashid,
        ]);
    }



    /**
     * rollback the model to the latest history batch
     *
     * @param SickRollbackRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function rollback(SickRollbackRequest $request)
    {
        $done = $request->model->rollback();

        return $this->typicalSaveFeedback($done, [
             "id" => $request->model_id,
        ]);
    }



    /**
     * perform save of the general section
     *
     * @param SickLiverGeneralSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function saveLiverGeneral(SickLiverGeneralSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * perform save of the medical section
     *
     * @param SickLiverGeneralSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function saveLiverMedical(SickLiverMedicalSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * perform save of the medical section
     *
     * @param SickLiverGeneralSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function saveLiverClinical(SickLiverClinicalSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * perform save of the state section
     *
     * @param SickLiverStateSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function saveLiverState(SickLiverStateSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }
}
