<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use Modules\RegistryWaitings\Entities\RegistrySick;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property RegistrySick $model
 */
class SickSingleRequest extends YasnaFormRequest
{
    protected $model_name                = "RegistrySick";
    protected $automatic_injection_guard = false; // <~~ No Mass Assignment Involved
    protected $should_allow_create_mode  = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true; //TODO: To be completed!
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "including" => "required|array",
             "excluding" => "array",
             "batch"     => "numeric|integer",
        ];
    }

}
