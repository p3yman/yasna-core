<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use App\Models\RegistrySick;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property RegistrySick $model
 */
class SickRollbackRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Registry-Sick";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true; //@TODO: To be implemented once the criteria is supplied.
    }
}
