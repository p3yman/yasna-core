<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use App\Models\RegistrySick;

class SickLiverStateSaveRequest extends SickSaveAbstractRequest
{
    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        /** @var RegistrySick $empty_model */
        $empty_model = model('registry-sick');

        return $empty_model->getLiverStateResourceGroup();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "liver_received_at"    => "ed|date",
             "died_at"              => "ed|date",
             "liver_activated_at"   => "ed|date",
             "liver_deactivated_at" => "ed|date",
             "liver_reactivated_at" => "ed|date",
        ];
    }



    /**
     * get priority-related validation rules
     *
     * @return array
     */
    protected function priorityRules()
    {
        return [
             "liver_priority" => "required|boolean",
        ];
    }



    /**
     * get activePriority-related validation rules
     *
     * @return array
     */
    protected function activePriorityRules()
    {
        $combo_subject = "liver-priority-type";

        if ($this->getData("liver_priority") == 0) {
            $this->setData("liver_priority_type", null);
            return [];
        }

        if ($this->model->isMature() and $this->checkData("liver_priority_type", "$combo_subject-1b")) {
            return [
                 "liver_priority_type" => "invalid",
            ];
        }

        if ($this->model->isChild() and $this->checkData("liver_priority_type", "$combo_subject-1a")) {
            return [
                 "liver_priority_type" => "invalid",
            ];
        }

        return [
             "liver_priority_type" => "required_if:liver_priority,1|combo:$combo_subject",
        ];
    }



    /**
     * get nonActivePriority-related validation rules
     *
     * @return array
     */
    protected function nonActivePriorityRules()
    {
        if ($this->getData("liver_priority") == 1) {
            $this->setData("liver_priority_reason", null);
            $this->setData("liver_priority_reason_other", null);
            $this->setData("liver_priority_technical", null);
            $this->setData("liver_priority_technical_other", null);
            return [];
        }

        return [
             "liver_priority_reason"          => "required|combo:liver-priority-reason",
             "liver_priority_reason_other"    => "required_if:liver_priority_reason,liver-priority-reason-others",
             "liver_priority_technical"       => "required|combo:technical-reason-disabling",
             "liver_priority_technical_other" => "required_if:liver_priority_technical,technical-reason-disabling-others",
        ];
    }



    /**
     * get urgency-related validation rules
     *
     * @return array
     */
    protected function urgencyRules()
    {
        $combo_subject = "liver-priority-type";
        $priority_type = $this->getData("liver_priority_type");
        $field         = "liver_urgent";

        if ($this->getData($field) and !in_array($priority_type, ["$combo_subject-1a", "$combo_subject-1b"])) {
            return [
                 $field                  => "invalid",
                 "liver_non_urgent_need" => "required|boolean",
            ];
        }

        return [
             $field => "required|boolean",
             "liver_urgent_reason" => "required|combo:liver-urgent-reason",
        ];
    }



    /**
     * get urgencyReason-related validation rules
     *
     * @return array
     */
    protected function urgencyReasonRules()
    {
        if (!$this->getData("liver_urgent")) {
            $this->setData("liver_urgent_reason", null);
            $this->setData("liver_letter_file", null);
            return [];
        }

        return [
             "liver_non_urgent_need" => "",
             "liver_letter_file"     => "required|array",
        ];
    }



    /**
     * get status-related validation rules
     *
     * @return array
     */
    protected function statusRules()
    {
        return [
             "liver_status" => "required|combo:liver-status",
        ];
    }



    /**
     * get transplant-related validation rules
     *
     * @return array
     */
    protected function transplantRules()
    {
        if (!$this->isEditing()) {
            $this->setData("liver_received_at", null);
            return [];
        }

        return [
             "liver_received_at" => "date",
        ];
    }



    /**
     * get death-related validation rules
     *
     * @return array
     */
    protected function deathRules()
    {
        if (!$this->isEditing() or !$this->checkData("liver_status", "liver-status-dead")) {
            $this->setData("died_at", null);
            $this->setData("died_for_liver", 0);
            $this->setData("death_reason_non_liver", null);
            return [];
        }

        return [
             "died_at"                => "required|date",
             "died_for_liver"         => "required|boolean",
             "death_reason_non_liver" => "required_if:died_for_liver,0",
        ];
    }



    /**
     * get date-related validation rules
     *
     * @return array
     */
    protected function dateRules()
    {
        // if selected as active...
        if ($this->getData("liver_priority") == 1) {
            $this->unsetData("liver_deactivated_at"); // <~~ should keep the previous data, if any.

            if (!$this->getData("liver_activated_at")) {
                $this->setData("liver_activated_at", now()->toDateTimeString());
                $this->setData("liver_reactivated_at", now()->toDateTimeString());
            }

            return [
                 "liver_activated_at"   => "required_if:liver_priority,1|date",
                 "liver_reactivated_at" => "required_if:liver_priority,1|date",
            ];
        }

        //otherwise...
        $this->unsetData("liver_activated_at"); // <~~ should keep the previous data, if any.
        $this->unsetData("liver_reactivated_at"); // <~~ should keep the previous data, if any.

        if (!$this->getData("liver_activated_at")) {
            $this->setData("liver_deactivated_at", now()->toDateTimeString());
        }

        return [
             "liver_deactivated_at" => "required_if:liver_priority,0|date",
        ];
    }
}
