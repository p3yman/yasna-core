<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use App\Models\RegistrySick;
use Modules\Registry\Services\BloodType;

class SickLiverGeneralSaveRequest extends SickSaveAbstractRequest
{
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        /** @var RegistrySick $empty_model */
        $empty_model = model('registry-sick');

        return $empty_model->getLiverGeneralResourceGroup();
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveBloodType();
        $this->resolveFundSources();
        $this->resolveInsuranceType();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        parent::mutators();

        $this->unsetData("blood", null);
        $this->setData("fund_sources", json_encode($this->getData('fund_sources')));
        $this->setData("phones", json_encode($this->getData('phones')));
        $this->setData("mobiles", json_encode($this->getData('mobiles')));
        $this->correctInsurance();

        $this->setData('country_id', hashid($this->getData('country_id')));
        $this->setData('residency_id', hashid($this->getData('residency_id')));
        $this->setData('nationality_id', hashid($this->getData('nationality_id')));
        $this->setData('trans_hospital_id', hashid($this->getData('trans_hospital_id')));
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "mobiles.*"        => "ed",
             "phones.*"         => "ed",
             "liver_entered_at" => "ed|date",
             "birth_date"       => "ed|date",
             "code_melli"       => "ed",
        ];
    }



    /**
     * get general-Specs validation rules
     *
     * @return array
     */
    protected function generalSpecsRules()
    {
        return [
             'name_first'       => 'required',
             'name_last'        => 'required',
             'gender'           => 'required|integer|in:1,2',
             'birth_date'       => 'required|date',
             'nationality_id'   => 'required|string',
             'residency_id'     => 'required|string',
             'home_address'     => 'required',
        ];
    }


    /**
     * get `home_postal_code` validation rules
     *
     * @return array|void
     */
    protected function postalCodeRules()
    {
        if ($this->getData('country_id') !== 'NoDwA') { //If Iran were not ~~> id=495
            $this->setData('home_postal_code', null);
            return;
        }

        return [
             'home_postal_code' => 'required|postal_code',
        ];

    }



    /**
     *  get nationality validation rules
     *
     * @return array
     */
    protected function nationalityRules()
    {
        $data = $this->getData('nationality_id');
        if (!$data) {
            return [
                 'nationality_id' => 'required',
            ];
        }

        $country = country()->grabHashid($data);

        if (!$country->exists) {
            return [
                 'nationality_id' => 'invalid',
            ];
        }
    }



    /**
     *  get residency validation rules
     *
     * @return array
     */
    protected function residencyRules()
    {
        $data = $this->getData('residency_id');
        if (!$data) {
            return [
                 'residency_id' => 'required',
            ];
        }

        $country = country()->grabHashid($data);

        if (!$country->exists) {
            return [
                 'residency_id' => 'invalid',
            ];
        }
    }



    /**
     * get code_melli validation rules
     *
     * @return array
     */
    protected function codeMelliRules()
    {
        $data = $this->getData("code_melli");

        if (!$data) {
            return [
                 'code_melli' => 'required',
            ];
        }
        $model = $this->model->where("code_melli", $data)->where('id', '!=', $this->model->id)->first();

        if (!$model) {
            return [
                 'code_melli' => 'code_melli',
            ];
        }

        return [
             'code_melli' => 'invalid',
        ];

    }



    /**
     * get city validation rules
     *
     * @return array
     */
    protected function cityRules()
    {
        if ($this->getData('country_id') !== 'NoDwA') { //If Iran were not ~~> id=495

            $this->setData("city_id", 0);
            $this->setData("province_id", 0);

            return;
        }

        $data = $this->getData('city_id');

        if (!$data) {
            return [
                 'city_id' => 'required',
            ];
        }

        $city = city()->grabHashid($data);

        if (!$city->exists) {
            return [
                 'city_id' => 'invalid',
            ];
        }

        $this->setData("city_id", $city->id);
        $this->setData("province_id", $city->province_id);
        $this->setData("country_id", hashid($city->country_id));

    }



    /**
     * get blood validation rules
     *
     * @return array
     */
    protected function bloodRules()
    {
        return [
             'blood' => 'required',
        ];
    }



    /**
     * get liver-entered validation rules
     *
     * @return array
     */
    protected function liverEnteredRules()
    {
        return [
             "liver_entered_at" => "required|date|before:" . now(),
        ];
    }



    /**
     * get hospital validation rules
     *
     * @return array
     */
    protected function hospitalRules()
    {
        $data = $this->getData('trans_hospital_id');

        if (!$data) {
            return [
                 'trans_hospital_id' => 'required',
            ];
        }

        $hospital = hospital()->grabHashid($this->getData('trans_hospital_id'));

        if (!$hospital->exists) {
            return [
                 'trans_hospital_id' => "invalid",
            ];
        }

    }



    /**
     * get education validation rules
     *
     * @return array
     */
    protected function educationRules()
    {
        return [
             'education' => "required|combo:education",
        ];
    }



    /**
     * get job validation rules
     *
     * @return array
     */
    protected function jobRules()
    {
        $fields = [
             "job_has" => "required|boolean",
        ];

        if (!$this->getData("job_has")) {

            $fields['job_unemployment_reason'] = "required|combo:unemployment-reason";

            if ($this->getData('job_unemployment_reason') === 'unemployment-reason-does-not-work') {
                $fields['job_no_work_reason'] = "required|combo:cause-not-work";
            }

            $this->setData("job_type", null);
            $this->setData("job_not_full_reason", null);

        } else {

            if ($this->getData('job_type') === 'job-type-part-time') {
                $fields['job_not_full_reason'] = "required|combo:reason-not-full-time";
            } else {
                $this->setData("job_not_full_reason", null);
            }

            $fields['job_type'] = "required|combo:job-type";
            $this->setData("job_unemployment_reason", null);
            $this->setData("job_no_work_reason", null);
        }

        return $fields;
    }



    /**
     * get insurance validation rules
     *
     * @return array
     */
    protected function insuranceRules()
    {
        $data = $this->getData("insurance_has");

        if (!$data) {

            $this->setData("insurance_co", null);
            $this->setData("insurance_type", null);

            return [
                 "insurance_has" => "required",
            ];
        }

        $fields['insurance_has']    = "boolean";
        $fields['insurance_type.*'] = "required|combo:insurance-type";

        if (in_array('insurance-type-supplementary', $this->getData('insurance_type'))) {
            $fields['insurance_co'] = "required";
        } else {
            $this->setData("insurance_co", null);
        }

        return $fields;
    }



    /**
     * get fund-sources validation rules
     *
     * @return array
     */
    protected function fundSourcesRules()
    {
        return [
             'fund_sources.*' => 'required|combo:source-of-funding',
        ];
    }



    /**
     * get phone validation rules
     *
     * @return array|void
     */
    protected function phonesRules()
    {
        if (!$this->getData('phones')) {
            return [
                 'phones' => 'required',
            ];
        }

        return;
    }



    /**
     * get phone validation rules
     *
     * @return array|void
     */
    protected function mobilesRules()
    {
        if (!$this->getData('mobiles')) {
            return [
                 'mobiles' => 'required',
            ];
        }
        return;
    }



    /**
     * parse `blood` into `blood_type` and `blood_rh`
     *
     * @return void
     */
    private function resolveBloodType()
    {
        if (!$this->isset("blood")) {
            return;
        }

        $array = BloodType::parseTypeAndRH($this->getData("blood"));

        if (!$array['type'] or !$array['rh']) {
            $this->unsetData("blood");
            return;
        }

        $this->setData("blood_type", $array['type']);
        $this->setData("blood_rh", $array['rh']);
    }



    /**
     * resolve `fund-resource`
     *
     * @return void
     */
    private function resolveFundSources()
    {
        $data = (array)$this->getData('fund_sources');

        $this->setData('fund_sources', $data);

    }



    /**
     * resolve `insurance-type`
     *
     * @return void
     */
    private function resolveInsuranceType()
    {
        $data = (array)$this->getData('insurance_type');

        $this->setData('insurance_type', $data);

    }


    /**
     * mutator `insurance_type`
     *
     * @return void
     */
    private function correctInsurance()
    {

        if ($this->getData('insurance_type')) {
            $this->setData("insurance_type", json_encode($this->getData('insurance_type')));
        }
    }
}
