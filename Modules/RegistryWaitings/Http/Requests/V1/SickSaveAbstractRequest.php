<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use App\Models\RegistrySick;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property RegistrySick $model
 */
abstract class SickSaveAbstractRequest extends YasnaFormRequest
{
    protected $model_name               = "RegistrySick";
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true; //TODO: To be completed when criteria supplied.
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry-waitings::attributes");
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return trans("registry-waitings::messages");
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->setData("liver_updated_at", now()->toDateTimeString());
        $this->setData("liver_updated_by", user()->id);

        $this->mutateFileArrays();
    }



    /**
     * specify if the model is in editing mode.
     *
     * @return bool
     */
    protected function isEditing()
    {
        return $this->model->isFullyCreated();
    }



    /**
     * convert all file arrays to comma-separated strings.
     *
     * @return void
     */
    protected function mutateFileArrays()
    {
        foreach ($this->getDataArray() as $key => $value) {
            if (ends_with($key, "_file") and is_array($value)) {
                $this->setData($key, implode(",", $value));
            }
        }
    }
}
