<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use App\Models\RegistrySick;

class SickLiverClinicalSaveRequest extends SickSaveAbstractRequest
{
    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        /** @var RegistrySick $empty_model */
        $empty_model = model('registry-sick');

        return $empty_model->getLiverClinicalResourceGroup();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "test_cr"                => "ed",
             "test_bili_t"            => "ed",
             "test_inr"               => "ed",
             "test_na"                => "ed",
             "test_alb"               => "ed",
             "liver_tests_updated_at" => "ed|date",
             "peld_score"             => "ed",
             "meld_score"             => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        parent::mutators();

        $this->setData("liver_tests_updated_by", now()->toDateTimeString());
    }



    /**
     * get general validation rules
     *
     * @return array
     */
    protected function generalRules()
    {
        return [
             "test_cr"                => "required|numeric|between:0,15",
             "test_bili_t"            => "required|integer|between:0,1000",
             "test_inr"               => "required|numeric|between:1,30",
             "liver_tests_updated_at" => "required|date",
             "liver_tests_file"       => "array",
             "peld_score"             => "numeric",
             "meld_score"             => "numeric",
        ];
    }



    /**
     * get na-related validation rules
     *
     * @return array
     */
    protected function naRules()
    {
        if ($this->model->isChild()) {
            $this->setData("test_na", null);
            return [];
        }

        return [
             "test_na" => "required|integer|between:50,400",
        ];
    }



    /**
     * get alb-related validation rules
     *
     * @return array
     */
    protected function albRules()
    {
        if ($this->model->isMature()) {
            $this->setData("test_alb", null);
            return [];
        }

        return [
             "test_alb" => "required|numeric",
        ];
    }
}
