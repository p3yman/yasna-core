<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use App\Models\RegistrySick;

class SickLiverMedicalSaveRequest extends SickSaveAbstractRequest
{
    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        /** @var RegistrySick $empty_model */
        $empty_model = model('registry-sick');

        return $empty_model->getLiverMedicalResourceGroup();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "height"                   => "ed",
             "weight"                   => "ed",
             "measured_at"              => "ed|date",
             "diabetes_detected_at"     => "ed|date",
             "hypertension_detected_at" => "ed|date",
             "malignancy_detection"     => "ed",
             "malignancy_cure_time"     => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        parent::mutators();

        $liver_need_other_organs_names = $this->getData("liver_need_other_organs_names");
        $this->setData("liver_need_other_organs_names", implode(",", $liver_need_other_organs_names));

        $transplant_history_details = $this->getData("transplant_history_details");
        $this->setData("transplant_history_details", json_encode($transplant_history_details));
    }



    /**
     * get dimensional validation rules
     *
     * @return array
     */
    protected function dimensionRules()
    {
        return [
             "height"      => "required|numeric|between:50,250",
             "weight"      => "required|numeric|between:10,250",
             "measured_at" => "required|date|before:" . now(),
        ];
    }



    /**
     * get evolution-related validation rules
     *
     * @return array
     */
    protected function evolutionRules()
    {
        if ($this->model->isMature()) {
            $this->unsetData([
                 "evolution_cognitive_status",
                 "evolution_mobility_status",
                 "edu_achievement",
                 "edu_activity",
            ]);
            return [];
        }

        return [
             "evolution_cognitive_status" => "required|combo:evolution-status",
             "evolution_mobility_status"  => "required|combo:evolution-status",
             "edu_achievement"            => "required|combo:edu-achievement",
             "edu_activity"               => "required|combo:edu-achievement",
        ];
    }



    /**
     * get functional-related validation rules
     *
     * @return array
     */
    protected function functionalRules()
    {
        return [
             "liver_underlying_disease" => "required|combo:liver-underlying-disease",
             "functional_status"        => "required|combo:functional-status",
        ];
    }



    /**
     * get lifeCure-related validation rules
     *
     * @return array
     */
    protected function lifeCureRules()
    {
        $rules["liver_life_cure_liver_has"] = "required|boolean";

        // The combo...
        if ($this->getData("liver_life_cure_liver_has")) {
            $rules["liver_life_cure_liver_type"] = "required|combo:liver-life-cure";
        } else {
            $this->setData("liver_life_cure_liver_type", null);
        }

        // The `other` field...
        if ($this->checkData("liver_life_cure_liver_type", "liver-life-cure-others")) {
            $rules['liver_life_cure_liver_other'] = "required";
        } else {
            $this->setData("liver_life_cure_liver_other", null);
        }

        return $rules;
    }



    /**
     * get cigar-related validation rules
     *
     * @return array
     */
    protected function cigarRules()
    {
        $rules['cigar_has'] = "required|boolean";

        if ($this->getData("cigar_has")) {
            $rules['cigar_status']      = "required|combo:smoking";
            $rules['cigar_consumption'] = "required|combo:smoking-per-year";
        } else {
            $this->setData("cigar_status", null);
            $this->setData("cigar_quit_time", null);
            $this->setData("cigar_consumption", null);
        }

        if ($this->checkData("cigar_status", "smoking-quit")) {
            $rules['cigar_quit_time'] = "required|combo:no-smoking-period";
        } else {
            $this->setData("cigar_quit_time", null);
        }

        return $rules;
    }



    /**
     * get opium-related validation rules
     *
     * @return array
     */
    protected function opiumRules()
    {
        $rules['opium_has'] = "required|boolean";

        if ($this->getData("opium_has")) {
            $rules['opium_quit'] = "required|boolean";
        } else {
            $this->setData("opium_quit", null);
        }

        return $rules;
    }



    /**
     * get alcohol-related validation rules
     *
     * @return array
     */
    protected function alcoholRules()
    {
        $rules['alcohol_has'] = "required|boolean";

        if ($this->getData("alcohol_has")) {
            $rules['alcohol_quit'] = "required|boolean";
        } else {
            $this->setData("alcohol_quit", null);
        }

        return $rules;
    }



    /**
     * get otherDiseases-related validation rules
     *
     * @return array
     */
    protected function otherDiseasesRules()
    {
        return [
             "abdominal_surgery"     => "required|boolean",
             "peritonitis_bacterial" => "required|combo:boolean",
             "shunt_trans_jugular"   => "required|combo:boolean",
             "venous_thrombosis"     => "required|combo:boolean",
        ];
    }



    /**
     * get transplant-related validation rules
     *
     * @return array
     */
    protected function transplantRules()
    {
        return [
             "liver_need_other_organs"       => "required|boolean",
             "liver_need_other_organs_names" => "required_if:liver_need_other_organs,1|array",
             "transplant_history"            => "required|boolean",
             "transplant_history_details"    => "required_if:transplant_history,1|array",
        ];
    }



    /**
     * get diabetes-related validation rules
     *
     * @return array
     */
    protected function diabetesRules()
    {
        if (!$this->getData("diabetes_has")) {
            $this->setData("diabetes_type", null);
            $this->setData("diabetes_detected_at", null);
            $this->setData("diabetes_control", null);
            $this->setData("diabetes_cure", null);

            return [
                 "diabetes_has" => "required|boolean",
            ];
        }

        return [
             "diabetes_type"        => "required|combo:diabetes-type",
             "diabetes_detected_at" => "date",
             "diabetes_control"     => "required|combo:diabetes-control",
             "diabetes_cure"        => "combo:diabetes-cure",
        ];
    }



    /**
     * get kidney-related validation rules
     *
     * @return array
     */
    protected function kidneyRules()
    {
        if (!$this->getData("kidney_problem_has")) {
            $this->setData("kidney_problem_type", null);

            return [
                 "kidney_problem_has" => "required|boolean",
            ];
        }

        return [
             "kidney_problem_type" => "required",
        ];
    }



    /**
     * get dialysis-related validation rules
     *
     * @return array
     */
    protected function dialysisRules()
    {
        return [
             "dialysis_history"    => "required_if:kidney_problem_has,1|combo:dialysis-history",
             "neoadjuvant_therapy" => "required|boolean",
             "hcc_has"             => "required|boolean",
             "brain_vein_problems" => "required|boolean",
        ];
    }



    /**
     * get hypertension-related validation rules
     *
     * @return array
     */
    protected function hypertensionRules()
    {
        if (!$this->getData("hypertension_has")) {
            $this->setData("hypertension_detected_at", null);
            $this->setData("hypertension_status", null);

            return [
                 "hypertension_has" => "required|boolean",
            ];
        }

        return [
             "hypertension_detected_at" => "required|date|before:" . now(),
             "hypertension_status"      => "required|combo:hypertension-status",
        ];
    }



    /**
     * get hypertriglyceridemia-related validation rules
     *
     * @return array
     */
    protected function hypertriglyceridemiaRules()
    {
        return [
             "hypertriglyceridemia" => "required|boolean",
             "hypercholesterolemia" => "required|boolean",
        ];
    }



    /**
     * get malignancy-related validation rules
     *
     * @return array
     */
    protected function malignancyRules()
    {
        if (!$this->getData("malignancy_has")) {
            $this->setData("malignancy_type", null);
            $this->setData("malignancy_detection", null);
            $this->setData("malignancy_cure_time", null);
            $this->setData("malignancy_cure_status", null);
            $this->setData("malignancy_others", null);

            return [
                 "malignancy_has" => "required|boolean",
            ];
        }

        return [
             "malignancy_type"        => "required|combo:type-of-malignancy",
             "malignancy_detection"   => "required|numeric|between:1300," . jdate()->format("Y"),
             "malignancy_cure_time"   => "required|numeric|between:0,20",
             "malignancy_cure_status" => "",
             "malignancy_others"      => "required_if:malignancy_type,type-of-malignancy-others",
        ];
    }



    /**
     * get mental-related validation rules
     *
     * @return array
     */
    protected function mentalRules()
    {
        if (!$this->getData("mental_illness_has")) {
            $this->setData("mental_illness_type", null);
        }

        return [
             "mental_illness_has"  => "required|boolean",
             "mental_illness_type" => "required_if:mental_illness_has,1",
        ];
    }



    /**
     * get lung-related validation rules //TODO: Replace combo subject when determined!
     *
     * @return array
     */
    protected function lungRules()
    {
        if (!$this->getData("lung_illness_has")) {
            $this->setData("lung_illness_type", null);
        }

        return [
             "lung_illness_has"  => "required|boolean",
             "lung_illness_type" => "required_if:lung_illness_has,1|combo:type-of-malignancy",
        ];
    }



    /**
     * get rheumatology-related validation rules //TODO: Replace combo subject when determined!
     *
     * @return array
     */
    protected function rheumatologyRules()
    {
        if (!$this->getData("rheumatology_has")) {
            $this->setData("rheumatology_type", null);
        }

        return [
             "rheumatology_has"  => "required|boolean",
             "rheumatology_type" => "required_if:rheumatology_has,1|combo:type-of-malignancy",
        ];
    }
}
