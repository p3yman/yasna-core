<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaListRequest;


class SickListRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true; //TODO: To be replaced when criteria is given.
    }
}
