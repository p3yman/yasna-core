<?php

namespace Modules\RegistryWaitings\Http\Requests\V1;

use App\Models\RegistrySick;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property RegistrySick $model
 */
class SickDeleteRequest extends YasnaFormRequest
{
    protected $model_name = "RegistrySick";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (!$this->model->isDeletable()) {
            return false;
        }

        return true; //TODO: To be completed when criteria supplied.
    }
}
