<?php

namespace Modules\RegistryWaitings\Providers;

use Modules\RegistryWaitings\Http\Endpoints\V1\SickDeleteEndpoint;
use Modules\RegistryWaitings\Http\Endpoints\V1\SickHistoryEndpoint;
use Modules\RegistryWaitings\Http\Endpoints\V1\SickListEndpoint;
use Modules\RegistryWaitings\Http\Endpoints\V1\SickLiverSaveEndpoint;
use Modules\RegistryWaitings\Http\Endpoints\V1\SickRollbackEndpoint;
use Modules\RegistryWaitings\Http\Endpoints\V1\SickSingleEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class RegistryWaitingsServiceProvider
 *
 * @package Modules\RegistryWaitings\Providers
 */
class RegistryWaitingsServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
    }



    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        endpoint()->register(SickSingleEndpoint::class);
        endpoint()->register(SickListEndpoint::class);
        endpoint()->register(SickHistoryEndpoint::class);
        endpoint()->register(SickDeleteEndpoint::class);

        endpoint()->register(SickLiverSaveEndpoint::class);
        endpoint()->register(SickRollbackEndpoint::class);
    }
}
