<?php

namespace Modules\RegistryWaitings\Entities;

use Modules\Histories\Entities\Traits\HistoriesTrait;
use Modules\RegistryWaitings\Entities\Traits\SickAccessorsTrait;
use Modules\RegistryWaitings\Entities\Traits\SickElectorsTrait;
use Modules\RegistryWaitings\Entities\Traits\SickRelationsTrait;
use Modules\RegistryWaitings\Entities\Traits\SickResourceTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistrySick extends YasnaModel
{
    use SoftDeletes;
    use HistoriesTrait;

    use SickAccessorsTrait;
    use SickResourceTrait;
    use SickElectorsTrait;
    use SickRelationsTrait;



    /**
     * @inheritdoc
     */
    public function batchSave($request, $overflow_parameters = [])
    {
        $saved = parent::batchSave($request, $overflow_parameters);

        $this->addHistory($saved);

        return $saved;
    }



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
            //TODO: Fill this with the names of your meta fields, or remove the method if you do not want meta fields at all.
        ];
    }



    /**
     * check if the model is in a deletable state. It is not checking permissions etc.
     *
     * @return boolean
     */
    public function isDeletable()
    {
        if (!$this->isFullyCreated()) {
            return true;
        }

        $setting       = get_setting("sick_deletable_days_after_create");
        $created_at    = carbon()::parse($this->created_at);
        $allowed_until = $created_at->addDays($setting);

        return now() < $allowed_until;
    }



    /**
     * check if the model is fully created by completing all four sections of the create form.
     *
     * @return bool
     */
    public function isFullyCreated()
    {
        return (bool)$this->liver_status;
    }
}
