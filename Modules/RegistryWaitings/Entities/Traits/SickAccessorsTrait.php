<?php

namespace Modules\RegistryWaitings\Entities\Traits;

/**
 * @property int $age
 */
trait SickAccessorsTrait
{
    /**
     * get Age accessor.
     *
     * @return int
     */
    public function getAgeAttribute()
    {
        return carbon()::parse($this->getAttribute('birth_date'))->age;
    }



    /**
     * identify if the person is mature.
     *
     * @return bool
     */
    public function isMature()
    {
        return $this->age >= 18;
    }



    /**
     * identify if the person is a child.
     *
     * @return bool
     */
    public function isChild()
    {
        return !$this->isMature();
    }
}
