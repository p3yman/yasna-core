<?php

namespace Modules\RegistryWaitings\Entities\Traits;

use App\Models\RegistryHospital;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait SickRelationsTrait
{
    /**
     * get the relationship to transplant hospital record.
     *
     * @return BelongsTo
     */
    public function transHospital()
    {
        return $this->belongsTo(RegistryHospital::class, "trans_hospital_id");
    }
}
