<?php

namespace Modules\RegistryWaitings\Entities\Traits;

use Carbon\Carbon;

trait SickElectorsTrait
{

    /**
     * define NameFirst elector to filter by name.
     *
     * @param string $value
     *
     * @return void
     */
    protected function electorNameFirst($value): void
    {
        $this->elector()->where("name_first", $value);
    }



    /**
     * define NameLast elector to filter by name.
     *
     * @param string $value
     *
     * @return void
     */
    protected function electorNameLast($value): void
    {
        $this->elector()->where("name_last", $value);
    }



    /**
     * define code_melli elector to filter by name.
     *
     * @param string $value
     *
     * @return void
     */
    protected function electorCodeMelli($value): void
    {
        $this->elector()->where("code_melli", $value);
    }



    /**
     * define LiverPriority elector to filter by waiting-list status.
     *
     * @param bool $value
     *
     * @return void
     */
    protected function electorLiverPriority($value): void
    {
        $this->elector()->where("liver_priority", $value);
    }



    /**
     * define Country elector to filter by country.
     *
     * @param string|int $value
     *
     * @return void
     */
    protected function electorCountry($value): void
    {
        $value = hashid_number($value);
        $this->elector()->where("country_id", $value);
    }



    /**
     * define province elector to filter by province.
     *
     * @param string|int $value
     *
     * @return void
     */
    protected function electorProvince($value): void
    {
        $value = hashid_number($value);
        $this->elector()->where("province_id", $value);
    }



    /**
     * define city elector to filter by city.
     *
     * @param string|int $value
     *
     * @return void
     */
    protected function electorCity($value): void
    {
        $value = hashid_number($value);
        $this->elector()->where("city_id", $value);
    }



    /**
     * define BloodType elector to filter by blood type.
     *
     * @param string $value
     *
     * @return void
     */
    protected function electorBloodType($value): void
    {
        $this->elector()->where("blood_type", $value);
    }



    /**
     * define LiverEnteredFrom elector to filter data entered after a certain date in the liver waiting list.
     *
     * @param int|string|Carbon $value
     *
     * @return void
     */
    protected function electorLiverEnteredAfter($value): void
    {
        if (is_int($value)) {
            $value = carbon()::createFromTimestamp($value);
        }

        $this->elector()->whereDate("liver_entered_at", ">=", $value);
    }



    /**
     * define LiverEnteredFrom elector to filter data entered before a certain date in the liver waiting list.
     *
     * @param int|string|Carbon $value
     *
     * @return void
     */
    protected function electorLiverEnteredBefore($value): void
    {
        if (is_int($value)) {
            $value = carbon()::createFromTimestamp($value);
        }

        $this->elector()->whereDate("liver_entered_at", "<=", $value);
    }



    /**
     * define Age elector to filter by age.
     *
     * @param int $value
     *
     * @return void
     */
    protected function electorAge($value): void
    {
        $min = now()->subYears($value + 1);
        $max = now()->subYears($value);

        $this->elector()->whereBetween("birth_date", [$min, $max]);
    }
}
