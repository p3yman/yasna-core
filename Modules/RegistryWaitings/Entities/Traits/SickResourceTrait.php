<?php

namespace Modules\RegistryWaitings\Entities\Traits;

use App\Models\RegistryHospital;

trait SickResourceTrait
{
    /**
     * boot SickResourceTrait
     *
     * @return void
     */
    public static function bootSickResourceTrait()
    {
        static::addDirectResources('*');
    }



    /**
     * get array of resources involved in General resource group. (made public to be accessible in the request layer.)
     *
     * @return array
     */
    public function getLiverGeneralResourceGroup()
    {
        return [
             "name_first",
             "name_last",
             "code_melli",
             "gender",
             "blood_type",
             "blood_rh",
             "birth_date",
             "liver_entered_at",
             "trans_hospital_id",
             "education",
             "job_has",
             "job_type",
             "job_unemployment_reason",
             "job_not_full_reason",
             "job_no_work_reason",
             "insurance_has",
             "insurance_type",
             "insurance_co",
             "fund_sources",
             "phones",
             "mobiles",
             "nationality_id",
             "residency_id",
             "country_id",
             "province_id",
             "city_id",
             "home_postal_code",
             "home_address",
             "blood",
        ];
    }



    /**
     * get array of resources involved in Medical resource group. (made public to be accessible in the request layer.)
     *
     * @return array
     */
    public function getLiverMedicalResourceGroup()
    {
        return [
             "height",
             "weight",
             "measured_at",
             "bmi",
             "evolution_cognitive_status",
             "evolution_mobility_status",
             "edu_achievement",
             "edu_activity",
             "liver_underlying_disease",
             "functional_status",
             "liver_life_cure_liver_has",
             "liver_life_cure_liver_type",
             "liver_life_cure_liver_other",
             "cigar_has",
             "cigar_status",
             "cigar_quit_time",
             "cigar_consumption",
             "opium_has",
             "opium_quit",
             "alcohol_has",
             "alcohol_quit",
             "abdominal_surgery",
             "peritonitis_bacterial",
             "shunt_trans_jugular",
             "venous_thrombosis",
             "liver_need_other_organs",
             "liver_need_other_organs_names",
             "transplant_history",
             "transplant_history_details",
             "diabetes_has",
             "diabetes_type",
             "diabetes_detected_at",
             "diabetes_control",
             "diabetes_cure",
             "kidney_problem_has",
             "kidney_problem_type",
             "dialysis_history",
             "neoadjuvant_therapy",
             "hcc_has",
             "brain_vein_problems",
             "hypertension_has",
             "hypertension_detected_at",
             "hypertension_status",
             "hypertriglyceridemia",
             "hypercholesterolemia",
             "malignancy_has",
             "malignancy_type",
             "malignancy_detection",
             "malignancy_cure_time",
             "malignancy_cure_status",
             "malignancy_others",
             "mental_illness_has",
             "mental_illness_type",
             "lung_illness_has",
             "lung_illness_type",
             "rheumatology_has",
             "rheumatology_type",
             "liver_medical_notes",
        ];
    }



    /**
     * get array of resources involved in clinical resource group. (made public to be accessible in the request layer.)
     *
     * @return array
     */
    public function getLiverClinicalResourceGroup()
    {
        return [
             "test_cr",
             "test_bili_t",
             "test_inr",
             "test_na",
             "test_alb",
             "liver_tests_updated_at",
             "liver_tests_file",
             "_file_liver_tests_file",
             "peld_score",
             "meld_score",
        ];
    }



    /**
     * get array of resources involved in State resource group. (made public to be accessible in the request layer.)
     *
     * @return array
     */
    public function getLiverStateResourceGroup()
    {
        return [
             "liver_priority",
             "liver_priority_type",
             "liver_priority_reason",
             "liver_priority_reason_other",
             "liver_priority_technical",
             "liver_priority_technical_other",
             "liver_other_deceases",
             "liver_urgent",
             "liver_urgent_reason",
             "liver_non_urgent_need",
             "liver_letter_file",
             "_file_liver_letter_file",
             "liver_activated_at",
             "liver_deactivated_at",
             "liver_reactivated_at",
             "liver_status",
             "liver_received_at",
             "died_at",
             "died_for_liver",
             "death_reason_non_liver",
        ];
    }



    /**
     * get array of resources involved in Info resource group.
     *
     * @return array
     */
    protected function getInfoResourceGroup()
    {
        return [
             "name_first",
             "name_last",
             "code_melli",
             "trans_hospital_id",
             "liver_entered_at",
             "blood_type",
             "blood_rh",
             "liver_status",
             "birth_date",
             "_age",
             "_is_mature",
             "_is_child",
             "_created_by",
             "_trans_hospital",
        ];
    }



    /**
     * get LiverTestsFile resource.
     *
     * @return array
     */
    protected function getLiverTestsFileResource()
    {
        return explode_not_empty(",", $this->getAttribute("liver_tests_file"));
    }



    /**
     * get FileLiverTestsFile resource.
     *
     * @return array
     */
    protected function getFileLiverTestsFileResource()
    {
        return $this->getResourceForFiles($this->getAttribute("liver_tests_file"));
    }



    /**
     * get LiverLetterFile resource.
     *
     * @return array
     */
    protected function getLiverLetterFileResource()
    {
        return explode_not_empty(",", $this->getAttribute("liver_letter_file"));
    }



    /**
     * get file_liver_letter_file resource.
     *
     * @return array
     */
    protected function getFileLiverLetterFileResource()
    {
        return $this->getResourceForFiles($this->getAttribute("liver_letter_file"));
    }



    /**
     * get LiverNeedOtherOrgansNames resource.
     *
     * @return array
     */
    protected function getLiverNeedOtherOrgansNamesResource()
    {
        return explode_not_empty(",", $this->getAttribute("liver_need_other_organs_names"));
    }



    /**
     * get BirthDate resource.
     *
     * @return int
     */
    protected function getBirthDateResource()
    {
        return $this->getResourceForTimestamps($this->getAttribute("birth_date"));
    }



    /**
     * get Age resource.
     *
     * @return int
     */
    protected function getAgeResource()
    {
        return $this->age;
    }



    /**
     * get IsChild resource.
     *
     * @return boolean
     */
    protected function getIsChildResource()
    {
        return $this->isChild();
    }



    /**
     * get IsMature resource.
     *
     * @return boolean
     */
    protected function getIsMatureResource()
    {
        return $this->isMature();
    }



    /**
     * get blood resource.
     *
     * @return string
     */
    protected function getBloodResource()
    {
        return $this->getAttribute("blood_type") . $this->getAttribute("blood_rh");
    }



    /**
     * get fund-sources resource.
     *
     * @return array
     */
    protected function getFundSourcesResource()
    {
        return json_decode($this->getAttribute("fund_sources"));
    }



    /**
     * get insurance-type resource.
     *
     * @return array
     */
    protected function getInsuranceTypeResource()
    {
        return json_decode($this->getAttribute("insurance_type"));
    }



    /**
     * get mobiles resource.
     *
     * @return array
     */
    protected function getMobilesResource()
    {
        return json_decode($this->getAttribute("mobiles"));
    }



    /**
     * get phones resource.
     *
     * @return array
     */
    protected function getPhonesResource()
    {
        return json_decode($this->getAttribute("phones"));
    }



    /**
     * get transplant_history_details resource.
     *
     * @return array
     */
    protected function getTransplantHistoryDetailsResource()
    {
        return json_decode($this->getAttribute("transplant_history_details"));
    }



    /**
     * get trans_hospital resource.
     *
     * @return array
     */
    protected function getTransHospitalResource()
    {
        /** @var RegistryHospital $hospital */
        $hospital = $this->getAttribute("transHospital");

        if (!$hospital) {
            return null;
        }

        return [
             "id"   => $hospital->hashid,
             "name" => $hospital->name,
        ];
    }

}
