<?php

return [
     'evolution-status' => [
          "healthy"  => "سالم، بدون تأخیر یا اختلال",
          "probable" => "احتمالاً تأخیر یا اختلال دارد.",
          "certain"  => "قطعاً تأخیر یا اختلال دارد.",
     ],

     'edu-achievement' => [
          "same"      => "در سطح مشابه همسالان",
          "delay"     => "در سطح مدرک با تأخیر",
          "special"   => "تحصیلات ویژه",
          "na"        => "غیر قابل قضاوت در کمتر از پنج سال",
          "graduated" => "فارغ‌التحصیل دبیرستانی یا دانشگاهی",
          "unknown"   => "نامشخص",
     ],

     'liver-underlying-disease' => [
          "self"                 => "خودایمنی",
          "hepatitis-b"          => "هپاتیت B",
          "hepatitis-c"          => "هپاتیت C",
          "hepatitis-b-d"        => "هپاتیت B+D",
          "wilson"               => "ویلسون",
          "alcohol"              => "الکل",
          "budkari"              => "بودکیاری",
          "hcc"                  => "HCC",
          "cholangiocarcinoma"   => "کلانژیوکارسینوم سانترال",
          "carcinoid"            => "تومور نوروآندوکرین",
          "other-tumors"         => "سایر تومورها",
          "a1"                   => "کمبود آنتی‌تریپسین α1",
          "hyperoxoluria"        => "هیپراگزالوری",
          "hemochromatosis"      => "هموکروماتوزیس",
          "tyrosinemia"          => "تیروزنیمی",
          "cryptogenic"          => "کریپتوژنیک",
          "psc"                  => "PSC کلانژیت اسکلروزان اولیه",
          "pbc"                  => "PBC سیروز صفراوی اولیه",
          "pfic"                 => "PFIC کلستاز اینتراهپاتیک فامیلیال پیشرونده",
          "nash"                 => "NASH استئاتوهپاتیت غیرالکلی",
          "atresia"              => "آترزی مجازی صفراوی",
          "hypercholesterolemia" => "هایپرکلسترولمی",
          "liver-acute-failure"  => "نارسایی حاد کبدی (داروی، اتوایمیون، ناشناخته، سایر)",
          "others"               => "سایر",
     ],

     'functional-status' => [
          "1" => "درجه ۱: کاملا فعال، همه فعالیت‌های خود را بدون هیچ محدودیتی انجام می‌دهد.",
          "2" => "درجه ۲: در فعالیت بیشتر از معمول محدودیت دارد، ولی قادر به انجام کارهای سبک و روزمره هست.",
          "3" => "درجه ۳: در فعالیت‌های معمول خود نیز دچار محدودیت است.",
          "4" => "درجه ۴: کاملا غیرفعال است و نمی‌تواند هیچ مراقبتی از خود کند و کاملا در رختخواب یا صندلی قرار دارد.",
     ],

     'liver-life-cure' => [
          "ventilator" => "ونتیلاتور",
          "artificial" => "کبد مصنوعی",
          "others"     => "سایر",
     ],

     'liver-need-other-organs' => [
          "heart"    => "قلب",
          "kidney"   => "کلیه",
          "lung"     => "ریه",
          "pancreas" => "پانکراس",
          "gut"      => "روده",
          "marrow"   => "مغز استخوان",
     ],

     'dialysis-history' => [
          "no"         => "ندارد",
          "blood"      => "دیالیز خونی",
          "peritoneum" => "صفاقی",
          "unknown"    => "نامشخص",
     ],

     'hypertension-status' => [
          "c"  => "کنترل‌شده",
          "nc" => "کنترل‌نشده",
     ],

     'liver-priority-type' => [
          "1a"         => "1A",
          "1b"         => "1B",
          "calculated" => "Calculated MELD",
          "exception"  => "Exception MELD",
          "inactive"   => "Inactive Status",
     ],

     'liver-priority-reason' => [
          "heal"          => "بهبودی",
          "cancel"        => "انصراف از پیوند",
          "extreme"       => "شدیداً بدحال",
          "tests"         => "آزمایشات مختل",
          "infections"    => "عفونت فعال",
          "other-organs"  => "بیماری سایر ارگان‌ها",
          "psychological" => "اختلال سایکولوژیک",
          "opium"         => "اعتیاد فعال",
          "support"       => "حمایت ضعیف اقتصادی یا اجتماعی",
          "technical"     => "تکنیکی",
          "others"        => "سایر موارد",
     ],

     'liver-urgent-reason' => [
          "re"        => "Re Transplantation",
          "hepatitis" => "Fulminate Hepatitis Failure",
     ],

     'liver-status' => [
          "waiting" => "منتظر پیوند",
          "dead"    => "فوت قبل از پیوند",
          "unknown" => "نامعلوم",
     ],
];
