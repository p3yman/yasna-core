<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrySicksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry_sicks', function (Blueprint $table) {
            $table->increments('id');

            $this->generalInfo($table);
            $this->medicalInfo($table);
            $this->testsInfo($table);
            $this->listInfo($table);
            $this->transplantInfo($table);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registry_sicks');
    }



    /**
     * migrate general info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function generalInfo(Blueprint $table)
    {
        $table->string('code_melli', 20)->nullable()->index();
        $table->string('name_first', 50)->nullable()->index();
        $table->string('name_last', 50)->nullable()->index();
        $table->tinyInteger('gender')->default(0)->index();
        $table->date('birth_date')->nullable()->index();
        $table->string("blood_type", 2)->nullable();
        $table->string("blood_rh", 2)->nullable();

        $table->timestamp("liver_entered_at")->nullable()->index();
        $table->unsignedInteger("trans_hospital_id")->default(0)->index();
        $table->unsignedInteger("trans_ward_id")->default(0)->index();
        $table->unsignedInteger("trans_opu_id")->default(0)->index();
        $table->unsignedInteger("trans_university_id")->default(0)->index();

        $table->string('education', 30)->nullable();
        $table->boolean('job_has')->default(0);
        $table->string('job_type', 30)->nullable();
        $table->string('job_unemployment_reason', 40)->nullable();
        $table->string('job_not_full_reason', 40)->nullable();
        $table->string('job_no_work_reason', 40)->nullable();

        $table->boolean('insurance_has')->default(0);
        $table->text('insurance_type')->nullable();
        $table->string('insurance_co', 50)->nullable();

        $table->text('fund_sources')->nullable();

        $table->text('phones')->nullable();
        $table->text('mobiles')->nullable();

        $table->unsignedInteger('nationality_id')->default(0);
        $table->unsignedInteger('residency_id')->default(0);
        $table->unsignedInteger('country_id')->default(0);
        $table->unsignedInteger('province_id')->default(0);
        $table->unsignedInteger('city_id')->default(0);
        $table->string('home_postal_code', 20)->nullable();
        $table->text('home_address')->nullable();
    }



    /**
     * migrate medical info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function medicalInfo(Blueprint $table)
    {
        $table->integer("height")->default(0);
        $table->float("weight")->default(0);
        $table->timestamp('measured_at')->nullable();
        $table->float("bmi")->default(0);

        $table->string("evolution_cognitive_status", 30)->nullable();
        $table->string("evolution_mobility_status", 30)->nullable();
        $table->string("edu_achievement", 30)->nullable();
        $table->string("edu_activity", 30)->nullable();

        $table->text("liver_underlying_disease")->nullable();
        $table->string("functional_status", 30)->nullable();

        $table->boolean("liver_life_cure_liver_has")->default(0);
        $table->string("liver_life_cure_liver_type", 30)->nullable();
        $table->text("liver_life_cure_liver_other")->nullable();

        $table->boolean("cigar_has")->default(0);
        $table->string("cigar_status", 30)->nullable();
        $table->string("cigar_quit_time", 30)->nullable();
        $table->string("cigar_consumption", 30)->nullable();

        $table->boolean("opium_has")->default(0);
        $table->boolean("opium_quit")->default(0);
        $table->boolean("alcohol_has")->default(0);
        $table->boolean("alcohol_quit")->default(0);

        $table->boolean("abdominal_surgery")->default(0);
        $table->string("peritonitis_bacterial", 30)->nullable();
        $table->string("shunt_trans_jugular", 30)->nullable();
        $table->string("venous_thrombosis", 30)->nullable();

        $table->boolean("liver_need_other_organs")->default(0);
        $table->text("liver_need_other_organs_names")->nullable();
        $table->boolean("transplant_history")->default(0);
        $table->text("transplant_history_details")->nullable();

        $table->boolean("diabetes_has")->default(0);
        $table->string("diabetes_type", 30)->nullable();
        $table->timestamp("diabetes_detected_at")->nullable();
        $table->string("diabetes_control", 30)->nullable();
        $table->string("diabetes_cure", 30)->nullable();

        $table->boolean("kidney_problem_has")->default(0);
        $table->text("kidney_problem_type")->nullable();

        $table->string("dialysis_history", 30)->nullable();
        $table->boolean("neoadjuvant_therapy")->default(0);
        $table->boolean("hcc_has")->default(0);
        $table->boolean("brain_vein_problems")->default(0);
        $table->boolean("hypertension_has")->default(0);
        $table->timestamp("hypertension_detected_at")->nullable();
        $table->string("hypertension_status", 30)->nullable();

        $table->boolean("hypertriglyceridemia")->default(0);
        $table->boolean("hypercholesterolemia")->default(0);

        $table->boolean("malignancy_has")->default(0);
        $table->text("malignancy_type")->nullable();
        $table->text("malignancy_detection")->nullable();
        $table->tinyInteger("malignancy_cure_time")->default(0);
        $table->text("malignancy_cure_status")->nullable();
        $table->text("malignancy_others")->nullable();

        $table->boolean("mental_illness_has")->default(0);
        $table->text("mental_illness_type")->nullable();

        $table->boolean("lung_illness_has")->default(0);
        $table->string("lung_illness_type", 30)->nullable();

        $table->boolean("rheumatology_has")->default(0);
        $table->string("rheumatology_type", 30)->nullable();

        $table->text("liver_medical_notes")->nullable();
    }



    /**
     * migrate kidney tests
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function testsInfo(Blueprint $table)
    {
        $table->float("test_cr")->nullable();
        $table->float("test_bili_t")->nullable();
        $table->float("test_inr")->nullable();
        $table->float("test_na")->nullable();
        $table->float("test_alb")->nullable();
        $table->timestamp("liver_tests_updated_at")->nullable();
        $table->unsignedInteger("liver_tests_updated_by")->default(0);
        $table->text("liver_tests_file")->nullable();

        $table->float("peld_score")->nullable();
        $table->float("meld_score")->nullable();
    }



    /**
     * migrate list info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function listInfo(Blueprint $table)
    {
        $table->boolean("liver_priority")->default(0);
        $table->string("liver_priority_type", 30)->nullable();
        $table->string("liver_priority_reason", 30)->nullable();
        $table->text("liver_priority_reason_other")->nullable();
        $table->string("liver_priority_technical", 30)->nullable();
        $table->text("liver_priority_technical_other")->nullable();

        $table->text("liver_other_deceases")->nullable();

        $table->boolean("liver_urgent")->default(0);
        $table->string("liver_urgent_reason", 30)->nullable();
        $table->boolean("liver_non_urgent_need")->default(0);
        $table->text("liver_letter_file")->nullable();

        $table->timestamp("liver_activated_at")->nullable();
        $table->timestamp("liver_deactivated_at")->nullable();
        $table->timestamp("liver_reactivated_at")->nullable();
        $table->integer("liver_active_time")->nullable();
        $table->integer("liver_inactive_time")->nullable();

        $table->string("liver_status", 30)->nullable();

        $table->unsignedInteger("liver_confirmed_by")->default(0);
        $table->unsignedInteger("liver_updated_by")->default(0);
        $table->timestamp("liver_updated_at")->nullable();
    }



    /**
     * migrate transplant info
     *
     * @param Blueprint $table
     *
     * @return void
     */
    private function transplantInfo(Blueprint $table)
    {
        $table->timestamp("liver_received_at")->nullable();
        $table->timestamp("died_at")->nullable();
        $table->boolean("died_for_liver")->default(0);
        $table->mediumText("death_reason_non_liver")->nullable();
    }

}
