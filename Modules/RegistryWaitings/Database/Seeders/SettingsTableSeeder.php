<?php

namespace Modules\RegistryWaitings\Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()::seed("settings", $this->getData());
    }



    /**
     * get seeding data
     *
     * @return array
     */
    private function getData()
    {
        return [
             [
                  'slug'             => 'sick_deletable_days_after_create',
                  'title'            => trans('registry-waitings::seeder.setting.sick_deletable_days_after_create'),
                  'category'         => 'template',
                  'order'            => '102',
                  'data_type'        => 'text',
                  'default_value'    => 5,
                  "api_discoverable" => "1",
             ],
        ];
    }
}
