<?php

namespace Modules\RegistryWaitings\Database\Seeders;

use Illuminate\Database\Seeder;

class RegistryWaitingsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CombosTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
