<?php

namespace Modules\Aids\Listeners;

use App\Models\SupportTicket;
use App\Models\TimelineNode;
use Illuminate\Support\Facades\Notification;
use Modules\Aids\Notifications\NewTicketOpenedNotification;
use Modules\Timeline\Events\NodeInserted;
use Modules\Timeline\Services\NodeAbstract;

class NotifyPeopleOnNodeInserted
{
    /** @var NodeInserted */
    private $event;

    /** @var SupportTicket */
    private $model;

    /** @var NodeAbstract */
    private $node;

    /** @var TimelineNode */
    private $record;



    /**
     * Handle the event.
     *
     * @param NodeInserted $event
     *
     * @return void
     */
    public function handle(NodeInserted $event)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($event->model->getClassName() != "SupportTicket") { // <~~ Irrelevant!
            return;
        }

        $method = "handle" . ($event->node)::calledClassName();
        if (!method_exists($this, $method)) {
            return;
        }

        /*-----------------------------------------------
        | Action ...
        */
        $this->event  = $event;
        $this->model  = $event->model;
        $this->node   = $event->node;
        $this->record = $this->node->record;

        $this->$method();
    }



    /**
     * handle the listener if the TicketOpenedNode was inserted.
     *
     * @return void
     */
    protected function handleTicketOpenedNode()
    {
        if ($this->model->customer_id != $this->model->created_by) {
            return; // <~~ tickets opened by an admin do not require notifications.
        }

        Notification::send(user(2), new NewTicketOpenedNotification($this->model));
        //TODO: list of admins should get the notification!
    }
}
