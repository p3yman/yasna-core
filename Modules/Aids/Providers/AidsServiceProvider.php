<?php

namespace Modules\Aids\Providers;

use Modules\Aids\Listeners\NotifyPeopleOnNodeInserted;
use Modules\Timeline\Events\NodeInserted;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class AidsServiceProvider
 *
 * @package Modules\Aids\Providers
 */
class AidsServiceProvider extends YasnaProvider
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->mapModuleVersions();
        $this->registerModelTraits();
        $this->registerListeners();
    }



    /**
     * register ModelTraits
     *
     * @return void
     */
    private function registerModelTraits()
    {
        $this->addModelTrait("TicketAidsTrait", "SupportTicket");
    }



    /**
     * map modules versions
     *
     * @return void
     */
    private function mapModuleVersions()
    {
        endpoint()->mapModulesVersion(1, [
             "endpoint" => "1",
             "support"  => "1",
        ]);
    }



    /**
     * register Listeners
     *
     * @return void
     */
    private function registerListeners()
    {
        $this->listen(NodeInserted::class, NotifyPeopleOnNodeInserted::class);
    }

}
