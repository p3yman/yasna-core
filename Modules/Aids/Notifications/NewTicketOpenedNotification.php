<?php

namespace Modules\Aids\Notifications;

use App\Models\SupportTicket;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class NewTicketOpenedNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    /** @var SupportTicket */
    protected $ticket;



    /**
     * NewTicketOpenedNotification constructor.
     *
     * @param SupportTicket $ticket
     */
    public function __construct(SupportTicket $ticket)
    {
        $this->ticket = $ticket;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * get the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
             ->subject($this->getEmailSubject())
             ->priority(4 - $this->ticket->priority)
             ->from("bot@support.yasna.team", trans("aids::messages.mail_sender"))
             ->greeting(trans("aids::messages.new_ticket_email.greeting"))
             ->line(trans("aids::messages.new_ticket_email.line_intro"))
             ->line($this->getSubjectLine())
             ->line($this->getTextLine())
             ->action(trans("aids::messages.new_ticket_email.action"), 'https://support.yasna.team')
             ->line(trans("aids::messages.new_ticket_email.line_end"))
             ;
    }



    /**
     * get email subject
     *
     * @return string
     */
    private function getEmailSubject()
    {
        return trans("aids::messages.new_ticket_email.subject", [
             "priority" => $this->ticket->getResource("priority_text"),
        ]);
    }



    /**
     * get subject line, including the subject of the ticket.
     *
     * @return string
     */
    private function getSubjectLine()
    {
        return trans("aids::messages.new_ticket_email.line_subject", [
             "subject" => $this->ticket->subject,
        ]);
    }



    /**
     * get text line, including the text of the ticket.
     *
     * @return string
     */
    private function getTextLine()
    {
        return trans("aids::messages.new_ticket_email.line_text", [
             "text" => $this->ticket->text,
        ]);
    }
}
