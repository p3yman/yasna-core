<?php
return [
     "mail_sender"      => "پشتیبانی یسناتیم",
     'new_ticket_email' => [
          "subject"      => "تیکت جدید (:priority)",
          "greeting"     => "سلام",
          "line_intro"   => "تیکت جدیدی در پشتیبانی یسناتیم دریافت شده است.",
          "line_subject" => "موضوع: :subject",
          "line_text"    => "متن: :text",
          "action"       => "اینجا ببینید",
          "line_end"     => "سپاس از توجه شما.",
     ],
];
