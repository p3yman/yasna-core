<?php

namespace Modules\Aids\Entities\Traits;

/**
 * @property int $customer_id
 */
trait TicketAidsTrait
{
    /**
     * @inheritdoc
     */
    public function canView()
    {
        return user()->id == $this->customer_id or user()->isAdmin();
    }

    /**
     * get PriorityText resource.
     *
     * @return string
     */
    protected function getPriorityTextResource()
    {
        return trans("support::tickets.priority.". $this->getAttribute("priority")) ;
    }

}
