<?php

Route::group(['middleware' => 'web', 'prefix' => 'googlemap', 'namespace' => 'Modules\GoogleMap\Http\Controllers'], function () {
    Route::get('/', 'GoogleMapController@index');
});
