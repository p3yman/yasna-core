let mapItems = [];
let clientCoords = null;
let currentLocationFlag = "__CURRENT__";

let mapJsLoadingDeferred = $.Deferred();

function googleMapsJsLoadingCallback() {
    mapJsLoadingDeferred.resolve();
}

function getClientLocation(successCallback, failCallback) {
    if (clientCoords) {
        if (typeof successCallback == 'function') {
            successCallback(clientCoords);
        }
    } else if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            if (typeof successCallback == 'function') {
                clientCoords = position.coords;
                successCallback(position.coords);
            }
        }, function (error) {
            if (typeof failCallback == 'function') {
                failCallback(error);
            }
        });
    } else {
        console.log("Geolocation is not supported by this browser.")
    }
}

function modifyData(source, changes) {
    if ($.isPlainObject(changes)) {
        $.each(changes, function (key, value) {
            if ($.type(source[key]) !== 'undefined') {
                if ($.isPlainObject(source[key]) && $.isPlainObject(value)) {
                    source[key] = modifyData(source[key], value);
                } else {
                    source[key] = value;
                }
            }
        });
    }

    return source;
}

function getValueOf(data) {
    return $.parseJSON(JSON.stringify(data));
}


//Classes
function MapItem(data) {
    let statuses = {
        WAITING: 0,
        LOADED: 1,
        REJECTED: -1
    };
    let thisMapItem = this;
    thisMapItem.key = data.elementId;
    thisMapItem.modified = false;
    thisMapItem.originalData = getValueOf(data);

    delete data.elementId;

    thisMapItem.__constructor = function (givenData) {
        thisMapItem.data = givenData;
        thisMapItem.element = $('#' + thisMapItem.key);
        thisMapItem.markers = {};
        thisMapItem.neededCliendLocation = null;
        changeStatus('WAITING');

        if (thisMapItem.data.autoGenerate) {
            mapContainer.generateMap();
        }
    };

    thisMapItem.generateMap = function () {
        mapJsLoadingDeferred.done(function () {

            if (thisMapItem.needsClientLocation()) {
                getClientLocation(function (coords) {
                    thisMapItem.data.mapOptions.center = [coords.latitude, coords.longitude];
                    thisMapItem.renderMap();
                }, function () {
                    google.maps.event.addListenerOnce(thisMapItem.map, 'tilesloaded', function () {
                        statusChanged('REJECTED');
                    });
                });
            } else {
                thisMapItem.renderMap();
            }
        });
    };

    thisMapItem.needsClientLocation = function () {
        if (thisMapItem.neededCliendLocation === null) {
            if (thisMapItem.data.mapOptions.center === currentLocationFlag) {
                thisMapItem.neededCliendLocation = true;
            } else {
                let result = false;
                $.each(thisMapItem.data.markers, function (index, marker) {
                    if (marker.position === currentLocationFlag) {
                        result = true;
                        return false;
                    }
                });
                thisMapItem.neededCliendLocation = result;
            }
        }

        return thisMapItem.neededCliendLocation;
    };

    thisMapItem.renderMap = function () {
        let options = thisMapItem.data.mapOptions;
        let center = new google.maps.LatLng(options.center[0], options.center[1]);

        $('#' + thisMapItem.key).html('');

        thisMapItem.map = new google.maps.Map(document.getElementById(thisMapItem.key), {
            center: center,
            zoom: 16
        });

        google.maps.event.addListenerOnce(thisMapItem.map, 'tilesloaded', function () {
            changeStatus('LOADED');
        });

        thisMapItem.renderMapMarkers();

    };

    thisMapItem.renderMapMarkers = function () {
        $.each(thisMapItem.data.markers, function (index, marker) {
            if (marker.position === currentLocationFlag) {
                getClientLocation(function (coords) {
                    marker.position = [coords.latitude, coords.longitude];
                    thisMapItem.addMarker(marker);
                });

            } else {
                thisMapItem.addMarker(marker)
            }
        });
    };

    thisMapItem.addMarker = function (marker) {
        let position = new google.maps.LatLng(marker.position[0], marker.position[1]);
        let thisMarker = new google.maps.Marker({
            position: position,
            map: thisMapItem.map,
            draggable: true,
        });
        window.ss = thisMarker;

        $.each(thisMapItem.data.callbacks.allMarkersCallbacks, function (event, handler) {
            google.maps.event.addListener(thisMarker, event, function () {
                arguments[Object.keys(arguments).length] = thisMarker;
                handler.apply(arguments, Object.values(arguments));
            });
        });


        thisMapItem.markers[Object.keys(thisMapItem.markers).length] = thisMarker;
    };

    thisMapItem.getStatus = function () {
        let statusString = Object.keys(statuses)[$.inArray(thisMapItem.status, Object.values(statuses))];
        if (statusString > -1) {
            return statusString;
        } else {
            return null;
        }
    };

    thisMapItem.isLoaded = function () {
        if (thisMapItem.getStatus() == 'LOADED') {
            return true;
        } else {
            return false;
        }
    }

    thisMapItem.isEnded = function () {
        if (thisMapItem.getStatus() == 'WAITING') {
            return false;
        } else {
            return true;
        }
    }

    thisMapItem.reset = function (givenData) {
        thisMapItem.modified = true;

        let newData = modifyData(thisMapItem.data, givenData);
        thisMapItem.__constructor(newData);
    };

    thisMapItem.refresh = function () {
        let newData = getValueOf(thisMapItem.originalData);
        thisMapItem.reset(newData);
    };

    function changeStatus(status) {
        let statusVal = statuses[status.toUpperCase()];
        thisMapItem.status = statusVal;
        statusChanged();
    }

    function statusChanged() {
        let callbacks = [];

        if (thisMapItem.status == 0) {

        }

        if (thisMapItem.status == 1) {
            callbacks.push('completedCallback');
            callbacks.push('allowedCallback');
        }

        if (thisMapItem.status == -1) {
            callbacks.push('completedCallback');
            callbacks.push('notAllowedCallback');
        }

        $.each(callbacks, function (index, event) {
            let functionVar = thisMapItem.data.callbacks[event];
            if (functionVar && ($.type(functionVar) == 'function')) {
                functionVar();
            }
        });

    }

    thisMapItem.__constructor(data);

    return thisMapItem;
}

function Maps(items) {
    let thisMaps = this;

    thisMaps.items = {};

    $.each(items, function (index, item) {
        thisMaps.items[item.elementId] = new MapItem(item);
    });

    this.findMapItem = function (mapElementId) {
        return thisMaps.items[mapElementId];
    };

    return thisMaps;
}