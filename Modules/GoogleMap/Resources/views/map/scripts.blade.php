<script>
    mapItems.push({
        elementId: "{{ $element or ('map_' . str_random(6)) }}",
        autoGenerate: {{ intval($autoGenerate) }},
        mapOptions: {
            center: "__CURRENT__",
        },
        callbacks: {
            allowedCallback: {{ $allowedCallback or '' }},
            notAllowedCallback: {{ $notAllowedCallback or '' }},
            completedCallback: {{ $completedCallback or '' }},
            allMarkersCallbacks: {
            @foreach($allMarkersCallbacks as $event => $handler)
            {{ $event }}: {{ $handler }},
            @endforeach
            },
        },
        markers: {!! json_encode($markers) !!},
    });
</script>