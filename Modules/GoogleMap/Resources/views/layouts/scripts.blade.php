{!! Html::script(Module::asset('googlemap:js/google-map-additions.min.js')) !!}

@foreach(GoogleMapTools::getRegisteredMaps() as $map)
    {!! $map->getScripts() !!}
@endforeach

{!! $additionalScripts !!}

{!! Html::script(Module::asset('googlemap:js/google-map-setup.min.js')) !!}
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=&callback=googleMapsJsLoadingCallback"
        type="text/javascript"></script>
