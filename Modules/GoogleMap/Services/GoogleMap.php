<?php

namespace Modules\GoogleMap\Services;

use Modules\GoogleMap\Providers\GoogleMapToolsServiceProvider;

class GoogleMap
{
    protected $data = [
        "element" => null,
        "center"  => null,
        "zoom"    => 8,
        "markers" => [],

        "autoGenerate"      => true,
        "setCenterAsMarker" => false,

        "allowedCallback"    => null,
        "notAllowedCallback" => null,
        "completedCallback"  => null,

        "allMarkersCallbacks" => [],
    ];


    public function __construct(array $param = [])
    {
        foreach ($param as $key => $value) {
            $this->$key($value);
        }

        GoogleMapToolsServiceProvider::registerMap($this);
    }

    public function __call($method, $parameters)
    {
        if (array_has($this->data, $method)) {
            return $this->setProperty($method, ...$parameters);
        }

        trigger_error(
            'Call to undefined method ' . __CLASS__ . '::' . $method . '()',
            E_USER_ERROR
        );
    }

    protected function setProperty($propertyName, $propertyValue)
    {
        $this->data[$propertyName] = $propertyValue;

        return $this;
    }

    public function autoGenerate($value)
    {
        return $this->setProperty('autoGenerate', boolval($value));
    }

    public function getView()
    {
        return view('googlemap::map.main', $this->data);
    }

    public function getScripts()
    {
        return view('googlemap::map.scripts', $this->data);
    }
}
