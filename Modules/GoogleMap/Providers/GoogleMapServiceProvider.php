<?php

namespace Modules\GoogleMap\Providers;

use Modules\Yasna\Services\YasnaProvider;

class GoogleMapServiceProvider extends YasnaProvider
{


    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->addProviders();
        $this->addAliases();
    }



    /**
     * add providers
     *
     * @return void
     */
    protected function addProviders()
    {
        $this->addProvider(GoogleMapToolsServiceProvider::class);
    }



    /**
     * add aliases
     *
     * @return void
     */
    protected function addAliases()
    {
        $this->addAlias("GoogleMapTools", GoogleMapToolsServiceProvider::class);
    }
}
