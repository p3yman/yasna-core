<?php

namespace Modules\GoogleMap\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\GoogleMap\Services\GoogleMap;

class GoogleMapToolsServiceProvider extends ServiceProvider
{
    protected static $maps = [];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function registerMap($map)
    {
        if ($map instanceof GoogleMap) {
            self::$maps[] = $map;
            return true;
        }

        return false;
    }

    public static function getRegisteredMaps()
    {
        return self::$maps;
    }

    public static function getScripts($additionalScripts = '')
    {
        return view('googlemap::layouts.scripts', compact('additionalScripts'));
    }
}
