<?php
return [
     "403" => "The action is unauthorized.",
     "404" => "The requested resource could not be found but may be available in the future.",
     "405" => "The request method is not supported for the requested resource.",
     "422" => "The request was well-formed but was unable to be followed due to semantic errors.",
     "501" => "The server either does not recognize the request method, or it lacks the ability to fulfil the request.",
     "507" => "the server is unable to store the representation needed to successfully complete the request.",
];
