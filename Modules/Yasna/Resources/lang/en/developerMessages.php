<?php
return [
     "403" => "Forbidden Access",
     "404" => "Not Found!",
     "405" => "Method Not Allowed",
     "422" => "Unprocessable Entity",
     "501" => "Not Implemented",
     "507" => "Insufficient Storage",
];
