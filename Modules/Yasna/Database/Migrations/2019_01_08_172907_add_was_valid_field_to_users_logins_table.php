<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWasValidFieldToUsersLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_logins', function (Blueprint $table) {
            $table->tinyInteger("was_valid")->default(0)->after("user_ip");
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_logins', function (Blueprint $table) {
            $table->dropColumn("was_valid");
        });
    }
}
