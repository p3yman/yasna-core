<?php

namespace Modules\Yasna\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class StatesSlugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ($this->columnDoesNotExist()) {
            return;
        }

        $this->updateRows();
    }



    /**
     * Checks if the `slug` column exists.
     *
     * @return bool
     */
    protected function columnExists()
    {
        return Schema::hasColumn('states', 'slug');
    }



    /**
     * Checks if the `slug` column does not exists.
     *
     * @return bool
     */
    protected function columnDoesNotExist()
    {
        return !$this->columnExists();
    }



    /**
     * Update rows in the `states` table.
     */
    protected function updateRows()
    {
        $data = $this->data();

        foreach ($data as $datum) {
            $model = model('state')->grabId($datum['id']);

            if ($model->slug) {
                continue;
            }

            $model->batchSave(['slug' => $datum['slug']]);
        }
    }



    /**
     * Returns the data to be updated in the `states` table.
     *
     * @return array
     */
    protected function data()
    {
        return [
             [
                  "id"   => "1",
                  "slug" => "azerbaijan-east",
             ],
             [
                  "id"   => "2",
                  "slug" => "azerbaijan-west",
             ],
             [
                  "id"   => "3",
                  "slug" => "ardabil",
             ],
             [
                  "id"   => "4",
                  "slug" => "isfahan",
             ],
             [
                  "id"   => "5",
                  "slug" => "alborz",
             ],
             [
                  "id"   => "6",
                  "slug" => "ilam",
             ],
             [
                  "id"   => "7",
                  "slug" => "bushehr",
             ],
             [
                  "id"   => "8",
                  "slug" => "tehran",
             ],
             [
                  "id"   => "9",
                  "slug" => "chaharmahal-and-bakhtiari",
             ],
             [
                  "id"   => "10",
                  "slug" => "khorasan-south",
             ],
             [
                  "id"   => "11",
                  "slug" => "khorasan-razavi",
             ],
             [
                  "id"   => "12",
                  "slug" => "khorasan-north",
             ],
             [
                  "id"   => "13",
                  "slug" => "khouzestan",
             ],
             [
                  "id"   => "14",
                  "slug" => "zanjan",
             ],
             [
                  "id"   => "15",
                  "slug" => "semnan",
             ],
             [
                  "id"   => "16",
                  "slug" => "sistan-baluchestan",
             ],
             [
                  "id"   => "17",
                  "slug" => "fars",
             ],
             [
                  "id"   => "18",
                  "slug" => "qazvin",
             ],
             [
                  "id"   => "19",
                  "slug" => "qom",
             ],
             [
                  "id"   => "20",
                  "slug" => "kurdistan",
             ],
             [
                  "id"   => "21",
                  "slug" => "kerman",
             ],
             [
                  "id"   => "22",
                  "slug" => "kermanshah",
             ],
             [
                  "id"   => "23",
                  "slug" => "kohgiluyeh-boyer-ahmad",
             ],
             [
                  "id"   => "24",
                  "slug" => "golestan",
             ],
             [
                  "id"   => "25",
                  "slug" => "gilan",
             ],
             [
                  "id"   => "26",
                  "slug" => "lorestan",
             ],
             [
                  "id"   => "27",
                  "slug" => "mazandaran",
             ],
             [
                  "id"   => "28",
                  "slug" => "markazi",
             ],
             [
                  "id"   => "29",
                  "slug" => "hormozgan",
             ],
             [
                  "id"   => "30",
                  "slug" => "hamadan",
             ],
             [
                  "id"   => "31",
                  "slug" => "yazd",
             ],
        ];
    }
}
