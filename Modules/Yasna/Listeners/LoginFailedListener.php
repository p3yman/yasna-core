<?php

namespace Modules\Yasna\Listeners;

use Modules\Yasna\Events\LoginFailed;

//this is not queue because Request can't be serialize
class LoginFailedListener
{
    public $tries = 5;
    /**
     * @var LoginFailed
     */
    private $event;



    /**
     * log failed attempt
     *
     * @param LoginFailed $event
     *
     * @return bool|\Modules\Yasna\Services\YasnaModel
     */
    public function handle(LoginFailed $event)
    {
        $this->event = $event;

        return $this->addLog();
    }



    /**
     * add log of user
     *
     * @return bool|\Modules\Yasna\Services\YasnaModel
     */
    private function addLog()
    {
        if (!get_setting('user_login_log')) {
            return false;
        }

        $user = user(-1);
        if (($username = $this->event->request->get(user()->usernameField())) == null) {
            $username = $this->event->request->get("username");
            $user     = user()->findByUsername($username);
        }


        return model('UsersLogin')->batchSave([
             "user_id"   => $user->id,
             'username'  => $username,
             'user_ip'   => $this->event->request->ip(),
             "was_valid" => 0,
        ]);
    }
}

