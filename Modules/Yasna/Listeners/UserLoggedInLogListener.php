<?php

namespace Modules\Yasna\Listeners;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Yasna\Events\UserLoggedIn;

//this is not queue because Request can't be serialize
class UserLoggedInLogListener
{
    public $tries = 5;
    /**
     * @var User $user
     */
    private $user;
    private $ip;



    /**
     * make log of login
     *
     * @param UserLoggedIn $event
     */
    public function handle(UserLoggedIn $event)
    {
        $this->user = $event->model;
        $this->ip   = $event->request->ip();
        $this->addLog();
    }



    /**
     * add log of user login
     *
     * @return bool|\Modules\Yasna\Services\YasnaModel
     */
    protected function addLog()
    {
        if (!get_setting('user_login_log')) {
            return false;
        }

        $roles = get_setting('login_log_roles');


        if (!empty($roles)) {
            if (!$this->user->isAnyOf($roles)) {
                return false;
            }
        }

        return model('UsersLogin')->batchSave([
             'user_id'   => $this->user->id,
             'username'  => $this->user->username,
             'user_ip'   => $this->ip,
             "was_valid" => 1,
        ]);
    }
}

