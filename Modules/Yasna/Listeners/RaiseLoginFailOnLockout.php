<?php

namespace Modules\Yasna\Listeners;

use Illuminate\Auth\Events\Lockout;
use Modules\Yasna\Events\LoginFailed;

class RaiseLoginFailOnLockout
{

    /**
     * handle the listener
     *
     * @param Lockout $event
     */
    public function handle(Lockout $event)
    {
        event( new LoginFailed());
    }
}
