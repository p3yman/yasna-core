<?php

namespace Modules\Yasna\Exceptions;

use Exception;

class UndefinedHookAttributeException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     */
    public function __construct(string $hook_name, string $attribute)
    {
        parent::__construct("Undefined attribute `$attribute` in the hook `$hook_name`.");
    }
}
