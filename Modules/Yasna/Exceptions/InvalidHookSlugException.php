<?php

namespace Modules\Yasna\Exceptions;

use Exception;

/**
 * Class InvalidHookSlugException
 * is thrown when someone tries to use an invalid hook slug
 *
 * @package Modules\Yasna\Exceptions
 */
class InvalidHookSlugException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct("`$name` is not a valid hook slug.");
    }
}
