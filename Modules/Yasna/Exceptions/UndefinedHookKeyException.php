<?php

namespace Modules\Yasna\Exceptions;

use Exception;

/**
 * Class UndefinedHookKeyException
 * is thrown when someone tries to alter an undefined hook key
 *
 * @package Modules\Yasna\Exceptions
 */
class UndefinedHookKeyException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     * @param string $key
     */
    public function __construct(string $name, string $key)
    {
        parent::__construct("The hook `$name` has not a key `$key`.");
    }
}
