<?php

namespace Modules\Yasna\Exceptions;

use Exception;

/**
 * Class UnregisteredHookException
 * is thrown when someone tries to use unregistered hooks
 *
 * @package Modules\Yasna\Exceptions
 */
class UnregisteredHookException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct("The hook `$name` is not a registered.");
    }
}
