<?php

namespace Modules\Yasna\Services\GeneralTraits;

/**
 * add class recognition capabilities to the serializable classes
 *
 * @TODO: add Wiki link
 */
trait ClassRecognitionsTrait
{
    /**
     * get the called class name, fully qualified
     *
     * @return string
     */
    public static function calledClass()
    {
        return get_called_class();
    }



    /**
     * get the called class name, without namespace or anything extra
     *
     * @return string
     */
    public static function calledClassName()
    {
        return array_last(explode("\\", static::calledClass()));
    }



    /**
     * get the reflector object of the called class
     *
     * @return \ReflectionClass
     */
    public static function reflector()
    {
        return (new \ReflectionClass(static::class));
    }



    /**
     * check if the class has the given method
     *
     * @param string $method_name
     *
     * @return bool
     */
    protected static function hasMethod(string $method_name): bool
    {
        return method_exists(static::class, $method_name);
    }



    /**
     * check if the class has not the given method
     *
     * @param string $method_name
     *
     * @return bool
     */
    protected static function hasNotMethod(string $method_name): bool
    {
        return !static::hasMethod($method_name);
    }
}
