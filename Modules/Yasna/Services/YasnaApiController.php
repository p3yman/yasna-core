<?php

namespace Modules\Yasna\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Routing\Controller;
use Modules\Yasna\Services\GeneralTraits\ClassRecognitionsTrait;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Modules\Yasna\Services\ResourceMapper\ListMapper;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

abstract class YasnaApiController extends Controller
{
    use ModuleRecognitionsTrait;
    use ClassRecognitionsTrait;

    /**
     * keep the model name of this controller
     *
     * @var string
     */
    protected $model_name;



    /**
     * dispatch a 200 http status with the given white-house-compatible set of data
     *
     * @param array $results
     * @param array $metadata
     *
     * @return array
     */
    protected function success($results = [], $metadata = [])
    {
        return api()->inModule($this->runningModuleName())->successArray($results, $metadata);
    }



    /**
     * dispatch a 500 status with the given white-house-compatible set of error messages
     *
     * @param int $error_code
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function serverError($error_code)
    {
        return api()->inModule($this->runningModuleName())->serverErrorRespond($error_code);
    }



    /**
     * dispatch a 400 status with the given white-house-compatible set of error messages
     *
     * @param $error_code
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function clientError($error_code)
    {
        return api()->inModule($this->runningModuleName())->clientErrorRespond($error_code);
    }



    /**
     * dispatch a model save feedback, conditionally based on the passed model
     *
     * @param YasnaModel|mixed $saved_model
     * @param array            $meta
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function modelSaveFeedback($saved_model, $meta = [])
    {
        return $this->typicalSaveFeedback(
             $saved_model->exists,
             array_merge($meta, ["hashid" => $saved_model->hashid])
        );
    }



    /**
     * dispatch a typical save feedback, conditionally based on the passed model
     *
     * @param YasnaModel|mixed $saved_model
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function typicalSaveFeedback(bool $is_saved, array $metadata = [])
    {
        if (!$is_saved) {
            return $this->saveFailureError();
        }

        return $this->success(["done" => "1",], $metadata);
    }



    /**
     * dispatch a suitable save failure error
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function saveFailureError()
    {
        return $this->serverError(507);
    }



    /**
     * find the model by id/hashid/slug and return the instance
     *
     * @param string|int $identifier
     * @param bool       $with_trashed
     *
     * @return YasnaModel|mixed
     */
    protected function findModel($identifier, bool $with_trashed = false)
    {
        if (!$this->model_name) {
            return model($this->model_name);
        }

        $model = model($this->model_name, $identifier, $with_trashed);

        return $model->spreadMeta();
    }



    /**
     * find the model by id/hashid/slug and return the instance
     *
     * @param string|int $identifier
     * @param bool       $with_trashed
     *
     * @return YasnaModel|mixed
     */
    protected function findOrFailModel($identifier, bool $with_trashed = false)
    {
        $model = $this->findModel($identifier, $with_trashed);

        if ($model->isNotSet()) {
            dd("ERROR");
            // TODO: A standard whitehouse-compatible error instance is to be thrown.
        }

        return $model;
    }



    /**
     * Return a ready-to-use metadata array, extracted from a length-aware paginator object.
     *
     * @deprecated since 97/09/03
     *
     * @param LengthAwarePaginator $paginator
     *
     * @return array
     */
    protected function paginatorMetadataGenerator(LengthAwarePaginator $paginator): array
    {
        return $this->paginatorMetadata($paginator);
    }



    /**
     * Return a ready-to-use metadata array, extracted from a length-aware paginator object.
     *
     * @param LengthAwarePaginator $paginator
     *
     * @return array
     */
    protected function paginatorMetadata(LengthAwarePaginator $paginator): array
    {
        $paginator->appends(request()->all());

        return [
             'total'             => $paginator->total(),
             'count'             => $paginator->count(),
             'per_page'          => $paginator->perPage(),
             'last_page'         => $paginator->lastPage(),
             'current_page'      => $paginator->currentPage(),
             'next_page_url'     => $paginator->nextPageUrl(),
             'previous_page_url' => $paginator->previousPageUrl(),
        ];
    }



    /**
     * Maps the builder to the resources array.
     * <br>
     * _The information about the pagination of the result will be guessed based on the given request._
     *
     * @param Builder               $builder
     * @param YasnaListRequest|null $request
     *
     * @return array
     */
    protected function getResourcesFromBuilder($builder, $request = null)
    {
        return (new ListMapper($builder, $request))->toArray();
    }
}
