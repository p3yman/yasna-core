<?php

namespace Modules\Yasna\Services\V4\Request;

trait RequestRuleInjectionTrait
{
    /** @var array */
    private $injected_rules = [];



    /**
     * inject validation rule
     *
     * @param string $attribute
     * @param string $rule
     *
     * @return void
     */
    protected function injectValidationRule(string $attribute, string $rule)
    {
        $this->injected_rules[$attribute][] = $rule;
    }



    /**
     * add injected validation rules (called in the `rules()` method).
     *
     * @return array
     */
    private function injectedRules()
    {
        $array = [];
        foreach ($this->injected_rules as $attribute => $rules) {
            $array[$attribute] = implode("|", $rules);
            //TODO: Rule merger needs to be refactored to support arrayed rules.
        }

        return $array;
    }
}
