<?php

namespace Modules\Yasna\Services\V4\Request;

trait RequestOutputTrait
{
    /**
     * Safely converts the submitted data into a key-value array
     *
     * @return array
     */
    public function toArray(): array
    {
        $this->mutators();

        return $this->data;
    }



    /**
     * gets the array of all not-going-to-be-escaped fields
     *
     * @return array
     */
    public function getMainFieldsArray(): array
    {
        $array  = $this->toArray();
        $result = [];

        foreach ($array as $key => $value) {
            if (substr($key, 0, 1) != '_') {
                $result[$key] = $value;
            }
        }

        return $result;
    }



    /**
     * corrects the data as appropriate, just before conversion to array (which is normally executed in batchSave)
     */
    protected function mutators()
    {
        // override this method to define your corrections.
    }

}
