<?php

namespace Modules\Yasna\Services\V4\Request;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;

trait RequestModelTrait
{
    /**
     * keep the field name, the user has tried to load model with.
     *
     * @var string
     */
    private $tried_model_handle;



    /**
     * check if the request contains tries to fetch some model instance
     *
     * @return bool
     */
    public function modelRequested(): bool
    {
        foreach ($this->allowedModelHandles() as $attribute) {
            if ($this->isset($attribute)) {
                return true;
            }
        }

        return false;
    }



    /**
     * check if the request is in edit mode
     *
     * @return bool
     */
    public function editMode(): bool
    {
        return $this->modelRequested();
    }



    /**
     * check if the request is in creation mode
     *
     * @return bool
     */
    public function createMode(): bool
    {
        return !$this->editMode();
    }



    /**
     * manually set the model instance
     *
     * @param YasnaModel $model
     */
    public function setModel($model)
    {
        $this->model        = $model;
        $this->model_hashid = $model->hashid;
        $this->model_id     = $model->id;
        $this->model_name   = $model->getClassName();
    }



    /**
     * automatically load model
     *
     * @return void
     */
    protected function loadModel()
    {
        if (!$this->shouldLoadModel()) {
            return;
        }

        $this->model = $this->getModelInstance();

        $this->fillModelHandleProperties();
        $this->failIfModelNotFound();
    }



    /**
     * fill model instance for more safety
     */
    protected function fillModelForMoreSafety()
    {
        if ($this->model_name) {
            $this->model = model($this->model_name);
        } else {
            $this->model = model("setting");
        }

    }



    /**
     * get model instance (override this, if you need some special queries to take place)
     *
     * @return YasnaModel
     */
    protected function getModelInstance()
    {
        $this->unsetZeroModelHandles();

        /** @var YasnaModel|Builder $instance */
        $instance = model($this->model_name);

        if ($this->should_load_trashed_models) {
            $instance = $instance->withTrashed();
        }
        if ($this->should_load_trashed_models_only) {
            $instance = $instance->onlyTrashed();
        }

        /*-----------------------------------------------
        | If id is allowed and given...
        */
        if ($this->should_load_model_with_id and $this->isset('id')) {
            $this->tried_model_handle = "id";
            return $instance->grabId($this->getData('id'));
        }

        /*-----------------------------------------------
        | If hashid is allowed and given...
        */
        if ($this->should_load_model_with_hashid) {
            if ($this->isset('id')) {
                $this->tried_model_handle = "id";
                return $instance->grabHashid($this->getData('id'));
            }
            if ($this->isset('hashid')) {
                $this->tried_model_handle = "hashid";
                return $instance->grabHashid($this->getData('hashid'));
            }
        }

        /*-----------------------------------------------
        | If slug is allowed and given ...
        */
        if ($this->should_load_model_with_slug and $this->isset($this->model_slug_attribute)) {
            $this->tried_model_handle = "slug";
            if ($this->model_slug_column == "slug") {
                return $instance->grabSlug($this->getData($this->model_slug_attribute));
            } else {
                return $instance
                     ->where($this->model_slug_column, $this->getData($this->model_slug_attribute))
                     ->firstOrNew([])
                     ;
            }
        }

        /*-----------------------------------------------
        | Otherwise ...
        */
        return $instance;
    }



    /**
     * check if the class should automatically load the model instance
     *
     * @return bool
     */
    protected function shouldLoadModel()
    {
        return $this->should_load_model and boolval($this->model_name);
    }



    /**
     * get the attributes which are allowed to load a model, regardless of the result of `shouldLoadModel()` method.
     *
     * @return array
     */
    protected function allowedModelHandles(): array
    {
        $result = [];

        if ($this->should_load_model_with_id) {
            $result[] = "id";
        }
        if ($this->should_load_model_with_hashid) {
            $result[] = "id";
            $result[] = "hashid";
        }
        if ($this->should_load_model_with_slug) {
            $result[] = $this->model_slug_attribute;
        }

        return array_unique($result);
    }



    /**
     * fill model handle properties, to make it easier to use in the Controller layer
     */
    protected function fillModelHandleProperties()
    {
        if (!$this->model) {
            return;
        }

        $this->model_hashid = $this->model->hashid;
        $this->model_id     = $this->model->id;
    }



    /**
     * fail if the model is not found, after an unsuccessful attempt TODO: Test in web!
     *
     * @throws ValidationException
     * @return void
     */
    protected function failIfModelNotFound()
    {
        if ($this->model->exists) {
            return;
        }
        if ($this->should_allow_create_mode and $this->createMode()) {
            return;
        }

        $content  = [
             "status"           => "400",
             "developerMessage" => "Content not found!",
             "userMessage"      => "Content not found!",
             "errorCode"        => "404",
             "moreInfo"         => "",
             "errors"           => [],
        ];
        $response = new JsonResponse($content, 400);
        throw (new ValidationException($this->getValidatorInstance(), $response));

    }



    /**
     * unset zero model handles (id and hashid only)
     */
    private function unsetZeroModelHandles()
    {
        if ($this->should_load_model_with_hashid and $this->isset('hashid') and $this->getData('hashid') == hashid(0)) {
            $this->unsetData('hashid');
        }

        if ($this->should_load_model_with_id and $this->isset('id')) {
            if (in_array($this->getData('id'), [0, hashid(0)])) {
                $this->unsetData('id');
            }
        }
    }
}
