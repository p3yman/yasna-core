<?php

namespace Modules\Yasna\Services\V4\Request;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Modules\Yasna\Services\YasnaModel;
use Modules\Yasna\Providers\ValidationServiceProvider;

/**
 * Class YasnaRequest
 *
 * @link    https://github.com/mhrezaei/yasna-core/wiki/D.-YasnaRequest
 */
class YasnaFormRequest extends FormRequest
{
    use RequestDataTrait;
    use RequestOutputTrait;
    use RequestGuardTrait;
    use RequestCustomFailsTrait;
    use RequestModelTrait;
    use RequestRuleInjectionTrait;
    use ModuleRecognitionsTrait;

    /**
     * keep the model instance
     *
     * @var YasnaModel
     */
    public $model = null;

    /**
     * Keeps the automatic-retrieved model id.
     * This is automatically filled and you don't need to override.
     * Made public to be available in the Controller level as well.
     *
     * @var int|null
     */
    public $model_id = 0;

    /**
     * Keeps the automatic-retrieved model hashid.
     * This is automatically filled and you don't need to override.
     * Made public to be available in the Controller level as well.
     *
     * @var string|null
     */
    public $model_hashid = null;

    /**
     * keep the model name
     *
     * @var string
     */
    protected $model_name;

    /**
     * Indicates which responder should be used to convey error messages in case of validation or authentication
     * failure.
     * `default` will let Laravel handle the job, and any other thing will use the corresponding standard.
     * "white-house" is the only custom standard, available for the time being.
     *
     * @link https://github.com/mhrezaei/yasna-core/wiki/C.-API-Responder
     * @var string
     */
    protected $responder = 'white-house';

    /**
     * determine if the class should automatically try to load the model instance
     *
     * @var bool
     */
    protected $should_load_model = true;

    /**
     * determine if the class is allowed to load the model with plain `id` field
     *
     * @var bool
     */
    protected $should_load_model_with_id = false;


    /**
     * determine if the class is allowed to load the model with encrypted `id` field (hashid)
     *
     * @var bool
     */
    protected $should_load_model_with_hashid = true;

    /**
     * determine of the class is allowed to load the model with plain `slug` field
     *
     * @var bool
     */
    protected $should_load_model_with_slug = false;

    /**
     * determine the name of the slug attribute to be used for loading with `slug`
     *
     * @var string
     */
    protected $model_slug_attribute = "slug";

    /**
     * determine the name of the slug column (in the database) to be used for loading with `slug`
     *
     * @var string
     */
    protected $model_slug_column = "slug";

    /**
     * determine if the class is allowed to load trashed models
     *
     * @var bool
     */
    protected $should_load_trashed_models = false;

    /**
     * determine if the class is ONLY allowed to load trashed models
     *
     * @var bool
     */
    protected $should_load_trashed_models_only = false;

    /**
     * determine if the class should consider absence of id as a signal of create mode
     *
     * @var bool
     */
    protected $should_allow_create_mode = false;

    /**
     * determine if the injection guard must be automatically executed
     *
     * @var bool
     */
    protected $automatic_injection_guard = true;

    /**
     * Indicate which error code should be conveyed, when the validation fails.
     * This is api-dependant and works only on white-house responder.
     *
     * @var int
     */
    protected $validation_error_code = 422;

    /**
     * Indicate which error code should be conveyed, when the authorization fails.
     * This is api-dependant and works only on white-house responder.
     *
     * @var int
     */
    protected $authorization_error_code = 403;


    /**
     * Purification rules to be applied automatically on all received data
     * Override this, if you feel necessary.
     *
     * @var string
     */
    protected $automatic_purification_rules = 'trim | stripArabic';

    /**
     * keep the submitted data
     *
     * @var array
     */
    private $data = [];



    /**
     * check if the online user is authorized to submit the request
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * call any "folanRules" method and merge their results, to form a central rules array
     *
     * @return array
     */
    public function rules()
    {
        $methods     = get_class_methods($this);
        $rules_array = [];
        $exceptions  = ["mergeRules", "rules"];

        foreach ($methods as $method) {
            if (str_contains($method, "Rules") and !in_array($method, $exceptions)) {
                $rules_array = $this->mergeRules($rules_array, $this->$method());
            }
        }

        return $rules_array;
    }



    /**
     * @inheritdoc
     */
    public function all($keys = null)
    {
        return $this->data;
    }



    /**
     * get an array of the data purification rules
     *
     * @return array
     */
    public function purifier()
    {
        return [];
    }



    /**
     * run the corrections that has to be applied before the validation process
     *
     * @return void
     */
    public function corrections()
    {
        // override this method to define your corrections.
    }



    /**
     * run the corrections that has to be applied before the purification take place
     */
    public function correctionsBeforePurifier()
    {
        // Override this method if you need corrections to be executed before the purifier.
    }



    /**
     * run the corrections that has to be applied before loading the model
     */
    public function correctionsBeforeLoadingModel()
    {
        // Override this method if you need corrections to be executed before loading the model.
    }



    /**
     * @inheritdoc
     */
    protected function passesAuthorization()
    {
        $this->initRequestSequence();

        return parent::passesAuthorization();
    }



    /**
     * safely merges incoming array into the existing one and returns the enriched existing array.
     *
     * @param array $existing
     * @param array $incoming
     *
     * @return array
     */
    protected function mergeRules(array $existing, $incoming): array
    {
        if (!is_array($incoming)) {
            return $existing;
        }

        foreach ($incoming as $key => $item) {
            if (array_has($existing, $key)) {
                $existing[$key] .= "|$item";
            } else {
                $existing[$key] = $item;
            }
        }

        foreach ($existing as $key => $item) {
            $array          = array_unique(explode_not_empty("|", $item));
            $existing[$key] = implode("|", $array);
        }

        return $existing;
    }



    /**
     * init the request sequence by loading necessary data and models, before first steps of validation.
     *
     * @return void
     */
    private function initRequestSequence()
    {
        $this->data = parent::all();
        $this->fillModelForMoreSafety();

        if ($this->automatic_injection_guard) {
            $this->injectionGuard();
        }

        $this->correctionsBeforeLoadingModel();
        $this->loadModel();
        $this->correctionsBeforePurifier();
        $this->purifierRun();
        $this->corrections();
    }



    /**
     * run the purifier system.
     *
     * @return void
     */
    private function purifierRun()
    {
        $this->data = ValidationServiceProvider::purifier(
             $this->data,
             $this->purifier(),
             $this->automatic_purification_rules
        );
    }
}
