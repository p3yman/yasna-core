<?php

namespace Modules\Yasna\Services\V4\Request;

/**
 * @property bool   $paginated
 * @property int    $per_page
 * @property int    $page
 * @property string $sort
 * @property string $order
 * @property array  $including
 * @property array  $excluding
 * @link    https://github.com/mhrezaei/yasna-core/wiki/D.-YasnaRequest
 */
class YasnaListRequest extends YasnaFormRequest
{
    use RequestPaginationTrait;

    /**
     * @inheritdoc
     */
    protected $should_load_model = false;

    /**
     * @inheritdoc
     */
    protected $should_load_model_with_hashid = false;

    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "paginated" => "boolean",
             "per_page"  => "integer",
             "order"     => "in:desc,asc,rand",
             "sort"      => "string",
             "count"     => "boolean",
        ];
    }
}
