<?php

namespace Modules\Yasna\Services\V4\Request;

trait RequestPaginationTrait
{
    /**
     * determine the maximum items allowed per page
     *
     * @var int
     */
    protected $max_allowed_per_page = 100;



    /**
     * check if the response is supposed to be paginated.
     *
     * @return bool
     */
    public function isPaginated(): bool
    {
        $paginated = $this->getData('paginated', 0);

        return (bool)$paginated;
    }



    /**
     * check if the response is NOT supposed to be paginated.
     *
     * @return bool
     */
    public function isNotPaginated(): bool
    {
        return !$this->isPaginated();
    }



    /**
     * Set a per_page value from a request class for setting a number of results on an paginated manner.
     *
     * @param int $default
     *
     * @return int
     */
    public function perPage(int $default = 15): int
    {
        return min((int)$this->getData('per_page', $default), $this->max_allowed_per_page);
    }



    /**
     * get the number of the given page on a paginated manner.
     *
     * @param int $page
     *
     * @return int
     */
    public function page(int $page = 1): int
    {
        return (int)$this->getData('page', $page);
    }

}
