<?php

namespace Modules\Yasna\Services\V4\Request;

use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Validation\ValidationException;

trait RequestCustomFailsTrait
{

    /**
     * get an array of errors, just like how Laravel does in the background.
     *
     * @param Validator $validator
     *
     * @return array
     */
    public function getErrors(Validator $validator)
    {
        $array  = [];
        $errors = $validator->errors();

        foreach ($errors->keys() as $key) {
            $array[$key] = [$errors->get($key)];
        }

        return $array;
    }



    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        if ($this->responder == "white-house") {
            $this->customFailedAuthorization();
            die();
        }

        parent::failedAuthorization();
    }



    /**
     * handle a failed validation attempt.
     *
     * @param Validator $validator
     *
     * @throws ValidationException
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->responder == "white-house") {
            $this->customFailedValidation($validator);
            die();
        }


        parent::failedValidation($validator);
    }



    /**
     * implement WhiteHouse standard system, regardless of what has provided in $this->responder
     *
     * @throws ValidationException
     * @return void
     */
    private function customFailedAuthorization()
    {
        $content  = $this->jsonContentForFailedAuthorization();
        $response = new JsonResponse($content, 400);

        throw (new ValidationException($this->getValidatorInstance(), $response));
    }



    /**
     * implement WhiteHouse standard system, regardless of what has provided in $this->responder
     *
     * @param Validator $validator
     *
     * @throws ValidationException
     * @return void
     */
    private function customFailedValidation(Validator $validator)
    {
        $content  = $this->jsonContentForFailedValidation($validator);
        $response = new JsonResponse($content, 400);

        throw (new ValidationException($validator, $response));
    }



    /**
     * prepare array content for failed authorization
     *
     * @return array
     */
    private function jsonContentForFailedAuthorization()
    {
        return api()
             ->inModule($this->runningModuleName())
             ->errorArray($this->authorization_error_code)
             ;
    }



    /**
     * prepare array content for failed validation
     *
     * @param Validator $validator
     *
     * @return array
     */
    private function jsonContentForFailedValidation(Validator $validator)
    {
        return api()
             ->inModule($this->runningModuleName())
             ->withErrors($this->getErrors($validator))
             ->errorArray($this->validation_error_code)
             ;
    }
}
