<?php

namespace Modules\Yasna\Services\V4\Request;

use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;

trait RequestGuardTrait
{
    /**
     * get an array of fillable fields, which are the only ones allowed to appear in the request submit.
     * the ones started with an underscore (ex. _label, _token) are always allowed
     *
     * @return array
     */
    protected function fillableFields()
    {
        return [];
    }



    /**
     * get an array of guarded fields, which are not supposed to appear in the request submit.
     *
     * @return array
     */
    protected function guardedFields()
    {
        return [];
    }



    /**
     * run the guard against injecting unplanned data in the form submit.
     *
     * @return void
     */
    protected function injectionGuard()
    {
        $this->preventGuardedFields();
        $this->preventUnfillableFields();
    }



    /**
     * prevent guarded fields from being submitted among the request.
     *
     * @throws ValidationException
     * @return void
     */
    private function preventGuardedFields()
    {
        $supplied_fields = array_keys($this->data);
        $guarded_fields  = $this->guardedFields();

        if (!count($guarded_fields)) {
            return;
        }

        foreach ($supplied_fields as $field) {
            if (in_array($field, $guarded_fields)) {
                $this->failedInjectionGuard($field);
            }
        }
    }



    /**
     * prevent fields not mentioned in $this->fillableFields from being submitted among the request.
     *
     * @throws ValidationException
     * @return void
     */
    private function preventUnfillableFields()
    {
        $supplied_fields = array_keys($this->data);
        $fillable_fields = $this->fillableFields();

        if ($fillable_fields == ['*']) {
            return;
        }
        if(!count($fillable_fields) and count($this->guardedFields())) {
            return;
        }

        foreach ($supplied_fields as $field) {

            if ($field[0] == '_' or $field == 'id' or $field == 'hashid') {
                continue;
            }

            if (!in_array($field, $fillable_fields)) {
                $this->failedInjectionGuard($field);
            }
        }
    }



    /**
     * fail on injection guard problems with custom behavior when the app is in debug mode
     *
     * @param string $trouble_maker_field
     *
     * @throws ValidationException
     * @return void
     */
    private function failedInjectionGuard(string $trouble_maker_field)
    {
        if (debugMode()) {
            $content  = [
                 "status"           => "400",
                 "developerMessage" => "Detected forbidden field `$trouble_maker_field`",
                 "userMessage"      => "Forbidden Access!",
                 "errorCode"        => "403",
                 "moreInfo"         => "You are seeing this friendly message because the app is in debug mode.",
                 "errors"           => [],
            ];
            $response = new JsonResponse($content, 400);

            throw (new ValidationException($this->getValidatorInstance(), $response));
        }

        $this->failedAuthorization();
    }

}
