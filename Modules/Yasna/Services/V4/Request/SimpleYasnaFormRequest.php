<?php

namespace Modules\Yasna\Services\V4\Request;

class SimpleYasnaFormRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;
}
