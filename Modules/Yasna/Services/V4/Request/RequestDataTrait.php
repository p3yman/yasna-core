<?php

namespace Modules\Yasna\Services\V4\Request;

trait RequestDataTrait
{
    /**
     * check if the given attribute has been set in the request.
     *
     * @param string $attribute
     *
     * @return bool
     */
    public function isset(string $attribute)
    {
        return isset($this->data[$attribute]);
    }



    /**
     * manually set data to the request class
     *
     * @param array $data
     *
     * @return void
     */
    public function setDataArray(array $data)
    {
        $this->data = $data;
    }



    /**
     * get the data array in raw
     *
     * @return array
     */
    public function getDataArray()
    {
        return $this->data;
    }



    /**
     * safely get data from the $this->data[] array
     *
     * @param string $attribute
     * @param mixed  $default
     *
     * @return mixed
     */
    protected function getData(string $attribute, $default = null)
    {
        return $this->isset($attribute) ? $this->data[$attribute] : $default;
    }



    /**
     * check if the data set with the given attribute equals to the given reference value
     *
     * @param string $attribute
     * @param mixed  $value
     * @param mixed  $default
     *
     * @return bool
     */
    protected function checkData(string $attribute, $value, $default = null)
    {
        return $this->getData($attribute, $default) == $value;
    }



    /**
     * safely get data from the $this->data[] array and unset the value
     *
     * @param string $attribute
     * @param null   $default
     *
     * @return mixed
     */
    protected function pullData(string $attribute, $default = null)
    {
        $return = $this->getData($attribute, $default);
        $this->unsetData($attribute);

        return $return;
    }



    /**
     * rename attribute in $this->data array.
     *
     * @param string $old_key
     * @param string $new_key
     */
    protected function renameData(string $old_key, string $new_key)
    {
        $this->setData($new_key, $this->pullData($old_key));
    }



    /**
     * put data into the $this->data[] array
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return void
     */
    protected function setData(string $attribute, $value)
    {
        $this->data[$attribute] = $value;
    }



    /**
     * unset the given attribute from $this->data[] array
     *
     * @param string|array $attribute
     * @param mixed        $default
     *
     * @return mixed
     */
    protected function unsetData($attribute, $default = null)
    {
        if (is_array($attribute)) {
            array_forget($this->data, $attribute);
            return true;
        }

        $value = $this->getData($attribute, $default);
        unset($this->data[$attribute]);

        return $value;
    }
}
