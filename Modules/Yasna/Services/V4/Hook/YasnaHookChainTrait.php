<?php

namespace Modules\Yasna\Services\V4\Hook;

use Modules\Yasna\Exceptions\UndefinedHookAttributeException;
use Modules\Yasna\Exceptions\UndefinedHookKeyException;
use Modules\Yasna\Exceptions\UnregisteredHookException;

trait YasnaHookChainTrait
{
    /**
     * magic method for general setters
     *
     * @param string $field
     * @param array  $value
     *
     * @return $this
     */
    public function __call($name, $arguments)
    {
        return $this->set($name, $arguments[0]);
    }



    /**
     * set an attribute to the current hook key
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return $this
     */
    public function set(string $attribute, $value)
    {
        $this->throwExceptionsIfNecessary($attribute);

        $value = $this->filterTransStrings($value);

        static::$hooks[$this->slug][$this->current_key][$attribute] = $value;

        return $this;
    }



    /**
     * check if a given attribute is valid to be used on a hook
     *
     * @param string $attribute
     *
     * @return bool
     */
    private function hasAttribute(string $attribute): bool
    {
        return in_array($attribute, $this->getValidAttributes());
    }



    /**
     * check if a given attribute is NOT valid to be used on a hook
     *
     * @param string $attribute
     *
     * @return bool
     */
    private function hasNotAttribute(string $attribute): bool
    {
        return !$this->hasAttribute($attribute);
    }



    /**
     * get the list of all valid attributes of the hook
     *
     * @return array
     */
    private function getValidAttributes(): array
    {
        return array_merge($this->getRegisteredAttributes(), $this->getParamountAttributes());
    }



    /**
     * get the attributes registered with the hook
     *
     * @return array
     */
    private function getRegisteredAttributes(): array
    {
        $registered = static::$registered[$this->module_name][$this->hook_name]['attributes'];

        return array_keys($registered);
    }



    /**
     * get the paramount attributes that are always available in all hooks
     *
     * @return array
     */
    private function getParamountAttributes(): array
    {
        return [
             "key",
             "order",
             "condition",
        ];
    }



    /**
     * filter trans strings to set closures instead of direct trans
     * (because trans function does not work fine in provider layer)
     *
     * @param mixed $value
     *
     * @return mixed
     */
    private function filterTransStrings($value)
    {
        if (!is_string($value) or !str_contains($value, "trans:")) {
            return $value;
        }

        $map = str_after($value, "trans:");

        return function () use ($map) {
            return trans($map);
        };
    }



    /**
     * throws errors if necessary
     *
     * @param string $attribute
     *
     * @throws UndefinedHookKeyException
     * @throws UnregisteredHookException
     * @throws UndefinedHookAttributeException
     */
    private function throwExceptionsIfNecessary(string $attribute)
    {
        if ($this->isNotRegistered()) {
            throw new UnregisteredHookException($this->slug);
        }
        if (!$this->current_key or $this->hasNotKey($this->current_key)) {
            throw new UndefinedHookKeyException($this->slug, $this->current_key);
        }
        if ($this->hasNotAttribute($attribute)) {
            throw  new UndefinedHookAttributeException($this->slug, $this->current_key);
        }

    }
}
