<?php

namespace Modules\Yasna\Services\V4\Hook;


use Modules\Yasna\Exceptions\InvalidHookSlugException;
use Modules\Yasna\Services\ModuleHelper;

class YasnaHook
{
    use YasnaHookRegisterTrait;
    use YasnaHookLoadTrait;
    use YasnaHookChainTrait;
    use YasnaHookReadTrait;

    /**
     * keep the static array of the registered hooks
     *
     * @var array
     */
    private static $hooks = [];

    /**
     * keep the full hook slug ("$module_name::$hook_name")
     *
     * @var
     */
    private $slug;

    /**
     * keep the module instance
     *
     * @var ModuleHelper
     */
    private $module;

    /**
     * keep the module name
     *
     * @var string
     */
    private $module_name;

    /**
     * keep the hook name
     *
     * @var string
     */
    private $hook_name;

    /**
     * keep the current key
     *
     * @var string|null
     */
    private $current_key = null;



    /**
     * YasnaHook constructor.
     *
     * @param string $slug
     */
    public function __construct(string $slug)
    {
        $this->slug        = strtolower(snake_case($slug));
        $this->module      = module(str_before($slug, "::"));
        $this->module_name = $this->module->name;
        $this->hook_name   = str_after($slug, "::");

        $this->validateSlug();
    }



    /**
     * get a raw array of all registered hooks
     *
     * @return array
     */
    public static function getRaw()
    {
        return static::$hooks;
    }



    /**
     * throws exception if the module name and hook name could not be discovered from the given slug
     *
     * @throws InvalidHookSlugException
     */
    protected function validateSlug()
    {
        if (!$this->module_name or !$this->hook_name or $this->module->isNotActive()) {
            throw new InvalidHookSlugException($this->slug);
        }
    }

}
