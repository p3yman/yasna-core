<?php

namespace Modules\Yasna\Services\V4\Hook;

trait YasnaHookRegisterTrait
{
    /** @var array */
    private static $registered = [];



    /**
     * get a list of all hooks
     *
     * @return array
     */
    public static function index()
    {
        return static::$registered;
    }



    /**
     * register a hook
     *
     * @param string $comment
     * @param array  $attributes
     * @param bool   $needs_handler
     *
     * @return void
     */
    public function register(string $comment, array $attributes, bool $needs_handler = false)
    {
        if (!isset(static::$registered[$this->module_name])) {
            static::$registered[$this->module_name] = [];
        }

        static::$registered[$this->module_name][$this->hook_name] = [
             "comment"    => $comment,
             "attributes" => $attributes,
        ];

        if ($needs_handler) {
            $this->registerHandler();
        }
    }



    /**
     * check if the hook in question is registered
     *
     * @return bool
     */
    public function isRegistered()
    {
        return isset(static::$registered[$this->module_name][$this->hook_name]);
    }



    /**
     * check if the hook in question is not registered
     *
     * @return bool
     */
    public function isNotRegistered()
    {
        return !$this->isRegistered();
    }



    /**
     * automatically register handler of the current hook.
     *
     * @return void
     */
    private function registerHandler()
    {
        $handler_slug = $this->slug . "Handler";

        hook($handler_slug)->register("the automatically generated handler of $this->slug", [
             "method" => "The public static method responsible to add items.",
             "class"  => "The class, which the method is statically defined in.",
        ]);
    }
}
