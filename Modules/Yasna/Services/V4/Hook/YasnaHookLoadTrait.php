<?php

namespace Modules\Yasna\Services\V4\Hook;

use Modules\Yasna\Exceptions\UndefinedHookKeyException;
use Modules\Yasna\Exceptions\UnregisteredHookException;

trait YasnaHookLoadTrait
{
    /**
     * add a key to the current hook
     *
     * @param string $key
     *
     * @return $this
     * @throws UnregisteredHookException
     */
    public function add(string $key)
    {
        $this->current_key = $key;

        if ($this->isNotRegistered()) {
            throw new UnregisteredHookException($this->slug);
        }

        if ($this->hasNotKey($key)) {
            static::$hooks[$this->slug][$key] = [];
        }

        static::$hooks[$this->slug][$key] = [
             "key"       => $key,
             "order"     => 11,
             "condition" => true,
        ];

        return $this;
    }



    /**
     * alter a key from the current hook
     *
     * @param string $key
     *
     * @return $this
     * @throws UndefinedHookKeyException
     * @throws UnregisteredHookException
     */
    public function alter(string $key)
    {
        $this->current_key = $key;

        if ($this->isNotRegistered()) {
            throw new UnregisteredHookException($this->slug);
        }
        if ($this->hasNotKey($key)) {
            throw new UndefinedHookKeyException($this->slug, $key);
        }

        return $this;
    }



    /**
     * remove a key from the current hook
     *
     * @param string $key
     *
     * @return $this
     */
    public function remove(string $key)
    {
        if ($this->isNotRegistered()) {
            return $this;
        }

        if ($this->hasKey($key)) {
            unset(static::$hooks[$this->slug][$key]);
        }

        return $this;
    }



    /**
     * get all the keys of a registered hook, together with their `order`
     *
     * @return array
     */
    public function keys()
    {
        return $this->getPaired("order");
    }



    /**
     * check if the current hook has the given key
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasKey(string $key)
    {
        return isset(static::$hooks[$this->slug][$key]);
    }



    /**
     * check if the current hook HAS NOT the given key
     *
     * @param string $key
     *
     * @return bool
     */
    public function hasNotKey(string $key)
    {
        return !$this->hasKey($key);
    }
}
