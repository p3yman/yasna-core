<?php

namespace Modules\Yasna\Services\V4\Hook;

use Modules\Yasna\Exceptions\UnregisteredHookException;

trait YasnaHookReadTrait
{
    /**
     * @var bool
     */
    private $bypass_conditions = false;



    /**
     * set the flag to bypass the conditions
     *
     * @return $this
     */
    public function bypassConditions()
    {
        $this->bypass_conditions = true;

        return $this;
    }



    /**
     * find a hook key and get its array
     *
     * @param string $key
     *
     * @return array
     * @throws UnregisteredHookException
     */
    public function find(string $key): array
    {
        if ($this->isNotRegistered()) {
            throw new UnregisteredHookException($this->slug);
        }

        if ($this->hasNotKey($key)) {
            return [];
        }

        return $this->getRawForSlug()[$key];
    }



    /**
     * get an array of the hooks
     *
     * @return array
     * @throws UnregisteredHookException
     */
    public function get()
    {
        if ($this->isNotRegistered()) {
            throw new UnregisteredHookException($this->slug);
        }

        $array = $this->getSortedArray();
        $array = $this->applyClosures($array);
        $array = $this->removeDisabledOnes($array);

        return $array;
    }



    /**
     * get an array of the hooks, with the conditions bypassed
     *
     * @return array
     */
    public function all()
    {
        return $this->bypassConditions()->get();
    }



    /**
     * get keys of all the registered hooks
     *
     * @return array
     */
    public function getKeys()
    {
        return array_keys($this->get());
    }



    /**
     * get a key-value array of a hook
     *
     * @param string $value_attribute
     * @param string $key_attribute
     *
     * @return array
     */
    public function getPaired(string $value_attribute, string $key_attribute = "key"): array
    {
        $array = [];

        foreach ($this->get() as $item) {
            if (!isset($item[$key_attribute]) or !isset($item[$value_attribute])) {
                continue;
            }

            $key         = $item[$key_attribute];
            $value       = $item[$value_attribute];
            $array[$key] = $value;
        }

        return $array;
    }



    /**
     * process through the `method` and `class` attributes of the hook array and run them one by one.
     *
     * @param array ...$args
     *
     * @return void
     */
    public function handle(... $args)
    {
        foreach ($this->getPaired("method", "class") as $class => $method) {
            $class::$method($args);
        }
    }



    /**
     * get a relatively safe raw (unprocessed) array of the selected hook.
     *
     * @return array
     */
    protected function getRawForSlug()
    {
        if (!isset(static::$hooks[$this->slug])) {
            return [];
        }

        return static::$hooks[$this->slug];
    }



    /**
     * apply all closures
     *
     * @param array $array
     *
     * @return array
     */
    private function applyClosures(array $array): array
    {
        foreach ($array as $key => $inside) {
            foreach ($inside as $field => $value) {
                if (is_closure($value)) {
                    $array[$key][$field] = $value();
                } elseif ($field == "caption" and starts_with($value, "trans:")) {
                    $array[$key][$field] = trans(str_after($value, "trans:"));
                }
            }
        }

        return $array;
    }



    /**
     * remove disabled items
     *
     * @param array $array
     *
     * @return array
     */
    private function removeDisabledOnes(array $array): array
    {
        if ($this->bypass_conditions) {
            return $array;
        }

        $array = array_where($array, function ($value, $key) {
            return boolval($value['condition']);

        });

        return $array;
    }



    /**
     * get a sorted array of hooks
     *
     * @return array
     */
    private function getSortedArray()
    {
        $array = $this->getRawForSlug();

        if (!$array) {
            return [];
        }

        return array_sort($array, function ($value) {
            return $value['order'];
        });
    }
}
