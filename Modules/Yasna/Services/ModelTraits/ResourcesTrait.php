<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/17/18
 * Time: 4:57 PM
 */

namespace Modules\Yasna\Services\ModelTraits;

/**
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property int    $created_by
 * @property int    $updated_by
 * @property int    $deleted_by
 */
trait ResourcesTrait
{


    /**
     * Convert model to a resource array in the fetch view (special single view for the purpose of editor only).
     *
     * @return array
     */
    public function toFetchResource(): array
    {
        $result = [
             "id" => $this->hashid,
        ];

        $methods = $this->getFetchResourceMethods();

        if (!$methods) {
            return $this->toSingleResource();
        }

        foreach ($methods as $method) {
            $method_result = $this->$method();
            $result        = array_merge($result, $method_result);
        }

        return $result;
    }



    /**
     * Returns an array of the methods to be called for generating the fetch resource special single view for the
     * purpose of editor only).
     *
     * @return array
     */
    protected function getFetchResourceMethods(): array
    {
        return collect($this->methodsArray())
             // Methods should start with `toFetch`
             // Methods should end with `Resource`
             // Methods should have at least one character after `toFetch` and before `Resource`
             ->filter(function (string $method) {
                 return preg_match('/^toFetch(.)+Resource$/', $method);
             })->values()
             ->toArray()
             ;
    }
}
