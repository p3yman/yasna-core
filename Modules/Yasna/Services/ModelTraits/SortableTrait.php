<?php

namespace Modules\Yasna\Services\ModelTraits;


use Illuminate\Database\Eloquent\Builder;

trait SortableTrait
{
    /**
     * Checks if this model has a sortable field with the given name
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasSortableField(string $name): bool
    {
        return in_array($name, $this->sortableFields());
    }



    /**
     * Returns an array to be used in the sortable fields combo.
     *
     * @param string $value_field
     * @param string $caption_field
     *
     * @return array
     */
    public function sortableFieldsCombo(string $value_field = 'value', string $caption_field = 'title')
    {
        $titles = $this->sortableFieldsTitles();
        $result = [];

        foreach ($titles as $field => $title) {
            $result[] = [
                 $value_field   => $field,
                 $caption_field => $title,
            ];
        }

        return $result;
    }



    /**
     * Returns an array of sortable fields.
     *
     * @return array
     */
    public function sortableFields(): array
    {
        return array_keys($this->sortableFieldsTitles());
    }



    /**
     * Returns an array of sortable fields titles.
     *
     * @return array
     */
    public function sortableFieldsTitles(): array
    {
        $result  = [];
        $methods = $this->getSortableFieldsMethods();

        foreach ($methods as $method) {
            $method_result = $this->$method();
            $result        = array_merge_recursive($result, $method_result);
        }

        return $result;
    }



    /**
     * Returns an array of the methods to be called while finding the sortable fields.
     *
     * @return array
     */
    protected function getSortableFieldsMethods(): array
    {
        return collect($this->methodsArray())
             // Methods should end with `SortableFields`
             ->filter(function (string $method) {
                 return ends_with($method, 'SortableFields');
             })->values()
             ->toArray()
             ;
    }
}
