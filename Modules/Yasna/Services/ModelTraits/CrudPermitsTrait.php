<?php
namespace Modules\Yasna\Services\ModelTraits;

use App\Models\User;

/**
 * Class PersonsTrait
 *
 * @package Modules\Yasna\Services\ModelTraits
 * @property $creator
 */
trait CrudPermitsTrait
{
    /**
     * determine if the online user is permitted to get a list-view of the records.
     *
     * @return bool
     */
    public static function canList()
    {
        return dev();
    }



    /**
     * get dominant electors to be automatically applied when called by master crud.
     *
     * @return array
     */
    public static function dominantElectors()
    {
        return [];
    }



    /**
     * determine if the online user is permitted to soft-delete the model.
     *
     * @return bool
     */
    public function canTrash()
    {
        return dev();
    }



    /**
     * determine if the online user is permitted to destroy the model or restore it from the trash.
     *
     * @return bool
     */
    public function canDestroy()
    {
        return dev();
    }



    /**
     * determine if the online user is permitted to view the model.
     *
     * @return bool
     */
    public function canView()
    {
        return dev();
    }



    /**
     * determine if the online user can view the model when she is soft-deleted.
     *
     * @return bool
     */
    public function canViewTrashed()
    {
        return dev();
    }
}
