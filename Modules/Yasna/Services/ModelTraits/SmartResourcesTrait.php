<?php

namespace Modules\Yasna\Services\ModelTraits;

trait SmartResourcesTrait
{
    /**
     * keep the direct resources which hold the resources with direct access to the column content.
     *
     * @var array
     */
    protected static $direct_resources = [];



    /**
     * get selective resources.
     *
     * @param array|null|string $includes
     * @param array|null|string $excludes
     *
     * @return array
     */
    public function toResource($includes = null, $excludes = []): array
    {
        if (is_string($includes)) {
            $includes = (array)$includes;
        }
        $resources    = $this->getRequestedResources($includes, (array)$excludes);
        $result['id'] = $this->hashid;

        foreach ($resources as $resource) {
            $value             = $this->getResourceValue($resource);
            $result[$resource] = $value;
        }

        return $result;
    }



    /**
     * get the value of a specific resource.
     *
     * @param string $resource
     *
     * @return mixed
     */
    public function getResource($resource)
    {
        return $this->getResourceValue($resource);
    }



    /**
     * get available resources.
     *
     * @return array
     */
    public function getAvailableResources()
    {
        $cache_name = "resources_of_" . $this->getClassName();

        if (!debugMode() and cache()->has($cache_name)) {
            return cache()->get($cache_name);
        }

        $array = array_unique(array_merge(
             $this->getDirectResources(),
             $this->getAvailableResourceNames(),
             $this->getAvailableResourceGroupNames()
        ));

        cache()->add($cache_name, $array, 1000);

        return $array;
    }



    /**
     * Convert model to a resource array in the single view.
     *
     * @deprecated
     * @return array
     */
    public function toSingleResource(): array
    {
        return $this->toResource();
    }



    /**
     * Convert model to a resource array in the list view.
     *
     * @return array
     */
    public function toListResource(): array
    {
        return $this->toResource();
    }



    /**
     * add direct resource.
     *
     * @param string|array $names
     *
     * @return void
     */
    protected static function addDirectResources($names)
    {
        $called = static::className();

        if ($names == "*") {
            try {
                $instance = new static();
                $names    = array_merge($instance->getFields(), $instance->getMetaFields());
                $names    = array_drop($names, ["id", "meta"]);
            } catch (\Exception $e) {
                return;
            }
        }

        if (array_has(static::$direct_resources, $called)) {
            $names = array_unique(array_merge(static::$direct_resources[$called], (array)$names));
        } else {
            $names = (array)$names;
        }

        static::$direct_resources[$called] = $names;
    }



    /**
     * get the list of direct resource fields.
     *
     * @return array
     */
    private function getDirectResources()
    {
        $called = static::className();

        return static::$direct_resources[$called];
    }



    /**
     * get an array of the available resource methods, by checking the method name structures: getFolanResource()
     *
     * @return array
     */
    private function getAvailableResourceMethods(): array
    {
        return collect($this->methodsArray())
             ->filter(function (string $method) {
                 return preg_match('/^get(.)+Resource$/', $method);
             })->values()
             ->toArray()
             ;
    }



    /**
     * get an array of available resource names by masking $this->getAvailableResourceMethods() output.
     *
     * @return array
     */
    private function getAvailableResourceNames(): array
    {
        $array = $this->getAvailableResourceMethods();

        return array_map(function ($item) {
            return snake_case(str_after(str_before($item, "Resource"), "get"));
        }, $array);
    }



    /**
     * get an array of the available resource groups, by checking the method name structures: getFolanResourceGroup()
     *
     * @return array
     */
    private function getAvailableResourceGroupMethods(): array
    {
        return collect($this->methodsArray())
             ->filter(function (string $method) {
                 return preg_match('/^get(.)+ResourceGroup$/', $method);
             })->values()
             ->toArray()
             ;
    }



    /**
     * get an array of available resource names by masking $this->getAvailableResourceMethods() output.
     *
     * @return array
     */
    private function getAvailableResourceGroupNames(): array
    {
        $array = $this->getAvailableResourceGroupMethods();

        return array_map(function ($item) {
            return snake_case(str_after($item, "get"));
        }, $array);
    }



    /**
     * get requested resources.
     *
     * @param array|null $includes
     * @param array      $excludes
     *
     * @return array
     */
    private function getRequestedResources(?array $includes, ?array $excludes): array
    {
        $available = $this->getAvailableResources();
        if (is_null($includes)) {
            $includes = $available;
        }

        $includes = array_unique($this->spreadGroupedResources($includes));
        $excludes = array_unique($this->spreadGroupedResources($excludes));
        $result   = [];

        foreach ($includes as $item) {
            if (in_array($item, $excludes) or in_array("_" . $item, $excludes)) {
                continue;
            }

            if (starts_with($item, "_") and in_array(str_after($item, "_"), $excludes)) {
                continue;
            }

            $result[] = $item;
        }

        return array_unique(array_filter($result));
    }



    /**
     * spread grouped resource references by removing them from the array and adding their content instead.
     *
     * @param array $resources
     *
     * @return array
     */
    private function spreadGroupedResources(array $resources): array
    {
        $result = [];

        foreach ($resources as $resource) {
            if (!ends_with($resource, "resource_group")) {
                $result[] = $resource;
                continue;
            }

            $result = array_merge($result, $this->getResourceGroupItems($resource));
        }

        return $result;
    }



    /**
     * get items inside a resource group.
     *
     * @param string $group
     *
     * @return array
     */
    private function getResourceGroupItems($group): array
    {
        $method = camel_case("get-$group");
        if ($this->hasNotMethod($method)) {
            return [];
        }

        $items = $this->$method();
        if (starts_with($group, "_")) {
            foreach ($items as $key => $item) {
                if (!starts_with($item, "_")) {
                    $items[$key] = "_" . $item;
                }
            }
        }

        return $items;
    }



    /**
     * get resource value
     *
     * @param $resource
     *
     * @return mixed
     */
    private function getResourceValue($resource)
    {
        $resource = starts_with($resource, "_") ? str_after($resource, "_") : $resource;
        $method   = camel_case("get-$resource-Resource");

        if ($this->hasMethod($method)) {
            return $this->$method();
        }

        /*-----------------------------------------------
        | Auto Values ...
        */
        if (!in_array($resource, $this->getDirectResources())) {
            return null;
        }

        if ($this->isMeta($resource)) {
            $value = $this->getMeta($resource);
        } else {
            $value = $this->getAttribute($resource);
        }

        if (ends_with($resource, "_at")) {
            return $this->getResourceForTimestamps($value);
        }

        if (ends_with($resource, "_by")) {
            return $this->getResourceForPersons($value);
        }

        if (ends_with($resource, "_id")) {
            return $value ? hashid($value) : null;
        }

        return $value;
    }
}
