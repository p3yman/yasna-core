<?php

namespace Modules\Yasna\Services\ModelTraits;

use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\YasnaModel;

trait BasicResourcesTrait
{
    /**
     * boot BasicResourcesTrait
     *
     * @return void
     */
    public static function bootBasicResourcesTrait()
    {
        static::addDirectResources([
             "created_at",
             "updated_at",
             "deleted_at",
             "created_by",
             "updated_by",
             "deleted_by",
        ]);
    }



    /**
     * quickly get resource for persons.
     *
     * @param int $id
     *
     * @return array
     */
    protected function getResourceForPersons($id)
    {
        return user()::quickFind($id)->toResource(["full_name"]);
    }



    /**
     * quickly get resource for timestamps.
     *
     * @param int $id
     *
     * @return int
     */
    protected function getResourceForTimestamps($value)
    {
        if (is_int($value)) {
            return $value;
        }

        $return = $value ? carbon()::parse($value)->getTimestamp() : null;

        if($return < 0) {
            return null;
        }

        return $return;
    }



    /**
     * get standard resource from an eloquent collection
     *
     * @param Collection $collection
     * @param array|null $includes
     * @param array      $excludes
     *
     * @return Collection
     */
    protected function getResourceFromCollection(Collection $collection, $includes = null, $excludes = [])
    {
        return $collection->map(function ($model) use ($includes, $excludes) {
            /** @var YasnaModel $model */
            return $model->toResource($includes, $excludes);
        });
    }



    /**
     * get resource from array of files bounded in a field
     *
     * @param string $value
     *
     * @return array
     */
    protected function getResourceForFiles($value)
    {
        $hashids = explode_not_empty(",", $value);
        $files   = uploader()->filesArray($hashids);

        return $files;
    }



    /**
     * get array of resources involved in BasicTimestamps resource group.
     *
     * @return array
     */
    protected function getBasicTimestampsResourceGroup()
    {
        return [
             "created_at",
             "updated_at",
             "deleted_at",
        ];
    }
}
