<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/18/19
 * Time: 10:17 AM
 */

namespace Modules\Yasna\Services\TransCollector;


use Illuminate\Support\Facades\File;
use Modules\Yasna\Services\ModuleHelper;
use Modules\Yasna\Services\TransCollector\Guesser\LocalesGuesser;
use Modules\Yasna\Services\TransCollector\Guesser\ModulesGuesser;
use Symfony\Component\Finder\SplFileInfo;

class TransCollector
{
    /** @var ModulesGuesser */
    protected $modules_guesser;

    /** @var LocalesGuesser */
    protected $locales_guesser;

    /** @var bool */
    protected $with_overall;



    /**
     * TransCollector constructor.
     *
     * @param array|string|null $modules      If `null`, all the enabled modules will be applied.
     * @param array|string|null $locales      If `null`, all the site locales will be applied.
     * @param bool              $with_overall If the overall translations should be included.
     */
    public function __construct($modules = null, $locales = null, $with_overall = true)
    {
        $this->modules_guesser = new ModulesGuesser($modules);
        $this->locales_guesser = new LocalesGuesser($locales);
        $this->with_overall    = $with_overall;
    }



    /**
     * Returns an array for the translations.
     *
     * @return array
     */
    public function get()
    {
        $result  = [];
        $locales = $this->locales_guesser->getLocales();

        foreach ($locales as $locale) {
            $result[$locale] = $this->getForLocale($locale);
        }

        return $result;
    }



    /**
     * Returns an array of the translations for a locale.
     *
     * @param string $locale
     *
     * @return array
     */
    protected function getForLocale(string $locale)
    {
        return array_merge_recursive(
             $this->getOverallTranslationsForLocale($locale),
             $this->getModulesTranslationsForLocale($locale)
        );
    }



    /**
     * Returns the translations outside the Modules folder.
     *
     * @param string $locale
     *
     * @return array
     */
    protected function getOverallTranslationsForLocale(string $locale)
    {
        if (!$this->with_overall) {
            return [];
        }

        $result    = [];
        $directory = $this->getOverallTranslationsDirectory($locale);
        $files     = $this->getTranslationFilesInDirectory($directory);

        foreach ($files as $file) {
            $result[$file] = trans($file, [], $locale);
        }

        return [
             'overall' => $result,
        ];
    }



    /**
     * Returns the directory of the overall translations.
     *
     * @param string $locale
     *
     * @return string
     */
    protected function getOverallTranslationsDirectory(string $locale)
    {
        return base_path(implode(DIRECTORY_SEPARATOR, [
             'resources',
             'lang',
             $locale,
        ]));
    }



    /**
     * Returns the translations of all modules in the given locale.
     *
     * @param string $locale
     *
     * @return array
     */
    protected function getModulesTranslationsForLocale(string $locale)
    {
        $result  = [];
        $modules = $this->modules_guesser->getModules();

        foreach ($modules as $module) {
            $module_alias          = module($module)->getAlias();
            $result[$module_alias] = $this->getForModule($module, $locale);
        }

        return $result;
    }



    /**
     * Returns an array of the translations for the given module in the given locale.
     *
     * @param string $module_name
     * @param string $locale
     *
     * @return array
     */
    protected function getForModule(string $module_name, string $locale)
    {
        $result = [];
        $module = module($module_name);
        $files  = $this->getModuleTransFiles($module, $locale);

        foreach ($files as $file) {
            $result[$file] = $module->getTrans($file, [], $locale);
        }

        return $result;
    }



    /**
     * Returns an array of the trans file of the given module and the in given locale.
     *
     * @param ModuleHelper $module
     * @param string       $locale
     *
     * @return array
     */
    protected function getModuleTransFiles(ModuleHelper $module, string $locale)
    {
        $directory = $this->getModuleTransDirectory($module, $locale);

        return $this->getTranslationFilesInDirectory($directory);
    }



    /**
     * Returns the path to the trans directory of the given module and the in given locale.
     *
     * @param ModuleHelper $module
     * @param string       $locale
     *
     * @return string
     */
    protected function getModuleTransDirectory(ModuleHelper $module, string $locale)
    {
        return $module->getPath($this->transSubPath($locale));
    }



    /**
     * Returns the sub path for the translations in the given locale inside a module.
     * <br>
     * The returning path may be added to the path of a module to find the path of the translation files of that module.
     *
     * @param string $locale
     *
     * @return string
     */
    protected function transSubPath(string $locale)
    {
        return implode(DIRECTORY_SEPARATOR, $this->transSubPathParts($locale));
    }



    /**
     * Returns the parts of the sub path for the translations in the given locale inside a module.
     * <br>
     * This method returns the parts of the returning value for the `transSubPath()` method.
     *
     * @param string $locale
     *
     * @return array
     */
    protected function transSubPathParts(string $locale)
    {
        return [
             'Resources',
             'lang',
             $locale,
        ];
    }



    /**
     * Returns the name of the translations files in the given directory.
     *
     * @param string $directory
     *
     * @return array
     */
    protected function getTranslationFilesInDirectory(string $directory)
    {
        // check if the folder exists
        if (!File::exists($directory)) {
            return [];
        }

        // return the name of files without their extensions
        return collect(File::files($directory))
             ->map(function (SplFileInfo $file_info) {
                 $name      = $file_info->getFilename();
                 $extension = $file_info->getExtension();

                 return str_before($name, ".$extension");
             })
             ->toArray()
             ;
    }

}
