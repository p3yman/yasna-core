<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/18/19
 * Time: 11:31 AM
 */

namespace Modules\Yasna\Services\TransCollector\Guesser;


class LocalesGuesser
{
    /** @var array|string[] */
    protected $locales = [];



    /**
     * LocalesGuesser constructor.
     *
     * @param array|string|null $locales If `null`, all the site locales will be applied.
     */
    public function __construct($locales = null)
    {
        if (empty($locales)) {
            return;
        }

        $this->locales = (array)$locales;
    }



    /**
     * Returns an array of the acceptable locales.
     *
     * @return array|string[]
     */
    public static function acceptableLocales(): array
    {
        return (get_setting('site_locales') ?? [config('app.locale')]);
    }



    /**
     * Returns an array of the guessed locales.
     *
     * @return array|string[]
     */
    public function getLocales(): array
    {
        $safe_locales = $this->getSafeLocales();

        if (empty($safe_locales)) {
            return $this->acceptableLocales();
        }

        return $safe_locales;
    }



    /**
     * Returns a safe version of the input locales.
     *
     * @return array|string[]
     */
    protected function getSafeLocales(): array
    {
        return collect($this->locales)
             ->filter(function (string $locale) {
                 return in_array($locale, $this->acceptableLocales());
             })->toArray()
             ;
    }
}
