<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/18/19
 * Time: 11:26 AM
 */

namespace Modules\Yasna\Services\TransCollector\Guesser;


class ModulesGuesser
{
    /** @var array|string[] */
    protected $modules = [];



    /**
     * ModulesGuesser constructor.
     *
     * @param array|string|null $modules If `null`, all the enabled modules will be applied.
     */
    public function __construct($modules = null)
    {
        if (empty($modules)) {
            return;
        }

        $this->modules = (array)$modules;
    }



    /**
     * Returns an array of the acceptable modules.
     *
     * @return array|string[]
     */
    public static function acceptableModules(): array
    {
        return module()->list();
    }



    /**
     * Returns an array of the guessed modules.
     *
     * @return array|string[]
     */
    public function getModules(): array
    {
        $safe_modules = $this->getSafeModules();

        if (empty($safe_modules)) {
            return $this->acceptableModules();
        }

        return $safe_modules;
    }



    /**
     * Returns a safe version of the input modules.
     *
     * @return array|string[]
     */
    protected function getSafeModules(): array
    {
        return collect($this->modules)
             // make modules names studly case
             ->map(function (string $module_name) {
                 return studly_case($module_name);
             })
             // filter invalid modules names
             ->filter(function (string $module_name) {
                 return in_array($module_name, $this->acceptableModules());
             })
             ->toArray()
             ;
    }
}
