<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 3/3/19
 * Time: 12:43 PM
 */

namespace Modules\Yasna\Services\ResourceMapper;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;
use Modules\Yasna\Services\YasnaModel;

class ListMapper
{
    use ModuleRecognitionsTrait;

    /** @var Builder */
    protected $builder;

    /** @var YasnaListRequest */
    protected $request;



    /**
     * ListMapper constructor.
     *
     * @param Builder               $builder
     * @param YasnaListRequest|null $request
     */
    public function __construct($builder, $request)
    {
        $this->builder = $builder;

        if ($request) {
            $this->request = $request;
        } else {
            $this->request = (new SimpleYasnaListRequest());
        }
    }



    /**
     * Converts the builder to a white-house-compatible array.
     *
     * @return array
     */
    public function toArray()
    {
        if ($this->request->count) {
            return $this->toCountResult();
        }

        $this->applySort();

        if ($this->shouldBePaginated()) {
            return $this->toPaginatedResult();
        }

        $this->applyLimit();
        return $this->toFlatResult();
    }



    /**
     * apply sort on the builder
     *
     * @return void
     */
    protected function applySort()
    {
        $sort  = $this->request->sort;
        $order = $this->request->order;

        if ($sort == "rand" or $order == "rand") {
            $this->builder->inRandomOrder();
            return;
        }

        if (!$sort) {
            $sort = "id";
        }
        if (!$order) {
            $order = "desc";
        }

        $this->builder->orderBy($sort, $order);
    }



    /**
     * apply limit on non-paginated builders
     */
    protected function applyLimit()
    {
        $limit = min(500, $this->request->limit);

        $this->builder->limit($limit);
    }



    /**
     * Returns the paginated result.
     *
     * @return array
     */
    protected function toPaginatedResult()
    {
        $per_page  = $this->getPaginationLimit();
        $paginator = $this->builder->paginate($per_page);
        $result    = $this->mapResults($paginator);
        $meta      = $this->getPaginationMetaData($paginator);

        return $this->getSuccessResult($result, $meta);
    }



    /**
     * Returns the flat result.
     *
     * @return array
     */
    protected function toFlatResult()
    {
        $collection = $this->builder->get();
        $meta       = [
             "total" => $collection->count(),
        ];
        $data       = $this->mapResults($collection);

        return $this->getSuccessResult($data, $meta);
    }



    /**
     * returns the total number of the results only.
     *
     * @return array
     */
    protected function toCountResult()
    {
        return $this->getSuccessResult([], [
             "total" => $this->builder->count(),
        ]);
    }



    /**
     * If the result should be paginated or not.
     *
     * @return bool
     */
    protected function shouldBePaginated()
    {
        if (method_exists($this->request, 'isPaginated')) {
            return $this->request->isPaginated();
        }

        return false;
    }



    /**
     * Returns the limit of the pagination.
     *
     * @return int|null
     */
    protected function getPaginationLimit()
    {
        if (method_exists($this->request, 'perPage')) {
            return $this->request->perPage();
        }

        return null;
    }



    /**
     * Returns the metadata for the paginated result based on the given paginator.
     *
     * @param LengthAwarePaginator $paginator
     *
     * @return array
     */
    protected function getPaginationMetaData(LengthAwarePaginator $paginator)
    {
        $paginator->appends(request()->all());

        return [
             'total'             => $paginator->total(),
             'count'             => $paginator->count(),
             'per_page'          => $paginator->perPage(),
             'last_page'         => $paginator->lastPage(),
             'current_page'      => $paginator->currentPage(),
             'next_page_url'     => $paginator->nextPageUrl(),
             'previous_page_url' => $paginator->previousPageUrl(),
        ];
    }



    /**
     * Maps the given collection to the list resource of its items and returns it.
     *
     * @param Collection|LengthAwarePaginator $collection
     *
     * @return array
     */
    protected function mapResults($collection)
    {
        return $collection
             ->map(function (YasnaModel $model) {
                 $excluding = (array)$this->request->excluding;
                 $including = $this->request->including ? (array)$this->request->including : null;

                 return $model->toResource($including, $excluding);
             })
             ->toArray()
             ;
    }



    /**
     * Converts the given result and metadata to an API success array.
     *
     * @param array $results
     * @param array $metadata
     *
     * @return array
     */
    protected function getSuccessResult($results = [], $metadata = [])
    {
        return api()
             ->inModule($this->runningModuleName())
             ->successArray($results, $metadata)
             ;
    }
}
