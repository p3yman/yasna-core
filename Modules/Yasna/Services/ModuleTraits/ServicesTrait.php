<?php
namespace Modules\Yasna\Services\ModuleTraits;

use Closure;

trait ServicesTrait
{
    protected static $services;
    protected static $registered_services = [];

    protected $current_service;
    protected $current_key;
    protected $bypass_conditions = false;


    /*
    |--------------------------------------------------------------------------
    | General Inquery
    |--------------------------------------------------------------------------
    |
    */

    /**
     * get a rendered version of the services array.
     *
     * @return array
     */
    public function services()
    {
        /*-----------------------------------------------
        | Statically Called ...
        */
        if ($this->static()) {
            $result = [];
            $i      = 1;
            foreach ($this->list() as $module_name) {
                foreach (module($module_name)->services() as $service_name) {
                    $result[$i] = "$module_name:$service_name";
                    $i++;
                }
            }

            return $result;
        }


        /*-----------------------------------------------
        | Dynamic ...
        */
        $result = [];
        foreach ($this->registeredKeys() as $service) {
            $module_name  = str_before($service, ':');
            $service_name = str_after($service, ':');
            if ($module_name != $this->name) {
                continue;
            }

            $count = module($module_name)->service($service_name)->count();

            $result[] = "$service_name ($count)";
        }

        return $result;
    }



    /**
     * get a raw version of the services array.
     *
     * @return array
     */
    public function rawServices()
    {
        return self::$services;
    }



    /*
    |--------------------------------------------------------------------------
    | Registry
    |--------------------------------------------------------------------------
    |
    */

    /**
     * specify if the requested service (in chains) is correctly registered.
     *
     * @return bool
     */
    public function isRegistered()
    {
        $needle = "$this->name:$this->current_service";

        return in_array($needle, $this->registeredKeys());
    }



    /**
     * get the list of keys of the registered services.
     *
     * @return array
     */
    public function registeredKeys()
    {
        return array_keys(self::$registered_services);
    }



    /**
     * get a sorted array of registered services.
     *
     * @return array
     */
    public function registered()
    {
        $array = [];

        foreach (self::$registered_services as $key => $service) {
            if (!$this->static() and $this->name != $service['module_name']) {
                continue;
            }
            self::$registered_services[$key]['usages'] = service($key)->count();
            $array[]                                   = self::$registered_services[$key];
        }

        $array = array_sort($array, function ($value) {
            return $value['service_name'];
        });

        return $array;
    }



    /**
     * get a sorted array of unregistered services. (but why?!)
     *
     * @return array
     */
    public function unregistered()
    {
        $return = [];

        /*-----------------------------------------------
        | Static Calls ...
        */
        if ($this->static()) {
            foreach ($this->rawServices() as $module_name => $service) {
                $return[] = module($module_name)->unregistered();
            }

            return array_flatten($return);
        }

        /*-----------------------------------------------
        | Dynamic Calls ...
        */
        if (!isset($this->rawServices()[$this->name])) {
            return [];
        }
        foreach ($this->rawServices()[$this->name] as $service_name => $service) {
            $needle = "$this->name:$service_name";
            if (!service($needle)->isRegistered()) {
                $return[] = $needle;
            }
        }

        return $return;
    }



    /**
     * register a new service.
     *
     * @param string $service
     * @param string $comment
     *
     * @return $this
     */
    public function register($service = null, $comment = null)
    {
        /*-----------------------------------------------
        | If Input Argument is filled ...
        */
        if (isset($service)) {
            if (str_contains($service, ":")) {
                return service($service)->register(null, $comment);
            } else {
                return $this->service($service)->register(null, $comment);
            }
        }

        /*-----------------------------------------------
        | Normal Action ...
        */
        $slug = "$this->name:$this->current_service";

        self::$registered_services[$slug] = [
             'module_name'  => $this->name,
             'service_name' => $this->current_service,
             'comment'      => $comment,
             'usages'       => 0,
        ];

        return $this;
    }



    /*
    |--------------------------------------------------------------------------
    | Builder
    |--------------------------------------------------------------------------
    |
    */


    /**
     * set a flag to carry the service name.
     *
     * @param string $service_name
     *
     * @return $this
     */
    public function service($service_name)
    {
        $this->current_service = $service_name;

        return $this;
    }



    /**
     * set a flag to involve disabled services.
     *
     * @return $this
     */
    public function withDisabled()
    {
        $this->bypass_conditions = true;

        return $this;
    }



    /*
    |--------------------------------------------------------------------------
    | Add Chain Methods
    |--------------------------------------------------------------------------
    |
    */

    /**
     * add a service.
     *
     * @param string $key
     *
     * @return $this
     */
    public function add($key = null)
    {
        $map = "$this->name.$this->current_service";

        /*-----------------------------------------------
        | Random Key Generation (if not provided) ...
        */
        if (!$key) {
            do {
                $key = str_random(10);
            } while (array_has(self::$services, "$map.$key"));
        }

        /*-----------------------------------------------
        | Keys, already set ...
        */
        if ($this->has($key)) {
            $this->current_key = $key;
            return $this;
        }


        /*-----------------------------------------------
        | Property Set ...
        */
        $this->current_key = $key;
        array_set(self::$services, "$map.$key", [
             'default'   => false,
             'key'       => $key,
             'order'     => "22",
             'permit'    => null,
             'condition' => true,
             'comment'   => null,
        ]);

        return $this;
    }



    /**
     * set a key and its corresponding value to the service.
     *
     * @param string $field_name
     * @param mixed  $value
     *
     * @return $this
     */
    public function set($field_name, $value)
    {
        array_set(self::$services, "$this->name.$this->current_service.$this->current_key.$field_name", $value);

        return $this;
    }



    /**
     * set an icon to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function icon($value)
    {
        return $this->set('icon', $value);
    }



    /**
     * set a caption to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function caption($value)
    {
        return $this->set('caption', $value);
    }



    /**
     * set a trans argument to the service.
     *
     * @param string|Closure $value
     * @param array          ...$trans_arguments
     *
     * @return $this
     */
    public function trans($value, ... $trans_arguments)
    {
        return $this->set("caption", function () use ($value, $trans_arguments) {
            return trans($value, $trans_arguments);
        });
    }



    /**
     * set a link to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function link($value)
    {
        if (!is_closure($value)) {
            if (str_contains($value, 'url:')) {
                $value = url(str_after($value, 'url:'));
            }
        }

        return $this->set('link', $value);
    }



    /**
     * set an order to the service.
     *
     * @param int $value
     *
     * @return $this
     */
    public function order($value)
    {
        return $this->set('order', intval($value));
    }



    /**
     * set a short comment to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function comment($value)
    {
        return $this->set('comment', $value);
    }



    /**
     * set a permission string to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function permit($value)
    {
        return $this->set('permit', $value);
    }



    /**
     * set a condition to the service.
     *
     * @param boolean|Closure $value
     *
     * @return $this
     */
    public function condition($value)
    {
        return $this->set('condition', $value);
    }



    /**
     * set a blade to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function blade($value)
    {
        if (!is_closure($value)) {
            $value = strtolower($value);
        }

        /*-----------------------------------------------
        | Return ...
        */
        return $this->set('blade', $value);
    }



    /**
     * set a width to the service.
     *
     * @param int|Closure $value
     *
     * @return $this
     */
    public function width($value)
    {
        return $this->set('width', $value);
    }



    /**
     * set a method to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function method($value)
    {
        if (str_contains($value, ':') and !str_contains($value, '::')) {
            $module = module(str_before($value, ':'));
            $value  = str_after($value, ':');

            if (str_contains($value, "Controller")) { //@TODO: Add more Identifier, like providers etc.
                $value = $module->controller($value);
                $value = str_replace("@", "::", $value);
            } else {
                $value = $module->provider() . '::' . $value;
            }
        }

        return $this->set('method', $value);
    }



    /**
     * set a default value to the service.
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function default($value)
    {
        return $this->set('default', $value);
    }



    /**
     * set a trait to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function trait($value)
    {
        if (str_contains($value, ':')) {
            $module     = module(str_before($value, ':'));
            $trait_name = str_after($value, ':');
            $value      = $module->getNamespace("Entities\\Traits\\" . studly_case($trait_name));
        }

        return $this->set('trait', $value);
    }



    /**
     * set a class to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function class($value)
    {
        return $this->set('class', $value);
    }



    /**
     * set a color to the service.
     *
     * @param string|Closure $value
     *
     * @return $this
     */
    public function color($value)
    {
        return $this->set('color', $value);
    }



    /**
     * set a `to` key to the service.
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function to($value)
    {
        if (str_contains($value, ':')) {
            $value = str_after($value, ':');
        }

        return $this->set('to', $value);
    }



    /**
     * set a value to the service.
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function value($value)
    {
        return $this->set('value', $value);
    }



    /*
    |--------------------------------------------------------------------------
    | Read Chain Methods
    |--------------------------------------------------------------------------
    |
    */

    /**
     * read the registered services
     * TODO: Need to be refactored!
     *
     * @param array $filters
     *
     * @return array|mixed
     */
    public function read($filters = [])
    {
        $array = $this->raw();

        if (!$array) {
            return [];
        }

        /*-----------------------------------------------
        | Preparations ...
        */
        $array = array_sort($array, function ($value) {
            if (!isset($value['order'])) {
                $value['order'] = 1;
            }

            return $value['order'];
        });

        /*-----------------------------------------------
        | Closures ...
        */
        foreach ($array as $key => $inside) {
            foreach ($inside as $field => $value) {
                if (is_closure($value)) {
                    $array[$key][$field] = $value();
                } elseif ($field == "caption" and starts_with($value, "trans:")) {
                    $array[$key][$field] = trans(str_after($value, "trans:"));
                }
            }
        }


        /*-----------------------------------------------
        | WithDisabled ...
        */
        if (!$this->bypass_conditions) {
            $array = array_where($array, function ($value, $key) {
                $condition = isset($value['condition']) ? $value['condition'] : false;
                if (is_closure($condition)) {
                    /** @var \Closure $condition */
                    $condition = $condition();
                }
                return boolval($condition);
            });
        }

        /*-----------------------------------------------
        | Filters ...
        */
        $array = $this->applyFilters($array, $filters);

        /*-----------------------------------------------
        | Return ...
        */

        return $array;
    }



    /**
     * get the raw array, without sort or render
     *
     * @return array
     */
    public function raw()
    {
        if (!$this->isInitialized()) {
            return [];
        }
        /*-----------------------------------------------
        | Check Registry ...
        */
        if (!$this->isRegistered()) {
            return [];
        }

        $array = array_get(self::$services, "$this->name.$this->current_service");

        return $array;
    }



    /**
     * apply filters on the services array
     *
     * @param array $services
     * @param array $filters
     *
     * @return $array
     */
    protected function applyFilters(array $services, array $filters): array
    {
        foreach ($filters as $key => $value) {
            $services = $this->applyFilter($services, $key, $value);
        }

        return $services;
    }



    /**
     * apply a single filter on the services array
     *
     * @param array  $services
     * @param string $key
     * @param mixed  $desired_value
     *
     * @return array
     */
    protected function applyFilter(array $services, string $key, $desired_value): array
    {
        $services = array_where($services, function ($service) use ($key, $desired_value) {
            return isset($service[$key]) and $service[$key] == $desired_value;
        });

        return $services;
    }



    /**
     * count the number of items added to the service.
     *
     * @return int
     */
    public function count()
    {
        return count($this->raw());
    }



    /**
     * get an indexed version of the service in question.
     *
     * @param array ...$keys
     *
     * @return array
     */
    public function indexed(... $keys)
    {
        $result  = [];
        $array   = $this->read();
        $counter = 0;

        foreach ($array as $item) {
            foreach ($keys as $key) {
                if (isset($item[$key])) {
                    $result[$counter][] = $item[$key];
                } else {
                    $result[$counter][] = null;
                }
            }
            $counter++;
        }

        return $result;
    }



    /**
     * get a paired array of key-value entries
     *
     * @param mixed  $result_value
     * @param string $result_key
     * @param array  $filters
     *
     * @return array
     */
    public function paired($result_value, $result_key = 'key', array $filters = [])
    {
        $result = [];
        $array  = $this->read($filters);

        foreach ($array as $key => $item) {
            if (isset($item[$result_key]) and isset($item[$result_value])) {
                $result[$item[$result_key]] = $item[$result_value];
            }
        }

        return $result;
    }



    /**
     * specify if the service has the given key defined.
     *
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        $array = $this->raw();

        return array_has($array, $key);
    }



    /**
     * find the service key.
     *
     * @param string $key
     *
     * @return $this
     */
    public function find($key)
    {
        if (isset($this->raw()[$key])) {
            $this->current_key = $key;
        } else {
            $this->current_key = null;
        }

        return $this;
    }



    /**
     * TODO
     *
     * @param string $key1
     * @param string $key2
     *
     * @return array|null
     */
    public function get($key1 = null, $key2 = null)
    {
        /*-----------------------------------------------
        | Polymorphism handling ...
        */
        if (isset($key2)) {
            return $this->find($key1)->get($key2);
        } else {
            $key = $key1;
        }


        /*-----------------------------------------------
        | When not properly set ...
        */
        if (!$this->current_key) {
            if ($key) {
                return null;
            } else {
                return [];
            }
        }

        /*-----------------------------------------------
        | When a whole array is requested ...
        */
        if (!$key) {
            $array = $this->read();
            if (isset($array[$this->current_key])) {
                return $array[$this->current_key];
            }
            return [];
        }

        /*-----------------------------------------------
        | When a single key is requested ...
        */
        if (isset($this->read()[$this->current_key][$key])) {
            return $this->read()[$this->current_key][$key];
        } else {
            return null;
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Process
    |--------------------------------------------------------------------------
    |
    */
    /**
     * remove a key from the service.
     *
     * @param string $key
     *
     * @return $this
     */
    public function remove($key)
    {
        $map = "$this->name.$this->current_service.$key";
        array_forget(self::$services, $map);

        return $this;
    }



    /**
     * set a flag to identify the service in question is being updated.
     *
     * @param string $key
     *
     * @return $this
     */
    public function update($key)
    {
        $this->current_key = $key;

        return $this;
    }



    /**
     * TODO!
     *
     * @param array ...$arguments
     *
     * @return $this
     */
    public function handle(...$arguments)
    {
        $array = $this->read();
        foreach ($array as $item) {
            if (isset($item['method'])) {
                $item['method'](... $arguments);
            }
        }

        return $this;
    }



    /**
     * TODO
     *
     * @param string $argument
     *
     * @return bool
     */
    public function isset($argument = null)
    {
        if (!count($this->raw())) {
            return false;
        }
        if ($argument) {
            return isset($this->raw()[$argument]);
        }

        return true;
    }
}
