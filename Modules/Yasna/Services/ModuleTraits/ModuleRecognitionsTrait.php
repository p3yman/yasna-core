<?php namespace Modules\Yasna\Services\ModuleTraits;

/**
 * can be used on any class inside modules to retreive the information about the current running module
 *
 * @package Modules\Yasna\Services\ModuleTraits
 */
trait ModuleRecognitionsTrait
{
    private $running_module;



    /**
     * get the module name of the called class
     *
     * @return string|null
     */
    public static function getModuleName()
    {
        $namespace = (new \ReflectionClass(get_called_class()))->getNamespaceName();

        $array = explode("\\", $namespace);

        if (array_first($array) == "Modules") {
            return $array[1];
        }

        return null;
    }



    /**
     * get the module instance of the called class
     *
     * @return \Modules\Yasna\Services\ModuleHelper
     */
    public static function getModuleInstance()
    {
        return module(static::getModuleInstance());
    }



    /**
     * get the module alias of the called class
     *
     * @return string
     */
    public static function getModuleAlias()
    {
        return static::getModuleInstance()->getAlias();
    }



    /**
     * get the running module instance
     *
     * @return \Modules\Yasna\Services\ModuleHelper
     */
    protected function runningModule()
    {
        return module($this->runningModuleName());
    }



    /**
     * get the running module name
     *
     * @return string
     */
    protected function runningModuleName()
    {
        $name  = get_class($this);
        $array = explode("\\", $name);

        if (array_first($array) == 'Modules') {
            return $array[1];
        }

        return null;
    }



    /**
     * get the running module alias
     *
     * @return string
     */
    protected function runningModuleAlias()
    {
        return $this->runningModule()->getAlias();
    }
}
