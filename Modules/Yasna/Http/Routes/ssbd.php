<?php

Route::group([
     'namespace' => module('Yasna')->getControllersNamespace(),
], function () {
    Route::patch('api/ssbd', 'SsbdController@action');
});
