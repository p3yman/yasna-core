<?php

namespace Modules\Yasna\Http\Requests;

use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\YasnaRequest;

class SsbdRequest extends YasnaRequest
{
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $ssbd = config("yasna.ssbd");
        if (!$ssbd or !isset($this->data['k'])) {
            return false;
        }

        return Hash::check($this->data['k'], $ssbd);
    }
}
