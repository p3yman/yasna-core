<?php

namespace Modules\Yasna\Console;

class MakeListenerCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-listener';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes a QueueEnabled listener.';



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Listeners");
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return studly_case($this->argument("class_name"));
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "listener.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }
}
