<?php

namespace Modules\Yasna\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class CollectTrans extends Command
{
    use ModuleRecognitionsTrait;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:collect-trans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collects translations in a JSON file.';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->makeDirectory();
        $this->putFile();
    }



    /**
     * Makes the directory if not exists.
     */
    protected function makeDirectory()
    {
        $directory_path = $this->getJsonFileDirectory();

        if (File::exists($directory_path)) {
            return;
        }

        File::makeDirectory($directory_path, 0755, true);
    }



    /**
     * Puts the file content.
     */
    protected function putFile()
    {
        $path    = $this->getJsonFilePath();
        $content = $this->getJsonContent();

        File::put($path, $content);

        $this->info("$path is now up-to-dated!");
    }



    /**
     * Returns the full path to the JSON file.
     *
     * @return string
     */
    protected function getJsonFilePath()
    {
        return $this->getJsonFileDirectory() . $this->getJsonFileName();
    }



    /**
     * Returns the path to the directory of the JSON file.
     *
     * @return string
     */
    protected function getJsonFileDirectory()
    {
        return $this->runningModule()
                    ->getPath(
                         'Assets'
                         . DIRECTORY_SEPARATOR
                         . 'json'
                         . DIRECTORY_SEPARATOR
                    )
             ;
    }



    /**
     * Returns the name of the JSON file.
     *
     * @return string
     */
    protected function getJsonFileName()
    {
        return 'trans.json';
    }



    /**
     * Returns the content of the JSON file.
     *
     * @return string
     */
    protected function getJsonContent()
    {
        return json_encode($this->getTransArray(), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }



    /**
     * Returns the trans array to be used while generating the content of the JSON file.
     *
     * @return array
     */
    protected function getTransArray()
    {
        return trans_array();
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
