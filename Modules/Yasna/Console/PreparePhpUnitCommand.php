<?php

namespace Modules\Yasna\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Modules\Yasna\Services\ModuleHelper;
use Nwidart\Modules\Support\Stub;
use Symfony\Component\Console\Input\InputArgument;

class PreparePhpUnitCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:prepare-phpunit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare phpunit.xml according to the active modules.';

    /**
     * keep the array of modules, to be present in the phpunit.xml
     *
     * @var array
     */
    protected $modules;

    /**
     * keep the filesystem injected dependency
     *
     * @var FileSystem
     */
    protected $filesystem;



    /**
     * makeRuleCommand constructor.
     *
     * @param Filesystem|null $filesystem
     */
    public function __construct(Filesystem $filesystem = null)
    {
        $this->filesystem = $filesystem;

        parent::__construct();
    }



    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->setModulesArray();
        $this->makeFile();
    }



    protected function setModulesArray()
    {
        $requested = $this->argument("modules");

        if ($requested) {
            $this->modules = explode_not_empty(",", $requested);
            return;
        }

        $this->modules = module()->list();
    }



    /**
     * make the requested file
     */
    protected function makeFile()
    {
        $path = $this->getFolderPath() . DIRECTORY_SEPARATOR . "phpunit.xml";

        $this->filesystem->put($path, $this->getStubContent());
        $this->info("Prepared phpunit.xml!");
    }



    /**
     * get stub content
     *
     * @return string
     */
    protected function getStubContent()
    {
        $base_path = __DIR__ . DIRECTORY_SEPARATOR . "stubs";
        Stub::setBasePath($base_path);

        $stub = new Stub("/phpunit.stub", $this->getReplacement());

        return $stub->render();
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "FEATURES" => $this->getFeatureTestsTagContent(),
             "UNITS"    => $this->getUnitTestsTagContent(),
        ];
    }



    /**
     * get the tags for feature tests
     *
     * @return string
     */
    protected function getFeatureTestsTagContent()
    {
        $array = [];

        foreach ($this->modules as $module) {
            $array[] = "<directory suffix=\"Test.php\">./Modules/$module/Tests/Feature</directory>";
        }

        return implode(LINE_BREAK . TAB . TAB . TAB, $array);
    }



    /**
     * get the tags for unit tests
     *
     * @return string
     */
    protected function getUnitTestsTagContent()
    {
        $array = [];

        foreach ($this->modules as $module) {
            $array[] = "<directory suffix=\"Test.php\">./Modules/$module/Tests/Unit</directory>";
        }

        return implode(LINE_BREAK . TAB . TAB . TAB, $array);
    }



    /**
     * get the target folder os path
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return app_path() . "/..";
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['modules', InputArgument::OPTIONAL, 'Modules to be Present in the phpunit.xml'],
        ];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
