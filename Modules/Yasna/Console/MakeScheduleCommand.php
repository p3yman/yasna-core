<?php

namespace Modules\Yasna\Console;

class MakeScheduleCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a Schedule class, extended from YasnaSchedule.';



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Schedules");
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Schedule')) {
            return $file_name . "Schedule";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "schedule.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }

}
