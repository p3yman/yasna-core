<?php

namespace Modules\Yasna\Console;

class MakeRequestCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes a Validation Request file, extending YasnaRequest class.';

    /** @var  int */
    protected $version;

    /** @var  string */
    protected $parent;

    /**
     * check the request is api or not
     *
     * @var bool
     */
    protected $is_api;



    /**
     * @inheritDoc
     */
    protected function customHandle()
    {
        $this->askIsApi();
        if ($this->is_api) {
            $this->askVersion();
        }
        $this->askRequestNature();
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "CLASS"   => $this->getFileName(),
             "MODULE"  => $this->module_name,
             "VERSION" => "V" . $this->version,
             "PARENT"  => $this->parent,
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        $parent_path = $this->module->getPath("Http" . DIRECTORY_SEPARATOR . "Requests");
        if ($this->version != null) {
            return $parent_path . DIRECTORY_SEPARATOR . "V" . $this->version;
        }
        return $parent_path;

    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Request')) {
            return $file_name . "Request";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        if ($this->is_api) {
            return "api-request.stub";
        }
        return "request.stub";
    }



    /**
     * ask this request is for Api via a dialogue
     *
     * @return void
     */
    protected function askIsApi()
    {
        $allowed_answers = ["No", "Yes"];
        $answer          = $this->choice("Is it for an API endpoint?", $allowed_answers, 1);

        $this->line("Let's use $answer.");

        $this->is_api = boolval($answer == "Yes");
    }



    /**
     * ask the nature of the request (form / list)
     *
     * @return void
     */
    protected function askRequestNature()
    {
        $allowed_answers = ["Form Validation", "List-View Preparation"];
        $answer          = $this->choice("What do you need this for?", $allowed_answers, 0);

        $this->line("Let's use $answer.");

        if ($answer == $allowed_answers[0]) {
            $this->parent = "YasnaFormRequest";
        } elseif ($answer == $allowed_answers[1]) {
            $this->parent = "YasnaListRequest";
        }
    }



    /**
     * Ask the requested version via a dialogue
     *
     * @return void
     */
    protected function askVersion()
    {
        $suggest = $this->discoverCurrentVersion();
        $version = $this->ask("Desired Version (Probably $suggest)?");

        if (is_null($version)) {
            $version = $suggest;
        }

        while (!is_numeric($version) or $version < 1) {
            $version = $this->ask("Something greater than 1 please.");
            if (is_null($version)) {
                $version = $suggest;
            }
        }

        $this->version = $version;
    }



    /**
     * discover a possible version based on the file system
     *
     * @return int
     */
    protected function discoverCurrentVersion(): int
    {
        $parent_path = $this->getFolderPath();
        if (!file_exists(($parent_path))) {
            return 1;
        }

        $subs = scandir($parent_path);
        $max  = 1;
        foreach ($subs as $sub) {
            $version = (int)str_after($sub, "V");
            $max     = max($version, $max);
        }

        return $max;
    }
}
