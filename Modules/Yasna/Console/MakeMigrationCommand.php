<?php

namespace Modules\Yasna\Console;

use Nwidart\Modules\Support\Stub;
use Symfony\Component\Console\Input\InputArgument;

class MakeMigrationCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes a Migration Table, for the specified module.';

    /**
     * keep the schema name
     *
     * @var string
     */
    protected $schema;



    /**
     * purify input
     *
     * @return void
     */
    protected function purifier()
    {
        $this->class_name  = $this->argument('class_name');
        $this->module_name = $this->argument('module_name');

        if (!$this->class_name or !$this->module_name) {
            $this->error('Pattern: yasna:make-migration [class_name] [module_name]. Both are required.');
            die();
        }

        $this->class_name = $this->desiredTimestampBeforeMigrationFileName() . '_' . $this->class_name;

        $this->class_name  = snake_case($this->class_name);
        $this->module_name = studly_case($this->module_name);

        $this->module = module($this->module_name);

        if ($this->module->isNotValid()) {
            $this->error("Invalid module name");
            die();
        }
    }



    /**
     * get stub content
     *
     * @return string
     */
    protected function getStubContent()
    {
        $base_path = __DIR__ . DIRECTORY_SEPARATOR . "stubs";
        Stub::setBasePath($base_path);

        $this->desiredSchemaName();
        $this->desiredClassName();

        $stub = new Stub("/" . $this->getStubName(), $this->getReplacement());

        return $stub->render();
    }



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('Database' . DIRECTORY_SEPARATOR . 'Migrations');
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return $this->class_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        if ($this->isCreateMode()) {
            return "create-migration.stub";
        }

        return "add-migration.stub";
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "CLASS"  => $this->class_name,
             "SCHEMA" => $this->schema,
        ];
    }



    /**
     * check if we are in the create mode.
     *
     * @return bool
     */
    protected function isCreateMode()
    {
        return str_contains($this->class_name, 'Create');
    }



    /**
     * Checks if the class_name contains the keyword 'add'.
     *
     * @return bool
     */
    protected function hasAddKeyword()
    {
        return str_contains($this->class_name, 'add');
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['class_name', InputArgument::OPTIONAL, 'Request Class Name'],
             ['module_name', InputArgument::OPTIONAL, 'Module Name'],
        ];
    }



    /**
     * Returns the desired string before the migration file name.
     *
     * @return string
     */
    protected function desiredTimestampBeforeMigrationFileName()
    {
        $current_timestamp    = date(now());
        $first_step           = str_replace(['-', ' '], '_', $current_timestamp);
        $final_desired_string = str_replace(':', '', $first_step);

        return $final_desired_string;
    }



    /**
     * Returns the desired class name in studly case form.
     *
     * @return string
     */
    protected function desiredClassName()
    {
        return $this->class_name = substr(studly_case($this->class_name), 14);
    }



    /**
     * Returns the desired schema name.
     *
     * @return string
     */
    protected function desiredSchemaName()
    {
        if ($this->hasAddKeyword()) {
            return $this->schema = strtolower(str_before(str_after($this->class_name, 'add_'), '_table'));
        }
        return $this->schema = strtolower(str_before(str_after($this->class_name, 'create_'), '_table'));
    }

}
