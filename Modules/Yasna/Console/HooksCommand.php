<?php

namespace Modules\Yasna\Console;

use Hamcrest\Core\Set;
use Illuminate\Console\Command;
use Modules\Yasna\Entities\Setting;
use Modules\Yasna\Services\ModuleHelper;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HooksCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:hooks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of all registered hooks.';

    /**
     * keep the module name requested by the user
     *
     * @var string
     */
    protected $module_name;

    /**
     * keep the module instance requested by the user
     *
     * @var ModuleHelper
     */
    protected $module;



    /**
     * handle the command
     *
     * @return void
     */
    public function handle()
    {
        $this->purifyInput();

        if ($this->module_name) {
            $this->showOneModule($this->module, true);
            return;
        }

        $this->showAllModules();
    }



    /**
     * show the hooks of all modules
     *
     * @return void
     */
    protected function showAllModules()
    {
        foreach (module()->list() as $module_name) {
            $this->showOneModule(module($module_name));

        }
    }



    /**
     * show the hooks of one module
     *
     * @param ModuleHelper $module
     * @param bool         $show_empty
     *
     * @return void
     */
    protected function showOneModule(ModuleHelper $module, $show_empty = false)
    {
        $hooks = $module->hooks();

        if (!$hooks and !$show_empty) {
            return;
        }

        $title = $module->name . " Module";
        $chars = strlen($title);

        $this->line("");
        $this->warn($title);
        $this->warn(str_repeat("-", $chars));


        foreach ($hooks as $hook => $array) {
            $title = $hook . " (" . $array['comment'] . ")";

            $this->info($title . "...");

            foreach ($array['attributes'] as $attribute => $description) {
                $this->line("$attribute: $description");
            }
            if (!count($array['attributes'])) {
                $this->line("(without particular attributes!)");
            }
            $this->line("");

        }
        if(!$hooks) {
            $this->line("No hooks in this module.");
        }
    }



    /**
     * purify input
     */
    protected function purifyInput()
    {
        $this->module_name = $this->argument('module_name');
        $this->module      = module($this->module_name);

        if ($this->module_name and $this->module->isNotValid()) {
            $this->error("Invalid module name!");
            die();
        }
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['module_name', InputArgument::OPTIONAL, 'Module Name'],
        ];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
