<?php

namespace Modules\Yasna\Console;

use Illuminate\Database\Console\Migrations\BaseCommand;
use Illuminate\Database\Migrations\Migrator as BaseMigrator;
use Illuminate\Support\Facades\DB;
use Nwidart\Modules\Migrations\Migrator;

class CheckMigrationCommand extends BaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:check-migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show list of non-executed migrations of enable modules';

    /**
     * The migrator instance.
     *
     * @var BaseMigrator
     */
    protected $migrator;



    /**
     * return false if there is some migrations to execute
     *
     * @return bool
     */
    public static function verify()
    {
        $obj = new CheckMigrationCommand();
        $obj->setLaravel(app());

        $migrations = $obj->findMigrations();
        $result     = $obj->grepNonExecutedMigrations($migrations);
        if (empty($result)) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->migrator = app('migrator');
    }



    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->allMigrationsRan()) {
            if (empty($result)) {
                $this->showAllRanMessage();
                return;
            }
        }

        $this->showResult();
    }



    /**
     * Whether all the migrations have been ran.
     *
     * @return bool
     */
    protected function allMigrationsRan()
    {
        $migrations = $this->findMigrations();
        $pending    = $this->grepNonExecutedMigrations($migrations);

        return empty($pending);
    }



    /**
     * Shows a message if all migration ran.
     */
    protected function showAllRanMessage()
    {
        $this->info("All Migrations Ran :)");
    }



    /**
     * Shows the result.
     */
    protected function showResult()
    {
        $this->error("Detected Not-Executed Migrations:");

        $this->showOverallMigrations();
        $this->showModulesMigrations();
    }



    /**
     * Shows overall pending migrations.
     */
    protected function showOverallMigrations()
    {
        $migrations = $this->findOverallMigrations();
        $pending    = $this->grepNonExecutedMigrations($migrations);

        if (empty($pending)) {
            return;
        }

        $this->showResultGroup("Overall:", $pending['Overall']);
    }



    /**
     * Shows modules pending migrations.
     */
    private function showModulesMigrations()
    {
        $migrations = $this->findModulesMigrations();
        $result     = $this->grepNonExecutedMigrations($migrations);

        foreach ($result as $module => $migrations) {
            $this->showResultGroup("In `$module` Module:", $migrations);
        }
    }



    /**
     * Shows a group of migrations.
     *
     * @param string $title
     * @param array  $lines
     */
    protected function showResultGroup(string $title, array $lines)
    {
        $this->warn($title);
        foreach ($lines as $line) {
            $this->line($line);
        }
    }



    /**
     * Ger all migrations.
     *
     * @return array
     */
    private function findMigrations()
    {
        return array_merge(
             $this->findModulesMigrations(),
             $this->findOverallMigrations()
        );
    }



    /**
     * get migrations of enable modules
     *
     * @return array
     */
    protected function findModulesMigrations()
    {
        $modules    = module()->list();
        $migrations = [];

        foreach ($modules as $module) {
            $founded_module      = module($module)->module;
            $path                = (new Migrator($founded_module))->getPath();
            $migrations[$module] = $this->getMigrationsFiles($path);
        }

        return $migrations;
    }



    /**
     * Finds all overall migrations.
     *
     * @return array
     */
    protected function findOverallMigrations()
    {
        $path               = $this->getMigrationPath();
        $overall_migrations = $this
             ->migrator
             ->getMigrationFiles($path);

        if (empty($overall_migrations)) {
            return [];
        }

        return [
             'Overall' => array_keys($overall_migrations),
        ];
    }



    /**
     * return none-executed migrations
     *
     * @param array $migrations
     *
     * @return array
     */
    private function grepNonExecutedMigrations($migrations)
    {
        $nones_migrations = [];
        foreach ($migrations as $module => $migration) {
            foreach ($migration as $item) {
                $ran = DB::table('migrations')
                         ->where('migration', $item)
                         ->exists()
                ;
                if (!$ran) {
                    $nones_migrations[$module][] = $item;
                }
            }
        }
        return $nones_migrations;
    }



    /**
     * get migrations from a path
     *
     * @param string $path
     *
     * @return array
     */
    private function getMigrationsFiles($path)
    {
        return array_map(function ($item) {
            $file_name = substr($item, strrpos($item, "/") + 1);
            return substr($file_name, 0, strlen($file_name) - 4);
        }, glob($path . "/*.php"));
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
