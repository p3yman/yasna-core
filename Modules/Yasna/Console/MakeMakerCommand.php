<?php


namespace Modules\Yasna\Console;


class MakeMakerCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-maker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make the uniform maker command file in the chosen module.';



    /**
     * get the folder path.
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Console");
    }



    /**
     * get stub replacement.
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Command')) {
            return $file_name . "Command";
        }
        return $file_name;
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "maker.stub";
    }
}
