<?php

namespace Modules\Yasna\Console;

use Illuminate\Console\Command;

class RemoveUnusedModulesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:remove-unused-modules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'removes unused modules.';



    /**
     * Execute the console command.
     */
    public function handle()
    {
        $n = 0;

        foreach (module()->list(true) as $module) {
            if (module($module)->isActive()) {
                continue;
            }

            $this->deleteModule($module);
            $n++;
        }

        $this->info("Deleted $n inactive modules.");
    }


    /**
     * delete the given module
     *
     * @param string $path
     */
    private function deleteModule(string $module_name)
    {
        $path = module($module_name)->getPath();

        $this->deleteDirectory($path);
        $this->line("Deleted Module/$module_name directory.");
    }



    /**
     * delete a directory recursively
     *
     * @param string $path
     */
    private function deleteDirectory(string $path)
    {
        if (is_dir($path)) {
            $objects = scandir($path);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($path . "/" . $object)) {
                        $this->deleteDirectory($path . "/" . $object);
                    } else {
                        unlink($path . "/" . $object);
                    }
                }
            }
            rmdir($path);
        }
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }



    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
