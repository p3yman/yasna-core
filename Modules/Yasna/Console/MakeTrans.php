<?php

namespace Modules\Yasna\Console;

use Symfony\Component\Console\Input\InputOption;

class MakeTrans extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-trans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes a translation file in the module trans folder.';

    /**
     * keep the stub file name
     *
     * @var string
     */
    protected $stub_name = "trans";

    /**
     * keep the lang name
     *
     * @var string
     */
    protected $lang;



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->purifier();

        if ($this->option('fa') === true
             or ($this->option('fa') !== true
                  and $this->option('en') !== true
                  and $this->option('ar') !== true)) {
            $this->lang = 'fa';
            $this->makeFolder();
            $this->makeFile();
        }

        if ($this->option('en') === true) {
            $this->lang = 'en';
            $this->makeFolder();
            $this->makeFile();
        }

        if ($this->option('ar') === true) {
            $this->lang = 'ar';
            $this->makeFolder();
            $this->makeFile();
        }

        return;
    }



    /**
     * make the required folder if not exist already
     *
     * @return void
     */
    protected function makeFolder()
    {
        $path = $this->getFolderPath() . DIRECTORY_SEPARATOR . strtolower($this->lang);

        if (!is_dir($path)) {
            mkdir($path);
            $this->line("Made folder...");
        }
    }



    /**
     * make the requested file
     *
     * @return void
     */
    protected function makeFile()
    {
        $path = $this->getFolderPath()
             . DIRECTORY_SEPARATOR . strtolower($this->lang)
             . DIRECTORY_SEPARATOR . $this->getFileName() . '.php';

        if (file_exists($path)) {
            $this->error("Already Exists: $path");
            die();
        }

        $this->filesystem->put($path, $this->getStubContent());
        $this->info("Created: $path");

    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
             ['fa', null, InputOption::VALUE_NONE, 'Flag to make for Persian', null],
             ['en', null, InputOption::VALUE_NONE, 'Flag to make for English', null],
             ['ar', null, InputOption::VALUE_NONE, 'Flag to make for Arabic', null],
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('Resources' . DIRECTORY_SEPARATOR . 'lang');
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return strtolower($this->argument("class_name"));
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [];
    }



    /**
     * @return string
     */
    protected function getStubName()
    {
        return "trans.stub";
    }
}
