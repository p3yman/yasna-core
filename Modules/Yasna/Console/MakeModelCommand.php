<?php

namespace Modules\Yasna\Console;

class MakeModelCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes an empty Model file, extending YasnaModel class.';



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('Entities');
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return studly_case($this->argument("class_name"));
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "model.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }
}
