<?php

namespace Modules\Yasna\Console;

class MakeRouteCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-route';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes a partial route file';



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('Http' . DIRECTORY_SEPARATOR . 'Routes');
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return snake_case($this->argument("class_name"));
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "routes.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }


}