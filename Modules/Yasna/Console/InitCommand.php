<?php

namespace Modules\Yasna\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Symfony\Component\Console\Input\InputOption;

class InitCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initializes Yasna Core with all installed modules.';



    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->line('Initializing a Yasna Project... ');
        $this->deleteCaches();
        $this->deleteCurrentFiles();
        $this->publishModels();
        $this->spreadTraits();
        $this->storeInDatabase();

        if (!$this->option('no-check')) {
            $this->warnNonExecutedMigrations();
        }
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
             ['no-check', null, InputOption::VALUE_NONE, "run without health checks (migrations etc.)"],
             ['details', 'd', InputOption::VALUE_NONE, "express with executive normal details."],
        ];
    }



    /**
     * show warning if non-executed migrations found
     *
     * @return void
     */
    private function warnNonExecutedMigrations()
    {
        if (CheckMigrationCommand::verify()) {
            $this->line("All migrations seems to be fully executed!");
        } else {
            $this->error("Detected Non-Executed Migrations. Run `php artisan yasna:check-migrations` for more information.");
        }
    }



    /**
     * Stores module state in database
     *
     * @return void
     */
    private function storeInDatabase()
    {

        /*-----------------------------------------------
        | Make Setting, if not available already ...
        */
        setting()->new([
             'slug'  => "active_modules",
             'title' => trans('manage::statue.setting'),
        ]);

        /*-----------------------------------------------
        | Save Current Module State ...
        */
        $current_module_state = implode("|", module()->list());
        setting('active_modules')->setValue($current_module_state);
        $this->info("Active modules saved into database.");
        return;
    }



    /**
     * Delete caches
     *
     * @return void
     */
    private function deleteCaches()
    {
        Artisan::call("cache:clear");
        $this->line('Caches Cleared!');
    }



    /**
     * Delete current files
     *
     * @return void
     */
    private function deleteCurrentFiles()
    {
        $path = app_path('Models');

        if (!is_dir($path)) {
            mkdir($path);
        }

        $files = glob($path . DIRECTORY_SEPARATOR . '*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }


        $this->info("app/Models directory is made ready.");
    }



    /**
     * Gets Active modules with their parents, and their parents of parents, and their parents of parents of parents...
     *
     * @return array
     */
    private function getActiveModulesWithParents()
    {
        $list         = module()->activeModules();
        $more_parents = true;

        while ($more_parents) {
            $more_parents = false;

            foreach ($list as $item) {
                if (!module($item)->name) {
                    continue;
                }
                $parents = module($item)->getAllParentModules();
                if (is_array($parents)) {
                    foreach ($parents as $parent) {
                        if (!in_array($parent, $list)) {
                            $more_parents = true;
                            $list[]       = $parent;
                        }
                    }
                }
            }
        }

        setting('active_modules')->setCustomValue(implode('|', $list));
        return array_unique($list);
    }



    /**
     * Publish models
     *
     * @return void
     */
    private function publishModels()
    {
        $published = "";
        $failed    = "";
        $done      = 0;

        foreach ($this->getActiveModulesWithParents() as $module_name) {
            $module = module($module_name);
            if (!$module->name) {
                $this->error("Error occurred while publishing models: Invalid Module Name: $module_name");
                continue;
            }

            foreach ($module->models() as $model) {
                $this->makeModelFile($model, $module->getNamespace("Entities\\$model"));
                $done++;
                if ($done) {
                    $published .= ", $model";
                } else {
                    $failed .= ", $model";
                }
            }
        }

        if (!$this->option("details")) {
            $this->info("Models Published.");
            return;
        }

        /*-----------------------------------------------
        | Feedback ...
        */
        if ($published) {
            $this->info("Published:" . ltrim($published, ','));
        }
        if ($failed) {
            $this->warn("Failed to Publish:" . ltrim($published, ','));
        }
    }



    /**
     * make a model file, extending it from the original in-module model class.
     *
     * @param string $model_name
     * @param string $extender
     *
     * @return void
     */
    private function makeModelFile($model_name, $extender)
    {
        $path = app_path('Models') . DIRECTORY_SEPARATOR . "$model_name.php";

        $file = fopen($path, 'w', true);
        fwrite($file, "<?php ");
        fwrite($file, "namespace App\\Models; ");
        fwrite($file, LINE_BREAK);
        fwrite($file, LINE_BREAK);
        fwrite($file, "class $model_name extends $extender ");
        fwrite($file, LINE_BREAK);

        fwrite($file, "{");

        fwrite($file, LINE_BREAK);
        $this->writeFieldList($extender, $file);

        fwrite($file, "}");
        fclose($file);
    }



    /**
     * write field list in the published model class.
     *
     * @param string   $model_name
     * @param resource $file
     *
     * @return void
     */
    private function writeFieldList($model_namespace, $file)
    {
        $model      = new $model_namespace();
        $table_name = $model->getTable();
        $array      = Schema::getColumnListing($table_name);

        fwrite($file, LINE_BREAK . TAB . 'protected $fields_array = [');

        foreach ($array as $item) {
            fwrite($file, LINE_BREAK . TAB . TAB . "'$item',");
        }

        fwrite($file, LINE_BREAK . TAB . '];');
    }



    /**
     * Spread Traits
     *
     * @return void
     */
    private function spreadTraits()
    {
        foreach (service("yasna:traits")->read() as $service) {
            $this->injectTrait($service['to'], $service['trait']);
        }

        $this->info("Traits published.");
    }



    /**
     * inject a trait into a class.
     *
     * @param string $model_name
     * @param string $trait
     *
     * @return void
     */
    private function injectTrait($model_name, $trait)
    {
        $model_name  = studly_case($model_name);
        $file_path   = app_path('Models') . DIRECTORY_SEPARATOR . "$model_name.php";
        $file_exists = file_exists($file_path);

        if (!$file_exists) {
            $this->warn("Detected a disabled required module: didn't find model $model_name");
            return;
        }

        $file_content = file_get_contents($file_path);
        $line         = "use $trait;";
        $file_content = preg_replace("/{/", "{" . LINE_BREAK . TAB . $line, $file_content, 1);

        $wrote = file_put_contents($file_path, $file_content);

        if ($wrote and !$this->option("details")) {
            return;
        }
        elseif ($wrote) {
            $this->info("Added $trait to model $model_name.");
        } else {
            $this->warn("Failed to add $trait to model $model_name!");
        }
    }
}
