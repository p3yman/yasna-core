<?php


namespace Modules\Yasna\Console;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Modules\Yasna\Services\ModuleHelper;
use Nwidart\Modules\Support\Stub;
use Symfony\Component\Console\Input\InputArgument;

abstract class YasnaMakerCommand extends Command
{
    /**
     * keep the module name requested by the user
     *
     * @var string
     */
    protected $module_name;

    /**
     * keep the module instance requested by the user
     *
     * @var ModuleHelper
     */
    protected $module;

    /**
     * keep the class name requested by the user
     *
     * @var string
     */
    protected $class_name;

    /**
     * keep the filesystem injected dependency
     *
     * @var FileSystem
     */
    protected $filesystem;



    /**
     * makeRuleCommand constructor.
     *
     * @param Filesystem|null $filesystem
     */
    public function __construct(Filesystem $filesystem = null)
    {
        $this->filesystem = $filesystem;

        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->purifier();
        $this->customHandle();
        $this->makeFolder();
        $this->makeFile();
    }



    /**
     * purify input
     *
     * @return void
     */
    protected function purifier()
    {
        $this->class_name  = $this->argument('class_name');
        $this->module_name = $this->argument('module_name');

        if (!$this->class_name or !$this->module_name) {
            $this->error('Pattern: ' . $this->name . ' [class_name] [module_name]. Both are required.');
            die();
        }

        $this->class_name  = $this->getFileName();
        $this->module_name = studly_case($this->module_name);

        $this->module = module($this->module_name);

        if ($this->module->isNotValid()) {
            $this->error("Invalid module name");
            die();
        }
    }



    /**
     * handle the additional process
     *
     * @return void
     */
    protected function customHandle()
    {

    }



    /**
     * get the target folder os path
     *
     * @return string
     */
    abstract protected function getFolderPath();



    /**
     * define the file name
     *
     * @return string
     */
    abstract protected function getFileName();



    /**
     * get stub replacement
     *
     * @return array
     */
    abstract protected function getReplacement();



    /**
     * get stub file name
     *
     * @return  string
     */
    abstract protected function getStubName();



    /**
     * make the required folder if not exist already
     *
     * @return void
     */
    protected function makeFolder()
    {
        $folders = explode_not_empty(DIRECTORY_SEPARATOR, $this->getFolderPath());
        $path    = '';
        foreach ($folders as $folder) {
            $path .= DIRECTORY_SEPARATOR . $folder;
            if (!is_dir($path)) {
                mkdir($path);
                $this->line("Made folder $path");
            }
        }
    }



    /**
     * make the requested file
     *
     * @return void
     */
    protected function makeFile()
    {
        $path = $this->getFolderPath() . DIRECTORY_SEPARATOR . $this->getFileName() . '.php';

        if (file_exists($path)) {
            $this->error("Already Exists: $path");
            die();
        }

        $this->filesystem->put($path, $this->getStubContent());
        $this->info("Created: $path");
    }



    /**
     * get content from stub file for creating requested file
     *
     * @return string
     */
    protected function getStubContent()
    {
        $base_path = $this->getBasePath();

        Stub::setBasePath($base_path);

        $stub = new Stub("/" . $this->getStubName(), $this->getReplacement());


        return $stub->render();
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['class_name', InputArgument::OPTIONAL, 'Request Class Name'],
             ['module_name', InputArgument::OPTIONAL, 'Module Name'],
        ];
    }



    /**
     * get child class name
     *
     * @return string
     */
    private function getBasePath()
    {
        $base_path = get_called_class();
        $base_path = str_before($base_path, 'Console');

        $base_path = str_replace('\\', '/', $base_path) . "Console" . DIRECTORY_SEPARATOR . "stubs";

        return base_path($base_path);
    }

}
