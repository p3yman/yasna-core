<?php

namespace Modules\Yasna\Console;

class MakeTestCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make the uniform test classes in the chosen module.';

    /**
     * keep the type of test requested by the user
     *
     * @var string
     */
    protected $test_type;



    /**
     * @inheritDoc
     */
    protected function customHandle()
    {
        $this->askType();
    }



    /**
     * ask the type of test
     *
     * @return void
     */
    protected function askType()
    {
        $test_type = ["Feature", "Unit"];

        $this->test_type = $this->choice("Test type?", $test_type, 0);

        $this->line("Let's use $this->test_type.");

    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        if ($this->test_type == "Feature") {
            return "test-feature.stub";
        } else {
            return "test-unit.stub";
        }
    }



    /**
     * get the folder path.
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Tests" . DIRECTORY_SEPARATOR . $this->test_type);
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Test')) {
            return $file_name . "Test";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
             "TYPE"   => $this->test_type,
        ];
    }
}
