<?php

namespace Modules\Yasna\Console;

use Illuminate\Support\Facades\View;
use Symfony\Component\Console\Input\InputArgument;

class MakeBlade extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-blade {full_blade_address}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes an empty blade file, with the fully qualified name.';

    /**
     * keep the address requested by the user
     *
     * @var string
     */
    protected $address;

    /**
     * keep the array of folders
     *
     * @var array
     */
    protected $folders_array;

    /**
     * keep the name of blade file
     *
     * @var string
     */
    protected $blade_name;

    /**
     * keep the base folder
     *
     * @var string
     */
    protected $base_folder;



    /**
     * validate the address format
     *
     * @return bool
     */
    protected function purifier()
    {

        $this->receiveAddress();
        $this->parseAddress();
        if (!str_contains($this->address, '::')) {
            $this->error("Invalid Address Format. Use Module::folder.blade pattern.");
            die();
        }

        if (View::exists($this->address)) {
            $this->error("The blade already exists!");
            die();
        }

        if (module()->isNotDefined($this->module_name)) {
            $this->error("Module '$this->module_name' is not defined.");
            die();
        }

        return true;
    }



    /**
     * get the target folder os path
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return module($this->module_name)
                  ->getPath("Resources" . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR)
             . implode(DIRECTORY_SEPARATOR, $this->folders_array);
    }



    /**
     * parse the entered address by user
     *
     * @return void
     */
    protected function parseAddress()
    {
        $this->module_name       = studly_case(str_before($this->address, "::"));
        $folder_and_blade_string = str_after($this->address, "::");
        $folder_and_blade_array  = explode('.', $folder_and_blade_string);
        $this->blade_name        = array_last($folder_and_blade_array);
        $this->class_name        = $this->blade_name . '.blade';
        $this->folders_array     = array_remove($folder_and_blade_array, $this->blade_name);
    }



    /**
     * get blade address from user
     *
     * @return void
     */
    protected function receiveAddress()
    {
        $this->address = $this->argument('blade-address');
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['blade-address', InputArgument::REQUIRED, 'Fully-Qualified Blade File Address'],
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return $this->class_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "blade.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [];
    }
}
