<?php

namespace Modules\Yasna\Console;

class MakeModelTraitCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-trait';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes a Model Trait, in the specified module.';



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath('Entities' . DIRECTORY_SEPARATOR . 'Traits');
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Trait')) {
            return $file_name . "Trait";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "trait.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }


}
