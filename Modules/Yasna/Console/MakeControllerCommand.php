<?php

namespace Modules\Yasna\Console;

class MakeControllerCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a controller class.';

    /**
     * keep the value of the desired version
     *
     * @var int
     */
    protected $version;

    /**
     * check the request is api or not
     *
     * @var bool
     */
    protected $is_api;



    /**
     * @inheritDoc
     */
    protected function customHandle()
    {
        $this->askIsApi();
        if ($this->is_api) {
            $this->askVersion();
        }
    }



    /**
     * ask this request is for Api via a dialogue
     *
     * @return void
     */
    protected function askIsApi()
    {
        $allowed_answers = ["No", "Yes"];
        $answer          = $this->choice("Is it for an API endpoint?", $allowed_answers, 1);

        $this->line("Let's use $answer.");

        $is_api = boolval($answer == "Yes");

        $this->is_api = $is_api;
    }



    /**
     * Ask the requested version via a dialogue
     *
     * @return void
     */
    protected function askVersion()
    {
        $suggest = $this->discoverCurrentVersion();
        $version = $this->ask("Desired Version (Probably $suggest)?");

        if (is_null($version)) {
            $version = $suggest;
        }

        while (!is_numeric($version) or $version < 1) {
            $version = $this->ask("Something greater than 1 please.");
            if (is_null($version)) {
                $version = $suggest;
            }
        }

        $this->version = $version;
    }



    /**
     * discover a possible version based on the file system
     *
     * @return int
     */
    protected function discoverCurrentVersion(): int
    {
        $parent_path = $this->getFolderPath();
        if (!file_exists(($parent_path))) {
            return 1;
        }

        $subs = scandir($parent_path);
        $max  = 1;
        foreach ($subs as $sub) {
            $version = (int)str_after($sub, "V");
            $max     = max($version, $max);
        }

        return $max;
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE"  => $this->module_name,
             "CLASS"   => $this->class_name,
             "VERSION" => "V" . $this->version,
        ];
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Controller')) {
            return $file_name . "Controller";
        }
        return $file_name;
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        if ($this->is_api) {
            return "api-controller.stub";
        }
        return "controller.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        $parent_path = $this->module->getPath("Http" . DIRECTORY_SEPARATOR . "Controllers");
        if ($this->version != null) {
            return $parent_path . DIRECTORY_SEPARATOR . "V" . $this->version;
        }
        return $parent_path;
    }
}
