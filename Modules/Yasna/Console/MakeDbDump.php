<?php

namespace Modules\Yasna\Console;

use Carbon\Carbon;
use Chumper\Zipper\Zipper;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Spatie\DbDumper\Compressors\GzipCompressor;
use Spatie\DbDumper\Databases\MySql as Dumper;
use Symfony\Component\Console\Input\InputOption;

class MakeDbDump extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'yasna:dump-db
                            {--m=* : The mode of dump file generating. The values can be "full" for all tables, "inclusive" for only specific tables & "exclusive" for all tables except some one. Default = full}
                            {--t=* : The name of tables. For passing multiple table name you have to use multiple independent --t=table_name pair, there is no problem for using an option more than one time . This is just necessary when your --m option is not in "full" mode. }
                            {--c=* : The name of laravel database connection. You can choose connections with "MySQL", "PostgreSQL", "SQLite" & "MongoDB" drivers. Default = mysql}
                            {--n=* : The name of output file. Default = DATABASE_NAME in .env}
                            {--p=* : The absolute path of saving dump file directory without ending "/". Default = PRIVATE_FOLDER in .env place at storage path}
                            {--f=* : Format of output file. You can use "sql", "zip" or "gz". Default = sql}';
    protected $mode;
    protected $default_path;
    protected $file_name;
    protected $file_path;
    protected $file_type;
    protected $db_connection;
    protected $db_host;
    protected $db_port;
    protected $db_name;
    protected $db_tables;
    protected $db_user;
    protected $db_password;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for dumping back-end database to local file. You can customize your actions using command parameters. Please be sure that in server side you have installed "zip" & "gzip" library dependencies & also have "mysqldump", "pg_dump", "sqlite3", "mongodump". Your database drivers, zip & gzip php extensions also must be included. enjoy it :)';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Validate input arguments.
     *
     * @return boolean
     */
    public function validation()
    {
        // @TODO add input validation for passed params values.
        return true;
    }



    /**
     * Get command parameters & preparing them form later actions.
     *
     * @return boolean
     */
    protected function setParams()
    {
        // @TODO add support for ->addExtraOption('mysql_dump_param') if needed. User can have more database advanced control when driver type is mysql.

        $default_folder = config('yasna.backup_folder');

        if (!$default_folder) {
            $this->info("Please update artisan's config cache and try again.");
            return false;
        }

        if (!File::exists(storage_path($default_folder))) {
            File::makeDirectory(storage_path($default_folder), 0777, true, true);
            $this->default_path = storage_path($default_folder);
        } else {
            $this->default_path = storage_path($default_folder);
        }

        if ($this->option('m')) {
            $this->mode = $this->option('m')[0];
        } else {
            $this->mode = "full";
        }

        if ($this->option('c')) {
            $this->db_connection = $this->option('c')[0];
        } else {
            $this->db_connection = "mysql";
        }

        if ($this->option('t')) {
            $this->db_tables = $this->option('t');
        }

        if ($this->option('n')) {
            $this->file_name = $this->option('n')[0];
        } else {
            $this->file_name = config('database.connections.' . $this->db_connection . '.database');
        }

        if ($this->option('p')) {
            $this->file_path = $this->option('p')[0];
        } else {
            $this->file_path = $this->default_path;
        }

        if ($this->option('f')) {
            $this->file_type = $this->option('f')[0];
        } else {
            $this->file_type = "sql";
        }
        $now               = Carbon::now('Asia/Tehran')->format('Y-m-d-H-i-s');
        $this->file_name   = $this->file_name . '-' . $now;
        $this->db_host     = config('database.connections.' . $this->db_connection . '.host');
        $this->db_port     = config('database.connections.' . $this->db_connection . '.port');
        $this->db_name     = config('database.connections.' . $this->db_connection . '.database');
        $this->db_user     = config('database.connections.' . $this->db_connection . '.username');
        $this->db_password = config('database.connections.' . $this->db_connection . '.password');
        return true;
    }



    /**
     * Get the console command options.
     *
     * @return boolean
     */
    protected function dumpDb()
    {
        try {
            /*---------------------------------------------------------------------------------------------------
            | if file type is zip, first we dump in .sql & then zip it so we will use .sql logic and just process
            | output for zip generation at the end
            */
            $file_type_overwritten = false;
            $dumper_class          = Dumper::create();
            $dumper_class
                 ->setHost($this->db_host)
                 ->setPort($this->db_port)
                 ->setDbName($this->db_name)
                 ->setUserName($this->db_user)
                 ->setPassword($this->db_password)
            ;
            if ($this->mode == "inclusive") {
                $dumper_class->includeTables($this->db_tables);
            }
            if ($this->mode == "exclusive") {
                $dumper_class->excludeTables($this->db_tables);
            }
            if ($this->file_type == "gz") {
                $dumper_class->useCompressor(new GzipCompressor());
            }
            /*----------------------
            | overwrite when is zip.
            */
            if ($this->file_type == "zip") {
                $this->file_type = "sql";
                $file_type_overwritten = true;
            }
            $dumper_class->dumpToFile($this->file_path . '/' . $this->file_name . '.' . $this->file_type);
            /*------------------------------------------------
            | turn it back to original type & do final process
            */
            if ($file_type_overwritten) {
                $this->file_type = "zip";
                if (file_exists($this->file_path . '/' . $this->file_name . '.sql')) {
                    try {
                        $zipper = new Zipper;
                        $zipper->make($this->file_path . '/' . $this->file_name . '.zip')
                               ->add(glob($this->file_path . '/' . $this->file_name . '.sql'))
                               ->close()
                        ;
                        unlink($this->file_path . '/' . $this->file_name . '.sql');
                    } catch (Exception $e) {
                        $this->info("Error: $e.");
                    }
                }
            }
        } catch (Exception $e) {
            $this->info("Error: $e.");
        }
    }



    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        /*-----------------------------------------------
        | Preparations ...
        */
        if (!$this->validation()) {
            return;
        }
        $this->setParams();

        /*-----------------------------------------------
        | Actions ...
        */
        $this->dumpDb();
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }



    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
             ['m', null, InputOption::VALUE_OPTIONAL, 'Dump generation mode.', null],
             ['t', null, InputOption::VALUE_OPTIONAL, 'Dump mode related tables.', null],
             ['c', null, InputOption::VALUE_OPTIONAL, 'Name of laravel database connection.', null],
             ['n', null, InputOption::VALUE_OPTIONAL, 'Output file name.', null],
             ['p', null, InputOption::VALUE_OPTIONAL, 'Output file absolute path.', null],
             ['f', null, InputOption::VALUE_OPTIONAL, 'Output file type.', null],
        ];
    }
}
