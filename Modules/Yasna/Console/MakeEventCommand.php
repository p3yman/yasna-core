<?php

namespace Modules\Yasna\Console;

class MakeEventCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-event';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Makes an event, compatible with the rest of architecture.';



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Events");
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return studly_case($this->argument("class_name"));
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "event.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }
}
