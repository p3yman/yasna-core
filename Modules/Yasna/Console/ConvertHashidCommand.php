<?php

namespace Modules\Yasna\Console;

use Illuminate\Console\Command;

class ConvertHashidCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:hashid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert hashid to id or another way.';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yasna:hashid {hashid : Enter hashid or id to convert}';

    protected $hashid;



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->purifyInput();
        $this->returnValue();
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['hashid', InputArgument::REQUIRED, '1'],
        ];
    }



    /**
     * Purify inputs of the console command.
     *
     * @return void
     */
    protected function purifyInput()
    {
        $this->hashid = $this->argument('hashid');
    }



    /**
     * Convert hashid to id or another way.
     *
     * @return void
     */
    private function returnValue()
    {
        $feedback = hashid($this->hashid);

        if ($feedback) {
            $this->info($feedback);
        } else {
            $this->warn("this hashid is not acceptable!");
        }
    }
}
