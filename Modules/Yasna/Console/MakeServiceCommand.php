<?php

namespace Modules\Yasna\Console;

class MakeServiceCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-service';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Make a general service class, in the module's Services folder";



    /**
     * @inheritDoc
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Services");
    }



    /**
     * @inheritDoc
     */
    protected function getFileName()
    {
        return studly_case($this->argument("class_name"));
    }



    /**
     * @inheritDoc
     */
    protected function getStubName()
    {
        return "service.stub";
    }



    /**
     * @inheritDoc
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
             "CLASS"  => $this->getFileName(),
        ];
    }

}
