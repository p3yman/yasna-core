<?php

namespace Modules\Yasna\Events;

use GuzzleHttp\Psr7\Request;
use Modules\Yasna\Services\YasnaEvent;

class UserLoggedIn extends YasnaEvent
{
    public $model_name = 'User';
    /**
     * @var Request
     */
    public $request;



    /**
     * UserLoggedIn constructor.
     *
     * @param bool $model
     */
    public function __construct($model = false)
    {
        $this->request = request();
        parent::__construct($model);
    }
}
