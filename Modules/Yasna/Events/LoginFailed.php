<?php

namespace Modules\Yasna\Events;

use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Modules\Yasna\Services\YasnaEvent;

class LoginFailed extends YasnaEvent
{
    use SerializesModels;

    protected $user;
    /**
     * @var Request
     */
    public $request;



    /**
     * LoginFailed constructor.
     */
    public function __construct()
    {
        $this->request  = request();

        $this->chalk();
    }
}
