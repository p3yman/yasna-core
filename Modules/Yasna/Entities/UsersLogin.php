<?php

namespace Modules\Yasna\Entities;

use App\Models\User;
use Modules\Yasna\Entities\Traits\UsersLoginElectorTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersLogin extends YasnaModel
{
    use SoftDeletes;
    use UsersLoginElectorTrait;



    /**
     * get relationship with user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }



    /**
     * get Ip resource.
     *
     * @return string
     */
    protected function getIpResource()
    {
        return $this->user_ip;
    }



    /**
     * get WasValid resource.
     *
     * @return boolean
     */
    protected function getWasValidResource()
    {
        return $this->was_valid;
    }


}
