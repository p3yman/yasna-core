<?php


namespace Modules\Yasna\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Entities\Traits\DeveloperTrait;
use Modules\Yasna\Entities\Traits\AuthorizationTrait;
use Modules\Yasna\Entities\Traits\UserFormTrait;
use Modules\Yasna\Entities\Traits\UserPreferencesTrait;
use Modules\Yasna\Entities\Traits\UserResourcesTrait;
use Modules\Yasna\Entities\Traits\UserTokenClaimsTrait;
use Modules\Yasna\Entities\Traits\UserUsernameTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends YasnaModel implements AuthenticatableContract, CanResetPasswordContract, JWTSubject
{
    use SoftDeletes;
    use AuthorizationTrait;
    use Authenticatable;
    use CanResetPassword;
    use DeveloperTrait;
    use UserPreferencesTrait;
    use UserUsernameTrait;
    use UserFormTrait;
    use UserTokenClaimsTrait;
    use UserResourcesTrait;

    protected $guarded = ['status'];
    protected $hidden  = ['password'];
    protected $casts   = [
         'meta'                  => "array",
         'preferences'           => "array",
         'password_force_change' => 'boolean',
         'marriage_date'         => 'datetime',
         'birth_date'            => 'datetime',

    ];



    /**
     * get a user by their `id`/`hashid`, with minimal fields and resource
     *
     * @param string|int $handle
     *
     * @return \App\Models\User
     */
    public static function quickFind($handle)
    {
        $id = hashid_number($handle);

        if (!$id) {
            return user(-1);
        }

        return user()->select(["id", "name_first", "name_last"])->where("id", $id)->firstOrNew([]);
    }



    /*
    |--------------------------------------------------------------------------
    | Assessors
    |--------------------------------------------------------------------------
    |
    */
    /**
     * get full_name attribute.
     * @param string $original
     *
     * @return string
     */
    public function getFullNameAttribute($original)
    {
        if ($original) {
            return $original;
        }

        return trim($this->name_first . ' ' . $this->name_last);
    }



    public function getManageLinkAttribute()
    {
        if ($this->exists) {
            //TODO: Profile Link here
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Stators
    |--------------------------------------------------------------------------
    |
    */
    //public function preference($slug)
    //{
    //	$this->spreadMeta();
    //	$preferences = array_normalize($this->preferences, [
    //		'max_rows_per_page' => "50",
    //	]);
    //
    //	return $preferences[ $slug ];
    //}


    public function getSignatureAttribute()
    {
        return md5($this->id . $this->created_at . $this->updated_at . $this->deleted_at);
    }



    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->exists and $this->id == user()->id;
    }



    /**
     * @return bool
     */
    public function isNotLoggedIn()
    {
        return !$this->isLoggedIn();
    }
}
