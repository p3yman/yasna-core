<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 11/18/18
 * Time: 12:55 PM
 */

namespace Modules\Yasna\Entities\Traits;


use App\Models\UsersLogin;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Schema\Builder;

trait UserLoginHistoryTrait
{
    /**
     * return relationship with UserLogin
     *
     * @return Builder
     */
    public function logins()
    {
        return $this->hasMany(UsersLogin::class, 'user_id', 'id')
                    ->orderBy('id', 'desc')
             ;
    }



    /**
     * find latest login of user
     *
     * @return UsersLogin
     */
    public function latestLogin()
    {
        return $this->logins()->first();
    }



    /**
     * find one before of latest login
     *
     * @return UsersLogin
     */
    public function lastLogin()
    {
        $logins = $this->logins()->limit(2)->get();

        return $logins->last();
    }



    /**
     * find x last logins
     *
     * @param int $counts
     *
     * @return Collection
     */
    public function lastLogins(int $counts = 5)
    {
        $logins = $this->logins()->limit($counts)->get();

        return $logins;
    }
}
