<?php

namespace Modules\Yasna\Entities\Traits;

trait UserResourcesTrait
{
    /**
     * boot UserResourcesTrait
     *
     * @return void
     */
    public static function bootUserResourcesTrait()
    {
        static::addDirectResources('*');
    }



    /**
     * get a minimal resource, suitable for the purpose of `_by` fields
     *
     * @return array
     */
    public function toMinimalResource()
    {
        return $this->toResource(["full_name"]);
    }



    /**
     * get FullName resource.
     *
     * @return string
     */
    protected function getFullNameResource()
    {
        return $this->full_name;
    }



    /**
     * get Password resource.
     *
     * @return null
     */
    protected function getPasswordResource()
    {
        return null;
    }



    /**
     * get CacheRoles resource.
     *
     * @return null
     */
    protected function getCacheRolesResource()
    {
        return null;
    }

    /**
     * get array of resources involved in ContactDetails resource group.
     *
     * @return array
     */
    protected function getContactDetailsResourceGroup()
    {
        return [
            "full_name",
            "email",
            "mobile",
            "tel_emergency",
            "tel",
            "home_address",
            "postal_code"
        ];
        //TODO: Missing divisional data!
    }

}
