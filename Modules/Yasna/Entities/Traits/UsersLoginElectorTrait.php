<?php

namespace Modules\Yasna\Entities\Traits;

use Carbon\Carbon;

trait UsersLoginElectorTrait
{
    /**
     * apply the criteria elector
     *
     * @param string $criteria
     */
    protected function electorCriteria($criteria)
    {
        if ($criteria == "valid") {
            $this->elector()->where("was_valid", 1);
        }

        if ($criteria == "invalid") {
            $this->elector()->where("was_valid", 0);
        }
    }



    /**
     * apply the elector for the beginning of time
     *
     * @param Carbon|string $date
     */
    protected function electorFrom($date)
    {
        $this->elector()->whereDate("created_at", ">=", $date);
    }



    /**
     * apply the elector for the end of time
     *
     * @param Carbon|string $date
     */
    protected function electorTo($date)
    {
        $this->elector()->whereDate("created_at", "<=", $date);
    }



    /**
     * apply the elector for the user
     *
     * @param int $user_id
     */
    protected function electorUser($user_id)
    {
        $this->electorFieldId($user_id, "user_id");
    }
}
