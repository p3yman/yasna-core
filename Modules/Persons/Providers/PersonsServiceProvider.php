<?php

namespace Modules\Persons\Providers;

use Modules\Persons\Http\Endpoints\V1\LoginEndpoint;
use Modules\Persons\Http\Endpoints\V1\LoginFormEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class PersonsServiceProvider
 *
 * @package Modules\Persons\Providers
 */
class PersonsServiceProvider extends YasnaProvider
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerHooks();
        $this->registerEndpoints();
    }



    /**
     * register Hooks
     *
     * @return void
     */
    private function registerHooks()
    {
        hook("persons::permits")->register("permit definable for each rule", [
             "role"  => "The slug of the role, to be loaded by the permit.",
             "title" => "The localed title of the permit.",
        ], true);
    }



    /**
     * register Endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        endpoint()->register(LoginFormEndpoint::class);
        endpoint()->register(LoginEndpoint::class);
    }

}
