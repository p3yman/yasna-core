<?php
return [
     'roles' => [
          "manager"  => "Manager",
          "super"    => "Superadmin",
          "customer" => "Customer",
     ],
];
