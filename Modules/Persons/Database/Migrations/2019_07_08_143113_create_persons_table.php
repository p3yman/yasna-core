<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("parent_id")->index();
            $table->string("locale", 5)->index();

            $table->boolean("is_legal")->default(0);

            $table->string("username")->nullable()->index();
            $table->string("code_melli")->nullable()->index();
            $table->timestamp("code_melli_confirmed_at")->nullable()->index();
            $table->string("email")->nullable()->index();
            $table->timestamp("email_confirmed_at")->nullable()->index();
            $table->string("mobile")->nullable()->index();
            $table->timestamp("mobile_confirmed_at")->nullable()->index();
            $table->string("passport")->nullable()->index();
            $table->timestamp("passport_confirmed_at")->nullable()->index();
            $table->string("postal_code")->nullable()->index();
            $table->timestamp("postal_code_confirmed_at")->nullable()->index();
            $table->string("birth_no")->nullable()->index();

            $table->string("name_title")->nullable();
            $table->string("name_first")->nullable();
            $table->string("name_middle")->nullable();
            $table->string("name_last")->nullable();
            $table->string("name_full")->nullable()->index();
            $table->string("name_alias")->nullable();
            $table->string("name_father")->nullable();
            $table->string("name_mother")->nullable();

            $table->string("registered_no")->nullable()->comment("for legals");
            $table->timestamp("registered_at")->nullable()->comment("for legals");
            $table->string("rrk_no")->nullable()->comment("for legals");
            $table->timestamp("rrk_at")->nullable()->comment("for legals");

            $table->string("type")->nullable()->comment("for legals");
            $table->string("gender")->nullable();

            $table->unsignedInteger("continent_id")->default(0)->index();
            $table->unsignedInteger("country_id")->default(0)->index();
            $table->unsignedInteger("province_id")->default(0)->index();
            $table->unsignedInteger("city_id")->default(0)->index();
            $table->unsignedInteger("neighbourhood_id")->default(0)->index();

            $table->timestamp("birth_at")->nullable()->index();
            $table->unsignedInteger("birth_continent_id")->default(0)->index();
            $table->unsignedInteger("birth_country_id")->default(0)->index();
            $table->unsignedInteger("birth_province_id")->default(0)->index();
            $table->unsignedInteger("birth_city_id")->default(0)->index();
            $table->unsignedInteger("birth_neighbourhood_id")->default(0)->index();

            $table->timestamp("death_at")->nullable()->index();
            $table->unsignedInteger("death_continent_id")->default(0)->index();
            $table->unsignedInteger("death_country_id")->default(0)->index();
            $table->unsignedInteger("death_province_id")->default(0)->index();
            $table->unsignedInteger("death_city_id")->default(0)->index();
            $table->unsignedInteger("death_neighbourhood_id")->default(0)->index();

            $table->string("marital_status")->nullable()->index();
            $table->timestamp("married_at")->nullable();
            $table->timestamp("divorced_at")->nullable();

            $table->string("edu_level")->nullable();
            $table->string("edu_field")->nullable();
            $table->unsignedInteger("edu_continent_id")->default(0)->index();
            $table->unsignedInteger("edu_country_id")->default(0)->index();
            $table->unsignedInteger("edu_province_id")->default(0)->index();
            $table->unsignedInteger("edu_city_id")->default(0)->index();
            $table->unsignedInteger("edu_neighbourhood_id")->default(0)->index();
            $table->timestamp("edu_graduated_at")->nullable()->index();

            $table->string("pw")->nullable();
            $table->boolean("pw_force_change")->default(0);
            $table->timestamp("pw_expires_at")->nullable();
            $table->string("otp")->nullable();
            $table->timestamp("otp_expires_at")->nullable();

            $table->string("remember_token")->nullable();
            $table->string("reset_token")->nullable();

            $table->string("status")->nullable()->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons');
    }
}
