<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsPermitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons_permits', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("user_id")->index();
            $table->unsignedInteger("organization_id")->index();
            $table->unsignedInteger("role_id")->index();

            $table->unsignedInteger("level");
            $table->text("permits")->nullable();
            $table->string("key")->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->foreign('user_id')->references('id')->on('persons')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('persons')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('persons_roles')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_permits');
    }
}
