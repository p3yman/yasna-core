<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons_levels', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("role_id")->index();
            $table->integer("level")->index();
            $table->string("locales");

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->foreign('role_id')->references('id')->on('persons_roles')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_levels');
    }
}
