<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons_roles', function (Blueprint $table) {
            $table->increments('id');

            $table->string("slug")->index();
            $table->string("locales");
            $table->boolean("is_admin")->default(0);
            $table->tinyInteger("level_default")->default(100);
            $table->tinyInteger("level_min_active")->default(100);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_roles');
    }
}
