<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonsRolesPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons_roles_pivots', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("organization_id")->index();
            $table->unsignedInteger("role_id")->index();

            $table->integer("level_default")->nullable();
            $table->integer("level_min_active")->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->foreign('organization_id')->references('id')->on('persons')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('persons_roles')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_roles_pivots');
    }
}
