<?php

namespace Modules\Persons\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedSomeUsers(50);
        $this->seedSomeOrganizations(10);
        $this->seedSomeRoles(10);
    }



    /**
     * seed some users into the `persons` table.
     *
     * @param int $total
     *
     * @return void
     */
    private function seedSomeUsers($total)
    {
        if (user()->count() >= $total) {
            return;
        }

        for ($i = 1; $i <= $total; $i++) {
            $this->seedOneUser();
        }
    }



    /**
     * seed some organizations into the `persons` table.
     *
     * @param int $total
     *
     * @return void
     */
    private function seedSomeOrganizations($total)
    {
        if (model('organization')->count() >= $total) {
            return;
        }

        for ($i = 1; $i <= $total; $i++) {
            $this->seedOneOrganization();
        }
    }



    /**
     * seed one user into the `persons` table.
     *
     * @return void
     */
    private function seedOneUser()
    {
        $name_first = dummy()::persianName();
        $name_last  = dummy()::persianFamily();
        $username   = snake_case(camel_case(str_slug($name_first . "_" . $name_last . "_" . str_random(3))));

        $data = [
             "is_legal"                 => "0",
             "username"                 => $username,
             "code_melli"               => dummy()::codeMelli(),
             "code_melli_confirmed_at"  => rand(0, 1) ? dummy()::pastDate() : null,
             "email"                    => $username . "@" . dummy()::englishWord() . ".com",
             "email_confirmed_at"       => rand(0, 1) ? dummy()::pastDate() : null,
             "mobile"                   => dummy()::mobile(),
             "mobile_confirmed_at"      => rand(0, 1) ? dummy()::pastDate() : null,
             "passport"                 => rand(0, 1) ? dummy()::digit(10) : null,
             "passport_confirmed_at"    => rand(0, 1) ? dummy()::pastDate() : null,
             "postal_code"              => dummy()::digit(10),
             "postal_code_confirmed_at" => rand(0, 1) ? dummy()::pastDate() : null,
             "birth_no"                 => rand(0, 1) ? dummy()::digit() : null,
             "name_title"               => dummy()::persianNameTitle(30),
             "name_first"               => $name_first,
             "name_last"                => $name_last,
             "name_full"                => "",
             "name_alias"               => dummy()::chance(10) ? dummy()::persianName() : null,
             "name_father"              => dummy()::persianName(),
             "name_mother"              => dummy()::persianName(),
             "pw"                       => Hash::make(1),
             "pw_force_change"          => dummy()::chance(10) ? 1 : 0,
        ];

        //TODO: add the missing fields.

        model('user')->batchSave($data);
    }



    /**
     * seed one user into the `persons` table.
     *
     * @return void
     */
    private function seedOneOrganization()
    {
        $name = dummy()::persianName() . " " . dummy()::persianName() . " " . dummy()::persianName();

        $data = [
             "is_legal"                 => "1",
             "code_melli"               => dummy()::codeMelli(),
             "code_melli_confirmed_at"  => rand(0, 1) ? dummy()::pastDate() : null,
             "email"                    => dummy()::email(),
             "email_confirmed_at"       => rand(0, 1) ? dummy()::pastDate() : null,
             "postal_code"              => dummy()::digit(10),
             "postal_code_confirmed_at" => rand(0, 1) ? dummy()::pastDate() : null,
             "name_first"               => $name,
             "name_alias"               => dummy()::chance(10) ? dummy()::persianName() : null,
             "registered_no"            => dummy()::digit(15),
             "registered_at"            => rand(0, 1) ? dummy()::pastDate() : null,
             "rrk_no"                   => dummy()::digit(5),
             "rrk_at"                   => rand(0, 1) ? dummy()::pastDate() : null,
        ];

        //TODO: add the missing fields.

        model('user')->batchSave($data);
    }



    /**
     * seed some roles.
     *
     * @param int $total
     *
     * @return void
     */
    private function seedSomeRoles($total)
    {
        if (role()->where("slug", "like", "dummy-%")->exists()) {
            return;
        }

        for ($i = 1; $i <= $total; $i++) {
            $data = [
                 "slug"             => str_slug("dummy-$i"),
                 "titles"           => [
                      "fa" => dummy()::persianWord(),
                      "en" => dummy()::englishWord(),
                 ],
                 "locales"          => "fa,en",
                 "is_admin"         => dummy()::chance(20),
                 "level_default"    => rand(1, 10) * 10,
                 "level_min_active" => rand(1, 10) * 10,
            ];

            $role = role()->batchSave($data);
            model('organization')->inRandomOrder()->first()->attachRole($role);
        }
    }
}
