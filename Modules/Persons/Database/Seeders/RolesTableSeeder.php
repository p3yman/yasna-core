<?php

namespace Modules\Persons\Database\Seeders;

use App\Models\Organization;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /** @var Organization */
    private $root_org = null;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->root_org = organization()::root();

        $this->seedOneRole("manager", true);
        $this->seedOneRole("super", true);
        $this->seedOneRole("customer", false);
    }



    /**
     * seed one role.
     *
     * @param string $slug
     * @param bool   $is_admin
     *
     * @return void
     */
    private function seedOneRole(string $slug, bool $is_admin)
    {
        if (role($slug)->exists) {
            return;
        }

        role()->batchSave([
             "slug"             => $slug,
             "locales"          => "fa,en",
             "is_admin"         => $is_admin,
             "level_default"    => 100,
             "level_min_active" => 100,
             'titles'           => [
                  "fa" => trans("persons::seed.roles.$slug", [], "fa"),
                  "en" => trans("persons::seed.roles.$slug", [], "en"),
             ],
        ]);

        $this->root_org->attachRole($slug);
    }
}
