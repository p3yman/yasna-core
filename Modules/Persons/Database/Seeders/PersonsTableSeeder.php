<?php

namespace Modules\Persons\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PersonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedDeveloper();
    }



    /**
     * seed developer account.
     *
     * @return void
     */
    private function seedDeveloper()
    {
        if (user()->count()) {
            return;
        }

        $user = user()->batchSave([
             "is_legal"                => "0",
             "username"                => "dev",
             "code_melli"              => "4608968882",
             "code_melli_confirmed_at" => now(),
             "email"                   => "dev@yasnateam.com",
             "email_confirmed_at"      => now(),
             "name_first"              => trans_safe("yasna::seeders.dev_first_name"),
             "name_last"               => trans_safe("yasna::seeders.dev_last_name"),
             'pw'                      => '$2y$10$U53pXZEMjLfkoSccigVdQunR/FLcw4K0up5YSVTeHaDBwQLQmHhrC',
        ]);

        $user->as('manager')->assign();
        $user->as("super")->assign();
        $user->markAsDeveloper();
    }
}
