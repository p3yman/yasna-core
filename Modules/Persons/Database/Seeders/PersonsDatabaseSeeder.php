<?php

namespace Modules\Persons\Database\Seeders;

use Illuminate\Database\Seeder;

class PersonsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(PersonsTableSeeder::class);
    }
}
