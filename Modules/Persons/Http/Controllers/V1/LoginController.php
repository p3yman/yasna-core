<?php

namespace Modules\Persons\Http\Controllers\V1;

use Modules\Persons\FormBinders\LoginForm;
use Modules\Persons\Http\Requests\V1\LoginRequest;
use Modules\Yasna\Services\YasnaApiController;

class LoginController extends YasnaApiController
{
    /**
     * fetch the login form.
     *
     * @return array
     */
    public function loginForm()
    {
        return $this->success([], [
             "form" => (new LoginForm())->toArray(),
        ]);
    }



    /**
     * Attempts to login a user with the given credentials.
     *
     * @param LoginRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function login(LoginRequest $request)
    {
        return $this->success([
             "access_token" => $request->token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ]);
    }

}
