<?php

namespace Modules\Persons\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;


class LoginRequest extends YasnaFormRequest
{
    /**
     * keep JWT token, in case of successful login.
     *
     * @var null
     */
    public $token = null;

    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "username"    => 'required',
             'pw'          => 'required',
             'recaptcha'   => debugMode() ? '' : 'required|captcha',
             "credentials" => $this->token ? "" : "accepted",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->attempt();
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "credentials.accepted" => trans("persons::messages.credentials"),
        ];
    }



    /**
     * manually attempt to login the user
     *
     * @return void
     */
    private function attempt()
    {
        $fields  = user()::usernameFields();
        $builder = user()->whereRaw('1=0');

        foreach ($fields as $field) {
            $builder = $builder->orWhere($field, $this->getData('username'));
        }
        $user = $builder->first();

        if (!$user or !Hash::check($this->getData('pw'), $user->pw)) {
            return;
        }

        $this->setTokenValidityTime();
        $this->token = JWTAuth::fromUser($user);
    }



    /**
     * Sets the token validity time from the settings.
     *
     * @return void
     */
    private function setTokenValidityTime()
    {
        $days = (int)get_setting('token_validity_time');
        if (!$days) {
            $days = 1;
        }

        $minutes = $days * 24 * 60;

        auth('api')->factory()->setTTL($minutes);
    }
}
