<?php

namespace Modules\Persons\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Persons\Http\Controllers\V1\LoginController;

/**
 * @api               {POST}
 *                    /api/modular/v1/persons-login
 *                    Login
 * @apiDescription
 * @apiVersion        1.0.0
 * @apiName           Login
 * @apiGroup          Persons
 * @apiPermission     Guest
 * @apiParam {string} username  the username
 * @apiParam {string} pw        the password
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "access_token":
 *           "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb2R1bGFyXC92MVwvZnJvbnRpZXItbG9naW4iLCJpYXQiOjE1NDk4MTAzMDIsImV4cCI6MTU0OTgxMDkwMiwibmJmIjoxNTQ5ODEwMzAyLCJqdGkiOiJ0MEF6Snl5ZDJyNUtRNEQ1Iiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.oHlljpPtAKwOqouy0ADuRtuj_MLmFx9a4VMiT2nEh-o",
 *           "token_type": "bearer",
 *           "expires_in": 600
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "",
 *      "errors": {
 *      "password": [
 *           [
 *                "The Password field is required."
 *           ]
 *      ]
 *      }
 * }
 * @method LoginController controller()
 */
class LoginEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Login";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Persons\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'LoginController@login';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'status'   => 200,
             'metadata' =>
                  [
                       'authenticated_user' => 'qKXVA',
                  ],
             'results'  =>
                  [
                       'access_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb2R1bGFyXC92MVwvZnJvbnRpZXItbG9naW4iLCJpYXQiOjE1NDk4MTAzMDIsImV4cCI6MTU0OTgxMDkwMiwibmJmIjoxNTQ5ODEwMzAyLCJqdGkiOiJ0MEF6Snl5ZDJyNUtRNEQ1Iiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.oHlljpPtAKwOqouy0ADuRtuj_MLmFx9a4VMiT2nEh-o',
                       'token_type'   => 'bearer',
                       'expires_in'   => 600,
                  ],
        ]);
    }
}
