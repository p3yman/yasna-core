<?php

namespace Modules\Persons\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Persons\Http\Controllers\V1\LoginController;

/**
 * @api               {GET}
 *                    /api/modular/v1/persons-login-form
 *                    Login Form
 * @apiDescription    Login Form
 * @apiVersion        1.0.0
 * @apiName           Login Form
 * @apiGroup          Persons
 * @apiPermission     Guest
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "form": [
 *          {
 *              "component_name": "text",
 *              "order": 1,
 *              "id": "",
 *              "name": "username",
 *              "css": "",
 *              "css_class": "",
 *              "width": "",
 *              "col": "",
 *              "default": "",
 *              "value": "",
 *              "placeholder": "",
 *              "validation": "",
 *              "label": "Username",
 *              "label_desc": "",
 *              "desc": "",
 *              "clone": "",
 *              "size": "",
 *              "char_limit": "",
 *              "prepend": "",
 *              "append": "",
 *              "disabled": false,
 *              "readonly": false,
 *              "no_label": false
 *          },
 *          {
 *              "component_name": "password",
 *              "order": 2,
 *              "id": "",
 *              "name": "pw",
 *              "css": "",
 *              "css_class": "",
 *              "width": "",
 *              "col": "",
 *              "default": "",
 *              "value": "",
 *              "placeholder": "",
 *              "validation": "",
 *              "label": "Password",
 *              "label_desc": "",
 *              "desc": "",
 *              "clone": "",
 *              "size": "",
 *              "char_limit": "",
 *              "prepend": "",
 *              "append": "",
 *              "disabled": false,
 *              "readonly": false,
 *              "no_label": false
 *          },
 *          {
 *              "component_name": "recaptcha",
 *              "order": 3,
 *              "id": "",
 *              "name": "recaptcha",
 *              "css": "",
 *              "css_class": "",
 *              "width": "",
 *              "col": "",
 *              "default": "",
 *              "value": "",
 *              "placeholder": "",
 *              "validation": "",
 *              "label": "",
 *              "label_desc": "",
 *              "desc": "",
 *              "clone": "",
 *              "size": "",
 *              "char_limit": "",
 *              "prepend": "",
 *              "append": "",
 *              "disabled": false,
 *              "readonly": false,
 *              "no_label": false,
 *              "invisible": true,
 *              "compact": false
 *          }
 * ], *      },
 *      "results": []
 * }
 * @method LoginController controller()
 */
class LoginFormEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Login Form";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Persons\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'LoginController@loginForm';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([], [
             'form' => [
                  [
                       "component_name" => "text",
                       "order"          => 1,
                       "id"             => "",
                       "name"           => "username",
                       "css"            => "",
                       "css_class"      => "",
                       "width"          => "",
                       "col"            => "",
                       "default"        => "",
                       "value"          => "",
                       "placeholder"    => "",
                       "validation"     => "",
                       "label"          => "User Name",
                       "label_desc"     => "",
                       "desc"           => "",
                       "clone"          => "",
                       "size"           => "",
                       "char_limit"     => "",
                       "prepend"        => "",
                       "append"         => "",
                       "disabled"       => false,
                       "readonly"       => false,
                       "no_label"       => false,
                  ],
                  [
                       "component_name" => "password",
                       "order"          => 2,
                       "id"             => "",
                       "name"           => "pw",
                       "css"            => "",
                       "css_class"      => "",
                       "width"          => "",
                       "col"            => "",
                       "default"        => "",
                       "value"          => "",
                       "placeholder"    => "",
                       "validation"     => "",
                       "label"          => "Password",
                       "label_desc"     => "",
                       "desc"           => "",
                       "clone"          => "",
                       "size"           => "",
                       "char_limit"     => "",
                       "prepend"        => "",
                       "append"         => "",
                       "disabled"       => false,
                       "readonly"       => false,
                       "no_label"       => false,
                  ],
                  [
                       "component_name" => "recaptcha",
                       "order"          => 3,
                       "id"             => "",
                       "name"           => "recaptcha",
                       "css"            => "",
                       "css_class"      => "",
                       "width"          => "",
                       "col"            => "",
                       "default"        => "",
                       "value"          => "",
                       "placeholder"    => "",
                       "validation"     => "",
                       "label"          => "",
                       "label_desc"     => "",
                       "desc"           => "",
                       "clone"          => "",
                       "size"           => "",
                       "char_limit"     => "",
                       "prepend"        => "",
                       "append"         => "",
                       "disabled"       => false,
                       "readonly"       => false,
                       "no_label"       => false,
                       "invisible"      => true,
                       "compact"        => false,
                  ],

             ],
        ]);
    }
}
