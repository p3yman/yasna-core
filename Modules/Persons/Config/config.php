<?php

return [
     'name'            => 'Persons',
     'username_fields' => env("USERNAME_FIELDS", [
          'email',
          'username',
          'mobile',
     ]),
];
