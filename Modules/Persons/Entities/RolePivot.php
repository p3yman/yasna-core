<?php

namespace Modules\Persons\Entities;

use App\Models\Organization;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Role;
use App\Models\RolePivot as RuntimeSelf;

class RolePivot extends YasnaModel
{
    use SoftDeletes;

    protected $table = "persons_roles_pivots";



    /**
     * get an instance, based on the given org and role.
     *
     * @param int|string|Organization $org_id
     * @param int|string|Role         $role_id
     *
     * @return \App\Models\RolePivot
     */
    public static function getInstance($org_id, $role_id)
    {
        if (!is_int($org_id)) {
            $org_id = organization($org_id)->id;
        }
        if (!is_int($role_id)) {
            $role_id = role($role_id);
        }

        if (!$org_id or !$role_id) {
            return new RuntimeSelf();
        }

        return RuntimeSelf::where("organization_id", $org_id)->where("role_id", $role_id)->firstOrNew([]);
    }



    /**
     * get the relationship instance to the organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class, "organization_id");
    }



    /**
     * get relationship instance to the role model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, "role_id");
    }



    /**
     * get Level_default accessor.
     *
     * @param int|null $original
     *
     * @return int
     */
    public function getLevelDefaultAttribute($original)
    {
        if (is_null($original)) {
            $original = $this->role->level_default;
        }

        return $original;
    }



    /**
     * get level_min_active accessor.
     *
     * @param int|null $original
     *
     * @return int
     */
    public function getLevelMinActiveAttribute($original)
    {
        if (is_null($original)) {
            $original = $this->role->level_min_active;
        }

        return $original;
    }
}
