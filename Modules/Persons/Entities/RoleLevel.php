<?php

namespace Modules\Persons\Entities;

use App\Models\Role;
use Modules\Yasna\Services\ModelTraits\YasnaLocaleTitlesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleLevel extends YasnaModel
{
    use SoftDeletes;
    use YasnaLocaleTitlesTrait;



    /**
     * get relationship instance to the role model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, "role_id");
    }
}
