<?php

namespace Modules\Persons\Entities;

use App\Models\Organization;
use App\Models\RoleLevel;
use Modules\Persons\Entities\Traits\RolePermitsTrait;
use Modules\Yasna\Services\ModelTraits\YasnaLocaleTitlesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends YasnaModel
{
    use SoftDeletes;
    use YasnaLocaleTitlesTrait;
    use RolePermitsTrait;

    protected $table = "persons_roles";



    /**
     * get relationship instance to the levels, defined for this role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function levels()
    {
        return $this->hasMany(RoleLevel::class, "role_id");
    }



    /**
     * get the organizations relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organizations()
    {
        return $this
             ->belongsToMany(Organization::class, "persons_roles_pivots", "role_id")
             ->withPivot('level_default', 'level_min_active', 'deleted_at')
             ->withTimestamps()
             ;
    }
}
