<?php

namespace Modules\Persons\Entities;

use Illuminate\Auth\Authenticatable;
use Modules\Persons\Entities\Abstracts\PersonAbstract;
use Modules\Persons\Entities\Traits\UserAuthTrait;
use Modules\Persons\Entities\Traits\UserDeveloperTrait;
use Modules\Persons\Entities\Traits\UsernamesTrait;
use Modules\Persons\Entities\Traits\UserResourcesTrait;
use Modules\Persons\Entities\Traits\UserTokenClaimsTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends PersonAbstract implements AuthenticatableContract, JWTSubject
{
    use Authenticatable;
    use UserDeveloperTrait;
    use UsernamesTrait;
    use UserTokenClaimsTrait;
    use UserAuthTrait;
    use UserResourcesTrait;



    /**
     * get a user by their `id`/`hashid`, with minimal fields and resource
     *
     * @param string|int $handle
     *
     * @return \App\Models\User
     */
    public static function quickFind($handle)
    {
        $id = hashid_number($handle);

        if (!$id) {
            return user(-1);
        }

        return user()->select(["id", "name_full"])->where("id", $id)->firstOrNew([]);
    }



    /**
     * set full_name attribute.
     *
     * @return void
     */
    public function setNameFullAttribute()
    {
        $array = [
             $this->attributes["name_title"],
             $this->attributes["name_first"],
             $this->attributes["name_last"],
        ];

        $this->attributes['name_full'] = implode(" ", $array);
    }
}
