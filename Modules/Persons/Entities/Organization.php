<?php

namespace Modules\Persons\Entities;

use Modules\Persons\Entities\Abstracts\PersonAbstract;
use Modules\Persons\Entities\Traits\OrgRolesTrait;
use Modules\Persons\Entities\Traits\OrgRootTrait;
use Modules\Persons\Entities\Traits\OrgUsersTrait;

class Organization extends PersonAbstract
{
    use OrgUsersTrait;
    use OrgRolesTrait;
    use OrgRootTrait;
}
