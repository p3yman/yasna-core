<?php

namespace Modules\Persons\Entities\Abstracts;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Persons\Entities\Traits\PersonResourcesTrait;
use Modules\Persons\Entities\Traits\PersonScopeTrait;
use Modules\Yasna\Services\YasnaModel;


class PersonAbstract extends YasnaModel
{
    use SoftDeletes;
    use PersonScopeTrait;
    use PersonResourcesTrait;

    protected $table = "persons";



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "preferences",
        ];
    }

}
