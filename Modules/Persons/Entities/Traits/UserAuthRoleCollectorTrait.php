<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\UserPermit;
use Illuminate\Database\Eloquent\Collection;

trait UserAuthRoleCollectorTrait
{
    /**
     * get roles collection.
     *
     * @return Collection
     */
    public function getRolesCollection()
    {
        $collection = $this->getRolesRawCollection();
        $collection = $this->applyMinLevelToRolesCollection($collection);
        $collection = $this->applyMaxLevelToRolesCollection($collection);
        $collection = $this->applyRolesToRolesCollection($collection);
        $collection = $this->applyDisabledToRolesCollection($collection);

        return $collection;
    }



    /**
     * get roles raw collection, only to filter a user in a specific organization.
     *
     * @return Collection
     */
    private function getRolesRawCollection()
    {
        if (!$this->no_cache and cache()->has($this->getRolesCacheName())) {
            return cache()->get($this->getRolesCacheName());
        }

        $collection = UserPermit::withTrashed()
                                ->where("user_id", $this->id)
                                ->where("organization_id", $this->getChainedOrg()->id)
                                ->get()
        ;

        cache()->forever($this->getRolesCacheName(), $collection);

        return $collection;
    }



    /**
     * apply minimum level limit to the roles collection.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    private function applyMinLevelToRolesCollection(Collection $collection)
    {
        if (!is_null($this->min_level)) {
            return $collection->where("level", '>=', $this->min_level);
        }

        return $collection->filter(function ($model) {
            return $model->level_min_active <= $model->level;
        });
    }



    /**
     * apply maximum level limit to the roles collection.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    private function applyMaxLevelToRolesCollection(Collection $collection)
    {
        if (is_null($this->max_level)) {
            return $collection;
        }

        return $collection->where("level", '<=', $this->max_level);
    }



    /**
     * apply roles to the roles collection.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    private function applyRolesToRolesCollection(Collection $collection)
    {
        return $collection->whereIn("role_id", $this->getChainedRolesIds());
    }



    /**
     * apply filtering disabled roles to the roles collection.
     *
     * @param Collection $collection
     *
     * @return Collection
     */
    private function applyDisabledToRolesCollection(Collection $collection)
    {
        if ($this->with_disableds) {
            return $collection;
        }

        return $collection->where('deleted_at', null);
    }



    /**
     * get an array of role ids, stored in the chain.
     *
     * @return array
     */
    private function getChainedRolesIds()
    {
        return array_map(function ($item) {
            return $item->id;
        }, $this->getChainedRoles());
    }
}

