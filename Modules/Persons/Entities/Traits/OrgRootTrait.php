<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\Organization;

trait OrgRootTrait
{
    /**
     * get the root organization
     *
     * @return Organization
     */
    public static function root()
    {
        $root = organization()->where("username", "root")->first();

        if (!$root) {
            return static::redefineRoot();
        }

        return $root;
    }



    /**
     * redefine the root organization.
     *
     * @return Organization
     */
    public static function redefineRoot()
    {
        $data = [
             "is_legal"   => 1,
             "username"   => "root",
             "name_alias" => "root",
        ];

        return organization()->batchSave($data);
    }
}
