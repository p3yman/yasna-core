<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\UserPermit;


trait UserAuthRoleAttachmentTrait
{
    /**
     * assign a role to the user in a specific organization. role and org are read from chain.
     *
     * @param int|null $level    : specify the level of the user. send null to load from the defaults.
     * @param bool     $override : specify if any existing record should be overridden. True by default.
     *
     * @return int
     */
    public function assign($level = null, $override = true)
    {
        $pivot = $this->getRolePivotFromChain();
        if (!$pivot->exists) {
            return false;
        }

        if (is_null($level)) {
            $level = $pivot->level_default;
        }

        $instance = UserPermit::saveInstance([
             "user_id"    => $this->id,
             "pivot"      => $pivot,
             "level"      => $level,
             "deleted_at" => null,
             "deleted_by" => 0,
        ], false, $override);

        return $instance->exists;
    }



    /**
     * resign a role from the user in a specific organization. role and org are read from chain.
     *
     * @return bool
     */
    public function resign()
    {
        $pivot    = $this->getRolePivotFromChain();
        $instance = UserPermit::getInstance($this->id, $pivot->organization_id, $pivot->role_id);

        return $instance->hardDelete();
    }



    /**
     * set the level of an existing permit. role and org are read from chain.
     *
     * @param int $level
     *
     * @return bool
     */
    public function setLevel(int $level)
    {
        $pivot    = $this->getRolePivotFromChain();
        $instance = UserPermit::saveInstance([
             "user_id" => $this->id,
             "pivot"   => $pivot,
             "level"   => $level,
        ]);

        return $instance->exists;
    }



    /**
     * enable an existing soft-deleted permit. role and org are read from chain.
     *
     * @return bool
     */
    public function enable()
    {
        $pivot    = $this->getRolePivotFromChain();
        $instance = UserPermit::saveInstance([
             "user_id"    => $this->id,
             "pivot"      => $pivot,
             "deleted_at" => null,
             "deleted_by" => 0,
        ]);

        return $instance->exists;
    }



    /**
     * disable an existing soft-deleted permit. role and org are read from chain.
     *
     * @return bool
     */
    public function disable()
    {
        $pivot    = $this->getRolePivotFromChain();
        $instance = UserPermit::saveInstance([
             "user_id"    => $this->id,
             "pivot"      => $pivot,
             "deleted_at" => now(),
             "deleted_by" => user()->id,
        ]);

        return $instance->exists;
    }
}
