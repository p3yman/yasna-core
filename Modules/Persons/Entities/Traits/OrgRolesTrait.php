<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property Collection $roles
 */
trait OrgRolesTrait
{
    /**
     * get the roles relationship.
     *
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this
             ->belongsToMany(Role::class, "persons_roles_pivots", "organization_id", "role_id")
             ->withPivot('level_default', 'level_min_active', 'deleted_at')
             ->withTimestamps()
             ;
    }



    /**
     * get an array of roles ids.
     *
     * @return array
     */
    public function rolesIds()
    {
        return $this->roles->pluck('id')->toArray();
    }



    /**
     * attach role to the organization
     *
     * @param Role|int|string $handler          : accepts the model instance, id, hashid or slug.
     * @param null|int        $level_default    : null means to be inherited from the role instance.
     * @param null|int        $level_min_active : null means to be inherited from the role instance.
     *
     * @return bool
     */
    public function attachRole($handler, $level_default = null, $level_min_active = null): bool
    {
        $role = role($handler);
        if (!$role->exists) {
            return false;
        }

        $this->roles()->detach($role->id);
        $this->roles()->attach($role->id, [
             "level_default"    => $level_default,
             "level_min_active" => $level_min_active,
        ])
        ;

        //TODO: Raise and event!
        return true;
    }



    /**
     * attach a number of roles to the organization
     *
     * @param array    $handlers         : array of model handlers (id, hashid, slug or instance).
     * @param null|int $level_default    : null means to be inherited from the role instance.
     * @param null|int $level_min_active : null means to be inherited from the role instance.
     *
     * @return int
     */
    public function attachRoles($handlers, $level_default = null, $level_min_active = null): int
    {
        $attached = 0;

        foreach ((array)$handlers as $handler) {
            $attached += $this->attachRole($handler, $level_default, $level_min_active);
        }

        return $attached;
    }



    /**
     * detach role from the organization
     *
     * @param Role|int|string $handler : accepts the model instance, id, hashid or slug.
     *
     * @return bool
     */
    public function detachRole($handler): bool
    {
        $role = role($handler);
        if (!$role->exists) {
            return false;
        }

        $done = $this->roles()->detach($role->id);

        //TODO: Raise and event!
        return $done;
    }



    /**
     * detach a number of roles from the organization
     *
     * @param array $handlers : array of model handlers (id, hashid, slug or instance).
     *
     * @return bool
     */
    public function detachRoles($handlers): bool
    {
        $detached = 0;

        foreach ((array)$handlers as $handler) {
            $detached += $this->detachRole($handler);
        }

        return $detached;
    }



    /**
     * find and return the role instance.
     *
     * @param int|string|Role $handle
     *
     * @return Role
     */
    public function findRole($handle)
    {
        $role = role($handle);

        if (!$role->exists or !in_array($role->id, $this->rolesIds())) {
            return role();
        }

        return $role;
    }



    /**
     * get admin roles of the organization.
     *
     * @return BelongsToMany
     */
    public function adminRoles()
    {
        return $this->roles()->where('is_admin', 1);
    }



    /**
     * get an array of admin role instances.
     *
     * @return array
     */
    public function adminRolesInstances()
    {
        $array = [];

        foreach ($this->adminRoles()->get() as $model) {
            $array[] = $model;
        }

        return $array;
    }



    /**
     * get an array of admin roles slugs.
     *
     * @return array
     */
    public function adminRolesSlugs()
    {
        return $this->adminRoles()->get()->pluck('slug')->toArray();
    }
}
