<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\Organization;

trait UserAuthOrgQueryTrait
{
    /**
     * get the organizations relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organizations()
    {
        return $this
             ->belongsToMany(Organization::class, "persons_permits", "user_id", "organization_id")
             ->withPivot('level', 'permits', 'key', 'deleted_at')
             ->withTimestamps()
             ;
    }
}
