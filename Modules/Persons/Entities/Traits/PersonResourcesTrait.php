<?php

namespace Modules\Persons\Entities\Traits;

trait PersonResourcesTrait
{
    /**
     * get FullName resource.
     *
     * @return string
     */
    protected function getFullNameResource()
    {
        return $this->getAttribute("name_full");
    }

}
