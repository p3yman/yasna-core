<?php

namespace Modules\Persons\Entities\Traits;

trait RolePermitsTrait
{
    public function permits()
    {
        hook("persons::permitsHandler")->handle();

        $array = hook("persons::permits")->get();

        return $array;
    }
}
