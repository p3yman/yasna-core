<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\UserPermit;
use Modules\Persons\Exceptions\MultipleRolesException;

trait UserAuthPermitTrait
{

    /** @var null|string */
    protected $cached_permits = null;



    /**
     * get permits, based on the roles query passed by chain methods.
     *
     * @return string
     */
    public function permits()
    {
        if ($this->cached_permits) {
            return $this->cached_permits;
        }

        $collection = $this->getRolesCollection();
        $array      = $collection->pluck("permits")->toArray();

        return $this->cached_permits = implode(" ", $array);
    }



    /**
     * set permits to one role (only one role at a time).
     *
     * @param string|array $permits
     *
     * @return bool
     * @throws MultipleRolesException
     */
    public function setPermit($permits)
    {
        $pivot = $this->getRolePivotFromChain();
        if (!$pivot->exists) {
            return false;
        }

        $instance = UserPermit::saveInstance([
             "user_id" => $this->id,
             "pivot"   => $pivot,
             "permits" => $permits,
        ]);

        return $instance->exists;
    }



    /**
     * add a permit to one role (only one role at a time).
     *
     * @param string $permit
     *
     * @return bool
     * @throws MultipleRolesException
     */
    public function addPermit($permit)
    {
        $pivot = $this->getRolePivotFromChain();
        if (!$pivot->exists) {
            return false;
        }

        $instance = UserPermit::saveInstance([
             "user_id" => $this->id,
             "pivot"   => $pivot,
             "permits" => "+" . $permit,
        ]);

        return $instance->exists;
    }



    /**
     * remove a permit from one role (only one role at a time).
     *
     * @param string $permit
     *
     * @return bool
     * @throws MultipleRolesException
     */
    public function removePermit($permit)
    {
        $pivot = $this->getRolePivotFromChain();
        if (!$pivot->exists) {
            return false;
        }

        $instance = UserPermit::saveInstance([
             "user_id" => $this->id,
             "pivot"   => $pivot,
             "permits" => "-" . $permit,
        ]);

        return $instance->exists;
    }
}
