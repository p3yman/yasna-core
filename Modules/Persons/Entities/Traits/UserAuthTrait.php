<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\Organization;
use App\Models\RolePivot;
use Modules\Persons\Exceptions\MultipleRolesException;

/**
 * @property int $id
 */
trait UserAuthTrait
{
    use UserAuthChainTrait;
    use UserAuthRoleAttachmentTrait;
    use UserAuthRolesTrait;
    use UserAuthRoleCollectorTrait;
    use UserAuthOrgQueryTrait;
    use UserAuthPermitTrait;
    use UserAuthCanTrait;

    /** @var null|Organization */
    protected $at_org = null;

    /** @var array */
    protected $as_role = [];

    /** @var null|int */
    protected $max_level = null;

    /** @var null|int */
    protected $min_level = null;

    /** @var bool */
    protected $with_disableds = false;

    /** @var bool */
    protected $no_cache = false;



    /**
     * get chained organization.
     *
     * @return Organization
     */
    protected function getChainedOrg()
    {
        if (is_null($this->at_org)) {
            return organization()::root();
        }

        return $this->at_org;
    }



    /**
     * get chained role.
     *
     * @return array
     */
    protected function getChainedRoles()
    {
        if (!$this->as_role) {
            $this->asAdmin();
        }

        return $this->as_role;
    }



    /**
     * get cache name.
     *
     * @return string
     */
    protected function getRolesCacheName()
    {
        $user = $this->hashid;
        $org  = $this->getChainedOrg()->hashid;

        return "user-org-$user-$org";
    }



    /**
     * get one (and only one) role pivot from the chain.
     *
     * @return RolePivot
     * @throws MultipleRolesException
     */
    protected function getRolePivotFromChain()
    {
        $org   = $this->getChainedOrg();
        $roles = $this->getChainedRoles();
        if (count($roles) > 1) {
            throw (new MultipleRolesException($roles));
        }
        $role = array_first($roles);

        return RolePivot::getInstance($org->id, $role->id);
    }
}
