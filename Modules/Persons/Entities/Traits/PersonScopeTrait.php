<?php

namespace Modules\Persons\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait PersonScopeTrait
{
    /**
     * boot the trait
     *
     * @return void
     */
    public static function bootPersonScopeTrait()
    {
        static::addGlobalScope("type", function (Builder $builder) {
            $called = static::className();

            if ($called == "User") {
                $builder->where("is_legal", 0);
            } elseif ($called == "Organization") {
                $builder->where("is_legal", 1);
            }
        });
    }
}
