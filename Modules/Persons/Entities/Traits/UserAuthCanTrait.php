<?php

namespace Modules\Persons\Entities\Traits;

trait UserAuthCanTrait
{
    /**
     * specify if the user has access to something (considering everything in cache).
     *
     * @param string $permit
     *
     * @return bool
     */
    public function can($permit = "*")
    {
        if ($this->isDeveloper()) {
            return true;
        }

        if ((clone $this)->as($this->getChainedOrg())->isSuperadmin() and not_in_array($permit, ['dev', 'developer'])) {
            return true;
        }

        $permit = str_replace(".*", SPACE, $permit);
        $permit = str_replace("*", SPACE, $permit);

        return str_contains($this->permits(), $permit);
    }



    /**
     * specify if the user do not have access to something (considering everything in cache).
     *
     * @param string $permit
     *
     * @return bool
     */
    public function cannot($permit = "*")
    {
        return !$this->can($permit);
    }



    /**
     * specify if the user has access to any of the specified roles (considering everything in cache).
     *
     * @param array ...$permits
     *
     * @return bool
     */
    public function canAny(... $permits)
    {
        if (!isset($permits[0])) {
            return false;
        }

        if (is_array($permits[0])) {
            $permits = $permits[0];
        }

        foreach ($permits as $permit) {
            if ($this->can($permit)) {
                return true;
            }
        }

        return false;
    }



    /**
     * specify if the user has access to all of the specified roles (considering everything in cache).
     *
     * @param array ...$permits
     *
     * @return bool
     */
    public function canAll(... $permits)
    {
        if (!isset($permits[0])) {
            return false;
        }

        if (is_array($permits[0])) {
            $permits = $permits[0];
        }

        foreach ($permits as $permit) {
            if ($this->cannot($permit)) {
                return false;
            }
        }

        return true;
    }
}
