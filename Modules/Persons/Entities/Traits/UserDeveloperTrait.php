<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\User;

trait UserDeveloperTrait
{
    /**
     * remove developer account.
     *
     * @return bool
     */
    public static function removeDeveloperAccount()
    {
        return setting('app_key')->hardDelete();
    }



    /**
     * find an retrieve the developer account.
     *
     * @return User
     */
    public static function findDeveloper()
    {
        return user(static::getDeveloperId());
    }



    /**
     * get the id of the developer account (if any).
     *
     * @return int|null
     */
    public static function getDeveloperId($no_cache = false)
    {
        $setting = setting('app_key')->noCache($no_cache);

        if ($setting->isNotDefined()) {
            return false;
        } else {
            return safe_decrypt($setting->gain(), 0);
        }
    }



    /**
     * mark the current user as developer.
     *
     * @return bool
     */
    public function markAsDeveloper()
    {
        $setting = setting('app_key');

        if ($setting->isNotDefined()) {
            return $this->makeSettingRow();
        }

        return setting('app_key')->setValue(encrypt($this->id));
    }



    /**
     * check if the current user is marked as the developer.
     *
     * @return bool
     */
    public function isDeveloper()
    {
        return boolval($this->id == $this->getDeveloperId());
    }



    /**
     * check if the current user is NOT marked as the developer.
     *
     * @return bool
     */
    public function isNotDeveloper()
    {
        return !$this->isDeveloper();
    }



    /**
     * make a setting row to define developer account.
     *
     * @return bool
     */
    private function makeSettingRow()
    {
        return setting()->new([
             "slug"          => "app_key",
             "title"         => trans_safe("yasna::seeders.app_key"),
             "category"      => "upstream",
             "order"         => "6",
             "data_type"     => "text",
             "default_value" => encrypt($this->id),
        ]);
    }


}
