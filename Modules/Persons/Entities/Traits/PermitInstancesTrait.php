<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\RolePivot;
use App\Models\UserPermit as PublishedUserPermit;

/**
 * @property int $user_id
 * @property int $organization_id
 */
trait PermitInstancesTrait
{
    /**
     * save instance into the pivot.
     *
     * @param array $data
     * @param bool  $should_exist
     * @param bool  $should_override
     *
     * @return \App\Models\UserPermit
     */
    public static function saveInstance(array $data, bool $should_exist = true, bool $should_override = true)
    {
        $data     = static::evaluateSaveData($data);
        $instance = static::getInstance($data['user_id'], $data['organization_id'], $data['role_id']);

        if (!$instance->exists and $should_exist) {
            return new PublishedUserPermit();
        }
        if ($instance->exists and !$should_override) {
            return new PublishedUserPermit();
        }

        return $instance->batchSave($data, ['pivot'])->forgetMyCache();
    }



    /**
     * get an instance, based on the given user, org and role.
     *
     * @param int|null $user_id
     * @param int|null $org_id
     * @param int|null $role_id
     *
     * @return \App\Models\UserPermit
     */
    public static function getInstance(?int $user_id, ?int $org_id, ?int $role_id)
    {
        if (!$user_id or !$org_id or !$role_id) {
            return new PublishedUserPermit();
        }

        $conditions = [
             "user_id"         => $user_id,
             "organization_id" => $org_id,
             "role_id"         => $role_id,
        ];

        //TODO: Checking the lock can be done here.

        return PublishedUserPermit::where($conditions)->withTrashed()->firstOrNew([]);
    }



    /**
     * forget the cache related to this user-org pair.
     *
     * @return $this
     */
    public function forgetMyCache()
    {
        $user = hashid($this->user_id);
        $org  = hashid($this->organization_id);

        cache()->forget("user-org-$user-$org");

        return $this;
    }



    /**
     * evaluate and purify the save data.
     *
     * @param array $data
     *
     * @return array
     */
    private static function evaluateSaveData(array $data): array
    {
        /** @var RolePivot $pivot */
        $pivot = $data['pivot'];

        $data['organization_id'] = $pivot->organization_id;
        $data['role_id']         = $pivot->role_id;

        //TODO: Locking the record can be added here.

        return $data;
    }
}
