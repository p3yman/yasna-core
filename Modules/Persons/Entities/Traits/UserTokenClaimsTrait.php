<?php

namespace Modules\Persons\Entities\Traits;

/**
 * handle everything about JTW token claims
 *
 * @property string $full_name
 */
trait UserTokenClaimsTrait
{
    /**
     * Returns the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }



    /**
     * Returns a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        $array = [];

        foreach ($this->getJWTClaimMethods() as $claim) {
            $array = array_merge($array, $this->$claim());
        }

        return [
            "payload" => $array,
        ];
    }



    /**
     * get the vary basic user token claims
     *
     * @return array
     */
    public function getJWTCustomClaimsBasic()
    {
        return [
             "id"        => $this->hashid,
             "full_name" => $this->full_name,
        ];
    }



    /**
     * get JWT Claim Methods
     *
     * @return array
     */
    private function getJWTClaimMethods()
    {
        return collect($this->methodsArray())
             // Methods should start with `getJWTCustomClaims` followed by at least one character
             ->filter(function (string $method) {
                 return preg_match('/^getJWTCustomClaims(.)/m', $method);
             })->values()
             ->toArray()
             ;
    }

}
