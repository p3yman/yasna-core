<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\Organization;
use App\Models\Role;

trait UserAuthChainTrait
{
    /**
     * set organization in the chain of asking for user's role and permission.
     *
     * @param string|int|Organization $handle
     *
     * @return $this
     */
    public function at($handle)
    {
        $this->at_org = organization($handle);

        return $this;
    }



    /**
     * set role in the chain of asking for user's role and permission.
     *
     * @param string|int|Role $handle
     *
     * @return $this
     */
    public function as ($handle)
    {
        //Special Methods...
        $method = camel_case("as-$handle");
        if ($this->hasMethod($method)) {
            return $this->$method();
        }

        //Normal Situations...
        $this->as_role = [$this->getChainedOrg()->findRole($handle)];

        return $this;
    }



    /**
     * set maximum acceptable level for user's role and permission.
     *
     * @param int $level
     *
     * @return $this;
     */
    public function max(int $level)
    {
        $this->max_level = $level;

        return $this;
    }



    /**
     * set minimum acceptable level for user's role and permission.
     *
     * @param int $level
     *
     * @return $this;
     */
    public function min(int $level)
    {
        $this->min_level = $level;

        return $this;
    }



    /**
     * set the `with_disableds` flag to include disabled roles in checking user's role and permission.
     *
     * @param bool $condition
     *
     * @return $this
     */
    public function withDisableds(bool $condition = true)
    {
        $this->with_disableds = $condition;

        return $this;
    }



    /**
     * set the `no_cache` flag to bypass caches in checking user's role and permission.
     *
     * @param bool $condition
     *
     * @return $this
     */
    public function noCache(bool $condition = true)
    {
        $this->no_cache = $condition;

        return $this;
    }



    /**
     * set the admin roles of the specific organization.
     *
     * @return $this
     */
    public function asAdmin()
    {
        $this->as_role = $this->getChainedOrg()->adminRolesInstances();

        return $this;
    }
}
