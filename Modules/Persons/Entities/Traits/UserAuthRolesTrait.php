<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\Role;
use App\Models\RolePivot;
use Illuminate\Database\Eloquent\Builder;
use Modules\Persons\Entities\UserPermit;

trait UserAuthRolesTrait
{
    /**
     * get the roles relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this
             ->belongsToMany(Role::class, "persons_permits", "user_id", "role_id")
             ->withPivot('level', 'permits', 'key', 'deleted_at')
             ->withTimestamps()
             ;
    }



    /**
     * specify if the user is the holder of the given role in the organization, given in chains.
     *
     * @param int|string|Role|null|array $role_handle
     *
     * @return bool
     */
    public function is($role_handle = null): bool
    {
        if ($role_handle) {
            $this->as($role_handle);
        }

        return $this->getRolesCollection()->isNotEmpty();
    }



    /**
     * specify if the user is not the holder of the given role in the organization, given in chains.
     *
     * @param int|string|Role $role_handle
     *
     * @return bool
     */
    public function isNot($role_handle): bool
    {
        return !$this->is($role_handle);
    }



    /**
     * specify if the user is the holder of any of the given roles in the organization, given in chains.
     *
     * @param array ...$role_handles
     *
     * @return bool
     */
    public function isAnyOf(... $role_handles): bool
    {
        if (isset($role_handles[0]) and is_array($role_handles[0])) {
            $role_handles = $role_handles[0];
        }

        return $this->is($role_handles); // <~~ uses OR by nature!
    }



    /**
     * specify if the user is the holder of all of the given roles in the organization, given in chains.
     *
     * @param array ...$role_handles
     *
     * @return bool
     */
    public function isAllOf(... $role_handles): bool
    {
        if (isset($role_handles[0]) and is_array($role_handles[0])) {
            $role_handles = $role_handles[0];
        }

        foreach ($role_handles as $role_handle) {
            if ($this->isNot($role_handle)) {
                return false;
            }
        }

        return true;
    }



    /**
     * serve as a shortcut to `->is('admin') to specify if the user holds one of the admin roles of the organization.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return dev() or $this->is('admin');
    }



    /**
     * serve as a shortcut to `->is('admin') to specify if the user holds the superadmin role of the organization.
     *
     * @return bool
     */
    public function isSuperadmin()
    {
        return $this->isSuper();
    }



    /**
     * serve as a shortcut to `->is('admin') to specify if the user holds the superadmin role of the organization.
     *
     * @return bool
     */
    public function isSuper()
    {
        return dev() or $this->is('super');
    }
}
