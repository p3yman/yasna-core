<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\User;

trait UsernamesTrait
{

    /**
     * get the name of the username fields.
     *
     * @return array
     */
    public static function usernameFields(): array
    {
        return (array)config("persons.username_fields");
    }



    /**
     * find a user by their username, with support to multiple username fields.
     *
     * @param string $username
     * @param bool   $with_trashed
     *
     * @return User
     */
    public static function findByUsername(string $username, bool $with_trashed = false): User
    {
        $builder = $with_trashed ? User::withTrashed() : user()->newBuilderInstance();

        $builder->where(function ($builder) use ($username) {
            foreach (static::usernameFields() as $field) {
                $builder->orWhere($field, $username);
            }
        });

        return $builder->firstOrNew([]);
    }
}
