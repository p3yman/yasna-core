<?php

namespace Modules\Persons\Entities\Traits;

use App\Models\User;

trait OrgUsersTrait
{
    /**
     * get the users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this
             ->belongsToMany(User::class, "persons_permits", "organization_id", "user_id")
             ->withPivot('level', 'permits', 'key', 'deleted_at')
             ->withTimestamps()
             ;
    }
}
