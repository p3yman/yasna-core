<?php

namespace Modules\Persons\Entities;

use App\Models\Organization;
use App\Models\Role;
use App\Models\RolePivot;
use App\Models\User;
use Modules\Persons\Entities\Traits\PermitInstancesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPermit extends YasnaModel
{
    use SoftDeletes;
    use PermitInstancesTrait;

    protected $table = "persons_permits";



    /**
     * get the relationship instance to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }



    /**
     * get the relationship instance to the organization.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class, "organization_id");
    }



    /**
     * get relationship instance to the role model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class, "role_id");
    }



    /**
     * get relationship instance to the role-org pivot model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pivot()
    {
        return $this->belongsTo(RolePivot::class, "role_id", "role_id")
                    ->where("organization_id", $this->organization_id)
             ;
    }



    /**
     * get level_min_active accessor.
     *
     * @return int
     */
    public function getLevelMinActiveAttribute()
    {
        return $this->pivot->level_min_active;
    }



    /**
     * set permits attribute.
     *
     * @param string|array $value
     *
     * @return void
     */
    public function setPermitsAttribute($value)
    {
        if (is_array($value)) {
            $value = implode(" ", $value);
        }

        if(starts_with($value, "+")) {
            $value = $this->attributes['permits'] . " " . str_after($value, "+");
        }
        if(starts_with($value, "-")) {
            $value = str_replace(str_after($value, "-"), null, $this->attributes['permits']);
        }

        $this->attributes['permits'] = trim($value);
    }
}
