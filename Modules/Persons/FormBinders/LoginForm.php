<?php

namespace Modules\Persons\FormBinders;

use Modules\Forms\Services\FormBinder;

class LoginForm extends FormBinder
{
    public function components()
    {
        $this->add("text")
             ->name("username")
             ->label(trans("validation.attributes.username"))
             ->order(1)
             ->required()
        ;

        $this->add("password")
             ->name("pw")
             ->label(trans("validation.attributes.pw"))
             ->order(2)
             ->required()
        ;

        if (debugMode()) {
            $this->add("recaptcha")
                 ->name("recaptcha")
                 ->invisible()
                 ->order(3)
                 ->required()
            ;
        }
    }
}
