<?php

namespace Modules\Persons\Exceptions;

use Exception;

class MultipleRolesException extends Exception
{

    /**
     * MultipleRolesException constructor.
     *
     * @param array $roles
     */
    public function __construct(array $roles)
    {
        $count = count($roles);

        parent::__construct("One role can be performed at a time. Given $count.");
    }
}
