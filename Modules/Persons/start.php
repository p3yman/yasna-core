<?php

if (!function_exists("org")) {
    /**
     * get an instance of the Organization.
     *
     * @param string|int|\App\Models\Organization $handle
     * @param bool                                $with_trashed
     *
     * @return \App\Models\Organization
     */
    function organization($handle = 0, $with_trashed = false)
    {
        /** @var \App\Models\Organization $org */
        $org = model("organization", $handle, $with_trashed);

        return $org;
    }
}
