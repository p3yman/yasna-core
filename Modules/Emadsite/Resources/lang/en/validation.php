<?php
return [
    "attributes-placeholders" => [
        "message" => "Type your message ...",
    ],

    "javascript-validation" => [
        "name"       => "Please enter a valid \"Name\".",
        "name_first" => "Please enter a valid \"Name\".",
        "name_last"  => "Please enter a valid \"Last Name\".",
        "email"      => "Please enter a valid \"Email\".",
        "text"       => "Please enter a valid \"Text\".",
    ],
];
