<?php
return [
    "option" => [
        "remember" => "Remember Me",
    ],

    "button" => [
        "submit" => "Submit",
        "send"   => "Send",
    ],

    "feed" => [
        "error" => "Error occurred!",
        "wait"  => "Please waite.",
        "done"  => "Done.",
    ],
];
