<?php
return [
    "biography"     => "Biography",
    "news"          => "News",
    "next-post"     => "Next Post",
    "previous-post" => "Previous Post",
    "resume"        => "Resume",
    "contact"       => "Contact",

    "work" => [
        "title" => [
            "plural" => "Works",
        ],
    ],

    "social" => [
        "facebook"  => "Facebook",
        "instagram" => "Instagram",
    ],

    "alert" => [
        "success" => [
            "send-message" => "Your message has been sent successfully.",
        ],
        "error"   => [
            "send-message" => "Unable to send message.",
        ],
    ],

    "contact-reply" => [
        "email-subject" => "Message Reply",
    ],
];
