@extends('emadsite::layouts.inner-plane')

{{-- MAIN PAGE --}}
@php
    $title = $category->titleIn(getLocale());
    Template::appendToPageTitle($title);
    Template::setItemInOpenGraphs('description', $title);
    $noFooter = true;
    $category->spreadMeta();
    $post->spreadMeta();
    $currentPostIndex = $allPosts->search(function ($item) use ($post) {
        return $post->id == $item->id;
    });
    $previousLink = $currentPostIndex ?
        route('emadsite.works.category', [
            'category' => $category->slug,
            'post' => $allPosts->get($currentPostIndex - 1)->hashid
            ]) :
        null;
    $back = $previousLink ? ['link' => $previousLink, 'link_title' => trans('emadsite::general.previous-post')] : [];
    $nextLink = ($currentPostIndex < ($allPosts->count() - 1)) ?
        route('emadsite.works.category', [
            'category' => $category->slug,
            'post' => $allPosts->get($currentPostIndex + 1)->hashid
            ]) :
        null;
    $next = $nextLink ? ['link' => $nextLink, 'link_title' => trans('emadsite::general.next-post')] : [];
@endphp

@section('specific_header_assets')
    <!-- Owl Carousel -->
    {!! Html::style(Module::asset('emadsite:libs/owlcarousel/dist/assets/owl.carousel.min.css')) !!}
    {!! Html::style(Module::asset('emadsite:libs/owlcarousel/dist/assets/owl.theme.default.min.css')) !!}
    
    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/blog-style.min.css')) !!}
@append


@section('main-content')
    <!--Blog Post-->
    {{-- "post_year" and "post_info" are optional properties --}}
    @include('emadsite::blog-page.blog-post.0blog-post',[
        "type" => "blog" ,
        "page_title" => $category->titleIn(getLocale()) ,
        "post_year" => $category->textIn(getLocale()) ,
        "post_src" => $postImage = doc($post->featured_image)->getUrl() ,
        "post_alt" => "post" , 
        "post_info" => [
                           "title" => $post->title ,
                           "material" => $post->technique,
                           "size" => $post->dimensions ,
                           "year" => $post->year ,
                        ] ,
        "post_desc" => $post->text,
        "back" => $back,
        "next" => $next,
    ])
    <!--End Of Blog Post-->
    
    @if($postImage)
        @php Template::setItemInOpenGraphs('image', $postImage); @endphp
    @endif
    
    <!-- Image Bar -->
    @php
        $imageBarData = $allPosts->map(function ($post) use ($category, $posttype) {
            return [
                'image' => doc($post->featured_image)
                    ->posttype($posttype)
                    ->version('100x100')
                    ->getUrl(),
                'src' => route('emadsite.works.category', ['category' => $category->slug, 'post' => $post->hashid])
            ];
        });
    @endphp
    @include('emadsite::blog-page.imagebar.0imagebar',[
        "images" => $imageBarData
    ])
    <!-- End Of Image Bar -->

@endsection


@section('specific_endbody_assets')
    
    <!-- Owl Carousel -->
    {!! Html::script (Module::asset('emadsite:libs/owlcarousel/dist/owl.carousel.min.js')) !!}
    <!-- Slider init js -->
    {!! Html::script (Module::asset('emadsite:js/slider.js')) !!}
@append