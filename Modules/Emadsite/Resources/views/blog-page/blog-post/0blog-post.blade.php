<section class="blog-post {{ $type or "" }}">
    <div class="post">
        <div class="container">
            <div class="main-data">
                <h1 class="main-title">
                    {{ $page_title or "" }}
                </h1>
                @if(isset($post_year) and $post_year)
                    <h2 class="year-title"> ( {{ $post_year or "" }} ) </h2>
                @endif
                <div class="post-image js-img">
                    <img src="{{ $post_src or "" }}" alt="{{ $post_alt }}">
                </div>
                @if(isset($post_info))
                    <div class="post-info">
                        @foreach($post_info as $key => $info)
                            <span>{{ $info or "" }}</span>
                            @if($loop->iteration !== count($post_info))
                                /
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
            @if(isset($news_date))
                <div class="news-date">
                    Posted at
                    {{ $news_date or "" }}
                </div>
            @endif
            <div class="post-description">
                {!! $post_desc or "" !!}
            </div>
        </div>
       {{-- @include('emadsite::layouts.footer.0footer')--}}
        @if(isset($next) and count($next))
        <span class="navigator nav_next">
            <a href="{{ $next['link'] or "" }}" data-toggle="tooltip" title="{{ $next['link_title'] }}" data-placement="bottom">
                <svg version="1.1" id="arrow-next-m" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 59.414 59.414" style="enable-background:new 0 0 59.414 59.414;" xml:space="preserve">
                    <polygon points="15.561,0 14.146,1.414 42.439,29.707 14.146,58 15.561,59.414 45.268,29.707 "/>
                </svg>
            </a>
        </span>
        @endif
        @if(isset($back) and count($back))
        <span class="navigator nav_back">
            <a href="{{ $back['link'] or "" }}" data-toggle="tooltip" title="{{ $back['link_title'] }}" data-placement="bottom">
                <svg version="1.1" id="arrow-back-m" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 59.414 59.414" style="enable-background:new 0 0 59.414 59.414;" xml:space="preserve">
                    <polygon points="45.268,1.414 43.854,0 14.146,29.707 43.854,59.414 45.268,58 16.975,29.707 "/>
                </svg>
            </a>
        </span>
        @endif

    </div>
    <div class="gallery-single js-single">
            <span class="close_icon js-close">
                <img src="{{ Module::asset('emadsite:images/cross-out.svg') }}" alt="cross">
            </span>

        <img src="" alt="" class="img-single">
        {{--<div class="caption"></div>--}}
    </div>
</section>