{{-- footer image bar --}}
<section class="img-bar">
    <div class="img-bar-inner">
        <ul class="img-row owl-carousel">
            @foreach($images as $image)
                <li class="item">
                    <a href="{{ $image['src'] or "#" }}">
                        <img src="{{ $image['image'] }}" alt="{{ $image['alt'] or "" }}">
                    </a>
                </li>
            @endforeach

        </ul>
    </div>
</section>