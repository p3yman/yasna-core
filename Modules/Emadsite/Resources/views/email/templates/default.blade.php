<!DOCTYPE html>
<html lang="en" style="width: 100%; height: 100%;">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mohammad Hossein Emad</title>

</head>
<body style="font-family: Tahoma, Helvetica, Arial; margin: 0; width: 100%; height: 100%;">

<table style="width: 100%;">
    <tr>
        <th style="font-family: Helvetica, Arial;  font-size: 30px; border-bottom: 1px solid #e9e9e9; color: #191919; text-align: center; padding: 20px;"
            width="100%">
            <a href="#" style="text-decoration: none; color: #191919;">
                Mohammad Hossein Emad
            </a>
        </th>
    </tr>
    <tr>
        <td style="font-size: 18px; color: #5e5e5e; padding: 20px; line-height: 1.5;">
            <div style="max-width: 800px; margin-left: auto; margin-right: auto;">
                {!! $mailContent !!}
            </div>
        </td>
    </tr>
    <tr>
        <th style="font-family: Tahome, Helvetica, Arial; font-size: 16px; font-weight: normal; border-top: 1px solid #e9e9e9; border-bottom: 1px solid #e9e9e9; color: #777; text-align: center;"
            width="100%">
            <table style="padding: 10px" width="100%">
                <tr>
                    <td style="border-right: 1px solid #e9e9e9;">
                        <a href="{{ route('emadsite.biography') }}" style="color: #777; text-decoration: none;">Biography</a>
                    </td>
                    <td style="border-right: 1px solid #e9e9e9;">
                        <a href="{{ route('emadsite.works') }}" style="color: #777; text-decoration: none;">Works</a>
                    </td>
                    <td style="border-right: 1px solid #e9e9e9;">
                        <a href="{{ route('emadsite.news.list') }}" style="color: #777; text-decoration: none;">News</a>
                    </td>
                    <td>
                        <a href="{{ route('emadsite.contact') }}" style="color: #777; text-decoration: none;">Contact</a>
                    </td>
                </tr>
            </table>
        </th>
    </tr>
</table>

</body>
</html>