@if ($message)
    <h4>Your Message: </h4>
    <blockquote style="font-family: Tahoma, Helvetica, Arial;"><q>{!! nl2br($message) !!}</q></blockquote>
    <br/>
    <h4>Our Reply: </h4>
@endif
<blockquote style="font-family: Tahoma, Helvetica, Arial;"><q>{!! nl2br($reply) !!}</q></blockquote>
<br/>
