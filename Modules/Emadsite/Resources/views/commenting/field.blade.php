{{-- Making decision about requirenment --}}
@if (array_key_exists($fieldName, $rules) and (array_search('required', $rules[$fieldName]) !== false))
    @php
        $inputClass ='form-required'
    @endphp
@else
    @php
        $inputClass =''
    @endphp
@endif

{{-- Making decision about label --}}
@if(!$fieldInfo['label'])
    @php
        $inputData['label'] = $fieldInfo['label']
    @endphp
@endif

{{-- Making decision about column size --}}
@if(is_numeric($fieldInfo['size'])
    and is_int((int) $fieldInfo['size'])
    and $fieldInfo['size'] <= 12)
    @php
        $inputSize = $fieldInfo['size']
    @endphp
@else
    @php
        $inputSize = 12
    @endphp
@endif

<div class="col-sm-{{ $inputSize }}">
    <label for="{{ $fieldName }}">{{ trans('validation.attributes.' . $fieldName) }}</label>
    @if (Lang::has($transPath = 'emadsite::validation.javascript-validation.' . $fieldName))
        @php $errorValue = trans($transPath) @endphp
    @endif
    @switch($fieldName)
        @case('name')
        @case('name_first')
        @case('name_last')
        <input type="text" name="{{ $fieldName }}" id="{{ $fieldName }}" class="{{ $inputClass }}"
               placeholder="{{ trans('validation.attributes.' . $fieldName) }}" error-value="{{ $errorValue or '' }}">
        @break
        
        @case('email')
        <input type="email" name="{{ $fieldName }}" id="{{ $fieldName }}" class="form-email {{ $inputClass }}"
               placeholder="{{ trans('validation.attributes.' . $fieldName) }}" error-value="{{ $errorValue or '' }}">
        @break
        
        @case('text')
        <textarea name="{{ $fieldName }}" id="{{ $fieldName }}" cols="30" rows="10" class="{{ $inputClass }}"
                  placeholder="{{ trans('emadsite::validation.attributes-placeholders.message') }}"
                  error-value="{{ $errorValue or '' }}"></textarea>
        @break
    @endswitch
</div>
