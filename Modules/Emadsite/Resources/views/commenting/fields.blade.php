{{--to add any input:--}}
{{--    1. add its name to $availableFields--}}
{{--    2. add its block to form with using its size--}}
{{--    3. add its input with useing $inputData of the input--}}

@php
    $availableFields = [
        'name',
        'name_first',
        'name_last',
        'email',
        'text',
    ];
    $inputSize = array_flip($availableFields);
    $inputSize = array_fill_keys($availableFields, 12);
@endphp

@foreach($fields as $fieldName => $fieldInfo)
    @if(!in_array($fieldName, $availableFields))
        @continue
    @endif
    @include('emadsite::commenting.field')
@endforeach