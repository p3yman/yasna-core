{!! Form::open(['url' => route('emadsite.comments.new'), 'class' => 'js', 'id' => 'comment-form']) !!}

<input type="hidden" name="post_id" value="{{ $commentingPost->hashid }}">

@include('emadsite::commenting.fields')

<div class="col-sm-12">
    @include('emadsite::form.feed')
</div>

<div class="col-sm-4 col-sm-offset-8">
    <button id="submit" name="_submit" type="submit">{{ trans('emadsite::form.button.send') }}</button>
</div>


{!! Form::close() !!}


@include('emadsite::form.scripts')