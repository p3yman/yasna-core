<!-- Main Section -->
<main>
    <div class="header-container">
        <div class="inner-container">
            <h1 class="title-big vwfont">{{ Template::siteTitle() }}</h1>
            <div class="line-container">
                <div class="load-line"></div>
            </div>
            <!-- Nav -->
                {{-- Needs "href" and "title" --}}
                @include('emadsite::index-page.main.index-main-nav',[
                    'links' => Template::navBarItems()
                ])
            <!-- !END Nav -->
        </div>
    </div>
    @include('emadsite::layouts.footer.0footer')
</main>
<!-- End Of Main Section -->