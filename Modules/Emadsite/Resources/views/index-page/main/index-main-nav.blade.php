{{-- Main page navigation --}}
<nav class="nav-center">
    @foreach($links as $link)
        <a href="{{ $link['href'] or "" }}">{{ $link['title'] or ""}}</a>
    @endforeach
</nav>