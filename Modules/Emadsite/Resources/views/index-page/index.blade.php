@extends('emadsite::layouts.template')

{{-- MAIN PAGE --}}

@section('specific_header_assets')
    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/index-style.min.css')) !!}
@endsection


@section('inner-wrapper')
    @include('emadsite::index-page.main.index-main')
@endsection


@section('specific_endbody_assets')
    {!! Html::script (Module::asset('emadsite:js/custom.js')) !!}
@endsection