{{-- Navbar of header for desktop --}}

<nav class="nav-bar">
    <ul>
        @foreach( $nav_list as $nav_item)
            <li class="menu-item">
                <a href="{{ $nav_item['href'] or "" }}">{{ $nav_item['title'] or "" }}</a>
            </li>
        @endforeach
    </ul>
</nav>