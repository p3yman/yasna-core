{{-- Header Logo --}}

<div class="logo">
    <a href="{{ $href or "" }}">{{ $title or "" }}</a>
</div>