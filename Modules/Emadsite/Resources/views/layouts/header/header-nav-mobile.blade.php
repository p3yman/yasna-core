{{-- Header nav for Mobiles--}}

{{-- Button --}}
<button class="nav-toggle mobile">
    <span class="line"></span>
    <span class="line"></span>
    <span class="line"></span>
</button>

{{-- Nav listt --}}
<nav class="nav-list mobile">
    <ul>
        @foreach( $nav_list as $nav_item)
            <li class="menu-item">
                <a href="{{ $nav_item['href'] or "" }}">{{ $nav_item['title'] or "" }}</a>
            </li>
        @endforeach
    </ul>
</nav>