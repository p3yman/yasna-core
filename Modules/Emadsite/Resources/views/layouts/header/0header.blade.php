{{-- Header of site --}}
<header class="header">
    <div class="inner-header">
        <!-- Header Logo -->
        {{-- Needs "href" and "title" --}}
        @include('emadsite::layouts.header.header-logo',[
            'href' => url("/"),
            "title" => "Mohammad Hossein Emad"
        ])        
        <!-- !END Header Logo -->
        
        @php $navItems = Template::navBarItems(); @endphp
        <!-- Navigation -->
        @include('emadsite::layouts.header.header-nav',[
            'nav_list' => $navItems
        ])        
        <!-- !END Navigation -->
        
        <!-- mobile navigation -->
        @include('emadsite::layouts.header.header-nav-mobile',[
               'nav_list' => $navItems
        ])
        <!-- !END mobile navigation -->
    </div>
</header>