<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('page_title', Template::implodePageTitle(' :: '))</title>

        @yield('header_assets')
    </head>
    <body>
        @yield('content')

        @yield('end_body_assets')
    </body>
</html>
