
{{-- General end-body assets used in all pages --}}

<!-- jquery -->
{!! Html::script (Module::asset('emadsite:libs/jquery/jquery-3.2.1.min.js')) !!}

<!-- bootstrap js -->
{!! Html::script (Module::asset('emadsite:libs/bootstrap/bootstrap.min.js')) !!}