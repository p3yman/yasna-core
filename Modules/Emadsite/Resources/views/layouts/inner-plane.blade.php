@extends('emadsite::layouts.template')

@section('inner-wrapper')
    
    <!-- Header -->
    @include('emadsite::layouts.header.0header')
    <!-- End Of Header -->
    
    @yield('main-content')
    
    @if(!isset($noFooter) or !$noFooter)
        @include('emadsite::layouts.footer.0footer')
    @endif

@endsection

@section('specific_endbody_assets')
    <!-- Site Js File -->
    {!! Html::script (Module::asset('emadsite:js/custom.js')) !!}
@append