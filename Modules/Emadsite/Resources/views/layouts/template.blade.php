@extends('emadsite::layouts.plane')



{{-- Header assets --}}
@section('header_assets')
    {{-- general assets used for all pages --}}
    @include('emadsite::layouts.general-header-assets')
    
    @include('emadsite::layouts.open-graph')

    {{-- specific assets used in each page
    --> look for it in main blade of each page --}}
    @yield('specific_header_assets')

@append



{{-- Page contents --}}
@section('content')
    <div class="wrapper">
        {{-- specific content of each page
        --> look for it in main blade of each page --}}
        @yield('inner-wrapper')
    </div>
@endsection



{{-- End-body assets --}}
@section('end_body_assets')
    {{-- general assets used for all pages --}}
    @include('emadsite::layouts.general-endbody-assets')

    {{-- specific assets used in each page
    --> look for it in main blade of each page --}}
    @yield('specific_endbody_assets')
@append





