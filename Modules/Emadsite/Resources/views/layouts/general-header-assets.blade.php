
{{-- General assets used in all pages --}}

<!-- Bootstrap -->
{!! Html::style(Module::asset('emadsite:libs/bootstrap/bootstrap.min.css')) !!}

<!-- Fonts -->
{!! Html::style(Module::asset('emadsite:css/opensans-font.css')) !!}
{!! Html::style(Module::asset('emadsite:css/fontiran.css')) !!}

<!-- Font Awesome -->
{!! Html::style(Module::asset('emadsite:libs/font-awesome/css/font-awesome.min.css')) !!}

