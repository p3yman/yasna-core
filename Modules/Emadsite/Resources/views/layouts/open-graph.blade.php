@foreach(Template::openGraphs() as $tagTitle =>  $tagContent)
    <meta property="og:{{ $tagTitle }}" content="{{ $tagContent }}"/>
@endforeach