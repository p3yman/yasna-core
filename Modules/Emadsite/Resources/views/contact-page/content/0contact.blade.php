{{-- Contact Content --}}

<section class="contact wv-container">
    <div class="container">
        @include('emadsite::contact-page.content.contact-box',[
            "contact" => $contact,
            "links" => $links,
        ])
    </div>
</section>