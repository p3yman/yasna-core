<div class="contact-box">
    <div class="contact-box-inner">
        <div class="contact-container">
            <h1>{{ trans('emadsite::general.contact') }}</h1>
            <ul class="contact-list">
                @foreach($contact as $type => $data)
                    <li class="contact-item">
                        <span class="contact-item_title">{{ $type or "" }}</span>
                        <span class="contact-item_data">
                        {{ $data or '' }}
                        </span>
                    </li>
                @endforeach

                <li class="contact-item contact-links">
                    @foreach($links as $link)
                        <a href="{{ $link['href'] }}" class="btn social" data-toggle="tooltip" data-placement="bottom" title="{{ $link['title'] or "" }}">
                            <i class="fa fa-{{ $link['icon'] or "" }}" aria-hidden="true"></i>
                        </a>
                    @endforeach
                </li>
            </ul>
            <div class="contact-form">
                @include('emadsite::commenting.form', [
                    'fields' => $post->fieldsArray,
                    'rules' => $post->rulesArray,
                    'commentingPost' => $post,
                ])
            </div>
        </div>
    </div>

</div>