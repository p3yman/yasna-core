@extends('emadsite::layouts.inner-plane')

{{-- MAIN PAGE --}}
@php
    Template::appendToPageTitle($post->title);
    Template::setItemInOpenGraphs('description', $post->title);
@endphp

@section('specific_header_assets')
    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/contact-style.min.css')) !!}
@append


@section('main-content')
    <!-- Contact Main -->
    @include('emadsite::contact-page.content.0contact')
    <!-- !END Contact Main -->
@append


@section('specific_endbody_assets')
    <!-- Three.js -->
    {!! Html::script (Module::asset('emadsite:libs/threejs/three.min.js')) !!}
@append