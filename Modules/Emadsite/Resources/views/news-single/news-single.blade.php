@extends('emadsite::layouts.inner-plane')

{{-- MAIN PAGE --}}
@php
    Template::appendToPageTitle($post->title);
    Template::setItemInOpenGraphs('description', $post->title);
@endphp

@section('specific_header_assets')
    <!-- Owl Carousel -->
    {!! Html::style(Module::asset('emadsite:libs/owlcarousel/dist/assets/owl.carousel.min.css')) !!}
    {!! Html::style(Module::asset('emadsite:libs/owlcarousel/dist/assets/owl.theme.default.min.css')) !!}

    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/blog-style.min.css')) !!}
@append


@section('main-content')
    <!--Blog Post-->
    {{-- "post_year" and "post_info" are optional properties --}}
    @include('emadsite::blog-page.blog-post.0blog-post',[
        "type" => "news" , 
        "page_title" => $post->title ,
        "post_src" => $postImage = doc($post->featured_image)->getUrl() ,
        "news_date" => echoDate($post->created_at, 'd M Y') ,
        "post_alt" => "post" ,
        "post_desc" => $post->text,
    ])
    <!--End Of Blog Post-->
    
    @if($postImage)
        @php Template::setItemInOpenGraphs('image', $postImage); @endphp
    @endif

@append


@section('specific_endbody_assets')
    
    <!-- Owl Carousel -->
    {!! Html::script (Module::asset('emadsite:libs/owlcarousel/dist/owl.carousel.min.js')) !!}
    <!-- Slider init js -->
    {!! Html::script (Module::asset('emadsite:js/slider.js')) !!}
@append