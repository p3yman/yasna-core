<section class="news-list">
    <div class="container">
        <h1 class="main-title">{{ $heading_title }}</h1>

        <div class="row">
            <ul class="news-stack">
                @foreach($news_stack as $news)
                    <li class="news-item">
                        <a href="{{ $news['link'] or "" }}">
                            <div class="news-image">
                                <img src="{{ $news['src'] or "" }}" alt="{{ $news['alt'] or "" }}">
                            </div>
                        </a>
                        <h1 class="news-title">
                            <a href="{{ $news['link'] or "" }}">{{ $news['title'] or "" }}</a>
                        </h1>
                        <div class="news-date">
                            {{ $news['date'] or "" }}
                        </div>
                        <div class="news-abstract">
                            {{ $news['abstract'] or "" }}
                        </div>
                    </li>
                    @if($loop->iteration !== count($news_stack))
                        <hr>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
</section>
