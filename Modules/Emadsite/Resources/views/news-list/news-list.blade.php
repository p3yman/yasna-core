@extends('emadsite::layouts.inner-plane')

{{-- MAIN PAGE --}}
@php
    $heading_title = $__module->getTrans('general.news');
    Template::appendToPageTitle($heading_title);
    Template::setItemInOpenGraphs('description', $heading_title);
@endphp

@section('specific_header_assets')
    
    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/newslist-style.min.css')) !!}
@append

@section('main-content')
    <!-- News List -->
    @php
        $listData = $posts->map(function ($item) {
            return [
                "src" => doc($item->featured_image)->getUrl() ,
                "alt" => "news" ,
                "link" => route('emadsite.news.single', ['post' => $item->hashid]) ,
                "date" => echoDate($item->created_at, 'd M Y') ,
                "title" => $item->title ,
                "abstract" => strip_tags($item->abstract) ,
            ];
        });
    @endphp
    @include('emadsite::news-list.0list',[
        "news_stack" => $listData ,
    ])
    <!-- !END News List -->

    <!-- Pagination -->
    <div class="pagination-container">
        {{ $posts->render() }}
    </div>
    <!-- !END Pagination -->


@append
