@extends('emadsite::layouts.inner-plane')

{{-- MAIN PAGE --}}
@php
    $heading_title = $__module->getTrans('general.work.title.plural');
    Template::appendToPageTitle($heading_title);
    Template::setItemInOpenGraphs('description', $heading_title);
@endphp

@section('specific_header_assets')

    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/works-style.min.css')) !!}
@append


@section('main-content')
    <!--years clouds-->
    @php
        $listData = $categories->map(function ($item) {
            return [
                'duration' => $item->titleIn(getLocale()),
                'link' => route('emadsite.works.category', ['category' => $item->slug])
            ];
        });

    @endphp
    @include('emadsite::works-page.years.0years', ['titles' => $listData])

    <!-- Categories -->
    {{--@include('emadsite::appendix-page.categories',[
        "categories" => [
            [
                "link" => "#" , 
                "title" => "Battling Amnesia" , 
                "description" => "Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula." ,
            ],
            [
                "link" => "#" ,
                "title" => "Exports from Iran" ,
                "description" => "Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula." ,
            ],
            [
                "link" => "#" ,
                "title" => "Lit Shadow" ,
                "description" => "Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula." ,
            ],
            [
                "link" => "#" ,
                "title" => "Acceptance" ,
                "description" => "Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula." ,
            ],
            [
                "link" => "#" ,
                "title" => "Drawing, Sculpture" ,
                "description" => "Pellentesque in ipsum id orci porta dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula." ,
            ],

        ] , 
    ])--}}

    <!--End Of years clouds-->
@endsection


@section('specific_endbody_assets')
    <!-- Page js -->
    {{--{!! Html::script (Module::asset('emadsite:js/random.js')) !!}--}}
@append
