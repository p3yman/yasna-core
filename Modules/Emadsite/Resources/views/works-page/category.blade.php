<!-- Category -->
    <div class="category-card">
        <h2>
            <a href="{{ $link or "" }}">
                {{ $title or "" }}
            </a>
        </h2>
        <div class="category-line"></div>
        <div class="category-desc">
            {{ $description or "" }}
        </div>
    </div>
<!-- !END Category -->