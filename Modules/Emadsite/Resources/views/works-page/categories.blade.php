<section class="categories">
    <div class="container">
        <h1 class="main-title">Works</h1>
        <div class="row">
            @php
                $even = array_filter($categories, function ($key){
                            return ($key % 2 == 0);
                        },ARRAY_FILTER_USE_KEY);
                $odd = array_filter($categories, function ($key){
                            return ($key % 2 == 1);
                        },ARRAY_FILTER_USE_KEY);
            @endphp
            <div class="col-md-6">
                @foreach($even as $category)
                        @include('emadsite::appendix-page.category',[
                            "title" => $category['title'] ,
                            "link" => $category['title'] ,
                            "description" => $category['description'] ,
                        ])
                @endforeach
            </div>
            <div class="col-md-6">
                @foreach($odd as $category)
                    @include('emadsite::appendix-page.category',[
                        "title" => $category['title'] ,
                        "link" => $category['title'] ,
                        "description" => $category['description'] ,
                    ])
                @endforeach
            </div>
        </div>
    </div>
</section>