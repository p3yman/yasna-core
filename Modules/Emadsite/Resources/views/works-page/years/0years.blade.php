<section class="appendix">
    <div class="appendix-inner">
        <div class="container-fluid">
            <div class="year-container">
                <h1 class="main-title">{{ $heading_title }}</h1>
                <div class="years">
                    @foreach( $titles as $title)
                        <div class="year">
                            <a href="{{ $title['link'] }}">{{ $title['duration'] }}</a>
                            <span>.</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
