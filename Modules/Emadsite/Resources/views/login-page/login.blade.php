@extends('emadsite::layouts.template')

{{-- MAIN PAGE --}}
@php
    $title = trans('emadsite::user.login');
    Template::appendToPageTitle($title);
    Template::setItemInOpenGraphs('description', $title);
@endphp

@section('specific_header_assets')
    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/login-style.min.css')) !!}
@endsection


@section('inner-wrapper')

    <!-- Login section -->
    @include('emadsite::login-page.login-section')
    <!-- !END Login section -->

    @include('emadsite::layouts.footer.0footer')

@endsection


@section('specific_endbody_assets')
    <!-- Three.js -->
    {!! Html::script (Module::asset('emadsite:libs/threejs/three.min.js')) !!}

    <!-- Page js -->
    {!! Html::script (Module::asset('emadsite:js/custom.js')) !!}
@endsection