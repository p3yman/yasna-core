<section class="login wv-container">
    <div class="container">
        <div class="login-box">
            <div class="login-box-inner">
                <div class="login-container">
                    <div class="login-form">
                        <h2>Login</h2>
                        {!! Form::open([
                                'url' => route('login'),
                                'method' => 'POST',
                            ]) !!}
                        
                        <div class="col-sm-12">
                            <label for="email">{{ trans('validation.attributes.email') }}</label>
                            <input type="email" name="email" id="email"
                                   placeholder="{{ trans('validation.attributes.email') }} " value="{{ old('email') }}"
                                   autofocus>
                        </div>
                        
                        <div class="col-sm-12">
                            <label for="password">{{ trans('validation.attributes.password') }}</label>
                            <input type="password" name="password" id="password"
                                   placeholder="{{ trans('validation.attributes.password') }}">
                        </div>

                        @if (!env('APP_DEBUG'))
                            {!! app('captcha')->render(getLocale()) !!}
                        @endif
                        
                        @if($errors->count())
                            <div class="col-sm-12">
                                <div class="message alert alert-danger">
                                    @foreach($errors->getBag('default')->all() as $error)
                                        {{ $error }}
                                        @if (!$loop->last)
                                            <br />
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        
                        <div class="col-sm-12">
                            <button id="btn-submit" {{--name="submit"--}} type="submit">send</button>
                        </div>
                        
                        <div class="col-sm-12">
                            <div class="remember">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                {{ trans('emadsite::form.option.remember') }}
                            </div>
                        </div>
                        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
