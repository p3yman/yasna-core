{{-- Biography abstract --}}

<div class="row">
    <div class="bio-abstract">
        {{ $abstract or '' }}
    </div>
</div>