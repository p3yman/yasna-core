{{-- Biography main body --}}
<div class="container b-biography">
    <h1 class="main-title">Biography</h1>
    
    <!-- Biography image -->
    @php $image = doc($post->featured_image)->getUrl() @endphp
    @if($image)
        @php Template::setItemInOpenGraphs('image', $image); @endphp
        @include('emadsite::biography-page.content.bio-image',[
            "src" => $image,
            "alt" => "avatar"
        ])
    @endif
    <!-- !END Biography image -->
    
    <!-- Abstract Bio -->
    @include('emadsite::biography-page.content.bio-abstract',[
        "abstract" => $post->abstract ,
    ])
    <!-- !END Abstract Bio -->
    
    <!-- Main Bio -->
    @include('emadsite::biography-page.content.bio-main-context',[
        "biography" => $post->text ,
    ])
    <!-- !END Main Bio -->

    <!-- Footer -->
    @include('emadsite::biography-page.content.bio-footer',[
        'links'=> $links,
    ])
    <!-- !END Footer -->

</div>

