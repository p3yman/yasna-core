{{-- Top Bio Portrait --}}

<div class="row">
    <div class="bio-img">
        <img src="{{ $src or '' }}" alt="{{ $alt or '' }}">
    </div>
</div>