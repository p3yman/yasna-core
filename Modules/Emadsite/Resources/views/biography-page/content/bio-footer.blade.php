{{-- Footer of biography --}}

<div class="row">
    <footer class="bio-footer">
        <div class="socials">
            @foreach($links as $link)
                <a href="{{ $link['href'] }}" class="btn social" data-toggle="tooltip" data-placement="top" title="{{ $link['title'] or "" }}">
                    <i class="fa fa-{{ $link['icon'] or "" }}" aria-hidden="true"></i>
                </a>
            @endforeach
        </div>

    </footer>
</div>