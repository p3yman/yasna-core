@extends('emadsite::layouts.inner-plane')

@php
    Template::appendToPageTitle($post->title);
    Template::setItemInOpenGraphs('description', $post->title);
@endphp

@section('specific_header_assets')
    
    <!-- Page Style -->
    {!! Html::style(Module::asset('emadsite:css/biography-style.min.css')) !!}
@append

@section('main-content')
    <!-- Main content -->
    @include('emadsite::biography-page.content.0bio-content')
    <!-- !END Main content -->
@endsection