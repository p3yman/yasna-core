<?php
//test
Route::group(['middleware' => 'web'], function () {
    Route::group([
        'middleware' => 'locale',
        'namespace'  => 'Modules\Emadsite\Http\Controllers',
    ], function (\Illuminate\Routing\Router $router) {
        Route::get('/', 'EmadsiteController@index')->name('emadsite.main');

        Route::get('biography', 'EmadsiteController@biography')->name('emadsite.biography');

        Route::get('works', 'EmadsiteController@works')->name('emadsite.works');

        Route::get('works/{category}/{post?}', 'EmadsiteController@workCategory')->name('emadsite.works.category');

        Route::get('news', 'EmadsiteController@newsList')->name('emadsite.news.list');

        Route::get('news/{post}', 'EmadsiteController@newsSingle')->name('emadsite.news.single');

        Route::get('contact', 'EmadsiteController@contact')->name('emadsite.contact');

        Route::post('/form-submit', 'EmadsiteController@contactFormSubmit');

        Route::group(['middleware' => 'locale', 'namespace' => 'Auth'], function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::post('login', 'LoginController@login');
            Route::get('home', 'LoginController@home')->name('home');
        });
        $router->getRoutes()->getByAction('Modules\Yasna\Http\Controllers\Auth\LoginController@login')
            ->middleware('locale');
        $router->getRoutes()->getByAction('Modules\Yasna\Http\Controllers\Auth\RegisterController@showRegistrationForm')
            ->middleware('auth');

        Route::group(['prefix' => 'comments'], function () {
            Route::post('new', 'CommentsController@new')->name('emadsite.comments.new');
        });
    });
});
