<?php

namespace Modules\Emadsite\Http\Requests;

use App\Models\Post;
use Modules\Emadsite\Providers\CommentingServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class NewCommentRequest extends YasnaRequest
{
    protected $model_name   = "";
    protected $post;
    protected $generalRules = ['post_id' => 'exists:posts,id'];

    public function authorize()
    {
        if ($this->post) {
            return true;
        } else {
            return false;
        }
    }

    public function purifier()
    {
        return [
            'post_id' => 'dehash',
        ];
    }

    public function rules()
    {
        if (
            array_key_exists('post_id', $data = $this->data) and
            ($post = post()->grabId($data['post_id'])) and
            $post->exists
        ) {
            $this->post = $post;
            return array_merge(
                $this->generalRules,
                CommentingServiceProvider::translateRules($this->post->meta('rules'))
            );
        }
        return $this->generalRules;
    }

    public function getPost()
    {
        return $this->post;
    }
}
