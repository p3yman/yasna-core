<?php

namespace Modules\Emadsite\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Emadsite\Http\Controllers\Traits\EmadsiteFrontControllerTrait;
use Modules\Emadsite\Providers\CommentingServiceProvider;

class EmadsiteController extends Controller
{
    use EmadsiteFrontControllerTrait;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->template('index-page.index');
    }

    public function biography()
    {
        if (!($post = Post::selector(['slug' => 'biography', 'locale' => getLocale()])->first()) or !$post->exists) {
            $this->abort(404);
        }

        $links = $this->generateBiographyLinks($post);

        return $this->template('biography-page.biography', compact('post', 'links'));
    }

    protected function generateBiographyLinks(Post $post)
    {
        $links = [];
        if ($facebookLink = setting('facebook_link')->gain()) {
            $links[] = [
                "href"  => $facebookLink,
                "title" => trans('emadsite::general.social.facebook'),
                "icon"  => "facebook-square",
            ];
        }
        if ($instagramLink = setting('instagram_link')->gain()) {
            $links[] = [
                "href"  => $instagramLink,
                "title" => trans('emadsite::general.social.instagram'),
                "icon"  => "instagram",
            ];
        }
        if (
            ($resumeHashid = $post->meta('resume')) and
            ($resumeLink = doc($resumeHashid)->getDownloadUrl())
        ) {
            $links[] = [
                "href"  => $resumeLink,
                "title" => trans('emadsite::general.resume'),
                "icon"  => "id-card",
            ];
        }
        return $links;
    }

    public function works()
    {
        if (
            !($posttype = posttype('works')) or
            !$posttype->exists or
            !($categories = $posttype->categories) or
            !$categories->count()
        ) {
            return $this->abort(404);
        }
        return $this->template('works-page.works', compact('posttype', 'categories'));
    }

    public function workCategory(Request $request)
    {
        if (
            !($category = $this->readRequestCategory($request)) or
            !($posttype = $category->posttype) or
            !$posttype->exists or
            !($allPosts = $category->posts()->orderBy('published_at', 'desc')->where('locale', getLocale())->get()) or
            !$allPosts->count() or
            (
                $request->post and
                (
                    !($post = $this->readRequestPost($request)) or
                    ($post->type != $posttype->slug)
                )
            )
        ) {
            return redirect()->route('emadsite.main');
        }

        if (!isset($post)) {
            $post = $allPosts->first();
        }

        return $this->template('blog-page.blog', compact('category', 'posttype', 'post', 'allPosts'));
    }

    protected function readRequestCategory($request)
    {
        if ($request->category and ($category = model("category")->grabSlug($request->category)) and $category->exists) {
            return $category;
        }
        return null;
    }

    protected function readRequestPost($request)
    {
        if ($request->post and ($post = Post::findByHashid($request->post)) and $post->exists) {
            return $post;
        }

        return null;
    }

    public function contact()
    {
        $post = Post::selector(['type' => 'commenting', 'slug' => 'contact'])->first();
        if (!$post or !$post->exists) {
            return $this->abort(404);
        }
        $post = $this->readCommentingInfo($post);
        return $this->template(
            'contact-page.contact',
            array_merge(compact('post'), $this->getContactData())
        );
    }

    protected function getContactData()
    {
        $data = ['links' => [], 'contact' => []];
        if ($facebookLink = setting('facebook_link')->gain()) {
            $data['links'][] = [
                "href"  => $facebookLink,
                "title" => trans('emadsite::general.social.facebook'),
                "icon"  => "facebook-square",
            ];
        }
        if ($instagramLink = setting('instagram_link')->gain()) {
            $data['links'][] = [
                "href"  => $instagramLink,
                "title" => trans('emadsite::general.social.instagram'),
                "icon"  => "instagram",
            ];
        }
        if ($emails = setting('email')->gain()) {
            $data['contact']['email'] = implode(', ', $emails);
        }
        return $data;
    }

    protected function readCommentingInfo(Post $post)
    {
        return $this->readCommentingRules($this->readCommentingFields($post->spreadMeta()));
    }

    protected function readCommentingFields(Post $post)
    {
        $post->fieldsArray = CommentingServiceProvider::translateFields($post->fields);
        return $post;
    }

    protected function readCommentingRules(Post $post)
    {
        $post->rulesArray = CommentingServiceProvider::translateRules($post->rules);
        return $post;
    }

    public function newsList()
    {
        $posts = Post::selector([
            'type'   => 'news',
            'locale' => getLocale(),
        ])->orderBy('created_at', 'DESC')
            ->paginate(15);

        $posttype = posttype('news');

        if (!$posttype or !$posttype->exists or !$posts->count()) {
            return redirect()->route('emadsite.main');
        }

        return $this->template('news-list.news-list', compact('posts', 'posttype'));
    }

    public function newsSingle(Request $request)
    {
        if (!($post = $this->readRequestPost($request))) {
            return redirect()->route('emadsite.main');
        }

        return $this->template('news-single.news-single', compact('post'));
    }

    public function loginPage()
    {
        return view('emadsite::login-page.login');
    }

    public function contactFormSubmit(Request $request)
    {
        dd($request);
        return "submit";
    }
}
