<?php

namespace Modules\Emadsite\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\URL;
use Modules\Emadsite\Http\Controllers\Traits\EmadsiteFrontControllerTrait;
use Modules\Emadsite\Http\Requests\NewCommentRequest;

class CommentsController extends Controller
{
    use EmadsiteFrontControllerTrait;

    protected $indexesToChange = [
        'name'    => 'guest_name',
        'email'   => 'guest_email',
        'mobile'  => 'guest_mobile',
        'message' => 'text',
    ];

    protected $sectionFeedTransMap = [
        'contact' => 'send-message',
    ];

    protected $supportRolesMap = [
        'contact' => 'support-contact',
    ];

    protected $request;


    public function new(NewCommentRequest $request)
    {
        $this->request = $request;

        $savedComment = model('comment')
            ->batchSave($this->changeIndexes($request->all()));

        return $this->jsonAjaxSaveFeedback($savedComment->exists, [
            'success_message'    => $this->getFeed(($post = $request->getPost())->slug),
            'success_form_reset' => 1,

            'danger_message' => $this->getFeed($post->slug, false),
        ]);
    }

    protected function changeIndexes(array $data)
    {
        foreach ($this->indexesToChange as $from => $to) {
            if (array_key_exists($from, $data)) {
                $data[$to] = $data[$from];
                unset($data[$from]);
            }
        }
        return array_default($data, $this->additionalIndexes());
    }

    protected function additionalIndexes()
    {
        return [
            'guest_ip'              => \request()->ip(),
            'user_id'               => user()->id,
            'posttype_id'           => $this->request->getPost()->posttype->id,
            'initial_department_id' => ($departmentId = $this->getDepartmentId($this->request->getPost()->slug)),
            'current_department_id' => $departmentId,
        ];
    }

    protected function getFeed($section = null, bool $success = true)
    {
        if (!is_null($section) and $section and ($feedIndex = $this->getSectionFeedIndex($section))) {
            return trans(
                'emadsite::general.alert.' .
                ($success ? 'success' : 'error') .
                ".$feedIndex"
            );
        } else {
            return trans('emadsite::form.feed.' . ($success ? 'done' : 'error'));
        }
    }

    protected function getSectionFeedIndex($section)
    {
        return array_key_exists($section, $this->sectionFeedTransMap)
            ? $this->sectionFeedTransMap[$section]
            : false;
    }

    protected function getDepartmentId(string $section)
    {
        return role(
            array_key_exists($section, $this->supportRolesMap)
                ? $this->supportRolesMap[$section]
                : ''
        )->id;
    }
}
