<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 11/12/2017
 * Time: 10:35 AM
 */

namespace Modules\Emadsite\Http\Controllers\Auth;

use Modules\Emadsite\Http\Controllers\Traits\EmadsiteFrontControllerTrait;

class LoginController extends \Modules\Yasna\Http\Controllers\Auth\LoginController
{
    use EmadsiteFrontControllerTrait;

    public function showLoginForm()
    {
        return $this->template('login-page.login');
    }

    public function redirectAfterLogin()
    {
        if (user()->is_an('admin')) {
            $url = url('/manage');
        } else {
            $url = route('emadsite.main');
        }

        return redirect()->intended($url);
    }

    public function redirectTo()
    {
        return route('home');
    }
}
