<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 10/12/2017
 * Time: 01:32 PM
 */

namespace Modules\Emadsite\Http\Controllers\Traits;

trait TemplateControllerTrait
{
    use ModuleControllerTrait;

    private function template($view, $data = [], $mergeData = [])
    {
        if (!view()->exists($view = $this->moduleView($view))) {
            return $this->abort(404);
        }

        return view($view, $data, array_merge($mergeData, $this->additionalData()));
    }

    private function moduleView($view)
    {
        if (is_string($view) and !str_contains($view, '::')) {
            $view = self::getModuleName() . '::' . $view;
        }

        return $view;
    }

    private function additionalData()
    {
        if (
            (method_exists($this, 'templateVariables')) and
            ($additionalData = $this->templateVariables()) and
            is_array($additionalData)
        ) {
            return $additionalData;
        }
        return [];
    }
}
