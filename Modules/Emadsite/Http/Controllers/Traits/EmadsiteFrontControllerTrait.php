<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 10/12/2017
 * Time: 01:27 PM
 */

namespace Modules\Emadsite\Http\Controllers\Traits;

trait EmadsiteFrontControllerTrait
{
    use EmadsiteControllerTrait;
    use FeedbackControllerTrait;
}
