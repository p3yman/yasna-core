<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 10/12/2017
 * Time: 01:36 PM
 */

namespace Modules\Emadsite\Http\Controllers\Traits;

use Modules\Emadsite\Providers\TemplateServiceProvider;

trait EmadsiteControllerTrait
{
    use TemplateControllerTrait;

    protected static $moduleName = 'emadsite';

    private function templateVariables()
    {
        $this->setTemplateProperties();

        return [
            'footer_title' => setting('yasna-group-title')->gain(),
            'footer_link'  => setting('yasna-group-url')->gain(),
        ];
    }

    private function setTemplateProperties()
    {
        $this->setSiteTitle();
        $this->setMenuItems();
        $this->setOpenGraphs();
    }

    private function setSiteTitle()
    {
        $siteTitleConfig = setting('site_title')->gain();
        if ($siteTitleConfig) {
            TemplateServiceProvider::appendToPageTitle($siteTitleConfig);
            TemplateServiceProvider::setSiteTitle($siteTitleConfig);
        }
    }

    private function setMenuItems()
    {
        TemplateServiceProvider::setNavBarItems([
            [
                "href"  => route('emadsite.biography'),
                "title" => trans('emadsite::general.biography'),
            ],
            [
                "href"  => route('emadsite.works'),
                "title" => trans("emadsite::general.work.title.plural"),
            ],
            [
                "href"  => route('emadsite.news.list'),
                "title" => trans('emadsite::general.news'),
            ],
            [
                "href"  => route('emadsite.contact'),
                "title" => trans('emadsite::general.contact'),
            ],
        ]);
    }

    private function setOpenGraphs()
    {
        TemplateServiceProvider::setOpenGraphs(array_default(TemplateServiceProvider::openGraphs(), [
            'title'       => get_setting('site_title'),
            'url'         => request()->url(),
            'image'       => doc(get_setting('open-graph-image'))->getUrl(),
            'description' => get_setting('open_graph_default_description'),
        ]));
    }
}
