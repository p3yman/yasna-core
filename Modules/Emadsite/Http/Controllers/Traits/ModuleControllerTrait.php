<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 10/12/2017
 * Time: 01:33 PM
 */

namespace Modules\Emadsite\Http\Controllers\Traits;

trait ModuleControllerTrait
{
    public static function getModuleName()
    {
        return self::$moduleName;
    }
}
