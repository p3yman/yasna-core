<?php

namespace Modules\Emadsite\Providers;

use Illuminate\Support\ServiceProvider;

class TemplateServiceProvider extends ServiceProvider
{
    private static $methodPrefixes = [
        'appending'     => 'appendTo',
        'merging'       => 'mergeWith',
        'imploding'     => 'implode',
        'assigning'     => 'set',
        'settingItemIn' => 'setItemIn',
    ];
    private static $__calledMethod = null;

    protected static $pageTitle = [];
    protected static $siteTitle = '';
    protected static $navBarItems = [];
    protected static $openGraphs = [];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }


    public static function __callStatic($name, $arguments)
    {
        self::$__calledMethod = $name;

        if (starts_with($name, self::$methodPrefixes['appending'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['appending']));
            return self::appendToProperty($property, $arguments);
        } elseif (starts_with($name, self::$methodPrefixes['merging'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['merging']));
            return self::mergeWithProperty($property, ...$arguments);
        } elseif (starts_with($name, self::$methodPrefixes['settingItemIn'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['settingItemIn']));
            return self::setItemInProperty($property, $arguments);
        } elseif (starts_with($name, self::$methodPrefixes['imploding'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['imploding']));
            return self::implodeProperty($property, $arguments);
        } elseif (starts_with($name, self::$methodPrefixes['assigning'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['assigning']));
            return self::setProperty($property, $arguments);
        } else {
            if (property_exists(__CLASS__, $name)) {
                return self::$$name;
            }
        }

        return self::undefinedMethodException();
    }

    protected static function appendToProperty($property, $appending)
    {
        if (property_exists(__CLASS__, $property)) {
            if (is_array(self::$$property)) {
                self::$$property = array_merge(self::$$property, $appending);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function mergeWithProperty($property, $merging)
    {
        if (property_exists(__CLASS__, $property)) {
            if (is_array(self::$$property)) {
                self::$$property = array_merge(self::$$property, $merging);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function setItemInProperty($property, $parameters)
    {
        if (property_exists(__CLASS__, $property)) {
            if (is_array(self::$$property)) {
                if (count($parameters) > 1) {
                    self::$$property[$parameters[0]] = $parameters[1];
                } else {
                    self::tooFewArgumentsException(2);
                }
            }

            return true;
        }
        self::undefinedMethodException();
    }

    protected static function setProperty($property, $parameters)
    {
        if (property_exists(__CLASS__, $property)) {
            if (count($parameters)) {
                self::$$property = $parameters[0];
            } else {
                self::tooFewArgumentsException(1);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function implodeProperty($property, $parameters)
    {
        if (property_exists(__CLASS__, $property) and is_array($propertyVal = self::$$property)) {
            if (count($parameters)) {
                return implode($parameters[0], $propertyVal);
            } else {
                self::tooFewArgumentsException(1);
            }
        }

        self::undefinedMethodException();
    }

    private static function undefinedMethodException()
    {
        trigger_error(
            'Call to undefined method ' . __CLASS__ . '::' . self::$__calledMethod . '()', E_USER_ERROR
        );
    }

    private static function tooFewArgumentsException($passedArgumentsCount)
    {
        trigger_error(
            'Too few arguments to function '
            . __CLASS__ . '::' .
            self::$__calledMethod
            . ', '
            . $passedArgumentsCount
            . ' passed', E_USER_ERROR
        );
    }
}
