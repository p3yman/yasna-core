<?php

namespace Modules\Emadsite\Providers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;

class EmailServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    public static function send($mailContent, $to, $name, $subject, $template)
    {
        return Mail::send(
            'emadsite::email.templates.' . $template,
            compact('mailContent'),
            function ($m) use ($to, $name, $subject) {
                $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME') ?: get_setting('site_title'));

                $m->to($to, $name)
                    ->subject($subject);
            }
        );
    }
}
