<?php

namespace Modules\Emadsite\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Commenting\Events\CommentReplied;
use Modules\Emadsite\Listeners\SendEmailOnCommentReplied;
use Modules\Yasna\Services\YasnaProvider;

class EmadsiteServiceProvider extends YasnaProvider
{

    protected $providers = [
        TemplateServiceProvider::class,
        CommentingServiceProvider::class,
        EmailServiceProvider::class,
    ];
    protected $aliases   = [
        'Template' => TemplateServiceProvider::class,
    ];

    public function index()
    {
        $this->addProviders();
        $this->addAliases();
        $this->registerEventListeners();
    }

    protected function addProviders()
    {
        foreach ($this->providers as $provider) {
            $this->addProvider($provider);
        }
    }

    protected function addAliases()
    {
        foreach ($this->aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }
    }

    protected function registerEventListeners()
    {
        $this->listen(CommentReplied::class, SendEmailOnCommentReplied::class);
    }
}
