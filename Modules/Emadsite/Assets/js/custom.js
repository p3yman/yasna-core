jQuery(function($){

    /*
    *-------------------------------------------------------
    * Main page js
    *-------------------------------------------------------
    */

    $('.nav-toggle').on('click',function(){
       $('.nav-list').slideToggle();
    });

    $('.load-line').animate({
        width:"100%"
    },1500,titleFadeIn);

    function titleFadeIn() {
        $('.title-big').delay(500).animate({
            opacity :1
        },1500, slideFade);
    }
    function slideFade() {
        $('.nav-center').css({
            opacity:1,
            marginTop:0,
            marginBottom:20
        });
    }
    /*
    *-------------------------------------------------------
    * single Image openner
    *-------------------------------------------------------
    */
    var singleWin = $('.js-single'),
        singleImg = $('.img-single'),
        singleCap = $('.js-single .caption');

    $('.blog .js-img').on('click',function () {
        imgChanger($(this));
        singleWin.fadeIn();
    });

    $('.js-close').on('click',function () {
        singleWin.fadeOut();
    });

    singleWin.on('click',function () {
        singleWin.fadeOut();
    });
    singleImg.on('click',function (e) {
        e.stopPropagation();
    });
    singleCap.on('click',function (e) {
        e.stopPropagation();
    });

    function imgChanger(target) {
        var img = target.find('img'),
            src = img.attr('src');
            // caption = target.next('.post-info');
        singleImg.attr('src',src);
        
        // if(caption.length){
        //     singleCap.empty().text(caption.text()).show();
        // }else{
        //     singleCap.hide();
        // }
    }

    /*
    *-------------------------------------------------------
    *   IE page resize
    *-------------------------------------------------------
    */
    var resizingObj = $('.blog-post .main-data'),
        imageObj = resizingObj.find('.post-image');

    if(msieversion() === 1 && resizingObj){
        var $window = $(window),
            winHeight = $window.outerHeight();

        resizingObj.outerHeight(winHeight);
        imageObj.outerHeight(winHeight * 0.60);

        $window.on('resize',function () {
            winHeight = $window.outerHeight();

            resizingObj.outerHeight(winHeight);
            imageObj.outerHeight(winHeight * 0.60);
        });
    }

    function msieversion() {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
        {
            return 1;
        }
        else  // If another browser, return 0
        {
            return 0;
        }

        return false;
    }

    /*
    *-------------------------------------------------------
    * Check persian character
    *-------------------------------------------------------
    */

    var newsTitle = $('.news-stack .news-title a'),
        mainTitle = $('.main-title');

    checkDir(newsTitle);
    checkDir(mainTitle);

    function checkDir(selector) {

        var $persChars = ['ا', 'آ', 'ب', 'پ', 'ت', 'س', 'ج', 'چ', 'ح', 'خ', 'د', 'ذ', 'ر', 'ز', 'ژ', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 'ف', 'ق', 'ک', 'گ', 'ل', 'م', 'ن', 'و', 'ه', 'ی', 'ء'];
        var $isPersian = false;


        selector.each(function (index,el) {

            var $object = $(el);
            var $string = $object.text().trim();
            var $strLength = $string.length;
            var $Char = $string.substr(0, 1);
            var $isPersian = false;
            var $i = 0;
            var $j = 0;

            if (!$string) {
                $object.removeClass('persian');
                return;
            }
            for($j = 0; $j < $strLength; $j++){
                $Char = $string.substr($j, 1);
                for ($i = 0; $i < 45; $i++) {
                    if ($persChars[$i] == $Char) {
                        $isPersian = true;
                        break;
                    }
                }
            }

            if ($isPersian) {
                $object.addClass('persian');
            }
            else{
                $object.removeClass('persian');
            }

        });

    }

    /*
    *-------------------------------------------------------
    * Tooltips
    *-------------------------------------------------------
    */

    $('[data-toggle="tooltip"]').tooltip();

    /*
    *-------------------------------------------------------
    * Three.js setup
    *-------------------------------------------------------
    */

    var SEPARATION = 40, AMOUNTX = 130, AMOUNTY = 35;

    var container;
    var camera, scene, renderer;
    var wvContainer = $('.wv-container');
    /*

     if (window.WebGLRenderingContext){
     renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
     }
     else {
     renderer = new THREE.CanvasRenderer();
     }
     */

    var particles, particle, count = 0;

    var windowHalfX = wvContainer.outerWidth() / 2;
    var windowHalfY = wvContainer.outerHeight() / 2;

    

    if(typeof THREE !== "undefined"){
        init();
        animate();
    }

    function init() {

        container = document.createElement( 'div' );
        wvContainer.prepend(container);
        //document.getElementById('wv-container').appendChild( container );
        if(container) {
            container.className += container.className ? ' waves' : 'waves';
        }

        camera = new THREE.PerspectiveCamera( 120, window.innerWidth / window.innerHeight, 1, 10000 );
        camera.position.y = 150; //changes how far back you can see i.e the particles towards horizon
        camera.position.z = 300; //This is how close or far the particles are seen

        camera.rotation.x = 0.35;

        scene = new THREE.Scene();

        particles = new Array();

        var PI2 = Math.PI * 2;
        var material = new THREE.SpriteCanvasMaterial( {

            color: 0x939393, //changes color of particles
            program: function ( context ) {

                context.beginPath();
                context.arc( 0, 0, 0.1, 0, PI2, true );
                context.fill();

            }

        } );

        var i = 0;

        for ( var ix = 0; ix < AMOUNTX; ix ++ ) {

            for ( var iy = 0; iy < AMOUNTY; iy ++ ) {

                particle = particles[ i ++ ] = new THREE.Sprite( material );
                particle.position.x = ix * SEPARATION - ( ( AMOUNTX * SEPARATION ) / 2 );
                particle.position.z = iy * SEPARATION - ( ( AMOUNTY * SEPARATION ) - 10 );
                scene.add( particle );

            }

        }

        renderer = new THREE.CanvasRenderer();
        renderer.setSize( window.innerWidth, window.innerHeight );
        renderer.setClearColor( 0xffffff, 1);
        container.appendChild( renderer.domElement );

        window.addEventListener( 'resize', onWindowResize, false );

    }

    function onWindowResize() {

        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;

        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize( window.innerWidth, window.innerHeight );

    }

    function animate() {

        requestAnimationFrame( animate );

        render();

    }

    function render() {

        var i = 0;

        for ( var ix = 0; ix < AMOUNTX; ix ++ ) {

            for ( var iy = 0; iy < AMOUNTY; iy ++ ) {

                particle = particles[ i++ ];
                particle.position.y = ( Math.sin( ( ix + count ) * 0.5 ) * 20 ) + ( Math.sin( ( iy + count ) * 0.5 ) * 20 );
                particle.scale.x = particle.scale.y = ( Math.sin( ( ix + count ) * 0.3 ) + 2 ) * 4 + ( Math.sin( ( iy + count ) * 0.5 ) + 1 ) * 4;

            }

        }

        renderer.render( scene, camera );

        // This increases or decreases speed
        count += 0.2;

    }


}); //End Of siaf!
