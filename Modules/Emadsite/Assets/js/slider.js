jQuery(function($){

    var imgRow = $('.img-row'),
        $window = $(window),
        winWidth = $window.outerWidth(),
        imgRowWidth = imgRow.outerWidth();

    checkLayout(imgRowWidth,winWidth);

    $window.on('resize',function () {
        winWidth = $window.outerWidth();
        checkLayout(imgRowWidth,winWidth);
    });

    // checks if slider is necessary or not
    function checkLayout(row,win) {
        if(row > win ){
            imgRow.addClass('owl-carousel').owlCarousel({
                items:30,
                margin:0,
                loop:true,
                autoplay:true,
                autoplaySpeed: 1000,
                autoplayTimeout:1500,
                nav:false,
                autoWidth:true,
                stagePadding: 50,
                responsiveRefreshRate: 100,
            })
                .on('mouseenter',function () {
                    imgRow.trigger('stop.owl.autoplay');
                })
                .on('mouseleave',function () {
                    imgRow.trigger('play.owl.autoplay');
                })
        }else{
            imgRow.owlCarousel({}).trigger('destroy.owl.carousel');
            imgRow.removeClass('owl-carousel');
        }
    }


}); //End Of siaf!
