<?php

namespace Modules\Emadsite\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Commenting\Events\CommentReplied;
use Modules\Emadsite\Providers\EmailServiceProvider;

class SendEmailOnCommentReplied
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    protected $event;

    /**
     * Handle the event.
     *
     * @param \Modules\Emadsite\Events\\Modules\Commenting\Events\CommentReplied $event
     *
     * @return void
     */
    public function handle(CommentReplied $event)
    {
        $this->event = $event;

        $this->sendEmail();
    }

    protected function sendEmail()
    {
        try {
            if ($email = $this->targetEmail()) {
                $model   = $this->event->model;
                $message = (($parent = $model->parent) and $parent->exists) ? $parent->text : '';
                $reply   = $model->text;

                $mailContent = view('emadsite::email.contact-reply', compact('message', 'reply'));

                return $sendingEmailResult = EmailServiceProvider::send(
                    $mailContent,
                    $email,
                    get_setting('site_title'),
                    trans('emadsite::general.contact-reply.email-subject'),
                    'default'
                );
            }
        } catch (\Exception $e) {
            // If failed sending email
        }
    }

    protected function targetEmail()
    {
        if (($parent = $this->event->model->parent) and $parent->exists) {
            if ($parent->guest_email) {
                return $parent->guest_email;
            } elseif (($user = $parent->user) and $user->exists and $user->email) {
                return $user->email;
            }
        }

        return null;
    }
}
