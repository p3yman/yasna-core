<?php

namespace Modules\Gachet\Events;

use Modules\Yasna\Entities\User;
use Modules\Yasna\Services\YasnaEvent;

class PasswordResetRequested extends YasnaEvent
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    public $model_name = 'user';
    /**
     * The Token to Be Sent
     *
     * @var string
     */
    public $token;



    /**
     * PasswordResetRequested constructor.
     *
     * @param string|bool|User $model
     * @param string           $token
     */
    public function __construct($model = false, string $token)
    {
        $this->token = $token;

        parent::__construct($model);
    }
}
