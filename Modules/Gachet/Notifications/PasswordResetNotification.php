<?php

namespace Modules\Gachet\Notifications;

use App\Models\User;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PasswordResetNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use ModuleRecognitionsTrait;

    protected $token;



    /**
     * PasswordResetNotification constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }



    /**
     * Returns the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via(User $notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * Returns the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $notifiable)
    {
        $subject    = $this->runningModule()->getTrans('user.password.restore');
        $blade_path = $this->runningModule()->getBladePath('email.user-restore-password');

        return (new MailMessage)
             ->subject($subject)
             ->view($blade_path, [
                  'user'       => $notifiable,
                  'token'      => $this->token,
                  'site_url'   => get_setting('site_url'),
                  'site_title' => get_setting('site_title'),
             ])
             ;
    }



    /**
     * Returns the array representation of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function toArray(User $notifiable)
    {
        return [
             'user_id'   => $notifiable->id,
             'user_name' => $notifiable->full_name,
             'token'     => $this->token,
        ];
    }
}
