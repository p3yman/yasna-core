<?php

namespace Modules\Gachet\Notifications;

use App\Models\User;
use Modules\Cart\Events\CartCheckedOut;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class CartCheckoutUserNotification extends YasnaNotificationAbstract
{
    /** @var  CartCheckedOut $event */
    public $event;



    /**
     * Create a new notification instance.
     *
     * @param CartCheckedOut $event
     *
     * @return void
     */
    public function __construct(CartCheckedOut $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param CartCheckedOut $event
     *
     * @return void
     */
    public function handle(CartCheckedOut $event)
    {
        /** @var User $user */
        $user = $event->model->user;


        $user->notify(new CartCheckoutUserNotification($event));

    }



    /**
     * Get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {

        $message = trans_safe('gachet::message.checkout_sms_buyer', [
             'code' => $this->event->model->code,
        ]);

        return $message;
    }

}
