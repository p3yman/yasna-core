<?php

namespace Modules\Gachet\Notifications;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class RegistrationNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use ModuleRecognitionsTrait;



    /**
     * Returns the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via(User $notifiable)
    {
        $channels = [];

        if ($this->haveToSendSms()) {
            $channels[] = $this->yasnaSmsChannel();
        }

        if ($this->haveToSendEmail()) {
            $channels[] = $this->yasnaMailChannel();
        }

        return $channels;
    }



    /**
     * Whether the SMS notification should be sent.
     *
     * @return bool
     */
    protected function haveToSendSms()
    {
        return get_setting('user_registration_send_sms');
    }



    /**
     * Whether the mail notification should be sent.
     *
     * @return bool
     */
    protected function haveToSendEmail()
    {
        return get_setting('user_registration_send_email');
    }



    /**
     * Returns the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $notifiable)
    {
        $subject    = $this->runningModule()->getTrans('user.successful-registration');
        $blade_path = $this->runningModule()->getBladePath('email.user-registered');

        return (new MailMessage)
             ->subject($subject)
             ->view($blade_path, [
                  'user'       => $notifiable,
                  'site_url'   => get_setting('site_url'),
                  'site_title' => get_setting('site_title'),
             ])
             ;
    }



    /**
     * Returns the sms representation of the notification.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function toSms(User $notifiable)
    {
        $text = $this->runningModule()->getTrans('user.message.register-success-sms', [
             'name' => $notifiable->full_name,
             'site' => get_setting('site_title'),
        ])
        ;
        $text .= LINE_BREAK;
        $text .= get_setting('site_url');

        return $text;
    }



    /**
     * Returns the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray(User $notifiable)
    {
        return [
             'user_id'   => $notifiable->id,
             'user_name' => $notifiable->full_name,
        ];
    }
}
