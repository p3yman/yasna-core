<?php

namespace Modules\Gachet\Notifications;

use App\Models\User;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Modules\Payment\Events\FailedPayment;

class FailedPaymentNotification extends YasnaNotificationAbstract
{
    /** @var  FailedPayment $event */
    public $event;



    /**
     * Create a new notification instance.
     *
     * @param FailedPayment $event
     */
    public function __construct(FailedPayment $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param FailedPayment $event
     *
     * @return void
     */
    public function handle(FailedPayment $event)
    {
        /** @var User $user */
        $user = $event->model->user;
        $user->notify(new FailedPaymentNotification($event));

    }



    /**
     * Get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $message = trans_safe('gachet::message.failed_payment', [
             'code' => $this->event->model->cart->code
        ]);

        return $message;
    }

}
