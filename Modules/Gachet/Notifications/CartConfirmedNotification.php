<?php

namespace Modules\Gachet\Notifications;

use App\Models\User;
use Modules\Cart\Events\CartConfirmed;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class CartConfirmedNotification extends YasnaNotificationAbstract
{
    /** @var  CartConfirmed $event */
    public $event;



    /**
     * Create a new notification instance.
     *
     * @param CartConfirmed $event
     */
    public function __construct(CartConfirmed $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param CartConfirmed $event
     *
     * @return void
     */
    public function handle(CartConfirmed $event)
    {
        /** @var User $user */
        $user = $event->model->user;

        $user->notify(new CartConfirmedNotification($event));

    }



    /**
     * Get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {

        $message = trans_safe('gachet::message.confirmed_sms_buyer', [
             'code' => $this->event->model->code,
        ]);

        return $message;
    }

}
