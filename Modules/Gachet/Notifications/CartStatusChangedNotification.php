<?php

namespace Modules\Gachet\Notifications;

use App\Models\Cart;
use App\Models\User;
use Modules\Cart\Events\CartStatusChanged;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class CartStatusChangedNotification extends YasnaNotificationAbstract
{
    /** @var  CartStatusChanged $event */
    public $event;



    /**
     * Create a new notification instance.
     *
     * @param CartStatusChanged $event
     */
    public function __construct(CartStatusChanged $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param CartStatusChanged $event
     *
     * @return void
     */
    public function handle(CartStatusChanged $event)
    {
        $cart = $event->model;
        /** @var User $user */
        $user = $event->model->user;

        if ($cart->isConfirmed() or $cart->isSent() or $cart->isDelivered()) {
            $user->notify(new CartStatusChangedNotification($event));
        }

    }



    /**
     * Get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {

        /** @var Cart $cart */
        $cart = $this->event->model;
        $code = $cart->code;
        if ($cart->isConfirmed()) {
            $message = trans_safe('gachet::message.confirmed_sms_buyer', [
                 'code' => $this->event->model->code,
            ]);
        }
        if ($cart->isSent()) {
            $message = trans_safe('gachet::message.is_sent_sms_buyer', [
                 'code' => $code,
            ]);
        }
        if ($cart->isDelivered()) {
            $message = trans_safe('gachet::message.is_delivered_sms_buyer', [
                 'code' => $code,
            ]);
        }
        if (!isset($message)) {
            die();
        }

        return $message;
    }

}
