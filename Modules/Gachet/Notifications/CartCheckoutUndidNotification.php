<?php

namespace Modules\Gachet\Notifications;

use App\Models\User;
use Modules\Cart\Events\CartCheckoutUndid;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class CartCheckoutUndidNotification extends YasnaNotificationAbstract
{
    /** @var  CartCheckoutUndid $event */
    public $event;



    /**
     * Create a new notification instance.
     *
     * @param CartCheckoutUndid $event
     */
    public function __construct(CartCheckoutUndid $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param CartCheckoutUndid $event
     *
     * @return void
     */
    public function handle(CartCheckoutUndid $event)
    {
        /** @var User $user */
        $user = $event->model->user;
        $user->notify(new CartCheckoutUndidNotification($event));

    }



    /**
     * Get the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $code    = $this->event->model->code;
        $message = trans_safe('gachet::message.is_not_confirmed_sms_buyer', [
             'code' => $code,
        ]);

        return $message;
    }

}
