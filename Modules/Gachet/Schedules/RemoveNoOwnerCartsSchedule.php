<?php namespace Modules\Gachet\Schedules;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaSchedule;

class RemoveNoOwnerCartsSchedule extends YasnaSchedule
{
    //Feel free to incorporate additional() and condition() methods, or even override handle() method as appropriates.

    protected function job()
    {
        $cartsToDelete = model('cart')
            ->where('user_id', '<', 1)
            ->whereNull('raised_at')
            ->where('updated_at', '<', Carbon::now()->subDay())
            ->get();

        foreach ($cartsToDelete as $cart) {
            $cart->forceDelete();
        }
    }


    protected function frequency()
    {
        return 'hourly';
    }
}
