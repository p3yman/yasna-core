<?php namespace Modules\Gachet\Schedules;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaSchedule;

class RemoveOldAndPendingCartsSchedule extends YasnaSchedule
{
    //Feel free to incorporate additional() and condition() methods, or even override handle() method as appropriates.

    protected function job()
    {
        $cartsToDelete = model('cart')
            ->whereNull('raised_at')
            ->where('updated_at', '<', Carbon::now()->subDays(7))
            ->get();

        foreach ($cartsToDelete as $cart) {
            $cart->delete();
        }
    }


    protected function frequency()
    {
        return 'hourly';
    }
}
