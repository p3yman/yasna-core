<?php
return [
     "products-plural"               => "محصولات",
     "products-singular"             => "محصول",
     "news-plural"                   => "اخبار",
     "news-singular"                 => "خبر",
     "blog-plural"                   => "مطالب وبلاگ",
     "blog-singular"                 => "مطلب وبلاگ",
     "fields"                        => "فیلدها",
     "rules"                         => "قوانین",
     "commenting-plural"             => "کامنت‌گیرها",
     "commenting-singular"           => "کامنت‌گیر",
     "pages-plural"                  => "برگه‌ها",
     "pages-singular"                => "برگه",
     "faq-plural"                    => "پرسش‌های متداول",
     "faq-singular"                  => "پرسش متداول",
     "home-sliders-plural"           => "اسلایدرهای صفحه‌ی اصلی",
     "home-sliders-singular"         => "اسیلایدر صفحه‌ی اصلی",
     "min-payable-amount"            => "حداقل مبلغ قابل پرداخت",
     "min-payable-amount-help"       => "حد اقل مبلغی که باید برای ثبت سفارش پرداخت شود.",
     "shipping-price-per-floor"      => "هزینه‌ی حمل به ازای هر طبقه",
     "shipping-price-per-floor-help" => "هزینه‌ای که برای حمل کالا به ازای هر طبقه به هزینه‌ی سفارش افزوده می‌شود.",
     "catalog"                       => "کاتالوگ",
     "on_sale"                       => "این محصول حراج است.",


     "featured-image-of" => "عکس سربرگ :title",
     "attachments-of"    => "فایل‌های ضمیمه‌ی :title",
     "category-image-of" => "عکس دسته‌بندی :title",
     "catalogs-of"       => "کاتالوگ‌های :title",

     "category-image-upload-config" => "تنظیمات آپلود عکس دسته‌بندی",
     "catalogs-upload-config"       => "تنظیمات آپلود کاتالوگ‌ها",

     "roles" => [
          "customer-singular" => "مشتری",
          "customer-plural"   => "مشتری‌ها",
          "vip-singular"      => "کاربر VIP",
          "vip-plural"        => "کاربران VIP",
          "coworker-singular" => "همکار",
          "coworker-plural"   => "همکاران",
     ],

     "settings" => [
          "maximum-pending-jobs"         => "بیشترین تعداد jobهای در انتظار",
          "maximum-pending-jobs-help"    => "اگر به تعداد عدد ثبت شده در این قسمت job در انتظار داشته باشیم queue:work اجرا می‌شود.",
          "user-registration-send-sms"   => "ارسال پیامک برای ثبت نام کاربر",
          "user-registration-send-email" => "ارسال ایمیل برای ثبت نام کاربر",
          "fax"                          => "فکس",
     ],

     "menus" => [
          "main"    => "منوی اصلی",
          "footer"  => "منوی فوتر",
          "contact" => "تماس با ما",
          "news"    => "اخبار",
          "blog"    => "وبلاگ",
     ],

     "contact-commenting-title2" => "ارسال پیام",
     "faq-commenting-title2"     => "پاسخ پرسش خود را نیافتید؟",


     "categories" => [
          "home-slider"     => "اسلایدرهای صفحه‌ی اصلی",
          "products-brands" => "برند محصولات",
     ],

     "after-sale-services" => "خدمات پس از فروش",

     "point-rate" => "نرخ امتیاز",

     "yasna-group-title" => "عنوان گروه یسنا",
     "yasna-group-url"   => "آدرس سایت گروه یسنا",
     "minimum_payment"   => "حداقل مبلغ قابل پرداخت",
];
