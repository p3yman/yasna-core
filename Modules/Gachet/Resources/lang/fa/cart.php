<?php
return [
     "title" => [
          "singular" => "سبد خرید",
     ],

     "purchase" => [
          "singular" => "خرید",
          "plural"   => "خریدها",
     ],

     "numeration-label" => [
          "number" => "تعداد",
          "amount" => "مقدار",
     ],

     "message" => [
          "is-empty"                  => "سبد خرید خالی‌ست.",
          "order-address-description" => "این سفارش به :receiver به نشانی :address و شماره تماس :telephone تحویل می‌گردد.",
          "order-courier-description" => "تحویل توسط :courier ",
          "cart-changed"              => "سبد خرید شما تغییر کرده‌است.",
          "select-gateway-hint"       => "درگاه مورد نظر خود را انتخاب کنید. از طریق تمام کارت‌های بانکی عضو شتاب می‌توانید پرداخت خود را انجام دهید.",
          "thanks-for-purchasement"   => "از خرید شما سپاس‌گذاریم.",
          "pending-cart"              => [
               "sms" => "سبد خرید شما تکمیل نشده برای تکمیل به سایت مراجعه کنید.",
          ],
          "unpaid-cart"               => [
               "sms" => "سفارش شما در انتظار پرداخت است. برای پرداخت به آدرس زیر مراجعه کنید.",
          ],
          "min-payable-amount"        => "در صورت انتخاب پرداخت نقدی، باید مبلغ :amount را به صورت آنلاین پرداخت کنید.",
          "have-coupon-code"          => "بن تخفیف دارید؟",
          "coupon-code-is-applied"    => "کد تخفیف اعمال شده‌است.",
     ],

     "alert" => [
          "success" => [
               "order-remove"        => "محصول با موفقیت حذف شد.",
               "order-add"           => "محصول به سبد خرید اضافه شد.",
               "payment"             => "سفارش شما با موفقیت ثبت و پرداخت شد. از شما متشکریم.",
               "coupon-applied"      => "کد تخفیف پذیرفته شد.",
               "coupon-removed"      => "کد تخفیف حذف شد.",
               "tracking-cart-found" => "سفارش پیدا شد. منتظر بمانید...",
          ],
          "error"   => [
               "order-remove"                 => "حذف محصول با مشکل مواجه شد.",
               "order-add"                    => "اضافه کردن محصول به سبد خرید با خطا مواجه شد. مجددا تلاش کنید.",
               "required-selection"           => "انتخاب :attribute الزامی است.",
               "payment"                      => "متاسفانه پرداخت ناموفق بود!",
               "unable-to-verify"             => "امکان تایید این تراکنش وجود ندارد.",
               "rejected-by-gateway"          => "تراکنش از طرف درگاهی بانکی تایید نشد.",
               "system-error-in-purchasement" => "به دلیل خطای سیستمی در حال حاضر امکان اتمام فرایند خرید وجود ندارد.",
               "too-few-amount"               => "مبلغ وارد شده خیلی کم است.",
               "coupon-code-is-not-valid"     => "کد تخفیف معتبر نیست.",
               "tracking-cart-not-found"      => "سفارش پیدا نشد. از درست بودن کد پیگیری مطمئن شوید.",
          ],
     ],

     "button" => [
          "add_to_compare"     => "افزودن به مقایسه",
          "back-to-shop"       => "بازگشت به فروشگاه",
          "back-to-cart"       => "بازگشت به سبد خرید",
          "add"                => "افزودن به سبد",
          "flush"              => "خالی کردن سبد",
          "settlement"         => "تسویه حساب",
          "confirm-and-review" => "تایید و بازبینی سفارش",
          "confirm-and-pay"    => "تایید و پرداخت",
          "purchase-online"    => "پرداخت آنلاین",
          "apply-coupon-code"  => "ثبت کد",
          "remove-coupon-code" => "حذف کد",
     ],

     "price" => [
          "unit"                => "قیمت واحد",
          "total"               => "قیمت کل",
          "your-purchase-total" => "جمع خرید شما",
          "purchasable"         => "قابل پرداخت",
          "delivery"            => "هزینه‌ی تحویل",
          "free"                => "رایگان",
     ],

     "headline" => [
          "discount"                   => "تخفیف",
          "tax"                        => "مالیات",
          "shipping-info"              => "اطلاعات ارسال سفارش",
          "review-order"               => "بازبینی سفارش",
          "address-and-courier"        => "نشانی و نوع تحویل",
          "account-number"             => "شماره حساب",
          "card-number"                => "شماره کارت",
          "bank"                       => "بانک",
          "account-owner"              => "صاحب حساب",
          "tracking-code"              => "شماره پیگیری",
          "declaration-date"           => "تاریخ واریز",
          "delivery-info"              => "اطلاعات ارسالی سفارش",
          "receiver"                   => "دریافت‌کننده",
          "order-status-summary"       => "خلاصه وضعیت سفارش",
          "receipt-number"             => "شماره رسید",
          "order-status"               => "وضعیت سفارش",
          "your-purchasements-details" => "جزییات پرداخت‌های شما",
          "gateway"                    => "درگاه پرداخت",
          "payment-type"               => "نوع پرداخت",
          "amount"                     => "مبلغ",
          "order-details"              => "جزییات سفارش",
          "details-of-order"           => "جزییات سفارش :order",
          "min-payable-amount"         => "حداقل مبلغ پرداخت",
     ],

     "payment" => [
          "title"  => [
               "singular" => "پرداخت",
          ],
          "status" => "وضعیت پرداخت",

          "status-levels" => [
               "refunded"        => "برگشت‌داده‌شده",
               "canceled"        => "لغوشده",
               "cancelled"       => "لغوشده",
               "rejected"        => "ردشده",
               "verified"        => "تأییدشده",
               "declared"        => "در انتظار تأیید",
               "sent-to-gateway" => "در انتظار درگاه",
               "created"         => "پرداخت‌نشده",
          ],

          "message" => [
               "unsuccess-online-hint"       => "در صورتی که طی این فرایند مبلغی از حساب شما کسر شده است، طی ۷۲ ساعت آینده به حساب شما باز خواهد گشت.",
               "select-gateway-and-purchase" => "یکی از درگاه‌ها را انتخاب و مبلغ سفارش را پرداخت نمایید تا سفارش شما تکمیل شد.",
          ],
     ],

     "courier" => [
          "title" => [
               "singular" => "روش ارسال",
               "plural"   => "روش‌های ارسال",
          ],

          "select" => "انتخاب روش ارسال",
     ],

     "package" => [
          "title" => [
               "singular" => "بسته‌بندی",
               "plural"   => "بسته‌بندی‌ها",
          ],
     ],

     "discount" => [
          "i-have-code"   => "کد تخفیف دارم.",
          "register-code" => "ثبت کد تخفیف",
          "message"       => [
               "register-and-submit" => " لطفا کد تخفیف را وارد کرده و دکمه ثبت را کلیک کنید. ",
          ],
     ],

     "payment-method" => [
          "title" => [
               "plural" => "شیوه‌های پرداخت",
          ],

          "option" => [
               "online"  => "پرداخت آنلاین",
               "deposit" => "واریز به حساب",
               "shetab"  => "کارت به کارت",
               "cash"    => "پرداخت در محل",
          ],
     ],

     "tracking" => [
          "title" => "پیگیری سفارش",
          "code"  => "کد سفارش",
     ],


     "i-need-official-bill"          => "فاکتور رسمی می‌خواهم.",
     "is-required-for-official-bill" => "وارد کردن «:attribute» برای صدور فاکتور رسمی الزامی است.",

     "delivery"                 => [
          "delivery_method"                => "روش تحویل کالای سنگین",
          "delivery_method_Next_vehicle"   => "محل تخلیه در کنار وسیله نقلیه",
          "delivery_method_Distance_place" => "محل تخلیه کالا دور است",
          "delivery_method_floor"          => "محل تخلیه در طبقات",
          "delivery_method_ground_floor"   => "محل تخلیه در طبقه همکف",
          "from"                           => "از",
          'note'                           => [
               'floor'    => 'به ازای هر طبقه مبلغی اضافه خواهد شد',
               'distance' => 'به ازای هر وسیله سنگین «:price» تومان',
               'free'     => 'رایگان',
          ],
          "title"                          => [
               "singular" => "روش تحویل",
          ],
     ],
     "factor"                   => "درخواست فاکتور",
     "delivery_time"            => "زمان تحویل",
     "work_day"                 => "روز تحویل",
     "is-required-for-delivery" => "وارد کردن «:attribute» الزامی است.",
     "description"              => "توضیحات",

];
