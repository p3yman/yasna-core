<?php
return [

     "checkout_sms_buyer"         => "فروشگاه گاچت\n مشتری گرامی سفارش شما با کد :code با موفقیت ذخیره گردید و درحال بررسی است.",
     "checkout_sms_manager"       => "فروشگاه گاچت\n سفارشی با کد :code ثبت گردید.",
     "confirmed_sms_buyer"        => "فروشگاه گاچت\n مشتری گرامی سفارش شما با کد :code تایید گردید و در حال بسته‌بندی می‌باشد.\n جهت اطلاع از روند سفارش خود به قسمت پیگیری سفارش در سایت مراجعه نمایید.",
     "is_sent_sms_buyer"          => "فروشگاه گاچت\n مشتری گرامی سفارش شما با کد :code درحال ارسال است.",
     "is_not_confirmed_sms_buyer" => "فروشگاه گاچت\n مشتری گرامی سفارش شما با کد :code تایید نگردید، کارشناسان گاچت با شما تماس خواهند گرفت.",
     "is_delivered_sms_buyer"     => "فروشگاه گاچت\n مشتری سفارش شما با کد :code تحویل داده شد.\n خواهشمند است انتقادات و پیشنهادات خود را در بخش تماس باما ارسال نمایید.",
     "failed_payment"             => "مشتری گرامی فاکتور شما با کد :code ثبت و در وضعیت پرداخت متوقف شده است، خواهشمند است نسبت به پرداخت سفارش خود اقدام نمایید.\nفروشگاه گاچت",
];
