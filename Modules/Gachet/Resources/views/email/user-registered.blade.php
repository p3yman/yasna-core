@extends($__module->getBladePath('email.template.email_frame'))

@section('email_content')
	@php
		$trans_path = $__module->getTransPath('user.message.register-success-email');
		$message_parts_keys = array_keys(trans($trans_path));
	@endphp

	@foreach($message_parts_keys as $index => $key)
		@if($index != 0) <br/> @endif
		@php
			$part_path = $trans_path . '.' . $key;
			$replace = [
				'name' => $user->full_name,
				'site' => $site_title
			];
		@endphp

		{{ trans($part_path, $replace) }}
	@endforeach

	<br>

	<a href="{{ $site_url }}">{{ $site_title }}</a>
@endsection
