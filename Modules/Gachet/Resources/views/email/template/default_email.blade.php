@extends($__module->getBladePath('email.template.email_frame'))

@section('email_content')
    {!! $content !!}
@endsection
