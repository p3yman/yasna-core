<div class="container">
	@if($posts and $posts->count())
		<div class="col-xs-12">
			@foreach($posts as $post)
				@include($__module->getBladePath($viewFolder . '.item'))
			@endforeach
		</div>
		<div class="col-xs-12 text-center">
			{!! $posts->render() !!}
		</div>

	@else

		<div class="col-xs-12">
			<div class="alert">
				{{ $__module->getTrans('general.nothing-to-show') }}
			</div>
		</div>

	@endif
</div>
