<div class="blog-item style-1">
    @php
        $post->spreadMeta();
        $postImage = $__module->getAsset("images/image-not-found.png");
        if($post->featured_image) {
            $dbImage = doc($post->featured_image)
                ->posttype($post->posttype)
                ->config('featured_image')
                ->version('211x139')
                ->getUrl();
            if($dbImage) {
                $postImage = $dbImage;
            }
        }
    @endphp
    <div class="thumbnail"><img src="{{ $postImage }}"></div>
    <div class="content">
        <a href="{{ $post->direct_url }}"><h3 class="title"> {{ $post->title }} </h3></a>
        <div class="excerpt">
            <p>
                {{ $post->abstract }}
            </p>
        </div>
        <div class="action">
            <a href="{{ $post->direct_url }}" class="more">{{ $__module->getTrans('general.continue-reading') }}
                <span class="icon-angle-right"></span>
            </a>
        </div>
    </div>
</div>
