<div class="col-sm-12">
	@foreach($posts as $post)
		<div class="faq-item">
			<a href="#" class="q">{{ $post->long_title }}</a>
			<div class="answer">
				<p>{{ $post->abstract }}</p>
			</div>
		</div>
	@endforeach

	@include($__module->getBladePath('posts.list.faq.new'))

</div>
