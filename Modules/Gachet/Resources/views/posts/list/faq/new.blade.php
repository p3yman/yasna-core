@php
	$commenting_post = gachet()->findCommentingPostWithSlug('faq');
@endphp

@if ($commenting_post->not_exists)
	@php return @endphp
@endif

@php
	$fields = gachet()->commenting()->translateFields($commenting_post->spreadMeta()->fields);
@endphp
<div class="faq-item">
	<a href="#" class="q">{{ $commenting_post->title2 }}</a>
	<div class="answer">
		<form action="{{ route('gachet.comment.save') }}" id="contact-form" class="js"
			  method="post">
			{{ csrf_field() }}
			<input type="hidden" name="post_id" value="{{ $commenting_post->hashid }}">
			<div class="row">
				@foreach($fields as $field_name => $field_value)
					@switch($field_name)
						@case('name')
						@case('email')
						@case('mobile')
						<div class="col-md-4">
							<div class="field">
								<label>{{ trans('validation.attributes.' . $field_name) }}</label>
								<input type="text" name="{{ $field_name }}">
							</div>
						</div>
						@break

						@case('subject')
						<div class="col-md-12">
							<div class="field">
								<label>{{ trans('validation.attributes.' . $field_name) }}</label>
								<input type="text" name="{{ $field_name }}">
							</div>
						</div>
						@break

						@case('question')
						<div class="col-xs-12">
							<div class="field">
								<label>{{ $__module->getTrans('validation.attributes.question') }}</label>
								<textarea rows="4" name="{{ $field_name }}"></textarea>
							</div>
						</div>
						@break
					@endswitch
				@endforeach

				<div class="col-xs-12">
					<div class="contact-action tal">
						<button class="green">{{ $__module->getTrans('form.button.send') }}</button>
						<div class="result" style="text-align: right">
							@include($__module->getBladePath('layouts.widgets.form.feed'))
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@include($__module->getBladePath('layouts.widgets.form.scripts'))

