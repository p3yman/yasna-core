@php $level = ($level ?? 0) @endphp

<div class="checkbox blue mb10" style="padding-right: {{ $level * 20 }}px">
	<input id="category-{{ $category->slug }}" value="{{$category->slug}}" type="checkbox"
		   data-hashid="{{ $category->hashid }}" checked="checked"
		   @isset($parent_category) data-parent="category-{{ $parent_category->slug }}" @endisset>
	<label for="category-{{ $category->slug }}"> {{ $category->title }} </label>
</div>

@php $children = $category->children()->get() @endphp
@foreach($children as $child)
	@include($__module->getBladePath('posts.list.product.filter-categories-item'), [
		'parent_category' => $category,
		'category' => $child,
		'level' => $level + 1,
	])
@endforeach
