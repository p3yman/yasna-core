@php
	// @todo: read best version of product photo depending on ui
	$post->spreadMeta();
	$posttype = $post->posttype()->spreadMeta();
	$postImage = $__module->getAsset("images/image-not-found.png");
	if(
		$post->featured_image and
		($dbImage = doc($post->featured_image)
			->posttype($posttype)
			->config('featured_image')
			->version('296x172')
			->getUrl())
		) {
		$postImage = $dbImage;
	} else {if ($posttypeImage = doc($posttype->default_featured_image)
			->posttype($posttype)
			->config('featured_image')
			->version('235x198')
			->getUrl()) {
		$postImage = $posttypeImage;
	}}

	$isOnSale = $post->ware_original_price > $post->ware_sale_price;
@endphp
<div class="col-lg-4 col-sm-4 filterable"
	 data-category="{{ implode(',', $post->labels()->pluck('slug')->toArray()) }}"
	 data-available="{{ ((int) $post->is_available) }}"
	 data-special-sale="{{ ((int) ($isOnSale)) }}"
	 data-title="{{ $post->title }}"
	 data-price="{{ $post->current_price }}">
	{{--<a href="#" class="product-item ribbon-new ribbon-sale">--}}
	<a href="{{ $post->direct_url }}"
	   class="product-item {{--@if($post->isIt('NEW')) ribbon-new @endif--}} @if($post->on_sale) ribbon-sale @endif">
		<div class="thumbnail"><img src="{{ $postImage }}">
		</div>
		<div class="content">
			<h6>
				{{ $post->title }}
				@if(user()->isDeveloper())
					- {{ $post->id }}
				@endif
			</h6>
			@if($post->ware_original_price > 0)
				<div class="price">
					@if($post->is_available)
						@if($isOnSale)
							@php
								$off = 100 - round(($post->ware_sale_price / $post->ware_original_price), 2) * 100;
							@endphp
							<del>
								{{ gachet()->shop()->showPrice($post->ware_original_price, false) }}
							</del>

							<span class="label alt red fw-b">
								{{ pd($off) . $__module->getTrans('general.percent_sign') }}
							</span>

							<br>
							<ins>
								{{ gachet()->shop()->showPrice($post->ware_sale_price) }}
							</ins>

						@else
							<ins>
								{{ gachet()->shop()->showPrice($post->ware_original_price) }}
							</ins>
						@endif
					@else
						<div class="status">
							<div class="label red"> {{ $__module->getTrans('post.product.is-not-available') }}</div>
						</div>
					@endif
				</div>
			@endif
			@if(ShopTools::postShopWareCustomCode($post->id))
				<div class="status">
					<div class="meta-title"> {{ $__module->getTrans('post.product.code') }}
						<div class="label blue"> {{ ad(ShopTools::postShopWareCustomCode($post->id)) }}</div>
					</div>
				</div>
			@endif
		</div>
	</a>
</div>
