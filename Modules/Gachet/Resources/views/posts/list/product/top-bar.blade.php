<header id="category-header">
    {{--        @include('front.posts.general.search-form')--}}
    <div class="field sort"><label> {{ $__module->getTrans('filter.sort.title') }} </label>
        <div class="select rounded">
            <select class="ajax-sort">
                <option data-identifier="publishing" data-value="des">
                    {{ $__module->getTrans('filter.sort.new') }}
                </option>
                {{--<option data-identifier="code" data-value="desc">--}}
                    {{--{{ $__module->getTrans('filter.sort.code.max-to-min') }}--}}
                {{--</option>--}}
                {{--<option data-identifier="code" data-value="asc">--}}
                    {{--{{ $__module->getTrans('filter.sort.code.min-to-max') }}--}}
                {{--</option>--}}
                <option data-identifier="price" data-value="desc">
                    {{ $__module->getTrans('filter.sort.price.max-to-min') }}
                </option>
                <option data-identifier="price" data-value="asc">
                    {{ $__module->getTrans('filter.sort.price.min-to-max') }}
                </option>
                {{--<option> {{ trans('front.best_seller') }} </option>--}}
                {{--<option> {{ trans('front.favorites') }} </option>--}}
            </select>
        </div>
    </div>
</header>
