@include($__module->getBladePath('layouts.widgets.form.scripts'))

@section('html-footer')
    {!! Html::script($__module->getAsset('js/timer.min.js')) !!}
    {!! Html::script($__module->getAsset('js/ajax-filter.min.js')) !!}

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    </script>
@append
