@if (isset($posts))
	@php
		$posttype = ($posts->first()->posttype ?? posttype());
	@endphp
@else
	@php
		$posttype = ($allPosts->first()->posttype ?? posttype());
	@endphp
@endif

@include($__module->getBladePath($viewFolder . '.content'))
