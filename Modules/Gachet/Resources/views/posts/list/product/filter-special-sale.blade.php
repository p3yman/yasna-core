<div class="field with-switch filter-switch filter-switch-checkbox" data-identifier="special-sale">
    <label for="switch-a-5"> {{ $__module->getTrans('filter.sale-type.special') }}</label>
    <div class="switch d-ib blue">
        <input id="special_sale" type="checkbox">
        <label for="special_sale"></label>
    </div>
</div>
