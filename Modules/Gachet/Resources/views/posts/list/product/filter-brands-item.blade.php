@php $level = ($level ?? 0) @endphp

<div class="checkbox blue mb10" style="padding-right: {{ $level * 20 }}px">
	<input id="brand-{{ $brand->slug }}" value="{{$brand->slug}}" type="checkbox"
		   data-hashid="{{ $brand->hashid }}" checked="checked"
		   @isset($parent_brand) data-parent="brand-{{ $parent_brand->slug }}" @endisset>
	<label for="brand-{{ $brand->slug }}"> {{ $brand->title }} </label>
</div>

{{--@php $children = $brand->children()->get() @endphp--}}
{{--@foreach($children as $child)--}}
	{{--@include($__module->getBladePath('posts.list.product.filter-brands-item'), [--}}
		{{--'parent_brand' => $brand,--}}
		{{--'brand' => $child,--}}
		{{--'level' => $level + 1,--}}
	{{--])--}}
{{--@endforeach--}}
