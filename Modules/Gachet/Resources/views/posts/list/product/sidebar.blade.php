@php $ajaxFilter = true @endphp
@if($ajaxFilter)
	@include($__module->getBladePath($viewFolder . '.ajax-filter-assets'))
@else
	@include($__module->getBladePath($viewFolder . '.filter-assets'))
@endif

<div class="col-sm-3 sidebar-filter">
	<button class="block blue filter-opener" onclick="filterOpener()">
		<i class="fa fa-filter" aria-hidden="true"></i>
		{{ $__module->getTrans('filter.title.plural') }}
	</button>

	@if (isset($root_category))
		@php
			$filter_url = route('gachet.products.filter', ['label' => $root_category->slug]);
		@endphp
	@else
		@php
			$filter_url = route('gachet.products.offer.filter', ['offer' => $offer->slug]);
		@endphp
	@endif
	<div class="filter-container">
		<div class="category-filters filters-panel"
			 data-filter-url="{{ $filter_url }}">
            <span class="filter-panel--close" onclick="filterCloser()">
                <em class="icon-cancel"></em>
            </span>
			<div class="title"> {{ $__module->getTrans('filter.title.plural') }}</div>
			<article>
				@include($__module->getBladePath($viewFolder . '.filter-title'))
				@include($__module->getBladePath($viewFolder . '.filter-price'))
				@include($__module->getBladePath($viewFolder . '.filter-categories'))
				@include($__module->getBladePath($viewFolder . '.filter-brands'))
				@include($__module->getBladePath($viewFolder . '.filter-packages'))
				@include($__module->getBladePath($viewFolder . '.filter-availability'))
				@include($__module->getBladePath($viewFolder . '.filter-special-sale'))
				<hr class="small">
				@include($__module->getBladePath($viewFolder . '.filter-refresh'))

				<button class="block blue mt20 filter-submit" onclick="filterCloser()">
					{{ $__module->getTrans('filter.option.filtering') }}
				</button>
			</article>
		</div>
	</div>

</div>
