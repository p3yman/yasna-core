@if(!$ajaxRequest)
    @include($__module->getBladePath($viewFolder . '.top-bar'))
    <div class="row">
    @if($showFilter)
        @include($__module->getBladePath($viewFolder . '.sidebar'))
        <div class="col-sm-9 has-dialog">
    @else
        <div class="col-sm-12 has-dialog">
    @endif
@endif

@include($__module->getBladePath($viewFolder . '.list'))

@if(!$ajaxRequest)
            <div class="alert alert-danger" id="hidden-alert" style="display: none; position: absolute; top: 0; z-index: 999">
                {{ $__module->getTrans('form.feed.error') }}
            </div>
        </div>
        @include($__module->getBladePath('layouts.widgets.dialog.loading'), [
                'id' => 'loading-dialog'
            ])
    </div>
@endif
