<div class="text-center">
    <a class="green button-link" href="#">
        <i class="fa fa-refresh"></i>
        {{ $__module->getTrans('filter.option.refresh') }}
    </a>
</div>
