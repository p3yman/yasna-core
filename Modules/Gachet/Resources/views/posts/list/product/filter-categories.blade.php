@if (!isset($root_category) or $root_category->not_exists)
    @php return @endphp
@endif

@php $allCategories = $root_category->children()->get() @endphp
@if(count($allCategories) > 1)
	<hr class="small">
	<div class="field mb0 filter-checkbox" data-identifier="category">
		<label class="label-big">
			{{ $__module->getTrans('general.categories') }}
		</label>
		@foreach($allCategories as $category)
			@include($__module->getBladePath('posts.list.product.filter-categories-item'), [
				'category' => $category,
			])
		@endforeach
	</div>
@endif
