@php
	/** @var Products $products */
	$products = app('gachet.shop.products');
	$folder = $products->brandsLabelFolder();
@endphp

@if ($folder->not_exists)
    @php return @endphp
@endif

@php
	$brands = $posttype->brands
@endphp

@if(count($brands) > 1)
	<hr class="small">
	<div class="field mb0 filter-checkbox" data-identifier="brand">
		<label class="label-big">
			{{ $__module->getTrans('general.brands') }}
		</label>
		@foreach($brands as $brand)
			@include($__module->getBladePath('posts.list.product.filter-brands-item'), [
				'brand' => $brand
			])
		@endforeach
	</div>
@endif
