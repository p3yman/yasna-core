<div class="product-list result-container">
    @if(!$isBasePage)
        <div class="row">
            @if($posts and $posts->count())
                @foreach($posts as $index => $post)
                    {{--@if(($index % 4) == 0)
                        <div class="row">
                            <div class="col-xs-12">
                                @endif--}}
                    @include($__module->getBladePath($viewFolder . '.item'))
                    {{--@if(($index % 4) == 3)
                </div>
            </div>
        @endif--}}
                @endforeach
            @else
                <div class="row">
                    <div class="col-xs-12 pr30 pl30 text-center">
                        <div class="alert alert-danger">
                            {{ $__module->getTrans('post.product.no-result-found') }}
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="pagination-wrapper mt20">
            {!! $posts->render() !!}
        </div>
    @endif
</div>
