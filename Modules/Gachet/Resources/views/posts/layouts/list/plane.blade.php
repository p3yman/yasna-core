@extends($__module->getBladePath('layouts.plane'))

@php
    gachet()->template()->appendToPageTitle($posttype->titleIn(getLocale()));
    gachet()->template()->appendToNavBar([
        $posttype->titleIn(getLocale()),
        ($posttype->slug != 'products')
            ? RouteTools::getPosttypeRoute($posttype->slug)
            : RouteTools::getPosttypeCategoriesRoute('products')
    ]);

    $category_to_show = ($root_category ?? null);
    if ($category_to_show and $category_to_show->exists) {
        gachet()->template()->appendToPageTitle($category_to_show->titleIn(getLocale()));
        gachet()->template()->appendToNavBar([
            $category_to_show->titleIn(getLocale()), route('gachet.products.list', ['label' => $category_to_show->slug])
        ]);
    }
@endphp

@isset($offer)
    @php
        gachet()->template()->appendToPageTitle($offer->title);
        gachet()->template()->appendToNavBar([
            $offer->title,
            request()->url()
        ]);
    @endphp
@endisset

@section('body')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="@if(isset($fullPage) and $fullPage) col-xs-12 @else col-md-11 col-center @endif">
                    {!! $list !!}
                </div>
            </div>
        </div>
    </div>
@append

