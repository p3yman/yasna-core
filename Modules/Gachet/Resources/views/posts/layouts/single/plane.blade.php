@extends($__module->getBladePath('layouts.plane'))

@php
    $posttype = $post->posttype;
    gachet()->template()->appendToPageTitle($posttype->titleIn(getLocale()));
    gachet()->template()->appendToPageTitle($post->title);
    $posttype_url = ($posttype->slug == gachet()->getPosttypeSlug('product'))
        ? RouteTools::getPosttypeCategoriesRoute($posttype->slug)
        : RouteTools::getPosttypeRoute($posttype->slug);
        
    gachet()->template()->appendToNavBar([
        $posttype->titleIn(getLocale()), $posttype_url
    ]);
    gachet()->template()->appendToNavBar([
        $post->title, request()->url()
    ]);
    if ($post->getMeta('show_header')) {
        if ($postCover = doc($post->getMeta('header_image'))->getUrl()) {
            $coverSrc = $postCover;
        } else if(
            ($defaultCategory = $post->default_category_model) and
            $categoryImage = doc($defaultCategory->imageIn(getLocale()))->getUrl()
        ) {
            $coverSrc = $categoryImage;
        }
    }
    $cover_img = boolval($post->getMeta('show_header') and isset($coverSrc));
@endphp

@section('body')
    @php $viewVariables = gachet()->template()->viewVariables(); @endphp
    @if(!$viewVariables or !is_array($viewVariables))
        @php $viewVariables = [] @endphp
    @endif

    @if($cover_img)
        @include($__module->getBladePath('posts.layouts.single.cover'),[
            "cover" => [
                "src" => $coverSrc ,
                "alt" => "cover" ,
            ] ,
        ])
    @endif
    <div class="page-content
            @if($cover_img) cover-layout @endif
            @if(array_key_exists('mainContentClass', $viewVariables)) {{ $viewVariables['mainContentClass'] }} @endif">
        <div class="container">
            {!! $post_view !!}
        </div>
    </div>
@append
