@extends($__module->getBladePath('layouts.plane'))

@php
    gachet()->template()->appendToPageTitle($posttype->titleIn(getLocale()));
    gachet()->template()->appendToNavBar([
        $__module->getTrans('general.categories-of-something', ['something' => $posttype->titleIn(getLocale())]),
        request()->url(),
    ]);
@endphp

@section('body')
    <div class="page-content">
        <div class="container">
            <div class="part-title">
                <h3>
                    @php
                        $partTitleParts = $__module->getTrans("general.categories-of-something-partial");
                        $partTitleParts = str_replace(":something", $posttype->titleIn(getLocale()), $partTitleParts);
                    @endphp
                    {{ $partTitleParts['part1'] }}
                    <span>{{ $partTitleParts['part2'] }}</span>
                    {{ $partTitleParts['part3'] }}
                </h3>
            </div>
            <div class="cats">
                <div class="row">
                    {!! $categories_view !!}
                </div>
            </div>
        </div>
    </div>
@append
