<div class="col-md-4 col-sm-6">
    <a href="{{ route('gachet.products.list', ['label' => $cat->slug]) }}" class="category-item">
        @php
            $default_image = $__module->getAsset("images/image-not-found.png");
            // todo: show folder image

            $cat_image_hashid = ($cat->spreadMeta()->custom['pic'][0] ?? null);
            $cat_image_file = uploader()->file($cat_image_hashid);

            $cat_image = $cat_image_file->exists ? $cat_image_file->getVersionLink('thumb') : $default_image;
        @endphp
        <img src="{{ $cat_image }}">

        <div class="cat-name">
            <svg width="100%" height="100%" viewBox="0 0 220 42" version="1.1"
                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <polygon id="Rectangle" stroke="none" fill-opacity="0.802026721" fill="#31415B"
                         fill-rule="evenodd" points="42 0 220 0 220 42 0 42"></polygon>
            </svg>
            <span> {{ $cat->titleIn(getLocale()) }} </span></div>
        <div class="more">
            <svg width="100%" height="100%" viewBox="0 0 220 42" version="1.1"
            xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <polygon id="Rectangle" stroke="none" fill-opacity="0.802026721" fill="#000"
            fill-rule="evenodd"
            points="0 0 220 0 178 42 3.55271368e-15 42"></polygon>
            </svg>
            <span> {{ $__module->getTrans('post.product.view-plural') }} </span></div>
    </a>
</div>
