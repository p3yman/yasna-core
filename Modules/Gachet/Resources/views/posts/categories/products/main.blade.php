@php
    $categories = app('gachet.posts.labels')->rootCategoriesLabels($posttype);
@endphp

@foreach($categories as $cat)
    @include($__module->getBladePath('posts.categories.products.item'))
@endforeach
