@php
    $chart_data = gachet()->shop()->generateProductChartData($post);
	$id= 'prices_graph';
	$labels = $chart_data['labels'];
    $data = $chart_data['data'];
@endphp

<div class="chart-parent">
	<canvas id="{{ $id or '' }}" style="height: 400px; width: {{ $width or "" }}"></canvas>
</div>



@section('html-footer')
	{!! Html::script(Module::asset('manage:libs/vendor/Chart.js/dist/Chart.js')) !!}

	<!-- Chart.js Scripts -->

	<script>

        $(document).on('click', 'a[href=#prices]',function () {
            console.log("clicked");

            var lineData = {
                labels: {!! json_encode($labels) !!},
                datasets: {!! json_encode($data) !!}
            };

            var lineOptions = {
                legend: {
                },
                tooltips: {
                    callbacks: {
                        // use label callback to return the desired label
                        label: function(tooltipItem, data) {
                            return tooltipItem.xLabel + " :" + ad(addCommas(tooltipItem.yLabel));
                        },
                        // remove title
                        title: function(tooltipItem, data) {
                            return;
                        }
                    }
                },
                maintainAspectRatio: false ,
                elements: {
                    line: {
                        tension: 0
                    }
                }
            };

            Chart.defaults.global.defaultFontFamily = 'IRANSans';
            var linectx = document.getElementById('{{ $id or "" }}').getContext('2d');
            var lineChart = new Chart(linectx, {
                data: lineData,
                type: 'line',
                options: lineOptions
            });
        });

	</script>
@append
