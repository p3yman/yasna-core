@php
    $selectedWare = $selectedWare->spreadMeta()->inLocale(getLocale())->in($previewCurrency);
    $unit = $selectedWare->unit;
@endphp
@if ($wares->count() > 1)
    {{--<h3 class="product-name">{{ $selectedWare->titleIn(getLocale()) }}</h3>--}}
@endif

@foreach($wares as $ware)
    @php
        $sale_price = $ware->getGachetClientPrice('sale');
		$original_price = $ware->getGachetClientPrice('original');
		$sale_price_to_show = gachet()->shop()->priceToShow($sale_price);
		$original_price_to_show = gachet()->shop()->priceToShow($original_price);
		$on_sale = ($original_price > $sale_price);
    @endphp
    @php($ware = $ware->spreadMeta()->inLocale(getLocale())->in($previewCurrency))

    @if($ware->checkAvailability())
    <div class="ware-detail" data-ware-id="{{ $ware->hashid }}" style="display: none;">

        <div class="excerpt mt15"><p>{{ $ware->remarkIn(getLocale()) }}</p></div>

        <hr/>

        @include($__module->getBladePath($viewFolder . '.options'))

        @if($sale_price)
        <div class="price">
            @if($on_sale)
                <ins>
                    {{ ad(number_format($sale_price_to_show)) }}
                    {{ $previewCurrencyTrans }}
                </ins>
                <del>
                    {{ ad(number_format($original_price_to_show)) }}
                    {{ $previewCurrencyTrans }}
                </del>
            @else
                <ins>
                    {{ ad(number_format($original_price_to_show)) }}
                    {{ $previewCurrencyTrans }}
                </ins>
            @endif
            <div class="status">
                <div class="label green"> {{ $__module->getTrans('post.product.is-available') }}</div>
            </div>
        </div>
        @endif
        @if($ware->isPurchasable())
            <div class="field add-to-card mt20">
                {{--<label>تعداد</label>--}}
                <div class="row">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-xs-6 mb5">
                                <input class="ware-count product-counter @if(!$unit->is_discrete) float @endif" value="{{ ad(1) }}"
                                       min="1" data-ware="{{ $ware->hashid }}">
                            </div>
                            <div class="col-xs-6 mb5">
                                <div class="add-label">
                                    <div class="content">
                                        <h4>{{ $unit->title }}</h4>
                                    </div>
                                </div>
                                {{--<label>تعداد</label>--}}
                                {{--<div class="select">--}}
                                {{--<select>--}}
                                {{--<option value="1">کیلو</option>--}}
                                {{--<option value="2">بسته</option>--}}
                                {{--</select>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-6 mb5">
                                <a href="#" class="button block blue add-to-cart" data-ware="{{ $ware->hashid }}">
                                    {{ $__module->getTrans('cart.button.add_to_compare') }}
                                </a>
                            </div>
                            <div class="col-md-6 mb5">
                                <button class="block green add-to-cart" data-ware="{{ $ware->hashid }}">
                                    {{ $__module->getTrans('cart.button.add') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>
    @else
    <div class="ware-detail" data-ware-id="{{ $ware->hashid }}" style="display: none;">
        <div class="excerpt"><p>{{ $ware->remarkIn(getLocale()) }}</p></div>
        <hr>
        <div class="price">
            <div class="status">
                <div class="label red"> {{ $__module->getTrans('post.product.is-not-available') }}</div>
            </div>
        </div>
    </div>
    @endif

@endforeach

