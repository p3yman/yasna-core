@section('radios')

	@php $checked = ($ware->id == $selected_ware->id) @endphp

	<div class="radio {{--d-ib--}} d-n" >
		<input id="{{ $ware->hashid }}" type="radio" name="ware" @if($checked) checked @endif
		class="js-ware-radio" data-identifier="{{ $ware_identifier }}">
		<label for="{{ $ware->hashid }}">
			{{ $ware->titleIn(getLocale()) }}
		</label>
	</div>
@append
