@php
	$product_meta = $post->getMeta();
@endphp
@if (isset($product_meta['on_sale']) and $product_meta['on_sale'] === "on")

@section('on-sale')
	ribbon-sale
@endsection

@endif

@if (boolval($ware->validOffers()->count()))

@section('special-offer')
	ribbon-offer
@endsection

@endif
