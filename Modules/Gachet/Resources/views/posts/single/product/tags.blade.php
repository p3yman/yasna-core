@php $tags = $post->tags @endphp
@if ($tags->count())
	<hr>
	<div class="row">
		<div class="col-md-1 col-sm-2">
			<div class="meta-title"
				 style="font-size: 12px !important;"> {{ $__module->getTrans('general.tag.title.plural') }}:
			</div>
		</div>
		<div class="col-md-11 sol-sm-10">
			<div class="meta-value">
				@foreach($tags as $tag)
					<div class="status" style="float: right; padding: 0px !important; margin: 1px !important;">
						<div class="label">
							<a style="font-size: 11px !important;" href="{{ $tag->direct_url }}" target="_blank">
								{{ $tag->slug }}
							</a>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endif