@php
	$groups_items = gachet()->shop()->getSpecsPivotParents($post);
@endphp

@if (!$groups_items->count())
	@php return @endphp
@endif

@php
	$groups = $groups_items->map(function ($items, $parent_id) use ($__module) {
		$parent_spec = model('spec', $parent_id);
		return [
			'title' => $parent_spec->title,
			'rows' => $items->map(function ($item) use ($__module) {
				$spec = $item->spec;
				if ($spec->input_type == 'boolean') {
					$path = $item->value ? 'yes' : 'no';
					$value = $__module->getTrans("general.$path");
				} else {
					$value = ad($item->value);
				}
				return [
					'title' => $item->spec->title,
					'cols' => [
						$value,
					],
				];
			})->toArray()
		];
	})->toArray();
@endphp

<div class="products-compare">
	<div class="table-responsive bd-n">
		<table class="table bordered">
			<tbody>

			@foreach($groups as $group)
				<tr class="row-title">
					<td colspan="100%">
						{{ $group['title'] }}
					</td>
				</tr>
				@foreach($group['rows'] as $group_row)
					<tr class="row-data">
						<td>
							{{ $group_row['title'] }}
						</td>
						@foreach($group_row['cols'] as $cell)
							<td>
								@if(is_bool($cell))
									<span class="{{ $cell? 'icon-check color-green': 'icon-close color-red' }} fz-lg"></span>
								@else
									{{ $cell }}
								@endif
							</td>
						@endforeach

					</tr>
				@endforeach
			@endforeach

			</tbody>

		</table>

	</div>


</div>
