@php
	$posttype = $post->posttype->spreadMeta();
	gachet()->template()->mergeWithViewVariables(['mainContentClass' => 'product-single']);
@endphp

@php
	$post->spreadMeta();
	$wares = $post->wares;
	$previewCurrency = CurrencyTools::getPreviewCurrency();
	$previewCurrencyTrans = $__module->getTrans('general.unit.currency.' . $previewCurrency);

	if(!isset($selected_ware) or !$selected_ware or !$selected_ware->exists) {
		$selected_ware = ($wares->first() ?? model('ware'));
	}

@endphp

@include($__module->getBladePath($viewFolder . '.wares-loop'))

<div class="row">
	<div class="col-sm-5">
		@include($__module->getBladePath($viewFolder . '.image'))
	</div>
	<div class="col-sm-7 product-detials" id="product_details">
		<h2 class="product-name mb0">{{ $post->title }}</h2>
		<h4 class="color-gray">{{ $post->title2 }}</h4>

		<hr>

		@include($__module->getBladePath($viewFolder . '.colors'))
		@include($__module->getBladePath($viewFolder . '.price'))
		@include($__module->getBladePath($viewFolder . '.add-to-cart'))
		<hr>
		@include($__module->getBladePath($viewFolder . '.meta'))
		@include($__module->getBladePath('layouts.widgets.dialog.loading'), [
				'id' => 'cart-adding-loading'
			])
	</div>
</div>


@include($__module->getBladePath('layouts.widgets.tiles'), [
    "monochrome" => true ,
])

<hr>

{{--@include($__module->getBladePath($viewFolder . '.content')--}}
{{--@include($__module->getBladePath($viewFolder. '.rating')--}}
{{--<hr>--}}
@include($__module->getBladePath($viewFolder . '.abstract'))
@include($__module->getBladePath($viewFolder . '.similar-items'))
@include($__module->getBladePath($viewFolder . '.tab-wrapper'), [
    "tabs" => gachet()->shop()->getProductSingleTabs($post)
])
@include($__module->getBladePath($viewFolder . '.similar-posts'))
@include($__module->getBladePath($viewFolder . '.scripts'))
@include($__module->getBladePath($viewFolder . '.notification'))
@include($__module->getBladePath($viewFolder . '.tags'))

