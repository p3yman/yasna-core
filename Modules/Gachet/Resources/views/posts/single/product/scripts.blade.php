@include($__module->getBladePath('layouts.widgets.form.scripts'))
@section('html-footer')
	<script>
        let token        = $('meta[name=csrf-token]').attr("content");
        let checkedColor = $('.color-selector .radio input:checked');


        $('document').ready(function () {
            let notification = new Notif({
                topPos    : 50,
                classNames: 'success danger',
            });
            $(document).on({
                click: function (e) {
                    e.preventDefault();
                    let that    = $(this);
                    let Loading = $('#cart-adding-loading');
                    let ware    = that.data('ware');
                    let count   = $('.ware-count[data-ware=' + ware + ']').val();
                    notification.hide();
                    Loading.show();
                    if (ware && count) {
                        $.ajax({
                            url    : "{{ route('gachet.cart.item.add') }}",
                            type   : "POST",
                            data   : {
                                _token: token,
                                hashid: ware,
                                count : count,
                            },
                            success: function () {
                                arguments[arguments.length] = $('.form');
                                reloadCart();
                                forms_responde.apply(arguments, Object.values(arguments));
                                Loading.hide().removeAttr('style');
                                notification.show('{{ $__module->getTrans('cart.alert.success.order-add') }}', 'success');
                            },
                            error  : function () {
                                //that.removeClass('disabled');
                                Loading.hide().removeAttr('style');
                                notification.show('{{ $__module->getTrans('cart.alert.error.order-add') }}', 'danger');
                            }
                        })
                    }
                },
            }, '.add-to-cart');

            $('.color-selector .radio input').on('change', function () {
                let radio  = $(this),
                    wareId = radio.val();
                $('[data-identifier]').hide();
                $('[data-identifier=' + wareId + ']').show();

            });

            if (checkedColor.length) {
                $('.color-selector .radio input:checked').change();
            } else {
                $('[data-identifier]').first().show();
            }


            $('.js-ware-radio').change(function () {
                let that             = $(this);
                let identifier       = that.data('identifier');
                let info_boxes       = $('.js-ware-info');
                let own_info_boxes   = info_boxes.filter('[data-identifier=' + identifier + ']')
                let other_info_boxes = info_boxes.not(own_info_boxes);

                own_info_boxes.show();
                other_info_boxes.hide();
            });

            $('.js-ware-radio:checked').trigger('change');

            let hash            = window.location.hash;
            let active_tab_link = $('.js-tabs').find('a[href="' + hash + '"]');
            if (active_tab_link.length) {
                let active_tab = active_tab_link.closest('li');
                let other_tabs = $('.js-tabs').children('li').not(active_tab);

                active_tab.addClass('active');
                other_tabs.removeClass('active');
            }

            $('.js-tabs').children('li.active').find('a').trigger('click');

            $(document).on({
                click: function (e) {
                    e.preventDefault();
                    let that = $(this);
                    let url  = that.data('url');

                    $.ajax({
                        url    : url,
                        success: function (rs) {
                            if (rs.success) {
                                $('.js-compare-link').show();
                                notification.show('{{ $__module->getTrans('general.added-to-compare') }}', 'success')
                            } else {
                                $('.js-compare-link').hide();
                                notification.show('{{ $__module->getTrans('general.unable-to-add-to-compare') }}', 'danger')
                            }
                        }
                    });

                },
            }, '.add-to-compare');
        });
	</script>
@append
