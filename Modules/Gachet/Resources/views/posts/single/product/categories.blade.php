@php
	$categories = $post->gachet_categories;
	$categories_to_show = $categories->filter(function ($category) use($categories) {
		// include only items which are not parent of any item in the list
		return !$categories
			->where('parent_id', $category->id)
			->first()
		;

	});
@endphp
@if ($categories_to_show->count())
	<div class="row">
		<div class="col-md-2 col-sm-3">
			<div class="meta-title"> {{ $__module->getTrans('general.category') }}:</div>
		</div>
		<div class="col-md-10 sol-sm-9">
			<div class="meta-value">
				<ul class="categories">
					@foreach($categories_to_show as $category)
						<li class="not-sp">
							<a target="_blank">
								{{ $category->parent()->title }}
								{{ $category->title }}
							</a>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
@endif
