@php $remainedWares = $wares->except($selectedWare->id) @endphp
@if($remainedWares->count())
    <div class="nano packaging">
        <div class="nano-content">
            @foreach($remainedWares as $ware)
                @php
                    $ware = $ware->inLocale(getLocale())->in($previewCurrency);
                    $unit = $ware->unit;
                @endphp
                <div class="package-item">
                    {{--@todo: uncomment this line and remove style of detail box--}}
                    {{--<div class="thumb"><img src="images/sample/product-item.jpg"></div>--}}
                    <div class="detail" style="padding-right: 0">
                        <h5 class="package-title">{{ $ware->title }}</h5>
                        <p>
                            {{ $ware->remarkIn(getLocale()) }}
                        </p>
                        <div class="actions">
                            <div class="right f-r">
                                <div class="add-label">
                                    <div class="content">
                                        <div class="price">
                                            @if($ware->isOnSale())
                                                <ins>
                                                    {{ ad(number_format($ware->salePrice()->withPackage()->in($previewCurrency)->get())) }}
                                                    {{ $previewCurrencyTrans }}
                                                </ins>
                                                <del>
                                                    {{ ad(number_format($ware->originalPrice()->withPackage()->in($previewCurrency)->get())) }}
                                                    {{ $previewCurrencyTrans }}
                                                </del>
                                            @else
                                                <ins>
                                                    {{ ad(number_format($ware->originalPrice()->in($previewCurrency)->withPackage()->get())) }}
                                                    {{ $previewCurrencyTrans }}
                                                </ins>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if ($ware->isPurchasable())
                                <div class="field f-l d-n">
                                    <div class="action">
                                        <label for="#" class="tar">
                                            @if ($unit->is_discrete)
                                                {{ $__module->getTrans('cart.numeration-label.number') }}
                                            @else
                                                @php $inputClass = 'float' @endphp
                                                {{ $__module->getTrans('cart.numeration-label.amount') }}
                                                (
                                                {{ $unit->title }}
                                                )
                                            @endif
                                        </label>
                                        <input class="ware-count product-counter {{ $inputClass or '' }}" min="1"
                                               value="{{ ad(1) }}" data-ware="{{ $ware->hashid }}">
                                        <a href="#" class="button green icon-plus add-to-cart"
                                           data-ware="{{ $ware->hashid }}"></a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
