@if ($ware->checkAvailability())

	@php
		$sale_price = $ware->getGachetClientPrice('sale');
		$original_price = $ware->getGachetClientPrice('original');
		$sale_price_to_show = gachet()->shop()->priceToShow($sale_price);
		$original_price_to_show = gachet()->shop()->priceToShow($original_price);
		$on_sale = ($original_price > $sale_price);
		$unit = ($ware->unit ?? model('unit')) ;
		$ware_identifier = $ware->hashid;
	@endphp

	@include($__module->getBladePath($viewFolder . '.ware-item-ribbons'))
	@include($__module->getBladePath($viewFolder . '.ware-item-remark'))
	@include($__module->getBladePath($viewFolder . '.ware-item-radio'))
	@include($__module->getBladePath($viewFolder . '.ware-item-purchase'))
	@include($__module->getBladePath($viewFolder . '.ware-item-code'))

@endif
