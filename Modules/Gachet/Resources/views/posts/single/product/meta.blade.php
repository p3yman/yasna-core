<div class="product-meta">
	<div class="row">
		<div class="col-md-7">
			@include($__module->getBladePath($viewFolder . '.categories'))

			@yield('product-codes')

		</div>

		<div class="col-md-5 tac">
			<a href="#" class="button blue block add-to-compare"
			   data-url="{{ route('gachet.compare.add', ['hashid' => $post->hashid]) }}">
				{{ $__module->getTrans('cart.button.add_to_compare') }}
			</a>

			<div class="mb15"></div>

			<a href="{{ route('gachet.compare.index') }}" class="button instagram js-compare-link"
			   style="display: none">
				<i class="fa fa-eye"></i>
				{{ $__module->getTrans('general.preview-compare') }}
			</a>
		</div>
	</div>


	@include($__module->getBladePath($viewFolder . '.download-catalog'))
</div>
