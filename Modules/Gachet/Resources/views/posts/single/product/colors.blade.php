@if(isset($wares) and count($wares))
	<div class="row">
		<div class="col-md-2 col-sm-3">
			<p style="font-size: .8125rem;color: #666;">
				{{ $__module->getTrans('general.labels.color.plural'). ": "  }}
			</p>
		</div>

		<div class="col-md-10 col-sm-9">
			<div class="color-selector">
				@foreach($wares as $ware)

					@php
						$labels = $ware->labels()->get();
						$is_first_ware = !boolval($loop->index);
						$originalPrice = 0;
						$originalPrice = ad(number_format($ware->originalPrice()->in($previewCurrency)->get()));
						$salePrice = 0;
						if ($ware->isOnSale()){
							$salePrice = ad(number_format($ware->salePrice()->in($previewCurrency)->get()));
						}
						$isAvailable = ($ware->checkAvailability()) ? 1 : 0;

					@endphp

					@foreach($labels as $label)
						@php
							$is_first_label = ($is_first_ware and !boolval($loop->index));
							$slug = str_after($label->slug, ware()::LABEL_POSTTYPE_DIVIDER );
							$title = $label->titleIn(getLocale());
						@endphp

						@php( $color_code = config('gachet.colors.' . $slug))
						<div class="radio control--radio" title="{{ $title }}">
							<label>
								<input type="radio" value="{{ $ware->hashid }}" name="color"
									   data-currency="{{ $previewCurrencyTrans }}"
									   data-original-price = "{{ $originalPrice}}"
									   data-sale-price="{{ $salePrice }}"
									   data-availability="{{ $isAvailable }}"
									   id="{{ $ware->hashid }}"
									   @if($is_first_label) checked @endif>
								<div class="control__indicator" style="background-color: {{ $color_code }}"></div>
								<label for="{{ $ware->hashid }}">{{ $title }}</label>
							</label>
						</div>
					@endforeach
				@endforeach
			</div>
		</div>

	</div>

@endif

