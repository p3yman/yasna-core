@php
	$catalog_link = fileManager()
		->file($post->catalog)
		->getDownloadUrl()
@endphp

@if ($catalog_link)
	<a href="{{ $catalog_link }}" class="button red inverted">
		<i class="fa fa-download"></i>
		{{ $__module->getTrans('general.download_catalog') }}
	</a>
@endif
