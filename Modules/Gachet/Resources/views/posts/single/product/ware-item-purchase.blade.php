@section('purchase')
	<div class="row">
		<div class="col-md-7">
			@if($sale_price)
				<div class="price js-ware-info" data-identifier="{{ $ware_identifier }}" style="display: none">
					@if($on_sale)
						@php
							$off_percent = 100 - round(($sale_price_to_show / $original_price_to_show), 2) * 100;
						@endphp
						<del>
							{{ ad(number_format($original_price_to_show)) }}
							{{--{{ $previewCurrencyTrans }}--}}
						</del>

						<span class="label alt red" style="font-weight: bold">
					{{ pd($off_percent . $__module->getTrans('general.percent_sign')). ' '. $__module->getTrans('general.sale')}}
				</span>

						<br>

						<ins>
							{{ ad(number_format($sale_price_to_show)) }}
							{{ $previewCurrencyTrans }}
						</ins>
					@else
						<ins>
							{{ ad(number_format($original_price_to_show)) }}
							{{ $previewCurrencyTrans }}
						</ins>
					@endif
					<div class="mt15">
						<div class="label green"> {{ $__module->getTrans('post.product.is-available') }}</div>
					</div>
				</div>
			@endif
		</div>
		<div class="col-md-5 tac">
			{{--check shop is active from setting--}}
			@if(get_setting('shop_available'))
				{{--check the ware is purchasable --}}
				@if($ware->isPurchasable())
					<div class="field add-to-card mt20 js-ware-info" data-identifier="{{ $ware_identifier }}"
						 style="display: none">
						<div class="row">
							<div class="col-md-5 d-n">
								<div class="row">
									<div class="col-xs-6 mb5">
										<input class="ware-count product-counter @if(!$unit->is_discrete) float @endif"
											   value="{{ ad(1) }}"
											   min="1" data-ware="{{ $ware->hashid }}">
									</div>
									<div class="col-xs-6 mb5">
										<div class="add-label">
											<div class="content">
												<h4>{{ $unit->title }}</h4>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="{{--col-md-7--}}">
								<button class="block green add-to-cart" data-ware="{{ $ware->hashid }}">
									<i class="icon-cart"></i>
									{{ $__module->getTrans('cart.button.add') }}
								</button>
							</div>
						</div>
					</div>
				@else

					<div class="price js-ware-info mt20" data-identifier="{{ $ware_identifier }}" style="display: none">
						<div class="status">
							<div class="label red"> {{ $__module->getTrans('post.product.is-not-available') }}</div>
						</div>
					</div>
				@endif
			@else
				<div>
					<a class="button block red labeled" href="{{ url('contact') }}" target="_blank">
						<i class="icon-phone"></i>
						{{ get_setting("call_for_price") }}
						<br>
						@php $tel = get_setting('telephone'); @endphp
						{{ pd($tel[0]) }}
					</a>
				</div>
			@endif
		</div>
	</div>




@append
