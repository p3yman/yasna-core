<ul class="nav-tabs clearfix js-tabs">
    @foreach($tabs as $tab)
        <li @if(isset($tab['active']) and $tab['active']) class="active" @endif><a href="{{ $tab['link'] or "" }}">{{ $tab['title'] or ""}}</a></li>
    @endforeach
</ul>
