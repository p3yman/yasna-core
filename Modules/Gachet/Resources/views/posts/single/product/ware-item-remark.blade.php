@section('remarks')
	<div class="excerpt mt15 js-ware-info" data-identifier="{{ $ware_identifier }}" style="display: none">
		<p>{!! nl2br($ware->remarkIn(getLocale())) !!}</p>
	</div>
@append
