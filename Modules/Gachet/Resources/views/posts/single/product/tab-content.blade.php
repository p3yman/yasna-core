<div class="tab-content-wrapper">
    <div class="tab-content" data-id="details">
        @include($__module->getBladePath('posts.single.product.content'))
    </div>
    <div class="tab-content" data-id="comments">
        @include($__module->getBladePath('comment.main'))
    </div>
    <div class="tab-content" data-id="support">
        @include($__module->getBladePath('posts.single.product.support'))
    </div>
    <div class="tab-content" data-id="specs">
        @include($__module->getBladePath('posts.single.product.specs'))
    </div>
    <div class="tab-content" data-id="prices">
        @include($__module->getBladePath('posts.single.product.prices-graph'))
    </div>
</div>
