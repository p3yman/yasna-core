@php $similarPosts = $post->similars(4) @endphp
@if($similarPosts and $similarPosts->count())
    <div class="related">
        <div class="part-title">
            <h3>
                @php
                    $relatedTitleParts = str_replace(
                        ':things',
                        $posttype->titleIn(getLocale()),
                        $__module->getTrans('general.similar-things')
                    );
                @endphp
                {{ $relatedTitleParts['part1'] }}
                <span>{{ $relatedTitleParts['part2'] }}</span>
                {{ $relatedTitleParts['part3'] }}
            </h3>
        </div>
    </div>
    <div class="row d-f" style="align-items: stretch;">
        @foreach($similarPosts as $similarPost)
            <div class="col-sm-3 d-f" style="align-items: stretch;">
                <a href="{{ $similarPost->direct_url }}" class="product-item">
                    @php
                        // @todo: read best version of product photo depending on ui
                        $similarPostImage = $__module->getAsset("images/image-not-found.png");
                        if($similarPost->featured_image) {
                            $dbImage = doc($similarPost->featured_image)
                                ->getUrl();
                            if($dbImage) {
                                $similarPostImage = $dbImage;
                            }
                        }
                        $defaultWare = $similarPost->wares->first();
                    @endphp
                    <div class="thumbnail"><img src="{{ $similarPostImage }}"></div>
                    <div class="content">
                        <h6> {{ $similarPost->title }} </h6>
                        <div class="price">
                            @if($defaultWare and $defaultWare->checkAvailability())
                                @if($defaultWare->isOnSale())
                                    @php
                                        $off = 100 - round(($defaultWare->salePrice()->in($previewCurrency)->get() / $defaultWare->originalPrice()->in($previewCurrency)->get()), 2) * 100;
                                    @endphp
                                    <del>
                                        {{ ad(number_format($defaultWare->originalPrice()->in($previewCurrency)->get())) }}
                                        {{ $previewCurrencyTrans }}
                                    </del>

                                    <span class="label alt red fw-b">
                                        {{ pd($off) . $__module->getTrans('general.percent_sign') }}
                                    </span>

                                    <br>
                                    <ins>
                                        {{ ad(number_format($defaultWare->salePrice()->in($previewCurrency)->get())) }}
                                        {{ $previewCurrencyTrans }}
                                    </ins>

                                @else
                                    <ins>
                                        {{ ad(number_format($defaultWare->originalPrice()->in($previewCurrency)->get())) }}
                                        {{ $previewCurrencyTrans }}
                                    </ins>
                                @endif
                            @else
                                <div class="status">
                                    <div class="label red"> {{ $__module->getTrans('post.product.is-not-available') }}</div>
                                </div>
                            @endif
                        </div>
                        @if(ShopTools::postShopWareCustomCode($post->id))
                            <div class="status">
                                <div class="meta-title"> {{ $__module->getTrans('post.product.code') }}
                                    <div class="label blue"> {{ ad(ShopTools::postShopWareCustomCode($post->id)) }}</div>
                                </div>
                            </div>
                        @endif
                    </div>
                </a>
            </div>
        @endforeach
    </div>
@endif
