@php
	$default_image = $__module->getAsset("images/image-not-found-200x200.png");
	$products = gachet()
		->posts()
		->findRelatedPosts($post, 8)
		->get()
		->map(function ($post) use ($default_image) {
			$image = fileManager()
				->file($post->featured_image)
				->posttype($post->posttype)
				->config('featured_image')
				->version('200x200')
				->getUrl() ?? $default_image;

			$default_ware = $post->purchasableWares()->first() ?? ware();
			$price =  $default_ware->getGachetClientPrice('original');
			$sale_price = $default_ware->getGachetClientPrice('sale');

			return [
				"image" => $image ,
				"title" => $post->title ,
				"link" => "#" ,
				"price" => gachet()->shop()->showPrice($price) ,
				"sale_price" => gachet()->shop()->showPrice($sale_price) ,
			];
		})
		->toArray()
	;
@endphp

@if (empty($products))
    @php return @endphp
@endif

@component($__module->getBladePath('layouts.widgets.items-slider.main'),[
	"title" => [
		$__module->getTrans('general.accessories') ,
		'',
	] ,
])

	@foreach($products as $product)
		<div class="slide">
			<a href="{{ $product['link'] }}" class="product-item">
				<div class="thumbnail">
					<img src="{{ $product['image'] }}" style="width: 200px; height: 200px;">
				</div>
				<div class="content">
					<h6>
						{{ $product['title'] }}
					</h6>
					<div class="price">
						<del>
							{{ $product['price'] }}
						</del>
						<ins>
							{{ $product['sale_price'] }}
						</ins>
					</div>
				</div>
			</a>
		</div>
	@endforeach

@endcomponent
