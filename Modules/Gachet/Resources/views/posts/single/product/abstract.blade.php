@if ($post->abstract)
    <div class="abstract-content mb50">
        <p>{{ $post->abstract }}</p>
    </div>
@endif
