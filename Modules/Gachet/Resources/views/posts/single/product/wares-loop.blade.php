@php $any_ware = ($wares->count() and $selected_ware->checkAvailability()) @endphp

@if ($any_ware)
	@foreach($wares as $ware)
		@include($__module->getBladePath($viewFolder . '.ware-item'))
	@endforeach
@endif

@section('not-available')
	@if (!$any_ware)
		<div class="price">
			<div class="status">
				<div class="label red"> {{ $__module->getTrans('post.product.is-not-available') }}</div>
			</div>
		</div>
	@endif
@append
