<div class="socials">
	<div class="a2a_kit a2a_kit_size_24">
		<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
		<a class="a2a_button_twitter"></a>
		<a class="a2a_button_telegram"></a>
		<a class="a2a_button_facebook"></a>
		<a class="a2a_button_google_gmail"></a>
		<a class="a2a_button_copy_link"></a>
	</div>
</div>

@section('html-footer')
	<!-- AddToAny BEGIN -->
	<script>
        var a2a_config = a2a_config || {};
        a2a_config.linkname = '{{ $title or "" }}';
        a2a_config.linkurl = '{{ $url or request()->fullUrl() }}';
        a2a_config.icon_color = "transparent,#56c5d0";
        a2a_config.color_main = "dddddd";
        a2a_config.color_border = "dddddd";
        a2a_config.color_link_text = "333333";
        a2a_config.color_link_text_hover = "333333";
	</script>
	{!! Html::script($__module->getAsset('libs/addtoany/page.js')) !!}
@append
