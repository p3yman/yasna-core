@php
	$post->spreadMeta();
	$source_link  = $post->source_link;
	$source_label = $post->source_label
@endphp
@if ($source_link)
	<div class="row mt20">
		<div class="col-xs-12 text-gray">
			@if ($source_label)
				<i class="fa fa-link"></i>
				{{ $__module->getTrans('general.source') }}:
				<a href="{{ $source_link }}" target="_blank">{{ $source_label }}</a>
			@else
				<a href="{{ $source_link }}" target="_blank">
					<i class="fa fa-link"></i>
					{{ $__module->getTrans('general.source') }}
				</a>
			@endif
		</div>
	</div>
@endif
