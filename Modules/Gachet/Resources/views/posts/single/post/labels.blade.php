<hr>
<div class="col sm-12">
	<div class="row">
		<div class="col-sm-6">
			@include($__module->getBladePath($viewFolder . '.categories'))
			@include($__module->getBladePath($viewFolder . '.tags'))
		</div>
		<div class="col-sm-6">
			@include($__module->getBladePath('posts.single.post.add-to-any'))
		</div>
	</div>
</div>

