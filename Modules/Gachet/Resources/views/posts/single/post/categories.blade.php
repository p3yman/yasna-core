@php $categories = $post->gachet_categories @endphp

@if (!$categories->count())
	@php return @endphp
@endif

<div class="row">
	<div class="col-sm-3">
		<div class="text-gray"> {{ $__module->getTrans('general.categories') }}:</div>
	</div>
	<div class="col-sm-9">
		@foreach ($categories as $category)
			<a href="{{ $posttype->getPostLabelUrl($category) ?: '#' }}" class="label blue alt">
				{{ $category->titleIn(getLocale()) }}
			</a>
			&nbsp;
		@endforeach
	</div>
</div>
