<div class="row">
	<div class="col-sm-10 col-center">
		<div class="blog-single">
			<div class="blog-cover" style="text-align: center;">
				@php
					$posttype = $post->posttype;
					$postImage = fileManager()
						->file($post->featured_image)
						->getUrl();
				@endphp
				@if($postImage)
					<img src="{{ $postImage }}" style="width: auto; max-width: 100%;">
				@endif
			</div>
			<div class="blog-header">
				<h1 class="title"> {{ $post->title }} </h1>
				@include($__module->getBladePath($viewFolder . '.publish_info'))
			</div>
			@include($__module->getBladePath($viewFolder . '.content'))
			@include($__module->getBladePath($viewFolder . '.album'))
			@include($__module->getBladePath($viewFolder . '.source'))
			@include($__module->getBladePath($viewFolder . '.labels'))
		</div>
		@include($__module->getBladePath($viewFolder . '.related_posts'))
	</div>
</div>
