@php $tags = $post->tags @endphp

@if (!$tags->count())
	@php return @endphp
@endif

<div class="row pt10">
	<div class="col-sm-3">
		<div class="text-gray"> {{ $__module->getTrans('general.tag.title.plural') }}:</div>
	</div>
	<div class="col-sm-9">
		@foreach ($tags as $tag)
			<a href="{{ $tag->direct_url ?: '#' }}" class="label blue">
				{{ $tag->slug }}
			</a>
			&nbsp;
		@endforeach
	</div>
</div>
