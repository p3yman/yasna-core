<div class="meta">
	<div class="item date"> {{ ad(echoDate($post->published_at, 'd F Y')) }} </div>
	@php $publisher = $post->publisher @endphp
	@if ($publisher and $publisher->exists)
		<div class="item author"> {{ $publisher->full_name }} </div>
	@endif
</div>
