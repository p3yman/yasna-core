<div class="team-item leader">
    {{--- If the post has a valid featured image, it will be shown, --}}
    {{--- else if the posttype has a valid default featured image it will be shown --}}
    {{--- and else the not found image will be shown. --}}
    @php
        $personImage = CurrentModule::asset("images/image-not-found.png");
        if($post->featured_image) {
            $postImage = doc($post->featured_image)->getUrl();
        }
        if(isset($postImage)) {
            $personImage = $postImage;
        } else {
            if($posttype->default_featured_image) {
                $posttypeImage = doc($posttype->default_featured_image)->getUrl();
            }
            if(isset($posttypeImage)) {
                $personImage = $posttypeImage;
            }
        }
    @endphp
    <div class="avatar">
        <img src="{{ $personImage }}"></div>
    <div class="content">
        <h3> {{ $post->title }} </h3>
        <h4> {{ $post->seat }} </h4>
        <p> {!! $post->abstract !!} </p>
    </div>
</div>