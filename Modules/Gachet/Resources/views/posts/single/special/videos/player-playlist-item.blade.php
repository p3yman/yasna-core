<div class="col-md-3 col-sm-4 col-xs-6 player-list-item @if($key == 0) current @endif"
     data-hashid="{{ $file->aparat_hash_code }}">
    <div class="player-list-item-inner">
        @php
            $videoThumb = doc($file->src)->version('thumb')->getUrl() ?:
                CurrentModule::asset("images/image-not-found.png");
        @endphp
        <img src="{{ $videoThumb }}">
        <div class="player-list-item-text align-vertical-center align-horizontal-center">
            <p>
                <br/>
                {{ $file->label }}
                <br/>
                <span class="player-list-item-playing">
                <span class="fa fa-video-camera"></span>
                    {{ $__module->getTrans('general.media.playing') }}
            </span>
            </p>
        </div>
    </div>
</div>
