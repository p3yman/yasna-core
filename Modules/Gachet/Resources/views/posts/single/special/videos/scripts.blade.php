@section('html-footer')
    @if ($files and count($files))
        <script type="text/JavaScript" id="video-script"
                src="https://www.aparat.com/embed/{{ $files[0]->aparat_hash_code }}?data[rnddiv]=player-div&data[responsive]=yes">
        </script>
        <script>
            $(document).ready(function () {
                $('.player-list-item-inner').click(function () {
                    let item = $(this).closest('.player-list-item');
                    if (!item.hasClass('current')) {
                        let hashid = item.attr('data-hashid');
                        let newSrc = "https://www.aparat.com/embed/" +
                            hashid +
                            "?data[rnddiv]=player-div&data[responsive]=yes";
                        let currentScript = $('#video-script');

                        let newScript = document.createElement("script");
                        newScript.type = "text/javascript";
                        newScript.id = 'video-script';
                        newScript.src = newSrc;
                        currentScript.after(newScript);
                        currentScript.remove();

                        $('.player-list-item.current').removeClass('current');
                        item.addClass('current');
                    }
                });
            });
        </script>
    @endif
@append