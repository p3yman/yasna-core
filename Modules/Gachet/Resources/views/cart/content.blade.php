@php $orders = $cart->orders; @endphp
@if ($orders->count())
    @include($__module->getBladePath('cart.nonempty'))
@else
    @include($__module->getBladePath('cart.empty'))
@endif
