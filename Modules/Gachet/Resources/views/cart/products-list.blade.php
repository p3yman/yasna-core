<table class="table bordered xs-hide">
    <thead>
    <tr>
        <th>{{ $__module->getTrans('post.product.singular') }}</th>
        <th>{{ $__module->getTrans('cart.price.unit') }}</th>
        <th>{{ $__module->getTrans('cart.numeration-label.amount') }}</th>
        <th>{{ $__module->getTrans('cart.price.total') }}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        @include($__module->getBladePath('cart.products-list-item'))
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="100%" class="tal">
            <a href="#" class="button flush-cart" onclick="flushCart()">
                {{ $__module->getTrans('cart.button.flush') }}
            </a>
            <a href="{{ route('gachet.purchase.step1') }}" class="button green">
                {{ $__module->getTrans('cart.button.settlement') }}
            </a>
        </td>
    </tr>
    </tfoot>
</table>
