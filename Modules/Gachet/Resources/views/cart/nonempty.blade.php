<div class="cart-full">
    @include($__module->getBladePath('cart.products-list'))
    @include($__module->getBladePath('cart.product-list-tablet'))
    @include($__module->getBladePath('cart.discount-form'))
    @include($__module->getBladePath('cart.total'))
</div>
{{--@include($__module->getBladePath('cart.related'))--}}
