@if ($ware = $order->ware)
    @php
        $ware = $ware->spreadMeta()->inLocale(getLocale());
        $unit = $ware->unit;
        $post = $order->post;
    
        $postImage = doc($post->featured_image)
            ->version('thumb')
            ->getUrl();
        $productUrl = gachet()->shop()->productWareUrl($post, $ware);
        $productTitle =  $ware->full_title;
        if ($isAvailable = ($order->single_original_price and $order->original_price and $order->sale_price)) {
            $unitPrice = round($order->single_original_price / 10);
            $totalPrice = round($order->original_price / 10);
            $unitPriceToShow = ad(number_format($unitPrice));
            $totalPriceToShow = ad(number_format($totalPrice));
            $countToShow =  ad($order->count);
        }
    @endphp
    <tr data-item="{{ $order->hashid }}" @if(!$isAvailable) class="bg-light" @endif>
        <td>
            <a href="{{ $productUrl }}" class="product-name" target="_blank">
                <img src="{{ $postImage }}" width="80">
                <h5>{{ $productTitle }}</h5>
            </a>
        </td>
        @if ($isAvailable)
            <td class="price">
                @php $unitPrice = round($order->single_original_price / 10); @endphp
                {{ $unitPriceToShow }}
                {{ $__module->getTrans('general.unit.currency.IRT') }}
            </td>
            <td class="tac">
                <input type="text" value="{{ $countToShow }}" style="max-width: 100px; display: inline;"
                       class="product-amount product-counter @if(!$unit->is_discrete) float @endif">
                {{ $unit->title }}
            </td>
            <td class="price">
                @if ($isOnSale = $order->sale_price != $order->original_price)
                    @php
                        $discount = round(($order->original_price - $order->sale_price) / 10);
                        $discountToShow = ad(number_format($discount));
                        $salePrice = round($order->sale_price / 10);
                        $salePriceToShow = ad(number_format($salePrice));
                    @endphp
                    <table>
                        <tr>
                            <td>{{ $__module->getTrans('cart.price.total') }}:</td>
                            <td>
                                {{ $totalPriceToShow }}
                                {{ $__module->getTrans('general.unit.currency.IRT') }}
                            </td>
                        </tr>
                        <tr>
                            <td>{{ $__module->getTrans('cart.headline.discount') }}:</td>
                            <td>
                                {{ $discountToShow }}
                                {{ $__module->getTrans('general.unit.currency.IRT') }}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                {{ $salePriceToShow }}
                            </td>
                        </tr>
                    </table>
                @else
                    {{ $totalPriceToShow }}
                    {{ $__module->getTrans('general.unit.currency.IRT') }}
                @endif
            </td>
        @else
            <td colspan="3" class="color-red price">
                {{ $__module->getTrans('post.product.is-not-available') }}
            </td>
        @endif
        <td class="remove-row">
            <a href="#" class="icon-close" onclick="removeItem('{{ $order->hashid }}')"></a>
        </td>
    </tr>

@section('products-list-tablet')
    @include($__module->getBladePath('cart.product-item-tablet'))
@append
@endif
