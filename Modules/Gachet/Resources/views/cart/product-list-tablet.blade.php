<div class="product-list--tablet">
    @yield('products-list-tablet')
    
    <div class="cart-buttons">
        <a href="#" class="button flush-cart" onclick="flushCart()">
            {{ $__module->getTrans('cart.button.flush') }}
        </a>
        <a href="{{ route('gachet.purchase.step1') }}" class="button green">
            {{ $__module->getTrans('cart.button.settlement') }}
        </a>
    </div>
</div>
