<div class="cart-total-container">
	<table class="table bordered cart-total">
		<tbody>
		<tr>
			<td>{{ $__module->getTrans('cart.price.your-purchase-total') }}:</td>
			<td>
				{{ ad(number_format($original_price = gachet()->shop()->priceToShow($cart->original_price))) }}
				{{ $__module->getTrans('general.unit.currency.IRT') }}
			</td>
		</tr>

		@php $discount = ($cart->original_price - $cart->sale_price) + $cart->added_discount @endphp
		@if ($discount)
			<tr>
				<td>{{ $__module->getTrans('cart.headline.discount') }}:</td>
				<td>
					{{ ad(number_format($discount = round(gachet()->shop()->priceToShow($discount)))) }}
					{{ $__module->getTrans('general.unit.currency.IRT') }}
				</td>
			</tr>
		@endif

		@php $tax = $cart->tax_amount @endphp
		@if ($tax)
			<tr>
				<td>{{ $__module->getTrans('cart.headline.tax') }}:</td>
				<td>
					{{ ad(number_format($tax = round(gachet()->shop()->priceToShow($tax)))) }}
					{{ $__module->getTrans('general.unit.currency.IRT') }}
				</td>
			</tr>
		@endif

		<tr class="total">
			<td>{{ $__module->getTrans('cart.price.purchasable') }}:</td>
			<td>
				{{ ad(number_format($original_price - $discount + $tax)) }}
				{{ $__module->getTrans('general.unit.currency.IRT') }}
			</td>
		</tr>
		</tbody>
	</table>
</div>
