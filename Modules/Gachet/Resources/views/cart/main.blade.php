@extends($__module->getBladePath('layouts.plane'))

@php
    Template::appendToPageTitle($__module->getTrans('cart.title.singular'));
    Template::appendToNavBar([
        $__module->getTrans('cart.title.singular'), request()->url()
    ]);
@endphp

@section('body')
    <div class="page-content">
        <div class="container">
            @include($__module->getBladePath('cart.content'))
        </div>
    </div>
@append

@include($__module->getBladePath('cart.scripts'))

