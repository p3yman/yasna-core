@if ($cart->hasAppliedCouponCode())

	<div class="coupon">
		<h5> {{ $__module->getTrans('cart.message.coupon-code-is-applied') }} </h5>

		{!!
			widget('form-open')
				->target(route('gachet.cart.remove-coupon'))
				->class('js')
		!!}

		<button class="btn btn-link red xs">
			{{ $__module->getTrans('cart.button.remove-coupon-code') }}
		</button>

		<div class="result">
			{!! widget('feed')	 !!}
		</div>

		{!! widget('form-close') !!}
	</div>

@else
	{!!
		widget('form-open')
			->target(route('gachet.cart.coupon'))
			->class('js')
	!!}

	<div class="coupon">
		<h5>{{ $__module->getTrans('cart.message.have-coupon-code') }}</h5>
		<form action="#">
			<div class="field action">
				<input type="text" name="code">
				<button class="blue">
					{{ $__module->getTrans('cart.button.apply-coupon-code') }}
				</button>
			</div>
			<div class="result">
				{!! widget('feed') !!}
			</div>
		</form>
	</div>


	{!! widget('form-close') !!}
@endif

<div class="clearfix"></div>
