<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                <div class="cart-empty"><span class="icon-cart"></span>
                    <div class="text">{{ $__module->getTrans('cart.message.is-empty') }}</div>
                    <a href="{{ RouteTools::getPosttypeCategoriesRoute('products') }}" class="button green">
                        {{ $__module->getTrans('cart.button.back-to-shop') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
