<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<html lang="{{ getLocale() }}">
<body>
<div id="result">

</div>
{!! Html::script(CurrentModule::asset('libs/jquery/jquery.min.js')) !!}
<script>
    $(document).ready(function () {
        let result = $('#result');
        let count = 3000;
        let limit = 5;
        let steps = parseInt(count / limit);
        let url = "{{ CurrentModule::action('DummyController@dummyProducts', [
                    'offset' => '__offset__',
                    'limit' => '__limit__',
                ]) }}".replace('__limit__', limit);

        function callAjax(step = 0) {
            let offset = step * limit;
            let thisUrl = url.replace('__offset__', offset);
            console.log("Calling for group " + offset + "...");
            let itemEl = $('<div></div>').html("Request Number: " + (step + 1));
            result.prepend(itemEl);

            $.ajax({
                url: thisUrl,
                dataType: 'json',
                success: function (rs) {
                    let rsText = 'Request Number: ' + (step + 1) + ' ,' + rs.length + ' posts inserted <br />';
                    itemEl.html(rsText);

                    let ul = $('<ul></ul>');
                    $.each(rs, function (i, item) {
                        let li = $('<li></li>');
                        li.html('Post ID: ' + item['post-id'] + ', Wares: ' + item['wares-ids'].length);
                        ul.append(li);
                    });
                    itemEl.append(ul);

                    if (step < steps - 1) {
                        callAjax(step + 1);
                    }
                },
                complete: function (rs) {
                    console.log(rs.status);
                    if (rs.status != 200) {
                        itemEl.remove();
                        callAjax(step);
                    }
                }
            });
        }

        callAjax();
    })
</script>
</body>
</html>

