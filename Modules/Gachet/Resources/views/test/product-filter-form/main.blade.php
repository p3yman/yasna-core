@extends(CurrentModule::bladePath('layouts.plane'))

@section('body')
    {{ Form::open([
        'url' => CurrentModule::action('PosttypeController@filter', [
            'posttype' => 'products'
        ]),
        'method' => 'post',
        'target' => '_blank',
    ]) }}

    <input type="text" name="hash" />
    
    <button type="submit">Send</button>
    
    {{ Form::close() }}
@append
