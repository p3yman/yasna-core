@php
	$default_image = $__module->getAsset("images/image-not-found-200x200.png");
@endphp

@if (!$wares->count())
	@php return @endphp
@endif

@php
	$specials = $wares->map(function ($ware) use ($default_image) {
		$post = $ware->post;
		$image = fileManager()
			->file($post->featured_image)
			->posttype($post->posttype)
			->config('featured_image')
			->version('200x200')
			->getUrl() ?? $default_image;

		$price =  $ware->getGachetClientPrice('original');
		$sale_price = $ware->getGachetClientPrice('sale');

		if($price) {
			$off = 100 - round(($sale_price / $price), 2) * 100;
		} else {
			$off = 0;
		}

		return [
			"image" => $image ,
			"title" => $post->title ,
			"link" => gachet()->shop()->productWareUrl($post, $ware) ,
			"price" => gachet()->shop()->showPrice($price) ,
			"sale_price" => gachet()->shop()->showPrice($sale_price) ,
			"off_percent" => ad($off),
			"is_available" => $post->is_available
		];

	})->toArray()
@endphp

<div class="ware_slider">
	@component($__module->getBladePath('layouts.widgets.items-slider.main'),[
		"title" => ad($title),
	])
		@foreach($specials as $special)
			<div class="slide">
				<a href="{{ $special['link'] }}" class="product-item">
					<div class="thumbnail">
						<img src="{{ $special['image'] }}">
					</div>
					<div class="content">
						<h6>
							{{ $special['title'] }}
						</h6>
						@if ($special['price'] != $special['sale_price'])
							<div class="price">
								@if($special['is_available'])
									<del>
										{{ $special['price'] }}
									</del>
									<br>
									<ins class="d-ib">
										{{ $special['sale_price'] }}
										<span class="badge label red" style="font-weight: bold">
									{{ $special['off_percent'] . $__module->getTrans('general.percent_sign') }}
								</span>
									</ins>
								@else
									<div class="status">
										<div class="label red"> {{ $__module->getTrans('post.product.is-not-available') }}</div>
									</div>
								@endif
							</div>
						@else
							<div class="price">
								<ins>
									{{ $special['sale_price'] }}
								</ins>
							</div>
						@endif
					</div>
				</a>
			</div>
		@endforeach
	@endcomponent
</div>

