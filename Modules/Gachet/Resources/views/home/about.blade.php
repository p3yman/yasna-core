<style>
    iframe {
        border: none;
    }
</style>
@if($about)
    <div id="home-about">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-center">
                    <p style="text-align: justify;">{{ $about->abstract }}</p>
                    @if($about_video)
                        <p style="text-align: justify;">{!! $about_video->text !!}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif