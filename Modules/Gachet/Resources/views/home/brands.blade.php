@php
	/** @var \Modules\Gachet\Services\Products\Products $products */
	$products = app('gachet.shop.products');
	$brands_collection = $products->brands();
@endphp

@if (!$brands_collection->count())
	@php return @endphp
@endif

@php
	$brands = $brands_collection->map(function ($brand) {
		return [
			"title" => $brand->title,
			"img" => $brand->getPostLabelImageLink('thumb') ,
			"link" => route('gachet.products.brands.base') . "#!brand_{$brand->slug}?checkbox!" ,
		];
	})->filter(function ($brand) {
		return $brand['img'];
	});
@endphp

@if (empty($brands))
	@php return @endphp
@endif

@php
	$section_title = $__module->getTrans('general.brands');
@endphp

<div class="brands_slider">
	@component($__module->getBladePath('layouts.widgets.items-slider.main'),[
		"title" => [
			"0" => $__module->getTrans('general.brands') ,
			"1" => "" ,
		],
	])
		@foreach($brands as $brand)
			<div class="slide brand_slide">
				<div class="panel">
					<a href="{{ $brand['link'] }}" title="{{ $brand['title'] }}">
						{{--width="200" height="200"--}}
						<img src="{{ $brand['img'] }}" alt="{{ $brand['title'] }}">
					</a>
				</div>
			</div>
		@endforeach
	@endcomponent
</div>

