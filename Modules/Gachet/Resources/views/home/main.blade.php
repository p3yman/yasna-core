@extends($__module->getBladePath('layouts.plane'))

@php
	$hideNavbar = false;
	$isIndex = true;
@endphp

@section('body')
    @include($__module->getBladePath('home.slider'))

    @include($__module->getBladePath('home.tiles-info'))

	@include($__module->getBladePath('home.offers'))
	@include($__module->getBladePath('home.best-sellers'))
	@include($__module->getBladePath('home.brands'))

    @include($__module->getBladePath('home.products-categories'))

@append
