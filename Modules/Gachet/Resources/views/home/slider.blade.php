@if($slideshow and $slideshow->count())

	@php
		$images = $slideshow->map(function ($post) {
			return fileManager()
				->file($post->featured_image)
				->getUrl();
		})->filter();
		$slides_posttype = posttype(gachet()->getPosttypeSlug('home-slider'));
	@endphp

	@if (!$images->count())
		@php return @endphp
	@endif

	<div id="home-slides">
		<ul class="home-slides"
			data-slideshow-auto="{{ $slides_posttype->getMeta('slideshow_auto') ?? true }}"
			data-slideshow-pause="{{ $slides_posttype->getMeta('slideshow_pause') ?? true }}"
			data-slideshow-speed="{{ $slides_posttype->getMeta('slideshow_speed') ?? 500 }}"
			data-slideshow-timeout="{{ $slides_posttype->getMeta('slideshow_timeout') ?? 3000 }}"
		>
			@foreach($slideshow as $slide)
				@php $img = $images->get($loop->index) @endphp

				@if (!$img)
				    @continue
				@endif

				@php $slide->spreadMeta() @endphp
				@php $style = ($color = $slide->text_color) ? "style='color: $color'" : '' @endphp
				<li class="home-slide" style="background-image: url({{ $img }});">
					<a @if($slide->href) href="{{ $slide->href }}"
					   @endif style="display: block; height: 100%; width: 100%;" target="_blank">
						<div class="container">
							<div class="content">
								@if(strlen($slide->title2))
									<h1 {!! $style !!}> {{ $slide->title2 }} </h1>
								@endif
								@if(strlen($slide->abstract))
									<p {!! $style !!}> {{ $slide->abstract }} </p>
								@endif
							</div>
						</div>
					</a>
				</li>
			@endforeach
		</ul>
	</div>
@endif
