@if(sizeof($product_folders))
    <div id="categories">
        <div class="container">
            <div class="part-title">
                <h3>
                    <span>{{ $__module->getTrans('general.categories-of') }}</span>
                    {{ $__module->getTrans('post.product.plural') }}
                </h3>
            </div>
            <div class="cats">
                <div class="row">
                    @foreach($product_folders as $cat)
                        @include($__module->getBladePath('posts.categories.products.item'))
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endif
