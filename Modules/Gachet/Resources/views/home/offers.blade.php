@php
	$offers = model('shop-offer')->whereValid()->get();
@endphp

@foreach($offers as $offer)
	@include($__module->getBladePath('home.offer-item'))
@endforeach
