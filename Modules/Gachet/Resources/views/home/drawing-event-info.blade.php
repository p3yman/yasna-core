@if($event and $event->exists)
    <div class="row">
        <div class="col-xs-12">
            <h3>{{ $event->title }}</h3>
            <p class="text-black f12">
                {{ $__module->getTrans('post.event.from') }}
                {{ ad(echoDate($event->event_starts_at, 'j F Y')) }}
                {{ $__module->getTrans('post.event.to') }}
                {{ ad(echoDate($event->event_ends_at, 'j F Y')) }}
            </p>
        </div>
    </div>
@endif
