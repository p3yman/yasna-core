@extends($__module->getBladePath('auth.layouts.plane'))

@php
	$title = $__module->getTrans('user.login');
	gachet()->template()->appendToPageTitle($title);
	gachet()->template()->setRedTitle($title);
@endphp

@section('form')
	<form class="form-horizontal" role="form" method="POST"
		  action="{{ route('login') }}">
		{{ csrf_field() }}
		<div class="field">
			<input type="text" name="email" id="code_melli"
				   placeholder="{{ trans('validation.attributes.email') }}" required autofocus>
		</div>
		<div class="field">
			<input type="password" name="password" id="password"
				   placeholder="{{ trans('validation.attributes.password') }}" required>
		</div>

		@if (!env('APP_DEBUG'))
			{!! app('captcha')->render(getLocale()) !!}
		@endif

		<div class="tal">
			<button class="green block"> {{ $__module->getTrans('user.login') }} </button>
		</div>

		@if($errors->all())
			<div class="alert alert-danger" style="margin-top: 10px;">
				@foreach ($errors->all() as $error)
					{{ $error }}<br>
				@endforeach
			</div>
		@endif


		<div class="row">
			<div class="col-xs-12 pt15 text-center">
				<a href="{{ route('password.request') }}">
					{{ $__module->getTrans('user.password.restore') }}
				</a>
			</div>
		</div>
		<hr class="or" data-text="{{ $__module->getTrans('general.or') }}">
		<div class="tal">
			<a href="{{ route('register') }}"
			   class="button blue block">
				{{ $__module->getTrans('user.register') }}
			</a>
		</div>

	</form>
@append
