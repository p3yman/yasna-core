<body class="auth">
<div class="auth-wrapper">
    <div class="auth-col">
        <a href="{{ CurrentModule::action('FrontController@index') }}" class="logo">
            <img src="{{ gachet()->template()->siteLogoUrl() }}">
        </a>
    </div>
    <div class="auth-col">
        @php $redTitle = gachet()->template()->redTitle() @endphp
        @if($redTitle)
            <h1 class="auth-title"> {{ $redTitle }} </h1>
        @endif
        @yield('form')
    </div>
</div>
</body>

@include($__module->getBladePath('layouts.scripts'))
@yield('html-footer')
