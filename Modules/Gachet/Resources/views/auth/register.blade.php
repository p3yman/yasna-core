@extends($__module->getBladePath('auth.layouts.plane'))

@php
	$title = $__module->getTrans('user.register');
	gachet()->template()->appendToPageTitle($title);
	gachet()->template()->setRedTitle($title);
@endphp

@section('form')
	{!! Form::open([
		'url'	=> route('register'),
		'method'=> 'post',
		'class' => 'form-horizontal js',
		'name' => 'registerForm',
		'id' => 'registerForm',
	]) !!}

	@include($__module->getBladePath('auth.field'), [
		'name' => 'name_first',
		'other' => 'autofocus',
		'type' => 'text',
		'value' => old('name_first'),
		'class' => 'form-required form-persian',
		'min' => 2,
	])

	@include($__module->getBladePath('auth.field'), [
		'name' => 'name_last',
		'type' => 'text',
		'value' => old('name_last'),
		'class' => 'form-required form-persian',
		'min' => 2,
	])

	@include($__module->getBladePath('auth.field'), [
		'name' => 'mobile',
		'type' => 'text',
		'value' => old('mobile'),
		'class' => 'form-required form-mobile',
	])

	@include($__module->getBladePath('auth.field'), [
		'name' => 'email',
		'type' => 'text',
		'value' => old('email'),
        'class' => 'form-required form-email',
	])

	@include($__module->getBladePath('auth.field'), [
		'name' => 'password',
		'type' => 'password',
		'value' => old('password'),
		'class' => 'form-required form-password',
	])

	@include($__module->getBladePath('auth.field'), [
		'name' => 'password2',
		'type' => 'password',
		'value' => old('password2'),
		'class' => 'form-required',

	])

	<div class="tal" style="margin-bottom: 15px;">
		<button class="green block" type="submit"> {{ $__module->getTrans('user.register') }} </button>
	</div>

	@include($__module->getBladePath('layouts.widgets.form.feed'))

	<hr class="or">

	<div class="tal" style="margin-bottom: 15px;">
		<a href="{{ route('login') }}" class="button blue block">
			{{ $__module->getTrans('user.login') }}
		</a>
	</div>



	{!! Form::close() !!}

	@include($__module->getBladePath('layouts.widgets.form.scripts'))
@append

