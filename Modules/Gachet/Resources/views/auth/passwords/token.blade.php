@extends($__module->getBladePath('auth.layouts.plane'))

@php
	$title = $__module->getTrans('user.password.restore');
	gachet()->template()->appendToPageTitle($title);
	gachet()->template()->setRedTitle($title);
@endphp

@section('form')
	{!! Form::open([
		'url' => route('password.token'),
		'method'=> 'post',
		'class' => 'js',
		'name' => 'editForm',
		'id' => 'editForm',
		'style' => 'padding: 15px;',
	]) !!}

	@if(!isset($email) or !$email)
		@include($__module->getBladePath('auth.field'), [
			'name' => 'email',
			'type' => 'text',
			'class' => 'form-required form-email',
		])
	@endif

	@include($__module->getBladePath('auth.field'), [
		'name' => 'password_reset_token',
		'type' => 'text',
		'class' => 'form-required',
	])

	<div class="tal pb15">
		<button class="green block"> {{ $__module->getTrans('user.password.check-token') }} </button>
	</div>

	@include($__module->getBladePath('layouts.widgets.form.feed'))


	{!! Form::close() !!}

	@include($__module->getBladePath('layouts.widgets.form.scripts'))
@append
