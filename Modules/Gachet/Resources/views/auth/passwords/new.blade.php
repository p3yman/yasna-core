@extends($__module->getBladePath('auth.layouts.plane'))

@php
    $title = $__module->getTrans('user.password.restore');
    gachet()->template()->appendToPageTitle($title);
    gachet()->template()->setRedTitle($title);
@endphp
@section('form')
    {!! Form::open([
        'url' => request()->url(),
        'method'=> 'post',
        'class' => 'js',
        'name' => 'editForm',
        'id' => 'editForm',
        'style' => 'padding: 15px;',
    ]) !!}


    <div class="row">
        @include($__module->getBladePath('layouts.widgets.form.input'),[
            'name' => 'new_password',
            'type' => 'password',
            'label' => false,
            'placeholder' => trans('validation.attributes.new_password'),
            'containerClass' => 'field',
        ])
    </div>

    <div class="row">
        @include($__module->getBladePath('layouts.widgets.form.input'),[
            'name' => 'new_password2',
            'type' => 'password',
            'label' => false,
            'placeholder' => trans('validation.attributes.new_password2'),
            'containerClass' => 'field',
        ])
    </div>

    <div class="tal pb15">
        <button class="green block"> {{ $__module->getTrans('form.button.save') }} </button>
    </div>

    @include($__module->getBladePath('layouts.widgets.form.feed'))

    {!! Form::close() !!}

    @include($__module->getBladePath('layouts.widgets.form.scripts'))
@append
