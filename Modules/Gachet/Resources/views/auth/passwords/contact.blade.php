@extends($__module->getBladePath('auth.layouts.plane'))

@php
	$title = $__module->getTrans('user.password.restore');
	gachet()->template()->appendToPageTitle($title);
	gachet()->template()->setRedTitle($title);
@endphp

@section('form')
	{!! Form::open([
		'url' => route('password.request'),
		'method'=> 'post',
		'class' => 'js',
		'name' => 'editForm',
		'id' => 'editForm',
		'style' => 'padding: 15px;',
	]) !!}

	@include($__module->getBladePath('auth.field'), [
		'name' => 'email',
		'type' => 'text',
		'class' => 'form-required form-email',
	])

	<div class="tal pb15">
		<button class="green block">
			{{ $__module->getTrans('user.password.send-reset-token') }}
		</button>
	</div>

	<div class="tal pb15">
		<a class="button green block"
		   href="{{ route('password.token', ['haveCode' => 'code']) }}">
			{{ $__module->getTrans('user.password.have-code') }}
		</a>
	</div>

	@include($__module->getBladePath('layouts.widgets.form.feed'))

	{!! Form::close() !!}

	@include($__module->getBladePath('layouts.widgets.form.scripts'))
@append
