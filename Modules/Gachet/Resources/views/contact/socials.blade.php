@php
	$socials = [
		[
			"icon" => "facebook" ,
			"link" => $facebook_link ,
		],
		[
			"icon" => "twitter" ,
			"link" => $twitter_link ,
		],
		[
			"icon" => "instagram" ,
			"link" => $instagram_link ,
		],
		[
			"icon" => "telegram" ,
			"link" => $telegram_link ,
		],
	];
@endphp
<div class="socials mb20">
	@foreach($socials as $social)
		@if (!$social['link'])
			@continue
		@endif
		<a href="{{ $social['link'] }}" class="fa fa-{{ $social['icon'] }}"></a>
	@endforeach
</div>
