@extends($__module->getBladePath('layouts.plane'))

@php
    gachet()->template()->appendToPageTitle($__module->getTrans('general.contact-us'));
    gachet()->template()->appendToNavBar([
        $__module->getTrans('general.contact-us'), request()->url()
    ]);
@endphp

@section('body')
    <div class="page-content mb40">
        <div class="container-fluid contact-page">
            <div class="row">
                <div class="col-md-10 col-center">
                    <div class="col-sm-6">
                        @include($__module->getBladePath('contact.contact-info'))
                        @include($__module->getBladePath('contact.socials'))

                        @if(isset($contact_text) and $contact_text and $contact_text->text)
                            {{--<div class="part-title">--}}
                                {{--<h3>{{ $contact_text->title }}</h3>--}}
                            {{--</div>--}}

                            <p>{!! $contact_text->text !!}</p>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        @include($__module->getBladePath('contact.form'))
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include($__module->getBladePath('contact.map'))
@append
