	@if ($commenting_post and $commenting_post->exists)
	@php
			$fields = gachet()->commenting()->translateFields($commenting_post->spreadMeta()->fields);
	@endphp
	<div class="part-title">
		<h3>{{ $commenting_post->title2 }}</h3></div>
	<form action="{{ route('gachet.comment.save') }}" id="contact-form" class="js"
		  method="post">
		{{ csrf_field() }}
		<input type="hidden" name="post_id" value="{{ $commenting_post->hashid }}">
		<div class="row">
			@foreach($fields as $field_name => $field_value)
				@switch($field_name)
					@case('first_and_last_name')
					@case('email')
					@case('tel')
					<div class="col-md-4">
						<div class="field">
							<label>{{ trans('validation.attributes.' . $field_name) }}</label>
							<input type="text" name="{{ $field_name }}">
						</div>
					</div>
					@break

					@case('subject')
					<div class="col-md-12">
						<div class="field">
							<label>{{ trans('validation.attributes.' . $field_name) }}</label>

							<div class="select rounded">
								<select name="{{ $field_name }}">

									<option value="{{ $__module->getTrans('general.contact_subjects.sale') }}">
										{{ $__module->getTrans('general.contact_subjects.sale') }}
									</option>
									<option value="{{ $__module->getTrans('general.contact_subjects.tracking') }}">
										{{ $__module->getTrans('general.contact_subjects.tracking') }}
									</option>
									<option value="{{ $__module->getTrans('general.contact_subjects.support') }}">
										{{ $__module->getTrans('general.contact_subjects.support') }}
									</option>
									<option value="{{ $__module->getTrans('general.contact_subjects.suggestions') }}">
										{{ $__module->getTrans('general.contact_subjects.suggestions') }}
									</option>
								</select>
							</div>
						</div>
					</div>
					@break

					@case('text')
					<div class="col-xs-12">
						<div class="field">
							<label>{{ trans('validation.attributes.message') }}</label>
							<textarea rows="4" name="{{ $field_name }}"></textarea>
						</div>
						<div class="contact-action tal">
							<button class="green">{{ $__module->getTrans('form.button.send') }}</button>
							<div class="result" style="text-align: right">
								@include($__module->getBladePath('layouts.widgets.form.feed'))
							</div>
						</div>
					</div>
					@break
				@endswitch
			@endforeach
		</div>
	</form>

	@include($__module->getBladePath('layouts.widgets.form.scripts'))
@endif
