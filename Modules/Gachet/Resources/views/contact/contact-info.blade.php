<div class="row mb20 mt50">
	<div class="col-xs-12">

		@if($shop_manager and is_string($shop_manager))
			<p>
				<b>{{ $__module->getTrans('general.contact.shop_manager') }}:</b>

				<span>{{ ad($shop_manager) }}</span>
			</p>
		@endif

		@if($address and is_string($address))
			<p>
				<b>{{ $__module->getTrans('general.contact.address') }}:</b>
				{{ ad($address) }}
			</p>
		@endif

		@if($postal_code and is_string($postal_code))
			<p>
				<b>{{ $__module->getTrans('general.contact.postal_code') }}:</b>
				{{ ad($postal_code) }}
			</p>
		@endif

		@if($work_hours and is_string($work_hours))
			<p>
				<b>{{ $__module->getTrans('general.contact.work_hours') }}:</b>

				<span>{{ ad($work_hours) }}</span>
			</p>
		@endif

		@php $telephone = gachet()->template()->contactInfo()['telephone'] @endphp
		@if($telephone and is_array($telephone) and count($telephone))
			<p>
				<b>{{ $__module->getTrans('general.contact.telephone') }}:</b>
				@foreach($telephone as $phone)
					@if ($loop->index)
					    -
					@endif
					<a href="tel:{{ $phone }}" style="display: inline-block; direction: ltr;">
						{{ ad($phone) }}
					</a>
				@endforeach
			</p>
		@endif

		@if($fax and is_array($fax) and count($fax))
			<p>
				<b>{{ $__module->getTrans('general.contact.fax') }}:</b>
				@foreach($fax as $phone)
					@if ($loop->index)
						-
					@endif
					<a href="tel:{{ $phone }}" style="display: inline-block; direction: ltr;">{{ ad($phone) }}</a>
				@endforeach
			</p>
		@endif

		@if($whatsapp_call and is_string($whatsapp_call))
			<p>
				<b>{{ $__module->getTrans('general.contact.whatsapp_call') }}:</b>
				<a href="tel:{{ $whatsapp_call }}" style="display: inline-block; direction: ltr;">{{ ad($whatsapp_call) }}</a>

			</p>
		@endif

		@if($email and is_array($email) and count($email))
			<p>
				<b>{{ $__module->getTrans('general.contact.email') }}:</b>
				@foreach($email as $mail)
					@if ($loop->index)
						-
					@endif
					<a href="mailto:{{ $mail }}">{{ ad($mail) }}</a>
				@endforeach
			</p>
		@endif


	</div>
</div>
