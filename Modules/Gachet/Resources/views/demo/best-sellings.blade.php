@php
	/*
	*	ATTENTION! @_@
	*   TODO: Use 200*200 images for 'best selling section' for better appearance. :)
	*/
	$specials = [
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
		[
			"image" => Module::asset('gachet:images/sample/product-2.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
			"price" => "۹۹،۰۰۰" ,
			"sale_price" => "۸۵،۵۰۰ تومان" ,
		],
	];
@endphp


@component($__module->getBladePath('layouts.widgets.items-slider.main'),[
		"title" => [
			"0" => "محصولات" ,
			"1" => "ویژه" ,
		] ,
	])
	@foreach($specials as $special)
		<div class="slide">
			<a href="{{ $special['link'] }}" class="product-item">
				<div class="thumbnail">
					<img src="{{ $special['image'] }}" style="width: 200px; height: 200px;">
				</div>
				<div class="content">
					<h6>
						{{ $special['title'] }}
					</h6>
					<div class="price">
						<del>
							{{ $special['price'] }}
						</del>
						<ins>
							{{ $special['sale_price'] }}
						</ins>
					</div>
				</div>
			</a>
		</div>
	@endforeach
@endcomponent
