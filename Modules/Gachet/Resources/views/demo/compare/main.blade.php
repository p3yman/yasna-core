@extends($__module->getBladePath('layouts.plane'))

@php
	Template::appendToPageTitle('');
	Template::appendToNavBar([
		'alh','ldjvd'
	]);

$products = [
		[
			"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
			"image" => Module::asset('gachet:images/sample/small-product.png') ,
			"price" => "7500000" ,
			"link" => "#" ,
		],
		[
			"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
			"image" => Module::asset('gachet:images/sample/small-product.png') ,
			"price" => "7500000" ,
			"link" => "#" ,
		],
		[
			"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
			"image" => Module::asset('gachet:images/sample/small-product.png') ,
			"price" => "7500000" ,
			"link" => "#" ,
		]
	];

$groups = [
	[
		"title" => "مشخصات فنی" ,
		"rows" => [
			[
				"title" => "وزن" ,
				"cols" => [
					"130" ,
					"-" ,
					"140" ,
				] ,
			],
			[
				"title" => "ارتفاع" ,
				"cols" => [
					"100" ,
					"120" ,
					"140" ,
				] ,
			],
			[
				"title" => "رنگ" ,
				"cols" => [
					"سفید" ,
					"سیاه" ,
					"طوسی" ,
				] ,
			],
		] ,
	],
	[
		"title" => "مشخصات دیگر" ,
		"rows" => [
			[
				"title" => "وزن" ,
				"cols" => [
					"130" ,
					"-" ,
					"140" ,
				] ,
			],
			[
				"title" => "ارتفاع" ,
				"cols" => [
					true ,
					false ,
					"" ,
				] ,
			],
			[
				"title" => "رنگ" ,
				"cols" => [
					"سفید" ,
					"سیاه" ,
					"طوسی" ,
				] ,
			],
		] ,
	],
]
@endphp

@section('body')
	<div class="page-content mt50">
		<div class="container">
			<div class="part-title">
				<h3>
					<span>
						{{ $__module->getTrans('general.compare') }}
					</span>
					{{ $__module->getTrans('general.products') }}
				</h3>
			</div>

			<div class="products-compare">
				<div class="table-responsive">
					<table class="table bordered">

						<thead>
						<tr>
							<th class="empty"></th>
							@foreach($products as $product)
								<th class="head">
									@isset($product['image'])
										<img src="{{ $product['image'] }}">
									@endisset
									<div class="title">{{ $product['title'] or "-" }}</div>
									@isset($product['price'])
										<div class="price">{{ ad(number_format($product['price']))}}</div>
									@endisset
									@isset($product['link'])
										<a href="{{ $product['link'] }}" class="button blue block">
											{{ $__module->getTrans('general.view_and_purchase') }}
										</a>
									@endisset
										<a href="#" class="remove">
											{{ $__module->getTrans('general.remove_compare') }}
										</a>
								</th>
							@endforeach
						</tr>
						</thead>

						<tbody>

						@foreach($groups as $group)
							<tr class="row-title">
								<td colspan="100%">
									{{ $group['title'] }}
								</td>
							</tr>
							@foreach($group['rows'] as $group_row)
								<tr class="row-data">
									<td>
										{{ $group_row['title'] }}
									</td>
									@foreach($group_row['cols'] as $cell)
										<td>
											@if(is_bool($cell))
												<span class="{{ $cell? 'icon-check color-green': 'icon-close color-red' }} fz-lg"></span>
											@else
												{{ $cell }}
											@endif
										</td>
									@endforeach

								</tr>
							@endforeach
						@endforeach

						</tbody>

					</table>

				</div>


			</div>
		</div>
	</div>
@stop
