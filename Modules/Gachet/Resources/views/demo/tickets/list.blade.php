@extends($__module->getBladePath('user.frame.main'))

@php
	$activeTab = 'profile';
	Template::appendToPageTitle($__module->getTrans('user.edit-profile'));
	Template::appendToNavBar([
		$__module->getTrans('user.edit-profile'), CurrentModule::action('UserController@profile')
	]);

$tickets = [
	[
		"id" => "۳۴۵۳۴۵" , 
		"title" => "پیگیری قطعه A3532 در مرحله ارسال به باربری" , 
		"organization" => "حسابداری" , 
		"flag" => [
			"color" => "green" , 
			"title" => "باز" , 
		] ,
		"date" => " ۱۷ خرداد ۹۵" ,
		"link" => "#" ,
	],

	[
		"id" => "۳۴۵۳۴۵" ,
		"title" => "پیگیری قطعه A3532 در مرحله ارسال به باربری" ,
		"organization" => "حسابداری" ,
		"flag" => [
			"color" => "green" ,
			"title" => "باز" ,
		] ,
		"date" => " ۱۷ خرداد ۹۵" ,
		"link" => "#" ,
	],

	[
		"id" => "۳۴۵۳۴۵" ,
		"title" => "پیگیری قطعه A3532 در مرحله ارسال به باربری" ,
		"organization" => "حسابداری" ,
		"flag" => [
			"color" => "green" ,
			"title" => "باز" ,
		] ,
		"date" => " ۱۷ خرداد ۹۵" ,
		"link" => "#" ,
	],

	[
		"id" => "۳۴۵۳۴۵" ,
		"title" => "پیگیری قطعه A3532 در مرحله ارسال به باربری" ,
		"organization" => "حسابداری" ,
		"flag" => [
			"color" => "green" ,
			"title" => "باز" ,
		] ,
		"date" => " ۱۷ خرداد ۹۵" ,
		"link" => "#" ,
	],
];
@endphp

@section('dashboard-body')
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-center">
				<section class="panel">
					<header>
						<div class="title">{{ $__module->getTrans('tickets.title.plural') }}</div>
						<div class="functions">
							<button class="button action blue">
								{{ $__module->getTrans('tickets.new') }}
							</button>
						</div>
					</header>
					<div class="table-responsive bd-n m0">
						<table class="table orders">
							<thead>
								<tr>
									<th>
										{{ $__module->getTrans('tickets.ticket_id') }}
									</th>
									<th>
										{{ $__module->getTrans('tickets.subject') }}
									</th>
									<th>
										{{ $__module->getTrans('tickets.organization') }}
									</th>
									<th>
										{{ $__module->getTrans('tickets.status') }}
									</th>
									<th>
										{{ $__module->getTrans('tickets.updated_at') }}
									</th>
									{{--<th>--}}
									{{--{{ $__module->getTrans('general.table.ticket_id') }}--}}
									{{--</th>--}}
									<th width="1"></th>
								</tr>
							</thead>
							<tbody>
							@foreach($tickets as $ticket)

								<tr>
									<td class="fw-b color-blue-darker">
										{{ ad($ticket['id']) }}
									</td>
									<td>
										{{ $ticket['title'] }}
									</td>
									<td class="fw-b color-gray">
										{{ $ticket['organization'] }}
									</td>
									<td>
										<div class="label {{ $ticket['flag']['color'] }}">
											{{ $ticket['flag']['title'] }}
										</div>
									</td>
									<td>
										{{ ad($ticket['date']) }}
									</td>
									{{--<td>--}}
									{{--<div class="label {{ $ticket['status']['color'] }}">--}}
									{{--{{ $ticket['status']['title'] }}--}}
									{{--</div>--}}
									{{--</td>--}}
									<td>
										<a href="{{$ticket['link']}}" class="button icon-eye gray-lightest alt">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>
	</div>
@stop
