@extends($__module->getBladePath('user.frame.main'))

@php
	$activeTab = 'profile';
	Template::appendToPageTitle($__module->getTrans('user.edit-profile'));
	Template::appendToNavBar([
		$__module->getTrans('user.edit-profile'), CurrentModule::action('UserController@profile')
	]);
@endphp

@section('dashboard-body')
	<div class="container">

		<div class="row">
			<div class="col-sm-8 col-center">
				<section class="panel">
					<article>

						<div class="field">
							<label>
								{{ $__module->getTrans('tickets.subject') }}
							</label>
							<input type="text">
						</div>

						<div class="row">

							<div class="col-sm-6">

								<div class="field">
									<label>
										{{ $__module->getTrans('tickets.organization') }}
									</label>
									<div class="select">
										<select>
											<option value="0">حسابداری</option>
											<option value="1">فروش</option>
											<option value="2">پشتیبانی</option>
											<option value="3">مدیریت</option>
											<option value="4">نیروی انسانی</option>
										</select>
									</div>
								</div>

							</div>

							<div class="col-sm-6">

								<div class="field">
									<label>
										{{ $__module->getTrans('tickets.priority') }}
									</label>
									<div class="select">
										<select>
											<option value="0">زیاد</option>
											<option value="1">معمولی</option>
											<option value="2">کم</option>
										</select>
									</div>
								</div>

							</div>

						</div>

						<div class="field">
							<label>
								{{ $__module->getTrans('tickets.ticket_text') }}
							</label>
							<textarea rows="10"></textarea>
						</div>

						<div class="ta-l mt10">
							<button class="button blue lg">ثبت تیکت</button>
						</div>

					</article>
				</section>
			</div>
		</div>


	</div>
@stop
