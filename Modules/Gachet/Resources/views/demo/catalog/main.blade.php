@extends($__module->getBladePath('layouts.plane'))

@php
	Template::appendToPageTitle('');
	Template::appendToNavBar([
		'alh','ldjvd'
	]);

	$catalogs = [
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
		[
			"image" => $__module->getAsset('images/sample/product.jpg') ,
			"title" => "پسته خام دامغان" ,
			"link" => "#" ,
		],
	];

@endphp

@section('body')
	<div class="container">

		<div class="part-title">
			<h3>
				<span>
					{{ $__module->getTrans('general.catalogs_of') }}
				</span>
				{{ $__module->getTrans('general.products') }}
			</h3>
		</div>

		<div class="row">
			<div class="col-sm-10 col-center">
				<section class="panel">
					<article>
						<form action="">
							<div class="field m0">
								<label>
									{{ $__module->getTrans('general.search_catalogs') }}
								</label>
								<input type="text">
								<button class="button mt10 action blue">
									{{ $__module->getTrans('general.search') }}
								</button>
							</div>
						</form>

					</article>

				</section>

				<section class="mt50">
					<div class="row">
						@foreach($catalogs as $catalog)
							<div class="col-sm-3">
								@include($__module->getBladePath('demo.catalog.catalog-item'),[
									"image" => $catalog['image'] ,
									"title" => $catalog['title'] ,
									"link" => $catalog['link'] ,
								])
							</div>
						@endforeach
					</div>
				</section>
			</div>
		</div>
	</div>
@stop
