<div class="product-item">
	<div class="thumbnail p20">
		<img src="{{ $image }}">
	</div>
	<div class="content">
		<h6>
			{{ $title }}
		</h6>
		<div class="text-center">
			<a href="{{ $link }}" class="button blue">
				{{ $__module->getTrans('general.download') }}
			</a>
		</div>
	</div>
</div>
