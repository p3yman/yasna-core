@include("manage::forms.feed" , [
	'div_class' => "m10",
])

<span class="refresh">
    {{ route('gachet.manage.single-action', ['hashid' => $model->hashid, 'viewFile' => 'winners-table']) }}
</span>
<table class="table table-striped">

    {{--
    |--------------------------------------------------------------------------
    | Header
    |--------------------------------------------------------------------------
    |
    --}}

    <thead>
    <tr>
        <td>#</td>
        <td>{{ trans('validation.attributes.name_first') }}</td>
        <td>{{ trans('validation.attributes.mobile') }}</td>
        <td>{{ $__module->getTrans('cart.purchase.singular') }}</td>
        <td>&nbsp;</td>
    </tr>
    </thead>

    {{--
    |--------------------------------------------------------------------------
    | New Form
    |--------------------------------------------------------------------------
    |
    --}}
    @if($model->isDrawingReady())
        @include($__module->getBladePath('manage.drawing.winners-add'))
    @endif


    {{--
    |--------------------------------------------------------------------------
    | Browser
    |--------------------------------------------------------------------------
    |
    --}}

    @foreach($model->winners_array as $key => $winner)
        @include($__module->getBladePath('manage.drawing.winners-row'))
    @endforeach

    {{--
    |--------------------------------------------------------------------------
    | no-results
    |--------------------------------------------------------------------------
    |
    --}}

    @if(!count($model->winners_array))
        <td colspan="5">
            <div class="no-results m20">
                {{ $__module->getTrans('general.drawing.no-winner-so-far') }}
            </div>
        </td>
    @endif
</table>
