@include('manage::layouts.modal-start' , [
	'form_url' => route('gachet.manage.draw.select'),
	'modal_title' => $__module->getTrans('general.drawing.winner').' '.$model->title,
])
<div class='modal-body'>

    <div id="divWinnersTable" class="panel panel-default m10">
        @include($__module->getBladePath('manage.drawing.winners-table'))
    </div>

</div>

@if(!$model->isDrawingReady())
    <div class="modal-footer">
        @include("manage::forms.button" , [
            'label' => $__module->getTrans('general.drawing.redraw'),
            'shape' => "primary",
            'link' => "masterModal('" . route('gachet.manage.draw', ['hashid' => $model->hashid]) . "')",
        ])
    </div>
@endif

@include('manage::layouts.modal-end')
