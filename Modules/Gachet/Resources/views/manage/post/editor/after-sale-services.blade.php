<div class="mv20">
	{!!
		widget('textarea')
			->label($__module->getTrans('general.supports'))
			->id('txt-after-sale-services')
			->name('after_sale_services')
			->value($model->getMeta('after_sale_services'))
			->class('tinyEditor')
			->onChange('postFormChange()')
	!!}
</div>
