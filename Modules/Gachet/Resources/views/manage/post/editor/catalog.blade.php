<div class="mv20">
	@include(module('filemanager')->getBladePath('presenters.input-file'), [
		'name' => 'catalog',
		'value' => $model->catalog,
		'label' => $__module->getTrans('validation.attributes.catalog'),
		'config_name' => 'catalogs'
	])
</div>
