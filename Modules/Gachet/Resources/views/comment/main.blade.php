<div class="comment-section">
    <h2>
        {{ $__module->getTrans('general.comment.title.plural') }}
    </h2>
    @include($__module->getBladePath('comment.comments'))
</div>
