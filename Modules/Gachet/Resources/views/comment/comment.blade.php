<!-- Comments -->
@php $canReply = !$comment->parent_id @endphp
<div class="comment" data-key="{{ $comment->hashid }}">
    @php $user = $comment->user @endphp
    <a class="user-avatar" {{--href="#"--}}> {{--@todo: go to user profile page--}}
        <div class="avatar-wrapper">
            <div class="avatar round-image">
                <img src="{{ $user->gravatar($__module->getAsset('images/user-3.png')) }}"
                     alt="{{ $user->full_name }}">
            </div>
        </div>
    </a>
    <div class="contents">
        {{--@todo: go to user profile page--}}
        <a {{--href="{{ $user['link'] or ""}}"--}} class="user-name">{{ $user->full_name }}</a>
        <span class="time">{{ ad(echoDate($comment->created_at, 'j F Y')) }}</span>
        <div class="comment-text">
            {!! nl2br($comment->text) !!}
        </div>
        <div class="actions">
            @if ($canReply) {{--If this comment is not reply of another comment, it can be replied--}}
            <a href="#" class="btn reply">
                <i class="fa fa-mail-reply" aria-hidden="true"></i>
                {{ $__module->getTrans('general.comment.answer') }}
            </a>
            @endif
        </div>

        {{--@if ($canReply)--}}
		{{--@include($__module->getBladePath('comment.new-comment-form'), ['invisible' => true, 'parent' => $comment->hashid])--}}
        {{--@endif--}}
    </div>


    @if (
        $canReply and
        ($replies = gachet()->commenting()->selectCommentReplies($comment, $commentRepliesLimit)) and
        $replies->count()
    )
        <div class="comments">
            {{--@foreach($replies as $reply)--}}
			{{--@include($__module->getBladePath('comment.comment'), ['comment' => $reply])--}}
            {{--@endforeach--}}
            @include($__module->getBladePath('comment.comments-loop'), ['comments' => $replies, 'parent' => $comment])
        </div>
    @endif
</div>
<!-- !END Comments -->
