@if (user()->id or $posttype->commenting_allow_guests)
	<div class="reply-comment pb10">
		{{--<h2>{{ trans('persiandesign::general.leave_comment') }}</h2>--}}
		<div class="row">

			<div class="col-md-12">
				<div class="reply-preview" style="display: none;">
                    <span class="reply-icon">
                        <i class="fa fa-mail-reply" aria-hidden="true"></i>
                    </span>
					<span class="reply-close" onclick="escapeReplyComment()">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </span>
					<span class="parent-text"></span>
				</div>
			</div>

			{!! Form::open([
				'id' => 'comment-form',
				'url' => route('gachet.comment.save'),
				'class' => 'js'
			]) !!}
			<input type="hidden" name="post_id" value="{{ $post->hashid }}">
			<input type="hidden" name="parent_id" value="">
			<div class="col-sm-4">
				<!-- Fullname -->
				<label for="fullname">{{ $__module->getTrans('general.comment.form.fullname') }}</label>
				<input type="text" name="name" id="fullname" class="reply-input form-required"
					   placeholder="{{ $__module->getTrans('general.comment.form.fullname') }}"
					   error-value="{{ $__module->getTrans('validation.javascript-validation.name') }}"
					   value="{{ user()->id ? user()->full_name : '' }}"
				>
			</div>
			<div class="col-sm-4">
				<!-- Email -->
				<label for="email">{{ $__module->getTrans('general.comment.form.email') }}</label>
				<input type="email" name="email" id="email" class="reply-input form-required form-email"
					   placeholder="{{ $__module->getTrans('general.comment.form.email') }}"
					   error-value="{{ $__module->getTrans('validation.javascript-validation.email') }}"
					   value="{{ user()->id ? user()->email : '' }}"
				>
			</div>
			<div class="col-sm-4">
				<!-- Url -->
				<label for="url">{{ $__module->getTrans('validation.attributes.mobile') }}</label>
				<input type="text" name="mobile" id="url" class="reply-input form-required form-mobile"
					   placeholder="{{ $__module->getTrans('validation.attributes.mobile') }}"
					   error-value="{{ $__module->getTrans('validation.javascript-validation.mobile') }}"
					   value="{{ user()->id ? user()->mobile : '' }}"
				>
			</div>

			<!-- Comment -->
			<div class="col-md-12">
				<label for="comment">{{ $__module->getTrans('general.comment.title.singular') }}</label>
				<textarea name="text" id="comment" cols="30" rows="10" class="reply-input form-required"
						  placeholder="{{ $__module->getTrans('general.comment.form.statement')."..." }}"
						  error-value="{{ $__module->getTrans('validation.javascript-validation.text') }}"></textarea>
			</div>

			<div class="col-xs-12 mt10">
				@include($__module->getBladePath('layouts.widgets.form.feed'))
			</div>

			<div class="col-sm-6 mt10">
				<button class="green" type="submit" name="_submit">
					{{ $__module->getTrans('general.comment.submit') }}
				</button>
			</div>

			{!! Form::close() !!}
		</div>
	</div>
@endif
