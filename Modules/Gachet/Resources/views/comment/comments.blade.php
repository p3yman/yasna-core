@if ($posttype->getMeta('commenting_receive'))
    <div class="comments">
        @include($__module->getBladePath('comment.new-comment-form'), ['general' => true])
        @if ($posttype->getMeta('commenting_display'))
            @php
                $comments = gachet()->commenting()->selectPostComments($post, $commentsLimit);
            @endphp
            @include($__module->getBladePath('comment.comments-loop'))
        @endif
    </div>
@endif
