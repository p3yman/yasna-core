@extends($__module->getBladePath('purchase.layout.plane'))

@php
    $currentStep = 5;
    $statusSlug = $transactionHandler->statusSlug();
    $statusCode = $transactionHandler->statusCode();
    $isFinalized = $transactionHandler->isFinalized();
    $isRaised = $cart->isRaised();
    $isPaid = $cart->isPaid();
    $isDisbursed = $cart->isDisbursed();
    $isOnline = $transactionHandler->isOnline();
    $type = $transactionHandler->type();
@endphp

@section('inner-body')
    <section class="panel">
        <article>
            @php $transactionSuccess = isset($transactionSuccess) ? $transactionSuccess : false @endphp
            @if (isset($transactionAlert))
                <div class="alert alt @if($transactionSuccess) green icon-check @else red icon-close @endif">
                    <p>{{ $transactionAlert }}</p>
                </div>
            @endif
            <div class="row">
                @include($__module->getBladePath('purchase.step5.result'))
                @include($__module->getBladePath('purchase.step5.status-info'))
                @include($__module->getBladePath('purchase.step5.delivery-info'))
            </div>
            @include($__module->getBladePath('purchase.step5.transaction-history'))
        </article>
    </section>
@append

@include($__module->getBladePath('purchase.step5.scripts'))
