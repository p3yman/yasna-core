@if (!$isDisbursed)
    @if (in_array($statusSlug, ['sent-to-gateway', 'created']))
        @if ($transactionHandler->isDeclarable())
            @include($__module->getBladePath('purchase.step5.declare-form'))
        @endif
    @endif
    
    @include($__module->getBladePath('purchase.step5.make-action-online'))
@endif
