@if (!$isDisbursed)
    @if ($isOnline)
        <div class="label gray whs-n">
            {{ $__module->getTrans('cart.payment.message.unsuccess-online-hint') }}
        </div>
    @else
        <button class="button blue" id="pay-online" type="button">
            {{ $__module->getTrans('cart.button.purchase-online') }}
        </button>
    @endif
    @php $onlineAccounts = payment()->accounts()->online()->get() @endphp
    @if ($onlineAccounts->count())
        <div class="row" id="online-payment" @if(!$isOnline) style="display: none" @endif>
            <div class="col-xs-12">
                <div class="color-dark" style="font-size: 13px;margin-top: 15px;">
                    {{ $__module->getTrans('cart.payment.message.select-gateway-and-purchase') }}
                </div>
                {!! Form::open([
                    'url' => route('gachet.payment.repurchase', ['transaction' => $transactionHandler->getModel()->hashid]),
                    'method'=> 'post',
                    'class' => 'js',
                    'id' => 'purchase-form',
                ]) !!}
                @include($__module->getBladePath('layouts.widgets.form.radio-group'), [
                    'name' => 'account_id',
                    'label' => false,
                    'value' => $onlineAccounts->first()->hashid,
                    'options' => $onlineAccounts->pluck('name', 'hashid')->toArray(),
                    'class' => 'form-required',
                    'color' => 'green',
                ])
                <button class="button block green">
                    {{ $__module->getTrans('cart.button.purchase-online') }}
                </button>
                <div class="row">
                    <div class="col-xs-12" style="margin-top:12px">
                        @include($__module->getBladePath('layouts.widgets.form.feed'))
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    @endif
@endif
