<div class="col-sm-4">
    @php
        $address = $cart->address;
    @endphp
    <table class="table bordered">
        <thead>
        <tr>
            <th colspan="100%" class="tac">{{ $__module->getTrans('cart.headline.delivery-info') }}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="whs-nw">{{ $__module->getTrans('cart.headline.receiver') }}</td>
            <td class="color-dark fw-b">{{ $address->receiver }}</td>
        </tr>
        <tr>
            <td>{{ $__module->getTrans('general.address.title.singular') }}</td>
            <td class="color-dark">{{ $address->address }}</td>
        </tr>
        <tr>
            <td>{{ $__module->getTrans('general.contact.telephone') }}</td>
            <td class="color-dark">{{ ad($address->telephone) }}</td>
        </tr>
        </tbody>
    </table>
</div>
