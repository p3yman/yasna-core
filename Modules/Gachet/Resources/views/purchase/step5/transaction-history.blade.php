@if ($transactions->count())
    <hr>
    <h3 class="mb15">{{ $__module->getTrans('cart.headline.your-purchasements-details') }}</h3>
    <table class="table bordered">
        <thead>
        <tr>
            <th>{{ $__module->getTrans('general.row') }}</th>
            <th>{{ $__module->getTrans('cart.headline.gateway') }}</th>
            <th>{{ $__module->getTrans('cart.headline.payment-type') }}</th>
            <th>{{ $__module->getTrans('cart.headline.receipt-number') }}</th>
            <th>{{ $__module->getTrans('general.date') }}</th>
            <th>{{ $__module->getTrans('cart.headline.amount') }}</th>
            <th>{{ $__module->getTrans('cart.payment.status') }}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transactions as $transaction)
            @php
                $account = $transaction->account
            @endphp
            <tr>
                <td>{{ ad($loop->iteration) }}</td>
                <td>{{ $account->title }}</td>
                <td>{{ $__module->getTrans('cart.payment-method.option.' . $account->type) }}</td>
                <td>{{ ad($transaction->tracking_no) }}</td>
                <td>{{ ad(echoDate($transaction->created_at, 'j F Y - H:i')) }}</td>
                <td>{{ gachet()->shop()->showPrice($transaction->payable_amount) }}</td>
                <td class="fw-b @if($transaction->handler->statusCode() > 0) color-green @else color-red @endif">
                    {{ $__module->getTrans('cart.payment.status-levels.' . $transaction->status ) }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
