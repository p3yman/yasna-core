<div class="row" id="declare-payment">
	<div class="col-xs-12">
		{!! Form::open([
				'url' => route('gachet.payment.declare', ['transaction' => $transactionHandler->getModel()->hashid]) ,
				'method'=> 'post',
				'class' => 'js',
				'id' => 'purchase-form',
			]) !!}

		<table class="table bordered method-offline" style="width: 100% !important;">
			<tbody>
			<tr>
				<td class="p30">
					<div class="field">
						<label>{{ $__module->getTrans('form.amount') }}</label>
						<input type="text" name="amount">
					</div>
					<div class="field">
						<label>{{ $__module->getTrans('cart.headline.tracking-code') }}</label>
						<input type="text" name="trackingCode"/>
					</div>
					<div class="field">
						<label>{{ $__module->getTrans('cart.headline.declaration-date') }}</label>
						<input type="text" name="declarationDate"
							   placeholder="{{ ad(\Morilog\Jalali\jDateTime::date('Y/m/d')) }}"/>
					</div>
					<div class="tal">
						<button class="button green block">
							{{ $__module->getTrans('form.button.submit') }}
						</button>
					</div>
				</td>
			</tr>
			</tbody>
		</table>
		<div class="row">
			<div class="col-xs-12" style="margin-top:12px">
				@include($__module->getBladePath('layouts.widgets.form.feed'))
			</div>
		</div>

		{!! Form::close() !!}
	</div>
</div>
