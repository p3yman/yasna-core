@include($__module->getBladePath('layouts.widgets.form.scripts'))

@section('html-footer')
    <script>
        $(document).ready(function () {
            $('#pay-online').click(function () {
                $('#declare-payment').slideUp();
                $('#online-payment').slideDown();
                $(this).hide();
            });
        });
    </script>
@append
