<div class="label green whs-n sm">
    {{ $__module->getTrans('cart.message.thanks-for-purchasement') }}
</div>
<p class="text-xs " style="font-size: 13px; margin-top: 20px">
    {{ $__module->getTrans('general.message.any-question-about-your-order') }}
    <a href="{{ RouteTools::getPosttypeRoute('faqs') }}">{{ $__module->getTrans('post.faq.plural') }}</a>
</p>



@if(($telephone = get_setting('telephone')) and is_array($telephone) and count($telephone))
    <p>
        {{ $__module->getTrans('general.contact.telephone') }}: {{ ad(implode(' - ', $telephone)) }}
    </p>
@endif
