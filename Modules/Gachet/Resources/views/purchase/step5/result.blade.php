<div class="col-sm-4">
    @if ($isDisbursed or ($type == 'cash'))
        @include($__module->getBladePath('purchase.step5.final-page'))
    @endif
    
    @include($__module->getBladePath('purchase.step5.make-action'))
</div>
