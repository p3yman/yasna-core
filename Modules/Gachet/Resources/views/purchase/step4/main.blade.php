@extends($__module->getBladePath('purchase.layout.plane'))

@php
    $currentStep = 4;
    $any_account_exists = $online_accounts->count() or
        $deposit_accounts->count() or
        $shetab_accounts->count() or
        $cash_accounts->count();
@endphp

@section('inner-body')
    @if ($any_account_exists)
        {!! Form::open([
                'url' => route('gachet.purchase.final'),
                'method'=> 'post',
                'id' => 'payment-form',
            ]) !!}
        {{--@include($__module->getBladePath('purchase.step4.discount-code'))--}}
        {{--<hr>--}}
        @include($__module->getBladePath('purchase.step4.payment-method'))
        @include($__module->getBladePath('purchase.step4.buttons'))
        {!! Form::close() !!}
    @else
        <div class="alert">{{ $__module->getTrans('cart.alert.error.system-error-in-purchasement') }}</div>
    @endif
@append

@include($__module->getBladePath('purchase.step4.scripts'))
