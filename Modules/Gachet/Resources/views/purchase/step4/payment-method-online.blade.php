@if($online_accounts->count())
    <div class="selectable-radio">
        <div class="radio green">
            <input id="radio-1" type="radio" name="paymentType" value="online">
            <label for="radio-1"></label></div>
        <div class="left">
            <h5>{{ $__module->getTrans('cart.payment-method.option.online') }}</h5>
            <div class="hide-content">
                <p>
                    {{ $__module->getTrans('cart.message.select-gateway-hint') }}
                </p>
                <div class="row">
                    <div class="col-sm-7">
                        @foreach ($online_accounts as $account)
                            @if (($loop->iteration % 2) == 1)
                                <div class="row">
                                    @endif
                                    <div class="col-sm-6">
                                        <div class="radio blue">
                                            <input id="radio-{{ $account->hashid }}" type="radio" name="onlineAccount"
                                                   value="{{ $account->hashid }}" @if ($loop->first) checked @endif>
                                            <label for="radio-{{ $account->hashid }}">
                                                {{ $account->title }}
                                            </label>
                                        </div>
                                    </div>
                                    @if (($loop->iteration % 2) == 0 or $loop->last)
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
