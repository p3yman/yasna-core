<section class="panel checkout-actions mt30">
	<article>
		<a href="{{ route('gachet.purchase.step3') }}" class="button">
			{{ $__module->getTrans('form.button.back') }}
		</a>
		<button type="submit" class="button green">
			{{ $__module->getTrans('cart.button.confirm-and-pay') }}
		</button>
	</article>
</section>
