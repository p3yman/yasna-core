@php
	$cart = gachet()->cart()->findActiveCart();
	$min_payable_amount = $cart->min_payable_amount;
@endphp
@if ($cash_accounts->count() and (!$min_payable_amount or $online_accounts->count()) and $cart->address->province_id == 8)
	@php $cashAccount = $cash_accounts->first() @endphp
	@php $cashAccount->spreadMeta() @endphp
	<input type="hidden" name="cashAccount" value="{{ $cashAccount->hashid }}"/>
	<div class="selectable-radio">
		<div class="radio green">
			<input id="radio-4" type="radio" name="paymentType" value="cash">
			<label for="radio-4"></label>
		</div>
		<div class="left">
			<h5>{{ $__module->getTrans('cart.payment-method.option.cash') }}</h5>
			<div class="hide-content">
				@if(get_setting('minimum_payment'))
					@if ($min_payable_amount)
						<div class="alert alert-danger col-sm-12">
							{{
								$__module->getTrans('cart.message.min-payable-amount', [
									'amount' => gachet()->shop()->showPrice($min_payable_amount)
								])
							}}
						</div>
						<div class="col-sm-7">
							@foreach ($online_accounts as $account)
								@if (($loop->iteration % 2) == 1)
									<div class="row">
										@endif
										<div class="col-sm-6">
											<div class="radio blue">
												<input id="radio-{{ $account->hashid }}" type="radio"
													   name="online_min_account"
													   value="{{ $account->hashid }}" @if ($loop->first) checked @endif>
												<label for="radio-{{ $account->hashid }}">
													{{ $account->title }}
												</label>
											</div>
										</div>
										@if (($loop->iteration % 2) == 0 or $loop->last)
									</div>
								@endif
							@endforeach
						</div>
					@endif
				@endif
			</div>
		</div>
	</div>
@endif
