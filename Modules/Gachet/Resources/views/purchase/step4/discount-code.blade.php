{{-- @todo: uncomment this lines and write ajax codes to apply code --}}
<div class="selectable-checkbox">
    <div class="checkbox blue"><input id="check-1" type="checkbox" name="address-radio"> <label for="check-1">
        </label></div>
    <div class="left">
        <h5>
            {{ $__module->getTrans('cart.discount.i-have-code') }}
        </h5>
        <div class="hide-content">
            <div class="row">
                <div class="col-sm-7">
                    <p>
                        {{ $__module->getTrans('cart.discount.message.register-and-submit') }}
                    </p>
                </div>
                <div class="col-sm-5">
                    <div class="field mb0">
                        <div class="action f-l"><input type="text" name="discountCode">
                            <a href="#" class="button blue">
                                {{ $__module->getTrans('cart.discount.register-code') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
