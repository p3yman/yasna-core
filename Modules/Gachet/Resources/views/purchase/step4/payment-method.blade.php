<div class="payments-methods">
    <h4 class="mb10 color-gray">{{ $__module->getTrans('cart.payment-method.title.plural') }}</h4>
    @include($__module->getBladePath('purchase.step4.payment-method-online'))
    @include($__module->getBladePath('purchase.step4.payment-method-deposit'))
    @include($__module->getBladePath('purchase.step4.payment-method-shetab'))
    @include($__module->getBladePath('purchase.step4.payment-method-cash'))
</div>
