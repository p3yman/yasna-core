@if ($deposit_accounts->count())
    <div class="selectable-radio">
        <div class="radio green">
            <input id="radio-2" type="radio" name="paymentType" value="deposit">
            <label for="radio-2"></label>
        </div>
        <div class="left">
            <h5>{{ $__module->getTrans('cart.payment-method.option.deposit') }}</h5>
            <div class="hide-content">
                <table style="width: 100%" class="radio-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ $__module->getTrans('cart.headline.account-number') }}</th>
                        <th>{{ $__module->getTrans('cart.headline.bank') }}</th>
                        <th>{{ $__module->getTrans('cart.headline.account-owner') }}</th>
                    </tr>
                    </thead>
                    <tbody style="text-align: center">
                    @foreach($deposit_accounts as $depositAccount)
                        <tr>
                            @php $depositAccount->spreadMeta() @endphp
                            <td>
                                <input type="radio" name="depositAccount" value="{{ $depositAccount->hashid }}"
                                       @if ($loop->first) checked @endif />
                            </td>
                            <td>{{ $depositAccount->account_number }}</td>
                            <td>{{ $depositAccount->bank_name }}</td>
                            <td>{{ $depositAccount->owner }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
