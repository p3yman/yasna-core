@if ($shetab_accounts->count())
    <div class="selectable-radio">
        <div class="radio green">
            <input id="radio-3" type="radio" name="paymentType" value="shetab">
            <label for="radio-3"></label>
        </div>
        <div class="left">
            <h5>{{ $__module->getTrans('cart.payment-method.option.shetab') }}</h5>
            <div class="hide-content">
                <table style="width: 100%" class="radio-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ $__module->getTrans('cart.headline.card-number') }}</th>
                        <th>{{ $__module->getTrans('cart.headline.bank') }}</th>
                        <th>{{ $__module->getTrans('cart.headline.account-owner') }}</th>
                    </tr>
                    </thead>
                    <tbody style="text-align: center">
                    @foreach($shetab_accounts as $shetabAccount)
                        <tr>
                            @php $shetabAccount->spreadMeta() @endphp
                            <td>
                                <input type="radio" name="shetabAccount" value="{{ $shetabAccount->hashid }}"
                                       @if ($loop->first) checked @endif />
                            </td>
                            <td>{{ rtrim(chunk_split($shetabAccount->card_number, 4, '-'), '-') }}</td>
                            <td>{{ $shetabAccount->bank_name }}</td>
                            <td>{{ $shetabAccount->owner }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
