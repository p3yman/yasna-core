@if($cart->address)
    @if($cart->address->province_id === 8)
        @if(count($delivery))
                <section class="panel">
                    <header>
                        <div class="title">
                            <span class="icon-truck"></span>
                            {{ $__module->getTrans('cart.delivery.delivery_method') }}
                        </div>
                    </header>
                    <article>
                            @include($__module->getBladePath('purchase.step2.delivery_item'))
                    </article>
                </section>
        @endif
    @endif
@endif
