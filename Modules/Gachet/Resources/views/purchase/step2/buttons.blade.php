@include($__module->getBladePath('layouts.widgets.form.feed'), ['div_class' => 'verify-feedback'])
<section class="panel checkout-actions">
	<article>
		<a href="{{ route('gachet.cart.index') }}"
		   class="button">{{ $__module->getTrans('form.button.back') }}</a>
		<button class="button green" type="button" id="continue">
			{{ $__module->getTrans('cart.button.confirm-and-review') }}
		</button>
	</article>
</section>
