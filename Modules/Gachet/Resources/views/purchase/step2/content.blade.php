<div class="col-sm-12">

	<div class="row">

		<div class="col-sm-7">

			@include($__module->getBladePath('purchase.step2.addresses'))

			@if($cart->existHeavyProduct())
				@include($__module->getBladePath('purchase.step2.delivery'))
			@endif

			@include($__module->getBladePath('purchase.step2.description'))

		</div>


		<div class="col-sm-5">

			@include($__module->getBladePath('purchase.step2.couriers'))

			@include($__module->getBladePath('purchase.step2.delivery_time'))

			@include($__module->getBladePath('purchase.step2.work_days'))

			@include($__module->getBladePath('purchase.step2.factor'))

		</div>


	</div>

	<div class="row">

		@include($__module->getBladePath('purchase.step2.official-bill'))

	</div>

</div>
