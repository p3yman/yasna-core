<section class="panel">
	<header>
		<div class="title">
			<span class="icon-truck"></span>
			{{ $__module->getTrans('cart.description') }}
		</div>
	</header>
	<article id="description-data">
		<div class="field">
			<textarea type="text" rows="4" name="description">{{ $cart->getMeta('description') }}</textarea>
		</div>
	</article>
</section>
