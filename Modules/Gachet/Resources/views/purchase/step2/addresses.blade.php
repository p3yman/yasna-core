<div class="">
	<section class="panel">
		<header>
			<div class="title">
				<span class="icon-location"></span>{{ $__module->getTrans('general.address.select') }}
			</div>
			<div class="functions">
				<button class="blue" data-modal="add-address-modal">
					{{ $__module->getTrans('general.address.add') }}
				</button>
			</div>
		</header>
		<article>
			@if ($addresses->count())
				@foreach($addresses as $address)
					@include($__module->getBladePath('purchase.step2.address-item'))
				@endforeach
			@else
				<div class="alert text-center">
					{{ $__module->getTrans('general.address.no-address-alert') }}
				</div>
			@endif
		</article>
	</section>
</div>

@section('inner-body-modals')
	@include($__module->getBladePath('user.addresses.form'))
@append
