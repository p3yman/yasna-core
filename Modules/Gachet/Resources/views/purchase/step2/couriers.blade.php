@if(count($shipping_methods))
        <section class="panel">
            <header>
                <div class="title">
                    <span class="icon-truck"></span>
                    {{ $__module->getTrans('cart.courier.title.singular') }}
                </div>
            </header>
            <article>
                @foreach($shipping_methods as $shipping_method)
                    @include($__module->getBladePath('purchase.step2.courier-item'))
                @endforeach
            </article>
        </section>
@endif
