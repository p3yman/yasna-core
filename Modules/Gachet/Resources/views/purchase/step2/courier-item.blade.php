@php $hashid = $shipping_method->hashid @endphp
<div class="radio selectable-type-radio green">
	<input id="radio-{{ $hashid }}" type="radio" name="courier" value="{{ $hashid }}"
		   @if($shipping_method->id == $cart->shipping_method_id) checked @endif>
	<label for="radio-{{ $hashid }}">
		<h5>{{ $shipping_method->getTitleInCurrentLocale() }}</h5>
		{{--<p>{{ $courier->descriptionIn(getLocale()) }}</p>--}}
		<p>
			@if ($delivery_price = $shipping_method->priceFor($cart->toShippingInvoice()))
				{{ $__module->getTrans('cart.price.delivery') }}:
				{{ $__module->getTrans('cart.delivery.from') }}
				{{ gachet()->shop()->showPrice($delivery_price) }}
				{{--@else--}}
				{{--<span class="text-green">--}}
				{{--<i class="fa fa-check"></i> {{ $__module->getTrans('cart.price.free') }}--}}
				{{--</span>--}}
			@endif
		</p>
	</label>
</div>
