@if ($cart->officialBillIsNotSupported())
	@php return @endphp
@endif

<div class="col-sm-12">
	<div class="panel">
		<article id="official-bill-data">
			<div class="checkbox">
				<input type="checkbox" id="official-bill" name="official_bill"
					   @if($cart->officialBillIsNeeded()) checked @endif>
				<label for="official-bill">
					{{ $__module->getTrans('cart.i-need-official-bill') }}
				</label>
			</div>

			<div class="mt20 row" id="official-bill-info" style="display: none">
				<div class="col-sm-4">
					<div class="field">
						<label for="company-name">
							{{ module('cart')->getTrans('official-bill.company-name') }}
						</label>
						<input type="text" id="company-name" name="company_name"
							   value="{{ $cart->getMeta('company_name') }}">
					</div>
				</div>
				<div class="col-sm-4">
					<div class="field">
						<label for="economical-id">
							{{ module('cart')->getTrans('official-bill.economical-id') }}
						</label>
						<input type="text" id="economical-id" name="economical_id"
							   value="{{ $cart->getMeta('economical_id') }}">
					</div>
				</div>
				<div class="col-sm-4">
					<div class="field">
						<label for="registration-id">
							{{ module('cart')->getTrans('official-bill.registration-id') }}
						</label>
						<input type="text" id="registration-id" name="registration_id"
							   value="{{ $cart->getMeta('registration_id') }}">
					</div>
				</div>
			</div>

		</article>
	</div>
</div>
