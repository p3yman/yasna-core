@php $hashid = hashid($key) @endphp
<div class="radio selectable-type-radio green">
	<input id="radio3-{{ $hashid }}" type="radio" name="delivery_time" value="{{$time}}"
		   @if(isset($cart['meta']['delivery_time']) && $cart['meta']['delivery_time'] == $time) checked @endif>
	<label for="radio3-{{ $hashid }}">
		<h5>{{ pd($time) }}</h5>
	</label>
</div>
