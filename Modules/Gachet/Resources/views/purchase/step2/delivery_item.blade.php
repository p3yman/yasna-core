@php
	$hashid =15;
	$price = is_numeric(get_setting('price_heavy')) ? ad(number_format(round(gachet()->shop()->priceToShow(get_setting('price_heavy'))))) : ad(0) ;
@endphp
<div class="radio selectable-type-radio green">
	<input id="radio2-{{ hashid(1) }}" type="radio" name="delivery" value="next-vehicle"
		   @if($delivery_active === 'next-vehicle') checked @endif>
	<label for="radio2-{{ hashid(1) }}">
		<h5>{{get_setting('next-vehicle')[0]}}</h5>
		<h6 style="color: #888">{{get_setting('next-vehicle')[1] }}</h6>
	</label>
</div>
<div class="radio selectable-type-radio green">
	<input id="radio2-{{ hashid(2) }}" type="radio" name="delivery" value="distance-place"
		   @if($delivery_active ==='distance-place') checked @endif>
	<label for="radio2-{{ hashid(2) }}">
		<h5>{{get_setting('distance-place')[0]}}</h5>
		<h6 style="color: #888">{{get_setting('distance-place')[1] }}</h6>
	</label>
</div>
<div class="radio selectable-type-radio green">
	<input id="radio2-{{ hashid(3) }}" type="radio" name="delivery" value="on-floor"
		   @if($delivery_active === 'on-floor') checked @endif>
	<label for="radio2-{{ hashid(3) }}">
		<h5>{{get_setting('on-floor')[0]}}</h5>
		<h6 style="color: #888">{{get_setting('on-floor')[1] }}</h6>
	</label>
</div>
<div class="radio selectable-type-radio green">
	<input id="radio2-{{ hashid(4) }}" type="radio" name="delivery" value="distance-place-floor"
		   @if($delivery_active === 'distance-place-floor') checked @endif>
	<label for="radio2-{{ hashid(4) }}">
		<h5>{{get_setting('distance-place-floor')[0]}}</h5>
		<h6 style="color: #888">{{get_setting('distance-place-floor')[1] }}</h6>
	</label>
</div>

