@extends($__module->getBladePath('purchase.layout.plane'))

@php
    $currentStep = 2;
@endphp

@section('inner-body')
    @if ($ready_for_purchasement)
        <div id="page-content" class="row">
            @include($__module->getBladePath('purchase.step2.content'))
        </div>
        @include($__module->getBladePath('purchase.step2.buttons'))
    @else
        <div class="alert">{{ $__module->getTrans('cart.alert.error.system-error-in-purchasement') }}</div>
    @endif
@append

@include($__module->getBladePath('purchase.step2.scripts'))
