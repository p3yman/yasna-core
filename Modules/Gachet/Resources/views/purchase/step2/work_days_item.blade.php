@php
$hashid = hashid($key);
$timestamp = Carbon\Carbon::parse($day->date)->timestamp;
@endphp
<div class="radio selectable-type-radio green">
	<input id="radio4-{{ $hashid }}" type="radio" name="work_day" value="{{$timestamp}}"
		   @if(isset($cart['meta']['work_day']) && $cart['meta']['work_day'] == $timestamp) checked @endif>
	<label for="radio4-{{ $hashid }}">
		<h5>{{pd(jDate::forge($day->date)->format('l j F Y'))}}</h5>
	</label>
</div>
