@if($cart->address)
	@if($cart->address->province_id === 8)
		@if(count($delivery_time))
			<section class="panel">
				<header>
					<div class="title">
						<span class="icon-truck"></span>
						{{ $__module->getTrans('cart.delivery_time') }}
					</div>
				</header>
				<article>
					@foreach($delivery_time as $key=>$time)
						@include($__module->getBladePath('purchase.step2.delivery_time_item'))
					@endforeach
				</article>
			</section>
		@endif
	@endif
@endif
