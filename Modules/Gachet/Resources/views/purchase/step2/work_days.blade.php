@php
$work_day_title=  get_setting('title-work-day-city') == '' ? $__module->getTrans('cart.work_day') : get_setting('title-work-day-city') ;
@endphp
@if(count($work_day))
		<section class="panel">
			<header>
				<div class="title">
					<span class="icon-truck"></span>
					@if($cart->address)
						@if($cart->address->province_id === 8)
							 {{ $__module->getTrans('cart.work_day') }}
						@else
							{{$work_day_title}}
						@endif
					@endif
				</div>
			</header>
			<article>
				@foreach($work_day as $key=>$day)
					@include($__module->getBladePath('purchase.step2.work_days_item'))
				@endforeach
			</article>
		</section>
@endif
