<section class="panel">
	<header>
		<div class="title">
			<span class="icon-truck"></span>
			{{ $__module->getTrans('cart.factor') }}
		</div>
	</header>
	<article>
		<div class="checkbox">
			<input id="checkbox-factor" type="checkbox" name="factor_request"
				   @if($factor === '1') checked @endif>
			<label for="checkbox-factor">
				{{ $__module->getTrans('cart.factor') }}
			</label>
		</div>
	</article>
</section>

