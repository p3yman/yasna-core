@extends($__module->getBladePath('layouts.plane'))

@section('body')
    <div class="page-content">
        <div class="container">
            <div class="checkout-steps">
                @php
                    $steps = [
                        $__module->getTrans('user.login'),
                        $__module->getTrans('cart.headline.shipping-info'),
                        $__module->getTrans('cart.headline.review-order'),
                        $__module->getTrans('cart.payment.title.singular'),
                    ];
                @endphp
                @if (!isset($currentStep))
                    @php $currentStep = 1 @endphp
                @endif

                @foreach($steps as $step)
                    @php
                        $stepNumber = $loop->iteration;
                        $done = false;
                        $current = false;
                    @endphp
                    @if($stepNumber < $currentStep)
                        @php $done = 'done' @endphp
                    @else
                        @if ($stepNumber == $currentStep)
                            @php $current = 'current' @endphp
                        @endif
                    @endif

                    <div class="step {{ $done }} {{ $current }}">
                        <div class="count">
                            @if($done)
                                <span class="icon-check"></span>
                            @else
                                {{ ad($stepNumber) }}
                            @endif
                        </div>
                        <h5>{{ $step }}</h5>
                    </div>
                @endforeach
            </div>

            @yield('inner-body')
        </div>
    </div>
    @yield('inner-body-modals')
@append
