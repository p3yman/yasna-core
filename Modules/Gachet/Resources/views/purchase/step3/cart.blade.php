<div class="cart-full" id="page-content">
    @include($__module->getBladePath('purchase.step3.cart-items-list'))
    @include($__module->getBladePath('purchase.step3.cart-items-list-tablet'))
    @include($__module->getBladePath('purchase.step3.cart-total'))
    @include($__module->getBladePath('purchase.step3.cart-delivery'))
</div>
