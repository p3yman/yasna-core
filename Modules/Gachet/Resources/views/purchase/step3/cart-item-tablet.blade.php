@php
    $ware = $order->ware->spreadMeta()->inLocale(getLocale())->in('IRT');
    $unit = $ware->unit;
    $post = $order->post;

    $postImage = doc($post->featured_image)
        ->version('thumb')
        ->getUrl();
@endphp

<div class="product-card" data-item="{{ $order->hashid }}">
    {{--<div class="remove-row">--}}
        {{--<a href="#" class="icon-close" onclick="removeItem('{{ $order->hashid }}')"></a>--}}
    {{--</div>--}}
    <div class="product-header">
        <a href="{{ ShopTools::productWareUrl($post, $ware) }}" class="product-name" target="_blank">
            <img src="{{ $postImage }}" width="80">
            <h5>{{ $ware->full_title }}</h5>
        </a>
    </div>
    <div class="product-details">
        <div class="clearfix">
            <div class="col-half">
                <div class="product-price">
                    <span class="title">
                        {{ $__module->getTrans('cart.price.unit') }}
                    </span>
                    @php $unitPrice = round($order->single_original_price / 10); @endphp
                    {{ ad(number_format($unitPrice)) }}
                    {{ $__module->getTrans('general.unit.currency.IRT') }}
                </div>
            </div>
            <div class="col-half">
                <div class="product-count">
                    <span class="title">
                    {{ $__module->getTrans('cart.numeration-label.amount') }}
                </span>
                    {{ ad($order->count) }}
                    {{ $unit->title }}
                </div>
            </div>
        </div>
        <div class="col-full">
            <div class="price-total">
                @php $totalPrice = round($order->original_price / 10); @endphp
                @if ($order->sale_price == $order->original_price)
                    <div class="price-total--single">
                            <span class="title">
                                {{ $__module->getTrans('cart.price.total').": " }}
                            </span>
                        {{ ad(number_format($totalPrice)) }}
                        {{ $__module->getTrans('general.unit.currency.IRT') }}}}
                    </div>
                @else
                    <table>
                        <tr>
                            <td>{{ $__module->getTrans('cart.price.total') }}:</td>
                            <td>
                                {{ ad(number_format($totalPrice)) }}
                                {{ $__module->getTrans('general.unit.currency.IRT') }}
                            </td>
                        </tr>
                        <tr>
                            <td>{{ $__module->getTrans('cart.headline.discount') }}:</td>
                            <td>
                                {{ ad(number_format(round(($order->original_price - $order->sale_price) / 10))) }}
                                {{ $__module->getTrans('general.unit.currency.IRT') }}
                            </td>
                        </tr>
                        <tr>
                            <td class="total" colspan="2">
                                {{ ad(number_format(round($order->sale_price / 10))) }}
                            </td>
                        </tr>
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
