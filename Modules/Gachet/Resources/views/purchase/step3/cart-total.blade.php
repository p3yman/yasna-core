<div class="cart-total-container clearfix">
	<table class="table bordered cart-total">
		<tbody>
		<tr>
			<td>{{ $__module->getTrans('cart.price.your-purchase-total') }}:</td>
			<td>
				{{ ad(number_format(gachet()->shop()->priceToShow($cart->original_price))) }}
				{{ $__module->getTrans('general.unit.currency.IRT') }}
			</td>
		</tr>

		@php $discount = ($cart->original_price - $cart->sale_price) + $cart->added_discount @endphp
		@if ($discount)
			<tr>
				<td>{{ $__module->getTrans('cart.headline.discount') }}:</td>
				<td>
					{{ ad(number_format(round(gachet()->shop()->priceToShow($discount)))) }}
					{{ $__module->getTrans('general.unit.currency.IRT') }}
				</td>
			</tr>
		@endif

		@php $tax = $cart->tax_amount @endphp
		@if ($tax)
			<tr>
				<td>{{ $__module->getTrans('cart.headline.tax') }}:</td>
				<td>
					{{ ad(number_format(round(gachet()->shop()->priceToShow($tax)))) }}
					{{ $__module->getTrans('general.unit.currency.IRT') }}
				</td>
			</tr>
		@endif

		<tr>
			<td>{{ $__module->getTrans('cart.price.delivery') }}:</td>
			<td>
				{{ ad(number_format(round(($cart->delivery_price ?: 0) / 10 + ($cart->getMeta('heavy_delivery_price') /10 ?? 0)))) }}
				{{ $__module->getTrans('general.unit.currency.IRT') }}
			</td>
		</tr>

		<tr class="total">
			<td>{{ $__module->getTrans('cart.price.purchasable') }}:</td>
			<td>
				{{ ad(number_format(round(gachet()->shop()->priceToShow($cart->invoiced_amount)))) }}
				{{ $__module->getTrans('general.unit.currency.IRT') }}
			</td>
		</tr>

		</tbody>
	</table>
</div>
<div class="clearfix"></div>
