<div class="cart-total-container clearfix mt10">
	<div class="delivery">
		<h5>{{ $__module->getTrans('cart.headline.address-and-courier') }}</h5>
		<p>
			<span class="icon-location"></span>
			{{ $__module->getTrans('cart.message.order-address-description', [
				'receiver' => $address->receiver,
				'address' => $address->address,
				'telephone' => ad($address->telephone),
			]) }}
		</p>
		<hr>
		<p>
			@php
				$previewCurrency = CurrencyTools::getPreviewCurrency();
				$ratio = CurrencyTools::getPreviewCurrencyRatio();
				$currency = CurrencyTools::getCurrency();
			@endphp
			<span class="icon-truck"></span>
			{{ $__module->getTrans('cart.message.order-courier-description', [
				'courier' => $shipping_method->getTitleInCurrentLocale()
			]) }}
		</p>
	</div>
</div>
