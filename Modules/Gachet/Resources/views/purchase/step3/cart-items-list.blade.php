<table class="table bordered xs-hide">
    <thead>
    <tr>
        <th>{{ $__module->getTrans('post.product.singular') }}</th>
        <th>{{ $__module->getTrans('cart.price.unit') }}</th>
        <th>{{ $__module->getTrans('cart.numeration-label.amount') }}</th>
        <th>{{ $__module->getTrans('cart.price.total') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cart->orders as $order )
        @include($__module->getBladePath('purchase.step3.cart-item'))
    @endforeach
    </tbody>
</table>
<div class="clearfix"></div>
