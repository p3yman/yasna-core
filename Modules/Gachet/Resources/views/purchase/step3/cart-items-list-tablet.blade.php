<div class="product-list--tablet">
    @foreach($cart->orders as $order )
        @include($__module->getBladePath('purchase.step3.cart-item-tablet'))
    @endforeach
</div>
