@extends($__module->getBladePath('purchase.layout.plane'))

@php
    $currentStep = 3;
@endphp

@section('inner-body')
    <div class="">
        @include($__module->getBladePath('purchase.step3.cart'))
        @include($__module->getBladePath('purchase.step3.buttons'))
    </div>
@append

@include($__module->getBladePath('purchase.step3.scripts'))
