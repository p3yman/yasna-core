@include($__module->getBladePath('layouts.widgets.form.scripts'))
@section('html-footer')
    <script>
        $(document).ready(function () {

            $(document).on({
                click: function (event) {
                    let contentContainer = $('#page-content');
                    let feedContainer = $('.verify-feedback');
                    console.log(contentContainer)
                    console.log(feedContainer)

                    $.ajax({
                        url: "{{ route('gachet.purchase.step3.verify') }}",
                        dataType: 'JSON',
                        beforeSend: function () {
                            contentContainer.addClass('disabled-box');
                            feedContainer.slideUp();
                        },
                        success: function (rs) {
                            if (rs.success) {
                                window.location.replace("{{ route('gachet.purchase.step4') }}");
                            }
                        },
                        error: function (jqXhr) {
                            if (jqXhr.status == 422) {
                                let errors = jqXhr.responseJSON.errors;
                                let ul = $('<ul />');

                                $.each(errors, function (key, value) {
                                    let li = $('<li />').html(forms_digit_fa(value));
                                    ul.append(li);
                                });


                                feedContainer.addClass("alert-danger")
                                    .html(ul)
                                    .slideDown();

                                contentContainer.removeClass('disabled-box');
                            }
                        }
                    });
                }
            }, 'button#continue');
        });
    </script>
@append
