@extends($__module->getBladePath('purchase.layout.plane'))

@php $siteName = setting('site_title')->gain() ?: $__module->getTrans('general.our-site') @endphp
@section('inner-body')
    <div id="page-content" class="row">
        <div class="col-sm-6 col-xs-12 text-center">
            <div style="padding-top: 50px">
                <h1><i class="fa fa-lock"></i></h1>
                <p>{{ $__module->getTrans('user.message.have-an-account-on-site', ['site' => $siteName]) }}</p>
                <p>{{ $__module->getTrans('user.message.login-to-complete-purchasement') }}</p>
                <a href="{{ route('login', ['redirect' => 'referrer']) }}" class="button blue">
                    {{ $__module->getTrans('user.message.login-to-site', ['site' => $siteName]) }}
                </a>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12 text-center">
            <div style="padding-top: 50px">
                <h1><i class="fa fa-user"></i></h1>
                <p>{{ $__module->getTrans('user.message.are-you-new') }}</p>
                <p>{{ $__module->getTrans('user.message.login-to-complete-purchasement') }}</p>
                <a href="{{ route('register', ['redirect' => 'referrer']) }}" class="button green">
                    {{ $__module->getTrans('user.message.register-in-site', ['site' => $siteName]) }}
                </a>
                <p>
                    <small>{{ $__module->getTrans('user.message.register-in-site-benefits', ['site' => $siteName]) }}</small>
                </p>
            </div>
        </div>
    </div>
@append
