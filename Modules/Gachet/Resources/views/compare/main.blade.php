@extends($__module->getBladePath('layouts.plane'))

@php
	$title = $__module->getTrans('general.compare');
	Template::appendToPageTitle($title);
	Template::appendToNavBar([
		$title,request()->url()
	]);

	$previewCurrency = CurrencyTools::getPreviewCurrency();
	$previewCurrencyTrans = $__module->getTrans('general.unit.currency.' . $previewCurrency);

@endphp

@section('body')
	<div class="page-content mt50">
		<div class="container">
			<div class="part-title">
				<h3>
					<span>
						{{ $__module->getTrans('general.compare') }}
					</span>
					{{ $__module->getTrans('general.products') }}
				</h3>
			</div>

			<div class="products-compare">
				<div class="table-responsive">
					<table class="table bordered">

						<thead>
						<tr>
							<th class="empty"></th>
							@foreach($products as $product)
								@php
									$isOnSale = $product['original_price'] > $product['sale_price'];
								@endphp

								<th class="head">
									@isset($product['image'])
										<img src="{{ $product['image'] }}">
									@endisset
									<div class="title">{{ $product['title'] or "-" }}</div>

										<div class="prices">
											@if($isOnSale)
												@php
													$off = 100 - round(($product['sale_price'] / $product['original_price'] ), 2) * 100;
												@endphp
												<del>
													{{ ad(number_format($product['original_price'])) }}
												</del>

												<span class="label alt red fw-b">
													{{ pd($off) . $__module->getTrans('general.percent_sign') }}
												</span>

												<br>
												<ins>
													{{ ad(number_format($product['sale_price'])) }}
													{{ $previewCurrencyTrans }}
												</ins>

											@else
												<ins>
													{{ ad(number_format($product['original_price'])) }}
													{{ $previewCurrencyTrans }}
												</ins>
											@endif
										</div>


									@isset($product['price'])
										<div class="price">{{ ad(number_format($product['price']))}}</div>
									@endisset
									@isset($product['link'])
										<a href="{{ $product['link'] }}" class="button blue block">
											{{ $__module->getTrans('general.view_and_purchase') }}
										</a>
									@endisset
									<a href="{{ $product['remove_link'] }}" class="remove">
										{{ $__module->getTrans('general.remove_compare') }}
									</a>
								</th>
							@endforeach
						</tr>
						</thead>

						<tbody>

						@foreach($groups as $group)
							<tr class="row-title">
								<td colspan="100%">
									{{ $group['title'] }}
								</td>
							</tr>
							@foreach($group['rows'] as $group_row)
								<tr class="row-data">
									<td>
										{{ $group_row['title'] }}
									</td>
									@foreach($group_row['cols'] as $cell)
										<td>
											@if(is_bool($cell))
												<span class="{{ $cell? 'icon-check color-green': 'icon-close color-red' }} fz-lg"></span>
											@else
												{{ $cell }}
											@endif
										</td>
									@endforeach

								</tr>
							@endforeach
						@endforeach

						</tbody>

					</table>

				</div>


			</div>
		</div>
	</div>
@stop
