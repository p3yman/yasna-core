@extends($__module->getBladePath('layouts.plane'))

@php
	$title = $__module->getTrans('general.catalogs');
	gachet()->template()->appendToPageTitle($title);
	Template::appendToNavBar([
		$title, request()->fullUrl()
	]);

	$default_image = $__module->getAsset("images/image-not-found-200x200.png");

	$catalogs = $catalog_posts->map(function($post) {
		$image = fileManager()
			->file($post->featured_image)
			->posttype($post->posttype)
			->config('featured_image')
			->version('200x200')
			->getUrl() ?? $default_image;

		$link = fileManager()
			->file($post->catalog)
			->getDownloadUrl();

		return [
			"image" => $image ,
			"title" => $post->title ,
			"link" => $link,
		];
	})->toArray();


@endphp

@section('body')
	<div class="container">

		<div class="part-title">
			<h3>
				<span>
					{{ $__module->getTrans('general.catalogs_of') }}
				</span>
				{{ $__module->getTrans('general.products') }}
			</h3>
		</div>

		<div class="row">
			<div class="col-sm-10 col-center">
				@if (count($catalogs))
					@include($__module->getBladePath('catalog.content'))
				@else
					@include($__module->getBladePath('catalog.empty'))
				@endif
			</div>
		</div>
	</div>
@stop
