<section class="panel">
	<article>
		<form action="">
			<div class="field m0">
				<label>
					{{ $__module->getTrans('general.search_catalogs') }}
				</label>
				<input type="text" name="search" value="{{ request()->search }}">
				<button class="button mt10 action blue">
					{{ $__module->getTrans('general.search') }}
				</button>
			</div>
		</form>

	</article>

</section>

<section class="mt50">
	<div class="row">
		@foreach($catalogs as $catalog)
			<div class="col-sm-3">
				@include($__module->getBladePath('catalog.catalog-item'),[
					"image" => $catalog['image'] ,
					"title" => $catalog['title'] ,
					"link" => $catalog['link'] ,
				])
			</div>
		@endforeach
	</div>
</section>

<section class="mt10 text-center">
	{!! $catalog_posts->render() !!}
</section>
