@extends($__module->getBladePath('layouts.plane'))

@php
    gachet()->template()->appendToPageTitle($post->title);
    gachet()->template()->appendToNavBar([
        $post->title, request()->url()
    ]);
@endphp

@section('body')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-center">
                    @php $post->spreadMeta() @endphp
                    {!! $post->text !!}
                </div>
            </div>
        </div>
    </div>
@append
