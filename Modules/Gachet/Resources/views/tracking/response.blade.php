{{ $__module->getTrans('cart.payment.status') }}:
{{ $cart->payment_status_text }}

<br>

{{ $__module->getTrans('cart.headline.order-status') }}:
{{ $cart->status_text }}
