@extends($__module->getBladePath('layouts.plane'))

@php
	$title = ($static_post->title ?: $__module->getTrans('cart.tracking.title'));
    Template::appendToPageTitle($title);
    Template::appendToNavBar([
        $title, request()->url()
    ]);
@endphp

@section('body')

	<div class="page-content">
		<div class="container">
			<div class="col-center col-sm-8">
				@include($__module->getBladePath('tracking.index-text'))
				@include($__module->getBladePath('tracking.index-form'))
			</div>
		</div>
	</div>

@append
