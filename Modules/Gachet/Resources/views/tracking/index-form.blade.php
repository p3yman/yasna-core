{!!
	widget('form-open')
		->class('js')
!!}

<div class="col-md-6 col-center">
	<div class="row">
		<div class="col-sm-12">
			<div class="field">
				<label>{{ $__module->getTrans('cart.tracking.code') }}</label>
				<input type="text" name="code">
			</div>
		</div>

		<div class="col-sm-12 tal">
			<button class="green">{{ $__module->getTrans('form.button.send') }}</button>
		</div>

		<div class="col-sm-12 mt10">
			<div class="result">
				@include($__module->getBladePath('layouts.widgets.form.feed'))
			</div>
		</div>
	</div>
</div>


{!! widget('form-close') !!}


@section('html-footer')
	@include($__module->getBladePath('layouts.widgets.form.scripts'))
@append
