<div id="logo-part">
    <div class="container">
        @include($__module->getBladePath('layouts.header-logo'))
        @include($__module->getBladePath('layouts.header-left-buttons'))
        @include($__module->getBladePath('layouts.header-main-menu'))
    </div>
</div>
