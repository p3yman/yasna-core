@php
    $footerLogoHashid = setting('footer-logo')->gain();
    $footerLogo = $footerLogoHashid ? doc($footerLogoHashid)->getUrl() : null;
@endphp
@if($footerLogo)
    <a href="{{ CurrentModule::action('FrontController@index') }}" id="logo-footer">
        <img src="{{ $footerLogo }}">
    </a>
@endif
