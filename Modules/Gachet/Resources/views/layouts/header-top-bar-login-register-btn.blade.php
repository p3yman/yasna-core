@if(!user()->exists)
    <a href="{{ route('login') }}" class="auth-link">
        {{ $__module->getTrans('general.login-or-register') }}
    </a>
@endif
