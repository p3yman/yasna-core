<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Html::style($__module->getAsset('libs/jquery-ui/jquery-ui.min.css')) !!}
    {!! Html::style($__module->getAsset('css/front-style-new.min.css')) !!}
    {!! Html::style($__module->getAsset('css/more.min.css')) !!}
    {!! Html::style($__module->getAsset('libs/font-awesome/css/font-awesome.min.css')) !!}
    <title>{{ Template::implodePageTitle(' | ') }}</title>
    @yield('html-header')
    @include($__module->getBladePath('layouts.raychat'))
</head>
