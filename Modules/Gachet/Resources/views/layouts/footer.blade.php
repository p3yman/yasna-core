<footer id="main-footer">
    <div class="container">
        @include($__module->getBladePath('layouts.footer-logo'))
        @include($__module->getBladePath('layouts.footer-menu'))
        @include($__module->getBladePath('layouts.footer-enamad'))
        @include($__module->getBladePath('layouts.footer-copy-right'))
    </div>
</footer>
