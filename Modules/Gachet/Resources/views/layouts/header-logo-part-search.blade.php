<div class="cart search-form">
    <form action="{{ route('gachet.products.search') }}">
        <input type="text" id="site-search" class="f-r" name="search" placeholder="{{ $__module->getTrans('general.search_products') }}">
        <button type="submit" class="icon-search f-l" id="submit-search"></button>
    </form>
</div>