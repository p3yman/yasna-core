<div class="dark-navbar">
	@include($__module->getBladePath('layouts.header-top-bar'))
	<div class="menu-fixed">
		@include($__module->getBladePath('layouts.header-logo-part'))
		@if(isset($isIndex) and $isIndex)
		@include($__module->getBladePath('layouts.dark-navbar.logo-shadow'))
		@endif
	</div>
</div>
