@if (!isset($hideNavbar) or $hideNavbar)
    <div class="cover small">
        <div class="container">
            <ul class="breadcrumbs">
                @foreach(Template::navBar() as $item)
                    <li>
                        @if(isset($item[1]) and strlen(trim($item[1])))
                            <a href="{{ $item[1] }}">
                                {{ $item[0] }}
                            </a>
                        @else
                            <span>
                            {{ $item[0] }}
                        </span>
                        @endif
                    </li>
                @endforeach
            </ul>
            @php $redTitle = Template::redTitle() @endphp
            @if($redTitle)
                <div class="title"> {{ $redTitle or '' }} </div>
            @endif
        </div>
    </div>
@endif
