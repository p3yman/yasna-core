@php
    $logoHashid = setting('site_logo')->in(getLocale())->gain();
    $logoUrl = $logoHashid ? doc($logoHashid)->getUrl() : null;
@endphp

@if($logoUrl)
    <a href="{{ url('') }}" id="logo" class="index-logo">
        <img src="{{ $logoUrl }}">
    </a>
@endif
