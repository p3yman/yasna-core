<div id="topbar">
    <div class="container">
        @include($__module->getBladePath('layouts.header-top-bar-tel-contact'))
        <div class="f-l">
            @include($__module->getBladePath('layouts.header-top-bar-select-lang'))
            @include($__module->getBladePath('layouts.header-top-bar-login-register-btn'))
            @include($__module->getBladePath('layouts.header-top-bar-user-menu'))
            {{--@include($__module->getBladePath('layouts.header-top-bar-socials'))--}}
        </div>
    </div>
</div>
