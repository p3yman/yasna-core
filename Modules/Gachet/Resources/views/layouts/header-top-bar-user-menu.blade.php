@if(user()->exists)
	<div class="dropdown bottom-left user">
		<a href="#" class="dropdown-toggle">
        {{--<span>--}}
            {{--<img src="{{ $__module->getAsset("images/user-white.svg") }}">--}}
        {{--</span>--}}
		<span class="text">
			{{ user()->full_name }}
		</span>
		</a>
		<div class="menu">
			<a href="{{ route('gachet.user.orders') }}">
				{{ $__module->getTrans('user.dashboard') }}
			</a>
			<a href="{{ route('gachet.user.profile') }}">
				{{ $__module->getTrans('user.edit-profile') }}
			</a>
			{{--<a href="{{ CurrentModule::action('UserController@drawing') }}">--}}
				{{--{{ $__module->getTrans('general.drawing.code.registered-plural') }}--}}
			{{--</a>--}}
			{{--<a href="{{ CurrentModule::action('UserController@events') }}">--}}
				{{--{{ $__module->getTrans('post.event.plural') }}--}}
			{{--</a>--}}
			<a href="{{ route('logout') }}">
				{{ $__module->getTrans('user.logout') }}
			</a>
		</div>
	</div>
@endif
