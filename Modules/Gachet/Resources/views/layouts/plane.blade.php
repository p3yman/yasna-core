<!DOCTYPE html>
<html lang="{{ getLocale() }}">
@include($__module->getBladePath('layouts.head'))
@include($__module->getBladePath('layouts.body'))
</html>
