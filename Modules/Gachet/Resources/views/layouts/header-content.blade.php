<header id="main-header">
    @include($__module->getBladePath('layouts.header-logo-part'))
	<div id="menu-container">
		<div class="container">
			@include($__module->getBladePath('layouts.header-main-menu'))
		</div>
	</div>

</header>
@include($__module->getBladePath('layouts.navbar'))
