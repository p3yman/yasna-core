<div id="logo-part">
	<div class="container">
        <div class="header-flex">
            <div class="right">
                @include($__module->getBladePath('layouts.header-logo'))
            </div>
            <div class="center xs-hide">
                @include($__module->getBladePath('layouts.header-logo-part-search'))
            </div>
            <div class="left">
                @include($__module->getBladePath('layouts.header-left-buttons'))
            </div>
        </div>
        <div class="xs-show res-search">
            @include($__module->getBladePath('layouts.header-logo-part-search'))
        </div>
	</div>
</div>
