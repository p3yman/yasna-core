{{ $__module->getTrans('user.password.token-expired') }}
<br/>
<a href="{{ route('password.request') }}">{{ $__module->getTrans('user.password.get-new-token') }}</a>
