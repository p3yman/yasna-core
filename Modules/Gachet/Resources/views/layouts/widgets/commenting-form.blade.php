@if($post->canReceiveComments())
    {!! Form::open([
        'url' => CurrentModule::action('CommentController@save'),
        'method'=> 'post',
        'class' => 'js',
        'name' => 'commentForm',
        'id' => 'commentForm',
        'style' => 'padding: 15px;',
    ]) !!}
    <div class="row">
        @include($__module->getBladePath('layouts.widgets.form.hidden'),[
            'name' => 'post_id',
            'value' => $post->hashid,
        ])

        @include($__module->getBladePath('layouts.widgets.form.textarea'), [
            'name' => 'text',
            'label' => false,
            'rows' => 4,
            'placeholder' => trans('validation.attributes_placeholder.your_comment'),
        ])

        <div class="col-xs-12">
            <div class="form-group pt15">
                <div class="action tal">
                    <button class="blue">{{ $__module->getTrans('general.comment.submit') }}</button>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pt15">
            @include($__module->getBladePath('layouts.widgets.form.feed'))
        </div>
    </div>
    {!! Form::close() !!}
    @include($__module->getBladePath('layouts.widgets.form.scripts'))
@endif
