@extends($__module->getBladePath('layouts.widgets.dialog.main'))

@php $dialogImage = true @endphp

@section('dialog-image-link')
    {{ $__module->getAsset("images/loading/bar-red.gif") }}
@endsection

{{--
@section('dialog-text')
    {{ $__module->getTrans('form.feed.wait') }}
@endsection--}}
