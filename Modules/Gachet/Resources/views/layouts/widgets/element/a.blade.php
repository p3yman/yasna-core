@extends($__module->getBladePath('layouts.widgets.element._element'))

@php
    $tagName = 'a';
    if(isset($href)) {
        if(isset($otherAttributes) and is_array($otherAttributes)) {
            $otherAttributes['href'] = $href;
        } else {
            $otherAttributes = ['href' => $href];
        }
    }
@endphp

