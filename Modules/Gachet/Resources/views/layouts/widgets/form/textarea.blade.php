@php
	if (!isset($extra))
		{$extra = '';}
	if (!isset($in_form))
		{$in_form = true;}

	if (isset($class) && str_contains($class, 'form-required')) {
		$required = true;
	}

	if (isset($value) and is_object($value))
		{$value = $value->$name;}

	if (isset($disabled) and $disabled) {
		$required = false;
		$extra .= ' disabled ';
	}

	$error_value = FormTools::findErrorValue($name, ($error_value ?? null));
	$attribute_example = FormTools::findAttributeExample($name, ($attribute_example ?? null));
	$placeholder = FormTools::findAttributeExample($name, ($placeholder ?? null));
@endphp

@if(!isset($condition) or $condition)

	@if($in_form)
		<div class="field">
			@if(!isset($label))
				@php
					$label = Lang::has("validation.attributes.$name") ? trans("validation.attributes.$name") : $name;
				@endphp
			@endif
			@if($label)
				<label
						for="{{$name}}"
						class="control-label {{$label_class or ''}}"
				>
					{{ $label }}
					@if(isset($required) and $required)
						{{--<span class="fa fa-star required-sign " title="{{trans('forms.logic.required')}}"></span>--}}
					@endif
				</label>

			@endif
			@endif
			@if(isset($top_label))
				<label for="{{$name}}" class="control-label mv10 text-gray">{{ $top_label }}...</label>
			@endif
			<textarea
					id="{{$id or ''}}"
					name="{{$name}}" value="{{$value or ''}}"
					class="form-control {{$class or ''}}"
					placeholder="{{ $placeholder }}"
					title="{{ $attribute_example }}"
					rows="{{$rows or 5}}"
					error-value="{{ $error_value }}"
					{!! $extra or '' !!}
			>{{$value or ''}}</textarea>
			@if($in_form)
				<span class="help-block">
                {{ $hint or '' }}
            </span>
		</div>
	@endif
@endif