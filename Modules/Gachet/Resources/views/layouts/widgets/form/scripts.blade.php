@section('html-footer')
    <!-- Load Form Scripts -->
    {!! Html::script($__module->getAsset('js/jquery.form.min.js')) !!}
    {!! Html::script($__module->getAsset('js/calendar.min.js')) !!}
    {!! Html::script($__module->getAsset('js/jquery.ui.datepicker-cc.min.js')) !!}
    {!! Html::script($__module->getAsset('js/jquery.ui.datepicker-cc-fa.min.js')) !!}
    {!! Html::script($__module->getAsset('js/forms.min.js')) !!}
@append
