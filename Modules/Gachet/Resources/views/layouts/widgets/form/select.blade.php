<div class="field">
    <label> {{ trans('validation.attributes.' . $name) }} </label>
    @include($__module->getBladePath('layouts.widgets.form.select-self'))
</div>
