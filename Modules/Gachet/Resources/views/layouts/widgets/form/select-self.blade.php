@php
	$error_value = FormTools::findErrorValue($name, ($error_value ?? null));
	$attribute_example = FormTools::findAttributeExample($name, ($attribute_example ?? null));
@endphp

<select name="{{ $name or '' }}" id="{{ $name or '' }}"
		class="{{ $class or '' }}"
		title="{{ $attribute_example }}"
		error-value="{{ $error_value }}"
		{{ $other or '' }}
>
	@include($__module->getBladePath('layouts.widgets.form.select-self-options'))
</select>
