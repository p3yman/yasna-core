@php
	$is_monochrome = isset($monochrome)? $monochrome : false;
	$folder = '';

	if($is_monochrome){
		$folder = '/monochrome';
	}
@endphp

<div class="tiles">
	<div class="tile-box">
		<a href="{{ url('/').'/page/good-shopping-experience' }}" class="tile">
			<img src="{{ asset('modules/gachet/images/tiles'. $folder .'/good.svg') }}">
			<span class="title">{{ $__module->getTrans('general.tiles-info.good-shopping') }}</span>
		</a>
	</div>
	<div class="tile-box">
		<a href="{{ url('/').'/page/ten-days-guarantee' }}" class="tile">
			<img src="{{ asset('modules/gachet/images/tiles'. $folder .'/guarantee.svg') }}">
			<span class="title">{{ $__module->getTrans('general.tiles-info.guarantee') }}</span>
		</a>
	</div>
	<div class="tile-box">
		<a href="{{ url('/').'/page/best-prices' }}" class="tile">
			<img src="{{ asset('modules/gachet/images/tiles'. $folder .'/tag.svg') }}">
			<span class="title">{{ $__module->getTrans('general.tiles-info.best-price') }}</span>
		</a>
	</div>
	<div class="tile-box">
		<a href="{{ url('/').'/page/fast-delivery' }}" class="tile">
			<img src="{{ asset('modules/gachet/images/tiles'. $folder .'/delivery-truck.svg') }}">
			<span class="title">{{ $__module->getTrans('general.tiles-info.delivery') }}</span>
		</a>
	</div>
	<div class="tile-box">
		<a href="{{ url('/').'/page/pay-on-destination' }}" class="tile">
			<img src="{{ asset('modules/gachet/images/tiles'. $folder .'/payment-method.svg') }}">
			<span class="title">{{ $__module->getTrans('general.tiles-info.pay') }}</span>
		</a>
	</div>
	<div class="tile-box">
		<a href="{{ url('/').'/page/best-support' }}" class="tile">
			<img src="{{ asset('modules/gachet/images/tiles'. $folder .'/call.svg') }}">
			<span class="title">{{ $__module->getTrans('general.tiles-info.service') }}</span>
		</a>
	</div>
</div>
