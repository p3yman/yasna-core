<div class="best-selling mb50">
	<div class="container">
		<div class="part-title has-nav">
			<h3>
				<span>
					{{ $title[0] }}
				</span>
				{{ $title[1] }}

				<span class="nav">
					<span class="prev icon-angle-right"></span>
					<span class="next icon-angle-left"></span>
				</span>
			</h3>
		</div>
		<div class="items-slider owl-carousel">
			{!! $slot !!}
		</div>
	</div>
</div>
