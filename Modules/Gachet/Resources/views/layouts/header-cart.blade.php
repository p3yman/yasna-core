@php
	$cart = gachet()->template()->cart();
	$cart_orders = $cart->orders;
	$orders_count = $cart_orders->count();
@endphp
<div class="cart cart-button cart-counter @if($orders_count) full @endif">
	<a href="{{ route('gachet.cart.index') }}" data-notify="{{ ad($orders_count) }}">
        <i class="icon-cart"></i>
        <span>سبد خرید</span>
    </a>
</div>
