<ul class="menu-footer">
	@foreach(gachet()->template()->footerMenu() as $menu_item)
		<li>
			<a href="{{ url($menu_item->linkIn(getLocale())) }}">
				{{ $menu_item->titleIn(getLocale()) }}
			</a>
		</li>
	@endforeach
</ul>
