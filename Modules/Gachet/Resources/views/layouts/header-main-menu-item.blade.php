@php
	$children = $menu->children()->get();
@endphp
<li @if( $children->count() ) class="has-child" @endif>
	<a href="{{ route('gachet.products.list', ['label' => $menu->slug]) }}#!category_{{ $menu->slug }}?checkbox!">
		{{ $menu->titleIn(getLocale()) }}
	</a>
	@if( $children->count() )
		<ul class="sub-menu">
			@foreach($children as $submenu)
				@include($__module->getBladePath('layouts.header-main-menu-item'), ['menu' => $submenu])
			@endforeach
		</ul>
	@endif
</li>
