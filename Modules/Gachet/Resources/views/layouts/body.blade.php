<body>
@include($__module->getBladePath('layouts.header-content'))
@yield('body')
@include($__module->getBladePath('layouts.footer'))
@include($__module->getBladePath('layouts.widgets.floating-btn'))
@include($__module->getBladePath('layouts.scripts'))
@yield('html-footer')
</body>
