<div class="cart xs-show">
    <a href="#" class="icon-menu" id="open-menu"></a>
</div>

<a href="#" class="icon-cancel xs-show mt10" id="close-menu"></a>

@include($__module->getBladePath('layouts.header-cart'))

<div class="cart cart-button">
    <a href="{{ url('/tracking') }}">
        {{ $__module->getTrans('general.tiles-links.track-order') }}
    </a>
</div>

@if(!user()->exists)
    <div class="cart cart-button">
        <a href="{{ route('login') }}" class="auth-button">
            {{ $__module->getTrans('general.login-or-register') }}
        </a>
    </div>
@else
    <div class="cart cart-button dropdown bottom-left user">
        <a href="#" class="dropdown-toggle">
            {{ user()->full_name }}
        </a>
        <div class="menu">
            <a href="{{ route('gachet.user.orders') }}">
                {{ $__module->getTrans('user.dashboard') }}
            </a>
            <a href="{{ route('gachet.user.profile') }}">
                {{ $__module->getTrans('user.edit-profile') }}
            </a>
            <a href="{{ route('logout') }}">
                {{ $__module->getTrans('user.logout') }}
            </a>
        </div>
    </div>
@endif