<div class="copyright">
    {{ $__module->getTrans('general.footer.copyright', ['site' => setting()->ask('site_title')->gain()]) }}
    <br/>

    @php
        $yasna_url = get_setting('yasna_group_url');
        $yasna_title = get_setting('yasna_group_title');
    @endphp

    @if ($yasna_url and $yasna_title)
    <a class="text-gray" href="{{ $yasna_url }}" target="_blank">
        {{
            $__module->getTrans('general.powered-by', [
                'title' => $yasna_title
            ])
        }}
    @endif
    </a>
</div>
