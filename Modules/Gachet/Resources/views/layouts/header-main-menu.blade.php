<ul class="menu">
    @php
        $categories = app('gachet.shop.products')->rootCategoriesLabels();
    @endphp
    @isset($categories)
        @foreach ($categories as $menu)
            @include($__module->getBladePath('layouts.header-main-menu-item'))
        @endforeach
    @endisset
</ul>
