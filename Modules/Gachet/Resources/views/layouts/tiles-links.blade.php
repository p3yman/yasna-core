<div class="container">
    <div class="tiles">
        <div class="tile-box">
            <a href="#" class="tile">
                <img src="{{ asset('Modules/Gachet/images/tiles/shop.svg') }}">
                <span class="title">{{ $__module->getTrans('general.tiles-links.track-order') }}</span>
            </a>
        </div>
        <div class="tile-box">
            <a href="#" class="tile">
                <img src="{{ asset('Modules/Gachet/images/tiles/24-hours.svg') }}">
                <span class="title">{{ $__module->getTrans('general.tiles-links.contact') }}</span>
            </a>
        </div>
        <div class="tile-box">
            <a href="#" class="tile">
                <img src="{{ asset('Modules/Gachet/images/tiles/invoice.svg') }}">
                <span class="title">{{ $__module->getTrans('general.tiles-links.about') }}</span>
            </a>
        </div>
        <div class="tile-box">
            <a href="#" class="tile">
                <img src="{{ asset('Modules/Gachet/images/tiles/discount.svg') }}">
                <span class="title">{{ $__module->getTrans('general.tiles-links.rate') }}</span>
            </a>
        </div>
    </div>
</div>