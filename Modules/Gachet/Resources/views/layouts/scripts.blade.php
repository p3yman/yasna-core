<script>
    window.urls = {
        reloadCart: "{{ route('gachet.cart.reload-view') }}"
    };
</script>
<!-- Load Scripts -->

{!! Html::script($__module->getAsset('libs/jquery/jquery.min.js')) !!}
{!! Html::script($__module->getAsset('libs/jquery-ui/jquery-ui.min.js')) !!}
{!! Html::script($__module->getAsset('js/responsiveslides.min.js')) !!}
{!! Html::script($__module->getAsset('libs/owl-carousel/js/owl.carousel.min.js')) !!}
{!! Html::script($__module->getAsset('js/lightbox.min.js')) !!}
{!! Html::script($__module->getAsset('js/slick.min.js')) !!}
{!! Html::script($__module->getAsset('js/persian.min.js')) !!}
{{--{!! Html::script($__module->getAsset('js/bootstrap-select.min.js')) !!}--}}
{!! Html::script($__module->getAsset('js/nanoscroller.min.js')) !!}
{!! Html::script($__module->getAsset('js/notify.min.js')) !!}
{!! Html::script($__module->getAsset('js/app.min.js')) !!}
{!! Html::script($__module->getAsset('js/front.min.js')) !!}
