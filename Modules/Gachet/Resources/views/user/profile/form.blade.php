<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-center">
            <section class="panel">
                <header>
                    <div class="title">
                        <span class="icon-pencil"></span> {{ $__module->getTrans('user.edit-profile') }}
                    </div>
                </header>
                {!! Form::open([
                    'url' => route('gachet.user.profile.save'),
                    'method'=> 'post',
                    'class' => 'js',
                    'name' => 'editForm',
                    'id' => 'editForm',
                    'style' => 'padding: 15px;',
                ]) !!}
                <article>

                    <div class="row">
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'name_first',
                                'type' => 'text',
                                'value' => user()->name_first,
                                'class' => 'form-required form-persian',
                                'min' => 2,
                            ])
                        </div>
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'name_last',
                                'type' => 'text',
                                'value' => user()->name_last,
                                'class' => 'form-required form-persian',
                                'min' => 2,
                            ])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'code_melli',
                                'type' => 'text',
                                'value' => user()->code_melli,
                                'class' => 'form-required form-national',
                            ])
                        </div>
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'name_father',
                                'type' => 'text',
                                'value' => user()->name_father,
                                'class' => 'form-persian',
                                'min' => 2,
                            ])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('layouts.widgets.form.datepicker'), [
                                'name' => 'birth_date',
                                'type' => 'text',
                                'value' => (echoDate(user()->birth_date, 'Y/m/d') == '-') ? '' : echoDate(user()->birth_date, 'Y/m/d'),
                                'class' => 'form-required',
                                'options' => [
                                    'maxDate' => 0,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '-100:+0',
                                ]
                            ])
                        </div>
                        {{--<div class="col-sm-6">--}}
                            {{--@php--}}
                                {{--$configEduLevels = CurrentModule::config('edu-levels-values');--}}
                                {{--$eduOptions = [];--}}
                                {{--foreach ($configEduLevels as $configEduLevelKey => $configEduLevelVal) {--}}
                                    {{--$eduOptions[$configEduLevelVal] =--}}
                                        {{--$__module->getTrans('general.edu-level.full.' . $configEduLevelKey);--}}
                                {{--}--}}
                            {{--@endphp--}}
                            {{--@include(CurrentModule::bladePath('layouts.widgets.form.select'), [--}}
                                {{--'name' => 'edu_level',--}}
                                {{--'value' => user()->edu_level,--}}
                                {{--'options' => $eduOptions,--}}
                            {{--])--}}
                        {{--</div>--}}
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'mobile',
                                'type' => 'text',
                                'value' => user()->mobile,
                                'class' => 'form-mobile',
                            ])
                        </div>
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'tel',
                                'type' => 'text',
                                'value' => user()->tel,
                                'class' => 'form-phone',
                            ])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'email',
                                'type' => 'text',
                                'value' => user()->email,
                                'class' => 'form-email',
                                'other' => 'disabled=disabled',
                            ])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                @php
                                    $configGenders = CurrentModule::config('genders-values');
                                    array_pop($configGenders);
                                    $genderOptions = [];
                                    foreach ($configGenders as $configGendersKey => $configGendersVal) {
                                        $genderOptions[$configGendersVal] =
                                            $__module->getTrans('general.gender.' . $configGendersKey);
                                    }
                                @endphp
                                @include(CurrentModule::bladePath('layouts.widgets.form.radio-group'), [
                                    'name' => 'gender',
                                    'value' => user()->gender,
                                    'options' => $genderOptions,
                                    'class' => 'form-required',
                                ])
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                @php
                                    $configMaritalStatuses = CurrentModule::config('marital-statuses-values');
                                    $maritalStatusesOptions = [];
                                    foreach ($configMaritalStatuses as $configMaritalStatusesKey => $configMaritalStatusesVal) {
                                        $maritalStatusesOptions[$configMaritalStatusesVal] =
                                            $__module->getTrans('general.marital-status.' . $configMaritalStatusesKey);
                                    }
                                @endphp
                                @include(CurrentModule::bladePath('layouts.widgets.form.radio-group'), [
                                    'name' => 'marital',
                                    'value' => user()->marital,
                                    'options' => $maritalStatusesOptions,
                                    'class' => 'form-required',
                                ])
                            </div>
                            @include(CurrentModule::bladePath('layouts.widgets.form.datepicker'), [
                                'name' => 'marriage_date',
                                'type' => 'text',
                                'value' => (echoDate(user()->marriage_date, 'Y/m/d') == '-') ? '' : echoDate(user()->marriage_date, 'Y/m/d'),
                                'options' => [
                                    'maxDate' => 0,
                                    'changeMonth' => true,
                                    'changeYear' => true,
                                    'yearRange' => '-80:+0',
                                ],
                                'container' => [
                                    'style' =>  'display: none;'
                                ]
                            ])
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'new_password',
                                'type' => 'password',
                            ])
                        </div>
                        <div class="col-sm-6">
                            @include(CurrentModule::bladePath('user.profile.field'), [
                                'name' => 'new_password2',
                                'type' => 'password',
                            ])
                        </div>
                    </div>

                </article>
                @include(CurrentModule::bladePath('layouts.widgets.form.feed'))
                <footer class="tal">
                    <button type="submit" class="button green">{{ $__module->getTrans('form.button.save') }}</button>
                </footer>

                {!! Form::close() !!}
            </section>
        </div>
    </div>
</div>

@include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
