<div class="field">
	@if(!isset($label))
		@php $label = trans('validation.attributes.' . $name) @endphp
	@endif
	@if($label)
		<label>
			{{ $label }}
		</label>
	@endif

	@php
		$error_value = FormTools::findErrorValue($name, ($error_value ?? null));
		$attribute_example = FormTools::findAttributeExample($name, ($attribute_example ?? null));
		$placeholder = FormTools::findAttributeExample($name, ($placeholder ?? null));
	@endphp

	<input type="{{ $type or '' }}" name="{{ $name or '' }}" id="{{ $name or '' }}" class="{{ $class or '' }}"
		   placeholder="{{ $placeholder }}"
		   title="{{ $attribute_example }}"
		   minlength="{{ $min or '' }}"
		   maxlength="{{ $max or '' }}"
		   value="{{ $value or '' }}"
		   @if ($error_value) error-value="{{ $error_value }}" @endif
			{{ $other or '' }}>
</div>