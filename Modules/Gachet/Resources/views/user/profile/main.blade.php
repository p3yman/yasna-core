@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'profile';
    Template::appendToPageTitle($__module->getTrans('user.edit-profile'));
    Template::appendToNavBar([
        $__module->getTrans('user.edit-profile'), CurrentModule::action('UserController@profile')
    ]);
@endphp

@section('dashboard-body')
    @include(CurrentModule::bladePath('user.profile.form'))
@append
