@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'dashboard';
    Template::appendToPageTitle($__module->getTrans('user.dashboard'));
    Template::appendToNavBar([
        $__module->getTrans('user.dashboard'), CurrentModule::action('UserController@index')
    ]);
@endphp

@section('dashboard-body')
    @if(setting('dashboard_comment')->gain())
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-8 col-center">--}}
                    {{----}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-8 col-center">
					{!! $commentingPostPreview !!}
				</div>
			</div>
		</div>
    @endif
@append
