<header class="profile-header">
    <div class="container">
        <div class="row mb40">
            @php user()->spreadMeta()  @endphp
            <div class="col-xs-12">
                <div class="avatar tac">
                    <img class="round-image" src="{{ user()->gravatar(CurrentModule::asset("images/user.svg")) }}" width="">
                </div>
                <h2 class="name"> {{ user()->full_name }} </h2>
                @php
                    $colors = ['blue','red', 'green'];
                    //$trendingEvents = user()->drawingRecentScores(3, 7)
                @endphp
                <div class="row">
					<span class="label alt green md">
						@php
							$currency = gachet()->shop()->usableCurrency();
						@endphp
						{{ $__module->getTrans("user.type.$currency") }}
                    </span>

                    {{--@foreach($trendingEvents as $key => $event)--}}
                    {{--{{ null, $event->spreadMeta() }}--}}
                    {{--@if(\Carbon\Carbon::parse($event->ends_at)->lt(\Carbon\Carbon::now()))--}}
                    {{--{{ null, $class = 'gray' }}--}}
                    {{--{{ null, $icon = 'calendar-times-o' }}--}}
                    {{--@else--}}
                    {{--{{ null, $class = $colors[$key%count($colors)] }}--}}
                    {{--{{ null, $icon = 'calendar-check-o' }}--}}
                    {{--@endif--}}
                    {{--<div style="display: table-cell;">--}}
                    {{--<span class="label alt {{ $class }} md">--}}
                    {{--{{ $event->title }} :--}}
                    {{--{{ trans('front.all_user_score') }}--}}
                    {{--<strong>--}}
                    {{--{{ pd(floor($event->sum_amount / $event->rate_point)) }}--}}
                    {{--امتیاز--}}
                    {{--</strong>--}}
                    {{-- @TODO: rate_point should be set dynamically. Currently, was hardcode! --}}
                    {{--</span>--}}
                    {{--</div>--}}
                    {{--<div class="score-box {{ $class }}" style="width: {{ 100 / count($trendingEvents) }}%">--}}
                    {{--<div class="score-box-inner">--}}
                    {{--<div class="score-box-icon">--}}
                    {{--<i class="fa fa-{{ $icon }}"></i>--}}
                    {{--</div>--}}
                    {{--<div class="score-box-title">--}}
                    {{--{{ str_limit($event->title) }}--}}
                    {{--</div>--}}
                    {{--<div class="score-box-score">--}}
                    {{--{{ pd(floor($event->sum_amount / $event->rate_point)) }} امتیاز--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                </div>
            </div>
            @if(!array_default(model('user')::getRequiredFields(), user()->toArray()))
                <div class="col-xs-12 pt20">
                    <div class="row">
                        <div class="alert alert-danger text-right">
                            {{ $__module->getTrans('user.not-enough-information') }}
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="{{ route('gachet.user.profile') }}">
                                {{ $__module->getTrans('user.edit-profile') }}
                            </a>
                        </div>
                    </div>
                </div>
            @endif
            <div class="profile-tabs col-xs-12">
                {{--<div class="tab @if($activeTab == 'dashboard') active @endif">--}}
                    {{--<a href="{{ route('gachet.user.dashboard') }}">--}}
                        {{--<span class="icon-dashboard"></span> {{ $__module->getTrans('user.dashboard') }} </a>--}}
                {{--</div>--}}
                <div class="tab @if($activeTab == 'orders') active @endif">
                    <a href="{{ route('gachet.user.orders') }}">
                        <span class="icon-cart"></span> {{ $__module->getTrans('general.order.plural') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'profile') active @endif">
                    <a href="{{ route('gachet.user.profile') }}">
                        <span class="icon-pencil"></span> {{ $__module->getTrans('user.edit-profile') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'tickets') active @endif">
                    <a href="{{ route('gachet.users.tickets') }}">
                        <span class="icon-comment"></span> {{ $__module->getTrans('general.ticket.title.plural') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'addresses') active @endif">
                    <a href="{{ route('gachet.user.addresses') }}">
                        <span class="fa fa-address-card-o"></span>
                        {{ $__module->getTrans('general.address.title.plural') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'drawings') active @endif">
                    <a href="{{ route('gachet.user.drawing') }}">
                        <span class="icon-pencil"></span> {{ $__module->getTrans('general.drawing.code.registered-plural') }}
                    </a>
                </div>
                {{--<div class="tab @if($activeTab == 'events') active @endif">--}}
                    {{--<a href="{{ CurrentModule::action('UserController@events') }}">--}}
                        {{--<span class="icon-calendar"></span> {{ $__module->getTrans('post.event.plural') }}--}}
                    {{--</a>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</header>
