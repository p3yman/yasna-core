@extends($__module->getBladePath('user.frame.main'))

@php
	$activeTab = 'tickets';
	Template::appendToPageTitle($__module->getTrans('general.ticket.title.plural'));
	Template::appendToNavBar([
		$__module->getTrans('general.ticket.title.plural'), route('gachet.users.tickets')
	]);

	$tickets = $tickets
		->map(function ($ticket) {
			return [
				"id" => $ticket->hashid ,
				"title" => $ticket->title ,
				"organization" => $ticket->ticket_type->title ,
				"flag" => [
					"color" => ($ticket->flag == 'done') ? 'red' : 'green' ,
					"title" => $ticket->flag_title ,
				] ,
				"date" => echoDate($ticket->created_at, 'j F Y - H:i') ,
				"link" => route('gachet.users.tickets.single', ['hashid' => $ticket->hashid]) ,
			];
		})->toArray();


@endphp

@section('dashboard-body')
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-center">
				<section class="panel">
					<header>
						<div class="title">{{ $__module->getTrans('tickets.title.plural') }}</div>
						<div class="functions">
							<a class="button action" href="{{ route('gachet.users.tickets.new') }}">
								{{ $__module->getTrans('tickets.new') }}
							</a>
						</div>
					</header>
					<div class="table-responsive bd-n m0">
						<table class="table orders">
							<thead>
							<tr>
								<th>
									{{ $__module->getTrans('tickets.ticket_id') }}
								</th>
								<th>
									{{ $__module->getTrans('tickets.subject') }}
								</th>
								<th>
									{{ $__module->getTrans('tickets.organization') }}
								</th>
								<th>
									{{ $__module->getTrans('tickets.status') }}
								</th>
								<th>
									{{ $__module->getTrans('tickets.updated_at') }}
								</th>
								{{--<th>--}}
								{{--{{ $__module->getTrans('general.table.ticket_id') }}--}}
								{{--</th>--}}
								<th width="1"></th>
							</tr>
							</thead>
							<tbody>
							@foreach($tickets as $ticket)

								<tr>
									<td class="fw-b color-blue-darker">
										{{ ad($ticket['id']) }}
									</td>
									<td>
										{{ $ticket['title'] }}
									</td>
									<td class="fw-b color-gray">
										{{ $ticket['organization'] }}
									</td>
									<td>
										<div class="label {{ $ticket['flag']['color'] }}">
											{{ $ticket['flag']['title'] }}
										</div>
									</td>
									<td>
										{{ ad($ticket['date']) }}
									</td>
									{{--<td>--}}
									{{--<div class="label {{ $ticket['status']['color'] }}">--}}
									{{--{{ $ticket['status']['title'] }}--}}
									{{--</div>--}}
									{{--</td>--}}
									<td>
										<a href="{{$ticket['link']}}" class="button icon-eye gray-lightest alt">
											<i class="fa fa-pencil"></i>
										</a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</section>
			</div>
		</div>
	</div>
@stop
