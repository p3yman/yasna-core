@extends($__module->getBladePath('user.frame.main'))

@php
	$activeTab = 'tickets';
	Template::appendToPageTitle($__module->getTrans('general.ticket.title.plural'));
	Template::appendToNavBar([
		$__module->getTrans('general.ticket.title.plural'), route('gachet.users.tickets')
	]);
@endphp

@section('dashboard-body')
	<div class="container">

		<div class="row">
			<div class="col-sm-8 col-center">
				{!! Form::open([
					'url' => route('gachet.users.tickets.save'),
					'method'=> 'post',
					'class' => 'js',
					'id' => 'ticket-form',
					'autocomplete' => 'off'
				]) !!}
				<section class="panel">
					<article>

						<div class="field">
							<label>
								{{ $__module->getTrans('tickets.subject') }}
							</label>
							<input type="text" name="title">
						</div>

						<div class="row">

							<div class="col-sm-6">

								<div class="field">
									<label>
										{{ $__module->getTrans('tickets.organization') }}
									</label>
									<div class="select">
										@php $ticket_types = gachet()->tickets()->ticketTypesCombo() @endphp
										<select name="ticket_type">
											<option value=""></option>
											@foreach($ticket_types as $type)
												<option value="{{ $type['value'] }}">{{ $type['title'] }}</option>
											@endforeach
										</select>
									</div>
								</div>

							</div>

							<div class="col-sm-6">

								<div class="field">
									<label>
										{{ $__module->getTrans('tickets.priority') }}
									</label>
									<div class="select">
										@php $priorities = gachet()->tickets()->prioritiesCombo() @endphp
										<select name="priority">
											@foreach ($priorities as $priority)
												<option value="{{ $priority['value'] }}">{{ $priority['title'] }}</option>
											@endforeach
										</select>
									</div>
								</div>

							</div>

						</div>

						<div class="field">
							<label>
								{{ $__module->getTrans('tickets.ticket_text') }}
							</label>
							<textarea rows="10" name="text"></textarea>
						</div>

						<div class="ta-l mt10">
							<button class="button blue lg">ثبت تیکت</button>
						</div>

						<div class="ta-l mt10">
							@include($__module->getBladePath('layouts.widgets.form.feed'))
						</div>

					</article>
				</section>
				</form>
			</div>
		</div>


	</div>
@stop

@include($__module->getBladePath('layouts.widgets.form.scripts'))
