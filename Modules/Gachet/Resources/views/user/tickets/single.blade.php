@extends($__module->getBladePath('user.frame.main'))

@php
	$activeTab = 'tickets';
	Template::appendToPageTitle($__module->getTrans('general.ticket.title.plural'));
	Template::appendToNavBar([
		$__module->getTrans('general.ticket.title.plural'), route('gachet.users.tickets')
	]);


$ticket_info = [
		"id" => $ticket->hashid ,
		"title" => $ticket->title ,
		"organization" => $ticket->ticket_type->title ,
		"flag" => [
			"color" => ($ticket->flag == 'done') ? 'red' : 'green' ,
			"title" => $ticket->flag_title ,
		] ,
		"date" => echoDate($ticket->created_at, 'j F Y - H:i') ,
		"link" => "#" ,
	];
@endphp

@section('dashboard-body')

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-center">
				<section class="panel ticket-info">
					<article>
						<div class="item">
							<small>
								{{ $__module->getTrans('tickets.updated_at') }}
							</small>
							<h5>
								{{ ad($ticket_info['date']) }}
							</h5>
						</div>
						<div class="item">
							<small>
								{{ $__module->getTrans('tickets.organization') }}
							</small>
							<h5>
								{{ $ticket_info['organization'] }}
							</h5>
						</div>
						<div class="item">
							<small>
								{{ $__module->getTrans('tickets.status') }}
							</small>
							<div class="label {{ $ticket_info['flag']['color'] }}">
								{{ $ticket_info['flag']['title']  }}
							</div>
						</div>
						{{--<div class="item">--}}
						{{--<small>--}}
						{{--{{ $__module->getTrans('tickets.priority') }}--}}
						{{--</small>--}}
						{{--<div class="label {{ $ticket_info['priority']['color'] }}">--}}
						{{--{{ $ticket_info['priority']['title'] }}--}}
						{{--</div>--}}
						{{--</div>--}}
					</article>
				</section>
				<section class="panel ticket-content">
					<header>
						<div class="title">
							{{ $ticket_info['title'] }}
						</div>
					</header>

					@php $replies = $ticket->replies()->withType('default')->orderByDesc('created_at')->get() @endphp
					@foreach($replies as $reply)
						<article>
							<div class="ticket-post">
								<div class="head">
									<h5>{{ $reply->user->full_name }}</h5>
									<span class="date">{{ ad(echoDate($reply->created_at, 'j F Y - H:i'))  }}</span>
								</div>
								<div class="content">
									<p>
										{{ $reply->text }}
									</p>
								</div>
							</div>
						</article>
					@endforeach
				</section>

				@if ($ticket->flag != 'done')
					<section class="panel">
						<article>
							{!! widget('form-open')->target(route('gachet.users.tickets.reply'))->class('js') !!}

							{!! widget('hidden')->name('hashid')->value($ticket->hashid) !!}

							<div class="field">
								<label>
									{{ $__module->getTrans('tickets.reply') }}
								</label>
								<textarea rows="10" name="text"></textarea>
							</div>
							<div class="ta-l mt10">
								<button class="button blue lg">
									{{ $__module->getTrans('tickets.send_reply') }}
								</button>
							</div>

							<div class="mt10">
								{!! widget('feed') !!}
							</div>
							{!! widget('form-close') !!}
						</article>
					</section>
				@endif
			</div>
		</div>
	</div>
@stop


@include($__module->getBladePath('layouts.widgets.form.scripts'))
