<table class="table bordered">
    <thead>
    <tr>
        <th>{{ $__module->getTrans('post.product.singular') }}</th>
        <th>{{ $__module->getTrans('cart.price.unit') }}</th>
        <th>{{ $__module->getTrans('cart.numeration-label.amount') }}</th>
        <th>{{ $__module->getTrans('cart.price.total') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cart->orders as $order)
        @include(CurrentModule::bladePath('user.orders.modal-content-products-item'))
    @endforeach
    </tbody>
</table>
