<header>
    <div class="title">
        {!! $__module->getTrans('cart.headline.details-of-order', ['order' => '<b>' . $cart->code . '</b>']) !!}
    </div>
</header>
<article>
    @include(CurrentModule::bladePath('user.orders.modal-content-cart-info'))
    <div class="cart-full">
        @include(CurrentModule::bladePath('user.orders.modal-content-products-list'))
        <div class="row">
            <div class="col-xs-12">
                <div class="clearfix"></div>
                <table class="table bordered cart-total">
                    <tbody>
                    @if($cart->original_price != $cart->sale_price)
                        <tr>
                            <td>{{ $__module->getTrans('cart.price.your-purchase-total') }}:</td>
                            <td>
                                {{ ad(number_format(round($cart->invoiced_amount / 10))) }}
                                {{ $__module->getTrans('general.unit.currency.IRT') }}
                            </td>
                        </tr>
                        <tr>
                            <td>{{ $__module->getTrans('cart.headline.discount') }}:</td>
                            <td>
                                {{ ad(number_format(round(($cart->original_price - $cart->sale_price) / 10))) }}
                                {{ $__module->getTrans('general.unit.currency.IRT') }}
                            </td>
                        </tr>
                    @endif
                    <tr class="total">
                        <td>{{ $__module->getTrans('cart.price.purchasable') }}:</td>
                        <td>
                            {{ ad(number_format(round($cart->invoiced_amount / 10))) }}
                            {{ $__module->getTrans('general.unit.currency.IRT') }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        @include(CurrentModule::bladePath('user.orders.modal-content-transaction-history'))
        <div class="row">
            <div class="col-xs-12">
                <button class="button red close-modal">{{ $__module->getTrans('general.close') }}</button>
                <button class="button blue purchase-details">
                    {{ $__module->getTrans('cart.headline.your-purchasements-details') }}
                </button>
                @if ($cart->isNotPaid())
                    <a href="{{ CurrentModule::action('PurchaseController@paymentCallback', ['cart' => $cart->hashid]) }}"
                       class="button green" target="_blank">
                        {{ $__module->getTrans('cart.payment.title.singular') }}
                    </a>
                @endif
            </div>
        </div>
    </div>
</article>
