<section class="panel">
    <header>
        <div class="title"><span class="icon-cart"></span> سفارشات</div>
    </header>
    <article style="overflow-x: auto">
        <table class="table order-list">
            <thead>
            <tr>
                <th>{{ $__module->getTrans('general.row') }}</th>
                <th>{{ $__module->getTrans('cart.headline.receipt-number') }}</th>
                <th>{{ $__module->getTrans('general.date') }}</th>
                <th>{{ $__module->getTrans('cart.price.total') }}</th>
                <th>{{ $__module->getTrans('general.status') }}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @include(CurrentModule::bladePath('user.orders.list'))
            </tbody>
        </table>
    </article>
</section>

@include(CurrentModule::bladePath('user.orders.modal'))
