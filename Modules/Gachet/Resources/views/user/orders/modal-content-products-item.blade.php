@if ($ware = $order->ware)
    @php
        $ware = $ware->spreadMeta()->inLocale(getLocale())->in('IRT');
        $unit = $ware->unit;
        $post = $order->post()->withTrashed()->first();
        $postIsDeleted = !is_null($post->deleted_at);
    @endphp
    <tr>
        <td>
            <a @if (!$postIsDeleted) href="{{ ShopTools::productWareUrl($post, $ware) }}" @endif  class="product-name"
               target="_blank">
                <h5>{{ $ware->full_title }}</h5>
            </a>
        </td>
        <td class="price">
            @php $unitPrice = round($order->single_original_price / 10); @endphp
            {{ ad(number_format($unitPrice)) }}
            {{ $__module->getTrans('general.unit.currency.IRT') }}
        </td>
        <td class="tac">
            {{ ad($order->count) }}
            {{ $unit->title }}
        </td>
        <td class="price">
            @php $totalPrice = round($order->original_price / 10); @endphp
            @if ($order->sale_price == $order->original_price)
                {{ ad(number_format($totalPrice)) }}
                {{ $__module->getTrans('general.unit.currency.IRT') }}
            @else
                <table>
                    <tr>
                        <td>{{ $__module->getTrans('cart.price.total') }}:</td>
                        <td>
                            {{ ad(number_format($totalPrice)) }}
                            {{ $__module->getTrans('general.unit.currency.IRT') }}
                        </td>
                    </tr>
                    <tr>
                        <td>{{ $__module->getTrans('cart.headline.discount') }}:</td>
                        <td>
                            {{ ad(number_format(round(($order->original_price - $order->sale_price) / 10))) }}
                            {{ $__module->getTrans('general.unit.currency.IRT') }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {{ ad(number_format(round($order->sale_price / 10))) }}
                        </td>
                    </tr>
                </table>
            @endif
        </td>
    </tr>
@endif
