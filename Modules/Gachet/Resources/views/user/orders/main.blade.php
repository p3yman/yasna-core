@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'orders';
    Template::appendToPageTitle($__module->getTrans('general.order.plural'));
    Template::appendToNavBar([
        $__module->getTrans('general.order.plural'), CurrentModule::action('UserController@orders')
    ]);
@endphp

@section('dashboard-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-center">
                @include(CurrentModule::bladePath('user.orders.content'))
            </div>
        </div>
    </div>
@append

@include(CurrentModule::bladePath('user.orders.scripts'))
