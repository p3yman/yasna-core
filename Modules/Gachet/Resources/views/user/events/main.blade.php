@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'events';
    Template::appendToPageTitle($__module->getTrans('post.event.plural'));
    Template::appendToNavBar([
        $__module->getTrans('post.event.plural'), CurrentModule::action('UserController@events')
    ]);
@endphp

@section('dashboard-body')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center pt10">
                <article>
                    @include(CurrentModule::bladePath('user.events.accordion'))
                </article>
            </div>
        </div>
    </div>
@append
