@if ($currentEventsHTML or $waitingCount or $expiredCount)
    <article class="yt-accordion">
        @if ($currentEventsHTML)
            <h4 class="yt-accordion-header first default-active">
                <i class="shop-icon shop-icon-accordion"></i>
                <span class="text-violet">{{ $__module->getTrans('post.event.running-plural') }}</span>
            </h4>
            <div class="yt-accordion-panel auto-height">
                {!! $currentEventsHTML !!}
            </div>
        @endif
        @if($waitingCount)
            <h4 class="yt-accordion-header default-active">
                <i class="shop-icon shop-icon-accordion"></i>
                <span class="text-violet">{{ $__module->getTrans('post.event.soon') }}</span>
            </h4>
            <div class="yt-accordion-panel yt-lazy-load auto-height"
                 data-url="{{ CurrentModule::action('PostController@eventsAjax', ['type' => 'waiting']) }}">
            </div>
        @endif
        @if($expiredCount)
            <h4 class="yt-accordion-header last">
                <i class="shop-icon shop-icon-accordion"></i>
                <span class="text-violet">{{ $__module->getTrans('post.event.expired-plural') }}</span>
            </h4>
            <div class="yt-accordion-panel yt-lazy-load auto-height"
                 data-url="{{ CurrentModule::action('PostController@eventsAjax', ['type' => 'expired']) }}">
            </div>
        @endif
    </article>
@else
    <div class="col-xs-12">
        <div class="alert">{{ $__module->getTrans('post.event.no-event-fount') }}</div>
    </div>
@endif
