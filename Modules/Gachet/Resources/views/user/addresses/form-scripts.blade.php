@include($__module->getBladePath('layouts.widgets.form.scripts'))

@section('html-footer')
	<script>
        let main_map;
        let main_marker;

        function initMap() {
            if (typeof main_map == 'undefined') {
                main_map = L.map('map-box', {
                    doubleClickZoom: false,
                });
            } else {
                main_map.eachLayer(function (layer) {
                    main_map.removeLayer(layer);
                });
                main_marker = undefined;
            }

            L.tileLayer(window.mapbox.layer_uri, {
                maxZoom    : 18,
                id         : 'mapbox.streets',
                accessToken: window.mapbox.token,
            }).addTo(main_map);

            return main_map;
        }


        function initMarker(latlng) {
            main_marker = L.marker(latlng, {
                draggable: true,
            }).addTo(main_map);

            main_marker.on('move', function (e) {
                setInputValue(e.latlng);
            });
        }


        function setInputValue(latlng) {
            let input_value = latlng.lat + ',' + latlng.lng;
            $('.map-container').find('#location').val(input_value);
        }

        function showLocation(latlng) {
            initMap();
            main_map.setView(latlng, 13);
            initMarker(latlng);
            setInputValue(latlng);
            $('.map-in-form-cover').hide()
        }

        $(document).ready(function () {
            $('.map-in-form-cover').click(function () {
                let cover_element = $(this);

                let map = initMap();

                map.locate({
                    setView: true,
                    maxZoom: 16,
                });

                map.off('locationfound')
                    .on('locationfound', function (e) {
                        initMarker(e.latlng);
                        setInputValue(e.latlng);
                    });

                map.off('locationerror')
                    .on('locationerror', function () {
                        let site_location = {!! json_encode(get_setting('location') ?? []) !!};
                        if (site_location.length == 2) {
                            let latlng = L.latLng(site_location[0], site_location[1]);
                            main_map.setView(latlng, 13);
                            initMarker(latlng);
                            setInputValue(latlng);
                        } else {
                            cover_element.siblings('.not-allowed-message').show();
                        }
                    });

                cover_element.hide();
            });
//            showCurrentMarker();
        });
	</script>

	<script>
        let mapIdentifier = '{{ $mapContainerId }}';
        let urls          = {
            getCitiesSelect: '{{ route('gachet.province.cities-select', ['province' => '__PROVINCE__']) }}',
        };

        function addressAddedCallback(key) {
            $('body').removeClass('modal-open');
            $('.modal').removeClass('open');
            $('.modal-wrapper').removeClass('open');

            $('.overlay').removeClass('open');
            $('.blurable').removeClass('open');
            setTimeout(function () {
                $('.overlay').remove();
            }, 300);

            refreshAddressRow(key);
        }

        function refreshForm() {
            $('form#address-from').find('input,textarea,select').not('[name^=_]').val('').change();
        }
	</script>
	{!! GoogleMapTools::getScripts() !!}
	<script>
        let inFormMapItem = null;

        function getInFormMapItem() {
            if (!inFormMapItem) {
                inFormMapItem = maps.findMapItem(mapIdentifier);
            }

            return inFormMapItem;
        }

        function modalShown() {
            $('.map-in-form-cover').show();
        }

        $(document).ready(function () {

            $('#province_id').change(function (event) {
                if (event.originalEvent !== undefined) { // is clicked by human
                    let that = $(this);

                    $.ajax({
                        url       : urls.getCitiesSelect.replace('__PROVINCE__', that.val()),
                        beforeSend: function () {
                            $('#city_id').attr('disabled', 'disabled');
                        },
                        success   : function (rs) {
                            $('#city_id').html(rs);
                            $('#city_id').removeAttr('disabled');
                        }
                    });
                }
            });
        });
	</script>
@append
