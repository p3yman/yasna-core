<!-- Add code modal -->
<div class="modal-wrapper">
	<section class="panel modal lg" id="add-address-modal" data-onshow="modalShown()">
		{!! Form::open([
			'url' => route('gachet.address.save'),
			'method'=> 'post',
			'class' => 'js',
			'name' => 'address-from',
			'id' => 'address-from',
			'style' => 'padding: 15px;',
			'autocomplete' => 'off'
		]) !!}
		<article>
			@php
				$mapContainerId = 'my-map';
			@endphp
			<h2> {{ $__module->getTrans('general.address.register') }} </h2>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<input type="hidden" value="" name="id">

					@include($__module->getBladePath('user.profile.field'), [
						'name' => 'receiver',
						'placeholder' => '',
						'type' => 'text',
						'class' => 'form-required',
						'min' => 2,
					])

					<div class="row">
						<div class="col-md-6 col-xs-12">
							@include($__module->getBladePath('layouts.widgets.form.select'), [
								'name' => 'province_id',
								'value' => $default_province ?: '',
								'options' => $provinces_combo,
								'class' => 'form-required',
								'error_value' => $__module->getTrans('validation.javascript-validation.province'),
							])
						</div>
						<div class="col-md-6 col-xs-12">
							@include($__module->getBladePath('layouts.widgets.form.select'), [
								'name' => 'city_id',
								'value' => $default_city ?: '',
								'options' => $cities_combo,
								'class' => 'form-required',
								'error_value' => $__module->getTrans('validation.javascript-validation.city'),
							])
						</div>
					</div>

					@include($__module->getBladePath('user.profile.field'), [
						'name' => 'telephone',
						'placeholder' => '',
						'type' => 'text',
						'class' => 'form-required form-phone',
						'min' => 11,
						'max' => 11,
					])

					@include($__module->getBladePath('user.profile.field'), [
						'name' => 'postal_code',
						'placeholder' => '',
						'type' => 'text',
						'class' => 'form-required',
						'min' => 10,
						'max' => 10,
					])

					@include($__module->getBladePath('user.profile.field'), [
						'name' => 'floor',
						'label' => $__module->getTrans('validation.attributes.floor'),
						'error_value' => $__module->getTrans('validation.javascript-validation.floor'),
						'placeholder' => '',
						'type' => 'text',
						'class' => 'form-required form-number',
					])

					@include($__module->getBladePath('layouts.widgets.form.textarea'), [
						'name' => 'address',
						'class' => 'form-required',
					])

					@include($__module->getBladePath('layouts.widgets.form.feed'))

					<button class="green">
						{{ $__module->getTrans('general.address.register') }}
					</button>
					<input type="hidden" value="" name="location" id="location">
				</div>
				{{--<div class="col-xs-12 col-md-6">--}}
					{{--<div style="height: 400px;" class="map-in-form map-container">--}}
						{{--<input type="hidden" value="" name="location" id="location">--}}

						{{--<div id="map-box" class="map-box" style="width: 100%;height: 100%"></div>--}}

						{{--<div class="not-allowed-message">--}}
							{{--<h1 class="text-center"><i class="fa fa-exclamation-triangle"></i></h1>--}}
							{{--<p>{{ $__module->getTrans('general.location.message.is-disabled-in-browser') }}</p>--}}
							{{--<p>--}}
								{{--{{ $__module->getTrans('general.browser.firefox') }}:--}}
								{{--{{ $__module->getTrans('general.location.guide.activation-firefox') }}--}}
							{{--</p>--}}
							{{--<p>--}}
								{{--{{ $__module->getTrans('general.browser.chrome') }}:--}}
								{{--{!! $__module->getTrans('general.location.guide.activation-chrome', [--}}
									{{--'button' => '<i class="fa fa-exclamation-circle"></i>'--}}
								{{--])  !!}--}}
							{{--</p>--}}
						{{--</div>--}}
						{{--<div class="map-in-form-cover">--}}
							{{--<div class="map-in-form-cover-alert">--}}
								{{--{{ $__module->getTrans('general.location.message.select-to-increase-precision') }}--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
			</div>
			<div class="result">
				<div class="result-item" style="display: none;"></div>
			</div>

		</article>

		{!! Form::close() !!}
	</section>
</div>

@include($__module->getBladePath('user.addresses.form-scripts'))
@include($__module->getBladePath('user.addresses.form-styles'))
