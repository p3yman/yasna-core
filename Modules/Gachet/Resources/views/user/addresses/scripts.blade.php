@section('html-header')
	{!!
		Html::style('https://unpkg.com/leaflet@1.3.3/dist/leaflet.css', [
			'integrity' => 'sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==',
			'crossorigin' => ''
		])
	!!}
@append

@section('html-footer')
	<script>
        window.mapbox = {
            layer_uri: 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
            token    : "{{ $__module->getConfig('map-box.token') }}",
        };
	</script>

	{!!
		Html::script('https://unpkg.com/leaflet@1.3.3/dist/leaflet.js', [
			'integrity' => 'sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q==',
			'crossorigin' => ''
		])
	!!}

	<script>
        function refreshAddressRow(key) {
            $.ajax({
                url    : "{{ route('gachet.user.addresses.row', ['hashid' => '__KEY__']) }}"
                    .replace('__KEY__', key),
                success: function (rs) {
                    let row = $('.addresses-list-item#' + key);
                    if (row.length) {
                        row.replaceWith(rs);
                    } else {
                        $('.addresses-list').prepend(rs);
                    }
                }
            });
        }

        function addressAddedCallback(key) {
            $('body').removeClass('modal-open');
            $('.modal').removeClass('open');
            $('.modal-wrapper').removeClass('open');

            $('.overlay').removeClass('open');
            $('.blurable').removeClass('open');
            setTimeout(function () {
                $('.overlay').remove();
            }, 300);

            refreshAddressRow(key);
        }

        $(document).ready(function () {
            let modalBtn = $('button[data-modal=add-address-modal]');

            $(document).on({
                change: function () {
                    let key = $(this).val();
                    $.ajax({
                        url       : "{{ route('address.makeDefault', ['hashid' => '__KEY__']) }}"
                            .replace('__KEY__', key),
                        dataType  : 'JSON',
                        beforeSend: function () {
                            $('.addresses-list').addClass('disabled-box');
                        },
                        success   : function (rs) {
                            if (rs.success) {
                                $('.addresses-list').removeClass('disabled-box');
                            }
                        }
                    })
                }
            }, 'input[type=radio][name=default-address]');

            $(document).on({
                click: function (e) {
                    e.preventDefault();
                    let row  = $(this).closest('.addresses-list-item');
                    let data = $.parseJSON(row.find('.json').val());
                    $('.map-in-form-cover').show();

                    $.each(data, function (name, value) {
                        $('form#address-from').find(':input[name=' + name + ']').val(value).change();
                    });

                    if (data.province_id) {
                        let city_select = $('#city_id');
                        $.ajax({
                            url       : urls.getCitiesSelect.replace('__PROVINCE__', data.province_id),
                            beforeSend: function () {
                                city_select.attr('disabled', 'disabled');
                            },
                            success   : function (rs) {
                                city_select.html(rs);
                                city_select.removeAttr('disabled');

                                if (data.city_id) {
                                    city_select.val(data.city_id)
                                }
                            }
                        });
                    }


                    modalBtn.trigger('click');

                    if (data.location) {
                        let centerPoint = data.location.split(',');

                        let latlng = L.latLng(centerPoint[0], centerPoint[1]);
                        showLocation(latlng);
                        $('.map-in-form-cover').hide();
                    }
                }
            }, '.addresses-list-item-button.btn-edit');

			$(document).on({
                click: function (e) {
                    e.preventDefault();
                    let key = $(this).closest('.addresses-list-item').attr('id');
                    $.ajax({
                        url     : "{{ route('address.remove', ['hashid' => '__KEY__']) }}"
                                .replace('__KEY__', key),
                        dataType: 'JSON',
                        success : function (rs) {
                            if (rs.success) {
                                refreshAddressRow(key);
                            }
                        }
                    })
                }
            }, '.addresses-list-item-button.btn-remove');

            modalBtn.click(function (event) {
                if (event.originalEvent !== undefined) { // is clicked by human
                    refreshForm();
                }
            });
        });
	</script>
@append
