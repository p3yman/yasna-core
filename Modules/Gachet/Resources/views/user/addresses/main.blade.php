@extends($__module->getBladePath('user.frame.main'))

@php
    $activeTab = 'addresses';
    Template::appendToPageTitle($__module->getTrans('general.address.title.plural'));
    Template::appendToNavBar([
        $__module->getTrans('general.address.title.plural'), request()->url()
    ]);
@endphp

@section('dashboard-body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-center">
                <section class="panel">
                    <header>
                        <div class="title">
                            <span class="icon-tag"></span>
                            {{ $__module->getTrans('general.address.title.plural') }}
                        </div>
                        <div class="functions">
                            <button class="blue" data-modal="add-address-modal">
                                {{ $__module->getTrans('general.address.add') }}
                            </button>
                        </div>
                    </header>
                    @include($__module->getBladePath('user.addresses.list'))
                </section>
                @include($__module->getBladePath('user.addresses.form'))
            </div>
        </div>
    </div>
    @include($__module->getBladePath('user.addresses.scripts'))
@append
