<article>
    @if($addresses->count())
        <div class="row addresses-list">
                @foreach($addresses as $address)
                        @include($__module->getBladePath('user.addresses.item'))
                @endforeach
        </div>
        <div class="pagination-wrapper mt20">
            {!! $addresses->render() !!}
        </div>
    @else
        <div class="alert red">
            {{ $__module->getTrans('general.address.no-address-alert') }}
        </div>
    @endif
</article>
