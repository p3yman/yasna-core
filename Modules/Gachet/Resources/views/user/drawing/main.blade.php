@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'drawings';
    Template::appendToPageTitle($__module->getTrans('general.drawing.code.registered-plural'));
    Template::appendToNavBar([
        $__module->getTrans('general.drawing.code.registered-plural'), CurrentModule::action('UserController@drawing')
    ]);
@endphp

@section('dashboard-body')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                <section class="panel">
                    <header>
                        <div class="title">
                            <span class="icon-tag"></span>
                            {{ $__module->getTrans('general.drawing.code.registered-plural') }}
                        </div>
                        <div class="functions">
                            <button class="blue" data-modal="add-code-modal">
                                {{ $__module->getTrans('general.drawing.code.add') }}
                            </button>
                        </div>
                    </header>
                    <article>
                        @if(!array_default(\App\Models\User::getRequiredFields(), user()->toArray()))
                            <div class="alert alert-danger text-right">
                                {{ $__module->getTrans('user.message.complete-to-join-drawing') }}
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="{{ CurrentModule::action('UserController@profile') }}">
                                    {{ $__module->getTrans('user.edit-profile') }}
                                </a>
                            </div>
                        @endif

                        @if($receipts->count())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th> {{ $__module->getTrans('general.drawing.code.singular') }} </th>
                                    <th> {{ $__module->getTrans('form.created-at') }} </th>
                                    <th> {{ $__module->getTrans('form.purchased-at') }} </th>
                                    <th>
                                        {{ $__module->getTrans('form.amount') }}
                                        ({{ $__module->getTrans('general.unit.currency.IRR') }})
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($receipts as $receipt)
                                    <tr>
                                        <td class="fw-b color-green">{{ ad($receipt->dashed_code) }}</td>
                                        <td> {{ ad(echoDate($receipt->created_at, 'j F Y')) }} </td>
                                        <td> {{ ad(echoDate($receipt->purchased_at, 'j F Y')) }} </td>
                                        <td> {{ ad($receipt->amount_format) }} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper mt20">
                                {!! $receipts->render() !!}
                            </div>
                        @else
                            <div class="alert red">
                                {{ $__module->getTrans('user.message.drawing-code-not-found') }}
                            </div>
                        @endif
                    </article>
                </section>
                @include(CurrentModule::bladePath('user.drawing.form'))
            </div>
        </div>
    </div>
    @include(CurrentModule::bladePath('user.drawing.scripts'))
@append
