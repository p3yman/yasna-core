@section('html-footer')
    @include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
    <script>
        let drawingVars = {
            routes: {
                submit: "{{ CurrentModule::action('DrawingController@submitCode') }}",
                userDrawings: "{{ CurrentModule::action('UserController@drawing') }}",
                register: "{{ route('register') }}",
            },
            messages: {
                invalidCode: "{{ $__module->getTrans('general.drawing.message.invalid-code') }}"
            }
        };
    </script>
    {!! Html::script(CurrentModule::asset('js/drawing.min.js')) !!}
@append
