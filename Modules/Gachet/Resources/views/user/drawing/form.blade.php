<!-- Add code modal -->
<div class="modal-wrapper">
    <section class="panel modal sm add-code" id="add-code-modal">
        <article>
            <h2> {{ $__module->getTrans('general.drawing.code.register') }} </h2>
            <form class="form-horizontal" id="drawing-form">
                {{ csrf_field() }}
                <div class="gift-inputs">
                    <input type="text" class="gift-input" maxlength="26" id="gift-input" placeholder="-----  -----  -----  -----"
                           style="width: 100%;direction: ltr">
                </div>
                <div class="action">
                    <button class="block green" type="button" onclick="drawingCode();">
                        {{ $__module->getTrans('general.drawing.code.check') }}
                    </button>
                </div>
                <div class="result">
                    <div class="result-item" style="display: none;"></div>
                </div>
            </form>
        </article>
    </section>
</div>
