$(document).ready(function(){

    var body_rtl = false;
    if($('body').css('direction') == 'rtl'){
        body_rtl = true;
    }

    /*-----------------------------------------------------------------
     - Dropdown
     -----------------------------------------------------------------*/
    $('.dropdown-toggle').on('click', function(e) {
        e.preventDefault();
        if($(this).next('.menu').hasClass('open')){
            $('.dropdown-toggle').removeClass('active').next('.menu').removeClass('open');
        } else {
            $('.dropdown-toggle').removeClass('active').next('.menu').removeClass('open');
            $(this).addClass('active').next('.menu').addClass('open');
        }

    });
    $(document).on('click', function(e) {
        var target = e.target;
        if (!$(target).is('.dropdown-toggle, .dropdown-toggle *') && !$(target).parents().is('.dropdown-toggle + .menu')) {
            $('.dropdown-toggle').removeClass('active');
            $('.dropdown .menu').removeClass('open');
        }
    });

    /*-----------------------------------------------------------------
    - FAQ
    -----------------------------------------------------------------*/
    $('.faq-item:first-child').addClass('open');
    $('.faq-item a.q').on('click', function (e) {
        e.preventDefault();
        $(this).next('.answer').slideToggle('fast', function () {
            $(this).parent('.faq-item').toggleClass('open');
        });
    });

    /*-----------------------------------------------------------------
    - Home Slider
    -----------------------------------------------------------------*/
    let home_slider = $('#home-slides ul.home-slides');
    home_slider.responsiveSlides({
        auto    : (home_slider.data('slideshowAuto') == true),
        pause   : (home_slider.data('slideshowPause') == true),
        speed   : home_slider.data('slideshowSpeed'),
        timeout : home_slider.data('slideshowTimeout'),
        pager   : true,
        nav     : true,
        prevText: '<em href="#" class="arrow icon-angle-left"></em>',
        nextText: '<em href="#" class="arrow icon-angle-right"></em>',
    });

    /*-----------------------------------------------------------------
    - Product Gallery
    -----------------------------------------------------------------*/
    $('ul.slides').responsiveSlides({
        auto: false,
        manualControls: "#product-gallery-thumbnails",
    });
    $('.thumbnails').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        rtl: body_rtl,
        prevArrow: '<a href="#" class="arrow icon-angle-left"></a>',
        nextArrow: '<a href="#" class="arrow icon-angle-right"></a>',
    });

    /*-----------------------------------------------------------------
    - Testimonials
    -----------------------------------------------------------------*/
//    $('.testimonials-list').slick({
//        autoplay: true,
//        centerMode: true,
//        slidesToShow: 1,
//        slidesToScroll: 1,
//        rtl: false,
//        arrows: false,
//    });


    /*-----------------------------------------------------------------
    - Gift input
    -----------------------------------------------------------------*/
    $("input.gift-input").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        $(this).val(persianJs($(this).val()).englishNumber().toString());
    });
    $("input.gift-input").keyup(function(){
        $(this).val(persianJs($(this).val()).englishNumber().toString());
        if (this.value.length >= this.attributes["maxlength"].value) {
            $(this).next('.gift-input').focus();
        }
    });


    /*-----------------------------------------------------------------
     - Modal
     -----------------------------------------------------------------*/
    $(document).on({
        click: function (e) {
        e.preventDefault();
        var target = $(this).attr('data-modal');
        var file_elm = $(this).attr('data-index');
        if (!target.match("^#")) {
            target = '#' + target;
        }
        $('body').addClass('modal-open');
        $('.modal-wrapper').addClass('open');
        $(target).addClass('open').attr('data-file', file_elm);

            let showCallback = $(target).attr('data-onshow');
            if (showCallback) {
                eval(showCallback);
            }

        $( 'body' ).append( '<div class="overlay"></div>' );
        setTimeout(function(){
            $('.overlay').addClass('open');
            $('.blurable').addClass('open');
        }, 10);
        }
    }, '[data-modal]');

    $(document).on('click', '.modal-wrapper, .close-modal', function(e) {
        if(!$(this).hasClass('close-modal') && $(e.target).is('.modal *')){
            return;
        }

        e.preventDefault();

        $('body').removeClass('modal-open');
        $('.modal').removeClass('open');
        $('.modal-wrapper').removeClass('open');

        $('.overlay').removeClass('open');
        $('.blurable').removeClass('open');
        setTimeout(function(){
            $('.overlay').remove();
        }, 300);
    });

    /*-----------------------------------------------------------------
    - Nano scroller
    -----------------------------------------------------------------*/
    $(".nano").nanoScroller();

    /*-----------------------------------------------------------------
    - Selectable radio
    -----------------------------------------------------------------*/
    $('.selectable-radio input[type="radio"]').on('change', function () {
        $(this).parents().eq(2).find('.selectable-radio').removeClass('selected');
        $(this).parents().eq(1).addClass('selected');
    });
    $('.selectable-checkbox input[type="checkbox"]').on('change', function () {
        console.log($(this));
        $(this).parents().eq(1).toggleClass('selected');
    });

    /*-----------------------------------------------------------------
     - Menu
     -----------------------------------------------------------------*/
    $('a#open-menu').on('click', function (e) {
        e.preventDefault();
        $('body').addClass('menu-opened');
        $('ul.menu').addClass('opened');
    });
    $('a#close-menu').on('click', function (e) {
        e.preventDefault();
        $('body').removeClass('menu-opened');
        $('ul.menu').removeClass('opened');
    });
    var toggleLink = '<a href="#" class="icon-angle-down sub-toggle"></a>';
    $('header#main-header ul.menu li.has-child').append(toggleLink);
    $(document).on('click', 'a.sub-toggle', function (e) {
        e.preventDefault();
        $(this).toggleClass('toggled').prev().slideToggle();
    });

    /*-----------------------------------------------------------------
     - Items Slider
     -----------------------------------------------------------------*/
    $('.best-selling').each(function () {
        var container = $(this);

        var slider = container.find('.items-slider').owlCarousel({
            loop           : true,
            margin         : 10,
            autoWidth      : false,
            rtl            : true,
            responsiveClass: true,
            stagePadding   : 3,
            // items          : 20,
            nav            : false,
            responsive     : {
                0:{
                    items:1
                },
                500:{
                    items:3
                },
                1000:{
                    items:5
                }
            }
        });

        container.find('.nav').on('click','.prev', function () {
            slider.trigger('prev.owl.carousel')
        });

        container.find('.nav').on('click', '.next', function () {
            slider.trigger('next.owl.carousel')
        });

    });

});
