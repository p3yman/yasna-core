<?php

namespace Modules\Gachet\Providers;

use App\Models\Post;
use Illuminate\Support\ServiceProvider;

class CompareServiceProvider extends ServiceProvider
{
    /**
     * Adds the given post to the compare.
     *
     * @param Post $post
     *
     * @return bool
     */
    public static function addItem(Post $post)
    {
        $compare            = specs_compare();
        $products           = static::getSessionProducts();
        $product_identifier = $post->hashid;

        $products[] = $product_identifier;
        $products   = array_unique($products);

        $compare->addProduct(...$products);

        $comparable = $compare->productsAreComparable();

        if ($comparable) {
            session()->put(static::getSessionName(), $products);
        }

        return $comparable;
    }



    /**
     * Returns an array of hashids of products in compare.
     *
     * @return array
     */
    public static function getSessionProducts()
    {
        return session()
                  ->get(static::getSessionName())
             ?? [];
    }



    /**
     * Returns the name of the session which is holding the comparing products.
     *
     * @return string
     */
    public static function getSessionName()
    {
        return 'comparing_products';
    }



    /**
     * Returns the compare array.
     *
     * @return array
     */
    public static function getArray()
    {
        $compare  = specs_compare();
        $products = static::getSessionProducts();

        $compare->addProduct(...$products);

        return $compare->getCompareArray();
    }



    /**
     * Whether the compare session is empty.
     *
     * @return bool
     */
    public static function isEmpty()
    {
        return empty(static::getSessionProducts());
    }



    /**
     * Removes the specified post from the comparing items.
     *
     * @param Post $post
     *
     * @return bool
     */
    public static function removeItem(Post $post)
    {
        $products = static::getSessionProducts();
        $index    = array_search($post->hashid, $products);

        if ($index >= 0) {
            unset($products[$index]);

            $products = array_values($products);

            session()->put(static::getSessionName(), $products);
        }

        return false;
    }
}
