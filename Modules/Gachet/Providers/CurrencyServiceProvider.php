<?php

namespace Modules\Gachet\Providers;

use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Returns the default currency of the site.
     *
     * @return string
     */
    public static function getCurrency()
    {
        return 'IRR';
    }



    /**
     * Returns the default currency.
     *
     * @return string
     */
    public static function getDefaultCurrency()
    {
        return static::getCurrency();
    }



    /**
     * Returns the currency that should be used in showing prices to user
     *
     * @return string
     */
    public static function getPreviewCurrency()
    {
        return 'IRT';
    }



    /**
     * Returns the ratio to be used while converting a price from the default currency to the preview currency.
     *
     * @return float
     */
    public static function getPreviewCurrencyRatio()
    {
        return 0.1;
    }



    /**
     * Returns an array of all currencies.
     *
     * @return array
     */
    public static function getAllCurrencies()
    {
        $default_currency    = static::getCurrency();
        $specific_currencies = static::getSpecificCurrencies();

        return array_merge([$default_currency], $specific_currencies);
    }



    /**
     * Returns an array of specific currencies.
     *
     * @return array
     */
    public static function getSpecificCurrencies()
    {
        return ['VIP', 'CWR'];
    }
}
