<?php

namespace Modules\Gachet\Providers;

use Illuminate\Support\ServiceProvider;

class DrawingCodeServiceProvider extends ServiceProvider
{
    protected static $codeLength = 20;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function checkCode($unique)
    {
        if (strlen($unique) != self::$codeLength or ! is_numeric($unique)) {
            return false;
        }

        $unique = str_split($unique);
        $data = array();

        // change 20 and 15 uniq code character
        $character15 = $unique[14];
        $character20 = $unique[19];
        $unique[14] = $character20;
        $unique[19] = $character15;

        $controller = 9 - $unique[19];

        // controller number
        $starter = 9 - $unique[0];
        if ($starter <= 2) {
            $starter = 8;
        }

        // create controller number
        $sum = 0;
        for ($i = 18; $i > 0; $i--) {
            if ($starter < 2) {
                $starter = 8;
            }
            $sum += ($unique[$i] * $starter);
            $starter--;
        }
        $sum += $unique[0];
        $sum = str_split($sum);

        if ($sum[count($sum) - 1] != $controller) {
            return false;
        }

        // change 1 and 5 uniq code character
        $character1 = $unique[0];
        $character5 = $unique[4];
        $unique[0] = $character5;
        $unique[4] = $character1;

        $data['date'] = 1 . $unique[1] . $unique[3] . $unique[5] . $unique[7] . $unique[9] . $unique[11] . $unique[13] . $unique[15] . $unique[17];

        $price = $unique[2] . $unique[4] . $unique[6] . $unique[8] . $unique[10] . $unique[12] . $unique[14] . $unique[16] . $unique[18];
        $price = str_split($price);
        $data['price'] = '';
        for ($i = 0; $i < $unique[0]; $i++) {
            $data['price'] .= (9 - $price[$i]);
        }

        return $data;
    }



    /**
     * Creates a code based on timestamp and amount.
     *
     * @param string $timestamp
     * @param string $amount
     *
     * @return string
     */
    public static function createCode(string $timestamp, $amount)
    {
        $timestamp           = str_split(ltrim($timestamp, '1'));
        $invoice_price_count = strlen($amount);
        $amount              = str_split($amount);
        $unique_code         = $invoice_price_count;

        // Reverse invoice price reduce per character from 9
        $reverse_invoice_price = '';
        for ($i = 0; $i < $invoice_price_count; $i++) {
            $reverse_invoice_price .= (9 - $amount[$i]);
        }

        // Add numbers to reversed amount characters
        // until the characters' number raise to 0
        $invoice_price_be_change = $reverse_invoice_price;
        for ($i = 0, $a = 1; $i < 9 - $invoice_price_count; $i++) {
            $invoice_price_be_change .= $a++;
        }

        // Mix timestamp and invoice
        for ($i = 0; $i < 9; $i++) {
            $unique_code .= $timestamp[$i];
            $unique_code .= $invoice_price_be_change[$i];
        }

        // Change 1st and 5th characters character of the unique code
        $character1     = $unique_code[0];
        $character5     = $unique_code[4];
        $unique_code[0] = $character5;
        $unique_code[4] = $character1;

        // Controller number
        $starter = 9 - $unique_code[0];
        if ($starter <= 2) {
            $starter = 8;
        }

        // Create Controller number
        $sum = 0;
        for ($i = 18; $i > 0; $i--) {
            if ($starter < 2) {
                $starter = 8;
            }
            $sum += ($unique_code[$i] * $starter);
            $starter--;
        }
        $sum += $unique_code[0];
        $sum = str_split($sum);

        // Create unique code character 20
        $unique_code .= (9 - $sum[count($sum) - 1]);

        // Change 20th and 15th unique code character
        $character15     = $unique_code[14];
        $character20     = $unique_code[19];
        $unique_code[14] = $character20;
        $unique_code[19] = $character15;

        // Separator
        $final_uniq_code = '';
        for ($i = 0; $i <= 19; $i++) {
            if ($i == 4 or $i == 9 or $i == 14) {
                $seperator = '-';
            } else {
                $seperator = '';
            }
            $final_uniq_code .= $unique_code[$i] . $seperator;
        }

        return $final_uniq_code;
    }
}
