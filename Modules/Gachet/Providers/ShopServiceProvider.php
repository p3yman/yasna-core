<?php

namespace Modules\Gachet\Providers;

use App\Models\Post;
use App\Models\Ware;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Modules\Gachet\Services\Module\CurrentModuleHelper;
use Modules\Gachet\Services\Products\Products;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class ShopServiceProvider extends ServiceProvider
{
    use ModuleRecognitionsTrait;


    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * boot the provider
     *
     * @param Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->registerProductSingleton();
    }



    /**
     * Registers the `gachet.shop.products` singleton.
     */
    protected function registerProductSingleton()
    {
        $this->app->singleton('gachet.shop.products', function () {
            return new Products();
        });
    }



    /**
     * Returns the max price of the given posts.
     *
     * @param Collection $posts
     *
     * @return float|null
     */
    public static function postsMaxPrice($posts)
    {
        return Ware::whereIn('sisterhood', $posts->pluck('sisterhood')->toArray())
                   ->max(gachet()->shop()->getClientWarePriceColumn('sale'))
             ;
    }



    /**
     * Returns the min price of the given posts.
     *
     * @param Collection $posts
     *
     * @return float|null
     */
    public static function postsMinPrice($posts)
    {
        return Ware::whereIn('sisterhood', $posts->pluck('sisterhood')->toArray())
                   ->min(gachet()->shop()->getClientWarePriceColumn('sale'))
             ;
    }



    /**
     * return ware custom code from first post's ware
     *
     * @param $posts_id
     *
     * @return string|bool
     */
    public static function postShopWareCustomCode($posts_id)
    {
        $custom_code = model('post', $posts_id)->wares()->where('custom_code', '>', 0)->first();
        if ($custom_code) {
            return $custom_code->custom_code;
        } else {
            return false;
        }
    }



    /**
     * Returns url to specific ware of a $post
     *
     * @param \App\Models\Post      $post
     * @param \App\Models\Ware|null $ware
     *
     * @return string
     */
    public static function productWareUrl(Post $post, Ware $ware = null)
    {
        $route_parameters = ['identifier' => RouteServiceProvider::$postDirectUrlPrefix . $post->hashid];
        $title            = urlHyphen($post->title);

        if (
             $ware and // Ware specified
             $ware->exists and // Ware exists
             $post->wares()->where(['id' => $ware->id])->count() // Ware belongs to specified product
        ) {
            $route_parameters['wareOrTitle'] = $ware->hashid;
            $route_parameters['title']       = $title;
        } else {
            $route_parameters['wareOrTitle'] = $title;
        }

        return route('gachet.product.single', $route_parameters);
    }



    /**
     * Returns all packages.
     *
     * @param bool $active
     *
     * @return Collection
     */
    public static function getAllPackages(bool $active = false)
    {
        $packages = Ware::whereNull('sisterhood');

        if ($active) {
            $usedPackages = Ware::select('package_id')
                                ->whereNotNull('sisterhood')
                                ->where('package_id', '>', 0)
                                ->groupBy('package_id')
                                ->get()
                                ->pluck('package_id')
                                ->toArray()
            ;

            $packages->whereIn('id', $usedPackages);
        }

        return $packages->get();
    }



    /**
     * Returns a version of the price to be shown.
     *
     * @param float|int|string $amount
     * @param bool             $show_currency
     *
     * @return string
     */
    public static function showPrice($amount, $show_currency = true)
    {
        $price_part = ad(number_format(self::priceToShow($amount)));

        if ($show_currency) {
            $preview_currency = gachet()->currency()->getPreviewCurrency();
            $currency_part    = module('gachet')->getTrans("general.unit.currency.$preview_currency");
        } else {

            $currency_part = '';
        }

        return trim($price_part . ' ' . $currency_part);
    }



    /**
     * Returns the value of the price in the preview currency.
     *
     * @param float|int|string $amount
     *
     * @return float
     */
    public static function priceToShow($amount)
    {
        return round($amount * CurrencyServiceProvider::getPreviewCurrencyRatio());
    }



    /**
     * Returns the usable currency based on the roles of current client.
     *
     * @return string
     */
    public static function usableCurrency()
    {
        if (user()->hasRole('vip')) {
            return 'VIP';
        }

        if (user()->hasRole('coworker')) {
            return 'CWR';
        }


        return gachet()->currency()->getDefaultCurrency();
    }



    /**
     * Returns the name of the column to be used in the `wares` table based on the current client.
     *
     * @param string $type
     *
     * @return string
     */
    public static function getClientWarePriceColumn(string $type = 'original')
    {
        $currency = gachet()->shop()->usableCurrency();

        return gachet()->shop()->getWarePriceColumn($currency, $type);
    }



    /**
     * Returns the name of the column to be used in the `wares` table based on the given currency.
     *
     * @param string $currency
     * @param string $type
     *
     * @return string
     */
    public static function getWarePriceColumn(string $currency, string $type = 'original')
    {
        $parts = [$type, 'price'];
        if ($currency != gachet()->currency()->getDefaultCurrency()) {
            array_unshift($parts, strtolower($currency));
        }

        return implode('_', $parts);
    }



    /**
     * Returns an array of tabs to be shown in the single page of the given product.
     *
     * @param Post $post
     *
     * @return array
     */
    public static function getProductSingleTabs(Post $post)
    {
        $module        = module('gachet');
        $posttype      = posttype($post->type);
        $tabs          = gachet()->shop()->getProductsSingleGeneralTabs();
        $show_comments = (
             $post->has('commenting') and
             $posttype->getMeta('commenting_receive') and
             $posttype->getMeta('commenting_display')
        );
        $pure_text     = preg_replace("/&#?[a-z0-9]+;/i", "", strip_tags($post->text));

        if ($pure_text) {
            $tabs[0] = [
                 "title"  => $module->getTrans('general.description'),
                 "link"   => "#details",
                 "active" => false,
            ];
        }

        if ($post->getMeta('after_sale_services')) {
            $tabs[3] = [
                 "title"  => $module->getTrans('general.supports'),
                 "link"   => "#support",
                 "active" => false,
            ];
        }

        if ($show_comments) {
            $tabs[4] = [
                 "title"  => $module->getTrans('general.comment.title.plural'),
                 "link"   => "#comments",
                 "active" => false,
            ];
        }

        if ($post->hasSpecs() and boolval($post->specsPivots()->count())) {
            $tabs[1] = [
                 "title"  => $module->getTrans('general.specs'),
                 "link"   => "#specs",
                 "active" => false,
            ];
        }

        ksort($tabs);
        $tabs              = array_values($tabs);
        $tabs[0]['active'] = true;

        return $tabs;
    }



    /**
     * Returns an array of general tabs to be shown in the single page of products.
     *
     * @return array
     */
    public static function getProductsSingleGeneralTabs()
    {
        $module = module('gachet');
        return [
             2 => [
                  "title"  => $module->getTrans('general.prices_graph'),
                  "link"   => "#prices",
                  "active" => false,
             ],
        ];
    }



    /**
     * Generates data to be used while rendering chart data for a product post.
     *
     * @param Post $post
     *
     * @return array
     */
    public static function generateProductChartData(Post $post)
    {
        $data   = [];
        $labels = [];
        $wares  = $post->wares;

        foreach ($wares as $ware) {
            $ware_info = gachet()->shop()->generateWareChartData($ware);
            $data[]    = $ware_info;
            $labels    = array_merge($labels, $ware_info['labels']);
            unset($ware_info['labels']);
        }

        $labels = static::makeChartLabelsReady($labels);


        return compact('labels', 'data');
    }



    /**
     * Makes the given labels array ready to be used while rendering chart data.
     *
     * @param array $labels
     *
     * @return array
     */
    public static function makeChartLabelsReady(array $labels)
    {
        $labels = array_unique($labels);
        rsort($labels);
        $labels = array_values($labels);
        $labels = array_map(function ($label) {
            return static::renderChartLabel($label);
        }, $labels);

        return $labels;
    }



    /**
     * Generates data for a ware to be shown in a product's price chart.
     *
     * @param Ware $ware
     *
     * @return array
     */
    public static function generateWareChartData(Ware $ware)
    {
        $color       = gachet()->randomColor();
        $result      = [
             'label'            => $ware->titleIn(getLocale()),
             'fill'             => false,
             'borderColor'      => $color,
             'pointBorderColor' => $color,
        ];
        $last_prices = $ware
             ->prices()
             ->where('currency', gachet()->shop()->usableCurrency())
             ->where('type', 'original')
             ->orderByDesc('effected_at')
             ->limit(50)
             ->get()
        ;

        foreach ($last_prices->reverse() as $price) {
            $date               = $price->effected_at->toDayDateTimeString();
            $price              = gachet()->shop()->priceToShow($price->amount);
            $result['labels'][] = $date;
            $result['data'][]   = [
                 'x' => static::renderChartLabel($date),
                 'y' => $price,
            ];
        }


        return $result;
    }



    /**
     * Renders a label to be used in a product's price chart.
     *
     * @param string|Carbon $date
     *
     * @return string
     */
    public static function renderChartLabel($date)
    {
        return ad(echoDate($date, 'j F Y'));
    }



    /**
     * Returns a collection of specs pivot grouped by their parent IDs.
     *
     * @param Post $post
     *
     * @return Collection
     */
    public static function getSpecsPivotParents(Post $post)
    {
        return $post
             ->specsPivots()
             ->whereNull('hid_at')
             ->get()
             ->groupBy(function ($item) {
                 return $item->spec->parent_id;
             })
             ;
    }



    /**
     * Returns the slug of the folder reserved for holding brands.
     *
     * @return string
     */
    public static function getBrandsFolderSlug()
    {
        return 'brands';
    }



    /**
     * Returns a list of wares to be shown in the best sellers section.
     *
     * @param int $limit
     *
     * @return Collection
     */
    public function getBestSellerWares(int $limit = 8)
    {
        $default          = model('ware')->bestSellers()->get();
        $remained_numbers = $limit - $default->count();

        if ($remained_numbers) {
            $random_wares = model('ware')
                 ->inRandomOrder()
                 ->limit($remained_numbers)
                 ->get()
            ;

            return $default->merge($random_wares);
        } else {
            return $default;
        }
    }

}
