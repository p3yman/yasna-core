<?php

namespace Modules\Gachet\Providers;

use App\Models\Post;
use Carbon\Carbon;
use Modules\Cart\Events\CartCheckedOut;
use Modules\Cart\Events\CartCheckoutUndid;
use Modules\Cart\Events\CartConfirmed;
use Modules\Cart\Events\CartStatusChanged;
use Modules\Gachet\Events\PasswordResetRequested;
use Modules\Gachet\Http\Middleware\CartNonemptyMiddleware;
use Modules\Gachet\Http\Middleware\DebugMiddleware;
use Modules\Gachet\Http\Middleware\OwnCartMiddleware;
use Modules\Gachet\Listeners\AssignRolesOnUserRegistered;
use Modules\Gachet\Listeners\HandleSpecificCurrenciesCacheWhenWareUpdated;
use Modules\Gachet\Listeners\HandleSpecificCurrenciesWhenWareUpdated;
use Modules\Gachet\Listeners\ManageUserCartOnUserLoggedIn;
use Modules\Gachet\Listeners\NotifyUserWhenPasswordResetRequested;
use Modules\Gachet\Listeners\SaveReceiptWhenCartStatusChanged;
use Modules\Gachet\Listeners\SendRegistrationConfirmationsWhenNewUserRegistered;
use Modules\Gachet\Notifications\CartCheckoutAdminNotification;
use Modules\Gachet\Notifications\CartCheckoutUndidNotification;
use Modules\Gachet\Notifications\CartCheckoutUserNotification;
use Modules\Gachet\Notifications\CartConfirmedNotification;
use Modules\Gachet\Notifications\CartStatusChangedNotification;
use Modules\Gachet\Notifications\FailedPaymentNotification;
use Modules\Gachet\Schedules\NotifyForPendingCartsSchedule;
use Modules\Gachet\Schedules\NotifyForUnpaidCartsSchedule;
use Modules\Gachet\Schedules\RemoveNoOwnerCartsSchedule;
use Modules\Gachet\Schedules\RemoveOldAndPendingCartsSchedule;
use Modules\Gachet\Schedules\RunQueueWorkSchedule;
use Modules\Gachet\Services\AsanakSms\AsanakSmsProvider;
use Modules\Gachet\Services\Email\EmailServiceProvider;
use Modules\Gachet\Services\Module\CurrentModuleHelper;
use Modules\Gachet\Services\Template\GachetTemplateHandler;
use Modules\Gachet\ShippingRules\DestinationRule;
use Modules\Gachet\ShippingRules\StateRule;
use Modules\Payment\Events\FailedPayment;
use Modules\Shop\Events\WareUpdated;
use Modules\Yasna\Events\NewUserRegistered;
use Modules\Yasna\Events\UserLoggedIn;
use Modules\Yasna\Providers\ModuleEnrichmentTrait;
use Modules\Yasna\Services\YasnaProvider;

class GachetServiceProvider extends YasnaProvider
{
    use ModuleEnrichmentTrait;



    /**
     * Do things after default boot of this service provider.
     */
    public function index()
    {
        $this->addMiddlewares();
        $this->addProviders();
        $this->addAliases();
        $this->registerModelTraits();
        $this->registerPostHandlers();
        $this->registerEventListeners();
        $this->registerSchedules();
        $this->registerShippingRules();
    }



    /**
     * Register middlewares that are defined and used in this module.
     */
    protected function addMiddlewares()
    {
        $middlewares = [
             'cartNonempty' => CartNonemptyMiddleware::class,
             'debug'        => DebugMiddleware::class,
             'ownCart'      => OwnCartMiddleware::class,
        ];
        foreach ($middlewares as $alias => $middleware) {
            $this->addMiddleware($alias, $middleware);
        }
    }



    /**
     * Register provider that are defined and used in this module.
     */
    protected function addProviders()
    {
        $providers = [
             PostServiceProvider::class,
             DrawingCodeServiceProvider::class,
             RouteServiceProvider::class,
             AsanakSmsProvider::class,
             EmailServiceProvider::class,
             PostLabelsServiceProvider::class,
             ShopServiceProvider::class,
             PostsCollectionServiceProvider::class,
             CartServiceProvider::class,
             CurrencyServiceProvider::class,
             CommentServiceProvider::class,
             FormServiceProvider::class,
             TicketsServiceProvider::class,
             CompareServiceProvider::class,
        ];
        foreach ($providers as $provider) {
            $this->addProvider($provider);
        }
    }



    /**
     * Register aliases that are used in this module.
     */
    protected function addAliases()
    {
        $aliases = [
             'Template'             => GachetTemplateHandler::class,
             'RouteTools'           => RouteServiceProvider::class,
             'Carbon'               => Carbon::class,
             'ShopTools'            => ShopServiceProvider::class,
             'PostsCollectionTools' => PostsCollectionServiceProvider::class,
             'PosttypeTools'        => PosttypeServiceProvider::class,
             'CurrencyTools'        => CurrencyServiceProvider::class,
             'CommentTools'         => CommentServiceProvider::class,
             'CurrentModule'        => CurrentModuleHelper::class,
             'FormTools'            => FormServiceProvider::class,
        ];
        foreach ($aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }
    }



    /**
     * Register model traits that are defined and used in this module.
     */
    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":GachetUserTrait")->to("User")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":GachetUnitTrait")->to("Unit")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":GachetTransactionTrait")->to("Transaction")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":TagGachetTrait")->to("Tag")
        ;

        $this->addModelTrait('PostGachetTrait', 'Post');
        $this->addModelTrait('CategoryGachetTrait', 'Category');
        $this->addModelTrait('WareGachetTrait', 'Ware');
        $this->addModelTrait('PosttypeGachetTrait', 'Posttype');
        $this->addModelTrait('CartGachetTrait', 'Cart');
        $this->addModelTrait('OrderGachetTrait', 'Order');
        $this->addModelTrait('AddressGachetTrait', 'Address');
        $this->addModelTrait('LabelGachetTrait', 'Label');
        $this->addModelTrait('CalculatePriceDeliveryTrait', 'Cart');
    }



    /**
     * Add some items to <i>"browse_headings_handlers"</i> service of the <b>Post</b> module.
     */
    protected function registerPostHandlers()
    {
        module('posts')
             ->service('browse_headings_handlers')
             ->add($this->moduleAlias())
             ->method($this->moduleAlias() . ":handleBrowseHeadings")
        ;

        module('posts')
             ->service('model_selector')
             ->add('folder')
             ->default('')
             ->method('selector_folder')
             ->order(32)
        ;

        service('posts:editor_handlers')
             ->add($this->moduleAlias())
             ->method($this->moduleName() . ':handlePostEditor')
        ;
    }



    /**
     * Register event listeners.
     */
    protected function registerEventListeners()
    {
        $this->listen(UserLoggedIn::class, ManageUserCartOnUserLoggedIn::class);

        $this->listen(NewUserRegistered::class, SendRegistrationConfirmationsWhenNewUserRegistered::class);
        $this->listen(NewUserRegistered::class, AssignRolesOnUserRegistered::class);

        $this->listen(PasswordResetRequested::class, NotifyUserWhenPasswordResetRequested::class);

        $this->listen(WareUpdated::class, HandleSpecificCurrenciesWhenWareUpdated::class);
        $this->listen(WareUpdated::class, HandleSpecificCurrenciesCacheWhenWareUpdated::class);

        $this->listen(CartStatusChanged::class, SaveReceiptWhenCartStatusChanged::class);


        $this->listen(CartCheckedOut::class, CartCheckoutUserNotification::class);
        $this->listen(CartCheckedOut::class, CartCheckoutAdminNotification::class);
        $this->listen(CartConfirmed::class, CartConfirmedNotification::class);
        $this->listen(CartStatusChanged::class, CartStatusChangedNotification::class);
        $this->listen(CartCheckoutUndid::class, CartCheckoutUndidNotification::class);
        $this->listen(FailedPayment::class, FailedPaymentNotification::class);
    }



    /**
     * Register schedules.
     */
    protected function registerSchedules()
    {
        $schedules = [
             RemoveNoOwnerCartsSchedule::class,
             RemoveOldAndPendingCartsSchedule::class,
             NotifyForPendingCartsSchedule::class,
             NotifyForUnpaidCartsSchedule::class,
        ];
        foreach ($schedules as $schedule) {
            $this->addSchedule($schedule);
        }
    }



    /**
     * Registers the shipping rules.
     */
    protected function registerShippingRules()
    {
        shipping()->registerRule(StateRule::class);
        shipping()->registerRule(DestinationRule::class);
    }



    /**
     * @param array $arguments
     */
    public static function handleBrowseHeadings($arguments)
    {
        $posttype = $arguments['posttype'];
        $module   = gachet()->module();

        // Js Files
        module("manage")
             ->service("template_assets")
             ->add("drawJs")
             ->link($module->getAlias() . ":js/draw.min.js")
             ->order(60)
        ;
        // Columns
        module('posts')
             ->service('browse_headings')
             ->add($module->getAlias())
             ->trans($module->getTransPath('general.drawing.draw.singular'))
             ->blade($module->getAlias() . "::manage.post.browser.draw-column")
             ->order(31)
             ->condition($posttype->has('event'))
        ;
    }



    /**
     * Handles the posts editor based on this module.
     *
     * @param Post $model
     */
    public static function handlePostEditor(Post $model)
    {
        $module = gachet()->module();

        service('posts:editor_main')
             ->add('gachet-products-after-sale-services')
             ->blade($module->getBladePath('manage.post.editor.after-sale-services'))
             ->condition($model->has('after_sale_services'))
             ->order(40)
        ;

        service('posts:editor_main')
             ->add('gachet-products-catalog')
             ->blade($module->getBladePath('manage.post.editor.catalog'))
             ->condition($model->has('catalog'))
             ->order(41)
        ;
    }



    /**
     * Returns an array of site's locales.
     *
     * @return array
     */
    public function getLocales()
    {
        return $this->module()->getConfig('locales');
    }



    /**
     * Returns a news instance of the `Modules\Gachet\Services\Template\GachetTemplateHandler` class.
     *
     * @return GachetTemplateHandler
     */
    public static function template()
    {
        return new GachetTemplateHandler();
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\CommentServiceProvider` class
     *
     * @return CommentServiceProvider
     */
    public function commenting()
    {
        return new CommentServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\ShopServiceProvider` class
     *
     * @return ShopServiceProvider
     */
    public function shop()
    {
        return new ShopServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\CurrencyServiceProvider` class
     *
     * @return CurrencyServiceProvider
     */
    public function currency()
    {
        return new CurrencyServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\CartServiceProvider` class
     *
     * @return CartServiceProvider
     */
    public function cart()
    {
        return new CartServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\PostServiceProvider` class
     *
     * @return PostServiceProvider
     */
    public function posts()
    {
        return new PostServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\TicketsServiceProvider` class
     *
     * @return TicketsServiceProvider
     */
    public function tickets()
    {
        return new TicketsServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\DrawingCodeServiceProvider` class
     *
     * @return DrawingCodeServiceProvider
     */
    public function draw()
    {
        return new DrawingCodeServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Gachet\Providers\CompareServiceProvider` class
     *
     * @return CompareServiceProvider
     */
    public function compare()
    {
        return new CompareServiceProvider($this->app);
    }



    /**
     * Finds a post with its slug an type.
     *
     * @param string $slug
     * @param string $type
     *
     * @return \App\Models\Post|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public static function findPostWithSlug(string $slug, string $type)
    {
        $post = PostServiceProvider::selectPosts([
             'slug' => $slug,
             'type' => $type,
        ])->first()
        ;

        if (!$post) {
            $post = post();
        }

        return $post;
    }



    /**
     * Finds a commenting post with its slug.
     *
     * @param string $slug
     *
     * @return \App\Models\Post|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    public static function findCommentingPostWithSlug(string $slug)
    {
        return static::findPostWithSlug($slug, 'commenting');
    }



    /**
     * Returns an array for mapping posttypes and their slugs.
     *
     * @return array
     */
    public static function posttypeSlugs()
    {
        return [
             'home-slider' => 'home-sliders',
             'static'      => 'pages',
             'event'       => 'events',
             'product'     => 'products',
             'commenting'  => 'commenting',
             'blog'        => 'blog',
             'news'        => 'news',
             'faq'         => 'faq',
        ];
    }



    /**
     * Returns the slug of the requested posttype.
     *
     * @param string $posttype
     *
     * @return string|null
     */
    public static function getPosttypeSlug(string $posttype)
    {
        return (static::posttypeSlugs()[$posttype] ?? null);
    }



    /**
     * Returns the slug of the category of the sliders to be shown in the home page.
     *
     * @return string
     */
    public static function homeSliderCategorySlug()
    {
        return 'home';
    }



    /**
     * Returns an array of site colors.
     *
     * @return array
     */
    public static function colors()
    {
        return [
             'rgb(86, 197, 208)',
             'rgb(85, 85, 85)',
             'rgb(72, 199, 52)',
             'rgb(227, 79, 76)',
             'rgb(167, 169, 172)',
        ];
    }



    /**
     * Returns a random site color.
     *
     * @return string
     */
    public static function randomColor()
    {
        return array_random(static::colors());
    }
}
