<?php

namespace Modules\Gachet\Providers;

use App\Models\TicketType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\ServiceProvider;
use Modules\Tickets\Providers\TicketsServiceProvider as TicketModuleProvider;

class TicketsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Returns a builder to find ticket types.
     *
     * @return Builder
     */
    public static function ticketTypesBuilder()
    {
        return ticket()->select();
    }



    /**
     * Returns a collection of the ticket types.
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function ticketTypes()
    {
        return static::ticketTypesBuilder()->get();
    }



    /**
     * Returns an array of ticket types for combo.
     *
     * @return array
     */
    public static function ticketTypesCombo()
    {
        return static::ticketTypes()
                     ->map(function (TicketType $type) {
                         return [
                              'value' => $type->hashid,
                              'title' => $type->title,
                         ];
                     })->toArray()
             ;
    }



    /**
     * Returns an array of priorities.
     *
     * @return array
     */
    public static function priorities()
    {
        $flags = ticket()->guessPossibleRemarkFlags();

        return array_filter($flags, function (string $flag) {
            return ends_with($flag, 'priority');
        });
    }



    /**
     * Returns an array of priorities for combo.
     *
     * @return array
     */
    public static function prioritiesCombo()
    {
        $priorities = static::priorities();
        $combo      = [];

        foreach ($priorities as $priority) {
            $combo[$priority] = [
                 'value' => $priority,
                 'title' => TicketModuleProvider::getFlagTitle($priority),
            ];
        }

        return $combo;
    }
}
