<?php

namespace Modules\Gachet\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Modules\Gachet\Services\PostLabels\PostLabels;

class PostLabelsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * boot the provider
     *
     * @param Router $router
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $this->registerPostLabelsSingleton();
    }



    /**
     * Registers the `gachet.posts.labels` singleton.
     */
    protected function registerPostLabelsSingleton()
    {
        $this->app->singleton('gachet.posts.labels', function () {
            return new PostLabels();
        });
    }
}
