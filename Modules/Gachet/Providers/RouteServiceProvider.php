<?php

namespace Modules\Gachet\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Gachet\Services\Module\CurrentModuleHelper;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The List of Posttypes that has static routes (without "archive" prefix)
     *
     * @var array $static_posttypes
     */
    protected static $static_posttypes     = [
         'faq',
    ];
    protected static $controllersNamespace = '\Modules\Gachet\Http\Controllers\\';

    public static $postDirectUrlPrefix = 'P';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Returns the route for the archive of the requested posttype.
     *
     * @param string $posttype_slug
     *
     * @return string
     */
    public static function getPosttypeRoute($posttype_slug)
    {
        if (self::isStaticPosttype($posttype_slug)) {
            return route("gachet.$posttype_slug.archive");
        }

        return route('gachet.posts.list', ['posttype' => $posttype_slug]);
    }



    /**
     * Returns the route for the list of categories of the requested posttype.
     *
     * @param string $posttype_slug
     *
     * @return string
     */
    public static function getPosttypeCategoriesRoute($posttype_slug)
    {
        if (self::isStaticPosttype($posttype_slug)) {
            return route("gachet.$posttype_slug.categories");
        }

        return route('gachet.categories-list', ['posttype' => $posttype_slug]);
    }



    /**
     * Returns the list of posttypes that has static routes (without "archive" prefix)
     *
     * @return array
     */
    public static function getStaticPosttypes()
    {
        return self::$static_posttypes;
    }



    /**
     * Whether the specified posttype is a static posttype.
     *
     * @param string $posttype_slug
     *
     * @return bool
     */
    public static function isStaticPosttype($posttype_slug)
    {
        if (in_array($posttype_slug, self::getStaticPosttypes())) {
            return true;
        } else {
            return false;
        }
    }

    public static function action(string $action, ...$otherParameters)
    {
        return action(self::$controllersNamespace . $action, ...$otherParameters);
    }

    public static function actionLocale(string $action, ...$otherParameters)
    {
        return action_locale(self::$controllersNamespace . $action, ...$otherParameters);
    }
}
