<?php

namespace Modules\Gachet\Providers;

use App\Models\Cart;
use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Mockery\Exception;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected static $session_name                      = 'cart';
    protected static $progressive_couriers_slugs_prefix = 'progressive';



    public static function addToActiveCart(...$parameters)
    {
        $cart = CartServiceProvider::findActiveCart(user(), true);
        return $cart->addOrder(... $parameters);
    }



    /**
     * Finds an returns a proper Cart object for current client.
     *
     * @param User|null $user
     * @param bool      $create
     *
     * @return Cart
     */
    public static function findActiveCart(User $user = null, $create = false)
    {
        if ($user and $user->exists) {
            return self::getUsersCart($user, $create);
        } elseif (!auth()->guest()) {
            return self::getUsersCart(user(), $create);
        } else {
            return self::getSessionCart($create);
        }
    }



    /**
     * Returns the cart object for the specified user.
     *
     * @param User $user
     * @param bool $create
     *
     * @return Cart
     */
    public static function getUsersCart(User $user, $create = false)
    {
        $conditions = array_merge(self::newCartGeneralData(), [
             'user_id'   => $user->id,
             'raised_at' => null,
             'currency'  => gachet()->shop()->usableCurrency(),
             'locale'    => getLocale(),
        ]);

        if ($create) {
            return model('cart')->firstOrCreate($conditions);
        } else {
            return model('cart')->firstOrNew($conditions);
        }
    }



    /**
     * Clears the cart stored in the session.
     *
     * @return void
     */
    public static function clearSession()
    {
        session()->forget(self::$session_name);
    }



    public static function getSessionCart($create = false)
    {
        if (session()->exists(self::$session_name)) {

            $cart_hashid = session()->get(self::$session_name);
            $found_cart  = model('Cart')->grabHashid($cart_hashid);

            if ($found_cart->exists) {
                return $found_cart;
            }
        }

        return static::newCart($create);
    }



    /**
     * Returns a news instance of the Cart model.
     *
     * @param bool      $create If true, the new object will be stored in the DB first.
     * @param User|null $user
     *
     * @return Cart
     */
    protected static function newCart($create, User $user = null)
    {
        $attributes = array_merge(static::newCartGeneralData(), [
             'user_id'  => $user ? $user->id : 0,
             'currency' => gachet()->shop()->usableCurrency(),
             'locale'   => getLocale(),
        ]);

        if ($create) {
            $new_cart = Cart::create($attributes);
            session()->put(static::$session_name, $new_cart->hashid);
            return $new_cart;

        } else {
            return new Cart($attributes);
        }
    }



    /**
     * Returns an array to be used while creating a new cart.
     *
     * @return array
     */
    protected static function newCartGeneralData()
    {
        return [
             'currency' => gachet()->shop()->usableCurrency(),
             'locale'   => getLocale(),
        ];
    }



    /**
     * Reads and returns the cart ID in the session.
     *
     * @return string|null
     */
    public static function getCartIdFromSession()
    {
        if (session()->exists(static::$session_name)) {
            return session()->get(static::$session_name);
        }

        return null;
    }



    /**
     * Checks if the specified cart has any verified transaction or not.
     *
     * @param Cart $cart
     *
     * @return bool
     */
    public static function hasVerifiedTransaction(Cart $cart)
    {
        return boolval(payment()
             ->invoice($cart->id)
             ->status('verified')
             ->count());
    }



    /**
     * Finds and returns the suitable courier for the given cart.
     *
     * @param Cart $cart
     *
     * @return null
     */
    public static function getSuitableCourier(Cart $cart)
    {
        $cart->removeCourier();
        $cart_price = $cart->invoiced_amount;

        $courier_slug = static::findSuitableCourierSlug($cart_price);
        if (!$courier_slug) {
            return null;
        }

        $courier = model('Courier')->grabSlug($courier_slug);
        if (!$courier or $courier->not_exists) {
            return null;
        }

        return $courier;
    }



    /**
     * Finds an returns the slug of the suitable courier for the given price.
     *
     * @param $price
     *
     * @return null|string
     */
    protected static function findSuitableCourierSlug($price)
    {
        $productsPosttype = posttype('products');

        if (
             $levelsStr = $productsPosttype->spreadMeta()->courier_price_levels and
             count($levels = array_filter(preg_split("/\\r\\n|\\r|\\n/", $levelsStr)))
        ) {
            foreach ($levels as $level) {
                try {
                    if (count($levelParts = explode_not_empty(':', $level)) < 2) {
                        continue;
                    }
                    list($range, $slug) = $levelParts;

                    if (count($rangeParts = explode('-', $range)) < 2) {
                        continue;
                    }

                    list($floor, $ceil) = $rangeParts;

                    if ($price > $floor and $price <= $ceil) {
                        return trim($slug);
                    }
                } catch (Exception $exception) {
                    continue;
                }
            }
        }

        return null;
    }
}
