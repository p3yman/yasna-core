<?php

namespace Modules\Gachet\ShippingRules;

use App\Models\Address;
use App\Models\Cart;
use App\Models\State;
use Modules\Manage\Services\Form;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;


class StateRule extends ShippingRule
{
    /** @var string|null */
    protected $province_hashid = null;



    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return module('gachet')->getTrans('general.state-shipping-rule');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $options       = static::getProvinces();
        $value_field   = 'hashid';
        $caption_field = 'title';

        $form->add("selectize")
             ->name("allowed")
             ->label("tr:shipping::rules.allowed")
             ->options($options)
             ->valueField($value_field)
             ->captionField($caption_field)
             ->inForm()
        ;

        $form->add("selectize")
             ->name("not_allowed")
             ->label("tr:shipping::rules.not_allowed")
             ->options($options)
             ->valueField($value_field)
             ->captionField($caption_field)
             ->inForm()
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return $this->isInWhiteList() and !$this->isInBlackList();
    }



    /**
     * validate allowed days
     *
     * @return bool
     */
    protected function isInWhiteList(): bool
    {
        $array = explode_not_empty(',', $this->para['allowed']);

        if(!$array) {
            return true;
        }

        return $this->isInList($array);
    }



    /**
     * validate un-allowed days
     *
     * @return bool
     */
    protected function isInBlackList(): bool
    {
        $array = explode_not_empty(',', $this->para['not_allowed']);

        return $this->isInList($array);
    }



    /**
     * Checks if the province hashid is in the given list.
     *
     * @param array $list
     *
     * @return bool
     */
    protected function isInList(array $list)
    {
        return in_array($this->getInvoiceProvinceHashid(), $list);
    }



    /**
     * Returns the hashid of the province based on the invoice.
     *
     * @return string
     */
    protected function getInvoiceProvinceHashid()
    {
        if (is_null($this->province_hashid)) {
            $address               = $this->getAddress();
            $province_id           = ($address->province->id ?? 0);
            $this->province_hashid = hashid($province_id);
        }


        return $this->province_hashid;
    }



    /**
     * Returns the address instance based on the original instance.
     *
     * @return Address
     */
    protected function getAddress()
    {
        $cart = $this->getCart();

        return model('address', $cart->address_id);
    }



    /**
     * Returns the original invoice instance.
     *
     * @return Cart
     */
    protected function getCart()
    {
        return $this->invoice->getOriginalInstance();
    }



    /**
     * Returns the provinces.
     *
     * @return array
     */
    protected static function getProvinces()
    {
        return model('state')
             ->allProvinces()
             ->get()
             ->map(function (State $state) {
                 return [
                      'hashid' => $state->hashid,
                      'title'  => $state->title,
                 ];
             })->toArray()
             ;
    }

}
