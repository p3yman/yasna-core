<?php

namespace Modules\Gachet\ShippingRules;

use App\Models\Cart;
use Modules\Manage\Services\Form;
use Modules\Shipping\Services\PriceModifier;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;


/**
 * Class FloorPriceRule
 *
 * @deprecated 2019-03-18
 */
class FloorPriceRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return module('gachet')->getTrans('general.floor-shipping-rule');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add("note")
             ->label("tr:gachet::general.floor-shipping-rule-not")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return ($this->invoice->getOriginalInstance() instanceof Cart);
    }



    /**
     * get the final price
     *
     * @return float
     */
    public function getPrice(): float
    {
        $price_per_floor = $this->invoice->getOriginalInstance()->shipping_price_per_floor;
        $floors          = $this->getFloors();

        return ($price_per_floor * $floors);
    }



    /**
     * @inheritdoc
     */
    public function getPriceModifier(): string
    {
        return PriceModifier::ADD_AMOUNT;
    }



    /**
     * Returns the number of floors to be calculated.
     *
     * @return array|int|null
     */
    protected function getFloors()
    {
        $cart    = $this->invoice->getOriginalInstance();
        $address = ($cart->address ?: model('address'));

        return ($address->getMeta('floor') ?? 1);
    }
}
