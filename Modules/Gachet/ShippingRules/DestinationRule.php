<?php

namespace Modules\Gachet\ShippingRules;

use App\Models\Cart;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;
use Modules\Manage\Services\Form;
use Modules\Shipping\Services\PriceModifier;


class DestinationRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return module('gachet')->getTrans('general.destination-shipping-rule');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();


        $form->add("input")
             ->name("distance_price")
             ->label("tr:gachet::general.destination-shipping-price")
             ->inForm()
             ->numberFormat()
             ->purificationRules("ed|numeric")
             ->required()
             ->type("float")
        ;

        $form->add("note")
             ->label("tr:gachet::general.floor-shipping-rule-not")
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return ($this->invoice->getOriginalInstance() instanceof Cart);
    }



    /**
     * get the final price
     *
     * @return float
     */
    public function getPrice(): float
    {
        if ($this->needsDistancePrice()) {
            return $this->getDistancePrice();
        }

        if ($this->needsFloorsPrice()) {
            return $this->getFloorsPrice();
        }

        return 0;
    }



    /**
     * @inheritdoc
     */
    public function getPriceModifier(): string
    {
        return PriceModifier::ADD_AMOUNT;
    }



    /**
     * Checks if the invoice needs the distance price.
     *
     * @return bool
     */
    protected function needsDistancePrice()
    {
        return ($this->getDeliveryMethod() == 'distance-place');
    }



    /**
     * Returns the price based on the distance.
     *
     * @return float
     */
    protected function getDistancePrice(): float
    {
        $distance_price = ($this->para['distance_price'] ?? 0);

        return ((float)$distance_price);
    }



    /**
     * Checks if the invoice needs the floors price.
     *
     * @return bool
     */
    protected function needsFloorsPrice()
    {
        return ($this->getDeliveryMethod() == 'on-floor');
    }



    /**
     * Returns the price based on the floors.
     *
     * @return float
     */
    protected function getFloorsPrice(): float
    {
        $price_per_floor = $this->invoice->getOriginalInstance()->shipping_price_per_floor;
        $floors          = $this->getFloors();

        return ($price_per_floor * $floors);
    }



    /**
     * Returns the number of floors to be calculated.
     *
     * @return int|null
     */
    protected function getFloors()
    {
        $cart    = $this->getCart();
        $address = ($cart->address ?: model('address'));

        return ($address->getMeta('floor') ?? 1);
    }



    /**
     * Returns the delivery method selected by the user.
     *
     * @return string|null
     */
    protected function getDeliveryMethod()
    {
        return $this->getCart()->getMeta('delivery_method');
    }



    /**
     * Returns the original invoice instance.
     *
     * @return Cart
     */
    protected function getCart()
    {
        return $this->invoice->getOriginalInstance();
    }

}
