<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrenciesToWaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wares', function (Blueprint $table) {
            $columns = array_reverse($this->getColumns());

            foreach ($columns as $column) {
                $table
                     ->float($column, 15, 2)
                     ->default(0)
                     ->after('sale_price')
                     ->index()
                ;
            }
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wares', function (Blueprint $table) {
            $table->dropColumn($this->getColumns());
        });
    }



    /**
     * Returns an array of columns to be added to the `wares` tabl.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns    = [];
        $currencies = gachet()->currency()->getSpecificCurrencies();

        foreach ($currencies as $currency) {
            $prefix = strtolower($currency);

            $columns[] = $prefix . '_original_price';
            $columns[] = $prefix . '_sale_price';
        }

        return $columns;
    }
}
