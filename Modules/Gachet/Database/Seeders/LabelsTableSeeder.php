<?php

namespace Modules\Gachet\Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LabelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->labelsMaster();
        $this->labelsSlaves();
    }



    /**
     * Seeds parent labels
     */
    private function labelsMaster()
    {
        $posttypes    = posttype()->having('shop')->get();
        $titles_array = [
             "fa" => trans_safe('gachet::general.labels.color.plural', [], "fa"),
             "en" => trans_safe('gachet::general.labels.color.plural', [], "en"),
             "ar" => trans_safe('gachet::general.labels.color.plural', [], "ar"),
        ];

        foreach ($posttypes as $posttype) {
            ware()->defineLabel($posttype->slug . ware()::LABEL_POSTTYPE_DIVIDER . "colors", $titles_array);
        }
    }



    /**
     * Seeds children label
     */
    private function labelsSlaves()
    {
        $parent_labels = ware()->definedLabels()->where('parent_id', 0)->get();
        foreach ($parent_labels as $parent_label) {
            $this->defineColorLabels($parent_label);
        }
    }



    /**
     * Seeds Colors Labels
     *
     * @param $parent
     */
    public function defineColorLabels($parent)
    {
        $colors = array_keys(config('gachet.colors'));

        foreach ($colors as $color) {
            $titles_array  = [
                 "fa" => trans_safe("gachet::general.colors.$color", [], "fa"),
                 "en" => trans_safe("gachet::general.colors.$color", [], "en"),
                 "ar" => trans_safe("gachet::general.colors.$color", [], "ar"),
            ];
            $posttype_slug = str_before($parent->slug, ware()::LABEL_POSTTYPE_DIVIDER);
            $slug          = $posttype_slug . ware()::LABEL_POSTTYPE_DIVIDER . $color;

            ware()->defineLabel($slug, $titles_array, $parent->id);
        }
    }
}
