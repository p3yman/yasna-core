<?php

namespace Modules\Gachet\Database\Seeders;

use App\Models\Category;
use App\Models\Posttype;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Modules\Yasna\Services\YasnaSeedingTool;

class CategoriesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new YasnaSeedingTool('categories', $this->data(), 'id'))->run();
    }



    /**
     * Returns an array of the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        $basic_data = $this->getCategoriesData();
        $data       = [];

        foreach ($basic_data as $posttype_slug => $categories) {
            $data = array_merge($data, $this->generatePosttypeCategoriesArray($posttype_slug, $categories));
        }

        return $data;
    }



    /**
     * Generates an array of categories of the specified posttype to be seeded.
     *
     * @param string $posttype_slug
     * @param array  $categories
     *
     * @return array
     */
    protected function generatePosttypeCategoriesArray(string $posttype_slug, array $categories)
    {
        $posttype = posttype($posttype_slug);
        $data     = [];

        if ($posttype->not_exists) {
            return [];
        }

        $general_data = [
             'posttype_id' => $posttype->id,
             'locales'     => $this->getLocalesString(),
        ];

        foreach ($categories as $category) {
            if ($this->categoryExisted($posttype->id, $category['slug'])) {
                continue;
            }

            $data[] = array_merge_recursive($general_data, [
                 'slug'        => $category['slug'],
                 'is_folder'   => ($category['is_folder'] ?? 0),
                 'parent_id'   => ($category['parent_id'] ?? 0),
                 'meta-titles' => $this->getTitlesArray($category['trans']),
            ]);
        }

        return $data;
    }



    /**
     * Checks if category with the given posttype id and slug exists.
     *
     * @param int    $posttype_id
     * @param string $slug
     *
     * @return bool
     */
    protected function categoryExisted(int $posttype_id, string $slug)
    {
        return boolval(
             model('category')
                  ->wherePosttypeId($posttype_id)
                  ->whereSlug($slug)
                  ->first()
        );
    }



    /**
     * Returns a string to be inserted in the `locales` column.
     *
     * @return string
     */
    protected function getLocalesString()
    {
        $separator = '|';
        return $separator . implode($separator, gachet()->getLocales()) . $separator;
    }



    /**
     * Returns the array of titles with the specified trans.
     *
     * @param string $trans
     *
     * @return array
     */
    protected function getTitlesArray(string $trans)
    {
        $locales = gachet()->getLocales();
        $result  = [];

        foreach ($locales as $locale) {
            $result[$locale] = $this->runningModule()->getTrans(
                 "seeder.categories.$trans",
                 [],
                 $locale
            )
            ;
        }

        return $result;
    }



    /**
     * Returns an array of categories of the all posttypes.
     *
     * @return array
     */
    protected function getCategoriesData()
    {
        return array_merge_recursive($this->productCategories());
    }



    /**
     * Returns the data to be seeded for categories of the products posttype.
     *
     * @return array
     */
    protected function productCategories()
    {

        $posttype_slug = gachet()->getPosttypeSlug('product');

        return [
             $posttype_slug => [
                  [
                       'slug'      => gachet()->shop()->getBrandsFolderSlug(),
                       'trans'     => 'products-brands',
                       'is_folder' => 1,
                  ],
             ],
        ];
    }
}
