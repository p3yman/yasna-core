<?php

namespace Modules\Gachet\Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Yasna\Providers\YasnaServiceProvider;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        $module = $this->runningModule();

        return [
             [
                  'slug'          => 'maximum_pending_jobs',
                  'title'         => $module->getTrans('seeder.settings.maximum-pending-jobs'),
                  'category'      => 'upstream',
                  'order'         => '92',
                  'data_type'     => 'text',
                  'default_value' => '10',
                  'hint'          => $module->getTrans('seeder.settings.maximum-pending-jobs-help'),
                  'css_class'     => '',
                  'is_localized'  => '',
             ],
             [
                  'slug'          => 'user_registration_send_sms',
                  'title'         => $module->getTrans('seeder.settings.user-registration-send-sms'),
                  'category'      => 'upstream',
                  'order'         => '93',
                  'data_type'     => 'boolean',
                  'default_value' => '0',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '',
             ],
             [
                  'slug'          => 'user_registration_send_email',
                  'title'         => $module->getTrans('seeder.settings.user-registration-send-email'),
                  'category'      => 'upstream',
                  'order'         => '94',
                  'data_type'     => 'boolean',
                  'default_value' => '0',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '',
             ],
             [
                  'slug'          => 'fax',
                  'title'         => $module->getTrans('seeder.settings.fax'),
                  'category'      => 'contact',
                  'order'         => '21',
                  'data_type'     => 'array',
                  'default_value' => '+982122324472',
                  'hint'          => '',
                  'css_class'     => 'ltr',
                  'is_localized'  => '0',

             ],
             [
                  'slug'          => 'yasna_group_title',
                  'title'         => $module->getTrans('seeder.yasna-group-title'),
                  'category'      => 'upstream',
                  'order'         => '33',
                  'data_type'     => 'text',
                  'default_value' => 'YasnaWeb',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '1',
             ],
             [
                  'slug'          => 'yasna_group_url',
                  'title'         => $module->getTrans('seeder.yasna-group-url'),
                  'category'      => 'upstream',
                  'order'         => '33',
                  'data_type'     => 'text',
                  'default_value' => 'http://yasna.team',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => 'minimum_payment',
                  'title'         => $module->getTrans('seeder.minimum_payment'),
                  'category'      => 'template',
                  'order'         => '34',
                  'data_type'     => 'boolean',
                  'default_value' => 'true',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
        ];
    }
}
