<?php

namespace Modules\Gachet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PostsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert($this->safeData());
    }



    /**
     * Generates an array to be used while normalizing array of every record before insert.
     * If normalizing be omitted, it may occurring differences between records' keys and cause SQL error.
     *
     * @param array $data
     *
     * @return array
     */
    protected function generateNormalized(array $data)
    {
        return array_fill_keys(array_keys(array_merge(...$data)), null);
    }



    /**
     * Returns the safe data to be inserted.
     *
     * @return array
     */
    protected function safeData()
    {
        $data       = $this->data();
        $normalizer = $this->generateNormalized($data);

        foreach ($data as $key => $datum) {
            $conditions = array_only($datum, ['slug', 'locale', 'type']);

            if (post()->where($conditions)->first()) {
                unset($data[$key]);
            } else {
                $data[$key] = array_normalize($datum, $normalizer);
            }
        }

        return array_values($data);
    }



    /**
     * Returns all data to be inserted.
     *
     * @return array
     */
    protected function data()
    {
        return array_merge(
             $this->contactPosts(),
             $this->faqPosts(),
             $this->aboutPosts(),
             $this->supportPosts(),
             $this->howToBuyPosts(),
             $this->returnPoliciesPosts()
        );
    }



    /**
     * Return commenting posts for contact form.
     *
     * @return array
     */
    protected function contactPosts()
    {
        $sisterhood  = hashid(time(), 'main');
        $now         = now()->toDateTimeString();
        $common_meta = [
             'fields' => "name*,email*,mobile*,subject*,text*",
             'rules'  => "name=>required,\r\nmobile=>phone:mobile|required,\r\nemail=>required|email,\r\nsubject=>required,\r\ntext=>required",
        ];
        $common      = [
             'type'         => 'commenting',
             'slug'         => 'contact',
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
             'meta'         => json_encode($common_meta),
        ];
        $data        = [];

        foreach ($this->getLocales() as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => $this->runningModule()->getTrans('general.contact-us', [], $locale),
                 'title2' => $this->runningModule()->getTrans('seeder.contact-commenting-title2', [], $locale),
            ]);
        }

        return $data;
    }



    /**
     * Return data of commenting posts for FAQ page.
     *
     * @return array
     */
    protected function faqPosts()
    {
        $sisterhood  = hashid(time(), 'main');
        $now         = now()->toDateTimeString();
        $common_meta = [
             'fields' => "name*,email*,mobile*,question*",
             'rules'  => "name=>required,\r\nmobile=>phone:mobile|required,\r\nemail=>required|email,\r\nquestion=>required",
        ];
        $common      = [
             'type'         => 'commenting',
             'slug'         => 'faq',
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
             'meta'         => json_encode($common_meta),
        ];
        $data        = [];

        foreach ($this->getLocales() as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => $this->runningModule()->getTrans('post.faq.plural', [], $locale),
                 'title2' => $this->runningModule()->getTrans('seeder.faq-commenting-title2', [], $locale),
            ]);
        }

        return $data;
    }



    /**
     * Return `about` posts to be inserted.
     *
     * @return array
     */
    protected function aboutPosts()
    {
        return $this->generateStaticPosts('about', 'general.about-us');
    }



    /**
     * Return `support` posts to be inserted.
     *
     * @return array
     */
    protected function supportPosts()
    {
        return $this->generateStaticPosts('support', 'general.support');
    }



    /**
     * Returns `how-to-buy` posts to be inserted.
     *
     * @return array
     */
    protected function howToBuyPosts()
    {
        return $this->generateStaticPosts('how-to-buy', 'general.how-to-buy');
    }



    /**
     * Returns `return-policies` posts to be inserted.
     *
     * @return array
     */
    protected function returnPoliciesPosts()
    {
        return $this->generateStaticPosts('return-policies', 'general.return-policies');
    }



    /**
     * Generates and returns an array to be inserted for posts with the given slug in all the site's locales.
     *
     * @param string $slug
     * @param string $trans_path
     *
     * @return array
     */
    protected function generateStaticPosts(string $slug, string $trans_path)
    {
        $sisterhood = hashid(time(), 'main');
        $now        = now()->toDateTimeString();
        $common     = [
             'type'         => 'pages',
             'slug'         => $slug,
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
        ];
        $data       = [];

        foreach ($this->getLocales() as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => $this->runningModule()->getTrans($trans_path, [], $locale),
            ]);
        }

        return $data;
    }



    /**
     * Returns the site's locales.
     *
     * @return array
     */
    protected function getLocales()
    {
        return $this
             ->runningModule()
             ->getProvider()
             ->getLocales()
             ;
    }
}
