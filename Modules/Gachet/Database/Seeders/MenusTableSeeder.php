<?php

namespace Modules\Gachet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class MenusTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedArray($this->mainData());
    }



    /**
     * Seeds the main array of the menus.
     *
     * @param array $parents
     */
    protected function seedArray(array $parents)
    {
        foreach ($parents as $parent) {
            $menu_info = ($parent['menu'] ?? $parent);
            $slug      = $menu_info['slug'];
            $submenus  = ($parent['submenus'] ?? []);

            $this->seedData([$menu_info]);
            $this->seedChildren($slug, $submenus);
        }
    }



    /**
     * Seeds children of the menu.
     *
     * @param string $parent_slug
     * @param array  $submenus
     */
    protected function seedChildren(string $parent_slug, array $submenus)
    {
        $data   = [];
        $parent = model('menu', $parent_slug);

        if ($parent->not_exists) {
            return;
        }

        foreach ($submenus as $submenu) {
            $submenu['menu']['parent_id'] = $parent->id;
            $data[]                       = $submenu;
        }


        $this->seedArray($data);
    }



    /**
     * Returns the main data to be seeded.
     *
     * @return array
     */
    protected function mainData()
    {
        $module = $this->runningModule();

        return [
             [
                  'menu'     => [
                       "slug"    => "footer",
                       "order"   => 2,
                       "locales" => $this->getLocalesValue(),
                       "titles"  => json_encode([
                            "fa" => $module->getTrans('seeder.menus.footer'),
                       ]),
                       "meta"    => json_encode([
                            "settings" => [
                                 "icons"        => "2",
                                 "links"        => "0",
                                 "label"        => "2",
                                 "style"        => "2",
                                 "description"  => "2",
                                 "condition"    => "2",
                                 "target"       => "2",
                                 "classAtr"     => "2",
                                 "idAtr"        => "2",
                                 "color"        => "2",
                                 "html"         => "2",
                                 "onClick"      => "2",
                                 "onDblClick"   => "2",
                                 "onMouseEnter" => "2",
                                 "onMouseLeave" => "2",
                                 "onMouseOver"  => "2",
                            ],
                       ]),
                  ],
                  'submenus' => [
                       [
                            "menu" => [
                                 "slug"    => "footer-home",
                                 "order"   => 1,
                                 "locales" => $this->getLocalesValue(),
                                 "titles"  => json_encode([
                                      "fa" => $module->getTrans('general.home-page'),
                                 ]),
                                 "links"   => json_encode([
                                      "fa" => route('gachet.home', [], false),
                                 ]),
                                 "meta"    => json_encode([]),
                            ],
                       ],

                       [
                            "menu" => [
                                 "slug"    => "footer-about",
                                 "order"   => 2,
                                 "locales" => $this->getLocalesValue(),
                                 "titles"  => json_encode([
                                      "fa" => $module->getTrans('general.about-us'),
                                 ]),
                                 "links"   => json_encode([
                                      "fa" => route('gachet.page', ['page' => 'about'], false),
                                 ]),
                                 "meta"    => json_encode([]),
                            ],
                       ],

                       [
                            "menu" => [
                                 "slug"    => "footer-support",
                                 "order"   => 3,
                                 "locales" => $this->getLocalesValue(),
                                 "titles"  => json_encode([
                                      "fa" => $module->getTrans('general.support'),
                                 ]),
                                 "links"   => json_encode([
                                      "fa" => route('gachet.page', ['page' => 'support'], false),
                                 ]),
                                 "meta"    => json_encode([]),
                            ],
                       ],

                       [
                            "menu" => [
                                 "slug"    => "footer-how-to-buy",
                                 "order"   => 4,
                                 "locales" => $this->getLocalesValue(),
                                 "titles"  => json_encode([
                                      "fa" => $module->getTrans('general.how-to-buy'),
                                 ]),
                                 "links"   => json_encode([
                                      "fa" => route('gachet.page', ['page' => 'how-to-buy'], false),
                                 ]),
                                 "meta"    => json_encode([]),
                            ],
                       ],

                       [
                            "menu" => [
                                 "slug"    => "footer-return-policies",
                                 "order"   => 4,
                                 "locales" => $this->getLocalesValue(),
                                 "titles"  => json_encode([
                                      "fa" => $module->getTrans('general.return-policies'),
                                 ]),
                                 "links"   => json_encode([
                                      "fa" => route('gachet.page', ['page' => 'return-policies'], false),
                                 ]),
                                 "meta"    => json_encode([]),
                            ],
                       ],
                  ],
             ],
        ];
    }



    /**
     * Returns the language of the menus.
     *
     * @return string
     */
    protected function getLocalesValue()
    {
        return 'fa';
    }



    /**
     * Seeds the given data.
     *
     * @param array $data
     */
    protected function seedData(array $data)
    {
        yasna()->seed('menus', $data);
    }
}
