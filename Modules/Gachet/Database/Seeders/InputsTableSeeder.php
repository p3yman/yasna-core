<?php

namespace Modules\Gachet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class InputsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('inputs', $this->data());

        $this->seedRelations();
    }



    /**
     * Returns an array to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        $module = $this->runningModule();

        return [


             [
                  'slug'              => 'fields',
                  'title'             => $module->getTrans('seeder.fields'),
                  'order'             => '52',
                  'type'              => 'editor',
                  'data_type'         => 'textarea',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',

             ],

             [
                  'slug'              => 'rules',
                  'title'             => $module->getTrans('seeder.rules'),
                  'order'             => '53',
                  'type'              => 'editor',
                  'data_type'         => 'textarea',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],

             [
                  'slug'              => 'category_image_upload_configs',
                  'title'             => $module->getTrans('seeder.category-image-upload-config'),
                  'order'             => '54',
                  'type'              => 'upstream',
                  'data_type'         => 'combo',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => 'filemanager::getConfigsCombo',
             ],

             [
                  'slug'              => 'min_payable_amount',
                  'title'             => $module->getTrans('seeder.min-payable-amount'),
                  'order'             => '55',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => 'numeric|min:1',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => $module->getTrans('seeder.min-payable-amount-help'),
                  'meta-options'      => '',
             ],

             [
                  'slug'              => 'shipping_price_per_floor',
                  'title'             => $module->getTrans('seeder.shipping-price-per-floor'),
                  'order'             => '56',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => 'numeric',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => $module->getTrans('seeder.shipping-price-per-floor-help'),
                  'meta-options'      => '',
             ],

             [
                  'slug'              => 'catalogs_upload_configs',
                  'title'             => $module->getTrans('seeder.catalogs-upload-config'),
                  'order'             => '57',
                  'type'              => 'upstream',
                  'data_type'         => 'combo',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => 'filemanager::getConfigsCombo',
             ],

             [
                  'slug'              => 'point_rate',
                  'title'             => $module->getTrans('seeder.point-rate'),
                  'order'             => '58',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => 'required|numeric|min:1',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
             [
                  'slug'              => 'on_sale',
                  'title'             => $module->getTrans('seeder.on_sale'),
                  'order'             => '59',
                  'type'              => 'editor',
                  'data_type'         => 'boolean',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
        ];
    }



    /**
     * Seeds the relation between features and inputs.
     */
    protected function seedRelations()
    {
        $data = $this->relationsData();
        foreach ($data as $feature_slug => $inputs_slug) {
            $feature = model('feature', $feature_slug);

            if ($feature->not_exists) {
                continue;
            }


            $feature->attachInputs($inputs_slug);
        }
    }



    /**
     * Returns the data for the relation between features and inputs.
     *
     * @return array
     */
    protected function relationsData()
    {
        return [
             'event' => [
                  'point_rate',
             ],
        ];
    }
}
