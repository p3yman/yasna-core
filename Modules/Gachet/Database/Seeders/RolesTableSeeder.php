<?php

namespace Modules\Gachet\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class RolesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('roles', $this->data());
    }



    /**
     * Returns the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        $module = $this->runningModule();

        return [
             [
                  'slug'         => "customer",
                  'title'        => $module->getTrans('seeder.roles.customer-singular'),
                  'plural_title' => $module->getTrans('seeder.roles.customer-plural'),
                  'created_at'   => now()->toDateTimeString(),
                  'is_admin'     => "0",
             ],

             [
                  'slug'         => "vip",
                  'title'        => $module->getTrans('seeder.roles.vip-singular'),
                  'plural_title' => $module->getTrans('seeder.roles.vip-plural'),
                  'created_at'   => now()->toDateTimeString(),
                  'is_admin'     => "0",
             ],

             [
                  'slug'         => "coworker",
                  'title'        => $module->getTrans('seeder.roles.coworker-singular'),
                  'plural_title' => $module->getTrans('seeder.roles.coworker-plural'),
                  'created_at'   => now()->toDateTimeString(),
                  'is_admin'     => "0",
             ],
        ];

    }
}
