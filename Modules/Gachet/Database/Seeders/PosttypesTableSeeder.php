<?php

namespace Modules\Gachet\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Filemanager\Services\Seeder\PosttypeSeedingTool;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PosttypesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        (new PosttypeSeedingTool)->seedUploadConfigs($this->uploadConfigsData());
        (new PosttypeSeedingTool)->seedPosttypes($this->posttypesData());
    }



    /**
     * Returns an array of upload configs data.
     *
     * @return array
     */
    protected function uploadConfigsData()
    {
        return [
             [
                  'slug'          => 'news-featured-image',
                  'title'         => $this->runningModule()->getTrans('seeder.featured-image-of', [
                       'title' => $this->runningModule()->getTrans('seeder.news-plural'),
                  ]),
                  'category'      => '',
                  'order'         => '20',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "news-featured-image.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],

             [
                  'slug'          => 'blog-featured-image',
                  'title'         => $this->runningModule()->getTrans('seeder.featured-image-of', [
                       'title' => $this->runningModule()->getTrans('seeder.blog-plural'),
                  ]),
                  'category'      => '',
                  'order'         => '20',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "blog-featured-image.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],

             [
                  'slug'          => 'slider-featured-image',
                  'title'         => $this->runningModule()->getTrans('seeder.featured-image-of', [
                       'title' => $this->runningModule()->getTrans('seeder.sliders-plural'),
                  ]),
                  'category'      => '',
                  'order'         => '20',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "slider-featured-image.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],

             [
                  'slug'          => 'products-category-image',
                  'title'         => $this->runningModule()->getTrans('seeder.category-image-of', [
                       'title' => $this->runningModule()->getTrans('seeder.products-plural'),
                  ]),
                  'category'      => '',
                  'order'         => '20',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "products-category-image.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],

             [
                  'slug'          => 'products-featured-image',
                  'title'         => $this->runningModule()->getTrans('seeder.featured-image-of', [
                       'title' => $this->runningModule()->getTrans('seeder.products-plural'),
                  ]),
                  'category'      => '',
                  'order'         => '21',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "products-featured-image.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],

             [
                  'slug'          => 'products-attachments',
                  'title'         => $this->runningModule()->getTrans('seeder.attachments-of', [
                       'title' => $this->runningModule()->getTrans('seeder.products-plural'),
                  ]),
                  'category'      => '',
                  'order'         => '22',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "products-attachments.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],

             [
                  'slug'          => 'products-catalog',
                  'title'         => $this->runningModule()->getTrans('seeder.catalogs-of', [
                       'title' => $this->runningModule()->getTrans('seeder.products-plural'),
                  ]),
                  'category'      => '',
                  'order'         => '22',
                  'data_type'     => '',
                  'default_value' => implode(DIRECTORY_SEPARATOR, [
                       $this->runningModule()->getPath(),
                       'Resources',
                       'json',
                       'upload-config',
                       "products-catalog.json",
                  ]),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
        ];
    }



    /**
     * Returns an array of posttypes data.
     *
     * @return array
     */
    protected function posttypesData()
    {
        return [
             [
                  'posttype' => [
                       'slug'           => gachet()->getPosttypeSlug('static'),
                       'title'          => $this->runningModule()->getTrans('seeder.pages-plural'),
                       'singular_title' => $this->runningModule()->getTrans('seeder.pages-singular'),
                       'template'       => "post",
                       'icon'           => "file-o",
                       'order'          => "2",
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'locales',
                       'text',
                       'single_view',
                       'searchable',
                       'slug',
                       'manage_sidebar',
                  ],
             ],
             [
                  'posttype'       => [
                       'slug'           => gachet()->getPosttypeSlug('home-slider'),
                       'title'          => $this->runningModule()->getTrans('seeder.home-sliders-plural'),
                       'singular_title' => $this->runningModule()->getTrans('seeder.home-sliders-singular'),
                       'template'       => "special",
                       'icon'           => "image",
                       'order'          => "1",
                       'locales'        => 'fa',
                  ],
                  'features'       => [
                       'category',
                       'featured_image',
                       'title2',
                       'abstract',
                       'slideshow',
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => 'slider-featured-image',
                  ],
             ],

             [
                  'posttype'       => [
                       'slug'                         => gachet()->getPosttypeSlug('product'),
                       'title'                        => $this->runningModule()->getTrans('seeder.products-plural'),
                       'order'                        => '5',
                       'singular_title'               => $this->runningModule()->getTrans('seeder.products-singular'),
                       'template'                     => 'product',
                       'icon'                         => 'shopping-basket',
                       'meta-currencies'              => implode(',', gachet()->currency()->getAllCurrencies()),
                       'meta-commenting_receive'      => 1,
                       'meta-commenting_display'      => 1,
                       'meta-commenting_allow_guests' => 1,
                       'meta-commenting_enter_email'  => 'must',
                       'meta-commenting_enter_mobile' => 'must',
                  ],
                  'features'       => [
                       'text',
                       'abstract',
                       'shop',
                       'featured_image',
                       'attachments',
                       'category',
                       'category_image',
                       'list_view',
                       'single_view',
                       'manage_sidebar',
                       'mappable',
                       'tag',
                       'commenting',
                       'specs',
                       'related_posts',
                       'after_sale_services',
                       'catalog',
                  ],
                  'inputs'         => [
                       'category_image_upload_configs',
                       'min_payable_amount',
                       'shipping_price_per_floor',
                       'catalogs_upload_configs',
                       'on_sale'
                  ],
                  'upload-configs' => [
                       'category_image_upload_configs' => 'products-category-image',
                       'featured_image_upload_configs' => 'products-featured-image',
                       'attachments_upload_configs'    => 'products-attachments',
                       'catalogs_upload_configs'    => 'products-catalog',
                  ],
             ],

             [
                  'posttype'       => [
                       'slug'           => gachet()->getPosttypeSlug('news'),
                       'title'          => $this->runningModule()->getTrans('seeder.news-plural'),
                       'order'          => '6',
                       'singular_title' => $this->runningModule()->getTrans('seeder.news-singular'),
                       'template'       => 'post',
                       'icon'           => 'list',
                  ],
                  'features'       => [
                       'manage_sidebar',
                       'text',
                       'abstract',
                       'featured_image',
                       'single_view',
                       'list_view',
                       'category',
                       'tag',
                       'mappable',
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => 'news-featured-image',
                  ],
             ],

             [
                  'posttype'       => [
                       'slug'           => gachet()->getPosttypeSlug('blog'),
                       'title'          => $this->runningModule()->getTrans('seeder.blog-plural'),
                       'order'          => '7',
                       'singular_title' => $this->runningModule()->getTrans('seeder.blog-singular'),
                       'template'       => 'post',
                       'icon'           => 'list',
                  ],
                  'features'       => [
                       'manage_sidebar',
                       'text',
                       'abstract',
                       'featured_image',
                       'single_view',
                       'list_view',
                       'category',
                       'tag',
                       'mappable',
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => 'blog-featured-image',
                  ],
             ],
             [

                  'posttype' => [
                       'slug'           => gachet()->getPosttypeSlug('commenting'),
                       'order'          => '8',
                       'title'          => $this->runningModule()->getTrans('seeder.commenting-plural'),
                       'singular_title' => $this->runningModule()->getTrans('seeder.commenting-singular'),
                       'template'       => 'special',
                       'icon'           => 'commenting',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       "commenting",
                       "text",
                       "manage_sidebar",
                       "title2",
                  ],
                  'inputs'   => [
                       "fields",
                       "rules",
                  ],
             ],

             [
                  'posttype' => [
                       'slug'           => gachet()->getPosttypeSlug('faq'),
                       'order'          => '9',
                       'title'          => $this->runningModule()->getTrans('seeder.faq-plural'),
                       'singular_title' => $this->runningModule()->getTrans('seeder.faq-singular'),
                       'template'       => 'faq',
                       'icon'           => 'question-circle',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       "locales",
                       "manage_sidebar",
                       "long_title",
                       "abstract",
                       "list_view",
                  ],
             ],
        ];
    }
}
