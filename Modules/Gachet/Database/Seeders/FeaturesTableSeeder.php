<?php

namespace Modules\Gachet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class FeaturesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->data());
    }



    /**
     * Returns an array of the data to be seeded.
     *
     * @return array
     */
    public function data()
    {
        return [
             [
                  'slug'      => 'after_sale_services',
                  'title'     => $this->runningModule()->getTrans('seeder.after-sale-services'),
                  'order'     => '1',
                  'icon'      => 'wrench',
                  'post_meta' => '',
                  'meta-hint' => '',
             ],

             [
                  'slug'      => 'catalog',
                  'title'     => $this->runningModule()->getTrans('seeder.catalog'),
                  'order'     => '1',
                  'icon'      => 'file',
                  'post_meta' => '',
                  'meta-hint' => '',
             ],
        ];
    }
}
