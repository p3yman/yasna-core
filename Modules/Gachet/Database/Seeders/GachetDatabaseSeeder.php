<?php

namespace Modules\Gachet\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class GachetDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(RolesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(InputsTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
        $this->call(PosttypesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(LabelsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(PostsTableSeeder::class);
    }
}
