<?php

namespace Modules\Gachet\Entities\Traits;

use App\Models\UploaderFile;

trait LabelGachetTrait
{
    /**
     * Returns a link to the post label image.
     *
     * @param string|null $version If `null`, the original version will be assumed.
     *
     * @return string|null
     */
    public function getPostLabelImageLink(?string $version = null)
    {
        if ($version) {
            return $this->getPostLabelImageVersionLink($version);
        } else {
            return $this->getPostLabelOriginalImageLink();
        }
    }



    /**
     * Returns a link to the original version of the post label image.
     *
     * @return string|null
     */
    public function getPostLabelOriginalImageLink()
    {
        $file = $this->getPostLabelImage();

        return $file->exists ? $file->getLink() : null;
    }



    /**
     * Returns a link to the specified version of the post label image.
     *
     * @param string $version
     *
     * @return string|null
     */
    public function getPostLabelImageVersionLink(string $version)
    {
        $file = $this->getPostLabelImage();

        return $file->exists ? $file->getVersionLink($version) : null;
    }



    /**
     * Returns the instance of the post label image.
     *
     * @return UploaderFile
     */
    public function getPostLabelImage()
    {
        if ($this->model_name != post()->getClassName()) {
            return uploader()->file();
        }

        $custom_meta = $this->getMeta('custom');
        $hashid      = ($custom_meta['pic'][0] ?? null);

        return uploader()->file($hashid);
    }
}
