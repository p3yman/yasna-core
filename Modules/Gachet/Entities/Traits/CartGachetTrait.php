<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 01/01/2018
 * Time: 12:53 PM
 */

namespace Modules\Gachet\Entities\Traits;

use App\Models\Order;
use App\Models\Receipt;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Payment\Services\PaymentHandler\Search\SearchBuilder;
use Modules\Gachet\Entities\Traits\CalculatePriceDeliveryTrait;

trait CartGachetTrait
{
    /**
     * Returns the minimum payable amount.
     *
     * @return int|float
     */
    public function minimumPayableAmount()
    {
        return $this->orders->map(function (Order $order) {
            return $order->minimum_payable_amount;
        })->sum()
             ;
    }



    /**
     * Accessor for the `minimumPayableAmount()` Relation
     *
     * @return float|int
     */
    public function getMinimumPayableAmountAttribute()
    {
        return $this->minimumPayableAmount();
    }



    /**
     * Accessor for the `minimumPayableAmount()` Relation
     *
     * @return float|int
     */
    public function getMinPayableAmountAttribute()
    {
        return $this->minimumPayableAmount();
    }



    /**
     * Returns the shipping price per floor.
     *
     * @return float|int
     */
    public function shippingPricePerFloor()
    {
        return $this->orders->map(function (Order $order) {
            return $order->shipping_price_per_floor;
        })->sum()
             ;
    }



    /**
     * Accessor for the `shippingPricePerFloor()` Relation.
     *
     * @return float|int
     */
    public function getShippingPricePerFloorAttribute()
    {
        return $this->shippingPricePerFloor();
    }



    /**
     * One-to-Many Relationship with receipts.
     *
     * @return HasMany
     */
    public function receipts()
    {
        return $this->hasMany(Receipt::class, 'invoice_id');
    }



    /**
     * define delivery meta field
     *
     * @return array
     */
    public function deliveryMetaFields()
    {
        return [
             'delivery_method',
        ];
    }



    /**
     * define delivery meta field
     *
     * @return array
     */
    public function factorMetaFields()
    {
        return [
             'factor_request',
        ];
    }



    /**
     * define delivery_time meta field
     *
     * @return array
     */
    public function deliveryTimeMetaFields()
    {
        return [
             'delivery_time',
        ];
    }



    /**
     * define work_day meta field
     *
     * @return array
     */
    public function workDayMetaFields()
    {
        return [
             'work_day',
        ];
    }



    /**
     * define work_day meta field
     *
     * @return array
     */
    public function descriptionMetaFields()
    {
        return [
             'description',
        ];
    }



    /**
     * define delivery_price meta field
     *
     * @return array
     */
    public function deliveryPriceMetaFields()
    {
        return [
             'heavy_delivery_price',
        ];
    }
}
