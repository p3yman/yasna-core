<?php
/**
 * Created by PhpStorm.
 * User: Yasna-PC1
 * Date: 29/10/2017
 * Time: 03:11 PM
 */

namespace Modules\Gachet\Entities\Traits;

use App\Models\Comment;
use App\Models\Drawing;
use App\Models\Label;
use App\Models\Receipt;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Providers\RouteServiceProvider;
use Modules\Gachet\Services\Module\CurrentModuleHelper;
use Modules\Gachet\Services\PostLabels\PostLabels;

trait PostGachetTrait
{
    protected $cachedReceipts;



    /**
     * @return mixed
     */
    public function receipts()
    {
        return Receipt::whereBetween('purchased_at', [$this->event_starts_at, $this->event_ends_at]);
    }



    /**
     * Returns the similar posts.
     *
     * @param int|null $number if `null`, a builder will be returned.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder|\Illuminate\Support\Collection
     */
    public function similars(?int $number = null)
    {
        $this->spreadMeta();
        $select_array = [
             'type'       => $this->type, // similar post type
             'conditions' => [
                  ['id', '<>', $this->id],        // not self post
             ],
        ];

        // similar categories
        /** @var Collection $labels */
        $labels               = $this->labels;
        $current_labels_array = $labels->pluck('id')->toArray();

        $labels_array = [];
        if ($labels->count()) {
            /** @var Label $label */
            foreach ($labels as $label) {
                $children     = $label->children()->pluck("id")->toArray();
                $should_write = true;
                foreach ($children as $child) {

                    if (in_array($child, $current_labels_array)) {
                        $should_write = false;
                        break;
                    }

                    $should_write = true;
                }

                if ($should_write && $label->parent()->model_name === 'Post') {
                    $labels_array[] = $label->slug;
                }

            }

            $select_array['label'] = $labels_array;
        }

        // check availability for "products"
        if ($this->has('price')) {
            $select_array[] = ['is_available' => true];
        }

        if ($number and is_int($number)) {
            $select_array['limit'] = $number;

            return PostServiceProvider::collectPosts($select_array);
        }

        return PostServiceProvider::selectPosts($select_array);
    }



    /**
     * @return bool
     */
    public function isDrawingReady()
    {
        return Drawing::isReady($this->id);
    }



    /**
     * Prepare post for drawing.
     */
    public function prepareForDrawing()
    {
        return Drawing::prepareDatabase($this);
    }



    /**
     * @param $switch
     * @param $table
     *
     * @return mixed
     */
    public function selector_folder($switch, $table)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if (!isset($switch['folder']) or !$switch['folder']) {
            return $table;
        }

        /*-----------------------------------------------
        | Process ...
        */
        $category = $switch['folder'];

        if (!is_array($category)) {
            $category = [$category];
        }

        if (is_array($category) and count($category)) {
            $table = $table->whereHas('categories', function ($query) use ($category) {
                $query->join('categories as folders', 'categories.parent_id', 'folders.id');
                $query->where('categories.is_folder', 0);
                $query->where('folders.is_folder', 0);
                $query->whereIn('folders.id', $category)
                      ->orWhereIn('folders.slug', $category)
                ;
            });
        }

        /*-----------------------------------------------
        | Return ...
        */

        return $table;
    }



    /**
     * @param $switch
     *
     * @return bool
     */
    public function isIt($switch)
    {
        $switch = strtoupper($switch);
        switch ($switch) {
            case 'NEW':
                if (
                     $this->published_at and
                     ($freshnessSetting = get_setting('products_freshness_time')) and
                     (intval($freshnessSetting) == $freshnessSetting) and
                     Carbon::now()->subMinutes($freshnessSetting)->lessThan(Carbon::parse($this->published_at))
                ) {
                    return true;
                } else {
                    return false;
                }

                break;

            case 'IN_SALE':
                if (!$this->isIt('AVAILABLE')) {
                    break;
                }
                $this->spreadMeta();
                if ($this->sale_price and ($this->sale_price != $this->price)) {
                    return true;
                }
                break;

            case 'AVAILABLE':
                // @todo: make sure to check availability properly
                return (is_null($this->is_available) or $this->is_available);
                break;
        }

        return false;
    }



    /**
     * @return bool
     */
    public function canReceiveComments()
    {
        $this->spreadMeta();
        if ((user()->exists or $this->allow_anonymous_comment) and
             (!$this->disable_receiving_comments)
        ) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return string
     */
    public function directUrl()
    {
        $specific_method = studly_case($this->type) . 'DirectUrl';

        if (method_exists($this, $specific_method)) {
            return $this->$specific_method();
        } else {
            return $this->generalDirectUrl();
        }
    }



    /**
     * @return string
     */
    public function generalDirectUrl()
    {
        $action = module('gachet')->getControllersNamespace() . "\\PostController@single";

        return action($action, [
             'identifier' => RouteServiceProvider::$postDirectUrlPrefix . $this->hashid,
             'title'      => urlHyphen($this->title),
        ]);
    }



    /**
     * @return string
     */
    public function blogDirectUrl()
    {
        $this->spreadMeta();
        if ($this->redirect_to_source_link and $this->source_link) {
            return $this->source_link;
        } else {
            return $this->generalDirectUrl();
        }
    }



    /**
     * @return array
     */
    public function getWinnersArrayAttribute()
    {
        $winners = $this->getMeta('winners');
        if (!$winners or !is_array($winners)) {
            $winners = [];
        }

        return $winners;
    }



    /**
     * @return mixed
     */
    public function getReceiptsAttribute()
    {
        if (is_null($this->cachedReceipts)) {
            $this->cachedReceipts = $this->receipts()->get();
        }
        return $this->cachedReceipts;
    }



    public function getTotalReceiptsCountAttribute()
    {
        return $this->receipts->count();
    }



    /**
     * @return mixed
     */
    public function getTotalReceiptsAmountAttribute()
    {
        return $this->receipts->sum('purchased_amount');
    }



    /**
     * @return mixed
     */
    public function getCurrentPriceAttribute()
    {
        if ($this->isIt('IN_SALE')) {
            return $this->sale_price;
        } else {
            return $this->original_price;
        }
    }



    /**
     * @return string
     */
    public function getDirectUrlAttribute()
    {
        return $this->directUrl();
    }



    /**
     * @return null|string
     */
    public function getTextColorAttribute()
    {
        if ($original = $this->getMeta('text_color')) {
            if (!starts_with($original, '#') and ((strlen($original) == 3) or (strlen($original) == 6))) {
                return '#' . $original;
            }

            return $original;
        }

        return null;
    }



    /**
     * Returns a builder to find categories which are not brand.
     *
     * @return BelongsToMany
     */
    public function gachetCategories()
    {
        return $this->labels();
    }



    /**
     * Accessor for the `gachetCategories()` Relation
     *
     * @return Collection
     */
    public function getGachetCategoriesAttribute()
    {
        return $this->getRelationValue('gachetCategories');
    }



    /**
     * Returns a builder to access brands of the post.
     *
     * @return BelongsToMany
     */
    public function brands()
    {
        $posttype         = $this->posttype;
        $reserved_folders = $posttype->reservedFolders()->pluck('id')->toArray();

        return $this
             ->categories()
             ->whereIn('parent_id', $reserved_folders)
             ;
    }



    /**
     * Accessor for the `brands()` Relation.
     *
     * @return Collection
     */
    public function getBrandsAttribute()
    {
        return $this->getRelationValue('brands');
    }



    /**
     * Returns an array of meta fields based on the after-sale services feature.
     *
     * @return array
     */
    public function afterSaleServicesMetaFields()
    {
        if ($this->has('after_sale_services')) {
            return ['after_sale_services'];
        } else {
            return [];
        }
    }
}
