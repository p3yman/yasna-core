<?php

namespace Modules\Gachet\Entities\Traits;

trait TagGachetTrait
{
    /**
     * return the direct url of tag.
     *
     * @return string
     */
    public function directUrl(): string
    {
        return route('gachet.tag', [
             'tag' => urlencode($this->slug),
        ]);
    }



    /**
     * Accessor for the `directUrl()` method.
     *
     * @return string
     */
    public function getDirectUrlAttribute()
    {
        return $this->directUrl();
    }
}
