<?php

namespace Modules\Gachet\Entities\Traits;

use App\Models\Posttype;

trait CategoryGachetTrait
{
    /**
     * Returns the direct URL of the category.
     *
     * @return null|string
     */
    public function getDirectUrl()
    {
        $posttype = $this->posttype;

        if (!$posttype or $posttype->not_exists or $posttype->hasNot('list_view')) {
            return null;
        }

        return route('gachet.posts.list', [
             'posttype' => ($posttype->slug ?? ''),
             'category' => $this->slug,
        ]);
    }



    /**
     * Accessor for the `getDirectUrl()` Method
     *
     * @return null|string
     */
    public function getDirectUrlAttribute()
    {
        return $this->getDirectUrl();
    }



    /**
     * Returns a proper URL based on the given locale.
     *
     * @param string      $locale  The Locale of the Image to Read
     * @param null|string $default The Default URL to be returned if no image was available
     * @param array       $config  An Array of Configs to be called on the file object
     *
     * @return null|string
     */
    public function getImageUrlIn(string $locale, ?string $default = null, array $config = [])
    {
        $image_hashid = $this->imageIn($locale);

        if (!$image_hashid) {
            return $default;
        }

        $file_obj = fileManager()->file($image_hashid);

        foreach ($config as $method => $parameter) {
            $file_obj->$method($parameter);
        }

        return ($file_obj->getUrl() ?: $default);
    }
}
