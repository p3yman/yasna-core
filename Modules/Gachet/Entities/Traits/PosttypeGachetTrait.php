<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 12/11/2017
 * Time: 01:45 PM
 */

namespace Modules\Gachet\Entities\Traits;

use App\Models\Label;
use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Gachet\Providers\RouteServiceProvider;
use Modules\Gachet\Services\Products\Products;

trait PosttypeGachetTrait
{
    /**
     * Returns the URL for the list view of each posttype.
     *
     * @return string
     */
    public function getListUrlAttribute()
    {
        return RouteServiceProvider::getPosttypeRoute($this->slug);
    }



    /**
     * Returns an array containing the map of posttype and their reserved folders' slugs.
     *
     * @return array
     */
    protected static function reservedFoldersSlugsMap()
    {
        return [
             gachet()->getPosttypeSlug('product') => [
                  gachet()->shop()->getBrandsFolderSlug(),
             ],
        ];
    }



    /**
     * Returns an array of slugs of the reserved folders.
     *
     * @return array
     */
    public function reservedFoldersSlugs()
    {
        $map = $this->reservedFoldersSlugsMap();

        return ($map[$this->slug] ?? []);
    }



    /**
     * Returns a builder to find folders which are proper to use in the Gachet website.
     *
     * @return HasMany
     */
    public function gachetFolders()
    {
        return $this->folders()
                    ->whereNotIn('slug', $this->reservedFoldersSlugs())
             ;
    }



    /**
     * Accessor for the `gachetFolders()` Relation.
     *
     * @return Collection
     */
    public function getGachetFoldersAttribute()
    {
        return $this->getRelationValue('gachetFolders');
    }



    /**
     * Returns a builder to find reserved folders.
     *
     * @return BelongsToMany
     */
    public function reservedFolders()
    {
        return $this->folders()
                    ->whereIn('slug', $this->reservedFoldersSlugs())
             ;
    }



    /**
     * Accessor for the `reservedFolders()` Relation.
     *
     * @return Collection
     */
    public function getReservedFoldersAttribute()
    {
        return $this->getRelationValue('reservedFolders');
    }



    /**
     * Returns a builder to find brands of posttype.
     *
     * @return Builder
     */
    public function brands()
    {
        /** @var Products $products */
        $products = app('gachet.shop.products');
        return $products
             ->brandsLabelFolder()
             ->children()
             ;
    }



    /**
     * Returns a collection of the brands of the posttype.
     *
     * @return Collection
     */
    public function getBrandsAttribute()
    {
        return $this->brands()->get();
    }



    /**
     * Returns the URL to the list of the posts with the specified label.
     *
     * @param Label $label
     *
     * @return string|null
     */
    public function getPostLabelUrl(Label $label)
    {
        if ($this->hasnot('list_view') or !$label->definedFor(Post::class)) {
            return null;
        }

        return route('gachet.posts.list', [
             'posttype' => ($this->slug ?? ''),
             'category' => $label->slug,
        ]);
    }
}
