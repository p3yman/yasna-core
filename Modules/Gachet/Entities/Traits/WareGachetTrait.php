<?php

namespace Modules\Gachet\Entities\Traits;

trait WareGachetTrait
{
    /**
     * Returns a version of the price of the ware proper for the Gachet website.
     *
     * @param string $currency
     * @param string $type
     *
     * @return float
     */
    public function getGachetPrice(string $currency = 'IRR', string $type = 'original')
    {
        return
             $this
                  ->in(getLocale())
                  ->price($type)
                  ->in($currency)
                  ->get()
             ;
    }



    /**
     * Returns a version of the original price of the ware proper for the Gachet website.
     *
     * @param string $currency
     *
     * @return float
     */
    public function getGachetOriginalPrice(string $currency = 'IRR')
    {
        return $this->getGachetPrice($currency, 'original');
    }



    /**
     * Returns a version of the sale price of the ware proper for the Gachet website.
     *
     * @param string $currency
     *
     * @return float
     */
    public function getGachetSalePrice(string $currency = 'IRR')
    {
        return $this->getGachetPrice($currency, 'sale');
    }



    /**
     * Returns a version of the price of the ware proper for current Gachet client with the given type.
     *
     * @param string $type
     *
     * @return float
     */
    public function getGachetClientPrice(string $type)
    {
        return $this->getGachetPrice(
             gachet()->shop()->usableCurrency(),
             $type
        ) ?: $this->getGachetPrice(gachet()->currency()->getDefaultCurrency(), $type);
    }
}
