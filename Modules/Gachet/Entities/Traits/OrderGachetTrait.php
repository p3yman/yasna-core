<?php

namespace Modules\Gachet\Entities\Traits;

trait OrderGachetTrait
{
    /**
     * Returns the minimum payable ammount.
     *
     * @return int|float
     */
    public function minimumPayableAmount()
    {
        $post      = $this->post;
        $count     = $this->count;
        $min_value = ($post->getMeta('min_payable_amount') ?: 0);

        return ($min_value * $count);
    }



    /**
     * Accessor for the `minimumPayableAmount()` Relation
     *
     * @return float|int
     */
    public function getMinimumPayableAmountAttribute()
    {
        return $this->minimumPayableAmount();
    }



    /**
     * Accessor for the `minimumPayableAmount()` Relation
     *
     * @return float|int
     */
    public function getMinPayableAmountAttribute()
    {
        return $this->minimumPayableAmount();
    }



    /**
     * Returns the shipping price per floor.
     *
     * @return float|int
     */
    public function shippingPricePerFloor()
    {
        $post      = $this->post;
        $count     = $this->count;
        $min_value = ($post->getMeta('shipping_price_per_floor') ?: 0);

        return ($min_value * $count);
    }



    /**
     * Accessor for the `shippingPricePerFloor()` Relation.
     *
     * @return float|int
     */
    public function getShippingPricePerFloorAttribute()
    {
        return $this->shippingPricePerFloor();
    }
}
