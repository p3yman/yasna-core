<?php

namespace Modules\Gachet\Entities\Traits;

use Modules\Cart\Entities\Order;

trait CalculatePriceDeliveryTrait
{

    /**
     * Calculate the price of floors
     *
     * @return float|int
     */
    public function calculatePriceFloor()
    {
        if ($this->isDeliveryFloor()) {

            $total_price_floor = [];
            $orders            = $this->orders()->get();
            foreach ($orders as $order) {
                if ($this->isHeavy($order)) {
                    $post                = $order->post()->first();
                    $floor_price         = (float)$post['meta']['shipping_price_per_floor'];
                    $count               = (int)$order['count'];
                    $floor               = (int)$this->address['meta']['floor'];
                    $total_price_floor[] = $count * $floor_price * $floor;
                }
            }

            $total_price = array_sum($total_price_floor);

            return $total_price;
        }
    }



    /**
     * calculate the price of distance to destination
     *
     * @return float|int
     */
    public function calculatePriceHeavy()
    {
        if ($this->isDeliveryHeavy()) {

            $total_price = [];
            $orders      = $this->orders()->get();

            foreach ($orders as $order) {
                if ($this->isHeavy($order)) {
                    $post          = $order->post()->first();
                    $heavy_price   = (float)$post['meta']['shipping_price_per_floor'];
                    $count         = (int)$order['count'];
                    $total_price[] = $count * $heavy_price;
                }
            }

            $total_price_floor = array_sum($total_price);

            return $total_price_floor;
        }
    }



    /**
     * Calculate the price of floors when the place is far away
     *
     * @return float|int
     */
    public function calculatePriceDistanceFloor()
    {
        if ($this->isDeliveryDistanceFloor()) {

            $total_price_floor = [];
            $orders            = $this->orders()->get();
            foreach ($orders as $order) {
                if ($this->isHeavy($order)) {
                    $post                = $order->post()->first();
                    $floor_price         = (float)$post['meta']['shipping_price_per_floor'];
                    $count               = (int)$order['count'];
                    $floor               = (int)$this->address['meta']['floor'] + 1;
                    $total_price_floor[] = $count * $floor_price * $floor;
                }
            }

            $total_price = array_sum($total_price_floor);

            return $total_price;
        }
    }



    /**
     * Check the delivery method from [on-floor]
     *
     * @return bool
     */
    public function isDeliveryFloor()
    {

        if ($this->issetDeliveryMethod() == 'on-floor') {
            return true;
        }

        return false;
    }



    /**
     * Check the delivery method from [distance-place]
     *
     * @return bool
     */
    public function isDeliveryHeavy()
    {
        if ($this->issetDeliveryMethod() == 'distance-place') {
            return true;
        }

        return false;
    }



    /**
     * Check the delivery method from [distance-place-floor]
     *
     * @return bool
     */
    public function isDeliveryDistanceFloor()
    {
        if ($this->issetDeliveryMethod() == 'distance-place-floor') {
            return true;
        }

        return false;
    }



    /**
     * checking  exist the delivery method on the cart
     *
     * @return bool
     */
    public function issetDeliveryMethod()
    {
        return $this->meta['delivery_method'] ?? false;
    }



    /**
     * Checking the product's heaviness
     *
     * @param Order $order
     *
     * @return bool
     */
    public function isHeavy($order)
    {
        return $order->ware['approximated_size'] >= 4 ? true : false;
    }



    /**
     * check exist heavy product on cart
     *
     * @return bool
     */
    public function existHeavyProduct()
    {
        $flag = false;
        foreach ($this->orders()->get() as $order) {
            if ($order->ware['approximated_size'] >= 4) {
                $flag = true;
            }
        }
        return $flag;
    }
}
