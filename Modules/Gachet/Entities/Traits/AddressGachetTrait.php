<?php

namespace Modules\Gachet\Entities\Traits;

trait AddressGachetTrait
{
    /**
     * Returns an array of meta fields based on the floor property.
     *
     * @return array
     */
    public function floorMetaFields()
    {
        return [
             'floor',
        ];
    }
}
