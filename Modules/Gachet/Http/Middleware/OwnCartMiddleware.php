<?php

namespace Modules\Gachet\Http\Middleware;

use App\Models\Cart;
use Closure;
use Illuminate\Http\Request;

class OwnCartMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->guest()) {
            return $this->failureResponse();
        }

        $cart = model('Cart')->grabHashid($request->cart);

        if ($cart->not_exists) {
            return $this->failureResponse();
        }


        if ($cart->user_id != user()->id) {
            return $this->failureResponse();
        }

        return $next($request);
    }



    /**
     * Returns the response to be passed in case of failure.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function failureResponse()
    {
        return redirect('/');
    }
}
