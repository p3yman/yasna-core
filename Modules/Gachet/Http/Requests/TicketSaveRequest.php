<?php

namespace Modules\Gachet\Http\Requests;

use Illuminate\Validation\Rule;

class TicketSaveRequest extends TicketSingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'title'       => 'required',
             'text'        => 'required',
             'ticket_type' => [
                  'required',
                  Rule::in(
                       gachet()->tickets()->ticketTypes()->pluck('hashid')->toArray()
                  ),
             ],
             'priority'    => [
                  'required',
                  Rule::in(
                       gachet()->tickets()->priorities()
                  ),
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'ticket_type' => $this->runningModule()->getTrans('tickets.organization'),
             'priority'    => $this->runningModule()->getTrans('tickets.priority'),
        ];
    }
}
