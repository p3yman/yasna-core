<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TrackingRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'code' => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'code' => $this->runningModule()->getTrans('cart.tracking.code'),
        ];
    }
}
