<?php

namespace Modules\Gachet\Http\Requests;

use App\Models\Post;
use App\Models\Posttype;
use Modules\Gachet\Providers\CommentServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class CommentSaveRequest extends YasnaRequest
{
    protected $model_name = "post";
    protected $post;
    protected $posttype;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $post = $this->getPost();
        return ($post->exists and $post->has('commenting'));
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'post_id'   => 'dehash',
             'parent_id' => 'dehash',
             'mobile'    => 'ed',
             "tel"       => "ed",
        ];
    }



    /**
     * Returns the post's instance.
     *
     * @return Post
     */
    public function getPost()
    {
        if (!$this->post) {
            $post_id    = ($this->data['post_id'] ?? 0);
            $this->post = post($post_id);
        }

        return $this->post;
    }



    /**
     * Returns the posttype's instance.
     *
     * @return Posttype|null
     */
    public function getPosttype()
    {
        if (!$this->posttype) {
            $this->posttype = $this->getPost()->posttype;
        }

        return $this->posttype;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = array_merge_recursive(
             $this->getPostRules(),
             $this->getPosttypeRules(),
             $this->getGeneralRules()
        );

        return $rules;
    }



    /**
     * Returns the rules based on the post.
     *
     * @return array
     */
    protected function getPostRules()
    {
        if (
             array_key_exists('post_id', $data = $this->data) and
             ($post = post()->grabId($data['post_id'])) and
             $post->exists
        ) {
            $this->post = $post;
            return static::safeRulesArray(CommentServiceProvider::translateRules(
                 $this->post->getMeta('rules')
            ));
        }

        return [];
    }



    /**
     * Returns the posts based on the posttype.
     *
     * @return array
     */
    protected function getPosttypeRules()
    {
        return static::safeRulesArray(CommentServiceProvider::posttypeRules($this->getPosttype()));
    }



    /**
     * Returns the general rules.
     *
     * @return array
     */
    protected function getGeneralRules()
    {
        $rules = [
             'post_id'   => 'exists:posts,id',
             'parent_id' => 'exists:comments,id',
             'email'     => 'email',
             'mobile'    => 'phone:mobile',
             'text'      => function ($attribute, $value, $fail) {
                 if ($value) {
                     return true;
                 }

                 return $this->oneOfTextAlternativesIsPresented();
             },
        ];

        return static::safeRulesArray($rules);
    }



    /**
     * Modify the rules to be all array.
     *
     * @param array $rules
     *
     * @return array
     */
    protected static function safeRulesArray(array $rules)
    {
        return array_map(function ($item) {
            return is_string($item) ? explode('|', $item) : $item;
        }, $rules);
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'question' => $this->runningModule()->getTrans('validation.attributes.question'),
        ];
    }



    /**
     * Returns an array of the field which can be assumed as an alternative of the text field.
     *
     * @return array
     */
    protected function textAlternatives()
    {
        return ['message', 'question'];
    }



    /**
     * Checks if at least one of the alternatives of the text field has ben presented.
     *
     * @return bool
     */
    protected function oneOfTextAlternativesIsPresented()
    {
        $alternatives = $this->textAlternatives();

        foreach ($alternatives as $alternative) {
            $alternative_value = ($this->data[$alternative] ?? '');
            if ($alternative_value) {
                return true;
            }
        }

        return false;
    }
}
