<?php

namespace Modules\Gachet\Http\Requests;

use App\Models\Cart;
use Modules\Yasna\Services\YasnaRequest;

class PurchaseStep2VerifyRequest extends YasnaRequest
{
    protected $cart;
    protected $shipping_method;
    protected $address;
    protected $delivery_time;
    protected $delivery_method;
    protected $work_day;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'shipping_method' => function ($field, $value, $fail) {
                 if ($this->shipping_method and $this->shipping_method->exists) {
                     return true;
                 }

                 $error = $this->runningModule()->getTrans('cart.alert.error.required-selection', [
                      'attribute' => $this->runningModule()->getTrans('cart.courier.title.singular'),
                 ])
                 ;

                 $fail($error);

                 return false;
             },
             'address'         => function ($field, $value, $fail) {
                 if ($this->address and $this->address->exists) {
                     return true;
                 }

                 $error = $this->runningModule()->getTrans('cart.alert.error.required-selection', [
                      'attribute' => $this->runningModule()->getTrans('general.address.title.singular'),
                 ])
                 ;

                 $fail($error);

                 return false;
             },
             'delivery_time'   => $this->deliveryTimeValidate(),
             'delivery_method' => $this->deliveryMethodValidate(),
             'work_day'        => $this->workDayValidate(),
             'company_name'    => 'required_if:official_bill,true',
             'economical_id'   => 'required_if:official_bill,true',
             'registration_id' => 'required_if:official_bill,true',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctSippingMethod();
        $this->correctAddress();
        $this->correctOfficialBillInfo();
        $this->correctDescription();
    }



    /**
     * Corrects the shipping method value based on the client's active cart.
     */
    protected function correctSippingMethod()
    {
        $this->shipping_method         = ($this->getCart()->shipping_method ?? model('shipping-method'));
        $this->delivery_time           = ($this->getCart()->meta['delivery_time'] ?? null);
        $this->delivery_method         = ($this->getCart()->meta['delivery_method'] ?? null);
        $this->work_day                = ($this->getCart()->meta['work_day'] ?? null);
        $this->data['shipping_method'] = $this->shipping_method->id;
    }



    /**
     * Corrects the address value based on the client's active cart.
     */
    protected function correctAddress()
    {
        $this->address         = user()
                  ->addresses()
                  ->whereId($this->getCart()->address_id)
                  ->first() ?? model('address');
        $this->data['address'] = $this->address->id;
    }



    /**
     * Returns the instance of the client's active cart.
     *
     * @return Cart
     */
    public function getCart()
    {
        if (!$this->cart) {
            $this->cart = gachet()->cart()->findActiveCart();;
        }

        return $this->cart;
    }



    /**
     * Corrects official bill information.
     */
    protected function correctOfficialBillInfo()
    {
        $official_bill_input = boolval($this->data['official_bill'] ?? false);
        if ($this->getCart()->officialBillIsSupported() and $official_bill_input) {
            $this->data['official_bill'] = $official_bill_input;
        } else {
            $this->data['official_bill'] = false;

            unset($this->data['company_name']);
            unset($this->data['economical_id']);
            unset($this->data['registration_id']);
        }
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'company_name.required_if'    => $this
                  ->runningModule()
                  ->getTrans('cart.is-required-for-official-bill'),
             'economical_id.required_if'   => $this
                  ->runningModule()
                  ->getTrans('cart.is-required-for-official-bill'),
             'registration_id.required_if' => $this
                  ->runningModule()
                  ->getTrans('cart.is-required-for-official-bill'),
             'delivery_time.required'      => $this
                  ->runningModule()
                  ->getTrans('cart.is-required-for-delivery'),
             'delivery_method.required'    => $this
                  ->runningModule()
                  ->getTrans('cart.is-required-for-delivery'),
             'work_day.required'           => $this
                  ->runningModule()
                  ->getTrans('cart.is-required-for-delivery'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'company_name'    => module('cart')->getTrans('official-bill.company-name'),
             'economical_id'   => module('cart')->getTrans('official-bill.economical-id'),
             'registration_id' => module('cart')->getTrans('official-bill.registration-id'),
             'delivery_time'   => module('Gachet')->getTrans('cart.delivery_time'),
             'delivery_method' => module('Gachet')->getTrans('cart.delivery.delivery_method'),
             'work_day'        => module('Gachet')->getTrans('cart.work_day'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'company_name'    => 'ed',
             'economical_id'   => 'ed',
             'registration_id' => 'ed',
        ];
    }



    /**
     * delivery time validate
     *
     * @return bool|string
     */
    public function deliveryTimeValidate()
    {
        if ($this->getCart()->existHeavyProduct() && $this->getCart()->address->id == 8) {
            if ($this->delivery_time) {
                return "";
            }
            return 'required';
        }
        return "";
    }



    /**
     * delivery method validate
     *
     * @return bool|string
     */
    public function deliveryMethodValidate()
    {
        if ($this->getCart()->existHeavyProduct() && $this->getCart()->address->id == 8) {
            if ($this->delivery_method) {
                return "";
            }
            return 'required';
        }
        return "";
    }



    /**
     * delivery method validate
     *
     * @return bool|string
     */
    public function workDayValidate()
    {
        if ($this->work_day) {
            return "";
        }

        return 'required';
    }



    /**
     * Corrects description information.
     */
    protected function correctDescription()
    {
        $description = $this->data['description'] ?? null;
        if ($description) {
            $this->data['description'] = $description;
        } else {
            unset($this->data['description']);
        }
    }
}
