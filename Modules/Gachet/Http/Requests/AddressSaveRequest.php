<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Address\Http\Requests\AddressSaveRequest as OriginalRequest;

class AddressSaveRequest extends OriginalRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
             'floor' => 'required|numeric|integer',
        ]);
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return array_merge(parent::purifier(), [
             'floor' => 'ed',
        ]);
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::messages(), [
             'floor' => $this->runningModule()->getTrans('validation.attributes.floor'),
        ]);
    }
}
