<?php

namespace Modules\Gachet\Http\Requests\Manage;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;

class DrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //@TODO?
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => "required|numeric|min:1|max:" . session()->get('line_number'),
        ];
    }

    public function all($key = null)
    {
        $value = parent::all();
        $purified = ValidationServiceProvider::purifier($value, [
            'number' => "ed",
        ]);

        return $purified;
    }
}
