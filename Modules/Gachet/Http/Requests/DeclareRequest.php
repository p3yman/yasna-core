<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DeclareRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'amount'          => 'required|numeric|min:1',
             'trackingCode'    => ['required'],
             'declarationDate' => [
                  'required',
                  'date',
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'amount'          => 'ed',
             'trackingCode'    => 'ed',
             'declarationDate' => 'ed|gDate',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'trackingCode'    => $this->runningModule()->getTrans('cart.headline.tracking-code'),
             'declarationDate' => $this->runningModule()->getTrans('cart.headline.declaration-date'),
        ];
    }


    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'amount.min' => $this->runningModule()->getTrans('cart.alert.error.too-few-amount'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctAmount();
    }



    /**
     * Corrects the amount value.
     */
    protected function correctAmount()
    {
        $amount = ($this->data['amount'] ?? null);

        if (!$amount or !is_numeric($amount)) {
            return;
        }

        $this->data['amount'] = ($amount / gachet()->currency()->getPreviewCurrencyRatio());
    }
}
