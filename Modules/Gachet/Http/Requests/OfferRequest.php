<?php

namespace Modules\Gachet\Http\Requests;

use App\Models\ShopOffer;
use Modules\Yasna\Services\YasnaRequest;


/**
 * Class OfferRequest
 *
 * @property ShopOffer $model
 */
class OfferRequest extends YasnaRequest
{
    protected $model_name         = 'shop-offer';
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        return ($model->exists and $model->isValid());
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        $this->data['id']    = $this->getRequestedModelId($id_or_hashid);
        $this->data['model'] = $this->model = model($this->model_name, $this->slug, $this->model_with_trashed);

        $this->refillRequestHashid();
        $this->failIfModelNotExist();
    }
}
