<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketSingleRequest extends YasnaRequest
{
    protected $model_name         = "ticket";
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        return ($this->canBeOwned() and $model->isDefaultType());
    }



    /**
     * Checks if the ticket can be owned by current client.
     *
     * @return bool
     */
    protected function canBeOwned()
    {
        $model = $this->model;

        return (
             $model->not_exists or
             $model->user_id == user()->id
        );
    }
}
