<?php

namespace Modules\Gachet\Http\Requests\Auth;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class RegisterUserRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name_first' => 'required|persian:60',
             'name_last'  => 'required|persian:60',
             'mobile'     => 'required|phone:mobile',
             'email'      => [
                  'required',
                  'email',
                  Rule::unique('users'),
             ],
             'password'   => 'required|same:password2|min:8|max:50',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name_first' => 'pd',
             'name_last'  => 'pd',
             'mobile'     => 'ed',
             'password'   => 'ed',
             'password2'  => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'name_first',
             'name_last',
             'mobile',
             'email',
             'password',
             'password2',
             'password2',
        ];
    }
}
