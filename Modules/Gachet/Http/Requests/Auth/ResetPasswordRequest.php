<?php

namespace Modules\Gachet\Http\Requests\Auth;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class ResetPasswordRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'email' => [
                  'required',
                  'email',
                  Rule::exists('users'),
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'email' => 'ed',
        ];
    }
}
