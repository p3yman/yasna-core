<?php

namespace Modules\Gachet\Http\Requests\Auth;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Hash;
use Modules\Gachet\Http\Controllers\Auth\ResetPasswordController;
use Modules\Yasna\Services\YasnaRequest;

class PasswordTokenRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'email'                => [
                  'required_if:direct_request,true',
                  'exists:users',
             ],
             'password_reset_token' => [
                  'required',
                  'numeric',
                  function ($attribute, $value, $fail) {
                      return $this->tokenClosureRule($attribute, $value, $fail);
                  },
             ],
        ];
    }



    /**
     * Simulate a customized rule to validate the given token.
     *
     * @param string  $attribute
     * @param string  $value
     * @param Closure $fail
     *
     * @return bool
     */
    protected function tokenClosureRule($attribute, $value, $fail)
    {
        $user = $this->model;

        // Check if th code is valid
        if (!Hash::check($value, $user->reset_token)) {
            $fail($this->runningModule()->getTrans('user.password.token'));
            return false;
        }

        // Check the code's expiration
        $now = now();
        $exp = Carbon::parse($user->reset_token_expire);
        if ($now->gt($exp)) {
            $fail(
                 view($this->runningModule()->getBladePath('layouts.widgets.password-expired'))
            );
            return false;
        }

        return true;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $user_id = session()->get(ResetPasswordController::$reset_token_session_name);
        $user    = $this->model = model('user', $user_id);

        if ($user->exists) {
            $this->data['email'] = $user->email;
        } else {

            $this->model = model('user')->grab($this->email, 'email');

            $this->data['direct_request'] = true;
        }
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'code_melli'           => 'ed',
             'password_reset_token' => 'ed',
        ];
    }


}
