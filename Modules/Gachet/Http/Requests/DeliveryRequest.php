<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class DeliveryRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;


    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'delivery_time' => 'required|' . $this->validateDeliveryTime(),
        ];
    }



    /**
     * Check the allowed time interval
     *
     * @return string
     */
    public function validateDeliveryTime()
    {
        $delivery_time = get_setting('delivery_time');
        if (in_array($this->getData('delivery_time'), $delivery_time)) {
            return '';
        }
        return 'invalid';
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'delivery_time',
        ];
    }

}
