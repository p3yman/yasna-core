<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class NewPasswordRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            'new_password' => 'required|same:new_password2|min:8|max:50|',
        ];
    }
}
