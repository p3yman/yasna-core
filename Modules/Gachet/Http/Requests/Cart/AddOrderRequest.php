<?php

namespace Modules\Gachet\Http\Requests\Cart;

use Modules\Yasna\Services\YasnaRequest;

class AddOrderRequest extends YasnaRequest
{
    protected $model_name         = 'ware';
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'count' => 'required|numeric',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'count' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        return ($model->exists and $model->isPurchasable());
    }
}
