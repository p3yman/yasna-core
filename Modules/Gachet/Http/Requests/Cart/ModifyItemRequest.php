<?php

namespace Modules\Gachet\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class ModifyItemRequest extends YasnaRequest
{
    protected $model_name         = 'order';
    protected $model_with_trashed = false;


    public $cart;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'count' => 'required|numeric',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'count' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $order = $this->model;

        if ($order->not_exists) {
            return false;
        }

        $this->cart = gachet()->cart()->findActiveCart();

        return boolval(
             $this->cart
                  ->orders()
                  ->whereId($order->id)
                  ->first()
        );
    }
}
