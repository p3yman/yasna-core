<?php

namespace Modules\Gachet\Http\Requests\Cart;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class ModifyCartRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'address' => Rule::exists('addresses', 'id', function ($query) {
                 $query->where('user_id', user()->id);
             }),
             'courier' => function ($field, $value) {
                 return gachet()->cart()->findActiveCart()->canBeShippedBy($value);
             },
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'address' => 'dehash',
             'courier' => 'dehash',
        ];
    }
}
