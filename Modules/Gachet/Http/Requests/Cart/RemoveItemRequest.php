<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/13/18
 * Time: 5:59 PM
 */

namespace Modules\Gachet\Http\Requests\Cart;


use Modules\Yasna\Services\YasnaRequest;

class RemoveItemRequest extends YasnaRequest
{
    protected $model_name         = 'order';
    protected $model_with_trashed = false;


    public $cart;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $order = $this->model;

        if ($order->not_exists) {
            return false;
        }

        $this->cart = gachet()->cart()->findActiveCart();

        return boolval(
             $this->cart
                  ->orders()
                  ->whereId($order->id)
                  ->first()
        );
    }
}
