<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/19/18
 * Time: 11:59 AM
 */

namespace Modules\Gachet\Http\Requests;


class TicketReplyRequest extends TicketSingleRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'text' => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return parent::authorize() and ($this->model->flag != 'done');
    }
}
