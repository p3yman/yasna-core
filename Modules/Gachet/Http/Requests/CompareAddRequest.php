<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CompareAddRequest extends YasnaRequest
{
    protected $model_name         = "post";
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        return ($model->exists and $model->hasSpecs());
    }
}
