<?php

namespace Modules\Gachet\Http\Requests;

use Carbon\Carbon;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class WorkDayRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;


    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'work_day' => 'required|' . $this->validateWorkDay(),
        ];
    }



    /**
     * Check the allowed time interval
     *
     * @return string
     */
    public function validateWorkDay()
    {
        $active_day = Carbon::createFromTimestamp($this->getData('work_day'))->toDateTimeString();
        $now        = carbon::now()->startOfDay();
        $work_day   = model('Workday')
             ->where('open', 1)
             ->where('date', '>=', $now)
             ->where('date', $active_day)
             ->limit(7)->get()
        ;
        if (!$work_day->isEmpty()) {
            return '';
        }
        return 'invalid';
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'work_day',
        ];
    }

}
