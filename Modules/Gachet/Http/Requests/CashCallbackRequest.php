<?php

namespace Modules\Gachet\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CashCallbackRequest extends YasnaRequest
{
    protected $model_name         = 'account';
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $account = $this->model;

        return $account->exists and ($account->type == 'cash');
    }
}
