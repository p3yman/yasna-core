<?php

namespace Modules\Gachet\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Gachet\Services\Controller\GachetFrontControllerAbstract;
use Modules\Gachet\Services\Module\CurrentModuleHelper;

class ProvinceController extends GachetFrontControllerAbstract
{
    /**
     * get list of cities from requested province
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProvinceCitiesSelect(Request $request)
    {
        $options = model('state')->where('parent_id', $request->province)->pluck('title', 'id')->toArray();
        return view(CurrentModuleHelper::bladePath('layouts.widgets.form.select-self-options'), compact('options'));
    }
}
