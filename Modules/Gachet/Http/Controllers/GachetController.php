<?php

namespace Modules\Gachet\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Gachet\Services\Controller\GachetFrontControllerTrait;

class GachetController extends Controller
{
    use GachetFrontControllerTrait;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return $this->template('home.main');
    }
}
