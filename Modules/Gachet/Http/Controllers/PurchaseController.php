<?php

namespace Modules\Gachet\Http\Controllers;

use App\Models\Account;
use App\Models\Cart;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Modules\Gachet\Http\Requests\CashCallbackRequest;
use Modules\Gachet\Http\Requests\PurchaseStep2VerifyRequest;
use Modules\Gachet\Providers\RouteServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;
use Modules\Gachet\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Providers\ValidationServiceProvider;

class PurchaseController extends GachetFrontControllerAbstract
{
    protected static $acceptedPaymentTypes = ['online', 'deposit', 'shetab', 'cash'];
    protected        $viewVariables        = [];



    /**
     * PurchaseController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('cartNonempty')->except('paymentCallback', 'paymentShortLink');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function step1()
    {
        if (!auth()->guest()) {
            return redirect(route('gachet.purchase.step2'));
        }

        return $this->template('purchase.step1.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function step2()
    {
        return $this->template('purchase.step2.main', $this->getAddressAndShippingOptions());
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reloadStep2()
    {
        return $this->template('purchase.step2.content', $this->getAddressAndShippingOptions());
    }



    /**
     * @return array
     */
    protected function getAddressAndShippingOptions()
    {
        $ready_for_purchasement = true;
        $cart                   = gachet()->cart()->findActiveCart();

        $addresses       = $this->getAllUserAddresses();
        $default_address = $addresses->where('default', 1)->first();

        if ($default_address) {
            $cart->update(['address_id' => $default_address->id]);
            $cart->refresh();
        }

        $shipping_methods = $cart->availableShippingMethods();

        if (!count($shipping_methods)) {
            $ready_for_purchasement = false;
            return compact('ready_for_purchasement');
        }


        $combos = $this->getAddressComboInfo();

        $delivery = $this->getDeliveryMethod();

        $delivery_active = $cart->getMeta('delivery_method');

        $factor = $this->getFactorRequest($cart);

        $delivery_time = get_setting('delivery_time');

        $work_day = $this->workDay();

        // apply the shipping method to handle the correct delivery price
        $shipping_method = ($cart->shipping_method_id ?: $shipping_methods[0]->id);
        $cart->addShippingMethod($shipping_method);

        return array_merge(
             $combos, compact(
             'addresses',
             'shipping_methods',
             'ready_for_purchasement',
             'cart',
             'delivery',
             'delivery_active',
             'factor',
             'delivery_time',
             'work_day'
        ));
    }



    /**
     * Returns a list of user's addresses.
     *
     * @return LengthAwarePaginator
     */
    protected function getAllUserAddresses()
    {
        return user()->addresses()
                     ->orderBy('default', 'desc')
                     ->orderBy('created_at', 'desc')
                     ->paginate(12)
             ;
    }



    /**
     * Returns an array of data to be sent to the blade for the address form.
     *
     * @return array
     */
    protected function getAddressComboInfo()
    {
        $provinces_combo  = model('state')->allProvinces()->pluck('title', 'id')->toArray();
        $default_province = array_search($this->module()->getConfig('default-province'), $provinces_combo);

        $cities_combo = model('state')->citiesOf(0)->pluck('title', 'id')->toArray();
        $default_city = array_search($this->module()->getConfig('default-city'), $cities_combo);

        return compact('provinces_combo', 'default_province', 'cities_combo', 'default_city');
    }



    /**
     * Verifies the step 2 if cart is ready.
     *
     * @param PurchaseStep2VerifyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyStep2(PurchaseStep2VerifyRequest $request)
    {
        $cart = gachet()->cart()->findActiveCart();

        $this->saveDescription($cart, $request);
        $this->setCustomPrice($cart);

        if ($request->official_bill) {
            $cart->withOfficialBill();


            $cart = model('cart', $cart->id);

            $cart->batchSave(
                 $request->only('company_name', 'economical_id', 'registration_id')
            );
        } else {
            $this->unsetOfficialBillInfo($cart);
            $cart->withoutOfficialBill();
        }


        return response()->json(['success' => true]);
    }



    /**
     * Unsets the official bill information in the specified cart.
     *
     * @param Cart $cart
     */
    protected function unsetOfficialBillInfo(Cart $cart)
    {
        if ($cart->officialBillIsNotActivated()) {
            return;
        }

        $cart->batchSave(
             array_fill_keys(
                  ['company_name', 'economical_id', 'registration_id'],
                  'unset'
             )
        );
    }



    /**
     * save description on meta field
     *
     * @param Cart                       $cart
     * @param PurchaseStep2VerifyRequest $request
     */
    public function saveDescription($cart, $request)
    {
        if ($request->description) {
            $cart->batchSave($request->only('description'));
        }
        $this->unsetDescription($cart, $request);
    }



    /**
     * Unset the description in the specified cart.
     *
     * @param Cart $cart
     */
    protected function unsetDescription(Cart $cart, $request)
    {
        if (!$request->description) {
            $cart->batchSave(
                 array_fill_keys(
                      ['description'],
                      'unset'
                 )
            );
        }
    }



    /**
     * Renders the preview of the step 3 of the purchase process.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function step3()
    {
        $cart = gachet()->cart()->findActiveCart();

        $cart->refreshCalculations();

        $address         = $cart->address;
        $shipping_method = $cart->shipping_method;

        if (!$address or !$address->exists or !$shipping_method or !$shipping_method->exists) {
            return redirect()->route('gachet.purchase.step2');
        }

        return $this->template('purchase.step3.main', compact('cart', 'address', 'shipping_method'));
    }



    /**
     * Verifies the step 3 of the purchase process.
     *
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function verifyStep3()
    {
        $cart = gachet()->cart()->findActiveCart();

        if ($cart->isRaisable()) {
            return response()->json(['success' => true]);
        } else {
            $response_content = view($this->module()->getBladePath('layouts.widgets.element.a'), [
                 'html' => $this->module()->getTrans('cart.button.back-to-cart'),
                 'href' => route('gachet.cart.index'),
            ]);
            return $this->abort(422, true, true, [
                 'success' => false,
                 'errors'  => [
                      $this->module()->getTrans('cart.message.cart-changed') .
                      '. ' .
                      $response_content,
                 ],
            ]);
        }
    }



    /**
     * Renders preview of the step 4 of the purchase process.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function step4()
    {
        $currency         = gachet()->currency()->getCurrency();
        $online_accounts  = payment()->accounts()->currency($currency)->online()->get();
        $deposit_accounts = payment()->accounts()->currency($currency)->deposit()->get();
        $shetab_accounts  = payment()->accounts()->currency($currency)->shetab()->get();
        $cash_accounts    = payment()->accounts()->currency($currency)->cash()->get();

        return $this->template('purchase.step4.main', compact(
             'online_accounts',
             'deposit_accounts',
             'shetab_accounts',
             'cash_accounts'
        ));
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function final(Request $request)
    {
        $data = $this->paymentPurifier($request->all());
        if ($this->paymentValidation($data)) {
            $account_id = $this->fetchPaymentAccountId($data);
            $account    = model('account', $account_id);
            $cart       = gachet()->cart()->findActiveCart();

            if ($account->type == 'cash') {

                if (get_setting('minimum_payment') and $cart->min_payable_amount != 0) {
                    return $this->finalHandleCash($cart, $account, $data['online_min_account']);
                }
                return $this->cashPayment($cart, $account, $request);
            }

            $this->raiseCart($cart);

            $transaction = payment()
                 ->transaction()
                 ->invoiceObject($cart)
                 ->amount($cart->invoiced_amount)
                 ->accountId($account_id)
                 ->autoVerify(false)
                 ->callbackUrl(route('gachet.payment.callback',
                      ['cart' => $cart->hashid]))
            ;

            if ($declarationInfo = $this->fetchDeclarationInfo($data)) {
                $transaction->resnum($declarationInfo['declarationCode'])
                            ->declared_at($declarationInfo['declarationDate'])
                ;
            }

            $transactionResponse = $transaction->fire();

            if ($transactionResponse['slug'] == 'returnable-response') {
                return $transactionResponse['response'];
            }
        }

        return redirect()->route('gachet.purchase.step4');
    }



    /**
     * Handles the final process for cash account.
     *
     * @param Cart    $cart
     * @param Account $account
     * @param int     $online_account_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ReflectionException
     */
    protected function finalHandleCash(Cart $cart, Account $account, $online_account_id)
    {
        $transaction = payment()
             ->transaction()
             ->invoiceObject($cart)
             ->amount($cart->min_payable_amount)
             ->accountId($online_account_id)
             ->autoVerify(false)
             ->callbackUrl(
                  route('gachet.purchase.cash-callback', [
                       'cart'   => $cart->hashid,
                       'hashid' => $account->hashid,
                  ])
             )
        ;


        $response = $transaction->fire();


        if ($response['code'] == 100) {
            return $response['response'];
        } else {
            return redirect()->route('gachet.purchase.step4');
        }
    }



    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function paymentPurifier(array $data)
    {
        return ValidationServiceProvider::purifier($data, [
             'onlineAccount' => 'dehash',

             'depositAccount'         => 'dehash',
             'depositTrackingCode'    => 'ed',
             'depositDeclarationDate' => 'gDate',

             'shetabAccount'         => 'dehash',
             'shetabTrackingCode'    => 'ed',
             'shetabDeclarationDate' => 'gDate',

             'cashAccount' => 'dehash',

             'online_min_account' => 'dehash',

        ]);
    }



    /**
     * @param array $data
     *
     * @return bool
     */
    protected function paymentValidation(array $data)
    {
        $validator = Validator::make($data, [
             'paymentType'    => 'required|in:' . implode(',', self::$acceptedPaymentTypes),

             // Online
             'onlineAccount'  => [
                  'required_if:paymentType,online',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'online'),
             ],

             // Deposit
             'depositAccount' => [
                  'required_if:paymentType,deposit',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'deposit'),
             ],

             'depositDeclarationDate' => [
                  'required_with:depositTrackingCode',
                  'date',
             ],

             // Shetab
             'shetabAccount'          => [
                  'required_if:paymentType,shetab',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'shetab'),
             ],

             'shetabDeclarationDate' => [
                  'required_with:shetabTrackingCode',
                  'date',
             ],

             // Cash
             'cashAccount'           => [
                  'required_if:paymentType,cash',
                  Rule::exists('accounts', 'id')->where('accounts.type', 'cash'),
             ],

             'online_min_account' => [
                  Rule::exists('accounts', 'id')->where('accounts.type', 'online'),
             ],
        ]);

        return !$validator->fails();
    }



    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function fetchPaymentAccountId(array $data)
    {
        return $data[$data['paymentType'] . 'Account'];
    }



    /**
     * @param array $data
     *
     * @return array|bool
     */
    protected function fetchDeclarationInfo(array $data)
    {
        $declarationCode      = $data['paymentType'] . 'TrackingCode';
        $declarationDateIndex = $data['paymentType'] . 'DeclarationDate';
        if (
             array_key_exists($declarationCode, $data) and $data[$declarationCode] and
             array_key_exists($declarationDateIndex, $data) and $data[$declarationDateIndex]) {
            return [
                 'declarationCode' => $data[$declarationCode],
                 'declarationDate' => $data[$declarationDateIndex],
            ];
        }

        return false;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function paymentCallback(Request $request)
    {
        if ($transactions = $this->findTransactions($cart = $this->findCart($request->cart))) {
            $this->checkTransaction($cart, $transactions);
            $transactionHandler = $transactions->first()->handler;
            return $this->template('purchase.step5.main', compact(
                 'cart',
                 'transactions',
                 'transactionHandler'
            ), $this->viewVariables);
        }

        return redirect()->route('gachet.cart.index');
    }



    /**
     * @param $cartHashid
     *
     * @return mixed
     */
    protected function findCart($cartHashid)
    {
        return (($cart = model('Cart')->grabHashid($cartHashid)) and $cart->exists) ? $cart : null;
    }



    /**
     * @param $cart
     *
     * @return null
     */
    protected function findTransactions($cart)
    {
        if (
             $cart and
             ($transaction = payment()->invoice($cart->id)->orderBy('created_at', 'desc')->get()) and
             $transaction->count()
        ) {
            return $transaction;
        }

        return null;
    }



    /**
     * @param Cart $cart
     * @param      $transactions
     */
    protected function checkTransaction(Cart $cart, $transactions)
    {
        if (
             ($trackingNo = \request()->tracking_no) and
             $queryTransaction = $transactions->where('tracking_no', $trackingNo)->first()
        ) {
            $transactionHandler = $queryTransaction->handler;
            if ($transactionHandler->type() == 'online') {
                if ($oldVerifiedTransaction = $cart->verifiedTransactions()->first()) {
                    if ($oldVerifiedTransaction->id != $queryTransaction->id) {
                        $transactionHandler->makeCancelled(Carbon::now(),
                             'Cart has been verified with another transaction.');
                        $this->viewVariables['transactionAlert'] =
                             CurrentModuleHelper::trans('cart.alert.error.unable-to-verify');
                    }
                } else {
                    $verificationResult = $transactionHandler->verify();
                    if ($verificationResult['code'] > 0) {
                        $queryTransaction->refresh();
                        $cart->update([
                             'disbursed_at' => $queryTransaction->declared_at,
                             'disbursed_by' => $queryTransaction->declared_by,
                             'paid_at'      => $queryTransaction->verified_at,
                             'paid_by'      => $queryTransaction->verified_by,
                        ]);
                        $this->viewVariables['transactionAlert']   =
                             CurrentModuleHelper::trans('cart.alert.success.payment');
                        $this->viewVariables['transactionSuccess'] = true;
                    } else {
                        $this->viewVariables['transactionAlert'] =
                             CurrentModuleHelper::trans('cart.alert.error.rejected-by-gateway');
                    }
                }
            }
        }

        $this->modifyCart($cart);
    }



    /**
     * @param Cart $cart
     */
    protected function modifyCart(Cart $cart)
    {
        if (
             $cart->isNotDisbursed() and
             ($declaredTransaction = $cart->declaredTransactions()->offline()->first())
        ) {
            $cart->update([
                 'disbursed_at' => $declaredTransaction->declarad_at,
                 'disbursed_by' => $declaredTransaction->declarad_by,
            ]);
        }

        if ($cart->isNotPaid() and ($verifiedTransaction = $cart->verifiedTransactions()->first())) {
            $cart->update([
                 'paid_at' => $verifiedTransaction->verified_at,
                 'paid_by' => $verifiedTransaction->verified_by,
            ]);
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function paymentShortLink(Request $request)
    {
        return redirect(($cart = $this->findCart($request->cart))
             ? RouteServiceProvider::action('PurchaseController@paymentCallback', [
                  'locale' => $cart->locale,
                  'cart'   => $cart->hashid,
             ])
             : url('/'));
    }



    /**
     * The callback for the online payment which should be done before cash.
     *
     * @param CashCallbackRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ReflectionException
     */
    public function cashCallback(CashCallbackRequest $request)
    {
        $cart         = $this->findCart($request->cart);
        $account      = $request->model;
        $transactions = $this->findTransactions($cart);
        if ($transactions and $this->cashCallbackHandleCart($cart, $transactions)) {

            $this->raiseCart($cart);

            $cart->refresh();

            payment()
                 ->transaction()
                 ->invoiceObject($cart)
                 ->amount($cart->invoiced_amount - $cart->paid_amount)
                 ->accountId($account->id)
                 ->autoVerify(false)
                 ->callbackUrl(route('gachet.payment.callback', ['cart' => $cart->hashid]))
                 ->getTracking()
            ;

            return redirect()->route('gachet.payment.callback', [
                 'cart'        => $cart->hashid,
                 'tracking_no' => $request->tracking_no,
            ]);
        }

        return redirect()->route('gachet.purchase.step4');
    }



    /**
     * Handles the information for the cash callback.
     *
     * @param Cart       $cart
     * @param Collection $transactions
     *
     * @return bool
     */
    protected function cashCallbackHandleCart(Cart $cart, $transactions)
    {
        $trackingNo        = request()->tracking_no;
        $query_transaction = $transactions->where('tracking_no', $trackingNo)->first();
        if ($query_transaction) {
            $transaction_handler = $query_transaction->handler;

            if ($transaction_handler->type() == 'online') {
                $verification_result = $transaction_handler->verify();
                if ($verification_result['code'] > 0) {
                    $query_transaction->refresh();
                    $cart->update([
                         'disbursed_at' => $query_transaction->declared_at,
                         'disbursed_by' => $query_transaction->declared_by,
                         'paid_amount'  => $query_transaction->paid_amount,
                    ]);


                    return true;
                }
            }
        }

        return false;
    }



    /**
     * Raises the given cart.
     *
     * @param Cart $cart
     */
    protected function raiseCart(Cart $cart)
    {
        $cart->checkout();
        $cart->markAppliedCouponCodeAsUsed();
    }



    /**
     * return delivery methods
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getDeliveryMethod()
    {

        return collect([
             '0' => [
                  'title' => $this->module()->getTrans('cart.delivery.delivery_method_Next_vehicle'),
                  'slug'  => 'next-vehicle',
             ],
             '1' => [
                  'title' => $this->module()->getTrans('cart.delivery.delivery_method_Distance_place'),
                  'slug'  => 'distance-place',
             ],
             '2' => [
                  'title' => $this->module()->getTrans('cart.delivery.delivery_method_floor'),
                  'slug'  => 'on-floor',
             ],
             '3' => [
                  'title' => $this->module()->getTrans('cart.delivery.delivery_method_ground_floor'),
                  'slug'  => 'on-ground-floor',
             ],
        ]);

    }



    /**
     * return checkbox value for factor request
     *
     * @param Cart $cart
     *
     * @return array|string|null
     */
    protected function getFactorRequest($cart)
    {
        return $cart->getMeta('factor_request');

    }



    /**
     * Working days to post product
     *
     * @return array
     */
    protected function workDay()
    {
        $now      = carbon::now()->startOfDay();
        $work_day = model('Workday')->where('open', 1)->where('date', '>=', $now)->limit(7)->get();

        return $work_day;
    }



    /**
     * Calculate delivery price and heavy goods and save in meta.
     *
     * @param Cart $cart
     */
    public function setCustomPrice($cart)
    {
        if ($cart->address->province_id == 8) {

            $custom_price[] = $cart->calculatePriceFloor() ?? 0;
            $custom_price[] = $cart->calculatePriceHeavy() ?? 0;
            $custom_price[] = $cart->calculatePriceDistanceFloor() ?? 0;

            $cart->batchSave([
                 "heavy_delivery_price" => array_sum($custom_price),
            ]);

        } else {

            $cart->batchSave(
                 array_fill_keys(
                      ['heavy_delivery_price'],
                      'unset'
                 )
            );
        }
    }



    /**
     * cash payment without  minimum payment
     *
     * @param Cart    $cart
     * @param Account $account
     * @param Request        $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ReflectionException
     */
    private function cashPayment($cart, $account,$request)
    {
        $this->raiseCart($cart);

        payment()
             ->transaction()
             ->invoiceObject($cart)
             ->amount($cart->invoiced_amount)
             ->accountId($account->id)
             ->autoVerify(false)
             ->callbackUrl(route('gachet.payment.callback', ['cart' => $cart->hashid]))
             ->getTracking()
        ;

        return redirect()->route('gachet.payment.callback', [
             'cart' => $cart->hashid,
             'tracking_no' => $request->tracking_no,
        ]);
    }
}
