<?php

namespace Modules\Gachet\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;
use Modules\Gachet\Http\Requests\Cart\AddOrderRequest;
use Modules\Gachet\Http\Requests\Cart\ModifyCartRequest;
use Modules\Gachet\Http\Requests\Cart\ModifyItemRequest;
use Modules\Gachet\Http\Requests\Cart\RemoveItemRequest;
use Modules\Gachet\Http\Requests\CartAddCouponCodeRequest;
use Modules\Gachet\Http\Requests\DeliveryRequest;
use Modules\Gachet\Http\Requests\WorkDayRequest;
use Modules\Gachet\Providers\CartServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;
use Modules\Gachet\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class CartController extends GachetFrontControllerAbstract
{
    /**
     * Renders the index page of the cart.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $cart = gachet()->cart()->findActiveCart();
        $cart->refreshCalculations();

        return $this->template('cart.main', compact('cart'));
    }



    /**
     * Flushes the active cart of current client.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function flush()
    {
        $cart = gachet()->cart()->findActiveCart();

        $cart->batchSave([
             'heavy_delivery_price' => 0,
        ]);

        foreach ($cart->orders as $order) {
            $order->remove();
        }

        $cart->refresh();

        return view($this->module()->getBladePath('cart.content'), compact('cart'));
    }



    /**
     * Returns a new and fresh preview of the cart button in the header.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reloadView()
    {
        $this->setTemplateCart();
        return view($this->module()->getBladePath('layouts.header-cart'));
    }



    /**
     * Modifies cart with the requested data.
     *
     * @param ModifyCartRequest $request
     *
     * @return array
     */
    public function modifyCart(ModifyCartRequest $request)
    {
        $cart = gachet()->cart()->findActiveCart();

        if ($request->address) {
            $address = model('address', $request->address);

            if ($address and $address->exists and ($address->user_id == user()->id)) {
                user()->addresses()
                      ->where('default', 1)
                      ->where('id', '<>', $address->id)
                      ->update(['default' => 0])
                ;

                $address->update(['default' => 1]);
                $cart->update([
                     'address_id'         => $address->id,
                     'shipping_method_id' => null,
                ]);
            }
        }

        if ($request->courier) {
            $cart->addShippingMethod($request->courier);
        }

        return ['success' => $cart->exists];
    }



    /**
     * Adds the specified ware with the requested count to the active cart of current client.
     *
     * @param AddOrderRequest $request
     *
     * @return string
     */
    public function addItem(AddOrderRequest $request)
    {
        $done = gachet()->cart()->addToActiveCart($request->model->id, $request->count);

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback' => 'reloadCart()',
        ]);
    }



    /**
     * Removes the specified order form the active cart.
     *
     * @param RemoveItemRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function removeItem(RemoveItemRequest $request)
    {
        $order = $request->model;
        $cart  = $request->cart;

        if ($order->remove()) {
            $cart->refresh();
            return view($this->module()->getBladePath('cart.content'), compact('cart'));
        } else {
            return $this->abort(503);
        }
    }



    /**
     * Modifies the specified order of the active cart.
     *
     * @param ModifyItemRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function modifyItem(ModifyItemRequest $request)
    {
        $order = $request->model;
        $cart  = $request->cart;

        if (!$request->count) {
            return view($this->module()->getBladePath('cart.content'), compact('cart'));
        }

        if ($order->alterCount($request->count)) {
            $cart->refresh();
            return view($this->module()->getBladePath('cart.content'), compact('cart'));
        } else {
            return $this->abort(410);
        }
    }



    /**
     * Applies the requested coupon code on the active cart.
     *
     * @param CartAddCouponCodeRequest $request
     *
     * @return string
     */
    public function addCoupon(CartAddCouponCodeRequest $request)
    {
        $cart = gachet()->cart()->findActiveCart();
        $code = $request->code;


        return $this->jsonAjaxSaveFeedback($cart->applyCouponCode($code), [
             'success_callback' => "reloadCartContent()",
             'success_message'  => $this->module()->getTrans('cart.alert.success.coupon-applied'),
             'danger_message'   => $this->module()->getTrans('cart.alert.error.coupon-code-is-not-valid'),
        ]);
    }



    /**
     * Removes the applied coupon code form the active cart.
     *
     * @return string
     */
    public function removeCoupon()
    {
        $cart = gachet()->cart()->findActiveCart();
        $cart->clearAppliedDiscount();


        return $this->jsonAjaxSaveFeedback(1, [
             'success_callback' => "reloadCartContent()",
             'success_message'  => $this->module()->getTrans('cart.alert.success.coupon-removed'),
        ]);
    }



    /**
     * Reloads the content of the cart page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reloadCartContent()
    {
        $cart = gachet()->cart()->findActiveCart();

        return view($this->module()->getBladePath('cart.content'), compact('cart'));
    }



    /**
     * save delivery method in meta field
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function deliveryCart(SimpleYasnaRequest $request)
    {
        $cart = gachet()->cart()->findActiveCart();

        $cart->batchSave(['delivery_method' => $request->delivery]);

        return ['success' => $cart->exists];


    }



    /**
     * save factor request in cart meta field
     *
     * @param SimpleYasnaRequest $request
     */
    public function factorCart(SimpleYasnaRequest $request)
    {

        $cart = gachet()->cart()->findActiveCart();

        if ($request->factor_request === '0' || $request->factor_request === '1') {

            $cart->batchSave(['factor_request' => $request->factor_request]);

        }

    }



    /**
     * save delivery time in meta field
     *
     * @param DeliveryRequest $request
     *
     * @return array
     */
    public function deliveryTimeCart(deliveryRequest $request)
    {

        $cart = gachet()->cart()->findActiveCart();

        $cart->batchSave(['delivery_time' => $request->delivery_time]);

        return ['success' => $cart->exists];


    }



    /**
     * save delivery time in meta field
     *
     * @param workDayRequest $request
     *
     * @return array
     */
    public function workDayCart(workDayRequest $request)
    {

        $cart = gachet()->cart()->findActiveCart();

        $cart->batchSave(['work_day' => $request->work_day]);

        return ['success' => $cart->exists];


    }
}
