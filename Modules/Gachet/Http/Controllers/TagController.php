<?php

namespace Modules\Gachet\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use Modules\Gachet\Services\Controller\GachetFrontControllerAbstract;

class TagController extends GachetFrontControllerAbstract
{
    /**
     * find posts which have the specified tag.
     *
     * @param Tag $tag
     *
     * @return void
     */
    protected function findPostsWithTag(Tag $tag)
    {
        $this->appendToVariables('posts', $tag->posts()->simplePaginate(24));
    }



    /**
     * Index Page of a Tag
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $slug = urldecode($request->tag);
        $tag  = model('tag', $slug);

        if ($tag->not_exists) {
            return $this->abort(404);
        }

        $this->findPostsWithTag($tag);
        if (!$this->variables['posts']->count()) {
            return $this->abort(404);
        }

        $this->appendToVariables('tag', $tag);

        return $this->template('tag.main', $this->variables);
    }


}
