<?php

namespace Modules\Gachet\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Gachet\Providers\CommentServiceProvider;
use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;

class PostController extends GachetFrontControllerAbstract
{
    protected $general_variables = [
         'commentsLimit'       => 10,
         'commentRepliesLimit' => 3,
    ];



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(Request $request)
    {
        return $this->appendToVariables('hashid', $hashid = $this->identifierToHashid($request->identifier))
                    ->appendToVariables('post', $post = post()->grabHashid($hashid))
                    ->appendToVariables('post_view', PostServiceProvider::showPost($post, [
                         'showError' => false,
                         'variables' => $this->general_variables,
                    ]))
                    ->general_variables['post_view']
             ? $this->template('posts.layouts.single.plane')
             : $this->abort(404);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function productSingle(Request $request)
    {
        $ware = model('Ware')->grabHashid($request->wareOrTitle);

        if ($ware->exists) {
            $this->general_variables['selected_ware'] = $ware;
        }

        return $this->single($request);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function eventsAjax(Request $request)
    {
        if (!$request->ajax()) {
            return null;
        }

        $conditions = [
             'type'         => 'events',
             'feature'      => null,
             'ajax_request' => true,
             'max_per_page' => 5,
        ];

        switch ($request->type) {
            case 'waiting':
                $conditions['conditions'] = [
                     ['event_starts_at', '>=', Carbon::now()],
                     ['event_ends_at', '>=', Carbon::now()],
                ];
                break;

            case 'expired':
                $conditions['conditions'] = [
                     ['event_starts_at', '<=', Carbon::now()],
                     ['event_ends_at', '<=', Carbon::now()],
                ];
                break;
        }

        return PostServiceProvider::showList($conditions);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function moreComments(Request $request)
    {
        $post = $this->findPostByIdentifier($request);

        if (
             !$post or
             !($posttype = $post->posttype) or
             !$posttype->exists or
             !$posttype->spreadMeta()->commenting_receive or
             !$posttype->commenting_display
        ) {
            return $this->abort(404, true);
        }

        if ($request->comment) {
            if (
                 !($comment = model('comment', $request->comment)) or
                 !$comment->exists
            ) {
                return $this->abort(404, true);
            }
            $comments                          = CommentServiceProvider::selectCommentReplies(
                 $comment,
                 $this->general_variables['commentRepliesLimit']
            );
            $this->general_variables['parent'] = $comment;
        } else {
            $comments = CommentServiceProvider::selectPostComments($post, $this->general_variables['commentsLimit']);
        }


        return view(
             $this->module()->getBladePath('comment.comments-loop'),
             array_merge(compact('comments', 'post'), $this->general_variables)
        );
    }



    /**
     * @param $identifier
     *
     * @return string
     */
    protected function identifierToHashid($identifier)
    {
        return str_after($identifier, 'P');
    }



    /**
     * @param Request $request
     *
     * @return \Modules\Yasna\Services\YasnaModel|null
     */
    protected function findPostByIdentifier(Request $request)
    {
        return $this->findPostWithHashid($this->identifierToHashid($request->identifier));
    }



    /**
     * @param $hashid
     *
     * @return \Modules\Yasna\Services\YasnaModel|null
     */
    protected function findPostWithHashid($hashid)
    {
        return (($post = model('post', $hashid) and $post->exists))
             ? $post
             : null;
    }
}
