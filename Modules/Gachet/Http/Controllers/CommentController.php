<?php

namespace Modules\Gachet\Http\Controllers;

use Modules\Gachet\Http\Requests\CommentSaveRequest;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;

class CommentController extends GachetFrontControllerAbstract
{

    protected $indexes_to_change = [
         'first_and_last_name' => 'guest_name',
         'email'               => 'guest_email',
         'tel'                 => 'guest_mobile',
         'message'             => 'text',
         'question'            => 'text',
    ];

    protected $request;



    /**
     * Saves a comment.
     *
     * @param CommentSaveRequest $request
     *
     * @return string
     */
    public function save(CommentSaveRequest $request)
    {
        $this->request = $request;

        $saved_comment = model('comment')
             ->batchSave($this->changeIndexes($request->toArray()));


        return $this->jsonAjaxSaveFeedback($saved_comment->exists, [
             'success_message'      => $this->module()->getTrans('general.message.success.send-message'),
             'success_form_reset'   => 1,
             'success_feed_timeout' => 5000,

             'danger_message' => $this->module()->getTrans('general.message.error.send-message'),
        ]);
    }



    /**
     * Changes the indexes to be match with the database.
     *
     * @param array $data
     *
     * @return array
     */
    protected function changeIndexes(array $data)
    {
        foreach ($this->indexes_to_change as $from => $to) {
            if (array_key_exists($from, $data)) {
                $data[$to] = $data[$from];
                unset($data[$from]);
            }
        }

        return array_default($data, $this->additionalIndexes());
    }



    /**
     * Returns an array to be added as automatic data
     *
     * @return array
     */
    protected function additionalIndexes()
    {
        return [
             'guest_ip'    => request()->ip(),
             'user_id'     => user()->id,
             'posttype_id' => $this->request->getPost()->posttype->id,
        ];
    }
}
