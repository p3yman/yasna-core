<?php

namespace Modules\Gachet\Http\Controllers;

use Modules\Gachet\Http\Requests\OfferRequest;
use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;
use Modules\Shop\Entities\ShopOffer;

class OffersController extends GachetFrontControllerAbstract
{
    /**
     * The Main ShopOffer Object
     *
     * @var ShopOffer|null
     */
    protected $offer;



    /**
     * Renders the list view of the products with the requested offer.
     *
     * @param OfferRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function products(OfferRequest $request)
    {
        $this->offer = $request->model;

        $this->appendToVariables('list', $this->innerContent());
        $this->appendToVariables('posttype', posttype(gachet()->getPosttypeSlug('product')));
        $this->appendToVariables('offer', $this->offer);

        return $this->template('posts.layouts.list.plane');
    }



    /**
     * Renders and returns the inner content.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function innerContent()
    {
        $all_posts = $this->findAllPosts();

        if (!$all_posts->count()) {
            return PostServiceProvider::showError(
                 $this->module()->getTrans('general.message.error.no-result'),
                 false);
        }


        return view(
             $this->module()->getBladePath('posts.list.product.main'),
             [
                  'allPosts'    => $all_posts,
                  'showFilter'  => true,
                  'ajaxRequest' => false,
                  'isBasePage'  => true,
                  'viewFolder'  => 'posts.list.product',
                  'offer'       => $this->offer,
             ]
        );
    }



    /**
     * Provides and returns the builder to find all posts.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function findAllPosts()
    {
        $posts = post()
             ->elector([
                  'criteria'    => 'published',
                  'locale'      => getLocale(),
                  'type'        => gachet()->getPosttypeSlug('product'),
                  'valid-offer' => $this->offer->id,
             ]);

        $this->appendToVariables('allPosts', $posts);

        return $posts;
    }


}
