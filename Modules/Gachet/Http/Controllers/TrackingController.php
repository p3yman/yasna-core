<?php

namespace Modules\Gachet\Http\Controllers;


use App\Models\Cart;
use Modules\Gachet\Http\Requests\TrackingRequest;
use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;

class TrackingController extends GachetFrontControllerAbstract
{
    /**
     * Renders a form to use the tracking feature.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->findTrackingStaticPost();

        return $this->template('tracking.index');
    }



    /**
     * Finds the static post for the tracking page and sets in the general variables.
     */
    protected function findTrackingStaticPost()
    {
        $post = PostServiceProvider::selectPosts([
                  'type' => gachet()->getPosttypeSlug('static'),
                  'slug' => 'tracking',
             ])->first() ?? post();

        $this->appendToVariables('static_post', $post);
    }



    /**
     * Finds a cart with its code
     *
     * @param TrackingRequest $request
     *
     * @return string
     * @throws \Throwable
     */
    public function track(TrackingRequest $request)
    {
        $cart = model('cart')->smartSearchCode($request->code);

        return $this->jsonAjaxSaveFeedback($cart->exists, [
             'success_message'  => $this->trackSuccessMessage($cart),
             'success_redirect' => $this->trackSuccessCallback($cart),

             'danger_message' => $this->module()->getTrans('cart.alert.error.tracking-cart-not-found'),
        ]);
    }



    /**
     * Returns the success message of the tracking process.
     *
     * @param Cart $cart
     *
     * @return array|string|null
     * @throws \Throwable
     */
    protected function trackSuccessMessage(Cart $cart)
    {
        if ($this->userCanViewCart($cart)) {
            return $this->module()->getTrans('cart.alert.success.tracking-cart-found');
        } else {
            return $this->template('tracking.response', compact('cart'))->render();
        }
    }



    /**
     * Returns the success callback of the tracking process.
     *
     * @param Cart $cart
     *
     * @return bool|string
     */
    protected function trackSuccessCallback(Cart $cart)
    {
        if ($this->userCanViewCart($cart)) {
            return route('gachet.payment.callback', ['cart' => $cart->hashid]);
        } else {
            return false;
        }
    }



    /**
     * Whether the client can view the found cart or not.
     *
     * @param Cart $cart
     *
     * @return bool
     */
    protected function userCanViewCart(Cart $cart)
    {
        // If the logged in user is the owner of the found cart.
        return (user()->exists and ($cart->user_id == user()->id));
    }
}
