<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/18/18
 * Time: 9:57 PM
 */

namespace Modules\Gachet\Http\Controllers;


use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class CatalogController extends GachetFrontControllerAbstract
{
    /**
     * Renders the list of catalogs.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function list(SimpleYasnaRequest $request)
    {
        $this->setCatalogItems($request);

        return $this->template('catalog.main');
    }



    /**
     * Sets the catalogs list in the view parameters.
     */
    protected function setCatalogItems(SimpleYasnaRequest $request)
    {
        $builder = PostServiceProvider::selectPosts([
             'type' => gachet()->getPosttypeSlug('product'),
        ])->whereNotNull('catalog')
                                      ->where('catalog', '!=', '')
                                      ->orderByDesc('published_at')
        ;

        if ($request->search) {
            $builder->where('title', 'like', "%$request->search%");
        }

        $catalog_posts = $builder
             ->paginate(48)
             ->appends($request->except('page'))
        ;

        $this->appendToVariables('catalog_posts', $catalog_posts);
    }
}
