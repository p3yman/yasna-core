<?php

namespace Modules\Gachet\Http\Controllers\Traits;

use Modules\Gachet\Services\Module\ModuleHandler;

trait ModuleControllerTrait
{
    /**
     * @return string
     */
    public static function getModuleName()
    {
        return (ModuleHandler::create(self::class))->getAlias();
    }
}
