<?php

namespace Modules\Gachet\Http\Controllers;

use App\Models\Label;
use App\Models\Posttype;
use App\Models\ShopOffer;
use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;
use Modules\Gachet\Services\ProductFilter\FilterHash;
use Modules\Gachet\Services\ProductFilter\ProductFilter;
use Modules\Gachet\Services\Products\Products;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class ProductsController extends GachetFrontControllerAbstract
{
    /**
     * @var Label
     */
    protected $desired_category;

    /**
     * @var Label
     */
    protected $root_category;

    /**
     * @var ShopOffer
     */
    protected $desired_offer;

    /**
     * @var SimpleYasnaRequest
     */
    protected $request;



    /**
     * Renders the list page of the products.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function list(SimpleYasnaRequest $request)
    {
        $this->request = $request;


        if ($this->desiredCategoryNotExists() or $this->rootCategoryNotExists()) {
            return $this->abort(404);
        }

        return $this->template('posts.layouts.list.plane', $this->viewParameters());
    }



    /**
     * Renders the filter response.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function filter(SimpleYasnaRequest $request)
    {
        $this->request = $request;

        $posts = $this->filterPosts('applyCategoryOnFilter');

        return view(
             $this->module()->getBladePath('posts.list.product.main'),
             [
                  'posts'       => $posts,
                  'viewFolder'  => 'posts.list.product',
                  'isBasePage'  => false,
                  'ajaxRequest' => true,
                  'showFilter'  => false,
             ]
        );
    }



    /**
     * Renders the filter response for the offer list.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function offerFilter(SimpleYasnaRequest $request)
    {
        $this->request = $request;


        if ($this->desiredOfferNotExists()) {
            return $this->abort(404);
        }

        $posts = $this->filterPosts('applyOfferOnFilter');

        return view(
             $this->module()->getBladePath('posts.list.product.main'),
             [
                  'posts'       => $posts,
                  'viewFolder'  => 'posts.list.product',
                  'isBasePage'  => false,
                  'ajaxRequest' => true,
                  'showFilter'  => false,
             ]
        );
    }



    /**
     * Renders the base page for the products list with filter
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function base(SimpleYasnaRequest $request)
    {
        $this->request = $request;

        return $this->template('posts.layouts.list.plane', $this->viewParameters());
    }



    /**
     * Filters the products.
     *
     * @param string $calling_method
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function filterPosts(?string $calling_method = null)
    {
        $hash          = $this->request->hash;
        $translator    = new FilterHash($hash);
        $hash_array    = $translator->translate();
        $modified_hash = $this->modifyHash($hash_array, $hash);
        $filter        = new ProductFilter($hash_array);

        if ($calling_method) {
            $filter = $this->$calling_method($filter);
        }

        $max_per_page = ($this->posttype()->getMeta('max_per_page') ?: 12);
        $paginator    = $filter->paginate($max_per_page);
        $paginator->fragment('#' . $modified_hash);
        $paginator->setPath(url()->previous());

        return $paginator;
    }



    /**
     * Applies the desired category on the filter result.
     *
     * @param ProductFilter $filter
     *
     * @return ProductFilter
     */
    protected function applyCategoryOnFilter(ProductFilter $filter)
    {
        $filter->fallbackCategories($this->desiredCategory()->id);
        return $filter;
    }



    /**
     * Applies the desired offer on the filter result.
     *
     * @param ProductFilter $filter
     *
     * @return ProductFilter
     */
    protected function applyOfferOnFilter(ProductFilter $filter)
    {
        $filter->offer($this->desiredOffer());
        return $filter;
    }



    /**
     * Modifies the hash for the pagination links.
     *
     * @param array  $hash_array
     * @param string $hash
     *
     * @return mixed
     */
    protected function modifyHash($hash_array, $hash)
    {
        $categories_array = ($hash_array['checkbox']['category'] ?? []);
        $categories_array = (array)$categories_array;
        $ids              = array_map('hashid', $categories_array);
        $categories       = post()->definedLabels()->whereIn('id', $ids)->get();
        foreach ($categories as $category) {
            $hash = str_replace($category->hashid, $category->slug, $hash);
        }

        $brands_array = ($hash_array['checkbox']['brand'] ?? []);
        $brands_array = (array)$brands_array;
        $ids          = array_map('hashid', $brands_array);
        $brands       = post()->definedLabels()->whereIn('id', $ids)->get();
        foreach ($brands as $brand) {
            $hash = str_replace($brand->hashid, $brand->slug, $hash);
        }

        return $hash;
    }



    /**
     * If the root category not exits.
     *
     * @return bool
     */
    protected function rootCategoryNotExists()
    {
        return !$this->rootCategoryExists();
    }



    /**
     * If the root category exits.
     *
     * @return bool
     */
    protected function rootCategoryExists()
    {
        return $this->rootCategory()->exists;
    }



    /**
     * Returns the root category.
     *
     * @return Label
     */
    protected function rootCategory()
    {
        if (!$this->root_category) {
            $this->root_category = $this->findRootCategory();
        }

        return $this->root_category;
    }



    /**
     * Finds and returns the root category.
     *
     * @return Label
     */
    protected function findRootCategory()
    {
        $folder           = $this->categoriesFolder();
        $folder_id        = $folder->id;
        $desired_category = $this->desiredCategory();
        $parent           = $desired_category;

        while (true) {
            if (
                 $parent->not_exists
                 or
                 !$parent->parent_id
                 or
                 ($parent->parent_id == $folder_id)
            ) {
                return $parent;
            }

            $parent = $parent->parent();
        }

        return label();
    }



    /**
     * If the desired category not exists.
     *
     * @return bool
     */
    protected function desiredCategoryNotExists()
    {
        return !$this->desiredCategoryExists();
    }



    /**
     * If the desired category exists.
     *
     * @return bool
     */
    protected function desiredCategoryExists()
    {
        return $this->desiredCategory()->exists;
    }



    /**
     * Returns the desired category.
     *
     * @return Label
     */
    protected function desiredCategory()
    {
        if (!$this->desired_category) {
            $this->desired_category = $this->findDesiredCategory();
        }

        return $this->desired_category;
    }



    /**
     * Finds and returns the desired category.
     *
     * @return Label
     */
    protected function findDesiredCategory()
    {
        $label_slug = $this->request->label;

        if (!$label_slug) {
            return label();
        }


        return post()
                  ->definedLabels()
                  ->where('slug', $label_slug)
                  ->first() ?? label();
    }



    /**
     * If the desired offer not exists.
     *
     * @return bool
     */
    protected function desiredOfferNotExists()
    {
        return !$this->desiredOfferExists();
    }



    /**
     * If the desired offer exists.
     *
     * @return bool
     */
    protected function desiredOfferExists()
    {
        return $this->desiredOffer()->exists;
    }



    /**
     * Returns the desired offer.
     *
     * @return ShopOffer
     */
    protected function desiredOffer()
    {
        if (!$this->desired_offer) {
            $this->desired_offer = $this->findDesiredOffer();
        }

        return $this->desired_offer;
    }



    /**
     * Finds and returns the desired offer.
     *
     * @return ShopOffer
     */
    protected function findDesiredOffer()
    {
        $offer_slug = $this->request->offer;
        /** @var ShopOffer $offer_model */
        $offer_model = model('shop-offer');

        if (!$offer_slug) {
            return $offer_model;
        }


        return $offer_model
                  ->whereValid()
                  ->where('slug', $offer_slug)
                  ->first() ?? $offer_model;

    }



    /**
     * Returns the categories folder instance.
     *
     * @return Label
     */
    protected function categoriesFolder()
    {
        /** @var Products $products */
        $products = app('gachet.shop.products');

        return $products->categoriesLabelFolder();
    }



    /**
     * Returns the list view.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function getListView()
    {
        return $this->view(
             $this->module()->getBladePath('posts.list.product.main'),
             $this->listViewParameters()
        );
    }



    /**
     * Returns the parameters to be passed to the view.
     *
     * @return array
     */
    protected function viewParameters()
    {
        return [
             'list'          => $this->getListView(),
             'posttype'      => $this->posttype(),
             'root_category' => $this->rootCategory(),
        ];
    }



    /**
     * Returns the parameters for the list view.
     *
     * @return array
     */
    protected function listViewParameters()
    {
        return [
             'allPosts'    => $this->getAllPosts(),
             'viewFolder'  => 'posts.list.product',
             'isBasePage'  => true,
             'ajaxRequest' => false,
             'showFilter'  => true,

             'root_category' => $this->rootCategory(),
        ];
    }



    /**
     * Returns all the posts to show in a page.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    protected function getAllPosts()
    {
        return PostServiceProvider::collectPosts([
             'type'  => $this->posttype()->slug,
             'label' => $this->rootCategory()->slug,
        ]);
    }



    /**
     * Returns the products posttype instance.
     *
     * @return Posttype
     */
    protected function posttype()
    {
        /** @var Products $products */
        $products = app('gachet.shop.products');

        return $products->posttype();
    }


}
