<?php

namespace Modules\Gachet\Http\Controllers;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Gachet\Services\Controller\GachetFrontControllerTrait;
use Modules\Gachet\Http\Requests\DeclareRequest;
use Modules\Gachet\Http\Requests\RepurchaseRequest;
use Modules\Gachet\Services\Module\CurrentModuleHelper;

class PaymentController extends Controller
{
    use GachetFrontControllerTrait;

    public function repurchase(RepurchaseRequest $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction))) {
            return $this->abort(410);
        }

        $newTransaction = $transaction->clone($request->only('account_id'));

        return $this->jsonFeedback([
            'ok'           => 1,
            'redirect'     => CurrentModuleHelper::action('PaymentController@fire', [
                 'transaction' => $newTransaction->getModel()->hashid,
            ]),
            'message'      => CurrentModuleHelper::trans('form.feed.wait'),
            'redirectTime' => 3000,
        ]);
    }

    public function fire(Request $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction))) {
            return $this->abort(410);
        }


        $transactionResponse = $transaction->fire();

        if ($transactionResponse['slug'] == 'returnable-response') {
            return $transactionResponse['response'];
        }

        return redirect(CurrentModuleHelper::action('PurchaseController@paymentCallback', [
             'cart' => hashid_encrypt($transaction->getModel()->invoice_id, 'ids'),
        ]));
    }

    public function declare(DeclareRequest $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction)) or !$transaction->isDeclarable()) {
            return $this->abort(410);
        }

        if ($transaction->getModel()->amount != $request->amount) {
            $transaction = $transaction->clone();
            $transaction->amount($request->amount);
        }


        $rs = $transaction->resnum($request->trackingCode)
                          ->declared_at($request->declarationDate)
                          ->update()
        ;

        if ($rs['code'] > 0) {
            $transaction
                 ->getModel()
                 ->cart()
                 ->update([
                      'disbursed_at' => Carbon::now(),
                      'disbursed_by' => user()->id,
                 ])
            ;
            return $this->jsonFeedback([
                 'ok'           => 1,
                 'message'      => $this->getModule()->getTrans('form.feed.wait'),
                 'refresh'      => 1,
                 'redirectTime' => 3000,
            ]);
        } else {
            return $this->abort(404);
        }
    }

    protected function findTransaction($hashid)
    {
        if (($transaction = model('Transaction')->grabHashid($hashid)) and $transaction->exists) {
            return $transaction->handler;
        }

        return null;
    }
}
