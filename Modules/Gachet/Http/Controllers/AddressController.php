<?php

namespace Modules\Gachet\Http\Controllers;

use Modules\Address\Http\Controllers\AddressController as OriginalController;
use Modules\Gachet\Http\Requests\AddressSaveRequest;

class AddressController extends OriginalController
{
    /**
     * Saves the requested address.
     *
     * @param AddressSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function customSave(AddressSaveRequest $request)
    {
        return parent::save($request);
    }
}
