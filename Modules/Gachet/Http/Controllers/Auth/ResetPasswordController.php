<?php namespace Modules\Gachet\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Gachet\Events\PasswordResetRequested;
use Modules\Gachet\Http\Requests\Auth\PasswordTokenRequest;
use Modules\Gachet\Http\Requests\Auth\ResetPasswordRequest;
use Modules\Gachet\Http\Requests\NewPasswordRequest;
use Modules\Gachet\Services\Controller\GachetFrontControllerAbstract;
use Modules\Gachet\Services\Controllers\Traits\YasnaControllerAlternativeTrait;
use Modules\Gachet\Services\Email\EmailServiceProvider;
use Modules\Gachet\Services\Module\CurrentModuleHelper;

class ResetPasswordController extends GachetFrontControllerAbstract // extends Yasna
{
    use YasnaControllerAlternativeTrait;

    public static $reset_token_length       = 6;
    public static $reset_token_lifetime     = 15; // in minutes
    public static $reset_token_session_name = 'resetting_password_id';



    /**
     * Accespts the request to reset password.
     *
     * @param ResetPasswordRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(ResetPasswordRequest $request)
    {
        $user = $this->findUser($request);

        if (!$user) {
            return $this->jsonAjaxSaveFeedback(false, [
                 'ok'             => 0,
                 'danger_message' => CurrentModuleHelper::trans('user.password.user'),
            ]);
        }

        $token = $this->generateToken();

        $this->saveToken($user, $token);
        $this->saveSession($user);
        $this->raiseRequestEvent($user, $token);

        return $this->jsonAjaxSaveFeedback(true, [
             'success_message'  => $this->module()->getTrans('user.password.sent'),
             'success_redirect' => route('password.token'),
             'redirectTime'     => 2000,
        ]);
    }



    /**
     * Renders a form to get token from the client.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getToken(Request $request)
    {
        $user_id = session()->get(self::$reset_token_session_name);
        $user    = model('user', $user_id);

        if ($user_id and $user->not_exists) {
            return $this->redirectToRequestPage();
        }

        $have_code = ($request->haveCode == 'code');
        if (!$user_id and !$have_code) {
            return $this->redirectToRequestPage();
        }

        $email = $user->email;

        return $this->template('auth.passwords.token', compact('email'));
    }



    /**
     * Checks the token specified in the request.
     *
     * @param PasswordTokenRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkToken(PasswordTokenRequest $request)
    {
        session()->put(static::$reset_token_session_name, $request->model->id);

        return $this->jsonAjaxSaveFeedback(true, [
             'success_message'  => $this->module()->getTrans('user.password.token-verified'),
             'success_redirect' => route('password.reset'),
             'redirectTime'     => 2000,
        ]);
    }



    /**
     * Renders a form to get the new password from the user.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function newPassword()
    {
        if (session()->has(self::$reset_token_session_name)) {
            return $this->template('auth.passwords.new');
        } else {
            return $this->redirectToRequestPage();
        }
    }



    /**
     * Returns a response to redirect the user to the first form to request a reset password token.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function redirectToRequestPage()
    {
        return redirect()->route('password.request');
    }



    /**
     * Changes the password of the user
     *
     * @param NewPasswordRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePassword(NewPasswordRequest $request)
    {
        $user_id = session()->get(static::$reset_token_session_name);
        $user    = model('user', $user_id);

        if (!$user_id or $user->not_exists) {
            return $this->abort(403);
        }

        $saved = $user->batchSaveBoolean([
             'password'           => Hash::make($request['new_password']),
             'reset_token'        => null,
             'reset_token_expire' => null,
        ]);

        auth()->loginUsingId($user->id);

        return $this->jsonAjaxSaveFeedback($saved, [
             'success_message'  => $this->module()->getTrans('user.password.reset'),
             'success_redirect' => route('home'),
             'redirectTime'     => 2000,
        ]);
    }



    /**
     * Finds a user with its email.
     *
     * @param Request $request
     *
     * @return null|User
     */
    protected function findUser(Request $request)
    {
        $user = model('user')->grab($request->email, 'email');

        if ($user and $user->exists) {
            return $user;
        }

        return null;
    }



    /**
     * Generates an returns a new random token.
     *
     * @return int
     */
    protected function generateToken()
    {
        $start = str_repeat(1, self::$reset_token_length);
        $end   = str_repeat(9, self::$reset_token_length);

        return rand($start, $end);
    }



    /**
     * Saves the given token for the specified user.
     *
     * @param User   $user
     * @param string $reset_token
     *
     * @return bool
     */
    protected function saveToken(User $user, $reset_token)
    {
        return $user->batchSaveBoolean([
             'reset_token'        => Hash::make($reset_token),
             'reset_token_expire' => now()->addMinute($this->getTokenLifeTime())->toDateTimeString(),
        ]);
    }



    /**
     * Saves the identifier of the user which requested for password reset in the session storage.
     *
     * @param User $user
     */
    protected function saveSession(User $user)
    {
        session()->put(static::$reset_token_session_name, $user->id);
    }



    /**
     * Raises an event for password resetting request.
     *
     * @param User   $user
     * @param string $token
     */
    protected function raiseRequestEvent(User $user, string $token)
    {
        event(new PasswordResetRequested($user, $token));
    }



    /**
     * Returns the life time of tokens in minutes.
     *
     * @return int
     */
    protected function getTokenLifeTime()
    {
        return setting('password_token_expire_time')->gain() ?: self::$reset_token_lifetime;
    }
}
