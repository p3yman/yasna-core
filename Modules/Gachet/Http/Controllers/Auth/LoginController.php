<?php namespace Modules\Gachet\Http\Controllers\Auth;

use Modules\Gachet\Providers\CartServiceProvider;
use Modules\Gachet\Services\Module\CurrentModuleHelper;
use Modules\Gachet\Services\Module\ModuleHandler;
use Modules\Yasna\Http\Controllers\Auth\LoginController as YasnaLoginController;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class LoginController extends YasnaLoginController
{
    use ModuleRecognitionsTrait;
    protected $login_blade = "auth.login";



    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->setLoginBlade();

        parent::__construct();
    }



    /**
     * @inheritdoc
     */
    public function redirectAfterLogin()
    {
        if (session()->has('drawingCode')) {
            $target = CurrentModuleHelper::action('UserController@drawing');
        } elseif (user()->isAdmin()) {
            $target = url('manage');
        } else {
            $target = url('/');
        }

        return redirect()->intended($target);
    }



    /**
     * @inheritdoc
     */
    public function showLoginForm()
    {
        $this->setIntendSession();

        return parent::showLoginForm();
    }



    /**
     * Sets the login blade.
     */
    protected function setLoginBlade()
    {
        $this->login_blade = $this->runningModule()->getBladePath($this->login_blade);

    }



    /**
     * Sets the intended session if the redirect field has be specified in the request.
     */
    protected function setIntendSession()
    {
        if (!request()->has('redirect')) {
            return;
        }

        $redirect = strtolower(request('redirect'));

        if ($redirect == 'referrer') {
            session(['url.intended' => url()->previous()]);
            return;
        }
    }
}
