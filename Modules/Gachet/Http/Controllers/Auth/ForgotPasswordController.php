<?php namespace Modules\Gachet\Http\Controllers\Auth;

use Modules\Gachet\Services\Controllers\Traits\TemplateControllerTrait;
use Modules\Gachet\Services\Controllers\Traits\YasnaControllerAlternativeTrait;
use Modules\Yasna\Http\Controllers\Auth\ForgotPasswordController as YasnaForgotPasswordController;

class ForgotPasswordController extends YasnaForgotPasswordController
{
    use YasnaControllerAlternativeTrait;
    use TemplateControllerTrait;



    /**
     * Renders the form to reset password.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showLinkRequestForm()
    {
        return $this->template('auth.passwords.contact');
    }
}
