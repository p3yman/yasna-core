<?php namespace Modules\Gachet\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Gachet\Http\Requests\Auth\RegisterUserRequest;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;
use Modules\Gachet\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Events\NewUserRegistered;
use Modules\Yasna\Events\UserLoggedIn;

class RegisterController extends GachetFrontControllerAbstract
{
    use RedirectsUsers;
    protected $register_blade = "auth.register";



    /**
     * RegisterController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
        parent::__construct();
    }



    /**
     * @param RegisterUserRequest $request
     *
     * @return string
     */
    public function register(RegisterUserRequest $request)
    {
        $user = $this->createUser($request->all());
        $ok   = $user->exists;

        if ($ok) {
            $this->guard()->login($user);

            event(new NewUserRegistered($user));
            event(new UserLoggedIn($user));
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_message'  => $this->module()->getTrans('user.message.register-success'),
             'success_redirect' => $this->redirectPath(),
             'danger_message'   => $this->module()->getTrans('user.message.register-fail'),
        ]);
    }



    /**
     * Creates the user with the given data
     *
     * @param array $data
     *
     * @return null
     */
    protected function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);

        return model('User')->batchSave($data, ['password2']);
    }



    /**
     * @return string
     */
    protected function redirectTo()
    {
        return route('home');
    }



    /**
     * Returns the registration form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        if (request()->has('redirect')) {
            switch (strtolower(request('redirect'))) {
                case 'referrer':
                    session(['url.intended' => url()->previous()]);
                    break;
            }

            session()->save();
        }

        return $this->template('auth.register');
    }



    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
