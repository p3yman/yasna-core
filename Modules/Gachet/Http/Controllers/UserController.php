<?php

namespace Modules\Gachet\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Gachet\Http\Requests\ProfileSaveRequest;
use Modules\Gachet\Http\Requests\TicketReplyRequest;
use Modules\Gachet\Http\Requests\TicketSaveRequest;
use Modules\Gachet\Http\Requests\TicketSingleRequest;
use Modules\Gachet\Providers\DrawingCodeServiceProvider;
use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Services\Controller\GachetFrontControllerAbstract;
use Modules\Gachet\Services\Module\CurrentModuleHelper;

class UserController extends GachetFrontControllerAbstract
{
    public function index()
    {
        $commentingPostPreview = PostServiceProvider::showPost('customers-comments', [
            'type'      => 'commenting',
            'feature'   => null,
            'showError' => false,
        ]);

        return $this->template('user.dashboard.main', compact('commentingPostPreview'));
    }

    public function orders()
    {
        $orders = \user()->carts()->whereNotNull('raised_at')->orderby('created_at', 'desc')->get();

        return $this->template('user.orders.main', compact('orders'));
    }

    public function orderInfo(Request $request)
    {
        $cart = model('Cart')->grabHashid($request->order);

        if (!$cart or !$cart->exists) {
            return $this->abort(404);
        }

        return view(CurrentModuleHelper::bladePath('user.orders.modal-content'), compact('cart'));
    }

    public function profile()
    {
        return $this->template('user.profile.main');
    }

    public function update(ProfileSaveRequest $request)
    {
        $request = $request->all();
        if ($request['new_password']) {
            $request['password'] = Hash::make($request['new_password']);
        }
        $request['id'] = user()->id;
        if ($request['marital'] != 2) {
            $request['marriage_date'] = null;
        }
        return $this->jsonAjaxSaveFeedback(user()->batchSaveBoolean($request, ['new_password', 'new_password2']), [
            'success_refresh' => 1,
        ]);
    }

    public function drawing()
    {
        $receipt = session()->get('drawingCode');
        if ($receipt) {
            $receipt     = decrypt($receipt);
            $receiptInfo = DrawingCodeServiceProvider::checkCode($receipt);
            if (!$receiptInfo) {
                session()->forget('drawingCode');
            }

            $foundReceipt = model("Receipt")->grab($receipt, 'code');
            if ($foundReceipt and $foundReceipt->exists) {
                session()->forget('drawingCode');
            }

            $newReceiptData = [
                'user_id'          => user()->id,
                'code'             => $receipt,
                'purchased_at'     => Carbon::createFromTimestamp($receiptInfo['date'], 'Asia/Tehran')
                    ->setTimezone('UTC'),
                'purchased_amount' => $receiptInfo['price'],
            ];
            model('Receipt')->batchSaveId($newReceiptData);

            if (setting()->ask('send_sms_on_register_code')->gain()) {
                $smsText = CurrentModuleHelper::trans('general.drawing.message.register-code-success-please-wait', [
                        'name' => \user()->name_first,
                    ]) . "\n\r"
                    . setting()->ask('site_url')->gain();

                $sendingResult = sendSms(user()->mobile, $smsText);
                $sendingResult = json_decode($sendingResult);
            }

            session()->forget('drawingCode');
            session()->forget('drawing_try');
        }

        $receipts = \user()->receipts()->paginate(20);
        return $this->template('user.drawing.main', compact('receipts'));
    }

    public function events(Request $request)
    {
        $currentEventsHTML = PostServiceProvider::showList([
            'type'         => 'events',
            'feature'      => null,
            'conditions'   => [
                ['event_starts_at', '<=', Carbon::now()],
                ['event_ends_at', '>=', Carbon::now()],
            ],
            'max_per_page' => 5,
            'showError'    => false,
        ]);

        $waitingCount = PostServiceProvider::selectPosts([
            'type'       => 'events',
            'conditions' => [
                ['event_starts_at', '>=', Carbon::now()],
                ['event_ends_at', '>=', Carbon::now()],
            ],
        ])->count();

        $expiredCount = PostServiceProvider::selectPosts([
            'type'       => 'events',
            'conditions' => [
                ['event_starts_at', '<=', Carbon::now()],
                ['event_ends_at', '<=', Carbon::now()],
            ],
        ])->count();

        return $this->template('user.events.main', compact(
            'currentEventsHTML',
            'waitingCount',
            'expiredCount'
        ));
    }



    /**
     * Show list of user's addresses
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addresses(Request $request)
    {
        $provinces_combo  = model('state')->where('parent_id', 0)->pluck('title', 'id')->toArray();
        $default_province = array_search(CurrentModuleHelper::config('default-province'), $provinces_combo);

        $cities_combo = model('state')->where('parent_id', $default_province)->pluck('title', 'id')->toArray();
        $default_city = array_search(CurrentModuleHelper::config('default-city'), $cities_combo);


        $addresses = addresses()
             ->user(user())
             ->orderBy('created_at', 'desc')
             ->paginate(12)
        ;

        return $this->template('user.addresses.main', compact(
             'provinces_combo',
             'default_province',
             'cities_combo',
             'default_city',
             'addresses'
        ));
    }

    public function addressesRow(Request $request)
    {
        $address = model('address', $request->hashid);
        if ($address and $address->exists) {
            return view(CurrentModuleHelper::bladePath('user.addresses.item'), compact('address'));
        }

        return '';
    }



    /**
     * Renders the list of tickets.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tickets()
    {
        $tickets = user()
             ->tickets()
             ->withType('default')
             ->where('parent_id', 0)
             ->get()
        ;

        return $this->template('user.tickets.list', compact('tickets'));
    }



    /**
     * Renders the form to save a new ticket.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ticketNew()
    {
        return $this->template('user.tickets.new');
    }



    /**
     * Renders the preview of a single ticket.
     *
     * @param TicketSingleRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function ticketItem(TicketSingleRequest $request)
    {
        $ticket = $request->model;

        if ($ticket->not_exists) {
            return redirect()->route('gachet.users.tickets.new');
        }

        return $this->template('user.tickets.single', compact('ticket'));
    }



    /**
     * Saves a ticket.
     *
     * @param TicketSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ticketSave(TicketSaveRequest $request)
    {
        $ticket_type = ticket($request->ticket_type);

        $saved = $ticket_type->addRemark([
             'title' => $request->title,
             'text'  => $request->text,
             'flag'  => $request->priority,
        ]);

        return $this->jsonSaveFeedback($saved->exists, [
             'success_redirect' => route('gachet.users.tickets.single', ['hashid' => $saved->hashid]),
        ]);
    }



    /**
     * Saves a reply to a ticket.
     *
     * @param TicketReplyRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ticketReply(TicketReplyRequest $request)
    {
        $reply = $request
             ->model
             ->addReply($request->only('text'));

        return $this->jsonSaveFeedback($reply->exists, [
             'success_refresh' => true,
        ]);
    }
}
