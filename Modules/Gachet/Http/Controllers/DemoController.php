<?php

namespace Modules\Gachet\Http\Controllers;

use Modules\Gachet\Services\Controller\GachetFrontControllerAbstract;
use Modules\Yasna\Services\YasnaController;

class DemoController extends GachetFrontControllerAbstract
{
    protected $base_model  = ""; // @TODO <~~
    protected $view_folder = "gachet::demo"; // @TODO <~~ (Remove if same as parent)



    /**
     * Displays tickets list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tickets()
    {
        return $this->template('gachet::demo.tickets.list');
    }



    /**
     * Displays tickets single
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ticketSingle()
    {
        return $this->template('gachet::demo.tickets.single');
    }



    /**
     * Displays New Ticket
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ticketNew()
    {
        return $this->template('gachet::demo.tickets.new');
    }



    /**
     * Displays product compare
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function compare()
    {
        return $this->template('gachet::demo.compare.main');
    }



    /**
     * Displays products catalog
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function catalog()
    {
        return $this->template('gachet::demo.catalog.main');
    }
}
