<?php

namespace Modules\Gachet\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Gachet\Services\Controller\GachetFrontControllerTrait;

class TestController extends Controller
{
    use GachetFrontControllerTrait;

    public function productFilterForm()
    {
        return $this->template('test.product-filter-form.main');
    }
}
