<?php

namespace Modules\Gachet\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Gachet\Providers\PostServiceProvider;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class FrontController extends GachetFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->indexSlideShow()
                    ->indexAbout()
                    ->indexAboutVideo()
                    ->indexEvent()
                    ->indexProductsFolders()
                    ->indexBlog()
                    ->indexCustomersComments()
                    ->template('home.main')
             ;
    }



    /**
     * @return $this
     */
    protected function indexSlideShow()
    {
        return $this->appendToVariables('slideshow', PostServiceProvider::selectPosts([
             'type'     => gachet()->getPosttypeSlug('home-slider'),
             'criteria' => 'published',
        ])->orderBy('order')->get());
    }



    /**
     * @return $this
     */
    protected function indexAbout()
    {
        return $this->appendToVariables('about', PostServiceProvider::selectPosts([
             'type' => gachet()->getPosttypeSlug('static'),
             'slug' => 'about',
        ])->first() ?: post());
    }



    /**
     * return video section for home page
     *
     * @return $this
     */
    protected function indexAboutVideo()
    {
        return $this->appendToVariables('about_video', PostServiceProvider::selectPosts([
             'type' => gachet()->getPosttypeSlug('static'),
             'slug' => 'about-video',
        ])->first() ?: post());
    }



    /**
     * @return $this
     */
    protected function indexEvent()
    {
        return $this->appendToVariables('event', PostServiceProvider::selectPosts([
             'type' => gachet()->getPosttypeSlug('event'),
        ])->first() ?: post());
    }



    /**
     * @return $this
     */
    protected function indexProductsFolders()
    {
        return $this->appendToVariables(
             'product_folders',
             app('gachet.shop.products')->rootCategoriesLabels()
        );
    }



    /**
     * @return $this
     */
    protected function indexBlog()
    {
        return $this->indexBlogPosttype()->indexBlogPosts();
    }



    /**
     * @return $this
     */
    protected function indexBlogPosttype()
    {
        return $this->appendToVariables('blog_posttype', posttype()->grabSlug(gachet()->getPosttypeSlug('blog')));
    }



    /**
     * @return $this
     */
    protected function indexBlogPosts()
    {
        return $this->appendToVariables(
             'blog_posts',
             PostServiceProvider::collectPosts([
                  'type'       => gachet()->getPosttypeSlug('blog'),
                  'limit'      => 8,
                  'pagination' => false,
             ])
        );
    }



    /**
     * @return $this
     */
    public function indexCustomersComments()
    {
        $this->customersCommentsPost();

        $general_commenting_post = $this->general_variables['customers_comments_post'];
        $general_comments        = $general_commenting_post
             ->comments()
             ->whereNotNull('approved_at')
             ->orderBy('approved_at', 'desc')
             ->limit(10)
             ->get()
        ;


        return $this->appendToVariables('customers_comments', $general_comments);
    }



    /**
     * @return $this
     */
    public function customersCommentsPost()
    {
        return $this->appendToVariables('customers_comments_post', PostServiceProvider::selectPosts([
             'type' => gachet()->getPosttypeSlug('commenting'),
             'slug' => 'customers-comments',
        ])->first() ?: post());
    }



    /**
     * Renders the contact page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return $this->contactSettingInfo()
                    ->contactCommentingPost()
                    ->appendToVariables('contact_text', PostServiceProvider::selectPosts([
                         'type' => gachet()->getPosttypeSlug('static'),
                         'slug' => 'contact',
                    ])->first() ?: post())
                    ->template('contact.main')
             ;
    }



    /**
     * Reads needed settings from the DB and sets as variables to be passed to the blade.
     *
     * @return $this
     */
    protected function contactSettingInfo()
    {
        $setting_slugs = [
             'address',
             'fax',
             'email',
             'location',
             'telegram_link',
             'twitter_link',
             'facebook_link',
             'instagram_link',
             'whatsapp_call',
             'work_hours',
             'shop_manager',
             'postal_code',
        ];

        foreach ($setting_slugs as $setting_slug) {
            $this->appendToVariables($setting_slug, get_setting($setting_slug));
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function contactCommentingPost()
    {
        return $this->appendToVariables('commenting_post',
             PostServiceProvider::selectPosts([
                  'type' => gachet()->getPosttypeSlug('commenting'),
                  'slug' => 'contact',
             ])->first() ?: post()
        );
    }



    /**
     * Renders the preview of a page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function page(SimpleYasnaRequest $request)
    {
        $this->pageFindPost($request);

        if ($this->pagePostExists()) {
            return $this->template('page.main');
        } else {
            return $this->abort(404);
        }
    }



    /**
     * Finds the requested post and add it to the variables.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return $this
     */
    protected function pageFindPost(SimpleYasnaRequest $request)
    {
        $post = PostServiceProvider::selectPosts([
             'type' => gachet()->getPosttypeSlug('static'),
             'slug' => $request->page,
        ])->first()
        ;

        return $this->appendToVariables('post', $post);
    }



    /**
     * Whether the post of the page has been founded.
     *
     * @return bool
     */
    protected function pagePostExists()
    {
        return boolval($this->general_variables['post']);
    }
}
