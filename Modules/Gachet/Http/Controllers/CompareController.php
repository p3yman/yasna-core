<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/19/18
 * Time: 3:18 PM
 */

namespace Modules\Gachet\Http\Controllers;


use App\Models\Ware;
use Modules\Gachet\Http\Requests\CompareAddRequest;
use Modules\Gachet\Http\Requests\CompareRemoveRequest;
use Modules\Gachet\Services\Controllers\GachetFrontControllerAbstract;

class CompareController extends GachetFrontControllerAbstract
{
    /**
     * Provides the index page of the compare.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        if (gachet()->compare()->isEmpty()) {
            return redirect()->route('gachet.home');
        }

        $compare_array = gachet()->compare()->getArray();
        $products      = $this->indexProducts($compare_array);
        $groups        = $this->indexGroups($compare_array);

        return $this->template('compare.main', compact('compare_array', 'products', 'groups'));
    }



    /**
     * Returns an array of products for the compare.
     *
     * @param array $compare_array
     *
     * @return array
     */
    protected function indexProducts(array $compare_array)
    {
        return array_map(function ($item) {
            $post  = post($item['hashid']);
            $image = fileManager()
                 ->file($post->featured_image)
                 ->config('featured_image')
                 ->posttype(posttype(gachet()->getPosttypeSlug('product')))
                 ->version('thumb')
                 ->getUrl()
            ;
            /** @var Ware $ware */
            $ware     = $post->wares()->first();
            $currency = gachet()->shop()->usableCurrency();

            return [
                 "title"          => $post->title,
                 "image"          => $image,
                 "link"           => $post->direct_url,
                 "remove_link"    => route('gachet.compare.remove', ['hashid' => $post->hashid]),
                 "sale_price"     => $ware ? $ware->salePrice()->in($currency)->get() : null,
                 "original_price" => $ware ? $ware->originalPrice()->in($currency)->get() : null,
            ];
        }, $compare_array['items']);
    }



    /**
     * Returns an array of groups for the compare.
     *
     * @param array $compare_array
     *
     * @return array
     */
    protected function indexGroups(array $compare_array)
    {
        $groups  = [];
        $headers = $compare_array['headers'];
        $items   = $compare_array['items'];

        foreach ($headers as $header_key => $sub_headers) {
            if (!is_array($sub_headers)) {
                continue;
            }

            $group = [
                 'title' => $header_key,
            ];

            foreach ($sub_headers as $sub_header) {
                $subgroup = [
                     'title' => $sub_header,
                ];
                foreach ($items as $item) {
                    $value = $item[$header_key][$sub_header]['value'];

                    $subgroup['cols'][] = $value;
                }

                $group['rows'][] = $subgroup;
            }


            $groups[] = $group;
        }

        return $groups;
    }



    /**
     * Adds the specified product form compare.
     *
     * @param CompareAddRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(CompareAddRequest $request)
    {
        $ok = gachet()
             ->compare()
             ->addItem($request->model)
        ;

        if ($ok) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false]);
        }
    }



    /**
     * Remove the specified product from compare.
     *
     * @param CompareRemoveRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function removeItem(CompareRemoveRequest $request)
    {
        gachet()->compare()->removeItem($request->model);

        return redirect()->route('gachet.compare.index');
    }
}
