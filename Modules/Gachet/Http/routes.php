<?php

use Modules\Gachet\Providers\RouteServiceProvider;

/*
|--------------------------------------------------------------------------
| Manage
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::group(['prefix' => 'convert', 'namespace' => 'Modules\Gachet\Http\Controllers'], function () {
        Route::get('{methodTitle}', 'ConvertController@convertMain');
        Route::get('users/{offset}', 'ConvertController@users')->name('convert.users');
        Route::get('posts/{offset}', 'ConvertController@posts')->name('convert.posts');
    });

    Route::group(
         [
              'middleware' => ['auth', 'is:admin'],
              'prefix'     => 'manage/gachet',
              'namespace'  => 'Modules\Gachet\Http\Controllers\Manage',
         ],
         function () {
             /*
             |--------------------------------------------------------------------------
             | Drawing
             |--------------------------------------------------------------------------
             |
             */
             Route::group(['prefix' => 'draw'], function () {
                 Route::get('{hashid}', 'DrawingController@draw')->name('gachet.manage.draw');
                 Route::get('{hashid}/winners', 'DrawingController@winners')->name('gachet.manage.draw.winners');
                 Route::post('prepare', 'DrawingController@prepareForDraw')->name('gachet.manage.draw.prepare');
                 Route::post('select', 'DrawingController@select')->name('gachet.manage.draw.select');
                 Route::get('act/{hashid}/{viewFile}', 'DrawingController@singleAction')
                      ->name('gachet.manage.single-action')
                 ;
                 Route::get('delete/{key}', 'DrawingController@delete')->name('gachet.manage.draw.delete');
             });
         }
    );


    Route::group([
         'namespace'  => 'Modules\Gachet\Http\Controllers\Auth',
    ],
         function () {
             Route::get('home', 'LoginController@home')->name('home');

             Route::get('login', 'LoginController@showLoginForm')->name('login');
             Route::post('login', 'LoginController@login');

             Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
             Route::post('register', 'RegisterController@register');

             Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
             Route::post('password/reset', 'ResetPasswordController@reset');
             Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');

             Route::get('password/token/{haveCode?}', 'ResetPasswordController@getToken')->name('password.token');
             Route::post('password/token', 'ResetPasswordController@checkToken');
             Route::get('password/new', 'ResetPasswordController@newPassword')->name('password.reset');
             Route::post('password/new', 'ResetPasswordController@changePassword');
         }
    );

    Route::get('pay/{cart}', 'Modules\Gachet\Http\Controllers\PurchaseController@paymentShortLink');

    Route::group(
         [
              'namespace'  => 'Modules\Gachet\Http\Controllers',
         ],
         function () {
             Route::get('/', 'FrontController@index')->name('gachet.home');

             Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
                 Route::get('dashboard', 'UserController@index')->name('gachet.user.dashboard');

                 Route::get('orders', 'UserController@orders')->name('gachet.user.orders');
                 Route::get('orders/{order}/info', 'UserController@orderInfo');

                 Route::get('profile', 'UserController@profile')->name('gachet.user.profile');
                 Route::post('profile', 'UserController@update')->name('gachet.user.profile.save');

                 Route::get('drawing', 'UserController@drawing')->name('gachet.user.drawing');

                 //Route::get('events', 'UserController@events');

                 Route::get('addresses', 'UserController@addresses')->name('gachet.user.addresses');
                 Route::get('addresses/{hashid}', 'UserController@addressesRow')->name('gachet.user.addresses.row');
                 //            Route::get('/dashboard/previous_comments/{post_id}', 'UserController@previousComments');

                 Route::get('tickets', 'UserController@tickets')->name('gachet.users.tickets');
                 Route::get('tickets/new', 'UserController@ticketNew')->name('gachet.users.tickets.new');
                 Route::get('tickets/{hashid}', 'UserController@ticketItem')->name('gachet.users.tickets.single');

                 Route::post('tickets-save', 'UserController@ticketSave')->name('gachet.users.tickets.save');
                 Route::post('reply', 'UserController@ticketReply')->name('gachet.users.tickets.reply');
             });

             Route::group(['prefix' => 'address', 'middleware' => 'auth'], function () {
                 Route::post('save', 'AddressController@customSave')->name('gachet.address.save');
             });

             Route::group(['prefix' => 'cart'], function () {
                 Route::get('/', 'CartController@index')->name('gachet.cart.index');
                 Route::get('flush', 'CartController@flush')->name('gachet.cart.flush');
                 Route::post('modify', 'CartController@modifyCart')->name('gachet.cart.modify');
                 Route::post('add-coupon', 'CartController@addCoupon')->name('gachet.cart.coupon');
                 Route::post('remove-coupon', 'CartController@removeCoupon')->name('gachet.cart.remove-coupon');
                 Route::get('reload-content', 'CartController@reloadCartContent')->name('gachet.cart.reloadContent');
                 Route::post('delivery', 'CartController@deliveryCart')->name('gachet.cart.delivery');
                 Route::post('factor', 'CartController@factorCart')->name('gachet.cart.factor');
                 Route::post('/delivery-time', 'CartController@deliveryTimeCart')->name('gachet.cart.delivery_time');
                 Route::post('/work-day', 'CartController@workDayCart')->name('gachet.cart.work_day');



                 Route::group(['prefix' => 'item'], function () {
                     Route::post('add', 'CartController@addItem')->name('gachet.cart.item.add');
                     Route::post('remove', 'CartController@removeItem')->name('gachet.cart.item.remove');
                     Route::post('{hashid}', 'CartController@modifyItem')->name('gachet.cart.item.modify');
                 });

                 Route::group(['prefix' => 'view'], function () {
                     Route::get('reload', 'CartController@reloadView')->name('gachet.cart.reload-view');
                 });
             });

             Route::group(['prefix' => 'purchase'], function () {
                 Route::get('user', 'PurchaseController@step1')->name('gachet.purchase.step1');

                 Route::group(['middleware' => 'auth'], function () {
                     Route::get('shipping', 'PurchaseController@step2')->name('gachet.purchase.step2');
                     Route::get('shipping/content', 'PurchaseController@reloadStep2')
                          ->name('gachet.purchase.step2.reload')
                     ;
                     Route::get('shipping/verify', 'PurchaseController@verifyStep2')
                          ->name('gachet.purchase.step2.verify')
                     ;

                     Route::get('review', 'PurchaseController@step3')->name('gachet.purchase.step3');
                     Route::get('review/verify', 'PurchaseController@verifyStep3')
                          ->name('gachet.purchase.step3.verify')
                     ;

                     Route::get('pay', 'PurchaseController@step4')->name('gachet.purchase.step4');

                     Route::post('final', 'PurchaseController@final')->name('gachet.purchase.final');

                     Route::get('payment/result/{cart}', 'PurchaseController@paymentCallback')
                          ->name('gachet.payment.callback')
                          ->middleware('ownCart')
                     ;
                 });
             });

             Route::group(['prefix' => 'payment'], function () {
                 Route::post('repurchase/{transaction}', 'PaymentController@repurchase')
                      ->name('gachet.payment.repurchase')
                 ;
                 Route::get('fire/{transaction}', 'PaymentController@fire');
                 Route::post('declare/{transaction}', 'PaymentController@declare')->name('gachet.payment.declare');

                 Route::get('cash-callback/{cart}/{hashid}', 'PurchaseController@cashCallback')
                      ->name('gachet.purchase.cash-callback')
                      ->middleware('ownCart')
                 ;
             });

             Route::group(['prefix' => 'purchase'], function () {
                 Route::get('reload', 'CartController@reloadView');
             });


             Route::group(['prefix' => 'province'], function () {
                 Route::get('cities/{province}/select', 'ProvinceController@getProvinceCitiesSelect')
                      ->name('gachet.province.cities-select')
                 ;
             });

             Route::get('events/{type}', 'PostController@eventsAjax')
                  ->where('type', '^(waiting|expired){1,1}$')
             ;

             Route::group(['prefix' => 'drawing-code'], function () {
                 Route::post('submit', 'DrawingController@submitCode');
             });

             Route::get('contact', 'FrontController@contact')->name('gachet.contact');

             Route::get('products/search', 'PosttypeController@productsSearch')->name('gachet.products.search');
             Route::get('products/catalogs', 'CatalogController@list')->name('gachet.catalogs');

             Route::group(['prefix' => 'products/compare'], function () {
                 Route::get('', 'CompareController@index')->name('gachet.compare.index');
                 Route::get('add/{hashid}', 'CompareController@add')->name('gachet.compare.add');
                 Route::get('remove/{hashid}', 'CompareController@removeItem')->name('gachet.compare.remove');
             });

             Route::group(['prefix' => 'archive'], function () {
                 Route::get('{posttype}/categories', 'PosttypeController@showCategories')
                      ->name('gachet.categories-list')
                 ;
                 Route::get('{posttype}/filter', 'PosttypeController@filter')->name('gachet.posts.list.filter');
                 Route::get('{posttype}/{category?}', 'PosttypeController@showList')->name('gachet.posts.list');
             });
             foreach (RouteServiceProvider::getStaticPosttypes() as $posttype) {
                 Route::get("$posttype/categories", "PosttypeController@{$posttype}Categories")
                      ->name("gachet.$posttype.categories")
                 ;
                 Route::get("$posttype/{category?}", "PosttypeController@$posttype")->name("gachet.$posttype.archive");
             }

             Route::get('{identifier}/more-comments/{comment?}', 'PostController@moreComments')
                  ->where('identifier', '^P(\w|)+$')
                  ->name('gachet.post.more-comments')
             ;

             Route::get('{identifier}/{title?}', 'PostController@single')
                  ->where('identifier', '^P(\w|)+$')
             ;


             Route::get('product/{identifier}/{wareOrTitle?}/{title?}', 'PostController@productSingle')
                  ->where('identifier', '^P(\w|)+$')
                  ->name('gachet.product.single')
             ;
             Route::get('offer/{slug}', 'OffersController@products')
                  ->name('gachet.products.offer')
             ;

             Route::group(['prefix' => 'comment'], function () {
                 Route::post('save', 'CommentController@save')->name('gachet.comment.save');
             });

             Route::get('tag/{tag}', 'TagController@index')->name('gachet.tag');

             Route::get('page/{page}', 'FrontController@page')->name('gachet.page');

             Route::get('tracking','TrackingController@index')->name('gachet.tracking');
             Route::post('tracking','TrackingController@track');

             Route::group(['prefix' => 'test', 'middleware' => 'is:developer'], function () {
                 Route::get('product-filter-form', 'TestController@productFilterForm');
                 Route::get('tickets', 'DemoController@tickets');
                 Route::get('tickets/single', 'DemoController@ticketSingle');
                 Route::get('tickets/new', 'DemoController@ticketNew');
                 Route::get('compare', 'DemoController@compare');
                 Route::get('catalog', 'DemoController@catalog');
             });
         });
});
