<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Gachet')->getControllersNamespace(),
     'prefix'     => 'products',
], function () {
    Route::get('base', 'ProductsController@base')->name('gachet.products.brands.base');
    Route::get('filter/{label?}', 'ProductsController@filter')->name('gachet.products.filter');
    Route::get('{label}', 'ProductsController@list')->name('gachet.products.list');
    Route::get('offer/filter/{offer}', 'ProductsController@offerFilter')->name('gachet.products.offer.filter');
});
