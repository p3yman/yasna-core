<?php namespace Modules\Gachet\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Gachet\Providers\CartServiceProvider;
use Modules\Yasna\Events\UserLoggedIn;

class UserCartsManagement implements ShouldQueue
{
    public $tries = 5;

    public function handle(UserLoggedIn $event)
    {
        dd($event, __FILE__ . ' - ' . __LINE__);
    }
}
