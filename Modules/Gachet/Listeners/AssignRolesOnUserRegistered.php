<?php

namespace Modules\Gachet\Listeners;

use Modules\Yasna\Events\NewUserRegistered;

class AssignRolesOnUserRegistered
{
    /**
     * Handle the event.
     *
     * @param NewUserRegistered $event
     *
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        $user = $event->model;

        $user->attachRole('customer');
    }
}
