<?php namespace Modules\Gachet\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Gachet\Providers\CartServiceProvider;
use Modules\Yasna\Events\UserLoggedIn;

class ManageUserCartOnUserLoggedIn
{
    public $tries = 5;

    public function handle(UserLoggedIn $event)
    {
        $user = $event->model;

        $sessionCart = CartServiceProvider::getSessionCart();
        if ($sessionCart->exists) {
            $dbCart = CartServiceProvider::getUsersCart($user);
            if ($dbCart->exists) {
                $dbCart->mergeWith($sessionCart->id);
                $dbCart->refreshCalculations();
            } else {
                $sessionCart->update(['user_id' => $user->id]);
            }
            CartServiceProvider::clearSession();
        }
    }
}
