<?php

namespace Modules\Gachet\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Gachet\Providers\GachetServiceProvider;
use Modules\Shop\Events\WareUpdated;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class HandleSpecificCurrenciesCacheWhenWareUpdated implements ShouldQueue
{
    use ModuleRecognitionsTrait;

    protected $ware;
    protected $updating_data = [];

    protected $fetched = [];



    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(WareUpdated $event)
    {
        $this->ware = $event->model;

        if ($this->isTrashed()) {
            return;
        }

        foreach ($this->priceTypes() as $price_type) {
            $this->handlePriceType($price_type);
        }

        $this->ware->batchSave($this->updating_data);
    }



    /**
     * Checks if the ware has been trashed or not.
     *
     * @return bool
     */
    protected function isTrashed()
    {
        $ware = $this->ware;
        return ($ware->not_exists or $ware->isTrashed());
    }



    /**
     * Handle prices in the specific currencies with the given type.
     *
     * @param string $type
     */
    protected function handlePriceType(string $type)
    {
        if ($this->hasNotPriceInDefaultCurrency($type)) {
            return;
        }

        $default_price       = $this->getPriceInDefaultCurrency($type);
        $specific_currencies = $this->specificCurrencies();

        foreach ($specific_currencies as $currency) {
            $price = ($this->getPriceInCurrency($currency, $type) ?: $default_price);
            $this->updatePrice($currency, $type, $price);
        }
    }



    /**
     * Updates the price of the target ware.
     *
     * @param string $currency
     * @param string $type
     * @param float  $amount
     */
    protected function updatePrice(string $currency, string $type, $amount)
    {
        $column                       = strtolower($currency) . '_' . $type . '_price';
        $this->updating_data[$column] = $amount;
    }



    /**
     * Returns an array of specific currencies.
     *
     * @return array
     */
    protected function specificCurrencies()
    {
        $posttype_currencies = ($this->ware->posttype->currencies_array ?? []);
        $module_currencies   = $this
             ->getModuleProvider()
             ->currency()
             ->getSpecificCurrencies()
        ;

        return array_intersect($posttype_currencies, $module_currencies);
    }



    /**
     * Returns the default currency.
     *
     * @return string
     */
    protected function defaultCurrency()
    {
        return $this
             ->getModuleProvider()
             ->currency()
             ->getDefaultCurrency()
             ;
    }



    /**
     * Returns an instance of the `Modules\Gachet\Providers\GachetServiceProvider` class.
     *
     * @return GachetServiceProvider;
     */
    protected function getModuleProvider()
    {
        return $this->runningModule()->getProvider();
    }



    /**
     * Finds the price in the given currency and with the specified type.
     *
     * @param string $currency
     * @param string $type
     *
     * @return float
     */
    protected function findPrice(string $currency, string $type)
    {
        return $this->ware->price($type)->in($currency)->get();
    }



    /**
     * Returns the price in the given currency and with the specified type.
     *
     * @param string $currency
     * @param string $type
     *
     * @return float
     */
    protected function getPriceInCurrency(string $currency, string $type)
    {
        if (!isset($this->fetched[$type][$currency])) {
            $this->fetched[$type][$currency] = $this->findPrice($currency, $type);

        }

        return $this->fetched[$type][$currency];
    }



    /**
     * Checks if the target ware has price in the given currency and with the specified type.
     *
     * @param string $currency
     * @param string $type
     *
     * @return bool
     */
    protected function hasPriceInCurrency(string $currency, string $type)
    {
        return boolval($this->getPriceInCurrency($currency, $type));
    }



    /**
     * Checks if the target ware has not price in the given currency and with the specified type.
     *
     * @param string $currency
     * @param string $type
     *
     * @return bool
     */
    protected function hasNotPriceInCurrency(string $currency, string $type)
    {
        return !$this->hasNotPriceInCurrency($currency, $type);
    }



    /**
     * Returns the price of the target ware in the default currency and with the specified type.
     *
     * @param string $type
     *
     * @return float
     */
    protected function getPriceInDefaultCurrency(string $type)
    {
        return $this->getPriceInCurrency($this->defaultCurrency(), $type);
    }



    /**
     * Checks if the target ware has price in the default currency and with the specified type.
     *
     * @param string $type
     *
     * @return bool
     */
    protected function hasPriceInDefaultCurrency(string $type)
    {
        return $this->hasPriceInCurrency($this->defaultCurrency(), $type);
    }



    /**
     * Checks if the target ware has not price in the default currency and with the specified type.
     *
     * @param string $type
     *
     * @return bool
     */
    protected function hasNotPriceInDefaultCurrency(string $type)
    {
        return !$this->hasPriceInDefaultCurrency($type);
    }



    /**
     * Returns an array of types to be checked and handled.
     *
     * @return array
     */
    protected function priceTypes()
    {
        return ['sale', 'original'];
    }
}
