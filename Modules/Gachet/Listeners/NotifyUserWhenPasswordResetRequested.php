<?php

namespace Modules\Gachet\Listeners;

use Modules\Gachet\Events\PasswordResetRequested;
use Modules\Gachet\Notifications\PasswordResetNotification;

class NotifyUserWhenPasswordResetRequested
{
    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(PasswordResetRequested $event)
    {
        $user = $event->model;

        if ($user->not_exists) {
            return;
        }

        $user->notify(new PasswordResetNotification($event->token));
    }
}
