<?php

namespace Modules\Gachet\Listeners;

use App\Models\Cart;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Cart\Events\CartStatusChanged;

class SaveReceiptWhenCartStatusChanged implements ShouldQueue
{
    protected $event;
    protected $cart;



    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(CartStatusChanged $event)
    {
        $this->setEvent($event);

        if ($this->hasBeenDelivered()) {
            $this->handleNewlyDeliveredCart();
        } else {
            $this->handleUndeliveredCart();
        }
    }



    /**
     * Saves the event object on the listener object.
     *
     * @param CartStatusChanged $event
     *
     * @return void
     */
    protected function setEvent(CartStatusChanged $event)
    {
        $this->event = $event;
        $this->cart  = $event->model;
    }



    /**
     * Returns the cart model object.
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }



    /**
     * Weather the new status of cart is in delivered status.
     *
     * @return bool
     */
    public function hasBeenDelivered()
    {
        return ($this->getCart()->isDelivered());
    }



    /**
     * Makes needed actions for a newly delivered cart.
     */
    public function handleNewlyDeliveredCart()
    {
        $cart = $this->getCart();

        if ($cart->receipts()->count()) {
            return;
        }

        $this->storeReceipt(
             $cart->invoiced_amount,
             $cart->paid_at,
             $cart);
    }



    /**
     * Handle unpaid cart.
     */
    public function handleUndeliveredCart()
    {
        $cart = $this->getCart();

        $cart->receipts()->delete();
    }



    public function storeReceipt($amount, $timestamp, ?Cart $cart = null)
    {
        $cart = ($cart ?? model('cart'));
        $user = ($cart->user->exists) ? $cart->user : model('user');

        return model('receipt')
             ->batchSave([
                  'purchased_at'     => Carbon::parse($timestamp)->toDateTimeString(),
                  'user_id'          => $user->id,
                  'invoice_id'       => $cart->id,
                  'purchased_amount' => $amount,
                  'code'             => gachet()->draw()->createCode(
                       Carbon::parse($timestamp)->getTimestamp(),
                       $amount
                  ),
             ]);
    }
}
