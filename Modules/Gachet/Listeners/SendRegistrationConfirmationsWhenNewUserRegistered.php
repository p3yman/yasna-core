<?php

namespace Modules\Gachet\Listeners;

use Modules\Gachet\Notifications\RegistrationNotification;
use Modules\Yasna\Events\NewUserRegistered;

class SendRegistrationConfirmationsWhenNewUserRegistered
{
    /**
     * Handle the event.
     *
     * @param NewUserRegistered $event
     *
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        $user = $event->model;

        $user->notify(new RegistrationNotification());
    }
}
