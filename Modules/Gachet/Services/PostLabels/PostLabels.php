<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/3/19
 * Time: 11:05 AM
 */

namespace Modules\Gachet\Services\PostLabels;


use App\Models\Label;
use App\Models\Posttype;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;

class PostLabels
{
    /**
     * @var array
     */
    protected $categories_folders = [];



    /**
     * Returns a list of the labels which are the children of the categories label folder.
     *
     * @param Posttype $posttype
     *
     * @return Collection|BaseCollection|Label[]
     */
    public function rootCategoriesLabels(Posttype $posttype)
    {
        $folder = $this->categoriesLabelFolder($posttype);

        if ($folder->not_exists) {
            return collect();
        }

        return $folder->children()->orderByDesc('id')->get();
    }



    /**
     * Returns the instance of the categories label folder the given posttype.
     *
     * @param Posttype $posttype
     *
     * @return Label
     */
    public function categoriesLabelFolder(Posttype $posttype)
    {
        $posttype_slug = $posttype->slug;

        if (!array_key_exists($posttype_slug, $this->categories_folders)) {
            $this->categories_folders[$posttype_slug] = $this->freshCategoriesLabelFolder($posttype);
        }

        return $this->categories_folders[$posttype_slug];
    }



    /**
     * Returns a fresh instance of the categories label folder on the given posttype.
     *
     * @param Posttype $posttype
     *
     * @return Label
     */
    public function freshCategoriesLabelFolder(Posttype $posttype)
    {
        return $this->findLabelFolderBySlug($posttype, $this->categoriesFolderSlug());
    }



    /**
     * Finds the label folder with the given slug on the given posttype.
     *
     * @param Posttype $posttype
     * @param string   $slug
     *
     * @return Label
     */
    public function findLabelFolderBySlug(Posttype $posttype, string $slug)
    {
        return $posttype
                  ->labelFolders()
                  ->where('slug', $slug)
                  ->first() ?? label();
    }



    /**
     * Returns the slug of the categories folder.
     *
     * @return string
     */
    public function categoriesFolderSlug()
    {
        return 'category';
    }
}
