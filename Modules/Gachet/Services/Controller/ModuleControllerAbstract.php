<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:09 PM
 */

namespace Modules\Gachet\Services\Controller;

use Illuminate\Routing\Controller;

abstract class ModuleControllerAbstract extends Controller
{
    use ModuleControllerTrait;
}
