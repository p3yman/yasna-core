<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/31/19
 * Time: 3:19 PM
 */

namespace Modules\Gachet\Services\Products;


use App\Models\Label;
use App\Models\Posttype;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;
use Modules\Gachet\Services\PostLabels\PostLabels;

class Products
{
    /**
     * @var Label
     */
    protected $categories_folder;

    /**
     * @var Label
     */
    protected $brands_folder;



    /**
     * Returns a list of the labels which are the children of the categories label folder.
     *
     * @return Collection|BaseCollection|Label[]
     */
    public function rootCategoriesLabels()
    {
        return $this
             ->postLabels()
             ->rootCategoriesLabels($this->posttype())
             ;
    }



    /**
     * Returns the instance of the categories label folder.
     *
     * @return Label
     */
    public function categoriesLabelFolder()
    {
        if (!$this->categories_folder) {
            $this->categories_folder = $this->postLabels()->categoriesLabelFolder($this->posttype());
        }

        return $this->categories_folder;
    }



    /**
     * Returns the list of the brands.
     *
     * @return Collection|BaseCollection|Label[]
     */
    public function brands()
    {
        $folder = $this->brandsLabelFolder();

        if ($folder->not_exists) {
            return collect();
        }

        return $folder->children()->get();
    }



    /**
     * Returns the instance of the brands label folder.
     *
     * @return Label
     */
    public function brandsLabelFolder()
    {
        if (!$this->brands_folder) {
            $this->brands_folder = $this->freshBrandsLabelFolder();
        }

        return $this->brands_folder;
    }



    /**
     * Returns a fresh instance of the brands label folder.
     *
     * @return Label
     */
    public function freshBrandsLabelFolder()
    {
        return $this
                  ->posttype()
                  ->labelFolders()
                  ->where('slug', $this->brandsFolderSlug())
                  ->first() ?? label();
    }



    /**
     * Returns the instance of the products posttype model.
     *
     * @return Posttype
     */
    public function posttype()
    {
        return posttype($this->posttypeSlug());
    }



    /**
     * Returns the slug of the products posttype.
     *
     * @return string
     */
    public function posttypeSlug()
    {
        return gachet()->getPosttypeSlug('product');
    }



    /**
     * Returns the slug of the brands folder.
     *
     * @return string
     */
    protected function brandsFolderSlug()
    {
        return 'brand';
    }



    /**
     * Returns the instance of the `gachet.posts.labels` singleton.
     *
     * @return PostLabels
     */
    protected function postLabels()
    {
        return app('gachet.posts.labels');
    }
}
