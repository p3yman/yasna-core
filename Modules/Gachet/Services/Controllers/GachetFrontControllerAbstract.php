<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 1:13 PM
 */

namespace Modules\Gachet\Services\Controllers;


use Illuminate\Pagination\Paginator;
use Modules\Gachet\Services\Controllers\Traits\GachetFeedbackControllerTrait;
use Modules\Gachet\Services\Controllers\Traits\TemplateControllerTrait;

abstract class GachetFrontControllerAbstract extends GachetControllerAbstract
{
    use TemplateControllerTrait;
    use GachetFeedbackControllerTrait;

    protected $posttype = null;

    /**
     * An array to map post types with their slugs in the database.
     *
     * @var array
     */
    protected $posttype_slugs = [
         'slider'     => 'sliders',
         'static'     => 'pages',
         'event'      => 'events',
         'product'    => 'products',
         'commenting' => 'commenting',
         'blog'       => 'blog',
    ];



    /**
     * GachetFrontControllerAbstract constructor.
     * <br>
     * Sets dependencies.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setPaginationTemplate();
    }



    /**
     * Sets the pagination blade(s)
     */
    protected function setPaginationTemplate()
    {
        Paginator::defaultView($this->module()->getBladePath('pagination.default'));
    }
}
