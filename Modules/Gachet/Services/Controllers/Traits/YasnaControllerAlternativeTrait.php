<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 9:32 PM
 */

namespace Modules\Gachet\Services\Controllers\Traits;

use Illuminate\Support\Facades\View;
use Modules\Yasna\Services\ModuleHelper;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait YasnaControllerAlternativeTrait
 * This trait could be used in controllers which can not extend from the YasnaController. (ex: LoginController)
 *
 * @package Modules\Gachet\Services\Controllers\Traits
 */
trait YasnaControllerAlternativeTrait
{
    protected $view_folder = '';



    /**
     * Returns the save view depending on the view folder.
     *
     * @param string $view_file
     * @param bool   $arguments
     *
     * @return View|Response
     */
    protected function safeView($view_file, $arguments = false)
    {
        if ($this->view_folder and !str_contains($view_file, $this->view_folder)) {
            $view_file = "$this->view_folder.$view_file";
        }
        if (!View::exists($view_file)) {
            if (config('app.debug')) {
                return ss("view file [$view_file] not found!");
            } else {
                return $this->abort(404, true);
            }
        }

        return view($view_file, $arguments);
    }



    /**
     * Returns the save view depending on the view folder.
     *
     * @param string $view_file
     * @param array  $arguments
     *
     * @return View|Response
     */
    protected function view($view_file, $arguments = [])
    {
        return $this->safeView($view_file, $arguments);
    }



    /**
     * Returns the name of the running module.
     *
     * @return string
     */
    protected function moduleName()
    {
        $name  = get_class($this);
        $array = explode("\\", $name);

        if (array_first($array) == 'Modules') {
            return $array[1];
        }

        return null;
    }



    /**
     * Returns an instance of the ModuleHelper class based on the namespace of the current class.
     *
     * @return ModuleHelper
     */
    protected function module()
    {
        return module($this->moduleName());
    }



    /**
     * Returns the alias of the current module.
     *
     * @return string
     */
    public function moduleAlias()
    {
        return $this->module()->getAlias();
    }
}
