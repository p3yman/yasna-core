<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 12:23 PM
 */

namespace Modules\Gachet\Services\Controllers\Traits;


use App\Models\Menu;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Modules\Gachet\Providers\CartServiceProvider;

trait TemplateControllerTrait
{
    /**
     * An array to be passed to views as default.
     *
     * @var array
     */
    protected $general_variables = [];



    /**
     * Sets contact info in the template area.
     */
    protected function setTemplateContactInfo()
    {
        gachet()->template()->mergeWithContactInfo([
             'telephone' => get_setting('telephone'),
        ])
        ;
    }



    /**
     * Sets cart in the template area.
     */
    protected function setTemplateCart()
    {
        gachet()->template()->setCart(CartServiceProvider::findActiveCart());
    }



    /**
     * Sets the title the in the template area.
     */
    protected function setTemplateSiteTitle()
    {
        $setting_title = get_setting('site_title');

        gachet()->template()->setSiteTitle($setting_title);
        gachet()->template()->appendToPageTitle($setting_title);

    }



    /**
     * Sets the default value in the nav bar.
     */
    protected function setTemplateNavBar()
    {
        gachet()->template()->appendToNavBar([
             $this->module()->getTrans('general.home'),
             url('/'),
        ]);
    }



    /**
     * Sets the main menu in the template area.
     */
    protected function setMainMenu()
    {
        gachet()->template()->setMainMenu(menu('main')->items);
    }



    /**
     * Sets the footer menu in the template area.
     */
    protected function setFooterMenu()
    {
        gachet()->template()->setFooterMenu(menu('footer')->items);
    }



    /**
     * Sets variables of the template area.
     */
    protected function setTemplateVariables()
    {
        $this->setTemplateContactInfo();
        $this->setTemplateCart();
        $this->setTemplateSiteTitle();
        $this->setTemplateNavBar();
        $this->setMainMenu();
        $this->setFooterMenu();
    }



    /**
     * Sets needed values in the template properties
     * and then returns the view object to be rendered and shown.
     *
     * @param string $view_name  The path to the view which will be rendered as the response.
     * @param array  $parameters The parameters to be passed to the view
     *
     * @return View|Response
     */
    protected function template($view_name, array $parameters = [])
    {
        $view_name  = $this->templateModifyViewName($view_name);
        $parameters = array_merge($this->general_variables, $parameters);

        $this->setTemplateVariables();

        return $this->view($view_name, $parameters);
    }



    /**
     * Modifies and returns the view name based on the current module's alias.
     *
     * @param string $view_name
     *
     * @return string
     */
    protected function templateModifyViewName(string $view_name)
    {
        if ($view_name and !str_contains($view_name, '::')) {
            $view_name = $this->moduleAlias() . '::' . $view_name;
        }

        return $view_name;
    }



    /**
     * Appends the given value as the specified name to the general variable array.
     *
     * @param string $variable_name
     * @param mixed  $variable_value
     *
     * @return $this
     */
    protected function appendToVariables(string $variable_name, $variable_value)
    {
        $this->general_variables[$variable_name] = $variable_value;

        return $this;
    }
}
