<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 4:30 PM
 */

namespace Modules\Gachet\Services\Controllers\Traits;

use Symfony\Component\HttpFoundation\Response;

trait GachetFeedbackControllerTrait
{
    /**
     * An overwritten version of the `abort()` method in the `Modules\Yasna\Services\YasnaController`
     * which is effecting response's header by default.
     *
     * @param int|string $error_code    The HTTP error code.
     * @param bool       $is_minimal    Weather the returned blade should be minimal version.
     * @param bool       $effect_header Weather the HTTP header should be affected with the error code.
     * @param null|array $data          The data to be used while rendering the response.
     *
     * @return Response
     */
    public function abort($error_code, $is_minimal = false, $effect_header = true, $data = null)
    {
        return parent::abort($error_code, $is_minimal, $effect_header, $data);
    }
}
