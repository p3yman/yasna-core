<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 12:43 PM
 */

namespace Modules\Gachet\Services\Controllers\Traits;


use Modules\Gachet\Services\Module\ModuleHandler;

trait ModuleControllerTrait
{
    /**
     * ModuleControllerTrait constructor.
     * <br>
     * Sets dependencies.
     */
    public function __construct()
    {
        $this->makeViewFolderSafe();

        parent::__construct();
    }



    /**
     * Makes sure that the `$view_folder` property contains the module's alias.
     */
    protected function makeViewFolderSafe()
    {
        $view_folder = ($this->view_folder ?? null);
        if (is_string($view_folder) and !str_contains($view_folder, '::')) {
            $this->view_folder = $this->module()->bladePath($this->view_folder);
        }
    }
}
