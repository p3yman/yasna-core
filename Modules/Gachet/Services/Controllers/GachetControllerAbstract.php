<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 11:38 AM
 */

namespace Modules\Gachet\Services\Controllers;


use Modules\Gachet\Services\Controllers\Traits\ModuleControllerTrait;
use Modules\Yasna\Services\YasnaController;

abstract class GachetControllerAbstract extends YasnaController
{
    use ModuleControllerTrait;
}
