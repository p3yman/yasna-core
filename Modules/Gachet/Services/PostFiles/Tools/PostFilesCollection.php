<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 14/11/2017
 * Time: 10:49 AM
 */

namespace Modules\Gachet\Services\PostFiles\Tools;

use Illuminate\Support\Collection;

class PostFilesCollection extends Collection
{
    public function __construct(array $array)
    {
        foreach ($array as $item) {
            $this->push(new PostFile($item));
        }

        return parent::__construct($this->items);
    }
}
