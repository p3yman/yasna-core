<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 14/11/2017
 * Time: 10:48 AM
 */

namespace Modules\Gachet\Services\PostFiles\Tools;

class PostFile
{
    protected static $aparatPrefix = 'aparat.com/v/';
    protected $data = [];

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }

        $assessorName = self::getAssessorName($name);
        if (method_exists($this, $assessorName)) {
            return $this->$assessorName();
        }

        return null;
    }

    public function getAparatHashCodeProperty()
    {
        $link = $this->link;
        $aparatPrefix = self::$aparatPrefix;

        if ($this->isAparatLink()) {
            $this->aparat_hash_code = (str_after($link, $aparatPrefix) != $link)
                ? str_after($link, $aparatPrefix)
                : substr($link, strpos(strtolower($link), $aparatPrefix) + strlen($aparatPrefix));
            return $this->aparat_hash_code;
        }

        return $link;
    }

    public function isAparatLink()
    {
        $link = $this->link;
        $aparatPrefix = self::$aparatPrefix;

        if (is_string($link)) {
            $lower = strtolower($link);
            if (
                starts_with($lower, "https://www.$aparatPrefix") or
                starts_with($lower, "http://www.$aparatPrefix") or
                starts_with($lower, "https://$aparatPrefix") or
                starts_with($lower, "http://$aparatPrefix") or
                starts_with($lower, "www.$aparatPrefix") or
                starts_with($lower, "$aparatPrefix")
            ) {
                return true;
            }
        }

        return false;
    }

    protected static function getAssessorName(string $propertyName)
    {
        return 'get' . ucfirst(camel_case($propertyName)) . 'Property';
    }
}
