<?php

namespace Modules\Gachet\Services\AsanakSms\Facades;

use Illuminate\Support\Facades\Facade;

class AsanakSmsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AsanakSms';
    }
}
