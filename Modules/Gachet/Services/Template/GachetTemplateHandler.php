<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 11:52 AM
 */


namespace Modules\Gachet\Services\Template;

use Modules\Gachet\Services\Module\CurrentModuleHelper;

/**
 * Class IranstrickTemplateHandler
 *
 * @package Modules\Gachet\Services\Template
 */
class GachetTemplateHandler extends TemplateHandler
{
    protected static $site_title;
    protected static $page_title     = [];
    protected static $nav_bar        = [];
    protected static $main_menu      = [];
    protected static $red_title      = [];
    protected static $footer_menu    = [];
    protected static $languages_urls = [];
    protected static $site_logo_url;
    protected static $contact_info   = [];
    protected static $yasna_group_title;
    protected static $yasna_group_url;
    protected static $open_graphs    = [];
    protected static $cart;
    protected static $view_variables = [];



    /**
     * @retrun string
     */
    public static function siteLogoUrl()
    {
        if (is_null(static::$site_logo_url)) {
            if (
                 ($settingLogo = get_setting('site_logo')) and
                 ($logoFile = fileManager()->file($settingLogo)->getUrl())
            ) {
                static::$site_logo_url = $logoFile;
            } else {
                static::$site_logo_url = CurrentModuleHelper::asset('images/logo-' . getLocale() . '.png');
            }
        }

        return static::$site_logo_url;
    }
}
