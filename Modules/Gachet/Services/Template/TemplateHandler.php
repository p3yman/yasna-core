<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 11:40 AM
 */

namespace Modules\Gachet\Services\Template;

use Exception;

class TemplateHandler
{
    private static   $method_prefixes = [
         'appending' => 'appendTo',
         'merging'   => 'mergeWith',
         'imploding' => 'implode',
         'assigning' => 'set',
    ];
    protected static $__called_method = null;



    /**
     * Throws exception when unhandled method has been called.
     *
     * @throws Exception
     */
    private static function undefinedMethodException()
    {
        trigger_error(
             'Call to undefined method ' . __CLASS__ . '::' . static::$__called_method . '()', E_USER_ERROR
        );
    }



    /**
     * Throws exception when method parameters are not enough.
     *
     * @param int $passedArgumentsCount
     *
     * @throws Exception
     */
    private static function tooFewArgumentsException($passedArgumentsCount)
    {
        trigger_error(
             'Too few arguments to function '
             . __CLASS__ . '::' .
             static::$__called_method
             . ', '
             . $passedArgumentsCount
             . ' passed', E_USER_ERROR
        );
    }



    /**
     * Appends the given value the the specified property.
     *
     * @param string $property
     * @param array  $appending
     *
     * @return bool
     * @throws Exception
     */
    protected static function appendToProperty($property, $appending)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge(static::$$property, $appending);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * Merges the given value to the specified property.
     *
     * @param string $property
     * @param array  $merging
     *
     * @return bool
     * @throws Exception
     */
    protected static function mergeWithProperty($property, $merging)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge_recursive(static::$$property, $merging);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * Set the value of the specified property with as the given value.
     *
     * @param string $property
     * @param array  $parameters
     *
     * @return bool
     * @throws Exception
     */
    protected static function setProperty($property, $parameters)
    {
        if (property_exists(static::class, $property)) {
            if (count($parameters)) {
                static::$$property = $parameters[0];
            } else {
                static::tooFewArgumentsException(1);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * Implode the specified property with the given glue.
     *
     * @param string $property
     * @param array  $parameters
     *
     * @return string
     * @throws Exception
     */
    protected static function implodeProperty($property, $parameters)
    {
        if (property_exists(static::class, $property) and is_array($propertyVal = static::$$property)) {
            if (count($parameters)) {
                return implode($parameters[0], $propertyVal);
            } else {
                static::tooFewArgumentsException(1);
            }
        }

        self::undefinedMethodException();
    }



    /**
     * Handles the called method.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments)
    {
        static::$__called_method = $name;

        if (starts_with($name, self::$method_prefixes['appending'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['appending']));
            return self::appendToProperty($property, $arguments);
        } elseif (starts_with($name, self::$method_prefixes['merging'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['merging']));
            return self::mergeWithProperty($property, ...$arguments);
        } elseif (starts_with($name, self::$method_prefixes['imploding'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['imploding']));
            return self::implodeProperty($property, $arguments);
        } elseif (starts_with($name, self::$method_prefixes['assigning'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['assigning']));
            return self::setProperty($property, $arguments);
        } else {
            if (property_exists(static::class, ($property = snake_case($name)))) {
                return static::$$property;
            }
        }

        self::undefinedMethodException();
    }



    /**
     * Handles the static method called.
     *
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     * @throws Exception
     */
    public static function __callStatic($name, $arguments)
    {
        return (new static)->$name(...$arguments);
    }
}
