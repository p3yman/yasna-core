<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:12 PM
 */

namespace Modules\Gachet\Services\ProductFilter\Traits;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait OutputTrait
{
    /**
     * Returns a collection retrieved from the builder.
     *
     * @return Collection
     */
    public function run()
    {
        $this->makeQueryReady();

        return $this->getBuilder()->get();
    }



    /**
     * Paginates the result of the filter.
     *
     * @param int $limit
     *
     * @return LengthAwarePaginator
     */
    public function paginate($limit)
    {
        $obj = $this;
        Paginator::currentPageResolver(function () use ($obj) {
            return $obj->page;
        });

        $this->makeQueryReady();

        return $this->getBuilder()->paginate($limit);
    }



    /**
     * Returns a raw SQl of the filter's query.
     *
     * @return string
     */
    public function raw()
    {
        $this->makeQueryReady();

        return $this->getBuilder()->toRawSql();
    }



    /**
     * Throws bad request exception.
     *
     * @throws BadRequestHttpException
     */
    protected function badRequest()
    {
        throw new BadRequestHttpException();
    }
}
