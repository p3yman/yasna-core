<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:07 PM
 */

namespace Modules\Gachet\Services\ProductFilter\Traits;

use App\Models\Category;
use App\Models\ShopOffer;
use Modules\Gachet\Services\Products\Products;

trait CheckboxFilterTrait
{
    protected $categories = [];
    protected $brands     = [];
    protected $packages   = [];

    protected $fallback_category;


    protected $offer;



    /**
     * Applies a checkbox filter with the given conditions.
     *
     * @param array $filter_conditions
     *
     * @return $this
     */
    public function checkboxFilter(array $filter_conditions)
    {
        foreach ($filter_conditions as $condition_type => $condition) {
            $this->callMethod($condition_type, [$condition]);
        }

        return $this;
    }



    /**
     * Applies categories filter.
     *
     * @param array $categories
     *
     * @return $this
     */
    public function category($categories)
    {
        if (!is_array($categories)) {
            $categories = [$categories];
        }

        foreach ($categories as $category) {
            $this->setCategory($category);
        }

        return $this;
    }



    /**
     * Sets a category with its ID.
     *
     * @param string $category
     *
     * @return $this
     */
    protected function setCategory($category)
    {
        $category_id = $this->hashid2id($category);
        if ($category_id) {
            $this->categories = array_unique(array_merge($this->categories, [$category_id]));
        }

        return $this;
    }



    /**
     * Applies the specified package for filter.
     *
     * @param int|string|array $packages
     *
     * @return $this
     */
    public function package($packages)
    {
        if (!is_array($packages)) {
            $packages = [$packages];
        }

        foreach ($packages as $package) {
            $this->setPackage($package);
        }

        return $this;
    }



    /**
     * Sets a package with its ID.
     *
     * @param string $package
     *
     * @return $this
     */
    protected function setPackage($package)
    {
        $packageId = $this->hashid2id($package);
        if ($packageId) {
            $this->packages = array_unique(array_merge($this->packages, [$packageId]));
        }

        return $this;
    }



    /**
     * Converts ID to hashid.
     *
     * @param $hashid
     *
     * @return array|int|null|string
     */
    protected function hashid2id($hashid)
    {
        $dehashed = hashid($hashid);

        if ($dehashed) {
            return $dehashed;
        }

        return null;
    }



    /**
     * Applies checkboxes filters.
     *
     * @return $this
     */
    protected function applyCheckboxes()
    {
        return $this->applyCategories()
                    ->applyPackages()
                    ->applyBrands()
                    ->applyOffer()
             ;
    }



    /**
     * Applies categories filters.
     *
     * @return $this
     */
    protected function applyCategories()
    {
        $categories = $this->applicableCategories();

        if (is_array($categories) and !empty($categories) and !$this->offer) {

            $builder = $this->getBuilder();
            $builder->whereHas('labels', function ($query) use ($categories) {
                $query->whereIn('labels.id', $categories);
            });
            $this->setBuilder($builder);
        }

        return $this;
    }



    /**
     * Returns an array of all applicable categories.
     *
     * @return array
     */
    protected function applicableCategories()
    {
        if (!empty($this->categories)) {
            return post()
                 ->definedLabels()
                 ->whereIn('id', $this->categories)
                 ->pluck('id')
                 ->toArray()
                 ;
        }

        if ($this->fallback_category) {
            return [$this->fallback_category];
        }

        return [];
    }



    /**
     * Applies packages filter.
     *
     * @return $this
     */
    protected function applyPackages()
    {
        if (($packages = $this->packages) and count($packages)) {
            $builder = $this->getBuilder();
            $builder->whereIn('wares.package_id', $packages);
            $this->setBuilder($builder);
        }

        return $this;
    }



    /**
     * Checks if package has be specified or not.
     *
     * @return bool
     */
    protected function packageSpecified()
    {
        return boolval(count($this->packages));
    }



    /**
     * Sets the `$fallback_category` property.
     *
     * @param int $categories
     *
     * @return $this
     */
    public function fallbackCategories(int $categories)
    {
        $this->fallback_category = $categories;

        return $this;
    }



    /**
     * Applies the brands for filter.
     *
     * @param array $brands
     *
     * @return $this
     */
    public function brand($brands)
    {
        if (!is_array($brands)) {
            $brands = [$brands];
        }

        foreach ($brands as $brand) {
            $this->setBrand($brand);
        }

        return $this;
    }



    /**
     * Sets the brand to be used in filter.
     *
     * @param string $brand
     *
     * @return $this
     */
    protected function setBrand($brand)
    {
        $category_id = $this->hashid2id($brand);
        if ($category_id) {
            $this->brands = array_unique(array_merge($this->brands, [$category_id]));
        }

        return $this;
    }



    /**
     * Applies brands filter.
     *
     * @return $this
     */
    protected function applyBrands()
    {
        $brands = $this->applicableBrands();

        if (is_array($brands) and !empty($brands)) {

            $builder = $this->getBuilder();
            $builder->whereHas('labels', function ($query) use ($brands) {
                $query->whereIn('labels.id', $brands);
            });
            $this->setBuilder($builder);
        }

        return $this;
    }



    /**
     * Returns an array of applicable brands.
     *
     * @return array
     */
    protected function applicableBrands()
    {
        if (empty($this->brands)) {
            /** @var Products $products */
            $products = app('gachet.shop.products');
            return $products
                 ->brandsLabelFolder()
                 ->children()
                 ->get()
                 ->pluck('id')
                 ->toArray()
                 ;
        }

        return post()
             ->definedLabels()
             ->whereIn('id', $this->brands)
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Sets the specified offer as the main offer of the filter result.
     *
     * @param ShopOffer $offer
     *
     * @return $this
     */
    public function offer(ShopOffer $offer)
    {
        $this->offer = $offer;

        return $this;
    }



    /**
     * Applies the offer to the filter result.
     *
     * @return $this
     */
    protected function applyOffer()
    {
        if (!$this->offer) {
            return $this;
        }

        $builder = $this->getBuilder();

        $builder->withValidOffer($this->offer->id);

        $this->setBuilder($builder);

        return $this;
    }
}
