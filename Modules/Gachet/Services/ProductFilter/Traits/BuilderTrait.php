<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:13 PM
 */

namespace Modules\Gachet\Services\ProductFilter\Traits;

use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;

trait BuilderTrait
{
    protected $builder = null;

    protected $conditions = [];

    protected static $products_posttype              = 'products';
    protected static $selecting_columns_instructions = [
         'posts' => [
              'id',
              'title',
              'featured_image',
              'meta',
              'published_at',
              'type',
         ],
         'wares' => [
              'id' => 'ware_id',
              'is_available',
         ],
    ];



    /**
     * Resets the builder.
     *
     * @return Builder|null
     */
    protected function resetBuilder()
    {
        $joinType      = $this->wareImportance() ? 'inner' : '';
        $locale        = getLocale();
        $this->builder = Post::select(self::getSelectingColumns())
                             ->whereNotNull('published_at')
                             ->where('published_at', '<=', now())
                             ->where('type', self::$products_posttype)
                             ->where('posts.locale', $locale)
                             ->leftJoin('wares', function ($query) use ($locale
                             ) { // The `leftJoin` method is used to show products with no wares
                                 $query->whereColumn('wares.sisterhood', '=', 'posts.sisterhood')
                                       ->where('wares.locales', 'like', "%$locale%")
                                 ;
                             }, null, null, $joinType)
                             ->groupBy('posts.id')
        ;

        return $this->builder;
    }



    /**
     * Returns the usable builder.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getBuilder()
    {
        return $this->builder ?: $this->resetBuilder();
    }



    /**
     * Sets the builder property.
     *
     * @param Builder $builder
     *
     * @return $this
     */
    protected function setBuilder(Builder $builder)
    {
        $this->builder = $builder;

        return $this;
    }



    /**
     * Returns a basic version of the columns to be selected.
     *
     * @return array
     */
    public static function getSelectingColumnsInstructions()
    {
        $general = self::$selecting_columns_instructions;
        $dynamic =
             [
                  'wares' => [
                       static::getSalePriceColumn()     => 'ware_sale_price',
                       static::getOriginalPriceColumn() => 'ware_original_price',
                  ],
             ];
        return array_merge_recursive($general, $dynamic);
    }



    /**
     * Returns an array of the columns to be selected.
     *
     * @return array
     */
    protected static function getSelectingColumns()
    {
        $instructions   = self::getSelectingColumnsInstructions();
        $result_columns = [];
        foreach ($instructions as $table => $columns) {
            if (!is_array($columns)) {
                $columns = [$columns];
            }

            foreach ($columns as $key => $val) {
                $appending = '';
                if (is_numeric($key)) {
                    $column = $val;
                } else {
                    $column    = $key;
                    $appending = ' as ' . $val;
                }
                $result_columns[] = $table . '.' . $column . $appending;
            }
        }

        return $result_columns;
    }
}
