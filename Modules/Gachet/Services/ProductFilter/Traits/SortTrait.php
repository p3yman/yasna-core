<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/5/18
 * Time: 9:12 PM
 */

namespace Modules\Gachet\Services\ProductFilter\Traits;

trait SortTrait
{
    protected $orderColumn    = 'posts.created_at';
    protected $orderDirection = 'desc';

    /**
     * run filters method
     *
     * @param array $filterConditions
     * @return $this
     */
    public function sortFilter(array $filterConditions)
    {
        foreach ($filterConditions as $conditionType => $condition) {
            $this->callMethod($conditionType, [$condition]);
        }

        return $this;
    }

    /**
     * sort by price
     *
     * @param string $sort
     * @return $this
     */
    public function price($sort)
    {
        $this->orderColumn    = 'wares.' . static::getSalePriceColumn();
        $this->orderDirection = $sort;

        return $this;
    }

    /**
     * sort by custom code
     *
     * @param string $sort
     * @return $this
     */
    public function code($sort)
    {
        $this->orderColumn    = 'wares.custom_code';
        $this->orderDirection = $sort;

        return $this;
    }

    /**
     * sort product by publish at
     *
     * @param string $sort
     * @return $this
     */
    public function publishing($sort)
    {
        $this->orderColumn    = 'published_at';
        $this->orderDirection = $sort;

        return $this;
    }

    /**
     * apply sort query
     *
     * @return $this
     */
    protected function applySort()
    {
        $builder = $this->getBuilder();
        $builder->orderBy($this->orderColumn, $this->orderDirection);

        $this->setBuilder($builder);

        return $this;
    }
}
