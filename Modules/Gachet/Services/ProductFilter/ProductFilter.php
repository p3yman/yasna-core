<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 05/12/2017
 * Time: 01:12 PM
 */

namespace Modules\Gachet\Services\ProductFilter;

use App\Models\Post;
use Modules\Gachet\Services\ProductFilter\Traits\BuilderTrait;
use Modules\Gachet\Services\ProductFilter\Traits\CheckboxFilterTrait;
use Modules\Gachet\Services\ProductFilter\Traits\OutputTrait;
use Modules\Gachet\Services\ProductFilter\Traits\PaginationTrait;
use Modules\Gachet\Services\ProductFilter\Traits\RangeFilterTrait;
use Modules\Gachet\Services\ProductFilter\Traits\SortTrait;
use Modules\Gachet\Services\ProductFilter\Traits\SwitchKeyFilterTrait;
use Modules\Gachet\Services\ProductFilter\Traits\TextFilterTrait;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductFilter
{
    use BuilderTrait;
    use OutputTrait;
    use TextFilterTrait;
    use CheckboxFilterTrait;
    use RangeFilterTrait;
    use SwitchKeyFilterTrait;
    use PaginationTrait;
    use SortTrait;



    /**
     * ProductFilter constructor.
     *
     * @param array $filterData
     */
    public function __construct(array $filterData)
    {
        $this->assignFilterData($filterData);
    }



    /**
     * Assigns the filter data.
     *
     * @param array $filter_data
     *
     * @return $this
     */
    public function assignFilterData(array $filter_data)
    {
        foreach ($filter_data as $filterType => $filterValues) {
            $this->callMethod($filterType . 'Filter', [$filterValues]);
        }

        return $this;
    }



    /**
     * Calls the given method with the given parameters.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     * @throws BadRequestHttpException
     */
    protected function callMethod($method, $parameters)
    {
        if (method_exists($this, $method)) {
            return $this->$method(...$parameters);
        } else {
            $this->badRequest();
        }
    }



    /**
     * Makes the query of the filter ready.
     */
    protected function makeQueryReady()
    {
        $this->resetBuilder();
        $this->applyCheckboxes()
             ->applySearch()
             ->applyRanges()
             ->applySwitches()
             ->applySort()
        ;;
    }



    /**
     * Checks if the joining with the wares table is important or not.
     *
     * @return bool
     */
    protected function wareImportance()
    {
        return $this->availabilityIsChecked() or $this->specialSaleIsChecked() or $this->packageSpecified();
    }



    /**
     * Returns the name of the column to be used as the sale price column.
     *
     * @return string
     */
    protected static function getSalePriceColumn()
    {
        return gachet()->shop()->getClientWarePriceColumn('sale');
    }



    /**
     * Returns the name of the column to be used as the original price column.
     *
     * @return string
     */
    protected static function getOriginalPriceColumn()
    {
        return gachet()->shop()->getClientWarePriceColumn('original');
    }
}
