<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/31/19
 * Time: 5:43 PM
 */

namespace Modules\Gachet\Services\ProductFilter;


class FilterHash
{
    /**
     * @var string
     */
    protected $hash;



    /**
     * FilterHash constructor.
     *
     * @param string $hash
     */
    public function __construct(string $hash)
    {
        $this->hash = $hash;
    }



    /**
     * Translate the hash string to array.
     *
     * @return array
     */
    public function translate()
    {
        $hash_array = [];

        $hash = explode_not_empty('!', $this->hash);
        foreach ($hash as $field) {
            $field = explode_not_empty('?', $field);
            if (count($field) == 2) {
                $hash_array[$field[1]] = explode_not_empty('/', $field[0]);
                $current_group         = &$hash_array[$field[1]];
                $current_group         = $this->arrayPrefixToIndex('_', $current_group);
            }
        }

        return $hash_array;
    }



    /**
     * Converts the prefix to index.
     *
     * @param string       $delimiter
     * @param array|string $haystack
     *
     * @return array|string
     */
    protected function arrayPrefixToIndex($delimiter, $haystack)
    {
        if (is_array($haystack)) {
            foreach ($haystack as $index => $field) {
                $parts = explode($delimiter, $field, 2);
                if (count($parts) == 2) {
                    $key   = $parts[0];
                    $value = $parts[1];
                    $value = $this->arrayPrefixToIndex($delimiter, $value);

                    $target = &$haystack[$key];
                    if (isset($target)) {
                        if (!is_array($target)) {
                            $target = [$target];
                        }

                        if (!is_array($value)) {
                            $value = [$value];
                        }

                        $target = array_merge($target, $value);
                    } else {
                        $target = $value;
                    }
                }
                unset($haystack[$index]);
            }

            return $haystack;
        }

        if (is_string($haystack)) {
            $parts = explode($delimiter, $haystack, 2);
            if (count($parts) == 2) {
                $key   = $parts[0];
                $value = $parts[1];
                $value = $this->arrayPrefixToIndex($delimiter, $value);

                $haystack = [
                     $key => $value,
                ];

                return $haystack;
            }
        }

        return $haystack;
    }


}
