<?php

namespace Modules\Support\Entities\Traits;

trait TicketAttachmentsTrait
{
    /**
     * attach files to the model.
     *
     * @param array $files
     *
     * @return bool|int
     */
    public function attachFiles(array $files)
    {
        $attached = 0;

        foreach ($files as $file) {
            $attached += uploader()->file($file)->attachTo($this);
        }

        return $attached;
    }
}
