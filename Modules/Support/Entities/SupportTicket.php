<?php

namespace Modules\Support\Entities;

use Modules\Support\Entities\Traits\TicketAttachmentsTrait;
use Modules\Timeline\Entities\Traits\TimelineNodesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupportTicket extends YasnaModel
{
    use SoftDeletes;
    use TimelineNodesTrait;
    use TicketAttachmentsTrait;



    /**
     * generate tracking number.
     *
     * @return string
     */
    public static function generateTrackingNumber()
    {
        return strval(rand(1, 9)) . substr(strval(time()), -8) . strval(rand(0, 9));
    }
}
