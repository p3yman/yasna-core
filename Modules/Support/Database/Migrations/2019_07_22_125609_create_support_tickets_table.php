<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_tickets', function (Blueprint $table) {
            $table->increments('id');

            $table->string("tracking_number")->index();
            $table->unsignedInteger("organization_id")->default(0)->index();
            $table->unsignedInteger("admin_id")->default(0)->index();
            $table->unsignedInteger("customer_id")->default(0)->index();
            $table->string("customer_name")->nullable()->index();
            $table->string("customer_email")->nullable()->index();
            $table->string("customer_mobile")->nullable()->index();

            $table->string("subject")->nullable()->index();
            $table->text("text")->nullable();

            $table->string("status")->default("pending")->index();
            $table->tinyInteger("priority")->default(2)->index();

            $table->timestamp("due_at")->nullable()->index();
            $table->timestamp("customer_attended_at")->nullable();
            $table->timestamp("admin_attended_at")->nullable();
            $table->unsignedInteger("admin_attended_by")->default(0);
            $table->timestamp("first_admin_attended_at")->nullable();
            $table->unsignedInteger("first_admin_attended_by")->default(0);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_tickets');
    }
}
