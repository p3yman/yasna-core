<?php

namespace Modules\Support\Http\Controllers\V1;

use App\Models\SupportTicket;
use Modules\Support\Http\Requests\V1\TicketSubmitRequest;
use Modules\Support\Http\TimelineNodes\TicketOpenedNode;
use Modules\Yasna\Services\YasnaApiController;

class TicketsController extends YasnaApiController
{
    /**
     * submit the ticket.
     *
     * @param TicketSubmitRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function submit(TicketSubmitRequest $request)
    {
        /** @var SupportTicket $saved */
        $saved = model("support-ticket")->batchSave($request->toArray(), ["attachments"]);

        if ($saved->exists) {
            $saved->attachFiles($request->attachments);
            TicketOpenedNode::insert($saved);
        }

        return $this->modelSaveFeedback($saved, [
             "tracking_number" => $saved->tracking_number,
        ]);
    }
}
