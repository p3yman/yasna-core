<?php

namespace Modules\Support\Http\TimelineNodes;

use Modules\Timeline\Services\NodeAbstract;

class TicketOpenedNode extends NodeAbstract
{
    /**
     * @inheritdoc
     */
    public static function title()
    {
        return trans("support::nodes.opened");
    }



    /**
     * @inheritdoc
     */
    public static function defaults($model)
    {
        return [
             "organization_id" => $model->organization_id,
             "customer_id"     => $model->user_id,
             "subject"         => $model->subject,
             "text"            => $model->text,
             "created_at"      => $model->created_at,
             "created_by"      => $model->created_by,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return array_normalize([
             "created_at" => $this->model->getResource("created_at"),
             "created_by" => $this->model->getResource("created_by"),
        ], parent::toArray());
    }
}
