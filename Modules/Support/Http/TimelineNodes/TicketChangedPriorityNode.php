<?php

namespace Modules\Support\Http\TimelineNodes;

use Modules\Timeline\Services\NodeAbstract;

class TicketChangedPriorityNode extends NodeAbstract
{
    /**
     * @inheritdoc
     */
    public static function title()
    {
        return trans("support::nodes.changed_priority");
    }



    /**
     * @inheritdoc
     */
    public static function defaults($model)
    {
        return [
             "priority"     => $model->priority,
             "old_priority" => 0,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return array_merge(parent::toArray(), [
             "priority_text"     => trans("support::tickets.priority." . $this->getData("priority")),
             "old_priority_text" => trans("support::tickets.priority." . $this->getData("old_priority")),
        ]);
    }
}
