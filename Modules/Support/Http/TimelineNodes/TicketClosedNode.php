<?php

namespace Modules\Support\Http\TimelineNodes;

use Modules\Timeline\Services\NodeAbstract;

class TicketClosedNode extends NodeAbstract
{
    /**
     * @inheritdoc
     */
    public static function title()
    {
        return trans("support::nodes.closed");
    }



    /**
     * @inheritdoc
     */
    public static function defaults($model)
    {
        return [
             "closed_at" => now()->toDateTimeString(),
             "closed_by" => user()->id,
        ];
    }
}
