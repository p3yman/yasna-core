<?php

namespace Modules\Support\Http\TimelineNodes;

use Modules\Timeline\Services\NodeAbstract;

class TicketCommentedNode extends NodeAbstract
{
    /**
     * @inheritdoc
     */
    public static function title()
    {
        return trans("support::nodes.commented");
    }



    /**
     * @inheritdoc
     */
    public static function defaults($model)
    {
        return [
             "user_id" => user()->id,
             "text"    => null,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return [
             "user" => user()::quickFind($this->getData("user_id"))->toResource([
                  "full_name",
             ]),
             "text" => $this->getData("text"),
        ];
    }
}
