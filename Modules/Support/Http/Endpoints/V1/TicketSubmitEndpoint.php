<?php

namespace Modules\Support\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Support\Http\Controllers\V1\TicketsController;

/**
 * @api               {POST}
 *                    /api/modular/v1/support-ticket-submit
 *                    Ticket Submit
 * @apiDescription    Submit a new ticket
 * @apiVersion        1.0.0
 * @apiName           Ticket Submit
 * @apiGroup          Support
 * @apiPermission     User
 * @apiParam {string}       customer_id   the customer hashid, only if submitted by an admin
 * @apiParam {string}       subject       the title of the problem.
 * @apiParam {string}       text          the description of the problem.
 * @apiParam {int:1,2,3}    priority      the priority level
 * @apiParam {array}        [attachments] the attachment files
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method TicketsController controller()
 */
class TicketSubmitEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Ticket Submit";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Support\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TicketsController@submit';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "id" => hashid(rand(1,100)),
        ]);
    }
}
