<?php

namespace Modules\Support\Http\Requests\V1;

use App\Models\SupportTicket;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class TicketSubmitRequest extends YasnaFormRequest
{
    protected $model_name        = "SupportTicket";
    protected $should_load_model = false;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $array = [
             "subject",
             "text",
             "priority",
             "attachments",
        ];

        if (user()->isAdmin()) {
            $array[] = "customer_id";
        }

        return $array;
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->setOrganizationId();
        $this->setCustomerId();
        $this->setTrackingNumber();
    }



    /**
     * get general validation rules.
     *
     * @return array
     */
    protected function generalRules()
    {
        return [
             "subject"     => "required",
             "text"        => "required",
             "priority"    => "required|numeric|between:1,3",
             "attachments" => "array",
        ];
    }



    /**
     * get customer validation rules.
     *
     * @return array
     */
    protected function customerRules()
    {
        if (!user()->isAdmin()) {
            return [];
        }

        return [
             "customer_id" => "required", //TODO: To be completed when working on the appropriate story.
        ];
    }



    /**
     * set organization_id
     *
     * @return void
     */
    private function setOrganizationId()
    {
        $this->setData("organization_id", (organization()::root())->id);
    }



    /**
     * set customer_id
     *
     * @return void
     */
    private function setCustomerId()
    {
        if (!$this->isset("customer_id")) {
            $this->setData("customer_id", user()->id);
        }
    }



    /**
     * set tracking number
     *
     * @return void
     */
    private function setTrackingNumber()
    {
        $this->setData("tracking_number", SupportTicket::generateTrackingNumber());
    }
}
