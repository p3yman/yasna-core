<?php

namespace Modules\Support\Providers;

use Modules\Support\Http\Endpoints\V1\TicketSubmitEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class SupportServiceProvider
 *
 * @package Modules\Support\Providers
 */
class SupportServiceProvider extends YasnaProvider
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerEndpoints();
    }



    /**
     * register Endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        endpoint()->register(TicketSubmitEndpoint::class);
    }

}
