<?php
return [
     "opened"           => "بازگشایی تیکت",
     "closed"           => "پایان بررسی",
     "commented"        => "ثبت یادداشت",
     "changed_priority" => "تغییر اولویت",
];
