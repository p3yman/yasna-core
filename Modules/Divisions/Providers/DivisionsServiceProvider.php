<?php

namespace Modules\Divisions\Providers;

use Modules\Divisions\Components\DivisionComponent;
use Modules\Divisions\Http\Endpoints\V1\CitiesEndpoint as CitiesEndpointV1;
use Modules\Divisions\Http\Endpoints\V1\CountriesEndpoint;
use Modules\Divisions\Http\Endpoints\V1\DestroyEndpoint;
use Modules\Divisions\Http\Endpoints\V1\FetchEndpoint;
use Modules\Divisions\Http\Endpoints\V1\ListEndpoint;
use Modules\Divisions\Http\Endpoints\V1\ProvincesEndpoint as ProvincesEndpointV1;
use Modules\Divisions\Http\Endpoints\V1\RestoreEndpoint;
use Modules\Divisions\Http\Endpoints\V1\SaveEndpoint;
use Modules\Divisions\Http\Endpoints\V1\TrashEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class DivisionsServiceProvider
 *
 * @package Modules\Divisions\Providers
 */
class DivisionsServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
        $this->registerComponents();
    }



    /**
     * Register all endpoints provided by this module
     *
     * @return void
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule("Endpoint")) {
            return;
        }

        endpoint()->register(ProvincesEndpointV1::class);
        endpoint()->register(CitiesEndpointV1::class);
        endpoint()->register(CountriesEndpoint::class);
        endpoint()->register(FetchEndpoint::class);
        endpoint()->register(ListEndpoint::class);
        endpoint()->register(SaveEndpoint::class);
        endpoint()->register(TrashEndpoint::class);
        endpoint()->register(RestoreEndpoint::class);
        endpoint()->register(DestroyEndpoint::class);
    }


    /**
     * register components
     */
    private function registerComponents()
    {
        component()::register("division", DivisionComponent::class);
    }
}
