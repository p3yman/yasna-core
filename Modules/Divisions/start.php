<?php

if (!function_exists("division")) {
    /**
     * get division model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Division
     */
    function division($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Division $model */
        $model = model('Division', $id_or_hashid, $with_trashed);

        return $model;
    }
}


if (!function_exists("continent")) {
    /**
     * get continent model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Continent
     */
    function continent($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Continent $model */
        $model = model('Continent', $id_or_hashid, $with_trashed);

        return $model;
    }
}


if (!function_exists("country")) {
    /**
     * get country model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Country
     */
    function country($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Country $model */
        $model = model('country', $id_or_hashid, $with_trashed);

        return $model;
    }
}


if (!function_exists("province")) {
    /**
     * get province model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Province
     */
    function province($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Province $model */
        $model = model('province', $id_or_hashid, $with_trashed);

        return $model;
    }
}


if (!function_exists("city")) {
    /**
     * get city model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\City
     */
    function city($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\City $model */
        $model = model('city', $id_or_hashid, $with_trashed);

        return $model;
    }
}


if (!function_exists("neighborhood")) {
    /**
     * get neighborhood model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Neighborhood
     */
    function neighborhood($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Neighborhood $model */
        $model = model('Neighborhood', $id_or_hashid, $with_trashed);

        return $model;
    }
}


if (!function_exists("town")) {
    /**
     * get town model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Town
     */
    function town($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Town $model */
        $model = model('town', $id_or_hashid, $with_trashed);

        return $model;
    }
}


if (!function_exists("village")) {
    /**
     * get village model helper
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\Village
     */
    function village($id_or_hashid = 0, $with_trashed = false)
    {
        /** @var \App\Models\Village $model */
        $model = model('village', $id_or_hashid, $with_trashed);

        return $model;
    }
}
