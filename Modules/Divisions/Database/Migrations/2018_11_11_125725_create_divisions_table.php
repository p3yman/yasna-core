<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->index();
            $table->string('slug')->index();
            $table->string('title_en')->index();
            $table->string('title_fa')->index();
            $table->unsignedInteger('continent_id')->index();
            $table->unsignedInteger('country_id')->index();
            $table->unsignedInteger('province_id')->index();
            $table->unsignedInteger('city_id')->index();
            $table->unsignedInteger('capital_id')->index();
            $table->float('latitude')->nullable()->index();
            $table->float('longitude')->nullable()->index();
            $table->string('language', '3')->nullable()->index();
            $table->string('currency', '3')->nullable()->index();
            $table->string('timezone')->nullable()->index();
            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
