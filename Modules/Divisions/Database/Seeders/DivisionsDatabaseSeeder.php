<?php

namespace Modules\Divisions\Database\Seeders;

use Illuminate\Database\Seeder;

class DivisionsDatabaseSeeder extends Seeder
{

    /**
     * run database seed
     */
    public function run()
    {
        $this->call(DivisionTableSeeder::class);
    }


}
