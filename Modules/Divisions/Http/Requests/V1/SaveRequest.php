<?php

namespace Modules\Divisions\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class SaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Division";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id          = $this->model->id;
        $valid_types = implode(",", division()::getValidTypes());

        return [
             'slug'         => "alpha_dash|unique:divisions,slug,$id,id",
             "type"         => "required|in:$valid_types",
             "title_fa"     => "required|string",
             "continent_id" => "numeric",
             "country_id"   => "numeric",
             "province_id"  => "numeric",
             "city_id"      => "numeric",
             "capital_id"   => "numeric",
             'latitude'     => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
             'longitude'    => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
             "language"     => "string|size:2",
             "currency"     => "string|size:3",
             "timezone"     => "timezone",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $titles  = division()->getTitleFields();
        $normals = [
             "slug",
             "type",
             "continent_id",
             "country_id",
             "province_id",
             "city_id",
             "capital_id",
             'latitude',
             'longitude',
             "language",
             "currency",
             "timezone",
        ];

        return array_merge($titles, $normals);
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("divisions::attributes");
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return trans("divisions::messages");
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveContinent();
        $this->resolveCountry();
        $this->resolveProvince();
        $this->resolveCity();
        $this->resolveCapital();
        $this->purifyCurrency();
        $this->purifyLanguage();
    }



    /**
     * resolve Continent
     *
     * @return void
     */
    private function resolveContinent()
    {
        if ($this->getData("type") == "continent") {
            $this->unsetData("continent_id");
            return;
        }

        $continent = continent()->grabHashid($this->getData("continent_id"));

        if (!$continent->exists) {
            $this->setData("continent_id", "invalid");
            return;
        }

        $this->setData("continent_id", $continent->id);
    }



    /**
     * resolve Country
     *
     * @return void
     */
    private function resolveCountry()
    {
        if (in_array($this->getData("type"), ["continent", "country"])) {
            $this->unsetData("country_id");
            return;
        }

        $country = country()->grabHashid($this->getData("country_id"));

        if (!$country->exists) {
            $this->setData("country_id", "invalid");
            return;
        }

        $this->setData("country_id", $country->id);
    }



    /**
     * resolve Province
     *
     * @return void
     */
    private function resolveProvince()
    {
        if (in_array($this->getData("type"), ["continent", "country", "province"])) {
            $this->unsetData("province_id");
            return;
        }

        $province = province()->grabHashid($this->getData("province_id"));

        if (!$province->exists) {
            $this->setData("province_id", "invalid");
            return;
        }

        $this->setData("province_id", $province->id);
    }



    /**
     * resolve City
     *
     * @return void
     */
    private function resolveCity()
    {
        if ($this->getData("type") != "neighborhood") {
            $this->unsetData("city_id");
            return;
        }

        $city = city()->grabHashid($this->getData("city_id"));

        if (!$city->exists) {
            $this->setData("city_id", "invalid");
            return;
        }

        $this->setData("city_id", $city->id);
    }



    /**
     * resolve Capital
     *
     * @return void
     */
    private function resolveCapital()
    {
        if (!$this->isset("capital_id")) {
            return;
        }
        if (in_array($this->getData("type"), ["continent", "neighborhood", "town", "village"])) {
            $this->unsetData("capital_id");
            return;
        }

        $city = city()->grabHashid($this->getData("capital_id"));

        if (!$city->exists) {
            $this->setData("capital_id", "invalid");
            return;
        }

        $this->setData("capital_id", $city->id);
    }



    /**
     * purify language code to be a lower-case phrase
     *
     * @return void
     */
    private function purifyLanguage()
    {
        $field = "language";
        if (!$this->isset($field)) {
            return;
        }

        $this->setData($field, strtolower($this->getData($field)));
    }



    /**
     * purify language code to be a upper-case phrase
     *
     * @return void
     */
    private function purifyCurrency()
    {
        $field = "currency";
        if (!$this->isset($field)) {
            return;
        }

        $this->setData($field, strtoupper($this->getData($field)));
    }
}
