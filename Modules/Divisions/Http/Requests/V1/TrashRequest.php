<?php

namespace Modules\Divisions\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class TrashRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Division";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;
}
