<?php

namespace Modules\Divisions\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaListRequest;


class ListRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "division";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "types"      => "array",
             "continents" => "array",
             "countries"  => "array",
             "provinces"  => "array",
             "cities"     => "array",
             "languages"  => "array",
             "title"      => "string",
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->convertHashids("continents");
        $this->convertHashids("countries");
        $this->convertHashids("provinces");
        $this->convertHashids("cities");
    }



    /**
     * convert hashids to ids and ensure no id or invalid thing is passed
     *
     * @param string $attribute
     *
     * @return void
     */
    private function convertHashids(string $attribute)
    {
        if (!$this->isset($attribute)) {
            return;
        }

        /** @var array $data */
        $data   = $this->getData($attribute);
        $output = [];

        foreach ($data as $key => $value) {
            if (is_numeric($value)) {
                continue;
            }

            $value = hashid($value);
            if (!$value) {
                continue;
            }

            $output[] = $value;
        }

        $this->setData($attribute, $output);
    }
}
