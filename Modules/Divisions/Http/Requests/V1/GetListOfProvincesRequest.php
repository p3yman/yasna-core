<?php

namespace Modules\Divisions\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class GetListOfProvincesRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'continent' => 'string',
             'country'   => 'string',
             'lang'      => 'string|in:fa,en',
             'title'     => 'string',
             'slug'      => 'string',
             'paginated' => 'int|in:0,1',
             'per_page'  => 'int|min:1|max:50',
             'page'      => 'int|min:1',
        ];
    }
}
