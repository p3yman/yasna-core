<?php

namespace Modules\Divisions\Http\Forms;


use Illuminate\Database\Eloquent\Collection;
use Modules\Forms\Services\FormBinder;

class DivisionForm extends FormBinder
{
    /**
     * @inheritdoc
     */
    public function components()
    {
        $this->add("text")
             ->name("slug")
             ->label(trans("divisions::attributes.slug"))
        ;

        $this->add("select")
             ->name("type")
             ->label(trans("divisions::attributes.type"))
             ->options($this->getTypesCombo())
        ;

        $this->addTitlesComponents();
        $this->addParentalComponent("continent");
        $this->addParentalComponent("country");
        $this->addParentalComponent("province");
        $this->addParentalComponent("city");
        $this->addParentalComponent("capital", "city");

        $this->add("text")
             ->name("language")
             ->label(trans("divisions::attributes.language"))
        ;

        $this->add("text")
             ->name("currency")
             ->label(trans("divisions::attributes.currency"))
        ;

        //TODO: Lat, Long and timezone are missing.
    }



    /**
     * add titles components
     *
     * @return void
     */
    private function addTitlesComponents()
    {
        $titles = division()->getTitleFields();

        foreach ($titles as $title) {
            $this->add("text")
                 ->name($title)
                 ->label(trans("divisions::attributes.$title"))
            ;
        }
    }



    /**
     * add parental component
     *
     * @param string      $type
     * @param null|string $name
     */
    private function addParentalComponent(string $type, ?string $model = null)
    {
        $name = $type . "_id";

        /** @var Collection $options */
        $options = division()
             ->where("type", $model ? $model : $type)
             ->orderBy(division()->getDefaultTitleField())
             ->get()
        ;
        $mapped  = $options->map(function ($model) {
            return [
                 "value" => $model->hashid,
                 "label" => $model->title,
            ];
        });

        $this->add("select")
             ->name($name)
             ->label(trans("divisions::attributes.$name"))
             ->options($mapped)
        ;
    }



    /**
     * get types Combo
     *
     * @return array
     */
    private function getTypesCombo(): array
    {
        $types = division()::getValidTypes();

        return array_map(function ($k) {
            return [
                 "label" => trans("divisions::types.$k"),
                 "value" => $k,
            ];
        }, $types);
    }
}
