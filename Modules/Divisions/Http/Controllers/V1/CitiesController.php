<?php

namespace Modules\Divisions\Http\Controllers\V1;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Divisions\Entities\Division;
use Modules\Divisions\Http\Controllers\V1\Traits\DivisionSortTrait;
use Modules\Divisions\Http\Requests\V1\GetListOfCitiesRequest;
use Modules\Yasna\Services\YasnaApiController;

class CitiesController extends YasnaApiController
{
    use DivisionSortTrait;



    /**
     * Get list of all cities.
     *
     * @param GetListOfCitiesRequest $request
     *
     * @return array
     */
    public function getListOfCities(GetListOfCitiesRequest $request)
    {
        $elector = $this->cityElectorArray($request->toArray());
        $query   = model('division')->elector($elector);
        $this->applyRequestedSort($query, $request);

        if ($request->isPaginated()) {
            $paginator = $query->paginate($request->perPage());
            $meta      = $this->paginatorMetadata($paginator);

            return $this->success($this->getCityListFromPagination($paginator), $meta);
        }

        $results = $query->get()
                         ->map(function ($province) {
                             return $this->cityToArray($province);
                         })
                         ->toArray()
        ;

        return $this->success($results);
    }



    /**
     * Force limit the results on cities.
     *
     * @param array $request
     *
     * @return array
     */
    private function cityElectorArray(array $request): array
    {
        $request['selectCity'] = true;

        return $request;
    }



    /**
     * Return information of the cities from a paginator class
     *
     * @param LengthAwarePaginator $paginator
     *
     * @return array
     */
    private function getCityListFromPagination(LengthAwarePaginator $paginator): array
    {
        return $paginator->map(function ($city) {
            return $this->cityToArray($city);
        })
                         ->toArray()
             ;
    }



    /**
     * Return array equivalent of a city.
     *
     * @param Division $city
     *
     * @return array
     */
    private function cityToArray($city): array
    {
        return [
             'id'           => $city->hashid,
             'title_en'     => $city->title_en,
             'title_fa'     => $city->title_fa,
             'continent_id' => hashid($city->continent_id),
             'country_id'   => hashid($city->country_id),
             'province_id'  => hashid($city->province_id),
             'latitude'     => $city->latitude,
             'longitude'    => $city->longitude,
        ];
    }
}
