<?php

namespace Modules\Divisions\Http\Controllers\V1;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class CountriesController extends YasnaApiController
{

    /**
     * return list of countries .
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function getListOfCountries(SimpleYasnaRequest $request)
    {
        $elector = $this->countryElectorArray($request->toArray());
        $query   = model('division')->elector($elector);

        if ($request->isPaginated()) {
            $paginator = $query->paginate($request->perPage());
            $meta      = $this->paginatorMetadata($paginator);

            return $this->success($this->getCountriesListFromPagination($paginator), $meta);
        }

        $results = $query->get()
                         ->map(function ($country) {
                             return $this->countryToArray($country);
                         })
                         ->toArray()
        ;

        return $this->success($results, [
             "total"     => $query->count(),
             "continent" => $request->continent,
        ]);
    }



    /**
     * Return information of the countries from a paginator class
     *
     * @param LengthAwarePaginator $paginator
     *
     * @return array
     */
    private function getCountriesListFromPagination(LengthAwarePaginator $paginator): array
    {
        return $paginator->map(function ($province) {
            return $this->countryToArray($province);
        })->toArray()
             ;
    }



    /**
     * pass array to elector
     *
     * @param array $array
     *
     * @return array
     */
    private function countryElectorArray(array $array): array
    {
        return [
             "continent"      => isset($array['continent']) ? hashid($array['continent']) : null,
             "select-country" => true,
        ];
    }



    /**
     * return array of countries list
     *
     * @param $country
     *
     * @return array
     */
    private function countryToArray($country): array
    {
        return [
             'id'           => $country->hashid,
             'title_en'     => $country->title_en,
             'title_fa'     => $country->title_fa,
             'continent_id' => hashid($country->continent_id),
             'country_id'   => hashid($country->country_id),
             'capital_id'   => hashid($country->capital_id),
             'latitude'     => $country->latitude,
             'longitude'    => $country->longitude,
        ];
    }

}
