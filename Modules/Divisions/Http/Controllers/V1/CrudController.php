<?php

namespace Modules\Divisions\Http\Controllers\V1;

use Illuminate\Database\Eloquent\Builder;
use Modules\Divisions\Http\Forms\DivisionForm;
use Modules\Divisions\Http\Requests\V1\DestroyRequest;
use Modules\Divisions\Http\Requests\V1\FetchRequest;
use Modules\Divisions\Http\Requests\V1\ListRequest;
use Modules\Divisions\Http\Requests\V1\RestoreRequest;
use Modules\Divisions\Http\Requests\V1\SaveRequest;
use Modules\Divisions\Http\Requests\V1\TrashRequest;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\Yasna\Services\YasnaApiController;

class CrudController extends YasnaApiController
{
    /**
     * perform fetch action
     *
     * @param FetchRequest $request
     *
     * @return array
     */
    public function fetch(FetchRequest $request)
    {
        return $this->success(
             $request->model->toSingleResource(),
             [
                  "form" => (new DivisionForm())->toArray(),
             ]
        );
    }



    /**
     * get list of divisions
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array
     */
    public function list(ListRequest $request)
    {
        /** @var Builder $builder */
        $builder = division()->elector($request->toArray());

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * perform save action
     *
     * @param SaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(SaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * perform trash action
     *
     * @param TrashRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function trash(TrashRequest $request)
    {
        $done = $request->model->delete();

        return $this->typicalSaveFeedback($done);
    }



    /**
     * perform restore action
     *
     * @param RestoreRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restore(RestoreRequest $request)
    {
        $done = $request->model->undelete();

        return $this->typicalSaveFeedback($done);
    }



    /**
     * perform destroy action
     *
     * @param DestroyRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(DestroyRequest $request)
    {
        $done = $request->model->hardDelete();

        return $this->typicalSaveFeedback($done);
    }
}
