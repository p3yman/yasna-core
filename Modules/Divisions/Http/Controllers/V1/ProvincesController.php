<?php

namespace Modules\Divisions\Http\Controllers\V1;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Divisions\Http\Controllers\V1\Traits\DivisionSortTrait;
use Modules\Divisions\Http\Requests\V1\GetListOfProvincesRequest;
use Modules\Yasna\Services\YasnaApiController;

class ProvincesController extends YasnaApiController
{
    use DivisionSortTrait;



    /**
     * Return list of all provinces.
     *
     * @param GetListOfProvincesRequest $request
     *
     * @return array
     */
    public function getListOfProvinces(GetListOfProvincesRequest $request)
    {
        $elector = $this->provinceElectorArray($request->toArray());
        $query   = model('division')->elector($elector);
        $this->applyRequestedSort($query, $request);

        if ($request->isPaginated()) {
            $paginator = $query->paginate($request->perPage());
            $meta      = $this->paginatorMetadata($paginator);

            return $this->success($this->getProvinceListFromPagination($paginator), $meta);
        }

        $results = $query->get()
                         ->map(function ($province) {
                             return $this->provinceToArray($province);
                         })
                         ->toArray()
        ;

        return $this->success($results);
    }



    private function provinceElectorArray(array $request): array
    {
        $request['selectProvince'] = true;

        return $request;
    }



    /**
     * Return information of the provinces from a paginator class
     *
     * @param LengthAwarePaginator $paginator
     *
     * @return array
     */
    private function getProvinceListFromPagination(LengthAwarePaginator $paginator): array
    {
        return $paginator->map(function ($province) {
            return $this->provinceToArray($province);
        })
                         ->toArray()
             ;
    }



    private function provinceToArray($province): array
    {
        return [
             'id'           => $province->hashid,
             'title_en'     => $province->title_en,
             'title_fa'     => $province->title_fa,
             'continent_id' => hashid($province->continent_id),
             'country_id'   => hashid($province->country_id),
             'capital_id'   => hashid($province->capital_id),
             'latitude'     => $province->latitude,
             'longitude'    => $province->longitude,
        ];
    }
}
