<?php

namespace Modules\Divisions\Http\Controllers\V1\Traits;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Trait DivisionSortTrait add ability of sorting on the query based on a given request.
 *
 * @package Modules\Divisions\Http\Controllers\V1\Traits
 */
trait DivisionSortTrait
{
    /**
     * This method just prepare data to be applied sorts on them based on a given request.
     *
     * @param Builder      $builder
     * @param YasnaRequest $request
     */
    public function applyRequestedSort(Builder $builder, YasnaRequest $request)
    {
        $allowed_array = model('Division')->sortableFields();
        $sorts         = $request->input('sort', []);

        $this->applySorts($builder, $sorts, $allowed_array);
    }



    /**
     * Apply sort modifications on a given builder based on a given set. The given set (`$sorts`) contains some fields
     * name and maybe a `-` sign in front of them (for example `['name', '-salary']` which the `-` sign means descending
     * direction sort for that field. Also there can be a allowed fields which if it has been given with values, the
     * sorting process applied for a field, only when that field is on the allowed fields.
     *
     * @param Builder $builder
     * @param array   $sorts
     * @param array   $allowed_fields
     */
    public function applySorts(Builder $builder, array $sorts, array $allowed_fields = [])
    {
        foreach ($sorts as $sort) {
            $type   = $this->getTypeOfSort($sort);
            $column = $this->getColumnName($sort);
            if (!empty($allowed_fields) and not_in_array($column, $allowed_fields)) {
                continue;
            }

            $this->applySort($builder, $column, $type);
        }
    }



    /**
     * Apply sort on a given column.
     *
     * @param Builder $builder
     * @param string  $column
     * @param string  $type
     */
    private function applySort(Builder $builder, string $column, string $type = 'asc')
    {
        if (!$column) {
            return;
        }

        $builder->orderBy($column, $type);
    }



    /**
     * Get type of sort for a given column.
     *
     * @param string $column
     *
     * @return string
     */
    private function getTypeOfSort(string $column): string
    {
        return $column[0] == '-' ? 'desc' : 'asc';
    }



    /**
     * Remove the `-` from column name.
     *
     * @param string $column
     *
     * @return string
     */
    private function getColumnName(string $column): string
    {
        return $column[0] == '-' ? substr($column, 1) : $column;
    }
}
