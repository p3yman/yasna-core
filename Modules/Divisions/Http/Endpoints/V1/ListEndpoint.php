<?php

namespace Modules\Divisions\Http\Endpoints\V1;

use Modules\Divisions\Http\Controllers\V1\CrudController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/divisions-list
 *                    List
 * @apiDescription    List of divisions, with and without filter, with and without divisions
 * @apiVersion        1.0.0
 * @apiName           List
 * @apiGroup          Divisions
 * @apiPermission     Guest
 * @apiParam {array}       [types]       division types
 * @apiParam {array}       [continents]  hashids of the dominant continent
 * @apiParam {array}       [countries]   hashids of the dominant country
 * @apiParam {array}       [provinces]   hashids of the dominant province
 * @apiParam {array}       [cities]      hashids of the dominant city
 * @apiParam {array}       [languages]   two-letters language codes
 * @apiParam {array}       [currencies]  three-letters currency codes
 * @apiParam {string}      [title]       the title to be searched likewise
 * @apiParam {Integer=0,1} [trash]       Specifies that the results must look into the trashed data only (default 0).
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer}     [page=1]      Specifies the page number of results (only with `paginated=1`).
 * @apiParam {Integer}     [per_page=15] Specifies the number of results on each page (only with `paginated=1`).
 * @apiSuccessExample Success-Response: TODO: This is the default save feedback. Change if your work is different
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @method CrudController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Divisions\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
