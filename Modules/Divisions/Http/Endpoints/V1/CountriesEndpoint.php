<?php

namespace Modules\Divisions\Http\Endpoints\V1;

use Modules\Divisions\Http\Controllers\V1\CountriesController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/divisions-countries
 *                    get all countries
 * @apiDescription    get a list of all countries
 * @apiVersion        1.0.0
 * @apiName           get all countries
 * @apiGroup          Divisions
 * @apiPermission     User
 * @apiParam {String} [continent] Hashid, ID of continent for filtering.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {null},
 *      "results": {
 *          {
 *              "id": "xnDWN",
 *              "title_en": "Afghanistan",
 *              "title_fa": "",
 *              "continent_id": "xPMZK",
 *              "country_id": "PKEnN",
 *              "capital_id": "PKEnN",
 *              "latitude": null,
 *              "longitude": null
 *          },
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiDeprecated use now (#Divisions:List).
 */
class CountriesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get all countries";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Divisions\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CountriesController@getListOfCountries';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([

             "id"           => "xnDWN",
             "title_en"     => "Afghanistan",
             "title_fa"     => "",
             'continent_id' => "7nDo2",
             "latitude"     => 30,
             "longitude"    => 40,

        ], [
             "total"     => 10,
             "continent" => "folan",
        ]);
    }
}
