<?php

namespace Modules\Divisions\Http\Endpoints\V1;

use Modules\Divisions\Http\Controllers\V1\CrudController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/divisions-save
 *                    Save
 * @apiDescription    Save (create and update) a division record
 * @apiVersion        1.0.0
 * @apiName           Save
 * @apiGroup          Divisions
 * @apiPermission     developer
 * @apiParam {string} [id]           hashid of the record in question (create-mode in absence of the param)
 * @apiParam {string} [slug]         slug of the division
 * @apiParam {string} type           type of the division
 * @apiParam {string} title_fa       title of the division in Persian
 * @apiParam {string} [title_en]     title of the division in English (any other language can be added, if supported)
 * @apiParam {string} [continent_id] hashid of the continent division
 * @apiParam {string} [country_id]   hashid of the country division
 * @apiParam {string} [province_id]  hashid of the province division
 * @apiParam {string} [city_id]      hashid of the city division
 * @apiParam {string} [capital_id]   hashid of the capital division
 * @apiParam {string} [latitude]     latitude of the division
 * @apiParam {string} [longitude]    longitude of the division
 * @apiParam {string} [language]     two-letter language code
 * @apiParam {string} [currency]     three-letter language code
 * @apiParam {string} [timezone]     the timezone
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class SaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Divisions\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
    }
}
