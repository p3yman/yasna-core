<?php

namespace Modules\Divisions\Http\Endpoints\V1;

use Modules\Divisions\Http\Controllers\V1\CrudController;
use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {PUT}
 *                    /api/modular/v1/divisions-restore
 *                    Division Restore (Undelete)
 * @apiDescription    undelete a division
 * @apiVersion        1.0.0
 * @apiName           Division Restore (Undelete)
 * @apiGroup          Divisions
 * @apiPermission     Developer
 * @apiParam {string} id . hashid of the type
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "QKgJx"
 *      },
 *      "results": {
 *          done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CrudController controller()
 */
class RestoreEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Division Restore (Undelete)";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_PUT;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Divisions\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CrudController@restore';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "metadata" => hashid(12123213),
        ]);
    }
}
