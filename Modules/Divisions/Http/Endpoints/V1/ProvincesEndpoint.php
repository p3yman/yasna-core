<?php

namespace Modules\Divisions\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {Get}
 *                    /api/modular/V1/divisions-provinces
 *                    Get Provinces
 * @apiDescription    Get a list of all provinces
 * @apiVersion        1.0.1
 * @apiName           Provinces List
 * @apiGroup          Divisions
 * @apiPermission     User
 * @apiParam {String} [continent] Hashid, ID or slug of continent for filtering.
 * @apiParam {String} [country] Hashid, ID or slug of country for filtering.
 * @apiParam {String} [lang='fa'] The language which must be used on filtering title_en or title_fa.
 * @apiParam {String} [title] The title which must be searched. For using this filter, the `lang` parameter must had
 *           been set.
 * @apiParam {String} [slug] The slug of province for filter.
 * @apiParam {String} [paginated=0,1] Indicate that the format of the result. If `1` had been chosen, result will be in
 *           paginated format, otherwise it's in normal format.
 * @apiParam {String} [per_page=15] Indicate the number of results on per page. (available only with `paginated=1`).
 * @apiParam {String} [page=1] Page number of returned result (available only with `paginated=1`).
 * @apiParam {Array=title_fa,title_en,timezone} [sort] The sort array accept columns which must be sorted. The
 *           default direction of sort is ascending and if you need descending direction, just add a `-` sign at the
 *           beginning of the value. As an example `sort[]=title_fa&sort[]=-timezone` will do an ascending sort based on
 *           `title_fa` and then will does a descending sort on results based on `timezone` column.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": [
 *          {
 *              "id": "nAyBx",
 *              "title_en": "Ilam",
 *              "title_fa": "ایلام",
 *              "continent_id": "xPMZK",
 *              "country_id": "NoDwA",
 *              "capital_id": "AdryK",
 *              "latitude": 32.99,
 *              "longitude": 46.92
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthenticated-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiDeprecated use now (#Divisions:List).
 */
class ProvincesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Provinces";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Divisions\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ProvincesController@getListOfProvinces';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "id"           => "nAyBx",
                  "title_en"     => "Ilam",
                  "title_fa"     => "ایلام",
                  "continent_id" => "xPMZK",
                  "country_id"   => "NoDwA",
                  "capital_id"   => "AdryK",
                  "latitude"     => 32.99,
                  "longitude"    => 46.92,
             ],
        ]);
    }
}
