<?php


namespace Modules\Divisions\Components;


use Modules\Forms\Services\BasicComponents\InputAbstract;

class DivisionComponent extends InputAbstract
{

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "continent"     => "",
             "country"       => "",
             "province"      => "",
             "city"          => "",
             "neighbourhood" => "",
             "limit"         => "",
        ]);
    }

}
