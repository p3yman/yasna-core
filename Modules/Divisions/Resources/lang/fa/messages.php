<?php
return [
     "continent_id.numeric" => "قاره معتبر نیست.",
     "country_id.numeric"   => "کشور معتبر نیست.",
     "province_id.numeric"  => "استان معتبر نیست.",
     "city_id.numeric"      => "شهر معتبر نیست.",
     "capital_id.numeric"   => "مرکز معتبر نیست.",
];
