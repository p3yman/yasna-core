<?php
return [
     "slug"         => "نامک",
     "type"         => "نوع",
     "title_fa"     => "عنوان فارسی",
     "title_en"     => "عنوان انگلیسی",
     "continent_id" => "قاره",
     "country_id"   => "کشور",
     "province_id"  => "استان",
     "city_id"      => "شهر",
     "capital_id"   => "مرکز",
     'latitude'     => "عرض جغرافیایی",
     'longitude'    => "طول جغرافیایی",
     "language"     => "زبان",
     "currency"     => "واحد پول",
     "timezone"     => "ناحیه زمانی",
];
