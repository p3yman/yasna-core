<?php
return [
     "continent"    => "قاره",
     "country"      => "کشور",
     "province"     => "استان/ایالت",
     "city"         => "شهر",
     "town"         => "شهرستان",
     "village"      => "روستا",
     "neighborhood" => "محله",
];
