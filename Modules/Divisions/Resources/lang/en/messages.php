<?php
return [
     "continent_id.numeric" => "Invalid Continent.",
     "country_id.numeric"   => "Invalid Country.",
     "province_id.numeric"  => "Invalid Province.",
     "city_id.numeric"      => "Invalid City.",
     "capital_id.numeric"   => "Invalid Capital.",
];
