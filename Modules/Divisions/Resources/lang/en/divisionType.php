<?php
/**
 * Created by PhpStorm.
 * User: adelsedighi
 * Date: 11/12/18
 * Time: 10:02 AM
 */
return [
     "0" => "Unknown",
     "1" => "Continent",
     "2" => "Country",
     "3" => "Province",
     "4" => "City",

];
