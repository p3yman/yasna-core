<?php
return [
     "slug"         => "Slug",
     "type"         => "Type",
     "title_fa"     => "Title In Farsi",
     "title_en"     => "Title in English",
     "continent_id" => "Continent",
     "country_id"   => "Country",
     "province_id"  => "Province",
     "city_id"      => "City",
     "capital_id"   => "Capital",
     'latitude'     => "Latitude",
     'longitude'    => "Longitude",
     "language"     => "Language",
     "currency"     => "Currency",
     "timezone"     => "Timezone",
];
