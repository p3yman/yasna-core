<?php
return [
     "continent"    => "Continent",
     "country"      => "Country",
     "province"     => "Province/State",
     "city"         => "City",
     "town"         => "Town",
     "village"      => "Village",
     "neighborhood" => "Neighborhood",
];
