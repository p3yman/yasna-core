<?php

namespace Modules\Divisions\Entities;

use Modules\Divisions\Entities\Traits\DivisionRelationsTrait;
use Modules\Divisions\Entities\Traits\DivisionScopeTrait;
use Modules\Divisions\Entities\Traits\DivisionTitlesTrait;
use Modules\Divisions\Entities\Traits\DivisionTypeTrait;
use Modules\Divisions\Entities\Traits\DivisionElectorTrait;
use Modules\Divisions\Entities\Traits\DivisionResourceTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Division extends YasnaModel
{
    use SoftDeletes;
    use DivisionElectorTrait;
    use DivisionTypeTrait;
    use DivisionScopeTrait;
    use DivisionRelationsTrait;
    use DivisionTitlesTrait;
    use DivisionResourceTrait;

    protected $table = "divisions";



    /**
     * Return a list of all sortable fields of the model which can be used on queries.
     *
     * @return array
     */
    public function basicSortableFields(): array
    {
        $module = module('divisions');

        return [
             'title_fa' => $module->getTrans('fields.title_fa'),
             'title_en' => $module->getTrans('fields.title_en'),
             'timezone' => $module->getTrans('fields.timezone'),
        ];
    }



    /**
     * get the fully-qualified name of the current model
     *
     * @return string
     */
    protected function fqName()
    {
        return MODELS_NAMESPACE . "Division";
    }
}

