<?php

namespace Modules\Divisions\Entities\Traits;

trait DivisionRelationsTrait
{
    /**
     * get the continent of the current row
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function continent()
    {
        return $this->belongsTo($this->fqName(), "continent_id");
    }



    /**
     * Map the country relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function countries()
    {
        return $this->hasMany($this->fqName(), 'continent_id');
    }



    /**
     * Get the country of the current row
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo($this->fqName(), "country_id");
    }



    /**
     * Map the province relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function provinces()
    {
        $field = $this->type . "_id";

        return $this->hasMany($this->fqName(), $field);
    }



    /**
     * Get the province of the current row
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo($this->fqName(), 'province_id');
    }



    /**
     * This map the province to their parents which is capital of a province
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function capitals()
    {
        return $this->hasMany($this->fqName(), 'capital_id');
    }



    /**
     * Get the capital for current province
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function capital()
    {
        return $this->belongsTo($this->fqName(), 'capital_id');
    }



    /**
     * Map the city relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        $field = $this->type . "_id";

        return $this->hasMany($this->fqName(), $field);
    }



    /**
     * Get the city of the current row
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo($this->fqName(), 'city_id');
    }



    /**
     * Map the neighborhood relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function neighborhoods()
    {
        $field = $this->type . "_id";

        return $this->hasMany($this->fqName(), $field);
    }
}
