<?php

namespace Modules\Divisions\Entities\Traits;

/**
 * Class Division
 *
 * @property int    continent_id
 * @property int    country_id
 * @property int    province_id
 * @property int    city_id
 * @property string $type
 */
trait DivisionTypeTrait
{
    /**
     * get an array of valid division types
     *
     * @return array
     */
    public static function getValidTypes(): array
    {
        return [
             "continent",
             "country",
             "province",
             "city",
             "town",
             "village",
             "neighborhood",
        ];
    }



    /**
     * check if the model is a continent
     *
     * @return bool
     */
    public function isContinent()
    {
        return $this->type == "continent";
    }



    /**
     * check if the model is a country
     *
     * @return bool
     */
    public function isCountry()
    {
        return $this->type == "country";
    }



    /**
     * check if the model is a province
     *
     * @return bool
     */
    public function isProvince()
    {
        return $this->type == "province";
    }



    /**
     * check if the model is a city
     *
     * @return bool
     */
    public function isCity()
    {
        return $this->type == "city";
    }



    /**
     * check if the model is a neighborhood
     *
     * @return bool
     */
    public function isNeighborhood()
    {
        return $this->type == "neighborhood";
    }



    /**
     * check if the model is a village
     *
     * @return bool
     */
    public function isVillage()
    {
        return $this->type == "village";
    }



    /**
     * check if the model is a town
     *
     * @return bool
     */
    public function isTown()
    {
        return $this->type == "town";
    }
}
