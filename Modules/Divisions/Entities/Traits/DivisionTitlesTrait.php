<?php

namespace Modules\Divisions\Entities\Traits;

trait DivisionTitlesTrait
{
    /**
     * get title in the given language
     *
     * @param string $lang
     *
     * @return string
     */
    public function titleIn(string $lang)
    {
        $property = "title_$lang";

        return $this->$property;
    }



    /**
     * get default title field
     *
     * @return string
     */
    public function getDefaultTitleField()
    {
        $locale = getLocale();

        if (in_array("title_$locale", $this->getTitleFields())) {
            return "title_$locale";
        }

        return "title_fa";
    }



    /**
     * get title in the site_locale language
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->titleIn(getLocale());
    }



    /**
     * get an array of the title fields
     *
     * @return array
     */
    public function getTitleFields(): array
    {
        return array_where($this->fields_array, function ($value, $key) {
            return starts_with($value, "title_");
        });
    }
}
