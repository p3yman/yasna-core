<?php

namespace Modules\Divisions\Entities\Traits;

trait DivisionResourceTrait
{
    /**
     * boot DivisionResourceTrait
     *
     * @return void
     */
    public static function bootDivisionResourceTrait()
    {
        static::addDirectResources('*');
    }
}
