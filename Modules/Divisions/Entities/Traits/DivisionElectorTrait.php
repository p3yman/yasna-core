<?php

namespace Modules\Divisions\Entities\Traits;

/**
 * Trait ElectorTrait collects a set of electors related to the division model.
 *
 * @package Modules\Divisions\Entities
 */
trait DivisionElectorTrait
{

    /**
     * Elector for set filter on `continent_id`
     *
     * @param array|int $continent
     */
    public function electorContinent($continent)
    {
        $this->commonElection('continent_id', hashid_number($continent));
    }



    /**
     * Elector for set a `whereIn` filter on `continent_id`
     *
     * @param array $continents
     */
    public function electorContinents(array $continents)
    {
        $this->commonElection('continent_id', $continents);
    }



    /**
     * Elector for set filter on `country_id`
     *
     * @param array|int $country
     */
    public function electorCountry($country)
    {
        $this->commonElection('country_id', hashid_number($country));
    }



    /**
     * Elector for set a `whereIn` filter on `country_id`
     *
     * @param array $countries
     */
    public function electorCountries(array $countries)
    {
        $this->commonElection('country_id', $countries);
    }



    /**
     * Elector for set filter on `province_id`
     *
     * @param array|int $province
     */
    public function electorProvince($province)
    {
        $this->commonElection('province_id', hashid_number($province));
    }



    /**
     * Elector for set a `whereIn` filter on `province_id`
     *
     * @param array $provinces
     */
    public function electorProvinces(array $provinces)
    {
        $this->commonElection('province_id', $provinces);
    }



    /**
     * Elector for set filter on `city_id`
     *
     * @param array|int $city
     */
    public function electorCity($city)
    {
        $this->commonElection('province_id', hashid_number($city));
    }



    /**
     * Elector for set a `whereIn` filter on `city_id`
     *
     * @param array $cities
     */
    public function electorCities(array $cities)
    {
        $this->commonElection('city_id', $cities);
    }



    /**
     * Set a where-like filter on title of division. This will look in all title fields
     *
     * @param string $title
     */
    public function electorTitle(string $title)
    {
        $title_fields = $this->getTitleFields();

        $this->elector()->where(function ($q) use ($title_fields, $title) {
            $q = $q->whereRaw("1=0");

            foreach ($title_fields as $field) {
                $q->orWhere($field, 'like', "%$title%");
            }

            return $q;
        })
        ;
    }



    /**
     * Set the language of title which must be searched.
     *
     * @param string|null $lang
     */
    public function electorLang(string $lang)
    {
        if (!$lang || !in_array($lang, ['fa', 'en'])) {
            $lang = 'fa';
        }

        $this->title_language = $lang;
    }



    /**
     * set the language code queries
     *
     * @param array $langs
     */
    public function electorLanguages(array $languages)
    {
        $this->elector()->whereIn("language", $languages);
    }



    /**
     * set the currency code queries
     *
     * @param array $langs
     */
    public function electorCurrencies(array $currencies)
    {
        $this->elector()->whereIn("currency", $currencies);
    }



    /**
     * set the typ queries
     *
     * @param array $langs
     */
    public function electorTypes(array $types)
    {
        $this->elector()->whereIn("type", $types);
    }



    /**
     * Query must just return provinces.
     *
     * @param bool $flag
     */
    public function electorSelectProvince(bool $flag)
    {
        if (!$flag) {
            return;
        }

        $this->elector()->where('continent_id', '<>', 0)
             ->where('country_id', '<>', 0)
             ->where('province_id', '0')
        ;
    }



    /**
     * Query must just return countries.
     *
     * @param bool $flag
     */
    public function electorSelectCountry(bool $flag)
    {
        if (!$flag) {
            return;
        }

        $this->elector()->where('continent_id', '<>', 0)
             ->where('country_id', '=', 0)
        ;
    }



    /**
     * Query must just return cities.
     *
     * @param bool $flag
     */
    public function electorSelectCity(bool $flag)
    {
        if (!$flag) {
            return;
        }

        $this->elector()->where('continent_id', '<>', 0)
             ->where('country_id', '<>', 0)
             ->where('province_id', '<>', 0)
             ->where('city_id', '0')
        ;
    }



    /**
     * add trashed-only elector
     */
    public function electorTrash()
    {
        $this->elector()->onlyTrashed();
    }



    /**
     * Do a common task for electing. It gets a `key` (or column name) and a `value` which the query
     * must be filtered based on it. If the `value` is an array, a `whereIn` filter will be used,
     * otherwise elector will use a `where` statement. For `null` values no action will
     * be happen.
     *
     * @param string $key
     * @param mixed  $value
     */
    private function commonElection(string $key, $value)
    {
        if ($value !== 0 and empty($value)) {
            return;
        }

        if (is_array($value)) {
            $this->elector()->whereIn($key, $value);
            return;
        }

        $this->elector()->where($key, $value);
    }
}
