<?php

namespace Modules\Divisions\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait DivisionScopeTrait
{
    /**
     * boot the trait
     */
    public static function bootDivisionScopeTrait()
    {
        static::loadTypeScope();
    }



    /**
     * get type from class name
     *
     * @return null|string
     */
    protected static function getTypeFromClassName()
    {
        $type = kebab_case(static::className());

        if ($type == "division") {
            return null;
        }

        return $type;
    }



    /**
     * load type scope
     */
    private static function loadTypeScope()
    {
        $type = static::getTypeFromClassName();

        if (!$type) {
            return;
        }

        static::addGlobalScope("type", function (Builder $builder) {
            $builder->where("type", static::getTypeFromClassName());
        });
    }
}
