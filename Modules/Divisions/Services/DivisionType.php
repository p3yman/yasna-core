<?php

namespace Modules\Divisions\Services;


class DivisionType
{

    const UNKNOWN   = 0;
    const CONTINENT = 1;
    const COUNTRY   = 2;
    const PROVINCE  = 3;
    const CITY      = 4;

    protected static $min = 1;
    protected static $max = 4;



    /**
     * get the minimum allowed DivisionType index
     *
     * @return int
     */
    public static function minAllowed()
    {
        return static::$min;
    }



    /**
     * get the maximum allowed DivisionType index
     *
     * @return int
     */
    public static function maxAllowed()
    {
        return static::$max;
    }



    /**
     * get the caption of the divisionType index
     *
     * @param int    $index
     * @param string $locale
     *
     * @return string
     */
    public static function caption(int $index, $locale = null)
    {
        if (!$locale) {
            $locale = getLocale();
        }
        return trans("division::divisionType.$index", [], $locale);
    }



    /**
     * get an array of available options, suitable to be used in the user interface
     *
     * @param bool   $including_zero
     * @param string $locale
     *
     * @return array
     */
    public static function options($including_zero = false, $locale = null)
    {
        $result = [];
        $min    = static::$min;
        if ($including_zero) {
            $min = 0;
        }
        for ($i = $min; $i <= static::$max; $i++) {
            $result[$i] = static::caption($i, $locale);
        }
        return $result;
    }



    /**
     * get an array of available options, suitable to be used in a combo widget
     *
     * @param bool   $including_zero
     * @param string $locale
     *
     * @return array
     */
    public static function combo($including_zero = false, $locale = null)
    {
        $options = static::options($including_zero, $locale);
        $result  = [];
        foreach ($options as $key => $caption) {
            $result[] = [
                 'id'      => $key,
                 'caption' => $caption,
            ];
        }
        return $result;
    }


}
