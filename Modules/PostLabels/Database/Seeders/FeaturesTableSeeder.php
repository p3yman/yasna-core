<?php

namespace Modules\PostLabels\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()::seed("features", [
             [
                  'slug'      => 'labels',
                  'title'     => trans("post-labels::downstream.title"),
                  'order'     => '22',
                  'icon'      => 'glass',
             ],
        ]);
    }
}
