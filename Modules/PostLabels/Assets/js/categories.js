/**
 * Post Labels Module JS
 * -------------------------
 * Created by Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2019-02-05
 */

let PostLabels = {
    lock: true,

    /**
     * Gets active folder slug
     * @returns {String}
     */
    getActiveFolder: function () {
        return $('.categories_folder a.active').attr('href').slice(1);
    },


    /**
     * Init Jquery Sortable plugin
     */
    initSortablePlugin: function () {
        initJquerySortablePlugin(jQuery, window, 'sortable');
    },


    /**
     * Make categories sortable
     */
    makeSortable: function () {
        let folders;
        let oldContainer;
        let sortable_field = $('.categories_nested > .categories_list');

        this.initSortablePlugin();

        folders = sortable_field.sortable({
            group : 'nested',
            handle: '.category_drag_handle',
            onDrop: function ($item, container, _super) {

                var data       = folders.sortable("serialize").get();
                var jsonString = JSON.stringify(data);

                $('#js_categoriesOrder').val(jsonString);

                _super($item, container);
            },

            onDrag: function ($item, position) {
                // RTL Fix
                let winWidth = $(document).width();
                $item.css({
                    right: winWidth - position.left - $item.width,
                    top  : position.top
                });
            }
        });

        sortable_field.sortable('disable');
    },

    /**
     * Get all selected labels in editor
     * @return {Array}
     */
    getEditorCheckedLabels: function() {
    let panel = $('#divLabelsInside #categories_accordion');
    let checkboxes = panel.find('.categories_nested .category_item input[type="checkbox"]');
    let checked = [];

    checkboxes.map(function (index, checkbox) {
        if($(checkbox).is(':checked')){
            checked.push($(checkbox).attr('id'));
        }
    });

    return checked;
}
};


jQuery(function ($) {

    // Make categories potentialy sortable
    PostLabels.makeSortable();

    // Triggers when folders are clicked
    $(document).on('click', '.categories_folder a', function (e) {
        e.preventDefault();
        let folders     = $('.categories_folder a');
        let contentBody = $('.categories_content');
        let targetEl    = $(this);
        let target      = targetEl.attr('href').slice(1);

        folders.removeClass('active');
        targetEl.addClass('active');

        contentBody.attr('data-active-panel', target);

        $('.categories_inner_body').hide();
        $('#' + target).show();
    });

    // Show first folder content
    $('.categories_folder:not(.deactivated) a').first().click();

    // Toggle draggability and deleting action
    $(document).on('click', '.js_toggle_edit_lock', function () {
        let target               = $('.categories_content');
        let sortable_field       = $('.categories_nested > .categories_list');
        let deactivateds         = $(target).find('.category_box.deactivated, .category_label.deactivated');
        let deactivatedFolders   = $('.categories_folders').find('.categories_folder.deactivated');
        let deleteButtons        = $(target).find('.js_categoryDelete');
        let dragHandles          = $(target).find('.category_drag_handle');
        let footerControlButtons = $('.categories_footer .right');

        if (PostLabels.lock) {
            // Show Trash and Deactivated categories
            deactivateds.slideDown();
            deactivatedFolders.slideDown();
            deleteButtons.fadeIn();

            // Show Control Buttons
            footerControlButtons.fadeIn();

            // Enable draggability
            dragHandles.fadeIn();
            sortable_field.sortable('enable');

        } else {
            // hide Trash and Deactivated categories
            deactivateds.slideUp();
            deactivatedFolders.slideUp();
            deleteButtons.fadeOut();

            // Hide Control Buttons
            footerControlButtons.fadeOut();

            // Disable draggability
            dragHandles.fadeOut();
            sortable_field.sortable('disable');
        }

        $('.js_toggle_edit_lock').toggle();
        PostLabels.lock = !PostLabels.lock;
    });

}); //End Of siaf!


/**
 *   Get PostLabels Active folder
 *   @return string
 *   @deprecated Use PostLabel.getActiveFolder()
 */
function getPostLabelsActiveFolder() {
    return $('.categories_folder a.active').attr('href').slice(1);
}


/**
 * open modal to the address of editor
 *
 * @param route
 */
function postLabelsEditor(route) {
    let current_folder = PostLabels.getActiveFolder();
    route = route.replaceAll("-SLUG-", current_folder);

    masterModal(route);
}


/**
 * refresh the panel
 */
function postLabelsPanelRefresh() {
    let $div = $("#divPostLabelsPanel");
    let tab  = PostLabels.getActiveFolder();

    $div.addClass('loading');
    $.ajax({
        url  : $div.attr('data-src'),
        cache: false
    }).done(function (html) {
            $div.html(html);
            $div.removeClass('loading');

            PostLabels.makeSortable();
            PostLabels.lock = true;

            $('.categories_folder a[href="#' + tab + '"]').click();
        }
    );
}


function postLabelsEditorPanelRefresh()
{
    let $div = $("#divLabelsInside");
    let checked_items = PostLabels.getEditorCheckedLabels();
    let openPanel = $div.find('.categories_folder_panel .panel-collapse.in').attr('id');

    $div.addClass("loading");
    $.ajax({
        url: $div.attr('data-src'),
        cache: false
    }).done(function(html){
        $div.html(html);
        $div.removeClass('loading');

        checked_items.map(function (id) {
            $('#' + id).attr('checked', true);
        });

        if(!$('#' + openPanel).hasClass('in')){
            $div.find('.categories_folder_panel .panel-heading a[href="#'+ openPanel +'"]').click();
        }

    });

}

