<?php

namespace Modules\PostLabels\Http\Controllers\V1;

use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Modules\PostLabels\Http\Requests\V1\GetPostLabelsByLabelRequest;
use Modules\PostLabels\Http\Requests\V1\GetPostLabelsRequest;
use Modules\PostLabels\Http\Requests\V1\GetPostsListRequest;
use Modules\Yasna\Services\YasnaApiController;

class PostLabelsController extends YasnaApiController
{
    /**
     * get all label of posttype
     *
     * @param GetPostLabelsRequest $request
     *
     * @return array
     */
    public function getLabelsByPosttype(GetPostLabelsRequest $request)
    {
        $builder = $request->model->getAllLabelsRecursively();
        $meta    = [
             "posttype" => $request->model->hashid,
        ];

        return $this->getResourcesFromBuilder($builder, $request, $meta);
    }



    /**
     * get all labels nested under a particular label
     *
     * @param GetPostLabelsByLabelRequest $request
     *
     * @return array
     */
    public function getLabelsByLabel(GetPostLabelsByLabelRequest $request)
    {
        $builder = $request->model->allChildren();

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get all posts of the specified label
     *
     * @param GetPostsListRequest $request
     *
     * @return array
     */
    public function postsList(GetPostsListRequest $request)
    {
        /** @var Collection $collection */
        $collection = $request->model->models()->get();
        $result     = $collection->map(function (Post $item) {
            return $item->toListResource();
        });

        return $this->success($result->toArray(), [
             "label" => $request->model->toSingleResource(),
        ]);

    }
}
