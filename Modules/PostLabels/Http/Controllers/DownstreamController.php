<?php

namespace Modules\PostLabels\Http\Controllers;

use App\Models\Label;
use Modules\PostLabels\Events\PostLabelSaved;
use Modules\PostLabels\Http\Requests\FolderSaveRequest;
use Modules\PostLabels\Http\Requests\LabelSaveRequest;
use Modules\Yasna\Services\YasnaController;

class DownstreamController extends YasnaController
{
    protected $view_folder = "post-labels::downstream";



    /**
     * show the index of the downstream settings, shop tab.
     *
     * @return array
     */
    public static function index()
    {
        return [
             "view_file" => "post-labels::downstream.index",
             "nested"    => posttype()->getLabelsNestedArray(true),
        ];
    }



    /**
     * refresh the panel.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshPanel()
    {
        return $this->view("panel", [
             "nested" => posttype()->getLabelsNestedArray(true),
        ]);
    }



    /**
     * get the folder editor form
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function folderCreateForm()
    {
        $model = label();

        $model->feature_active = 1;

        return $this->view("folder.editor", compact('model'));
    }



    /**
     * get the folder editor form
     *
     * @param string $slug
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function folderEditForm($slug)
    {
        $model = posttype()->findLabel($slug, true);

        if (!$model->exists) {
            return $this->abort(410);
        }

        $model->feature_active = $model->isNotTrashed();
        $model->posttypes      = implode(',', $model->models()->get()->pluck('id')->toArray());


        return $this->view("folder.editor", compact('model'));
    }



    /**
     * save a folder
     *
     * @param FolderSaveRequest $request
     *
     * @return string
     */
    public function folderSave(FolderSaveRequest $request)
    {
        $saved = posttype()::saveLabel($request->model, $request->toArray());

        return $this->jsonAjaxSaveFeedback($saved, [
             "success_callback" => "postLabelsPanelRefresh()",
        ]);
    }



    /**
     * get the label editor form
     *
     * @param string $folder_slug
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function labelCreateForm(string $folder_slug)
    {
        $folder = posttype()->findLabel($folder_slug, true);

        if (!$folder->exists) {
            return $this->abort(410);
        }

        $model            = label();
        $model->parent_id = $folder->id;

        return $this->view("label.editor", compact('model', 'folder'));
    }



    /**
     * get the label editor form
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function labelEditForm(string $hashid)
    {
        /** @var Label $model */
        $model  = label()->withTrashed()->grabHashid($hashid);
        $folder = $model->folder();

        if (!$model->exists) {
            return $this->abort(410);
        }

        return $this->view("label.editor", compact('model', 'folder'));
    }



    /**
     * save a label
     *
     * @param LabelSaveRequest $request
     *
     * @return string
     */
    public function labelSave(LabelSaveRequest $request)
    {
        $model        = $request->model;
        $old_data     = $request->model->toArray();
        $saved        = post()::saveLabel($model, $request->toArray());
        $update_posts = ($request->post_update ?: 0);

        event(new PostLabelSaved(
             $model,
             $old_data,
             $update_posts
        ));

        return $this->jsonAjaxSaveFeedback($saved, [
             "success_callback" => "postLabelsPanelRefresh()",
        ]);
    }
}
