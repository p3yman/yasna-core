<?php

namespace Modules\PostLabels\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Modules\Yasna\Services\YasnaController;

class PostsController extends YasnaController
{
    /**
     * register the services required to show the category selector and save hooks
     *
     * @return void
     */
    public static function registerHandlers()
    {
        module("posts")
             ->service("editor_side")
             ->add("post-labels")
             ->order(31)
             ->blade("post-labels::post-selector.index")
        ;

        module("posts")
             ->service("after_save")
             ->add("post-labels")
             ->method("PostLabels:PostsController@saveLabels")
        ;
    }



    /**
     * save labels attached to a particular post
     *
     * @param Post  $post
     * @param array $data
     *
     * @return array|bool
     */
    public static function saveLabels($post, $data)
    {
        /*-----------------------------------------------
        | Simple Bypass ...
        */
        if ($post->hasnot("labels")) {
            return $data;
        }

        /*-----------------------------------------------
        | Action ...
        */
        $attachable = static::retrieveLabelsFromRequest($data);

        $post->detachAllLabels();
        $post->attachLabels($attachable);

        /*-----------------------------------------------
        | Return unchanged data ...
        */
        return $data;
    }



    /**
     * refresh the editor panel
     *
     * @param string $posttype_hashid
     * @param string $post_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshPanel($posttype_hashid, $post_hashid = 0)
    {
        if ($post_hashid) {
            $model = post()->grabHashid($post_hashid);
        } else {
            $model       = post();
            $type        = posttype($posttype_hashid);
            $model->type = $type->slug;
        }

        return $this->view("post-labels::post-selector.panel-inside", compact("model"));
    }



    /**
     * Retrieves and returns the attachable labels from the given request data.
     *
     * @param array $data
     *
     * @return array
     */
    protected static function retrieveLabelsFromRequest(array $data)
    {
        return array_merge(
             static::retrieveNestedLabelsFromRequest($data),
             static::retrieveFlatLabelsFromRequest($data)
        );
    }



    /**
     * Retrieves and returns the attachable nested labels from the given request data.
     *
     * @param array $data
     *
     * @return array
     */
    protected static function retrieveNestedLabelsFromRequest(array $data)
    {
        $labels_array = ($data['_label'] ?? []);

        return collect($labels_array)
             ->filter()
             ->keys()
             ->toArray()
             ;
    }



    /**
     * Retrieves and returns the attachable flat labels from the given request data.
     *
     * @param array $data
     *
     * @return array
     */
    protected static function retrieveFlatLabelsFromRequest(array $data)
    {
        $strings = ($data['_flat_labels'] ?? '');

        return collect($strings)
             ->map(function ($string) {
                 return explode_not_empty(',', $string);
             })
             ->flatten()
             ->toArray()
             ;
    }



    /**
     * Registers the filters for the browsing posts.
     */
    public static function filters()
    {
        $service_name = 'posts:browse_filters';

        service($service_name)
             ->add('labels')
             ->blade('post-labels::filter.label')
             ->order(6)
        ;
    }



    /**
     * Registers browse headings.
     */
    public static function browseHeadings($arguments)
    {
        $posttype = $arguments['posttype'];
        $my_name  = "post-labels";

        module('posts')
             ->service('browse_headings')
             ->add($my_name)
             ->trans("$my_name::downstream.title")
             ->blade("$my_name::post-browse.columns.post-labels")
             ->order(21)
             ->condition($posttype->has('labels'))
        ;
    }
}
