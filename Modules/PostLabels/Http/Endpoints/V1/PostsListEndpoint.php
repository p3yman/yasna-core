<?php

namespace Modules\PostLabels\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\PostLabels\Http\Controllers\V1\PostLabelsController;

/**
 * @api               {GET}
 *                    /api/modular/v1/post-labels-posts-list
 *                    Posts List
 * @apiDescription    get all posts with specified label
 * @apiVersion        1.0.1
 * @apiName           Posts List
 * @apiGroup          PostLabels
 * @apiPermission     None
 * @apiParam {string}   label  Hashid or Slug of label
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "PKEnN",
 *          "label": {
 *              "id"        : "AEBMn",
 *              "created_at": 1558355758,
 *              "updated_at": 1558355758,
 *              "deleted_at": 1559331360,
 *              "created_by": null,
 *              "updated_by": null,
 *              "deleted_by": null,
 *              "parent_id" : null,
 *              "model_name": "Posttype",
 *              "slug"      : "faq",
 *              "titles"    : [
 *                  "fa": "Title"
 *              ],
 *              "locales": [
 *                  "fa"
 *              ],
 *              "converted"  : 0,
 *              "color"      : null,
 *              "custom"     : []
 *          }
 *      },
 *      "results": {
 *          {
 *               [
 *                    "id": "qKXVA",
 *                    "slug": null,
 *                    "type": "product",
 *                    "title": null,
 *               ]
 *           }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *       "status": 400,
 *       "developerMessage": "post-labels::developerMessages.403",
 *       "userMessage": "post-labels::userMessages.403",
 *       "errorCode": 403,
 *       "moreInfo": "post-labels.moreInfo.403",
 *       "errors": []
 * }
 * @method  PostLabelsController controller()
 */
class PostsListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get all posts of a specific label";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\PostLabels\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostLabelsController@postsList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"     => "qKXVA",
             "slug"   => null,
             "type"   => "product",
             "title"  => "first content",
             "title2" => null,
        ], [
             "authenticated_user" => user()->hashid,
             "label"              => [
                  "id"         => "AEBMn",
                  "created_at" => 1558355758,
                  "updated_at" => 1558355758,
                  "deleted_at" => 1559331360,
                  "created_by" => null,
                  "updated_by" => null,
                  "deleted_by" => null,
                  "parent_id"  => null,
                  "model_name" => "Posttype",
                  "slug"       => "faq",
                  "titles"     => [
                       "fa" => dummy()::persianTitle(),
                  ],
                  "locales"    => [
                       "fa",
                  ],
                  "converted"  => 0,
                  "color"      => null,
                  "custom"     => [],
             ],
        ]);
    }
}
