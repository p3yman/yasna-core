<?php

namespace Modules\PostLabels\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\PostLabels\Http\Controllers\V1\PostLabelsController;

/**
 * @api               {GET}
 *                    /api/modular/v1/post-labels-list
 *                    Get Post Labels
 * @apiDescription    Get Post Labels
 * @apiVersion        1.0.1
 * @apiName           Get Post Labels
 * @apiGroup          PostLabels
 * @apiPermission     None
 * @apiParam {string} [posttype] Hashid or slug of a posttype
 * @apiParam {string} [label]    Hashid or slug of a label
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "PKEnN"
 *      },
 *      "results": [
 *             [
 *              {
 *                   "title": "second",
 *                    "slug": "second-Bbq",
 *                    "picture": [],
 *                    "text": null,
 *                    "description": null,
 *                    "color": null
 *              }
 *          ]
 *      ]
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *       "status": 400,
 *       "developerMessage": "post-labels::developerMessages.403",
 *       "userMessage": "post-labels::userMessages.403",
 *       "errorCode": 403,
 *       "moreInfo": "post-labels.moreInfo.403",
 *       "errors": []
 * }
 * @method  PostLabelsController controller()
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get all labels of a specific post type";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\PostLabels\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        if (request()->has("posttype")) {
            return 'PostLabelsController@getLabelsByPosttype';
        }

        if (request()->has("label")) {
            return 'PostLabelsController@getLabelsByLabel';
        }

        return "";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "title"       => "second",
             "slug"        => "second-Bbq",
             "picture"     => [],
             "text"        => null,
             "description" => null,
             "color"       => null,
        ], [
             "authenticated_user" => user()->hashid,
        ]);
    }
}
