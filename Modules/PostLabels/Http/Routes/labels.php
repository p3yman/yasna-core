<?php

Route::group([
     'middleware' => ['web', 'auth'],
     'namespace'  => module('PostLabels')->getControllersNamespace(),
     'prefix'     => 'manage/post-labels',
], function () {
    Route::get('create-label/{slug}', 'DownstreamController@labelCreateForm')->name("postLabels-label-create");
    Route::get('edit-label/{hashid}', 'DownstreamController@labelEditForm')->name("postLabels-label-edit");
    Route::post('save-label', 'DownstreamController@labelSave')->name("postLabels-label-save");
});
