<?php

Route::group([
     'middleware' => ['web', 'auth'],
     'namespace'  => module('PostLabels')->getControllersNamespace(),
     'prefix'     => 'manage/post-labels',
], function () {
    Route::get('panel-refresh', 'DownstreamController@refreshPanel')->name("postLabels-panel-refresh");
    Route::get('create-folder', 'DownstreamController@folderCreateForm')->name("postLabels-folder-create");
    Route::get('edit-folder/{slug}', 'DownstreamController@folderEditForm')->name("postLabels-folder-edit");
    Route::post('save-folder', 'DownstreamController@folderSave')->name("postLabels-folder-save");
});
