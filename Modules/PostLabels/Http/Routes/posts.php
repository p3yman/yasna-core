<?php

Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'namespace'  => module('PostLabels')->getControllersNamespace(),
     'prefix'     => 'manage/post-labels',
], function () {
    Route::get('post-panel-refresh/{type}/{hashid?}' , 'PostsController@refreshPanel')->name("postLabels-editor-panel-refresh");
});
