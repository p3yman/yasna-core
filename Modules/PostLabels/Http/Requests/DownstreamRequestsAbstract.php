<?php

namespace Modules\PostLabels\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;


abstract class DownstreamRequestsAbstract extends YasnaRequest
{
    protected $model_name = "Label";



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "titles.required"        => trans("post-labels::downstream.titles_required"),
             "posttypes.required"     => trans("post-labels::downstream.posttypes_required"),
             "folder_hashid.accepted" => trans("post-labels::downstream.folder_hashid_accepted"),
             "slug.invalid"           => trans("post-labels::downstream.folder_slug_unique"),
        ];
    }



    /**
     * filter empty entries in the titles array
     */
    protected function filterTitlesArray()
    {
        $this->setData("titles", array_filter($this->getData('titles')));
    }



    /**
     * create slug, according to the given titles
     */
    protected function createSlug()
    {
        $given = $this->getData("slug");
        $guess = $given ? $given : $this->guessSlug();

        do {
            $guess .= "-" . str_random(3);
        } while (label()->isDefined("Posttype", $guess));

        $this->setData("slug", $guess);
    }



    /**
     * guess a slug, according to the given titles
     *
     * @return string
     */
    private function guessSlug(): string
    {
        $titles = (array)$this->getData('titles');
        $title  = isset($titles['en']) ? $titles['en'] : array_first($titles);

        return str_slug($title);
    }
}
