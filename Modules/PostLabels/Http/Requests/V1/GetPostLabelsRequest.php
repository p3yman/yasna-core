<?php

namespace Modules\PostLabels\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class GetPostLabelsRequest extends YasnaRequest
{
    protected $responder = 'white-house';

    protected $model_name = "posttype";



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        $value = $this->getData('posttype');

        if (is_numeric($value)) {
            $this->failedAuthorization();
        }

        $this->model = posttype($value);

        if (!$this->model->exists) {
            $this->failedAuthorization();
        }
    }


}
