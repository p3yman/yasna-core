<?php

namespace Modules\PostLabels\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class GetPostsListRequest extends YasnaRequest
{
    protected $responder = 'white-house';

    protected $model_name = "label";



    /**
     * @inheritdoc
     */
    public function loadRequestedModel($id_or_hashid = false)
    {

        $label = $this->getData('label');

        if (is_numeric($label)) {
            $this->failedAuthorization();
        }

        $this->model = label($label);

        $model_name = $this->model->model_name;

        if ($model_name != "Post") {
            $this->failedAuthorization();
        }

        if (!$this->model->exists) {
            $this->failedAuthorization();
        }
    }
}
