<?php

namespace Modules\PostLabels\Http\Requests;

class FolderSaveRequest extends DownstreamRequestsAbstract
{

    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return posttype()->canConfigureLabelFolders();
    }



    /**
     * @inheritdoc
     */
    public function normalRules()
    {
        return [
             "titles"    => "required|array|filled|min:1",
             "posttypes" => "required|array|filled|min:1",
        ];
    }



    /**
     * @inheritdoc
     */
    public function slugRules()
    {
        return [
             "slug" => $this->slugValidation(),
        ];
    }



    /**
     * validation slug
     *
     * @return string
     */
    public function slugValidation()
    {
        if ($this->model->slug != $this->getData('slug')) {
            $array_param = ['slug' => $this->getData('slug'), 'model_name' => 'Posttype'];
            $model       = $this->model->where($array_param);

            return $model->exists() ? 'invalid' : '';
        }

        return "";
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->filterTitlesArray();
        $this->convertPosttypes();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        if (!$this->model->exists) {
            $this->checkSlug();
        }

        $this->configureCustomOptions();
    }



    /**
     * configure custom options
     */
    private function configureCustomOptions()
    {
        $features = posttype()->availableLabelFeatures();
        $custom   = [];

        foreach ($features as $feature) {
            $custom[$feature] = (bool)$this->getData($feature);
        }

        $this->setData("custom", $custom);
    }



    /**
     * convert posttypes to array
     */
    private function convertPosttypes()
    {
        $array = explode_not_empty(",", $this->getData("posttypes"));

        $this->setData("posttypes", $array);
    }



    /**
     * If not exist slug, it would create a unique slug
     */
    private function checkSlug()
    {
        if (!$this->slug) {
            $this->createSlug();
        }
    }
}
