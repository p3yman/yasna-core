<?php

namespace Modules\PostLabels\Providers;

use Modules\PostLabels\Events\PostLabelSaved;
use Modules\PostLabels\Http\Endpoints\V1\ListEndpoint;
use Modules\PostLabels\Http\Endpoints\V1\PostsListEndpoint;
use Modules\PostLabels\Listeners\UpdatePostsAttachmentsWhenPostLabelSaved;
use Modules\PostLabels\Providers\Traits\ProviderFrontTools;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class PostLabelsServiceProvider
 *
 * @package Modules\PostLabels\Providers
 */
class PostLabelsServiceProvider extends YasnaProvider
{
    use ProviderFrontTools;




    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->registerModelTraits();
        $this->registerDownstream();
        $this->registerAssets();
        $this->registerPostHandlers();
        $this->registerEndpoints();
        $this->registerEventListeners();
    }



    /**
     * register model traits
     *
     * @return void
     */
    private function registerModelTraits()
    {
        $this->addModelTrait("PostLabelsTrait", "Post");
        $this->addModelTrait("PosttypeLabelsTrait", "Posttype");
        $this->addModelTrait("LabelPostsTrait", "Label");
    }



    /**
     * register downstream settings tabs, required for the purpose of this module.
     *
     * @return void
     */
    private function registerDownstream()
    {
        if ($this->cannotUseModule("Manage")) {
            return;
        }

        service('manage:downstream')
             ->add("post-labels")
             ->link("post-labels")
             ->trans("post-labels::downstream.title")
             ->method("PostLabels:DownstreamController@index")
             ->order(12)
        ;
    }



    /**
     * register module assets
     *
     * @return void
     */
    private function registerAssets()
    {
        if ($this->cannotUseModule("Manage")) {
            return;
        }

        module('manage')
             ->service('template_assets')
             ->add('post-labels-custom-css')
             ->link("postlabels:css/categories.min.css")
             ->order(44)
        ;

        module('manage')
             ->service('template_assets')
             ->add('post-labels-sortable-js')
             ->link("postlabels:libs/jquery-sortable-min.js")
             ->order(44)
        ;

        module('manage')
             ->service('template_assets')
             ->add('post-labels-custom-js')
             ->link("postlabels:js/categories.js")
             ->order(45)
        ;
    }



    /**
     * register post handlers
     *
     * @return void
     */
    private function registerPostHandlers()
    {
        $my_name = "post-labels";

        module("posts")
             ->service("editor_handlers")
             ->add($my_name)
             ->method("PostLabels:PostsController@registerHandlers")
        ;
        module("posts")
             ->service("browse_filters_handlers")
             ->add($my_name)
             ->method("PostLabels:PostsController@filters")
        ;

        module('posts')
             ->service('browse_headings_handlers')
             ->add($my_name)
             ->method("PostLabels:PostsController@browseHeadings")
        ;
    }



    /**
     * Registers the endpoints only when the `Endpoint` module is available.
     *
     * @return void
     */
    private function registerEndpoints()
    {

        if ($this->cannotUseModule("endpoint")) {
            return;
        }

        endpoint()->register(ListEndpoint::class);
        endpoint()->register(PostsListEndpoint::class);
    }



    /**
     * Register event listeners.
     *
     * @return void
     */
    protected function registerEventListeners()
    {
        $this->listen(PostLabelSaved::class, UpdatePostsAttachmentsWhenPostLabelSaved::class);
    }
}
