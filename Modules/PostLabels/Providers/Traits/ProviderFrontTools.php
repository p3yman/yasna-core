<?php

namespace Modules\PostLabels\Providers\Traits;

trait ProviderFrontTools
{
    /**
     * Converts hex color code to an array of rgb
     *
     * @param string $color
     *
     * @return array|null
     */
    public static function hexToArray($color)
    {
        $default = null;

        //Return default if no color provided
        if (empty($color)) {
            return $default;
        }

        //Sanitize $color if "#" is provided
        if ($color[0] == '#') {
            $color = substr($color, 1);
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
            $hex = [$color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]];
        } elseif (strlen($color) == 3) {
            $hex = [$color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]];
        } else {
            return $default;
        }

        //Convert hexadec to rgb
        $rgb = array_map('hexdec', $hex);


        //Return rgb(a) color string
        return $rgb;
    }



    /**
     * Finds brightness difference of two rgb colors
     * Second color in defaulted to white
     *
     * @param int $r1
     * @param int $g1
     * @param int $b1
     * @param int $r2
     * @param int $g2
     * @param int $b2
     *
     * @return integer
     */
    public static function colorDiff($r1, $g1, $b1, $r2 = 255, $g2 = 255, $b2 = 255)
    {
        return max($r1, $r2) - min($r1, $r2) +
             max($g1, $g2) - min($g1, $g2) +
             max($b1, $b2) - min($b1, $b2);
    }



    /**
     * Returns readable text color for a given color
     *
     * @param string $color
     *
     * @return string
     */
    public static function getTextColor($color)
    {
        $white = '#fff';
        $black = '#000';

        $color_array = self::hexToArray($color);

        if (!$color_array) {
            return $black;
        }

        $diff = self::colorDiff($color_array[0], $color_array[1], $color_array[2]);

        if ($diff > 500) {
            return $white;
        }

        return $black;
    }

}
