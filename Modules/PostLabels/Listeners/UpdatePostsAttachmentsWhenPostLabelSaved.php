<?php

namespace Modules\PostLabels\Listeners;

use App\Models\Label;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\PostLabels\Events\PostLabelSaved;

class UpdatePostsAttachmentsWhenPostLabelSaved implements ShouldQueue
{
    /** @var PostLabelSaved */
    public $event;



    /**
     * Handle the event.
     *
     * @param PostLabelSaved $event
     *
     * @return void
     */
    public function handle(PostLabelSaved $event)
    {
        $this->event = $event;

        if ($this->processIsNotNeeded()) {
            return;
        }

        $this->process();
    }



    /**
     * Checks if the process is needed.
     *
     * @return bool
     */
    protected function processIsNeeded(): bool
    {
        return (
             $this->updatingRequested()
             and
             $this->isUpdated()
             and
             $this->parentChanged()
        );
    }



    /**
     * Checks if the process is not needed.
     *
     * @return bool
     */
    protected function processIsNotNeeded(): bool
    {
        return !$this->processIsNeeded();
    }



    /**
     * Checks if the updating the post attachments is request.
     *
     * @return bool
     */
    protected function updatingRequested(): bool
    {
        return $this->event->update_posts;
    }



    /**
     * Checks if the model has been updated (and not created).
     *
     * @return bool
     */
    protected function isUpdated(): bool
    {
        $old_data = $this->event->old_data;
        $old_id   = ($old_data['id'] ?? 0);
        
        return boolval($old_data) and $old_id;
    }



    /**
     * Checks if the parent of the model has been modified.
     *
     * @return bool
     */
    protected function parentChanged(): bool
    {
        $old_parent_id = ($this->event->old_data['parent_id'] ?? null);
        $new_parent_id = ($this->event->model->parent_id ?? null);

        return (
             $old_parent_id
             and
             $new_parent_id
             and
             ($old_parent_id != $new_parent_id)
        );
    }



    /**
     * Does the main task of this listener and updates the attachments.
     */
    protected function process()
    {
        $this->attachNewParents();
        $this->detachOldParents();
    }



    /**
     * Attaches the new parents to the attached posts.
     */
    protected function attachNewParents()
    {
        $posts = $this->attachedPosts();

        foreach ($posts as $post) {
            $this->attachNewParentsToPost($post);
        }
    }



    /**
     * Attaches the new parents to the specified attached posts.
     *
     * @param Post       $post
     * @param Label|null $label
     */
    protected function attachNewParentsToPost(Post $post, ?Label $label = null)
    {
        $label = $label ?? $this->event->model;

        if ($label->model_name != $post->getClassName()) {
            return;
        }


        $parent = $label->parent();
        if ($parent->not_exists) {
            return;
        }

        $post->attachLabel($parent->slug);
        $this->attachNewParentsToPost($post, $parent);
    }



    /**
     * Detaches the old parents from the attached posts.
     */
    protected function detachOldParents()
    {
        $posts = $this->attachedPosts();

        foreach ($posts as $post) {
            $this->detachOldParentsFromPost($post);
        }
    }



    /**
     * Detaches the old parents from the specified attached posts.
     *
     * @param Post       $post
     * @param Label|null $label
     */
    protected function detachOldParentsFromPost(Post $post, ?Label $label = null)
    {
        $label = $label ?? label($this->event->old_data['id']);

        if ($label->model_name != $post->getClassName()) {
            return;
        }

        $parent = $label->parent();
        if ($parent->not_exists or $this->postHasLabelOfFamily($post, $parent)) {
            return;
        }

        $post->detachLabel($parent->slug);

        $this->detachOldParentsFromPost($post, $parent);
    }



    /**
     * Checks if the specified post has been attached to any child of the given parent label.
     *
     * @param Post  $post
     * @param Label $parent
     *
     * @return bool
     */
    protected function postHasLabelOfFamily(Post $post, Label $parent)
    {
        $children_ids = $parent->children()->pluck('id')->toArray();

        return boolval(
             $post
                  ->labels()
                  ->whereIn('labels.id', $children_ids)
                  ->first()
        );
    }



    /**
     * Returns the posts which have been attached to the model.
     *
     * @return Collection|Post[]
     */
    protected function attachedPosts()
    {
        return $this->event->model->models;
    }
}
