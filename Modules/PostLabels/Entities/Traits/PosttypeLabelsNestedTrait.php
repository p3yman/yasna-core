<?php

namespace Modules\PostLabels\Entities\Traits;

use App\Models\Label;
use Illuminate\Database\Eloquent\Builder;

trait PosttypeLabelsNestedTrait
{
    /**
     * keep a cached version of the labels array to prevent duplicate queries in a single request.
     *
     * @var null|array
     */
    private static $all_labels_array = null;



    /**
     * get nested array
     *
     * @param bool $with_trashed
     *
     * @return array
     */
    public static function getLabelsNestedArray($with_trashed = false): array
    {
        $array = static::getAllLabelsArray($with_trashed);

        foreach ($array as $id => $row) {
            if (!$row['parent_id']) {
                break;
            }

            $array[$row['parent_id']]['categories'][] = $array[$id];
            unset($array[$id]);
        }

        return $array;
    }



    /**
     * get a nested array of defined labels inside a specific folder
     *
     * @param int  $folder_id
     * @param bool $with_trashed
     *
     * @return array
     */
    public static function getFolderLabelsNestedArray(int $folder_id, bool $with_trashed = false): array
    {
        $array = static::getLabelsNestedArray($with_trashed);

        if (!isset($array[$folder_id]['categories'])) {
            return [];
        }

        return $array[$folder_id]['categories'];
    }



    /**
     * get a Builder instance of defined labels inside a specific folder
     *
     * @param int  $folder_id
     * @param int  $exception
     * @param bool $with_trashed
     *
     * @return Builder
     */
    public static function getFolderLabels(int $folder_id, int $exception = 0, bool $with_trashed = false)
    {
        return label()
             ->where("id", '!=', $exception)
             ->whereIn("id", static::getFolderLabelsIdsArray($folder_id, $with_trashed))
             ;
    }



    /**
     * get a flat array of defined labels's ids inside a specific folder
     *
     * @param int  $folder_id
     * @param bool $with_trashed
     *
     * @return array
     */
    public static function getFolderLabelsIdsArray(int $folder_id, bool $with_trashed = false)
    {
        $array  = static::getFolderLabelsNestedArray($folder_id, $with_trashed);
        $result = [];

        foreach (array_dot($array) as $item => $value) {
            if (ends_with($item, ".id")) {
                $result[] = $value;
            }
        }

        return $result;
    }



    /**
     * get a Builder of all labels, recursively attached to the current posttype
     *
     * @param bool $with_trashed
     *
     * @return Builder
     */
    public function getAllLabelsRecursively(bool $with_trashed = false)
    {
        $folder_ids = $this->labelFoldersIds($with_trashed);
        $array      = $folder_ids;

        foreach ($folder_ids as $id) {
            $array = array_merge($array, static::getFolderLabelsIdsArray($id, $with_trashed));
        }

        return label()->whereIn("id", $array)->orderBy("created_at");
    }



    /**
     * get a flat array representation of all the labels with Post or Posttype model names
     *
     * @param bool $with_trashed
     *
     * @return array
     */
    private static function getAllLabelsArray(bool $with_trashed): array
    {
        if (static::$all_labels_array) {
            return static::$all_labels_array;
        }

        $builder = label();
        if ($with_trashed) {
            $builder = $builder->withTrashed();
        }
        $labels = $builder
             ->whereIn("model_name", ["Post", "Posttype"])
             ->orderBy("parent_id", "desc")
             ->get()
        ;

        $array = [];
        foreach ($labels as $label) {
            $array[$label->id] = static::getLabelArray($label);
        }

        static::$all_labels_array = $array;

        return $array;
    }



    /**
     * convert a label into a properly formed array
     *
     * @param Label $label
     *
     * @return array
     */
    private static function getLabelArray($label): array
    {
        return [
             "id"         => $label->id,
             "hashid"     => $label->hashid,
             "title"      => $label->title,
             "slug"       => $label->slug,
             "parent_id"  => $label->parent_id,
             "active"     => $label->isActive(),
             "type"       => $label->type,
             "color"      => $label->color,
             "categories" => [],
        ];
    }
}
