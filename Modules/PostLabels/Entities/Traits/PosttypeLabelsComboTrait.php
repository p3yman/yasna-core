<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/23/19
 * Time: 9:53 AM
 */

namespace Modules\PostLabels\Entities\Traits;


use App\Models\Label;
use Illuminate\Database\Eloquent\Collection;

trait PosttypeLabelsComboTrait
{
    /**
     * Returns an array of the labels which can be attached to the posts of this posttype.
     * <br>
     * Items in this array have an additional field named `caption` which contains the titles of the parents.
     *
     * @return array
     */
    public function deepLabelsCombo()
    {
        /** @var Collection|Label[] $folders */
        $folders = $this->labelFolders;
        $result  = [];

        foreach ($folders as $folder) {
            $children = $this->deepLabelsComboChildren($folder, $folder->title);
            $result   = array_merge($result, $children);
        }

        return $result;
    }



    /**
     * Does the recursive part for the `deepLabelsCombo()` method.
     *
     * @param Label  $label
     * @param string $prefix
     *
     * @return array
     */
    protected function deepLabelsComboChildren(Label $label, string $prefix = '')
    {
        /** @var Collection|Label[] $children */
        $children = $label->children()->get();
        $prefix   = $prefix ? ($prefix . ' - ') : '';
        $result   = [];

        foreach ($children as $child) {
            $child->caption  = $prefix . $child->title;
            $result[]        = $child;
            $children_result = $this->deepLabelsComboChildren($child, $prefix . $child->title);
            $result          = array_merge($result, $children_result);
        }

        return $result;
    }
}
