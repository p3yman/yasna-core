<?php

namespace Modules\PostLabels\Entities\Traits;

use App\Models\Label;
use App\Models\Posttype;
use Illuminate\Database\Eloquent\Collection;
use Modules\Label\Entities\Traits\LabelsTrait;

/**
 * Class Post
 *
 * @property Posttype $posttype
 */
trait PostLabelsTrait
{
    use LabelsTrait;
    use PostLabelsSaveTrait;



    /**
     * get a mapped version of the given collection
     *
     * @param Collection $collection
     *
     * @return array
     */
    public static function getMappedFromCollection(Collection $collection): array
    {
        $result = $collection->map(function ($item) {
            /** @var Label $item */
            return [
                 'id'          => $item->hashid,
                 'parent_id'   => $item->parent_id ? hashid($item->parent_id) : null,
                 'title'       => $item->title,
                 'slug'        => $item->slug,
                 'picture'     => uploader()->filesArray((array)$item->getCustomMeta('pic')),
                 'text'        => $item->getCustomMeta('text'),
                 'description' => $item->getCustomMeta('desc'),
                 'color'       => $item->getCustomMeta('color'),
            ];
        });

        return $result->toArray();
    }



    /**
     * return mapped array of labels from the specified post
     *
     * @return array
     */
    public function mappedLabels()
    {
        return static::getMappedFromCollection($this->labels()->get());
    }



    /**
     * get a builder instance of label folders of the related posttype
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function labelFolders()
    {
        return $this->posttype->labelFolders();
    }



    /**
     * get an array of label folder ids
     *
     * @return array
     */
    public function labelFoldersIds()
    {
        return $this->posttype->labelFoldersIds();
    }



    /**
     * get allowed Parent Models
     *
     * @return array
     */
    protected static function allowedParentModels()
    {
        return [
             "Posttype",
        ];
    }



    /**
     * get Labels resource.
     *
     * @return array|null
     */
    protected function getLabelsResource()
    {
        if ($this->hasnot("labels")) {
            return null;
        }

        return $this->mappedLabels();
    }



    /**
     * get FolderLabels resource.
     *
     * @return array
     */
    protected function getFolderLabelsResource()
    {
        if ($this->hasnot("labels")) {
            return null;
        }

        return static::getMappedFromCollection($this->labelFolders()->get());
    }

}
