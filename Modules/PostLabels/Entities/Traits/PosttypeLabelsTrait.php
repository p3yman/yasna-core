<?php

namespace Modules\PostLabels\Entities\Traits;

use App\Models\Label;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Label\Entities\Traits\LabelsTrait;

trait PosttypeLabelsTrait
{
    use LabelsTrait;
    use PosttypeLabelsNestedTrait;
    use PosttypeLabelsPermissionTrait;
    use PosttypeLabelsSaveTrait;
    use PosttypeLabelsComboTrait;



    /**
     * get a builder instance of ATTACHED label folders
     *
     * @return BelongsToMany
     */
    public function labelFolders()
    {
        return $this->labels();
    }



    /**
     * get an array of label folder ids
     *
     * @param bool $with_trashed //TODO: to be applied!
     *
     * @return array
     */
    public function labelFoldersIds(bool $with_trashed = false)
    {
        return $this->labelFolders()->pluck('labels.id')->toArray();
    }



    /**
     * insert a new post label
     *
     * @param string       $slug
     * @param array|string $titles
     * @param int          $parent_id
     * @param array        $other_options
     *
     * @return Label
     */
    public function newPostLabel(string $slug, $titles, int $parent_id, array $other_options = []): Label
    {
        if (!$this->checkParentId($parent_id)) {
            return label();
        }

        $options = array_merge($other_options, [
             "parent_id" => $parent_id,
        ]);

        return label()->new("Post", $slug, $titles, $options); // <~~ Bypasses the method provided by the trait
    }



    /**
     * get available label features
     *
     * @param bool $with_activeness
     *
     * @return array
     */
    public function availableLabelFeatures(bool $with_activeness = true): array
    {
        $array = [
             "feature_active",
             "feature_color",
             "feature_pic",
             "feature_nested",
             "feature_desc",
             "feature_text",
        ];

        if (!$with_activeness) {
            array_forget($array, 0);
        }

        return $array;
    }



    /**
     * check if the suggested parent_is suitable for the current post and posttype
     *
     * @param int $id
     *
     * @return bool
     */
    private function checkParentId(int $id): bool
    {
        if (!$id) {
            return false;
        }

        $parent = label($id);
        if ($parent->model_name == "Post") { // <~~ A loose check! It actually doesn't check anything.
            return true;
        }

        return in_array($id, $this->labelFoldersIds());
    }
}
