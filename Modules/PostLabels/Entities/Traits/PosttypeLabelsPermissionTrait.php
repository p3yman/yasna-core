<?php

namespace Modules\PostLabels\Entities\Traits;

/**
 * Class Posttype (attached via PosttypeLabelsTrait)
 * This is to define access levels of each particular labels set of actions.
 *
 * @TODO: To be revised and made compatible with the posttype requirements.
 */
trait PosttypeLabelsPermissionTrait
{
    /**
     * check if the current user can configure labels
     *
     * @return bool
     */
    public function canConfigureLabels(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the current user can configure label folders
     *
     * @return bool
     */
    public function canConfigureLabelFolders(): bool
    {
        return user()->isSuperadmin();
    }



    /**
     * determine if the current user can create new labels
     *
     * @return bool
     */
    public function canCreateLabels(): bool
    {
        return user()->isSuperadmin();
    }
}
