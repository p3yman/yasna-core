<?php

namespace Modules\PostLabels\Events;

use App\Models\Label;
use Modules\Yasna\Services\YasnaEvent;

/**
 * Class PostLabelSaved
 *
 * @property Label      $model
 * @property Label|null $old
 */
class PostLabelSaved extends YasnaEvent
{
    /** @var string */
    public $model_name = 'Label';

    /** @var bool */
    public $update_posts;

    /** @var array */
    public $old_data;



    /**
     * PostLabelSaved constructor.
     *
     * @param int|string|Label|bool $model
     * @param array                 $old_data
     * @param bool                  $update_posts
     */
    public function __construct($model = false, $old_data = [], bool $update_posts = true)
    {
        $this->update_posts = $update_posts;
        $this->old_data     = $old_data;

        parent::__construct($model);
    }


}
