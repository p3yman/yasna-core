<?php
return [
     "title"           => "Categories",
     "new_folder"      => "Add New Folder",
     "folder_settings" => "Folder Settings",
     "dragging_lock"   => "Lock Dragging",
     "dragging_unlock" => "Unlock Dragging",
     "manage_trash"    => "Manage Trash",

];
