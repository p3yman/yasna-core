<?php
return [
     "title"      => "دسته‌بندی مطالب",
     "new_folder" => "افزودن پوشه جدید",

     "folder_settings" => "تنظیمات پوشه",
     "dragging_lock"   => "قفل جابه‌جایی با موس",
     "dragging_unlock" => "جابه‌جایی با موس",
     "manage_trash"    => "مدیریت زباله‌دان",

     "edit_folder"    => "ویرایش پوشه",
     "posttypes"      => "مطالب تحت پوشش",
     "title_in"       => "عنوان در",
     "features"       => "ویژگی‌ها",
     "feature_active" => "فعال و قابل استفاده",
     "feature_color"  => "امکان انتخاب رنگ",
     "feature_pic"    => "امکان انتخاب عکس",
     "feature_nested" => "امکان تنظیم دسته‌های تو در تو",
     "feature_desc"   => "امکان درج توضیح کوتاه",
     "feature_text"   => "امکان درج توضیح مفصل",

     "titles_required"        => "وارد کردن یک عنوان لازم است.",
     "posttypes_required"     => "هیچ مطلبی تحت پوشش این پوشه قرار نگرفته است.",
     "folder_hashid_accepted" => "پوشه معتبر نیست. ممکن است توسط شخص دیگری پاک شده باشد.",

     "new_label"          => "افزودن دستهٔ جدید",
     "edit_label"         => "ویرایش دسته‌بندی",
     "parent"             => "دستهٔ والد",
     "no_parent"          => "بدون والد",
     "parent_update"      => "با تغییر دستهٔ والد، تمام پست‌های دارای این دسته نیز به‌روزرسانی شوند.",
     "color"              => "رنگ برچسب",
     "folder_slug_unique" => "نامک تکراری است",
];
