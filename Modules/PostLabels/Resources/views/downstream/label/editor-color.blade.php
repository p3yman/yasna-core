{!!
widget("color-picker")
	->name("color")
	->value($model->getCustomMeta("color"))
	->inForm()
	->label("tr:post-labels::downstream.color")
!!}
