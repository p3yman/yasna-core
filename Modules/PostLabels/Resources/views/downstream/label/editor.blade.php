{!!
widget("modal")
	->label("trans:post-labels::downstream." . ($model->exists? "edit_label" : "new_label"))
	->target("name:postLabels-label-save")
!!}

<div class="modal-body">

    @include("post-labels::downstream.label.editor-body")

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
