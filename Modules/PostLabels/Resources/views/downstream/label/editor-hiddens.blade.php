{!! widget('hidden')->name('hashid')->value($model->hashid) !!}
{!! widget('hidden')->name('folder_hashid')->value($folder->hashid) !!}

{!!
widget("input")
    ->label("id")
    ->inForm()
    ->condition(dev())
    ->value($model->id)
    ->disabled()
!!}


{!!
widget("input")
    ->label("folder_id")
    ->inForm()
    ->condition(dev())
    ->value($folder->id)
    ->disabled()
!!}

{!!
widget("input")
	->name('slug')
	->inForm()
	->condition(dev())
	->value($model->slug)
!!}



