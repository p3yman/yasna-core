{!!
widget("combo")
     ->name("parent_id")
     ->inForm()
     ->options( $options = posttype()::getFolderLabels($folder->id, $model->id)->get())
     ->value(hashid($model->parent_id))
     ->valueField('hashid')
     ->captionField('title')
     ->label("tr:post-labels::downstream.parent")
     ->searchableIf($options->count() < 5)
     ->blankCaption("tr:post-labels::downstream.no_parent")
     ->blankValue(0)
     ->onChange("$('#chkPostUpdate-container').slideDown('fast')")
!!}

{!!
widget("checkbox")
	->name("post_update")
	->id("chkPostUpdate")
	->inForm()
	->label("tr:post-labels::downstream.parent_update")
	->condition($model->exists and dev())
!!}

<script>
    $("#chkPostUpdate-container").hide();
</script>
