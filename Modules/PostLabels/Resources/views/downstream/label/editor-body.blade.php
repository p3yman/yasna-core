@include("post-labels::downstream.label.editor-hiddens")
@include("post-labels::downstream.folder.editor-titles") {{-- <~~ reads from folder titles blade --}}

@if($folder->hasFeature("nested"))
    @include("post-labels::downstream.label.editor-parent")
@endif

@if($folder->hasFeature("color"))
    @include("post-labels::downstream.label.editor-color")
@endif

@if($folder->hasFeature("desc"))
    @include("post-labels::downstream.label.editor-desc")
@endif

@if($folder->hasFeature("text"))
    @include("post-labels::downstream.label.editor-text")
@endif

@if($folder->hasFeature("pic"))
    @include("post-labels::downstream.label.editor-pic")
@endif

@include("post-labels::downstream.label.editor-activeness")

