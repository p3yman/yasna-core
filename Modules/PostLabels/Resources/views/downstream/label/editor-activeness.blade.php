{!!
widget("checkbox")
     ->name("activeness")
     ->value($model->isNotTrashed())
     ->inForm()
     ->label("trans:post-labels::downstream.feature_active")
!!}
