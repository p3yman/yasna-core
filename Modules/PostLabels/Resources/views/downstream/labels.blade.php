@if(isset($categories) and $categories and count($categories))
	@foreach($categories as $category)

		@php
			$color = '#000';
			$bg = '#f7f7f7';

			if(isset($category['color']) and $category['color']){
				$color = module('PostLabels')->callProviderMethod('getTextColor', $category['color']);
				$bg = $category['color'];
			}

			$link = route("postLabels-label-edit", [
				"hashid" => $category['hashid'],
			]);
		@endphp

		<div class="category_label {{ $category['active']? '' : 'deactivated' }}"
			 style="background-color: {{ $bg }}; color: {{ $color }};
					 {{ $category['active']? '' : 'display: none'  }}"
		>
            <span class="category_title">
                {{ $category['title'] }}
            </span>

			<div class="category_buttons">
                <span class="category_button" onclick="masterModal('{{ $link }}')">
                    <i class="fa fa-edit"></i>
                </span>
				{{--<span class="category_button js_categoryDelete" style="display: none;">--}}
                    {{--<i class="fa fa-trash"></i>--}}
                {{--</span>--}}
			</div>

		</div>
	@endforeach
@endif

