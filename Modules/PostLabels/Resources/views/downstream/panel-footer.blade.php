{!!
    widget("button")
         ->label("tr:post-labels::downstream.new_label")
         ->icon("tag")
         ->class("btn-taha")
          ->onClick("postLabelsEditor('".
              route("postLabels-label-create" , [
                  "slug" => "-SLUG-",
              ])
          ."')")
         ->condition(posttype()->canCreateLabels())
    !!}

<div class="right" style="display: none;">
    {!!
      widget('button')
          ->label('tr:post-labels::downstream.folder_settings')
          ->icon('cogs')
          ->class('btn-taha btn-inverse')
          ->onClick("postLabelsEditor('".
              route("postLabels-folder-edit" , [
                  "slug" => "-SLUG-",
              ])
          ."')")
          ->condition(posttype()->canConfigureLabelFolders())
    !!}

</div>

<div class="left">
    {!!
         widget('button')
         ->icon('lock m0')
         ->shape('danger')
         ->id('js_unlockEdit')
         ->class('js_toggle_edit_lock')
    !!}

    {!!
         widget('button')
         ->icon('unlock m0')
         ->shape('warning')
         ->class('js_toggle_edit_lock')
         ->id('js_lockEdit')
         ->style('display: none;')
    !!}
</div>


