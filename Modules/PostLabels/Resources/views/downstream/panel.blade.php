<div class="categories_browse bt0">

    <div class="categories_sidebar">
        @include("post-labels::downstream.panel-sidebar")
    </div>

    <div class="categories_body">
        <div class="categories_content" data-active-panel="">
            @include("post-labels::downstream.panel-content")
        </div>
        <div class="categories_footer">
            @include("post-labels::downstream.panel-footer")
        </div>
    </div>

</div>

{{--
	Hidden input containing the order of nested categories
--}}
{!!
	widget('hidden')
	->id('js_categoriesOrder')
!!}
