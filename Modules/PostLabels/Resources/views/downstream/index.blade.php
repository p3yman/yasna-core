@extends('manage::layouts.template')

@section('content')
    @include("manage::downstream.tabs")

    <div id="divPostLabelsPanel" data-src="{{ route("postLabels-panel-refresh") }}">
        @include("post-labels::downstream.panel")
    </div>
@endsection
