<ul class="categories_folders">

    @foreach($nested as $folder)
        @if(isset($folder['active']))

            <li class="categories_folder {{ $folder['active']? '' : 'deactivated' }}"
                style="{{ $folder['active']? '' : 'display: none' }}">

                <a href="#{{ $folder['slug'] }}">
                    {{ $folder['title'] }}
                </a>
            </li>

        @endif
    @endforeach

</ul>


<div class="categories_footer">
    {!!
        widget('button')
        ->label('tr:post-labels::downstream.new_folder')
        ->condition(posttype()->canConfigureLabelFolders())
        ->target("modal:".route("postLabels-folder-create"))
        ->shape('primary')
    !!}
</div>
