@foreach($nested as $folder)
    @if(isset($folder['active']))

        <div class="categories_inner_body" id="{{ $folder['slug'] }}" style="display: none;">

            @if($folder['type'] === "flat")
                <div class="categories_labels">
                    @include('post-labels::downstream.labels',[
                        "categories" => isset($folder['categories']) ? $folder['categories'] : [] ,
                    ])
                </div>
            @elseif($folder['type'] === "nested")
                <div class="categories_nested">
                    <ol class="categories_list" id="{{ $folder['slug'] }}">
                        @include("post-labels::downstream.panel-nested", [
                            "categories" => $folder['categories'],
                            "parent_id" => $folder['slug'] ,
                        ])
                    </ol>
                </div>
            @endif


        </div>
    @endif
@endforeach
