{!!
widget("group")
	->start()
	->label("trans:post-labels::downstream.features")
!!}

@foreach(posttype()->availableLabelFeatures() as $feature)
    {!!
    widget("checkbox")
    	->name($feature)
    	->value($model->$feature)
    	->label("trans:post-labels::downstream.$feature")
    !!}
@endforeach

{!!
widget("group")
	->stop()
!!}
