{!!
widget("modal")
	->label("trans:post-labels::downstream." . ($model->exists? "edit_folder" : "new_folder"))
	->target("name:postLabels-folder-save")
!!}

<div class="modal-body">

    @include("post-labels::downstream.folder.editor-body")

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
