{!!
widget("selectize")
     ->name("posttypes")
     ->inForm()
     ->value( $model->posttypes )
     ->options(posttype()->having('labels')->get())
     ->valueField('id')
     ->captionField('title')
     ->label("tr:post-labels::downstream.posttypes")
!!}
