@foreach($locales = get_setting("site_locales") as $locale)
    {!!
    widget("input")
     ->name("titles[$locale]")
     ->label(trans("post-labels::downstream.title_in") . SPACE . trans("yasna::lang.$locale"))
     ->value($model->titleIn($locale))
     ->inForm()
    !!}
@endforeach

{{--{!! widget("separator")->condition(count($locales)>1) !!}--}}
