{!! widget('hidden')->name('hashid')->value($model->id) !!}

{!!
widget("input")
	->name('slug')
	->inForm()
	->value($model->slug)
!!}

@include("post-labels::downstream.folder.editor-titles")
@include("post-labels::downstream.folder.editor-posttypes")
@include("post-labels::downstream.folder.editor-features")
