{{--
|--------------------------------------------------------------------------
| Recursive blade
| !! ATTENTION !! This blade is being called recursively. Be careful about the
|            codes being written in this blade.
|--------------------------------------------------------------------------
|
--}}

{{--<ol class="categories_list" id="{{ $parent_id }}">--}}
    @if(isset($categories) and $categories and count($categories))
        @foreach($categories as $category)
            @php
            $link = route("postLabels-label-edit", [
                "hashid" => $category['hashid'],
            ]);
            @endphp
            <li class="categories_item" data-id="{{$category['slug']}}">
                <div class="category_box {{ $category['active']? '' : 'deactivated' }}" style="{{ $category['active']? '' : 'display: none' }}">
					<span class="category_title_box">
						{{--<span class="category_drag_handle" style="display: none;">--}}
							{{--<i class="fa fa-bars"></i>--}}
						{{--</span>--}}
						<span class="mr-lg"></span>

						{{ $category['title'] }}

						<span class="mr"></span>

						@if(isset($category['color']) and $category['color'])
							<span class="category_color label" style="background-color: {{ $category['color'] }}"></span>
						@endif
					</span>


                    <div class="category_buttons">
                        {{--<span class="category_button js_categoryDelete text-red" style="display: none;">--}}
                            {{--<i class="fa fa-trash m0"></i>--}}
                        {{--</span>--}}
                        <span class="category_button text-inverse" onclick="masterModal('{{$link}}')">
                            <i class="fa fa-edit m0"></i>
                        </span>
                    </div>
                </div>
				<ol class="categories_list" data-id="{{ $category['slug']}}">

					@if($category['categories'] and count($category['categories']))
						@include("post-labels::downstream.panel-nested",[
							  "categories" => $category['categories'] ,
							  "parent_id" => $category['slug'] ,
						])
					@endif
				</ol>
            </li>
        @endforeach
    @endif
{{--</ol>--}}
