@if ($posttype->hasnot('labels'))
	@php return @endphp
@endif

@php
	$options = $posttype->deepLabelsCombo();
@endphp

@if (empty($options))
    @php return @endphp
@endif

<div class="col-md-6">
	{!!
		widget('combo')
			->name('label')
			->options($options)
			->valueField('slug')
			->captionField('caption')
			->searchable()
			->blankValue('')
			->blankCaption('')
			->value(request()->get('label'))
			->label("tr:post-labels::downstream.title")
	!!}
</div>
