@php
	$labels = $model->labels;
@endphp

@foreach ($labels as $label)
    @include('manage::widgets.grid-badge', [
    	'text' => $label->title,
    	'color' => 'info'
    ])
@endforeach
