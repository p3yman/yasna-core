<ul class="category_list">
    @foreach($categories as $category)
        <li class="category_item">
            {!!
                 widget('checkbox')
                 ->label($category['title'])
                 ->name("_label[{$category['slug']}]")
                 ->id($category['slug'])
                 ->value(in_array($category['slug'], $value))
            !!}

            @if(isset($category['categories']) and $category['categories']  and count($category['categories']))
                @include("post-labels::post-selector.nested-labels" , [
                   "categories" => $category['categories'] ,
                ])
            @endif
        </li>
    @endforeach
</ul>
