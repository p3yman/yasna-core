@if(count($folder['categories']))
    <div class="panel categories_folder_panel">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#{{ $parent_id }}" href="#{{ $folder['slug'] }}">
                    {{ $folder['title'] }}
                </a>
            </div>
        </div>

        <div id="{{ $folder['slug'] }}" class="panel-collapse collapse {{ $is_open ? 'in' : '' }}">
            <div class="panel-body ">
                @if($type === 'nested')
                    <div class="categories_nested">
                        @include("post-labels::post-selector.nested-labels" , [
                            "categories" => $folder['categories'],
                        ])
                    </div>
                @elseif($type === 'flat')
                    <div class="categories_label">
                        @include("post-labels::post-selector.flat-labels",[
                            "categories" => $folder['categories'] ,
                            "folder_slug" => $folder['slug']
                        ])
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif
