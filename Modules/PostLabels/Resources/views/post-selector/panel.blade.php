<div id="divLabels" class="panel panel-default">

    {{--
    |--------------------------------------------------------------------------
    | Header
    |--------------------------------------------------------------------------
    |
    --}}

    <div class="panel-heading">
        {{ trans('post-labels::editor-panel.title') }}
        <a href="{{v0()}}" onclick="postLabelsEditorPanelRefresh()" class="pull-right">
            <i class="fa fa-refresh text-gray"></i>
        </a>
    </div>


    {{--
    |--------------------------------------------------------------------------
    | Body
    |--------------------------------------------------------------------------
    |
    --}}
    <div id="divLabelsInside" data-src="{{route('postLabels-editor-panel-refresh',[
        "hashid" => $model->hashid,
        "type" => $model->posttype->hashid,
    ])}}"
         class="panel-body categories_widget" style="max-height: 400px;">


        @include("post-labels::post-selector.panel-inside")

    </div>

    
    {{--
    |--------------------------------------------------------------------------
    | Downstream Button
    |--------------------------------------------------------------------------
    |
    --}}
    <div class="panel-footer text-center mv5">
        {!!
       widget("button")
            ->onClick("window.open('".url("/manage/downstream/post-labels")."')")
            ->label("tr:post-labels::editor-panel.config")
            ->icon('clog')
            ->condition($model->posttype->canConfigureLabels())
       !!}
    </div>
</div>
