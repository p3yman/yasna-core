<div class="panel-group categories_accordion" id="categories_accordion">
    @foreach($model->posttype->labels as $folder)
        @include("post-labels::post-selector.folders" , [
            "nested" => $nested = posttype()::getLabelsNestedArray(),
            "folder" => $nested[$folder->id],
            "parent_id" => "categories_accordion" ,
            "is_open" => ($loop->iteration === 1) ,
            "type" => $folder->hasFeature('nested')? "nested" : "flat" ,
            "value" => $model->labelsArray(),
        ])
    @endforeach
</div>


@include("post-labels::post-selector.scripts")
