@php
	$slugs = array_column($categories, 'slug');
	$selected = array_intersect($slugs, $value);
@endphp

<div style="padding-bottom: 50px">
	{!!
		widget('selectize')
			->options($categories)
			->valueField('slug')
			->captionsField('title')
			->name("_flat_labels[$folder_slug]")
			->value(implode(',', $selected))
	!!}
</div>
