<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\DefectsController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-repair-order-defects-get
 *                    Repair Order Get Defects
 * @apiDescription    Getting the list of defects, saved by an operator
 * @apiVersion        1.0.0
 * @apiName           Repair Order Get Defects
 * @apiGroup          MobileSalam
 * @apiPermission     Operator and Repairer
 * @apiParam {string} id      order hashid
 * @apiSuccessExample Success-Response: TODO: This is the default save feedback. Change if your work is different
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method DefectsController controller()
 */
class RepairOrderDefectsGetEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Order Get Defects";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("operator", "super-operator", "repairer");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'DefectsController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1,100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
