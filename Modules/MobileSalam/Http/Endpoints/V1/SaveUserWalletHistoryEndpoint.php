<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\WalletController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-save-user-wallet-history
 *                    Save User Wallet History
 * @apiDescription    Save new user wallet history entry
 * @apiVersion        1.0.0
 * @apiName           Save User Wallet History
 * @apiGroup          MobileSalam
 * @apiPermission     Operator, Super Operator
 * @apiParam {string} id            the hashid of the user.
 * @apiParam {int}    amount        the amount to be charged with +/- (positive/negative)
 * @apiParam {string} account       the hashid of the account to be used
 * @apiParam {string} [title]       a custom title for the transaction
 * @apiParam {string} [description] a custom description for the transaction
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *            "account": "QKgJx",
 *            "amount": "-1000",
 *            "authenticated_user": "AdzyK"
 *      },
 *      "results": {
 *            "tracking_number": 1009,
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method WalletController controller()
 */
class SaveUserWalletHistoryEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Save User Wallet History";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'WalletController@saveUserHistory';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "ok" => "1",
        ], [
             "account"            => hashid(rand(1, 100)),
             "amount"             => rand(-10000, 10000),
             "authenticated_user" => hashid(rand(1, 100)),
             "tracking_number"    => rand(1000, 9999),
        ]);
    }
}
