<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-save-my-profile
 *                    Save My Profile
 * @apiDescription    Save Own Profile
 * @apiVersion        1.0.0
 * @apiName           Save My Profile
 * @apiGroup          MobileSalam
 * @apiPermission     User
 * @apiParam {string}   name_first     The first name of user
 * @apiParam {string}   name_last      The last name of user
 * @apiParam {int}      gender         The user gender
 * @apiParam {int}      mobile         The user mobile
 * @apiParam {int}      [code_melli]   The melli code of user
 * @apiParam {string}   [email]        The user email
 * @apiParam {string}   [home_address] The user address
 * @apiParam {int}      [postal_code]  The user postal code
 * @apiParam {int}      [tel]          The user phone number
 * @apiParam {int}      [city]         The user city id
 * @apiParam {float}    [longitude]    The user map coordination longitude
 * @apiParam {float}    [latitude]     The user map coordination latitude
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample   TODO: Follow the below example and change it as appropriate!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method UserController controller()
 */
class SaveMyProfileEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Save My Profile";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@saveMyProfile';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1,100)),
        ]);
    }
}
