<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-repair-order-repairer-estimation
 *                    Repair Order Repairer Estimation
 * @apiDescription    Repairer change estimation Form
 * @apiVersion        1.0.0
 * @apiName           Repair Order Repairer Estimation
 * @apiGroup          MobileSalam
 * @apiPermission     Repairer
 * @apiParam {string} id                    hashid of the order
 * @apiParam {float}  [new_price_estimation[  new price estimation
 * @apiParam {int}    [new_time_estimation]   new time estimation
 * @apiParam {string} [comment]               the custom comment
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class RepairOrderRepairerEstimationEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Order Repairer Estimation";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->is("repairer");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@repairerEstimation';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
    }
}
