<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\IndentController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-indent-part-review
 *                    Indent Review Part
 * @apiDescription    feedback repairer after repair
 * @apiVersion        1.0.0
 * @apiName           Indent Review Part
 * @apiGroup          MobileSalam
 * @apiPermission     repairer
 * @apiParam {string} hashid the hashid of indent
 * @apiParam {string} return_reason the slug of the return_reason combo
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *       "status": 200,
 *      "metadata": {
 *          "hashid": "kNrZN",
 *          "authenticated_user": "gxnDN"
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method IndentController controller() //
 */
class IndentPartReviewEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Indent Review Part";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->is('repairer');
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'IndentController@partReview';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(1),
        ]);
    }
}
