<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-get-repairers
 *                    Get Repairer List
 * @apiDescription    Get the list of repairers
 * @apiVersion        1.0.0
 * @apiName           Get Repairer List
 * @apiGroup          MobileSalam
 * @apiPermission     Guest
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *              [
 *                    "id": hashid(user),
 *                    "name": dummy()::persianName()
 *              ]
 *              [
 *                    "id": hashid(user),
 *                    "name": dummy()::persianName()
 *              ]
 *              [
 *                    "id": hashid(user),
 *                    "name": dummy()::persianName()
 *              ]
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class GetRepairersEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get Repairer List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@repairerList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "results" => [
                  [
                       [
                            "id"   => "KeJyN",
                            "name" => dummy()::persianName(),
                       ],
                       [
                            "id"   => "AGmZK",
                            "name" => dummy()::persianName(),
                       ],
                       [
                            "id"   => "AEjlx",
                            "name" => dummy()::persianName(),
                       ],
                  ],
             ],
        ]);

    }
}
