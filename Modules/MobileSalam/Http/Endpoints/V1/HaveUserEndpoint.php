<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\UserController;

/**
 * @api               {Post}
 *                    /api/modular/v1/mobile-salam-have-user
 *                    Have User
 * @apiDescription    Identify the user and send the token for each request
 * @apiVersion        1.0.0
 * @apiName           Have User
 * @apiGroup          MobileSalam
 * @apiPermission     Guest
 * @apiParam {string} field  the field name, in the users table to be checked in.
 * @apiParam {string} value  the value of which the user is checked against.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      },
 *      "results": {
 *          "hashid" => "sGhLkDs",
 *      }
 * }
 * @method UserController controller()
 */
class HaveUserEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Have User";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@haveUser';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "hashid" => hashid(rand(1, 100)),
        ], [
        ]);
    }
}
