<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-repair-order-submit
 *                    Repair Order Submit
 * @apiDescription    Create a new repair order, by either the user or the operator.
 * @apiVersion        1.0.0
 * @apiName           Repair Order Submit
 * @apiGroup          MobileSalam
 * @apiPermission     User
 * @apiParam {string}      [customer_id]        the customer's user id, only by the operators
 * @apiParam {string}      [application_type]   selected from combo, only by the operators
 * @apiParam {string}      [device_id]          device (post) hashid, selected from combo
 * @apiParam {string}      [color_id]           color (label) hashid, selected from combo
 * @apiParam {string}      [unknown_device]     free text for the device name, in case the user was not sure
 * @apiParam {string}      [unknown_problem]    free text for the problem, in case the user was not sure
 * @apiParam {string}      [problems]           array of problem hashids
 * @apiParam {boolean=0,1} [is_repaired_before] indicates whether or not the device has ever been repair before
 * @apiParam {string}      [customer_note]      custom explanations about the problem
 * @apiParam {string}      [estimated_price]    estimated price, calculated by the front-end
 * @apiParam {string}      [estimated_time]     estimated time, calculated by the front-end
 * @apiParam {string}      [serial]             serial number of the device
 * @apiParam {string}      [mobile]             mobile number of the user.(required if customer_id not exist, only by the operators)
 * @apiParam {string}      [name_first]         first name of the user.(required if customer_id not exist, only by the operators)
 * @apiParam {string}      [name_last]          last name of the user.(required if customer_id not exist, only by the operators)
 * @apiParam {string}      [home_address]       the home address of the user, only by the operators.
 * @apiParam {string}      [neighborhood]       the hashid of the neighborhood.(required if customer_id not exist, only by the operators)
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "mobile-salam::developerMessages.422",
 *      "userMessage": "mobile-salam::userMessages.422",
 *      "errorCode": 422,
 *      "moreInfo": "mobile-salam.moreInfo.422",
 *      "errors": {
 *      "device_type_id": [
 *           [
 *                "Device Type Id is Required."
 *           ]
 *      ]
 * }
 * @method RepairController controller()
 */
class RepairOrderSubmitEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Order Submit";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("member", "operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        if ($this->user->is("member")) {
            return 'RepairController@newOrder';
        }

        return 'RepairController@newOrderByOperator';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
    }
}
