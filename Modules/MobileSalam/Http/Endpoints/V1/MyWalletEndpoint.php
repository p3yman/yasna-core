<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\WalletController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-my-wallet
 *                    My Wallet
 * @apiDescription    Get fresh credit of the current user
 * @apiVersion        1.0.0
 * @apiName           My Wallet
 * @apiGroup          MobileSalam
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "credit": "12000"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "::developerMessages.endpoint-403",
 *      "userMessage": "::userMessages.endpoint-403",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": ".moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method WalletController controller()
 */
class MyWalletEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "My Wallet";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'WalletController@myWallet';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "credit" => "13000",
        ]);
    }
}
