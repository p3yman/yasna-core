<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\CartController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-cart-pay
 *                    Cart Pay (Shop Payment)
 * @apiDescription    Start payment procedure of a shopping cart.
 * @apiVersion        1.0.0
 * @apiName           Cart Pay (Shop Payment)
 * @apiGroup          MobileSalam
 * @apiPermission     Customer
 * @apiParam {string}  id         hashid of the shopping cart in question
 * @apiParam {string}  account    the hashid of the account to be used
 * @apiParam {boolean} use_wallet identify if wallet is going to be used.
 * @apiParam {string}  [coupon]   one (and only one) discount coupon to be applied on the payment.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "order": "QKgJx",
 *          "account": "QKgJx",
 *          "amount": 24970,
 *          "authenticated_user": "oxljN",
 *          "coupon: "gift-from-taha"
 *      },
 *      "results": {
 *          "wallet_share": 24970,
 *          "wallet_tracking_number": "1038",
 *          "online_share": 0,
 *          "online_gateway_url": "http://yasna.sexy/globe/mobile-salam/repair-payment-dive",
 *          "online_tracking_number": null,
 *          "discounted":1000
 *      }
 * }
 * @method CartController controller()
 */
class CartPayEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Cart Pay (Shop Payment)";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CartController@paymentStart';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "wallet_share"           => 24970,
             "wallet_tracking_number" => "1038",
             "online_share"           => 0,
             "online_gateway_url"     => "http://yasna.sexy/globe/mobile-salam/repair-payment-dive",
             "online_tracking_number" => null,
             "discounted"             => 1000,
        ], [
             "order"              => "QKgJx",
             "account"            => "QKgJx",
             "amount"             => 24970,
             "authenticated_user" => "oxljN",
             "coupon"             => "gift-1234-folan",
        ]);
    }
}
