<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-repair-order-counter
 *                    Order Counter
 * @apiDescription    Overall counter of the repair process, based on the user role.
 * @apiVersion        1.0.0
 * @apiName           Order Counter
 * @apiGroup          MobileSalam
 * @apiPermission     Operator, Repairer or Supplier
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *           "pending": 27,
 *           "waiting_for_device": 1,
 *           "processing": 0,
 *           "repaired": 1,
 *           "rejected": 1,
 *           "cancelled_by_customer": 0,
 *           "cancelled_by_repairer": 0,
 *           "waiting_for_send": 12,
 *           "indents_pending": 18,
 *           "indents_factory_fault": 1,
 *           "indents_repairer_fault": 1,
 *           "indents_guarantee": 0
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class RepairOrderCounterEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Order Counter";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isAnyOf([
             "superadmin",
             "operator",
             "super-operator",
             "repairer",
             "supplier",
        ]);
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        if (user()->isSuper()) {
            return 'RepairController@countForSuperadmin';
        }

        if (user()->isAnyOf("operator", "super-operator")) {
            return 'RepairController@countForOperator';
        }

        if (user()->is("repairer")) {
            return 'RepairController@countForRepairer';
        }

        if (user()->is("supplier")) {
            return 'RepairController@countForSupplier';
        }

    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "id" => hashid(rand(1, 100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
