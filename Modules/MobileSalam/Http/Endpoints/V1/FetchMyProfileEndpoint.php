<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\UserController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-fetch-my-profile
 *                    Edit My Profile
 * @apiDescription    Fetch Profile Edit Form
 * @apiVersion        1.0.0
 * @apiName           Edit My Profile
 * @apiGroup          MobileSalam
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *                    "form"[
 *                     {
 *                      "component_name": "text",
 *                      "order": 11,
 *                      "id": "",
 *                      "name": "name_first",
 *                      "css": "",
 *                      "css_class": "",
 *                      "width": "",
 *                      "col": "",
 *                      "default": "",
 *                      "value": "",
 *                      "placeholder": "",
 *                      "validation": "",
 *                      "label": "",
 *                      "label_desc": "",
 *                      "desc": "",
 *                      "clone": "",
 *                      "size": "",
 *                      "char_limit": "",
 *                      "prepend": "",
 *                      "append": "",
 *                      "disabled": false,
 *                      "readonly": false
 *                    }
 *                ],
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @method UserController controller()
 */
class FetchMyProfileEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Fetch My Profile (Form Data)";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@profileForm';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'status'   => 200,
             'metadata' =>
                  [
                       'form' =>[
                            [
                                 "component_name" => "text",
                                 "order"          => 11,
                                 "id"             => "",
                                 "name"           => "name_first",
                                 "css"            => "",
                                 "css_class"      => "",
                                 "width"          => "",
                                 "col"            => "",
                                 "default"        => "",
                                 "value"          => "",
                                 "placeholder"    => "",
                                 "validation"     => "",
                                 "label"          => "",
                                 "label_desc"     => "",
                                 "desc"           => "",
                                 "clone"          => "",
                                 "size"           => "",
                                 "char_limit"     => "",
                                 "prepend"        => "",
                                 "append"         => "",
                                 "disabled"       => false,
                                 "readonly"       => false,
                            ],
                       ],
                       'authenticated_user' => 'qKXVA',
                  ],
             'results'  => [] ,

        ]);
    }
}
