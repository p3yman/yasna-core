<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\UserController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-register-form
 * @apiDescription    The user register form
 * @apiVersion        1.0.0
 * @apiName           The user register form
 * @apiGroup          MobileSalam
 * @apiPermission     Guest
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *                    "form"[
 *                     {
 *                      "component_name": "text",
 *                      "order": 11,
 *                      "id": "",
 *                      "name": "name_first",
 *                      "css": "",
 *                      "css_class": "",
 *                      "width": "",
 *                      "col": "",
 *                      "default": "",
 *                      "value": "",
 *                      "placeholder": "",
 *                      "validation": "",
 *                      "label": "",
 *                      "label_desc": "",
 *                      "desc": "",
 *                      "clone": "",
 *                      "size": "",
 *                      "char_limit": "",
 *                      "prepend": "",
 *                      "append": "",
 *                      "disabled": false,
 *                      "readonly": false
 *                    }
 *                ],
 *          "authenticated_user" => hashid(1),
 *      },
 *      "results": []
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": ,
 *      "developerMessage": "",
 *      "userMessage": "",
 *      "errorCode": "",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method UserController controller()
 */
class RegisterFormEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "The user register form";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@registerForm';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'status'   => 200,
             'metadata' =>
                  [
                       'form' =>[
                            [
                                 "component_name" => "text",
                                 "order"          => 11,
                                 "id"             => "",
                                 "name"           => "name_first",
                                 "css"            => "",
                                 "css_class"      => "",
                                 "width"          => "",
                                 "col"            => "",
                                 "default"        => "",
                                 "value"          => "",
                                 "placeholder"    => "",
                                 "validation"     => "",
                                 "label"          => "",
                                 "label_desc"     => "",
                                 "desc"           => "",
                                 "clone"          => "",
                                 "size"           => "",
                                 "char_limit"     => "",
                                 "prepend"        => "",
                                 "append"         => "",
                                 "disabled"       => false,
                                 "readonly"       => false,
                            ],
                       ],
                       'authenticated_user' => 'qKXVA',
                  ],
             'results'  => [] ,

        ]);
    }
}

