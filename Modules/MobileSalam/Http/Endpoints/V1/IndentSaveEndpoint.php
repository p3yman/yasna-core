<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\IndentController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-indent-save
 *                    Save Indent
 * @apiDescription    create new indent by repairer
 * @apiVersion        1.0.0
 * @apiName           Save Indent
 * @apiGroup          MobileSalam
 * @apiPermission     repairer
 * @apiParam {string}  ware_id hashid of ware
 * @apiParam {string}  order_id hashid of salam-order
 * @apiParam {string}  defect_id hashid of salam-defect
 * @apiParam {integer} quantity number of quantity
 * @apiParam {integer} price price of indent
 * @apiParam {string}  [image] hashid of image
 * @apiParam {string}  [repairer_note] repairer comment
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "kNrZN",
 *          "authenticated_user": "gxnDN"
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method IndentController controller() //
 */
class IndentSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Save Indent";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->is('repairer');
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'IndentController@create';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(1),
        ]);
    }
}
