<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\DefectsController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-repair-order-defects-set
 *                    Repair Order Set Defects
 * @apiDescription    saving (create and edit) of the order defects by an operator
 * @apiVersion        1.0.0
 * @apiName           Repair Order Set Defects
 * @apiGroup          MobileSalam
 * @apiPermission     Operator
 * @apiParam {string} id      order hashid
 * @apiParam {array}  defects defects array
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method DefectsController controller()
 */
class RepairOrderDefectsSetEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Order Set Defects";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'DefectsController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
    }
}
