<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-orders-list
 *                    Orders List
 * @apiDescription    List of orders
 * @apiVersion        1.0.0
 * @apiName           Orders List
 * @apiGroup          MobileSalam
 * @apiPermission     member
 * @apiParam {String}   [foo]           whatever field with electors already set can be passed.
 * @apiParam {int=0,1}  [paginated=0]   Set the result must be paginated or not
 * @apiParam {int}      [per_page=15]   Set the number of results on each request
 * @apiParam {int}      [page=1]        Set the page number on pagination
 * @apiParam {int}      [limit=100]     Set limit to be applied on the number of orders to be retrieved
 * @apiParam {string}   [sort=id]       Set the order field
 * @apiParam {string=desc,asc} [order=desc] Set the order ordination
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "total": 1275,
 *           "authenticated_user": "QKgJx"
 *      },
 *      "results": {
 *          "{
 *               "id": "qKXVA",
 *               "created_at": 1558427189,
 *               "updated_at": "",
 *               "deleted_at": "",
 *               "created_by": {
 *                       "id": "",
 *                       "full_name": ""
 *               },
 *               "updated_by": {
 *                       "id": "",
 *                       "full_name": ""
 *               },
 *               "deleted_by": {
 *                       "id": "",
 *                       "full_name": ""
 *               }
 *           },
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class OrdersListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Orders List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf('member', 'repairer', 'operator', 'super-operator');
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@ordersList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "status"   => 200,
             "metadata" => [
                  "total"              => 1275,
                  "authenticated_user" => "QKgJx",
             ],
             "results"  => [
                  [
                       "id"         => "qKXVA",
                       "created_at" => 1558427189,
                       "updated_at" => "",
                       "deleted_at" => "",
                       "created_by" => [
                            "id"        => hashid(rand(1, 100)),
                            "full_name" => dummy()::persianName(),
                       ],
                       "updated_by" => [
                            "id"        => hashid(rand(1, 100)),
                            "full_name" => dummy()::persianName(),
                       ],
                       "deleted_by" => [
                            "id"        => "",
                            "full_name" => "",
                       ],
                  ],
                  [
                       "id"         => "QKgJx",
                       "created_at" => 1558427189,
                       "updated_at" => "",
                       "deleted_at" => "",
                       "created_by" => [
                            "id"        => hashid(rand(1, 100)),
                            "full_name" => dummy()::persianName(),
                       ],
                       "updated_by" => [
                            "id"        => "",
                            "full_name" => "",
                       ],
                       "deleted_by" => [
                            "id"        => hashid(rand(1, 100)),
                            "full_name" => dummy()::persianName(),
                       ],
                  ],
             ],
        ]);

    }
}
