<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-alopeyk-webhook
 *                    Alopeyk Webhook
 * @apiDescription    Will call by alopeyk automatically on order changes. Must register in alopeyk's management panel.
 * @apiVersion        1.0.0
 * @apiName           Alopeyk Webhook
 * @apiGroup          MobileSalam
 * @apiPermission     ATTENTION: Must be ip limited through web server.
 *                               Current remote allowed one is 79.175.149.5. (it's critical pay attention)
 * @apiParam {Object} is according to https://docs.alopeyk.com/#post-webhooks & must update with remote changes.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {},
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class AlopeykWebhookEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Alopeyk Webhook";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@fetchAlopeykWebhookCall';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], []);
    }
}
