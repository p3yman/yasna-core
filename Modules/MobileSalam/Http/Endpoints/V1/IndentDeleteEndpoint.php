<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\IndentController;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/mobile-salam-indent-delete
 *                    Delete Indent
 * @apiDescription    delete a indent
 * @apiVersion        1.0.0
 * @apiName           Delete Indent
 * @apiGroup          MobileSalam
 * @apiPermission     repairer
 * @apiParam {string} hashid hashid of indent
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(1),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Content not found!",
 *      "userMessage": "Content not found!",
 *      "errorCode": "404",
 *      "moreInfo": "",
 *      "errors": []
 * }
 * @method IndentController controller() //
 */
class IndentDeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Delete Indent";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->is('repairer');
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'IndentController@delete';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "metadata"           => hashid(1),
             "authenticated_user" => hashid(1),
        ]);
    }
}
