<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\WalletController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-fetch-user-wallet-history
 *                    Fetch User Wallet History
 * @apiDescription    Fetch user's wallet transactions.
 * @apiVersion        1.0.0
 * @apiName           Fetch User Wallet History
 * @apiGroup          MobileSalam
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" : "xWpGK",
 *          "authenticated_user": "xWpGK"
 *      },
 *      "results": [
 *                    {
 *                      "id": "qKXVA",
 *                      "created_at": 1558188726,
 *                      "updated_at": 1558188726,
 *                      "deleted_at": "",
 *                      "created_by": {
 *                             "id": "qKXVA",
 *                             "full_name": "full name"
 *                      },
 *                      "updated_by": {
 *                              "id": "qKXVA",
 *                              "full_name": "full name"
 *                      },
 *                      "deleted_by": {
 *                              "id": "",
 *                              "full_name": ""
 *                      },
 *                      "user_id": "xWpGK",
 *                      "invoice_model": "User",
 *                      "invoice_id": "PKEnN",
 *                      "tracking_no": "1001",
 *                      "client_ip": "127.0.0.1",
 *                      "status": "verified",
 *                      "status_text": "accepted",
 *                      "status_applied_at": 1558188726,
 *                      "type": "online",
 *                      "type_title": "online payment",
 *                      "account_id": "QKgJx",
 *                      "account_name": "saman bank",
 *                      "bank_responses": []
 *                  }
 *       ]
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method WalletController controller()
 */
class FetchUserWalletHistoryEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Fetch User Wallet History";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'WalletController@fetchUserHistory';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "id"                => "QKgJx",
                  "created_at"        => 1558250496,
                  "updated_at"        => 1558250496,
                  "deleted_at"        => "",
                  "created_by"        => [
                       "id"        => "qKXVA",
                       "full_name" => "full name",
                  ],
                  "updated_by"        => [
                       "id"        => "qKXVA",
                       "full_name" => "full name",
                  ],
                  "deleted_by"        => [
                       "id"        => "",
                       "full_name" => "",
                  ],
                  "user_id"           => "xWpGK",
                  "invoice_model"     => "User",
                  "invoice_id"        => "PKEnN",
                  "tracking_no"       => "1002",
                  "client_ip"         => "127.0.0.1",
                  "status"            => "verified",
                  "status_text"       => "accepted",
                  "status_applied_at" => 1558250496,
                  "type"              => "online",
                  "type_title"        => "online payment",
                  "account_id"        => "QKgJx",
                  "account_name"      => "saman bank",
                  "bank_responses"    => [],
             ]
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
    }
}
