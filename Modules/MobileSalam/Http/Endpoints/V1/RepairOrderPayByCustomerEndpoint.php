<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairPaymentController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-repair-order-pay-by-customer
 *                    Repair Order Pay By Customer
 * @apiDescription    to be called by the customer to pay his own repair bill.
 * @apiVersion        1.0.0
 * @apiName           Repair Order Pay By Customer
 * @apiGroup          MobileSalam
 * @apiPermission     Customer
 * @apiParam {string}  id         hashid of the order in question
 * @apiParam {string}  account    the hashid of the account to be used
 * @apiParam {boolean} use_wallet identify if wallet is going to be used.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "order": "QKgJx",
 *          "account": "QKgJx",
 *          "amount": 24970,
 *          "authenticated_user": "oxljN"
 *      },
 *      "results": {
 *          "wallet_share": 24970,
 *          "wallet_tracking_number": "1038",
 *          "online_share": 0,
 *          "online_gateway_url": "http://yasna.sexy/globe/mobile-salam/repair-payment-dive",
 *          "online_tracking_number": null
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairPaymentController controller()
 */
class RepairOrderPayByCustomerEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Order Pay By Customer";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->is("member");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairPaymentController@payByCustomer';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "wallet_share"           => 24970,
             "wallet_tracking_number" => "1038",
             "online_share"           => 0,
             "online_gateway_url"     => "http://yasna.sexy/globe/mobile-salam/repair-payment-dive",
             "online_tracking_number" => null,
        ], [
             "order"              => "QKgJx",
             "account"            => "QKgJx",
             "amount"             => 24970,
             "authenticated_user" => "oxljN",
        ]);
    }
}
