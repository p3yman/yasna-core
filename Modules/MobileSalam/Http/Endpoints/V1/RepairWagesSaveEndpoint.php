<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairWagesController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-repair-wages-save
 *                    Repair Wages Save
 * @apiDescription    Save the repair wages, compatible to the indent list
 * @apiVersion        1.0.0
 * @apiName           Repair Wages Save
 * @apiGroup          MobileSalam
 * @apiPermission     Operator, Super Operator
 * @apiParam {string} id                  hashid of the order in question
 * @apiParam {array}  amounts             an associated array of the amounts: amount[iNdEnT_hAsHiD] = 1200
 * @apiParam {float}  wage_added_amount   custom added amount
 * @apiParam {text}   [wage_custom_desc]  custom description
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairWagesController controller()
 */
class RepairWagesSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Wages Save";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairWagesController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
    }
}
