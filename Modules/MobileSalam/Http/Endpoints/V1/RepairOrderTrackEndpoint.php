<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-repair-order-track
 *                    Repair Order Track
 * @apiDescription    Tracking an order, by means of tracking number, available to the guests too.
 * @apiVersion        1.0.0
 * @apiName           Repair Order Track
 * @apiGroup          MobileSalam
 * @apiPermission     Guest
 * @apiParam {string} tracking_number  the tracking number
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          {
 *              "id": "0",
 *              "type": "TrackNode",
 *              "title": "RepairRequest,
 *              "created_at": 1558525202,
 *              "created_by": {
 *                  "id": "Hashal",
 *                  "full_name": "Taha Kamkar"
 *              },
 *              "data": {
 *                  "tracking_number": "4585252024",
 *                  "application_type": "in-person",
 *                  "serial": null,
 *                  "unknown_device": null,
 *                  "unknown_problem": null,
 *                  "is_repaired_before": 0,
 *                  "estimated_price": 44733,
 *                  "estimated_time": 1,
 *                  "final_price": null,
 *                  "guarantee_expires_at": null,
 *                  "status": "pending",
 *                  "status_label": "approval",
 *                  "customer_note": "some stupid meaningful text."
 *              }
 *          }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-404",
 *      "userMessage": "Not Found!",
 *      "errorCode": "endpoint-404",
 *      "moreInfo": "endpoint.moreInfo.endpoint-404",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class RepairOrderTrackEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Order Track";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@trackOrder';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond(
             [
                  "id"         => "0",
                  "type"       => "TrackNode",
                  "title"      => "Repair Request",
                  "created_at" => 1558525202,
                  "created_by" => [
                       "id"        => "hashal",
                       "full_name" => "Taha Kamkar",
                  ],
                  "data"       => [
                       "tracking_number"      => "4585252024",
                       "application_type"     => "in-person",
                       "serial"               => null,
                       "unknown_device"       => null,
                       "unknown_problem"      => null,
                       "is_repaired_before"   => 0,
                       "estimated_price"      => 44733.0,
                       "estimated_time"       => 1,
                       "final_price"          => null,
                       "guarantee_expires_at" => null,
                       "status"               => "pending",
                       "status_label"         => "Pending",
                       "customer_note"        => "Some stupid text.",
                  ],
             ],
             [
                  "hashid" => hashid(rand(1, 100)),
             ]
        );
    }
}
