<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairWagesController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-repair-wages
 *                    Repair Wages
 * @apiDescription    Get the list of repair wages, from the indent list
 * @apiVersion        1.0.0
 * @apiName           Repair Wages
 * @apiGroup          MobileSalam
 * @apiPermission     Operator, Super Operator
 * @apiParam {string} id  hashid of the order in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *          "wage_calculated_at": null,
 *          "authenticated_user": "DAvQN"
 *      },
 *      "results": {
 *          "indents" => [
 *              "id": "qKXVA",
 *              "created_at": 1559409507,
 *              "updated_at": 1560614926,
 *              "deleted_at": null,
 *              "created_by": {
 *                  "id": "aALnK",
 *                  "full_name": "Jafar the Repairer"
 *              },
 *              "updated_by": {
 *                  "id": "nAyBx",
 *                  "full_name": "Jafar the Supplier"
 *              },
 *              "deleted_by": {
 *                  "id": "",
 *                  "full_name": ""
 *              },
 *              "repairer_info": {
 *                  "id": "aALnK",
 *                  "full_name": "Jafar the Repairer"
 *              },
 *              "supplier_info": {
 *                  "id": "nAyBx",
 *                  "full_name": "Jafar the Supplier"
 *              },
 *              "defect": {
 *                  "title": "frame",
 *                  "type": "repair",
 *                  "type_label": "Repair"
 *              },
 *              "defect_type": "repair",
 *              "part_title": "LCG Glass S3 4W, High Fake",
 *              "tracking_number": "6588833668",
 *              "quantity": 1,
 *              "supply_status": "supply-status-available",
 *              "supply_status_label": "supportable",
 *              "supplied_at": "2019-06-01 21:56:17",
 *              "return_reason": "",
 *              "return_reason_label": "Unknown",
 *              "price": null,
 *              "repairer_note": "want it.",
 *              "supplier_note": "",
 *              "amount_in_shop": 0,
 *              "amount_for_repairer": 0,
 *              "wage_calculated": 1779200,
 *              "wage_modified": 12000,
 *              "wage_final": 12000,
 *              "wage_modified_at": 1560614926,
 *              "wage_modified_by": {
 *                  "id": "hAsHiD",
 *                  "full_name": "Jafar the Operator"
 *              }
 *          ],
 *          "wage_added_amount": 6000,
 *          "wage_custom_desc": ""
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairWagesController controller()
 */
class RepairWagesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repair Wages";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairWagesController@get';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
