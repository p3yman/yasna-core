<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-save-repair-order-rating
 *                    Save Repair Order Rating
 * @apiDescription    Save an repair order's rating
 * @apiVersion        1.0.0
 * @apiName           Save Repair Order Rating
 * @apiGroup          MobileSalam
 * @apiPermission     "member", "operator", "super-operator"
 * @apiParam {string} id the hashid of the repair order
 * @apiParam {array}  posts the array of questions with their rate. must scaffold like:
 *                           {
 *                              0 => {
 *                                      'post_hashid' => '1-5'
 *                                   },
 *                              1 => {
 *                                      ...
 *                                  },
 *                              ...
 *                           }
 * @apiParam {string} [note] the note of the repair order rate
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "user" => hashid(0),
 *          "order" => hashid(0)
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class SaveRepairOrderRatingEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Save Order Rating";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("member", "operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@saveRepairOrderRate';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
                  "order" => hashid(rand(1, 100)),
                  "user"  => hashid(rand(1, 100)),
             ]
        );
    }
}
