<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-estimate-repair-order-pickup
 *                    Estimate Repair Order Pickup
 * @apiDescription    Calculate new repair order pickup details
 * @apiVersion        1.0.0
 * @apiName           Estimate Repair Order Pickup
 * @apiGroup          MobileSalam
 * @apiPermission     "operator", "super-operator"
 * @apiParam {string} id hashid of the repair order.
 * @apiParam {string} parent_entity related parent entity. can be: "salam-order","cart".
 * @apiParam {string} pickup_method method of repair order pickup. can be: "owner","alopeyk","custom".
 * @apiParam {string} pickup_type type of repair order pickup. can be: "send","receive","custom".
 * @apiParam {string} [origin_city] hashid of origin city. (default: tehran)
 * @apiParam {string} [origin_latitude] latitude of origin location. (necessary when type is alopeyk)
 * @apiParam {string} [origin_longitude] longitude of origin location. (necessary when type is alopeyk)
 * @apiParam {string} origin_address origin's full address.
 * @apiParam {string} [destination_city] hashid of destination city. (default: tehran)
 * @apiParam {string} [destination_latitude] latitude of destination address. (necessary when type is alopeyk)
 * @apiParam {string} [destination_longitude] longitude of destination location. (necessary when type is alopeyk)
 * @apiParam {string} destination_address destination's full address.
 * @apiParam {string} [driver_full_name] driver's full name. (necessary when type is custom)
 * @apiParam {string} [driver_phone_number] driver's phone number. (necessary when type is custom)
 * @apiParam {string} [note] the note of the repair order pickup.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "user" => hashid(0),
 *          "order" => hashid(0)
 *          "price" => 15000
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class EstimateRepairOrderPickupEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Estimate Repair Order Pickup";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@estimateRepairOrderPickup';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
                  "order" => hashid(rand(1, 100)),
                  "user"  => hashid(rand(1, 100)),
                  "price" => 15000,
             ]
        );
    }
}
