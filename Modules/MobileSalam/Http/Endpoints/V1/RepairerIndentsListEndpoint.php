<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\IndentController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-repairer-indents-list
 *                    Repairer Indents List
 * @apiDescription    The list of the status of parts that the repairer ordered for a device.
 * @apiVersion        1.0.0
 * @apiName           Repairer Indents List
 * @apiGroup          MobileSalam
 * @apiPermission     Repairer
 * @apiParam {string}   id  hashid of order
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 3,
 *          "authenticated_user": "ZAQrx",
 *      },
 *      "results": {
 *          {
 *               "id": "PNOLA",
 *               "created_at": 1559041141,
 *               "updated_at": "",
 *               "deleted_at": "",
 *               "created_by": {
 *                       "id": "",
 *                       "full_name": ""
 *                },
 *              "updated_by": {
 *                      "id": "",
 *                      "full_name": ""
 *               },
 *              "deleted_by": {
 *                     "id": "",
 *                     "full_name": ""
 *               },
 *              "repairer_info": {
 *                    "id": "kNrZN",
 *                    "full_name": " Repairer Full name"
 *               },
 *              "supplier_info": {
 *                    "id": "ZAQrx",
 *                    "full_name": "Supplier Full Name"
 *              },
 *              "defect": {
 *                    "title": null,
 *                    "type": null,
 *                    "type_label": "mobile-salam::combo.defect-type."
 *               },
 *              "part_title": "",
 *              "tracking_number": "7590411278",
 *               "device_model": "iPad Mini"
 *              "quantity": 7,
 *              "supply_status": "supply-status-follow-up",
 *              "supply_status_label": "",
 *              "supplied_at": "2019-05-27 00:00:00",
 *              "return_reason": "return-reason-repairer-fault",
 *              "return_reason_label": "",
 *              "price": "42932",
 *              "repairer_note": "",
 *              "supplier_note": ""
 *          },
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method IndentController controller()
 */
class RepairerIndentsListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Repairer Indents List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isAnyOf("repairer", "operator", "super-operator");
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'IndentController@repairerIndentsList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "total"              => 1,
             "authenticated_user" => hashid(user()->id),
        ], [
             "results" => [
                  [
                       "id"                  => "PNOLA",
                       "created_at"          => 1559041141,
                       "updated_at"          => "",
                       "deleted_at"          => "",
                       "created_by"          => [
                            "id"        => "",
                            "full_name" => "",
                       ],
                       "updated_by"          => [
                            "id"        => "",
                            "full_name" => "",
                       ],
                       "deleted_by"          => [
                            "id"        => "",
                            "full_name" => "",
                       ],
                       "repairer_info"       => [
                            "id"        => "kNrZN",
                            "full_name" => dummy()::persianName(),
                       ],
                       "supplier_info"       => [
                            "id"        => "ZAQrx",
                            "full_name" => dummy()::persianName(),
                       ],
                       "defect"              => [
                            "title"      => null,
                            "type"       => null,
                            "type_label" => trans("mobile-salam::combo.defect-type.replace"),
                       ],
                       "part_title"          => dummy()::persianTitle(),
                       "tracking_number"     => "7590411278",
                       "device_model"        => "iPad Mini",
                       "quantity"            => 7,
                       "supply_status"       => "supply-status-follow-up",
                       "supply_status_label" => trans("mobile-salam::combo.supply-status.follow-up"),
                       "supplied_at"         => "2019-05-27 00=>00=>00",
                       "return_reason"       => "return-reason-repairer-fault",
                       "return_reason_label" => trans("mobile-salam::combo.return-reason.repairer-fault"),
                       "price"               => "42932",
                       "repairer_note"       => dummy()::persianText(),
                       "supplier_note"       => dummy()::persianText(),
                  ],
             ],
        ]);

    }
}
