<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\CreditController;

/**
 * @api               {PUT}
 *                    /api/modular/v1/mobile-salam-credit-charge
 *                    Credit Charge
 * @apiDescription    Request to charge the credit to make an unpaid transaction, ready for the gateway
 * @apiVersion        1.0.0
 * @apiName           Credit Charge
 * @apiGroup          MobileSalam
 * @apiPermission     User
 * @apiParam {int}    amount    the amount to be charged
 * @apiParam {string} [account] the hashid of the account to be used (default in case of absence)
 * @apiSuccessExample Success-Response: TODO: This is the default save feedback. Change if your work is different
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample   TODO: Follow the below example and change it as appropriate!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CreditController controller()
 */
class CreditChargeEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Credit Charge";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_PUT;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CreditController@charge';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
