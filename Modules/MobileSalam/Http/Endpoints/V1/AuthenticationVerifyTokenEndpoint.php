<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-authentication-verify-token
 *                    Authentication Verify Token
 * @apiDescription    verify token
 * @apiVersion        1.0.0
 * @apiName           Authentication Verify Token
 * @apiGroup          MobileSalam
 * @apiPermission     none
 * @apiParam {string} token Token was sent in SMS
 * @apiParam {string} [hashid] hashid the existing user
 * @apiParam {string} mobile Mobile available on request
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *          "done": "1",
 *           "payload": {
 *               "payload": {
 *                  "id": "qKXVA",
 *                   "full_name": "حضرت برنامه\u200cنویس",
 *                   "roles": [
 *                       "manager",
 *                       "operator",
 *                       "admin"
 *                   ],
 *                   "credit": 0,
 *                   "password_force_change": false
 *                   }
 *               }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method UserController controller() //
 */
class AuthenticationVerifyTokenEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Authentication Verify Token";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@verifyToken';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done"    => "1",
             "payload" => user(1)->getJWTCustomClaims(),
        ], [
             "id" => hashid(rand(1, 100)),
        ]);
    }
}
