<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\IndentController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-indent-supply
 *                    Indent Supply
 * @apiDescription    supply the indent
 * @apiVersion        1.0.0
 * @apiName           Indent Supply
 * @apiGroup          MobileSalam
 * @apiPermission     supplier
 * @apiParam {string} hashid the hashid Indent
 * @apiParam {string} supply_status the slug of the supply-status combo
 * @apiParam {string} [supplied_at] the date is optional if the supply_status is not follow-up
 * @apiParam {string} [supplier_note] if supply_status = 'supply-status-not-available' is required
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "kNrZN",
 *          "authenticated_user": "gxnDN"
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method IndentController controller() //
 */
class IndentSupplyEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Indent Supply";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->is('supplier');
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'IndentController@supplyIndent';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(1),
        ]);
    }
}
