<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/mobile-salam-user-register
 *                    User Register
 * @apiDescription    Register user via API
 * @apiVersion        1.0.0
 * @apiName           User Register
 * @apiGroup          MobileSalam
 * @apiPermission     Guest
 * @apiParam {string}   name_first The first name of user
 * @apiParam {string}   name_last The last name of user
 * @apiParam {int}      mobile The user mobile
 * @apiParam {string}   password The user account password
 * @apiParam {string}   password2 The user account password
 * @apiParam {String}   g-recaptcha-response The value of the google recaptcha
 * @apiParam {string}   [email] The user email
 * @apiParam {string}   home_address The user address
 * @apiParam {int}      postal_code The user postal code
 * @apiParam {int}      [tel] The user phone number
 * @apiParam {int}      neighborhood The user neighborhood id
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" : "QKgJx",
 *          "authenticated_user": "PKEnN",
 *          "access_token": "TOKEN!",
 *          "token_type": "bearer",
 *          "expires_in" 3600000
 *      },
 *      "results": {
 *          "done" : "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Error validating data",
 *      "userMessage": "Received data has errors"
 *      "errorCode": 422,
 *      "moreInfo": "mobilesalam.moreInfo.422",
 *      "errors": [
 *                  "mobile": [
 *                      [
 *                      "The Mobile field is required."
 *                    ]
 *               ],
 *                 "password": [
 *                      [
 *                      "The Password field is required."
 *                     ]
 *              ],
 *      ]
 * }
 * @method UserController controller()
 */
class UserRegisterEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Register";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@register';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid"       => "hA5hId",
             "access_token" => "sample-token",
             "token_type"   => "bearer",
             "expires_in"   => 3600000,
        ]);
    }
}
