<?php

namespace Modules\MobileSalam\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\MobileSalam\Http\Controllers\V1\RepairController;

/**
 * @api               {GET}
 *                    /api/modular/v1/mobile-salam-order-timeline
 *                    Order Timeline
 * @apiDescription    Repair order, single view (=timeline)
 * @apiVersion        1.0.0
 * @apiName           Order Timeline
 * @apiGroup          MobileSalam
 * @apiPermission     Customer, Operator or Repairer
 * @apiParam {string} id         the hashid of the order in question
 * @apiParam {array}  including  the list of resources to be included in the timeline.
 * @apiParam {array}  excluding  the list of resources to be excluded from the timeline.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "model" => {
 *              "id": "oxljN",
 *              "payment_status": "paid"
 *          },
 *      },
 *      "results": {
 *          "AEjlx": {
 *              "id": "AEjlx",
 *              "type": "SubmittedNewIndentNode",
 *              "title": "Indent",
 *              "created_at": 1560293218,
 *              "data" : {
 *              }
 *          }
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-404",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-404",
 *      "moreInfo": "endpoint.moreInfo.endpoint-404",
 *      "errors": []
 * }
 * @method RepairController controller()
 */
class OrderTimelineEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Order Timeline";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true; // <~~ checked in the validation layer.
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\MobileSalam\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RepairController@getTimeline';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
