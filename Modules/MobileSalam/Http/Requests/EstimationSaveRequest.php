<?php

namespace Modules\MobileSalam\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class EstimationSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "SalamEstimation";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "post_id",
             "title",
             "color",
             "price",
             "duration",
             "is_precise",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "title"    => "required",
             "price"    => "required|numeric|min:0",
             "duration" => "required|numeric|min:0|max:100",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "post_id"    => "hashid",
             "price"      => "ed|numeric",
             "duration"   => "ed|numeric",
             "is_precise" => "boolean",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "title"    => trans("mobile-salam::devices.estimation_title"),
             "color"    => trans("mobile-salam::devices.estimation_color"),
             "price"    => trans("mobile-salam::devices.estimation_price"),
             "duration" => trans("mobile-salam::devices.estimation_duration"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $label = label($this->getData("color"));
        if ($label->exists) {
            $this->setData("color", $label->id);
            $this->setData("color_title", $label->title);
        }
    }
}
