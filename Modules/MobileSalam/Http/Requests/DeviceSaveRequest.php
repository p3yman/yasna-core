<?php

namespace Modules\MobileSalam\Http\Requests;

use App\Models\Post;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property Post $model
 */
class DeviceSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "Post";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;

    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "type"  => "required",
             "brand" => "required",
             "title" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if (!$this->getData("type")) {
            $this->unsetData("type");
        }

        if (!$this->getData("brand")) {
            $this->unsetData("brand");
        }

        if (!$this->getData("series")) {
            $this->unsetData("series");
        }

        $this->mutators();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::devices.attributes");
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $colors = explode_not_empty(",", $this->getData("colors"));
        $type   = [$this->getData("type")];
        $brand  = [$this->getData("brand")];
        $series = [$this->getData("series")];

        $this->setData("labels", array_filter(array_merge($colors, $type, $brand, $series)));
    }
}
