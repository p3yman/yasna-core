<?php

namespace Modules\MobileSalam\Http\Requests;

use App\Models\User;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class WalletAdminChangeRequest
 *
 * @property User $model
 */
class WalletAdminChangeRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "User";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "amount"  => "required|numeric|min:1000",
             "type"    => "required|in:increase,decrease",
             "account" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "amount",
             "type",
             "account",
             'title',
             'description',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "amount" => "ed|numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveAccount();
    }



    /**
     * resolve Account
     *
     * @return void
     */
    private function resolveAccount()
    {
        $account = model("account")->grabHashid($this->getData("account"));

        $this->setData("account", $account->id);
    }
}
