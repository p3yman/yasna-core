<?php

namespace Modules\MobileSalam\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class ChangeForcePasswordRequest extends YasnaFormRequest
{

    /**
     * @inheritDoc
     */
    public $model_name = 'User';

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * A set of rules for validating the given set of passwords
     *
     * @return array
     */
    public function passwordRules()
    {
        $rules = [
             "password"  => "required|string|min:8",
             "password2" => "required|string|same:password",
        ];

        return $rules;
    }



    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'password',
             'password2',
        ];
    }



    /**
     * @inheritDoc
     */
    public function attributes()
    {
        return [
             'password'  => trans("mobile-salam::general.password"),
             'password2' => trans("mobile-salam::general.password2"),
        ];
    }

}
