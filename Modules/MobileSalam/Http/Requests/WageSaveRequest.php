<?php

namespace Modules\MobileSalam\Http\Requests;

use App\Models\Post;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property Post $model
 */
class WageSaveRequest extends YasnaFormRequest
{
    protected $responder                 = 'laravel';
    protected $model_name                = "Post";
    protected $should_allow_create_mode  = false;
    protected $automatic_injection_guard = false; // <~~ No Mass Assignment!



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canEdit();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "tag_wage_repair"   => "numeric|min:0" . (($this->getData("tag_wage_repair_type") == "percent") ? "|max:100" : ""),
             "tag_wage_replace"  => "numeric|min:0" . (($this->getData("tag_wage_replace_type") == "percent") ? "|max:100" : ""),
             "tag_penalty"       => "numeric|min:0" . (($this->getData("tag_penalty_type") == "percent") ? "|max:100" : ""),
             "post_wage_repair"  => "numeric|min:0" . (($this->getData("post_wage_repair_type") == "percent") ? "|max:100" : ""),
             "post_wage_replace" => "numeric|min:0" . (($this->getData("post_wage_replace_type") == "percent") ? "|max:100" : ""),
             "post_penalty"      => "numeric|min:0" . (($this->getData("post_penalty_type") == "percent") ? "|max:100" : ""),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "tag_wage_repair"   => "ed|numeric",
             "tag_wage_replace"  => "ed|numeric",
             "tag_penalty"       => "ed|numeric",
             "post_wage_repair"  => "ed|numeric",
             "post_wage_replace" => "ed|numeric",
             "post_penalty"      => "ed|numeric",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::wages");
    }
}
