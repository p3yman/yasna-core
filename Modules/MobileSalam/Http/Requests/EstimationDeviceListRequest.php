<?php

namespace Modules\MobileSalam\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaListRequest;


class EstimationDeviceListRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->can("estimate");
    }
}
