<?php

namespace Modules\MobileSalam\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use App\Models\Division;
use App\Models\Cart;

class SaveCartPickupRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @var Cart
     */
    public $model;


    /** @var Division */
    public $origin_city;


    /** @var Division */
    public $destination_city;

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "parent_entity" => "required|string",
             "pickup_method" => "required|string",
             "pickup_type"   => "required|string",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "just_estimate",
             "parent_entity",
             "pickup_method",
             "pickup_type",
             "pickup_price",
             "pickup_time",
             "origin_city",
             "is_cashed",
             "origin_latitude",
             "origin_longitude",
             "origin_address",
             "destination_city",
             "destination_latitude",
             "destination_longitude",
             "destination_address",
             "driver_full_name",
             "driver_phone_number",
             "note",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveOrigin();
        $this->resolveDestination();
        $this->checkType();
        $this->resolveCartCode();
    }



    /**
     * resolve cart's code
     *
     * @return void
     */
    private function resolveCartCode()
    {
        $code  = $this->getData('id');
        $model = model('cart')->findByCode($code);
        if ($model) {
            $this->setData("id", $model->id);
            $this->model = $model;
        }
    }



    /**
     * resolve origin city
     *
     * @return void
     */
    private function resolveOrigin()
    {
        $param = $this->getData('origin_city');
        if ($param) {
            $origin_city       = model('division')->find(hashid($param));
            $this->origin_city = $origin_city->title_en;
            $this->setData("origin_city_id", $origin_city->id);
            $this->setData("origin_city_title_en", $origin_city->title_en);
        } else {
            $origin_city       = model('division')->find(setting("mobile_salam_location_city_id")->custom_value);
            $this->origin_city = $origin_city->title_en;
            $this->setData("origin_city_id", setting("mobile_salam_location_city_id")->custom_value);
            $this->setData("origin_city_title_en", $this->origin_city);
        }
        $this->unsetData("origin_city");
    }



    /**
     * resolve destination city
     *
     * @return void
     */
    private function resolveDestination()
    {
        $param = $this->getData('destination_city');
        if ($param) {
            $destination_city       = model('division')->find(hashid($param));
            $this->destination_city = $destination_city->title_en;
            $this->setData("destination_city_id", $destination_city->id);
            $this->setData("destination_city_title_en", $destination_city->title_en);
        } else {
            $destination_city       = model('division')->find(setting("mobile_salam_location_city_id")->custom_value);
            $this->destination_city = $destination_city->title_en;
            $this->setData("destination_city_id", setting("mobile_salam_location_city_id")->custom_value);
            $this->setData("destination_city_title_en", $this->destination_city);
        }
        $this->unsetData("destination_city");
    }



    /**
     * apply pickup type related conditions
     *
     * @return void
     */
    private function checkType()
    {
        $pickup_type = $this->getData('pickup_type');
        if ($pickup_type == "send") {
            $this->setData("origin_latitude", setting("mobile_salam_location_lat")->custom_value);
            $this->setData("origin_longitude", setting("mobile_salam_location_lng")->custom_value);
            $this->setData("origin_address", setting("mobile_salam_location_address")->custom_value);
        }
        if ($pickup_type == "receive") {
            $this->setData("destination_latitude", setting("mobile_salam_location_lat")->custom_value);
            $this->setData("destination_longitude", setting("mobile_salam_location_lng")->custom_value);
            $this->setData("destination_address", setting("mobile_salam_location_address")->custom_value);
        }
    }
}
