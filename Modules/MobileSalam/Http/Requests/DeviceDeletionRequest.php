<?php

namespace Modules\MobileSalam\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class DeviceDeletionRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "Post";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     */
    protected $should_load_trashed_models = true;
}
