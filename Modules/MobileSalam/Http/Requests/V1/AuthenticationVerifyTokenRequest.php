<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthenticationVerifyTokenRequest extends YasnaFormRequest
{
    protected $model_name                = "User";
    protected $should_allow_create_mode  = true;
    protected $automatic_injection_guard = false;



    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             "token",
             "mobile",
        ];
    }



    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
             'mobile' => 'required|phone:mobile',
             'token'  => 'required|' . $this->tokenValidation(),
        ];
    }



    /**
     * token validation.
     *
     * @return string
     */
    public function tokenValidation()
    {
        $token = $this->getData('token');
        if ($this->userTokenIsValid($token)) {
            return '';
        }

        return 'invalid';

    }



    /**
     * Whether the specified token is acceptable for the found user.
     *
     * @param string $token
     *
     * @return bool
     */
    private function userTokenIsValid($token)
    {
        $this->correctionModel();
        return (
            // check if token matchs
             Hash::check($token, $this->model->reset_token)
             and
             // check if token has not been expired
             Carbon::parse($this->model->reset_token_expires_at)->greaterThan(now())
        );
    }



    /**
     * correction model.
     *
     * @return string
     */
    private function correctionModel()
    {
        if ($this->model->exists) {
            $this->setData('payload', $this->model->getJWTCustomClaims());
            $this->setData('_token', $this->setJWTToken());
            return '';
        }

        $mobile = $this->getData('mobile');
        $model  = model('salam-authentication')
             ->where('mobile', $mobile)
             ->first()
        ;

        if ($model and $model->exists) {
            $this->model = $model;
        }

        return 'invalid';
    }



    /**
     * set JWT token
     *
     * @return mixed
     */
    private function setJWTToken()
    {
        return JWTAuth::fromUser($this->model);
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "mobile" => "ed",
        ];
    }
}
