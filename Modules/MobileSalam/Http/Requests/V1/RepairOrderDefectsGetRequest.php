<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderDefectsGetRequest extends YasnaListRequest
{
    protected $model_name                    = "salam-order";
    protected $should_allow_create_mode      = false;
    protected $should_load_model             = true;
    protected $should_load_model_with_hashid = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (user()->is("repairer")) {
            return boolval($this->model->repairer_id == user()->id);
        }

        return true; // <~~ Because only operators and repairers are allowed to pass the endpoint layer.
    }
}
