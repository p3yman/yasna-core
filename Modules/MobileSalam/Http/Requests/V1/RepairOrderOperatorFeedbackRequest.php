<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderOperatorFeedbackRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "SalamOrder";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "new_price_estimation",
             "new_time_estimation",
             "status",
             "comment",
             "guarantee_expires_at",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $status_list = implode(",", SalamOrder::availableStatuses());

        return [
             "new_price_estimation" => "numeric|between:1000,10000000",
             "new_time_estimation"  => "numeric|between:1,100",
             "status"               => "in:$status_list",
             "comment"              => $this->isCommentRequired() ? "required" : "",
             "guarantee_expires_at" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "new_price_estimation" => "ed",
             "new_time_estimation"  => "ed",
             "guarantee_expires_at" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * specify if the endpoint is used to merely insert a comment on the timeline.
     *
     * @return bool
     */
    public function isCommentOnly()
    {
        return boolval(
             $this->getData("comment") and
             !$this->getData("new_time_estimation") and
             !$this->getData("new_price_estimation") and
             !$this->getData("status")
        );
    }



    /**
     * identify if it is required to enter the comment.
     *
     * @return bool
     */
    private function isCommentRequired()
    {
        if ($this->getData("guarantee_expires_at")) {
            return false;
        }

        if (in_array($this->getData("status"), ["repaired", "rejected"])) {
            return true;
        }

        return false;
    }
}
