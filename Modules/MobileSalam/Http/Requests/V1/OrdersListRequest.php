<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaListRequest;


class OrdersListRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "salam-order";



    /**
     * @inheritDoc
     */
    public function purifier()
    {
        return [
             'date_from'  => 'date',
             'date_until' => 'date',
        ];
    }
}
