<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderPayByCustomerRequest extends YasnaFormRequest
{
    protected $model_name               = "SalamOrder";
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->customer_id == user()->id;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "use_wallet" => "required|boolean",
             "account"    => $this->getData("online_share") ? "required|" : "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "use_wallet",
             "account",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "account.required" => trans("mobile-salam::messages.account_not_accepted"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->calculateWalletShares();
        $this->resolveAccount();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * resolve Account
     *
     * @return void
     */
    private function resolveAccount()
    {
        if (!$this->getData("online_share")) {
            return;
        }

        $account = model("account")->where("type", "online")->grabHashid($this->getData("account"));

        if (!$account->exists) {
            $this->unsetData("account");
            return;
        }

        $this->setData("account", $account->id);
    }



    /**
     * calculate wallet shares.
     *
     * @return void
     */
    private function calculateWalletShares()
    {
        $wallet_share = $this->getData("use_wallet") ? min(user()->credit, $this->model->amount_outstanding) : 0;
        $online_share = $this->model->amount_outstanding - $wallet_share;

        $this->setData("wallet_share", $wallet_share);
        $this->setData("online_share", $online_share);
    }
}
