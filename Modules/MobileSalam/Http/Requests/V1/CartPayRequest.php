<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\Cart;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property Cart $model
 */
class CartPayRequest extends YasnaFormRequest
{
    protected $model_name               = "Cart";
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->user_id == user()->id;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "id"         => ($this->model->isRaised()) ? "invalid" : "",
             "use_wallet" => "required|boolean",
             "account"    => "required|",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "use_wallet",
             "account",
             "coupon",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "account.required" => trans("mobile-salam::messages.account_not_accepted"),
             "id.invalid"       => trans("mobile-salam::messages.cart_not_payable"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveAccount();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * resolve Account
     *
     * @return void
     */
    private function resolveAccount()
    {
        $account = model("account")->where("type", "online")->grabHashid($this->getData("account"));

        if (!$account->exists) {
            $this->unsetData("account");
            return;
        }

        $this->setData("account", $account->id);
    }

}
