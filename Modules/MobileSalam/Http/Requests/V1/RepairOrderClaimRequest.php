<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderClaimRequest extends YasnaFormRequest
{
    protected $model_name               = "SalamOrder";
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (user()->is("member")) {
            return $this->model->customer_id == user()->id;
        }

        return true; // <~~ Other roles, other than operators, are filtered in the endpoint layer.
    }



    /**
     * get main validation rules
     *
     * @return array
     */
    public function mainRules()
    {
        return [
             "problems"         => "required|array",
             "customer_note"    => "required|string",
             "application_type" => "required|in:application-type-in-person,application-type-by-operator,application-type-by-user",
        ];
    }



    /**
     * get model-related validation rules
     *
     * @return array
     */
    public function modelRules()
    {
        if (!$this->model->isClaimable()) {
            return [
                 "id" => "invalid",
            ];
        }

        return [];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "problems",
             'application_type',
             "customer_note",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "id.invalid" => trans("mobile-salam::messages.claim_on_invalid_order"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctApplicationType();
        $this->setPredefinedStatus();
    }



    /**
     * set predefined status, depending on the application type
     *
     * @return void
     */
    private function setPredefinedStatus()
    {
        $this->setData("confirmed_at", null);
        $this->setData("confirmed_by", 0);
        $this->setData("received_at", null);
        $this->setData("received_by", 0);
        $this->setData("claimed_at", now()->toDateTimeString());
        $this->setData("claimed_by", user()->id);

        if (user()->is("member")) {
            return;
        }

        $this->setData("confirmed_at", now()->toDateTimeString());
        $this->setData("confirmed_by", user()->id);

        if ($this->getData("application_type") == "application-type-in-person") {
            $this->setData("received_at", now()->toDateTimeString());
            $this->setData("received_by", user()->id);
        }
    }



    /**
     * correct `application_type` based on the submitter role
     *
     * @return void
     */
    private function correctApplicationType()
    {
        if (user()->is("member")) {
            $this->setData("application_type", "application-type-by-user");
            return;
        }

        if ($this->getData("application_type") == "application-type-by-user") {
            $this->unsetData("application_type");
            return;
        }
    }
}
