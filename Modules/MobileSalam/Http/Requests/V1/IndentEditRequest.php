<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamIndent;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class IndentEditRequest extends YasnaFormRequest
{
    /**
     * @var SalamIndent
     */
    public $model;

    /**
     * @inheritdoc
     */
    protected $model_name = "salam-indent";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->editAllowed()
             and
             $this->repairerAssigned()
             and
             $this->user()->is('repairer')
        );
    }



    /** ensure the repair of the assigned work.
     *
     * @return bool
     */
    public function repairerAssigned()
    {
        $repairer_assigned = model('salam-order', $this->model->order_id)->repairer_id;
        return ($repairer_assigned == $this->user()->id);

    }



    /**
     * Check that the order has not been finalized
     *
     * @return bool
     */
    public function editAllowed()
    {
        if ($this->model->supply_status === "") {
            return true;
        }
        return false;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'ware_id'   => 'required',
             'defect_id' => 'required',
             'quantity'  => 'required|integer',
             'image'     => 'array',
        ];

    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'ware_id',
             'defect_id',
             'quantity',
             'repairer_note',
             'image',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->repairerCorrection();
        $this->correctIndent();
    }



    /**
     * correction [repairer_id]
     */
    public function repairerCorrection()
    {
        if ($this->user()->is('repairer')) {
            $this->setData('repairer_id', $this->user()->id);
        }
    }



    /**
     * ensure the given attribute is received as a hashid and converted to a valid id
     *
     * @return void
     */
    private function correctIndent()
    {
        $this->ensureOrderHashid('ware_id');
        $this->ensureOrderHashid('defect_id');

    }



    /**
     * ensure the given attribute is received as a hashid and converted to a valid id
     *
     * @param string|integer $attribute
     */
    public function ensureOrderHashid($attribute)
    {
        if (is_numeric($this->getData($attribute))) {
            $this->setData($attribute, 0);
            return;
        }
        $this->setData($attribute, hashid_number($this->getData($attribute)));
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }

}
