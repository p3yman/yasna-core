<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class RegisterUserRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "user";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name_first'           => 'required|persian:60',
             'name_last'            => 'required|persian:60',
             //'gender'               => 'integer:1',
             //'code_melli'           => [
             //     'code_melli',
             //     Rule::unique('users'),
             //],
             'mobile'               => [
                  'required',
                  'phone:mobile',
                  Rule::unique('users'),
             ],
             'email'                => [
                  'email',
                  Rule::unique('users'),
             ],
             'postal_code'          => 'required|postal_code',
             'tel'                  => 'phone:fixed',
             'neighborhood'         => $this->checkValidCity(),
             'password'             => 'required|same:password2|min:8|max:50',
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
        ];
    }



    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [
             "neighborhood.accepted" => trans("mobile-salam::validation.neighborhood_not_valid"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name_first'  => 'pd',
             'name_last'   => 'pd',
             'code_melli'  => 'ed',
             'postal_code' => 'ed',
             'mobile'      => 'ed',
             'tel'         => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'name_first',
             'name_last',
             'gender',
             'code_melli',
             'mobile',
             'email',
             'home_address',
             'postal_code',
             'tel',
             'city',
             'password',
             'password2',
             'g-recaptcha-response',
             "neighborhood",
        ];
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {
        $this->setIdForCity();
        $this->setHashPassword();
        $this->unsetPassword2();
        $this->unsetGRecaptcha();
        $this->setProvince();
    }



    /**
     * hash password
     */
    protected function setHashPassword()
    {
        $password = $this->getData('password');

        $password_hash = Hash::make($password);

        $this->setData('password', $password_hash);
    }



    /**
     * unset password2
     */
    protected function unsetPassword2()
    {
        $this->unsetData('password2');
    }



    /**
     * unset g-recaptcha-response
     */
    protected function unsetGRecaptcha()
    {
        $this->unsetData('g-recaptcha-response');
    }



    /**
     * check the city entered is valid or not
     *
     * @return void
     */
    protected function setIdForCity()
    {
        $city_hashid = $this->getData('neighborhood');

        $this->setData('neighborhood', hashid_number($city_hashid));

    }



    /**
     * determine the province of city
     */
    protected function setProvince()
    {
        $neighborhood = neighborhood($this->getData('neighborhood'));

        $this->setData('province', $neighborhood->province_id);
        $this->setData('city', $neighborhood->city_id);
        $this->renameData("neighborhood", "neighbourhood");
    }



    /**
     * check the city is valid or not
     *
     * @return string
     */
    private function checkValidCity()
    {
        if (is_numeric($this->getData('neighborhood'))) {
            return 'accepted';
        }

        $model = neighborhood($this->getData('neighborhood'));
        if (!$model->exists) {
            return 'accepted';
        }

        return '';
    }

}
