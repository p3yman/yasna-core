<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * @property SalamOrder $model
 */
class RepairerIndentsListRequest extends YasnaListRequest
{
    protected $should_load_model             = true;
    protected $should_load_model_with_hashid = true;
    protected $should_allow_create_mode      = false;
    protected $model_name                    = "salam-order";



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        if (user()->is("repairer")) {
            return $this->model->repairer_id == user()->id;
        }

        return true; // <~~ Other roles are filtered in the endpoint layer.
    }
}
