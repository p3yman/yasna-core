<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class OrderTrackRequest extends YasnaFormRequest
{
    protected $automatic_injection_guard     = false; // <~~ Nothing is saved in this endpoint.
    protected $should_allow_create_mode      = false;
    protected $should_load_model_with_hashid = false;
    protected $should_load_model_with_slug   = true;
    protected $model_slug_attribute          = "tracking_number";
    protected $model_slug_column             = "tracking_number";

    protected $model_name = "salam-order";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (!user()->exists) {
            return true; // <~~ Guests are welcomed to see limited amount of data.
        }

        if (user()->isAnyOf("operator", "super-operator")) {
            return true;
        }

        if (user()->is("member")) {
            return boolval($this->model->customer_id == user()->id);
        }

        if (user()->is("repairer")) {
            return boolval($this->model->repairer_id == user()->id);
        }

        return false;
    }

}
