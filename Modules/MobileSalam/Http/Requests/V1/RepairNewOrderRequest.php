<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\Post;
use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class RepairNewOrderRequest extends YasnaFormRequest
{

    /**
     * @var Post
     */
    protected $device;

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * get main validation rules
     *
     * @return array
     */
    public function mainRules()
    {
        return [
             "customer_id"        => "required|exists:users,id",
             "application_type"   => "required|in:application-type-in-person,application-type-by-operator,application-type-by-user",
             "problems"           => "required|array",
             "is_repaired_before" => "required|boolean",
             "estimated_price"    => "numeric",
             "estimated_time"     => "numeric",
             "unknown_device"     => "required_if:device_id,0",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "customer_id",
             "application_type",
             "device_id",
             "color_id",
             "unknown_device",
             "unknown_problem",
             "problems",
             "is_repaired_before",
             "customer_note",
             "estimated_price",
             "estimated_time",
             "serial",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "customer_id" => "hashid",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctApplicationType();
        $this->unsetUnknownDeviceIfModelIsGiven();
        $this->setTrackingNumber();
        $this->setCustomerId();
        $this->setPredefinedStatus();
        $this->resolveDevice();
        $this->resolveColor();
        $this->resolveRemainingTime();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->renameData("device_id", "post_id");
        $this->completeCustomerNames();
    }



    /**
     * correct `application_type` based on the submitter role
     *
     * @return void
     */
    private function correctApplicationType()
    {
        if (user()->is("member")) {
            $this->setData("application_type", "application-type-by-user");
            return;
        }

        if ($this->getData("application_type") == "application-type-by-user") {
            $this->unsetData("application_type");
            return;
        }


    }



    /**
     * unset `unknown_device` if exact model is given
     *
     * @return void
     */
    private function unsetUnknownDeviceIfModelIsGiven()
    {
        if ($this->getData("device_id")) {
            $this->unsetData("unknown_device");
        }
    }



    /**
     * set a random tracking number
     *
     * @return void
     */
    private function setTrackingNumber()
    {
        $this->setData("tracking_number", SalamOrder::getRandomTracking());
    }



    /**
     * set customer_id
     *
     * @return void
     */
    private function setCustomerId()
    {
        if (user()->is("member")) {
            $this->setData("customer_id", user()->id);
        }
    }



    /**
     * set predefined status, depending on the application type
     *
     * @return void
     */
    private function setPredefinedStatus()
    {
        if (user()->is("member")) {
            return;
        }

        $this->setData("confirmed_at", now()->toDateTimeString());
        $this->setData("confirmed_by", user()->id);

        if ($this->getData("application_type") == "application-type-in-person") {
            $this->setData("received_at", now()->toDateTimeString());
            $this->setData("received_by", user()->id);
        }
    }



    /**
     * resolve Device
     *
     * @return void
     */
    private function resolveDevice()
    {
        $this->device = post();
        $given        = $this->getData("device_id");

        if (!$given) {
            $this->setData("device_id", 0);
            return;
        }

        /** @var Post $device */
        $device = post()->where("type", "devices")->grabHashid($given);

        if (!$device->exists) {
            $this->injectValidationRule("device_id", "invalid");
            return;
        }

        $this->device = $device;
        $this->setData("device_id", $device->id);
    }



    /**
     * resolve Color
     *
     * @return void
     */
    private function resolveColor()
    {
        $given = $this->getData("color_id");

        if (!$given or !$this->device->exists) {
            return;
        }

        /** @var Post $device */
        $color     = label()->grabHashid($given);
        $available = explode_not_empty(",", $this->device->getMeta("labeled_colors"));

        if (!$color->exists or !in_array($color->slug, $available)) {
            $this->injectValidationRule("color_id", "invalid");
            return;
        }

        $this->setData("color_id", $color->id);
    }



    /**
     * set `name_first` and `name_last`
     *
     * @return void
     */
    private function completeCustomerNames()
    {
        if ($this->isset('customer_id')) {
            $user = user($this->customer_id);
            $this->setData('name_first', $user->name_first);
            $this->setData('name_last', $user->name_last);
        }
    }



    /**
     * Calculate the date based on the estimated time of each breakdown
     *
     * @return void
     */
    private function resolveRemainingTime()
    {
        $estimated_time = $this->getData('estimated_time');
        $due_at         = $estimated_time ? now()->addHour($estimated_time) : null;

        $this->setData('due_at', $due_at);
    }

}
