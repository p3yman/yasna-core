<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairWagesSaveRequest extends YasnaFormRequest
{
    protected $model_name                = "SalamOrder";
    protected $should_allow_create_mode  = false;
    protected $automatic_injection_guard = false; // <~~ No Mass Assignment



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "amounts"           => "required|array",
             "amounts.*"         => "numeric|min:0",
             "wage_added_amount" => "numeric|min:0",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "wage_added_amount" => "ed",
        ];
    }
}
