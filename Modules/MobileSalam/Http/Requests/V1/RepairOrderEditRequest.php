<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\Post;
use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderEditRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "SalamOrder";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;


    /** @var Post */
    protected $device;



    /**
     * get main validation rules
     *
     * @return array
     */
    public function mainRules()
    {
        return [
             "device_id"      => "required",
             "color_id"       => "required",
             "serial"         => "string",
             "unknown_device" => "required_if:device_id,0",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "device_id",
             "color_id",
             "serial",
             "unknown_device",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->nullUnknownDeviceIfModelIsGiven();
        $this->resolveDevice();
        $this->resolveColor();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->renameData("device_id", "post_id");
    }



    /**
     * set `unknown_device` to null if exact model is given
     *
     * @return void
     */
    private function nullUnknownDeviceIfModelIsGiven()
    {
        if ($this->getData("device_id")) {
            $this->setData("unknown_device", null);
        }
    }



    /**
     * resolve Device
     *
     * @return void
     */
    private function resolveDevice()
    {
        $this->device = post();

        $given = $this->getData("device_id");

        if (!$given) {
            $this->setData("device_id", 0);
            return;
        }

        /** @var Post $device */
        $device = post()->where("type", "devices")->grabHashid($given);

        if (!$device->exists) {
            $this->injectValidationRule("device_id", "invalid");
            return;
        }

        $this->device = $device;
        $this->setData("device_id", $device->id);
    }



    /**
     * resolve Color
     *
     * @return void
     */
    private function resolveColor()
    {
        $given = $this->getData("color_id");

        if (!$given or !$this->device->exists) {
            return;
        }

        /** @var Post $device */
        $color     = label()->grabHashid($given);
        $available = explode_not_empty(",", $this->device->getMeta("labeled_colors"));

        if (!$color->exists or !in_array($color->slug, $available)) {
            $this->injectValidationRule("color_id", "invalid");
            return;
        }

        $this->setData("color_id", $color->id);
    }

}
