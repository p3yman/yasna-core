<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class IndentSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "salam-indent";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->repairerAssigned()
             and
             $this->user()->is('repairer')
        );
    }



    /**
     * ensure the repair of the assigned work.
     *
     * @return bool
     */
    public function repairerAssigned()
    {
        $indents = $this->getData('indents');
        foreach ($indents as $indent) {
            if (isset($indent['order_id'])) {

                $repairer_assigned = model('salam-order', $indent['order_id'])->repairer_id;
                if ($repairer_assigned !== $this->user()->id) {
                    return false;
                }
            }
        }
        return true;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'indents.*.ware_id'   => 'required',
             'indents.*.order_id'  => 'required',
             'indents.*.defect_id' => 'required',
             'indents.*.quantity'  => 'required|integer',
             'indents.*.image'     => 'array',
        ];

    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->repairerCorrection();
        $this->correctIndent();
    }



    /**
     * correction [repairer_id]
     */
    public function repairerCorrection()
    {
        if ($this->user()->is('repairer')) {
            $this->setData('repairer_id', $this->user()->id);
        }
    }



    /**
     * ensure the given attribute is received as a hashid and converted to a valid id
     *
     * @return void
     */
    private function correctIndent()
    {
        $indents = [];
        foreach ($this->get('indents') as $key => $indent) {

            $this->ensureOrderHashid($indent);
            $this->ensureWareHashid($indent);
            $this->ensureDEfectHashid($indent);


            $indent['repairer_id'] = $this->getData('repairer_id');

            $indents['indents'][$key] = $indent;

        }

        $this->setDataArray($indents);

    }



    /**
     * ensure the given attribute is received as a hashid and converted to a valid id
     *
     * @param array $indent
     *
     * @return bool|void
     */
    public function ensureOrderHashid(&$indent)
    {
        if (!isset($indent['order_id'])) {
            return false;
        }

        if (isset($indent['order_id'])) {

            if (is_numeric($indent['order_id'])) {
                $indent['order_id'] = 0;
            } else {
                $indent['order_id'] = hashid_number($indent['order_id']);
            }

        }
    }



    /**
     * ensure the given attribute is received as a hashid and converted to a valid id
     *
     * @param array $indent
     *
     * @return bool|void
     */
    public function ensureWareHashid(&$indent)
    {
        if (!isset($indent['ware_id'])) {
            return false;
        }

        if (is_numeric($indent['ware_id'])) {
            $indent['ware_id'] = 0;
        } else {
            $indent['ware_id'] = hashid_number($indent['ware_id']);
        }
    }



    /**
     * ensure the given attribute is received as a hashid and converted to a valid id
     *
     * @param array $indent
     *
     * @return bool|void
     */
    public function ensureDefectHashid(&$indent)
    {

        if (!isset($indent['defect_id'])) {
            return false;
        }
        if (is_numeric($indent['defect_id'])) {
            $indent['defect_id'] = 0;
        } else {
            $indent['defect_id'] = hashid_number($indent['defect_id']);
        }
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "indents.*.order_id.required"  => trans("mobile-salam::validation.order_id"),
             "indents.*.ware_id.required"   => trans("mobile-salam::validation.ware_id"),
             "indents.*.defect_id.required" => trans("mobile-salam::validation.defect_id"),
        ];
    }

}
