<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderRepairerEstimationRequest extends YasnaFormRequest
{
    protected $model_name                = "salam-order";
    protected $should_allow_create_mode  = false;
    protected $automatic_injection_guard = false; // <~~ No mass assignment



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->repairer_id == user()->id;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "new_price_estimation" => "numeric|required|between:1000,10000000",
             "new_time_estimation"  => "numeric|required|between:1,100",
             "comment"              => "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "new_price_estimation" => "ed",
             "new_time_estimation"  => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }

}
