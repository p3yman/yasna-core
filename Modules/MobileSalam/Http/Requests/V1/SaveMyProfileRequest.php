<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class SaveMyProfileRequest extends YasnaFormRequest
{
    private $invalid_city = false;

    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id = user()->id;

        return [
             'name_first'  => 'required|persian:60',
             'name_last'   => 'required|persian:60',
             'gender'      => 'numeric:between:1,3',
             'code_melli'  => "code_melli|unique:users,code_melli,$id,id",
             'mobile'      => "required|phone:mobile|unique:users,mobile,$id,id",
             'email'       => "email|unique:users,email,$id,id",
             'postal_code' => 'postal_code',
             "city"        => $this->invalid_city ? "invalid" : "",
             'password'    => 'same:password2|min:8|max:50',
             "longitude"   => ['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
             'latitude'    => ['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
        ];

    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'name_first',
             'name_last',
             'gender',
             'code_melli',
             'mobile',
             'email',
             'home_address',
             'postal_code',
             'tel',
             "city",
             "longitude",
             'latitude',
             'password',
             'password2',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveDivisions();
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {

        if ($this->isset('password')) {
            $this->setHashPassword();
            $this->unsetPassword2();
            $this->setForcePassword();
        }
    }



    /**
     * hash password
     */
    protected function setHashPassword()
    {
        $password = $this->getData('password');

        $password_hash = Hash::make($password);

        $this->setData('password', $password_hash);
    }



    /**
     * unset password2
     */
    protected function unsetPassword2()
    {
        $this->unsetData('password2');
    }



    /**
     * resolve Divisions
     *
     * @return void
     */
    private function resolveDivisions()
    {
        if (!$this->isset("city")) {
            return;
        }

        $city = city()->grabHashid($this->getData("city"));

        if (!$city->exists) {
            $this->invalid_city = true;
            return;
        }

        $this->setData("city", $city->id);
        $this->setData("province", $city->province_id);
    }



    /**
     * set `password_force_change` for Change the password unnecessarily.
     *
     * @return void
     */
    private function setForcePassword()
    {
        $this->setData('password_force_change', 0);
    }

}
