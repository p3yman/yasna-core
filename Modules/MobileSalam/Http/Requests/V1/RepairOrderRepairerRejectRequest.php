<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderRepairerRejectRequest extends YasnaFormRequest
{
    protected $model_name                = "salam-order";
    protected $should_allow_create_mode  = false;
    protected $automatic_injection_guard = false; // <~~ No mass assignment



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->repairer_id == user()->id;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "comment" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }

}
