<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamIndent;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class IndentPartReviewRequest extends YasnaFormRequest
{
    /**
     * @var SalamIndent
     */
    public $model;

    /**
     * @inheritdoc
     */
    protected $model_name = "salam-indent";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        //TODO The permission to change with the specified status from the supplier's side must be checked.
        return $this->repairerAssigned();
    }



    /** ensure the repair of the assigned work.
     *
     * @return bool
     */
    public function repairerAssigned()
    {
        $repairer_assigned = model('salam-order', $this->model->order_id)->repairer_id;
        return ($repairer_assigned == $this->user()->id);

    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'return_reason' => 'required|in:return-reason-used,return-reason-unused,return-reason-factory-fault,return-reason-repairer-fault',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'return_reason',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "used_at"     => "date",
             "returned_at" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctReviewPartDate();
    }



    /**
     * set feedback time.
     */
    public function correctReviewPartDate()
    {
        $attribute = $this->getData('return_reason');
        $this->setDate($attribute);


    }



    /**
     * set the allowed date field according to the status change.
     *
     * @param string $attribute
     */
    private function setDate($attribute)
    {

        if ($attribute === 'return-reason-used') {
            $this->setData('used_at', now()->toDateTimeString());
            $this->setData('returned_at', null);

        } else {
            $this->setData('returned_at', now()->toDateTimeString());
            $this->setData('used_at', null);
        }
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        if (boolval($this->model->return_rejected_at)) {
            $this->setData('return_rejected_at', null);
        }
    }

}
