<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamIndent;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class IndentDeleteRequest extends YasnaFormRequest
{
    /**
     * @var SalamIndent
     */
    public $model;

    /**
     * @inheritdoc
     */
    protected $model_name = "SalamIndent";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->repairerAssigned()
             and
             $this->user()->is('repairer')
        );
    }



    /** ensure the repair of the assigned work.
     *
     * @return bool
     */
    public function repairerAssigned()
    {
        $repairer_assigned = model('salam-order', $this->model->order_id)->repairer_id;
        return ($repairer_assigned == $this->user()->id);

    }

}
