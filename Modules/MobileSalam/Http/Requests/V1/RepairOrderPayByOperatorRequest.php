<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderPayByOperatorRequest extends YasnaFormRequest
{
    protected $model_name               = "SalamOrder";
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $outstanding     = $this->model->amount_outstanding;
        $customer        = $this->model->customer;
        $customer_credit = 0;

        if ($customer) {
            $customer_credit = $this->model->customer->credit;
        }

        return [
             "amount"         => "required|numeric|between:1,$outstanding",
             "wallet_share"   => "required|numeric|between:0,$customer_credit|max:" . $this->getData("amount"),
             "account"        => "required|",
             "effective_date" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "amount",
             "wallet_share",
             "account",
             "effective_date",
             "tracking_number",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "account.required" => trans("mobile-salam::messages.account_not_accepted"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "amount"         => "ed",
             "wallet_share"   => "ed",
             "effective_date" => "ed|date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveAccount();
        $this->resolveEffectiveDate();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * resolve Account
     *
     * @return void
     */
    private function resolveAccount()
    {
        $account = model("account")->grabHashid($this->getData("account"));

        if (!$account->exists) {
            $this->unsetData("account");
            return;
        }

        $this->setData("account", $account->id);
    }



    /**
     * resolve EffectiveDate
     *
     * @return void
     */
    private function resolveEffectiveDate()
    {
        if (!$this->isset("effective_date")) {
            $this->setData("effective_date", now()->toDateTimeString());
        }
    }

}
