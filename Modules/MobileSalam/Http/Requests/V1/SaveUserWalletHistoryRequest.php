<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\User;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use Illuminate\Support\Facades\Validator;

/**
 * @property User $model
 */
class SaveUserWalletHistoryRequest extends YasnaFormRequest
{
    protected $model_name               = "User";
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        Validator::extend('not_zero', function ($attribute, $value) {
            if ($value != 0) {
                return true;
            }
            return false;
        }, trans("mobile-salam::messages.wallet_amount_must_not_be_zero"));

        return [
             "amount"  => "required|numeric|not_zero",
             "account" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'amount',
             "account",
             "title",
             "description",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "amount" => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveAccount();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * resolve Account
     *
     * @return void
     */
    private function resolveAccount()
    {
        $account = model("account")->grabHashid($this->getData("account"));

        if (!$account->exists) {
            $this->unsetData("account");
            return;
        }

        $this->setData("account", $account->id);
    }
}
