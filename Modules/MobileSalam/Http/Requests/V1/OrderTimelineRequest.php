<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * @property SalamOrder $model
 */
class OrderTimelineRequest extends YasnaListRequest
{
    protected $should_allow_create_mode      = false;
    protected $should_load_model_with_hashid = true;
    protected $should_load_model             = true;
    protected $model_name                    = "salam-order";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (user()->isAnyOf("operator", "super-operator")) {
            return true;
        }

        if (user()->is("member")) {
            return boolval($this->model->customer_id == user()->id);
        }

        if (user()->is("repairer")) {
            return boolval($this->model->repairer_id == user()->id);
        }

        return false;
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "including",
             "excluding",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "including" => "array",
             "excluding" => "array",
        ];
    }

}
