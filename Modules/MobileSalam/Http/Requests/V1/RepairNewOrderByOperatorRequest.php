<?php

namespace Modules\MobileSalam\Http\Requests\V1;


use App\Models\Neighborhood;

/**
 * @property int customer_id
 */
class RepairNewOrderByOperatorRequest extends RepairNewOrderRequest
{
    /**
     * get main validation rules
     *
     * @return array
     */
    public function mainRules()
    {
        return [
             "application_type"   => "required|in:application-type-in-person,application-type-by-operator",
             "problems"           => "required|array",
             "is_repaired_before" => "required|boolean",
             "estimated_price"    => "numeric",
             "estimated_time"     => "numeric",
             "unknown_device"     => "required_if:device_id,0",
        ];
    }



    /**
     * get user validation rules
     *
     * @return array
     */
    public function userRules()
    {
        if ($this->isset("customer_id")) {
            return [];
        }

        return [
             "mobile"       => "required|phone:mobile|unique:users",
             'name_first'   => 'required|persian:60',
             'name_last'    => 'required|persian:60',
             'neighborhood' => 'required',
             'home_address' => '',
             'postal_code'  => 'postal_code',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "customer_id",
             "application_type",
             "device_id",
             "color_id",
             "unknown_device",
             "unknown_problem",
             "problems",
             "is_repaired_before",
             "customer_note",
             "estimated_price",
             "estimated_time",
             "serial",
             "mobile",
             "name_first",
             "name_last",
             "home_address",
             "neighborhood",
             "postal_code",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        parent::corrections();
        $this->unsetCustomerId();
        $this->resolveCustomer();
        $this->checkValidNeighborhood();
    }



    /**
     * resolve the customer when the customer_id exist.
     *
     * @return void
     */
    private function resolveCustomer()
    {
        if (!$this->isset("customer_id")) {
            return;
        }

        $customer = user()->grabId($this->getData("customer_id"));
        $this->unsetData("mobile");
        if (!$customer->exists) {
            $this->injectValidationRule("customer_id", "invalid");
            return;
        }
    }



    /**
     * unset customer id when is empty or null
     *
     * @return void
     */
    private function unsetCustomerId()
    {
        if (!$this->getData("customer_id")) {
            $this->unsetData("customer_id");
        }
    }



    /**
     * check the entered neighborhood is valid
     *
     * @return void
     */
    private function checkValidNeighborhood()
    {
        $neighborhood = neighborhood()->grabHashid($this->getData('neighborhood'));
        if (!$neighborhood->exists) {
            $this->injectValidationRule("neighborhood", "invalid");
            return;
        }

        $this->setCityIdAndProvinceId($neighborhood);
    }



    /**
     * set the id for city and province
     *
     * @param Neighborhood $neighborhood
     *
     * @return void
     */
    private function setCityIdAndProvinceId(Neighborhood $neighborhood)
    {
        $this->setData("neighborhood", $neighborhood->id);
        $this->setData("city", $neighborhood->city_id);
        $this->setData("province", $neighborhood->province_id);
    }
}
