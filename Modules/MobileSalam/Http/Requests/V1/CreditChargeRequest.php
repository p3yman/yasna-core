<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\Account;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class CreditChargeRequest extends YasnaFormRequest
{
    /**
     * @var Account
     */
    public $account;

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "amount"  => "required|numeric|integer|min:1000",
             "account" => $this->account->exists ? "" : "invalid",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'amount',
             "account",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "amount" => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveAccount();
    }



    /**
     * resolve account
     */
    private function resolveAccount()
    {
        if (!$this->isset("account")) {
            $this->account = model('account')
                 ->where('order', 1)
                 ->where('type', 'online')
                 ->whereNotNull('published_at')
                 ->where('published_by', '>', 0)
                 ->firstOrNew([])
            ;
            return;
        }

        $this->account = model('account')
             ->whereNotNull('published_at')
             ->where('published_by', '>', 0)
             ->grabHashid($this->getData('account'))
        ;

    }
}
