<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamIndent;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class IndentPartRevertRequest extends YasnaFormRequest
{
    /**
     * @var SalamIndent
     */
    public $model;

    /**
     * @inheritdoc
     */
    protected $model_name = "salam-indent";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;

    /**
     * @inheritdoc
     * guard is manually executed in the corrections method
     */
    protected $automatic_injection_guard = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->user()->is('supplier');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
             'revert_status' => 'required|boolean',
        ];
        if (!boolval($this->revert_status)) {
            $rules = [
                 'supplier_note' => 'required',
            ];
        }
        return $rules;
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'revert_status',
             'supplier_note',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "return_confirmed_at" => "date",
             "return_rejected_at"  => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctPartRevertDate();
    }



    /**
     * set feedback time.
     */
    public function correctPartRevertDate()
    {
        $attribute = $this->getData('revert_status');
        $this->setDate($attribute);


    }



    /**
     * set the allowed date field according to the status change.
     *
     * @param string $attribute
     */
    private function setDate($attribute)
    {
        if (boolval($attribute)) {
            $this->setData('return_confirmed_at', now()->toDateTimeString());
            $this->setData('return_rejected_at', null);

        } else {
            $this->setData('return_rejected_at', now()->toDateTimeString());
            $this->setData('return_confirmed_at', null);
        }
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->unsetData('revert_status');
    }

}
