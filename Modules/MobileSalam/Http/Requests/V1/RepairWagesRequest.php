<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairWagesRequest extends YasnaFormRequest
{
    protected $model_name               = "SalamOrder";
    protected $should_allow_create_mode = false;
}
