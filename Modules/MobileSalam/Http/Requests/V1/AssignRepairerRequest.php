<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use App\Models\User;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class AssignRepairerRequest extends YasnaFormRequest
{
    protected $model_name                = "salam-order";
    protected $should_allow_create_mode  = false;
    protected $automatic_injection_guard = false; // <~~ Safely turned off, because no mass assignment presents in the controller.



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return boolval(!$this->model->repaired_at);
    }



    /**
     * @inheritdoc
     */
    public function mainRules()
    {
        return [
             "repairer" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveRepairer();
    }



    /**
     * resolve Repairer
     *
     * @return void
     */
    private function resolveRepairer()
    {
        $received = $this->getData("repairer");

        if (!$received) {
            return;
        }

        /** @var User $user */
        $user = user()->grabHashid($this->getData("repairer"));

        if (!$user or $user->isNot("repairer")) {
            $this->injectValidationRule("repairer", "invalid");
        }
    }
}
