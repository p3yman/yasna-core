<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\Post;
use Modules\MobileSalam\Entities\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class SaveRepairOrderRatingRequest extends YasnaFormRequest
{
    /**
     * @var SalamOrder
     */
    public $model;

    /** @var Post */
    public $posts;

    /**
     * @inheritdoc
     */
    protected $model_name = "salam-order";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if(!$this->model->canSurvey()) {
            return false;
        }

        if(user()->is("member")) {
            return $this->model->customer_id == user()->id;
        }

        return true; // <~~ Only operators are allowed to pass through the endpoint layer.
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "posts" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "posts",
             "note",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolvePosts();
    }



    /**
     * resolve post
     */
    private function resolvePosts()
    {
        $this->posts = [];
        $param       = $this->getData('posts');

        if ($param && is_array($param)) {
            $i = 0;
            foreach ($param as $post => $rate) {
                $post                    = model('post')
                     ->grabHashid($post)->id
                ;
                $this->posts[$i]['post'] = $post;
                $this->posts[$i]['rate'] = $rate;
                $i++;
            }
        }
    }

}
