<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderCancelRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "SalamOrder";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (user()->is("member")) {
            return boolval($this->model->customer_id == user()->id);
        }

        return true;
    }
}
