<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamIndent;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class IndentSupplyRequest extends YasnaFormRequest
{
    /**
     * @var SalamIndent
     */
    public $model;

    /**
     * @inheritdoc
     */
    protected $model_name = "salam-indent";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return ($this->user()->is('supplier') and !$this->model->return_reason);

    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
             'supply_status' => 'required|in:supply-status-available,supply-status-not-available,supply-status-follow-up',
        ];

        if ($this->getData('supply_status') == 'supply-status-not-available') {
            $rules['supplier_note'] = 'required';
        }

        if ($this->getData('supply_status') == 'supply-status-follow-up') {
            $rules['supplied_at'] = 'required|date|after:' . now()->toDateTimeString();

        } else {
            $rules['supplied_at'] = 'date';
        }

        return $rules;

    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'supply_status',
             'supplier_note',
             'supplied_at',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "supplied_at" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctSuppliedDate();
        $this->correctSupplierId();
    }



    /**
     * correction [supply_at] if is not follow up
     */
    public function correctSuppliedDate()
    {
        if ($this->getData('supply_status') === 'supply-status-available') {
            $this->setData('supplied_at', now()->toDateTimeString());
        }

    }



    /**
     * set [supplier_id] if user as supplier
     */
    public function correctSupplierId()
    {
        if ($this->user()->is('supplier')) {
            $this->setData('supplier_id', $this->user()->id);
        }
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {
        if ($this->getData('supply_status') === 'supply-status-not-available') {
            $this->setData('supplied_at', null);
        }
    }

}
