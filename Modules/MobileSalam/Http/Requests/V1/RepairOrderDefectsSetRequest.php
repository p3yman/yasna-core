<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderDefectsSetRequest extends YasnaFormRequest
{
    protected $model_name                = "salam-order";
    protected $should_allow_create_mode  = false;
    protected $automatic_injection_guard = false; // <~~ Mass assignment is not performed.



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "defects" => "required|array",
        ];
    }

}
