<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\Division;
use Illuminate\Validation\Rule;
use Modules\MobileSalam\Entities\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class SaveRepairOrderPickupRequest extends YasnaFormRequest
{
    /**
     * @var SalamOrder
     */
    public $model;


    /** @var Division */
    public $origin_city;


    /** @var Division */
    public $destination_city;

    /**
     * @inheritdoc
     */
    protected $model_name = "salam-order";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function mainRules()
    {
        return [
             "parent_entity" => "required|in:salam-order,cart",
             "pickup_method" => "required|string",
             "pickup_type"   => "required|string",
        ];
    }



    /**
     * @inheritdoc
     */
    public function originRules()
    {
        if (!$this->getData('pickup_method') !== 'alopeyk') {
            return '';
        }

        if ($this->getData('pickup_type') === 'send') {
            return '';
        }

        return [
             "origin_latitude"  => "required|" . $this->latValidation(),
             "origin_longitude" => "required|" . $this->longValidation(),
        ];

    }



    /**
     * validation cart model when the `entity_model` is the cart
     *
     * @return array
     */
    public function cartRules()
    {
        $id = $this->getData('hashid') ?? $this->getData('id');

        if ($this->getData('parent_entity') !== 'cart') {
            return '';
        }

        if (model('cart', hashid($id))->exists) {
            return '';
        }

        $id_name = $this->getData('hashid') ? 'hashid' : 'id';

        return [
             $id_name => 'invalid',
        ];
    }



    /**
     * @inheritdoc
     */
    public function destinationRules()
    {
        if ($this->getData('pickup_method') !== 'alopeyk') {
            return '';
        }

        return [
             "destination_latitude"  => "required|" . $this->latValidation(),
             "destination_longitude" => "required|" . $this->longValidation(),
        ];
    }



    public function bikeDeliveryRules()
    {
        if ($this->getData('pickup_method') !== 'custom') {
            return '';
        }

        return [
             "driver_full_name"    => "required|string",
             "driver_phone_number" => "required|phone:mobile",
             "pickup_price"        => "required|int",
             "destination_address" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "parent_entity",
             "pickup_method",
             "pickup_type",
             "pickup_price",
             "pickup_time",
             "origin_city",
             "is_cashed",
             "origin_latitude",
             "origin_longitude",
             "origin_address",
             "destination_city",
             "destination_latitude",
             "destination_longitude",
             "destination_address",
             "driver_full_name",
             "driver_phone_number",
             "note",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveOrigin();
        $this->checkType();
        $this->resolveDestination();

    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::pickups");
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return trans("mobile-salam::pickups");
    }



    /**
     * resolve origin city
     *
     * @return void
     */
    private function resolveOrigin()
    {
        $param = $this->getData('origin_city');
        if ($param) {
            $origin_city       = model('division')->find(hashid($param));
            $this->origin_city = $origin_city->title_en;
            $this->setData("origin_city_id", $origin_city->id);
            $this->setData("origin_city_title_en", $origin_city->title_en);
        } else {
            $origin_city       = model('division')->find(get_setting("mobile_salam_location_city_id"));
            $this->origin_city = $origin_city->title_en;
            $this->setData("origin_city_id", get_setting("mobile_salam_location_city_id"));
            $this->setData("origin_city_title_en", $this->origin_city);
        }
        $this->unsetData("origin_city");
    }



    /**
     * resolve destination city
     *
     * @return void
     */
    private function resolveDestination()
    {
        if ($this->getData('pickup_method') !== 'alopeyk') {
            return;
        }

        $this->setData('destination_city', 'AXDdA');// Alopeyk service is only possible for Tehran
        $param = $this->getData('destination_city');

        if ($param) {
            $destination_city       = city()->grabHashid($param);
            $this->destination_city = $destination_city->title_en;
            $this->setData("destination_city_id", $destination_city->id);
            $this->setData("destination_city_title_en", $destination_city->title_en);
        } else {
            $destination_city       = model('division')->find(get_setting("mobile_salam_location_city_id"));
            $this->destination_city = $destination_city->title_en;
            $this->setData("destination_city_id", get_setting("mobile_salam_location_city_id"));
            $this->setData("destination_city_title_en", $this->destination_city);
        }

        $this->unsetData("destination_city");
    }



    /**
     * apply pickup type related conditions
     *
     * @return void
     */
    private function checkType()
    {
        $pickup_type = $this->getData('pickup_type');

        if ($pickup_type == "send" or $pickup_type == "receive") {
            $this->setData("origin_latitude", get_setting("mobile_salam_location_lat"));
            $this->setData("origin_longitude", get_setting("mobile_salam_location_lng"));
            $this->setData("origin_address", get_setting("mobile_salam_location_address"));
        }
    }



    /**
     * longitude validation
     *
     * @return string
     */
    private function longValidation()
    {
        $lng      = $this->getData('origin_longitude');
        $validate = preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/',
             $lng);
        return $validate ? '' : 'invalid';

    }



    /**
     * latitude validation
     *
     * @return string
     */
    private function latValidation()
    {
        $lng      = $this->getData('origin_latitude');
        $validate = preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/',
             $lng);
        return $validate ? '' : 'invalid';

    }


}
