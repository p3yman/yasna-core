<?php

namespace Modules\MobileSalam\Http\Requests\V1;

use App\Models\SalamOrder;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property SalamOrder $model
 */
class RepairOrderCompleteRequest extends YasnaFormRequest
{
    protected $model_name                = "salam-order";
    protected $should_allow_create_mode  = false;
    protected $automatic_injection_guard = false; // <~~ No mass assignment



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->repairer_id == user()->id;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "comment"     => "required",
             "tested_at"   => "required|date|before:" . now()->toDateTimeString(),
             "final_price" => "required|numeric|between:1000,10000000",
             "final_time"  => "required|numeric|between:1,100",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("mobile-salam::attributes");
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "tested_at"   => "date",
             "final_price" => "ed",
             "final_time"  => "ed",
        ];
    }
}
