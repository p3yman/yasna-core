<?php

namespace Modules\MobileSalam\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * @property string $to_from_date
 * @property string $from_date
 */
class ReportListRequest extends YasnaListRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';



    /**
     * @inheritDoc
     */
    public function purifier()
    {
        return [
             'from_date'    => 'date',
             'to_from_date' => 'date',
        ];
    }
}
