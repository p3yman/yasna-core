<?php

namespace Modules\MobileSalam\Http\Requests;

use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class UserSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "User";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "name_first",
             "name_last",
             "gender",
             "mobile",
             "code_melli",
             "email",
             "home_address",
             "postal_code",
             "tel",
             "city",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id = $this->model_id;

        return [
             "name_first" => "required",
             "name_last"  => "required",
             "gender"     => "required|between:1,3",
             "mobile"     => "required|phone:mobile|unique:users,mobile,$id,id",
             "code_melli" => "code_melli|unique:users,code_melli,$id,id",
             "email"      => "email|unique:users,email,$id,id",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "mobile"     => "ed",
             "code_melli" => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        if ($this->createMode()) {
            $pw = Hash::make($this->getData("mobile"));
            $this->setData("password", $pw);
        }
    }
}
