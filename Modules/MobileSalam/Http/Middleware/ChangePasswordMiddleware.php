<?php

namespace Modules\MobileSalam\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ChangePasswordMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->currentUserIsWanderer() and user()->password_force_change) {
            return redirect()->to('manage/mobile-salam/users/password/change/'.user()->hashid);
        }

        return $next($request);
    }



    /**
     * Check an authenticated user wander in our routes or not.
     *
     * @return bool
     */
    protected function currentUserIsWanderer(): bool
    {
        $user = user();
        if ($user->isLoggedIn()) {
            return true;
        }

        return false;
    }
}
