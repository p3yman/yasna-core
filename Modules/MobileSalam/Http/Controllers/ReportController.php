<?php

namespace Modules\MobileSalam\Http\Controllers;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\MobileSalam\Http\Requests\ReportListRequest;
use Modules\Yasna\Services\YasnaController;

class ReportController extends YasnaController
{
    protected $base_model  = "Post";
    protected $view_folder = "mobile-salam::reports";



    /**
     * register row actions
     *
     * @param array $arguments
     *
     * @return void
     */
    public static function handleBrowseWareReport(array $arguments)
    {
        /** @var Post $model */
        $model = $arguments['model'];

        if ($model->type == "shop") {
            service('posts:row_actions')
                 ->add('salam-wares-report')
                 ->icon('bullhorn')
                 ->trans('mobile-salam::report.ware-report')
                 ->link("modal:manage/mobile-salam/reports/-hashid-")
                 ->order(15)
            ;

        }
    }



    /**
     * prepare and return the wages blade form
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function reportWaresForm($hashid)
    {
        $wares = post()->grabHashid($hashid)->wares;

        return $this->view("index", compact("wares"));
    }



    /**
     * return some report in panel
     *
     * @param ReportListRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function reportList(ReportListRequest $request)
    {
        $to_date             = Carbon::parse($request->to_from_date)->endOfDay();
        $from_date           = Carbon::parse($request->from_date)->startOfDay();
        $best_and_worst      = $this->bestAndWorstProducts($from_date, $to_date);
        $repair_benefit_shop = $this->getDataFromIndent($from_date, $to_date);
        $sale_benefit_shop   = $this->getOrdersDataFromCart($from_date, $to_date);
        $repairers_wage      = $this->repairersTotalWage($from_date, $to_date);
        $all_orders_status   = $this->getAllOrdersStatus($from_date, $to_date);
        $page                = $this->getPageInfo();

        return $this->view('list',
             compact('best_and_worst',
                  'repair_benefit_shop',
                  'sale_benefit_shop',
                  'repairers_wage',
                  'all_orders_status',
                  'page'
             ));
    }



    /**
     * get all wares id that sold
     *
     * @param string $from_date
     * @param string $to_date
     *
     * @return array
     */
    protected function bestAndWorstProducts($from_date, $to_date)
    {
        $ware_ids = model('cart')
             ->whereDate('carts.created_at', '<=', $to_date)
             ->whereDate('carts.created_at', '>=', $from_date)
             ->whereNotNull('confirmed_at')
             ->join('orders', 'carts.id', '=', 'orders.cart_id')
             ->select('orders.ware_id')->get()->pluck('ware_id')->toArray()
        ;

        if (!empty($ware_ids)) {
            $products['best']  = max(array_count_values($ware_ids));
            $products['worst'] = min(array_count_values($ware_ids));
        } else {
            $products['best']  = 0;
            $products['worst'] = 0;
        }
        return $products;
    }



    /**
     * get some data from salam indent
     *
     * @param string $from_date
     * @param string $to_date
     *
     * @return float|int
     */
    protected function getDataFromIndent($from_date, $to_date)
    {
        $arr_data = model('salam-indent')
             ->whereDate('created_at', '<=', $to_date)
             ->whereDate('created_at', '>=', $from_date)
             ->select('ware_id', 'wage_final', 'order_id')
             ->get()
             ->toArray()
        ;
        foreach ($arr_data as $data) {
            return $this->calculateRepairBenefitShop($data);
        }
    }



    /**
     * get orders data
     *
     * @param string $from_date
     * @param string $to_date
     *
     * @return float|int
     */
    protected function getOrdersDataFromCart($from_date, $to_date)
    {
        $arr_data = model('cart')
             ->whereNotNull('confirmed_at')
             ->whereDate('carts.created_at', '<=', $to_date)
             ->whereDate('carts.created_at', '>=', $from_date)
             ->join('orders', 'carts.id', '=', 'orders.cart_id')
             ->select('orders.ware_id', 'orders.sale_price')
             ->get()
             ->toArray()
        ;

        foreach ($arr_data as $data) {
            return $this->calculateSaleBenefitShop($data);
        }
    }



    /**
     * calculating the benefit of all repairers.
     *
     * @param string $from_date
     * @param string $to_date
     *
     * @return float|int
     */
    protected function repairersTotalWage($from_date, $to_date)
    {
        return model('salam-order')
             ->whereDate('created_at', '<=', $to_date)
             ->whereDate('created_at', '>=', $from_date)
             ->sum('wage_total_amount')
             ;
    }



    /**
     * get the number of orders by status
     *
     * @param string $from_date
     * @param string $to_date
     *
     * @return array
     */
    protected function getAllOrdersStatus($from_date, $to_date)
    {
        $all_orders = model('salam-order')
             ->whereDate('created_at', '<=', $to_date)
             ->whereDate('created_at', '>=', $from_date)
             ->select('status')
             ->get()
             ->pluck('status')
             ->toArray()
        ;


        return array_count_values($all_orders);
    }



    /**
     * calculation benefit shop from device repair
     *
     * @param array $data
     *
     * @return float|int
     */
    private function calculateRepairBenefitShop(array $data)
    {
        $ware = model('ware')
             ->where('id', $data['ware_id'])
             ->select('original_price')
             ->first()
        ;

        $wage_amount = model('salam-order')
             ->where('id', $data['order_id'])
             ->select('wage_total_amount')
             ->first()
        ;

        $shop_benefit[] = $wage_amount->wage_total_amount - $ware->original_price - $data['wage_final'];

        return array_sum($shop_benefit);
    }



    /**
     * calculation benefit shop from sale products
     *
     * @param array $data
     *
     * @return float|int
     */
    private function calculateSaleBenefitShop(array $data)
    {
        $original_price = model('price')
             ->where('ware_id', $data['ware_id'])
             ->where('type', 'original')
             ->select('amount')
             ->orderBy('created_at', 'desc')
             ->first()
             ->toArray()
        ;

        $shop_sale_benefit[] = $data['sale_price'] - $original_price['amount'];

        return array_sum($shop_sale_benefit);
    }



    /**
     * get page info
     *
     * @return array
     */
    private function getPageInfo()
    {
        return [
             [
                  "/reports/list",
                  trans('mobile-salam::report.sale_report'),
             ],
        ];


    }
}
