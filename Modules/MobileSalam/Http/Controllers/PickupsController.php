<?php

namespace Modules\MobileSalam\Http\Controllers;

use Modules\MobileSalam\Http\Requests\SaveCartPickupRequest;
use Modules\Yasna\Services\YasnaController;

class PickupsController extends YasnaController
{
    protected $base_model  = "salam-pickup";
    protected $view_folder = "mobile-salam::pickups";



    /**
     * resolve the blade responsible to show the index of pickups transactions and related actions
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function list()
    {
        $model  = $this->model()->where('parent_entity', 'cart')->with('cart');
        $page   = [];
        $models = $model->paginate(10);

        return $this->view("list", compact("model", "page", "models"));
    }



    /**
     * get the new form
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function newForm()
    {
        return $this->safeView("new", []);
    }



    /**
     * get the new form
     *
     * @param SaveCartPickupRequest $request
     *
     * @return string
     */
    public function saveForm(SaveCartPickupRequest $request)
    {
        $result        = model("cart")->saveRepairOrderPickup($request);
        $just_estimate = $request->just_estimate;

        if ($just_estimate == 1) {
            return $this->jsonAjaxSaveFeedback(true, [
                 "success_message" => trans('mobile-salam::pickups.pickup_price_estimation_message') . $result["price"],
            ]);
        } else {
            return $this->jsonAjaxSaveFeedback(true);
        }
    }



    /**
     * get the single pickup
     *
     * @param string $hashid
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single($hashid)
    {
        $pickup = $this->model()->grabHashid($hashid);

        return $this->safeView("single", [
             "pickup" => $pickup,
        ]);
    }
}
