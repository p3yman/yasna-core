<?php

namespace Modules\MobileSalam\Http\Controllers;

use App\Models\SalamEstimation;
use Illuminate\Database\Eloquent\Builder;
use Modules\MobileSalam\Http\Requests\DeviceDeletionRequest;
use Modules\MobileSalam\Http\Requests\DeviceSaveRequest;
use Modules\MobileSalam\Http\Requests\EstimationDeviceListRequest;
use Modules\MobileSalam\Http\Requests\EstimationSaveRequest;
use Modules\Yasna\Services\YasnaController;

class EstimationsController extends YasnaController
{
    protected $base_model  = "Post";
    protected $view_folder = "mobile-salam::estimation";



    /**
     * get the list view of all the posts, registered to be considered as repairable devices.
     *
     * @param EstimationDeviceListRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function devices(EstimationDeviceListRequest $request, $tab = 'all')
    {
        $models = $this->getGridBuilder($tab)->paginate(20);
        $page   = $this->getGridPageBreadcrumb($tab);

        if (not_in_array($tab, ["all", "bin"])) {
            return $this->abort(404);
        }

        return $this->view("devices.index", compact("models", "page"));
    }



    /**
     * get the device edit form
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function deviceEditForm($hashid)
    {
        $model = post()->withTrashed()->where("type", "devices")->grabHashid($hashid)->spreadMeta();

        if (!$model->exists) {
            return $this->abort(410);
        }

        return $this->view("devices.editor", compact("model"));
    }



    /**
     * get the device create form
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function deviceCreateForm()
    {
        $model = post();

        return $this->view("devices.editor", compact("model"));
    }



    /**
     * refresh brands combo
     *
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshBrandsCombo($hashid)
    {
        $options = post()::brandsCombo($hashid);
        $model   = post();

        return $this->view("devices.editor-form-brand", compact("options", "model"));
    }



    /**
     * refresh series combo
     *
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshSeriesCombo($hashid)
    {
        $options = post()::seriesCombo($hashid);
        $model   = post();

        return $this->view("devices.editor-form-series", compact("options", "model"));
    }



    /**
     * get the list view of the estimations, related to a single device
     *
     * @param string $post_hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($post_hashid)
    {
        $model = $this->findModel($post_hashid);
        if (!$model->exists) {
            return $this->abort(410);
        }

        return $this->view("device.index", compact("model"));
    }



    /**
     * get the view for estimation edit form
     *
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function estimationEditForm($hashid)
    {
        $model = model('salam-estimation')->grabHashid($hashid);
        $post  = $model->post()->withTrashed()->first();

        if (!$model->exists or !$post or !$post->exists) {
            return $this->abort(410);
        }

        return $this->view("device.edit", compact("model", "post"));
    }



    /**
     * save device
     *
     * @param DeviceSaveRequest $request
     *
     * @return string
     */
    public function deviceSave(DeviceSaveRequest $request)
    {
        $saved = $request->model->saveAsDevice($request->title, $request->labels);

        return $this->jsonAjaxSaveFeedback($saved, [
             'success_callback' => "",
             'success_refresh'  => true,
        ]);
    }



    /**
     * perform device delete action
     *
     * @param DeviceDeletionRequest $request
     *
     * @return string
     */
    public function deviceDelete(DeviceDeletionRequest $request)
    {
        $deleted = $request->model->delete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "",
             'success_refresh'  => true,
        ]);
    }



    /**
     * perform device undelete action
     *
     * @param DeviceDeletionRequest $request
     *
     * @return string
     */
    public function deviceUndelete(DeviceDeletionRequest $request)
    {
        $deleted = $request->model->undelete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "",
             'success_refresh'  => true,
        ]);
    }



    /**
     * perform device destroy action
     *
     * @param DeviceDeletionRequest $request
     *
     * @return string
     */
    public function deviceDestroy(DeviceDeletionRequest $request)
    {
        $deleted = $request->model->hardDelete();

        return $this->jsonAjaxSaveFeedback($deleted, [
             'success_callback' => "",
             'success_refresh'  => true,
        ]);
    }



    /**
     * save the estimation record
     *
     * @param EstimationSaveRequest $request
     *
     * @return string
     */
    public function estimationSave(EstimationSaveRequest $request)
    {
        $saved    = $request->model->batchSave($request);
        $callback = "masterModal('" . route("salam_estimation_device", [hashid($request->post_id)]) . "')";

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             'success_callback'   => $callback,
             "success_modalClose" => false,
        ]);
    }



    /**
     * toggle a estimation record
     *
     * @param string $hashid
     * @param string $value
     *
     * @return void
     */
    public function estimationToggle($hashid, $value)
    {
        /** @var SalamEstimation $model */
        $model = model("salam-estimation")->withTrashed()->grabHashid($hashid);

        if($value=="true") {
            $model->undelete();
        }
        else {
            $model->delete();
        }
    }



    /**
     * get grid page breadcrumb
     *
     * @param string $tab
     *
     * @return array
     */
    private function getGridPageBreadcrumb($tab)
    {
        return [
             [
                  "estimation/devices",
                  trans("mobile-salam::devices.repair_estimations"),
             ],
             [
                  $tab,
                  trans("mobile-salam::devices.$tab"),
             ],
        ];
    }



    /**
     * get grid builder
     *
     * @param string $tab
     *
     * @return Builder
     */
    private function getGridBuilder($tab)
    {
        $builder = posttype("devices")->posts();

        if (request()->searched) {
            $builder->where('title', 'like', '%' . request()->keyword . '%');
        }

        if ($tab == "bin") {
            $builder->onlyTrashed();
        }

        return $builder;
    }
}
