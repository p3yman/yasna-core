<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\SalamOrder;
use App\Models\User;
use Modules\MobileSalam\Http\Requests\V1\RepairWagesRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairWagesSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class RepairWagesController extends YasnaApiController
{
    /**
     * get the current calculated wages
     *
     * @param RepairWagesRequest $request
     *
     * @return array
     */
    public function get(RepairWagesRequest $request)
    {
        $meta = $request->model->toResource("wage_calculated_at");

        return $this->success($request->model->getWageInvoice(), $meta);
    }



    /**
     * perform save action of the wages.
     *
     * @param RepairWagesSaveRequest $request
     *
     * @return array
     */
    public function save(RepairWagesSaveRequest $request)
    {
        $wage_calculated = boolval($request->model->wage_calculated_at);
        $wage_current    = $request->model->wage_total_amount;
        $saved           = $request->model->saveWages($request->toArray());
        $wage_new        = $saved->wage_total_amount;
        $repairer        = $request->model->repairer;

        if (!$saved->exists) {
            return $this->typicalSaveFeedback(false);
        }


        if ($wage_current != $wage_new) {
            if ($wage_calculated) {
                $this->undoLastTransaction($repairer, $saved, $wage_current);
            }

            $this->setNewTransaction($repairer, $saved, $wage_new);
        }


        return $this->typicalSaveFeedback(true);
    }



    /**
     * set a new transaction for the wage of the repairer.
     *
     * @param User       $repairer
     * @param SalamOrder $model
     * @param float      $amount
     *
     * @return int
     */
    private function setNewTransaction($repairer, $model, $amount)
    {
        if (!$repairer) {
            return false;
        }

        return $repairer->changeCredit($amount, $model->id, [
             "title"       => trans("mobile-salam::wallet.repairer_wage"),
             "description" => trans("mobile-salam::wallet.repair_number_x", [
                  "x" => $model->tracking_number,
             ]),
        ]);
    }



    /**
     * undo last transaction of the repairer to prepare for the new amount.
     *
     * @param User       $repairer
     * @param SalamOrder $model
     * @param float      $amount
     *
     * @return int
     */
    private function undoLastTransaction($repairer, $model, $amount)
    {
        if (!$repairer) {
            return false;
        }

        return $repairer->changeCredit(0 - $amount, $model->id, [
             "title"       => trans("mobile-salam::wallet.repairer_wage_undo"),
             "description" => trans("mobile-salam::wallet.repair_number_x", [
                  "x" => $model->tracking_number,
             ]),
        ]);
    }
}
