<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\SalamOrder;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderDefectsGetRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderDefectsSetRequest;
use Modules\MobileSalam\Http\TimelineNodes\DefinedDefectsNode;
use Modules\Yasna\Services\YasnaApiController;

class DefectsController extends YasnaApiController
{

    /**
     * save repair order defects
     *
     * @param RepairOrderDefectsSetRequest $request
     *
     * @return array
     */
    public function save(RepairOrderDefectsSetRequest $request)
    {
        $deleted = $this->deleteProcess($request);
        $saved   = $this->saveProcess($request);

        if ($saved or $deleted) {
            $this->attachNode($request->model);
        }

        return $this->typicalSaveFeedback($saved, [
             "saved_count" => $saved,
        ]);
    }



    /**
     * get the list of order defects
     *
     * @param RepairOrderDefectsGetRequest $request
     *
     * @return array
     */
    public function list(RepairOrderDefectsGetRequest $request)
    {
        return $this->getResourcesFromBuilder($request->model->defects(), $request);
    }



    /**
     * attach `DefinedDefects` node, if not already attached once.
     *
     * @param SalamOrder $model
     *
     * @return void
     */
    private function attachNode($model)
    {
        if ($model->nodes()->where("class", "DefinedDefectsNode")->exists()) {
            return;
        }

        DefinedDefectsNode::insert($model);
    }



    /**
     * go through the defects and save the existing keys
     *
     * @param RepairOrderDefectsSetRequest $request
     *
     * @return bool
     */
    private function saveProcess(RepairOrderDefectsSetRequest $request)
    {
        $saved = 0;

        foreach ($request->defects as $item) {
            $item = array_normalize((array)$item, [
                 "id"    => null,
                 "type"  => null,
                 "title" => null,
            ]);

            if (!$item['type'] or !$item['title']) {
                continue;
            }

            if ($item['id']) {
                $id     = hashid($item['id']);
                $ids[]  = $id;
                $defect = model('salam-defect')->grabId($id);
                if (!$defect->exists) {
                    continue;
                }
            } else {
                $defect = model("salam-defect");
            }

            $saved += $defect->batchSaveBoolean([
                 "order_id" => $request->model->id,
                 "title"    => $item['title'],
                 "type"     => $item['type'],
            ]);
        }

        
        return $saved;
    }



    /**
     * go through the defects and delete the non-existing keys
     *
     * @param RepairOrderDefectsSetRequest $request
     *
     * @return bool
     */
    private function deleteProcess(RepairOrderDefectsSetRequest $request)
    {
        $ids = [];

        foreach ($request->defects as $item) {
            $item = array_normalize((array)$item, [
                 "id" => null,
            ]);

            if ($item['id']) {
                $ids[] = hashid($item['id']);
            }
        }

        $deleted = model("salam-defect")->where("order_id", $request->model->id)->whereNotIn("id", $ids)->update([
             "deleted_at" => now()->toDateTimeString(),
             "deleted_by" => user()->id,
        ])
        ;

        return (bool)$deleted;
    }
}
