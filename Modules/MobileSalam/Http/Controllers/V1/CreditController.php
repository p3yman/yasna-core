<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\Transaction;
use Modules\MobileSalam\Http\Requests\V1\CreditChargeRequest;
use Modules\Yasna\Services\YasnaApiController;

class CreditController extends YasnaApiController
{

    /**
     * charge the credit of the online user
     *
     * @param CreditChargeRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function charge(CreditChargeRequest $request)
    {
        $code = payment()
             ->transaction()
             ->accountId($request->account->id)
             ->invoiceId(0)
             ->userId(user()->id)
             ->callbackUrl('http://www.fake.com')
             ->payableAmount($request->amount)
             ->getTrackingNumber()
        ;

        if (!is_numeric($code)) {
            return $this->serverError(500);
        }

        $this->setCallbackUrl($code);

        return $this->success([
             "tracking_code" => $code,
             "url"           => route("user-credit-payment-dive", [hashid($code)]),
        ], [
             "account" => $request->account->hashid,
             "amount"  => $request->amount,
        ]);
    }



    /**
     * redirect the user to the online gateway
     *
     * @param string $tracking_hashid
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function dive(string $tracking_hashid)
    {
        $code = intval(hashid($tracking_hashid));

        if (!$code) {
            abort(404);
        }

        $fired = payment()->transaction()->tracking($code)->fire();

        if ($fired['slug'] != 'returnable-response') {
            abort(404);
        }

        return $fired['response'];
    }



    /**
     * charge the user credit in case of successful payment and redirect them to the front-end
     *
     * @param string $tracking_hashid
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callback(string $tracking_hashid)
    {
        $code = intval(hashid($tracking_hashid));
        $url  = get_setting("site_url") . "/payments/results?transaction=$tracking_hashid&model=credit&status=";

        if (!$code) {
            return redirect($url . "failed");
        }

        $trans  = payment()->transaction()->tracking($code);
        $model  = $trans->getModel();
        $amount = $model->verified_amount;
        $user   = user($model->user_id);

        if (!$trans->isVerified()) {
            return redirect($url . "failed");
        }

        $user->batchSave([
             "credit" => $user->credit + $amount,
        ]);

        return redirect($url . "ok&order=$user->id");
    }



    /**
     * set callback url to the already available transaction (sorry for the stupid idea, but I had no way.)
     *
     * @param string $tracking_number
     *
     * @return void
     */
    private function setCallbackUrl(string $tracking_number)
    {
        /** @var Transaction $model */
        $model = model("transaction")->where("tracking_no", $tracking_number)->firstOrNew([]);

        $model->batchSave([
             "callback_url" => route("user-credit-payment-callback", [hashid($tracking_number)]),
        ]);
    }
}
