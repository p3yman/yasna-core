<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\Transaction;
use Modules\MobileSalam\Http\Requests\V1\CartPayRequest;
use Modules\Yasna\Services\YasnaApiController;

class CartController extends YasnaApiController
{
    /**
     * perform payment by the customer
     *
     * @param CartPayRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function paymentStart(CartPayRequest $request)
    {
        $discount     = coupon()->evaluate($request->coupon, $request->model->getInvoiceObject())->getTotalDiscount();
        $payable      = $request->model->invoiced_amount - $request->model->paid_amount - $discount;
        $wallet_share = $request->use_wallet ? min(user()->credit, $payable) : 0;
        $online_share = $payable - $wallet_share;
        $wallet_track = null;
        $online_track = null;

        /*-----------------------------------------------
        | Mark Discount as Used ...
        */
        coupon()->markAsUsed($request->coupon, $request->model_id, $request->model->user_id, $discount);

        /*-----------------------------------------------
        | Wallet Share ...
        */
        if ($wallet_share) {
            $wallet_track = $request->model->payFromWallet($wallet_share);
            if (!$wallet_track) {
                return $this->serverError("salam-1001");
            }
        }

        /*-----------------------------------------------
        | Gateway ...
        */
        if ($online_share) {
            $online_track = payment()
                 ->transaction()
                 ->accountId($request->account)
                 ->invoiceId($request->model_id)
                 ->invoiceModel("Cart")
                 ->userId($request->model->user_id)
                 ->callbackUrl('http://www.fake.com')// <~~ will be replaced a few lines after. That's a silly bug!
                 ->payableAmount($online_share)
                 ->getTrackingNumber()
            ;

            if (!is_numeric($online_track)) {
                return $this->serverError("salam-1003");
            }

            $this->setOnlineCallbackUrl($online_track);
        }

        return $this->success(
             [
                  "wallet_share"           => $wallet_share,
                  "wallet_tracking_number" => $wallet_track,
                  "online_share"           => $online_share,
                  "online_gateway_url"     => route("cart-payment-dive", [hashid($online_track)]),
                  "online_tracking_number" => $online_track,
                  "discounted"             => $discount,
             ],
             [
                  "order"   => $request->model_hashid,
                  "account" => hashid($request->account),
                  "amount"  => $payable,
                  "coupon"  => $request->coupon,
             ]
        );
    }



    /**
     * redirect the user to the online gateway
     *
     * @param string $tracking_hashid
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function diveIntoGateway(string $tracking_hashid)
    {
        $code = intval(hashid($tracking_hashid));

        if (!$code) {
            abort(404);
        }

        $fired = payment()->transaction()->tracking($code)->fire();

        if ($fired['slug'] != 'returnable-response') {
            abort(404);
        }

        return $fired['response'];
    }



    /**
     * pay the user invoice in case of successful payment and redirect them to the front-end
     *
     * @param string $tracking_hashid
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callbackFromGateway(string $tracking_hashid)
    {
        $code = intval(hashid($tracking_hashid));
        $url  = get_setting("site_url") . "/payments/results?transaction=$tracking_hashid&model=cart&status=";

        if (!$code) {
            return redirect($url . "failed");
        }

        $trans = payment()->transaction()->tracking($code);
        $model = $trans->getModel();
        if (!$trans->isVerified()) {
            return redirect($url . "failed");
        }

        $order = cart($model->invoice_id);
        $order->refreshSalamPayments();

        return redirect($url . "ok&order=$model->invoice_id");
    }



    /**
     * set callback url to the already available transaction (sorry for the stupid idea, but I had no way.)
     *
     * @param string $tracking_number
     *
     * @return void
     */
    private function setOnlineCallbackUrl(string $tracking_number)
    {
        /** @var Transaction $model */
        $model = model("transaction")->where("tracking_no", $tracking_number)->firstOrNew([]);

        $model->batchSave([
             "callback_url" => route("cart-payment-callback", [hashid($tracking_number)]),
        ]);
    }

}
