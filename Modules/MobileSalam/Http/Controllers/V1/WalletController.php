<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use Modules\MobileSalam\Http\Requests\V1\SaveUserWalletHistoryRequest;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\Yasna\Services\YasnaApiController;

class WalletController extends YasnaApiController
{
    /**
     * fetch user's wallet history
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array
     */
    public function fetchUserHistory(SimpleYasnaListRequest $request)
    {
        $transactions_model = payment()->transaction()->getModel();
        $builder            = $transactions_model->where('user_id', user()->id);

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * save user's wallet history
     *
     * @param SaveUserWalletHistoryRequest $request
     *
     * @return array
     */
    public function saveUserHistory(SaveUserWalletHistoryRequest $request)
    {
        $code = $request->model->changeCredit($request->amount, $request->account, [
             "title"       => $request->title,
             "description" => $request->description,
        ]);

        return $this->typicalSaveFeedback($code, [
             "tracking_number" => $code,
        ]);
    }



    /**
     * Get fresh credit of the current user
     *
     * @return array
     */
    public function myWallet()
    {
        $credit = user()->credit;

        return $this->success([
             "credit" => $credit
        ]);
    }
}
