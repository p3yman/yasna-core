<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\SalamOrder;
use App\Models\Transaction;
use Modules\Yasna\Services\YasnaApiController;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderPayByCustomerRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderPayByOperatorRequest;

class RepairPaymentController extends YasnaApiController
{
    /**
     * perform payment by the customer
     *
     * @param RepairOrderPayByCustomerRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function payByCustomer(RepairOrderPayByCustomerRequest $request)
    {
        $wallet_share = $request->wallet_share;
        $online_share = $request->online_share;
        $wallet_track = null;
        $online_track = null;

        if ($wallet_share) {
            $wallet_track = $request->model->payFromWallet($wallet_share);
            if (!$wallet_track) {
                return $this->serverError("salam-1001");
            }
        }

        if ($online_share) {
            $online_track = payment()
                 ->transaction()
                 ->accountId($request->account)
                 ->invoiceId($request->model_id)
                 ->invoiceModel("SalamOrder")
                 ->userId($request->model->customer_id)
                 ->callbackUrl('http://www.fake.com')// <~~ will be replaced a few lines after. That's a silly bug!
                 ->payableAmount($online_share)
                 ->getTrackingNumber()
            ;

            if (!is_numeric($online_track)) {
                return $this->serverError("salam-1003");
            }

            $this->setOnlineCallbackUrl($online_track);
        }

        return $this->success(
             [
                  "wallet_share"           => $wallet_share,
                  "wallet_tracking_number" => $wallet_track,
                  "online_share"           => $online_share,
                  "online_gateway_url"     => route("repair-payment-dive", [hashid($online_track)]),
                  "online_tracking_number" => $online_track,
             ],
             [
                  "order"   => $request->model_hashid,
                  "account" => hashid($request->account),
                  "amount"  => $wallet_share + $online_share,
             ]
        );
    }



    /**
     * perform payment by operator
     *
     * @param RepairOrderPayByOperatorRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function payByOperator(RepairOrderPayByOperatorRequest $request)
    {
        if ($request->wallet_share) {
            $paid = $request->model->payFromWallet($request->wallet_share);
            if (!$paid) {
                return $this->serverError("salam-1001");
            }
        }

        $remained = $request->amount - $request->wallet_sharel;
        if ($remained) {
            $track = $request->model
                 ->payByOperator($remained, $request->account, $request->effective_date, $request->tracking_number);
            if (!$track) {
                return $this->serverError("salam-1002");
            }
        }

        return $this->typicalSaveFeedback(true);
    }



    /**
     * redirect the user to the online gateway
     *
     * @param string $tracking_hashid
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function diveIntoGateway(string $tracking_hashid)
    {
        $code = intval(hashid($tracking_hashid));

        if (!$code) {
            abort(404);
        }

        $fired = payment()->transaction()->tracking($code)->fire();

        if ($fired['slug'] != 'returnable-response') {
            abort(404);
        }

        return $fired['response'];
    }



    /**
     * pay the user invoice in case of successful payment and redirect them to the front-end
     *
     * @param string $tracking_hashid
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callbackFromGateway(string $tracking_hashid)
    {
        $code = intval(hashid($tracking_hashid));
        $url  = get_setting("site_url") . "/payments/results?transaction=$tracking_hashid&model=repair&status=";

        if (!$code) {
            return redirect($url . "failed");
        }

        $trans = payment()->transaction()->tracking($code);
        $model = $trans->getModel();
        if (!$trans->isVerified()) {
            return redirect($url . "failed");
        }

        /** @var SalamOrder $order */
        $order = model('salam-order', $model->invoice_id);
        $order->refreshPaidAmount();

        return redirect($url . "ok&order=$model->invoice_id");
    }



    /**
     * set callback url to the already available transaction (sorry for the stupid idea, but I had no way.)
     *
     * @param string $tracking_number
     *
     * @return void
     */
    private function setOnlineCallbackUrl(string $tracking_number)
    {
        /** @var Transaction $model */
        $model = model("transaction")->where("tracking_no", $tracking_number)->firstOrNew([]);

        $model->batchSave([
             "callback_url" => route("repair-payment-callback", [hashid($tracking_number)]),
        ]);
    }
}
