<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\SalamDefect;
use App\Models\SalamIndent;
use App\Models\SalamOrder;
use App\Models\User;
use Modules\MobileSalam\Http\Requests\V1\AssignRepairerRequest;
use Modules\MobileSalam\Http\Requests\V1\OrdersListRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairNewOrderByOperatorRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderCancelRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderEditRequest;
use Modules\MobileSalam\Http\Requests\V1\OrderTimelineRequest;
use Modules\MobileSalam\Http\Requests\V1\OrderTrackRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairNewOrderRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderClaimRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderCompleteRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderOperatorFeedbackRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderRepairerEstimationRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairOrderRepairerRejectRequest;
use Modules\MobileSalam\Http\Requests\V1\SaveRepairOrderPickupRequest;
use Modules\MobileSalam\Http\Requests\V1\SaveRepairOrderRatingRequest;
use Modules\MobileSalam\Http\TimelineNodes\CommentedNode;
use Modules\MobileSalam\Http\TimelineNodes\RepairedNode;
use Modules\MobileSalam\Http\TimelineNodes\SubmittedNewOrderNode;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class RepairController extends YasnaApiController
{
    /**
     * submit new order
     *
     * @param RepairNewOrderRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function newOrder(RepairNewOrderRequest $request)
    {
        /** @var SalamOrder $model */
        $model = model("salam-order")->batchSave($request);

        if ($model->exists) {
            SalamDefect::convertCustomerProblems($model->id, $request->problems);
            SubmittedNewOrderNode::insert($model);
        }

        return $this->modelSaveFeedback($model, [
             "tracking_number" => $model->tracking_number,
             "tracking_url"    => $model->tracking_url,
        ]);
    }



    /**
     * submit new order by operator
     *
     * @param RepairNewOrderByOperatorRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function newOrderByOperator(RepairNewOrderByOperatorRequest $request)
    {
        $request = $request->toArray();
        if (!isset($request['customer_id'])) {
            $customer_id = $this->createUser($request);
            if (!$customer_id) {
                return $this->serverError("salam-1004");
            }
        } else {
            $customer_id = $request['customer_id'];
        }

        $data                = $request;
        $data['customer_id'] = $customer_id;
        /** @var SalamOrder $model */
        $model = model("salam-order")->batchSave($data, ["mobile", "city", "province", "home_address", "postal_code"]);

        if ($model->exists) {
            SalamDefect::convertCustomerProblems($model->id, $request['problems']);
            SubmittedNewOrderNode::insert($model);
        }

        return $this->modelSaveFeedback($model, [
             "tracking_number" => $model->tracking_number,
             "tracking_url"    => $model->tracking_url,
        ]);
    }



    /**
     * get list of orders
     *
     * @param OrdersListRequest $request
     *
     * @return array
     */
    public function ordersList(OrdersListRequest $request)
    {
        $builder = $request->model->elector($request->toArray());

        if (user()->hasRole('member')) {
            $builder = $builder->where('customer_id', user()->id);
        }

        if (user()->hasRole('repairer')) {
            $builder = $builder->where('repairer_id', user()->id);
        }

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * get the list of users who have the role of repairer
     *
     * @return array
     */
    public function repairerList()
    {
        $users = role('repairer')
             ->users()
             ->get()
        ;

        $result = [];

        foreach ($users as $user) {

            array_push($result, [
                 'id'   => hashid($user->id),
                 'name' => $user->fullname,
            ]);

        }

        return $this->success($result);

    }



    /**
     * perform repairer assignment
     *
     * @param AssignRepairerRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function assignRepairer(AssignRepairerRequest $request)
    {
        $done = $request->model->assignRepairer($request->repairer);

        return $this->typicalSaveFeedback($done);
    }



    /**
     * get the timeline view of an order
     *
     * @param OrderTimelineRequest $request
     *
     * @return array
     */
    public function getTimeline(OrderTimelineRequest $request)
    {
        return $this->success($request->model->toTimelineArray(), [
             "model" => $request->model->toResource((array)$request->including, (array)$request->excluding),
        ]);
    }



    /**
     * get a limited version of the timeline view of an order.
     *
     * @param OrderTrackRequest $request
     *
     * @return array
     */
    public function trackOrder(OrderTrackRequest $request)
    {
        if (user()->exists) {
            return $this->success($request->model->toTimelineArray());
        }

        return $this->success($request->model->toTrackArray());
    }



    /**
     * perform the action of operator feedback
     *
     * @param RepairOrderOperatorFeedbackRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function operatorFeedback(RepairOrderOperatorFeedbackRequest $request)
    {
        /*-----------------------------------------------
        | Change of Estimations ...
        */
        if (!$request->isCommentOnly()) {
            $request->model->changeEstimations(
                 $request->new_price_estimation,
                 $request->new_time_estimation,
                 $request->comment
            );
        }

        /*-----------------------------------------------
        | Change of Status ...
        */
        if (!$request->isCommentOnly()) {
            $request->model->changeStatus($request->status, $request->comment);
        }

        /*-----------------------------------------------
        | Comment Only ...
        */
        if ($request->isCommentOnly()) {
            CommentedNode::insert($request->model, [
                 "comment" => $request->comment,
            ]);
        }

        /*-----------------------------------------------
        | guarantee_expires_at ...
        */
        if ($request->guarantee_expires_at and $request->guarantee_expires_at != $request->model->guarantee_expires_at) {
            $request->model->batchSave([
                 "guarantee_expires_at" => $request->guarantee_expires_at,
            ]);
        }


        return $this->typicalSaveFeedback(1);
    }



    /**
     * perform cancel order operation.
     *
     * @param RepairOrderCancelRequest $request
     *
     * @return array
     */
    public function cancelOrder(RepairOrderCancelRequest $request)
    {
        $canceled = $request->model->changeStatus("cancelled");

        return $this->typicalSaveFeedback($canceled);
    }



    /**
     * perform edit order operation.
     *
     * @param RepairOrderEditRequest $request
     *
     * @return array
     */
    public function editOrder(RepairOrderEditRequest $request)
    {
        $edited = $request->model->batchSave($request);

        return $this->modelSaveFeedback($edited);
    }



    /**
     * perform repairer reject action
     *
     * @param RepairOrderRepairerRejectRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function repairerReject(RepairOrderRepairerRejectRequest $request)
    {
        $done = $request->model->changeStatus("rejected", $request->comment);

        $request->model->batchSave([
             "rejected_reason" => $request->comment,
        ]);

        return $this->typicalSaveFeedback($done);
    }



    /**
     * perform the act of repairer change of estimation
     *
     * @param RepairOrderRepairerEstimationRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function repairerEstimation(RepairOrderRepairerEstimationRequest $request)
    {
        $done = $request->model->changeEstimations(
             $request->new_price_estimation,
             $request->new_time_estimation,
             $request->comment
        );

        return $this->typicalSaveFeedback($done);
    }



    /**
     * perform repairer work-completion action
     *
     * @param RepairOrderCompleteRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function complete(RepairOrderCompleteRequest $request)
    {
        $done = $request->model->markAsCompleted($request->final_price);

        if ($done) {
            RepairedNode::insert($request->model, [
                 "tested_at"   => $request->tested_at,
                 "comment"     => $request->comment,
                 "final_price" => $request->final_price,
                 "final_time"  => $request->final_time,
            ]);
        }

        return $this->typicalSaveFeedback($done);
    }



    /**
     * perform user claim
     *
     * @param RepairOrderClaimRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function claim(RepairOrderClaimRequest $request)
    {
        $done = $request->model->raiseClaim($request->toArray());

        return $this->typicalSaveFeedback($done);
    }



    /**
     * save order rating
     *
     * @param SaveRepairOrderRatingRequest $request
     *
     * @return array
     */
    public function saveRepairOrderRate(SaveRepairOrderRatingRequest $request)
    {
        $posts = $request->posts;
        $note  = $request->note;

        $request->model->saveRepairOrderRate($posts, $note);

        return $this->success([
             "done" => "1",
        ], [
             "order" => hashid($request->model->id),
             "user"  => user()->hashid,
        ]);
    }



    /**
     * add new pickup by operator
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return array
     */
    public function saveRepairOrderPickup(SaveRepairOrderPickupRequest $request)
    {

        $request->model->saveRepairOrderPickup($request);

        return $this->success([
             "done" => "1",
        ], [
             "order" => hashid($request->model->id),
             "user"  => user()->hashid,
        ]);
    }



    /**
     * estimate new pickup by operator
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return array
     */
    public function estimateRepairOrderPickup(SaveRepairOrderPickupRequest $request)
    {

        $response = $request->model->estimateRepairOrderPickup($request);

        return $this->success([
             "done"  => "1",
             "price" => $response['price'],
        ], [
             "order" => hashid($request->model->id),
             "user"  => user()->hashid,
        ]);
    }



    /**
     * create new user
     *
     * @param array $data
     *
     * @return int
     */
    private function createUser(array $data)
    {
        /**@var $user User */
        $user = model('user')->batchSave([
             "mobile"       => $data['mobile'],
             "name_first"   => $data['name_first'],
             "name_last"    => $data['name_last'],
             "neighborhood" => $data['neighborhood'],
             "city"         => $data['city'],
             "province"     => $data['province'],
             "home_address" => $data['home_address'],
             "postal_code"  => $data['postal_code'],
        ]);

        if ($user->exists) {
            $user->attachRole('member');
        }

        return $user->id;
    }



    /**
     * estimate new pickup by operator
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function fetchAlopeykWebhookData(SimpleYasnaRequest $request)
    {

        $request->model->applyAlopeykWebhookData($request);

        return $this->success([
             "done" => "1",
        ], []);
    }



    /**
     * count overall things for a superadmin.
     *
     * @return array
     */
    public function countForSuperadmin()
    {
        $cancelled_by_customer = SalamOrder::where("status", "cancelled")->whereRaw("cancelled_by = customer_id");
        $cancelled_by_shop     = SalamOrder::where("status", "cancelled")->whereRaw("cancelled_by != customer_id");

        $array = [
             "pending"                => SalamOrder::where("status", "pending")->count(),
             "waiting_for_device"     => SalamOrder::where("status", "waiting_for_device")->count(),
             "processing"             => SalamOrder::where("status", "processing")->count(),
             "repaired"               => SalamOrder::where("status", "repaired")->count(),
             "rejected"               => SalamOrder::where("status", "rejected")->count(),
             "cancelled_by_customer"  => $cancelled_by_customer->count(),
             "cancelled_by_repairer"  => $cancelled_by_shop->count(),
             "waiting_for_send"       => SalamOrder::whereNotNull("repaired_at")->whereNull("delivered_at"),
             "indents_pending"        => SalamIndent::where("supply_status", "")->count(),
             "indents_factory_fault"  => SalamIndent::where("return_reason", "return-reason-factory-fault")->count(),
             "indents_repairer_fault" => SalamIndent::where("return_reason", "return-reason-repairer-fault")->count(),
             "indents_unused"         => SalamIndent::where("return_reason", "return-reason-factory-fault")->count(),
        ];

        return $this->success($array);
    }



    /**
     * count overall things for an operator.
     *
     * @return array
     */
    public function countForOperator()
    {
        $cancelled_by_customer = SalamOrder::where("status", "cancelled")->whereRaw("cancelled_by = customer_id");
        $cancelled_by_shop     = SalamOrder::where("status", "cancelled")->whereRaw("cancelled_by != customer_id");

        $array = [
             "pending"               => SalamOrder::where("status", "pending")->count(),
             "waiting_for_device"    => SalamOrder::where("status", "waiting_for_device")->count(),
             "processing"            => SalamOrder::where("status", "processing")->count(),
             "repaired"              => SalamOrder::where("status", "repaired")->count(),
             "cancelled_by_customer" => $cancelled_by_customer->count(),
             "cancelled_by_repairer" => $cancelled_by_shop->count(),
             "waiting_for_send"      => SalamOrder::whereNotNull("repaired_at")->whereNull("delivered_at")->count(),
        ];

        return $this->success($array);
    }



    /**
     * count overall things for a repairer.
     *
     * @return array
     */
    public function countForRepairer()
    {
        $builder = SalamOrder::where("repairer_id", user()->id);
        $waiting = SalamIndent::whereIn("supply_status", ["", "supply-status-follow-up"])
                              ->where("repairer_id", user()->id)
                              ->groupBy("order_id")
                              ->count()
        ;

        $array = [
             "processing"          => $builder->where("status", "processing")->count(),
             "repaired"            => $builder->where("status", "repaired")->count(),
             "waiting_for_indents" => $waiting,
             "rejected"            => $builder->where("status", "rejected")->count(),
        ];

        return $this->success($array);
    }



    /**
     * count overall things for a supplier.
     *
     * @return array
     */
    public function countForSupplier()
    {
        $array = [
             "indents_pending"        => SalamIndent::where("supply_status", "")->count(),
             "indents_followup"       => SalamIndent::where("supply_status", "supply-status-follow-up")->count(),
             "indents_factory_fault"  => SalamIndent::where("return_reason", "return-reason-unused")->count(),
             "indents_repairer_fault" => SalamIndent::where("return_reason", "return-reason-repairer-fault")->count(),
             "indents_unused"         => SalamIndent::where("return_reason", "return-reason-factory-fault")->count(),
        ];

        return $this->success($array);
    }

}
