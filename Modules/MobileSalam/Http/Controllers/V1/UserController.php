<?php


namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\MobileSalam\Events\AuthenticationVerifyTokenSend;
use Modules\MobileSalam\Http\Forms\ProfileForm;
use Modules\MobileSalam\Http\Forms\RegisterForm;
use Modules\MobileSalam\Http\Requests\V1\AuthenticationVerifyTokenRequest;
use Modules\MobileSalam\Http\Requests\V1\HaveUserRequest;
use Modules\MobileSalam\Http\Requests\V1\RegisterUserRequest;
use Modules\MobileSalam\Http\Requests\V1\SaveMyProfileRequest;
use Modules\MobileSalam\Notifications\AuthenticationVerifyTokenNotification;
use Modules\Yasna\Services\YasnaApiController;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends YasnaApiController
{

    /**
     * Create a new user instance after a valid registration
     *
     * @param RegisterUserRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function register(RegisterUserRequest $request)
    {
        /**@var $user User */
        $user = model('user')->batchSave($request);

        if (!$user->exists) {
            return $this->typicalSaveFeedback(false);
        }

        $user->attachRole('member');
        $token = JWTAuth::fromUser($user);

        if (!$token) {
            return $this->clientError('40001');
        }

        return $this->typicalSaveFeedback(true, [
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => apiAuth()->factory()->getTTL() * 60,
        ]);
    }



    /**
     * call the user register form
     *
     * @return array
     */
    public function registerForm()
    {
        $form = (new RegisterForm);

        return $this->success([], ["form" => $form->toArray()]);
    }



    /**
     * Perform save action for the own profile
     *
     * @param SaveMyProfileRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function saveMyProfile(SaveMyProfileRequest $request)
    {
        $saved = user()->batchSave($request);
        $meta  = [
             "id" => user()->hashid,
        ];

        if ($request->password) {
            $meta['new_token'] = JWTAuth::fromUser(user());
        }

        return $this->modelSaveFeedback($saved, $meta);
    }



    /**
     * fetch the profile form
     *
     * @return array
     */
    public function profileForm()
    {
        $form = (new ProfileForm());

        return $this->success(
             [
                  "name_first"    => user()->name_first,
                  "name_last"     => user()->name_last,
                  "gender"        => user()->gender,
                  "code_melli"    => user()->code_melli,
                  "mobile"        => user()->mobile,
                  "tel"           => user()->tel,
                  "email"         => user()->email,
                  "postal_code"   => user()->postal_code,
                  "city"          => user()->city ? hashid(user()->city) : "",
                  "_details_city" => [
                       "city"     => user()->city ? hashid(user()->city) : "",
                       "province" => user()->province ? hashid(user()->province) : "",
                  ],
                  "longitude"     => user()->getMeta("longitude"),
                  "latitude"      => user()->getMeta("latitude"),
                  "home_address"  => user()->home_address,
             ],
             [
                  "form" => $form->toArray(),
             ]
        );
    }



    /**
     * check authentication user and send token.
     *
     * @param HaveUserRequest $request
     *
     * @return array
     */
    public function haveUser(HaveUserRequest $request)
    {
        if (user()->hasnotField($request->field)) {
            return $this->success(["hashid" => null], $request->commonMetaResponse());
        }

        if (!user()->is('operator')) {
            $user = user()->where($request->field, $request->value)->first();
            $this->sendUserToken($user, $request);
        }

        return $this->success([
             "hashid" => $user->hashid ?? null,
        ],
             $request->commonMetaResponse()
        );
    }



    /**
     * Send authentication token for user.
     *
     * @param User            $user
     * @param HaveUserRequest $request
     *
     * @return void
     */
    public function sendUserToken($user, $request)
    {
        $token = $this->generateToken();

        if ($user) {
            $this->saveUserToken($token, $user);
        } else {
            $new_user_mobile = $this->saveNewUserToken($token, $request);
            $user            = user(1);
            $user->mobile    = $new_user_mobile;
        }

        $user->notify(new AuthenticationVerifyTokenNotification($token));
    }



    /**
     * Generates a new token for resetting password.
     *
     * @return string
     */
    private function generateToken(): string
    {
        return rand(str_repeat(1, 6), str_repeat(9, 6));
    }



    /**
     * Saves the specified token for the user with the specified validity time.
     *
     * @param User   $user
     * @param string $token The token to be sent.
     *
     * @return User
     */
    private function saveUserToken(string $token, $user)
    {
        $reset_token            = Hash::make($token);
        $reset_token_expires_at = now()->addMinutes(1)->toDateTimeString();

        return $user->batchSave(compact('reset_token', 'reset_token_expires_at'));
    }



    /**
     * Temporarily Saves new user data on the authentication table
     *
     * @param string          $token The token to be sent.
     * @param HaveUserRequest $request
     *
     * @return string
     */
    private function saveNewUserToken(string $token, $request)
    {
        $mobile                 = $request->value;
        $reset_token            = Hash::make($token);
        $reset_token_expires_at = now()->addMinutes(10)->toDateTimeString();
        $model                  = model('salam-authentication')->where('mobile', $request->value)->first();
        if (!$model) {
            $model = model('salam-authentication');
        }
        $saved = $model->batchSave(compact('mobile', 'reset_token', 'reset_token_expires_at'));

        return $saved->mobile;
    }



    /**
     * verify token
     *
     * @param AuthenticationVerifyTokenRequest $request
     *
     * @return array
     */
    public function verifyToken(AuthenticationVerifyTokenRequest $request)
    {
        return $this->success([
             'done'    => 1,
             'payload' => $request->payload ?? null,
             '_token'  => $request->_token ?? null,
        ]);
    }
}
