<?php

namespace Modules\MobileSalam\Http\Controllers\V1;

use App\Models\SalamIndent;
use App\Models\SalamOrder;
use App\Models\User;
use Modules\MobileSalam\Events\NewRepairOrderSubmitted;
use Modules\MobileSalam\Events\SupplyIndentStatusChanged;
use Modules\MobileSalam\Http\Requests\V1\IndentEditRequest;
use Modules\MobileSalam\Http\Requests\V1\IndentPartRevertRequest;
use Modules\MobileSalam\Http\Requests\V1\IndentPartReviewRequest;
use Modules\MobileSalam\Http\Requests\V1\IndentSaveRequest;
use Modules\MobileSalam\Http\Requests\V1\IndentDeleteRequest;
use Modules\MobileSalam\Http\Requests\V1\RepairerIndentsListRequest;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\MobileSalam\Http\Requests\V1\IndentSupplyRequest;
use Modules\MobileSalam\Http\TimelineNodes\SubmittedNewIndentNode;
use Modules\Yasna\Services\YasnaApiController;

class IndentController extends YasnaApiController
{


    /**
     * create new indent by repairer.
     *
     * @param IndentSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function create(IndentSaveRequest $request)
    {
        $model        = $request->model;
        $order_id     = 0;
        $array_hashid = [];
        foreach ($request->indents as $indent) {

            $order_id = $indent['order_id'];

            $this->unsetIllegalField($indent);

            /** @var SalamIndent $saved */
            $saved = $model->batchSave($indent, [
                 'image',
            ]);

            $array_hashid[] = $saved->hashid;

            if (isset($indent['image'])) {
                $this->attachFiles($indent['image'], $saved);
            }

        }
        $this->createNode($order_id, $array_hashid);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * create node on timeline
     *
     * @param string $id
     * @param array  $array
     *
     * @return void
     */
    public function createNode($id, $array)
    {
        /** @var SalamOrder $order */
        $order = model('salam-order', $id);
        SubmittedNewIndentNode::insert($order, [
             'indents' => hashid($array),
        ]);
    }



    /**
     * unset Illegal field.
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    public function unsetIllegalField(&$indent)
    {
        $allow_fields = ['order_id', 'ware_id', 'defect_id', 'quantity', 'image', 'repairer_note', 'repairer_id'];
        foreach ($indent as $key => $illegal_field) {
            if (!in_array($key, $allow_fields)) {
                unset($indent[$key]);
            }
        }
    }



    /**
     * delete a indent.
     *
     * @param IndentDeleteRequest $request
     *
     * @return array
     */
    public function delete(IndentDeleteRequest $request)
    {
        $model = $request->model->delete();

        return $this->typicalSaveFeedback($model);

    }



    /**
     * edit a indent.
     *
     * @param IndentEditRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(IndentEditRequest $request)
    {
        $model = $request->model;
        $saved = $model->batchSave($request, [
             'image',
        ]);

        if (isset($request->image)) {
            $this->attachFiles($request->image, $saved);
        } else {
            $this->detachFiles($saved);
        }

        return $this->modelSaveFeedback($saved);
    }



    /**
     * detach files of indent.
     *
     * @param SalamIndent $model
     *
     * @return void
     */
    public function detachFiles($model)
    {
        $files = $model->files()->pluck('id');

        foreach ($files as $file) {
            uploader()->file($file, true)->detachFromEntity();
        }
    }



    /**
     * attach files to indent.
     *
     * @param array       $files
     * @param SalamIndent $model
     *
     * @return void
     */
    public function attachFiles($files, $model)
    {
        foreach ($files as $image) {
            uploader()->file($image, true)->attachTo($model);
        }
    }



    /**
     * List of orders required by the repairer who sees the supplier.
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function supplierOrdersList(SimpleYasnaListRequest $request)
    {
        $builder = model('salam_indent')->elector($request->toArray());

        return $this->getResourcesFromBuilder($builder, $request);

    }



    /**
     * supply the indent by supplier.
     *
     * @param IndentSupplyRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function supplyIndent(IndentSupplyRequest $request)
    {
        $flag  = false;
        $model = $request->model;

        if ($model->supply_status === 'supply-status-available') {
            $flag = true;
        }
        $saved = $model->batchSave($request);

        $this->afterChangeStatus($saved, $flag);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * Operation after the pieces status change
     *
     * @param SalamIndent $indent
     * @param bool        $flag
     *
     * @return void
     */
    public function afterChangeStatus($indent, $flag)
    {
        if ($indent->supply_status !== 'supply-status-available' and $flag) {
            $this->revertInventory($indent);
        }

        if ($indent->supply_status === 'supply-status-available') {
            $this->reduceInventory($indent);
        }

        $this->notify($indent);
    }



    /**
     * get the list of the indents that the repairer ordered.
     *
     * @param RepairerIndentsListRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function repairerIndentsList(RepairerIndentsListRequest $request)
    {
        $builder = $request->model->indents();

        return $this->getResourcesFromBuilder($builder, $request);
    }



    /**
     * reduce inventory.
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    public function reduceInventory($indent)
    {
        $inventory = $indent->ware->inventories()->where('type', 'supplied_in')->get()->last();

        $reduce = [
             'description' => trans_safe('mobile-salam::messages.indent-supply'),
             'account_id'  => $inventory->account_id,
             'fee'         => $indent->ware->sale_price,
             'alteration'  => 0 - $indent->quantity,
             'type'        => "sold_out",
        ];
        $indent->ware->setInventory($reduce);
    }



    /**
     * send notification to operator and supplier
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    public function notify($indent)
    {
        event(new SupplyIndentStatusChanged($indent));
    }



    /**
     * save review part by the repairer.
     *
     * @param IndentPartReviewRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function partReview(IndentPartReviewRequest $request)
    {

        $model = $request->model;
        $saved = $model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * revert part by supplier.
     *
     * @param IndentPartRevertRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|void
     */
    public function partRevert(IndentPartRevertRequest $request)
    {
        $model = $request->model;
        $saved = $model->batchSave($request);

        if (boolval($saved->return_confirmed_at)) {
            $this->afterChangeRevertStatus($model);
        }

        return $this->modelSaveFeedback($saved);
    }



    /**
     * supplier after the changing status by the repairer.
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    protected function afterChangeRevertStatus($indent)
    {
        if ($indent->return_reason === 'return-reason-unused') {
            $this->IncreaseInventory($indent);
        }
        if ($indent->return_reason === 'return-reason-factory-fault') {
            $this->logChangeInventory($indent);
        }
        if ($indent->return_reason === 'return-reason-repairer-fault') {
            $this->DebtRegistration($indent);
        }
    }



    /**
     * increase inventory.
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    protected function IncreaseInventory($indent)
    {
        $inventory = $indent->ware->inventories()->where('type', 'supplied_in')->get()->last();

        $option = [
             'description' => trans_safe('mobile-salam::messages.indent-unused'),
             'account_id'  => $inventory->account_id,
             'fee'         => $indent->ware->sale_price,
             'alteration'  => $indent->quantity,
        ];
        $indent->ware->setInventory($option);

    }



    /**
     * Increase in inventory when it comes from `available` to the `non-available` mode
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    protected function revertInventory($indent)
    {
        $inventory = $indent->ware->inventories()->where('type', 'supplied_in')->get()->last();

        $option = [
             'description' => trans_safe('mobile-salam::messages.indent-revert'),
             'account_id'  => $inventory->account_id,
             'fee'         => $indent->ware->sale_price,
             'alteration'  => $indent->quantity,
        ];
        $indent->ware->setInventory($option);
    }



    /**
     * log change inventory.
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    protected function logChangeInventory($indent)
    {
        $inventory = $indent->ware->inventories()->where('type', 'supplied_in')->get()->last();

        //increase inventory log
        $increase = [
             'description' => trans_safe('mobile-salam::messages.indent-return-factory-fault'),
             'account_id'  => $inventory->account_id,
             'fee'         => $indent->ware->sale_price,
             'alteration'  => $indent->quantity,
             'type'        => "supplied_at",

        ];
        $indent->ware->setInventory($increase);


        //reduce inventory log
        $reduce = [
             'description' => trans_safe('mobile-salam::messages.indent-factory-fault'),
             'account_id'  => $inventory->account_id,
             'fee'         => $indent->ware->sale_price,
             'alteration'  => 0 - $indent->quantity,
             'type'        => "returned_out",
        ];
        $indent->ware->setInventory($reduce);

    }



    /**
     * debt registration on the repairer account
     *
     * @param SalamIndent $indent
     *
     * @return void
     */
    protected function debtRegistration($indent)
    {
        $debt = 0 - $indent->calculatePenaltyWage();

        /** @var User $repairer */
        $repairer = $indent->repairer;
        $repairer->changeCredit($debt, 0, [
             "invoice_id"  => $indent->order_id,
             "title"       => trans_safe('mobile-salam::messages.indent-repairer-debt'),
             "description" => trans_safe('mobile-salam::messages.indent-repairer-fault'),
        ]);

    }

}
