<?php

namespace Modules\MobileSalam\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\MobileSalam\Http\Requests\ChangeForcePasswordRequest;
use Modules\MobileSalam\Http\Requests\UserSaveRequest;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use Modules\Yasna\Services\YasnaController;

class UsersController extends YasnaController
{
    protected $base_model  = "User";
    protected $view_folder = "mobile-salam::users";



    /**
     * register grid row actions
     *
     * @param array $arguments
     *
     * @return void
     */
    public static function registerGridRowActions($arguments)
    {
        $model = $arguments['model'];

        if ($model->is('member')) {
            service('users:row_actions')
                 ->update('edit')
                 ->link("modal:manage/mobile-salam/users/edit/-hashid-")
                 ->condition($model->canEdit())
                 ->order(11)
            ;
        }
    }



    /**
     * register grid actions
     *
     * @param array $arguments
     *
     * @return void
     */
    public static function registerGridButtons($arguments)
    {
        $service = "users:browse_buttons";
        $role    = $arguments['role'];

        if ($role->slug == "member") {
            service($service)
                 ->update('create')
                 ->link("modal:manage/mobile-salam/users/create/member")
                 ->set('type', 'success')
                 ->condition(user()->as('admin')->can("users-member.create"))
            ;
        }
    }



    /**
     * handle browse columns, by adding things to further services
     *
     * @param array $arguments
     *
     * @return void
     */
    public static function handleBrowseColumns($arguments)
    {
        /** @var Role $role */
        $role = $arguments['role'];
        if (!in_array($role->slug, ["member", "repairer"])) {
            return;
        }

        module('users')
             ->service('browse_headings')
             ->add('wallet')
             ->trans("mobile-salam::wallet.title")
             ->blade("mobile-salam::members.wallet-cell")
             ->order(5)
        ;

        module('users')
             ->service('browse_headings')
             ->update('name')
             ->blade("mobile-salam::users.browser-cell")
        ;


    }



    /**
     * handle browse row actions, by adding things to further services
     *
     * @param array $arguments
     *
     * @return void
     */
    public static function handleRowActions($arguments)
    {
        $model = $arguments['model'];

        service('users:row_actions')
             ->add('wallet')
             ->icon('credit-card')
             ->trans("mobile-salam::wallet.manage")
             ->link("modal:manage/users/wallets/list/-hashid-")
             ->condition($model->is('member'))
             ->order(21)
        ;
    }



    /**
     * prepare the editor form for edit
     *
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editForm($hashid)
    {
        $model = user($hashid);

        if (!$model->exists) {
            return $this->abort(410);
        }
        if (!$model->canEdit()) {
            return $this->abort(403);
        }

        return $this->view("editor", compact("model"));
    }



    /**
     * prepare the editor form for create
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm()
    {
        /** @var User $model */
        $model = model('user');

        if (!$model->canCreate()) {
            return $this->abort(403);
        }

        return $this->view("editor", compact("model"));
    }



    /**
     * perform save action
     *
     * @param UserSaveRequest $request
     *
     * @return string
     */
    public function save(UserSaveRequest $request)
    {
        /** @var User $saved */
        $saved = $request->model->batchSave($request);

        if ($saved->exists) {
            $saved->attachRole("member");
        }

        return $this->jsonAjaxSaveFeedback($saved->exists, [
             'success_callback' => "rowUpdate('tblUsers','$request->model_hashid')",
             'success_refresh'  => "",
        ]);
    }



    /**
     * show user profile
     *
     * @param $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function showProfile($hashid)
    {
        $model = user($hashid);

        if (!$model->exists) {
            return $this->abort(410);
        }

        return $this->view("profile", [
             "model" => $model,
             "rows"  => $this->getProfileRows($model),
        ]);
    }



    /**
     * get profile rows
     *
     * @param User $user
     *
     * @return array
     */
    private function getProfileRows(User $user): array
    {

        return [
             [
                  "title" => trans("validation.attributes.code_melli"),
                  "value" => $user->code_melli,
             ],
             [
                  "title" => trans("validation.attributes.mobile"),
                  "value" => $user->mobile,
             ],
             [
                  "title" => trans("validation.attributes.city"),
                  "value" => $this->getCityCaption($user),
             ],
             [
                  "title" => trans("validation.attributes.home_address"),
                  "value" => $user->home_address,
             ],
             [
                  "title" => trans("validation.attributes.postal_code"),
                  "value" => $user->postal_code,
             ],
             [
                  "title" => trans("validation.attributes.tel"),
                  "value" => $user->tel,
             ],
        ];
    }



    /**
     * get city caption
     *
     * @param User $user
     *
     * @return string
     */
    private function getCityCaption(User $user)
    {
        $city  = city($user->city);
        $value = "-";

        if ($city->exists) {
            $value = $city->title_fa;
        }

        $province = $city->province;
        if ($province) {
            $value .= " ($province->title_fa) ";
        }

        return $value;

    }



    /**
     * register row actions
     *
     * @param array $arguments
     *
     * @return void
     */
    public static function handleBrowseUserSaleReport($arguments)
    {
        //TODO check permission
        service('users:row_actions')
             ->add('salam-user-wares-report')
             ->icon('bullhorn')
             ->trans('mobile-salam::report.user-sale-report')
             ->link("modal:manage/mobile-salam/reports/user/-hashid-")
             ->order(15)
        ;


    }



    /**
     * prepare and return the wages blade form
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function reportUserSaleForm($hashid)
    {
        $model    = model('transaction')
             ->where('user_id', hashid($hashid))
             ->get()
        ;
        $credit   = [];
        $repairer = [];
        foreach ($model as $item) {
            if ($item->invoice_model === 'SalamOrder') {
                $repairer[] = $item->verified_amount;
            }
            if ($item->invoice_model === 'User') {
                $credit[] = $item->verified_amount;
            }
        }
        $transactions['credit']   = array_sum($credit);
        $transactions['repairer'] = array_sum($repairer);
        return $this->view("report.index", compact("transactions"));
    }



    /**
     * return view change password.
     *
     * @param YasnaFormRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function ChangePassword(YasnaFormRequest $request)
    {
        $hashid = $request->hashid;
        return $this->view('change-password', compact('hashid'));
    }



    /**
     * save new password.
     *
     * @param YasnaFormRequest $request
     *
     * @return string
     */
    public function saveChangePassword(ChangeForcePasswordRequest $request)
    {
        $request->model->update([
             'password'              => Hash::make($request->password),
             'password_force_change' => 0,
        ]);
        return response()->json(['success' => true]);
    }
}
