<?php

namespace Modules\MobileSalam\Http\Controllers;

use Modules\MobileSalam\Http\Requests\WalletAdminChangeRequest;
use Modules\Yasna\Services\YasnaController;

class UsersWalletController extends YasnaController
{
    protected $base_model  = "User";
    protected $view_folder = "mobile-salam::members";



    /**
     * resolve the blade responsible to show the index of wallet transactions and related actions
     *
     * @param string $hashid
     */
    public function index($hashid)
    {
        $model = $this->findModel($hashid);

        if (!$model->exists) {
            return $this->abort(410);
        }

        $combo = [
             ["increase", trans("mobile-salam::wallet.increase")],
             ["decrease", trans("mobile-salam::wallet.decrease")],
        ];

        return $this->view("index", compact("model", "combo"));
    }



    /**
     * perform the change action
     *
     * @param WalletAdminChangeRequest $request
     *
     * @return string
     */
    public function change(WalletAdminChangeRequest $request)
    {
        $amount = $request->amount;
        if($request->type == "decrease") {
            $amount = 0 - $amount;
        }

        $changed  = $request->model->changeCredit($amount, $request->account, [
             "title"       => $request->title,
             "description" => $request->description,
        ]);
        $callback = "masterModal(url('manage/users/wallets/list/$request->model_hashid'))";

        return $this->jsonAjaxSaveFeedback($changed, [
             'success_callback'   => $callback . ";rowUpdate('tblUsers','$request->model_hashid')",
             "success_modalClose" => false,
        ]);
    }
}
