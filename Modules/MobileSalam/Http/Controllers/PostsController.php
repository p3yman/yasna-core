<?php

namespace Modules\MobileSalam\Http\Controllers;

use App\Models\Post;
use Modules\MobileSalam\Http\Requests\WageSaveRequest;
use Modules\Yasna\Services\YasnaController;

class PostsController extends YasnaController
{
    protected $base_model  = "Post";
    protected $view_folder = "mobile-salam::wages";



    /**
     * register row actions
     *
     * @param array $arguments
     *
     * @return void
     */
    public static function registerRowActions(array $arguments)
    {
        /** @var Post $model */
        $model = $arguments['model'];

        if ($model->type == "shop") {
            service('posts:row_actions')
                 ->add('salam-wages')
                 ->icon('money')
                 ->trans('mobile-salam::wages.title')
                 ->link("modal:manage/mobile-salam/wages/-hashid-")
                 ->condition($model->isNotTrashed() and $model->type == "shop" and $model->canEdit())
                 ->order(15)
            ;

        }
    }



    /**
     * prepare and return the wages blade form
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function wagesForm($hashid)
    {
        /** @var Post $model */
        $model = post()->grabHashid($hashid);

        if (!$model->exists) {
            return $this->abort(410);
        }
        if (!$model->canEdit()) {
            return $this->abort(403);
        }

        $tag       = $model->getWageLabel();
        $tag_title = $tag ? $tag->title : "!";

        return $this->view("index", compact("model", "tag", "tag_title"));
    }



    /**
     * perform wage save
     *
     * @param WageSaveRequest $request
     *
     * @return string
     */
    public function wagesSave(WageSaveRequest $request)
    {
        $done = $request->model->saveWages($request->toArray());

        return $this->jsonAjaxSaveFeedback($done);
    }
}
