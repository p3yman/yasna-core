<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('MobileSalam')->getControllersNamespace(),
     'prefix'     => 'manage/mobile-salam/users/',
], function () {
    Route::get('edit/{hashid}', 'UsersController@editForm');
    Route::get('create/member', 'UsersController@createForm');
    Route::post('save', 'UsersController@save')->name("mobile-salam-member-save");

    Route::get('profile/{hashid}', 'UsersController@showProfile');
});

Route::group([
     'namespace' => module('MobileSalam')->getControllersNamespace(),
     'prefix'    => 'manage/mobile-salam/users/',
], function () {
    Route::get('password/change/{hashid}', 'UsersController@changePassword');
    Route::post('save/password', 'UsersController@saveChangePassword')->name('save-change-password');

});
