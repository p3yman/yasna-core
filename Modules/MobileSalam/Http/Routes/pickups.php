<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('MobileSalam')->getControllersNamespace(),
     'prefix'     => 'manage/mobile-salam/pickups',
], function () {
    Route::get('list', 'PickupsController@list')->name("salam-pickups-list");
    Route::get('new', 'PickupsController@newForm')->name("salam-pickups-new");
    Route::post('new', 'PickupsController@saveForm')->name("salam-pickups-new-save");
    Route::get('{hashid}', 'PickupsController@single')->name("salam-pickups-single");
});
