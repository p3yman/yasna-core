<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('MobileSalam')->getControllersNamespace("V1"),
     'prefix'     => 'globe',
], function () {

    /*-----------------------------------------------
    | Dive to proceed for the Payment Gateway...
    */
    Route::get('mobile-salam/credit-payment-dive/{tracking}', 'CreditController@dive')
         ->name("user-credit-payment-dive")
    ;
    Route::get('mobile-salam/repair-payment-dive/{tracking}', 'RepairPaymentController@diveIntoGateway')
         ->name("repair-payment-dive")
    ;
    Route::get('mobile-salam/cart-payment-dive/{tracking}', 'CartController@diveIntoGateway')
         ->name("cart-payment-dive")
    ;

    /*-----------------------------------------------
    | Callback After the Payment Process ...
    */
    Route::match(['GET', 'POST'], 'mobile-salam/credit-payment-callback/{hashid}', 'CreditController@callback')
         ->name("user-credit-payment-callback")
    ;
    Route::match(['GET', 'POST'], 'mobile-salam/repair-payment-callback/{hashid}',
         'RepairPaymentController@callbackFromGateway')
         ->name("repair-payment-callback")
    ;
    Route::match(['GET', 'POST'], 'mobile-salam/cart-payment-callback/{hashid}',
         'CartController@callbackFromGateway')
         ->name("cart-payment-callback")
    ;
});
