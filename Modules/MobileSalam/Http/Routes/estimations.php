<?php

Route::group([
     'middleware' => ['web', 'can:estimate'],
     'namespace'  => module('MobileSalam')->getControllersNamespace(),
     'prefix'     => 'manage/estimation',
], function () {
    Route::get('devices/{tab?}', 'EstimationsController@devices')->name("salam_estimation_device_list");
    Route::get('device-new', 'EstimationsController@deviceCreateForm')->name("salam_estimation_new_device");
    Route::get('device-edit/{hashid}', 'EstimationsController@deviceEditForm')->name("salam_estimation_edit_device");
    Route::get('device-action/{hashid}/{action}', 'EstimationsController@singleAction')->name("salam_estimation_device_action");
    Route::post('device-save', 'EstimationsController@deviceSave')->name("salam_estimation_save_device");
    Route::post('device-delete', 'EstimationsController@deviceDelete')->name("salam_estimation_delete_device");
    Route::post('device-undelete', 'EstimationsController@deviceUndelete')->name("salam_estimation_undelete_device");
    Route::post('device-destroy', 'EstimationsController@deviceDestroy')->name("salam_estimation_destroy_device");

    Route::get('device-edit-refresh-brands/{hashid?}', 'EstimationsController@refreshBrandsCombo')
         ->name("salam_estimation_edit_device_refresh_brand")
    ;
    Route::get('device-edit-refresh-series/{hashid?}', 'EstimationsController@refreshSeriesCombo')
         ->name("salam_estimation_edit_device_refresh_series")
    ;

    Route::get('device/{hashid}', 'EstimationsController@index')->name("salam_estimation_device");
    Route::get('edit/{hashid}', 'EstimationsController@estimationEditForm')->name("salam_estimation_edit");
    Route::post('device/estimation-save', 'EstimationsController@estimationSave')->name("salam_estimation_save");
    Route::get('device/estimation-toggle/{hashid?}/{value?}', 'EstimationsController@estimationToggle')->name("salam_estimation_toggle");
});
