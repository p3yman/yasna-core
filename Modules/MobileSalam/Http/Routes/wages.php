<?php

Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'namespace'  => module('MobileSalam')->getControllersNamespace(),
     'prefix'     => 'manage/mobile-salam/wages',
], function () {

    Route::get('/{hashid}', 'PostsController@wagesForm');
    Route::post('/', 'PostsController@wagesSave')->name("mobile_salam_wages_save");
});
