<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('MobileSalam')->getControllersNamespace(),
     'prefix'     => 'manage/users/wallets',
], function () {
    Route::get('list/{user}', 'UsersWalletController@index')->name("mobile-salam-users-wallet-list");
    Route::post('change', 'UsersWalletController@change')->name("mobile-salam-users-wallet-change");
});
