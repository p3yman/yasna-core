<?php

Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'namespace'  => module('MobileSalam')->getControllersNamespace(),
     'prefix'     => 'manage/mobile-salam/reports',
], function () {

    Route::get('/{hashid}', 'ReportController@reportWaresForm');
    Route::get('/user/{hashid}', 'UsersController@reportUserSaleForm');
});

Route::group([
     'middleware' => ['web', 'auth', 'can:report'],
     'namespace'  => module('MobileSalam')->getControllersNamespace(),
     'prefix'     => 'manage/reports',
], function () {

    Route::get('/list', 'ReportController@reportList')->name('report.list');
});
