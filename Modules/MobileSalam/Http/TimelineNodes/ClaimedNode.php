<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use Modules\MobileSalam\Events\NewRepairClaimReceived;
use Modules\MobileSalam\Services\TimelineNode;

class ClaimedNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "problems"      => [],
             "customer_note" => "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function notify()
    {
        event(new NewRepairClaimReceived($this->model->order));
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        $problems = model("salam-estimation")->whereIn("id", $this->model->getData("problems"))->get();
        $problems_array = $problems->map(function($model) {
            return $model->toSingleResource();
        });

        return [
             "problems"      => $problems_array,
             "customer_note" => $this->model->getData("customer_note"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }
}
