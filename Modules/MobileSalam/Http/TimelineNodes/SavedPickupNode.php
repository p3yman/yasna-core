<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use App\Models\SalamPickup;
use Modules\MobileSalam\Services\TimelineNode;

class SavedPickupNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "pickup" => null
        ];
    }



    /**
     * @return array
     */
    public function toArray()
    {
        $pickup_id = $this->model->getData("pickup")['id'];
        $pickups   = model("salam-pickup")->where("id", $pickup_id)->get();

        $pickups_array = $pickups->map(function ($model) {
            return $model->toListResource();
        });

        return [
             "pickup" => $pickups_array,
        ];
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return false;
    }

}
