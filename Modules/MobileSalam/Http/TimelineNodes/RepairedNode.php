<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use Modules\MobileSalam\Events\RepairOrderStatusChanged;
use Modules\MobileSalam\Services\TimelineNode;

class RepairedNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "comment"     => null,
             "tested_at"   => null,
             "final_price" => 0,
             "final_time"  => 0,
        ];
    }



    /**
     * @return array
     */
    public function toArray()
    {
        $tested_at = $this->model->getData("tested_at");
        $tested_at = $tested_at ? carbon()::parse($tested_at)->getTimestamp() : "";

        return [
             "comment"     => $this->model->getData("comment"),
             "tested_at"   => $tested_at,
             "final_price" => $this->model->getData("final_price"),
             "final_time"  => $this->model->getData("final_time"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function notify()
    {
        event(new RepairOrderStatusChanged($this->model->order));
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }

}
