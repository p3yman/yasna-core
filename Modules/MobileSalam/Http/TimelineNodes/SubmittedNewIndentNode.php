<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use App\Models\SalamIndent;
use Modules\MobileSalam\Services\TimelineNode;

class SubmittedNewIndentNode extends TimelineNode
{


    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "indents" => [],
        ];
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return [
             "indents"  => $this->getIndents(),
        ];
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }



    /**
     * get indents array.
     *
     * @return array
     */
    private function getIndents()
    {
        $indents = model("salam-indent")->whereIn("id", $this->model->getData("indents"))->get();
        $array   = $indents->map(function ($model) {
            return $model->toListResource();
        });

        return $array;
    }
}
