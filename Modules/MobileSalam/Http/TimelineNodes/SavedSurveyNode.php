<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use App\Models\SalamRating;
use Modules\MobileSalam\Services\TimelineNode;

class SavedSurveyNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "posts" => null,
             "note"  => null,
        ];
    }



    /**
     * @return array
     */
    public function toArray()
    {

        $note    = $this->model->getData("note");
        $ratings = $this->model->order->ratings->map(function ($model) {
            /** @var SalamRating $model */
            return $model->toListResource();
        });

        return [
             "ratings" => $ratings,
             "note"    => $note,
        ];
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return false;
    }

}
