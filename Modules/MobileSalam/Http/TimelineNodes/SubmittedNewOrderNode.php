<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use Modules\MobileSalam\Services\TimelineNode;
use Modules\MobileSalam\Events\NewRepairOrderSubmitted;

class SubmittedNewOrderNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return $this->model->order->toSingleResource();
    }



    /**
     * @inheritdoc
     */
    public function notify()
    {
        event(new NewRepairOrderSubmitted($this->model->order));
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }
}
