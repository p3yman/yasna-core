<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use Modules\MobileSalam\Events\NewRepairEstimationReceived;
use Modules\MobileSalam\Services\TimelineNode;

class ChangedEstimationNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "comment"         => null,
             "estimated_price" => null,
             "estimated_time"  => null,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return [
             "new_price_estimation" => $this->model->getData("estimated_price"),
             "new_time_estimation"  => $this->model->getData("estimated_time"),
             "comment"              => $this->model->getData("comment"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function notify()
    {
        event(new NewRepairEstimationReceived($this->model->order));
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }
}
