<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use Modules\MobileSalam\Events\RepairOrderStatusChanged;
use Modules\MobileSalam\Services\TimelineNode;

class ChangedStatusNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "comment" => null,
             "status"  => null,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return [
             "status"  => $this->model->getData('status'),
             "comment" => $this->model->getData("comment"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function notify()
    {
        event(new RepairOrderStatusChanged($this->model->order));
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }
}
