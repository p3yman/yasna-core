<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use App\Models\SalamDefect;
use Modules\MobileSalam\Services\TimelineNode;

class DefinedDefectsNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public function toArray()
    {
        $builder = $this->model->order->defects();

        return $builder->get()->map(function ($defect) {
            /** @var SalamDefect $defect */
            return $defect->toListResource();
        })
             ;
    }
}
