<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use Modules\MobileSalam\Services\TimelineNode;

class CommentedNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "comment" => null,
        ];
    }



    /**
     * @inheritdoc
     */
    public function toArray()
    {
        return [
             "comment" => $this->model->getData("comment"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayCustomer()
    {
        return false;
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }
}
