<?php

namespace Modules\MobileSalam\Http\TimelineNodes;

use Modules\MobileSalam\Services\TimelineNode;

class AssignedRepairerNode extends TimelineNode
{
    /**
     * @inheritdoc
     */
    public static function customData()
    {
        return [
             "last_repairer_id" => null,
             "new_repairer_id"  => null,
        ];
    }



    /**
     * @return array
     */
    public function toArray()
    {
        $last = $this->model->getData("last_repairer_id");
        $new  = $this->model->getData("new_repairer_id");

        return [
             "last_repairer" => $last ? user($last)->toMinimalResource() : null,
             "new_repairer"  => $new ? user($new)->toMinimalResource() : null,
        ];
    }



    /**
     * @inheritdoc
     */
    public function shouldDisplayRepairer()
    {
        return true;
    }
}
