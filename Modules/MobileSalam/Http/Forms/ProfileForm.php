<?php


namespace Modules\MobileSalam\Http\Forms;


use Modules\Forms\Services\FormBinder;

class ProfileForm extends FormBinder
{

    /**
     * @inheritDoc
     */
    public function components()
    {

        $this->add("row")
             ->children([

                  component("text")
                       ->name("name_first")
                       ->label(trans('validation.attributes.name_first'))
                       ->order(11),

                  component("text")
                       ->name("name_last")
                       ->label(trans('validation.attributes.name_last'))
                       ->order(12),

             ])
        ;

        $this->add("row")
             ->children([

                  component("number")
                       ->name("code_melli")
                       ->label(trans('validation.attributes.code_melli'))
                       ->order(14),

                  component("email")
                       ->name("email")
                       ->label(trans('validation.attributes.email'))
                       ->order(16),

             ])
        ;

        $this->add("row")
             ->children([

                  component("radio")
                       ->name("gender")
                       ->inline()
                       ->label(trans('validation.attributes.gender'))
                       ->options($this->userGenderList())
                       ->order(13),

             ])
        ;

        $this->add("row")
             ->children([

                  component("number")
                       ->name("mobile")
                       ->label(trans('validation.attributes.mobile'))
                       ->order(15),

                  component("number")
                       ->name("tel")
                       ->label(trans('validation.attributes.tel'))
                       ->order(15),

             ])
        ;

        $this->add("row")
             ->children([

                  component("number")
                       ->name("postal_code")
                       ->label(trans('validation.attributes.postal_code'))
                       ->order(17),

                  component("division")
                       ->name("city")
                       ->country(hashid(495))
                       ->limit('city')
                       ->order(19),

             ])
        ;

        $this->add("row")
             ->children([

                  component("password")
                       ->name("password")
                       ->label(trans('validation.attributes.new_password'))
                       ->order(20),

                  component("password")
                       ->name("password2")
                       ->label(trans('validation.attributes.new_password2'))
                       ->order(21),

             ])
        ;

        $this->add("map")
             ->name("map") //TODO: To be discussed!
             ->center_lat(user()->getMeta("latitude"))
             ->center_lng(user()->getMeta("longitude"))
             ->apiKey("") //TODO: To be discussed!
        ;

        $this->add("textarea")
             ->name("home_address")
             ->rows(3)
             ->label(trans('validation.attributes.home_address'))
             ->order(22)
        ;

    }



    /**
     * user's gender list
     *
     * @return array
     */
    protected function userGenderList()
    {
        return [
             1 => trans('mobile-salam::registerForm.user-gender.men'),
             2 => trans('mobile-salam::registerForm.user-gender.women'),
             3 => trans('mobile-salam::registerForm.user-gender.other'),
        ];

    }
}
