<?php

namespace Modules\MobileSalam\Events;

use App\Models\SalamOrder;
use Modules\Yasna\Services\YasnaEvent;

/**
 * @property SalamOrder $model
 */
class NewRepairOrderSubmitted extends YasnaEvent
{
    public $model_name = "salam-order";
}
