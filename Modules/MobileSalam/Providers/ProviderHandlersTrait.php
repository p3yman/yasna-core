<?php

namespace Modules\MobileSalam\Providers;

trait ProviderHandlersTrait
{
    /**
     * Dashboard Widgets
     *
     * @return void
     */
    public static function handleWidgets()
    {
        $service = 'widgets';

        module('manage')
             ->service($service)
             ->add('feedback')
             ->blade('mobile-salam::widgets.feedback')
             ->trans('mobile-salam::widgets.result_of_survey')
             ->color('primary')
             ->icon('star')
        ;
    }


}
