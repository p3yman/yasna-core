<?php

namespace Modules\MobileSalam\Providers;

use Modules\Manage\Services\ManageDashboardWidget;
use Modules\MobileSalam\Events\NewRepairClaimReceived;
use Modules\MobileSalam\Events\NewRepairEstimationReceived;
use Modules\MobileSalam\Events\NewRepairOrderSubmitted;
use Modules\MobileSalam\Events\RepairOrderStatusChanged;
use Modules\MobileSalam\Events\SupplyIndentStatusChanged;
use Modules\MobileSalam\Http\Endpoints\V1\AssignRepairerEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\AuthenticationVerifyTokenEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\CartPayEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\HaveUserEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\MyWalletEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\IndentDeleteEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\FetchMyProfileEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\FetchUserWalletHistoryEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\GetRepairersEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\IndentEditEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\IndentPartRevertEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\IndentPartReviewEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\IndentSupplyEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\OrdersListEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\OrderTimelineEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RegisterFormEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\CreditChargeEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairerIndentsListEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderCancelEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderCounterEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderEditEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderClaimEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderCompleteEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderDefectsGetEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderDefectsSetEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderOperatorFeedbackEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderPayByCustomerEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderPayByOperatorEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderRepairerEstimationEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderRepairerRejectEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderSubmitEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairOrderTrackEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\IndentSaveEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairWagesEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\RepairWagesSaveEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\SaveMyProfileEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\SaveRepairOrderPickupEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\EstimateRepairOrderPickupEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\AlopeykWebhookEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\SaveUserWalletHistoryEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\SupplierOrdersListEndpoint;
use Modules\MobileSalam\Http\Endpoints\V1\UserRegisterEndpoint;
use Modules\MobileSalam\Http\Middleware\ChangePasswordMiddleware;
use Modules\MobileSalam\Http\Middleware\passwordChangeMiddleware;
use Modules\MobileSalam\Listeners\SendEmailWhenCommentReplied;
use Modules\Commenting\Events\CommentReplied;
use Modules\MobileSalam\Notifications\IndentSupplyStatusNotification;
use Modules\MobileSalam\Notifications\RepairOrderClaimedNotification;
use Modules\MobileSalam\Notifications\RepairOrderEstimationChangedNotification;
use Modules\MobileSalam\Notifications\RepairOrderStatusChangedNotification;
use Modules\MobileSalam\Notifications\RepairOrderSubmittedNotification;
use Modules\MobileSalam\Schedules\AutoCleanExpiredTokensSchedule;
use Modules\MobileSalam\Schedules\AutoCloseRepairOrdersSchedule;
use Modules\MobileSalam\Schedules\AutoStoreRepairOrdersSchedule;
use Modules\Yasna\Services\YasnaProvider;
use Modules\MobileSalam\Http\Endpoints\V1\SaveRepairOrderRatingEndpoint;

/**
 * Class MobileSalamServiceProvider
 *
 * @package Modules\MobileSalam\Providers
 */
class MobileSalamServiceProvider extends YasnaProvider
{
    use ProviderHandlersTrait;



    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
        $this->registerModelTraits();
        $this->registerEventListeners();
        $this->registerServices();
        $this->registerSidebarItems();
        $this->registerRoleSampleModules();
        $this->registerSchedules();
        $this->registerWidgets();
        $this->registerDefaultWidgets();
        $this->registerGlobalMiddlewares();
    }



    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        endpoint()->register(UserRegisterEndpoint::class);
        endpoint()->register(RegisterFormEndpoint::class);
        endpoint()->register(CreditChargeEndpoint::class);
        endpoint()->register(SaveMyProfileEndpoint::class);
        endpoint()->register(FetchMyProfileEndpoint::class);
        endpoint()->register(FetchUserWalletHistoryEndpoint::class);
        endpoint()->register(SaveUserWalletHistoryEndpoint::class);

        endpoint()->register(RepairOrderSubmitEndpoint::class);
        endpoint()->register(GetRepairersEndpoint::class);
        endpoint()->register(AssignRepairerEndpoint::class);
        endpoint()->register(OrdersListEndpoint::class);
        endpoint()->register(OrderTimelineEndpoint::class);
        endpoint()->register(RepairOrderOperatorFeedbackEndpoint::class);
        endpoint()->register(RepairOrderCancelEndpoint::class);
        endpoint()->register(RepairOrderTrackEndpoint::class);
        endpoint()->register(RepairOrderEditEndpoint::class);
        endpoint()->register(RepairOrderDefectsSetEndpoint::class);
        endpoint()->register(RepairOrderDefectsGetEndpoint::class);
        endpoint()->register(RepairOrderCounterEndpoint::class);


        endpoint()->register(RepairOrderRepairerRejectEndpoint::class);
        endpoint()->register(RepairOrderCompleteEndpoint::class);
        endpoint()->register(RepairOrderRepairerEstimationEndpoint::class);

        endpoint()->register(IndentSaveEndpoint::class);
        endpoint()->register(IndentDeleteEndpoint::class);
        endpoint()->register(IndentEditEndpoint::class);
        endpoint()->register(SupplierOrdersListEndpoint::class);
        endpoint()->register(IndentSupplyEndpoint::class);
        endpoint()->register(RepairOrderClaimEndpoint::class);
        endpoint()->register(RepairerIndentsListEndpoint::class);
        endpoint()->register(SaveRepairOrderRatingEndpoint::class);
        endpoint()->register(SaveRepairOrderPickupEndpoint::class);
        endpoint()->register(EstimateRepairOrderPickupEndpoint::class);
        endpoint()->register(AlopeykWebhookEndpoint::class);
        endpoint()->register(IndentPartReviewEndpoint::class);
        endpoint()->register(RepairOrderPayByOperatorEndpoint::class);
        endpoint()->register(RepairOrderPayByCustomerEndpoint::class);
        endpoint()->register(IndentPartRevertEndpoint::class);

        endpoint()->register(RepairWagesEndpoint::class);
        endpoint()->register(RepairWagesSaveEndpoint::class);

        endpoint()->register(CartPayEndpoint::class);
        endpoint()->register(MyWalletEndpoint::class);

        endpoint()->register(HaveUserEndpoint::class);
        endpoint()->register(AuthenticationVerifyTokenEndpoint::class);
    }



    /**
     * register ModelTraits
     *
     * @return void
     */
    private function registerModelTraits()
    {
        $this->addModelTrait("UserCreditTrait", "User");
        $this->addModelTrait("PostSalamTrait", "Post");
        $this->addModelTrait("PostRatingsTrait", "Post");
        $this->addModelTrait("OrderPickupTrait", "SalamOrder");
        $this->addModelTrait("LabelWagesTrait", "Label");
        $this->addModelTrait("CartPickupTrait", "Cart");
        $this->addModelTrait("CartSalamTrait", "Cart");
    }



    /**
     * register listeners of this module.
     */
    private function registerEventListeners()
    {
        $this->listen(CommentReplied::class, SendEmailWhenCommentReplied::class);
        $this->listen(NewRepairOrderSubmitted::class, RepairOrderSubmittedNotification::class);
        $this->listen(NewRepairEstimationReceived::class, RepairOrderEstimationChangedNotification::class);
        $this->listen(RepairOrderStatusChanged::class, RepairOrderStatusChangedNotification::class);
        $this->listen(NewRepairClaimReceived::class, RepairOrderClaimedNotification::class);
        $this->listen(SupplyIndentStatusChanged::class, IndentSupplyStatusNotification::class);
    }



    /**
     * register Services
     *
     * @return void
     */
    private function registerServices()
    {
        module('users')
             ->service('browse_headings_handlers')
             ->add("salam")
             ->method("mobileSalam:UsersController@handleBrowseColumns")
        ;

        module('users')
             ->service('row_action_handlers')
             ->add('salam')
             ->method("mobileSalam:UsersController@handleRowActions")
        ;

        module('users')
             ->service('row_action_handlers')
             ->add('salam')
             ->method("mobileSalam:UsersController@registerGridRowActions")
        ;

        module('users')
             ->service('browse_button_handlers')
             ->add('salam')
             ->method("mobileSalam:UsersController@registerGridButtons")
        ;

        module('manage')
             ->service('template_bottom_assets')
             ->add('salam')
             ->link("manage:libs/vendor/Chart.js/dist/Chart.js")
             ->order(35)
        ;

        module("posts")
             ->service("row_action_handlers")
             ->add("salam-wages")
             ->method("MobileSalam:PostsController@registerRowActions")
        ;
        module("posts")
             ->service("row_action_handlers")
             ->add("salam-wares-report")
             ->method("MobileSalam:ReportController@handleBrowseWareReport")
        ;
        module("users")
             ->service("row_action_handlers")
             ->add("salam-user-sale-report")
             ->method("MobileSalam:UsersController@handleBrowseUserSaleReport")
        ;


    }



    /**
     * Dashboard Widget Handlers
     *
     * @return void
     */
    private function registerWidgets()
    {
        module('manage')
             ->service('widgets_handler')
             ->add('salam')
             ->method('mobileSalam:handleWidgets')
             ->order(11)
        ;
    }



    /**
     * register default widgets
     *
     * @return void
     */
    private function registerDefaultWidgets()
    {
        ManageDashboardWidget::addDefault('gender-age', 3, 3, 24);
    }



    /**
     * register SidebarItems
     *
     * @return void
     */
    private function registerSidebarItems()
    {
        service('manage:settings_sidebar')
             ->add("devices")
             ->link("estimation/devices")
             ->caption("trans:mobile-salam::devices.repair_estimations")
             ->order(8)
             ->condition(function () {
                 return user()->can("estimate");
             })
        ;

        service('manage:settings_sidebar')
             // @TODO it most move to proper location like 'sidebar-link' later, all of my attempts failed at this time.
             ->add('pickups')
             ->link("mobile-salam/pickups/list")
             ->icon("fa-truck")
             ->caption("trans:mobile-salam::pickups.table_title")
             ->order(1)
             ->condition(
                  user()->isAnyOf("operator", "super-operator", "admin", "super-admin")
             )
        ;

        service('shop:shop_sidebar')
             ->add("reports")
             ->link("reports/list")
             ->blade('mobile-salam::reports.list')
             ->caption("trans:mobile-salam::report.sale_report")
             ->condition(function () {
                 return user()->can("report");
             })
        ;

        service('shop:shop_sidebar')
             ->add("pickup")
             ->link("mobile-salam/pickups/list")
             ->blade('mobile-salam::pickups.list')
             ->caption("trans:mobile-salam::pickups.pickup")
        ;

    }



    /**
     * get the module string, to be used as sample for role definitions.
     *
     * @return void
     */
    private function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('estimate')
             ->value('estimate')
        ;
        module('users')
             ->service('role_sample_modules')
             ->add('report')
             ->value('report')
        ;
    }



    /**
     * Register Scheduled Tasks
     *
     * @return void
     */
    private function registerSchedules()
    {
        $this->addSchedule(AutoCloseRepairOrdersSchedule::class);
        $this->addSchedule(AutoStoreRepairOrdersSchedule::class);
        $this->addSchedule(AutoCleanExpiredTokensSchedule::class);
    }



    /**
     * Register middleware which must surrounded around all web route.
     *
     * @return void
     */
    private function registerGlobalMiddlewares()
    {
        $this->app['router']->pushMiddlewareToGroup('web', ChangePasswordMiddleware::class);
    }
}
