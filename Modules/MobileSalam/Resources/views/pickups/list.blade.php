@extends('manage::layouts.template')

@section('content')
	{{--
		|--------------------------------------------------------------------------
		| Toolbar
		|--------------------------------------------------------------------------
		|
		--}}
	<div class="col-xs-12">
		@include("manage::widgets.toolbar" , [
			'buttons' => [
				[
					'caption' => trans('mobile-salam::pickups.new_modal_title'),
					'icon' => "plus",
					"type" => "primary",
					"target" => "masterModal('".route("salam-pickups-new")."')",
					"class" => "btn",
				],
			],
			'title' => trans("mobile-salam::pickups.table_title")
		])
	</div>


	{{--
	|--------------------------------------------------------------------------
	| Grid
	|--------------------------------------------------------------------------
	|
	--}}
	<div class="col-xs-12" id="cart-pickups-list">
		@include("manage::widgets.grid" , [
			'table_id' => "tblCartPickups",
			'row_view' => "mobile-salam::pickups.list-row",
			'handle' => "counter",
			'headings' => [
				trans("mobile-salam::pickups.cart_code"),
				trans("mobile-salam::pickups.pickup_method"),
				trans("mobile-salam::pickups.pickup_type"),
				[trans("mobile-salam::pickups.origin_address"), 250],
				[trans("mobile-salam::pickups.destination_address"),250],
			],
		])
	</div>
@endsection