{!!
widget("modal")
	->label(trans('mobile-salam::pickups.new_modal_title'))
	->target("name:salam-pickups-new-save")
!!}

<div class="modal-body">

	{!!
	widget("text")
		 ->name("parent_entity")
		 ->label(" ")
		 ->inForm()
		 ->value("cart")
		 ->style("display:none")
	!!}

	{!!
    widget("text")
         ->name("id")
         ->label(trans('mobile-salam::pickups.cart_code'))
         ->inForm()
    !!}

	{!!
    widget("combo")
         ->name("just_estimate")
         ->label(trans('mobile-salam::pickups.pickup_price_estimation_only'))
         ->inForm()
         ->options([
             [1, trans("mobile-salam::pickups.yes")],
             [0, trans("mobile-salam::pickups.no")],
         ])
         ->blankCaption(null)
         ->blankValue(0)
    !!}


	{!!
    widget("combo")
         ->name("pickup_method")
         ->label(trans('mobile-salam::pickups.pickup_method'))
         ->required()
         ->inForm()
         ->options([
             ["alopeyk", trans("mobile-salam::pickups.alopeyk")],
             ["owner", trans("mobile-salam::pickups.owner")],
             ["custom", trans("mobile-salam::pickups.custom")],
         ])
         ->blankCaption(null)
         ->blankValue(0)
    !!}

	{!!
	widget("combo")
		 ->name("pickup_type")
		 ->label(trans('mobile-salam::pickups.pickup_type'))
		 ->required()
		 ->inForm()
		 ->options([
			 ["send", trans("mobile-salam::pickups.send")],
			 ["receive", trans("mobile-salam::pickups.receive")],
			 ["custom", trans("mobile-salam::pickups.custom")],
		 ])
		 ->blankCaption(null)
		 ->blankValue(0)
	!!}

	{!!
	widget("combo")
		 ->name("is_cashed")
		 ->label(trans('mobile-salam::pickups.payment_method'))
		 ->inForm()
		 ->options([
			 [1, trans("mobile-salam::pickups.pay_by_cash")],
			 [0, trans("mobile-salam::pickups.pay_by_credit")],
		 ])
		 ->blankCaption(null)
		 ->blankValue(0)
	!!}

	{!!
    widget("text")
         ->name("origin_city")
         ->inForm()
         ->value("")
         ->style("display:none")
         ->label(" ")
    !!}

	{!!
    widget("text")
         ->name("pickup_price")
         ->inForm()
         ->label(trans('mobile-salam::pickups.price'))
    !!}

	{!!
    widget("text") /* @TODO must implement with a datetime widget that make unix timestamp */
         ->name("pickup_time")
         ->inForm()
         ->tooltip(trans('mobile-salam::pickups.pickup_time_tooltip'))
         ->label(trans('mobile-salam::pickups.pickup_time'))
    !!}

	{!!
    widget("text")
         ->name("origin_latitude")
         ->label(trans('mobile-salam::pickups.origin_latitude'))
         ->inForm()
    !!}

	{!!
    widget("text")
         ->name("origin_longitude")
         ->label(trans('mobile-salam::pickups.origin_longitude'))
         ->inForm()
    !!}

	{!!
    widget("textarea")
         ->name("origin_address")
         ->label(trans('mobile-salam::pickups.origin_address'))
         ->inForm()
    !!}

	{!!
    widget("text")
         ->name("destination_city")
         ->inForm()
         ->value("")
         ->style("display:none")
         ->label(" ")
    !!}

	{!!
    widget("text")
         ->name("destination_latitude")
         ->label(trans('mobile-salam::pickups.destination_latitude'))
         ->inForm()
    !!}

	{!!
    widget("text")
         ->name("destination_longitude")
         ->label(trans('mobile-salam::pickups.destination_longitude'))
         ->inForm()
    !!}

	{!!
    widget("textarea")
         ->name("destination_address")
         ->label(trans('mobile-salam::pickups.destination_address'))
         ->inForm()
    !!}

	{!!
    widget("text")
         ->name("driver_full_name")
         ->label(trans('mobile-salam::pickups.driver_full_name'))
         ->numberFormat()
         ->inForm()
    !!}

	{!!
    widget("text")
         ->name("driver_phone_number")
         ->label(trans('mobile-salam::pickups.driver_phone_number'))
         ->inForm()
    !!}

	{!!
	widget("textarea")
		 ->name("note")
		 ->label(trans('mobile-salam::pickups.note'))
		 ->inForm()
	!!}

</div>

<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
