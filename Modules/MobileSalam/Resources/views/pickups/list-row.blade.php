@include('manage::widgets.grid-row-handle', [
    'handle'      => "counter",
])

<td>
	{{$model->cart->code ?? null}}
</td>

<td>
	{{trans("mobile-salam::pickups.".$model->pickup_method)}}
</td>

<td>
	{{trans("mobile-salam::pickups.".$model->pickup_type)}}
</td>

<td>
	{{$model->origin_addr}}
</td>

<td>
	{{$model->destination_addr}}
</td>


<td>
	@include("manage::widgets.toolbar" , [
		'buttons' => [
			[
				'caption' => trans('mobile-salam::pickups.single_modal_title'),
				"type" => "primary",
				"target" => "masterModal('".route("salam-pickups-single", $model->hashid)."')",
				"class" => "btn",
			],
		],
		'title' => ""
	])
</td>
