{!!
widget("modal")
	->label(trans('mobile-salam::pickups.single_modal_title'))
!!}

<div class="modal-body">
	<div class="col-xs-12" id="cart-pickup-single">
		<div class="table-type table-static">
			<table class="table tableGrid table-hover">
				<tbody>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.cart_code")}}
					</td>
					<td>
						{{$pickup->cart->code}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.pickup_method")}}
					</td>
					<td>
						{{trans("mobile-salam::pickups.".$pickup->pickup_method)}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.pickup_type")}}
					</td>
					<td>
						{{trans("mobile-salam::pickups.".$pickup->pickup_type)}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.origin_lat_lng")}}
					</td>
					<td>
						{{$pickup->origin_lat}} {{$pickup->origin_lng}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.origin_address")}}
					</td>
					<td>
						{{$pickup->origin_addr}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.destination_lat_lng")}}
					</td>
					<td>
						{{$pickup->destination_lat}} {{$pickup->destination_lng}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.destination_address")}}
					</td>
					<td>
						{{$pickup->destination_addr}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.driver_full_name")}}
					</td>
					<td>
						{{$pickup->driver_full_name}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.driver_phone_number")}}
					</td>
					<td>
						{{$pickup->driver_phone_number}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.tracking_url")}}
					</td>
					<td>
						{{$pickup->delivery_tracking_url}}
					</td>
				</tr>
				<tr class="grid">
					<td>
						{{trans("mobile-salam::pickups.note")}}
					</td>
					<td>
						{{$pickup->pickup_note}}
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
