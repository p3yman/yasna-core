{!!
widget("modal")
	->label($model->exists? trans("manage::forms.button.edit_info") : trans("mobile-salam::devices.create"))
	->target("name:salam_estimation_save_device")
!!}

<div class="modal-body">

    {!! widget('hidden')->name('id')->value($model->id? $model->hashid : null) !!}

    @include("mobile-salam::estimation.devices.editor-form")

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
