@include("manage::widgets.grid" , [
    'headings'          => [
        trans("mobile-salam::devices.singular"),
        trans("mobile-salam::devices.labels"),
        trans("manage::forms.button.action"),
        "",
    ],
    'row_view'          => "mobile-salam::estimation.devices.row",
    'table_id'          => "tblDevices",
    'handle'            => "counter",
])
