<div class="row">

    <div class="col-sm-4">

        {!!
        widget("combo")
        	->name("type")
        	->id("cmbDeviceType")
        	->label("tr:mobile-salam::devices.attributes.type")
        	->options(post()::typesCombo())
        	->blankValue(0)
        	->value($type = $model->labeled_type)
        	->onChange("inlineDeviceTypeChanged()")
        !!}

    </div>

    <div id="divDeviceBrand" class="col-sm-4" data-src="{{ route("salam_estimation_edit_device_refresh_brand") }}">
        @include("mobile-salam::estimation.devices.editor-form-brand", [
            "options" => post()::brandsCombo($type),
            "value" => $brand = $model->labeled_brand,
        ])
    </div>

    <div id="divDeviceSeries" class="col-sm-4" data-src="{{ route("salam_estimation_edit_device_refresh_series") }}">
        @include("mobile-salam::estimation.devices.editor-form-series", [
            "options" => post()::seriesCombo($brand),
            "value" => $model->labeled_series,
        ])
    </div>

</div>


{!!
widget("text")
	->name("title")
	->label("tr:mobile-salam::devices.attributes.title")
	->value($model->title)
!!}

{!!
widget("selectize")
	->name("colors")
	->label("tr:mobile-salam::devices.attributes.colors")
	->value($model->labeled_colors)
	->options(post()::colorsCombo())
	->valueField("id")
!!}

<script>
    function inlineDeviceTypeChanged()
    {
        divReload('divDeviceBrand', '/'+$('#cmbDeviceType').val());
        $("#divDeviceSeries").html('')
    }
    function inlineDeviceBrandChanged()
    {
        divReload('divDeviceSeries', '/'+$('#cmbDeviceBrand').val());
    }
</script>
