@include("manage::widgets.toolbar" , [
    'title'             => $page[0][1],
    'search' => [
        'target' => route("salam_estimation_device_list"),
        "label"  => trans("manage::forms.button.search"),
        "value" => request()->keyword,
     ],
    'buttons'    => [
        [
	    'target'  => "masterModal('". route("salam_estimation_new_device") ."')",
	    'caption' => trans("mobile-salam::devices.create"),
	    'icon'    => "plus",
        ],
    ],
])
