@extends('manage::layouts.template')

@section('content')
    @include("mobile-salam::estimation.devices.tabs")
    @include("mobile-salam::estimation.devices.toolbar")
    @include("mobile-salam::estimation.devices.grid")
@endsection
