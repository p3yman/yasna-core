{!!
widget("combo")
     ->name("series")
     ->label("tr:mobile-salam::devices.attributes.series")
     ->options($options)
     ->blankValue(0)
    	->value(isset($value)? $value : "")
!!}
