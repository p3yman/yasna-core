@include('manage::widgets.grid-row-handle', [
//    'refresh_url' => "",
    'handle'      => "counter",
])

<td>
    @include("manage::widgets.grid-text" , [
        "text"      => $model->title,
        "locale"    => "en",
        "size"      => "14",
    ])

    @include("manage::widgets.grid-tiny" , [
        'condition' => user()->isDeveloper(),
        'text' => $model->id . ' | ' . $model->hashid,
        "icon" => "bug",
        'color' => "gray",
    ])

</td>

<td>
    @foreach ($model->labels as $label)
        @include('manage::widgets.grid-badge', [
             'text' => $label->title,
             'color' => 'white'
        ])
    @endforeach
</td>

<td>
    {!!
    widget("button")
    	->label("tr:mobile-salam::devices.estimations")
    	->target("masterModal('".route("salam_estimation_device" , [$model->hashid])."')")
    !!}
</td>


<td>
    @include("manage::widgets.grid-actionCol" , [
        "refresh_action" => false,
        "button_label" => "...",
        "actions" => [
            ["edit", trans("manage::forms.button.edit_info"), "masterModal('". route("salam_estimation_edit_device", [$model->hashid]) . "')", $model->isNotTrashed()],
            ["trash", trans("manage::forms.button.soft_delete"), "masterModal('". route("salam_estimation_device_action", [$model->hashid, "devices.delete"]) . "')", $model->isNotTrashed()],
            ["undo", trans("manage::forms.button.undelete"), "masterModal('". route("salam_estimation_device_action", [$model->hashid, "devices.undelete"]) . "')", $model->isTrashed()],
            ["times", trans("manage::forms.button.hard_delete"), "masterModal('". route("salam_estimation_device_action", [$model->hashid, "devices.destroy"]) . "')", $model->isTrashed()],
        ],
    ])
</td>
