@include("manage::widgets.tabs", [
    'current' => $page[1][0],
    'tabs' => [
        [
            'caption' => trans("mobile-salam::devices.all"),
            'url'     => "all",
        ],
        [
            'caption' => trans("mobile-salam::devices.bin"),
            'url'     => "bin",
        ],
    ],
])
