{!!
widget("combo")
     ->name("brand")
     ->id("cmbDeviceBrand")
     ->label("tr:mobile-salam::devices.attributes.brand")
     ->options($options)
     ->blankValue(0)
    	->onChange("inlineDeviceBrandChanged()")
    	->value(isset($value)? $value : "")
!!}
