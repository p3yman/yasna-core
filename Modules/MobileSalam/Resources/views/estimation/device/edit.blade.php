{!!
widget("modal")
	->label("trans:mobile-salam::devices.edit_estimation")
	->target("name:salam_estimation_save")
!!}

<div class="modal-body">

    @include("mobile-salam::estimation.device.form")

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal", [
        "cancel_link" => "masterModal('" . route("salam_estimation_device", [$post->hashid]) . "')",
    ])
</div>
