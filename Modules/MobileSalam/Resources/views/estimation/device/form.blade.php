{!! widget('hidden')->name('post_id')->value($post->hashid) !!}
{!! widget('hidden')->name('id')->value($model->id? $model->hashid : null) !!}

{!!
widget("text")
    ->name('title')
    ->required()
    ->label("tr:mobile-salam::devices.estimation_title")
    ->id("txtEstimationTitle")
    ->value($model->title)
    ->inForm()
!!}

{!!
widget("combo")
    ->name("color")
    ->id("cmbEstimationColor")
    ->label("tr:mobile-salam::devices.estimation_color")
    ->options($post->availableColorsCombo())
    ->blankValue("")
    ->value($model->color? label($model->color)->slug : "")
    ->inForm()
!!}

{!!
widget("text")
    ->name("duration")
    ->required()
    ->numberFormat()
    ->id("txtEstimationDuration")
    ->label("tr:mobile-salam::devices.estimation_duration")
    ->value($model->duration)
    ->inForm()
!!}

{!!
widget("text")
    ->name("price")
    ->required()
    ->numberFormat()
    ->id("txtEstimationPrice")
    ->label("tr:mobile-salam::devices.estimation_price")
    ->value($model->price)
    ->inForm()
!!}

{!!
widget("checkbox")
	->name("is_precise")
	->inForm()
	->value(0)
	->label("tr:mobile-salam::devices.is_precise")
!!}
