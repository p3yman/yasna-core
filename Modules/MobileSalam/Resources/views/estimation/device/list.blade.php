@include("manage::widgets.grid" , [
    "models"            => $model->estimations()->withTrashed()->get(),
    'headings'          => [
        trans("mobile-salam::devices.estimation_title"),
        trans("mobile-salam::devices.estimation_price"),
        trans("mobile-salam::devices.estimation_duration"),
        trans("manage::forms.status_text.active"),
    ],
    'row_view'          => "mobile-salam::estimation.device.row",
    'handle'            => "counter",
    'operation_heading' => false,
    'should_differ_deleted_rows' => false,
])

<script>
    function inlineEstimationToggle(hashid)
    {
        let val = $('#tglActive-' + hashid).is(':checked');
        let row = $("#tr-" + hashid);

        row.addClass("loading");
        $.ajax({
            url  : "{{ route("salam_estimation_toggle") }}" + "/" + hashid + "/" + val.toString(),
            cache: false
        }).done(function (html) {
            row.removeClass("loading");
        });

    }
</script>

