@include("manage::forms.buttons-for-modal", [
    "save_label" => trans("mobile-salam::devices.new_estimation"),
    "save_shape" => "default",
    "save_type"  => "button",
    "save_link"  => "$('.-form,.-list').toggle()"
])
