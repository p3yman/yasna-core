
<div class="panel panel-info">
    <div class="panel-heading">
        {{ trans("mobile-salam::devices.new_estimation") }}
    </div>
    <div class="panel-body">

        {!!
        widget("text")
        	->name('title')
        	->label("tr:mobile-salam::devices.estimation_title")
        	->id("txtEstimationTitle")
        !!}

        <div class="row">

            <div class="col-sm-4">
                {!!
                widget("combo")
                	->name("color")
                	->id("cmbEstimationColor")
                	->label("tr:mobile-salam::devices.estimation_color")
                	->options($model->availableColorsCombo())
                	->blankValue(hashid(0))
                !!}
            </div>

            <div class="col-sm-4">
                {!!
                widget("text")
                	->name("price")
                	->numberFormat()
                	->id("txtEstimationPrice")
                	->label("tr:mobile-salam::devices.estimation_price")
                !!}
            </div>

            <div class="col-sm-4">
                {!!
                widget("text")
                	->name("duration")
                	->numberFormat()
                	->id("txtEstimationDuration")
                	->label("tr:mobile-salam::devices.estimation_duration")
                !!}
            </div>

        </div>

    </div>
</div>
