@include('manage::widgets.grid-row-handle')

<td>
    @include("manage::widgets.grid-text" , [
        "text" => $model->full_title,
        "link" => "masterModal('". route("salam_estimation_edit" , [$model->hashid]) ."')",
    ])
</td>

<td id="tdEstimationText-{{$model->hashid}}">
    @include("mobile-salam::estimation.device.row-price")
</td>

<td>
    @include("manage::widgets.grid-text" , [
        "text" => ad(number_format($model->duration)),
    ])
</td>

<td>
    {!!
    widget("checkbox")
    	->id("tglActive-" . $model->hashid)
    	->value($model->isNotTrashed())
    	->onChange("inlineEstimationToggle('".$model->hashid."')")
    !!}
</td>
