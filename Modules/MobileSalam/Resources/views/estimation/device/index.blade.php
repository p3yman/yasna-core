{!!
widget("modal")
	->label(trans("mobile-salam::devices.estimations_of", ["name" => $model->title]))
	->target(route("salam_estimation_save"))
!!}

<div class="modal-body">

    <div class="-list">
        @include("mobile-salam::estimation.device.list")
    </div>
    <div class="-form noDisplay">
        @include("mobile-salam::estimation.device.form", [
            "post"  => $model,
            "model" => model("salam-estimation"),
        ])
    </div>

</div>

<div class="modal-footer">

    <div class="-list">
        @include("mobile-salam::estimation.device.list-buttons")
    </div>
    <div class="-form noDisplay">
        @include("mobile-salam::estimation.device.form-buttons")
    </div>

</div>
