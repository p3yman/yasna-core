<div class="row">
	<div class="col-lg-12">
		<div class="panel">
			<div class="panel-heading clearfix">
				<div class="panel-title pull-left">
					{{trans('mobile-salam::report.orders_count_with_status')}}
				</div>
				<div class="text-right pull-right">
					{{trans('mobile-salam::report.orders_count') . ':'}}
					<span class="label label-primary">{{ad(array_sum($all_orders_status))}}</span>
				</div>
			</div>
			<div class="panel-body">
				@include('manage::widgets.charts.pie.chartjs-pie',[
					'id' => 'chart-doughnut-3',
					'type' => 'doughnut',
					'labels'=> [
									trans('mobile-salam::status.waiting_for_device'),
									trans('mobile-salam::status.pending'),
									trans('mobile-salam::status.stored'),
									trans('mobile-salam::status.cancelled'),
									trans('mobile-salam::status.rejected'),
									trans('mobile-salam::status.repaired'),
									trans('mobile-salam::status.processing'),
									    ],
					'data' => [
						[
							'backgroundColor' => [ '#7266ba','#fad732','#23b7e5','#f05050', '#f532e5','#5cdd01','#fb7135'],
							'hoverBackgroundColor' => [ '#7266ba','#fad732','#23b7e5','#f05050', '#f532e5','#5cdd01','#fb7135'],
							'data' => [
							 $all_orders_status['waiting_for_device'] ?? 0,
							 $all_orders_status['pending'] ?? 0,
							 $all_orders_status['stored'] ?? 0,
							 $all_orders_status['cancelled'] ?? 0,
							 $all_orders_status['rejected'] ?? 0,
							 $all_orders_status['repaired'] ?? 0,
							 $all_orders_status['processing'] ?? 0,
							     ]
						]
					],
					"options" => [
						"legend" => [
							"display" => true ,
						] ,
					] ,

				])
			</div>
		</div>
	</div>
</div>
