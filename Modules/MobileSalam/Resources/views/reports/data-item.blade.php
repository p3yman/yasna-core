<div class="text-left mb-lg">
	{{ $label or "" }} :
	<b>
		@if(isset($is_text) && $is_text)
			<span>
				{{ $value or "" }}
			</span>
		@else
			@if($value < 0)
				<span class="text-red" style="display: inline-block; direction: ltr;">
					{{ad(number_format($value))}}
				</span>
			@else
				<span class="text-green" style="display: inline-block; direction: ltr;">
					{{ad(number_format($value))}}
				</span>
			@endif
		@endif
	</b>

</div>
