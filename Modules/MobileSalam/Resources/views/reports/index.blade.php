{!!
widget("modal")
	->label("tr:mobile-salam::report.ware-report")
!!}

<div class="modal-body">
	@if($wares->isEmpty())
		@include('mobile-salam::reports.report-error' , ["message" => "no-ware-report"])
	@else
		<table id="tblPosts" class="table tableGrid table-hover">
			<thead>
			<tr class="grid">
				<td>{{trans_safe('mobile-salam::report.ware_name')}}</td>
				<td>{{trans_safe('mobile-salam::report.purchase-price')}}</td>
				<td>{{trans_safe('mobile-salam::report.sale-price')}}</td>
				<td>{{trans_safe('mobile-salam::report.ware_inventory')}}</td>
			</tr>
			</thead>
			@foreach($wares as $ware)
				@php
					//*********************** calculate purchase ***************************//
				$inventories = $ware->inventories->map(function ($item){
					return[
					'type'=> $item->type,
					'count'=> $item->alteration,
					'fee'=> $item->fee,
					];
				});
				$inventory_purchase=[];
				$count_sale=[];
				foreach ($inventories as $inventory) {
					if ($inventory['type'] === 'supplied_in'){
					    $count= $inventory['count'] >= 0 ? $inventory['count'] : 0 - $inventory['count'];
						$inventory_purchase[]= $count * $inventory['fee'];
						$count_purchase[]= $inventory['count'];
					}elseif($inventory['type'] === 'sold_out'){
						$count= $inventory['count'] >= 0 ? $inventory['count'] : 0 - $inventory['count'];
						$inventory_sale[]= $count * $inventory['fee'] ?? 0;
						$count_sale[]= 0 - $inventory['count'];
					}

				}
				//**************************** purchase ********************************************/

					$purchase_total = count($count_purchase) > 0 ? array_sum($count_purchase) : 1;
					$purchase_price= isset($inventory_purchase) ? array_sum($inventory_purchase)/$purchase_total : 0;
					$purchase_result[]=$purchase_price;
				//*********************** sale ***************************//
					$sale_total= count($count_sale) > 0 ? array_sum($count_sale) : 1;
					$sale_price= isset($inventory_sale) ? array_sum($inventory_sale)/$sale_total : 0;
					$sale_result[]=$sale_price;
				/***************************************************************/
				$ware_inventory = $ware->current_inventory;

				@endphp
				<tr class="grid">
					<td>{{$ware->full_title}}</td>
					<td>{{ad(number_format(round($purchase_price) ?? 0))}}</td>
					<td>{{ad(number_format(round($sale_price)) ?? 0)}}</td>
					<td>{{ad(number_format($ware_inventory) ?? 0)}}</td>
				</tr>
			@endforeach
			<thead class="grid">
			<td></td>
			<td>{{trans_safe('mobile-salam::report.sum_purchase')}} {{ad(number_format(round(array_sum($purchase_result))) ?? 0)}}</td>
			<td>{{trans_safe('mobile-salam::report.sum_sale')}} {{ad(number_format(round(array_sum($sale_result))) ?? 0)}}</td>
			<td></td>
			</thead>
		</table>
	@endif
</div>
