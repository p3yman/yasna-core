@extends('manage::layouts.template')
@php
	$best_ware_title = ware($best_and_worst['best']);
	$worst_ware_title = ware($best_and_worst['worst']);
@endphp
@section('content')

	<div class="panel">
		<div class="panel-body">
			{!! widget('form-open')->method('GET')->target(route('report.list'))->id('report-form') !!}
			<div class="row">
				<div class="col-sm-10">
					{!!
						 widget('PersianDatePicker')
							 ->label(trans('mobile-salam::report.date'))
							 ->id('from_date')
							 ->name('from_date')
							 ->labelClass('mt0')
							 ->inRange()
							 ->value("")
							 ->onlyDatePicker()
							 ->withoutTime()
							 ->placeholderFrom('tr:manage::forms.general.from')
							 ->placeholderTo('tr:manage::forms.general.till')
							 ->valueFrom(request()->from_date ? date('Y-m-d' , request()->from_date/1000) : "")
			  				 ->valueTo(request()->to_from_date ? date('Y-m-d' , request()->to_from_date/1000) : "")
					 !!}
				</div>
				<div class="col-sm-2">
					{!!
						widget('button')
						->id('submit')
						->type('submit')
						->label(trans('mobile-salam::report.send'))
						->class('btn-primary mt20 btn-taha')
						->style("margin-top: 27px")
					!!}
				</div>
			</div>
			{!! widget('form-close') !!}
		</div>

	</div>

	 @if(is_null(request()->from_date) and is_null(request()->to_from_date))
		 @include('mobile-salam::reports.report-error' , ["message" => "enter_date"])
	@else

		<div class="row">
			<div class="col-md-6">
				<div class="panel">
					<div class="panel-heading">
						<div class="panel-title">
							{{trans('mobile-salam::report.sale_report')}}
						</div>
					</div>
					<div class="panel-body">
						<div>
							@include('mobile-salam::reports.data-item',[
											"label" => trans('mobile-salam::report.repair_benefit'),
											"value" => $repair_benefit_shop,
										])

							@include('mobile-salam::reports.data-item',[
										"label" => trans('mobile-salam::report.shop_benefit'),
										"value" => $sale_benefit_shop,
									])

							@include('mobile-salam::reports.data-item',[
										"label" => trans('mobile-salam::report.repairers_benefit'),
										"value" => $repairers_wage,
									])
							@include('mobile-salam::reports.data-item',[
											"label" => trans('mobile-salam::report.best_product'),
											"value" => $best_ware_title -> full_title ?? '',
											"is_text" => true,
										])

							@include('mobile-salam::reports.data-item',[
										"label" => trans('mobile-salam::report.worst_product'),
										"value" => $worst_ware_title -> full_title ?? '',
										"is_text" => true,
									])
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				@include('mobile-salam::reports.orders-count-with-status')
			</div>
		</div>
	@endif


@endsection
