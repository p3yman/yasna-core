{!! widget("separator")->label(trans("mobile-salam::wages.post_amounts") . " ($model->title) ") !!}

@include("mobile-salam::wages.amounts", [
    "type" => "post",
])
