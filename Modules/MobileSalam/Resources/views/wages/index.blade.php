{!!
widget("modal")
	->label("tr:mobile-salam::wages.title")
	->target("name:mobile_salam_wages_save")
!!}

<div class="modal-body">
    {!! widget('hidden')->name('id')->value($model->hashid) !!}

    {!!
    widget("note")
    	->label("tr:mobile-salam::wages.tag_change_notice")
    !!}

    @include("mobile-salam::wages.tag")
    @include("mobile-salam::wages.post")

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
