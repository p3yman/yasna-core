{!! widget("separator")->label(trans("mobile-salam::wages.tag_amounts") . " ($tag_title) ") !!}

@if(!$tag)
    <div class="noContent">
        {{ trans("mobile-salam::wages.no_tag") }}
    </div>
@endif

@if($tag)
    @include("mobile-salam::wages.amounts", [
        "type" => "tag",
    ])
@endif
