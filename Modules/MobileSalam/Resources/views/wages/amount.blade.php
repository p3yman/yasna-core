{!!
widget("group")
	->start()
	->label("tr:mobile-salam::wages.$field")
	->name("asd")
!!}

<div class="col-md-9">


    {!!
    widget("input")
         ->name($type . "_" . $field)
         ->numberFormat()
         ->value($model->getWage($field, $type))
    !!}

</div>

<div class="col-md-3">
    {!!
    widget("combo")
         ->name($type . "_" . $field . "_type")
         ->value($model->getWage($field . "_type", $type))
         ->options([
            ["absolute", trans("mobile-salam::wages.absolute")],
            ["percent", trans("mobile-salam::wages.percent")],
         ])
    !!}
</div>


{!!
widget("group")
	->stop()
!!}
