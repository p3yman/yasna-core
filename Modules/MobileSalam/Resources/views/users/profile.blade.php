{!!
    widget('Modal')
    ->label('tr:users::profile.title')
!!}

@php $profile = userProfile($model) @endphp

<div class="modal-body user-profile">
    <div class="panel b widget">
        <div class="bg-info gradient half-float p-xl">
            <div class="header"></div>

            <div class="half-float-bottom">
                <img src="{{ $profile->getAvatar() }}" alt="Image" class="img-thumbnail img-circle">
            </div>
        </div>

        <div class="pt-sm user-title">
            <h4>
                {{ $profile->getNameString() }}
            </h4>

            @php $identifier = $profile->getIdentifier() @endphp
            @isset($identifier)
            <div class="text-muted">
                {!! $identifier !!}
            </div>
            @endisset
        </div>

        <div class="panel-body pt-xl">

            <div class="mv40">
                @foreach($rows as $row)
                    @include('users::profile.widgets.data-row',[
                         "title" => $row['title'] ,
                         "value" => $row['value'] ,
                    ])
                @endforeach
            </div>

        </div>

        {{--@php $extra_blades = $profile->getBlades() @endphp--}}
        {{--@foreach($extra_blades as $blade)--}}
            {{--@include($blade, ['model' => $model])--}}
        {{--@endforeach--}}

    </div>

</div>

<div class="modal-footer">
    {!!
    widget("button")
    	->condition($model->canEdit())
    	->target("masterModal(url('manage/mobile-salam/users/edit/$model->hashid'))")
    	->label(trans("manage::forms.button.edit_info"))
    !!}

    {!!
    widget("button")
    	->condition(user()->can("carts"))
    	->target(url("manage/carts?user=$model->hashid"))
    	->newWindow()
    	->label(trans("cart::orders.titles"))
    !!}
</div>
