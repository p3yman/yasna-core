{!!
widget("modal")
	->label(trans( $model->id? "manage::forms.button.edit_info" : "users::forms.create_new_person" ))
	->target("name:mobile-salam-member-save")
!!}

<div class="modal-body">

    {!! widget('hidden')->name('hashid')->value($model->hashid) !!}

    @include("mobile-salam::users.editor-inside")

</div>

<div class="modal-footer">
    @include("manage::forms.buttons-for-modal")
</div>
