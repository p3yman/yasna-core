{!!
widget("modal")
	->label("tr:mobile-salam::report.ware-report")
!!}

<div class="modal-body">
	@php

			@endphp
	@if($transactions)
		<div class="col-12">
			<p>{{trans_safe('mobile-salam::report.cost_repairer')}} : {{ad(number_format($transactions['repairer']))}}</p>
			<p>{{trans_safe('mobile-salam::report.buy_shop')}} : {{ad(number_format($transactions['credit']))}}</p>
		</div>

		<div class="col-12">
			<div class="panel">
				<div class="panel-body">
					@include('manage::widgets.charts.pie.chartjs-pie',[
					   'id' => 'chart-pie',
					   'type' => 'pie', // doughnut or pie
					   'labels'=> [trans_safe('mobile-salam::report.buy_shop'),trans_safe('mobile-salam::report.cost_repairer')],
					   'data' => [
						   [
							   'backgroundColor' => ['#f05050', '#7266ba','#fad732','#23b7e5', '#f532e5'],
							   'hoverBackgroundColor' => ['#f05050', '#7266ba','#fad732','#23b7e5', '#f532e5'],
							   'data' => [$transactions['credit'], $transactions['repairer']]
						   ]
					   ]

				   ])
				</div>
			</div>
		</div>
	@else
		<p>{{trans_safe('mobile-salam::report.no-ware-report')}}</p>
	@endif
</div>
