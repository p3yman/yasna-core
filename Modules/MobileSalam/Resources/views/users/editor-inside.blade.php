{!!
widget("text")
	->name("name_first")
	->required()
	->value($model->name_first)
	->inForm()
!!}

{!!
widget("text")
	->name("name_last")
	->required()
	->value($model->name_last)
	->inForm()
!!}

{!!
widget("combo")
	->name("gender")
	->required()
	->options([
	   ["id" => "1", "title" => trans('mobile-salam::registerForm.user-gender.men'),],
	   ["id" => "2", "title" => trans('mobile-salam::registerForm.user-gender.women'),],
	   ["id" => "3", "title" => trans('mobile-salam::registerForm.user-gender.other'),],
	])
    ->value($model->gender)
    ->inForm()
!!}

{!!
widget("text")
	->name("mobile")
	->required()
	->inForm()
	->value($model->mobile)
!!}

{!!
widget("text")
	->name("code_melli")
	->inForm()
	->value($model->code_melli)
!!}

{!!
widget("text")
	->name("email")
	->inForm()
	->value($model->email)
!!}

{!!
widget("combo")
	->name("city")
	->inForm()
	->value($model->city)
	->options(model("state")->combo())
	->blankValue()
!!}

{!!
widget("text")
	->name("home_address")
	->inForm()
	->value($model->home_address)
!!}

{!!
widget("text")
	->name("postal_code")
	->inForm()
	->value($model->postal_code)
!!}

{!!
widget("text")
	->name("tel")
	->inForm()
	->value($model->tel)
!!}

@include('manage::forms.note' , [
     'text' => trans("users::forms.default_password"),
     'condition' => !$model->id ,
])


