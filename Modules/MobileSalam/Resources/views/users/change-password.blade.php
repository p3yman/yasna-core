<!DOCTYPE html>
<html lang="fa">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<title>{{ get_setting('site_title') }}</title>

	<!-- Bootstrap css -->
{!! Html::style(Module::asset('yasna:libs/bootstrap/css/bootstrap.min.css')) !!}
{!! Html::style(Module::asset('yasna:libs/bootstrap/css/bootstrap.rtl.min.css')) !!}

<!-- Fonts -->
{!! Html::style(Module::asset('yasna:css/fontiran.css')) !!}
{!! Html::style(Module::asset('yasna:libs/fontawesome/css/font-awesome.min.css')) !!}


<!-- Custom styles -->
	{!! Html::style(Module::asset('yasna:css/login-style.min.css')) !!}


    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

<!-- Site Wrapper -->
<div class="wrapper">
	<section class="login">
		<div class="bg-particles" id="bg-particles">

		</div>
		<h1>
			<a href="#">
				{{ get_setting('site_title') }}
			</a>
		</h1>
		<div class="inner-container">
			<div class="login-box">
				<div class="login-box-container">
					<div class="login-tabs-container" style="max-width: 210px">
						<ul class="login-tabs js-tabs-control">
							<li class="login-tabs__tab center active" data-target="login">
								<a href="{{ v0() }}">{{trans_safe('mobile-salam::general.change-password')}}</a>
							</li>
						</ul>
						<div style="width: 100%;height: 3px;-webkit-transition: left 0.5s cubic-bezier(0.23, 1, 0.32, 1);transition: left 0.5s cubic-bezier(0.23, 1, 0.32, 1);background-color: #23b7e5;"></div>
					</div>
				</div>
				<div class="login-content">
					<form id="form_post">
						{{ csrf_field() }}
						<input type="hidden" class="english" id="hashid" name="hashid" value="{{$hashid}}">
						<!-- Password -->
						<div class="form-input">
							<input type="password" class="english" id="password" name="password" value=""
								   autocomplete="off" required>
							<label for="password"
								   class="form-input-label">{{trans_safe('mobile-salam::general.password')}}</label>
							<div class="form-input-underline"></div>
							<span class="password-toggle js-toggle-visibility" data-passinput="password">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </span>
						</div>
						<!-- Password2 -->
						<div class="form-input">
							<input type="password" class="english" id="password2" name="password2" value=""
								   autocomplete="off" required>
							<label for="password2"
								   class="form-input-label">{{trans_safe('mobile-salam::general.password2')}}</label>
							<div class="form-input-underline"></div>
							<span class="password-toggle js-toggle-visibility" data-passinput="password2">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                            </span>
						</div>
						<div class="form-submit">
							<button class="btn secondary"
									type="submit" {{--name="submit"--}}>{{trans_safe('mobile-salam::general.save')}}</button>
						</div>
					</form>
				</div>
				<div class="verify-feedback">
					<ul style="padding: 9px 25px 9px 0px;text-align: right;line-height: 25px;">

					</ul>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- jQuery -->
{!! Html::script(Module::asset('yasna:libs/jquery/jquery.min.js')) !!}

<!-- Bootstrap js -->
{!! Html::script(Module::asset('yasna:libs/bootstrap/js/bootstrap.min.js')) !!}

<!-- Particles.js -->
{!! Html::script(Module::asset('yasna:libs/particles/particles.min.js')) !!}
{!! Html::script(Module::asset('yasna:libs/particles/app.js')) !!}


<!-- Custom js -->
{!! Html::script(Module::asset('yasna:js/login-form.js')) !!}

<!-- !END End-body Scripts -->
<script>
    $(document).ready(function () {

        $('#form_post').on('submit', function (e) {
            let feedContainer = $('.verify-feedback');
            var password      = $('#password').val();
            var hashid        = $('#hashid').val();
            var password2     = $('#password2').val();
            feedContainer.find('ul').html('');
            feedContainer.removeClass("alert-danger");
            e.preventDefault();
            $.ajax({
                url    : "{{route('save-change-password')}}",
                data   : {
                    _token   : '{{csrf_token()}}',
                    password : password,
                    password2: password2,
                    hashid   : hashid,
                },
                type   : "POST",
                success: function (rs) {
                    if (rs.success) {
                        window.location.href = '/manage';
                    }
                },
                error  : function (jqXhr) {
                    if (jqXhr.status == 400) {

                        console.log(jqXhr.responseJSON);
                        let errors = jqXhr.responseJSON.errors;

                        $.each(errors, function (key, item_errors) {
                            $.each(item_errors, function (key, error) {
                                let li = $('<li />').html(error);
                                feedContainer.find('ul').append(li);
                            });
                        });
                        feedContainer.addClass("alert-danger");
                    }
                }
            });
        });

    }, 'button#continue');

</script>

</body>
</html>
