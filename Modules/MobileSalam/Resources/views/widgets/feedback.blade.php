@php
	$data = posttype('questions')->posts()->with('ratings')->get();
			foreach ($data as $item) {
				$count      = ad($item->ratings()->count('post_id'));
				$rate[]     = $item->ratings()->average('rate') ?? 0;
				$titles[]   = $item->title . "(" . $count . ")";
			}
@endphp

<div class="panel-body">
	<div class="mt">
		@include('manage::widgets.charts.bar.chartjs-bar',[
			 'id'=> 'chartjs-bar',
					'labels'=> $titles,
					 'data' => [
							[
								'backgroundColor' => '#23b7e5',
								'borderColor'     => '#23b7e5',
								'data'            => $rate
							],

					]
			])
	</div>

</div>
<div class="col-sm-12">
	<h5>{{trans("mobile-salam::widgets.num")}}</h5>
</div>
