@extends('mobile-salam::email.email_frame')
@section('email_content')
	@if ($message)
		<blockquote style="font-family: Tahoma, Helvetica, Arial;color: #6AA84F;">
			<p>{!! trans_safe('mobile-salam::general.email.contact-us.title') !!}</p>
		</blockquote>
		<blockquote style="font-family: Tahoma, Helvetica, Arial;">
			<p>{!! nl2br(trans_safe('mobile-salam::general.email.contact-us.text',['time'=>ad(echoDate($time, 'j F Y - H:i')),'user'=>$user])) !!}</p>
		</blockquote>
		<blockquote
				style="margin-right:62px;border-collapse:collapse;background-color:rgb(243, 243, 243);font-size:8pt;padding:15px 40px !important;border-right: 3px solid rgb(221,221,221);">
			<q>{!! $user_message !!}</q>
		</blockquote>
		<blockquote>
			<b style="direction: rtl;font-weight: bold;">{!! trans_safe('mobile-salam::general.email.contact-us.admin_reply') !!}</b>
			<br>
			<q>{!! $admin_message !!}</q>
		</blockquote>
	@endif
@endsection
