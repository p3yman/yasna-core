<div class="panel panel-default">
    <div class="panel-heading">
        {{ trans("mobile-salam::wallet.balance") }}
    </div>
    <div class="panel-body">

        @include("mobile-salam::members.wallet-cell")

    </div>
</div>
