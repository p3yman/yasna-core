@include("manage::widgets.grid" , [
    "models"            => $model->creditTransactions()->orderBy("created_at", "desc")->get(),
    "user"              => $model,
    'headings'          => [
        trans("mobile-salam::wallet.amount"),
        trans("mobile-salam::wallet.account"),
        trans("validation.attributes.description"),
    ],
    'row_view'          => "mobile-salam::members.row",
    'handle'            => "counter",
    'operation_heading' => false,
])
