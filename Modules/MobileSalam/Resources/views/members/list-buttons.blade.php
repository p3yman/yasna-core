@include("manage::forms.buttons-for-modal", [
    "save_label" => trans("mobile-salam::wallet.manual_change"),
    "save_shape" => "default",
    "save_type"  => "button",
    "save_link"  => "$('.-form,.-list').toggle()"
])

