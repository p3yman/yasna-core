@php
$text = number_format(abs($model->credit)) . SPACE . trans("mobile-salam::wallet.currency");
if($model->credit < 0) {
    $text .= SPACE . trans("mobile-salam::wallet.negative") ;
}
@endphp

@include("manage::widgets.grid-text" , [
	'text'  => $text,
	"link"  => "modal:manage/users/wallets/list/-hashid-",
     "color" => $model->credit < 0 ? "red" : null,
])
