{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

{!!
widget("text")
	->name("amount")
	->required()
	->numberFormat()
     ->inForm()
     ->addon(trans("mobile-salam::wallet.currency"))
!!}

{!!
widget("combo")
	->name("type")
	->required()
	->inForm()
	->options($combo)
!!}

{!!
widget("combo")
     ->options(model("account")->whereNotNull("published_at")->get())
     ->name("account")
     ->required()
     ->inForm()
     ->label(trans("payment::general.account.title.singular"))
     ->blankValue(0)
     ->valueField("hashid")
     ->blankCaption(null)
!!}

{!!
widget("text")
     ->name("title")
     ->inForm()
!!}

{!!
widget("textarea")
     ->name("description")
     ->inForm()
!!}
