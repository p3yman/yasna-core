{!!
widget("modal")
	->label($model->full_name . " / " . trans("mobile-salam::wallet.manage") )
	->target(route("mobile-salam-users-wallet-change"))
!!}

<div class="modal-body">
    @include("mobile-salam::members.balance")

    <div class="-list">
        @include("mobile-salam::members.list")
    </div>
    <div class="-form noDisplay">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ trans("mobile-salam::wallet.manual_change") }}
            </div>
            <div class="panel-body">
                @include("mobile-salam::members.form")
            </div>
        </div>
    </div>

</div>

<div class="modal-footer">

    <div class="-list">
        @include("mobile-salam::members.list-buttons")
    </div>
    <div class="-form noDisplay">
        @include("mobile-salam::members.form-buttons")
    </div>

</div>
