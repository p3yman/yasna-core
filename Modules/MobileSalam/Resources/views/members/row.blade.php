@include('manage::widgets.grid-row-handle')

<td>
    @include("manage::widgets.grid-text" , [
        "color" => $model->payable_amount > 0? "green" : "orange",
        "text" => number_format(abs($model->payable_amount)) . SPACE . trans("mobile-salam::wallet.currency"),
        "size" => 14,
        "icon" => $model->payable_amount > 0? "level-up": "level-down",
    ])


    @include("manage::widgets.grid-date" , [
        "date" => $model->created_at,
    ])
</td>


<td>

    @include("manage::widgets.grid-text" , [
        "color"     => $model->payable_amount > 0? "green" : "orange",
        "text"      => $model->payable_amount > 0?  trans("mobile-salam::wallet.increase") : trans("mobile-salam::wallet.decrease"),
        "size"      => 12,
        "condition" => $model->verified_amount == $model->payable_amount,
    ])

    @include("manage::widgets.grid-text" , [
        "color"     => "red",
        "text"      => trans("mobile-salam::wallet.unconfirmed"),
        "size"      => 12,
        "condition" => $model->verified_amount != $model->payable_amount,
    ])

    @include("manage::widgets.grid-tiny" , [
        "color" => $model->verified_amount > 0? "green" : "orange",
        "text" => $model->account? $model->account->title : "",
        "icon" => "bank",
    ])

</td>


<td>
    @include("manage::widgets.grid-text" , [
        "text"      => $model->title,
        "size"      => 10,
    ])
    @include("manage::widgets.grid-text" , [
        "text"      => $model->description,
        "size"      => 10,
    ])
</td>
