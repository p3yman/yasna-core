<?php
return [
     "commenting-plural"         => "Comments",
     "commenting-singular"       => "Comment",
     "page-plural"               => "Pages",
     "page-singular"             => "Page",
     "contact-commenting-title2" => "Send message",
     "repairing_operator"        => "Repair operator",
     "repairing_operators"       => "Repair operators",
     "repairer"                  => "Repairer",
     "repairers"                 => "Repairers",
     "supplier"                  => "Supplier",
     "suppliers"                 => "Suppliers",
     "poll_question"             => "Poll Question",
     "poll_questions"            => "Poll Questions",
     "original_contents"         => "Original Contents",
     "original_content"          => "Original Content",
];
