<?php

return [
     'user-gender' => [
          'men'   => 'Male',
          'women' => 'Female',
          'other' => 'Other',
     ],
];
