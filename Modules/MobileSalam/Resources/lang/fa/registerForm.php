<?php
return [
     'user-gender' => [
          'men'   => 'آقا',
          'women' => 'خانم',
          'other' => 'سایر',
     ],
];
