<?php

return [
     "title"         => "کیف پول",
     "currency"      => "تومان",
     "manage"        => "تراکنش‌های کیف پول",
     "manual_change" => "تغییر دستی",
     "increase"      => "شارژ حساب",
     "decrease"      => "کسر از حساب",
     "negative"      => "بدهکار",
     "amount"        => "مبلغ",
     "time"          => "زمان",
     "account"       => "حساب",
     "balance"       => "مانده حساب",
     "unconfirmed"   => "تأییدنشده",

     "repair_order_payment"  => "پرداخت صورتحساب تعمیر",
     "cart_payment"          => "پرداخت صورتحساب فروشگاه",
     "deduct_from_wallet"    => "کسر از کیف پول",
     "manual_repair_payment" => "پرداخت دستی هزینه تعمیر توسط اوپراتور",
     "repairer_wage"         => "پرداخت هزینه تعمیر",
     "repairer_wage_undo"    => "اصلاح سند پرداخت هزینه تعمیر",
     "repair_number_x"       => "سفارش تعمیر به شماره :x",
];
