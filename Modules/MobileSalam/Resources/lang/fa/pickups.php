<?php
return [
     "list"                            => "فهرست",
     "id"                              => "شناسه",
     "cart_id"                         => "شناسه سبد خرید",
     "cart_code"                       => "کد پیگیری سبد خرید",
     "table_title"                     => "ارسال‌ها",
     "pickup_method"                   => "روش",
     "pickup_type"                     => "نوع",
     "origin_lat_lng"                  => "طول و عرض جغرافیایی مبدأ",
     "origin_latitude"                 => "طول جغرافیایی مبدأ",
     "origin_longitude"                => "عرض جغرفیایی مبدأ",
     "destination_latitude"            => "طول جغرافیایی مقصد",
     "destination_longitude"           => "عرض جغرافیایی مقصد",
     "origin_address"                  => "آدرس مبدأ",
     "destination_lat_lng"             => "طول و عرض جغرافیایی مقصد",
     "destination_address"             => "آدرس مقصد",
     "driver_full_name"                => "نام راننده",
     "driver_phone_number"             => "تلفن راننده",
     "tracking_url"                    => "آدرس پیگیری",
     "note"                            => "یادداشت",
     "alopeyk"                         => "الوپیک",
     "owner"                           => "مشتری",
     "custom"                          => "اختصاصی",
     "send"                            => "ارسال",
     "receive"                         => "دریافت",
     "new_modal_title"                 => "ارسال جدید",
     "single_modal_title"              => "جزئیات ارسال",
     "price"                           => "هزینه ارسال",
     "payment_method"                  => "روش پرداخت",
     "pay_by_cash"                     => "نقدی",
     "pay_by_credit"                   => "اعتباری",
     "pickup_time"                     => "زمان مراجعه مشتری",
     "pickup_time_tooltip"             => "می بایست به صورت unix timestamp وارد گردد.",
     "pickup_price_estimation_message" => "مبلغ تخمینی ارسال توسط الوپیک: ",
     "pickup_price_estimation_only"    => "فقط برآورد هزینه ارسال",
     "yes"                             => "بله",
     "no"                              => "خیر",
     "origin_latitude.invalid"         => "عرض جغرافیایی درست نیست",
     "origin_longitude.invalid"        => "طول جغرافیایی درست نیست",
     "pickup_price"                    => "قیمت پیک",
     "pickup"                          => "ارسال‌ها",
];
