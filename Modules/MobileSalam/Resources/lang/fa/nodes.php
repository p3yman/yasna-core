<?php
return [
     "SubmittedNewOrderNode"  => "ثبت درخواست تعمیر",
     "AssignedRepairerNode"   => "انتصاب تعمیرکار",
     "ChangedEstimationNode"  => "تغییر تخمین‌ها",
     "ChangedStatusNode"      => "تغییر وضعیت",
     "CommentedNode"          => "ثبت توضیحات",
     "DefinedDefectsNode"     => "ثبت مشکلات",
     "RepairedNode"           => "تکمیل تعمیر دستگاه",
     "SubmittedNewIndentNode" => "ثبت درخواست قطعه",
     "ClaimedNode"            => "ثبت اعتراض مشتری",
     "SavedSurveyNode"        => "ثبت نظرسنجی",
     "SavedPickupNode"        => "ثبت ارسال",
];
