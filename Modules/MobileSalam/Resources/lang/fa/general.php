<?php
return [
     "contact-us"                        => "تماس با ما",
     "email"                             => [
          'contact-us' => [
               'subject'     => 'یسناوب',
               'title'       => 'پاسخی به پیام شما ارسال شد.',
               'text'        => 'پاسخ زیر در تاریخ «:time» توسط «:user» ارسال شد.',
               'admin_reply' => 'پاسخ ادمین:',
          ],
     ],
     'email_has_been_sent_automatically' => 'توجه : این ایمیل به صورت خودکار فرستاده شده است ، بنابراین به آن پاسخ ندهید. ',

     "about"                        => "درباره ما",
     "about-us-footer"              => "درباره ما فوتر",
     "purchasing-instructions"      => "راهنمای خرید",
     "repair-ordering-instructions" => "راهنمای درخواست تعمیر",
     "purchase-guarantee-procedure" => "رویه گارانتی قطعات",
     "repair-guarantee-procedure"   => "رویه گارانتی تعمیرات",
     "terms-conditions"             => "قوانین و مقررات",
     "privacy"                      => "سیاست حفظ حریم شخصی",
     "repair-tariffs"               => "تعرفه تعمیرات اتحادیه",
     "product-return"               => "رویه بازگرداندن کالا",
     "original-parts"               => "قطعات اصلی",
     "original-parts-text"          => "مشکل خود را به ما بگویید تا هزینه و زمان تعمیر را اعلام کنیم.",
     "fair-price"                   => "قیمت منصفانه",
     "professional-repairers"       => "تعمیرکاران حرفه‌ای",
     "declare-repair-time"          => "اعلام زمان تعمیر",
     "video-title"                  => "موبایل سلام چه جوری دستگاه شما رو تعمیر می‌کنه؟",
     "video-title-text"             => "ما همیشه نهایت سعی‌مون رو می‌کنیم تا موبایل یا تبلت شما رو نجات بدیم.",
     "password"                     => "رمز عبور",
     "password2"                    => "تکرار رمز عبور",
     "save"                         => "ذخیره",
     "change-password"              => "تغییر رمز عبور",


];
