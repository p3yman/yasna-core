<?php
return [
     "title"             => "محاسبات دستمزد",
     "tag_amounts"       => "دسته‌بندی",
     "post_amounts"      => "کالا",
     "wage_repair"       => "اجرت تعمیر",
     "wage_replace"      => "اجرت تعویض",
     "penalty"           => "جریمه آسیب",
     "tag_wage_repair"   => "اجرت تعمیر دسته‌بندی",
     "tag_wage_replace"  => "اجرت تعویض دسته‌بندی",
     "tag_penalty"       => "جریمه آسیب دسته‌بندی",
     "post_wage_repair"  => "اجرت تعمیر کالا",
     "post_wage_replace" => "اجرت تعویض کالا",
     "post_penalty"      => "جریمه آسیب کالا",
     "percent"           => "درصد",
     "absolute"          => "تومان",

     "tag_change_notice" => "تغییر در مقادیر دسته‌بندی، روی تمام کالاهای دارای این دسته‌بندی تأثیر خواهد گذاشت.",
     "no_tag"            => "این محصول دسته‌بندی ندارد!",
];
