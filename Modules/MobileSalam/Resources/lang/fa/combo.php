<?php

return [
     'application-type' => [
          "in-person"   => "حضوری",
          "by-user"     => "ورود سیستمی توسط کاربر",
          "by-operator" => "ورود سیستمی توسط اوپراتور",
     ],
     'supply-status'    => [
          "available"     => "قابل تأمین",
          "not-available" => "غیر قابل تأمین",
          "follow-up"     => "تأمین با تأخیر",
     ],
     'return-reason'    => [
          "used"           => "استفاده شده",
          "unused"         => "بازگشت به دلیل عدم استفاده‫",
          "factory-fault"  => "بازگشت به دلیل خرابی کارخانه‌ای‫",
          "repairer-fault" => "بازگشت به دلیل آسیب هنگام تعمیر",
     ],

     'full-indent-states' => [
          "pending"        => "منتظر بررسی تأمین‌کننده",
          "supplied"       => "تأمین‌شده",
          "not-available"  => "غیر قابل تأمین",
          "follow-up"      => "تأمین با تأخیر",
          "used"           => "استفاده شده",
          "unused"         => "بازگشت به دلیل عدم استفاده‫",
          "factory-fault"  => "بازگشت به دلیل خرابی کارخانه‌ای‫",
          "repairer-fault" => "بازگشت به دلیل آسیب هنگام تعمیر",
     ],

     "defect-type" => [
          "replace" => "تعویض",
          "repair"  => "تعمیر",
     ],
];
