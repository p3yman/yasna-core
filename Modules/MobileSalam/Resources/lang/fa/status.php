<?php
return [
     "pending"            => "منتظر بررسی اولیه",
     "waiting_for_device" => "منتظر دریافت دستگاه",
     "processing"         => "در حال بررسی",
     "repaired"           => "تعمیر شده",
     "rejected"           => "غیر قابل تعمیر",
     "cancelled"          => "لغو شده",
     "stored"             => "انتقال به انبار پس از تعمیر",
     "unknown"            => "نامشخص",
];
