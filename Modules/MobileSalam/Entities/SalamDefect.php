<?php

namespace Modules\MobileSalam\Entities;

use Modules\MobileSalam\Entities\Traits\DefectResourceTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\SalamOrder;

class SalamDefect extends YasnaModel
{
    use SoftDeletes;
    use DefectResourceTrait;



    /**
     * convert customer problems into semi-standard defects.
     *
     * @param int   $order_id
     * @param array $problems
     *
     * @return void
     */
    public static function convertCustomerProblems(int $order_id, array $problems)
    {
        foreach ($problems as $problem) {
            model('salam-defect')->batchSave([
                 "order_id" => $order_id,
                 "title"    => model('salam-estimation', $problem)->title,
                 "type"     => "", // <~~ left empty to make sure operator has to enter this.
            ]);
        }
    }



    /**
     * get the order relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(SalamOrder::class);
    }



    /**
     * get the type label attribute
     *
     * @return string
     */
    public function getTypeLabelAttribute()
    {
        return trans("mobile-salam::combo.defect-type.$this->type");
    }
}
