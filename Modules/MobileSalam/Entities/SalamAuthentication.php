<?php

namespace Modules\MobileSalam\Entities;

use Modules\MobileSalam\Entities\Traits\AuthAutomaticOperationTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalamAuthentication extends YasnaModel
{
    use SoftDeletes;
    use AuthAutomaticOperationTrait;
}
