<?php

namespace Modules\MobileSalam\Entities;

use App\Models\Post;
use App\Models\User;
use App\Models\Ware;
use App\Models\SalamDefect;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\MobileSalam\Entities\Traits\IndentElectorTrait;
use Modules\MobileSalam\Entities\Traits\IndentResourceTrait;
use Modules\MobileSalam\Entities\Traits\IndentStatusTrait;
use Modules\MobileSalam\Entities\Traits\IndentWagesTrait;
use Modules\Uploader\Services\Entity\FilesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalamIndent extends YasnaModel
{
    use IndentStatusTrait;
    use IndentElectorTrait;
    use IndentResourceTrait;
    use IndentWagesTrait;
    use SoftDeletes;
    use FilesTrait;



    /**
     * get the relationship instance of the related ware record
     *
     * @return BelongsTo
     */
    public function ware()
    {
        return $this->belongsTo(Ware::class, "ware_id");
    }



    /**
     * get the relationship instance of the related order record
     *
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(SalamOrder::class, "order_id");
    }



    /**
     * get the relationship instance of the related supplier record
     *
     * @return BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo(User::class, "supplier_id");
    }



    /**
     * get the relationship instance of the related repairer record
     *
     * @return BelongsTo
     */
    public function repairer()
    {
        return $this->belongsTo(User::class, "repairer_id");
    }



    /**
     * get the relationship instance of the related salam-defect record
     *
     * @return BelongsTo
     */
    public function defect()
    {
        return $this->belongsTo(SalamDefect::class, "defect_id");
    }



    /**
     * get the defect time
     *
     * @return string|null
     */
    public function getDefectTypeAttribute()
    {
        $defect = $this->defect;

        return $defect ? $defect->type : null;
    }



    /**
     * get the post instance of the related ware record
     *
     * @return Post
     */
    public function getPostAttribute()
    {
        /** @var Ware $ware */
        $ware = $this->ware;

        return $ware ? $ware->postIn('fa') : post();
    }

}
