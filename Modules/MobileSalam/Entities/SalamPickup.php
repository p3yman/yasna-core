<?php

namespace Modules\MobileSalam\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MobileSalam\Entities\Traits\PickupResourceTrait;
use Modules\Yasna\Services\YasnaModel;

/**
 * @property SalamOrder $order
 * @property int        $order_id
 */
class SalamPickup extends YasnaModel
{
    use PickupResourceTrait;
    use SoftDeletes;

    /**
     * mutate extra time related columns
     *
     * @var array
     */
    protected $dates = [
         'pickup_time',
    ];



    /**
     * get the relationship instance of the related order
     *
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(model('salam-order'), "order_id");
    }



    /**
     * get the relationship instance of the related cart
     *
     * @return BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo(model('cart'), "order_id");
    }



    /**
     * get the relationship instance of the related origin division
     *
     * @return BelongsTo
     */
    public function originCity()
    {
        return $this->belongsTo(model('division'), "origin_city_id")->where('type', 'city');
    }



    /**
     * get the relationship instance of the related destination division
     *
     * @return BelongsTo
     */
    public function destinationCity()
    {
        return $this->belongsTo(model('division'), "destination_city_id")->where('type', 'city');
    }



    /**
     * get cashed fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "is_cashed",
        ];
    }
}
