<?php

namespace Modules\MobileSalam\Entities;

use App\Models\SalamOrder;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalamNode extends YasnaModel
{
    use SoftDeletes;



    /**
     * insert a new record
     *
     * @param array $data
     *
     * @return \App\Models\SalamNode
     */
    public static function newRecord(array $data)
    {
        /** @var \App\Models\SalamNode $return */
        $return = model("salam-node")->batchSave($data);

        return $return;
    }



    /**
     * get the order relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(SalamOrder::class);
    }



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "data",
        ];
    }



    /**
     * get data array from the meta.
     *
     * @return array
     */
    public function getDataArray()
    {
        return (array)$this->getMeta("data");
    }



    /**
     * get data value of the specific key.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getData(string $key)
    {
        $data = $this->getDataArray();

        return isset($data[$key]) ? $data[$key] : null;
    }
}
