<?php

namespace Modules\MobileSalam\Entities;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\MobileSalam\Entities\Traits\OrderAutomaticOperationsTrait;
use Modules\MobileSalam\Entities\Traits\OrderDefectsTrait;
use Modules\MobileSalam\Entities\Traits\OrderElectorTrait;
use Modules\MobileSalam\Entities\Traits\OrderNodesTrait;
use Modules\MobileSalam\Entities\Traits\OrderPaymentsTrait;
use Modules\MobileSalam\Entities\Traits\OrderRatingsTrait;
use Modules\MobileSalam\Entities\Traits\OrderRepairerTrait;
use Modules\MobileSalam\Entities\Traits\OrderResourceTrait;
use Modules\MobileSalam\Entities\Traits\OrderStatusTrait;
use Modules\MobileSalam\Entities\Traits\OrderWagesTrait;
use Modules\MobileSalam\Http\TimelineNodes\ChangedEstimationNode;
use Modules\Yasna\Services\YasnaModel;

/**
 * @property User $user
 */
class SalamOrder extends YasnaModel
{
    use OrderStatusTrait;
    use OrderElectorTrait;
    use OrderResourceTrait;
    use OrderNodesTrait;
    use OrderRepairerTrait;
    use OrderDefectsTrait;
    use OrderAutomaticOperationsTrait;
    use OrderPaymentsTrait;
    use OrderRatingsTrait;
    use OrderWagesTrait;
    use SoftDeletes;



    /**
     * get random unique tracking number
     *
     * @return string
     */
    public static function getRandomTracking()
    {
        return strval(rand(1, 9)) . substr(strval(time()), -8) . strval(rand(0, 9));

    }



    /**
     * get the relationship instance of the related customer record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(User::class, "customer_id");
    }



    /**
     * get the tracking URL.
     *
     * @return string
     */
    public function getTrackingUrlAttribute()
    {
        return get_setting("site_url") . "/repair/track/" . $this->tracking_number;
    }



    /**
     * get main meta fields
     *
     * @return array
     */
    public function getMainMetaFields()
    {
        return [
             "problems",
             "operator_feedback",
             "rejected_reason",
        ];
    }



    /**
     * change estimations
     *
     * @param float  $new_price_estimation
     * @param int    $new_time_estimation
     * @param string $comment
     *
     * @return bool
     */
    public function changeEstimations($new_price_estimation, $new_time_estimation, $comment = null)
    {
        $changes = [];
        if ($new_price_estimation and $new_price_estimation != $this->estimated_price) {
            $changes ["estimated_price"] = $new_price_estimation;
        }

        if ($new_time_estimation and $new_time_estimation != $this->estimated_time) {
            $changes ["estimated_time"] = $new_time_estimation;
            $changes ["due_at"]         = $this->calculateDueDate($new_time_estimation);
        }

        if ($comment) {
            $changes['operator_feedback'] = $comment;
        }

        if (!$changes) {
            return false;
        }

        $return = $this->batchSaveBoolean($changes);

        if ($return) {
            $this->refresh();
            ChangedEstimationNode::insert($this, [
                 "estimated_price" => $this->estimated_price,
                 "estimated_time"  => $this->estimated_time,
                 "comment"         => $comment,
            ]);
        }

        return $return;
    }



    /**
     * get the relationship instance of the related salam-order record
     *
     * @return HasMany
     */
    public function indents()
    {
        return $this->hasMany(SalamIndent::Class, 'order_id');
    }



    /**
     * calculate due date
     *
     * @param int $time_estimation
     *
     * @return string Carbon
     */
    private function calculateDueDate($time_estimation)
    {
        $date = now()->addHour($time_estimation);

        return $date;
    }



    /**
     * get the relationship instance of the related post record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class, "post_id");
    }


}
