<?php

namespace Modules\MobileSalam\Entities;

use App\Models\Post;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Services\YasnaModel;

/**
 * @property Post       $post
 * @property SalamOrder $order
 * @property int        $order_id
 * @property int        $rate
 */
class SalamRating extends YasnaModel
{
    use SoftDeletes;



    /**
     * get the relationship instance of the related post
     *
     * @return BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class, "post_id");
    }



    /**
     * get the relationship instance of the related order
     *
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(SalamOrder::class, "order_id");
    }



    /**
     * get Post resource.
     *
     * @return array
     */
    protected function getPostResource()
    {
        $post = $this->post;

        if (!$post) {
            return null;
        }

        return $post->toResource(["title","question"]);
    }



    /**
     * get Order resource.
     *
     * @return array
     */
    protected function getOrderResource()
    {
        return [
             "id" => $this->order_id ? hashid($this->order_id) : null,
        ];
    }

    /**
     * get Rate resource.
     *
     * @return int
     */
    protected function getRateResource()
    {
        return $this->rate;
    }
}
