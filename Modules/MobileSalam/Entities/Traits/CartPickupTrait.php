<?php

namespace Modules\MobileSalam\Entities\Traits;

use AloPeyk\Api\RESTful\Model\Address;
use AloPeyk\Api\RESTful\Model\Order;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\MobileSalam\Entities\SalamPickup;
use Modules\MobileSalam\Http\Requests\SaveCartPickupRequest;

trait CartPickupTrait
{
    /**
     * get the relationship instance of the related salam-pickup records
     *
     * @return HasMany
     */
    public function pickups()
    {
        return $this->hasMany(SalamPickup::class, 'order_id')->where('parent_entity', 'cart');
    }



    /**
     * save repair order pickup
     *
     * @param SaveCartPickupRequest $request
     *
     * @return array
     */
    public function saveRepairOrderPickup($request)
    {
        /** @var \App\Models\Cart $cart */
        $cart          = $this;
        $saved         = null;
        $parent_entity = $request->parent_entity;
        $pickup_method = $request->pickup_method;
        $pickup_type   = $request->pickup_type;
        $just_estimate = $request->just_estimate;


        if ($just_estimate == 1) {
            $result = self::estimateAlopeykOrder($request);
            return $result;
        }

        $saved = model("salam-pickup")->batchSaveId([
             "parent_entity"       => $parent_entity,
             "order_id"            => $this->id,
             "pickup_method"       => $pickup_method,
             "pickup_type"         => $pickup_type,
             "pickup_price"        => $request->pickup_price,
             "pickup_time"         => $request->pickup_time,
             "origin_city_id"      => $request->origin_city_id,
             "origin_lat"          => $request->origin_latitude,
             "origin_lng"          => $request->origin_longitude,
             "origin_addr"         => $request->origin_address,
             "destination_city_id" => $request->destination_city_id,
             "destination_lat"     => $request->destination_latitude,
             "destination_lng"     => $request->destination_longitude,
             "destination_addr"    => $request->destination_address,
             "driver_full_name"    => $request->driver_full_name,
             "driver_phone_number" => $request->driver_phone_number,
             "pickup_note"         => $request->note,
        ]);

        if ($saved) {

            if ($pickup_method == "alopeyk") {
                self::createAlopeykOrder($saved, $request);
            }

        }

        return [];
    }



    /**
     * estimate alopeyk order
     *
     * @param SaveCartPickupRequest $request
     *
     * @return array
     */
    public function estimateAlopeykOrder($request)
    {
        /*
         * Create Origin
         */
        $origin = new Address(
             'origin',
             $request->origin_city_title_en,
             $request->origin_latitude,
             $request->origin_longitude
        );
        $origin->setAddress($request->origin_address);

        /*
         * Create Destination
         */
        $destination = new Address(
             'destination',
             $request->destination_city_title_en,
             $request->destination_latitude,
             $request->destination_longitude
        );
        $destination->setAddress($request->destination_address);

        $alopeyk_order = new Order('motorbike', $origin, [$destination]);
        $alopeyk_order->setHasReturn(false);
        if ($request->is_cashed == 1) {
            $alopeyk_order->setCashed(true);
        }

        $api_response = $alopeyk_order->getPrice();

        if ($api_response->status == "success") {
            return [
                 "price" => $api_response->object->price,
            ];
        }

        return [];
    }



    /**
     * create alopeyk order
     *
     * @param integer               $pickup
     * @param SaveCartPickupRequest $request
     *
     * @return bool
     */
    public function createAlopeykOrder($pickup, $request)
    {
        /*
         * Create Origin
         */
        $origin = new Address(
             'origin',
             $request->origin_city_title_en,
             $request->origin_latitude,
             $request->origin_longitude
        );
        $origin->setAddress($request->origin_address);

        /*
         * Create Destination
         */
        $destination = new Address(
             'destination',
             $request->destination_city_title_en,
             $request->destination_latitude,
             $request->destination_longitude
        );
        $destination->setAddress($request->destination_address);

        $alopeyk_order = new Order('motorbike', $origin, [$destination]);
        $alopeyk_order->setHasReturn(false);
        if ($request->is_cashed == 1) {
            $alopeyk_order->setCashed(true);
        }

        $api_response = $alopeyk_order->create();
        if ($api_response->status == "success") {
            $savedPickup                        = model('salam-pickup')->find($pickup);
            $savedPickup->delivery_tracking_url = "https://tracking.alopeyk.com/#/" . $api_response->object->order_token;
            $savedPickup->save();
        }

        return true;
    }
}
