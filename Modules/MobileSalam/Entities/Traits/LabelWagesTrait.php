<?php

namespace Modules\MobileSalam\Entities\Traits;

trait LabelWagesTrait
{
    /**
     * get meta fields related to the repairer wages.
     *
     * @return array
     */
    public function getWagesMetaFields()
    {
        return [
             "wage_repair",
             "wage_replace",
             "penalty",
             "wage_repair_type",
             "wage_replace_type",
             "penalty_type",
        ];
    }
}
