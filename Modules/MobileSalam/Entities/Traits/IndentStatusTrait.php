<?php


namespace Modules\MobileSalam\Entities\Traits;

/**
 * @property string $supplied_at            ;
 * @property string $used_at                ;
 * @property string $returned_at            ;
 * @property string $return_confirmed_at    ;
 * @property string $return_rejected_at     ;
 * @property string $supply_status          ;
 * @property string $return_reason          ;
 */
trait IndentStatusTrait
{
    /**
     * get return reason
     *
     * @return array
     */
    public static function availableReturnReasons()
    {
        return [
             "used",
             "unused",
             "factory-fault",
             "repairer-fault",
        ];
    }



    /**
     * get the supply ware status code
     *
     * @return string
     */
    public function supplyWareStatus()
    {
        if ($this->isAvailable()) {
            return "available";
        }

        if ($this->isNotAvailable()) {
            return "not-available";
        }

        if ($this->isPending()) {
            return "follow-up";
        }

        return "unknown";
    }



    /**
     * get status label
     *
     * @return string
     */
    public function getSupplyStatusLabelAttribute()
    {
        if ($this->supplyWareStatus() == "unknown") {
            return trans("mobile-salam::status.unknown");
        }

        return trans("mobile-salam::combo.supply-status." . $this->supplyWareStatus());
    }



    /**
     * indicate if the indent is available.
     *
     * @return bool
     */
    public function isAvailable()
    {
        return $this->supply_status == 'supply-status-available' and !boolval($this->returned_at);
    }



    /**
     * indicate if the indent is not available.
     *
     * @return bool
     */
    public function isNotAvailable()
    {
        return $this->supply_status == 'supply-status-not-available' and !boolval($this->returned_at);
    }



    /**
     * indicate if the indent is still pending.
     *
     * @return bool
     */
    public function isPending()
    {
        return $this->supply_status == 'supply-status-follow-up' and !boolval($this->returned_at);
    }



    /**
     * get the return reason code
     *
     * @return string
     */
    public function getWareReturnReasonAttribute()
    {
        if ($this->isUsed()) {
            return "used";
        }

        if ($this->isUnused()) {
            return "unused";
        }

        if ($this->factoryFault()) {
            return "factory-fault";
        }

        if ($this->repairFault()) {
            return "repairer-fault";
        }

        return "unknown";
    }



    /**
     * get return reason label
     *
     * @return string
     */
    public function getReturnReasonLabelAttribute()
    {
        if ($this->getWareReturnReasonAttribute() == "unknown") {
            return trans("mobile-salam::status.unknown");
        }

        return trans("mobile-salam::combo.return-reason." . $this->getWareReturnReasonAttribute());
    }



    /**
     * indicate if the indent is used.
     *
     * @return bool
     */
    public function isUsed()
    {
        return $this->return_reason == "return-reason-used" and boolval($this->used_at);
    }



    /**
     * indicate if the indent is unused.
     *
     * @return bool
     */
    public function isUnused()
    {
        return $this->return_reason == "return-reason-unused";
    }



    /**
     * Indicates that the factory failure has been rejected.
     *
     * @return bool
     */
    public function factoryFault()
    {
        return $this->return_reason == "return-reason-factory-fault";
    }



    /**
     * indicate if the indent was returned due to a repair failure.
     *
     * @return bool
     */
    public function repairFault()
    {
        return $this->return_reason == "return-reason-repairer-fault";
    }

}
