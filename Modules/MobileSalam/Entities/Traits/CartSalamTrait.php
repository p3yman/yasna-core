<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\User;

/**
 * @property User $user
 */
trait CartSalamTrait
{
    /**
     * perform payment from the customer wallet. Amount could be a part of the total payable amount.
     *
     * @param float $amount
     *
     * @return int
     */
    public function payFromWallet($amount)
    {
        $user = $this->user;
        if (!$user) {
            return 0;
        }

        $track1 = $user->changeCredit(-$amount, 0, [
             "title" => trans("mobile-salam::wallet.deduct_from_wallet"),
        ]);

        if (!$track1) {
            return 0;
        }

        $track2 = payment()
             ->transaction()
             ->invoiceModel("Cart")
             ->invoiceId($this->id)
             ->accountId(0)
             ->title(trans("mobile-salam::wallet.cart_payment"))
             ->callbackUrl(config("mobile-salam.credit_callback"))
             ->payableAmount($amount)
             ->userId($user->id)
             ->getTrackingNumber()
        ;

        if (!is_numeric($track2)) {
            return 0;
        }

        payment()->transaction()->trackingNumber($track2)->makeVerified();
        $this->refreshSalamPayments();

        return $track2;

    }



    /**
     * refresh payments, regarding MobileSalam only.
     *
     * @return bool
     */
    public function refreshSalamPayments()
    {
        $paid = $this->transactions()->sum("verified_amount");

        return $this->update([
             "paid_amount" => $paid,
        ]);
    }
}
