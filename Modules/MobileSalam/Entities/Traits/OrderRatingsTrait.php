<?php

namespace Modules\MobileSalam\Entities\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\SalamRating;
use Modules\MobileSalam\Http\TimelineNodes\SavedSurveyNode;

/**
 * @property Collection $ratings
 */
trait OrderRatingsTrait
{
    /**
     *  get the relationship instance of posts ratings
     *
     * @return HasMany
     */
    public function ratings()
    {
        return $this->hasMany(SalamRating::class, "order_id");
    }



    /**
     * save repair order survey rate
     *
     * @param array  $posts
     * @param string $note
     *
     * @return bool
     */
    public function saveRepairOrderRate($posts, $note = null)
    {
        /** @var \App\Models\SalamOrder $order */
        $order = $this;
        $saved = 0;

        foreach ($posts as $item) {

            $saved += model("salam-rating")->batchSaveBoolean([
                 "order_id" => $this->id,
                 'post_id'  => $item['post'],
                 'rate'     => $item['rate'],
            ]);

        }

        if ($saved) {
            SavedSurveyNode::insert($order, [
                 "posts" => $this->ratings(),
                 "note"  => $note,
            ]);
        }


        return boolval($saved);
    }



    /**
     * check if the model can accept ratings
     *
     * @return bool
     */
    public function canSurvey()
    {
        return $this->isRepaired() and !$this->ratings()->exists();
    }



    /**
     * get CanSurvey resource.
     *
     * @return bool
     */
    protected function getCanSurveyResource()
    {
        return $this->canSurvey();
    }



    /**
     * get Ratings resource.
     *
     * @return array
     */
    protected function getRatingsResource()
    {
        return $this->getResourceFromCollection($this->ratings);
    }
}
