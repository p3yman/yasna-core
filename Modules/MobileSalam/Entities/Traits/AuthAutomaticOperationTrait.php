<?php


namespace Modules\MobileSalam\Entities\Traits;


use App\Models\SalamAuthentication;
use Illuminate\Database\Eloquent\Collection;

trait AuthAutomaticOperationTrait
{
    /**
     * destroy the expired tokens
     *
     * @return int
     */
    public static function cleanExpiredTokens()
    {
        $deleted = 0;
        /**@var SalamAuthentication $expiredToken */
        foreach (static::getExpiredTokens() as $expiredToken) {
            $deleted += $expiredToken->hardDelete();
        }
        return $deleted;
    }



    /**
     * get a collection of the expired tokens
     *
     * @return Collection
     */
    private static function getExpiredTokens()
    {
        return model('salam-authentication')
             ->where('reset_token_expires_at', '<', now()->subHour())
             ->get()
             ;
    }
}
