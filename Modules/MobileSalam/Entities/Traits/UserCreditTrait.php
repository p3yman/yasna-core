<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class User
 *
 * @property float $credit
 */
trait UserCreditTrait
{
    /**
     * get the credit of the user
     *
     * @return float
     */
    public function getCredit()
    {
        return (float)$this->credit;
    }



    /**
     * change the credit of the user and get the corresponding tracking number
     *
     * @param float $amount
     * @param int   $account_id
     * @param array $options
     *
     * @return int
     */
    public function changeCredit($amount, int $account_id, array $options = []): int
    {
        if (!$this->exists) {
            return 0;
        }

        $options = array_normalize($options, [
             "invoice_id"  => 0,
             "title"       => "",
             "description" => "",
        ]);

        $track = payment()
             ->transaction()
             ->invoiceModel("User")
             ->accountId($account_id)
             ->title($options['title'])
             ->description($options['description'])
             ->invoiceId($options['invoice_id'])
             ->callbackUrl(config("mobile-salam.credit_callback"))
             ->payableAmount($amount)
             ->userId($this->id)
             ->getTrackingNumber()
        ;

        if (!is_numeric($track)) {
            return 0;
        }

        payment()->transaction()->trackingNumber($track)->makeVerified();

        $this->refreshCredit();

        return $track;
    }



    /**
     * get the vary basic user token claims
     *
     * @return array
     */
    public function getJWTCustomClaimsMobileSalam()
    {
        return [
             "credit"                => $this->getCredit(),
             "password_force_change" => $this->password_force_change,
        ];
    }



    /**
     * get the Builder of credit transactions
     *
     * @return HasMany
     */
    public function creditTransactions()
    {
        return $this->hasMany(Transaction::class)->where("invoice_model", "User");
    }



    /**
     * refresh user credit according to the current transactions
     *
     * @return bool
     */
    public function refreshCredit()
    {
        $sum = $this->creditTransactions()->sum("verified_amount");

        return $this->update([
             "credit" => $sum,
        ]);
    }



    /**
     * get meta fields related to the geo coordination
     *
     * @return array
     */
    public function coordinationMetaFields()
    {
        return [
             "longitude",
             "latitude",
        ];
    }
}
