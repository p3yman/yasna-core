<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\Division;
use App\Models\SalamOrder;

trait PickupResourceTrait
{
    /**
     * boot PickupResourceTrait
     *
     * @return void
     */
    public static function bootPickupResourceTrait()
    {
        static::addDirectResources([
             "pickup_method",
             "pickup_type",
             "origin_latitude",
             "origin_longitude",
             "origin_address",
             "driver_full_name",
             "driver_phone_number",
             "delivery_tracking_url",
             "pickup_price",
             "pickup_note",
             "pickup_time",
             "is_cashed",
        ]);
    }



    /**
     * get Order resource.
     *
     * @return array
     */
    protected function getOrderResource()
    {
        /** @var SalamOrder $order */
        $order = $this->order;

        return $order ? $order->toResource() : null;
    }



    /**
     * get OriginCity resource.
     *
     * @return string|null
     */
    protected function getOriginCityResource()
    {
        /** @var Division $city */
        $city = $this->originCity()->first();

        return $city ? $city->toResource() : null;
    }



    /**
     * get DestinationCity resource.
     *
     * @return string|null
     */
    protected function getDestinationCityResource()
    {
        /** @var Division $city */
        $city = $this->destinationCity()->first();

        return $city ? $city->toResource() : null;
    }



    /**
     * get DestinationLatitude resource.
     *
     * @return string
     */
    protected function getDestinationLatitudeResource()
    {
        return $this->getAttribute('destination_lat');
    }



    /**
     * get DestinationLongitude resource.
     *
     * @return string
     */
    protected function getDestinationLongitudeResource()
    {
        return $this->getAttribute('destination_lng');
    }



    /**
     * get DestinationAddress resource.
     *
     * @return string
     */
    protected function getDestinationAddressResource()
    {
        return $this->getAttribute('destination_addr');
    }



    /**
     * get OriginLatitude resource.
     *
     * @return string
     */
    protected function getOriginLatitudeResource()
    {
        return $this->getAttribute('origin_lat');
    }



    /**
     * get OriginLongitude resource.
     *
     * @return string
     */
    protected function getOriginLongitudeResource()
    {
        return $this->getAttribute('origin_lng');
    }



    /**
     * get OriginAddress resource.
     *
     * @return string
     */
    protected function getOriginAddressResource()
    {
        return $this->getAttribute('origin_addr');
    }



    /**
     * get pickup_time resource.
     *
     * @return string
     */
    protected function getPickupTimeResource()
    {
        return $this->getAttribute('pickup_time') ? carbon()::parse($this->getAttribute('pickup_time'))
                                                            ->getTimestamp() : null;
    }



    /**
     * get `is_cashed` resource.
     *
     * @return string
     */
    protected function getIsCashedResource()
    {
        return json_decode($this->getMeta('is_cashed'));

    }
}
