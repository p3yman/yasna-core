<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\Label;

/**
 * @method bool batchSaveBoolean(array $data)
 * @method mixed getMeta(string $attr)
 */
trait PostWagesTrait
{
    /**
     * get meta fields related to the repairer wages.
     *
     * @return array
     */
    public function getWagesMetaFields()
    {
        return [
             "wage_repair",
             "wage_replace",
             "penalty",
             "wage_repair_type",
             "wage_replace_type",
             "penalty_type",
        ];
    }



    /**
     * get wage label record
     *
     * @return Label|null
     */
    public function getWageLabel()
    {
        return $this->labels()->where("parent_id", label("shop-categories")->id)->first();
    }



    /**
     * get wage
     *
     * @param string $slug
     * @param string $type
     *
     * @return string|double
     */
    public function getWage(string $slug, string $type = null)
    {
        // Post Value...
        $post_value = $this->getMeta($slug);
        if ($type == "post") {
            return $post_value;
        }

        // Tag Value ...
        $tag       = $this->getWageLabel();
        $tag_value = $tag ? $tag->getMeta($slug) : null;
        if ($type == "tag") {
            return $tag_value;
        }

        // Auto Mode...
        return $post_value ? $post_value : $tag_value;
    }



    /**
     * save wages data
     *
     * @param array $data
     *
     * @return bool
     */
    public function saveWages(array $data)
    {
        $post_saved = $this->savePostWages($data);
        $tag_saved  = $this->saveTagWages($data);

        return $post_saved and $tag_saved;
    }



    /**
     * save wages data, related to the post
     *
     * @param array $data
     *
     * @return bool
     */
    private function savePostWages(array $data)
    {
        return $this->batchSaveBoolean([
             "wage_repair"       => $data["post_wage_repair"],
             "wage_replace"      => $data["post_wage_replace"],
             "penalty"           => $data["post_penalty"],
             "wage_repair_type"  => $data["post_wage_repair_type"],
             "wage_replace_type" => $data["post_wage_replace_type"],
             "penalty_type"      => $data["post_penalty_type"],
        ]);
    }



    /**
     * save wages data, related to the tag (=label)
     *
     * @param array $data
     *
     * @return bool
     */
    private function saveTagWages(array $data)
    {
        $tag = $this->getWageLabel();

        if (!$tag) {
            return true;
        }

        return $tag->batchSaveBoolean([
             "wage_repair"       => $data["tag_wage_repair"],
             "wage_replace"      => $data["tag_wage_replace"],
             "penalty"           => $data["tag_penalty"],
             "wage_repair_type"  => $data["tag_wage_repair_type"],
             "wage_replace_type" => $data["tag_wage_replace_type"],
             "penalty_type"      => $data["tag_penalty_type"],
        ]);
    }


}
