<?php

namespace Modules\MobileSalam\Entities\Traits;


use App\Models\SalamRating;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait PostRatingsTrait
{
    /**
     * get the relationship instance of posts ratings
     *
     * @return HasMany
     */
    public function ratings()
    {
        return $this->hasMany(SalamRating::class, "post_id");
    }
}
