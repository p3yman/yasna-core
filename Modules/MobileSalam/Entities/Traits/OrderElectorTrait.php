<?php


namespace Modules\MobileSalam\Entities\Traits;


trait OrderElectorTrait
{
    /**
     * put the time limit until $created_date on elector
     *
     * @param string $create_date
     *
     * @return void
     */
    protected function electorDateUntil($create_date)
    {
        $this->elector()->whereDate('created_at', '<=', $create_date);
    }



    /**
     * put the time limit from $created_date on elector
     *
     * @param string $create_date
     *
     * @return void
     */
    protected function electorDateFrom($create_date)
    {
        $this->elector()->whereDate('created_at', '>=', $create_date);
    }



    /**
     * put the status filter on elector
     *
     * @param string $status
     *
     * @return void
     */
    protected function electorStatus($status)
    {
        if ($status == "all") {
            return;
        }

        if ($status == "non_surveyed") {
            $this->electorNonSurveyed();
            return;
        }

        $this->elector()->where("status", $status);
    }



    /**
     * put the tracking number filter on elector
     *
     * @param int $tracking_number
     *
     * @return void
     */
    protected function electorTrackingNumber($tracking_number)
    {
        $this->elector()->where('tracking_number', $tracking_number);
    }



    /**
     * put the rating filter on elector
     *
     * @return void
     */
    protected function electorNonSurveyed()
    {
        $this->elector()->where('status', 'repaired')->doesntHave('ratings');
    }



    /**
     * put the `name_first` filter on elector
     *
     * @param string $name_first
     *
     * @return void
     */
    protected function electorNameFirst($name_first)
    {
        $this->elector()->where('name_first','like', "%$name_first%");
    }



    /**
     * put the `name_last` filter on elector
     *
     * @param string $name_last
     *
     * @return void
     */
    protected function electorNameLast($name_last)
    {
        $this->elector()->where('name_last','like', "%$name_last%");
    }


}
