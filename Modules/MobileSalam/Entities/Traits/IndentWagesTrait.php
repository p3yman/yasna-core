<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\Post;
use App\Models\Ware;

/**
 * @property string      $defect_type
 * @property Post        $post
 * @property Ware        $ware
 * @property string|null $wage_modified_at
 * @property float       $wage_modified
 */
trait IndentWagesTrait
{
    /**
     * boot IndentWagesTrait
     *
     * @return void
     */
    public static function bootIndentWagesTrait()
    {
        static::addDirectResources([
             "amount_in_shop",
             "amount_for_repairer",
             "wage_calculated",
             "wage_modified",
             "wage_final",
             "wage_modified_at",
             "wage_modified_by",
        ]);
    }



    /**
     * refresh wage calculations.
     *
     * @return bool
     */
    public function refreshWageCalculations()
    {
        $defect_type = $this->defect_type;

        if ($this->getAttribute("wage_calculated")) {
            return true;
        }

        if ($defect_type == "repair") {
            $wage = $this->calculateRepairWage();
        } else {
            $wage = $this->calculateReplaceWage();
        }

        return $this->batchSaveBoolean([
             "wage_calculated" => $wage,
             "wage_final"      => $this->wage_modified_at ? $this->wage_modified : $wage,
        ]);
    }



    /**
     * save wage
     *
     * @param float $amount
     *
     * @return void
     */
    public function saveWage($amount)
    {
        if ($this->getAttribute('wage_calculated') == $amount) {
            $this->batchSaveBoolean([
                 "wage_modified"    => 0,
                 "wage_final"       => $this->getAttribute('wage_calculated'),
                 "wage_modified_at" => null,
                 "wage_modified_by" => 0,
            ]);
            return;
        }

        $this->batchSaveBoolean([
             "wage_modified"    => $amount,
             "wage_final"       => $amount,
             "wage_modified_at" => now()->toDateTimeString(),
             "wage_modified_by" => user()->id,
        ]);
    }



    /**
     * calculate the Debt Repayment of the Repairer
     *
     * @return string
     */
    public function calculatePenaltyWage()
    {
        $wage  = $this->post->getWage("penalty");
        $type  = $this->post->getWage("penalty_type");
        $price = $this->getWarePrice();

        if ($type == "percent") {
            $wage = round($price * $wage / 100);
        }

        return round($price - $wage);
    }



    /**
     * calculate repair wages.
     *
     * @return float
     */
    protected function calculateRepairWage()
    {
        $wage    = $this->post->getWage("wage_repair");
        $type    = $this->post->getWage("wage_repair_type");
        $price   = $this->getWarePrice();
        $percent = (float)get_setting("wage_repair_percent");

        if ($type == "percent") {
            $wage = round($price * $wage / 100);
        }

        return round(($price - $wage) * $percent / 100);
    }



    /**
     * calculate replace wages.
     *
     * @return int
     */
    protected function calculateReplaceWage()
    {
        $wage = $this->post->getWage("wage_replace");
        $type = $this->post->getWage("wage_replace_type");

        if ($type == "absolute") {
            return $wage;
        }

        $price = $this->getWarePrice();

        return round($price * $wage / 100);
    }



    /**
     * get ware price
     *
     * @return float
     */
    protected function getWarePrice()
    {
        $ware = $this->ware;

        return $ware ? $ware->price()->get() : 0;
    }
}
