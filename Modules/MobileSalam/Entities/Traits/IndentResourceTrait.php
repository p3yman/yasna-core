<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamOrder;

/**
 * @property int        $order_id
 * @property int        $ware_id
 * @property SalamOrder $order
 */
trait IndentResourceTrait
{
    /**
     * boot IndentResourceTrait
     *
     * @return void
     */
    public static function bootIndentResourceTrait()
    {
        static::addDirectResources([
             "defect_type",
             "quantity",
             "supply_status",
             "supply_status_label",
             "return_reason",
             'return_reason_label',
             'price',
             'repairer_note',
             'supplier_note',
             "supplied_at",
             "return_rejected_at",
             "return_confirmed_at",

        ]);
    }



    /**
     * get RepairerInfo resource.
     *
     * @return array
     */
    protected function getRepairerInfoResource()
    {
        return $this->getResourceForPersons($this->repairer_id);
    }



    /**
     * get SupplierInfo resource.
     *
     * @return array
     */
    protected function getSupplierInfoResource()
    {
        return $this->getResourceForPersons($this->supplier_id);
    }



    /**
     * get Defect resource.
     *
     * @return array
     */
    protected function getDefectResource()
    {
        $defect = model('salam_defect', $this->getAttribute('defect_id'));

        return [
             "title"      => $defect->title,
             "type"       => $defect->type,
             "type_label" => $defect->type_label,
        ];

    }



    /**
     * get PartTitle resource.
     *
     * @return string
     */
    protected function getPartTitleResource()
    {
        return ware($this->ware_id)->full_title;
    }



    /**
     * get TrackingNumber resource.
     *
     * @return string
     */
    protected function getTrackingNumberResource()
    {
        $order = $this->order;

        return $order ? $order->tracking_number : null;
    }



    /**
     * get Customer resource.
     *
     * @return array
     */
    protected function getCustomerResource()
    {
        $order = $this->order;

        return $order ? $order->getResource("customer") : [];
    }



    /**
     * get Color resource.
     *
     * @return array
     */
    protected function getColorResource()
    {
        $order = $this->order;

        return $order ? $order->getResource("color") : [];
    }



    /**
     * get due_at resource.
     *
     * @return array
     */
    protected function getDueAtResource()
    {
        $order = $this->order;

        return $order ? $order->getResource("due_at") : [];
    }



    /**
     * get device_model resource.
     *
     * @return string|null
     */
    protected function getDeviceModelResource()
    {
        try {
            return $this->order->post->title;
        } catch (\Exception $e) {
            return null;
        }
    }
}
