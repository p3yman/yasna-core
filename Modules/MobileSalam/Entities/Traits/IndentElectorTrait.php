<?php


namespace Modules\MobileSalam\Entities\Traits;


trait IndentElectorTrait
{
    /**
     * put the time limit until $created_date on elector
     *
     * @param string $create_date
     */
    protected function electorDateUntil($create_date)
    {
        $this->elector()->whereDate('created_at', '<=', $create_date);
    }



    /**
     * put the time limit from $created_date on elector
     *
     * @param string $create_date
     */
    protected function electorDateFrom($create_date)
    {
        $this->elector()->whereDate('created_at', '>=', $create_date);
    }



    /**
     * put the supply status on elector
     *
     * @param string $status
     */
    protected function electorSupplyStatus($status)
    {
        $this->elector()->where('supply_status', $status);
    }



    /**
     * put the `return reason` filter on elector
     *
     * @param $reason
     */
    protected function electorReturnReason($reason)
    {
        switch ($reason) {
            case 'used' :
                $this->elector()
                     ->where('return_reason', 'return-reason-used')
                     ->whereNotNull('used_at')
                ;
                return;

            case 'unused' :
                $this->elector()
                     ->where('return_reason', 'return-reason-unused')
                ;
                return;
            case 'factory-fault' :
                $this->elector()
                     ->where('return_reason', 'return-reason-factory-fault')
                ;
                return;
            case 'repair-fault' :
                $this->elector()
                     ->where('return_reason', 'return-reason-repairer-fault')
                ;
                return;
        }
    }



    /**
     * filter data against a full et of status (supply and return).
     *
     * @param string $status
     *
     * @return void
     */
    protected function electorStatus(string $status)
    {
        $status = str_after($status ,"full-indent-states-");

        switch ($status) {
            case "pending":
                $this->elector()->whereNotIn("supply_status", [
                     "supply-status-available",
                     "supply-status-not-available",
                     "supply-status-follow-up",
                ])
                ;
                return;

            case "supplied":
                $this->elector()->where("supply_status", "supply-status-available")->whereNull('returned_at');
                return;

            case "not-available":
                $this->elector()->where("supply_status", "supply-status-not-available")->whereNull('returned_at');
                return;

            case "follow-up":
                $this->elector()->where("supply_status", "supply-status-follow-up")->whereNull('returned_at');
                return;

            case "used":
                $this->elector()->whereNotNull("used_at");
                return;

            case "unused":
                $this->elector()->where('return_reason', 'return-reason-unused');
                return;

            case 'factory-fault' :
                $this->elector()->where('return_reason', 'return-reason-factory-fault');
                return;

            case "repairer-fault":
                $this->elector()->where('return_reason', 'return-reason-repairer-fault');
                return;
        }
    }
}
