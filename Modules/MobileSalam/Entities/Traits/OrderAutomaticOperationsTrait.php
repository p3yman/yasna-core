<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamOrder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

trait OrderAutomaticOperationsTrait
{
    /**
     * cancel delayed orders
     *
     * @return int
     */
    public static function cancelDelayedOrders(): int
    {
        $affected = 0;

        /** @var SalamOrder $item */
        foreach (static::getDelayedOrders() as $item) {
            $affected += (int)$item->changeStatus("cancelled");
        }

        return $affected;
    }



    /**
     * store non-delivered orders.
     *
     * @return int
     */
    public static function storeNonDeliveredOrders(): int
    {
        $affected = 0;

        /** @var SalamOrder $item */
        foreach (static::getNonDeliveredOrders() as $item) {
            $affected += (int)$item->changeStatus("stored");
        }

        return $affected;
    }



    /**
     * get a collection of delayed orders
     *
     * @return Collection
     */
    private static function getDelayedOrders()
    {
        return model("salam-order")
             ->whereNull("received_at")
             ->whereNotNull("receive_active_pickup")
             ->whereDate('eta_receive_created_at', '<=', now()->subHour(setting("repair_order_delivery_time_bar")))
             ->get()
             ;
    }



    /**
     * get a collection of non-delivered orders
     *
     * @return Collection
     */
    private static function getNonDeliveredOrders()
    {
        return model("salam-order")
             ->where("status", "repaired")
             ->whereDate("repaired_at", "<=", now()->subDay())
             ->get()
             ;
    }
}
