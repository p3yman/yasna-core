<?php

namespace Modules\MobileSalam\Entities\Traits;

use AloPeyk\Api\RESTful\Model\Address;
use AloPeyk\Api\RESTful\Model\Order;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\MobileSalam\Entities\SalamPickup;
use Modules\MobileSalam\Http\Requests\V1\SaveRepairOrderPickupRequest;
use Modules\MobileSalam\Http\TimelineNodes\SavedPickupNode;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

trait OrderPickupTrait
{
    /**
     * get the relationship instance of the related salam-pickup records
     *
     * @return HasMany
     */
    public function pickups()
    {
        return $this->hasMany(SalamPickup::Class, 'order_id')->where('parent_entity', 'salam-order');
    }



    /**
     * estimate repair order pickup
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return array
     */
    public function estimateRepairOrderPickup($request)
    {
        $pickup_method   = $request->pickup_method;
        $result          = [];
        $result['price'] = null;
        if ($pickup_method == "alopeyk") {
            $estimations     = self::estimateAlopeykOrder($request);
            $result['price'] = $estimations['price'];
        }
        return $result;
    }



    /**
     * save repair order pickup
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return bool
     */
    public function saveRepairOrderPickup($request)
    {
        /** @var \App\Models\SalamOrder $order */
        $order         = $this;
        $saved         = null;
        $parent_entity = $request->parent_entity;
        $pickup_method = $request->pickup_method;
        $pickup_type   = $request->pickup_type;

        $saved = model("salam-pickup")->batchSave([
             "parent_entity"       => $parent_entity,
             "order_id"            => $this->id,
             "pickup_method"       => $pickup_method,
             "pickup_type"         => $pickup_type,
             "pickup_price"        => $request->pickup_price,
             "pickup_time"         => $request->pickup_time,
             "origin_city_id"      => $request->origin_city_id,
             "origin_lat"          => $request->origin_latitude,
             "origin_lng"          => $request->origin_longitude,
             "origin_addr"         => $request->origin_address,
             "destination_city_id" => $request->destination_city_id,
             "destination_lat"     => $request->destination_latitude,
             "destination_lng"     => $request->destination_longitude,
             "destination_addr"    => $request->destination_address,
             "driver_full_name"    => $request->driver_full_name,
             "driver_phone_number" => $request->driver_phone_number,
             "pickup_note"         => $request->note,
        ]);

        if ($saved->exists) {

            if ($pickup_method == "alopeyk") {
                self::createAlopeykOrder($saved, $request);
            }

            if ($pickup_type == "receive" && $pickup_method == "owner") {
                $this->receive_active_pickup  = $saved->id;
                $this->eta_receive_created_at = $saved->created_at;
                $this->save();
            }

            SavedPickupNode::insert($order, [
                 "pickup" => $saved,
            ]);

        }

        return boolval($saved);
    }



    /**
     * create alopeyk order
     *
     * @param integer                      $pickup
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return bool
     */
    public function createAlopeykOrder($pickup, $request)
    {
        /*
         * Create Origin
         */
        $origin = $this->createOrigin($request);

        /*
         * Create Destination
         */
        $destination_result[] = $this->createDestination($request);

        /*
         * Creating a second destination when sending two way.
         */
        if ($request->pickup_type === 'receive') {
            $destination_result[] = $this->createSecondDestination($request);
        }

        $alopeyk_order = new Order('motorbike', $origin, $destination_result);
        $alopeyk_order->setHasReturn(false);

        if ($request->is_cashed == 1) {
            $alopeyk_order->setCashed(true);
        }

        $api_response = $alopeyk_order->create();


        if ($api_response->status == "success") {
            /** @var SalamPickup $model */
            $model = model('salam-pickup')->find($pickup->id);

            $savedPickup                      = [];
            $savedPickup['is_cashed']         = json_encode($api_response->object->cashed);
            $savedPickup['delivery_order_id'] = $api_response->object->id;

            $model->batchSave($savedPickup);
        }

        return true;
    }



    /**
     * estimate alopeyk order
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return array
     */
    public function estimateAlopeykOrder($request)
    {

        /*
        * Create origin
        */
        $origin = $this->createOrigin($request);

        /*
         * Create Destination
         */
        $destination_result[] = $this->createDestination($request);

        $alopeyk_order = new Order('motorbike', $origin, $destination_result);
        $alopeyk_order->setHasReturn(false);
        if ($request->is_cashed == 1) {
            $alopeyk_order->setCashed(true);
        }

        $api_response = $alopeyk_order->getPrice();

        if ($api_response->status == "success") {
            return [
                 "price" => $api_response->object->price,
            ];
        }

        return [];
    }



    /**
     * apply alopeyk webhook data
     *
     * @param SimpleYasnaRequest $request
     *
     * @return boolean
     */
    public function applyAlopeykWebhookData($request)
    {
        $delivery_order_id             = $request->order->id;
        $pickup                        = model('salam-pickups')
             ->where('delivery_order_id', $delivery_order_id)
             ->get()
        ;
        $pickup->pickup_price          = $request->order->price;
        $pickup->delivery_tracking_url = $request->order->tracking_url;
        $pickup->save();

        return true;
    }



    /**
     * create origin address
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return Address Address
     */
    private function createOrigin($request)
    {
        $origin = new Address(
             'origin',
             $request->origin_city_title_en,
             $request->origin_latitude,
             $request->origin_longitude
        );
        $origin->setAddress($request->origin_address);

        return $origin;
    }



    /**
     * create destination address
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return Address Address
     */
    private function createDestination($request)
    {
        $destination = new Address(
             'destination',
             $request->destination_city_title_en,
             $request->destination_latitude,
             $request->destination_longitude
        );
        $destination->setAddress($request->destination_address);

        return $destination;

    }



    /**
     * create destination address
     *
     * @param SaveRepairOrderPickupRequest $request
     *
     * @return Address Address
     */
    private function createSecondDestination($request)
    {
        $destination_second = new Address(
             'destination',
             $request->origin_city_title_en,
             $request->origin_latitude,
             $request->origin_longitude
        );
        $destination_second->setAddress($request->origin_address);

        return $destination_second;
    }

}
