<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamIndent;
use App\Models\SalamOrder;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property Collection $indents
 * @property float      $wage_added_amount
 */
trait OrderWagesTrait
{
    /**
     * get wage invoice.
     *
     * @return array
     */
    public function getWageInvoice()
    {
        $this->refreshWageCalculations();

        $indents = $this->indents->map(function ($indent) {
            /** @var SalamIndent $indent */
            return $indent->toListResource();
        })->toArray()
        ;

        return [
             "indents"           => $indents,
             "wage_added_amount" => $this->getAttribute("wage_added_amount"),
             "wage_custom_desc"  => $this->getAttribute("wage_custom_desc"),
        ];
    }



    /**
     * refresh wage calculations
     *
     * @return void
     */
    public function refreshWageCalculations()
    {
        /** @var SalamIndent $indent */
        foreach ($this->indents()->get() as $indent) {
            $indent->refreshWageCalculations();
        }
    }



    /**
     * save wages
     *
     * @param array $data
     *
     * @return SalamOrder
     */
    public function saveWages(array $data)
    {
        $ids                     = hashid(array_keys($data['amounts']));
        $this->wage_added_amount = isset($data['wage_added_amount']) ? $data['wage_added_amount'] : 0;

        foreach ($this->indents()->whereIn('id', $ids)->get() as $indent) {
            /** @var SalamIndent $indent */
            $indent->saveWage($data['amounts'][hashid($indent->id)]);
        }

        return $this->batchSave([
             "wage_added_amount"  => $this->wage_added_amount,
             "wage_total_amount"  => $this->getWageTotalAmount(),
             "wage_custom_desc"   => isset($data['wage_custom_desc']) ? $data['wage_custom_desc'] : '',
             "wage_calculated_at" => now()->toDateTimeString(),
             "wage_calculated_by" => user()->id,
        ]);
    }



    /**
     * get wage total amount
     *
     * @return float
     */
    public function getWageTotalAmount()
    {
        return $this->indents()->sum("wage_final") + $this->wage_added_amount;
    }
}
