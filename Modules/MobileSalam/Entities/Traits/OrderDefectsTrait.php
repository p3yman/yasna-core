<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamDefect;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property Collection $defects
 */
trait OrderDefectsTrait
{
    /**
     * get the relationship instance of order nodes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function defects()
    {
        return $this->hasMany(SalamDefect::class, "order_id");
    }



    /**
     * get Defects resource.
     *
     * @return array
     */
    protected function getDefectsResource()
    {
        return $this->getResourceFromCollection($this->defects);
    }

}
