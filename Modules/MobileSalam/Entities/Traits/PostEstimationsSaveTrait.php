<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\Post;

/**
 * @property bool $exists
 * @method Post batchSave($data)
 */
trait PostEstimationsSaveTrait
{
    /**
     * save a post as device
     *
     * @param string $title
     * @param array  $labels
     *
     * @return Post
     */
    public function saveAsDevice(string $title, array $labels)
    {
        $data = $this->getDataRequiredForNewDevice($title);
        $data = $this->enrichDeviceDataWithCachedLabeledValues($data, $labels);

        $saved = $this->batchSave($data);
        if ($saved->exists) {
            $saved->attachLabels($labels);
        }

        return $saved;
    }



    /**
     * get data required for a new device
     *
     * @param string $title
     *
     * @return array
     */
    private function getDataRequiredForNewDevice(string $title): array
    {
        if ($this->exists) {
            return [
                 "title" => $title,
            ];
        }

        return [
             "slug"         => str_slug($title),
             "type"         => "devices",
             "title"        => $title,
             "locale"       => "fa",
             "sisterhood"   => post()::generateSisterhood(),
             "published_at" => now()->toDateTimeString(),
             "published_by" => 1,
        ];
    }



    /**
     * enrich device data with cached labeled values
     *
     * @param array $array
     * @param array $labels
     *
     * @return array
     */
    private function enrichDeviceDataWithCachedLabeledValues(array $array, array $labels)
    {
        $new = [
             "labeled_type"   => $type = $this->discoverLabeledThing($labels, "devices"),
             "labeled_brand"  => $brand = $this->discoverLabeledThing($labels, $type),
             "labeled_series" => $this->discoverLabeledThing($labels, $brand),
             "labeled_colors" => $this->discoverLabeledColors($labels),
        ];

        return array_merge($array, $new);
    }



    /**
     * discover labeled type/brand/series
     *
     * @param array  $labels
     * @param string $parent
     *
     * @return string
     */
    private function discoverLabeledThing(array $labels, string $parent)
    {
        return label($parent)
             ->children()
             ->select("id", "slug")
             ->whereIn("slug", $labels)
             ->get()
             ->pluck("slug")
             ->first()
             ;
    }



    /**
     * discover labeled colors
     *
     * @param array $labels
     *
     * @return string
     */
    private function discoverLabeledColors(array $labels)
    {
        $colors = label("colors")
             ->children()
             ->select("id", "slug")
             ->whereIn("slug", $labels)
             ->get()
             ->pluck("slug")
             ->toArray()
        ;

        return implode(",", $colors);
    }
}
