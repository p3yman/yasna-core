<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\User;
use Modules\MobileSalam\Http\TimelineNodes\AssignedRepairerNode;
use App\Models\SalamOrder;

trait OrderRepairerTrait
{
    /**
     * get the relationship instance of the related repairer record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function repairer()
    {
        return $this->belongsTo(User::class, "repairer_id");
    }



    /**
     * assign a new repairer
     *
     * @param int $repairer_id
     *
     * @return bool
     */
    public function assignRepairer($repairer_id)
    {
        /** @var SalamOrder $record */
        $record      = $this;
        $repairer_id = hashid_number($repairer_id);

        if ($this->repairer_id == $repairer_id) {
            return true;
        }

        $node = AssignedRepairerNode::insert($record, [
             "last_repairer_id" => $this->repairer_id,
             "new_repairer_id"  => $repairer_id,
        ]);

        if (!$node->exists()) {
            return false;
        }

        return $this->update([
             "repairer_id" => $repairer_id,
        ]);
    }

}
