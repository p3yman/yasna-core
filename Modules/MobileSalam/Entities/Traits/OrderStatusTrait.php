<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamOrder;
use Modules\MobileSalam\Http\TimelineNodes\ChangedStatusNode;
use Modules\MobileSalam\Http\TimelineNodes\ClaimedNode;

/**
 * @property string $confirmed_at
 * @property string $received_at
 * @property string $dispatched_at
 * @property string $delivered_at
 * @property string $repaired_at
 * @property string $rejected_at
 * @property string $cancelled_at
 * @property string $closed_at
 * @property string $claimed_at
 */
trait OrderStatusTrait
{
    /**
     * get available statuses
     *
     * @return array
     */
    public static function availableStatuses()
    {
        return [
             "pending",
             "waiting_for_device",
             "processing",
             "repaired",
             "rejected",
             "cancelled",
             "stored",
        ];
    }



    /**
     * get status label
     *
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        return trans("mobile-salam::status." . $this->status);
    }



    /**
     * indicate if the order is still pending.
     *
     * @return bool
     */
    public function isPending()
    {
        return $this->hasStatus("pending");
    }



    /**
     * indicate if the order is approved by an operator.
     *
     * @return bool
     */
    public function isApproved()
    {
        return boolval($this->confirmed_at);
    }



    /**
     * indicate if the device is physically received by any of the delivery methods.
     *
     * @return bool
     */
    public function isReceived()
    {
        return boolval($this->received_at);
    }



    /**
     * indicate if the device is not physically received by any of the delivery methods.
     *
     * @return bool
     */
    public function isNotReceived()
    {
        return !$this->isReceived();
    }



    /**
     * indicate if the device is repaired.
     *
     * @return bool
     */
    public function isRepaired()
    {
        return $this->hasStatus("repaired");
    }



    /**
     * indicate if the device is stored
     *
     * @return bool
     */
    public function isStored()
    {
        return $this->hasStatus("stored");
    }



    /**
     * indicate if the device is not repaired.
     *
     * @return bool
     */
    public function isNotRepaired()
    {
        return !$this->isRepaired();
    }



    /**
     * indicate if the device is rejected to the customer.
     *
     * @return bool
     */
    public function isRejected()
    {
        return $this->hasStatus("rejected");
    }



    /**
     * indicate if the device is not rejected to the customer.
     *
     * @return bool
     */
    public function isNotRejected()
    {
        return !$this->isRejected();
    }



    /**
     * indicate if the device is closed.
     *
     * @return bool
     */
    public function isClosed()
    {
        return boolval($this->closed_at);
    }



    /**
     * indicate if the device is open.
     *
     * @return bool
     */
    public function isOpen()
    {
        return !$this->isClosed();
    }



    /**
     * indicate if the repair is held up to receive the device.
     *
     * @return bool
     */
    public function isWaitingForDevice()
    {
        return $this->hasStatus("waiting_for_device");
    }



    /**
     * indicate if the order is under process.
     *
     * @return bool
     */
    public function isProcessing()
    {
        return $this->hasStatus("processing");
    }



    /**
     * indicate if the order is cancelled.
     *
     * @return bool
     */
    public function isCancelled()
    {
        return $this->hasStatus("cancelled");
    }



    /**
     * change status
     *
     * @param string $new_status
     * @param string $comment
     *
     * @return bool
     */
    public function changeStatus($new_status, $comment = "")
    {
        if (!$new_status or $new_status == $this->status) {
            return false;
        }

        $method = camel_case("changeStatusTo" . $new_status);
        if (!method_exists($this, $method)) {
            return false;
        }

        $return = $this->$method();

        if ($return) {
            $this->refresh();
            ChangedStatusNode::insert($this, [
                 "status"  => $this->status,
                 "comment" => $comment,
            ]);
        }

        return $return;
    }



    /**
     * change status to `pending`
     *
     * @return bool
     */
    public function changeStatusToPending()
    {
        return $this->update([
             "confirmed_at" => null,
             "confirmed_by" => 0,
             "cancelled_at" => null,
             "cancelled_by" => 0,
             "status"       => "pending",
        ]);
    }



    /**
     * change status to `waiting_for_device`
     *
     * @return bool
     */
    public function changeStatusToWaitingForDevice()
    {
        $changes = [
             "received_at" => null,
             "received_by" => 0,
             "status"      => "waiting_for_device",
        ];

        if ($this->isPending()) {
            $changes['confirmed_at'] = now()->toDateTimeString();
            $changes['confirmed_by'] = user()->id;
        }

        return $this->update($changes);
    }



    /**
     * change status to `processing`
     *
     * @return bool
     */
    public function changeStatusToProcessing()
    {
        $changes = [
             "closed_at"   => null,
             "repaired_at" => null,
             "rejected_at" => null,
             "closed_by"   => 0,
             "repaired_by" => 0,
             "rejected_by" => 0,
             "status"      => "processing",
        ];

        if ($this->isPending()) {
            $changes['confirmed_at'] = now()->toDateTimeString();
            $changes['confirmed_by'] = user()->id;
        }

        if ($this->isNotReceived()) {
            $changes['received_at'] = now()->toDateTimeString();
            $changes['received_by'] = user()->id;
        }

        return $this->update($changes);
    }



    /**
     * change status to `repaired`
     *
     * @return bool
     */
    public function changeStatusToRepaired()
    {
        $changes = [
             "rejected_at" => null,
             "rejected_by" => 0,
             "status"      => "repaired",
        ];

        if ($this->isNotRepaired()) {
            $changes['repaired_at'] = now()->toDateTimeString();
            $changes['repaired_by'] = user()->id;
        }

        return $this->update($changes);
    }



    /**
     * change status to `rejected`
     *
     * @return bool
     */
    public function changeStatusToRejected()
    {
        $changes = [
             "repaired_at" => null,
             "repaired_by" => 0,
             "status"      => "rejected",
        ];

        if ($this->isNotRejected()) {
            $changes['rejected_at'] = now()->toDateTimeString();
            $changes['rejected_by'] = user()->id;
        }

        return $this->update($changes);
    }



    /**
     * change status to `cancelled`
     *
     * @return bool
     */
    public function changeStatusToCancelled()
    {
        return $this->update([
             "cancelled_at" => now()->toDateTimeString(),
             "cancelled_by" => user()->id,
             "status"       => "cancelled",
        ]);
    }



    /**
     * change status to `cancelled`
     *
     * @return bool
     */
    public function changeStatusToStored()
    {
        return $this->update([
             "stored_at" => now()->toDateTimeString(),
             "stored_by" => user()->id,
             "status"    => "stored",
        ]);
    }



    /**
     * mark an order as completed (repaired)
     *
     * @param float $final_price
     *
     * @return bool
     */
    public function markAsCompleted($final_price)
    {
        $updated = $this->batchSaveBoolean([
             "rejected_at" => null,
             "rejected_by" => 0,
             'repaired_at' => now()->toDateTimeString(),
             'repaired_by' => user()->id,
             "final_price" => $final_price,
             "status"      => "repaired",
        ]);

        return $updated;
    }



    /**
     * raise claim
     *
     * @param array $array
     *
     * @return bool
     */
    public function raiseClaim(array $array)
    {
        $updated = $this->batchSaveBoolean($array, ["problems", "customer_note"]);

        if ($updated) {
            /** @var SalamOrder $this */
            ClaimedNode::insert($this, [
                 "problems"      => hashid($array['problems']),
                 "customer_note" => $array['customer_note'],
            ]);
        }

        return $updated;
    }



    /**
     * specify if the order is claimable
     *
     * @return bool
     */
    public function isClaimable()
    {
        return boolval($this->isRepaired() and $this->delivered_at and !$this->claimed_at);
    }



    /**
     * specify if the order is reorderable. (the customer can order same.)
     *
     * @return bool
     */
    public function isReorderable()
    {
        return boolval($this->isRepaired() and $this->delivered_at);
    }



    /**
     * identify if the model has the given status.
     *
     * @param string $status
     *
     * @return bool
     */
    protected function hasStatus(string $status): bool
    {
        return $this->status == $status;
    }
}
