<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamEstimation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string     $type
 * @property Collection $estimations
 */
trait PostEstimationsTrait
{
    use PostEstimationsSaveTrait;



    /**
     * get an array of type labels, suitable to generate a combo
     *
     * @return array
     */
    public static function typesCombo()
    {
        return static::labelChildrenCombo("devices");
    }



    /**
     * get an array of brand labels, related to the given type, suitable to generate a combo
     *
     * @param string|int $type
     *
     * @return array
     */
    public static function brandsCombo($type)
    {
        return static::labelChildrenCombo($type);
    }



    /**
     * get an array of series labels, related to the given brand, suitable to generate a combo
     *
     * @param string|int $type
     *
     * @return array
     */
    public static function seriesCombo($type)
    {
        return static::labelChildrenCombo($type);
    }



    /**
     * get all colors combo
     *
     * @return array
     */
    public static function colorsCombo()
    {
        return static::labelChildrenCombo("colors");
    }



    /**
     * get the relationship instance of the estimations (device defects)
     *
     * @return HasMany
     */
    public function estimations()
    {
        return $this->hasMany(SalamEstimation::class);
    }



    /**
     * get a combo-suitable array of the available colors
     *
     * @return array
     */
    public function availableColorsCombo()
    {
        $all_colors = label("colors")->children()->select("id", "slug")->get()->pluck("slug")->toArray();
        $labels     = $this
             ->labels()
             ->whereIn("labels.slug", $all_colors)
             ->select("labels.slug", "titles")
             ->get()
        ;

        return $labels->map(function ($model) {
            return [
                 "id"    => $model->slug,
                 "title" => $model->title,
            ];
        });
    }



    /**
     * get estimation-related fields
     *
     * @return array
     */
    protected function estimationMetaFields()
    {
        return [
             "labeled_type",
             "labeled_brand",
             "labeled_series",
             "labeled_colors",
        ];
    }



    /**
     * get Estimations resource.
     *
     * @return array|null
     */
    protected function getEstimationsResource()
    {
        if ($this->type != "devices") {
            return null;
        }

        $map = $this->estimations->map(function ($model) {
            /** @var SalamEstimation $model */
            return $model->toSingleResource();
        });

        return $map->toArray();
    }



    /**
     * get an array of labels, which are direct children of a parent label indicated by the handle
     *
     * @param string|int $parent_handle
     *
     * @return array
     */
    private static function labelChildrenCombo($parent_handle)
    {
        $parent = label($parent_handle);
        if (!$parent->exists) {
            return [];
        }

        $labels = $parent->children()->get();
        $map    = $labels->map(function ($model) {
            return [
                 "id"    => $model->slug,
                 "title" => $model->title,
            ];
        });

        return $map->toArray();
    }
}
