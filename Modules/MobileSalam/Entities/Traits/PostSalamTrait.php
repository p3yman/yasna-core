<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamEstimation;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property Collection $wares
 * @property string     $hashid
 * @property string     $slug
 * @method bool hasnot($feature)
 */
trait PostSalamTrait
{
    use PostEstimationsTrait;
    use PostWagesTrait;



    /**
     * boot PostSalamTrait
     *
     * @return void
     */
    public static function bootPostSalamTrait()
    {
        static::addDirectResources([
             "after_sale",
             "warranty",
        ]);
    }



    /**
     * get the relationship instance to the estimations.
     *
     * @return HasMany
     */
    public function estimations()
    {
        return $this->hasMany(SalamEstimation::class);
    }



    /**
     * get the direct link of the model.
     *
     * @return string|null
     */
    public function getDirectLinkAttribute()
    {
        switch ($this->type) {
            case "learning":
            case "news":
            case "shop":
                return "$this->type/single/$this->hashid/$this->slug";

            case "page":
                return "p/single/$this->slug";

            case "software":
                return "app/single/$this->hashid/$this->slug";
        }

        return null;
    }



    /**
     * get Question resource.
     *
     * @return string
     */
    protected function getQuestionResource()
    {
        return $this->getMeta("question");
    }

}
