<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamNode;
use Illuminate\Database\Eloquent\Collection;
use Modules\MobileSalam\Services\TimelineNode;

/**
 * @property int        $repairer_id
 * @property Collection $nodes
 */
trait OrderNodesTrait
{
    /**
     * get the relationship instance of order nodes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nodes()
    {
        return $this->hasMany(SalamNode::class, "order_id");
    }



    /**
     * get the timeline array
     *
     * @return array
     */
    public function toTimelineArray(): array
    {
        $namespace = 'Modules\MobileSalam\Http\TimelineNodes';
        $array     = [];

        foreach ($this->nodes()->orderBy("created_at", "DESC")->get() as $node) {
            $class = $namespace . "\\" . $node->class;

            /** @var TimelineNode $instance */
            $instance = new $class($node);

            if (user()->is("member") and !$instance->shouldDisplayCustomer()) {
                continue;
            }
            if (user()->isAnyOf("operator", "super-operator") and !$instance->shouldDisplayOperators()) {
                continue;
            }
            if (user()->is("repairer") and !$instance->shouldDisplayRepairer()) {
                continue;
            }

            $array[$node->hashid] = [
                 "id"         => $node->hashid,
                 "type"       => $node->class,
                 "title"      => $instance::title(),
                 "created_at" => $node->created_at ? $node->created_at->getTimestamp() : "",
                 "created_by" => user($node->created_by)->toMinimalResource(),
                 "data"       => $instance->toArray(),
            ];
        }

        return $array;
    }



    /**
     * get the limited timeline array, for the guest user.
     *
     * @return array
     */
    public function toTrackArray()
    {
        return [
             '0' => [
                  "id"         => "0",
                  "type"       => "TrackNode",
                  "title"      => trans("mobile-salam::nodes.SubmittedNewOrderNode"),
                  "created_at" => $this->created_at ? $this->created_at->getTimestamp() : "",
                  "created_by" => user($this->created_by)->toMinimalResource(),
                  "data"       => [
                       'device'               => $this->getCustomerResource(),
                       'tracking_number'      => $this->tracking_number,
                       'application_type'     => $this->application_type,
                       'serial'               => $this->serial,
                       'unknown_device'       => $this->unknown_device,
                       'unknown_problem'      => $this->unknown_problem,
                       'is_repaired_before'   => $this->is_repaired_before,
                       'estimated_price'      => $this->estimated_price,
                       'estimated_time'       => $this->estimated_time,
                       'final_price'          => $this->final_price,
                       'guarantee_expires_at' => $this->guarantee_expires_at,
                       'status'               => $this->status,
                       'status_label'         => $this->status_label,
                       'customer_note'        => $this->customer_note,
                  ],
             ],
        ];
    }
}
