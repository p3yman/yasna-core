<?php


namespace Modules\MobileSalam\Entities\Traits;

use App\Models\SalamEstimation;
use Carbon\Carbon;

/**
 * @property int    $customer_id
 * @property string $tracking_number
 * @property string $application_type
 * @property string $serial
 * @property string $unknown_device
 * @property string $unknown_problem
 * @property bool   $is_repaired_before
 * @property float  $estimated_price
 * @property int    $estimated_time
 * @property float  $final_price
 * @property string $guarantee_expires_at
 * @property string $status
 * @property string $status_label
 * @property string $customer_note
 * @property int    $post_id
 * @property int    $color_id
 */
trait OrderResourceTrait
{
    /**
     * boot OrderResourcesTrait
     *
     * @return void
     */
    public static function bootOrderResourceTrait()
    {
        static::addDirectResources([
             'tracking_number',
             'application_type',
             'serial',
             'unknown_device',
             'unknown_problem',
             'is_repaired_before',
             'estimated_price',
             'estimated_time',
             'final_price',
             'status',
             'status_label',
             'customer_note',
             "guarantee_expires_at",
             "due_at",
             "repaired_at",
             "wage_calculated_at",
             "operator_feedback",
             "rejected_reason",
        ]);
    }



    /**
     * get Customer resource.
     *
     * @return array
     */
    protected function getCustomerResource()
    {
        $user = user($this->customer_id);

        return [
             'id'        => $user->hashid,
             'full_name' => $user->full_name,
             'mobile'    => $user->mobile,
             "tel"       => $user->tel,
             "credit"    => $user->credit,
        ];
    }



    /**
     * get Repairer resource.
     *
     * @return array
     */
    protected function getRepairerResource()
    {
        return $this->getResourceForPersons($this->repairer_id);
    }



    /**
     * get Device resource.
     *
     * @return array
     */
    protected function getDeviceResource()
    {
        $post   = post($this->post_id);
        $type   = label($post->getMeta('labeled_type'));
        $brand  = label($post->getMeta('labeled_brand'));
        $series = label($post->getMeta('labeled_series'));

        return [
             'id'     => hashid($post->id),
             'title'  => $post->title,
             'slug'   => $post->slug,
             'labels' => [
                  'type'      => $type->title,
                  "type_id"   => $type->hashid,
                  'brand'     => $brand->title,
                  "brand_id"  => $brand->hashid,
                  'series'    => $series->title,
                  "series_id" => $series->hashid,
             ],

        ];
    }



    /**
     * get Color resource.
     *
     * @return array
     */
    protected function getColorResource()
    {
        $label = label($this->color_id);

        return [
             'id'         => $label->hashid,
             'slug'       => $label->slug,
             'color_name' => $label->title,
        ];
    }



    /**
     * get IsClaimable resource.
     *
     * @return bool
     */
    protected function getIsClaimableResource()
    {
        return $this->isClaimable();
    }



    /**
     * get IsReorderable resource.
     *
     * @return bool
     */
    protected function getIsReorderableResource()
    {
        return $this->isReorderable();
    }



    /**
     * get problems resource.
     *
     * @return array
     */
    protected function getProblemsResource()
    {
        $problems = (array)$this->getMeta("problems");
        $array    = [];

        foreach ($problems as $problem) {
            /** @var SalamEstimation $estimation */
            $estimation = model("SalamEstimation")->grabHashid($problem);

            if ($estimation->exists) {
                $array[] = $estimation->toResource();
            }
        }

        return $array;
    }



    /**
     * get `remaining_time` resource.
     *
     * @return int|null
     */
    protected function getRemainingTimeResource()
    {
        $remaining_time = $this->due_at ? now()->diffInRealSeconds($this->due_at) : null;

        return $remaining_time;
    }



    /**
     * get AvailableProblems resource.
     *
     * @return array
     */
    protected function getAvailableProblemsResource()
    {
        try {
            $estimations = $this->post->estimations;
        } catch (\Exception $e) {
            return [];
        }

        return $estimations->pluck('title')->toArray();
    }

}
