<?php

namespace Modules\MobileSalam\Entities\Traits;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Modules\Payment\Services\Entity\TransactionsTrait;

/**
 * @property Collection $transactions
 * @property User       $customer
 */
trait OrderPaymentsTrait
{
    use TransactionsTrait;



    /**
     * boot OrderPaymentsTrait
     *
     * @return void
     */
    public static function bootOrderPaymentsTrait()
    {
        static::addDirectResources([
             "amount_paid",
             "amount_outstanding",
             "online_payment_status",
        ]);
    }



    /**
     * get the outstanding amount of the order invoice
     *
     * @return float
     */
    public function getAmountOutstandingAttribute()
    {
        $price = (float)$this->getAttribute("final_price");
        $paid  = (float)$this->getAttribute("amount_paid");

        return $price - $paid;
    }



    /**
     * refresh `paid_amount` attribute, according to the registered transactions
     *
     * @return bool
     */
    public function refreshPaidAmount()
    {
        $paid = $this->transactions()->sum("verified_amount");

        return $this->update([
             "amount_paid" => $paid,
        ]);
    }



    /**
     * perform payment from the customer wallet. Amount could be a part of the total payable amount.
     *
     * @param float $amount
     *
     * @return int
     */
    public function payFromWallet($amount)
    {
        $track1 = $this->customer->changeCredit(-$amount, 0, [
             "title" => trans("mobile-salam::wallet.deduct_from_wallet"),
        ]);

        if (!$track1) {
            return 0;
        }

        $track2 = payment()
             ->transaction()
             ->invoiceModel("SalamOrder")
             ->invoiceId($this->id)
             ->accountId(0)
             ->title(trans("mobile-salam::wallet.repair_order_payment"))
             ->callbackUrl(config("mobile-salam.credit_callback"))
             ->payableAmount($amount)
             ->userId($this->customer->id)
             ->getTrackingNumber()
        ;

        if (!is_numeric($track2)) {
            return 0;
        }

        payment()->transaction()->trackingNumber($track2)->makeVerified();
        $this->refreshPaidAmount();

        return $track2;
    }



    /**
     * perform manual invoice payment by operator.
     *
     * @param float  $amount
     * @param int    $account_id
     * @param string $date
     * @param string $bank_track
     *
     * @return int
     */
    public function payByOperator($amount, $account_id, $date, $bank_track)
    {
        $track = payment()
             ->transaction()
             ->invoiceModel("SalamOrder")
             ->invoiceId($this->id)
             ->accountId($account_id)
             ->title(trans("mobile-salam::wallet.manual_repair_payment"))
             ->callbackUrl(config("mobile-salam.credit_callback"))
             ->payableAmount($amount)
             ->userId($this->customer->id)
             ->effectedAt($date)
             ->bankResnum($bank_track)
             ->getTrackingNumber()
        ;

        if (!is_numeric($track)) {
            return false;
        }

        payment()->transaction()->trackingNumber($track)->makeVerified();


        $this->refreshPaidAmount();
        return $track;
    }



    /**
     * get Transactions resource.
     *
     * @return array
     */
    protected function getTransactionsResource()
    {
        $paid = (float)$this->getAttribute("amount_paid");

        if ($paid <= 0) {
            return [];
        }

        return $this->getResourceFromCollection($this->transactions, null, ["model"]);
    }



    /**
     * get `payment_status` resource.
     *
     * @return string|null
     */
    protected function getPaymentStatusResource()
    {
        if ($this->isNotRepaired()) {
            return null;
        }

        if ($this->getAttribute("amount_paid") >= $this->getAttribute("final_price")) {
            return "paid";
        }

        return "waiting";
    }
}
