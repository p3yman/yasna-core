<?php

namespace Modules\MobileSalam\Entities\Traits;

trait DefectResourceTrait
{
    /**
     * boot DefectResourceTrait
     *
     * @return void
     */
    public static function bootDefectResourceTrait()
    {
        static::addDirectResources([
             "title",
             "type",
             "type_label",
        ]);
    }
}
