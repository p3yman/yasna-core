<?php

namespace Modules\MobileSalam\Entities;

use App\Models\Post;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalamEstimation extends YasnaModel
{
    use SoftDeletes;



    /**
     * get the relationship instance of the related post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }



    /**
     * get the main meta fields
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "color_title",
             "is_precise",
        ];
    }



    /**
     * get full title (title and color)
     *
     * @return string
     */
    public function getFullTitleAttribute()
    {
        $color = $this->getMeta("color_title");

        if ($color) {
            return $this->title . " ($color)";
        }

        return $this->title;
    }



    /**
     * get Color resource.
     *
     * @return string
     */
    protected function getColorResource()
    {
        return label($this->color)->slug;
    }



    /**
     * get Title resource.
     *
     * @return string
     */
    protected function getTitleResource()
    {
        return $this->title;
    }



    /**
     * get Price resource.
     *
     * @return float
     */
    protected function getPriceResource()
    {
        return $this->price;
    }



    /**
     * get Duration resource.
     *
     * @return int
     */
    protected function getDurationResource()
    {
        return $this->duration;
    }



    /**
     * get is_precise resource.
     *
     * @return bool
     */
    protected function getIsPreciseResource()
    {
        return boolval($this->getMeta("is_precise"));
    }

}
