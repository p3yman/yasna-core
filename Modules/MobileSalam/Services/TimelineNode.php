<?php

namespace Modules\MobileSalam\Services;

use App\Models\SalamNode;
use App\Models\SalamOrder;
use Illuminate\Contracts\Support\Arrayable;
use Modules\Yasna\Services\GeneralTraits\ClassRecognitionsTrait;

/**
 * Class RepairOrderNode
 */
abstract class TimelineNode implements Arrayable
{
    use ClassRecognitionsTrait;

    /** @var SalamNode */
    protected $model;



    /**
     * RepairOrderNode constructor.
     *
     * @param SalamNode $model
     */
    public function __construct(SalamNode $model)
    {
        $this->model = $model;
    }



    /**
     * insert a new node in the order
     *
     * @param array $data
     *
     * @return TimelineNode
     */
    public static function insert(SalamOrder $order, array $data = [])
    {
        $array = [
             "order_id" => $order->id,
             "class"    => static::calledClassName(),
             "data"     => array_normalize($data, static::customData()),
        ];

        $node     = SalamNode::newRecord($array);
        $instance = new static($node);

        $instance->notify();

        return $instance;
    }



    /**
     * get the human-friendly title of the node.
     *
     * @return string
     */
    public static function title()
    {
        $name = static::calledClassName();

        return trans("mobile-salam::nodes.$name");
    }



    /**
     * get the custom fields and data, to be stored in the meta field of the model
     *
     * @return array
     */
    public static function customData()
    {
        return [];
    }



    /**
     * raise appropriate notifications.
     *
     * @return void
     */
    public function notify()
    {
        // Override this to raise the appropriate notifications.
    }



    /**
     * indicate if the node should be displayed to the customer
     *
     * @return bool
     */
    public function shouldDisplayCustomer()
    {
        return false;
    }



    /**
     * indicate if the node should be displayed to the current repairer
     *
     * @return bool
     */
    public function shouldDisplayRepairer()
    {
        return false;
    }



    /**
     * indicate if the node should be displayed to the operators
     *
     * @return bool
     */
    final public function shouldDisplayOperators()
    {
        return true;
    }



    /**
     * indicate if the node should be allowed to be created by the user
     *
     * @return bool
     */
    public function shouldAllowCustomer()
    {
        return false;
    }



    /**
     * indicate if the node should be allowed to be created by the current repairer
     *
     * @return bool
     */
    public function shouldAllowRepairer()
    {
        return false;
    }



    /**
     * indicate if the node should be allowed to be created by the operators
     *
     * @return bool
     */
    public function shouldAllowOperators()
    {
        return true;
    }



    /**
     * indicate if the model already exists.
     *
     * @return bool
     */
    public function exists()
    {
        return $this->model->exists;
    }
}
