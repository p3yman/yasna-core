<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class InputsTableSeeder extends Seeder
{

    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()::seed("inputs", [
             [
                  'slug'      => 'warranty',
                  'title'     => trans("mobile-salam::seeder.warranty"),
                  'order'     => '51',
                  'type'      => 'editor',
                  'data_type' => 'textarea',
                  'css_class' => 'tinyMini',
             ],
             [
                  'slug'      => 'after_sale',
                  'title'     => trans("mobile-salam::seeder.after-sale"),
                  'order'     => '52',
                  'type'      => 'editor',
                  'data_type' => 'textarea',
                  'css_class' => 'tinyMini',
             ],
             [
                  'slug'      => 'video-link',
                  'title'     => trans("mobile-salam::seeder.video_link"),
                  'order'     => '53',
                  'type'      => 'editor',
                  'data_type' => 'textarea',
                  'css_class' => '',
             ],
             [
                  "slug"             => "question",
                  'title'            => trans("mobile-salam::seeder.question"),
                  'order'            => '53',
                  'type'             => 'editor',
                  'data_type'        => 'textarea',
                  'validation_rules' => 'required',
             ],
        ]);
    }
}
