<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;

class MobileSalamDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PostsTableSeeder::class);
        $this->call(InputsTableSeeder::class);
        $this->call(PosttypesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(CombosTableSeeder::class);
        $this->call(PredefinedDeviceSeeder::class);
        $this->call(LabelsTableSeeder::class);
        $this->call(DeviceEstimationsSeeder::class);
        $this->call(MapLinksTableSeeder::class);
    }
}
