<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;


class PostsTableSeeder extends Seeder
{

    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert($this->safeData());
    }



    /**
     * Returns the safe data to be inserted.
     *
     * @return array
     */
    protected function safeData()
    {
        $data       = $this->data();
        $normalizer = $this->generateNormalized($data);

        foreach ($data as $key => $datum) {
            $conditions = array_only($datum, ['slug', 'locale', 'type']);

            if (post()->where($conditions)->first()) {
                unset($data[$key]);
            } else {
                $data[$key] = array_normalize($datum, $normalizer);
            }
        }

        return array_values($data);
    }



    /**
     * Returns all data to be inserted.
     *
     * @return array
     */
    protected function data()
    {
        return array_merge(
             $this->contactPosts(),
             $this->aboutPosts(),
             $this->aboutFooterPosts(),
             $this->purchasingInstructionsPosts(),
             $this->repairOrderingInstructionsPosts(),
             $this->purchaseGuaranteeProcedurePosts(),
             $this->repairGuaranteeProcedurePosts(),
             $this->termsConditionsPosts(),
             $this->repairTariffsPosts(),
             $this->productReturnPosts(),
             $this->originalPartsPost(),
             $this->fairPricePost(),
             $this->professionalRepairersPost(),
             $this->declareRepairTimePost(),
             $this->videoPost()


        );
    }



    /**
     * Return commenting posts for contact form.
     *
     * @return array
     */
    protected function contactPosts()
    {
        $sisterhood  = hashid(time(), 'main');
        $now         = now()->toDateTimeString();
        $common_meta = [
             'fields' => "name*,email*,mobile*,subject*,text*",
             'rules'  => "name=>required,\r\nmobile=>phone:mobile|required,\r\nemail=>email,\r\nsubject=>required,\r\ntext=>required",
        ];
        $common      = [
             'type'         => 'commenting',
             'slug'         => 'contact',
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
             'meta'         => json_encode($common_meta),
        ];
        $data        = [];
        $locales     = $this->availableLocales();

        foreach ($locales as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => $this->runningModule()->getTrans('general.contact-us', [], $locale),
                 'title2' => $this->runningModule()->getTrans('seeder.contact-commenting-title2', [], $locale),
            ]);
        }

        return $data;
    }



    /**
     * Generates an array to be used while normalizing array of every record before insert.
     * If normalizing be omitted, it may occurring differences between records' keys and cause SQL error.
     *
     * @param array $data
     *
     * @return array
     */
    protected function generateNormalized(array $data)
    {
        return array_fill_keys(array_keys(array_merge(...$data)), null);
    }



    /**
     * Returns the site's locales.
     *
     * @return array
     */
    protected function availableLocales()
    {
        $setting = setting()->ask('site_locales')->gain();
        if ($setting and is_array($setting) and count($setting)) {
            return $setting;
        }
        return config('locale.site_locales');
    }



    /**
     * Returns about posts
     *
     * @return array
     */
    protected function aboutPosts()
    {
        return $this->generateStaticPosts('page', 'about', 'general.about');
    }



    /**
     * Returns about us footer posts
     *
     * @return array
     */
    protected function aboutFooterPosts()
    {
        return $this->generateStaticPosts('page', 'about-us-footer', 'general.about-us-footer');
    }



    /**
     * Returns purchasing instructions posts
     *
     * @return array
     */
    protected function purchasingInstructionsPosts()
    {
        return $this->generateStaticPosts('page', 'purchasing-instructions', 'general.purchasing-instructions');
    }



    /**
     * Returns repair ordering instructions posts
     *
     * @return array
     */
    protected function repairOrderingInstructionsPosts()
    {
        return $this->generateStaticPosts('page', 'repair-ordering-instructions',
             'general.repair-ordering-instructions');
    }



    /**
     * Returns purchase guarantee procedure posts
     *
     * @return array
     */
    protected function purchaseGuaranteeProcedurePosts()
    {
        return $this->generateStaticPosts('page', 'purchase-guarantee-procedure',
             'general.purchase-guarantee-procedure');
    }



    /**
     * Returns repair guarantee procedure posts
     *
     * @return array
     */
    protected function repairGuaranteeProcedurePosts()
    {
        return $this->generateStaticPosts('page', 'repair-guarantee-procedure', 'general.repair-guarantee-procedure');
    }



    /**
     * Returns terms conditions posts
     *
     * @return array
     */
    protected function termsConditionsPosts()
    {
        return $this->generateStaticPosts('page', 'terms-conditions', 'general.terms-conditions');
    }



    /**
     * Returns privacy posts
     *
     * @return array
     */
    protected function privacyPosts()
    {
        return $this->generateStaticPosts('page', 'privacy', 'general.privacy');
    }



    /**
     * Returns repair tariffs posts
     *
     * @return array
     */
    protected function repairTariffsPosts()
    {
        return $this->generateStaticPosts('page', 'repair-tariffs', 'general.repair-tariffs');
    }



    /**
     * Returns product return posts
     *
     * @return array
     */
    protected function productReturnPosts()
    {
        return $this->generateStaticPosts('page', 'product-return', 'general.product-return');
    }



    /**
     * Generates and returns an array to be inserted for posts with the given slug
     *
     * @param string $type
     * @param string $slug
     * @param string $trans_path
     * @param string $text
     *
     * @return array
     */
    protected function generateStaticPosts(string $type, string $slug, string $trans_path, string $text = null)
    {
        $sisterhood = hashid(time(), 'main');
        $now        = now()->toDateTimeString();
        $common     = [
             'type'         => $type,
             'slug'         => $slug,
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
             'locale'       => 'fa',
             'title'        => $this->runningModule()->getTrans($trans_path, [], 'fa'),
             'text'         => $text,
        ];

        $data = [];

        $data[] = array_merge($common, []);

        return $data;
    }



    /**
     * Returns original parts posts
     *
     * @return array
     */
    protected function originalPartsPost()
    {
        return $this->generateStaticPosts('original-contents', 'service-original-parts',
             'general.original-parts',
             trans("mobile-salam::general.original-parts-text"));
    }



    /**
     * Returns fair price posts
     *
     * @return array
     */
    protected function fairPricePost()
    {
        return $this->generateStaticPosts('original-contents', 'service-fair-price',
             'general.fair-price',
             trans("mobile-salam::general.original-parts-text"));
    }



    /**
     * Returns professional repairers posts
     *
     * @return array
     */
    protected function professionalRepairersPost()
    {
        return $this->generateStaticPosts('original-contents', 'service-professional-repairers',
             'general.professional-repairers',
             trans("mobile-salam::general.original-parts-text"));
    }



    /**
     * Returns declare repair time posts
     *
     * @return array
     */
    protected function declareRepairTimePost()
    {
        return $this->generateStaticPosts('original-contents', 'service-declare-repair-time',
             'general.declare-repair-time',
             trans("mobile-salam::general.original-parts-text"));
    }



    /**
     * Returns video posts
     *
     * @return array
     */
    protected function videoPost()
    {
        return $this->generateStaticPosts('original-contents', 'service-video',
             'general.video-title',
             trans("mobile-salam::general.video-title-text"));
    }
}
