<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class RolesTableSeeder extends Seeder
{

    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('roles', $this->data());
    }



    /**
     * Return the data to seed.
     *
     * @return array
     */
    protected function data()
    {

        return [
             [
                  'slug'         => 'super-operator',
                  'title'        => $this->runningModule()->getTrans('seeder.super_operator'),
                  'plural_title' => $this->runningModule()->getTrans('seeder.super_operators'),
             ],
             [
                  'slug'         => 'operator',
                  'title'        => $this->runningModule()->getTrans('seeder.operator'),
                  'plural_title' => $this->runningModule()->getTrans('seeder.operators'),
             ],
             [
                  'slug'         => 'repairer',
                  'title'        => $this->runningModule()->getTrans('seeder.repairer'),
                  'plural_title' => $this->runningModule()->getTrans('seeder.repairers'),
             ],
             [
                  'slug'         => 'supplier',
                  'title'        => $this->runningModule()->getTrans('seeder.supplier'),
                  'plural_title' => $this->runningModule()->getTrans('seeder.suppliers'),
             ],
             [
                  'slug'         => 'member',
                  'title'        => $this->runningModule()->getTrans('seeder.customer'),
                  'plural_title' => $this->runningModule()->getTrans('seeder.customers'),
             ],
        ];
    }
}
