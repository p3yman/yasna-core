<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;

class LabelsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedParentLabels();
        $this->seedShopCategoriesLabel();
    }



    /**
     * seed parent labels
     *
     * @return void
     */
    private function seedParentLabels()
    {
        $this->seedOneParentLabel("faq", "faq", false);
        $this->seedOneParentLabel("software", "software-categories", true);
        $this->seedOneParentLabel("software", "software-tags", false);
        $this->seedOneParentLabel("news", "news-categories", true);
        $this->seedOneParentLabel("news", "news-tags", false);
        $this->seedOneParentLabel("learning", "learning-categories", true);
        $this->seedOneParentLabel("learning", "learning-tags", false);
        $this->seedOneParentLabel("shop", "shop-categories", false);
        $this->seedOneParentLabel("shop", "shop-brands", false);
    }



    /**
     * seed one parent label and attach it to the given posttype
     *
     * @param string $slug
     * @param string $posttype
     * @param bool   $nested
     *
     * @return void
     */
    private function seedOneParentLabel($slug, $posttype, $nested = false)
    {
        posttype()->newLabel($slug, trans("mobile-salam::seeder.label.$slug"), [
             'custom' => [
                  "feature_nested" => $nested,
             ],
        ]);

        model("posttype", $posttype)->attachLabel($slug);
    }



    /**
     * get seeders shop final labels, belonging to one parent label.
     *
     * @return void
     */
    private function seedShopCategoriesLabel()
    {
        $array  = ["lcd", "battery", "flat", "camera", "back", "cover",];
        $parent = label("shop-categories");

        foreach ($array as $item) {
            post()->newLabel($parent->slug . "-" . $item, trans("mobile-salam::devices.shop_items.$item"), [
                 "parent_id" => $parent->id,
            ]);
        }
    }
}
