<?php

namespace Modules\MobileSalam\Database\Seeders;

use App\Models\Posttype;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Filemanager\Services\Seeder\PosttypeSeedingTool;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PosttypesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PosttypeSeedingTool)->seedPosttypes($this->posttypesData());
        $this->applyContactCommentOptions();
    }



    /**
     * Returns an array of posttypes data.
     *
     * @return array
     */
    private function posttypesData()
    {
        return [
             [
                  'posttype' => [
                       'slug'           => 'commenting',
                       'order'          => '8',
                       'title'          => $this->runningModule()->getTrans('seeder.commenting-plural'),
                       'singular_title' => $this->runningModule()->getTrans('seeder.commenting-singular'),
                       'template'       => 'special',
                       'icon'           => 'commenting',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       "commenting",
                       "text",
                       "manage_sidebar",
                       "title2",
                  ],
                  'inputs'   => [
                       "fields",
                       "rules",
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'page',
                       'title'          => $this->runningModule()->getTrans('seeder.page-plural'),
                       'singular_title' => $this->runningModule()->getTrans('seeder.page-singular'),
                       'order'          => '11',
                       'template'       => 'post',
                       'icon'           => 'file-alt',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'text',
                       'abstract',
                       'locales',
                       'slug',
                       'manage_sidebar',
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'shop',
                       'title'          => trans("shop::wares.title"),
                       'singular_title' => trans("shop::wares.single"),
                       'order'          => '15',
                       'template'       => 'post',
                       'icon'           => 'shopping-basket',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'text',
                       'abstract',
                       'shop',
                       'shop_inventory',
                       'manage_sidebar',
                       'searchable',
                       'single_view',
                       'list_view',
                       'attachments',
                       'featured_image',
                       'labels',
                  ],
                  'inputs'   => [
                       "warranty",
                       "after_sale",
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'faq',
                       'title'          => trans("mobile-salam::seeder.faq"),
                       'singular_title' => trans("mobile-salam::seeder.question"),
                       'order'          => '15',
                       'template'       => 'post',
                       'icon'           => 'question-circle',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'text',
                       'long_title',
                       'manage_sidebar',
                       'labels',
                  ],
                  'inputs'   => [
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'software',
                       'title'          => trans("mobile-salam::seeder.software_plural"),
                       'singular_title' => trans("mobile-salam::seeder.software"),
                       'order'          => '15',
                       'template'       => 'post',
                       'icon'           => 'cubes',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'text',
                       'manage_sidebar',
                       'labels',
                       'single_view',
                       'list_view',
                  ],
                  'inputs'   => [
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'news',
                       'title'          => trans("mobile-salam::seeder.news"),
                       'singular_title' => trans("mobile-salam::seeder.news_single"),
                       'order'          => '15',
                       'template'       => 'post',
                       'icon'           => 'newspaper-o',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'text',
                       'manage_sidebar',
                       'labels',
                       'single_view',
                       'list_view',
                  ],
                  'inputs'   => [
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'learning',
                       'title'          => trans("mobile-salam::seeder.learning_plural"),
                       'singular_title' => trans("mobile-salam::seeder.learning_single"),
                       'order'          => '15',
                       'template'       => 'post',
                       'icon'           => 'lightbulb-o',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'text',
                       'manage_sidebar',
                       'labels',
                       'single_view',
                       'list_view',
                  ],
                  'inputs'   => [
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'questions',
                       'title'          => trans("mobile-salam::seeder.poll_questions"),
                       'singular_title' => trans("mobile-salam::seeder.poll_question"),
                       'order'          => '15',
                       'template'       => 'post',
                       'icon'           => 'star-o',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'manage_sidebar',
                  ],
                  'inputs'   => [
                       "question",
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'original-contents',
                       'title'          => trans("mobile-salam::seeder.original_contents"),
                       'singular_title' => trans("mobile-salam::seeder.original_content"),
                       'order'          => '15',
                       'template'       => 'post',
                       'icon'           => 'home',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       'text',
                       'manage_sidebar',
                       'labels',
                       'single_view',
                       'list_view',
                       "title2",
                       "slug",
                  ],
                  'inputs'   => [
                       'video-link'
                  ],
             ],
        ];

    }



    /**
     * apply contact comment options
     *
     * @return void
     */
    private function applyContactCommentOptions()
    {
        posttype('commenting')->batchSave([
             "commenting_receive"             => "1",
             "commenting_allow_guests"        => "1",
             "commenting_enter_subject"       => "must",
             "commenting_enter_email"         => "must",
             "commenting_email_notifications" => "1",
             "commenting_sms_notifications"   => "1",
        ]);
    }
}
