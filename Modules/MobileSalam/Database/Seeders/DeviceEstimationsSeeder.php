<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;


class DeviceEstimationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedDefectsEstimations();
    }



    /**
     * seed defects estimations
     *
     * @return void
     */
    private function seedDefectsEstimations()
    {
        $this->seedMobilesDefects();
        $this->seedTabletsDefects();
        $this->seedWatchesDefects();
    }



    /**
     * seed mobiles defects
     *
     * @return void
     */
    private function seedMobilesDefects()
    {
        $mobiles = label('phone')->models()->get();
        $defects = trans('mobile-salam::devices.phone_defects');
        $this->seedDevicesDefects($mobiles, $defects);
    }



    /**
     * seed tablet defects
     *
     * @return void
     */
    private function seedTabletsDefects()
    {
        $tablets = label('tablet')->models()->get();
        $defects = trans('mobile-salam::devices.phone_defects');
        $this->seedDevicesDefects($tablets, $defects);
    }



    /**
     * seed watches defects
     *
     * @return void
     */
    private function seedWatchesDefects()
    {
        $watch   = label('watch')->models()->get();
        $defects = trans('mobile-salam::devices.watch_defects');
        $this->seedDevicesDefects($watch, $defects);
    }



    /**
     * seed devices defects in `salam_estimations` table
     *
     * @param Collection $devices
     * @param array      $defects
     *
     * @return void
     */
    private function seedDevicesDefects(Collection $devices, array $defects)
    {
        $result = [];
        foreach ($devices as $device) {
            $data['post_id'] = $device->id;
            foreach ($defects as $key => $defect) {
                if ($key == "battery" or $key == "lcd" or $key == "replace_glass_with_risk" or $key == "replace_glass_without_risk") {
                    $data['meta']['is_precise'] = true;
                } else {
                    $data['meta']['is_precise'] = false;
                }
                $data['title'] = $defect;
                $result[]      = $data;
            }
        }
        yasna()::seed("salam_estimations", $result);
    }
}
