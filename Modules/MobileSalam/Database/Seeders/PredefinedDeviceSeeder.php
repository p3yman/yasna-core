<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Posts\Services\PosttypeSeedingTool;

class PredefinedDeviceSeeder extends Seeder
{
    /**
     * run the seeder
     *
     * @return void
     */
    public function run()
    {
        $this->seedPosttype();
        $this->seedLabels();
        $this->seedPosts();
    }



    /**
     * seed the necessary posttype
     *
     * @return void
     */
    private function seedPosttype()
    {
        $data = [
             [
                  'posttype' => [
                       'slug'           => 'devices',
                       'order'          => '8',
                       'title'          => trans("mobile-salam::devices.plural"),
                       'singular_title' => trans("mobile-salam::devices.singular"),
                       'template'       => 'special',
                       'icon'           => 'mobile',
                       'locales'        => 'fa',
                  ],
                  'features' => [
                       "labels",
                       "title2",
                  ],
                  'inputs'   => [
                  ],
             ],
        ];

        (new PosttypeSeedingTool)->seedPosttypes($data);
    }



    /**
     * seed nested list of device labels
     *
     * @return void
     */
    private function seedLabels()
    {
        $this->seedParentLabel();
        $this->seedTypeLabels();
        $this->seedBrandLabels();
        $this->seedColorLabels();
        $this->seedSeriesLabels();
    }



    /**
     * seed parent label
     *
     * @return void
     */
    private function seedParentLabel()
    {
        posttype()->newLabel("devices", trans("mobile-salam::devices.plural"), [
             'custom' => [
                  "feature_nested" => true,
             ],
        ]);

        model("posttype", "devices")->attachLabel("devices");
    }



    /**
     * seed type labels
     *
     * @return void
     */
    private function seedTypeLabels()
    {
        $parent = label("devices");

        post()->newLabel("tablet", trans("mobile-salam::devices.tablet"), ["parent_id" => $parent->id]);
        post()->newLabel("phone", trans("mobile-salam::devices.phone"), ["parent_id" => $parent->id]);
        post()->newLabel("watch", trans("mobile-salam::devices.watch"), ["parent_id" => $parent->id]);
    }



    /**
     * seed brand labels
     *
     * @return void
     */
    private function seedBrandLabels()
    {
        /*-----------------------------------------------
        | Tablet ...
        */
        post()->newLabel("tablet-apple", trans("mobile-salam::devices.apple"),
             ["parent_id" => label("tablet")->id]
        );
        post()->newLabel("tablet-samsung", trans("mobile-salam::devices.samsung"),
             ["parent_id" => label("tablet")->id]
        );

        /*-----------------------------------------------
        | Phone ...
        */
        post()->newLabel("phone-apple", trans("mobile-salam::devices.apple"),
             ["parent_id" => label("phone")->id]
        );
        post()->newLabel("phone-samsung", trans("mobile-salam::devices.samsung"),
             ["parent_id" => label("phone")->id]
        );
        post()->newLabel("phone-huawei", trans("mobile-salam::devices.huawei"),
             ["parent_id" => label("phone")->id]
        );

        /*-----------------------------------------------
        | Watch ...
        */
        post()->newLabel("watch-apple", trans("mobile-salam::devices.apple"),
             ["parent_id" => label("watch")->id]
        );
        post()->newLabel("watch-samsung", trans("mobile-salam::devices.samsung"),
             ["parent_id" => label("watch")->id]
        );
    }



    /**
     * seed series labels
     *
     * @return void
     */
    private function seedSeriesLabels()
    {
        /*-----------------------------------------------
        | Samsung ...
        */
        post()->newLabel("phone-samsung-s", trans("mobile-salam::devices.series.s"), [
             "parent_id" => label("phone-samsung")->id,
        ]);

        post()->newLabel("phone-samsung-a", trans("mobile-salam::devices.series.a"), [
             "parent_id" => label("phone-samsung")->id,
        ]);

        post()->newLabel("phone-samsung-j", trans("mobile-salam::devices.series.j"), [
             "parent_id" => label("phone-samsung")->id,
        ]);

        post()->newLabel("phone-samsung-note", trans("mobile-salam::devices.series.note"), [
             "parent_id" => label("phone-samsung")->id,
        ]);

        post()->newLabel("phone-samsung-misc", trans("mobile-salam::devices.series.misc"), [
             "parent_id" => label("phone-samsung")->id,
        ]);

        /*-----------------------------------------------
        | Huawei ...
        */

        post()->newLabel("phone-huawei-g", trans("mobile-salam::devices.series.g"), [
             "parent_id" => label("phone-huawei")->id,
        ]);

        post()->newLabel("phone-huawei-mate", trans("mobile-salam::devices.series.mate"), [
             "parent_id" => label("phone-huawei")->id,
        ]);

        post()->newLabel("phone-huawei-p", trans("mobile-salam::devices.series.p"), [
             "parent_id" => label("phone-huawei")->id,
        ]);

        post()->newLabel("phone-huawei-y", trans("mobile-salam::devices.series.y"), [
             "parent_id" => label("phone-huawei")->id,
        ]);

        post()->newLabel("phone-huawei-nova", trans("mobile-salam::devices.series.nova"), [
             "parent_id" => label("phone-huawei")->id,
        ]);

        post()->newLabel("phone-huawei-honor", trans("mobile-salam::devices.series.honor"), [
             "parent_id" => label("phone-huawei")->id,
        ]);

        post()->newLabel("phone-huawei-misc", trans("mobile-salam::devices.series.misc"), [
             "parent_id" => label("phone-huawei")->id,
        ]);
    }



    /**
     * seed color labels
     *
     * @return void
     */
    private function seedColorLabels()
    {
        $parent = posttype()->newLabel("colors", trans("mobile-salam::devices.colors"));
        model("posttype", "devices")->attachLabel("colors");

        $colors = (array)trans("mobile-salam::devices.color");

        foreach ($colors as $color => $persian) {
            post()->newLabel("color-$color", $persian, ["parent_id" => $parent->id]);
        }
    }



    /**
     * seed posts
     *
     * @return void
     */
    private function seedPosts()
    {
        $this->seedAppleTablets();
        $this->seedSamsungTablets();

        $this->seedApplePhones();
        $this->seedSamsungPhonesS();
        $this->seedSamsungPhonesA();
        $this->seedSamsungPhonesJ();
        $this->seedSamsungPhonesNote();
        $this->seedSamsungPhonesMisc();
        $this->seedHuaweiPhonesG();
        $this->seedHuaweiPhonesMate();
        $this->seedHuaweiPhonesMisc();
        $this->seedHuaweiPhonesNova();
        $this->seedHuaweiPhonesY();
        $this->seedHuaweiPhonesP();
        $this->seedHuaweiPhonesHonor();

        $this->seedAppleWatches();
        $this->seedSamsungWatches();
    }



    /**
     * seed apple tablets
     *
     * @return void
     */
    private function seedAppleTablets()
    {
        $this->seedOnePost("iPad Pro 2, 12.9 inch", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad Pro 3, 11 inch", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad Pro 3, 12.9 inch", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad Pro, 9.7 inch", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad Pro, 10.5 inch", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad Pro, 12.9 inch", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad Mini", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
        ]);
        $this->seedOnePost("iPad Mini 2", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
        ]);
        $this->seedOnePost("iPad Mini 3", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPad Mini 4", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPad Air", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad Air 2", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPad 1", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
        ]);
        $this->seedOnePost("iPad 2", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
        ]);
        $this->seedOnePost("iPad 3", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPad 4", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad 5", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPad 6", [
             "tablet",
             "tablet-apple",
             "color-grey",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
    }



    /**
     * seed samsung tablets
     *
     * @return void
     */
    private function seedSamsungTablets()
    {
        $this->seedOnePost("Galaxy Tab S4, 10.5 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab A, 8 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy View", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab E, 9.6 inch", [
             "tablet",
             "tablet-samsung",
             "color-seashell",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Tab A, 8 inch", [
             "tablet",
             "tablet-samsung",
             "color-titan",
             "color-blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab S, 8.4 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-bronze",
        ]);
        $this->seedOnePost("Galaxy Tab 4, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Tab Pro, 12.2 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab 3, 7 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy Tab, 8.9 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Tab 2, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Note, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
             "color-silver",
        ]);
        $this->seedOnePost("Galaxy Note, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
             "color-silver",
        ]);
        $this->seedOnePost("Galaxy Tab A, 10.5 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab S3, 9.7 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Tab S2, 9.7 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy Tab 3 V", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Tab 3 Lite, 7 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-black",
             "color-green",
             "color-pink",
             "color-yellow",
        ]);
        $this->seedOnePost("Galaxy Tab S, 10.5 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-bronze",
        ]);
        $this->seedOnePost("Galaxy Note Pro, 12.2 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab Pro, 8.4 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab 3, 8 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy Tab, 7.7 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab 2, 7 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Note, 8 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
             "color-silver",
        ]);
        $this->seedOnePost("Galaxy Tab Active 2", [
             "tablet",
             "tablet-samsung",
             "color-green",
        ]);
        $this->seedOnePost("Galaxy Tab A, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab S2, 8 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy Tab A, 9.7 inch", [
             "tablet",
             "tablet-samsung",
             "color-titan",
             "color-blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab Active", [
             "tablet",
             "tablet-samsung",
             "color-green",
        ]);
        $this->seedOnePost("Galaxy Tab 4, 8 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Note, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab Pro, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Tab, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-brown",
             "color-red",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Tab 3, 10.1 inch", [
             "tablet",
             "tablet-samsung",
             "color-white",
             "color-brown",
             "color-red",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Tab, 7 inch", [
             "tablet",
             "tablet-samsung",
             "color-black",
             "color-white",
             "color-red",
        ]);
    }



    /**
     * seed apple phones
     *
     * @return void
     */
    private function seedApplePhones()
    {
        $this->seedOnePost("iPhone XS Max", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPhone X", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
        ]);
        $this->seedOnePost("iPhone 7 Plus", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
             "color-red",
        ]);
        $this->seedOnePost("iPhone 6s", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPhone SE", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
             "color-red",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPhone 5", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
        ]);
        $this->seedOnePost("iPhone 3G", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
        ]);
        $this->seedOnePost("iPhone XS", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPhone 8 Plus", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
             "color-red",
        ]);
        $this->seedOnePost("iPhone 7", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
             "color-red",
        ]);
        $this->seedOnePost("iPhone 6 Plus", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPhone 5s", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPhone 4s", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
        ]);
        $this->seedOnePost("iPhone 2G", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
        ]);
        $this->seedOnePost("iPhone XR", [
             "phone",
             "phone-apple",
             "color-black",
             "color-red",
             "color-white",
             "color-yellow",
             "color-blue",
             "color-coral",
        ]);
        $this->seedOnePost("iPhone 8", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
             "color-red",
        ]);
        $this->seedOnePost("iPhone 6s Plus", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
             "color-rose_gold",
        ]);
        $this->seedOnePost("iPhone 6", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("iPhone 5c", [
             "phone",
             "phone-apple",
             "color-white",
             "color-yellow",
             "color-green",
             "color-blue",
             "color-pink",
        ]);
        $this->seedOnePost("iPhone 3GS", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
        ]);
        $this->seedOnePost("iPhone 4", [
             "phone",
             "phone-apple",
             "color-black",
             "color-silver",
        ]);
    }



    /**
     * seed samsung phones s series
     *
     * @return void
     */
    private function seedSamsungPhonesS()
    {
        $this->seedOnePost("Galaxy S10 plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-black",
             "color-green",
             "color-blue",
             "color-yellow",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy S10", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-black",
             "color-green",
             "color-blue",
             "color-yellow",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy S10 e", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-black",
             "color-green",
             "color-blue",
             "color-yellow",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy S9 Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-black",
             "color-pink_blue",
             "color-grey",
             "color-purple",
        ]);
        $this->seedOnePost("Galaxy S9", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-black",
             "color-pink_blue",
             "color-grey",
             "color-purple",
        ]);
        $this->seedOnePost("Galaxy S8 Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-black",
             "color-pink_blue",
             "color-grey",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy S8", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-black",
             "color-pink_blue",
             "color-grey",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy S7 Edge", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-black",
             "color-white",
             "color-gold",
             "color-silver",
        ]);
        $this->seedOnePost("Galaxy S7", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-black",
             "color-white",
             "color-gold",
             "color-silver",
        ]);
        $this->seedOnePost("Galaxy S6 Edge Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-ruby_black",
             "color-platinum_gold",
             "color-silver",
             "color-seashell",
        ]);
        $this->seedOnePost("Galaxy S6 Edge", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-seashell",
             "color-ruby_black",
             "color-platinum_gold",
             "color-emerald",
        ]);
        $this->seedOnePost("Galaxy S6", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-seashell",
             "color-ruby_black",
             "color-platinum_gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy S5 Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-coal_black",
             "color-gold",
             "color-amber_blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy S5", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-black",
             "color-white",
             "color-blue",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy S5 mini", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-coal_black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy S4", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-pale_black",
        ]);
        $this->seedOnePost("Galaxy S4 mini", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-pale_black",
        ]);
        $this->seedOnePost("Galaxy S3", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-blue_black",
        ]);
        $this->seedOnePost("Galaxy S3 mini", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy S2", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-blue_black",
        ]);
        $this->seedOnePost("Galaxy S", [
             "phone",
             "phone-samsung",
             "phone-samsung-s",
             "color-white",
             "color-blue_black",
        ]);
    }



    /**
     * seed samsung phones a series
     *
     * @return void
     */
    private function seedSamsungPhonesA()
    {
        $this->seedOnePost("Galaxy A10", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-blue",
             "color-black",
             "color-red",
        ]);
        $this->seedOnePost("Galaxy A50", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-white",
             "color-blue",
             "color-pink_orange",
        ]);
        $this->seedOnePost("Galaxy A30", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-blue",
             "color-red",
        ]);
        $this->seedOnePost("Galaxy A920", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-blue",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy A750", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-blue",
             "color-gold",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy A8 Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-grey",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy A8", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-grey",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy A6 Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-gold",
             "color-blue",
             "color-pale_violet",
        ]);
        $this->seedOnePost("Galaxy A6", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-gold",
             "color-blue",
             "color-pale_violet",
        ]);
        $this->seedOnePost("Galaxy A720", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-sky_blue",
             "color-sand_gold",
             "color-pale_blue",
        ]);
        $this->seedOnePost("Galaxy A520", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-sky_blue",
             "color-sand_gold",
             "color-pale_blue",
        ]);
        $this->seedOnePost("Galaxy A320", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-sky_blue",
             "color-sand_gold",
             "color-pale_blue",
        ]);
        $this->seedOnePost("Galaxy A810", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-seashell",
             "color-black",
             "color-gold",
             "color-green",
        ]);
        $this->seedOnePost("Galaxy A9", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-seashell",
             "color-black",
             "color-gold",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy A710", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-white",
             "color-gold",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy A510", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-white",
             "color-gold",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy A310", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-white",
             "color-gold",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy A7", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-seashell",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy A5", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-seashell",
             "color-black",
             "color-blue",
             "color-silver",
        ]);
        $this->seedOnePost("Galaxy A3", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-seashell",
             "color-black",
             "color-silver",
             "color-pink",
             "color-light_blue",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy A70", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-pink_orange",
             "color-blue",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy A40", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-black",
             "color-white",
             "color-blue",
             "color-pink_orange",
        ]);
        $this->seedOnePost("Galaxy A20", [
             "phone",
             "phone-samsung",
             "phone-samsung-a",
             "color-blue",
             "color-black",
        ]);
    }



    /**
     * seed samsung phones j series
     *
     * @return void
     */
    private function seedSamsungPhonesJ()
    {
        $this->seedOnePost("Galaxy J8", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy J7 Pro", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-blue",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J7 Prime 2", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J7 Prime", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J710", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J7 Duo", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy J7 Core", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J7", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J6 Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-grey",
             "color-red",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy J6", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy J5 Pro", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-blue",
             "color-pink",
             "color-gold",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy J5 Prime", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J510", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J5", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J4 Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-pink",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy J4 Core", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy J4", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-grey",
        ]);
        $this->seedOnePost("Galaxy J3 Pro", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J320", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-blue",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J3", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J2 Pro", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-silver",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy J2 Core", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-black",
             "color-gold",
             "color-pale_violet",
        ]);
        $this->seedOnePost("Galaxy J2", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy J1 Ace", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy J1", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-white",
             "color-black",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy J1 mini Prime", [
             "phone",
             "phone-samsung",
             "phone-samsung-j",
             "color-gold",
             "color-black",
             "color-white",
        ]);
    }



    /**
     * seed samsung phones note series
     *
     * @return void
     */
    private function seedSamsungPhonesNote()
    {
        $this->seedOnePost("Galaxy Note 9", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-copper",
             "color-purple",
             "color-blue",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Note 8", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-black",
             "color-gold",
             "color-grey",
             "color-blue",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy Note 7", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-blue",
             "color-pink_orange",
             "color-gold",
             "color-titan",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Note 5", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-ruby_black",
             "color-platinum_gold",
             "color-silver",
             "color-seashell",
        ]);
        $this->seedOnePost("Galaxy Note Edge", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Note 4", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-white",
             "color-coal_black",
             "color-gold",
             "color-pink",
        ]);
        $this->seedOnePost("Galaxy Note 3", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-black",
             "color-white",
             "color-green",
        ]);
        $this->seedOnePost("Galaxy Note 2", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-grey",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Note", [
             "phone",
             "phone-samsung",
             "phone-samsung-note",
             "color-black",
             "color-white",
        ]);
    }



    /**
     * seed samsung phones misc series
     *
     * @return void
     */
    private function seedSamsungPhonesMisc()
    {
        $this->seedOnePost("Galaxy Alpha", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-coal_black",
             "color-silver",
             "color-white",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy C9 Pro", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy C9", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy C7", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-silver",
             "color-grey",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy C5", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-silver",
             "color-grey",
             "color-gold",
        ]);
        $this->seedOnePost("Galaxy E7", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-white",
             "color-brown",
        ]);
        $this->seedOnePost("Galaxy E5", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-white",
             "color-brown",
        ]);
        $this->seedOnePost("Galaxy Nexus", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Fit", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Gio", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy K zoom", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-coal_black",
             "color-amber_blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Star", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-white",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy Young 2", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-coal_black",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Young", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy One 8", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-coal_black",
             "color-amber_blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy One 7", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-coal_black",
             "color-amber_blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy One 5", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-coal_black",
             "color-amber_blue",
             "color-white",
        ]);
        $this->seedOnePost("Galaxy Mega", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Grand Prime Pro", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Galaxy Grand Prime Plus", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-gold",
             "color-silver",
        ]);
        $this->seedOnePost("Galaxy Core 2", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Galaxy Core Prime", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-black",
             "color-white",
             "color-grey",
        ]);
        $this->seedOnePost("Galaxy Grand Prime", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-white",
             "color-grey",
        ]);
        $this->seedOnePost("Galaxy Grand 2", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-white",
             "color-grey",
        ]);
        $this->seedOnePost("Galaxy Core", [
             "phone",
             "phone-samsung",
             "phone-samsung-misc",
             "color-blue",
             "color-white",
        ]);
    }



    /**
     * seed huawei phones g series
     *
     * @return void
     */
    private function seedHuaweiPhonesG()
    {
        $this->seedOnePost("G300", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-silver",
        ]);
        $this->seedOnePost("G510", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G535", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G615", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G7", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
        ]);
        $this->seedOnePost("G8", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("G312", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
        ]);
        $this->seedOnePost("G525", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G600", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G620", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G700", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G9 (P9 Lite)", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("G330", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-pale_black",
             "color-silver",
        ]);
        $this->seedOnePost("G526", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G610", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G630", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G740", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("G9 Plus", [
             "phone",
             "phone-huawei",
             "phone-huawei-g",
             "color-black",
             "color-white",
             "color-gold",
        ]);
    }



    /**
     * seed huawei phones mate series
     *
     * @return void
     */
    private function seedHuaweiPhonesMate()
    {
        $this->seedOnePost("Mate X", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-blue",
        ]);
        $this->seedOnePost("Mate 10 Pro", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-dark_blue",
             "color-grey",
             "color-brown",
             "color-gold",
        ]);
        $this->seedOnePost("Mate 9", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-grey",
             "color-moon",
             "color-light_silver",
             "color-gold",
             "color-brown",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Mate SE", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-gold",
             "color-grey",
        ]);
        $this->seedOnePost("Mate 2", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Mate 20", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-dark_blue",
             "color-twilight",
             "color-black",
        ]);
        $this->seedOnePost("Mate 20 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-blue",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Mate 20 Pro", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-green",
             "color-dark_blue",
             "color-twilight",
             "color-gold",
             "color-black",
        ]);
        $this->seedOnePost("Mate 10", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-brown",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Mate 9 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-grey",
             "color-gold",
             "color-silver",
        ]);
        $this->seedOnePost("Mate S", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-gold",
             "color-grey",
        ]);
        $this->seedOnePost("Mate", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Mate 20 X", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-dark_blue",
             "color-silver",
        ]);
        $this->seedOnePost("Mate 10 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Mate 9 Pro", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-gold",
             "color-grey",
        ]);
        $this->seedOnePost("Mate 8", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-gold",
             "color-moon",
             "color-light_silver",
             "color-grey",
             "color-brown",
        ]);
        $this->seedOnePost("Mate 7", [
             "phone",
             "phone-huawei",
             "phone-huawei-mate",
             "color-black",
             "color-moon",
             "color-light_silver",
             "color-gold",
        ]);
    }



    /**
     * seed huawei phones misc series
     *
     * @return void
     */
    private function seedHuaweiPhonesMisc()
    {
        $this->seedOnePost("GR3", [
             "phone",
             "phone-huawei",
             "phone-huawei-misc",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("GR5 2017", [
             "phone",
             "phone-huawei",
             "phone-huawei-misc",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("GR3 2017", [
             "phone",
             "phone-huawei",
             "phone-huawei-misc",
             "color-black",
             "color-white",
             "color-gold",
        ]);
    }



    /**
     * seed huawei phones nova series
     *
     * @return void
     */
    private function seedHuaweiPhonesNova()
    {
        $this->seedOnePost("Nova 4", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-blue",
             "color-red",
             "color-white",
             "color-black",
        ]);
        $this->seedOnePost("Nova 3", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-black",
             "color-blue",
             "color-purple",
             "color-gold",
        ]);
        $this->seedOnePost("Nova 2s", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-black",
             "color-grey",
             "color-red",
             "color-gold",
             "color-light_blue",
        ]);
        $this->seedOnePost("Nova 3i", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-black",
             "color-seashell",
             "color-purple",
        ]);
        $this->seedOnePost("Nova Lite 2", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Nova 2 Plus", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-blue",
             "color-green",
             "color-gold",
             "color-black",
        ]);
        $this->seedOnePost("Nova 3e", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-blue",
             "color-pink",
             "color-black",
        ]);
        $this->seedOnePost("Nova 2i", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Nova Plus", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-gold",
             "color-silver",
             "color-grey",
        ]);
        $this->seedOnePost("Nova 2", [
             "phone",
             "phone-huawei",
             "phone-huawei-nova",
             "color-blue",
             "color-green",
             "color-gold",
             "color-black",
        ]);
    }



    /**
     * seed huawei phones y series
     *
     * @return void
     */
    private function seedHuaweiPhonesY()
    {
        $this->seedOnePost("Y9 (2019)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-blue",
             "color-purple",
        ]);
        $this->seedOnePost("Y7", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-grey",
             "color-prestige_gold",
             "color-silver",
        ]);
        $this->seedOnePost("Y550", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y511", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y321", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y220", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y3 (2018)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-gold",
             "color-white",
             "color-grey",
        ]);
        $this->seedOnePost("Y3 (2017)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-gold",
             "color-pink",
             "color-blue",
             "color-white",
             "color-grey",
        ]);
        $this->seedOnePost("Y3 (2015)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y9 Prime", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Y635", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y530", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
             "color-red",
             "color-yellow",
        ]);
        $this->seedOnePost("Y360", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y6 (2019)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-blue",
             "color-brown",
        ]);
        $this->seedOnePost("Y320", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y210", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y5 (2018)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Y5 (2017)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-gold",
             "color-pink",
             "color-blue",
             "color-white",
             "color-grey",
        ]);
        $this->seedOnePost("Y5 (2015)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
             "color-gold",
             "color-pink",
             "color-blue",
        ]);
        $this->seedOnePost("Y7 Prime", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-silver",
             "color-grey",
             "color-gold",
        ]);
        $this->seedOnePost("Y560", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
             "color-pink",
             "color-blue",
        ]);
        $this->seedOnePost("Y520", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
             "color-blue",
             "color-pink",
        ]);
        $this->seedOnePost("Y330", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
             "color-red",
             "color-yellow",
        ]);
        $this->seedOnePost("Y300", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Y200", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
        ]);
        $this->seedOnePost("Y6 (2018)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Y6 (2017)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-white",
             "color-gold",
             "color-grey",
        ]);
        $this->seedOnePost("Y6 (2015)", [
             "phone",
             "phone-huawei",
             "phone-huawei-y",
             "color-white",
             "color-black",
             "color-gold",
        ]);
    }



    /**
     * seed huawei phones p series
     *
     * @return void
     */
    private function seedHuaweiPhonesP()
    {
        $this->seedOnePost("P Smart", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-black",
             "color-blue",
             "color-gold",
        ]);
        $this->seedOnePost("P20 Pro", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-twilight",
             "color-black",
             "color-dark_blue",
             "color-gold",
        ]);
        $this->seedOnePost("P9 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("P8 Max", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-gold",
             "color-grey",
        ]);
        $this->seedOnePost("P7", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-black",
             "color-white",
             "color-pink",
        ]);
        $this->seedOnePost("P Smart Plus", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-black",
             "color-blue",
        ]);
        $this->seedOnePost("P20", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-twilight",
             "color-black",
             "color-dark_blue",
             "color-gold",
        ]);
        $this->seedOnePost("P10 Plus", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-white",
             "color-blue",
             "color-gold",
             "color-black",
             "color-silver",
             "color-ery_green",
        ]);
        $this->seedOnePost("P10", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-silver",
             "color-gold",
             "color-black",
             "color-blue",
             "color-ery_green",
        ]);
        $this->seedOnePost("P9", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-white",
             "color-gold",
             "color-grey",
             "color-silver",
        ]);
        $this->seedOnePost("P8", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-grey",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("P6", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-black",
             "color-white",
             "color-pink",
        ]);
        $this->seedOnePost("P20 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-blue",
             "color-pink",
             "color-black",
        ]);
        $this->seedOnePost("P10 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-seashell",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("P9 Plus", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-gold",
             "color-grey",
             "color-white",
        ]);
        $this->seedOnePost("P8 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("P2", [
             "phone",
             "phone-huawei",
             "phone-huawei-p",
             "color-black",
             "color-white",
        ]);
    }



    /**
     * seed huawei phones honor series
     *
     * @return void
     */
    private function seedHuaweiPhonesHonor()
    {
        $this->seedOnePost("Honor 8X", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
             "color-red",
             "color-pink",
        ]);
        $this->seedOnePost("Honor 8C", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
             "color-gold",
        ]);
        $this->seedOnePost("Honor Note 10", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
        ]);
        $this->seedOnePost("Honor 10", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-blue",
             "color-green",
             "color-black",
             "color-grey",
        ]);
        $this->seedOnePost("Honor 9 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
             "color-grey",
             "color-kohl",
             "color-magic_nightfall",
             "color-seashell",
        ]);
        $this->seedOnePost("Honor V8", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-silver",
             "color-gold",
        ]);
        $this->seedOnePost("Honor 7S", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Honor 7A", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Honor 6 Plus", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("Honor 3X", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Honor 2", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Honor Magic 2", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-blue",
             "color-purple",
        ]);
        $this->seedOnePost("Honor 8X Max", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
             "color-red",
        ]);
        $this->seedOnePost("Honor Play", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
             "color-violet",
        ]);
        $this->seedOnePost("Honor V20", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-blue",
             "color-red",
             "color-magic_black",
        ]);
        $this->seedOnePost("Honor 9", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
             "color-purple",
             "color-lemon",
        ]);
        $this->seedOnePost("Honor 5A", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-white",
             "color-yellow",
             "color-pink",
             "color-black",
             "color-gold",
             "color-azure",
        ]);
        $this->seedOnePost("Honor 7C", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-red",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Honor 7", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("Honor 6", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Honor 3C", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Honor", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
             "color-gold",
        ]);
        $this->seedOnePost("Honor Magic", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-white",
        ]);
        $this->seedOnePost("Honor 7S", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Honor 10 Lite", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-white",
             "color-black",
             "color-blue",
             "color-red",
        ]);
        $this->seedOnePost("Honor V10", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-blue",
             "color-black",
             "color-gold",
             "color-red",
        ]);
        $this->seedOnePost("Honor 8 Pro", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-gold",
             "color-blue",
        ]);
        $this->seedOnePost("Honor 8", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-seashell",
             "color-gold",
             "color-black",
             "color-pink",
             "color-blue",
        ]);
        $this->seedOnePost("Honor 7X", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-black",
             "color-blue",
             "color-gold",
        ]);
        $this->seedOnePost("Honor 6X", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-grey",
             "color-gold",
             "color-silver",
        ]);
        $this->seedOnePost("Honor 4A", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-white",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Honor 3", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-white",
        ]);
        $this->seedOnePost("Honor 5C", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-silver",
             "color-black",
             "color-gold",
        ]);
        $this->seedOnePost("Honor 5X", [
             "phone",
             "phone-huawei",
             "phone-huawei-honor",
             "color-white",
             "color-black",
             "color-gold",
        ]);
    }



    /**
     * seed apple watches
     *
     * @return void
     */
    private function seedAppleWatches()
    {
        $this->seedOnePost("Apple Watch (38 mm)", [
             "watch",
             "watch-apple",
        ]);
        $this->seedOnePost("Apple Watch (42 mm)", [
             "watch",
             "watch-apple",
        ]);
        $this->seedOnePost("Apple Watch 2 (38 mm)", [
             "watch",
             "watch-apple",
        ]);
        $this->seedOnePost("Apple Watch 2 (42 mm)", [
             "watch",
             "watch-apple",
        ]);
        $this->seedOnePost("Apple Watch 3 (38 mm)", [
             "watch",
             "watch-apple",
        ]);
        $this->seedOnePost("Apple Watch 3 (42 mm)", [
             "watch",
             "watch-apple",
        ]);
        $this->seedOnePost("Apple Watch 4 (40 mm)", [
             "watch",
             "watch-apple",
        ]);
        $this->seedOnePost("Apple Watch 4 (44 mm)", [
             "watch",
             "watch-apple",
        ]);
    }



    /**
     * seed samsung watches
     *
     * @return void
     */
    private function seedSamsungWatches()
    {
        $this->seedOnePost("Galaxy Gear", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear 2", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear S", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear S2", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear S3", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear Sport", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear Fit", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear Fit 2", [
             "watch",
             "watch-samsung",
        ]);
        $this->seedOnePost("Gear Fit2 Pro", [
             "watch",
             "watch-samsung",
        ]);
    }



    /**
     * seed a single post, with its labels
     *
     * @param string $title
     * @param array  $labels
     *
     * @return void
     */
    private function seedOnePost($title, array $labels = [])
    {
        if(post()->grabSlug(str_slug($title))->exists) {
            return;
        }

        post()->saveAsDevice($title, $labels);
    }
}
