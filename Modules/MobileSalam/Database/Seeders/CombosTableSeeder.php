<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;

class CombosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = trans("mobile-salam::combo");

        foreach ($list as $subject => $array) {
            $this->seedArray($subject, $array);
        }
    }



    /**
     * seed the array
     *
     * @param string $subject
     * @param array  $array
     */
    private function seedArray(string $subject, array $array)
    {
        $order = 1;

        foreach ($array as $key => $title) {
            combo()::seed($subject, $key, $title, $order);
            $order++;
        }
    }
}
