<?php

namespace Modules\MobileSalam\Database\Seeders;

use App\Models\SalamOrder;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedSomeUsers();
        $this->spreadRolesAcrossUsers();
        $this->seedSomeOrders();
        $this->seedSomeIndents();
    }



    /**
     * seed some users
     *
     * @return void
     */
    private function seedSomeUsers()
    {
        $data = [];

        for ($i = 1; $i <= 100; $i++) {
            $data[] = [
                 "code_melli" => dummy()::codeMelli(),
                 "email"      => dummy()::email(),
                 "name_first" => dummy()::persianName(),
                 "name_last"  => dummy()::persianFamily(),
                 "mobile"     => "09" . rand(100000000, 399999999),
                 "password"   => Hash::make(1),
            ];
        }

        yasna()::seed("users", $data);
    }



    /**
     * spread some roles across users
     *
     * @return void
     */
    private function spreadRolesAcrossUsers()
    {
        $i = 0;
        /** @var User $user */
        foreach (user()->where('id', '>', 1)->limit(100)->get() as $user) {
            $i++;

            if ($i < 3) {
                $user->attachRole("super-operator");
                $user->attachRole("operator");
                continue;
            }

            if ($i < 10) {
                $user->attachRole("operator");
                continue;
            }

            if ($i < 20) {
                $user->attachRole("repairer");
                continue;
            }

            if ($i < 30) {
                $user->attachRole("supplier");
                continue;
            }

            $user->attachRole("member");
        }
    }



    /**
     * seed some orders
     *
     * @return void
     */
    private function seedSomeOrders()
    {
        $data = [];

        for ($i = 1; $i <= 200; $i++) {
            $post   = posttype("devices")->posts()->inRandomOrder()->first();
            $colors = explode(",", $post->getMeta("labeled_colors"));
            $color  = empty($colors) ? 0 : label(array_random($colors))->id;

            $data[] = [
                 "customer_id"        => role('member')->users()->inRandomOrder()->first()->id,
                 "tracking_number"    => SalamOrder::getRandomTracking(),
                 "application_type"   => array_random(array_keys(trans("mobile-salam::combo.application-type"))),
                 "post_id"            => $post->id,
                 "color_id"           => $color,
                 "is_repaired_before" => rand(0, 1),
                 "customer_note"      => dummy()::persianText(),
                 "estimated_price"    => rand(1000, 100000),
                 "estimated_time"     => rand(1, 10),
            ];

        }

        yasna()::seed("salam_orders", $data);
    }



    /**
     * seed some indents
     *
     * @return void
     */
    private function seedSomeIndents()
    {
        $indent = [];

        for ($i = 1; $i < 200; $i++) {
            $post = posttype("devices")->posts()->inRandomOrder()->first();

            $indent[] = [
                 'ware_id'       => $post->id,
                 'order_id'      => model("salam_order")->inRandomOrder()->first()->id,
                 'defect_id'     => rand(1, 10),
                 'repairer_id'   => role('repairer')->users()->inRandomOrder()->first()->id,
                 'quantity'      => rand(1, 10),
                 'repairer_note' => dummy()::persianText(),
                 'supplier_note' => dummy()::persianText(),
                 'price'         => rand(1000, 100000),
                 'supply_status' => "supply-status-" . array_random(array_keys(trans("mobile-salam::combo.supply-status"))),
                 'return_reason' => "return-reason-" . array_random(array_keys(trans("mobile-salam::combo.return-reason"))),
            ];

        }
        yasna()::seed("salam_indents", $indent);
    }
}
