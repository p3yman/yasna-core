<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->resetCurrentEntries();
        $this->seedNewEntries();
        $this->seedRequestHelpSteps();
    }



    /**
     * reset some current entries
     *
     * @return void
     */
    private function resetCurrentEntries()
    {
        setting("username_fields")->setValue([
             "email",
             "mobile",
             "code_melli",
        ]);

        setting("site_locales")->setValue("fa");
    }



    /**
     * seed some new entries
     *
     * @return void
     */
    private function seedNewEntries()
    {
        $data = [
             [
                  'slug'             => 'shopping_enabled',
                  'title'            => trans('mobile-salam::seeder.shopping_enabled'),
                  'category'         => 'template',
                  'order'            => '102',
                  'data_type'        => 'boolean',
                  'default_value'    => 1,
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'repair_order_delivery_time_bar',
                  'title'            => trans('mobile-salam::seeder.repair_order_delivery_time_bar'),
                  "hint"             => trans('mobile-salam::seeder.repair_order_delivery_time_bar_hint'),
                  'category'         => 'template',
                  'order'            => '102',
                  'data_type'        => 'text',
                  'default_value'    => 10,
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'mobile_salam_location_city_id',
                  'title'            => trans('mobile-salam::seeder.mobile_salam_location_city_id'),
                  "hint"             => trans('mobile-salam::seeder.mobile_salam_location_city_id_hint'),
                  'category'         => 'upstream',
                  'order'            => '103',
                  'data_type'        => 'text',
                  'default_value'    => 135,
                  'custom_value'     => 135,
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'mobile_salam_location_address',
                  'title'            => trans('mobile-salam::seeder.mobile_salam_location_address'),
                  "hint"             => trans('mobile-salam::mobile_salam_location_address_hint'),
                  'category'         => 'upstream',
                  'order'            => '103',
                  'data_type'        => 'text',
                  'default_value'    => 'some where in tehran',
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'mobile_salam_location_lat',
                  'title'            => trans('mobile-salam::seeder.mobile_salam_location_lat'),
                  "hint"             => trans('mobile-salam::seeder.mobile_salam_location_lat_hint'),
                  'category'         => 'upstream',
                  'order'            => '104',
                  'data_type'        => 'text',
                  'default_value'    => 10,
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'mobile_salam_location_lng',
                  'title'            => trans('mobile-salam::seeder.mobile_salam_location_lng'),
                  "hint"             => trans('mobile-salam::seeder.mobile_salam_location_lng_hint'),
                  'category'         => 'upstream',
                  'order'            => '105',
                  'data_type'        => 'text',
                  'default_value'    => 10,
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'seo_keywords',
                  'title'            => trans('mobile-salam::seeder.seo_keywords'),
                  'category'         => 'template',
                  'order'            => '106',
                  'data_type'        => 'textarea',
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'seo_meta',
                  'title'            => trans('mobile-salam::seeder.seo_meta'),
                  'category'         => 'template',
                  'order'            => '107',
                  'data_type'        => 'textarea',
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'wage_repair_percent',
                  'title'            => trans('mobile-salam::seeder.wage_repair_percent'),
                  'category'         => 'template',
                  'order'            => '108',
                  'data_type'        => 'text',
                  'default_value'    => 20,
                  "api_discoverable" => "1",
             ],
        ];

        yasna()::seed("settings", $data);
    }



    /**
     * seed some new repair request step title
     *
     * @return void
     */
    private function seedRequestHelpSteps()
    {
        $data = [
             [
                  'slug'             => 'request_help_step_1',
                  'title'            => trans('mobile-salam::seeder.request_help_title.1'),
                  'category'         => 'template',
                  'order'            => '111',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.1'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_2',
                  'title'            => trans('mobile-salam::seeder.request_help_title.2'),
                  'category'         => 'template',
                  'order'            => '112',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.2'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_3',
                  'title'            => trans('mobile-salam::seeder.request_help_title.3'),
                  'category'         => 'template',
                  'order'            => '113',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.3'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_4',
                  'title'            => trans('mobile-salam::seeder.request_help_title.4'),
                  'category'         => 'template',
                  'order'            => '114',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.4'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_5',
                  'title'            => trans('mobile-salam::seeder.request_help_title.5'),
                  'category'         => 'template',
                  'order'            => '115',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.5'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_6',
                  'title'            => trans('mobile-salam::seeder.request_help_title.6'),
                  'category'         => 'template',
                  'order'            => '116',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.6'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_7',
                  'title'            => trans('mobile-salam::seeder.request_help_title.7'),
                  'category'         => 'template',
                  'order'            => '117',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.7'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_8',
                  'title'            => trans('mobile-salam::seeder.request_help_title.8'),
                  'category'         => 'template',
                  'order'            => '118',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.8'),
                  "api_discoverable" => "1",
             ],
             [
                  'slug'             => 'request_help_step_9',
                  'title'            => trans('mobile-salam::seeder.request_help_title.9'),
                  'category'         => 'template',
                  'order'            => '119',
                  'data_type'        => 'textarea',
                  'default_value'    => trans('mobile-salam::seeder.request_help_step.9'),
                  "api_discoverable" => "1",
             ],
        ];

        yasna()::seed("settings", $data);
    }
}
