<?php

namespace Modules\MobileSalam\Database\Seeders;

use Illuminate\Database\Seeder;

class MapLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        mapper()::db()->url("faq")->changeFrequency(mapper()::MONTHLY)->priority(0.5)->insert();
        mapper()::db()->url("contact-us")->changeFrequency(mapper()::NEVER)->priority(0.1)->insert();
        mapper()::db()->url("learning")->changeFrequency(mapper()::WEEKLY)->priority(0.6)->insert();
        mapper()::db()->url("news")->changeFrequency(mapper()::DAILY)->priority(0.6)->insert();
        mapper()::db()->url("app")->changeFrequency(mapper()::WEEKLY)->priority(0.6)->insert();
    }
}
