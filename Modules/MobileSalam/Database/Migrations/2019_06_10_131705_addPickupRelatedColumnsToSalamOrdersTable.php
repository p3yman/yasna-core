<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPickupRelatedColumnsToSalamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->timestamp("eta_received_at")->after("rejected_at")->nullable();  // for customer > shop cases
            $table->timestamp("eta_delivered_at")->after("rejected_at")->nullable(); // for shop > customer cases
            $table->unsignedInteger("receive_active_pickup")->after("is_repaired_before")->nullable();
            $table->unsignedInteger("deliver_active_pickup")->after("is_repaired_before")->nullable();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->dropColumn([
                 "eta_received_at",
                 "eta_delivered_at",
                 "receive_active_pickup",
                 "deliver_active_pickup",
            ]);
        });
    }
}
