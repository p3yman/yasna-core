<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentDetailsToSalamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->float("amount_paid", 14, 2)->default(0)->after("guarantee_expires_at");
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->dropColumn([
                 "amount_paid",
            ]);
        });
    }
}
