<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('salam_orders');

        Schema::create('salam_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("customer_id")->index();
            $table->unsignedInteger("repairer_id")->default(0)->index();

            $table->string("tracking_number")->index();
            $table->string("application_type")->index();

            $table->unsignedInteger("post_id")->default(0)->index();
            $table->unsignedInteger("color_id")->default(0)->index();
            $table->string("serial")->nullable();
            $table->string("unknown_device")->nullable();
            $table->string("unknown_problem")->nullable();
            $table->boolean("is_repaired_before")->default(0);
            $table->text("customer_note")->nullable();

            $table->float('estimated_price', 15, 2)->nullable();
            $table->integer('estimated_time')->nullable();
            $table->float('final_price', 15, 2)->nullable();

            $table->timestamp("guarantee_expires_at")->nullable()->index();

            $this->addStatusColumns($table, "confirmed");
            $this->addStatusColumns($table, "received");
            $this->addStatusColumns($table, "dispatched");
            $this->addStatusColumns($table, "delivered");
            $this->addStatusColumns($table, "repaired");
            $this->addStatusColumns($table, "rejected");
            $this->addStatusColumns($table, "closed");
            $this->addStatusColumns($table, "claimed");

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salam_orders');
    }



    /**
     * add status columns
     *
     * @param Blueprint $table
     * @param string    $field
     *
     * @return void
     */
    private function addStatusColumns(Blueprint $table, string $field)
    {
        $table->timestamp($field . "_at")->nullable();
        $table->unsignedInteger($field . "_by")->default(0);

    }
}
