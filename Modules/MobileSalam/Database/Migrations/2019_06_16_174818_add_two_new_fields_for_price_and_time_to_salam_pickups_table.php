<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwoNewFieldsForPriceAndTimeToSalamPickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_pickups', function (Blueprint $table) {
            $table->text("pickup_price")->after("destination_addr")->nullable();
            $table->timestamp("pickup_time")->after("pickup_note")->nullable();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_pickups', function (Blueprint $table) {
            $table->dropColumn(["pickup_price", "pickup_time"]);
        });
    }
}
