<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWageCalculationsToSalamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $after = "eta_receive_created_at";

            $table->unsignedInteger("wage_calculated_by")->default(0)->after($after);
            $table->timestamp("wage_calculated_at")->nullable()->after($after);
            $table->text("wage_custom_desc")->nullable()->after($after);
            $table->float("wage_added_amount", 14, 2)->default(0)->after($after);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->dropColumn([
                 "wage_calculated_at",
                 "wage_calculated_by",
                 "wage_custom_desc",
                 "wage_added_amount",
            ]);
        });
    }
}
