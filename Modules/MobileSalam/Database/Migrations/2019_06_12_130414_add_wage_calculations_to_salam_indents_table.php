<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWageCalculationsToSalamIndentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_indents', function (Blueprint $table) {
            $after = "return_rejected_at";

            $table->dropColumn("price");

            $table->unsignedInteger("wage_modified_by")->default(0)->after($after);
            $table->timestamp("wage_modified_at")->nullable()->after($after);
            $table->float("wage_final", 14, 2)->default(0)->after($after);
            $table->float("wage_modified", 14, 2)->default(0)->after($after);
            $table->float("wage_calculated", 14, 2)->default(0)->after($after);
            $table->float("wage_percent")->default(0)->after($after);
            $table->float("amount_for_repairer", 14, 2)->default(0)->after($after);
            $table->float("amount_in_shop", 14, 2)->default(0)->after($after);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_indents', function (Blueprint $table) {
            $table->dropColumn([
                 "amount_in_shop",
                 "amount_for_repairer",
                 "wage_percent",
                 "wage_calculated",
                 "wage_modified",
                 "wage_final",
                 "wage_modified_at",
                 "wage_modified_by",
            ]);
        });
    }
}
