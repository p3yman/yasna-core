<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalamNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('salam_logs');

        Schema::create('salam_nodes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger("order_id")->index();
            $table->string("class")->index();

            $table->timestamps();

            $table->timestamp("approved_at")->nullable();
            $table->unsignedInteger("approved_by")->default(0);

            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salam_nodes');
    }
}
