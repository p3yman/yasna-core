<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToSalamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->string("name_first")->after("repairer_id")->index();
            $table->string("name_last")->after("name_first")->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->dropColumn("name_first");
            $table->dropColumn("name_last");
        });
    }
}
