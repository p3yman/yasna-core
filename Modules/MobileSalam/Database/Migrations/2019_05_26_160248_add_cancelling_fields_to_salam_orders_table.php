<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCancellingFieldsToSalamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->timestamp("cancelled_at")->after("rejected_at")->nullable();
            $table->unsignedInteger("cancelled_by")->after("cancelled_at")->default(0);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->dropColumn(["cancelled_at", "cancelled_by"]);
        });
    }
}
