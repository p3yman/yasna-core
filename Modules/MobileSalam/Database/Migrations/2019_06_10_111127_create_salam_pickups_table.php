<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalamPickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('salam_pickup');

        Schema::create('salam_pickups', function (Blueprint $table) {
            $table->increments('id');
            // basic required data
            $table->unsignedInteger('order_id')->index();
            $table->text('pickup_type'); // send - receive - custom
            // origin detail (required on alopeyk)
            $table->unsignedInteger('origin_city_id')->nullable()->index();
            $table->decimal('origin_lat', 10, 7)->nullable();
            $table->decimal('origin_lng', 10, 7)->nullable();
            $table->longText('origin_addr')->nullable();
            // destination detail (required on alopeyk)
            $table->unsignedInteger('destination_city_id')->nullable()->index();
            $table->decimal('destination_lat', 10, 7)->nullable();
            $table->decimal('destination_lng', 10, 7)->nullable();
            $table->longText('destination_addr')->nullable();
            // driver detail (required on custom bike & performance booster on alopeyk)
            $table->text('driver_full_name')->nullable();
            $table->text('driver_phone_number')->nullable();
            // upstream response detail on alopeyk
            $table->text('delivery_order_id')->nullable();
            $table->text('delivery_tracking_url')->nullable();
            // not very serious
            $table->longText('pickup_note')->nullable();
            // default
            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salam_pickups');
    }
}
