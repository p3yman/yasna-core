<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMethodColumnToSalamPickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_pickups', function (Blueprint $table) {
            $table->text("pickup_method")->after("order_id"); // owner - alopeyk - custom
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_pickups', function (Blueprint $table) {
            $table->dropColumn(["pickup_method"]);
        });
    }
}
