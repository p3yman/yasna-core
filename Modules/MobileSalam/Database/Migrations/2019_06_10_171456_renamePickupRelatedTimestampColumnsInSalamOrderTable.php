<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamePickupRelatedTimestampColumnsInSalamOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->renameColumn('eta_received_at', 'eta_receive_created_at')->change();
            $table->renameColumn('eta_delivered_at', 'eta_deliver_created_at')->change();

        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_orders', function (Blueprint $table) {
            $table->renameColumn('eta_receive_created_at', 'eta_received_at')->change();
            $table->renameColumn('eta_deliver_created_at', 'eta_delivered_at')->change();

        });
    }
}
