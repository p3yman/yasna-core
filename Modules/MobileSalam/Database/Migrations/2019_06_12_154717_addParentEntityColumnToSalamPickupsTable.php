<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentEntityColumnToSalamPickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salam_pickups', function (Blueprint $table) {
            $table->text("parent_entity")->after("id");
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salam_pickups', function (Blueprint $table) {
            $table->dropColumn(["parent_entity"]);
        });
    }
}
