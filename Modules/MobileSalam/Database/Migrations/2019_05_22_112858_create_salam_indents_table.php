<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalamIndentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salam_indents', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('post_id')->index();
            $table->unsignedInteger('order_id')->index();
            $table->unsignedInteger('defect_id')->index();

            $table->integer('repairer_id')->default(0)->index();
            $table->integer('supplier_id')->default(0);

            $table->unsignedInteger('quantity')->default(1);
            $table->text('repairer_note');
            $table->text('supplier_note');
            $table->string('price');
            $table->string('supply_status')->index();
            $table->string("return_reason")->index();

            $table->timestamp("supplied_at")->nullable()->index();
            $table->timestamp("used_at")->nullable()->index();
            $table->timestamp("returned_at")->nullable()->index();
            $table->timestamp("return_confirmed_at")->nullable()->index();
            $table->timestamp("return_rejected_at")->nullable()->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salam_indents');
    }
}
