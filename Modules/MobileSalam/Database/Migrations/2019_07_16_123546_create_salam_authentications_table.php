<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalamAuthenticationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salam_authentications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mobile')->index();
            $table->string('reset_token')->index();
            $table->timestamp('reset_token_expires_at')->index();
            $table->timestamps();
            yasna()->additionalMigrations($table);

        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salam_authentications');
    }
}
