<?php

namespace Modules\MobileSalam\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Commenting\Entities\Comment;
use Modules\Commenting\Events\CommentReplied;
use Modules\MobileSalam\Notifications\CommentRepliedNotification;

class SendEmailWhenCommentReplied implements ShouldQueue
{
    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param CommentReplied $event
     */
    public function handle(CommentReplied $event)
    {
        $user        = user(1);
        $user->email = $this->targetEmail($event);

        $user->notify(new CommentRepliedNotification($event));
    }



    /**
     * return user [guest_email]
     *
     * @param Comment $event
     *
     * @return string|null
     */
    public function targetEmail($event)
    {
        if (($parent = $event->model->parent) and $parent->exists) {
            if ($parent->guest_email) {
                return $parent->guest_email;
            } elseif (($user = $parent->user) and $user->exists and $user->email) {
                return $user->email;
            }
        }
        return null;
    }
}
