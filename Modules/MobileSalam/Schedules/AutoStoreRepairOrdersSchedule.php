<?php

namespace Modules\MobileSalam\Schedules;

use App\Models\SalamOrder;
use Modules\Yasna\Services\YasnaSchedule;

class AutoStoreRepairOrdersSchedule extends YasnaSchedule
{
    /**
     * @inheritdoc
     */
    protected function job()
    {
        SalamOrder::storeNonDeliveredOrders();
    }



    /**
     * @inheritdoc
     */
    protected function withoutOverlapping()
    {
        return true;
    }
}
