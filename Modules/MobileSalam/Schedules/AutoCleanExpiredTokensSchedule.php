<?php

namespace Modules\MobileSalam\Schedules;

use Modules\MobileSalam\Entities\SalamAuthentication;
use Modules\Yasna\Services\YasnaSchedule;

class AutoCleanExpiredTokensSchedule extends YasnaSchedule
{

    /**
     * @inheritdoc
     */
    protected function job()
    {
        SalamAuthentication::cleanExpiredTokens();
    }



    /**
     * @inheritdoc
     */
    protected function withoutOverlapping()
    {
        return true;
    }
}
