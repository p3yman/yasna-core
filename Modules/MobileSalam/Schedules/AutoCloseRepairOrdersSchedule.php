<?php

namespace Modules\MobileSalam\Schedules;

use App\Models\SalamOrder;
use Modules\Yasna\Services\YasnaSchedule;

class AutoCloseRepairOrdersSchedule extends YasnaSchedule
{
    /**
     * @inheritdoc
     */
    protected function job()
    {
        SalamOrder::cancelDelayedOrders();
    }



    /**
     * @inheritdoc
     */
    protected function withoutOverlapping()
    {
        return true;
    }
}
