<?php

namespace Modules\MobileSalam\Notifications;

use App\Models\User;
use Modules\MobileSalam\Events\SupplyIndentStatusChanged;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class IndentSupplyStatusNotification extends YasnaNotificationAbstract
{
    /** @var SupplyIndentStatusChanged */
    public $event;



    /**
     * RepairOrderSubmittedNotification constructor.
     *
     * @param SupplyIndentStatusChanged $event
     */
    public function __construct(SupplyIndentStatusChanged $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param SupplyIndentStatusChanged $event
     */
    public function handle(SupplyIndentStatusChanged $event)
    {
        $this->operatorNotify($event);
        $this->superOperatorNotify($event);
        $this->repairerNotify($event);
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $message = trans("mobile-salam::messages.sms_on_supply_status");
        $url     = $this->event->model->order->tracking_url;

        return $message . SPACE . $url;
    }



    /**
     * send sms to repairer
     *
     * @param SupplyIndentStatusChanged $event
     */
    public function repairerNotify($event)
    {
        $user = $event->model->repairer;
        /** @var User $user */
        $user->notify(new IndentSupplyStatusNotification($event));

    }



    /**
     *  send sms to operator
     *
     * @param SupplyIndentStatusChanged $event
     */
    public function operatorNotify($event)
    {
        $users = user()->elector(['role' => 'operator'])->get();
        foreach ($users as $user) {
            $user->notify(new IndentSupplyStatusNotification($event));
        }
    }



    /**
     * send sms to super-operator
     *
     * @param SupplyIndentStatusChanged $event
     */
    public function superOperatorNotify($event)
    {
        $users = user()->elector(['role' => 'super-operator'])->get();
        foreach ($users as $user) {
            $user->notify(new IndentSupplyStatusNotification($event));
        }
    }
}
