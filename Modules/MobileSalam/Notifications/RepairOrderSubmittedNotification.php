<?php

namespace Modules\MobileSalam\Notifications;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\MobileSalam\Events\NewRepairOrderSubmitted;
use Modules\Notifier\Messages\BrowserMessage;
use Modules\Notifier\Messages\Parts\BrowserWebButton;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class RepairOrderSubmittedNotification extends YasnaNotificationAbstract
{
    /** @var NewRepairOrderSubmitted */
    public $event;



    /**
     * RepairOrderSubmittedNotification constructor.
     *
     * @param NewRepairOrderSubmitted $event
     */
    public function __construct(NewRepairOrderSubmitted $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param NewRepairOrderSubmitted $event
     */
    public function handle(NewRepairOrderSubmitted $event)
    {
        $user = $event->model->customer;

        $user->notify(new RepairOrderSubmittedNotification($event));
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $message = trans("mobile-salam::messages.sms_on_repair_order_submit");
        $url     = $this->event->model->tracking_url;

        return $message . SPACE . $url;
    }
}
