<?php

namespace Modules\MobileSalam\Notifications;

use Modules\MobileSalam\Events\NewRepairClaimReceived;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class RepairOrderClaimedNotification extends YasnaNotificationAbstract
{
    /** @var NewRepairClaimReceived */
    public $event;



    /**
     * RepairOrderSubmittedNotification constructor.
     *
     * @param NewRepairClaimReceived $event
     */
    public function __construct(NewRepairClaimReceived $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param NewRepairClaimReceived $event
     */
    public function handle(NewRepairClaimReceived $event)
    {
        $user = $event->model->customer;

        $user->notify(new RepairOrderClaimedNotification($event));
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $message = trans("mobile-salam::messages.sms_on_repair_order_claim");
        $url     = $this->event->model->tracking_url;

        return $message . SPACE . $url;
    }
}
