<?php

namespace Modules\MobileSalam\Notifications;

use App\Models\User;
use Modules\MobileSalam\Events\AuthenticationVerifyTokenSend;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class AuthenticationVerifyTokenNotification extends YasnaNotificationAbstract
{
    /** @var AuthenticationVerifyTokenSend $event */
    public $event;
    public $token;



    /**
     * RepairOrderSubmittedNotification constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via(User $notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms(User $notifiable)
    {
        $message = trans_safe("mobile-salam::messages.authentication_token", [
             'token' => $this->token,
        ]);
        return $message;
    }
}
