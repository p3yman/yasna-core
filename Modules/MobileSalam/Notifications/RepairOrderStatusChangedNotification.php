<?php

namespace Modules\MobileSalam\Notifications;

use Modules\MobileSalam\Events\RepairOrderStatusChanged;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class RepairOrderStatusChangedNotification extends YasnaNotificationAbstract
{
    /** @var RepairOrderStatusChanged */
    public $event;



    /**
     * RepairOrderSubmittedNotification constructor.
     *
     * @param RepairOrderStatusChanged $event
     */
    public function __construct(RepairOrderStatusChanged $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param RepairOrderStatusChanged $event
     */
    public function handle(RepairOrderStatusChanged $event)
    {
        $user = $event->model->customer;

        $user->notify(new RepairOrderStatusChangedNotification($event));
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $message = trans("mobile-salam::messages.sms_on_status_change", [
             "status" => $this->event->model->status_label,
        ]);
        $url     = $this->event->model->tracking_url;

        return $message . SPACE . $url;
    }
}
