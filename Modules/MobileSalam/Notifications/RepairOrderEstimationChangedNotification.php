<?php

namespace Modules\MobileSalam\Notifications;

use Modules\MobileSalam\Events\NewRepairEstimationReceived;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class RepairOrderEstimationChangedNotification extends YasnaNotificationAbstract
{
    /** @var NewRepairEstimationReceived */
    public $event;



    /**
     * RepairOrderSubmittedNotification constructor.
     *
     * @param NewRepairEstimationReceived $event
     */
    public function __construct(NewRepairEstimationReceived $event)
    {
        $this->event = $event;
    }



    /**
     * Handle the event.
     *
     * @param NewRepairEstimationReceived $event
     */
    public function handle(NewRepairEstimationReceived $event)
    {
        $user = $event->model->customer;

        $user->notify(new RepairOrderEstimationChangedNotification($event));
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        $message = trans("mobile-salam::messages.sms_on_repair_order_new_estimation");
        $url     = $this->event->model->tracking_url;

        return $message . SPACE . $url;
    }
}
