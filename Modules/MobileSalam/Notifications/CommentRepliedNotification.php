<?php

namespace Modules\MobileSalam\Notifications;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Commenting\Entities\Comment;
use Modules\Notifier\Messages\BrowserMessage;
use Modules\Notifier\Messages\Parts\BrowserWebButton;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class CommentRepliedNotification extends YasnaNotificationAbstract
{

    public $event;
    public $time;



    /**
     * CommentRepliedNotification constructor.
     *
     * @param Comment $event
     */
    public function __construct($event)
    {
        $this->event = $event;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * Send an email when a contact message is answered
     *
     * @param mixed $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = trans_safe('mobile-salam::general.email.contact-us.subject');
        $user    = user($this->event->model->user_id);

        return (new MailMessage)
             ->subject($subject)
             ->view('mobile-salam::email.contact.contacts_us_reply', [
                  'time' => $this->event->model->created_at,
                  'user' => $user->full_name,
                  'admin_message' => $this->event->model->text,
                  'user_message' => $this->event->model->parent->text,
             ])
             ;

    }

}
