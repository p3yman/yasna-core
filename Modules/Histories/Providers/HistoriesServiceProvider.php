<?php

namespace Modules\Histories\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class HistoriesServiceProvider
 *
 * @package Modules\Histories\Providers
 */
class HistoriesServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        // Call your methods here!
    }
}
