<?php

namespace Modules\Histories\Entities;

use Modules\Histories\Entities\Traits\HistoryBatchesTrait;
use Modules\Histories\Entities\Traits\HistorySaveTrait;
use Modules\Histories\Entities\Traits\HistoryScopesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends YasnaModel
{
    use SoftDeletes;
    use HistorySaveTrait;
    use HistoryScopesTrait;
    use HistoryBatchesTrait;



    /**
     * get the relationship to the parent model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function related()
    {
        return $this->belongsTo(MODELS_NAMESPACE . $this->model_name . "model_id");
    }



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
             "changes",
        ];
    }



    /**
     * get Changes accessor.
     *
     * @return array
     */
    public function getChangesAttribute()
    {
        return (array)$this->getMeta("changes");
    }



    /**
     * get change.
     *
     * @param string $field
     */
    public function getChange(string $field)
    {
        $changes = $this->getChangesAttribute();

        try {
            return $changes[$field];
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * get Batch resource.
     *
     * @return int
     */
    protected function getBatchResource()
    {
        return $this->batch;
    }
}
