<?php

namespace Modules\Histories\Entities\Traits;

use App\Models\History;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Yasna\Services\YasnaModel;

trait HistoriesTrait
{
    /**
     * get the relationship instance of the attached histories
     *
     * @return HasMany
     */
    public function histories()
    {
        return $this->hasMany(History::class, "model_id")->where("model_name", $this->getClassName());
    }



    /**
     * get the history of a selected attribute
     *
     * @param string     $field
     * @param array|null $includes
     * @param array      $excludes
     *
     * @return array
     */
    public function getHistoryOf($attribute, $includes = null, $excludes = [])
    {
        if (!$includes) {
            $includes = ["created_at", "batch"];
        }
        $histories = $this->histories()->withChangesIn($attribute)->get();

        /** @var Collection $mapped */
        $mapped = $histories->map(function ($model) use ($attribute, $includes, $excludes) {
            /** @var History $model */

            $resource = $model->toResource($includes, (array)$excludes);
            $extend   = [
                 $attribute => $model->getChange($attribute),
            ];

            return array_merge($resource, $extend);
        });

        return $mapped->toArray();
    }



    /**
     * load the history to the model instance, without updating it
     *
     * @param int  $deep            : the number of batches that need to be processed.
     * @param bool $by_batch_number : use batch number instead of deep system.
     *
     * @return array
     */
    public function revert(int $deep = 1, bool $by_batch_number = false)
    {
        $current_batch    = $this->getCurrentHistoryBatchNo();
        $involved_batches = [];

        if ($by_batch_number) {
            $deep = $current_batch - $deep;
        }

        for ($i = 0; $i <= $deep and $i < $current_batch; $i++) {
            $batch = $this->getSpecificHistoryBatchRecord($current_batch - $i);
            if (!$batch->exists) {
                continue; // <~~ strange situation!
            }

            $data               = (array)$batch->getMeta("changes");
            $involved_batches[] = $batch->id;

            foreach ($data as $key => $value) {
                if ($key == "meta" or $key == "id") {
                    continue; // <~~ strange situation!
                }
                if ($this->isMeta($key)) {
                    $this->setMeta($key, $value);
                } else {
                    $this->setAttribute($key, $value);
                }
            }
        }

        array_pop($involved_batches);

        return $involved_batches;
    }



    /**
     * rollback the model instance to a history point
     *
     * @param int $deep : the number of batches that need to be processed.
     *
     * @return bool
     */
    public function rollback(int $deep = 1): bool
    {
        $involved_batches = $this->revert($deep);
        $saved            = $this->save();

        if ($saved) {
            History::whereIn("id", $involved_batches)->forceDelete();
        }

        return $saved;
    }



    /**
     * add history
     *
     * @param YasnaModel|null $saved
     *
     * @return boolean
     */
    protected function addHistory($saved = null)
    {
        $id = $this->id;
        if ($saved) {
            $id = $saved->id;
        }

        return History::saveHistory($this->getClassName(), $id, $this->getFullDataBeingSaved());
    }



    /**
     * get the current history batch
     *
     * @return int
     */
    private function getCurrentHistoryBatchNo()
    {
        return History::getCurrentBatchNo($this->getClassName(), $this->id);
    }



    /**
     * get a specific batch record
     *
     * @param int $number
     *
     * @return History
     */
    private function getSpecificHistoryBatchRecord(int $number)
    {
        return History::findBatch($this->getClassName(), $this->id, $number);
    }
}
