<?php

namespace Modules\Histories\Entities\Traits;

use Modules\Histories\Entities\Scopes\WithChangesInScope;

trait HistoryScopesTrait
{
    /**
     * boot HistoryScopesTrait
     *
     * @return void
     */
    public static function bootHistoryScopesTrait()
    {
        static::addGlobalScope(new WithChangesInScope());
    }

}
