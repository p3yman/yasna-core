<?php

namespace Modules\Histories\Entities\Traits;

trait HistorySaveTrait
{
    /**
     * save a history log of the given table.
     *
     * @param string $model_name
     * @param int    $model_id
     * @param array  $changes
     *
     * @return bool
     */
    public static function saveHistory(string $model_name, int $model_id, array $changes)
    {
        if (!$model_id) {
            return false;
        }

        $data = [
             "model_name"    => $model_name,
             "model_id"      => $model_id,
             "batch"         => static::getNextBatchNo($model_name, $model_id),
             "changed_items" => static::getChangedItems($changes),
             "changes"       => $changes,
        ];

        return model("History")->batchSaveBoolean($data);
    }



    /**
     * get the suitable string to represent the changed items.
     *
     * @param array $changes
     *
     * @return string
     */
    private static function getChangedItems(array $changes): string
    {
        $fields   = array_keys($changes);
        $imploded = implode("-", $fields);

        return "-$imploded-";
    }
}
