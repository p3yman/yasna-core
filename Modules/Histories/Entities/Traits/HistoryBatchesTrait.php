<?php

namespace Modules\Histories\Entities\Traits;

use App\Models\History;

trait HistoryBatchesTrait
{
    /**
     * get the current batch number for a specific model.
     *
     * @param string $model_name
     * @param string $model_id
     *
     * @return int
     */
    public static function getCurrentBatchNo(string $model_name, string $model_id): int
    {
        $latest = static::select(["id", "model_name", "model_id", "batch"])
                        ->where("model_name", $model_name)
                        ->where("model_id", $model_id)
                        ->orderBy("batch", "desc")
                        ->firstOrNew([])
        ;

        return (int)$latest->batch;
    }



    /**
     * get a specific batch record.
     *
     * @param string $model_name
     * @param string $model_id
     * @param int    $batch
     *
     * @return History
     */
    public static function findBatch(string $model_name, string $model_id, int $batch)
    {
        return static::select(["id", "model_name", "model_id", "meta", "batch"])
                     ->where("model_name", $model_name)
                     ->where("model_id", $model_id)
                     ->where("batch", $batch)
                     ->firstOrNew([])
             ;
    }



    /**
     * get suitable batch number for a specific model to insert a new history record.
     *
     * @param string $model_name
     * @param string $model_id
     *
     * @return int
     */
    protected static function getNextBatchNo(string $model_name, string $model_id): int
    {
        return static::getCurrentBatchNo($model_name, $model_id) + 1;
    }
}
