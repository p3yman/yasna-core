<?php

namespace Modules\Histories\Entities\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class WithChangesInScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  Builder $builder
     * @param  Model   $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
    }



    /**
     * @param Builder $builder
     *
     * @return void
     */
    public function extend(Builder $builder)
    {
        $builder->macro("withChangesIn", function (Builder $builder, $field) {
            return $builder->where("changed_items", "like", "%-$field-%");
        });
    }

}
