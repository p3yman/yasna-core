/**
 * returns all data-attributes starting with "data-prefix" as an object with camelCase names
 * @param prefix
 * @returns {{}}
 */

var acoordionIcons = {
    header: "ui-icon-plusthick",
    activeHeader: "ui-icon-minusthick"
};

$.fn.dataStartsWith = function (prefix) {
    var result = {};
    var data = $(this).data();

    $.each(data, function (key, val) {
        if (key.startsWith(prefix)) {
            result[key.substr(prefix.length).lcfirst()] = val;
        }
    });

    return result;
};

$.fn.collapse = function () {
    var target = $(this);
    var targetId = target.attr('id');
    if (targetId) {
        var clickable = $('[data-toggle=collapse][data-target=#' + targetId + '], [data-toggle=collapse][href=#' + targetId + ']');
        if (clickable.length) {
            clickable.click(function () {
                target.slideToggle();
            })
        }
    }
};

$.fn.updateContent = function (callback) {
    var container = $(this);
    var url = container.attr('data-url');
    if (url) {
        $.ajax({
            url: url,
            success: function (result) {
                container.html(result);
                if (typeof callback == 'function') {
                    callback();
                }
            }
        });
    }
};

$.fn.scrollToView = function (extra, duration) {
    var item = $(this);

    if (!$.isNumeric(duration)) {
        duration = 1000;
    }

    if (!$.isNumeric(extra)) {
        extra = 0;
    }

    $('html, body').animate({
        scrollTop: item.offset().top + extra
    }, duration);
};

/**
 * Serializes inputs data and makes object
 * @returns {{}}
 */
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};

String.prototype.ucfirst = function () {
    return this.replace(/(?:^|\s)\w/g, function (match) {
        return match.toUpperCase();
    });
};

String.prototype.lcfirst = function () {
    return this.replace(/(?:^|\s)\w/g, function (match) {
        return match.toLowerCase();
    });
};

String.prototype.isValidJalaliDate = function (separator) {
    if (typeof separator == 'undefined') { // set "/" as default separator
        separator = '/';
    }
    var regex = new RegExp('^((\\d{4})|(\\d{2}))' + separator + '\\d{1,2}' + separator + '\\d{1,2}$');
    var bits = this.split(separator);
    if (regex.test(this) && bits.length == 3) {
        var d = new JalaliDate(bits[0], bits[1], bits[2]);
        return !!(d && (d.getMonth()) == Number(bits[1]) && d.getDate() == Number(bits[2]));
    } else {
        return false;
    }
};

String.prototype.smartSearch = function (search) {
    source = this;

    source = source.toLowerCase();
    search = search.toLowerCase();
    var searchArray = search.split(" ");
    var found = 0;

    $.each(searchArray, function (index, value) {
        if (source.startsWith(value) ||
            (source.search(" " + value) > -1)) {
            found++;
        }
    });

    if (found == searchArray.length) {
        return true;
    } else {
        return false;
    }
};

String.prototype.reverse = function () {
    return this.split("").reverse().join("");
};

String.prototype.splitNotEmpty = function (delimiter) {
    return this.match(new RegExp("[^" + delimiter + "]+", "gi"));
};

String.prototype.limitedSplit = function (delimiter, limit, notEmpty) {
    if ((typeof notEmpty == typeof undefined) || !notEmpty) {
        var arr = this.split(delimiter);
    } else {
        var arr = this.splitNotEmpty(delimiter);
    }

    if ((typeof limit == typeof undefined) || (arr.length <= limit)) {
        return arr;
    }

    var result = arr.splice(0, (limit - 1));

    result.push(arr.join(delimiter));

    return result;
};

String.prototype.clearDigits = function () {
    let s = ad(this.replace(/((?![\u06F0-\u06F9\u0660-\u066990-9]).)/g, ''));
    return (typeof s != 'undefined') ? s : '';
};

$.smartMerge = function () {
    var allArray = true;
    var checking = arguments;
    $.each(checking, function (i, argument) {
        if (!$.isArray(argument)) {
            allArray = false;
            return false;
        }
    });

    if (allArray) {
        return $.merge.apply(this, checking);
    }

    $.each(checking, function (i, argument) {
        if ($.isArray(argument)) {
            checking[i] = $.extend({}, argument);
        }
    });

    return $.extend.apply(this, checking);
};

function nummber_format(text) {
    if (typeof text == 'number') {
        text = text.toString();
    }

    if (typeof parseInt(text) == 'number') {
        text = text.reverse();
        var parts = text.match(/.{1,3}/g);
        text = parts.join(',');
        text = text.reverse();
        return text;
    }
}

function prefixToIndex(delimiter, haystack) {
    switch (typeof haystack) {
        case 'object':
            var output = {};
            $.each(haystack, function (index, field) {
                var parts = field.limitedSplit(delimiter, 2);
                if (parts.length == 2) {
                    var key = parts[0];
                    var value = parts[1];
                    value = prefixToIndex(delimiter, value);
                    if (isDefined(output[key])) {
                        if (typeof output[key] != 'object') {
                            output[key] = [output[key]];
                        }

                        if (typeof value != 'object') {
                            value = [value];
                        }

                        output[key] = $.smartMerge(output[key], value);
                    } else {
                        output[key] = value;
                    }
                }
            });
            return output;
            break;

        case 'string':
            var parts = haystack.limitedSplit(delimiter, 2);
            if (parts.length == 2) {
                var key = parts[0];
                var value = parts[1];
                value = prefixToIndex(delimiter, value);

                haystack = {};
                haystack[key] = value;

                return haystack;
            }
            break;
    }
    return haystack;
}

function isDefined(input) {
    return !(typeof input == typeof undefined);
}

function getHashUrl(url) {
    if (url) {
        var hashIndex = url.indexOf('#');
        var hashString = "";
        if (hashIndex != -1) {
            hashString = url.substring(hashIndex + 1);
        }
        return hashString;
    }

    return decodeURIComponent(window.location.hash.substr(1));
}

function setHashUrl(hashString, url) {
    if (url) {
        var hashIndex = url.indexOf('#');
        if (hashIndex == -1) {
            url = url + '#' + hashString;
        } else {
            url = url.substring(0, hashIndex + 1) + hashString;
        }
        return url;
    }

    window.location.hash = hashString;
}

function getPageUrl() {
    return window.location.href.replace(getHashUrl(), '').replace('#', '');
}

function getUrlParameterByName(name, url) {
    if (!url) url = window.location.href;

    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function addUrlParameter(name, value, url) {
    if (!url) url = window.location.href;
    name = name.toString().replace(/[\[\]]/g, "\\$&");
    value = value.toString().replace(/[\[\]]/g, "\\$&");

    let currentValue = getUrlParameterByName(name, url);
    if (currentValue) {
        return url.replace(name + '=' + currentValue, name + '=' + value);
    }

    let hasHash = false;
    let afterHash = '';
    if (url.includes('#')) {
        hasHash = true;
        afterHash = url.substr(url.indexOf("#") + 1);
        url = url.substr(0, url.indexOf("#"));
    }

    if (url.includes('?')) {
        url += "&";
    } else {
        url += "?";
    }

    return url + name + '=' + value + (hasHash ? ('#' + afterHash) : '');
}

function removeUrlParameterByName(key, url) {
    if (!url) url = window.location.href;

    var hashIndex = url.indexOf('#');
    var hashString = '';
    if (hashIndex != -1) {
        hashString = url.substring(hashIndex + 1);
        url = url.substring(0, (hashIndex != -1 ? hashIndex : url.length));
    }

    var rtn = url.split("?")[0],
        param,
        params_arr = [],
        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";

    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }

        if (params_arr.length) {
            rtn = rtn + "?" + params_arr.join("&");
        }
    }

    if (hashString) {
        rtn = rtn + '#' + hashString;
    }

    return rtn;
}

function removeUrlParameterByName(key, url) {
    if (!url) url = window.location.href;

    var hashIndex = url.indexOf('#');
    var hashString = '';
    if (hashIndex != -1) {
        hashString = url.substring(hashIndex + 1);
        url = url.substring(0, (hashIndex != -1 ? hashIndex : url.length));
    }

    var rtn = url.split("?")[0],
        param,
        params_arr = [],
        queryString = (url.indexOf("?") !== -1) ? url.split("?")[1] : "";

    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }

        if (params_arr.length) {
            rtn = rtn + "?" + params_arr.join("&");
        }
    }

    if (hashString) {
        rtn = rtn + '#' + hashString;
    }

    return rtn;
}

function ksort(obj) {
    if (obj) {
        var sorted = {};
        var keys = [];

        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
    }

    keys.sort();

    for (i = 0; i < keys.length; i++) {
        sorted[keys[i]] = obj[keys[i]];
    }

    return sorted;
}

function loadingDialog(parameter, dialog) {
    if (isDefined(dialog)) {
        if (!(dialog instanceof HtmlElement) && (typeof dialog == 'string')) {
            dialog = $(dialog);
        }
    } else {
        dialog = $('#loading-dialog');
    }

    if (dialog.length) {
        if ($.inArray(parameter, ['hide', false, 0]) > -1) {
            dialog.hide();
        } else {
            dialog.show();
        }
    }
}

function reloadCart() {
    $.ajax({
        url: window.urls.reloadCart,
        success: function (rs) {
            let rsEl = $(rs);
            if (rsEl.hasClass('cart')) {
                $('header').find('.cart-counter').replaceWith(rsEl)
            }
        }
    })
}

function filterOpener() {
    // $('.filter-opener').hide();
    $('.filter-container').addClass('opened');
}

function filterCloser() {
    // $('.filter-opener').show();
    $('.filter-container').removeClass('opened');
}

function searchOpener() {
    $('.search-form').addClass('opened');
    $('#site-search').focus();
}

function searchCloser() {
    $('.search-form').removeClass('opened');
}

function showRelativeTabContent() {
    var hash = $('.nav-tabs .active a').attr('href').replace("#", "");

    $('.tab-content.show').removeClass('show');
    $('.tab-content[data-id=' + hash + ']').addClass('show')
}

function Notif(option) {
    var el = this;

    // Setup
    el.self = $('.toast-message');
    el.close = this.self.find('.close');
    el.message = el.self.find('.message');
    el.top = option.topPos;
    el.classNames = option.classNames;
    el.autoClose = (typeof option.autoClose === "boolean") ? option.autoClose : false;
    el.autoCloseTimeout = (option.autoClose && typeof option.autoCloseTimeout === "number") ? option.autoCloseTimeout : 3000;


    // Methods
    el.reset = function () {
        el.message.empty();
        el.self.removeClass(el.classNames);
    };
    el.show = function (msg, type) {
        el.reset();
        el.self.css('top', el.top);
        el.message.text(msg);
        el.self.addClass(type);

        if (el.autoClose) {
            setTimeout(function () {
                el.hide();
            }, el.autoCloseTimeout);
        }
    };
    el.hide = function () {
        el.self.css('top', '-100%');
        el.reset();
    };

    el.close.on('click', this.hide);

}

function escapeReplyComment() {
    let replyBox = $('.reply-comment');
    replyBox.find('.reply-preview').slideUp();
    replyBox.find('.parent-text').html('');
    replyBox.find('input[name=parent_id]').val('');
}

function scrollUp() {
    $('body').scrollToView();
}

$(document).ready(function () {

    /**
     * make datepicker for marriage date visible if "married" has been selected form marital radio
     */
    $('input[type=radio][name=marital]').change(function () {
        var field = $('#marriage_date').closest('.field');
        if (field.length) {
            if ($(this).is(':checked')) {
                if ($(this).val() == 2) {
                    field.slideDown();
                } else {
                    field.slideUp();
                }
            }
        }
    }).change();

    $('.yt-accordion').accordion({
        collapsible: true,
        autoHeight: true,
        icons: acoordionIcons,

        beforeActivate: function (event, ui) {
            // The accordion believes a panel is being opened
            if (ui.newHeader[0]) {
                var currHeader = ui.newHeader;
                var currContent = currHeader.next('.ui-accordion-content');
                // The accordion believes a panel is being closed
            } else {
                var currHeader = ui.oldHeader;
                var currContent = currHeader.next('.ui-accordion-content');
            }
            // Since we've changed the default behavior, this detects the actual status
            var isPanelSelected = currHeader.attr('aria-selected') == 'true';

            // Toggle the panel's header
            currHeader.toggleClass('ui-corner-all', isPanelSelected)
                .toggleClass('ui-accordion-header-active', !isPanelSelected)
                .attr('aria-selected', ((!isPanelSelected).toString()));

            // Toggle the panel's icon
            currHeader.find('.ui-icon')
                .toggleClass(acoordionIcons.header, isPanelSelected)
                .toggleClass(acoordionIcons.activeHeader, !isPanelSelected);

            // Toggle the panel's content
            currContent.toggleClass('accordion-content-active', !isPanelSelected);
            if (isPanelSelected) {
                currContent.slideUp();
            } else {
                currContent.slideDown();
            }

            return false; // Cancel the default action
        }
    });

    $('.yt-lazy-load').each(function () {
        var container = $(this);
        var url = container.attr('data-url');

        if (url) {
            if (container.hasClass('auto-height')) {
                container.css('height', 'auto');
            }

            $.ajax({
                url: url,
                type: container.attr('data-method') ? container.attr('data-method') : 'GET',
                success: function (result) {
                    container.html(result);
                }
            });


            container.on({
                click: function (e) {
                    e.preventDefault();

                    url = $(this).attr('href');
                    if (url) {
                        $.ajax({
                            url: url,
                            success: function (result) {
                                container.html(result);
                                container.removeClass('loading');
                            },
                            beforeSend: function () {
                                container.addClass('loading');
                            }
                        });
                    }
                }
            }, '.pagination li a');
        }
    })

    $(".yt-accordion").find('.yt-accordion-header.default-active').each(function () {
        var item = $(this);
        var accordion = item.closest('.yt-accordion');

        if (!item.hasClass('ui-accordion-header-active')) {
            var index = item.index('.default-active') ? item.index('.default-active') : false;
            var panel = accordion.find('.yt-accordion-panel').eq(index);
            accordion.accordion("option", "active", index);
            if (panel.hasClass('auto-height')) {
                panel.css('height', 'auto');
            }
        }
    });

    $('.like-submit-button').click(function () {
        var form = $(this).closest('form');
        if (form.length) {
            if (!form.find(':input[type=submit]').length) {
                form.append($('<input type="submit"/>').css('display', 'none'));
            }

            form.find(':input[type=submit]').click();
        }
    });

    $('.collapse').each(function () {
        $(this).collapse();
    });

    if ($('header').find('.cart-counter').length) {
        setInterval(reloadCart, 15000);
    }

    $("input.gift-input").off('keyup').off('keydown').on('input', function () {
        let that = $(this);
        let digits = that.val().clearDigits();
        digits = digits.replace(/(\S{5})(?!$)/g, "$1  ");
        that.val(digits);
    }).trigger('input');

    $('.radio-table').find('tr').click(function () {
        let that = $(this);
        that.find('input[type=radio]').prop('checked', true);
    });


    if ($('.nav-tabs li').length) {
        showRelativeTabContent();

        $('.nav-tabs li').on('click', function () {
            $('.nav-tabs .active').removeClass('active');
            $(this).addClass('active');

            showRelativeTabContent();
        })
    }


    $('.comment-section').on({
        click: function (e) {
            e.preventDefault();
            let comment = $(this).closest('.comment');
            let text = comment.children('.contents').children('.comment-text').html();
            let replyBox = $('.reply-comment');
            replyBox.find('.parent-text').html(text);
            replyBox.find('.reply-preview').slideDown();
            replyBox.find('input[name=parent_id]').val(comment.data('key'));
            replyBox.scrollToView(-20);
        }
    }, 'a.reply');


    $('.comment-section').on({
        click: function (e) {
            e.preventDefault();
            let that = $(this);
            let page = that.data('page');
            page = page ? page : 1;
            let newPage = page + 1;
            let url;
            if (!(url = that.data('loadUrl'))) {
                return false;
            }

            let box = that.parent('div');

            url = addUrlParameter('page', newPage, url);

            $.ajax({
                url: url,
                beforeSend: function () {
                    box.addClass('disabled-box');
                },
                success: function (rs) {
                    box.replaceWith(rs);
                }
            })
        }
    }, 'a.more');


    $('.product-counter').each(function () {
        $(this).on('input', function () {
            let that = $(this);
            let val = that.val();
            if (!val) {
                return false;
            }
            val = ed(val);
            if (that.hasClass('float')) {
                if (val[val.length - 1] === '.' && !val.slice(0, -1).includes('.')) {
                    return false;
                }
                that.val(ad(parseFloat(val).toString()));
            } else {
                that.val(ad(parseInt(val).toString()))
            }
        });
    });

    $('#open-search').on('click', function (e) {
        e.preventDefault();
        $(this).hide();
        searchOpener();
    });

    $('#close-search').on('click', function () {
        $('#open-search').removeAttr('style');
        searchCloser();
    });

    $(window).on('resize', function () {
        if ($(window).width() < 992) {
            $('#open-search').removeAttr('style');
        }
    });

    $(window).on('scroll',function () {
        if($(this).scrollTop() > 200){
            $('.btn-floating').removeClass('noDisplay');
        }else{
            $('.btn-floating').addClass('noDisplay');
        }
    })


    if ($('.map-container').length) {
        let map_container = $('.map-container');
        map_container.each(function (index, container) {
            console.log(container);
            initLeafletMap($(container))
        });
    }
    function initLeafletMap(container) {
        let lat         = container.data('lat');
        let lng         = container.data('lng');
        let title       = container.data('title');
        let zoom        = 17;
        let token       = window.mapbox.token;
        let layer_uri   = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}';
        let marker_icon = new L.Icon({
            iconUrl    : 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
            shadowUrl  : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
            iconSize   : [25, 41],
            iconAnchor : [12, 41],
            popupAnchor: [1, -34],
            shadowSize : [41, 41]
        });
        let my_map = L.map(container[0], {
            center: [lat, lng],
            zoom  : zoom
        });
        L.tileLayer(layer_uri, {
            maxZoom    : 18,
            id         : 'mapbox.streets',
            accessToken: token,
        }).addTo(my_map);
        L.marker([lat, lng], {
            icon   : marker_icon,
            autoPan: true,
            title  : title,
        }).addTo(my_map)
            .bindPopup(title)
            .openPopup();
    }
});
