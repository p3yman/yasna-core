<?php
return [
     "title" => [
          "singular" => "سبد خرید",
     ],

     "purchase" => [
          "singular" => "خرید",
          "plural"   => "خریدها",
     ],

     "numeration-label" => [
          "number" => "تعداد",
          "amount" => "مقدار",
     ],

     "message" => [
          "is-empty"                  => "سبد خرید خالی‌ست.",
          "order-address-description" => "این سفارش به :receiver به نشانی :address و شماره تماس :telephone تحویل می‌گردد.",
          "order-courier-description" => "تحویل توسط :courier - هزینه ارسال: :price",
          "cart-changed"              => "سبد خرید شما تغییر کرده‌است.",
          "select-gateway-hint"       => "درگاه مورد نظر خود را انتخاب کنید. از طریق تمام کارت‌های بانکی عضو شتاب می‌توانید پرداخت خود را انجام دهید.",
          "thanks-for-purchasement"   => "از خرید شما سپاس‌گذاریم.",
          "pending-cart"              => [
               "sms" => "سبد خرید شما تکمیل نشده برای تکمیل به سایت مراجعه کنید.",
          ],
          "unpaid-cart"               => [
               "sms" => "سفارش شما در انتظار پرداخت است. برای پرداخت به آدرس زیر مراجعه کنید.",
          ],
     ],

     "alert" => [
          "success" => [
               "order-remove" => "محصول با موفقیت حذف شد.",
               "order-add"    => "محصول به سبد خرید اضافه شد.",
               "payment"      => "سفارش شما با موفقیت ثبت و پرداخت شد. از شما متشکریم.",
          ],
          "error"   => [
               "order-remove"                 => "حذف محصول با مشکل مواجه شد.",
               "order-add"                    => "اضافه کردن محصول به سبد خرید با خطا مواجه شد. مجددا تلاش کنید.",
               "required-selection"           => "انتخاب :attribute الزامی است.",
               "payment"                      => "متاسفانه پرداخت ناموفق بود!",
               "unable-to-verify"             => "امکان تایید این تراکنش وجود ندارد.",
               "rejected-by-gateway"          => "تراکنش از طرف درگاهی بانکی تایید نشد.",
               "system-error-in-purchasement" => "به دلیل خطای سیستمی در حال حاضر امکان اتمام فرایند خرید وجود ندارد.",
          ],
     ],

     "button" => [
          "back-to-shop"       => "بازگشت به فروشگاه",
          "back-to-cart"       => "بازگشت به سبد خرید",
          "add"                => "افزودن به سبد",
          "flush"              => "خالی کردن سبد",
          "settlement"         => "تسویه حساب",
          "confirm-and-review" => "تایید و بازبینی سفارش",
          "confirm-and-pay"    => "تایید و پرداخت",
          "purchase-online"    => "پرداخت آنلاین",
     ],

     "price" => [
          "unit"                => "قیمت واحد",
          "total"               => "قیمت کل",
          "your-purchase-total" => "جمع خرید شما",
          "purchasable"         => "قابل پرداخت",
          "delivery"            => "هزینه‌ی تحویل",
          "free"                => "رایگان",
     ],

     "headline" => [
          "discount"                   => "تخفیف",
          "shipping-info"              => "اطلاعات ارسال سفارش",
          "review-order"               => "بازبینی سفارش",
          "address-and-courier"        => "نشانی و نوع تحویل",
          "account-number"             => "شماره حساب",
          "card-number"                => "شماره کارت",
          "bank"                       => "بانک",
          "account-owner"              => "صاحب حساب",
          "tracking-code"              => "شماره پیگیری",
          "declaration-date"           => "تاریخ واریز",
          "delivery-info"              => "اطلاعات ارسالی سفارش",
          "receiver"                   => "دریاف کننده",
          "order-status-summary"       => "خلاصه وضعیت سفارش",
          "receipt-number"             => "شماره رسید",
          "order-status"               => "وضعیت سفارش",
          "your-purchasements-details" => "جزییات پرداخت‌های شما",
          "gateway"                    => "درگاه پرداخت",
          "payment-type"               => "نوع پرداخت",
          "amount"                     => "مبلغ",
          "order-details"              => "جزییات سفارش",
          "details-of-order"           => "جزییات سفارش :order",
     ],

     "payment" => [
          "title"  => [
               "singular" => "پرداخت",
          ],
          "status" => "وضعیت پرداخت",

          "status-levels" => [
               "refunded"        => "برگشت‌داده‌شده",
               "canceled"        => "لغوشده",
               "cancelled"       => "لغوشده",
               "rejected"        => "ردشده",
               "verified"        => "تأییدشده",
               "declared"        => "در انتظار تأیید",
               "sent-to-gateway" => "در انتظار درگاه",
               "created"         => "پرداخت‌نشده",
          ],

          "message" => [
               "unsuccess-online-hint"       => "در صورتی که طی این فرایند مبلغی از حساب شما کسر شده است، طی ۷۲ ساعت آینده به حساب شما باز خواهد گشت.",
               "select-gateway-and-purchase" => "یکی از درگاه‌ها را انتخاب و مبلغ سفارش را پرداخت نمایید تا سفارش شما تکمیل شد.",
          ],
     ],

     "courier" => [
          "title" => [
               "singular" => "روش ارسال",
               "plural"   => "روش‌های ارسال",
          ],

          "select" => "انتخاب روش ارسال",
     ],

     "package" => [
          "title" => [
               "singular" => "بسته‌بندی",
               "plural"   => "بسته‌بندی‌ها",
          ],
     ],

     "discount" => [
          "i-have-code"   => "کد تخفیف دارم.",
          "register-code" => "ثبت کد تخفیف",
          "message"       => [
               "register-and-submit" => " لطفا کد تخفیف را وارد کرده و دکمه ثبت را کلیک کنید. ",
          ],
     ],

     "payment-method" => [
          "title" => [
               "plural" => "شیوه‌های پرداخت",
          ],

          "option" => [
               "online"  => "پرداخت آنلاین",
               "deposit" => "واریز به حساب",
               "shetab"  => "کارت به کارت",
               "cash"    => "پرداخت نقدی",
          ],
     ],
];
