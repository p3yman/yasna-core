<?php
return [
    "title" => [
        "plural" => "فیلتر‌ها",
    ],

    "option" => [
        "refresh"   => "تخلیه‌ی فیلترها",
        "filtering" => "فیلتر کردن",
    ],
    "search" => [
        "title" => "جست و جو",

    ],

    "sort" => [
        "title" => "مرتب‌سازی",

        "price"       => [
            "max-to-min" => "قیمت زیاد » کم",
            "min-to-max" => "قیمت کم » زیاد",
        ],
        "best-seller" => "پرفروش‌ترین‌ها",
        "favorites"   => "محبوب‌ترین‌ها",
        "new"         => "جدیدترین‌ها",
    ],

    "range" => [
        "from" => "از",
        "to"   => "تا",
    ],

    "availability" => [
        "positive" => "موجود",
        "negative" => "ناموجود",
    ],

    "sale-type" => [
        "special" => "فروش ویژه",
    ],
];
