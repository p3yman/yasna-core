<?php
return [
    "title" => [
        "plural" => "Filters",
    ],

    "option" => [
        "refresh"   => "Clear Filters",
        "filtering" => "Filter",
    ],
    "search" => [
        "title" => "Search",

    ],

    "sort" => [
        "title" => "Sort",

        "price"       => [
            "max-to-min" => "Price: High to Low",
            "min-to-max" => "Price: Low to High",
        ],
        "best-seller" => "Most Sell",
        "favorites"   => "Favorites",
        "new"         => "Newest",
    ],

    "range" => [
        "from" => "From",
        "to"   => "To",
    ],

    "availability" => [
        "positive" => "Available",
        "negative" => "Unavailable",
    ],

    "sale-type" => [
        "special" => "Special Sale",
    ],
];
