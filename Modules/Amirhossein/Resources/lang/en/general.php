<?php
return [
     "login-or-register"               => "Login / Register",
     "home"                            => "Home",
     "home-page"                       => "Home Page",
     "about-us"                        => "About Us",
     "return-policies"                 => "Return Policies",
     "contact-us"                      => "Contact Us",
     "description"                     => "Description",
     "categories"                      => "Categories",
     "categories-of"                   => "Categories of",
     "categories-of-something"         => "Categories of :something",
     "categories-of-something-partial" => [
          "part1" => "",
          "part2" => "Categories of",
          "part3" => ":something",
     ],
     "continue-reading"                => "Continuation of Story",
     "related-things"                  => [
          "part1" => "",
          "part2" => ":things",
          "part3" => "Related",
     ],
     "similar-things"                  => [
          "part1" => "",
          "part2" => ":things",
          "part3" => "Similar",
     ],
     "cancel"                          => "Cancel",
     "our-site"                        => "Our Site",
     "manager"                         => "Manager",
     "row"                             => "Row",
     "date"                            => "Date",
     "status"                          => "Status",
     "close"                           => "Close",
     "or"                              => "or",

     "message" => [
          "any-question-about-your-order" => "Any question about your order?",
          "rate-product"                  => "Dear :user, Thank you for your purchase. How do you rate this product?",
          "success"                       => [
               "send-message" => "Message sent successfully.",
          ],
          "error"                         => [
               "no-result"            => "No results founded.",
               "location-is-disabled" => "Your browser's location service is disabled.",
               "no-post-found"        => "No post found.",
               "send-message"         => "Unable to send message.",
          ],
     ],

     "browser" => [
          "firefox" => "Firefox",
          "chrome"  => "Google Chrome",
     ],

     "location" => [
          "message" => [
               "is-disabled-in-browser"         => "Your browser's location service is disabled.",
               "select-to-increase-precision"   => "For more accuracy, select the address on the map.",
               "thanks-for-specifying-location" => "Thank you for helping us to provide the best possible service by specifying the address on the map.",
          ],
          "guide"   => [
               "activation-firefox" => "Press Alt+t+i to enter settings and select the Permission tab, then remove Access your location from the block.",
               "activation-chrome"  => "Click the :button button or green lock icon at the beginning Address Bar, then in setting, remove Location from the block.",
          ],
     ],

     "drawing" => [
          "draw"                  => [
               "singular" => "Lottery",
               "prepare"  => "Preparation for Lottery",
          ],
          "receipts-count-amount" => "Receipt :amount Toman (total :count)",
          "winner"                => "Lottery Winner",
          "no-winner-so-far"      => "Nobody is currently selected as a winner.",
          "take-number-between"   => "Select a number from :number1 to :number2.",
          "random-number"         => "Random Number",
          "redraw"                => "Lottery Again",

          "code"    => [
               "singular"          => "Code",
               "registered-plural" => "Registered Codes",
               "register"          => "Register Lottery Code",
               "check"             => "Check Code",
               "add"               => "Add Code",
          ],
          "message" => [
               "invalid-code"                      => "Code is not valid.",
               "register-code-success-please-wait" => "The code is accepted. Wait a few moments.",
               "register-code-success-sms"         => "Hello dear :name, \n\r Your lottery code has been registered successfully .",
          ],
     ],

     "order" => [
          "plural" => "Orders",
     ],

     "footer" => [
          "copyright"          => "All rights reserved for :site.",
          "created-by"         => "Provided by",
          "yasna-team"         => "Yasna Team",
          'powered_by_someone' => 'Powered by :someone',
     ],

     "edu-level" => [
          "short" => [
               "not-specified"     => "Unknown",
               "less-than-diploma" => "Under Graduated",
               "diploma"           => "High School Degree",
               "associate-degree"  => "Associated Degree",
               "bachelor-degree"   => "Bachelor",
               "master-degree"     => "Master of Science",
               "phd-or-more"       => "PhD",
          ],
          "full"  => [
               "not-specified"     => "unknown",
               "less-than-diploma" => "Under Graduated",
               "diploma"           => "High School Degree",
               "associate-degree"  => "Associated Degree",
               "bachelor-degree"   => "Bachelor Degree",
               "master-degree"     => "Master of Science",
               "phd-or-more"       => "PhD and more",
          ],
     ],

     "gender" => [
          "male"   => "Male",
          "female" => "Female",
          "other"  => "Other",
     ],

     "marital-status" => [
          "single"  => "Single",
          "married" => "Married",
     ],

     "unit" => [
          "currency" => [
               "IRT" => "Tomans",
               "IRR" => "Rials",
          ],
     ],

     "contact" => [
          "address"   => "Address",
          "telephone" => "Telephone",
          "fax"       => "Fax",
          "email"     => "Email",
     ],

     "media" => [
          "playing" => "Playing",
     ],

     "comment" => [
          "title" => [
               "singular" => "Comment",
               "plural"   => "Comments",
          ],
          "form"  => [
               "name"      => "Name",
               "lastname"  => "Surname",
               "fullname"  => "Full Name",
               "email"     => "Email",
               "url"       => "Url",
               "statement" => "Leave your comment",
          ],

          "answer" => "Answer",
          "submit" => "Submit a Comment",
          "more"   => "More Comments",
     ],

     "address" => [
          "title"            => [
               "singular" => "Address",
               "plural"   => "Addresses",
          ],
          "add"              => "Add Address",
          "register"         => "Register Address",
          "select"           => "Select Address ",
          "no-address-alert" => "You have no saved address. Please register an address.",
     ],

     "tag" => [
          "title" => [
               "plural" => "Labels",
          ],
     ],

     "branch-1" => "First Branch",
     "branch-2" => "Second Branch",
];
