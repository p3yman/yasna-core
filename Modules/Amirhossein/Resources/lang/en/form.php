<?php
return [
    'button' => [
        'save'   => "Save",
        'submit' => "Submit",
        "back"   => "Back",
        "send"   => "Send",
    ],

    'feed' => [
        'error' => "Error Occurred",
        'wait'  => "Just a moment...",
        'done'  => "Done",
    ],

    'created-at'   => "Date of Registration",
    'purchased-at' => "Date Invoice",
    'amount'       => "Amount",
];
