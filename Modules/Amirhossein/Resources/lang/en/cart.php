<?php
return [
     "title" => [
          "singular" => "Cart",
     ],

     "purchase" => [
          "singular" => "Purchase",
          "plural"   => "Purchases",
     ],

     "numeration-label" => [
          "number" => "Number",
          "amount" => "Amount",
     ],

     "message" => [
          "is-empty"                  => "Shopping Cart is empty.",
          "order-address-description" => "This order will be delivered to :receiver (:address, :telephone).",
          "order-courier-description" => "Will be delivered via :courier - Delivery Cost: :price",
          "cart-changed"              => "Your shopping cart has been changed.",
          "select-gateway-hint"       => "Choose a gateway. You can pay with carts of all banks in shetab network.",
          "thanks-for-purchasement"   => "Thank you for your purchase.",
          "pending-cart"              => [
               "sms" => "Your cart is not finalized. Please get back to site to finish it.",
          ],
          "unpaid-cart"               => [
               "sms" => "Your order is waiting for payment. Go to the url below to pay it.",
          ],
     ],

     "alert" => [
          "success" => [
               "order-remove" => "The product is successfully removed.",
               "order-add"    => "Your order is added to the cart.",
               "payment"      => "Your order has been submitted and paid successfully. Thank You.",
          ],
          "error"   => [
               "order-remove"                 => "Problem occurred while deleting product.",
               "order-add"                    => "An error occurred while adding product to the cart. Try again.",
               "required-selection"           => "Select :attribute is necessary.",
               "payment"                      => "Unfortunately your payment has not ended successfully!",
               "unable-to-verify"             => "Unable to verify this transaction.",
               "rejected-by-gateway"          => "Transaction has not been verified by the gateway.",
               "system-error-in-purchasement" => "Due to a system error, it is not possible to complete the purchase process at this time.",
          ],
     ],

     "button" => [
          "back-to-shop"       => "Return to Store",
          "back-to-cart"       => "Return to Cart",
          "add"                => "Add to Cart",
          "flush"              => "Empty Cart",
          "settlement"         => "Checkout",
          "confirm-and-review" => "Verify and Review",
          "confirm-and-pay"    => "Verify and Pay",
          "purchase-online"    => "Purchase Online",
     ],

     "price" => [
          "unit"                => "Unit Price",
          "total"               => "Total",
          "your-purchase-total" => "Your Purchasement Total Cost",
          "purchasable"         => "Payable",
          "delivery"            => "Delivery Fee",
          "free"                => "Free",
     ],

     "headline" => [
          "discount"                   => "Discount",
          "shipping-info"              => "Delivery Information",
          "review-order"               => "Review Order",
          "address-and-courier"        => "Address and Delivery Type",
          "account-number"             => "Account Number",
          "card-number"                => "Card Number",
          "bank"                       => "Bank",
          "account-owner"              => "Account Owner",
          "tracking-code"              => "Tracking Number",
          "declaration-date"           => "Deposit Date",
          "delivery-info"              => "Delivery Information",
          "receiver"                   => "Receiver",
          "order-status-summary"       => "Order Status Summary",
          "receipt-number"             => "Receipt Number",
          "order-status"               => "Order Status",
          "your-purchasements-details" => "Details of Your Purchasements",
          "gateway"                    => "Gateway",
          "payment-type"               => "Payment Type",
          "amount"                     => "Amount",
          "order-details"              => "Order Details",
          "details-of-order"           => "Details of Order :order",
     ],

     "payment" => [
          "title"  => [
               "singular" => "Payment",
          ],
          "status" => "Payment Status",

          "status-levels" => [
               "refunded"        => "Refunded",
               "canceled"        => "Canceled",
               "cancelled"       => "Canceled",
               "rejected"        => "Rejected",
               "verified"        => "Verified",
               "declared"        => "Waiting for Confirmation",
               "sent-to-gateway" => "Waiting for Gateway",
               "created"         => "Created",
          ],

          "message" => [
               "unsuccess-online-hint"       => "If during this process your account has lost any credit, it will be refunded in 72 hours.",
               "select-gateway-and-purchase" => "Select a gateway and purchase to complete your order.",
          ],
     ],

     "courier" => [
          "title" => [
               "singular" => "Sending Method",
               "plural"   => "Sending Methods",
          ],

          "select" => "Select Sending Method",
     ],

     "package" => [
          "title" => [
               "singular" => "Package",
               "plural"   => "Packages",
          ],
     ],

     "discount" => [
          "i-have-code"   => "I have discount code.",
          "register-code" => "Register Discount Code",
          "message"       => [
               "register-and-submit" => "Please insert your discount code and hit the register button.",
          ],
     ],


     "payment-method" => [
          "title" => [
               "plural" => "Payment Methods",
          ],

          "option" => [
               "online"  => "Online Payment",
               "deposit" => "Deposit",
               "shetab"  => "Card to Card",
               "cash"    => "Cash",
          ],
     ],
];
