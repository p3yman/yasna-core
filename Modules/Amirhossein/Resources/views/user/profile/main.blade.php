@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'profile';
    Template::appendToPageTitle(CurrentModule::trans('user.edit-profile'));
    Template::appendToNavBar([
        CurrentModule::trans('user.edit-profile'), CurrentModule::actionLocale('UserController@profile')
    ]);
@endphp

@section('dashboard-body')
    @include(CurrentModule::bladePath('user.profile.form'))
@append
