<tr data-hashid="{{ $order->hashid }}">
    <td>
        {{ ad($loop->iteration) }}
        @if (user()->isDeveloper())
            -
            {{ $order->id }}
        @endif
    </td>
    <td><a href="#" class="open-order-details">{{ $order->code }}</a></td>
    <td>{{ ad(echoDate($order->created_at, 'j F Y')) }}</td>
    <td>{{ ShopTools::showPrice($order->sale_price) }}</td>
    <td>
        {{--<div class="label alt @if($order->isPaid()) green @else red @endif">--}}
            {{--{{ $order->payment_status_text }}--}}
        {{--</div>--}}
        <div class="label alt @if($order->isConfirmed()) green @else red @endif">
            {{ $order->status_text }}
        </div>
    </td>
    <td class="tal pl0 pr0" width="36">
        <button class="icon-search open-order-details" type="button"></button>
    </td>
</tr>