<table class="table bordered">
    <thead>
    <tr>
        <th>{{ CurrentModule::trans('post.product.singular') }}</th>
        <th>{{ CurrentModule::trans('cart.price.unit') }}</th>
        <th>{{ CurrentModule::trans('cart.numeration-label.amount') }}</th>
        <th>{{ CurrentModule::trans('cart.price.total') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($cart->orders as $order)
        @include(CurrentModule::bladePath('user.orders.modal-content-products-item'))
    @endforeach
    </tbody>
</table>