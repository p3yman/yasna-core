<div class="row" style="margin-bottom: 20px">
    
    @php $isPaid = $cart->isPaid(); @endphp
    <div class="col-sm-6">
        <table class="table bordered">
            <thead>
            <tr style="background: #f9fafd;">
                <th colspan="100%" class="tac color-darker">
                    {{ CurrentModule::trans('cart.headline.order-status-summary') }}
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ CurrentModule::trans('cart.headline.receipt-number') }}</td>
                <td class="color-blue tac fw-b">{{ $cart->code }}</td>
            </tr>
            <tr>
                <td>{{ CurrentModule::trans('cart.price.total') }}</td>
                <td class="color-blue tac">
                    {{ ad(number_format(round($cart->sale_price / 10))) }}
                    {{ CurrentModule::trans('general.unit.currency.IRT') }}
                </td>
            </tr>
            <tr>
                <td>{{ CurrentModule::trans('cart.payment.status') }}</td>
                <td class="color-{{ $isPaid ? 'green' : 'red' }} tac">
                    @if ($isPaid)
                        <i class="icon-check"></i>
                    @else
                        -
                    @endif
                    {{ $cart->payment_status_text }}
                    @if (!$isPaid)
                        -
                    @endif
                </td>
            </tr>
            <tr>
                <td>{{ CurrentModule::trans('cart.headline.order-status') }}</td>
                <td class="tac">{{ $cart->status_text }}</td>
            </tr>
            </tbody>
        </table>
    </div>
    
    <div class="col-sm-6">
        @php
            $address = $cart->address;
        @endphp
        <table class="table bordered">
            <thead>
            <tr>
                <th colspan="100%" class="tac">{{ CurrentModule::trans('cart.headline.delivery-info') }}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="whs-nw">{{ CurrentModule::trans('cart.headline.receiver') }}</td>
                <td class="color-dark fw-b">{{ $address->receiver }}</td>
            </tr>
            <tr>
                <td>{{ CurrentModule::trans('general.address.title.singular') }}</td>
                <td class="color-dark">{{ $address->address }}</td>
            </tr>
            <tr>
                <td>{{ CurrentModule::trans('general.contact.telephone') }}</td>
                <td class="color-dark">{{ ad($address->telephone) }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
