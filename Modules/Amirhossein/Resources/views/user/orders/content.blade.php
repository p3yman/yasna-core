<section class="panel">
    <header>
        <div class="title"><span class="icon-cart"></span> سفارشات</div>
    </header>
    <article>
        <table class="table order-list">
            <thead>
            <tr>
                <th>{{ CurrentModule::trans('general.row') }}</th>
                <th>{{ CurrentModule::trans('cart.headline.receipt-number') }}</th>
                <th>{{ CurrentModule::trans('general.date') }}</th>
                <th>{{ CurrentModule::trans('cart.price.total') }}</th>
                <th>{{ CurrentModule::trans('general.status') }}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @include(CurrentModule::bladePath('user.orders.list'))
            </tbody>
        </table>
    </article>
</section>

@include(CurrentModule::bladePath('user.orders.modal'))