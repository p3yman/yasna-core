@extends(CurrentModule::bladePath('layouts.plane'))

@section('body')
    <div class="page-content profile mt5">
        @include(CurrentModule::bladePath('user.frame.header'))
        @yield('dashboard-body')
    </div>
@append

