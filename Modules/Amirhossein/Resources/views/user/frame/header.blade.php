<header class="profile-header">
    <div class="container">
        <div class="row mb40">
            @php user()->spreadMeta()  @endphp
            <div class="col-xs-12">
                <div class="avatar tac">
                    <img class="round-image" src="{{ user()->gravatar(CurrentModule::asset("images/user.svg")) }}" width="">
                </div>
                <h2 class="name"> {{ user()->full_name }} </h2>
                @php
                    $colors = ['blue','red', 'green'];
                    //$trendingEvents = user()->drawingRecentScores(3, 7)
                @endphp
                <div class="row">
                    
                    {{--@foreach($trendingEvents as $key => $event)--}}
                    {{--{{ null, $event->spreadMeta() }}--}}
                    {{--@if(\Carbon\Carbon::parse($event->ends_at)->lt(\Carbon\Carbon::now()))--}}
                    {{--{{ null, $class = 'gray' }}--}}
                    {{--{{ null, $icon = 'calendar-times-o' }}--}}
                    {{--@else--}}
                    {{--{{ null, $class = $colors[$key%count($colors)] }}--}}
                    {{--{{ null, $icon = 'calendar-check-o' }}--}}
                    {{--@endif--}}
                    {{--<div style="display: table-cell;">--}}
                    {{--<span class="label alt {{ $class }} md">--}}
                    {{--{{ $event->title }} :--}}
                    {{--{{ trans('front.all_user_score') }}--}}
                    {{--<strong>--}}
                    {{--{{ pd(floor($event->sum_amount / $event->rate_point)) }}--}}
                    {{--امتیاز--}}
                    {{--</strong>--}}
                    {{-- @TODO: rate_point should be set dynamically. Currently, was hardcode! --}}
                    {{--</span>--}}
                    {{--</div>--}}
                    {{--<div class="score-box {{ $class }}" style="width: {{ 100 / count($trendingEvents) }}%">--}}
                    {{--<div class="score-box-inner">--}}
                    {{--<div class="score-box-icon">--}}
                    {{--<i class="fa fa-{{ $icon }}"></i>--}}
                    {{--</div>--}}
                    {{--<div class="score-box-title">--}}
                    {{--{{ str_limit($event->title) }}--}}
                    {{--</div>--}}
                    {{--<div class="score-box-score">--}}
                    {{--{{ pd(floor($event->sum_amount / $event->rate_point)) }} امتیاز--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                </div>
            </div>
            @if(!array_default(model('user')::getRequiredFields(), user()->toArray()))
                <div class="col-xs-12 pt20">
                    <div class="row">
                        <div class="alert alert-danger text-right">
                            {{ CurrentModule::trans('user.not-enough-information') }}
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="{{ CurrentModule::actionLocale('UserController@profile') }}">
                                {{ CurrentModule::trans('user.edit-profile') }}
                            </a>
                        </div>
                    </div>
                </div>
            @endif
            <div class="profile-tabs col-xs-12">
                <div class="tab @if($activeTab == 'dashboard') active @endif">
                    <a href="{{ CurrentModule::actionLocale('UserController@index') }}">
                        <span class="icon-dashboard"></span> {{ CurrentModule::trans('user.dashboard') }} </a>
                </div>
                <div class="tab @if($activeTab == 'orders') active @endif">
                    <a href="{{ CurrentModule::actionLocale('UserController@orders') }}">
                        <span class="icon-cart"></span> {{ CurrentModule::trans('general.order.plural') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'profile') active @endif">
                    <a href="{{ CurrentModule::actionLocale('UserController@profile') }}">
                        <span class="icon-tag"></span> {{ CurrentModule::trans('user.edit-profile') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'addresses') active @endif">
                    <a href="{{ CurrentModule::actionLocale('UserController@addresses') }}">
                        <span class="fa fa-address-card-o"></span>
                        {{ CurrentModule::trans('general.address.title.plural') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'drawings') active @endif">
                    <a href="{{ CurrentModule::actionLocale('UserController@drawing') }}">
                        <span class="icon-pencil"></span> {{ CurrentModule::trans('general.drawing.code.registered-plural') }}
                    </a>
                </div>
                <div class="tab @if($activeTab == 'events') active @endif">
                    <a href="{{ CurrentModule::actionLocale('UserController@events') }}">
                        <span class="icon-calendar"></span> {{ CurrentModule::trans('post.event.plural') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>