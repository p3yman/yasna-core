@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'dashboard';
    Template::appendToPageTitle(CurrentModule::trans('user.dashboard'));
    Template::appendToNavBar([
        CurrentModule::trans('user.dashboard'), CurrentModule::actionLocale('UserController@index')
    ]);
@endphp

@section('dashboard-body')
    @if(setting('dashboard_comment')->gain())
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-center">
                    {!! $commentingPostPreview !!}
                </div>
            </div>
        </div>
    @endif
@append