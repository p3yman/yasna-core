@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'drawings';
    Template::appendToPageTitle(CurrentModule::trans('general.drawing.code.registered-plural'));
    Template::appendToNavBar([
        CurrentModule::trans('general.drawing.code.registered-plural'), CurrentModule::actionLocale('UserController@drawing')
    ]);
@endphp

@section('dashboard-body')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                <section class="panel">
                    <header>
                        <div class="title">
                            <span class="icon-tag"></span>
                            {{ CurrentModule::trans('general.drawing.code.registered-plural') }}
                        </div>
                        <div class="functions">
                            <button class="blue" data-modal="add-code-modal">
                                {{ CurrentModule::trans('general.drawing.code.add') }}
                            </button>
                        </div>
                    </header>
                    <article>
                        @if(!array_default(\App\Models\User::getRequiredFields(), user()->toArray()))
                            <div class="alert alert-danger text-right">
                                {{ CurrentModule::trans('user.message.complete-to-join-drawing') }}
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="{{ CurrentModule::actionLocale('UserController@profile') }}">
                                    {{ CurrentModule::trans('user.edit-profile') }}
                                </a>
                            </div>
                        @endif

                        @if($receipts->count())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th> {{ CurrentModule::trans('general.drawing.code.singular') }} </th>
                                    <th> {{ CurrentModule::trans('form.created-at') }} </th>
                                    <th> {{ CurrentModule::trans('form.purchased-at') }} </th>
                                    <th>
                                        {{ CurrentModule::trans('form.amount') }}
                                        ({{ CurrentModule::trans('general.unit.currency.IRR') }})
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($receipts as $receipt)
                                    <tr>
                                        <td class="fw-b color-green">{{ ad($receipt->dashed_code) }}</td>
                                        <td> {{ ad(echoDate($receipt->created_at, 'j F Y')) }} </td>
                                        <td> {{ ad(echoDate($receipt->purchased_at, 'j F Y')) }} </td>
                                        <td> {{ ad($receipt->amount_format) }} </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper mt20">
                                {!! $receipts->render() !!}
                            </div>
                        @else
                            <div class="alert red">
                                {{ CurrentModule::trans('user.message.drawing-code-not-found') }}
                            </div>
                        @endif
                    </article>
                </section>
                @include(CurrentModule::bladePath('user.drawing.form'))
            </div>
        </div>
    </div>
    @include(CurrentModule::bladePath('user.drawing.scripts'))
@append