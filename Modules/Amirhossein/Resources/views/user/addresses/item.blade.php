@php $itemId = $address->hashid @endphp
@php $radioId = 'address-' . $itemId @endphp

<div class="radio selectable-address green addresses-list-item" id="{{ $itemId }}">
    @php
        $jsonData = array_only(
            $address->toArray(),
            ['address', 'postal_code', 'location', 'telephone', 'province_id', 'city_id', 'receiver' ]
        );
        $jsonData['id'] = $address->hashid;
    @endphp
    <input type="hidden" class="json" value='{{ json_encode($jsonData) }}'>
    <input type="radio" class="select-address" id="{{ $radioId }}" name="default-address" value="{{ $itemId }}"
           @if($address->default) checked @endif>
    <label for="{{ $radioId }}">
        <span class="icon-check"></span>
        <span class="border"></span>
        <table class="table bordered">
            <tr>
                <td colspan="3">
                    <h5> {{ $address->receiver }} </h5>
                    @if ($address->location)
                        <div class="alert green compact">
                            {{ CurrentModule::trans('general.location.message.thanks-for-specifying-location') }}
                        </div>
                    @else
                        <div class="alert red compact">
                            {{ CurrentModule::trans('general.location.message.select-to-increase-precision') }}
                        </div>
                    @endif
                </td>
                <td rowspan="3" class="functions all xs-hide">
                    <a href="#" class="icon-pencil addresses-list-item-button btn-edit"></a>
                    <a href="#" class="icon-close addresses-list-item-button btn-remove"></a>
                </td>
            </tr>
            <tr>
                <td class="contact-info">{{ trans('yasna::states.province') }} : {{ $address->province->title }}</td>
                <td class="contact-info bottom" rowspan="2">{{ $address->address }}</td>
                <td class="contact-info">
                    {{ trans('validation.attributes.postal_code') }}: {{ ad($address->postal_code) }}
                </td>
            </tr>
            <tr>
                <td class="contact-info">{{ trans('yasna::states.city') }} : {{ $address->city->title }}</td>
                <td class="contact-info">
                    {{ CurrentModule::trans('general.contact.telephone') }}: {{ ad($address->telephone) }}
                </td>
            </tr>
            <tr>
                <td colspan="3" class="functions functions--horizontal">
                    <div class="functions-inner">
                        <a href="#" class="icon-pencil addresses-list-item-button btn-edit"></a>
                        <a href="#" class="icon-close addresses-list-item-button btn-remove"></a>
                    </div>
                </td>
            </tr>
        </table>
    </label>
</div>

