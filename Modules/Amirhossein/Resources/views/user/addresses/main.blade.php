@extends(CurrentModule::bladePath('user.frame.main'))

@php
    $activeTab = 'addresses';
    Template::appendToPageTitle(CurrentModule::trans('general.address.title.plural'));
    Template::appendToNavBar([
        CurrentModule::trans('general.address.title.plural'), request()->url()
    ]);
@endphp

@section('dashboard-body')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                <section class="panel">
                    <header>
                        <div class="title">
                            <span class="icon-tag"></span>
                            {{ CurrentModule::trans('general.address.title.plural') }}
                        </div>
                        <div class="functions">
                            <button class="blue" data-modal="add-address-modal">
                                {{ CurrentModule::trans('general.address.add') }}
                            </button>
                        </div>
                    </header>
                    @include(CurrentModule::bladePath('user.addresses.list'))
                </section>
                @include(CurrentModule::bladePath('user.addresses.form'))
            </div>
        </div>
    </div>
    @include(CurrentModule::bladePath('user.addresses.scripts'))
@append