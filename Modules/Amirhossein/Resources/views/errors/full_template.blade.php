<!DOCTYPE html>
<html>
<head>
    <title>
        @yield('error_code')!
    </title>
    
    {!! Html::style(CurrentModule::asset('css/fontiran.css')) !!}
    {!! Html::style(CurrentModule::asset('libs/font-awesome/css/font-awesome.min.css')) !!}
    {!! Html::style(CurrentModule::asset('css/errors.min.css')) !!}

</head>
<body @if (isLangRtl()) style="direction: rtl" @endif>
<div class="container">
    <div class="content">
        <div class="box">
            <h1 class="icon"><i class="fa fa-exclamation-triangle"></i></h1>
            @yield('message')
    
            <hr>
            <div class="links">
                <a href="{{ CurrentModule::actionLocale('FrontController@index') }}">
                    <i class="fa fa-home"></i>
                    {{ CurrentModule::trans('general.home') }}
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
