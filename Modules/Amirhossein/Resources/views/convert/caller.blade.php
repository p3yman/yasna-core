<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Converting {{ ucfirst($method) }}</title>
</head>
<body>
<h3>Result:</h3>
<div id="resultContainer">

</div>


{!! Html::script(CurrentModule::asset('libs/jquery/jquery.min.js')) !!}

<script>
    let uri = "{{ $route }}";
    let resultContainer = $('#resultContainer');

    $(document).ready(function () {
        ajaxCall();
    });

    function ajaxCall(offset = 0) {
        let url = uri.replace('__offset__', offset);
        let currentResultElement = $('<div></div>');

        $.ajax({
            url: url,
            dataType: 'json',
            beforeSend: function () {
                currentResultElement.html('Converting group number ' + (offset + 1) + ' ...');
                resultContainer.append(currentResultElement);
            },
            success: function (rs) {
                console.log(rs.success)
                if (rs.success) {
                    if (rs.continue) {
                        currentResultElement.html('Group number ' + (offset + 1) + ' converted');
                        ajaxCall(offset + 1);
                    } else {
                        currentResultElement.html('Finished ^________^');
                    }
                } else {
                    if (rs.errors) {
                        currentResultElement.html(
                            'Group number ' + (offset + 1) + ' ended with error(s): <br>' +
                            rs.errors.join('<br />')
                        );
                    } else {
                        currentResultElement.html('Group number ' + (offset + 1) + ' ended with error :(');
                    }
                }
            },
            error: function () {
                currentResultElement.html('Group number ' + (offset + 1) + ' ended with error :(');
            }
        });
    }
</script>
</body>
</html>