@extends(CurrentModule::bladePath('auth.layouts.plane'))

@php
	$title = CurrentModule::trans('user.login');
	Template::appendToPageTitle($title);
	Template::setRedTitle($title);
@endphp

@section('form')
	<form class="form-horizontal" role="form" method="POST"
		  action="{{ CurrentModule::actionLocale('Auth\LoginController@login') }}">
		{{ csrf_field() }}
		<div class="field">
			<input type="text" name="code_melli" id="code_melli"
				   placeholder="{{ trans('validation.attributes.code_melli') }}" required autofocus>
		</div>
		<div class="field">
			<input type="password" name="password" id="password"
				   placeholder="{{ trans('validation.attributes.password') }}" required>
		</div>

		@if (!env('APP_DEBUG'))
			{!! app('captcha')->render(getLocale()) !!}
		@endif

		<div class="tal">
			<button class="green block"> {{ CurrentModule::trans('user.login') }} </button>
		</div>
		<div class="row">
			<div class="col-xs-12 pt15 text-center">
				<a href="{{ CurrentModule::actionLocale('Auth\ForgotPasswordController@showLinkRequestForm') }}">
					{{ CurrentModule::trans('user.password.restore') }}
				</a>
			</div>
		</div>
		<hr class="or" data-text="{{ CurrentModule::trans('general.or') }}">
		<div class="tal">
			<a href="{{ CurrentModule::actionLocale('Auth\RegisterController@showRegistrationForm') }}"
			   class="button blue block">
				{{ CurrentModule::trans('user.register') }}
			</a>
		</div>

		@if($errors->all())
			<div class="alert alert-danger" style="margin-top: 10px;">
				@foreach ($errors->all() as $error)
					{{ $error }}<br>
				@endforeach
			</div>
		@endif

	</form>
@append