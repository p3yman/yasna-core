@extends(CurrentModule::bladePath('auth.layouts.plane'))

@php
    $title = CurrentModule::trans('user.password.restore');
    Template::appendToPageTitle($title);
    Template::setRedTitle($title);
@endphp
@section('form')
    {!! Form::open([
        'url' => request()->url(),
        'method'=> 'post',
        'class' => 'js',
        'name' => 'editForm',
        'id' => 'editForm',
        'style' => 'padding: 15px;',
    ]) !!}


    <div class="row">
        @include(CurrentModule::bladePath('layouts.widgets.form.input'),[
            'name' => 'new_password',
            'type' => 'password',
            'label' => false,
            'placeholder' => trans('validation.attributes.new_password'),
            'containerClass' => 'field',
        ])
    </div>

    <div class="row">
        @include(CurrentModule::bladePath('layouts.widgets.form.input'),[
            'name' => 'new_password2',
            'type' => 'password',
            'label' => false,
            'placeholder' => trans('validation.attributes.new_password2'),
            'containerClass' => 'field',
        ])
    </div>

    <div class="tal pb15">
        <button class="green block"> {{ CurrentModule::trans('form.button.save') }} </button>
    </div>

    @include(CurrentModule::bladePath('layouts.widgets.form.feed'))

    {!! Form::close() !!}

    @include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@append
