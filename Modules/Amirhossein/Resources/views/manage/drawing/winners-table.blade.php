@include("manage::forms.feed" , [
	'div_class' => "m10",
])

<span class="refresh">
    {{ CurrentModule::action('Manage\DrawingController@singleAction', ['hashid' => $model->hashid, 'viewFile' => 'winners-table']) }}
</span>
<table class="table table-striped">

    {{--
    |--------------------------------------------------------------------------
    | Header
    |--------------------------------------------------------------------------
    |
    --}}

    <thead>
    <tr>
        <td>#</td>
        <td>{{ trans('validation.attributes.name_first') }}</td>
        <td>{{ trans('validation.attributes.mobile') }}</td>
        <td>{{ CurrentModule::trans('cart.purchase.singular') }}</td>
        <td>&nbsp;</td>
    </tr>
    </thead>

    {{--
    |--------------------------------------------------------------------------
    | New Form
    |--------------------------------------------------------------------------
    |
    --}}
    @if($model->isDrawingReady())
        @include(CurrentModule::bladePath('manage.drawing.winners-add'))
    @endif


    {{--
    |--------------------------------------------------------------------------
    | Browser
    |--------------------------------------------------------------------------
    |
    --}}

    @foreach($model->winners_array as $key => $winner)
        @include(CurrentModule::bladePath('manage.drawing.winners-row'))
    @endforeach

    {{--
    |--------------------------------------------------------------------------
    | no-results
    |--------------------------------------------------------------------------
    |
    --}}

    @if(!count($model->winners_array))
        <td colspan="5">
            <div class="no-results m20">
                {{ CurrentModule::trans('general.drawing.no-winner-so-far') }}
            </div>
        </td>
    @endif
</table>
