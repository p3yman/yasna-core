@include('manage::layouts.modal-start' , [
	'form_url' => CurrentModule::action('Manage\DrawingController@select'),
	'modal_title' => CurrentModule::trans('general.drawing.winner').' '.$model->title,
])
<div class='modal-body'>

    <div id="divWinnersTable" class="panel panel-default m10">
        @include(CurrentModule::bladePath('manage.drawing.winners-table'))
    </div>

</div>

@if(!$model->isDrawingReady())
    <div class="modal-footer">
        @include("manage::forms.button" , [
            'label' => CurrentModule::trans('general.drawing.redraw'),
            'shape' => "primary",
            'link' => "masterModal('" . CurrentModule::action('DrawingController@draw', ['hashid' => $model->hashid]) . "')",
        ])
    </div>
@endif

@include('manage::layouts.modal-end')
