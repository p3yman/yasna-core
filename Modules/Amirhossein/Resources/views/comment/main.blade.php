<div class="comment-section">
    <h2>
        {{ CurrentModule::trans('general.comment.title.plural') }}
    </h2>
    @include(CurrentModule::bladePath('comment.comments'),[
        "comments" => [
            [
                "user" => [
                    "link" => "#" ,
                    "name" => "خانم نگار" ,
                    "src" => CurrentModule::asset('images/user-3.png') ,
                    "alt" => "avatar" ,
                ] ,
                "action" => [
                    "link" => "#" , 
                    "icon" => "mail-reply" ,
                     "name" => "پاسخ" ,
                     "event" => "" ,
                ] ,
                "time" => "دیروز" ,
                "comment" => "ممنونم از محصولات خوبتون. ^__^" ,
                "responses" => [
                    [
                        "user" => [
                            "link" => "#" ,
                            "name" => "مدیر" ,
                            "src" => CurrentModule::asset('images/user-3.png') ,
                            "alt" => "avatar" ,
                        ] ,
                        "action" => [
                            "link" => "#" ,
                            "icon" => "mail-reply" ,
                             "name" => "پاسخ" ,
                             "event" => "" ,
                        ] ,
                        "time" => "دیروز" ,
                        "comment" => "خواهش می‌کنم." ,
                        "responses" =>""
                    ],
                    [
                        "user" => [
                            "link" => "#" ,
                            "name" => "مدیر" ,
                            "src" => CurrentModule::asset('images/user-3.png') ,
                            "alt" => "avatar" ,
                        ] ,
                        "action" => [
                            "link" => "#" ,
                            "icon" => "mail-reply" ,
                             "name" => "پاسخ" ,
                             "event" => "" ,
                        ] ,
                        "time" => "دیروز" ,
                        "comment" => "خواهش می‌کنم." ,
                        "responses" =>""
                    ]
                ] ,

            ],
            [
                "user" => [
                    "link" => "#" ,
                    "name" => "ناشناس" ,
                    "src" => CurrentModule::asset('images/user-3.png') ,
                    "alt" => "avatar" ,
                ] ,
                "action" => [
                    "link" => "#" ,
                    "icon" => "mail-reply" ,
                     "name" => "پاسخ" ,
                     "event" => "" ,
                ] ,
                "time" => "دیروز" ,
                "comment" => "به‌به چه سایت خوبی" ,
                "responses" => "" ,
            ]
        ] ,
    ])
</div>
