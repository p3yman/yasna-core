@if ($comments->hasMorePages())
    @php
        $routeParameters = ['identifier' => RouteTools::$postDirectUrlPrefix . $post->hashid];
        if(isset($parent)) {
            $routeParameters['comment'] = $parent->hashid;
        }
    @endphp
    <div class="mb15 mt15">
        <a href="#" class="btn more"
           data-load-url="{{ CurrentModule::actionLocale('PostController@moreComments', $routeParameters) }}"
           data-page="{{ $comments->currentPage() }}">
            <i class="fa fa-chevron-circle-down" aria-hidden="true"></i>
            {{ CurrentModule::trans('general.comment.more') }}
        </a>
    </div>
@endif