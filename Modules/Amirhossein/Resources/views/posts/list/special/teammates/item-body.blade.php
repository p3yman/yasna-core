<div class="avatar">
    {{--- If the post has a valid featured image, it will be shown, --}}
    {{--- else if the posttype has a valid default featured image it will be shown --}}
    {{--- and else the not found image will be shown. --}}
    @php
        $personImage = CurrentModule::asset("images/image-not-found.png");
        if($person->featured_image) {
            $postImage = doc($person->featured_image)->getUrl();
        }
        if(isset($postImage)) {
            $personImage = $postImage;
        } else {
            if($posttype->default_featured_image) {
                $posttypeImage = doc($posttype->default_featured_image)->getUrl();
            }
            if(isset($posttypeImage)) {
                $personImage = $posttypeImage;
            }
        }
    @endphp
    <img src="{{ $personImage }}"></div>
<div class="content">
    <h3> {{ $person->title }} </h3>
    <h4> {{ $person->seat }} </h4>
    @if(isset($showText) and $showText)
        <p> {!! $person->abstract !!} </p>
    @endif
</div>
