@if(sizeof($posts))
    @foreach($posts as $event)
        @include(CurrentModule::bladePath($viewFolder . '.item'))
    @endforeach

    @if($posts instanceof \Illuminate\Pagination\LengthAwarePaginator)
        <div class="pagination-wrapper">
            {!! $posts->render() !!}
        </div>
    @endif
@endif
