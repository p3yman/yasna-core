@php $ajaxFilter = true @endphp
@if($ajaxFilter)
    @include(CurrentModule::bladePath($viewFolder . '.ajax-filter-assets'))
@else
    @include(CurrentModule::bladePath($viewFolder . '.filter-assets'))
@endif

<div class="col-sm-3 sidebar-filter">
    <button class="block blue filter-opener" onclick="filterOpener()">
        <i class="fa fa-filter" aria-hidden="true"></i>
        {{ CurrentModule::trans('filter.title.plural') }}
    </button>
    <div class="filter-container">
        <div class="category-filters filters-panel"
             data-filter-url="{{ CurrentModule::actionLocale('PosttypeController@filter', ['posttype' => 'products']) }}">
            <span class="filter-panel--close" onclick="filterCloser()">
                <em class="icon-cancel"></em>
            </span>
            <div class="title"> {{ CurrentModule::trans('filter.title.plural') }}</div>
            <article>
                @include(CurrentModule::bladePath($viewFolder . '.filter-title'))
                @include(CurrentModule::bladePath($viewFolder . '.filter-price'))
                @include(CurrentModule::bladePath($viewFolder . '.filter-categories'))
                @include(CurrentModule::bladePath($viewFolder . '.filter-packages'))
                @include(CurrentModule::bladePath($viewFolder . '.filter-availability'))
                @include(CurrentModule::bladePath($viewFolder . '.filter-special-sale'))
                <hr class="small">
                @include(CurrentModule::bladePath($viewFolder . '.filter-refresh'))

                <button class="block blue mt20 filter-submit" onclick="filterCloser()">
                    {{ CurrentModule::trans('filter.option.filtering') }}
                </button>
            </article>
        </div>
    </div>

</div>