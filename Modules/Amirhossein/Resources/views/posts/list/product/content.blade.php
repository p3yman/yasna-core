@if(!$ajaxRequest)
    @include(CurrentModule::bladePath($viewFolder . '.top-bar'))
    <div class="row">
    @if($showFilter)
        @include(CurrentModule::bladePath($viewFolder . '.sidebar'))
        <div class="col-sm-9 has-dialog">
    @else
        <div class="col-sm-12 has-dialog">
    @endif
@endif

@include(CurrentModule::bladePath($viewFolder . '.list'))

@if(!$ajaxRequest)
            <div class="alert alert-danger" id="hidden-alert" style="display: none; position: absolute; top: 0; z-index: 999">
                {{ CurrentModule::trans('form.feed.error') }}
            </div>
            @include(CurrentModule::bladePath('layouts.widgets.dialog.loading'), [
                'id' => 'loading-dialog'
            ])
        </div>
    </div>
@endif
