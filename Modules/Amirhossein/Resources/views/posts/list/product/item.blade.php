@php
    // @todo: read best version of product photo depending on ui
    $post->spreadMeta();
    $posttype = $post->posttype()->spreadMeta();
    $postImage = CurrentModule::asset("images/image-not-found.png");
    if(
        $post->featured_image and
        ($dbImage = doc($post->featured_image)
            ->posttype($posttype)
            ->config('featured_image')
            ->version('235x198')
            ->getUrl())
        ) {
        $postImage = $dbImage;
    } else if ($posttypeImage = doc($posttype->default_featured_image)
            ->posttype($posttype)
            ->config('featured_image')
            ->version('235x198')
            ->getUrl()) {
        $postImage = $posttypeImage;
    }

    $isOnSale = $post->ware_original_price > $post->ware_sale_price;
@endphp
<div class="col-lg-3 col-sm-4 filterable"
     data-category="{{ implode(',', $post->categories->pluck('slug')->toArray()) }}"
     data-available="{{ ((int) $post->is_available) }}"
     data-special-sale="{{ ((int) ($isOnSale)) }}"
     data-title="{{ $post->title }}"
     data-price="{{ $post->current_price }}">
    {{--<a href="#" class="product-item ribbon-new ribbon-sale">--}}
    <a href="{{ $post->direct_url }}"
       class="product-item @if($post->isIt('NEW')) ribbon-new @endif @if($isOnSale) ribbon-sale @endif">
        <div class="thumbnail"><img src="{{ $postImage }}">
        </div>
        <div class="content">
            <h6>
                {{ $post->title }}
                @if(user()->isDeveloper())
                    - {{ $post->id }}
                @endif
            </h6>
            <div class="price">
                @if($post->is_available)
                    @if($isOnSale)
                        <del>
                            {{ ShopTools::showPrice($post->ware_original_price, false) }}
                        </del>
                        <ins>
                            {{ ShopTools::showPrice($post->ware_sale_price) }}
                        </ins>
                    @else
                        <ins>
                            {{ ShopTools::showPrice($post->ware_original_price) }}
                        </ins>
                    @endif
                @else
                    <div class="status">
                        <div class="label red"> {{ CurrentModule::trans('post.product.is-not-available') }}</div>
                    </div>
                @endif
            </div>
        </div>
    </a>
</div>