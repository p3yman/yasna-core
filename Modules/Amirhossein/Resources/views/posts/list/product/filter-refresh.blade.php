<div class="text-center">
    <a class="green button-link" href="#">
        <i class="fa fa-refresh"></i>
        {{ CurrentModule::trans('filter.option.refresh') }}
    </a>
</div>