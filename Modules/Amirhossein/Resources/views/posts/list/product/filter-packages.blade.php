@php $allPackages = ShopTools::getAllPackages(true) @endphp
@if(count($allPackages) > 1)
    <hr class="small">
    <div class="field mb0 filter-checkbox" data-identifier="package">
        <label class="label-big">
            {{ CurrentModule::trans('cart.package.title.plural') }}
        </label>
        @foreach($allPackages as $package)
            <div class="checkbox blue mb10">
                <input id="package-{{ $package->hashid }}" value="{{$package->hashid}}" type="checkbox" checked="checked">
                <label for="package-{{ $package->hashid }}"> {{ $package->titleIn(getLocale()) }} </label>
            </div>
        @endforeach
    </div>
@endif