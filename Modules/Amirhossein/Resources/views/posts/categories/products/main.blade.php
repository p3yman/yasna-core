@php
    $categories = $posttype->categories()->where('is_folder', 1)->get();
@endphp

@foreach($categories as $cat)
    @include(CurrentModule::bladePath('posts.categories.products.item'))
@endforeach