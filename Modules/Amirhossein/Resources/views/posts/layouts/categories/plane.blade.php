@extends(CurrentModule::bladePath('layouts.plane'))

@php
    Template::appendToPageTitle($posttype->titleIn(getLocale()));
    Template::appendToNavBar([
        CurrentModule::trans('general.categories-of-something', ['something' => $posttype->titleIn(getLocale())]),
        request()->url(),
    ]);
@endphp

@section('body')
    <div class="page-content">
        <div class="container">
            <div class="part-title">
                <h3>
                    @php
                        $partTitleParts = CurrentModule::trans("general.categories-of-something-partial");
                        $partTitleParts = str_replace(":something", $posttype->titleIn(getLocale()), $partTitleParts);
                    @endphp
                    {{ $partTitleParts['part1'] }}
                    <span>{{ $partTitleParts['part2'] }}</span>
                    {{ $partTitleParts['part3'] }}
                </h3>
            </div>
            <div class="cats">
                <div class="row">
                    {!! $categories_view !!}
                </div>
            </div>
        </div>
    </div>
@append
