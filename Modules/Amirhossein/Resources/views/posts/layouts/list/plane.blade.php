@extends(CurrentModule::bladePath('layouts.plane'))

@php
    Template::appendToPageTitle($posttype->titleIn(getLocale()));
    Template::appendToNavBar([
        $posttype->titleIn(getLocale()),
        ($posttype->slug != 'products')
            ? RouteTools::getPosttypeRoute($posttype->slug)
            : RouteTools::getPosttypeCategoriesRoute('products')
    ]);
    if (isset($folder)) {
    Template::appendToPageTitle($folder->titleIn(getLocale()));
    Template::appendToNavBar([
        $folder->titleIn(getLocale()), request()->url()
    ]);
    }
@endphp

@section('body')
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="@if(isset($fullPage) and $fullPage) col-xs-12 @else col-md-11 col-center @endif">
                    {!! $list !!}
                </div>
            </div>
        </div>
    </div>
@append

