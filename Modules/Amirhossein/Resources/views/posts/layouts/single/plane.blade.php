@extends(CurrentModule::bladePath('layouts.plane'))

@php
    $posttype = $post->posttype;
    Template::appendToPageTitle($posttype->titleIn(getLocale()));
    Template::appendToPageTitle($post->title);
    Template::appendToNavBar([
        $posttype->titleIn(getLocale()), RouteTools::getPosttypeRoute($posttype->slug)
    ]);
    Template::appendToNavBar([
        $post->title, request()->url()
    ]);
    if ($post->getMeta('show_header')) {
        if ($postCover = doc($post->getMeta('header_image'))->getUrl()) {
            $coverSrc = $postCover;
        } else if(
            ($defaultCategory = $post->default_category_model) and
            $categoryImage = doc($defaultCategory->imageIn(getLocale()))->getUrl()
        ) {
            $coverSrc = $categoryImage;
        }
    }
    $cover_img = boolval($post->getMeta('show_header') and isset($coverSrc));
@endphp

@section('body')
    @php $viewVariables = Template::viewVariables(); @endphp
    @if(!$viewVariables or !is_array($viewVariables))
        @php $viewVariables = [] @endphp
    @endif

    @if($cover_img)
        @include(CurrentModule::bladePath('posts.layouts.single.cover'),[
            "cover" => [
                "src" => $coverSrc ,
                "alt" => "cover" ,
            ] ,
        ])
    @endif
    <div class="page-content
            @if($cover_img) cover-layout @endif
            @if(array_key_exists('mainContentClass', $viewVariables)) {{ $viewVariables['mainContentClass'] }} @endif">
        <div class="container">
            {!! $post_view !!}
        </div>
    </div>
@append
