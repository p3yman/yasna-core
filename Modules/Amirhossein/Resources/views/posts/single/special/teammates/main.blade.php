@php
    $post->spreadMeta();
    $posttype = $post->posttype;
@endphp

<div class="row">
    <div class="col-sm-10 col-center">
        @include(CurrentModule::bladePath($viewFolder . '.header'))
        @include(CurrentModule::bladePath($viewFolder . '.content'))
    </div>
</div>