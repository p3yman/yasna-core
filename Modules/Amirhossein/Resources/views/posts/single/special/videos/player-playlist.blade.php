@if(count($files) > 1)
    <div class="col-xs-12 player-list pt10 pb10">
        @foreach($files as $key => $file)
            @include(CurrentModule::bladePath($viewFolder . '.player-playlist-item'))
        @endforeach
    </div>
@endif