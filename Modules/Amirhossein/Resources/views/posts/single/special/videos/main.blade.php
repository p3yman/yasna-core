@php
    $files = new \Modules\Amirhossein\Services\PostFiles\Tools\PostFilesCollection($post->files);
@endphp

<div class="row">
    <div class="col-sm-10 col-center">
        @include(CurrentModule::bladePath($viewFolder . '.text'))
        @include(CurrentModule::bladePath($viewFolder . '.player'))
        @include(CurrentModule::bladePath($viewFolder . '.scripts'))
    </div>
</div>