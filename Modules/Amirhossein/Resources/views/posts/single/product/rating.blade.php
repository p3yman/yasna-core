<div class="rating-alert mv15">
    <div class="row">
        <div class="col-sm-8">
            <div class="message">
                {{ CurrentModule::trans('general.message.rate-product',['user' => 'نگار']) }}
            </div>
        </div>
        <div class="col-sm-4">
            <div class="rating">
                {!! Form::open(['url' => 'foo/bar']) !!}
                        <span class="star-cb-group">
                            @for($rate = 0; $rate <=5; $rate++)
                                @include(CurrentModule::bladePath('posts.single.product.rating-star'),[
                                    "rate" => $rate ,
                                ])
                            @endfor
                        </span>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
