@php
    $posttype = $post->posttype->spreadMeta();
    Template::mergeWithViewVariables(['mainContentClass' => 'product-single']);
@endphp

@php
    $post->spreadMeta();
    $wares = $post->wares;
    $previewCurrency = CurrencyTools::getPreviewCurrency();
    $previewCurrencyTrans = CurrentModule::trans('general.unit.currency.' . $previewCurrency);

    if(!isset($selectedWare) or !$selectedWare or !$selectedWare->exists) {
        $selectedWare = $wares->first();
    }
@endphp
<div class="row">
    <div class="col-sm-5">
        @include(CurrentModule::bladePath($viewFolder . '.image'))
    </div>
    <div class="col-sm-7 product-detials">
        <h2 class="product-name">{{ $post->title }}</h2>
        @include(CurrentModule::bladePath($viewFolder . '.price'))
        @include(CurrentModule::bladePath($viewFolder . '.add-to-cart'))
        <hr>
        @include(CurrentModule::bladePath($viewFolder . '.meta'))
        @include(CurrentModule::bladePath('layouts.widgets.dialog.loading'), [
                'id' => 'cart-adding-loading'
            ])
    </div>
</div>
<hr>

{{--@include(CurrentModule::bladePath($viewFolder . '.content')--}}
{{--@include(CurrentModule::bladePath($viewFolder. '.rating')--}}
{{--<hr>--}}
@include(CurrentModule::bladePath($viewFolder . '.abstract'))
@include(CurrentModule::bladePath($viewFolder . '.tab-wrapper'), [
    "tabs" => [
        [
            "title" => CurrentModule::trans('general.description') ,
            "link" => "#details" ,
            "active" => true ,
        ],
        [
            "title" => CurrentModule::trans('general.comment.title.plural') ,
            "link" => "#comments" ,
            "active" => false ,
        ],
    ] ,
])
@include(CurrentModule::bladePath($viewFolder . '.similar-posts'))
@include(CurrentModule::bladePath($viewFolder . '.scripts'))
@include(CurrentModule::bladePath($viewFolder . '.notification'))