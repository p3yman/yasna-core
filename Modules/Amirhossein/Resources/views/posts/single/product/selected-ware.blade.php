@php
    $selectedWare = $selectedWare->spreadMeta()->inLocale(getLocale())->in($previewCurrency);
    $unit = $selectedWare->unit;
@endphp
@if ($wares->count() > 1)
    <h3 class="product-name">{{ $selectedWare->titleIn(getLocale()) }}</h3>
@endif
<div class="excerpt"><p>{{ $selectedWare->remarkIn(getLocale()) }}</p></div>
<hr/>
<div class="price">
    @if($selectedWare->isOnSale())
        <ins>
            {{ ad(number_format($selectedWare->salePrice()->in($previewCurrency)->get())) }}
            {{ $previewCurrencyTrans }}
        </ins>
        <del>
            {{ ad(number_format($selectedWare->originalPrice()->in($previewCurrency)->get())) }}
            {{ $previewCurrencyTrans }}
        </del>
    @else
        <ins>
            {{ ad(number_format($selectedWare->originalPrice()->in($previewCurrency)->get())) }}
            {{ $previewCurrencyTrans }}
        </ins>
    @endif
    <div class="status">
        <div class="label green"> {{ CurrentModule::trans('post.product.is-available') }}</div>
    </div>
</div>
@if($selectedWare->isPurchasable())
    <div class="field add-to-card mt20">
        {{--<label>تعداد</label>--}}
        <div class="row">
            <div class="col-xs-6 col-sm-3 mb5">
                <input class="ware-count product-counter @if(!$unit->is_discrete) float @endif" value="{{ ad(1) }}"
                       min="1" data-ware="{{ $selectedWare->hashid }}">
            </div>
            <div class="col-xs-6 col-sm-2 mb5">
                <div class="add-label">
                    <div class="content">
                        <h4>{{ $unit->title }}</h4>
                    </div>
                </div>
                {{--<label>تعداد</label>--}}
                {{--<div class="select">--}}
                {{--<select>--}}
                {{--<option value="1">کیلو</option>--}}
                {{--<option value="2">بسته</option>--}}
                {{--</select>--}}
                {{--</div>--}}
            </div>
            <div class="col-md-4 col-sm-7 mb5">
                <button class="block green add-to-cart" data-ware="{{ $selectedWare->hashid }}">
                    {{ CurrentModule::trans('cart.button.add') }}
                </button>
            </div>
        </div>
    </div>
@endif