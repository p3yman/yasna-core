@include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@section('html-footer')
    <script>
        let token = $('meta[name=csrf-token]').attr("content");

        $('document').ready(function () {
            let notification = new Notif({
                topPos: 50,
                classNames: 'success danger',
            });
            $(document).on({
                click: function (e) {
                    e.preventDefault();
                    let that = $(this);
                    let Loading = $('#cart-adding-loading');
                    let ware = that.data('ware');
                    let count = $('.ware-count[data-ware=' + ware + ']').val();
                    notification.hide();
                    Loading.show();
                    if (ware && count) {
                        $.ajax({
                            url: "{{ CurrentModule::actionLocale('CartController@addItem') }}",
                            type: "POST",
                            data: {
                                _token: token,
                                ware: ware,
                                count: count,
                            },
                            success: function () {
                                arguments[arguments.length] = $('.form');
                                forms_responde.apply(arguments, Object.values(arguments));
                                //that.removeClass('disabled');
                                Loading.hide().removeAttr('style');
                                notification.show('{{ CurrentModule::trans('cart.alert.success.order-add') }}','success');
                            },
                            error: function () {
                                //that.removeClass('disabled');
                                Loading.hide().removeAttr('style');
                                notification.show('{{ CurrentModule::trans('cart.alert.error.order-add') }}' ,'danger');
                            }
                        })
                    }
                },
            }, '.add-to-cart')
        });
    </script>
@append