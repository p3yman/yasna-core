@if ($post->abstract)
    <div class="abstract-content">
        <p>{{ $post->abstract }}</p>
    </div>
    <hr>
@endif
