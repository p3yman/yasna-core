@php
    if(isset($status) and $status){
        $class = "success";
        $text = "success";
    }else{
        $class = "danger";
        $text = "error";
    }
@endphp


<div role="alert" class="alert alert-{{ $class }} ">
    {{ CurrentModule::trans("cart.alert.$text.order-add") }}
</div>