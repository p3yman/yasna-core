<div class="product-meta">
    <div class="row">
        <div class="col-md-2 col-sm-3">
            <div class="meta-title"> {{ CurrentModule::trans('general.categories') }}:</div>
        </div>
        <div class="col-md-10 sol-sm-9">
            <div class="meta-value">
                <ul class="categories">
                    @foreach($post->categories as $category)
                        <li>
                            <a href="{{ $posttype->list_url }}/{{ $category->slug }}" target="_blank">
                                {{ $category->title }}
                            </a>
                        </li>
                        @section('product-tags')
                            <li>
                                <a href="{{ $posttype->list_url }}/{{ $category->slug }}" target="_blank">
                                    {{ $category->title }}
                                </a>
                            </li>
                        @append
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-3">
            <div class="meta-title"> {{ CurrentModule::trans('general.tag.title.plural') }}:</div>
        </div>
        <div class="col-md-10 sol-sm-9">
            <div class="meta-value">
                <ul class="tags">
                    @yield('product-tags')
                </ul>
            </div>
        </div>
    </div>
    @if($selectedWare and $selectedWare->exists)
        <div class="row">
            <div class="col-sm-2">
                <div class="meta-title">{{ CurrentModule::trans('post.product.code') }}:</div>
            </div>
            <div class="col-sm-10">
                <div class="meta-value">
                    <div class="code">{{ ad($selectedWare->code) }}</div>
                </div>
            </div>
        </div>
    @endif
</div>