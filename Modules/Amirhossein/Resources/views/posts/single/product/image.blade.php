<div class="product-images">
    @php $album = $post->files @endphp
    @if($album and count($album))
        <ul class="slides
        @if($post->isIt('NEW')) ribbon-new @endif
        @if($post->isOnSale()) ribbon-sale @endif
                ">
            @foreach($album as $image)
                @php
                    $originalFetchedImage = doc($image['src'])->getUrl();
                    $fetchedImage = doc($image['src'])
                        ->posttype($posttype)
                        ->config('featured_image')
                        ->version('470x396')
                        ->getUrl();
                @endphp
                @if ($originalFetchedImage)
                    <li>
                        <a href="{{ $originalFetchedImage }}" data-lightbox="product-gallery">
                            <img src="{{ $fetchedImage }}"></a>
                    </li>
                    @section('thumbnails-list')
                        {{--@todo: it should be read from thumbnail--}}
                        <li>
                            <a href="#"><img src="{{ $fetchedImage}}"></a>
                        </li>
                    @append
                @endif
            @endforeach
        </ul>
        <ul class="thumbnails @if(count($album)<=4) no-padding @endif" id="product-gallery-thumbnails" data-rtl="false">
            @yield('thumbnails-list')
        </ul>
    @else
        <ul class="slides
        @if($post->isIt('NEW')) ribbon-new @endif
        @if($post->isOnSale()) ribbon-sale @endif
                ">
            @php
                $originalFetchedImage = doc($post->featured_image)->getUrl();
                $fetchedImage = doc($post->featured_image)
                    ->posttype($posttype)
                    ->config('featured_image')
                    ->version('470x396')
                    ->getUrl();
            @endphp
            <li>
                <a href="{{ $originalFetchedImage }}" data-lightbox="product-gallery"><img
                            src="{{ $fetchedImage }}"></a>
            </li>
        </ul>
    @endif

</div>