<div class="row">
    <div class="col-sm-10 col-center">
        <div class="blog-single">
            <div class="blog-cover">
                @php
                    $posttype = $post->posttype;
                    $postImage = doc($post->featured_image)
                        ->getUrl();
                @endphp
                @if($postImage)
                    <img src="{{ $postImage }}">
                @endif
            </div>
            <div class="blog-header">
                @include(CurrentModule::bladePath($viewFolder . '.categories'))
                <h1 class="title"> {{ $post->title }} </h1>
                @include(CurrentModule::bladePath($viewFolder . '.publish_info'))
            </div>
            @include(CurrentModule::bladePath($viewFolder . '.content'))
            @include(CurrentModule::bladePath($viewFolder . '.album'))
        </div>
        @include(CurrentModule::bladePath($viewFolder . '.related_posts'))
    </div>
</div>