@php $similarPosts = $post->similars(3) @endphp

@if($similarPosts and $similarPosts->count())
    <div class="part-title mt50">
        <h3>
            @php
                $relatedTitleParts = str_replace(
                    ':things',
                    $posttype->titleIn(getLocale()),
                    CurrentModule::trans('general.related-things')
                );
            @endphp
            {{ $relatedTitleParts['part1'] }}
            <span>{{ $relatedTitleParts['part2'] }}</span>
            {{ $relatedTitleParts['part3'] }}
        </h3>
    </div>
    <div class="row">
        @foreach($similarPosts as $similarPost)
            <div class="col-sm-4">
                <div class="blog-item style-2">
                    <a href="{{ $similarPost->direct_url }}">
                        @php
                            $post->spreadMeta();
                            $similarPostImage = CurrentModule::asset("images/image-not-found.png");
                            if($similarPost->featured_image) {
                                $similarPostImage = doc($similarPost->featured_image)
                                    ->posttype($post->posttype)
                                    ->config('featured_image')
                                    ->version('240x130')
                                    ->getUrl();
                            }
                        @endphp
                        <div class="thumbnail"><img src="{{ $similarPostImage }}"></div>
                        <div class="content">
                            <h3 class="title">{{ str_limit($similarPost->title, 30) }}</h3>
                            <p class="text-gray f12">
                                {{ pd(echoDate($similarPost->published_at, 'd F Y')) }}
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endif