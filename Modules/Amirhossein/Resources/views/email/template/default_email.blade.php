@extends(CurrentModule::bladePath('email.template.email_frame'))

@section('email_content')
    {!! $content !!}
@endsection