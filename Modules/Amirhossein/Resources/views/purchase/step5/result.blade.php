<div class="col-sm-4">
    @if ($isDisbursed or ($type == 'cash'))
        @include(CurrentModule::bladePath('purchase.step5.final-page'))
    @endif
    
    @include(CurrentModule::bladePath('purchase.step5.make-action'))
</div>