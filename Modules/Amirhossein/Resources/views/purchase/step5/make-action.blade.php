@if (!$isDisbursed)
    @if (in_array($statusSlug, ['sent-to-gateway', 'created']))
        @if ($transactionHandler->isDeclarable())
            @include(CurrentModule::bladePath('purchase.step5.declare-form'))
        @endif
    @endif
    
    @include(CurrentModule::bladePath('purchase.step5.make-action-online'))
@endif
