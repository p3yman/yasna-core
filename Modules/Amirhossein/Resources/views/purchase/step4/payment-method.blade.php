<div class="payments-methods">
    <h4 class="mb10 color-gray">{{ CurrentModule::trans('cart.payment-method.title.plural') }}</h4>
    @include(CurrentModule::bladePath('purchase.step4.payment-method-online'))
    @include(CurrentModule::bladePath('purchase.step4.payment-method-deposit'))
    @include(CurrentModule::bladePath('purchase.step4.payment-method-shetab'))
    @include(CurrentModule::bladePath('purchase.step4.payment-method-cash'))
</div>