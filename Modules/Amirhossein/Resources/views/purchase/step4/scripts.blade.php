@section('html-footer')
    <script>
        $(document).ready(function () {
            $('input[type=radio][name=paymentType]:first, input[type=radio][name=onlineAccount]:first')
                .prop('checked', true)
                .change();
            
            @if(env('APP_DEBUG'))
            $('form#payment-form').prop('target', '_blank');
            @endif
        });
    </script>
@append