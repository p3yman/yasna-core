@extends(CurrentModule::bladePath('purchase.layout.plane'))

@php
    $currentStep = 4;
    $anyAccountExists = $onlineAccounts->count() or
        $depositAccounts->count() or
        $shetabAccounts->count() or
        $cashAccounts->count();
@endphp

@section('inner-body')
    @if ($anyAccountExists)
        {!! Form::open([
                'url' => CurrentModule::actionLocale('PurchaseController@final'),
                'method'=> 'post',
                'id' => 'payment-form',
            ]) !!}
        {{--@include(CurrentModule::bladePath('purchase.step4.discount-code'))--}}
        {{--<hr>--}}
        @include(CurrentModule::bladePath('purchase.step4.payment-method'))
        @include(CurrentModule::bladePath('purchase.step4.buttons'))
        {!! Form::close() !!}
    @else
        <div class="alert">{{ CurrentModule::trans('cart.alert.error.system-error-in-purchasement') }}</div>
    @endif
@append

@include(CurrentModule::bladePath('purchase.step4.scripts'))