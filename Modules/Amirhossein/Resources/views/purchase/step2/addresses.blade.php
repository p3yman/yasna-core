<div class="col-sm-7">
	<section class="panel">
		<header>
			<div class="title">
				<span class="icon-location"></span>{{ CurrentModule::trans('general.address.select') }}
			</div>
			<div class="functions">
				<button class="blue" data-modal="add-address-modal">
					{{ CurrentModule::trans('general.address.add') }}
				</button>
			</div>
		</header>
		<article>
			@if ($addresses->count())
				@foreach($addresses as $address)
					@include(CurrentModule::bladePath('purchase.step2.address-item'))
				@endforeach
			@else
				<div class="alert text-center">
					{{ CurrentModule::trans('general.address.no-address-alert') }}
				</div>
			@endif
		</article>
	</section>
</div>

@section('inner-body-modals')
	@include(CurrentModule::bladePath('user.addresses.form'))
@append
