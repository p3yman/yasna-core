@include(CurrentModule::bladePath('layouts.widgets.form.feed'), ['div_class' => 'verify-feedback'])
<section class="panel checkout-actions">
	<article>
		<a href="{{ CurrentModule::actionLocale('CartController@index') }}"
		   class="button">{{ CurrentModule::trans('form.button.back') }}</a>
		<button class="button green" type="button" id="continue">
			{{ CurrentModule::trans('cart.button.confirm-and-review') }}
		</button>
	</article>
</section>