<div class="cart-full" id="page-content">
    @include(CurrentModule::bladePath('purchase.step3.cart-items-list'))
    @include(CurrentModule::bladePath('purchase.step3.cart-items-list-tablet'))
    @include(CurrentModule::bladePath('purchase.step3.cart-total'))
    @include(CurrentModule::bladePath('purchase.step3.cart-delivery'))
</div>