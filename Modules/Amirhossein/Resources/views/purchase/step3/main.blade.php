@extends(CurrentModule::bladePath('purchase.layout.plane'))

@php
    $currentStep = 3;
@endphp

@section('inner-body')
    <div class="">
        @include(CurrentModule::bladePath('purchase.step3.cart'))
        @include(CurrentModule::bladePath('purchase.step3.buttons'))
    </div>
@append

@include(CurrentModule::bladePath('purchase.step3.scripts'))
