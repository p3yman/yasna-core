@php $orders = $cart->orders; @endphp
@if ($orders->count())
    @include(CurrentModule::bladePath('cart.nonempty'))
@else
    @include(CurrentModule::bladePath('cart.empty'))
@endif