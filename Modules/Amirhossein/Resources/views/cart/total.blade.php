<div class="cart-total-container">
    <table class="table bordered cart-total">
        <tbody>
        @if($cart->original_price != $cart->sale_price)
            <tr>
                <td>{{ CurrentModule::trans('cart.price.your-purchase-total') }}:</td>
                <td>
                    {{ ad(number_format(round($cart->original_price / 10))) }}
                    {{ CurrentModule::trans('general.unit.currency.IRT') }}
                </td>
            </tr>
            <tr>
                <td>{{ CurrentModule::trans('cart.headline.discount') }}:</td>
                <td>
                    {{ ad(number_format(round(($cart->original_price - $cart->sale_price) / 10))) }}
                    {{ CurrentModule::trans('general.unit.currency.IRT') }}
                </td>
            </tr>
        @endif
        <tr class="total">
            <td>{{ CurrentModule::trans('cart.price.purchasable') }}:</td>
            <td>
                {{ ad(number_format(round($cart->sale_price / 10))) }}
                {{ CurrentModule::trans('general.unit.currency.IRT') }}
            </td>
        </tr>
        </tbody>
    </table>
</div>
