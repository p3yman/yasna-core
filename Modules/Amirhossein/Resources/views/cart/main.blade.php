@extends(CurrentModule::bladePath('layouts.plane'))

@php
    Template::appendToPageTitle(CurrentModule::trans('cart.title.singular'));
    Template::appendToNavBar([
        CurrentModule::trans('cart.title.singular'), request()->url()
    ]);
@endphp

@section('body')
    <div class="page-content">
        <div class="container">
            @include(CurrentModule::bladePath('cart.content'))
        </div>
    </div>
@append

@include(CurrentModule::bladePath('cart.scripts'))

