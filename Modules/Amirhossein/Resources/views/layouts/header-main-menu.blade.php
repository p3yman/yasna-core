<ul class="menu">
	<li class="search-form">
		<form action="{{ CurrentModule::actionLocale('PosttypeController@productsSearch') }}">
			<input type="text" id="site-search" name="search" placeholder="">
			<em class="icon-close" id="close-search"></em>
			<button type="submit">
				<em class="icon-search"></em>
			</button>
		</form>
	</li>
	@foreach (Template::mainMenu() as $menu)
		<li>
			<a href="{{ url($menu->linkIn(getLocale())) }}">
				{{ $menu->titleIn(getLocale()) }}
			</a>
		</li>
	@endforeach
</ul>