<div class="cart sm-show">
    <a href="#" class="icon-menu" id="open-menu"></a>
</div>

<a href="#" class="icon-cancel sm-show" id="close-menu"></a>
@include(CurrentModule::bladePath('layouts.header-cart'))
<div class="cart sm-hide">
    <a href="#" class="icon-search" id="open-search"></a>
</div>
