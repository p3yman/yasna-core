<div class="copyright">
	{{ CurrentModule::trans('general.footer.copyright', ['site' => setting()->ask('site_title')->gain()]) }}
	<br/>
	<a class="text-gray" href="{{ Template::yasnaGroupUrl() }}" target="_blank">
		{{ CurrentModule::trans('general.footer.powered_by_someone', [
			'someone' => Template::yasnaGroupTitle()
		]) }}
	</a>
</div>