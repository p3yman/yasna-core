@php
    $logoHashid = setting('site_logo')->in(getLocale())->gain();
    $logoUrl = $logoHashid ? doc($logoHashid)->getUrl() : null;
@endphp

@if($logoUrl)
    <a href="{{ url_locale('') }}" id="logo">
        <img src="{{ $logoUrl }}">
    </a>
@endif