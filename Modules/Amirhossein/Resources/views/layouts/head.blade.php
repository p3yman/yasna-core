<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Html::style(CurrentModule::asset('libs/jquery-ui/jquery-ui.min.css')) !!}
    @if(getLocale() == 'en')
        {!! Html::style(CurrentModule::asset('css/front-style-en.min.css')) !!}
    @else
        {!! Html::style(CurrentModule::asset('css/front-style.min.css')) !!}
    @endif
    {!! Html::style(CurrentModule::asset('css/front-additives.min.css')) !!}
    @if(getLocale() != 'en')
        {!! Html::style(CurrentModule::asset('css/front-additives-fa.min.css')) !!}
    @endif
    {!! Html::style(CurrentModule::asset('libs/font-awesome/css/font-awesome.min.css')) !!}
    <title>{{ Template::implodePageTitle(' | ') }}</title>
    @yield('html-header')
</head>