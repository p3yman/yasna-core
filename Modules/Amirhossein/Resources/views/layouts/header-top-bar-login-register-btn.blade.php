@if(!user()->exists)
    <a href="{{ CurrentModule::actionLocale('Auth\LoginController@showLoginForm') }}" class="auth-link">
        {{ CurrentModule::trans('general.login-or-register') }}
    </a>
@endif