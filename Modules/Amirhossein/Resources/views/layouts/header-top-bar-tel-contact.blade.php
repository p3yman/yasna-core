@php $telephone = Template::contactInfo()['telephone'] @endphp

<div class="f-r">
    <div class="contact">
        @if($telephone and is_array($telephone) and count($telephone))
            <a href="{{ CurrentModule::actionLocale('FrontController@contact') }}">
                <i class="icon-phone"></i> {{ ad($telephone[0]) }}
            </a>
        @endif
    </div>
</div>
