@if($post->canReceiveComments())
    {!! Form::open([
        'url' => CurrentModule::actionLocale('CommentController@save'),
        'method'=> 'post',
        'class' => 'js',
        'name' => 'commentForm',
        'id' => 'commentForm',
        'style' => 'padding: 15px;',
    ]) !!}
    <div class="row">
        @include(CurrentModule::bladePath('layouts.widgets.form.hidden'),[
            'name' => 'post_id',
            'value' => $post->hashid,
        ])

        @include(CurrentModule::bladePath('layouts.widgets.form.textarea'), [
            'name' => 'text',
            'label' => false,
            'rows' => 4,
            'placeholder' => trans('validation.attributes_placeholder.your_comment'),
        ])

        <div class="col-xs-12">
            <div class="form-group pt15">
                <div class="action tal">
                    <button class="blue">{{ CurrentModule::trans('general.comment.submit') }}</button>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pt15">
            @include(CurrentModule::bladePath('layouts.widgets.form.feed'))
        </div>
    </div>
    {!! Form::close() !!}
    @include(CurrentModule::bladePath('layouts.widgets.form.scripts'))
@endif