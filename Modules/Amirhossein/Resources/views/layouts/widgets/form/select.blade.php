<div class="field">
    <label> {{ trans('validation.attributes.' . $name) }} </label>
    @include(CurrentModule::bladePath('layouts.widgets.form.select-self'))
</div>