@php
    if(!isset($value)) {
        $value = null;
    }
@endphp

<div class="field">
    <label> {{ trans('validation.attributes.' . $name) }} </label>
    <select name="{{ $name or '' }}" id="{{ $name or '' }}"
            class="{{ $class or '' }} selectpicker"
            title="{{ trans('validation.attributes_example.' . $name)}}"
            error-value="{{ trans('validation.javascript_validation.' . $name) }}"
            {{ $other or '' }}
    >
        @if(isset($options))
            @foreach($options as $option)
                <option value="{{ $option['id'] }}" @if($value == $option['id']) selected @endif>
                    {{ $option['title'] }}
                </option>
            @endforeach
        @endif
    </select>
</div>