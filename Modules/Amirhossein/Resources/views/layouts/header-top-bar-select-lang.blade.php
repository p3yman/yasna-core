<div class="dropdown bottom-left langs">
    @php
        $currentLocale = getLocale();
        $availableLocales = availableLocales();
    @endphp
    <a href="{{ url($currentLocale) }}" class="dropdown-toggle">
            <span>
                <img src="{{ CurrentModule::asset("images/flags/$currentLocale.png") }}">
            </span> {{ trans("yasna::lang.$currentLocale") }}
    </a>

    <div class="menu">
        @foreach($availableLocales as $availableLocale)
            <a href="{{ url($availableLocale) }}">
            <span>
                <img src="{{ CurrentModule::asset("images/flags/$availableLocale.png") }}">
            </span> {{ trans("yasna::lang.$availableLocale", [], $availableLocale) }}
            </a>
        @endforeach
    </div>
</div>