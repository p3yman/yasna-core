@extends(CurrentModule::bladePath('layouts.plane'))

@php
    Template::appendToPageTitle(CurrentModule::trans('general.contact-us'));
    Template::appendToNavBar([
        CurrentModule::trans('general.contact-us'), request()->url()
    ]);
@endphp

@section('body')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-center">
                    @include(CurrentModule::bladePath('contact.contact-info'))
                    @include(CurrentModule::bladePath('contact.form'))
                </div>
            </div>
        </div>
    </div>

@append
