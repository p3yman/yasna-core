@php $map_is_available = ($location and is_array($location) and (count($location) == 2)) @endphp

@if($map_is_available)

	@php $title = setting('site_title')->gain() ?: '' @endphp

	@php
		$lat = $location[0];
		$lng = $location[1];
	@endphp

	<div class="clearfix mt5">
		<div class="col-xs-12 map-container" data-lat="{{ $lat }}" data-lng="{{ $lng }}"
			 data-title="{{ $title or '' }}"
			 style="height: 400px; margin-bottom: 15px"></div>
	</div>


@endif



@section('html-header')
	@if ($map_is_available)
		{!!
			Html::style('https://unpkg.com/leaflet@1.3.3/dist/leaflet.css', [
				'integrity' => 'sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==',
				'crossorigin' => ''
			])
		!!}
	@endif
@append

@section('html-footer')
	@if ($map_is_available)
		{!!
			Html::script('https://unpkg.com/leaflet@1.3.3/dist/leaflet.js', [
				'integrity' => 'sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q==',
				'crossorigin' => ''
			])
		!!}

		<script>
            window.mapbox = {
                token: "{{ $__module->getConfig('map-box.token') }}",
            };
		</script>
	@endif
@append

