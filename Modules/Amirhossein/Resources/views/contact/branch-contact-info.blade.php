<div class="col-xs-6">
	@if($manager)
		<p>
			<b>{{ CurrentModule::trans('general.manager') }}:</b> {{ $manager }}
		</p>
	@endif

	@if($address and is_string($address))
		<p>
			<b>{{ CurrentModule::trans('general.contact.address') }}:</b> {{ $address }}
		</p>
	@endif

	@if($telephone and is_array($telephone) and count($telephone))
		<p>
			<b>{{ CurrentModule::trans('general.contact.telephone') }}:</b>
			@foreach($telephone as $phone)
				@if ($loop->index)
					-
				@endif
				<a href="tel:{{ $phone }}">{{ ad($phone) }}</a>
			@endforeach
		</p>
	@endif

	@if($fax and is_array($fax) and count($fax))
		<p>
			<b>{{ CurrentModule::trans('general.contact.fax') }}:</b>
			@foreach($fax as $phone)
				@if ($loop->index)
					-
				@endif
				<a href="tel:{{ $phone }}">{{ ad($phone) }}</a>
			@endforeach
		</p>
	@endif

	@if($email and is_array($email) and count($email))
		<p>
			<b>{{ CurrentModule::trans('general.contact.email') }}:</b>
			@foreach($email as $mail)
				@if ($loop->index)
					-
				@endif
				<a href="mailto:{{ $mail }}">{{ ad($mail) }}</a>
			@endforeach
		</p>
	@endif
</div>
