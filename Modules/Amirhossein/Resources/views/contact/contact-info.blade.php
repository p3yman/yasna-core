<div class="row mb20">
	<div class="col-sm-12 mb10">
		<h1>{{ $__module->getTrans('general.branch-1') }}</h1>
	</div>

	<div class="col-sm-6">
		<div class="row">
			@include(CurrentModule::bladePath('contact.map'))
		</div>
	</div>

	@include(
		$__module->getBladePath('contact.branch-contact-info'), [
			'address' => $address,
			'fax' => $fax,
			'email' => $email,
			'location' => $location,
			'manager' => $manager,
			'telephone' => Template::contactInfo()['telephone'],
		]
	)
</div>

<div class="row">
	<div class="col-sm-12 mb10">
		<h1>{{ $__module->getTrans('general.branch-2') }}</h1>
	</div>

	@include(
		$__module->getBladePath('contact.branch-contact-info'), [
			'address' => $address_2,
			'fax' => $fax_2,
			'email' => $email_2,
			'location' => $location_2,
			'manager' => $manager_2,
			'telephone' => $telephone_2,
		]
	)

	<div class="col-sm-6">
		<div class="row">
			@include(CurrentModule::bladePath('contact.map'), ['location' => $location_2])
		</div>
	</div>
</div>
