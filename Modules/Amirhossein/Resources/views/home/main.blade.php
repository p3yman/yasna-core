@extends(CurrentModule::bladePath('layouts.plane'))

@php $hideNavbar = false @endphp

@section('body')
    @include(CurrentModule::bladePath('home.slider'))
    @include(CurrentModule::bladePath('home.mouse-wrapper'))
    @include(CurrentModule::bladePath('home.about'))
    @include(CurrentModule::bladePath('home.products-categories'))
    @include(CurrentModule::bladePath('home.drawing'))
    @include(CurrentModule::bladePath('home.comments'))
@append