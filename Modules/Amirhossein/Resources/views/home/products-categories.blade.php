@if(sizeof($product_folders))
    <div id="categories">
        <div class="container">
            <div class="part-title">
                <h3>
                    <span>{{ CurrentModule::trans('general.categories-of') }}</span>
                    {{ CurrentModule::trans('post.product.plural') }}
                </h3>
            </div>
            <div class="cats">
                <div class="row">
                    @foreach($product_folders as $cat)
                        @include(CurrentModule::bladePath('posts.categories.products.item'))
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif