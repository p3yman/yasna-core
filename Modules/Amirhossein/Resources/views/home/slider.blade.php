@if($slideshow and $slideshow->count())
    <div id="home-slides">
        <ul class="home-slides">
            @foreach($slideshow as $slide)
                @php $img = doc($slide->featured_image)->getUrl() @endphp
                @if($img)
                    @php $slide->spreadMeta() @endphp
                    @php $style = ($color = $slide->text_color) ? "style='color: $color'" : '' @endphp
                    <li class="home-slide" style="background-image: url({{ $img }});">
                        <a @if($slide->href) href="{{ $slide->href }}"
                           @endif style="display: block; height: 100%; width: 100%;" target="_blank">
                            <div class="container">
                                <div class="content">
                                    @if(strlen($slide->title2))
                                        <h1 {!! $style !!}> {{ $slide->title2 }} </h1>
                                    @endif
                                    @if(strlen($slide->abstract))
                                        <p {!! $style !!}> {{ $slide->abstract }} </p>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
@endif