<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/29/18
 * Time: 11:59 PM
 */

return [
     'token' => env(
          'MAPBOX_TOKEN',
          'pk.eyJ1IjoieWFzbmF0ZWFtIiwiYSI6ImNqazlib2gzcjJ1NGgzcW1sYXF5aDZoM3kifQ.BLoLXT33-GG6NwVp7SaNQQ'
     ),
];
