<?php

return [
    'name' => 'Amirhossein',
    'slug' => 'amirhossein',

    'username-field' => 'code_melli',

    'post-url-prefix' => 'P',

    'edu-levels-values' => [
        'not-specified'     => '0',
        'less-than-diploma' => '1',
        'diploma'           => '2',
        'associate-degree'  => '3',
        'bachelor-degree'   => '4',
        'master-degree'     => '5',
        'phd-or-more'       => '6',
    ],

    'genders-values' => [
        'male'   => 1,
        'female' => 2,
        'other'  => 3,
    ],

    'marital-statuses-values' => [
        'single'  => '1',
        'married' => '2',
    ],

    "draw" => [
        "query-limit" => 300,
    ],

    "default-province" => "تهران",
    "default-city"     => "تهران",

    'map-box' => include(__DIR__ . DIRECTORY_SEPARATOR . 'map-box.php'),
];
