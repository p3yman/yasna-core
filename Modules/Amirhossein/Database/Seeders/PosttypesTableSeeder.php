<?php

namespace Modules\Amirhossein\Database\Seeders;

use App\Models\Posttype;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Providers\YasnaServiceProvider;

class PosttypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('posttypes', [
             [
                  'slug'           => 'products',
                  'title'          => 'محصولات',
                  'order'          => '5',
                  'singular_title' => 'محصول',
                  'template'       => 'product',
                  'icon'           => 'shopping-basket',
             ],
        ]);
    }
}
