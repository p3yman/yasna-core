<?php

namespace Modules\Amirhossein\Providers;

use Illuminate\Support\ServiceProvider;
use Nwidart\Modules\Facades\Module;

class TemplateServiceProvider extends ServiceProvider
{
    private static $methodPrefixes = [
        'appending' => 'appendTo',
        'merging'   => 'mergeWith',
        'imploding' => 'implode',
        'assigning' => 'set',
    ];
    private static $__calledMethod = null;

    protected static $pageTitle = [];
    protected static $navBar = [];
    protected static $redTitle = '';
    protected static $siteLogoUrl = null;
    protected static $viewVariables = [];
    protected static $moduleName = 'amirhossein';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function __callStatic($name, $arguments)
    {
        self::$__calledMethod = $name;

        if (starts_with($name, self::$methodPrefixes['appending'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['appending']));
            return self::appendToProperty($property, $arguments);
        } elseif (starts_with($name, self::$methodPrefixes['merging'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['merging']));
            return self::mergeWithProperty($property, ...$arguments);
        } elseif (starts_with($name, self::$methodPrefixes['imploding'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['imploding']));
            return self::implodeProperty($property, $arguments);
        } elseif (starts_with($name, self::$methodPrefixes['assigning'])) {
            $property = lcfirst(str_after($name, self::$methodPrefixes['assigning']));
            return self::setProperty($property, $arguments);
        } else {
            if (property_exists(__CLASS__, $name)) {
                return self::$$name;
            }
        }

        return self::undefinedMethodException();
    }

    protected static function appendToProperty($property, $appending)
    {
        if (property_exists(__CLASS__, $property)) {
            if (is_array(self::$$property)) {
                self::$$property = array_merge(self::$$property, $appending);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function mergeWithProperty($property, $merging)
    {
        if (property_exists(__CLASS__, $property)) {
            if (is_array(self::$$property)) {
                self::$$property = array_merge(self::$$property, $merging);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function setProperty($property, $parameters)
    {
        if (property_exists(__CLASS__, $property)) {
            if (count($parameters)) {
                self::$$property = $parameters[0];
            } else {
                self::tooFewArgumentsException(1);
            }

            return true;
        }

        self::undefinedMethodException();
    }

    protected static function implodeProperty($property, $parameters)
    {
        if (property_exists(__CLASS__, $property) and is_array($propertyVal = self::$$property)) {
            if (count($parameters)) {
                return implode($parameters[0], $propertyVal);
            } else {
                self::tooFewArgumentsException(1);
            }
        }

        self::undefinedMethodException();
    }

    private static function undefinedMethodException()
    {
        trigger_error(
            'Call to undefined method ' . __CLASS__ . '::' . self::$__calledMethod . '()', E_USER_ERROR
        );
    }

    private static function tooFewArgumentsException($passedArgumentsCount)
    {
        trigger_error(
            'Too few arguments to function '
            . __CLASS__ . '::' .
            self::$__calledMethod
            . ', '
            . $passedArgumentsCount
            . ' passed', E_USER_ERROR
        );
    }

    public static function siteLogoUrl()
    {
        if (is_null(self::$siteLogoUrl)) {
            $settingLogo = setting('site_logo')->gain();
            if ($settingLogo and ($logoFile = doc($settingLogo)->getUrl())) {
                self::$siteLogoUrl = $logoFile;
            } else {
                self::$siteLogoUrl = Module::asset(self::$moduleName . ':images/logo.png');
            }
        }

        return self::$siteLogoUrl;
    }
}
