<?php

namespace Modules\Amirhossein\Providers;

use App\Models\Category;
use Illuminate\Support\ServiceProvider;

class PostsCollectionServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function postsCategories($posts, $slug = 'id', array $neededAttributes = ['title'])
    {
        $cats = Category::whereHas('posts', function ($query) use ($posts) {
            $query->whereIn('posts.id', $posts->pluck('id')->toArray());
        })->get()->mapWithKeys(function ($category) use ($slug, $neededAttributes) {
            $value = [];
            foreach ($neededAttributes as $attribute) {
                $value[$attribute] = $category->$attribute;
            }
            return [$category->slug => $value];
        })->toArray();

        ksort($cats);

        return $cats;
    }
}
