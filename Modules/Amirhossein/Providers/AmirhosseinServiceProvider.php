<?php

namespace Modules\Amirhossein\Providers;

use Carbon\Carbon;
use Modules\Amirhossein\Events\Registered;
use Modules\Amirhossein\Http\Middleware\CartNonemptyMiddleware;
use Modules\Amirhossein\Http\Middleware\DebugMiddleware;
use Modules\Amirhossein\Http\Middleware\OwnCartMiddleware;
use Modules\Amirhossein\Listeners\AssignRolesOnUserRegistered;
use Modules\Amirhossein\Listeners\SendRegistrationConfirmationsOnUserRegistered;
use Modules\Amirhossein\Listeners\ManageUserCartOnUserLoggedIn;
use Modules\Amirhossein\Schedules\NotifyForPendingCartsSchedule;
use Modules\Amirhossein\Schedules\NotifyForUnpaidCartsSchedule;
use Modules\Amirhossein\Schedules\RemoveNoOwnerCartsSchedule;
use Modules\Amirhossein\Schedules\RemoveOldAndPendingCartsSchedule;
use Modules\Amirhossein\Services\AsanakSms\AsanakSmsProvider;
use Modules\Amirhossein\Services\Email\EmailServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Amirhossein\Services\Module\ModuleHandler;
use Modules\Amirhossein\Services\Template\AmirhosseinTemplateHandler;
use Modules\Yasna\Events\UserLoggedIn;
use Modules\Yasna\Providers\ModuleEnrichmentTrait;
use Modules\Amirhossein\Schedules\RunQueueWorkSchedule;
use Modules\Yasna\Services\YasnaProvider;

class AmirhosseinServiceProvider extends YasnaProvider
{
    use ModuleEnrichmentTrait;



    /**
     * Do things after default boot of this service provider.
     */
    public function index()
    {
        $this->addMiddlewares();
        $this->addProviders();
        $this->addAliases();
        $this->registerModelTraits();
        $this->registerPostHandlers();
        $this->registerEventListeners();
        $this->registerSchedules();
        $this->redefineUsernameField();
    }



    /**
     * Register middlewares that are defined and used in this module.
     */
    protected function addMiddlewares()
    {
        $middlewares = [
             'cartNonempty' => CartNonemptyMiddleware::class,
             'debug'        => DebugMiddleware::class,
             'ownCart'      => OwnCartMiddleware::class,
        ];
        foreach ($middlewares as $alias => $middleware) {
            $this->addMiddleware($alias, $middleware);
        }
    }



    /**
     * Register provider that are defined and used in this module.
     */
    protected function addProviders()
    {
        $providers = [
             PostServiceProvider::class,
             DrawingCodeServiceProvider::class,
             RouteServiceProvider::class,
             AsanakSmsProvider::class,
             EmailServiceProvider::class,
             ShopServiceProvider::class,
             PostsCollectionServiceProvider::class,
             CartServiceProvider::class,
             CurrencyServiceProvider::class,
             CommentServiceProvider::class,
             FormServiceProvider::class,
        ];
        foreach ($providers as $provider) {
            $this->addProvider($provider);
        }
    }



    /**
     * Register aliases that are used in this module.
     */
    protected function addAliases()
    {
        $aliases = [
             'Template'             => AmirhosseinTemplateHandler::class,
             'RouteTools'           => RouteServiceProvider::class,
             'Carbon'               => Carbon::class,
             'ShopTools'            => ShopServiceProvider::class,
             'PostsCollectionTools' => PostsCollectionServiceProvider::class,
             'PosttypeTools'        => PosttypeServiceProvider::class,
             'CurrencyTools'        => CurrencyServiceProvider::class,
             'CommentTools'         => CommentServiceProvider::class,
             'CurrentModule'        => CurrentModuleHelper::class,
             'FormTools'            => FormServiceProvider::class,
        ];
        foreach ($aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }
    }



    /**
     * Register model traits that are defined and used in this module.
     */
    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":AmirhosseinUserTrait")->to("User")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":AmirhosseinPostTrait")->to("Post")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":AmirhosseinPosttypeTrait")->to("Posttype")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":AmirhosseinUnitTrait")->to("Unit")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":AmirhosseinTransactionTrait")->to("Transaction")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait($this->moduleAlias() . ":AmirhosseinCartTrait")->to("Cart")
        ;
    }



    /**
     * Add some items to <i>"browse_headings_handlers"</i> service of the <b>Post</b> module.
     */
    protected function registerPostHandlers()
    {
        module('posts')
             ->service('browse_headings_handlers')
             ->add($this->moduleAlias())
             ->method($this->moduleAlias() . ":handleBrowseHeadings")
        ;

        module('posts')
             ->service('model_selector')
             ->add('folder')
             ->default('')
             ->method('selector_folder')
             ->order(32)
        ;
    }



    /**
     * Register event listeners.
     */
    protected function registerEventListeners()
    {
        $this->listen(UserLoggedIn::class, ManageUserCartOnUserLoggedIn::class);

        $this->listen(Registered::class, SendRegistrationConfirmationsOnUserRegistered::class);
        $this->listen(Registered::class, AssignRolesOnUserRegistered::class);
    }



    /**
     * Register schedules.
     */
    protected function registerSchedules()
    {
        $schedules = [
             RemoveNoOwnerCartsSchedule::class,
             RemoveOldAndPendingCartsSchedule::class,
             NotifyForPendingCartsSchedule::class,
             NotifyForUnpaidCartsSchedule::class,
             RunQueueWorkSchedule::class,
        ];
        foreach ($schedules as $schedule) {
            $this->addSchedule($schedule);
        }
    }



    /**
     * @param array $arguments
     */
    public static function handleBrowseHeadings($arguments)
    {
        $posttype = $arguments['posttype'];

        // Js Files
        module("manage")
             ->service("template_assets")
             ->add("drawJs")
             ->link(static::getModuleAlias() . ":js/draw.min.js")
             ->order(60)
        ;
        // Columns
        module('posts')
             ->service('browse_headings')
             ->add(static::getModuleAlias())
             ->trans(static::getModuleAlias() . "::general.drawing.draw.singular")
             ->blade(static::getModuleAlias() . "::manage.post.browser.draw-column")
             ->order(31)
             ->condition($posttype->has('event'))
        ;
    }



    /**
     * Alias of the module which this file belongs to.
     *
     * @return string
     * @deprecated at 2018-07-30
     */
    public static function getModuleAlias()
    {
        return ModuleHandler::create(self::class)->getAlias();
    }



    /**
     * Redefines Username Field
     */
    private function redefineUsernameField()
    {
        config([
             "auth.providers.users.field_name" => "code_melli",
        ]);
    }
}
