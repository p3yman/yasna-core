<?php

namespace Modules\Amirhossein\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Amirhossein\Services\Module\ModuleHandler;

class FormServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Decide about the proper `error-value` of a field.
     *
     * @param string      $name
     * @param string|null $given_value
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function findErrorValue($name, $given_value = null)
    {
        if (!is_null($given_value)) {
            return $given_value;
        }

        $trans_path = 'validation.javascript_validation.' . $name;
        if (trans()->has($trans_path)) {
            return trans($trans_path);
        }


        $trans_path = static::module()->transPath('validation.javascript-validation.' . $name);
        if (trans()->has($trans_path)) {
            return trans($trans_path);
        }

        return null;
    }



    /**
     * Decide about the proper `attribute-example` of a field.
     *
     * @param string      $name
     * @param string|null $given_value
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function findAttributeExample($name, $given_value = null)
    {
        if (!is_null($given_value)) {
            return $given_value;
        }

        $trans_path = 'validation.attributes_example.' . $name;
        if (trans()->has($trans_path)) {
            return trans($trans_path);
        }


        $trans_path = static::module()->transPath('validation.attributes_example.' . $name);
        if (trans()->has($trans_path)) {
            return trans($trans_path);
        }

        return null;
    }



    /**
     * Decide about the proper `placeholder` of a field.
     *
     * @param string      $name
     * @param string|null $given_value
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function findPlaceholder($name, $given_value = null)
    {
        if (!is_null($given_value)) {
            return $given_value;
        }

        $trans_path = 'validation.attributes_placeholder.' . $name;
        if (trans()->has($trans_path)) {
            return trans($trans_path);
        }


        $trans_path = static::module()->transPath('validation.attributes_placeholder.' . $name);
        if (trans()->has($trans_path)) {
            return trans($trans_path);
        }

        return null;
    }



    /**
     * Return the module of this file
     *
     * @return ModuleHandler
     */
    protected static function module()
    {
        return ModuleHandler::create(static::class);
    }
}
