<?php

namespace Modules\Amirhossein\Providers;

use App\Models\Post;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @param Collection $posts
     *
     * @return float|null
     */
    public static function postsMaxPrice($posts)
    {
        return Ware::whereIn('sisterhood', $posts->pluck('sisterhood')->toArray())
            ->max('sale_price');
    }

    /**
     * @param Collection $posts
     *
     * @return float|null
     */
    public static function postsMinPrice($posts)
    {
        return Ware::whereIn('sisterhood', $posts->pluck('sisterhood')->toArray())
            ->min('sale_price');
    }

    /**
     * Returns url to specific ware of a $post
     *
     * @param \App\Models\Post      $post
     * @param \App\Models\Ware|null $ware
     *
     * @return string
     */
    public static function productWareUrl(Post $post, Ware $ware = null)
    {
        $routeParameters = ['identifier' => RouteServiceProvider::$postDirectUrlPrefix . $post->hashid];
        $title           = urlHyphen($post->title);

        if (
            $ware and // Ware specified
            $ware->exists and // Ware exists
            $post->wares()->where(['id' => $ware->id])->count() // Ware belongs to specified product
        ) {
            $routeParameters['wareOrTitle'] = $ware->hashid;
            $routeParameters['title']       = $title;
        } else {
            $routeParameters['wareOrTitle'] = $title;
        }

        return CurrentModuleHelper::actionLocale('PostController@productSingle', $routeParameters);
    }

    public static function getAllPackages(bool $active = false)
    {
        $packages = Ware::whereNull('sisterhood');

        if ($active) {
            $usedPackages = Ware::select('package_id')
                ->whereNotNull('sisterhood')
                ->where('package_id', '>', 0)
                ->groupBy('package_id')
                ->get()
                ->pluck('package_id')
                ->toArray();

            $packages->whereIn('id', $usedPackages);
        }

        return $packages->get();
    }

    public static function showPrice($amount, $showCurrency = true)
    {
        return trim(ad(number_format(self::priceToShow($amount)))
            . ' '
            . (
            $showCurrency
                ? CurrentModuleHelper::trans('general.unit.currency.' . CurrencyServiceProvider::getPreviewCurrency())
                : ''
            ));
    }

    public static function priceToShow($amount)
    {
        return round($amount * CurrencyServiceProvider::getPreviewCurrencyRatio());
    }
}
