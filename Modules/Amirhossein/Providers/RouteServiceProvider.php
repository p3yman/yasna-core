<?php

namespace Modules\Amirhossein\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var array $staticPosttypes The List of Posttypes that has static routes (without "archive" prefix)
     */
    protected static $staticPosttypes      = [
        'teammates',
        'faqs',
        'products',
    ];
    protected static $controllersNamespace = '\Modules\Amirhossein\Http\Controllers\\';

    public static $postDirectUrlPrefix = 'P';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getPosttypeRoute($posttypeSlug)
    {
        if (self::isStaticPosttype($posttypeSlug)) {
            return CurrentModuleHelper::actionLocale("PosttypeController@$posttypeSlug");
        }

        return CurrentModuleHelper::actionLocale('PosttypeController@showList', ['posttype' => $posttypeSlug]);
    }

    public static function getPosttypeCategoriesRoute($posttypeSlug)
    {
        if (self::isStaticPosttype($posttypeSlug)) {
            return CurrentModuleHelper::actionLocale("PosttypeController@{$posttypeSlug}Categories");
        }

        return CurrentModuleHelper::actionLocale('PosttypeController@showCategories', ['posttype' => $posttypeSlug]);
    }

    public static function getStaticPosttypes()
    {
        return self::$staticPosttypes;
    }

    public static function isStaticPosttype($posttypeSlug)
    {
        if (in_array($posttypeSlug, $staticPosttypes = self::getStaticPosttypes())) {
            return true;
        } else {
            return false;
        }
    }

    public static function action(string $action, ...$otherParameters)
    {
        return action(self::$controllersNamespace . $action, ...$otherParameters);
    }

    public static function actionLocale(string $action, ...$otherParameters)
    {
        return action_locale(self::$controllersNamespace . $action, ...$otherParameters);
    }
}
