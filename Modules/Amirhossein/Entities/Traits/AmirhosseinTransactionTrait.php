<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 01/01/2018
 * Time: 12:37 PM
 */

namespace Modules\Amirhossein\Entities\Traits;

trait AmirhosseinTransactionTrait
{
    public function cart()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Cart', 'invoice_id');
    }
}
