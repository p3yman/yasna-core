<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 01/01/2018
 * Time: 12:53 PM
 */

namespace Modules\Amirhossein\Entities\Traits;

use Illuminate\Database\Eloquent\Collection;
use Modules\Payment\Services\PaymentHandler\Search\SearchBuilder;

trait AmirhosseinCartTrait
{
    /**
     * @inheritdoc
     */
    protected function shouldInteractWithModelName()
    {
        return false;
    }
}
