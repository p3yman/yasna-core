<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 12/11/2017
 * Time: 01:45 PM
 */

namespace Modules\Amirhossein\Entities\Traits;

use Modules\Amirhossein\Providers\RouteServiceProvider;

trait AmirhosseinPosttypeTrait
{
    public function getListUrlAttribute()
    {
        return RouteServiceProvider::getPosttypeRoute($this->slug);
    }
}
