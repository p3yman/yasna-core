<?php

namespace Modules\Amirhossein\Entities\Traits;

use App\Models\Post;
use App\Models\Receipt;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Nwidart\Modules\Facades\Module;

trait AmirhosseinUserTrait
{
    protected static $required_fields = [
        'name_first',
        'name_last',
        'code_melli',
        'mobile',
        'tel',
        'birth_date',
        'gender',
        'marital'
    ];

    public static function getRequiredFields()
    {
        return self::$required_fields;
    }

    /**
     * @param Post $event
     */
    public function eventScores($event)
    {
        if (!$event->exists or !$event->posttype->hasFeature('event')) {
            return false;
        }

        $event->spreadMeta();


        return floor(Receipt::where(['user_id' => $this->id])
            ->whereDate('purchased_at', '>=', $event->event_starts_at)
            ->whereDate('purchased_at', '<=', $event->event_ends_at)
            ->select(\DB::raw("SUM(purchased_amount)/'$event->rate_point' points"))
            ->pluck('points')
            ->first());
    }


    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */

    public function receipts()
    {
        return $this->hasMany('App\Models\Receipt');
    }


    /*
    |--------------------------------------------------------------------------
    | Helpers
    |--------------------------------------------------------------------------
    |
    */

    public function totalReceiptsAmountInEvent($post)
    {
        return $post->receipts->where('user_id', $this->id)->sum('purchased_amount');
    }

    /*
    |--------------------------------------------------------------------------
    | Accessors
    |--------------------------------------------------------------------------
    |
    */

    public function gravatar(string $default = null, $size = 100)
    {
        $default = $default ?: CurrentModuleHelper::asset('images/user-3.png');
        return $this->email
            ? "https://www.gravatar.com/avatar/"
            . md5(strtolower(trim($this->email)))
            . "?d="
            . urlencode(trim($default, '/'))
            . "&s="
            . $size
            : $default;
    }
}
