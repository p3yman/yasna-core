<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:07 PM
 */

namespace Modules\Amirhossein\Services\ProductFilter\Traits;

use App\Models\Category;

trait CheckboxFilterTrait
{
    protected $categories = [];
    protected $packages   = [];

    protected $categoryFolder;

    public function checkboxFilter(array $filterConditions)
    {
        foreach ($filterConditions as $conditionType => $condition) {
            $this->callMethod($conditionType, [$condition]);
        }

        return $this;
    }

    public function category($categories)
    {
        if (!is_array($categories)) {
            $categories = [$categories];
        }

        foreach ($categories as $category) {
            $this->setCategory($category);
        }

        return $this;
    }

    protected function setCategory($category)
    {
        $categoryId = $this->hashid2id($category);
        if ($categoryId) {
            $this->categories = array_unique(array_merge($this->categories, [$categoryId]));
        }

        return $this;
    }

    public function package($packages)
    {
        if (!is_array($packages)) {
            $packages = [$packages];
        }

        foreach ($packages as $package) {
            $this->setPackage($package);
        }

        return $this;
    }

    protected function setPackage($package)
    {
        $packageId = $this->hashid2id($package);
        if ($packageId) {
            $this->packages = array_unique(array_merge($this->packages, [$packageId]));
        }

        return $this;
    }

    protected function hashid2id($hashid)
    {
        $parts = hashid_decrypt($hashid, 'ids');

        if (count($parts)) {
            return $parts[0];
        }

        return null;
    }

    protected function applyCheckboxes()
    {
        return $this->applyCategories()
            ->applyPackages();
    }

    protected function applyCategories()
    {
        $categories = $this->categories;
        if ($this->categoryFolder) {
            $folderCategories = $this->categoryFolder->children->pluck('id')->toArray();
            if (count($categories)) {
                $categories = array_intersect($folderCategories, $categories);
            } else {
                $categories = $folderCategories;
            }
        }
        if (count($categories)) {
            $builder = $this->getBuilder();
            $builder->join('category_post', 'posts.id', '=', 'category_post.post_id')
                ->whereIn('category_post.category_id', $categories);
            $this->setBuilder($builder);
        }
        return $this;
    }

    protected function applyPackages()
    {
        if (($packages = $this->packages) and count($packages)) {
            $builder = $this->getBuilder();
            $builder->whereIn('wares.package_id', $packages);
            $this->setBuilder($builder);
        }

        return $this;
    }

    protected function packageSpecified()
    {
        return boolval(count($this->packages));
    }

    public function folder(Category $folder)
    {
        if ($folder->exists and $folder->is_folder) {
            $this->categoryFolder = $folder;
        }

        return $this;
    }

    public function categoryFolder(...$parameters)
    {
        return $this->folder(...$parameters);
    }

    public function categoriesFolder(...$parameters)
    {
        return $this->folder(...$parameters);
    }
}
