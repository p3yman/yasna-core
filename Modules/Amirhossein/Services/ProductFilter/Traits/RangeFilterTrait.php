<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:08 PM
 */

namespace Modules\Amirhossein\Services\ProductFilter\Traits;

trait RangeFilterTrait
{
    protected $ranges = [];

    protected static $rangeColumnsMap = [
        'price' => 'wares.sale_price',
    ];

    protected static $rangeRatio = [
        'price' => 10,
    ];

    public function rangeFilter(array $filterConditions)
    {
        foreach ($filterConditions as $conditionType => $condition) {
            if (array_key_exists('min', $condition) and array_key_exists('max', $condition)) {
                $this->addRangeFilter($conditionType, ...[$condition['min'], $condition['max']]);
            } else {
                $this->badRequest();
            }
        }

        return $this;
    }

    public function addRangeFilter($title, $min, $max)
    {
        if (array_key_exists($title, $ratios = self::$rangeRatio)) {
            $min *= $ratios[$title];
            $max *= $ratios[$title];
        }

        $map = self::getRangeColumnsMap();
        if (array_key_exists($title, $map)) {
            $this->ranges[$map[$title]] = compact('min', 'max');
        }

        return $this;
    }

    protected function hasRange($type)
    {
        if (
            array_key_exists($type, $this->ranges) or
            (
                array_key_exists($type, $map = self::getRangeColumnsMap()) and
                array_key_exists($map[$type], $this->ranges)
            )
        ) {
            return true;
        } else {
            return false;
        }
    }


    protected function applyRanges()
    {
        if (($ranges = $this->ranges) and count($ranges)) {
            $builder = $this->getBuilder();
            foreach ($ranges as $column => $range) {
                if (starts_with($column, ['wares', '`wares`']) and !$this->wareImportance()) {
                    $builder->where(function ($query) use ($column, $range) {
                        $query->where(function ($query) use ($column, $range) {
                            $query->where($column, '<=', $range['max'])
                                ->where($column, '>=', $range['min']);
                        })->orWhereNull($column);
                    });
                } else {
                    $builder->where($column, '<=', $range['max'])
                        ->where($column, '>=', $range['min']);
                }
            }
        }

        return $this;
    }


    protected static function getRangeColumnsMap()
    {
        return self::$rangeColumnsMap;
    }
}
