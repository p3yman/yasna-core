<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:06 PM
 */

namespace Modules\Amirhossein\Services\ProductFilter\Traits;

trait TextFilterTrait
{
    protected $search = [];

    protected static $searchColumnsMap = [
        'title' => 'posts.title',
    ];

    public function textFilter(array $filterConditions)
    {
        foreach ($filterConditions as $conditionType => $condition) {
            $this->addTextFilter($conditionType, $condition);
        }

        return $this;
    }

    protected function addTextFilter($title, $value)
    {
        $map = self::getSearchColumnsMap();
        if (array_key_exists($title, $map)) {
            $this->search[$map[$title]] = $value;
        }

        return $this;
    }

    protected function applySearch()
    {
        if (($search = $this->search) and count($search)) {
            $builder = $this->getBuilder();
            foreach ($search as $column => $value) {
                $builder->where($column, 'like', '%' . $value . '%');
            }

            $this->setBuilder($builder);
        }

        return $this;
    }


    public static function getSearchColumnsMap()
    {
        return self::$searchColumnsMap;
    }
}
