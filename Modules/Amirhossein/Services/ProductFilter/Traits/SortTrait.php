<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/5/18
 * Time: 9:12 PM
 */

namespace Modules\Amirhossein\Services\ProductFilter\Traits;

trait SortTrait
{
    protected $orderColumn    = 'posts.created_at';
    protected $orderDirection = 'desc';

    public function sortFilter(array $filterConditions)
    {
        foreach ($filterConditions as $conditionType => $condition) {
            $this->callMethod($conditionType, [$condition]);
        }

        return $this;
    }

    public function price($sort)
    {
        $this->orderColumn    = 'wares.sale_price';
        $this->orderDirection = $sort;

        return $this;
    }

    public function publishing($sort)
    {
        $this->orderColumn    = 'published_at';
        $this->orderDirection = $sort;

        return $this;
    }

    protected function applySort()
    {
        $builder = $this->getBuilder();
        $builder->orderBy($this->orderColumn, $this->orderDirection);

        $this->setBuilder($builder);

        return $this;
    }
}
