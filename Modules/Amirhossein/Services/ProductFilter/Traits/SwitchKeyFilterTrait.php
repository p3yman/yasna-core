<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 03:42 PM
 */

namespace Modules\Amirhossein\Services\ProductFilter\Traits;

trait SwitchKeyFilterTrait
{
    protected $switches = [];

    public function switchKeyFilter(array $filterConditions)
    {
//        dd($filterConditions, __FILE__ . ' - ' . __LINE__);
        foreach ($filterConditions as $conditionType => $condition) {
            $this->addSwitch($conditionType, $condition);
        }

        return $this;
    }

    protected function addSwitch(string $switchName, bool $switchValue)
    {
        $this->switches[camel_case($switchName)] = $switchValue;

        return $this;
    }

    protected function applySwitches()
    {
        if (($switches = $this->switches) and count($switches)) {
            foreach ($switches as $switchTitle => $switch) {
                $this->callMethod($switchTitle, [$switch]);
            }
        }

        return $this;
    }

    protected function available(bool $switch)
    {
        $builder = $this->getBuilder();
        $builder->where('wares.is_available', '=', $switch);
        $this->setBuilder($builder);

        return $this;
    }

    protected function specialSale(bool $switch)
    {
        $builder = $this->getBuilder();
        $builder->whereColumn('wares.sale_price', '<', 'wares.original_price');
        $this->setBuilder($builder);

        return $this;
    }

    protected function availabilityIsChecked()
    {
        return $this->switchChecked('available');
    }

    protected function specialSaleIsChecked()
    {
        return $this->switchChecked('special-sale');
    }

    protected function switchChecked($switch)
    {
        if (array_key_exists($switch = camel_case($switch), $this->switches) and $this->switches[$switch]) {
            return true;
        } else {
            return false;
        }
    }
}
