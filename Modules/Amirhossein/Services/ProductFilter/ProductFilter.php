<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 05/12/2017
 * Time: 01:12 PM
 */

namespace Modules\Amirhossein\Services\ProductFilter;

use App\Models\Post;
use Modules\Amirhossein\Services\ProductFilter\Traits\BuilderTrait;
use Modules\Amirhossein\Services\ProductFilter\Traits\CheckboxFilterTrait;
use Modules\Amirhossein\Services\ProductFilter\Traits\OutputTrait;
use Modules\Amirhossein\Services\ProductFilter\Traits\PaginationTrait;
use Modules\Amirhossein\Services\ProductFilter\Traits\RangeFilterTrait;
use Modules\Amirhossein\Services\ProductFilter\Traits\SortTrait;
use Modules\Amirhossein\Services\ProductFilter\Traits\SwitchKeyFilterTrait;
use Modules\Amirhossein\Services\ProductFilter\Traits\TextFilterTrait;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductFilter
{
    use BuilderTrait;
    use OutputTrait;
    use TextFilterTrait;
    use CheckboxFilterTrait;
    use RangeFilterTrait;
    use SwitchKeyFilterTrait;
    use PaginationTrait;
    use SortTrait;

    public function __construct(array $filterData)
    {
        $this->assignFilterData($filterData);
    }

    public function assignFilterData(array $filterData)
    {
        foreach ($filterData as $filterType => $filterValues) {
            $this->callMethod($filterType . 'Filter', [$filterValues]);
        }

        return $this;
    }

    protected function callMethod($method, $parameters)
    {
        if (method_exists($this, $method)) {
            return $this->$method(...$parameters);
        } else {
            $this->badRequest();
        }
    }

    protected function makeQueryReady()
    {
        $this->resetBuilder();
        $this->applyCheckboxes()
            ->applySearch()
            ->applyRanges()
            ->applySwitches()
            ->applySort();
        ;
    }

    protected function wareImportance()
    {
        return $this->availabilityIsChecked() or $this->specialSaleIsChecked() or $this->packageSpecified();
    }
}
