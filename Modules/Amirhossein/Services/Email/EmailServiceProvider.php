<?php

namespace Modules\Amirhossein\Services\Email;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class EmailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'email');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    public static function send($content, $to, $name, $subject, $template)
    {
        return Mail::send(
            CurrentModuleHelper::bladePath('email.template.' . $template),
            compact('content'),
            function ($m) use ($to, $name, $subject) {
                $m->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME') ?: get_setting('site_title'));

                $m->to($to, $name)
                    ->subject($subject);
            }
        );
    }
}
