<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:14 PM
 */

namespace Modules\Amirhossein\Services\Controller;

abstract class AmirhosseinFrontControllerAbstract extends AmirhosseinControllerAbstract
{
    use AmirhosseinControllerTemplateTrait {
        template as protected traitTemplate;
    }
    use AmirhosseinFrontControllerTrait;

    protected $variables = [];



    /**
     * @param string $variableName
     * @param mixed  $variableValue
     *
     * @return $this
     */
    protected function appendToVariables(string $variableName, $variableValue)
    {
        $this->variables[$variableName] = $variableValue;

        return $this;
    }



    /**
     * Add variables of class to the sent parameters.
     *
     * @param mixed ...$parameters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function template(...$parameters)
    {
        $parameters[1] = array_merge(($parameters[1] ?? []), $this->variables);

        return $this->traitTemplate(...$parameters);
    }
}
