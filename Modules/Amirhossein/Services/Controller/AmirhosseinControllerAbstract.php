<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:13 PM
 */

namespace Modules\Amirhossein\Services\Controller;

abstract class AmirhosseinControllerAbstract extends ModuleControllerAbstract
{
    protected $posttype_slugs = [
         'slider'     => 'slideshows',
         'static'     => 'pages',
         'event'      => 'events',
         'product'    => 'products',
         'commenting' => 'commenting',
    ];
}
