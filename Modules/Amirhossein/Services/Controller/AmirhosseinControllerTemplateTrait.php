<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:29 PM
 */

namespace Modules\Amirhossein\Services\Controller;

use Modules\Amirhossein\Providers\CartServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Amirhossein\Services\Template\AmirhosseinTemplateHandler as Template;

trait AmirhosseinControllerTemplateTrait
{
    protected $sisterhood_links = [];



    /**
     * Get the evaluated view contents for the given view in this current module.
     *
     * @param  string $view
     * @param  array  $data
     * @param  array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function template(...$parameters)
    {
        $this
             ->setTemplateCart()
             ->setTemplateSiteTitle()
             ->setTemplateNavbar()
             ->setTemplateOpenGraphs()
             ->setTemplateMainMenu()
             ->setTemplateFooterMenu()
             ->setTemplateYasnaInfo()
             ->setTemplateContactInfo()
        ;
        return parent::template(...$parameters);
    }



    protected function setTemplateCart()
    {
        Template::setCart(CartServiceProvider::findActiveCart());
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateSiteTitle()
    {
        Template::setSiteTitle($settingTitle = get_setting('site_title'));
        Template::appendToPageTitle($settingTitle);
        return $this;
    }



    protected function setTemplateNavbar()
    {
        Template::appendToNavBar([
             CurrentModuleHelper::trans('general.home'),
             CurrentModuleHelper::actionLocale('FrontController@index'),
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateOpenGraphs()
    {
        Template::setOpenGraphs([
             'title'       => Template::siteTitle(),
             'url'         => request()->url(),
             'image'       => Template::siteLogoUrl(),
             'description' => function () {
                 if (count($pageTitleArray = Template::pageTitle()) > 1) {
                     return implode(' - ', array_slice($pageTitleArray, 1));
                 }
                 return null;
             },
        ]);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateMainMenu()
    {
        Template::setMainMenu(menumaker()->get('main'));
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateFooterMenu()
    {
        Template::setFooterMenu(menumaker()->get('footer'));
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateContactInfo()
    {
        Template::mergeWithContactInfo(['telephone' => get_setting('telephone')]);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setTemplateYasnaInfo()
    {
        Template::setYasnaGroupTitle(get_setting('yasna_group_title'));
        Template::setYasnaGroupUrl(get_setting('yasna_group_url'));

        return $this;
    }
}
