<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/16/18
 * Time: 3:09 PM
 */

namespace Modules\Amirhossein\Services\Controller;

use Illuminate\Pagination\Paginator;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Amirhossein\Services\Template\AmirhosseinTemplateHandler as Template;

trait AmirhosseinFrontControllerTrait
{
    use FeedbackControllerTrait {
        abort as protected traitAbort;
    }



    /**
     * AmirhosseinFrontControllerAbstract constructor.
     */
    public function __construct()
    {
        parent::__construct();
        config(['auth.providers.users.field_name' => CurrentModuleHelper::config('username-field')]);
        Paginator::defaultSimpleView(CurrentModuleHelper::bladePath('pagination.simple-default'));
        Paginator::defaultView(CurrentModuleHelper::bladePath('pagination.default'));
    }



    /**
     * @param       $errorCode
     * @param bool  $minimal if true minimal view will be loading
     * @param mixed $data
     *
     * @return \Symfony\Component\HttpFoundation\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    protected function abort(...$parameters)
    {
        return $this->setTemplateSiteTitle()
                    ->traitAbort(...$parameters)
             ;
    }
}
