<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 12:10 PM
 */

namespace Modules\Amirhossein\Services\Module;

use Modules\Amirhossein\Services\OOP\MagicMethodsTrait;
use Modules\Yasna\Services\ModuleHelper;
use Nwidart\Modules\Facades\Module;

class ModuleHandler extends ModuleHelper
{
    /**
     * @param string $namespace
     *
     * @return $this
     */
    public static function create($namespace)
    {
        $array = explode("\\", $namespace);
        return new static($array[1] ?: null);
    }



    /**
     * @return string
     */
    public function bladePath($subPath)
    {
        return $this->getAlias() . '::' . $subPath;
    }



    /**
     * @param string $subPath
     *
     * @return string
     */
    public function assetPath($subPath)
    {
        return $this->getAlias() . ':' . $subPath;
    }



    /**
     * @param string $subPath
     *
     * @return string
     */
    public function asset($subPath)
    {
        return Module::asset(static::assetPath($subPath));
    }



    /**
     * @param string $subPath
     *
     * @return string
     */
    public function transPath($subPath)
    {
        return $this->getAlias() . '::' . $subPath;
    }



    /**
     * @param string $subPath
     *
     * @return string
     */
    public function configPath($subPath)
    {
        return $this->getAlias() . '.' . $subPath;
    }



    /**
     * @param string $subPath
     *
     * @return mixed
     */
    public function config($subPath)
    {
        return config($this->configPath($subPath));
    }



    /**
     * @param string $action
     * @param mixed  ...$otherParameters
     *
     * @return string
     */
    public function action(string $action, ...$otherParameters)
    {
        return action($this->getControllersNamespace() . '\\' . $action, ...$otherParameters);
    }



    /**
     * @param string $action
     * @param mixed  ...$otherParameters
     *
     * @return string
     */
    public function actionLocale(string $action, ...$otherParameters)
    {
        return action_locale($this->getControllersNamespace() . '\\' . $action, ...$otherParameters);
    }
}
