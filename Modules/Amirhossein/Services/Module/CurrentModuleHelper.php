<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/30/18
 * Time: 3:13 PM
 */

namespace Modules\Amirhossein\Services\Module;

class CurrentModuleHelper
{
    protected static $module;



    /**
     * @param ModuleHandler $module
     */
    public static function assign(ModuleHandler $module)
    {
        static::$module = $module;
    }



    /**
     * @return ModuleHandler|null
     */
    public static function getCurrentModule()
    {
        return static::$module ?: null;
    }



    /**
     * @param string $subPath
     *
     * @return null|string
     */
    public static function bladePath(string $subPath)
    {
        return ($module = static::getCurrentModule()) ? $module->bladePath($subPath) : null;
    }



    /**
     * @param string $subPath
     *
     * @return null|string
     */
    public static function assetPath(string $subPath)
    {
        return ($module = static::getCurrentModule()) ? $module->assetPath($subPath) : null;
    }



    /**
     * @param string $subPath
     *
     * @return null|string
     */
    public static function asset(string $subPath)
    {
        return ($module = static::getCurrentModule()) ? $module->asset($subPath) : null;
    }



    /**
     * @param string $subPath
     *
     * @return null|string
     */
    public static function transPath(string $subPath)
    {
        return ($module = static::getCurrentModule()) ? $module->transPath($subPath) : null;
    }



    /**
     * @param string $subPath
     *
     * @return null|string
     */
    public static function configPath(string $subPath)
    {
        return ($module = static::getCurrentModule()) ? $module->configPath($subPath) : null;
    }



    /**
     * @param string $subPath
     *
     * @return mixed
     */
    public static function config(string $subPath)
    {
        return ($module = static::getCurrentModule()) ? $module->config($subPath) : null;
    }



    /**
     * @param       $subPath
     * @param mixed ...$otherParameters
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function trans($subPath, ...$otherParameters)
    {
        return trans(static::transPath($subPath), ...$otherParameters);
    }



    /**
     * @param string $action
     * @param mixed  ...$otherParameters
     *
     * @return string
     */
    public static function action(...$parameters)
    {
        return ($module = static::getCurrentModule())
             ? $module->action(...$parameters)
             : "";
    }



    /**
     * @param string $action
     * @param mixed  ...$otherParameters
     *
     * @return string
     */
    public static function actionLocale(...$parameters)
    {
        return ($module = static::getCurrentModule())
             ? $module->actionLocale(...$parameters)
             : "";
    }
}
