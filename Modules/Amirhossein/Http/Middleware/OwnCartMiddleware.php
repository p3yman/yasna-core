<?php

namespace Modules\Amirhossein\Http\Middleware;

use App\Models\Cart;
use Closure;
use Illuminate\Http\Request;

class OwnCartMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->guest()) {
            if (
                !($cart = model('Cart')->grabHashid($request->cart)) or
                !$cart->exists or
                ($cart->user_id != user()->id)
            ) {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
