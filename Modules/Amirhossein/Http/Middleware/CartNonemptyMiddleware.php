<?php

namespace Modules\Amirhossein\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Amirhossein\Providers\CartServiceProvider;

class CartNonemptyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cart = CartServiceProvider::findActiveCart();
        if (!$cart->orders()->count()) {
            return redirect('/');
        }

        return $next($request);
    }
}
