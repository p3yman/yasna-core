<?php

namespace Modules\Amirhossein\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DebugMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!env('APP_DEBUG')) {
            return redirect(url('/'));
        }

        return $next($request);
    }
}
