<?php

use Modules\Amirhossein\Providers\RouteServiceProvider;

/*
|--------------------------------------------------------------------------
| Manage
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => 'web'], function () {

    Route::group(
        [
            'middleware' => ['auth', 'is:admin'],
            'prefix'     => 'manage/amirhossein',
            'namespace'  => 'Modules\Amirhossein\Http\Controllers\Manage',
        ],
        function () {
            /*
            |--------------------------------------------------------------------------
            | Drawing
            |--------------------------------------------------------------------------
            |
            */
            Route::group(['prefix' => 'draw'], function () {
                Route::get('{hashid}', 'DrawingController@draw')
                    ->name('amirhossein.draw');
                Route::get('{hashid}/winners', 'DrawingController@winners')
                    ->name('amirhossein.draw.winners');
                Route::post('prepare', 'DrawingController@prepareForDraw')
                    ->name('amirhossein.draw.prepare');
                Route::post('select', 'DrawingController@select')
                    ->name('amirhossein.draw.select');
                Route::get('act/{hashid}/{viewFile}', 'DrawingController@singleAction')
                    ->name('amirhossein.draw.act');
                Route::get('delete/{key}', 'DrawingController@delete')
                    ->name('amirhossein.draw.delete');
            });
        }
    );


    Route::group([
        'namespace' => 'Modules\Amirhossein\Http\Controllers\Auth',
    ],
        function () {
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::get('home', 'LoginController@home')->name('home');

            Route::get('register', 'RegisterController@showRegistrationForm')->name('register');

            Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        }
    );

    Route::get('pay/{cart}', 'Modules\Amirhossein\Http\Controllers\PurchaseController@paymentShortLink');

    Route::group(
        ['middleware' => ['locale'], 'namespace' => 'Modules\Amirhossein\Http\Controllers'],
        function () {
            Route::get('/', 'FrontController@index')->name('amirhossein.front');
            Route::group(
                [
                    'prefix' => '{lang}',
                    'where'  => ['lang' => '[^\/_][^\/]*+'],
                ], function ($router) {
                    Route::group([
                    'namespace' => 'Auth',
                ],
                    function () {
                        Route::get('login', 'LoginController@showLoginForm');
                        Route::post('login', 'LoginController@login');

                        Route::get('register', 'RegisterController@showRegistrationForm');
                        Route::post('register', 'RegisterController@register');

                        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm');
                        Route::post('password/reset', 'ResetPasswordController@reset');
                        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm');
                        //Route::post('password/email', 'ForgotPasswordController@showLinkRequestForm')->name('password.email');

                        Route::get('password/token/{haveCode?}', 'ResetPasswordController@getToken');
                        Route::post('password/token', 'ResetPasswordController@checkToken');
                        Route::get('password/new', 'ResetPasswordController@newPassword');
                        Route::post('password/new', 'ResetPasswordController@changePassword');
                    }
                );

                    Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
                        Route::get('dashboard', 'UserController@index')->name('amirhossein.user.dashboard');

                        Route::get('orders', 'UserController@orders')->name('amirhossein.user.orders');
                        Route::get('orders/{order}/info', 'UserController@orderInfo')->name('amirhossein.user.order.info');

                        Route::get('profile', 'UserController@profile')->name('amirhossein.user.profile');
                        Route::post('profile', 'UserController@update')->name('amirhossein.user.profile.update');

                        Route::get('drawing', 'UserController@drawing')->name('amirhossein.user.drawing');

                        Route::get('events', 'UserController@events')->name('amirhossein.user.events');

                        Route::get('addresses', 'UserController@addresses')->name('amirhossein.user.addresses');
                        Route::get('addresses/{hashid}', 'UserController@addressesRow')
                        ->name('amirhossein.user.addresses.row');
                        //            Route::get('/dashboard/previous_comments/{post_id}', 'UserController@previousComments');
                    });

                    Route::group(['prefix' => 'address', 'middleware' => 'auth'], function () {
                        Route::post('save', 'AddressController@save')->name('amirhossein.address.save');
                    });

                    Route::group(['prefix' => 'cart'], function () {
                        Route::get('/', 'CartController@index')->name('amirhossein.cart');
                        Route::get('flush', 'CartController@flush')->name('amirhossein.cart.flush');
                        Route::post('modify', 'CartController@modifyCart')->name('amirhossein.cart.modify');

                        Route::group(['prefix' => 'item'], function () {
                            Route::post('add', 'CartController@addItem')->name('amirhossein.cart.item.add');
                            Route::post('remove', 'CartController@removeItem')->name('amirhossein.cart.item.remove');
                            Route::post('{hashid}', 'CartController@modifyItem')->name('amirhossein.cart.item.modify');
                        });

                        Route::group(['prefix' => 'view'], function () {
                            Route::get('reload', 'CartController@reloadView')->name('amirhossein.cart.view.reload');
                        });
                    });

                    Route::group(['prefix' => 'purchase'], function () {
                        Route::get('user', 'PurchaseController@step1')->name('amirhossein.purchase.step1');

                        Route::group(['middleware' => 'auth'], function () {
                            Route::get('shipping', 'PurchaseController@step2')->name('amirhossein.purchase.step2');
                            Route::get('shipping/content', 'PurchaseController@reloadStep2')
                            ->name('amirhossein.purchase.step2.content');
                            Route::get('shipping/verify', 'PurchaseController@verifyStep2')
                            ->name('amirhossein.purchase.step2.verify');

                            Route::get('review', 'PurchaseController@step3')->name('amirhossein.purchase.step3');
                            Route::get('review/verify', 'PurchaseController@verifyStep3')
                            ->name('amirhossein.purchase.step3.verify');

                            Route::get('pay', 'PurchaseController@step4')->name('amirhossein.purchase.step4');

                            Route::post('final', 'PurchaseController@final')->name('amirhossein.purchase.final');

                            Route::get('payment/result/{cart}', 'PurchaseController@paymentCallback')
                                ->middleware('ownCart')
                                ->name('amirhossein.purchase.paymentResult');
                        });
                    });

                    Route::group(['prefix' => 'payment'], function () {
                        Route::post('repurchase/{transaction}', 'PaymentController@repurchase')
                        ->name('amirhossein.payment.repurchase');
                        Route::get('fire/{transaction}', 'PaymentController@fire')
                        ->name('amirhossein.payment.fire');
                        Route::post('declare/{transaction}', 'PaymentController@declare')
                        ->name('amirhossein.payment.declare');
                    });

                    Route::group(['prefix' => 'purchase'], function () {
                        Route::get('reload', 'CartController@reloadView')->name('amirhossein.cart.view.reload');
                    });


                    Route::group(['prefix' => 'province'], function () {
                        Route::get('cities/{province}/select', 'ProvinceController@getProvinceCitiesSelect')
                        ->name('amirhossein.province.cities.select');
                    });

                    Route::get('events/{type}', 'PostController@eventsAjax')
                    ->name('amirhossein.eventsAjax')
                    ->where('type', '^(waiting|expired){1,1}$');

                    Route::group(['prefix' => 'drawing-code'], function () {
                        Route::post('submit', 'DrawingController@submitCode')->name('amirhossein.drawing-code.submit');
                    });

                    Route::get('contact', 'FrontController@contact')->name('amirhossein.contact');

                    Route::get('products/search', 'PosttypeController@productsSearch')
                    ->name('amirhossein.products.list');

                    Route::group(['prefix' => 'archive'], function () {
                        Route::get('{posttype}/categories', 'PosttypeController@showCategories')
                        ->name('amirhossein.posttype.showCategories');
                        Route::post('{posttype}/filter', 'PosttypeController@filter')
                        ->name('amirhossein.posttype.filterList');
                        Route::get('{posttype}/{folder?}', 'PosttypeController@showList')
                        ->name('amirhossein.posttype.showList');
                    });
                    foreach (RouteServiceProvider::getStaticPosttypes() as $posttype) {
                        Route::get("$posttype/categories", "PosttypeController@{$posttype}Categories")
                        ->name("amirhossein.$posttype.showCategories");
                        Route::get("$posttype/{folder?}", "PosttypeController@$posttype")
                        ->name("amirhossein.$posttype.showList");
                    }

                    Route::get('{identifier}/more-comments/{comment?}', 'PostController@moreComments')
                    ->where('identifier', '^P(\w|)+$')
                    ->name('amirhossein.post.single.moreComments');

                    Route::get('{identifier}/{title?}', 'PostController@single')
                    ->where('identifier', '^P(\w|)+$')
                    ->name('amirhossein.post.single');


                    Route::get('product/{identifier}/{wareOrTitle?}/{title?}', 'PostController@productSingle')
                    ->name('amirhossein.product.single')
                    ->where('identifier', '^P(\w|)+$');

                    Route::group(['prefix' => 'comment'], function () {
                        Route::post('save', 'CommentController@save')
                        ->name('amirhossein.post.comment.save');
                    });

                    Route::get('{page}', 'FrontController@page')
                    ->name('amirhossein.static');

                    Route::get('', 'FrontController@index')->name('amirhossein.front.with-locale');

                    Route::group(['prefix' => 'test'], function () {
                        Route::get('product-filter-form', 'TestController@productFilterForm');

                    });
                });

            Route::group(['prefix' => 'amirhossein'], function () {
                Route::get('/', 'AmirhosseinController@index');
            });
        });
});
