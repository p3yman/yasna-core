<?php

namespace Modules\Amirhossein\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DeclareRequest extends YasnaRequest
{
    protected $model_name = "";

    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.

    public function rules()
    {
        return [
            'trackingCode'    => ['required'],
            'declarationDate' => [
                'required',
                'date',
            ],
        ];
    }

    public function purifier()
    {
        return [
            'trackingCode'    => 'ed',
            'declarationDate' => 'gDate',
        ];
    }
}
