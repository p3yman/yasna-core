<?php

namespace Modules\Amirhossein\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class RepurchaseRequest extends YasnaRequest
{
    protected $model_name = "Transaction";


    public function purifier()
    {
        return [
            'account_id' => 'dehash',
        ];
    }
}
