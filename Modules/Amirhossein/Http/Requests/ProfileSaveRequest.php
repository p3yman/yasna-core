<?php

namespace Modules\Amirhossein\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Providers\ValidationServiceProvider;

class ProfileSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return user()->exists;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_first'    => 'required|persian:60',
            'name_last'     => 'required|persian:60',
            'name_father'   => 'persian:60',
            'birth_date'    => 'required|before_or_equal:' . date('Y-m-d'),
            'edu_level'     => 'numeric|in:' . implode(',', CurrentModuleHelper::config('edu-levels-values')),
            'mobile'        => 'required|phone:mobile',
            'tel'           => 'required|phone:fixed',
            'email'         => 'email',
            'gender'        => 'required|numeric|in:' . implode(',', CurrentModuleHelper::config('genders-values')),
            'marital'       => 'required|numeric|in:' . implode(',', CurrentModuleHelper::config('marital-statuses-values')),
            'marriage_date' => 'date|required_if:marital,2|before_or_equal:' . date('Y-m-d'),
            'new_password'  => 'same:new_password2|min:8|max:50',
//            'new_password2' => 'required_with:new_password|same:new_password|min:8|max:50',
        ];
    }

    public function all($keys = null)
    {
        $value = parent::all();
        $purified = ValidationServiceProvider::purifier($value, [
            'name_first'    => 'pd',
            'name_last'     => 'pd',
            'name_father'   => 'pd',
            'birth_date'    => 'gDate',
            'marriage_date' => 'gDate',
            'mobile'        => 'ed',
            'password'      => 'ed',
            'password2'     => 'ed',
        ]);
        return $purified;
    }
}
