<?php

namespace Modules\Amirhossein\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Yasna\Providers\ValidationServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class AddOrderRequest extends YasnaRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ware'  => 'required|exists:wares,id',
            'count' => 'required|numeric',
        ];
    }

    public function purifier($key = null)
    {
        return [
            'ware'  => 'dehash',
            'count' => 'ed',
        ];
    }
}
