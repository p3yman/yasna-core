<?php

namespace Modules\Amirhossein\Http\Requests\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Yasna\Providers\ValidationServiceProvider;

class ModifyCartRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => Rule::exists('addresses', 'id', function ($query) {
                $query->where('user_id', user()->id);
            }),
            'courier' => 'exists:couriers,id',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $value = parent::all();
        $purified = ValidationServiceProvider::purifier($value, [
            'address' => 'dehash',
            'courier' => 'dehash',
        ]);
        return $purified;
    }
}
