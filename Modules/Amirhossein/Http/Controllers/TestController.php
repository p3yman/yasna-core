<?php

namespace Modules\Amirhossein\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;

class TestController extends Controller
{
    use AmirhosseinFrontControllerTrait;

    public function productFilterForm()
    {
        return $this->template('test.product-filter-form.main');
    }
}
