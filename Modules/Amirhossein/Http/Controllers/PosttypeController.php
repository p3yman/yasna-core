<?php

namespace Modules\Amirhossein\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Posttype;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\URL;
use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;
use Modules\Amirhossein\Providers\PostServiceProvider;
use Modules\Amirhossein\Providers\RouteServiceProvider;
use Modules\Amirhossein\Services\Controller\AmirhosseinFrontControllerAbstract;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Amirhossein\Services\ProductFilter\ProductFilter;

class PosttypeController extends AmirhosseinFrontControllerAbstract
{
    protected $show_list_configs   = [
         'showError' => false,
    ];
    protected $list_container_data = [];
    protected $direct_methods      = ['filter', 'showCategories'];
    protected $temporary_variables = [];

    protected static $filterable_posttypes = ['products'];



    /**
     * @param string $method
     * @param array  $parameters
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        if (!in_array($method, $this->direct_methods)) {
            if (RouteServiceProvider::isStaticPosttype($method)) {
                $parameters['posttype'] = $method;
                $newParameters          = [new Request($parameters)];
                return parent::callAction('showList', $newParameters);
            } elseif (
                 ends_with($method, 'Categories') and
                 RouteServiceProvider::isStaticPosttype($posttype = str_before($method, 'Categories'))
            ) {
                $parameters['posttype'] = $posttype;
                $newParameters          = [new Request($parameters)];
                return parent::callAction('showCategories', $newParameters);
            } elseif (
                 (
                      (
                           CurrentModuleHelper::getCurrentModule()->getControllersNamespace()
                           . '\\'
                           . 'PosttypeController@showList'
                      ) ==
                      ('\\' . request()->route()->getActionName())
                 ) and
                 RouteServiceProvider::isStaticPosttype($parameters['posttype'])) {
                return $this->abort(404);
            }

            $route = \request()->route();
            if (
                 $route->hasParameter('posttype') and
                 method_exists($this, $posttype = $route->parameter('posttype'))
            ) {
                return parent::callAction($posttype, $parameters);
            }
        }

        return parent::callAction($method, $parameters);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function showList(Request $request)
    {
        return (
             $this->appendPosttypeToVariables($posttype_slug = $request->posttype)
                  ->appendToShowListConfigs('type', $posttype_slug)
                  ->showListPaginationLength()
                  ->showListFolder($request)
                  ->showListFilteringInfo($request)
                  ->appendToVariables('list', PostServiceProvider::showList($this->show_list_configs))
                  ->variables['list'] and
             ($this->variables['folder'] ?? true)
        )
             ? $this->template('posts.layouts.list.plane')
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function showListPaginationLength()
    {
        if ($page_length = $this->variables['posttype']->spreadMeta()->list_page_length) {
            $this->show_list_configs['max_per_page'] = $page_length;
            $this->appendToShowListConfigs('max_per_page', $page_length);
        }
        return $this;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function showListFolder(Request $request)
    {
        if ($folder_slug = $request->folder) {
            if (
                 (
                 $specified_folder = $this->variables['posttype']
                      ->categories()
                      ->where('slug', $request->folder)
                      ->where('is_folder', 1)
                      ->first()
                 ) and
                 $specified_folder->exists
            ) {
                $this->appendToShowListConfigs('category', $specified_folder->children()->pluck('id')->toArray());
                $this->appendToShowListVariables('folder', $specified_folder)
                     ->appendToVariables('folder', $specified_folder)
                ;
            } else {
                $this->appendToVariables('folder', false);
            }
        }
        return $this;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function showListFilteringInfo(Request $request)
    {
        if (in_array($request->posttype, self::$filterable_posttypes)) {
            $this->appendToShowListConfigs('is_base_page', true)
                 ->appendToShowListConfigs('ajax_request', false)
            ;
        }

        return $this;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function showCategories(Request $request)
    {
        return (
             $this->appendPosttypeToVariables($request->posttype)->variables['posttype']->exists and
             view()->exists($this->showCategoriesCategoriesViewPath()->variables['categories_view_path'])
        )
             ? $this->showCategoriesCategoriesView()->template('posts.layouts.categories.plane')
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function showCategoriesCategoriesViewPath()
    {
        return $this->appendToVariables(
             'categories_view_path',
             CurrentModuleHelper::bladePath("posts.categories." . $this->variables['posttype']->slug . ".main")
        );
    }



    /**
     * @return $this
     */
    protected function showCategoriesCategoriesView()
    {
        return $this->appendToVariables(
             'categories_view',
             view($this->variables['categories_view_path'], $this->variables)
        );
    }



    /**
     * @param string $posttype_slug
     *
     * @return $this
     */
    protected function appendPosttypeToVariables($posttype_slug)
    {
        return $this->appendToVariables('posttype', model('posttype')->grabSlug($posttype_slug));
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function events(Request $request)
    {
        $posttype = posttype($request->posttype);

        if (!$posttype or !$posttype->exists or !$posttype->has('list_view')) {
            return $this->abort('404');
        }

        $list = $this->getEventsListView();

        if (!$list) {
            return $this->abort(404);
        }

        return $this->template('posts.layouts.list.plane', compact('list', 'posttype'));
    }



    /**
     * @return null|string
     */
    protected function getEventsListView()
    {
        $generalRules = ['type' => 'events', 'showError' => false];

        $currentEventsListView = $this->getCurrentEventsListView($generalRules);
        $waitingEventsListView = $this->getWaitingEventsListView($generalRules);
        $expiredEventsListView = $this->getExpiredEventsListView($generalRules);

        if ($currentEventsListView or $waitingEventsListView or $expiredEventsListView) {
            return $currentEventsListView . $waitingEventsListView . $expiredEventsListView;
        } else {
            return null;
        }
    }



    /**
     * @param array $generalRules
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getCurrentEventsListView($generalRules = [])
    {
        return PostServiceProvider::showList(array_merge(
             $generalRules,
             [
                  'conditions' => [
                       ['event_starts_at', '<=', Carbon::now()],
                       ['event_ends_at', '>=', Carbon::now()],
                  ],
                  'sort_by'    => 'event_ends_at',
                  'sort'       => 'ASC',
             ]
        ));
    }



    /**
     * @param array $generalRules
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getWaitingEventsListView($generalRules = [])
    {
        return PostServiceProvider::showList(array_merge(
             $generalRules,
             [
                  'conditions' => [
                       ['event_starts_at', '>=', Carbon::now()],
                       ['event_ends_at', '>=', Carbon::now()],
                  ],
                  'sort_by'    => 'event_starts_at',
                  'sort'       => 'ASC',
             ]
        ));
    }



    /**
     * @param array $generalRules
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getExpiredEventsListView($generalRules = [])
    {
        return PostServiceProvider::showList(array_merge(
             $generalRules,
             [
                  'conditions' => [
                       ['event_starts_at', '<=', Carbon::now()],
                       ['event_ends_at', '<=', Carbon::now()],
                  ],
                  'sort_by'    => 'event_ends_at',
             ]
        ));
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function products(Request $request)
    {
        return $this->appendToShowListConfigs('is_base_page', true)
                    ->showList($request)
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request)
    {
        return $this->appendPosttypeToVariables($request->posttype)
                    ->appendToTemporaryVariables('folder', $this->filterFolder())
                    ->filterPosts($request)
                    ->filterPaginationPath()
                    ->appendToVariables('viewFolder', 'posts.list.product')
                    ->appendToVariables('ajaxRequest', true)
                    ->appendToVariables('isBasePage', false)
                    ->template($this->variables['viewFolder'] . '.main')
             ;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function filterPosts(Request $request)
    {
        $this->appendToTemporaryVariables('hash', $request->hash)
             ->appendToTemporaryVariables('hash_array', $this->translateHash($this->temporary_variables['hash']))
             ->appendToTemporaryVariables('folder', $this->filterFolder())
             ->appendToTemporaryVariables('max_per_page',
                  $this->variables['posttype']->spreadMeta()->list_page_length ?: 12)
        ;
        if (count($this->temporary_variables['hash_array'])) {
            return $this->filterQuery();
        } else {
            return $this->filterWithNoHash();
        }
    }



    /**
     * @return $this
     */
    protected function filterQuery()
    {
        return $this->appendToTemporaryVariables(
             'hash',
             $this->modifyHash($hash_array = $this->temporary_variables['hash_array'],
                  $this->temporary_variables['hash'])
        )
                    ->appendToTemporaryVariables('filter', new ProductFilter($hash_array))
                    ->filterQueryFolder()
                    ->appendToVariables(
                         'posts',
                         $this->temporary_variables['filter']->paginate($this->temporary_variables['max_per_page'])
                    )
             ;
    }



    /**
     * @return $this
     */
    protected function filterQueryFolder()
    {
        if ($folder = $this->temporary_variables['folder']) {
            $this->temporary_variables['filter'] = $this->temporary_variables['filter']->folder($folder);
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function filterPaginatorChanges()
    {
        return $this->appendToVariables('posts', $this->variables['posts']
             ->withPath(url()->previous())
             ->fragment($this->temporary_variables['hash'] ?: '')
             ->links()
        );
    }



    /**
     * @return $this
     */
    protected function filterWithNoHash()
    {
        return $this->appendToTemporaryVariables('selecting', ProductFilter::getSelectingColumnsinstructions()['wares'])
                    ->appendToShowListConfigs('type', $this->posttype_slugs['product'])
                    ->filterWithNoHashFolder()
                    ->appendToShowListConfigs('max_per_page', $this->temporary_variables['max_per_page'])
                    ->appendToVariables('posts', PostServiceProvider::collectPosts($this->show_list_configs))
                    ->filterWithNoHashPostsWares()
             ;
    }



    /**
     * @return $this
     */
    protected function filterWithNoHashFolder()
    {
        if ($folder = $this->temporary_variables['folder']) {
            $this->appendToShowListConfigs('category', $folder->children()->pluck('id'));
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function filterWithNoHashPostsWares()
    {
        $posts     = $this->variables['posts'];
        $selecting = $this->temporary_variables['selecting'];
        foreach ($posts as $post) {
            $firstWare = $post->wares()->first();
            if ($firstWare) {
                foreach ($selecting as $key => $val) {
                    if (is_numeric($key)) {
                        $column = $val;
                        $value  = $firstWare->$val;
                    } else {
                        $column = $val;
                        $value  = $firstWare->$key;
                    }
                    $post->$column = $value;
                }
            } else {
                foreach ($selecting as $column) {
                    $post->$column = null;
                }
            }
        }
        return $this->appendToVariables('posts', $posts);
    }



    /**
     * @return null|Category
     */
    protected function filterFolder()
    {
        if (starts_with($referrer = url()->previous(), $prefix = RouteServiceProvider::getPosttypeRoute('products'))) {
            $important_url    = str_after($referrer, $prefix);
            $parameters_parts = explode_not_empty('/', $important_url);
            if (count($parameters_parts)) {
                $main_category_slug = $parameters_parts[0];

                if (
                     ($found_folder = $this->variables['posttype']
                          ->categories()
                          ->where('slug', $main_category_slug)
                          ->where('is_folder', 1)
                          ->first()) and
                     $found_folder->exists
                ) {
                    return $found_folder;
                }
            }
        }

        return null;
    }



    /**
     * @return $this
     */
    protected function filterPaginationPath()
    {
        $this->variables['posts']->withPath(url()->previous());
        return $this;
    }



    /**
     * @param string $hash
     *
     * @return array
     */
    protected function translateHash($hash)
    {
        $hashArray = [];

        $hash = explode_not_empty('!', $hash);
        foreach ($hash as $field) {
            $field = explode_not_empty('?', $field);
            if (count($field) == 2) {
                $hashArray[$field[1]] = explode_not_empty('/', $field[0]);
                $currentGroup         = &$hashArray[$field[1]];
                $currentGroup         = $this->arrayPrefixToIndex('_', $currentGroup);
            }
        }

        return $hashArray;
    }



    /**
     * @param array  $hashArray
     * @param string $hash
     *
     * @return string
     */
    protected function modifyHash($hashArray, $hash)
    {
        if (
             isset($hashArray['checkbox']['category']) and
             is_array(($categoriesArray = $hashArray['checkbox']['category']))
        ) {
            $ids        = array_map(function ($item) {
                return (($dec = hashid_decrypt($item, 'ids')) and count($dec)) ? $dec[0] : null;
            }, $categoriesArray);
            $categories = Category::whereIn('id', $ids)->get();
            foreach ($categories as $category) {
                $hash = str_replace($category->hashid, $category->slug, $hash);
            }
        }

        return $hash;
    }



    /**
     * @param string       $delimiter
     * @param array|string $haystack
     *
     * @return array|string
     */
    protected function arrayPrefixToIndex($delimiter, $haystack)
    {
        if (is_array($haystack)) {
            foreach ($haystack as $index => $field) {
                $parts = explode($delimiter, $field, 2);
                if (count($parts) == 2) {
                    $key   = $parts[0];
                    $value = $parts[1];
                    $value = $this->arrayPrefixToIndex($delimiter, $value);

                    $target = &$haystack[$key];
                    if (isset($target)) {
                        if (!is_array($target)) {
                            $target = [$target];
                        }

                        if (!is_array($value)) {
                            $value = [$value];
                        }

                        $target = array_merge($target, $value);
                    } else {
                        $target = $value;
                    }
                }
                unset($haystack[$index]);
            }

            return $haystack;
        }

        if (is_string($haystack)) {
            $parts = explode($delimiter, $haystack, 2);
            if (count($parts) == 2) {
                $key   = $parts[0];
                $value = $parts[1];
                $value = $this->arrayPrefixToIndex($delimiter, $value);

                $haystack = [
                     $key => $value,
                ];

                return $haystack;
            }
        }

        return $haystack;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function productsSearch(Request $request)
    {
        if (!($text = request('search'))) {
            return redirect(RouteServiceProvider::actionLocale('FrontController@index'));
        }

        return redirect(
             RouteServiceProvider::getPosttypeRoute('products')
             . '#'
             . $this->encryptFilterHash(['text' => ['title' => $text]])
        )->with('focusedElement', 'productName');
    }



    /**
     * @param array $hashArray
     * @param bool  $hashid
     *
     * @return string
     */
    protected function encryptFilterHash(array $hashArray, bool $hashid = true)
    {
        $result = '';

        foreach ($hashArray as $key => $group) {
            if (is_array($group) and count($group)) {
                $result .= '!';
                $parts  = [];

                foreach ($group as $identifier => $data) {
                    $groupPrefix = $identifier . '_';

                    if (is_bool($data)) {
                        $parts[] = $groupPrefix . ($data ? 1 : 0);
                    } elseif (is_string($data)) {
                        $parts[] = $groupPrefix . $data;
                    } elseif (is_array($data)) {
                        foreach ($data as $index => $value) {
                            $prefix  = is_string($index) ? ($groupPrefix . $index . '_') : $groupPrefix;
                            $parts[] = $this->filterHashMapValue($hashid, $prefix, $value);
                        }
                    }
                }

                $result .= implode('/', $parts);
                $result .= '?' . $key;
            }
        }

        $result .= (($result) ? '!' : '');

        return $result;
    }



    /**
     * @param string $hashid
     * @param string $prefix
     * @param mixed  $value
     *
     * @return string
     */
    public function filterHashMapValue($hashid, $prefix, $value = null)
    {
        if (is_bool($value)) {
            return $prefix . (($value) ? 1 : 0);
        } else {
            if (is_array($value) and array_key_exists('val', $value)) {
                if ($hashid and array_key_exists('hashid', $value)) {
                    return $prefix . $value['hashid'];
                } else {
                    return $prefix . $value['val'];
                }
            } else {
                return $prefix . $value;
            }
        }
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    protected function appendToShowListConfigs($key, $value)
    {
        $this->show_list_configs[$key] = $value;
        return $this;
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    protected function appendToShowListVariables($key, $value)
    {
        $this->show_list_configs['variables'][$key] = $value;
        return $this;
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    protected function appendToTemporaryVariables($key, $value)
    {
        $this->temporary_variables[$key] = $value;
        return $this;
    }
}
