<?php

namespace Modules\Amirhossein\Http\Controllers\Traits;

use Modules\Amirhossein\Services\Module\ModuleHandler;

trait ModuleControllerTrait
{
    /**
     * @return string
     */
    public static function getModuleName()
    {
        return (ModuleHandler::create(self::class))->getAlias();
    }
}
