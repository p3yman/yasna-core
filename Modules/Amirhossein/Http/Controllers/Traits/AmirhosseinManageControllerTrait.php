<?php

namespace Modules\Amirhossein\Http\Controllers\Traits;

use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;

trait AmirhosseinManageControllerTrait
{
    use AmirhosseinFrontControllerTrait;

    protected $view_folder = 'manage.drawing';

    public function singleAction($modelHashid, $viewFile, ... $option)
    {
        /*-----------------------------------------------
        | Mass Action Bypass ...
        */
        if ($modelHashid == 'mass') {
            return $this->massAction($viewFile, ... $option);
        }

        /*-----------------------------------------------
        | Special Method Before Model Retreive ...
        */
        $special_method = camel_case($viewFile . "_root_form");
        if (method_exists($this, $special_method)) {
            return $this->$special_method($modelHashid, ... $option);
        }

        /*-----------------------------------------------
        | Model ...
        */
        $base_model = $this->base_model;
        $with_trashed = method_exists($base_model, "restore");
        if ($modelHashid == '0' or $modelHashid == hashid(0)) {
            $model = new $base_model();
        } else {
            $model = model($base_model, $modelHashid, [
                'with_trashed' => $with_trashed,
            ]);

            if (!$model or !$model->exists) {
                return $this->abort(410, true);
            }

            $model->spreadMeta();
        }

        /*-----------------------------------------------
        | Special Method after Model Retreive  ...
        */
        $special_method = camel_case($viewFile . "_form");
        if (method_exists($this, $special_method)) {
            return $this->$special_method($model, ... $option);
        }

        /*-----------------------------------------------
        | View ...
        */
        return $this->template($this->view_folder . "." . $viewFile, compact('model', 'option'));
    }
}
