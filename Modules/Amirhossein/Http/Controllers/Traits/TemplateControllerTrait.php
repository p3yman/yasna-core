<?php

namespace Modules\Amirhossein\Http\Controllers\Traits;

trait TemplateControllerTrait
{
    use ModuleControllerTrait;

    private function template(...$parameters)
    {
        if (is_string($parameters[0]) and !str_contains($parameters[0], '::')) {
            $parameters[0] = self::getModuleName() . '::' . $parameters[0];
        }

        if (!view()->exists($parameters[0])) {
            return $this->abort(404);
        }

        return template(...$parameters);
    }
}
