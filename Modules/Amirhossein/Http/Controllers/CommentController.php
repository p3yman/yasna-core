<?php

namespace Modules\Amirhossein\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;
use Modules\Amirhossein\Http\Requests\CommentSaveRequest;

class CommentController extends Controller
{
    use AmirhosseinFrontControllerTrait;

    protected $indexesToChange = [
        'name'    => 'guest_name',
        'email'   => 'guest_email',
        'mobile'  => 'guest_mobile',
        'message' => 'text',
    ];

    protected $request;

    public function save(CommentSaveRequest $request)
    {
        $this->request = $request;

        $savedComment = model('comment')
            ->batchSave($this->changeIndexes($request->all()));


        return $this->jsonAjaxSaveFeedback($savedComment->exists, [
            'success_message'      => CurrentModule::trans('general.message.success.send-message'),
            'success_form_reset'   => 1,
            'success_feed_timeout' => 5000,

            'danger_message' => CurrentModule::trans('general.message.error.send-message'),
        ]);
    }

    protected function changeIndexes(array $data)
    {
        foreach ($this->indexesToChange as $from => $to) {
            if (array_key_exists($from, $data)) {
                $data[$to] = $data[$from];
                unset($data[$from]);
            }
        }

        return array_default($data, $this->additionalIndexes());
    }

    protected function additionalIndexes()
    {
        return [
            'guest_ip'    => \request()->ip(),
            'user_id'     => user()->id,
            'posttype_id' => $this->request->getPost()->posttype->id,
        ];
    }
}
