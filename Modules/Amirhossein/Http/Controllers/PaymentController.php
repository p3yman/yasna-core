<?php

namespace Modules\Amirhossein\Http\Controllers;

use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;
use Modules\Amirhossein\Http\Requests\DeclareRequest;
use Modules\Amirhossein\Http\Requests\RepurchaseRequest;
use Modules\Amirhossein\Services\Controller\AmirhosseinFrontControllerAbstract;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class PaymentController extends AmirhosseinFrontControllerAbstract
{
    /**
     * @param RepurchaseRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function repurchase(RepurchaseRequest $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction))) {
            return $this->abort(410);
        }

        $newTransaction = $transaction->clone($request->only('account_id'));

        return $this->jsonFeedback([
             'ok'           => 1,
             'redirect'     => CurrentModuleHelper::actionLocale('PaymentController@fire', [
                  'transaction' => $newTransaction->getModel()->hashid,
             ]),
             'message'      => CurrentModuleHelper::trans('form.feed.wait'),
             'redirectTime' => 3000,
        ]);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function fire(Request $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction))) {
            return $this->abort(410);
        }


        $transactionResponse = $transaction->fire();

        if ($transactionResponse['slug'] == 'returnable-response') {
            return $transactionResponse['response'];
        }

        return redirect(CurrentModuleHelper::actionLocale('PurchaseController@paymentCallback', [
             'cart' => hashid_encrypt($transaction->getModel()->invoice_id, 'ids'),
        ]));
    }



    /**
     * @param DeclareRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function declare(DeclareRequest $request)
    {
        if (!($transaction = $this->findTransaction($request->transaction)) or !$transaction->isDeclarable()) {
            return $this->abort(410);
        }

        $rs = $transaction->resnum($request->trackingCode)
                          ->declared_at($request->declarationDate)
                          ->update()
        ;

        if ($rs['code'] > 0) {
            $transaction->cart()->update([
                 'disbursed_at' => Carbon::now(),
                 'disbursed_by' => user()->id,
            ])
            ;
            return $this->jsonFeedback([
                 'ok'           => 1,
                 'message'      => CurrentModuleHelper::trans('form.feed.wait'),
                 'refresh'      => 1,
                 'redirectTime' => 3000,
            ]);
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @param $hashid
     *
     * @return null|\Modules\Payment\Services\PaymentHandler\Transaction\Transaction
     */
    protected function findTransaction($hashid)
    {
        if (($transaction = model('Transaction')->grabHashid($hashid)) and $transaction->exists) {
            return $transaction->handler;
        }

        return null;
    }
}
