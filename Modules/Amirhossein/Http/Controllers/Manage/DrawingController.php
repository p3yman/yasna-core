<?php

namespace Modules\Amirhossein\Http\Controllers\Manage;

use App\Models\Drawing;
use App\Models\Post;
use App\Models\Receipt;
use Illuminate\Http\Request;
use Modules\Amirhossein\Http\Requests\Manage\DrawRequest;
use Modules\Amirhossein\Providers\PostServiceProvider;
use Modules\Amirhossein\Services\Controller\ModuleControllerTrait;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Services\YasnaController;

class DrawingController extends YasnaController
{
    use ModuleControllerTrait {
        __construct as protected __traitConstruct;
    }

    protected $view_folder;
    protected $base_model = 'post';



    /**
     * DrawingController constructor.
     */
    public function __construct()
    {
        $this->__traitConstruct();
        $this->view_folder = $this->getModule()->getAlias() . '::manage.drawing';
    }



    /**
     * Renders the modal for draw process.
     * 
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function draw(Request $request)
    {
        $model = PostServiceProvider::smartFindPost($request->hashid, [
             'conditions' => [
                  'type' => 'features:event',
             ],
        ]);

        $posttype = $model->posttype;

        if (!$posttype->has('event')) {
            return $this->abort(403);
        }

        return $this->view('draw', compact('model', 'posttype'));
    }



    /**
     * Prepares the database for drawing.
     * 
     * @param Request $request
     *
     * @return string
     */
    public function prepareForDraw(Request $request)
    {
        /*-----------------------------------------------
        | Row Selection
        */
        $post = Post::find($request->id);
        if (!$post or $post->hasnot('event')) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }

        /*-----------------------------------------------
        | Action
        */
        $limit = CurrentModuleHelper::config('draw.query-limit');

        $receiptHolders = Receipt::select('user_id')
                                 ->where('operation_integer', 1)
                                 ->limit($limit)
        ;

        $lineNumber = session()->get('line_number');


        foreach ($receiptHolders->get() as $receipt_holder) {
            $amount       = $post->receipts->where('user_id', $receipt_holder->user_id)
                                           ->sum('purchased_amount')
            ;
            $total_points = intval(floor($amount / $post->getMeta('rate_point')));

            if ($total_points) {
                Drawing::create([
                     'user_id'    => $receipt_holder->user_id,
                     'post_id'    => $post->id,
                     'amount'     => $amount,
                     'lower_line' => $lineNumber + 1,
                     'upper_line' => $lineNumber += $total_points,
                ]);
            }
        }

        $finished = boolval($lineNumber == session()->get('line_number'));
        $receiptHolders->update(['operation_integer' => '2',]);
        session()->put('line_number', $lineNumber);

        /*-----------------------------------------------
        | Feedback
        */

        return $this->jsonAjaxSaveFeedback($finished, [
            //            'redirectTime'       => $finished ? 500 : 1,
            'redirectTime'       => 500,
            'success_message'    => CurrentModuleHelper::trans('form.feed.done'),
            'success_callback'   => "masterModal('"
                 . CurrentModuleHelper::action('Manage\DrawingController@winners', ['hashid' => $post->hashid]) .
                 "')",
            'success_modalClose' => "0",

            'danger_message'  => CurrentModuleHelper::trans('form.feed.wait'),
            'danger_callback' => "drawingProgress($limit);rowUpdate('tblPosts','$post->hashid')",
        ]);
    }



    /**
     * Returns the winners list.
     * 
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function winners(Request $request)
    {
        $model = PostServiceProvider::smartFindPost($request->hashid, [
             'conditions' => [
                  'type' => 'features:event',
             ],
        ]);

        $posttype = $model->posttype;

        if (!$posttype->has('event')) {
            return $this->abort(403);
        }

        return $this->view('winners', compact('model', 'posttype'));
    }



    /**
     * Selects a chance as winner.
     *
     * @param DrawRequest $request
     *
     * @return string
     */
    public function select(DrawRequest $request)
    {
        $user_accepted = false;
        /*-----------------------------------------------
        | Post Selection ...
        */
        $post = Post::find($request->post_id);
        if (!$post or !$post->exists) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }

        $winners = $post->winners_array;


        /*-----------------------------------------------
        | User Selection ...
        */
        $number = $request->number;
        for ($try = 1; $try <= 10; $try++) {
            $drawingRow = Drawing::pull($number);
            if (!$drawingRow or $drawingRow->post_id != $post->id) {
                return $this->jsonFeedback([
                     'message'  => CurrentModuleHelper::trans('form.feed.error'),
                     'callback' => "masterModal("
                          . CurrentModuleHelper::action('DrawingController@draw', ['hashid' => $post->hashid])
                          . ")",
                ]);
            }
            $user = $drawingRow->user;


            if ($user and $user->exists and !in_array($user->id, $winners)) {
                $user_accepted = true;
                break;
            }

            $number = rand(1, session()->get('line_number'));
        }

        if (!$user_accepted) {
            return $this->jsonSaveFeedback(0);
        }

        /*-----------------------------------------------
        | Save ...
        */
        $winners[] = $user->id;
        $ok        = $post->batchSaveBoolean(['winners' => $winners]);

        /*-----------------------------------------------
        | Feedback ...
        */

        return $this->jsonAjaxSaveFeedback($ok, [
                  'success_message'    => trans('validation.attributes.number')
                       . " "
                       . ad($request->number)
                       . ": "
                       . $user->full_name,
                  'success_modalClose' => false,
                  'success_callback'   => "divReload( 'divWinnersTable' );rowUpdate('tblPosts','$post->hashid')",
             ]
        );
    }



    /**
     * Deletes the specified winner.
     *
     * @param Request $request
     */
    public function delete(Request $request)
    {
        $key = $request->key;

        $postId = session()->get('drawing_post');
        $post   = Post::find($postId);
        if ($post and $post->exists) {
            $winners = $post->winners_array;
            unset($winners[$key]);
            $post->updateMeta(['winners' => array_values($winners)], true);
        }
    }
}
