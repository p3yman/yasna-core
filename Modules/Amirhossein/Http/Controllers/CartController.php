<?php

namespace Modules\Amirhossein\Http\Controllers;

use App\Models\Address;
use App\Models\Order;
use Illuminate\Http\Request;
use Modules\Amirhossein\Http\Requests\Cart\AddOrderRequest;
use Modules\Amirhossein\Http\Requests\Cart\ModifyCartRequest;
use Modules\Amirhossein\Http\Requests\Cart\ModifyItemRequest;
use Modules\Amirhossein\Providers\CartServiceProvider;
use Modules\Amirhossein\Services\Controller\AmirhosseinFrontControllerAbstract;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class CartController extends AmirhosseinFrontControllerAbstract
{
    /*
    |--------------------------------------------------------------------------
    | Cart Methods
    |--------------------------------------------------------------------------
    |
    */

    public function index()
    {
        $cart = CartServiceProvider::findActiveCart();
        $cart->refreshCalculations();

        return $this->template('cart.main', compact('cart'));
    }

    public function flush()
    {
        $cart = CartServiceProvider::findActiveCart();

        foreach ($cart->orders as $order) {
            $order->remove();
        }

        return view(CurrentModuleHelper::bladePath('cart.content'), ['cart' => CartServiceProvider::findActiveCart()]);
    }

    public function reloadView()
    {
        $cart = \Modules\Amirhossein\Providers\CartServiceProvider::findActiveCart();
        $this->setTemplateCart();
        return view(CurrentModuleHelper::bladePath('layouts.header-cart'), compact('cart'));
    }

    public function modifyCart(ModifyCartRequest $request)
    {
        $cart = CartServiceProvider::findActiveCart();

        if ($request->courier) {
            $cart->addCourier($request->courier);
        }

        if ($request->address) {
            $address = Address::find($request->address);

            if ($address and $address->exists and ($address->user_id == user()->id)) {
                user()->addresses()
                    ->where('default', 1)
                    ->where('id', '<>', $address->id)
                    ->update(['default' => 0]);

                $address->update(['default' => 1]);
                $cart->update(['address_id' => $address->id]);
            }
        }
//         die(__LINE__ . '-');

        return ['success' => $cart->exists];
    }

    /*
    |--------------------------------------------------------------------------
    | Item Methods
    |--------------------------------------------------------------------------
    |
    */

    public function addItem(AddOrderRequest $request)
    {
        $done = CartServiceProvider::addToActiveCart($request->ware, $request->count);

        return $this->jsonAjaxSaveFeedback($done, [
            'success_callback' => 'reloadCart()',
//            'danger_callback' => '',
        ]);
    }

    public function removeItem(Request $request)
    {
        $order = $this->validateOrder($request->item);

        if ($order->remove()) {
            return view(CurrentModuleHelper::bladePath('cart.content'), ['cart' => CartServiceProvider::findActiveCart()]);
        } else {
            return $this->abort(503);
        }
    }

    public function modifyItem(ModifyItemRequest $request)
    {
        $order = $this->validateOrder($request->hashid);

        if ($request->count and $order->alterCount($request->count)) {
            return view(CurrentModuleHelper::bladePath('cart.content'), ['cart' => CartServiceProvider::findActiveCart()]);
        } else {
            return $this->abort(410);
        }
    }

    /**
     * If $orderHashid points to an existed order of current user,
     *
     * @param string $orderHashid
     *
     * @return \App\Models\Order|null
     */
    protected function validateOrder($orderHashid)
    {
        $order = model('Order')->grabHashid($orderHashid);
        $cart = $order->cart;

        if ($order and $order->exists and
            (
                (user()->exists and ($cart->user_id == user()->id)) // Orders belongs to current user
                or
                (
                    // Order belongs to the cart in client's session
                    !user()->exists and
                    ($sessionCartId = CartServiceProvider::getCartIdFromSession()) and
                    ($cart->hashid == $sessionCartId)
                )
            )
        ) {
            return $order;
        }

        return null;
    }
}
