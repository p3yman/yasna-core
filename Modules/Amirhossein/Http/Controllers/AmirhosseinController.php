<?php

namespace Modules\Amirhossein\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;

class AmirhosseinController extends Controller
{
    use AmirhosseinFrontControllerTrait;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return $this->template('home.main');
    }
}
