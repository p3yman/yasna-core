<?php

namespace Modules\Amirhossein\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Posttype;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;
use Modules\Amirhossein\Providers\PostServiceProvider;
use Modules\Amirhossein\Services\Controller\AmirhosseinFrontControllerAbstract;

class FrontController extends AmirhosseinFrontControllerAbstract
{
    //use AmirhosseinFrontControllerTrait;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->indexSlideShow()
                    ->indexAbout()
                    ->indexEvent()
                    ->indexProductsFolders()
                    ->indexCustomersComments()
                    ->template('home.main')
             ;
    }



    /**
     * @return $this
     */
    protected function indexSlideShow()
    {
        return $this->appendToVariables('slideshow', PostServiceProvider::collectPosts([
             'type'     => $this->posttype_slugs['slider'],
             'category' => 'home-slideshow',
             'criteria' => 'published',
        ]));
    }



    /**
     * @return $this
     */
    protected function indexAbout()
    {
        return $this->appendToVariables('about', PostServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['static'],
             'slug' => 'about',
        ])->first() ?: post());
    }



    /**
     * @return $this
     */
    protected function indexEvent()
    {
        return $this->appendToVariables('event', PostServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['event'],
        ])->first() ?: post());
    }



    /**
     * @return $this
     */
    protected function indexProductsFolders()
    {
        return $this->appendToVariables(
             'product_folders',
             model('posttype')
                  ->grabSlug($this->posttype_slugs['product'])
                  ->categories()->where('is_folder', 1)
                  ->get()
        );
    }



    /**
     * @return $this
     */
    public function indexCustomersComments()
    {
        return $this->customersCommentsPost()
                    ->appendToVariables(
                         'customers_comments',
                         $this->variables['customers_comments_post']
                              ->comments()
                              ->whereNotNull('approved_at')
                              ->orderBy('approved_at', 'desc')
                              ->limit(10)
                              ->get())
             ;
    }



    /**
     * @return $this
     */
    public function customersCommentsPost()
    {
        return $this->appendToVariables('customers_comments_post', PostServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['commenting'],
             'slug' => 'customers-comments',
        ])->first() ?: post());
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return $this->contactSettingInfo()
                    ->contactCommentingPost()
                    ->template('contact.main')
             ;
    }



    /**
     * @return $this
     */
    protected function contactSettingInfo()
    {
        $setting_slugs = [
             'address',
             'fax',
             'email',
             'location',
             'manager',
             'telephone_2',
             'address_2',
             'fax_2',
             'email_2',
             'location_2',
             'manager_2',
        ];

        foreach ($setting_slugs as $setting_slug) {
            $this->appendToVariables($setting_slug, get_setting($setting_slug));
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function contactCommentingPost()
    {
        return $this->appendToVariables('commenting_post',
             PostServiceProvider::selectPosts([
                  'type' => $this->posttype_slugs['commenting'],
                  'slug' => 'contact',
             ])->first() ?: post()
        );
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function page(Request $request)
    {
        return $this->pageFindPost($request)->variables['post']
             ? $this->template('page.main')
             : $this->abort(404);
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function pageFindPost(Request $request)
    {
        return $this->appendToVariables('post', PostServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['static'],
             'slug' => $request->page,
        ])->first());
    }
}
