<?php namespace Modules\Amirhossein\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\Amirhossein\Http\Requests\Auth\PasswordTokenRequest;
use Modules\Amirhossein\Http\Requests\Auth\ResetPasswordRequest;
use Modules\Amirhossein\Http\Requests\NewPasswordRequest;
use Modules\Amirhossein\Services\Controller\AmirhosseinFrontControllerAbstract;
use Modules\Amirhossein\Services\Email\EmailServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class ResetPasswordController extends AmirhosseinFrontControllerAbstract // extends Yasna
{
    public static $resetTokenLength      = 6;
    public static $resetTokenLifetime    = 15; // in minutes
    public static $resetTokenSessionName = 'resetingPasswordNationalId';



    /**
     * @param ResetPasswordRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(ResetPasswordRequest $request)
    {
        if (!($user = $this->findUser($request)) or !$user->exists) {
            return $this->jsonAjaxSaveFeedback(false, [
                 'ok'             => 0,
                 'danger_message' => CurrentModuleHelper::trans('user.password.user'),
            ]);
        }

        $resetToken = $this->generateToken();

        try {
            $this->saveToken($user, $resetToken);
            $this->saveSession($user);

            $this->sendToken($request, $user, $resetToken);

            return $this->jsonAjaxSaveFeedback(true, [
                 'success_message'  => CurrentModuleHelper::trans('user.password.sent'),
                 'success_redirect' => CurrentModuleHelper::actionLocale('Auth\ResetPasswordController@getToken'),
                 'redirectTime'     => 3000,
            ]);
        } catch (\Exception $e) {
            return $this->jsonAjaxSaveFeedback(false, [
                 'danger_message' => CurrentModuleHelper::trans('user.password.sending-problem'),
            ]);
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getToken(Request $request)
    {
        if (session()->has(self::$resetTokenSessionName) or ($request->haveCode == 'code')) {
            $nationalId = session()->get(self::$resetTokenSessionName);
            return $this->template('auth.passwords.token', compact('nationalId'));
        } else {
            return redirect(CurrentModuleHelper::actionLocale('Auth\ForgotPasswordController@showLinkRequestForm'));
        }
    }



    /**
     * @param PasswordTokenRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function checkToken(PasswordTokenRequest $request)
    {
        $user = user()->grab($request->code_melli, 'code_melli');

        if (!$user or !$user->exists) {
            return $this->abort(410);
        }

        $now = Carbon::now();
        $exp = Carbon::parse($user->reset_token_expire);

        if (Hash::check($request->password_reset_token, $user->reset_token)) {
            if ($now->lte($exp)) {
                session([self::$resetTokenSessionName => $user->code_melli]);

                return $this->jsonAjaxSaveFeedback(true, [
                     'success_message'  => CurrentModuleHelper::trans('user.password.token-verified'),
                     'success_redirect' => CurrentModuleHelper::actionLocale('Auth\ResetPasswordController@newPassword'),
                     'redirectTime'     => 3000,
                ]);
            } else {
                return $this->jsonAjaxSaveFeedback(false, [
                     'danger_message' => view(CurrentModuleHelper::bladePath('layouts.widgets.password-expired')),
                ]);
            }
        } else {
            return $this->jsonAjaxSaveFeedback(false, [
                 'danger_message' => CurrentModuleHelper::trans('user.password.token'),
            ]);
        }
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function newPassword()
    {
        if (session()->has(self::$resetTokenSessionName)) {
            return $this->template('auth.passwords.new', compact('nationalId'));
        } else {
            return redirect(CurrentModuleHelper::actionLocale('Auth\ForgotPasswordController@showLinkRequestForm'));
        }
    }



    /**
     * @param NewPasswordRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePassword(NewPasswordRequest $request)
    {
        if (
             session()->has(self::$resetTokenSessionName) and
             ($user = user()->grab(session()->get(self::$resetTokenSessionName), 'code_melli')) and
             $user->exists
        ) {
            $storeData = [
                 'id'       => $user->id,
                 'password' => Hash::make($request['new_password']),
            ];

            \Auth::loginUsingId($user->id);

            return $this->jsonAjaxSaveFeedback(model('User')->batchSaveId($storeData), [
                 'success_message'  => CurrentModuleHelper::trans('user.password.reset'),
                 'success_redirect' => route('home'),
                 'redirectTime'     => 3000,
            ]);
        }

        return $this->abort(410);
    }



    /**
     * @param Request $request
     *
     * @return null
     */
    protected function findUser(Request $request)
    {
        $user = User::where([
             'code_melli'   => $request->code_melli,
             $request->type => $request[$request->type],
        ])->first()
        ;

        if ($user and $user->exists) {
            return $user;
        }

        return null;
    }



    /**
     * @return int
     */
    protected function generateToken()
    {
        return rand(str_repeat(1, self::$resetTokenLength), str_repeat(9, self::$resetTokenLength));
    }



    /**
     * @param User $user
     * @param      $resetToken
     *
     * @return bool
     */
    protected function saveToken(User $user, $resetToken)
    {
        return model('User')->batchSaveId([
             'id'                 => $user->id,
             'reset_token'        => Hash::make($resetToken),
             'reset_token_expire' => Carbon::now()->addMinute($this->getTokenLifeTime())->toDateTimeString(),
        ]) ? true : false;
    }



    /**
     * @param User $user
     */
    protected function saveSession(User $user)
    {
        session()->put('resetingPasswordNationalId', $user->code_melli);
    }



    /**
     * @param Request $request
     * @param User    $user
     * @param         $resetToken
     *
     * @return bool|mixed
     */
    protected function sendToken(Request $request, User $user, $resetToken)
    {
        switch ($request->type) {
            case 'email':
                return $this->sendTokenByEmail($user, $resetToken);

            case 'mobile':
                return $this->sendTokenBySms($user, $resetToken);

            default:
                return false;
        }
    }



    /**
     * @param User $user
     * @param      $resetToken
     *
     * @return mixed
     */
    protected function sendTokenByEmail(User $user, $resetToken)
    {
        $mailContent = view(CurrentModuleHelper::bladePath('email.user-restore-password'), compact('resetToken'));

        return EmailServiceProvider::send(
             $mailContent,
             $user->email,
             get_setting('site_title'),
             CurrentModuleHelper::trans('user.password.restore'),
             'default_email'
        );
    }



    /**
     * @param User $user
     * @param      $resetToken
     *
     * @return mixed
     */
    protected function sendTokenBySms(User $user, $resetToken)
    {
        $smsText = CurrentModuleHelper::trans('user.password.restore-sms', [
             'token' => $resetToken,
             'site'  => setting()->ask('site_url')->gain(),
        ]);

        return sendSms($user->mobile, $smsText);
    }



    /**
     * @return int
     */
    protected function getTokenLifeTime()
    {
        return setting('password_token_expire_time')->gain() ?: self::$resetTokenLifetime;
    }
}
