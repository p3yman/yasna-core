<?php namespace Modules\Amirhossein\Http\Controllers\Auth;

use Modules\Amirhossein\Services\Controller\AmirhosseinFrontControllerTrait;
use Modules\Amirhossein\Services\Controller\ModuleControllerTrait;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Amirhossein\Services\Module\ModuleHandler;
use Modules\Yasna\Http\Controllers\Auth\ForgotPasswordController as YasnaForgotPasswordController;

class ForgotPasswordController extends YasnaForgotPasswordController
{
    use ModuleControllerTrait {
        __construct as protected moduleTraitConstruct;
    }
    use AmirhosseinFrontControllerTrait {
        __construct as protected frontTraitConstruct;
    }



    /**
     * ForgotPasswordController constructor.
     */
    public function __construct()
    {
        $this->moduleTraitConstruct();
        $this->frontTraitConstruct();
        parent::__construct();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showLinkRequestForm()
    {
        return $this->template('auth.passwords.contact');
    }
}
