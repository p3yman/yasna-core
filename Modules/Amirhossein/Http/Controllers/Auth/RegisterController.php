<?php namespace Modules\Amirhossein\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\Auth;
use Modules\Amirhossein\Http\Controllers\Traits\AmirhosseinFrontControllerTrait;
use Modules\Amirhossein\Services\Controller\AmirhosseinFrontControllerAbstract;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;
use Modules\Amirhossein\Services\Module\ModuleHandler;
use Modules\Yasna\Events\UserLoggedIn;
use Modules\Yasna\Http\Controllers\Auth\RegisterController as YasnaRegisterController;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\Amirhossein\Events\Registered;
use Modules\Amirhossein\Http\Requests\Auth\RegisterUserRequest;

class RegisterController extends AmirhosseinFrontControllerAbstract //extends YasnaRegisterController // <~~ TODO: Has conflicts in declaration of register.
{
    use RedirectsUsers;
    protected $register_blade = "auth.register";



    /**
     * RegisterController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
        parent::__construct();
        $this->register_blade = CurrentModuleHelper::bladePath($this->register_blade);
    }



    /**
     * @param RegisterUserRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterUserRequest $request)
    {
        event(new Registered($user = $this->createUser($request->all())));

        $this->guard()->login($user);

        if ($this->registered()) {
            event(new UserLoggedIn($user));
            return $this->jsonFeedback(null, [
                 'ok'       => 1,
                 'message'  => CurrentModuleHelper::trans('user.message.register-success'),
                 'redirect' => $this->redirectPath(),
            ]);
        } else {
            return $this->jsonFeedback(null, [
                 'ok'      => 0,
                 'message' => CurrentModuleHelper::trans('user.message.register-fail'),
            ]);
        }
    }



    /**
     * @param array $data
     *
     * @return null
     */
    protected function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);

        $storingData = array_only($data, ['name_first', 'name_last', 'code_melli', 'mobile', 'email', 'password']);

        $userId = model('User')->batchSaveId($storingData);
        if ($userId) {
            $this->registeredUser = User::find($userId);
            return $this->registeredUser;
        }

        return null;
    }



    /**
     * @return bool
     */
    protected function registered()
    {
        if ($this->registeredUser and $this->registeredUser->exists) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return string
     */
    protected function redirectTo()
    {
        return route('home');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        if (request()->has('redirect')) {
            switch (strtolower(request('redirect'))) {
                case 'referrer':
                    session(['url.intended' => url()->previous()]);
                    break;
            }

            session()->save();
        }
        return $this->template('auth.register');
    }



    /**
     * @return mixed
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
