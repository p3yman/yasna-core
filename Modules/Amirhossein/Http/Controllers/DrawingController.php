<?php

namespace Modules\Amirhossein\Http\Controllers;

use App\Models\Receipt;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Amirhossein\Http\Requests\DrawingCodeSubmitRequest;
use Modules\Amirhossein\Providers\DrawingCodeServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class DrawingController extends Controller
{
    protected static $tryingLimit = 100;

    public function submitCode(DrawingCodeSubmitRequest $request)
    {
        $code = $request->code;
        $response = [];
        $continue = true;
        $count = 1;

        if ($request->session()->get('drawing_try')) {
            $try = $request->session()->get('drawing_try');
            $count = $try['count'];

            if ($count > self::$tryingLimit) {
                $continue = false;
            }

            $response['status'] = 'fail';
            $response['msg'] = CurrentModuleHelper::trans('general.drawing.message.invalid-code');
            $response['abc'] = 1;
        } else {
            $request->session()->put(
                'drawing_try',
                [
                    'count' => $count,
                    'last' => Carbon::now()->toDateTimeString(),
                ]);
        }

        if (DrawingCodeServiceProvider::checkCode($code) && $continue) {
            $foundReceipt = Receipt::where('code', $code)->get();
            if ($foundReceipt and $foundReceipt->count()) {
                $response['status'] = 'fail';
                $response['msg'] = CurrentModuleHelper::trans('general.drawing.message.invalid-code');
                $response['abc'] = 2;

                $try = $request->session()->get('drawing_try');
                $try['count'] = $try['count'] + 1;
                $request->session()->put('drawing_try', $try);
            } else {
                $request->session()->put('drawingCode', encrypt($code));
                $response['status'] = 'success';
                $response['msg'] = CurrentModuleHelper::trans('general.drawing.message.register-code-success-please-wait');
                $response['login'] = user()->exists;
            }
        } else {
            $try = $request->session()->get('drawing_try');
            $try['count'] = $try['count'] + 1;
            $request->session()->put('drawing_try', $try);

            $response['status'] = 'fail';
            $response['msg'] = CurrentModuleHelper::trans('general.drawing.message.invalid-code');
            $response['abc'] = 3;
        }

        return \response()->json($response);
    }
}
