<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("sendSms")) {
    function sendSms(...$parameters)
    {
        return \Modules\Amirhossein\Services\AsanakSms\Facade\AsanakSms::send(...$parameters);
    }
}

if (!function_exists("urlHyphen")) {
    function urlHyphen($str)
    {
        return str_replace('+', '-', urlencode($str));
    }
}

