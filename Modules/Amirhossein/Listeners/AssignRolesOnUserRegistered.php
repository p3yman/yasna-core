<?php

namespace Modules\Amirhossein\Listeners;

use Modules\Amirhossein\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AssignRolesOnUserRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Amirhossein\Events\Registered $event
     *
     * @return void
     */
    public function handle(\Modules\Amirhossein\Events\Registered $event)
    {
        $user = $event->user;

        $user->attachRole('customer');
    }
}
