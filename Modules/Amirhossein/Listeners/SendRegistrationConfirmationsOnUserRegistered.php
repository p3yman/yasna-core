<?php

namespace Modules\Amirhossein\Listeners;

use App\Models\User;
use Modules\Amirhossein\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Amirhossein\Services\Email\EmailServiceProvider;
use Modules\Amirhossein\Services\Module\CurrentModuleHelper;

class SendRegistrationConfirmationsOnUserRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }



    /**
     * Handle the event.
     *
     * @param \Modules\Amirhossein\Events\Registered $event
     *
     * @return void
     */
    public function handle(\Modules\Amirhossein\Events\Registered $event)
    {
        $user = $event->user;

        $this->sendSms($user);
        $this->sendEmail($user);
    }



    /**
     * @param User $user
     *
     * @return mixed
     */
    protected function sendSms(User $user)
    {
        if (($haveToSend = setting('user_registration_send_sms')->gain()) and $user->mobile) {
            $smsText = CurrentModuleHelper::trans('user.message.register-success-sms', [
                 'name' => $user->full_name,
                 'site' => setting()->ask('site_url')->gain(),
            ]);

            return sendSms($user->mobile, $smsText);
        }
    }



    /**
     * @param User $user
     *
     * @return mixed
     */
    protected function sendEmail(User $user)
    {
        try {
            if (($haveToSend = setting('user_registration_send_email')->gain()) and $user->email) {
                $mailContent = view(CurrentModuleHelper::bladePath('email.user-registered'), compact('user'));
                return $sendingEmailResult = EmailServiceProvider::send(
                     $mailContent,
                     $user->email,
                     get_setting('site_title'),
                     CurrentModuleHelper::trans('user.register'),
                     'default_email'
                );
            }
        } catch (\Exception $e) {
            // If failed sending email
        }
    }
}
