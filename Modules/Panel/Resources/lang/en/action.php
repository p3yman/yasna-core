<?php
return [
     "all"     => "All Together",
     "actives" => "Actives",
     "bin"     => "Trashed",
     "trash"   => "Move to Bin",
     "restore" => "Restore from Bin",
     "destroy" => "Delete Forever",
];
