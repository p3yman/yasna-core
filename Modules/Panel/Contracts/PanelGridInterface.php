<?php

namespace Modules\Panel\Contracts;


interface PanelGridInterface
{
    /**
     * get main title of the grid.
     *
     * @return string
     */
    public function getHeaderTitle(): string;



    /**
     * get main icon of the grid.
     *
     * @return string
     */
    public function getHeaderIcon(): string;



    /**
     * get main description of the grid, appeared just below the header title.
     *
     * @return null|string
     */
    public function getHeaderDescription(): ?string;



    /**
     * identify if a search box is required to be displayed.
     *
     * @return bool
     */
    public function hasSearch(): bool;



    /**
     * run the definition of the columns
     *
     * @return void
     */
    public function columns(): void;



    /**
     * run the definition of the toolbar buttons
     *
     * @return void
     */
    public function buttons(): void;



    /**
     * run the tabs
     *
     * @return void
     */
    public function tabs(): void;



    /**
     * load the items inside the mass-actions button
     *
     * @return void
     */
    public function massActions(): void;
}
