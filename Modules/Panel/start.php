<?php

if (!function_exists("panel")) {
    /**
     * get an instance of the PanelHelper for easier use
     *
     * @return \Modules\Panel\Services\PanelHelper
     */
    function panel()
    {
        return (new \Modules\Panel\Services\PanelHelper());
    }
}
