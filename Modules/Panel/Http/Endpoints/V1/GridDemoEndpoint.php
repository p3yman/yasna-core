<?php

namespace Modules\Panel\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Panel\Http\Controllers\V1\DemoController;

/**
 * @api               {GET}
 *                    /api/modular/v1/panel-grid-demo
 *                    Grid Demo
 * @apiDescription    Create a grid demo, based on the states table.
 * @apiVersion        1.0.0
 * @apiName           Grid Demo
 * @apiGroup          Panel
 * @apiPermission     Developer
 * @apiSuccessExample Success-Response: TODO: This is the default save feedback. Change if your work is different
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(0),
 *      },
 *      "results": {
 *          "done" => "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method DemoController controller()
 */
class GridDemoEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Grid Demo";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Panel\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'DemoController@grid';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1,100)),
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
