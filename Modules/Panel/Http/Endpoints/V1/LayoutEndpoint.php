<?php

namespace Modules\Panel\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Panel\Http\Controllers\V1\GeneralController;

/**
 * @api               {GET}
 *                    /api/modular/v1/panel-layout
 *                    Admin Panel Layout
 * @apiDescription    get admin panel layout
 * @apiVersion        1.0.0
 * @apiName           Admin Panel Layout
 * @apiGroup          Panel
 * @apiPermission     Admin
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      },
 *      "results":  {
 *          "has_search": false,
 *          "sidebar": [],
 *          "navbar": {
 *              "left": [],
 *              "right": []
 *          },
 *          "footer": {
 *              "copyright": {
 *                  "icon": "",
 *                  "link": "",
 *                  "text": ""
 *              },
 *              "right": [],
 *              "middle": [],
 *              "left": []
 *          },
 *          "profile": {
 *              "avatar": "",
 *              "name": "",
 *              "items": []
 *          }
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method GeneralController controller()
 */
class LayoutEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Admin Panel Layout";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Panel\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'GeneralController@layout';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond(panel()->layout()->toArray(), []);
    }
}
