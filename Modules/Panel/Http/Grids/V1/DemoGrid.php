<?php

namespace Modules\Panel\Http\Grids\V1;

use Modules\Panel\Services\PanelGridLayout;
use Modules\Forms\Services\FormBinder;
use Modules\Yasna\Services\YasnaModel;

class DemoGrid extends PanelGridLayout
{
    /**
     * @inheritdoc
     */
    public function getHeaderTitle(): string
    {
        return trans("panel::grid.title");
    }



    /**
     * @inheritdoc
     */
    public function getHeaderIcon(): string
    {
        return "md-podcast";
    }



    /**
     * @inheritdoc
     */
    public function getHeaderDescription(): string
    {
        return trans("panel::grid.desc");
    }



    /**
     * @inheritdoc
     */
    public function hasSearch(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function columns(): void
    {
        $this->add('title', 1)
             ->title(trans("validation.attributes.title"))
             ->icon("md-bandage")
             ->sortable()
             ->method("titleColumn")
        ;

        $this->add("slug", 2)
             ->title(trans("validation.attributes.slug"))
             ->icon("md-dice-d20")
             ->method("slugColumn")
        ;

        $this->add("location", 4)
             ->title(trans("validation.attributes.birth_city"))
             ->icon("md-mushroom-outline")
             ->method("locationColumn")
        ;
    }



    /**
     * @inheritdoc
     */
    public function buttons(): void
    {
        // TODO: Take the below example and add your own toolbar buttons, or leave the method empty if you don't need.

        $this->add("new", 1)
             ->title(trans("content::types.create"))
             ->link()
        ;
    }



    /**
     * @inheritdoc
     */
    public function tabs(): void
    {
        // TODO: Take the below example and add your own tabs, or leave the method empty if you don't need.

        $this->add("actives", 1)
             ->title()
             ->title(trans("panel::action.actives"))
             ->icon("md-marker-check")
             ->active()
             ->link()
        ;

        $this->add("bin", 99)
             ->title()
             ->icon("md-trash-can")
             ->title(trans("panel::action.bin"))
             ->link()
        ;
    }



    /**
     * @inheritdoc
     */
    public function massActions(): void
    {
        // TODO: Take the below example and add your own mass actions, or leave the method empty if you don't need.

        $this->add("trash", 1)
             ->title()
             ->title(trans("panel::action.trash"))
             ->icon("")
             ->link()
        ;
    }



    /**
     * bind the title column of the grid
     *
     * @param YasnaModel $model
     * @param FormBinder $binder
     *
     * @return FormBinder
     */
    public function titleColumn($model, $binder): FormBinder
    {
        $binder->add("title")->text($model->title);

        $binder->add("paragraph")->text(dummy()::persianWord(40));

        return $binder;
    }



    /**
     * bind the slug column of the grid
     *
     * @param YasnaModel $model
     * @param FormBinder $binder
     *
     * @return FormBinder
     */
    public function slugColumn($model, $binder): FormBinder
    {
        $binder->add("title")->text($model->slug);

        return $binder;
    }



    /**
     * bind the location column of the grid
     *
     * @param YasnaModel $model
     * @param FormBinder $binder
     *
     * @return FormBinder
     */
    public function locationColumn($model, $binder): FormBinder
    {
        $binder->add("map")->centerLat($model->getMeta('latitude'), $model->getMeta('longitude'));

        return $binder;
    }
}
