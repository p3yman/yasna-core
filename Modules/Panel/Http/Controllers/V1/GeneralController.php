<?php

namespace Modules\Panel\Http\Controllers\V1;

use Modules\Yasna\Services\YasnaApiController;

class GeneralController extends YasnaApiController
{
    /**
     * get admin panel layout
     *
     * @return array
     */
    public function layout()
    {
        return $this->success(
             panel()->layout()->collect()->toArray()
        );
    }
}
