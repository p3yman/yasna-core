<?php

namespace Modules\Panel\Http\Controllers\V1;

use Modules\Panel\Http\Grids\V1\DemoGrid;
use Modules\Yasna\Services\V4\Request\SimpleYasnaListRequest;
use Modules\Yasna\Services\YasnaApiController;

class DemoController extends YasnaApiController
{
    /**
     * get a sample demo grid, out of `states` table, only in debug mode, for developers.
     *
     * @param SimpleYasnaListRequest $request
     *
     * @return array
     */
    public function grid(SimpleYasnaListRequest $request)
    {
        $builder = model('state')->newBuilderInstance();

        return (new DemoGrid($builder, $request))->toArray();
    }
}
