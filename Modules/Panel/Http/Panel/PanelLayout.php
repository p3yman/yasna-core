<?php

namespace Modules\Panel\Http\Panel;

use Modules\Panel\Services\PanelMasterLayout;

/**
 * class PanelLayout
 * define the admin panel layout items, related to the Panel module
 */
class PanelLayout extends PanelMasterLayout
{
    /**
     * @inheritdoc
     */
    public function index()
    {
        $this->setCopyrightIcon("md-copyright");
        $this->setCopyrightText(trans("panel::layout.copyright_text"));
        $this->setCopyrightLink(trans("panel::layout.copyright_link"));
        $this->setProfileName(user()->full_name);

        $this->addSidebarItem('dashboard', 1)
             ->icon('md-home')
        ;

        $this->setBrandName(get_setting("site_title"));

        $this->addProfileMenuItem("logout", 9999)
             ->icon("md-logout")
             ->link(component("link")->name("logout")->title(trans("panel::layout.logout"))) //TODO: Check the link name
        ;

        //TODO: Remove the below lines, when the sample is taken.
        if (debugMode()) {
            $this->sampleThings();
        }
    }



    /**
     * add sample things to the dashboard
     * TODO: To be removed when samples are taken!
     */
    private function sampleThings()
    {
        $child1 = component("panel-item")->id('foan');
        $child2 = component("textarea")->name("taghi");

        $this->addSidebarItem('dashboard2', 5)
             ->icon('md-home')
             ->children([$child1, $child2])
        ;

        $this->addSidebarHeading('jafar', 2);
    }
}
