<?php

namespace Modules\Panel\Exceptions;

use Exception;

class PanelItemAlreadyExistsException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     */
    public function __construct(string $id)
    {
        parent::__construct("Panel item `$id` already exists. You can modify or remove it, but cannot override with add method.");
    }
}
