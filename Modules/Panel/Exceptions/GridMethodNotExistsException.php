<?php

namespace Modules\Panel\Exceptions;

use Exception;

class GridMethodNotExistsException extends Exception
{

    /**
     * GridMethodNotExistsException constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        parent::__construct("Could not resolve the method `$name` to form the grid.");
    }
}
