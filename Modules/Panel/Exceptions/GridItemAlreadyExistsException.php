<?php

namespace Modules\Panel\Exceptions;

use Exception;

class GridItemAlreadyExistsException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     */
    public function __construct(string $id)
    {
        parent::__construct("Grid item `$id` already exists. You can modify or remove it, but cannot override with add method.");
    }
}
