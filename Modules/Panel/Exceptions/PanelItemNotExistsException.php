<?php

namespace Modules\Panel\Exceptions;

use Exception;

class PanelItemNotExistsException extends Exception
{

    /**
     * ComponentNotDefinedException constructor.
     *
     * @param string $name
     */
    public function __construct(string $id)
    {
        parent::__construct("Panel item `$id` not exists. You cannot modify or remove missing thing.");
    }
}
