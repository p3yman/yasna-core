<?php

namespace Modules\Panel\Console;


use Modules\Yasna\Console\YasnaMakerCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakePanelLayoutCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-panel-layout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a panel layout class, with the prefixed name.';



    /**
     * @inheritDoc
     */
    protected function purifier()
    {
        $this->module_name = $this->argument('module_name');

        if (!$this->module_name) {
            $this->error('The module name is missed.');
            die();
        }

        $this->module_name = studly_case($this->module_name);
        $this->module      = module($this->module_name);

        if ($this->module->isNotValid()) {
            $this->error("Invalid module name.");
            die();
        }
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE" => $this->module_name,
        ];
    }



    /**
     * get the target folder os path
     *
     * @return string
     */
    protected function getFolderPath()
    {
        return $this->module->getPath("Http" . DIRECTORY_SEPARATOR . "Panel");
    }



    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
             ['module_name', InputArgument::REQUIRED, 'Module Name'],
        ];
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        return "PanelLayout";
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "layout.stub";
    }
}
