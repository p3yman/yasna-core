<?php

namespace Modules\Panel\Console;

use Modules\Yasna\Console\YasnaMakerCommand;

class MakeGridCommand extends YasnaMakerCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'yasna:make-grid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a grid class.';

    /**
     * keep the value of the desired version
     *
     * @var int
     */
    protected $version;



    /**
     * @inheritDoc
     */
    public function customHandle()
    {
        $this->askVersion();
    }



    /**
     * discover a suitable title
     *
     * @return string
     */
    protected function discoverSuitableTitle()
    {
        $kebab = kebab_case(str_before($this->class_name, "Grid"));

        return title_case(str_replace("-", " ", $kebab));
    }



    /**
     * Ask the requested version via a dialogue
     *
     * @return void
     */
    protected function askVersion()
    {
        $suggest = $this->discoverCurrentVersion();
        $version = $this->ask("Desired Version (Probably $suggest)?");

        if (is_null($version)) {
            $version = $suggest;
        }

        while (!is_numeric($version) or $version < 1) {
            $version = $this->ask("Something greater than 1 please.");
            if (is_null($version)) {
                $version = $suggest;
            }
        }

        $this->version = $version;
    }



    /**
     * discover a possible version based on the file system
     *
     * @return int
     */
    protected function discoverCurrentVersion(): int
    {
        $parent_path = $this->getFolderPath();
        if (!file_exists(($parent_path))) {
            return 1;
        }

        $subs = scandir($parent_path);
        $max  = 0;
        foreach ($subs as $sub) {
            $version = (int)str_after($sub, "V");
            $max     = max($version, $max);
        }

        return $max;
    }



    /**
     * get stub replacement
     *
     * @return array
     */
    protected function getReplacement()
    {
        return [
             "MODULE"  => $this->module_name,
             "CLASS"   => $this->class_name,
             "VERSION" => "V" . $this->version,
        ];
    }



    /**
     * get the target folder os path
     *
     * @return string
     */
    protected function getFolderPath()
    {
        $parent_path = $this->module->getPath("Http" . DIRECTORY_SEPARATOR . "Grids");
        if ($this->version != null) {
            return $parent_path . DIRECTORY_SEPARATOR . "V" . $this->version;
        }
        return $parent_path;
    }



    /**
     * define the file name
     *
     * @return string
     */
    protected function getFileName()
    {
        $file_name = studly_case($this->argument("class_name"));
        if (!str_contains($file_name, 'Grid')) {
            return $file_name . "Grid";
        }
        return $file_name;
    }



    /**
     * get stub file name
     *
     * @return  string
     */
    protected function getStubName()
    {
        return "grid.stub";
    }
}
