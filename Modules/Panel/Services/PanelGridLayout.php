<?php

namespace Modules\Panel\Services;

use Illuminate\Database\Eloquent\Builder;
use Modules\Panel\Contracts\PanelGridInterface;
use Modules\Panel\Services\Traits\GridDataTrait;
use Modules\Panel\Services\Traits\GridDefaultsTrait;
use Modules\Panel\Services\Traits\GridMagicsTrait;
use Modules\Panel\Services\Traits\GridOutputTrait;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Modules\Yasna\Services\V4\Request\YasnaListRequest;

/**
 * Class PanelGridLayout
 */
abstract class PanelGridLayout implements PanelGridInterface
{
    use GridOutputTrait;
    use GridDataTrait;
    use GridDefaultsTrait;
    use GridMagicsTrait;
    use ModuleRecognitionsTrait;

    const HANDLE_SELECTOR = "selector";
    const HANDLE_COUNTER  = "counter";

    /**
     * keep the definition of the columns.
     *
     * @var array
     */
    protected static $columns = [];

    /**
     * keep the list of buttons (or any other component) appear on the toolbar area.
     *
     * @var array
     */
    protected static $buttons = [];

    /**
     * keep the list of tab items.
     *
     * @var array
     */
    protected static $tabs = [];

    /**
     * keep the list of mass actions
     *
     * @var array
     */
    protected static $mass_actions = [];

    /**
     * keep the eloquent builder instance of the grid data.
     *
     * @var Builder
     */
    protected $builder;

    /**
     * keep the request instance, in which the data is requested through.
     *
     * @var YasnaListRequest
     */

    protected $request;

    /**
     * keep the paginated collection of the data
     *
     * @var \Illuminate\Pagination\LengthAwarePaginator
     */
    protected $paginator;



    /**
     * PanelGridLayout constructor.
     *
     * @param Builder          $builder
     * @param YasnaListRequest $request
     */
    public function __construct(Builder $builder, YasnaListRequest $request)
    {
        $this->builder   = $builder;
        $this->request   = $request;
        $this->paginator = $this->getPaginator();

        $this->boot();
    }



    /**
     * run custom boot procedures
     *
     * @return void
     */
    protected function boot()
    {
        // override this method to add things to the constructor layer of the grid class.
    }
}
