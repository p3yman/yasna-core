<?php

namespace Modules\Panel\Services\Traits;

use Modules\Forms\Components\HeadingComponent;
use Modules\Forms\Services\FormComponent;
use Modules\Panel\Exceptions\PanelItemAlreadyExistsException;
use Modules\Panel\Exceptions\PanelItemNotExistsException;

/**
 * @method void setCopyrightIcon(string $icon)
 * @method void setCopyrightText(string $text)
 * @method void setCopyrightLink(string $link)
 * @method void setProfileName(string $name)
 * @method void setProfileAvatar(string $avatar)
 * @method void setHasSearch(bool $state)
 * @method void setBrandName(string $name)
 * @method void setBrandLogo(string $logo)
 * @method FormComponent addSidebarItem(string $id, int $order)
 * @method FormComponent addSidebarDivider(string $id, int $order)
 * @method FormComponent addSidebarHeading(string $id, int $order)
 * @method FormComponent editSidebarItem(string $id)
 * @method void removeSidebarItem(string $id)
 * @method FormComponent addLeftNavbarItem(string $id, int $order)
 * @method FormComponent addLeftNavbarDivider(string $id, int $order)
 * @method FormComponent addLeftNavbarHeading(string $id, int $order)
 * @method FormComponent editLeftNavbarItem(string $id)
 * @method void removeLeftNavbarItem(string $id)
 * @method FormComponent addRightNavbarItem(string $id, int $order)
 * @method FormComponent addRightNavbarDivider(string $id, int $order)
 * @method FormComponent addRightNavbarHeading(string $id, int $order)
 * @method FormComponent editRightNavbarItem(string $id)
 * @method void removeRightNavbarItem(string $id)
 * @method FormComponent addProfileMenuItem(string $id, int $order)
 * @method FormComponent addProfileMenuDivider(string $id, int $order)
 * @method FormComponent addProfileMenuHeading(string $id, int $order)
 * @method FormComponent editProfileMenuItem(string $id)
 * @method void removeProfileMenuItem(string $id)
 */
trait LayoutMagicsTrait
{
    /**
     * general setters
     *
     * @param string $name
     * @param array  $args
     *
     * @return mixed
     */
    public function __call($name, $args)
    {
        $name_array = explode("-", kebab_case($name));
        $prefix     = $name_array[0];
        $postfix    = array_last($name_array);

        if ($prefix == "set") {
            $this->setItem(snake_case(str_after($name, $prefix)), $args[0]);
            return null;
        }

        if (!in_array($prefix, ['add', 'edit', 'remove'])) {
            return null;
        }

        $property = snake_case(str_after(str_before($name, studly_case($postfix)), $prefix));
        $method   = camel_case("$prefix-Array-$postfix");

        return $this->$method($property, $args[0], isset($args[1]) ? $args[1] : 0);
    }



    /**
     * set item
     *
     * @param string $property
     * @param  mixed $value
     *
     * @return void
     */
    private function setItem(string $property, $value)
    {
        static::${$property} = $value;
    }



    /**
     * add array item
     *
     * @param string $property
     * @param string $id
     * @param int    $order
     *
     * @return FormComponent
     * @throws PanelItemAlreadyExistsException
     */
    private function addArrayItem(string $property, string $id, int $order)
    {
        if (isset(static::${$property}[$id])) {
            throw new PanelItemAlreadyExistsException("$property:$id");
        }

        static::${$property}[$id] = component("panel-item")->id($id)->order($order);

        return static::${$property}[$id];
    }



    /**
     * add array divider
     *
     * @param string $property
     * @param string $id
     * @param int    $order
     *
     * @return null
     * @throws PanelItemAlreadyExistsException
     */
    private function addArrayDivider(string $property, string $id, int $order)
    {
        if (isset(static::${$property}[$id])) {
            throw new PanelItemAlreadyExistsException("$property:$id");
        }

        static::${$property}[$id] = component("divider")->id($id)->order($order);

        return static::${$property}[$id];
    }



    /**
     * add array heading
     *
     * @param string $property
     * @param string $id
     * @param int    $order
     *
     * @return HeadingComponent
     * @throws PanelItemAlreadyExistsException
     */
    private function addArrayHeading(string $property, string $id, int $order)
    {
        if (isset(static::${$property}[$id])) {
            throw new PanelItemAlreadyExistsException("$property:$id");
        }

        static::${$property}[$id] = component("panel-heading")->id($id)->order($order);

        return static::${$property}[$id];
    }



    /**
     * edit sidebar item
     *
     * @param string $property
     * @param string $id
     *
     * @return FormComponent
     * @throws PanelItemNotExistsException
     */
    private function editArrayItem(string $property, string $id)
    {
        if (!isset(static::${$property}[$id])) {
            throw new PanelItemNotExistsException("$property:$id");
        }

        return static::${$property}[$id];
    }



    /**
     * remove sidebar item
     *
     * @param string $property
     * @param string $id
     *
     * @return null
     * @throws PanelItemNotExistsException
     */
    private function removeArrayItem(string $property, string $id)
    {
        if (!isset(static::${$property}[$id])) {
            throw new PanelItemNotExistsException("$property:$id");
        }

        unset(static::${$property}[$id]);

        return null;
    }
}
