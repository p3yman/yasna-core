<?php

namespace Modules\Panel\Services\Traits;

use Modules\Forms\Services\FormBinder;
use Modules\Forms\Services\FormComponent;
use Modules\Panel\Exceptions\GridMethodNotExistsException;
use Modules\Yasna\Services\GeneralTraits\ClassRecognitionsTrait;
use Modules\Yasna\Services\YasnaModel;

trait GridOutputTrait
{
    use ClassRecognitionsTrait;



    /**
     * get the array representation of the grid, to be used in the front-end.
     *
     * @return array
     */
    public function toArray()
    {
        $this->renderColumns();

        return api()
             ->inModule($this->runningModuleName())
             ->successArray($this->getResultsArray(), $this->getMetadataArray())
             ;
    }



    /**
     * get the array to be appeared in the metadata area of white-house standard.
     *
     * @return array
     */
    private function getMetadataArray(): array
    {
        $paginator = $this->paginator;

        return [
             'total'             => $paginator->total(),
             'count'             => $paginator->count(),
             'per_page'          => $paginator->perPage(),
             'last_page'         => $paginator->lastPage(),
             'current_page'      => $paginator->currentPage(),
             'next_page_url'     => $paginator->nextPageUrl(),
             'previous_page_url' => $paginator->previousPageUrl(),
             "grid"              => $this->getGridTemplateArray(),
        ];
    }



    /**
     * get the grid template array
     *
     * @return array
     */
    private function getGridTemplateArray(): array
    {
        return [
             'header' => [
                  "title"        => [
                       "label"       => $this->getHeaderTitle(),
                       "icon"        => $this->getHeaderIcon(),
                       "description" => $this->getHeaderDescription(),
                  ],
                  "search"       => $this->hasSearch(),
                  "buttons"      => $this->getButtons(),
                  "mass_actions" => $this->getMassActions(),
                  "filters"      => [],
                  "tabs"         => $this->getTabs(),
                  "row_selector" => $this->hasRowSelector(),
             ],
             'table'  => [
                  "handle"  => $this->getHandleType(),
                  'columns' => $this->getHeadingColumns(),
             ],
             'footer' => [],
        ];
    }



    /**
     * get the array to be appeared in the result area of white-house standard.
     *
     * @return array
     */
    private function getResultsArray(): array
    {
        $array = [];

        foreach ($this->paginator as $model) {
            $array[$model->hashid] = $this->getRowColumns($model);
        }

        return $array;
    }



    /**
     * get row columns
     *
     * @param YasnaModel $model
     *
     * @return array
     */
    private function getRowColumns($model): array
    {
        $array = [];
        foreach (static::$columns as $column) {
            $array[] = $this->getCellContent($model, $column['method'], $column['in_class'])->toArray();
        }

        return $array;
    }



    /**
     * get the content of one cell
     *
     * @param YasnaModel $model
     * @param string     $method
     * @param string     $class
     *
     * @return FormBinder
     */
    private function getCellContent($model, $method, $class): FormBinder
    {
        if (empty($class)) {
            $form = $this->getCellContentFromNativeMethod($model, $method);
        } else {
            $form = $this->getCellContentFromNonnativeMethod($model, $method, $class);
        }

        return $form;
    }



    /**
     * get the content of one cell, from a native method
     *
     * @param YasnaModel $model
     * @param string     $method
     *
     * @return FormBinder
     * @throws GridMethodNotExistsException
     */
    private function getCellContentFromNativeMethod($model, $method): FormBinder
    {
        $binder = new FormBinder();

        if ($this->hasNotMethod($method)) {
            throw new GridMethodNotExistsException($method);
        }

        return $this->$method($model, $binder);
    }



    /**
     * get the content of one cell, from a nonnative method
     *
     * @param YasnaModel $model
     * @param string     $method
     * @param string     $class
     *
     * @return FormBinder
     * @throws GridMethodNotExistsException
     */
    private function getCellContentFromNonnativeMethod($model, $method, $class)
    {
        $binder = new FormBinder();
        $fq     = "$class::$method";

        if (!method_exists($class, $method)) {
            throw new GridMethodNotExistsException($fq);
        }

        return $fq($model, $binder);
    }



    /**
     * render columns
     *
     * @return void
     */
    private function renderColumns()
    {
        $this->columns();

        /**
         * @var string        $key
         * @var FormComponent $item
         */
        foreach (static::$columns as $key => $item) {
            static::$columns[$key] = $item->toArray();
        }

        static::$columns = array_sort(static::$columns, function ($item) {
            return $item['order'];
        });
    }



    /**
     * get heading columns
     *
     * @return array
     */
    private function getHeadingColumns(): array
    {
        return array_map(function ($item) {
            unset($item['method']);
            unset($item['in_class']);
            return $item;
        }, static::$columns);
    }



    /**
     * get buttons
     *
     * @return array
     */
    private function getButtons(): array
    {
        $this->buttons();

        foreach (static::$buttons as $key => $item) {
            static::$buttons[$key] = $item->toArray();
        }

        static::$buttons = array_sort(static::$buttons, function ($item) {
            return $item['order'];
        });

        return static::$buttons;
    }



    /**
     * get buttons
     *
     * @return array
     */
    private function getTabs(): array
    {
        $this->tabs();

        foreach (static::$tabs as $key => $item) {
            static::$tabs[$key] = $item->toArray();
        }

        static::$tabs = array_sort(static::$tabs, function ($item) {
            return $item['order'];
        });

        return static::$tabs;
    }



    /**
     * get buttons
     *
     * @return array
     */
    private function getMassActions(): array
    {
        $this->massActions();

        foreach (static::$mass_actions as $key => $item) {
            static::$mass_actions[$key] = $item->toArray();
        }

        static::$mass_actions = array_sort(static::$mass_actions, function ($item) {
            return $item['order'];
        });

        return static::$mass_actions;
    }



    /**
     * identify if the grid should has row selector (depending on the registered mass actions)
     *
     * @return bool
     */
    private function hasRowSelector()
    {
        return (bool)count(static::$mass_actions);
    }
}
