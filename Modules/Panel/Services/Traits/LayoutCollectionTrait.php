<?php

namespace Modules\Panel\Services\Traits;

use Modules\Forms\Services\FormComponent;
use Modules\Panel\Services\PanelMasterLayout;

trait LayoutCollectionTrait
{
    private static $modular_layouts = [];



    /**
     * register module-based layouts
     *
     * @param string $class
     *
     * @return void
     */
    final public function register(string $class): void
    {
        static::$modular_layouts[] = $class;
    }



    /**
     * collect layout things from all the registered modules
     *
     * @return $this
     */
    final public function collect()
    {
        /** @var PanelMasterLayout $instance */
        foreach ($this->layoutInstances() as $instance) {
            $instance->index();
        }

        $this->convertArrays();


        return $this;
    }



    /**
     * convert arrays
     *
     * @return void
     */
    private function convertArrays()
    {
        $properties = ["sidebar", "left_navbar", "right_navbar", "profile_menu",];

        foreach ($properties as $property) {

            /**
             * @var string        $key
             * @var FormComponent $item
             */
            foreach (static::${$property} as $key => $item) {
                static::${$property}[$key] = $item->toArray();
            }
        }
    }



    /**
     * get layout instances
     *
     * @return array
     */
    private function layoutInstances()
    {
        $array = [];

        foreach (static::$modular_layouts as $layout) {
            $array[] = new $layout();
        }

        return $array;
    }
}
