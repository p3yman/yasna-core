<?php

namespace Modules\Panel\Services\Traits;

trait GridDefaultsTrait
{
    /**
     * @inheritdoc
     */
    public function getHeaderIcon(): string
    {
        return "md-table";
    }



    /**
     * @inheritdoc
     */
    public function getHeaderDescription(): ?string
    {
        return null;
    }



    /**
     * @inheritdoc
     */
    public function hasSearch(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getHandleType(): string
    {
        return static::HANDLE_COUNTER;
    }
}
