<?php

namespace Modules\Panel\Services\Traits;

use Modules\Forms\Services\FormComponent;
use Modules\Panel\Exceptions\GridItemAlreadyExistsException;
use Modules\Panel\Exceptions\GridItemNotExistsException;

/**
 * @method static FormComponent addColumn(string $id, int $order)
 * @method static FormComponent editColumn(string $id)
 * @method static void removeColumn(string $id)
 * @method static FormComponent addButton(string $id, int $order)
 * @method static FormComponent editButton(string $id)
 * @method static void removeButton(string $id)
 * @method static FormComponent addMassAction(string $id, int $order)
 * @method static FormComponent editMassAction(string $id)
 * @method static void removeMassAction(string $id)
 */
trait GridMagicsTrait
{
    /**
     * general setters
     *
     * @param string $name
     * @param array  $args
     *
     * @return mixed
     */
    public function __call($name, $args)
    {
        return static::__callStatic($name, $args);
    }



    /**
     * general setters
     *
     * @param string $name
     * @param array  $args
     *
     * @return mixed
     */
    public static function __callStatic($name, $args)
    {
        $name_array = explode("-", kebab_case($name));
        $prefix     = $name_array[0];

        if (!in_array($prefix, ['add', 'edit', 'remove'])) {
            return null;
        }

        $property = str_plural(snake_case(str_after($name, $prefix)));
        $method   = camel_case("$prefix-Array-item");

        return static::$method($property, $args[0], isset($args[1]) ? $args[1] : 0);
    }



    /**
     * add item to the stack, automatically got from the called method
     *
     * @param string $id
     * @param int    $order
     *
     * @return FormComponent
     */
    public function add($id, int $order)
    {
        try {
            $item = studly_case(str_singular(debug_backtrace(1)[1]['function']));
        } catch (\Exception $e) {
            return component("text"); //<~~ Appears nowhere!
        }

        $method = "add" . $item;

        return static::$method($id, $order);
    }



    /**
     * add array item
     *
     * @param string $property
     * @param string $id
     * @param int    $order
     *
     * @return FormComponent
     * @throws GridItemAlreadyExistsException
     */
    protected static function addArrayItem(string $property, string $id, int $order)
    {
        if (isset(static::${$property}[$id])) {
            throw new GridItemAlreadyExistsException("$property:$id");
        }

        $component                = kebab_case(camel_case("grid-" . str_singular($property)));
        static::${$property}[$id] = component($component)->id($id)->order($order);

        return static::${$property}[$id];
    }



    /**
     * edit array item
     *
     * @param string $property
     * @param string $id
     *
     * @return FormComponent
     * @throws GridItemNotExistsException
     */
    protected static function editArrayItem(string $property, string $id)
    {
        if (!isset(static::${$property}[$id])) {
            throw new GridItemNotExistsException("$property:$id");
        }

        return static::${$property}[$id];
    }



    /**
     * remove array item
     *
     * @param string $property
     * @param string $id
     *
     * @return null
     * @throws GridItemNotExistsException
     */
    protected static function removeArrayItem(string $property, string $id)
    {
        if (!isset(static::${$property}[$id])) {
            throw new GridItemNotExistsException("$property:$id");
        }

        unset(static::${$property}[$id]);

        return null;
    }
}
