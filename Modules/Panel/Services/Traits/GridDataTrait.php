<?php

namespace Modules\Panel\Services\Traits;

use Illuminate\Pagination\LengthAwarePaginator;

trait GridDataTrait
{
    /**
     * @return LengthAwarePaginator $paginator
     */
    protected function getPaginator()
    {
        return $this->builder->paginate($this->request->perPage());
    }
}
