<?php

namespace Modules\Panel\Services;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Panel\Services\Traits\LayoutCollectionTrait;
use Modules\Panel\Services\Traits\LayoutMagicsTrait;

/**
 * Class PanelMasterLayout
 */
class PanelMasterLayout implements Arrayable
{
    use LayoutCollectionTrait;
    use LayoutMagicsTrait;

    /** @var array */
    protected static $sidebar = [];

    /** @var array */
    protected static $left_navbar = [];

    /** @var array */
    protected static $right_navbar = [];

    /** @var array */
    protected static $profile_menu = [];

    /** @var string */
    protected static $brand_logo;

    /** @var string */
    protected static $brand_name;

    /** @var string */
    protected static $profile_avatar;

    /** @var string */
    protected static $profile_name;

    /** @var string */
    protected static $copyright_icon;

    /** @var string */
    protected static $copyright_link;

    /** @var string */
    protected static $copyright_text;

    /** @var bool */
    protected static $has_search = true;



    /**
     * contain an index of the things to be executed.
     *
     * @return void
     */
    public function index()
    {
        // override this method to add your own details.
    }



    /**
     * convert the frame to a nested array, suitable to be used in the front-end
     *
     * @return array
     */
    final public function toArray()
    {
        return [
             "has_search" => static::$has_search,
             "sidebar"    => static::$sidebar,
             "navbar"     => [
                  "brand" => [
                       "name" => static::$brand_name,
                       "logo"  => static::$brand_logo,
                  ],
                  "left"  => static::$left_navbar,
                  "right" => static::$right_navbar,
             ],
             "footer"     => [
                  "copyright" => [
                       "text" => static::$copyright_text,
                       "link" => static::$copyright_link,
                       "icon" => static::$copyright_icon,
                  ],
                  "right"     => [],
                  "middle"    => [],
                  "left"      => [],
             ],
             "profile"    => [
                  "avatar"   => static::$profile_avatar,
                  "name"     => static::$profile_name,
                  "children" => static::$profile_menu,
             ],
        ];
    }
}
