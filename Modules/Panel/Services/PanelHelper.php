<?php

namespace Modules\Panel\Services;

/**
 * Class PanelHelper
 */
class PanelHelper
{
    /**
     * get an instance of the panel master layout
     *
     * @return PanelMasterLayout
     */
    public function layout()
    {
        return (new PanelMasterLayout());
    }
}
