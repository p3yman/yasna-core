<?php

namespace Modules\Panel\Components;

use Modules\Forms\Services\FormComponent;

class PanelHeadingComponent extends FormComponent
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "id"    => "",
             "type"  => "heading",
             "title" => "",

        ];
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
             "id",
        ];
    }

}
