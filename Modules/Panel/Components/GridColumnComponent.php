<?php

namespace Modules\Panel\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class GridColumnComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "title" => "",
             "icon"  => "",
             "sortable" => false,
             "method"   => "",
             "in_class" => null,
        ]);

    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
             "id",
             "title",
             "method",
        ];
    }
}
