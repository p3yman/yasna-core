<?php

namespace Modules\Panel\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class GridTabComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "title"  => "",
             "color"  => "",
             "active" => false,
             "link"   => "",
             "icon"   => "",
             "badge"  => "",
        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
             "id",
             'link',
        ];
    }
}
