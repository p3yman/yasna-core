<?php

namespace Modules\Panel\Components;

use Modules\Forms\Services\FormComponent;

class PanelItemComponent extends FormComponent
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "id"       => "",
             "type"     => "item",
             "icon"     => "",
             "link"     => "",
             "badge"    => "",
             "children" => [],
        ];
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
             "id",
        ];
    }



    /**
     * @inheritdoc
     */
    public function hasChildren()
    {
        return true;
    }

}
