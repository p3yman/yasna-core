<?php

namespace Modules\Panel\Components;

use Modules\Forms\Services\BasicComponents\ComponentAbstract;

class GridMassActionComponent extends ComponentAbstract
{
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
             "title" => "",
             "icon"  => "",
             "link"  => "",
             "color" => "",
        ]);
    }



    /**
     * @inheritdoc
     */
    public function requiredAttributes()
    {
        return [
             "id",
             "title",
        ];
    }
}
