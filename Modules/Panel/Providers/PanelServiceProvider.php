<?php

namespace Modules\Panel\Providers;

use Modules\Panel\Console\MakeGridCommand;
use Modules\Panel\Components\GridButtonComponent;
use Modules\Panel\Components\GridColumnComponent;
use Modules\Panel\Components\GridMassActionComponent;
use Modules\Panel\Components\GridTabComponent;
use Modules\Panel\Components\PanelHeadingComponent;
use Modules\Panel\Components\PanelItemComponent;
use Modules\Panel\Console\MakePanelLayoutCommand;
use Modules\Panel\Http\Endpoints\V1\GridDemoEndpoint;
use Modules\Panel\Http\Endpoints\V1\LayoutEndpoint;
use Modules\Panel\Http\Panel\PanelLayout;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class PanelServiceProvider
 *
 * @package Modules\Panel\Providers
 */
class PanelServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndpoints();
        $this->registerArtisans();
        $this->registerPanelLayout();
        $this->registerComponents();
    }



    /**
     * register Endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        endpoint()->register(LayoutEndpoint::class);

        if(debugMode()) {
            endpoint()->register(GridDemoEndpoint::class);
        }
    }



    /**
     * register Artisans
     *
     * @return void
     */
    private function registerArtisans()
    {
        $this->addArtisan(MakePanelLayoutCommand::class);
        $this->addArtisan(MakeGridCommand::class);
    }



    /**
     * register PanelLayout
     *
     * @return void
     */
    private function registerPanelLayout()
    {
        panel()->layout()->register(PanelLayout::class);
    }



    /**
     * register Components
     *
     * @return void
     */
    private function registerComponents()
    {
        $this->registerLayoutComponents();
        $this->registerGridComponents();
    }



    /**
     * register LayoutComponents
     *
     * @return void
     */
    private function registerLayoutComponents()
    {
        component()::register("panel-item", PanelItemComponent::class);
        component()::register("panel-heading", PanelHeadingComponent::class);
    }



    /**
     * register GridComponents
     *
     * @return void
     */
    private function registerGridComponents()
    {
        component()::register("grid-column", GridColumnComponent::class);
        component()::register("grid-button", GridButtonComponent::class);
        component()::register("grid-tab", GridTabComponent::class);
        component()::register("grid-mass-action", GridMassActionComponent::class);
    }

}
