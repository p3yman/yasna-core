<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/8/18
 * Time: 12:49 PM
 */

namespace Modules\Workdays\services;


use Carbon\Carbon;

class WorkdayUtility
{

    /**
     * check $carbon is exists in workdays
     *
     * @param   string $date
     * @param int      $organization
     *
     * @return boolean
     */
    public function isDefined($date, $organization = 0)
    {
        $carbon = Carbon::parse($date);
        return model('organization', $organization)
             ->workdays()
             ->where('date', $carbon->toDateString())
             ->exists()
             ;

    }



    /**
     *  inverse of isDefined method
     *
     * @param  string $date
     * @param int     $organization
     *
     * @return boolean
     */
    public function isNotDefined($date, $organization = 0)
    {
        $carbon = Carbon::parse($date);
        return !$this->isDefined($carbon, $organization);
    }



    /**
     * check $carbon is open in workdays (inverse)
     *
     * @param  string $date
     * @param int     $organization
     *
     * @return bool
     */
    public function isOpen($date, $organization = 0)
    {
        $carbon = Carbon::parse($date);
        if ($this->isDefined($carbon, $organization)) {
            return model('organization', $organization)
                 ->workdays()
                 ->where('date', $carbon->toDateString())
                 ->where('open', "1")
                 ->exists()
                 ;
        }

        return false;
    }



    /**
     * inverse of isOpen method
     *
     * @param  string $date
     * @param int     $organization
     *
     * @return bool
     */
    public function isClosed($date, $organization = 0)
    {
        $carbon = Carbon::parse($date);
        if ($this->isNotDefined($carbon, $organization)) {
            return false;
        }
        return !$this->isOpen($carbon, $organization);
    }



    /**
     * check that $carbon is a weekend
     *
     * @param   string $date
     * @param int      $organization
     *
     * @return bool
     */
    public function isWeekend($date, $organization = 0)
    {
        $carbon = Carbon::parse($date);

        $weekends = model('organization', $organization)->getWeeklySetting();
        if ($this->isDefined($carbon, $organization)) {
            return $this->guessIsOpening($carbon, $weekends);
        }
        return false;
    }



    /**
     * inverse of isWeekend method
     *
     * @param  string $date
     * @param int     $organization
     *
     * @return bool
     */
    public function isNotWeekend($date, $organization = 0)
    {
        $carbon = Carbon::parse($date);

        if ($this->isNotDefined($carbon, $organization)) {
            return false;
        }
        return !$this->isWeekend($carbon, $organization);
    }



    /**
     * check that $carbon is a holiday or no
     *
     * @param     string $date
     * @param    array   $weekends
     *
     * @return bool
     */
    public function guessIsOpening($date, $weekends)
    {
        $carbon = Carbon::parse($date);

        foreach ($weekends as $weekend) {
            if ($this->isWeekendCheck($carbon, $weekend)) {
                return true;
            }
        }
        return false;
    }



    /**
     * check that $day is what a day
     *
     * @param \Carbon\Carbon $carbon
     * @param      string    $day
     *
     * @return bool
     */
    private function isWeekendCheck(Carbon $carbon, $day)
    {
        switch ($day) {
            case "saturday":
                return $carbon->isSaturday();
            case "sunday":
                return $carbon->isSunday();
            case "monday":
                return $carbon->isMonday();
            case "tuesday":
                return $carbon->isTuesday();
            case "wednesday":
                return $carbon->isWednesday();
            case "thursday":
                return $carbon->isThursday();
            case "friday":
                return $carbon->isFriday();
        }
        return false;
    }

}