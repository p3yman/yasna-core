<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/5/18
 * Time: 1:04 PM
 */

namespace Modules\Workdays\Database\Seeders;

use Illuminate\Database\Seeder;

class DummyTableSeeder extends Seeder
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->addOrgan();
    }



    /**
     * ADD dummy organization
     *
     * @param int $total
     */
    private function addOrgan($total = 5)
    {
        $array = [];
        for ($i = 0; $i <= $total; $i++) {
            $array[] = [
                 "title" => "Organ-" . $i,
                 'code'  => "$i",
            ];
        }
        yasna()->seed('organizations', $array);
    }
}
