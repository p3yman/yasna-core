<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/5/18
 * Time: 4:02 PM
 */

namespace Modules\Workdays\Database\Seeders;

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data[] = [
             'slug'          => 'weekends_default',
             'title'         => trans("workdays::calendar.weekends-default"),
             'category'      => 'upstream',
             'order'         => '160',
             'data_type'     => 'text',
             'default_value' => '["thursday","friday"]',
             'hint'          => '',
             'css_class'     => '',
             'is_localized'  => '0',
        ];
        yasna()->seed('settings', $data);
    }
}
