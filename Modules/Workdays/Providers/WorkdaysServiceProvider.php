<?php

namespace Modules\Workdays\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class WorkdaysServiceProvider
 *
 * @package Modules\Workdays\Providers
 */
class WorkdaysServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerTrait();
        $this->registerSidebarSetting();
    }



    /**
     * add workdays item to setting sidebar
     */
    private function registerSidebarSetting()
    {
        module('manage')
             ->service('settings_sidebar')
             ->add('workdays')
             ->link('workdays/')
             ->trans("workdays::calendar.title")
             ->order(21)
             ->permit(function () {
                 return user()->isSuperadmin();
             })
        ;
    }



    /**
     * register traits
     */
    private function registerTrait()
    {
        module('yasna')->service('traits')
                       ->add()->trait('Workdays:OrganizationWorkdayTrait')->to('Organization')
        ;
    }
}
