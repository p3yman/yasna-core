/**
 * Workdays Js
 * -------------------------
 * Created by Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 29-07-2018
 */

jQuery(function ($) {
    /**
     * Capitalize strings
     * @param string
     * @return string
     */
    function capitalize(string) {

        return string.replace(/\b\w/g, function (l) {
            return l.toUpperCase()
        })
    }


    /**
     * Sets Days Status
     * @param options
     * @constructor
     */
    let WorkDays = function (options) {
        let self  = this;
        self.grid = $(options.selector);
        self.days = options.days;

        /**
         * Init
         */
        self.init = function () {
            self.setDaysStatus(self.days);
        };


        /**
         * Sets Days Status
         * @param days
         */
        self.setDaysStatus = function (days) {
            self.setYear(days[0]);
            days.forEach(function (day, index) {
                self.setDayStatus(day);
            });
        };


        /**
         * Changes Table Year Title
         * @param day
         */
        self.setYear = function (day) {
            let year        = day['j-year'];
            let yearTitle   = self.grid.find('.year-title > h1');
            let nextYearBtn = self.grid.find('.year-control #nextYear');
            let prevYearBtn = self.grid.find('.year-control #prevYear');

            nextYearBtn.attr('data-year', year);
            prevYearBtn.attr('data-year', year);

            yearTitle.text(ad(year));
        };


        /**
         * Sets Single Day Status
         * @param day
         */
        self.setDayStatus = function (day) {
            let dayNum   = day['j-day'].replace(/^0+/, '');
            let monthNum = day['j-month'].replace(/^0+/, '');
            let position = self.findPosition(dayNum, monthNum);

            self.decideStatus(day.status, position);
            self.decideModifiable(day.isModifiable, position);
            position.attr('data-id', day.id);
        };


        /**
         * Finds Table-cell Position
         * @param row
         * @param col
         * @return {*|jQuery|HTMLElement}
         */
        self.findPosition = function (row, col) {
            let selectorId = row + "-" + col;

            return $('[data-cel-place=' + selectorId + ']');
        };


        /**
         * Decides Table-cell Action Based On Status
         * @param status
         * @param position
         * @return {*}
         */
        self.decideStatus = function (status, position) {
            let methodName = "decide" + capitalize(status);
            let method     = self[methodName];

            if (typeof method === "function") {
                return method(position, status);
            } else {
                return self.decideDefault(methodName);
            }
        };


        /**
         * Decides if Table-cell is modifiable
         */
        self.decideModifiable = function (isModifiable, position) {

            if (isModifiable) {
                self.makeModifiable(position);
            } else {
                self.makeNonModifiable(position);
            }
        };


        /**
         * If Workday
         * @param pos
         * @param status
         */
        self.decideOn = function (pos, status) {
            pos.attr('role', 'button');
            pos.attr('data-status', status);
        };


        /**
         * If Off-day
         * @param pos
         * @param status
         */
        self.decideOff = function (pos, status) {
            pos.attr('role', 'button');
            pos.attr('data-status', status);
        };


        /**
         * If Inactive Day
         * @param pos
         */
        self.decideInactive = function (pos) {
            pos.removeAttr('role');
            pos.removeAttr('data-status')
        };


        /**
         * Default: Shows An Error If Method Not Found
         * @param methodName
         */
        self.decideDefault = function (methodName) {
            console.error(methodName + " is undefined.\n Try To set related method.");
        };


        /**
         * Makes cell clickable
         * @param pos
         */
        self.makeModifiable = function (pos) {
            pos.attr('role', 'button');
        };


        /**
         * Make cell unclickable
         * @param pos
         */
        self.makeNonModifiable = function (pos) {
            pos.removeAttr('role');
        }
    };


    /**
     * An empty object to store grid functions in
     * @type {{}}
     */
    let gridEvents = {};


    /**
     * Toggles Cell Status
     * @param status
     * @param el
     * @return {*}
     */
    function toggleStatus(status, el) {
        let method = gridEvents['change' + capitalize(status)];

        if (typeof method === "function") {
            return saveDayNewStatus(el, method);
            //return method(el);
        } else {
            console.error("Method not found.")
        }
    }


    /**
     * Changes Status from "on" to "off"
     * @param el
     */
    gridEvents.changeOn = function (el) {
        el.attr('data-status', 'off');
    };


    /**
     * Changes Status from "off" to "on"
     * @param el
     */
    gridEvents.changeOff = function (el) {
        el.attr('data-status', 'on')
    };


    /**
     * Ajax Call To Save New Status
     *
     * @param pos
     * @param action {function}
     */
    function saveDayNewStatus(pos, action) {
        let status = pos.attr('data-status');
        let id     = pos.attr('data-id');

        pos.removeAttr('role');
        $.ajax({
            url    : url('/manage/workdays/update_day'),
            type   : "json",
            method : "POST",
            data   : {
                "_token": $("#table").data('token'),
                id      : id,
                status  : status
            },
            success: function () {
                $('#workday-saved').click();
                pos.attr('role','button');
                action(pos);
            },
            error  : function () {
                $('#workday-not-saved').click();
                pos.attr('role','button');
            }
        })
    }


    /**
     * Ajax Call to render year days
     *
     * @param year
     * @param organ_id
     * @param create
     */
    function getYearlyProgram(year, organ_id, create) {
        if (typeof create === "undefined") {
            create = false;
        }
        if (create) {
            divReload('calendar-body', "/" + year + "/" + organ_id + "?create_new=true");
        }
        else {
            divReload('calendar-body', "/" + year + "/" + organ_id);
        }
    }

    /**
     * Ajax Call to decide rendering year days
     *
     * @param year
     * @param organ_id
     */
    function changeYear(year, organ_id) {
        var path = document.location.pathname;
        history.pushState({'year': year}, '', path + '?year=' + year);
        getYearlyProgram(year, organ_id);
    }


    /**
     * Renders days in grid
     * @param data
     */
    function renderYearView(data) {
        let grid = new WorkDays({
            selector: '#workdaysTable',
            days    : data
        }).init();
        console.log(WorkDays);
    }

    /**
     *if data exists for this organ it trigger
     */
    function loadTable() {
        $(".year-title h1").text(pd($("#table").attr('data-year').toString()));

        if ($("#table").data('hasdata') != "1")
            return;

        if ($("#table").attr('data-year') === $("#table").attr('data-this_year'))
            return;

        getYearlyProgram($("#table").attr('data-year'), $("#table").attr('data-organ'));
    }

    /**
     * hidden workdays
     */
    function hiddenTable() {
        $("#table").addClass('hidden');
        $(".workdays-not-set").removeClass('hidden');
    }

    /**
     * show workdays
     */
    function showTable() {
        $("#table").removeClass('hidden');
        $(".workdays-not-set").addClass('hidden');
    }

    /**
     * show loading to render
     */
    function showLoading() {
        $(".calendar-body").addClass('whirl');
    }

    /**
     * hidden loading
     */
    function hiddenLoading() {
        $(".calendar-body").removeClass('whirl');
    }

    /**
     *  set title and year attribute
     *
     * @param int year
     */
    function setTitleAndAttr(year) {
        $("#table").attr('data-year', year);
        $(".year-title h1").text(pd(year.toString()));
    }

    /**
     * enable or disable base on this year access-btn
     */
    function enableDisableAccessBtn() {
        var year      = $("#table").attr('data-year');
        var this_year = $("#table").attr('data-this_year');
        if (year < this_year) {
            $("#access-btn").attr('disabled', 'disabled')
        } else {
            $("#access-btn").removeAttr('disabled')
        }
    }

    /*
     *-------------------------------------------------------
     * Document Ready
     *-------------------------------------------------------
     */
    $(document).ready(function () {

        loadTable();

        $(document).on('click', '#access-btn', function () {
            getYearlyProgram($("#table").attr('data-year'), $("#table").attr('data-organ'), true);
        });

        // Grid Cell Click Event
        $(document).on('click', '.day-toggle-view' ,function () {
            let pos    = $(this);
            let status = pos.attr('data-status');
            let role   = pos.attr('role');

            if (typeof status === "undefined" || !status.length) {
                return;
            }

            if (role !== "button") {
                return;
            }

            toggleStatus(status, pos);

        });


        // Previous Year Button Event
        $('#prevYear').on('click', function () {
            var year = $("#table").attr('data-year');
            year--;
            setTitleAndAttr(year);
            changeYear($("#table").attr('data-year'), $("#table").attr('data-organ'));
        });

        // Next Year Button Event
        $('#nextYear').on('click', function () {
            var year = $("#table").attr('data-year');
            year++;
            setTitleAndAttr(year);
            changeYear($("#table").attr('data-year'), $("#table").attr('data-organ'));
        });

    });
}); //End Of siaf!


