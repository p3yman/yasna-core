@php
	if (isset($items) and count($items)){
		$collection = collect($items);
 		$full_year = $collection->groupBy('j-month');
	}
@endphp
<div id="table" class="{{($has_data)?'':"hidden"}}" data-year="{{$current_year}}"
	 data-this_year="{{$current_year}}"
	 data-organ="{{$organization_id}}" data-hasdata="{{$has_data}}" data-token="{{csrf_token()}}">
	@if($has_data and !isset($dont_render_table))
		@include('workdays::table.table',[
			"months" => $full_year ,
		])
	@endif
</div>

<div class="workdays-not-set {{($has_data) ? "hidden":''}}">
	<div class="col-sm-6 col-centered">
		@include('workdays::table.alert')
	</div>
</div>

<script>
	jQuery(function($){
        $('[data-toggle="tooltip"]').tooltip();
    }); //End Of siaf!
</script>
