@php
	$days = [
	  "saturday",
	  "sunday",
	  "monday",
	  "tuesday",
	  "wednesday",
	  "thursday",
	  "friday",
	];
@endphp
<div class="form-group">
	<label for="days" class="col-xs-2 label-control">
		{{ trans('workdays::calendar.weekends') }}
	</label>
	<div class="col-xs-10">
		<div class="btn-group btn-checkbox-group" id="days">

			@foreach($days as $day)
				<label>
					<input type="checkbox" class="hidden" name="weekends[]" id="{{ $day }}" value="{{ $day }}"
						   autocomplete="off" {{(in_array($day,$weekends)?'checked':'')}}>
					<span class="btn btn-danger btn-outline">
						{{ trans('workdays::calendar.weekDays.' . strtolower($day)) }}
					</span>
				</label>
			@endforeach
		</div>
	</div>
</div>
