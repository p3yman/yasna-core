<div class="table-header">
	<div class="type-selector">
		{{--{!!--}}
		    {{--widget('combo')--}}
		    {{--->label('انتخاب')--}}
		    {{--->inForm()--}}
		    {{--->options($options)--}}
		    {{--->captionField('key')--}}
		    {{--->valueField('value')--}}
		 {{--!!}--}}
	</div>
	<div class="year-title">
		<h1>
			---
		</h1>
	</div>
	<div class="year-control">
		<button class="btn btn-default mh-sm"
				onclick="masterModal('{{ Route('workdays-setting-modal',['organization_id'=>$organization_id]) }}')">
			{{ trans('workdays::calendar.setting') }}
		</button>
		<div class="btn-group">
			<button id="prevYear" data-year="" type="button" class="btn btn-default">
				{{ trans('workdays::calendar.controls.prev') }}
			</button>
			<button id="nextYear" data-year type="button" class="btn btn-default">
				{{ trans('workdays::calendar.controls.next') }}
			</button>
		</div>
	</div>
</div>