<div class="nTable">
	<div class="nColumn dateCol">
		<div class="nCell col-heading">
			<h3></h3>
		</div>

		@for($i=1; $i < 32; $i++)
			@include('workdays::table.date-cell',[
				"date" => $i ,
			])
		@endfor
	</div>

	@foreach($months as $month => $days)
		<div class="nColumn">
			<div class="nCell col-heading">
				<h3>
					{{ trans("workdays::calendar.solar.months.".(int)$month) }}
				</h3>
			</div>

			@foreach($days as $day)
				@include('workdays::table.cell',[
					"day" => $day ,
				])
			@endforeach

		</div>
	@endforeach
</div>
