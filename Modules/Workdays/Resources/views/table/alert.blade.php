<div class="panel panel-warning">
	<div class="panel-heading">
		{{ trans('workdays::calendar.message.warning') }}
	</div>
	<div class="f18 panel-body pv-xl text-center">
		{{ trans('workdays::calendar.year_view_not_ready') }}
		<div class="mt-lg">
			{{--<button class="btn btn-primary btn-taha">--}}
			{{--{{ trans('workdays::calendar.request_calendar') }}--}}
			{{--</button>--}}
			@if($enable_access_btn)
				<button class="btn btn-info btn-taha" id="access-btn">
					{{ trans('workdays::calendar.access_admin') }}
				</button>
			@endif
		</div>
	</div>
</div>