@php
	$week_day_title = trans("workdays::calendar.weekDays.".strtolower($day['weekday-name']));
	$month_title = trans("workdays::calendar.solar.months.".(int)$day['j-month']);
	$day_number = (int)$day['j-day'];
	$month_number = (int)$day['j-month'];
@endphp
<div class="nCell day-toggle-view"
	 data-status= "{{ $day['status'] }}"
	 data-id="{{ $day['id'] }}"
	 @if(isset($day['status']) and $day['status'] !== "inactive")
		@if($day['isModifiable'])
			role="button"
		@endif
	 @endif
	 data-cel-place="{{ "$day_number-$month_number" }}"
	 title="{{ "$week_day_title ".ad($day_number)." $month_title" }}"
	 data-toggle="tooltip">

	<span class="workday">
		<i class="far fa-check-circle"></i>
	</span>

	<span class="offday">
		<i class="far fa-times-circle"></i>
	</span>

</div>
