@extends('manage::layouts.template')
@section('content')
	@if(count($organs['tabs'])!=1)
		@include("manage::widgets.tabs",$organs)
	@endif
	@include("workdays::main")
@stop