{!!
    widget('modal')
    ->label('tr:workdays::calendar.setting')
    ->target(route('workdays-save-setting',['organization_id'=>$organization_id]))
    ->method("get")
    ->id('setting-weekly')
 !!}

<div class="modal-body pv40">
	@include('workdays::table.weekends')
</div>

@include("manage::widgets101.feed",['container_id'=>"setting-weekly",'container_class'=>'','container_style'=>''])

<div class="modal-footer">
	{!!
	    widget('button')
	    ->label('tr:workdays::calendar.save')
	    ->type('sumbit')
	    ->class('btn-success btn-taha pull-right')

	 !!}
</div>
