<div class="panel">
	<div class="workdays-table" id="workdaysTable">
		@include('workdays::table.header')
		<div class="calendar-body" id="calendar-body" data-src="{{url('/manage/workdays/show/')}}">
			@include("workdays::calendar-body")
		</div>
	</div>
</div>

<div class="noDisplay">
	<button
			id="workday-not-saved"
			type="button" data-notify=""
			data-message="{{ trans('workdays::calendar.message.error') }}"
			data-options="{&quot;status&quot;:&quot;danger&quot;}"
			class="btn btn-danger"></button>
	<button
			id="workday-saved"
			type="button" data-notify=""
			data-message="{{ trans('workdays::calendar.message.success') }}"
			data-options="{&quot;status&quot;:&quot;success&quot;}"
			class="btn btn-success"></button>
</div>

