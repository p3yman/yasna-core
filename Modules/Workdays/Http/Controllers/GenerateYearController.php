<?php

namespace Modules\Workdays\Http\Controllers;

use App\Models\Workday;
use Illuminate\Http\Request;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;
use Morilog\Jalali\jDate;
use Morilog\Jalali\jDateTime;
use Carbon\Carbon;

class GenerateYearController extends YasnaController
{
    private $model;
    private $year;
    private $organization_id;
    private $array_days = [];
    private $holidays;
    private $weekends;
    private $view_data;



    /**
     * generate a working year
     *
     * @param string $year
     * @param int    $organization_id
     *
     * @return array
     */
    public function index($year, $organization_id)
    {
        $this->model           = model('workday');
        $this->year            = $year;
        $this->organization_id = $organization_id;
        if (model("workday")->hasDateJalali($year, $organization_id)) {
            $this->drawTableFromCatch();
        } else {
            $this->drawJalaliTable();
        }

        return $this->array_days;

    }



    /**
     * load view workdays table
     *
     * @param            int     $year
     * @param               int  $organization_id
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loadViewOfWorkdays($year, $organization_id, SimpleYasnaRequest $request)
    {
        $this->view_data['has_data']        = model('workday')->hasDateJalali($year, $organization_id);
        $this->view_data['organization_id'] = $organization_id;
        $this->view_data['current_year']    = $year;
        if ($this->view_data['has_data'] or $request->create_new) {
            $this->view_data['items']    = $this->index($year, $organization_id);
            $this->view_data['has_data'] = true;
        }
        $this->view_data['enable_access_btn'] = $this->enableAccessBtn($year);
        return view("workdays::calendar-body", $this->view_data);
    }



    /**
     * create workdays with api (only differenet between this method and idex method is holiday field in
     * SimpleYasnaRequest that it allow array to set holidays)
     *
     * @param         int        $year
     * @param          int       $organization_id
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createWorkdaysWithApi($year, $organization_id, SimpleYasnaRequest $request)
    {
        $this->model           = model('workday');
        $this->year            = $year;
        $this->organization_id = $organization_id;
        if (model("workday")->hasDateJalali($year, $organization_id)) {
            $this->drawTableFromCatch();
        } else {
            $this->holidays = $request->get('holidays');
            $this->drawJalaliTable();
        }
        return response()->json($this->array_days);
    }



    /**
     * check that this year has already data
     *
     * @param string $year
     * @param int    $organization_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeYear($year, $organization_id)
    {
        $check_data       = model("workday")->hasDateJalali($year, $organization_id);
        $data['has_data'] = $check_data;
        return json_encode($data);
    }



    /**
     * decide that access btn will bew shown or no
     *
     * @param int $year
     *
     * @return bool
     */
    public function enableAccessBtn($year)
    {
        $this_year = jDate::forge()->format('Y');
        if ($this_year > $year) {
            return false;
        }
        return true;
    }



    /**
     * make data and store to draw jalali workdays
     */
    private function drawJalaliTable()
    {
        $carbon         = jDateTime::createCarbonFromFormat('Y-m-d', "$this->year-01-01");
        $current_year   = $this->year;
        $this->weekends = model('organization', $this->organization_id)->getWeeklySetting();
        while ($this->year == $current_year) {

            $is_holiday = $this->holidayOperation($carbon);

            $model = model('workday')->batchSave([
                 'organization_id' => $this->organization_id,
                 'date'            => $carbon->toDateString(),
                 'open'            => ($is_holiday) ? 0 : 1,
            ]);

            $jalali = jDateTime::toJalali($carbon->year, $carbon->month, $carbon->day);


            $this->addJsonArray($model, $jalali, $is_holiday);

            $carbon->addDay();
            $current_year = $this->getJalaliYear($carbon);
        }
    }



    /**
     * create json array from stored data
     */
    private function drawTableFromCatch()
    {
        $days = model('workday')->inJalaliYear($this->year)->where("organization_id", $this->organization_id)->get();
        foreach ($days as $day) {
            //dd($day->date);
            $jalali_obj = jDate::forge($day->date);

            $jalali[0] = $jalali_obj->format('Y');
            $jalali[1] = $jalali_obj->format('m');
            $jalali[2] = $jalali_obj->format('d');

            $this->addJsonArray($day, $jalali, !$day->open);
        }
    }



    /**
     * all check to set holiday
     *
     * @param Carbon $day
     *
     * @return bool
     */
    private function holidayOperation(Carbon $day)
    {
        $is_holiday = workdays()->guessIsOpening($day, $this->weekends);

        $custom_holiday = false;
        if (!empty($this->holidays)) {
            $custom_holiday = $this->checkHoliday($day);
        }

        return ($custom_holiday) ? $custom_holiday : $is_holiday;
    }



    /**
     * generate json array to render table
     *
     * @param Workday $model
     * @param array   $jalali
     * @param bool    $is_holiday
     */
    private function addJsonArray($model, $jalali, $is_holiday = false)
    {
        $today = Carbon::today();
        $day   = Carbon::parse($model->date);
        Carbon::setWeekStartsAt(Carbon::FRIDAY);
        $this->array_days[] = [
             'id'           => $model->id,
             //'date'         => 'bla bla',
             'j-day'        => "$jalali[2]",
             'j-month'      => "$jalali[1]",
             'j-year'       => "$jalali[0]",
             'status'       => ($is_holiday) ? "off" : "on",
             'isModifiable' => $day->greaterThanOrEqualTo($today),
             'weekday-name' => $this->getWeekdayName($day),
        ];
    }



    /**
     * check that today is custom holiday
     *
     * @param Carbon $day
     *
     * @return bool
     */
    private function checkHoliday(Carbon $day)
    {
        foreach ($this->holidays as $holiday) {
            $holiday_carbon = Carbon::parse($holiday);
            if ($holiday_carbon->equalTo($day)) {
                return true;
            }
        }
        return false;
    }



    /**
     * get name of the week
     *
     * @param Carbon $carbon
     *
     * @return string
     */
    private function getWeekdayName(Carbon $carbon)
    {
        return $carbon->format('l');
    }



    /**
     * get jalali year from carbon
     *
     * @param Carbon $carbon
     *
     * @return string
     */
    private function getJalaliYear($carbon)
    {
        return jDateTime::toJalali($carbon->year, $carbon->month, $carbon->day)[0];
    }
}
