<?php

namespace Modules\Workdays\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Workdays\Http\Requests\WorkdayRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;
use Modules\Yasna\Services\YasnaRequest;
use Morilog\Jalali\jDate;

class WorkdaysController extends YasnaController
{
    private $view_data;
    private $organ_id;



    /**
     * Display Workday Main View
     *
     * @param int            $organization_id
     * @param WorkdayRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($organization_id = 0, WorkdayRequest $request)
    {
        if (!user()->isSuperadmin()) {
            abort(403);
        }
        $this->organ_id                     = $organization_id;
        $this->view_data['organization_id'] = $organization_id;
        $this->view_data['current_year']    = (is_null($request->year)) ? jDate::forge()
                                                                               ->format('Y') : $request->year;
        $this->view_data['has_data']        = model('workday')->hasDateJalali(
             $this->view_data['current_year'],
             $organization_id
        );

        $this->generateYearControllerMethods($this->view_data['current_year'], $organization_id);

        $this->loadTabs();
        return view("workdays::tab", $this->view_data);
    }






    /**
     * load all organs as tab
     */
    public function loadTabs()
    {
        $this->assets();
        $this->view_data['page']              = $this->setPage();
        $organs                               = user()->organizations()->get();
        $this->view_data["organs"]['current'] = ($this->organ_id == 0) ? '/' : $this->organ_id;
        $this->setDefaultTab();
        foreach ($organs as $organ) {
            $this->view_data["organs"]["tabs"][] =
                 [
                      'caption' => $organ->alias,
                      'url'     => "$organ->id",
                 ];
        }
    }



    /*
     *
     * Displays modal of weekend setting
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function weeklySetting($organization_id = 0)
    {
        $view_data['organization_id'] = $organization_id;
        $view_data['weekends']        = model('organization', $organization_id)->getWeeklySetting();
        return view('workdays::modal', $view_data);
    }



    /**
     * save Weekly setting
     *
     * @param     int $organization_id
     * @param Request $request
     *
     * @return string
     */
    public function saveWeeklySetting($organization_id, Request $request)
    {
        if (count($request->get('weekends')) == 0) {
            return $this->jsonSaveFeedback(0, [
                 'success_message' => trans("workdays::calendar.save-setting"),
                 'success_refresh' => '0',
                 'danger_message'  => trans("workdays::calendar.server-error"),
                 'danger_refresh'  => '0',
            ]);
        }

        $is_saved = model('organization', $organization_id)->saveWeeklySetting($request->get('weekends'));

        return $this->jsonAjaxSaveFeedback($is_saved, [
             'success_message' => trans("workdays::calendar.save-setting"),
             'danger_message'  => trans("workdays::calendar.server-error"),
        ]);
    }



    /**
     * update state of day
     *
     * @param Request $request
     *
     * @return bool
     */
    public function updateDay(Request $request)
    {

        $model    = model('workday')->where('id', $request->get('id'))->first();
        $is_saved = $model->batchSave([
             'open' => ($request->get('status') == 'on') ? 0 : 1,
             'date' => $model->date,
        ]);


        return $this->jsonSaveFeedback($is_saved, [
             'success_message' => trans("workdays::calendar.save-setting"),
             'success_refresh' => '0',
             'danger_message'  => trans("workdays::calendar.server-error"),
             'danger_refresh'  => '0',
        ]);
    }


    /**
     * run helper methods from GenerateYearController
     *
     * @param int $year
     * @param int $organization_id
     */
    private function generateYearControllerMethods($year, $organization_id)
    {
        if ($this->view_data['has_data']) {
            $this->view_data['items'] = module('workdays')
                 ->callControllerMethod(
                      'GenerateYearController',
                      'index',
                      $this->view_data['current_year'],
                      $organization_id);
        }
        $this->view_data['enable_access_btn'] = module('workdays')
             ->callControllerMethod('GenerateYearController', 'enableAccessBtn', $year);
    }


    /**
     * set default tab for workdays
     */
    private function setDefaultTab()
    {
        $this->view_data["organs"]["tabs"][] =
             [
                  'caption' => trans('workdays::calendar.default-tab'),
                  'url'     => "/",
             ];
    }



    /**
     * set page variable array
     *
     * @return array
     */
    private function setPage()
    {
        return [
             ["workdays", trans('workdays::calendar.title')],
        ];
    }



    /**
     * load assets for workdays table
     */
    private function assets()
    {
        // CSS
        module('manage')
             ->service('template_assets')
             ->add('workdays-style')
             ->link("workdays:css/workdays-fa.min.css")
             ->order(42)
        ;

        // js
        module('manage')
             ->service('template_bottom_assets')
             ->add('workdays-js')
             ->link("workdays:js/workdays.min.js")
             ->order(42)
        ;
    }
}
