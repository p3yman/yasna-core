<?php

Route::group([
     'middleware' => 'web',
     'prefix'     => 'manage/workdays',
     'namespace'  => 'Modules\Workdays\Http\Controllers',
], function () {
    Route::get('/weekly-setting/{organization_id?}', 'WorkdaysController@weeklySetting')
         ->name('workdays-setting-modal')
    ;

    Route::get('/show/{year}/{organization_id}', 'GenerateYearController@loadViewOfWorkdays')->name('workdays-generate');
    Route::get('/api/generate/{year}/{organization_id}', 'GenerateYearController@createWorkdaysWithApi')
         ->name('workdays-generate')
    ;

    Route::post('/change_year/{year}/{organization_id}', 'GenerateYearController@changeYear');

    Route::post('/update_day', 'WorkdaysController@updateDay');

    Route::get('/save-setting/{organization_id}', 'WorkdaysController@saveWeeklySetting')
         ->name('workdays-save-setting')
    ;
    Route::get('/{organization_id?}', 'WorkdaysController@index');
});
