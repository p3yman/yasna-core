<?php

namespace Modules\Workdays\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;
use Morilog\Jalali\jDateTime;

class WorkdayRequest extends YasnaRequest
{
    /**
     * purify year field
     *
     * @return array|void
     */
    public function purifier()
    {
        if (is_null($this->year)) {
            return;
        }

        if (!$this->checkJalaliDate() or strlen($this->year) != 4) {
            $this->year = null;
        }

    }



    /**
     * validate jalali year
     *
     * @return bool
     */
    private function checkJalaliDate()
    {
        return jDateTime::checkDate($this->year, "1", "1", true);
    }

}
