<?php

namespace Modules\Workdays\Entities;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Morilog\Jalali\jDateTime;

class Workday extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];



    /**
     * check that in this year and organization has date or no
     *
     * @param string $year
     * @param int    $organization_id
     *
     * @return boolean
     */
    public function hasDateJalali($year, $organization_id)
    {
        $start_year       = jDateTime::createCarbonFromFormat('Y-m-d', "$year-01-01")->year;
        $end_year         = jDateTime::createCarbonFromFormat('Y-m-d', "$year-12-01")->year;
        $start_year_check = model('workday')
             ->whereYear('date', $start_year)
             ->where('organization_id', $organization_id)
             ->exists()
        ;

        $end_year_check = model('workday')
             ->whereYear('date', $end_year)
             ->where('organization_id', $organization_id)
             ->exists()
        ;
        return ($start_year_check and $end_year_check);
    }



    /**
     * vice versa of hadDate method
     *
     * @param string $year
     * @param int    $organization_id
     *
     * @return bool
     */
    public function hasNotDateJalali($year, $organization_id)
    {
        return !$this->hasDateJalali($year, $organization_id);
    }



    /**
     * make builder to get one year records
     *
     * @param int $year
     *
     * @return Builder
     */
    public function inJalaliYear($year)
    {
        $start = jDateTime::createCarbonFromFormat('Y-m-d', "$year-01-01");
        $year++;
        $end = jDateTime::createCarbonFromFormat('Y-m-d', "$year-01-01");
        return $this->whereDate("date", ">=", $start->toDateTimeString())
                    ->whereDate('date', "<", $end->toDateTimeString())
             ;
    }
}
