<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 8/5/18
 * Time: 3:38 PM
 */

namespace Modules\Workdays\Entities\Traits;

use App\Models\Workday;
use Illuminate\Database\Eloquent\Builder;

trait OrganizationWorkdayTrait
{

    /**
     * relation method between  Organization and workday
     *
     * @return Builder
     */
    public function workdays()
    {
        return $this->hasMany(Workday::class, "organization_id", "id");
    }



    /**
     * define meta fields
     *
     * @return array
     */
    public function WeekendMetaFields()
    {
        return [
             'weekends',
        ];
    }



    /**
     * save weekly setting of workdays
     *
     * @param array $days
     *
     * @return boolean
     */
    public function saveWeeklySetting($days)
    {
        if ($this->id == 0) {
            return setting('weekends_default')->setDefaultValue(json_encode($days));
        }
        return $this->grab($this->id)->batchSave([
             'weekends' => json_encode($days),
        ])
             ;
    }



    /**
     * get weekly setting of workdays
     */
    public function getWeeklySetting()
    {
        if ($this->id == 0) {
            return json_decode(setting('weekends_default')->default_value);
        } else {
            $days = json_decode($this->grabId($this->id)->getMeta('weekends'));
            //if setting has not define it read default setting
            if (empty($days)) {
                $days = json_decode(setting('weekends_default')->default_value);
            }
            return $days;
        }
    }
}
