<?php namespace Modules\Follows\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFollowing extends YasnaModel
{
    use SoftDeletes;
    protected $guarded = ["id"];



    /**
     * @param int  $follower_id
     * @param int  $following_id
     * @param bool $with_trashed
     *
     * @return Builder
     */
    public function rowQuery($follower_id, $following_id, $with_trashed = true)
    {
        $builder = $this->where([
             "follower_id"  => $follower_id,
             "following_id" => $following_id,
        ]);
        if ($with_trashed) {
            return $builder->withTrashed();
        }
        return $builder;
    }



    /**
     * @param int  $follower_id
     * @param int  $following_id
     * @param bool $with_trashed
     *
     * @return Model|Builder
     */
    public function row($follower_id, $following_id, $with_trashed = true)
    {
        return $this->rowQuery($follower_id, $following_id, $with_trashed)->first();
    }



    /**
     * @param int $follower_id
     * @param int $following_id
     *
     * @return bool
     */
    public function followAction($follower_id, $following_id)
    {
        $this->rowQuery($follower_id, $following_id, true)->forceDelete();
        $new_pivot = $this->create([
             "follower_id"  => $follower_id,
             "following_id" => $following_id,
        ]);
        if ($new_pivot and $new_pivot->id) {
            return true;
        }
        return false;
    }



    /**
     * @param int $follower_id
     * @param int $following_id
     *
     * @return bool
     */
    public function unfollowAction($follower_id, $following_id)
    {
        return boolval($this->rowQuery($follower_id, $following_id)->delete());
    }



    /**
     * @param int $follower_id
     * @param int $following_id
     *
     * @return bool
     */
    public function isFollowing($follower_id, $following_id)
    {
        return boolval($this->rowQuery($follower_id, $following_id, false)->count());
    }



    /**
     * @param int $user_id_1
     * @param int $user_id_2
     *
     * @return bool
     */
    public function areMutualFollowing($user_id_1, $user_id_2)
    {
        return $this->isFollowing($user_id_1, $user_id_2) and $this->isFollowing($user_id_2, $user_id_1);
    }
}
