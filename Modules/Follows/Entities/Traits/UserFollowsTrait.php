<?php namespace Modules\Follows\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

trait UserFollowsTrait
{
    /**
     * @return Builder
     */
    public function followers($with_trashed = false)
    {
        return user()->whereIn('id', $this->followerIds($with_trashed));
    }



    /**
     * @return Collection
     */
    public function getFollowersAttribute()
    {
        return $this->followers()->get();
    }



    /**
     * @return Builder
     */
    public function followings($with_trashed = false)
    {
        return user()->whereIn('id', $this->followingIds($with_trashed));
    }



    /**
     * @return Collection
     */
    public function getFollowingsAttribute()
    {
        return $this->followings()->get();
    }



    /**
     * @return Builder
     */
    private function friends()
    {
        //TODO: to be discussed
    }



    /**
     * @return Collection
     */
    private function getFriendsAttribute()
    {
        return $this->friends()->get();
    }



    /**
     * @param bool $with_trashed
     *
     * @return array
     */
    public function followerIds($with_trashed = false)
    {
        $builder = userFollowingPivot()->where('following_id', $this->id);
        if ($with_trashed) {
            $builder->withTrashed();
        }
        return $builder->get()->pluck('follower_id');
    }



    /**
     * @param bool $with_trashed
     *
     * @return array
     */
    public function followingIds($with_trashed = false)
    {
        $builder = userFollowingPivot()->where('follower_id', $this->id);
        if ($with_trashed) {
            $builder->withTrashed();
        }
        return $builder->get()->pluck('following_id');
    }



    /**
     * @param bool $fresh
     *
     * @return int
     */
    public function followingCount($fresh = false)
    {
        return count($this->followingIds());
    }


    /**
     * @param bool $fresh
     *
     * @return int
     */
    public function followerCount($fresh = false)
    {
        return count($this->followerIds());
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function follow($user_id)
    {
        return userFollowingPivot()->followAction($this->id, $user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function unfollow($user_id)
    {
        return userFollowingPivot()->unfollowAction($this->id, $user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isFollowing($user_id)
    {
        return userFollowingPivot()->isFollowing($this->id, $user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isNotFollowing($user_id)
    {
        return !$this->isFollowing($user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isFollowedBy($user_id)
    {
        return userFollowingPivot()->isFollowing($user_id, $this->id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isNotFollowedBy($user_id)
    {
        return !$this->isFollowedBy($user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isInMutualFollowWith($user_id)
    {
        return userFollowingPivot()->areMutualFollowing($this->id, $user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isNotInMutualFollowWith($user_id)
    {
        return !$this->isInMutualFollowWith($user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isFriendWith($user_id)
    {
        return $this->isInMutualFollowWith($user_id);
    }



    /**
     * @param $user_id
     *
     * @return bool
     */
    public function isNotFriendWith($user_id)
    {
        return !$this->isFriendWith($user_id);
    }
}
