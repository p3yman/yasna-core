<?php

Route::group(['middleware' => 'web', 'prefix' => 'follows', 'namespace' => 'Modules\Follows\Http\Controllers'], function () {
    Route::get('/', 'FollowsController@index');
});
