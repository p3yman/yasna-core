<?php

namespace Modules\Follows\Providers;

use Modules\Yasna\Services\YasnaProvider;

class FollowsServiceProvider extends YasnaProvider
{
    /**
     * Service Provider Index
     */
    public function index()
    {
        $this->registerTraits();
    }



    /**
     * Model Traits
     */
    public function registerTraits()
    {
        module('yasna')
             ->service('traits')
             ->add('follows')
             ->trait('Follows:UserFollowsTrait')
             ->to('User')
        ;
    }
}
