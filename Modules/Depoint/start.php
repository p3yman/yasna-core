<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}


if (!function_exists("depointTemplate")) {
    /**
     * Returns an instance of the `Modules\Depoint\Services\Template\DepointTemplateHandler` class
     * to be used while working with template information.
     *
     * @return \Modules\Depoint\Services\Template\DepointTemplateHandler
     */
    function depointTemplate()
    {
        return new \Modules\Depoint\Services\Template\DepointTemplateHandler;
    }
}


if (!function_exists("currentModule")) {

    /**
     * Returns an instance of the `Modules\Depoint\Services\Module\CurrentModuleHelper` class
     * to be used while getting information of the current module.
     *
     * @return \Modules\Depoint\Services\Module\CurrentModuleHelper
     */
    function currentModule()
    {
        return new \Modules\Depoint\Services\Module\CurrentModuleHelper;
    }
}

if (!function_exists("depoint")) {

    /**
     * Returns a new instance of the `Modules\Depoint\Providers\DepointServiceProvider` class.
     *
     * @return \Modules\Depoint\Providers\DepointServiceProvider
     */
    function depoint()
    {
        return new \Modules\Depoint\Providers\DepointServiceProvider(app());
    }
}

if (!function_exists("depointTools")) {

    /**
     * return instance of \Modules\Depoint\Services\Utilities\DepointTools
     *
     * @return \Modules\Depoint\Services\Utilities\DepointTools
     */
    function depointTools()
    {
        return new \Modules\Depoint\Services\Utilities\DepointTools();
    }
}

if (!function_exists("getWareColor")) {

    /**
     * get color  of a ware (first label of a ware)
     *
     * @param \Modules\Shop\Entities\Ware $ware
     *
     * @return array
     */
    function getWareColor($ware)
    {
        $parent_label = $ware->definedLabels()->whereSlug($ware->posttype->slug . '|colors')->first();
        $label        = $ware->labels()->whereParentId($parent_label->id)->first();
        $colors       = [];
        if (!$label) {
            return [];
        }
        $slug            = strtolower(explode('|', $label->slug)[1]);
        $colors['title'] = $label->title;
        $colors['code']  = config('depoint.product-colors.' . $slug);
        return $colors;
    }
}
