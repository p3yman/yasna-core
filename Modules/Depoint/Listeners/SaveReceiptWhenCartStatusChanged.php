<?php

namespace Modules\Depoint\Listeners;

use App\Models\Cart;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Cart\Events\CartStatusChanged;
use Modules\Depoint\Notifications\SendReceiptCreateNotification;

class SaveReceiptWhenCartStatusChanged implements ShouldQueue
{
    protected $event;
    protected $cart;



    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }



    /**
     * Handle the event.
     *
     * @param  object $event
     *
     * @return void
     */
    public function handle(CartStatusChanged $event)
    {
        $this->setEvent($event);

        if ($this->hasBeenDelivered()) {
            $this->handleNewlyDeliveredCart();
        } else {
            $this->handleUndeliveredCart();
        }
    }



    /**
     * Saves the event object on the listener object.
     *
     * @param CartStatusChanged $event
     *
     * @return void
     */
    protected function setEvent(CartStatusChanged $event)
    {
        $this->event = $event;
        $this->cart  = $event->model;
    }



    /**
     * Returns the cart model object.
     *
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }



    /**
     * Weather the new status of cart is in delivered status.
     *
     * @return bool
     */
    public function hasBeenDelivered()
    {
        return ($this->getCart()->status == 'delivered');
    }



    /**
     * Makes needed actions for a newly delivered cart.
     */
    public function handleNewlyDeliveredCart()
    {
        $cart = $this->getCart();

        if ($cart->receipts()->count()) {
            return;
        }

        $receipt = depoint()
             ->drawing()
             ->storeReceipt($cart->invoiced_amount, $cart->paied_at, $cart)
        ;

        $user = $cart->user;
        if ($user->exists) {
            $user->notify(new SendReceiptCreateNotification($receipt, $cart));
        }
    }



    /**
     * Handle undelivered cart.
     */
    public function handleUndeliveredCart()
    {
        $cart = $this->getCart();

        $cart->receipts()->delete();
    }
}
