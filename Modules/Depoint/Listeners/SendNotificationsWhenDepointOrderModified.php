<?php

namespace Modules\Depoint\Listeners;

use App\Models\User;
use Modules\Depoint\Events\DepointOrderModified;
use Modules\Depoint\Notifications\OrderModifyNotification;

class SendNotificationsWhenDepointOrderModified
{
    protected $order;



    /**
     * Notifies the specified user.
     *
     * @param User $user
     */
    protected function notifyUser(User $user)
    {
        $user->notify(new OrderModifyNotification($this->order));
    }



    /**
     * Sends notification to the client.
     */
    protected function sendNotificationToClient()
    {
        $user = $this->order->user;

        if (!$user) {
            return;
        }

        $this->notifyUser($user);
    }



    /**
     * Sends notification to all managers of the provider of the order.
     */
    protected function sendNotificationToProviderManagers()
    {
        $users = $this->order->provider->managers;

        foreach ($users as $user) {
            // The `pivot` relation was causing problems in serializing and unserializing model for queue.
            $user->unsetRelation('pivot');

            $this->notifyUser($user);
        }
    }



    /**
     * Handles the event.
     *
     * @param DepointOrderModified $event
     *
     * @return void
     */
    public function handle(DepointOrderModified $event)
    {
        $this->order = $event->model;

        $this->sendNotificationToClient();
        $this->sendNotificationToProviderManagers();
    }
}
