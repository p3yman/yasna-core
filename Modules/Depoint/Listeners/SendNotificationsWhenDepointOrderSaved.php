<?php

namespace Modules\Depoint\Listeners;

use App\Models\User;
use Modules\Depoint\Events\DepointOrderSaved;
use Modules\Depoint\Notifications\OrderCreateNotification;

class SendNotificationsWhenDepointOrderSaved
{
    protected $order;



    /**
     * Notify the specified user
     *
     * @param User $user
     */
    protected function notifyUser(User $user)
    {
        $user->notify(new OrderCreateNotification($this->order));
    }



    /**
     * Send notification to the client.
     */
    protected function sendNotificationToClient()
    {
        $user = $this->order->user;

        if (!$user) {
            return;
        }

        $this->notifyUser($user);
    }



    /**
     * Send notification to all managers of the provider of the order.
     */
    protected function sendNotificationToProviderManagers()
    {
        $users = $this->order->provider->managers;

        foreach ($users as $user) {
            // The `pivot` relation was causing problems in serializing and unserializing model for queue.
            $user->unsetRelation('pivot');

            $this->notifyUser($user);
        }
    }



    /**
     * Handle the event.
     *
     * @param DepointOrderSaved $event
     *
     * @return void
     */
    public function handle(DepointOrderSaved $event)
    {
        $this->order = $event->model;

        $this->sendNotificationToClient();
        $this->sendNotificationToProviderManagers();
    }
}
