<?php

namespace Modules\Depoint\Providers;

use App\Models\Account;
use App\Models\Transaction;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\ServiceProvider;
use Modules\Depoint\Services\Module\ModuleTrait;
use Modules\Payment\Services\PaymentHandler\Account\AccountBuilder;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class ShopServiceProvider extends ServiceProvider
{
    use ModuleTrait;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Returns the slug of main posttype of the shop.
     *
     * @return string
     */
    public static function mainPosttypeSlug()
    {
        return 'products';
    }



    /**
     * Returns the main posttype of the shop.
     *
     * @return \App\Models\Posttype
     */
    public static function mainPosttype()
    {
        return posttype(static::mainPosttypeSlug());
    }



    /**
     * Returns the string of the role of sellers.
     *
     * @return string
     */
    public static function sellerRoleSlug()
    {
        return 'seller';
    }



    /**
     * Returns the string of the role of sellers.
     *
     * @return string
     */
    public static function stockerRoleSlug()
    {
        return 'stocker';
    }



    /**
     * Checks if the current client is a logged in user with the seller role.
     *
     * @return bool
     */
    public static function sellerEnvironmentIsAccessible()
    {
        return user()->hasRole(static::sellerRoleSlug());
    }



    /**
     * Returns the prefix of the `custom_code` field for wares which are defined for sellers.
     *
     * @return string
     */
    public static function sellerWaresPrefix()
    {
        return 'seller-';
    }



    /**
     * Returns the prefix of slugs of products label.
     *
     * @return string
     */
    public static function labelsSlugPrefix()
    {
        return static::mainPosttypeSlug() . ware()::LABEL_POSTTYPE_DIVIDER;
    }



    /**
     * Returns list of color labels
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function availableColorLabels()
    {
        $colors_label_slug = static::labelsSlugPrefix() . "colors";
        $colors_label      = label($colors_label_slug);

        return $colors_label->children()->get();
    }



    /**
     * Checks if the specified color is acceptable.
     *
     * @param string $color
     *
     * @return bool
     */
    protected static function colorIsAcceptable($color)
    {
        $acceptable_colors_map = static::module()->config('product-colors');
        $acceptable_colors     = array_keys($acceptable_colors_map);


        return in_array($color, $acceptable_colors);
    }



    /**
     * Checks if the specified color is not acceptable.
     *
     * @param string $color
     *
     * @return bool
     */
    protected static function colorIsNotAcceptable($color)
    {
        return !static::colorIsAcceptable($color);
    }



    /**
     * Returns a list of available ware colors.
     *
     * @return array
     */
    public static function availableWareColors()
    {
        $labels            = static::availableColorLabels();
        $colors            = [];
        $label_slug_prefix = static::labelsSlugPrefix();

        foreach ($labels as $label) {
            $color_slug = str_after($label->slug, $label_slug_prefix);

            if (static::colorIsNotAcceptable($color_slug)) {
                continue;
            }

            $colors[] = $color_slug;
        }


        return $colors;
    }



    /**
     * Returns the base currency of the site.
     *
     * @return string
     */
    public static function baseCurrency()
    {
        return 'IRR';
    }



    /**
     * return currency
     *
     * @return string
     */
    public static function mainCurrency()
    {
        return 'IRT';
    }



    /**
     * return trans of currency
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public static function currencyTrans()
    {
        return module('depoint')->getTrans('general.currency.' . depoint()->shop()->mainCurrency());
    }



    /**
     * Returns the ratio to convert a price from base current to the main currency.
     *
     * @return float
     */
    public static function currencyRatio()
    {
        return 0.1;
    }



    /**
     * Converts the given price from the base currency to the main currency.
     *
     * @param double $price
     *
     * @return double|float|int
     */
    public static function convertToMainCurrency($price)
    {
        return ($price * static::currencyRatio());
    }



    /**
     * Returns an array of information about the specified color.
     *
     * @param string $color_slug
     *
     * @return array
     */
    public static function getColorInfo(string $color_slug)
    {
        $module            = module('depoint');
        $label_slug_prefix = $label_slug_prefix = depoint()->shop()->labelsSlugPrefix();
        $label             = label($label_slug_prefix . $color_slug);

        return [
             'slug'  => $color_slug,
             'title' => $label->title,
             'code'  => $module->getConfig("product-colors.$color_slug"),
        ];
    }



    /**
     * Returns the color slug of the specified ware.
     *
     * @param Ware $ware
     *
     * @return string
     */
    public static function getWareColorSlug(Ware $ware)
    {
        $parent_label = $ware->definedLabels()->whereSlug($ware->posttype->slug . '|colors')->first();
        $label        = $ware->labels()->whereParentId($parent_label->id)->first();
        if (!$label) {
            return '';
        }
        return strtolower(explode('|', $label->slug)[1]);
    }



    /**
     * Returns an array of the colors info of the specified ware.
     *
     * @param Ware $ware
     *
     * @return array
     */
    public static function getWareColorInfo(Ware $ware)
    {
        $slug = static::getWareColorSlug($ware);

        return static::getColorInfo($slug);
    }



    /**
     * Returns an array of usable payment types.
     *
     * @return array
     */
    public static function acceptablePaymentTypes()
    {
        return [
             'online',
             'deposit',
             'shetab',
             'cash',
        ];
    }



    /**
     * Returns an instance of the `Modules\Payment\Services\PaymentHandler\Account\AccountBuilder` class
     * to select accounts.
     *
     * @return AccountBuilder
     */
    public static function paymentAccountsBuilder()
    {
        return payment()
             ->accounts()
             ->whereIn('type', static::acceptablePaymentTypes())
             ;
    }



    /**
     * Returns a list of payment accounts.
     *
     * @return Collection
     */
    public static function paymentAccounts()
    {
        return static::paymentAccountsBuilder()
                     ->orderBy('order')
                     ->get()
             ;
    }



    /**
     * Returns a builder to select acceptable payment accounts for current client.
     *
     * @return AccountBuilder
     */
    public static function acceptablePaymentAccountsBuilder()
    {
        return static::paymentAccountsBuilder()
                     ->orderBy('order')
                     ->where(function ($query) {
                         if (user()->canBuyWithCredit()) {
                             return;
                         }

                         $query->where('slug', '<>', depoint()->shop()->creditAccountSlug());
                     })
             ;
    }



    /**
     * Returns a list of payment accounts which are available for current client.
     *
     * @return Collection
     */
    public static function acceptablePaymentAccounts()
    {
        return static::acceptablePaymentAccountsBuilder()->get();
    }



    /**
     * Returns a list of payment methods which are grouped by their types.
     *
     * @return Collection
     */
    public static function groupedPaymentAccounts()
    {
        return static::paymentAccounts()
                     ->groupBy('type')
                     ->sortBy(function (Collection $accounts, $type) {
                         return array_search($type, static::acceptablePaymentTypes());
                     })
             ;
    }



    /**
     * Returns a viewable card number for a shetab account.
     *
     * @param Account $account
     *
     * @return string
     */
    public static function shetabAccountNumberPreview(Account $account)
    {
        $card_number = ($account->card_number ?? '');
        $parts       = str_split($card_number, 4);
        return implode('-', $parts);
    }



    /**
     * Returns a viewable account number of an account based on its type.
     *
     * @param Account $account
     *
     * @return string
     */
    public static function accountNumberPreview(Account $account)
    {
        $type   = $account->type;
        $method = $type . studly_case(__FUNCTION__);

        if (method_exists(static::class, $method)) {
            return static::$method($account);
        }

        return $account->account_number;
    }



    /**
     * Returns a collection of account which can be used in the repurchase process.
     *
     * @return Collection
     */
    public static function repurchasePaymentAccounts()
    {
        return static::acceptablePaymentAccountsBuilder()
                     ->online()
                     ->orderBy('order')
                     ->get()
             ;

    }



    /**
     * Returns the slug of validated buyers role.
     *
     * @return string
     */
    public static function validatedBuyerRoleSlug()
    {
        return 'validated-buyer';
    }



    /**
     * Returns the slug of the credit payment account.
     *
     * @return string
     */
    public static function creditAccountSlug()
    {
        return 'credit';
    }



    /**
     * Returns the object of the account which will be used for credit payments.
     *
     * @return Account
     */
    public static function creditAccount()
    {
        return model('account', static::creditAccountSlug());
    }



    /**
     * Maps the transaction to readable title for depoint.
     *
     * @param Transaction $transaction
     *
     * @return string
     */
    public static function mapTransactionTitle(Transaction $transaction)
    {
        $account        = $transaction->account;
        $credit_account = static::creditAccount();
        if ($account->id == $credit_account->id) {
            return $credit_account->title;
        }

        return $transaction->type_text;
    }



    /**
     * Returns max price form the specified posts.
     *
     * @param Collection|LengthAwarePaginator $posts
     *
     * @return string
     */
    public static function postsMaxPrice($posts)
    {
        return Ware::whereIn('sisterhood', $posts->pluck('sisterhood')->toArray())
                   ->max('sale_price')
             ;
    }



    /**
     * Returns min price form the specified posts.
     *
     * @param Collection|LengthAwarePaginator $posts
     *
     * @return string
     */
    public static function postsMinPrice($posts)
    {
        return Ware::whereIn('sisterhood', $posts->pluck('sisterhood')->toArray())
                   ->min('sale_price')
             ;
    }
}
