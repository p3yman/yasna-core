<?php

namespace Modules\Depoint\Providers;

use App\Models\DepointProvider;
use Illuminate\Support\ServiceProvider;

class SpareProvidersServicePrvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Return the slug of the depoint provider role.
     *
     * @return string
     */
    public static function getRoleSlug()
    {
        return 'depoint-provider';
    }



    /**
     * Return a link to a modal form for creating new provider.
     *
     * @return string
     */
    public static function createLink()
    {
        return 'modal:' . route('depoint.manage.providers.single-action', [
                  'model_id' => hashid(0),
                  'action'   => 'create',
             ], false);
    }



    /**
     * Return a link to refresh the grid part of the browsing page of providers.
     *
     * @return string
     */
    public static function refreshGridLink()
    {
        return route('depoint.manage.providers.grid');
    }



    /**
     * Return the link to refresh a row in providers grid view.
     *
     * @param DepointProvider $provider
     *
     * @return string
     */
    public static function refreshRowLink(DepointProvider $provider)
    {
        return route('depoint.manage.providers.single-action', [
             'model_id' => $provider->hashid,
             'action'   => 'browse-row',
        ]);
    }



    /**
     * Return the link to edit an item in providers grid view.
     *
     * @param DepointProvider $provider
     *
     * @return string
     */
    public static function editRowLink(DepointProvider $provider)
    {
        return 'modal:' . route('depoint.manage.providers.single-action', [
                  'model_id' => $provider->hashid,
                  'action'   => 'edit',
             ], false);
    }



    /**
     * Return the link to delete an item in providers grid view.
     *
     * @param DepointProvider $provider
     *
     * @return string
     */
    public static function deleteRowLink(DepointProvider $provider)
    {
        return 'modal:' . route('depoint.manage.providers.single-action', [
                  'model_id' => $provider->hashid,
                  'action'   => 'delete',
                  'option0'  => 'delete',
             ], false);
    }



    /**
     * Return the link to undelete an item in providers grid view.
     *
     * @param DepointProvider $provider
     *
     * @return string
     */
    public static function undeleteRowLink(DepointProvider $provider)
    {
        return 'modal:' . route('depoint.manage.providers.single-action', [
                  'model_id' => $provider->hashid,
                  'action'   => 'delete',
                  'option0'  => 'undelete',
             ], false);
    }



    /**
     * @param DepointProvider $provider
     *
     * @return string
     */
    public static function managersRowLink(DepointProvider $provider)
    {
        return 'modal:' . route('depoint.manage.providers.single-action', [
                  'model_id' => $provider->hashid,
                  'action'   => 'managers',
             ], false);
    }



    /**
     * Array of provider users to be used in a combo/selectize widget.
     *
     * @return array
     */
    public static function allProviderUsersCombo()
    {
        return user()
             ->elector(['role' => static::getRoleSlug()])
             ->get()
             ->map(function ($user) {
                 return [
                      'id'    => $user->hashid,
                      'title' => $user->full_name,
                 ];
             })->toArray()
             ;
    }



    /**
     * return data to be loaded in a combo/selectize.
     *
     * @return array
     */
    public static function combo()
    {
        return model('depoint-provider')
             ->select()
             ->get()
             ->map(function ($user) {
                 return [
                      'id'    => $user->hashid,
                      'title' => $user->name,
                 ];
             })->toArray()
             ;
    }

}
