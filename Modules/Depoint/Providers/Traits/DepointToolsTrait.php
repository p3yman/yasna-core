<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 10:52 PM
 */

namespace Modules\Depoint\Providers\Traits;


trait DepointToolsTrait
{
    /**
     * Returns a URL to access the terms and conditions page.
     *
     * @return string
     */
    public static function termsAndConditionsUrl()
    {
        return route_locale('depoint.front.page', [
             'slug' => 'terms-and-conditions',
        ]);
    }



    /**
     * Returns all gender codes to be shown in forms.
     *
     * @return array
     */
    public static function genders()
    {
        return ['1', '2', '3'];
    }
}
