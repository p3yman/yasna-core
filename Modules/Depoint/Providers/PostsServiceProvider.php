<?php

namespace Modules\Depoint\Providers;

use Illuminate\Support\ServiceProvider;

class PostsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Returns an array containing default elector switches.
     *
     * @return array
     */
    protected static function defaultSelectSwitches()
    {
        return [
             'locale'   => getLocale(),
             'criteria' => 'published',
        ];
    }



    /**
     * Selects posts with the given switches and filled by the default switches.
     *
     * @param array $switches
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function select(array $switches = [])
    {
        $switches = array_default($switches, static::defaultSelectSwitches());
        return post()->elector($switches);
    }
}
