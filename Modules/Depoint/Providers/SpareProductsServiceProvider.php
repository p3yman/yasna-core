<?php

namespace Modules\Depoint\Providers;

use App\Models\DepointProduct;
use Illuminate\Support\ServiceProvider;

class SpareProductsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Return a link to a modal form for creating new product.
     *
     * @return string
     */
    public static function createLink()
    {
        return 'modal:' . route('depoint.manage.products.single-action', [
                  'model_id' => hashid(0),
                  'action'   => 'create',
             ], false);
    }



    /**
     * Return a link to refresh the grid part of the browsing page of products.
     *
     * @return string
     */
    public static function refreshGridLink()
    {
        return route('depoint.manage.products.grid');
    }



    /**
     * Return the link to refresh a row in products grid view.
     *
     * @param DepointProduct $product
     *
     * @return string
     */
    public static function refreshRowLink(DepointProduct $product)
    {
        return route('depoint.manage.products.single-action', [
             'model_id' => $product->hashid,
             'action'   => 'browse-row',
        ]);
    }



    /**
     * Return the link to edit an item in products grid view.
     *
     * @param DepointProduct $product
     *
     * @return string
     */
    public static function editRowLink(DepointProduct $product)
    {
        return 'modal:' . route('depoint.manage.products.single-action', [
                  'model_id' => $product->hashid,
                  'action'   => 'edit',
             ], false);
    }



    /**
     * Return the link to delete an item in products grid view.
     *
     * @param DepointProduct $product
     *
     * @return string
     */
    public static function deleteRowLink(DepointProduct $product)
    {
        return 'modal:' . route('depoint.manage.products.single-action', [
                  'model_id' => $product->hashid,
                  'action'   => 'delete',
                  'option0'  => 'delete',
             ], false);
    }



    /**
     * Return the link to undelete an item in products grid view.
     *
     * @param DepointProduct $product
     *
     * @return string
     */
    public static function undeleteRowLink(DepointProduct $product)
    {
        return 'modal:' . route('depoint.manage.products.single-action', [
                  'model_id' => $product->hashid,
                  'action'   => 'delete',
                  'option0'  => 'undelete',
             ], false);
    }



    /**
     * return data to be loaded in a combo/selectize.
     *
     * @return array
     */
    public static function combo()
    {
        return model('depoint-product')
             ->select()
             ->get()
             ->map(function ($user) {
                 return [
                      'id'    => $user->hashid,
                      'title' => $user->title,
                 ];
             })->toArray()
             ;
    }
}
