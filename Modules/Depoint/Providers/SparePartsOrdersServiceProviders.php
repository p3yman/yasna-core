<?php

namespace Modules\Depoint\Providers;

use App\Models\DepointOrder;
use Illuminate\Support\ServiceProvider;

class SparePartsOrdersServiceProviders extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Return the link to the browse page of orders.
     *
     * @return string
     */
    public static function browseLink()
    {
        return route('depoint.manage.orders');
    }



    /**
     * Return a link to refresh the grid part of the browsing page of orders.
     *
     * @return string
     */
    public static function refreshGridLink()
    {
        return route('depoint.manage.orders.grid');
    }



    /**
     * Return the link to refresh a row in orders grid view.
     *
     * @param DepointOrder $model
     *
     * @return string
     */
    public static function refreshRowLink(DepointOrder $model)
    {
        return route('depoint.manage.orders.single-action', [
             'model_id' => $model->hashid,
             'action'   => 'browse-row',
        ]);
    }



    /**
     * Returns the link to edit an order.
     *
     * @param DepointOrder $model
     *
     * @return string
     */
    public static function editLink(DepointOrder $model)
    {
        return 'modal:' . route('depoint.manage.orders.single-action', [
                  'model_id' => $model->hashid,
                  'action'   => 'edit',
             ], false);
    }



    /**
     * Returns the link to edit an order.
     *
     * @param DepointOrder $model
     *
     * @return string
     */
    public static function deleteLink(DepointOrder $model)
    {
        return 'modal:' . route('depoint.manage.orders.single-action', [
                  'model_id' => $model->hashid,
                  'action'   => 'delete',
             ], false);
    }


}
