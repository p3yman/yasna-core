<?php

namespace Modules\Depoint\Providers;

use App\Models\Posttype;
use Illuminate\Support\ServiceProvider;

class CommentingServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Translates standard `fields` value of a `commenting` post
     * <p>
     * <h4>Translation Rules</h4>
     * <ol>
     * <li>
     * separate fields description with comma (,)
     * </li>
     * <li>
     * start each field description with its "name" (ex: `subject,first_name`)
     * </li>
     * <li>
     * if you want that field to has a label add "-label" to the field description (ex: `subject-label`)
     * </li>
     * <li>
     * if you want that field to has a column size (bootstrap columns) add ":size" (ex: `subject:6`)
     * </li>
     * <li>
     * put no "next-line" in fields string
     * </li>
     * </ol>
     * </p>
     *
     * @param string $fields_string The string to be translated
     *
     * @return array
     */
    public static function translateFields($fields_string)
    {
        if ($fields_string and is_string($fields_string)) {
            $fields = explode_not_empty(',', $fields_string);
            foreach ($fields as $field_index => $field_value) {
                unset($fields[$field_index]);
                $field_value = trim($field_value);

                if (str_contains($field_value, '*')) {
                    $field_value          = str_replace('*', '', $field_value);
                    $tmpField['required'] = true;
                } else {
                    $tmpField['required'] = false;
                }

                if (str_contains($field_value, '-label')) {
                    $field_value       = str_replace('-label', '', $field_value);
                    $tmpField['label'] = true;
                } else {
                    $tmpField['label'] = false;
                }

                if (str_contains($field_value, ':')) {
                    $field_value_parts = explode_not_empty(':', $field_value);
                    $field_value       = $field_value_parts[0];
                    $tmpField['size']  = $field_value_parts[1];
                } else {
                    $tmpField['size'] = '';
                }
                $fields[$field_value] = $tmpField;
                unset($tmpField);
            }
            return $fields;
        }

        return [];
    }



    /**
     * Translates standard `rules` value of a `commenting` post
     * <p>
     * <h4>Translation Rules</h4>
     * <ol>
     * <li>
     * separate fields rules with comma (,)
     * </li>
     * <li>
     * start each field description with its "name" (ex: "subject,first_name")
     * </li>
     * <li>
     * after name of the field, put "=>" and enter rules in the pipeline separated format
     * </li>
     * <li>
     * each rule should be one of laravel standard rules
     * </li>
     * <li>
     * put no "next-line" in rules string
     * </li>
     * </ol>
     * </p>
     *
     * @param string $rules_string  The string to be translated
     * @param bool   $detailed      If "false" this function will return rules of any field in a single item (request
     *                              standard rule)
     *
     * @return array
     */
    public static function translateRules($rules_string, $detailed = true)
    {
        if ($rules_string and is_string($rules_string)) {
            $rules_string = str_replace("\n", '', $rules_string);
            $rules        = explode_not_empty(',', $rules_string);

            foreach ($rules as $index => $rule) {
                $rule = trim($rule);
                unset($rules[$index]);

                $parts = explode_not_empty('=>', $rule);

                if ($parts and is_array($parts) and (count($parts) == 2)) {
                    $field_name = trim($parts[0]);
                    if ($detailed) {
                        $field_rules = explode_not_empty('|', $parts[1]);
                        if (count($field_rules)) {
                            $rules[$field_name] = $field_rules;
                        }
                    } else {
                        $field_rules = $parts[1];
                        if ($field_rules) {
                            $rules[$field_name] = $field_rules;
                        }
                    }
                }
            }

            return $rules;
        }

        return [];
    }



    /**
     * Returns rules for saving comments based on the specified posttype.
     *
     * @param Posttype $posttype
     *
     * @return array
     */
    public static function posttypeRules(Posttype $posttype)
    {
        $rules = [];
        $posttype->spreadMeta();

        if (auth()->guest()) {
            $rules['name'] = ['required'];

            if ($posttype->commenting_enter_email == 'must') {
                $rules['email'] = ['required'];
            }

            if ($posttype->commenting_enter_mobile == 'must') {
                $rules['mobile'] = ['required'];
            }
        }

        return $rules;
    }
}
