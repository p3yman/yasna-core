<?php

namespace Modules\Depoint\Providers;

use App\Models\Post;
use App\Models\State;
use Illuminate\Database\Eloquent\Collection;
use Modules\Cart\Events\CartStatusChanged;
use Modules\Depoint\Events\DepointInsertTracking;
use Modules\Depoint\Events\DepointOrderDeleted;
use Modules\Depoint\Events\DepointOrderModified;
use Modules\Depoint\Events\DepointOrderSaved;
use Modules\Depoint\Http\Middleware\CartForPurchase;
use Modules\Depoint\Http\Middleware\CartNonemptyMiddleware;
use Modules\Depoint\Http\Middleware\OwnCart;
use Modules\Depoint\Http\Middleware\ProviderPanelMiddleware;
use Modules\Depoint\Listeners\SaveReceiptWhenCartStatusChanged;
use Modules\Depoint\Listeners\SendNotificationsWhenDepointOrderDeleted;
use Modules\Depoint\Listeners\SendNotificationsWhenDepointOrderModified;
use Modules\Depoint\Listeners\SendNotificationsWhenDepointOrderSaved;
use Modules\Depoint\Listeners\SendNotificationWhenDepointInsertTracking;
use Modules\Depoint\Providers\Traits\DepointToolsTrait;
use Modules\Depoint\Schedules\SparePartReduceSchedule;
use Modules\Depoint\Services\Module\ModuleHandler;
use Modules\Depoint\ShippingRules\PerWarePriceRule;
use Modules\Manage\Services\ManageDashboardWidget;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class DepointServiceProvider
 *
 * @package Modules\Depoint\Providers
 */
class DepointServiceProvider extends YasnaProvider
{
    use DepointToolsTrait;
    use DepointSorooshanTrait;


    /**
     * Register role samples.
     */
    protected function registerRoleSamples()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('depoint_products')
             ->value('browse, create, edit, delete, bin')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('depoint_providers')
             ->value('browse, create, edit, delete, bin')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('depoint_spare_parts')
             ->value('browse, create, edit, delete, bin')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('depoint_orders')
             ->value('browse, create, edit, delete, bin')
        ;

        module('users')
             ->service('role_sample_modules')
             ->add('resume')
             ->value('manage')
        ;
    }



    /**
     * Register providers.
     */
    protected function registerProviders()
    {
        $this->addProvider(SpareProductsServiceProvider::class);
        $this->addProvider(SpareProvidersServicePrvider::class);
        $this->addProvider(SparePartsServiceProvider::class);
        $this->addProvider(SparePartsOrdersServiceProviders::class);
        $this->addProvider(PostsServiceProvider::class);
        $this->addProvider(CommentingServiceProvider::class);
        $this->addProvider(ShopServiceProvider::class);
        $this->addProvider(DrawingServiceProvider::class);
    }



    /**
     * Register aliases.
     */
    protected function registerAliases()
    {
        $this->addAlias('SpareProductsTools', SpareProductsServiceProvider::class);
        $this->addAlias('SpareProvidersTools', SpareProvidersServicePrvider::class);
        $this->addAlias('SparePartsTools', SparePartsServiceProvider::class);
        $this->addAlias('SparePartsOrdersTools', SparePartsOrdersServiceProviders::class);
    }



    /**
     * register sidebar.
     */
    protected function registerSidebar()
    {
        module('manage')
             ->service('sidebar')
             ->add('tickets')
             ->blade('depoint::manage.layouts.sidebar')
             ->order(60)
        ;
        module('manage')
             ->service('sidebar')
             ->add('report')
             ->blade('depoint::manage.report.sidebar')
             ->order(61)
        ;
        module('manage')
             ->service('sidebar')
             ->add('report')
             ->blade($this->module()->getBladePath('manage.credit-payments.carts.sidebar'))
             ->order(62)
        ;
        module('manage')
             ->service('sidebar')
             ->add('resume')
             ->blade('depoint::resume.layouts.sidebar')
             ->order(63)
            ;
    }



    /**
     * register modal traits.
     */
    protected function registerModelTraits()
    {
        $this->addModelTrait('UserDepointTrait', 'User');
        $this->addModelTrait('PostDepointTrait', 'Post');
        $this->addModelTrait('TicketDepointTrait', 'Ticket');
        $this->addModelTrait('WareDepointTrait', 'Ware');
        $this->addModelTrait('CartDepointTrait', 'Cart');
    }



    /**
     * register event listeners
     */
    protected function registerEventListeners()
    {
        $this->listen(DepointOrderSaved::class, SendNotificationsWhenDepointOrderSaved::class);
        $this->listen(DepointOrderModified::class, SendNotificationsWhenDepointOrderModified::class);
        $this->listen(DepointOrderDeleted::class, SendNotificationsWhenDepointOrderDeleted::class);
        $this->listen(CartStatusChanged::class, SaveReceiptWhenCartStatusChanged::class);
    }



    /**
     * Registers schedules.
     */
    protected function registerSchedules()
    {
        $this->addSchedule(SparePartReduceSchedule::class);
    }



    /**
     * Adds item(s) to the posts rows actions.
     *
     * @param array $arguments
     */
    public static function postsBrowseRowActions($arguments)
    {
        $model = $arguments['model'];
        $url   = route('depoint.manage.posts.color-images', ['hashid' => $model->hashid], false);

        service('posts:row_actions')
             ->add('images')
             ->icon('picture-o')
             ->trans('depoint::general.colors-images')
             ->link("modal:$url")
             ->condition($model->hasColorImages())
             ->order(13)
        ;
    }



    /**
     * Handler for the Posts Browse Page
     *
     * @param array $arguments
     */
    public static function postsBrowseHandler(array $arguments)
    {
        $posttype = $arguments['posttype'];
        $module   = module('depoint');

        service("manage:template_assets")
             ->add("draw")
             ->link($module->getAssetPath('js/draw.min.js'))
             ->order(60)
             ->condition($posttype->has('event'))
        ;

        service("posts:browse_headings")
             ->add('draw')
             ->trans($module->getTrans('cart.drawing.draw.singular'))
             ->blade($module->getBladePath('manage.posts.browse-draw-column'))
             ->order(31)
             ->condition($posttype->has('event'))
        ;
    }



    /**
     * Posts Handlers
     *
     * @return void
     */
    protected function registerPostHandlers()
    {
        $module = "posts";

        service("$module:row_action_handlers")
             ->add($this->moduleName())
             ->method('depoint:postsBrowseRowActions')
        ;

        service("$module:browse_headings_handlers")
             ->add($this->moduleName())
             ->method('depoint:postsBrowseHandler')
        ;
    }



    /**
     * Register services of posts browse pages.
     */
    public function registerPostBrowseServices()
    {
        $module_handler = ModuleHandler::createFromNamespace(get_class($this));


        module('posts')
             ->service('browse_head_blade')
             ->add('browse_assets')
             ->blade($module_handler->bladePath('manage.posts.browse-assets'))
        ;
    }



    /**
     * Registers middlewares.
     */
    protected function registerMiddlewares()
    {
        $this->addMiddleware('provider_panel', ProviderPanelMiddleware::class);
        $this->addMiddleware('cart_nonempty', CartNonemptyMiddleware::class);
        $this->addMiddleware('own_cart', OwnCart::class);
        $this->addMiddleware('cart_for_purchase', CartForPurchase::class);
    }



    /**
     * Registers handlers for posts editor page.
     */
    protected function registerPostsEditorHandlers()
    {
        service('posts:editor_handlers')
             ->add('depoint')
             ->method($this->moduleName() . ':postsEditorHandler')
        ;
    }



    /**
     * Handler for Post Editor
     *
     * @param Post $model
     */
    public static function postsEditorHandler(Post $model)
    {
        $module = depoint()->module();

        service('posts:editor_main')
             ->add('depoint-products-details')
             ->blade($module->getBladePath('manage.posts.details-posts'))
             ->condition($model->has('depoint_products_details'))
             ->order(60)
        ;
    }



    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerRoleSamples();
        $this->registerProviders();
        $this->registerAliases();
        $this->registerSidebar();
        $this->registerModelTraits();
        $this->registerEventListeners();
        $this->registerSchedules();
        $this->registerPostHandlers();
        $this->registerPostBrowseServices();
        $this->registerMiddlewares();
        $this->registerPostsEditorHandlers();
        $this->registerWidgets();
        $this->registerDefaultWidgets();
        $this->registerShippingRules();
        $this->registerSorooshan();
    }



    /**
     * Returns a new instance of the `Modules\Depoint\Providers\CommentingServiceProvider` class.
     *
     * @return CommentingServiceProvider
     */
    public function commenting()
    {
        return new CommentingServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Depoint\Providers\ShopServiceProvider` class.
     *
     * @return ShopServiceProvider
     */
    public function shop()
    {
        return new ShopServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Depoint\Providers\RouteServiceProvider` class.
     *
     * @return RouteServiceProvider
     */
    public function routes()
    {
        return new RouteServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Depoint\Providers\CartServiceProvider` class.
     *
     * @return CartServiceProvider
     */
    public function cart()
    {
        return new CartServiceProvider($this->app);
    }



    /**
     * Returns a new instance of the `Modules\Depoint\Providers\DrawingServiceProvider` class.
     *
     * @return DrawingServiceProvider
     */
    public function drawing()
    {
        return new DrawingServiceProvider($this->app);
    }



    /**
     * return a pretty string to display festival date
     *
     * @param string $start_date this is a valid date
     * @param string $ends_date  this is a valid date
     *
     * @return string
     */
    public static function eventEchoDate($start_date, $ends_date)
    {
        if (echoDate($ends_date, 'Y') == echoDate($start_date, 'Y')) {
            return ad(echoDate($start_date,
                      'j F ')) . currentModule()->trans("general.until") . " " . ad(echoDate($ends_date,
                      'j F ')) . " " . ad(echoDate($ends_date, 'Y'));
        } else {
            return ad(echoDate($start_date,
                      'j F Y')) . " " . currentModule()->trans("general.until") . " " . ad(echoDate($ends_date,
                      'j F Y'));
        }
    }



    /**
     * Get the value of the closure.
     *
     * @param mixed $variable
     * @param array ...$closure_parameters
     *
     * @return mixed
     */
    public static function getValue($variable, ...$closure_parameters)
    {
        return is_closure($variable) ? $variable(...$closure_parameters) : $variable;
    }



    /**
     * Returns an array to be used in a combo to select product posts.
     */
    public static function getProductInputCombo()
    {
        $blank_item = ['id' => '', 'title' => ''];
        $posts      = PostsServiceProvider::select([
             'type'   => 'products',
             'locale' => 'all',
        ])->get()
        ;

        $array = $posts->map(function ($post) {
            return [
                 'id'    => $post->hashid,
                 'title' => $post->title,
            ];

        })->toArray()
        ;


        return array_merge([$blank_item], $array);
    }



    /**
     * Return active genders codes.
     *
     * @return array
     */
    public static function activeGendersCodes()
    {
        return ['1', '2', '3'];
    }



    /**
     * Return active genders codes.
     *
     * @return array
     */
    public static function getActiveGendersCodes()
    {
        return self::activeGendersCodes();
    }



    /**
     * Return active genders transes.
     *
     * @return array
     */
    public static function activeGendersTranses()
    {
        $codes   = self::activeGendersCodes();
        $transes = [];

        foreach ($codes as $code) {
            $transes[$code] = depoint()->module()->getTrans('form.gender.' . $code);
        }

        return $transes;
    }



    /**
     * Returns and array of possible download links for landing.
     *
     * @return array
     * @throws \Nwidart\Modules\Exceptions\InvalidAssetPath
     */
    public static function landingDownloadLinksMap()
    {
        $module = module('depoint');

        return [
             [
                  "title" => currentModule()->trans('general.download-manuals'),
                  "image" => $module->getAsset('images/download-help.svg'),
                  "input" => "manuals",
             ],
             [
                  "title" => currentModule()->trans('general.download-catalog'),
                  "image" => $module->getAsset('images/download-catalog.svg'),
                  "input" => "catalog",
             ],
        ];
    }



    /**
     * Returns the download links of a product post to be used in a landing page.
     * <br>
     * _The given post is the source post of a landing post._
     *
     * @param Post $post
     *
     * @return array
     * @throws \Nwidart\Modules\Exceptions\InvalidAssetPath
     */
    public static function getLandingDownloadLinks(Post $post)
    {
        $map    = static::landingDownloadLinksMap();
        $result = [];

        foreach ($map as $map_item) {
            $file_hashid  = $post->getMeta($map_item['input']);
            $download_url = fileManager()
                 ->file($file_hashid)
                 ->getDownloadUrl()
            ;

            if (!$download_url) {
                continue;
            }

            $result[] = [
                 'title' => $map_item['title'],
                 'image' => $map_item['image'],
                 'link'  => $download_url,
            ];
        }


        return $result;
    }



    /**
     * Returns the home URL.
     *
     * @param string|null $locale
     *
     * @return string
     */
    public static function homeUrl($locale = null)
    {
        if ($locale) {
            return route('depoint.front.home-with-locale', ['locale' => $locale]);
        } else {
            return route('depoint.front.home');
        }
    }



    /**
     * Returns a list of site locales.
     *
     * @return array
     */
    public static function siteLocales()
    {
        return availableLocales();
    }



    /**
     * Returns the URL of a flag with the specified locale.
     *
     * @param string $locale
     *
     * @return string
     * @throws \Nwidart\Modules\Exceptions\InvalidAssetPath
     */
    public static function getLocaleFlagUrl(string $locale)
    {
        return depoint()->module()->getAsset("images/flags/$locale.png");
    }



    /**
     * Returns the title of the specified the locale and in that locale.
     *
     * @param string $locale
     *
     * @return array|null|string
     */
    public static function getLocaleTitle(string $locale)
    {
        return depoint()->module()->getTrans('general.locale-title', [], $locale);
    }



    /**
     * Provides URL with the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    public static function getLocaleLink(string $locale)
    {
        $current_route = request()->route();
        if (!$current_route->hasParameter('lang')) {
            return static::homeUrl($locale);
        }

        $forced_parameters = depoint()->routes()->getForcedUrlparameters($locale);


        if ($forced_parameters === false) {
            return static::homeUrl($locale);
        }

        if (is_string($forced_parameters)) {
            return $forced_parameters;
        }

        $current_parameters = $current_route->parameters();
        $parameters         = array_merge($current_parameters, $forced_parameters, ['lang' => $locale]);

        return action('\\' . $current_route->getActionName(), $parameters);
    }



    /**
     * Returns a combo array of the products details posts.
     *
     * @param Post|null $post
     *
     * @return array
     */
    public static function getProductsDetailsCombo(?Post $post = null)
    {
        $select_data = [
             'type' => 'products-details',
        ];

        if ($post and $post->exists) {
            $select_data['locale'] = $post->locale;
        }

        return PostsServiceProvider::select($select_data)
                                   ->orderByDesc('published_at')
                                   ->get()
                                   ->map(function ($user) {
                                       return [
                                            'id'    => $user->hashid,
                                            'title' => $user->title,
                                       ];
                                   })->toArray()
             ;
    }



    /**
     * Returns a list of specs pivot which are grouped by their parent ids.
     *
     * @param Post $post
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getPostSpecsParentItems(Post $post)
    {
        return $post->specsPivots()->whereNull('hid_at')->get()->groupBy(function ($item) {
            return $item->spec->parent_id;
        })
             ;
    }



    /**
     * Returns array to be used in combo of provinces.
     *
     * @param string $value_field Name of the Value Field
     * @param string $title_field Name of the Title Field
     *
     * @return array
     */
    public static function provincesCombo(string $value_field = 'id', string $title_field = 'title')
    {
        return model('state')
             ->where('parent_id', 0)
             ->get()
             ->map(function (State $state) use ($value_field, $title_field) {
                 return [
                      $value_field => $state->hashid,
                      $title_field => $state->title,
                 ];
             })->toArray()
             ;
    }



    /**
     * Returns an array to be used in combo for cities of the given state.
     *
     * @param State  $province
     * @param string $value_field Name of the Value Field
     * @param string $title_field Name of the Title Field
     *
     * @return array
     */
    public static function provinceCitiesCombo(
         State $province,
         string $value_field = 'id',
         string $title_field = 'title'
    ) {
        return $province
             ->cities()
             ->get()
             ->map(function (State $state) use ($value_field, $title_field) {
                 return [
                      $value_field => $state->hashid,
                      $title_field => $state->title,
                 ];
             })->toArray()
             ;
    }



    /**
     * Returns list of addressed of the current client.
     *
     * @return Collection
     */
    public static function clientAddresses()
    {
        return addresses()
             ->user(user())
             ->orderByDesc('created_at')
             ->get()
             ;
    }



    /**
     * Register Dashboard Widgets
     */
    public function registerWidgets()
    {
        module('manage')
             ->service('widgets_handler')
             ->add('depoint')
             ->method('depoint:handleWidgets')
             ->order(90)
        ;

        /*-----------------------------------------------------
              |Chart.js ...
         */
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("manage:libs/vendor/Chart.js/dist/Chart.js")
             ->order(35)
        ;
        /*-----------------------------------------------
        | Sparkline ...
           */
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("manage:libs/vendor/sparkline/index.js")
             ->order(11)
        ;
    }



    /**
     * Handle Dashboard Widgets
     */
    public static function handleWidgets()
    {
        $service = 'widgets';

        module('manage')
             ->service($service)
             ->add('register-in-month')
             ->blade('depoint::manage.widgets.register')
             ->trans('depoint::widgets.register')
             ->color('pink')
             ->icon('user-circle-o')
             ->order(30)
        ;

        module('manage')
             ->service($service)
             ->add('gender-age')
             ->blade('depoint::manage.widgets.gender-age')
             ->trans('depoint::widgets.gender-age')
             ->color('blue')
             ->icon('users')
             ->order(31)
        ;

        module('manage')
             ->service($service)
             ->add('sales')
             ->blade('depoint::manage.widgets.sale-table')
             ->trans('depoint::widgets.sales')
             ->color('blue')
             ->icon('chart-line')
             ->order(31)
        ;

        module('manage')
             ->service($service)
             ->add('discount-codes')
             ->blade('depoint::manage.widgets.discount-codes')
             ->trans('depoint::widgets.discount-codes')
             ->color('yellow')
             ->icon('dollar')
             ->order(32)
        ;
    }



    /**
     * register default widgets
     */
    public function registerDefaultWidgets()
    {
        ManageDashboardWidget::addDefault('gender-age', 3, 0, 0);
        ManageDashboardWidget::addDefault('discount-codes', 3, 3, 0);
        ManageDashboardWidget::addDefault('sales', 3, 3, 0);
        ManageDashboardWidget::addDefault('register-in-month', 3, 0, 0);
    }



    /**
     * Registers the shipping rules.
     */
    protected function registerShippingRules()
    {
        shipping()->registerRule(PerWarePriceRule::class);
    }



    /**
     * Maps the specified color to a color which can be used in the front side.
     *
     * @param string $color
     *
     * @return string
     */
    public function mapColor(string $color)
    {
        $map = [
             'warning' => "yellow",
             'pink'    => "purple",
             'danger'  => "orange",
             'primary' => "blue",
             'success' => "green",
             'info'    => "teal",
        ];

        return ($map[$color] ?? $color);
    }



    /**
     * Returns an array to be used while selecting cities in a combo.
     *
     * @return array
     */
    public static function getCitiesCombo()
    {
        return model('state')
             ->allCities()
             ->get()
             ->map(function (State $state) {
                 return [
                      'id'    => $state->hashid,
                      'title' => $state->province->title . ' / ' . $state->title,
                 ];
             })->toArray()
             ;
    }
}
