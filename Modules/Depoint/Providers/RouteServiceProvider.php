<?php

namespace Modules\Depoint\Providers;

use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Forced values to be used in sisterhood links
     *
     * @var array
     */
    private static $forced_url_parameters = [];
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }



    /**
     * Forces variables to be used while generating url of sister pages.
     * <p>
     *     <i>Note: Language should be specified in parameters.</i>
     *     <ul>
     *         <li>
     *             Correct Example: <code>['en' => ['name' => 'John']];</code>
     *         </li>
     *         <li>
     *             Wrong Example: <code>['name' => 'John'];</code>
     *         </li>
     *     </ul>
     * </p>
     *
     * @param array $parameters
     *
     * @return void
     */
    public static function forceUrlParameters($parameters = [])
    {
        $site_locales = availableLocales();

        foreach ($parameters as $locale => $fields) {
            if (!in_array($locale, $site_locales)) {
                continue;
            }

            static::$forced_url_parameters = array_merge_recursive(static::$forced_url_parameters, [
                 $locale => $fields,
            ]);
        }
    }



    /**
     * Returns forced variables to be used while generating URL of sister pages.
     *
     * @param string $locale
     *
     * @return array|mixed
     */
    public static function getForcedUrlParameters(string $locale = '')
    {
        if ($locale) {
            return (static::$forced_url_parameters[$locale] ?? []);
        }

        return static::$forced_url_parameters;
    }
}
