<?php

namespace Modules\Depoint\Providers;

use App\Models\Cart;
use App\Models\User;
use Illuminate\Support\ServiceProvider;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    /**
     * The Name of the Session of the Cart
     *
     * @var string
     */
    protected static $session_name = 'cart';



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Returns an instance of the cart model.
     *
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return Cart
     */
    public static function cartModel($id = 0, $with_trashed = false)
    {
        return model('cart', $id, $with_trashed);
    }



    /**
     * Returns the cart of the specified user.
     *
     * @param User $user
     * @param bool $create Weather the returning cart should be created or not.
     *
     * @return Cart
     */
    public static function getUsersCart(User $user, $create = false)
    {
        $conditions = array_merge(static::newCartGeneralData(), [
             'user_id'   => $user ? $user->id : 0,
             'raised_at' => null,
        ]);

        if ($create) {
            return static::cartModel()->firstOrCreate($conditions);
        } else {
            return static::cartModel()->firstOrNew($conditions);
        }
    }



    /**
     * Clears the cart session.
     */
    public static function clearSession()
    {
        return session()->forget(static::$session_name);
    }



    /**
     * Returns the cart identifier if it is saved in the session.
     *
     * @return string|null
     */
    public static function getCartIdFromSession()
    {
        if (session()->exists(static::$session_name)) {
            return session()->get(static::$session_name);
        }

        return null;
    }



    /**
     * Returns an array of attributes which should are generally save for carts.
     *
     * @return array
     */
    protected static function newCartGeneralData()
    {
        return [
             'currency' => depoint()->shop()->baseCurrency(),
             'locale'   => getLocale(),
        ];
    }



    /**
     * Returns an instance of the cart model
     *
     * @param bool      $create Weather the instance should be saved in database or not.
     * @param User|null $user
     *
     * @return Cart
     */
    protected static function newCart(bool $create, User $user = null)
    {
        $attributes = array_merge(static::newCartGeneralData(), [
             'user_id' => $user ? $user->id : 0,
        ]);

        if ($create) {
            $new_cart = static::cartModel()->create($attributes);
            session()->put(static::$session_name, $new_cart->hashid);
            return $new_cart;
        } else {
            return static::cartModel()->fill($attributes);
        }
    }



    /**
     * Returns the cart which is saved in session.
     *
     * @param bool $create
     *
     * @return Cart
     */
    public static function getSessionCart($create = false)
    {
        if (session()->exists(static::$session_name)) {
            $cart_hashid = session()->get(static::$session_name);
            $found_cart  = static::cartModel()->grabHashid($cart_hashid);
            if ($found_cart->exists) {
                return $found_cart;
            }
        }

        return static::newCart($create);
    }



    /**
     * Returns the proper cart of the current client.
     *
     * @param User|null $user
     * @param bool      $create
     *
     * @return Cart
     */
    public static function findActiveCart(User $user = null, $create = false)
    {
        if ($user and $user->exists) {
            $cart = static::getUsersCart($user, $create);
        } elseif (!auth()->guest()) {
            $cart = static::getUsersCart(user(), $create);
        } else {
            $cart = static::getSessionCart($create);
        }

        return $cart;
    }



    /**
     * Add the specified ware to the active cart.
     *
     * @param int|string $hashid_or_id Identifier of the ware
     * @param int        $count
     *
     * @return bool
     */
    public static function addToActiveCart($hashid_or_id, $count = 1)
    {
        $cart = static::findActiveCart(user(), true);

        return $cart->addOrder($hashid_or_id, $count);
    }
}
