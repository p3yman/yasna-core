<?php

namespace Modules\Depoint\Providers;

use Modules\Depoint\Services\Sorooshan\Sorooshan;

trait DepointSorooshanTrait
{
    /**
     * Registers the needs for the Sorooshan service.
     */
    protected function registerSorooshan()
    {
        $this->registerSorooshanSingleton();
    }



    /**
     * Registers the singleton for the Sorooshan service.
     */
    protected function registerSorooshanSingleton()
    {
        $this->app->singleton('depoint.sorooshan', function () {
            return app()->make(Sorooshan::class);
        });
    }
}
