<?php

namespace Modules\Depoint\Providers;

use App\Models\Cart;
use App\Models\Receipt;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use phpseclib\Math\BigInteger;

class DrawingServiceProvider extends ServiceProvider
{
    protected static $code_length = 20;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Returns the model of the receipts
     *
     * @return Receipt
     */
    public static function receiptModel()
    {
        return model('receipt');
    }



    /**
     * Creates a code based on timestamp and amount.
     *
     * @param string $timestamp
     * @param string $amount
     *
     * @return string
     */
    public static function createCode(string $timestamp, $amount)
    {
        $timestamp           = str_split(ltrim($timestamp, '1'));
        $invoice_price_count = strlen($amount);
        $amount              = str_split($amount);
        $unique_code         = $invoice_price_count;

        // Reverse invoice price reduce per character from 9
        $reverse_invoice_price = '';
        for ($i = 0; $i < $invoice_price_count; $i++) {
            $reverse_invoice_price .= (9 - $amount[$i]);
        }

        // Add numbers to reversed amount characters
        // until the characters' number raise to 0
        $invoice_price_be_change = $reverse_invoice_price;
        for ($i = 0, $a = 1; $i < 9 - $invoice_price_count; $i++) {
            $invoice_price_be_change .= $a++;
        }

        // Mix timestamp and invoice
        for ($i = 0; $i < 9; $i++) {
            $unique_code .= $timestamp[$i];
            $unique_code .= $invoice_price_be_change[$i];
        }

        // Change 1st and 5th characters character of the unique code
        $character1     = $unique_code[0];
        $character5     = $unique_code[4];
        $unique_code[0] = $character5;
        $unique_code[4] = $character1;

        // Controller number
        $starter = 9 - $unique_code[0];
        if ($starter <= 2) {
            $starter = 8;
        }

        // Create Controller number
        $sum = 0;
        for ($i = 18; $i > 0; $i--) {
            if ($starter < 2) {
                $starter = 8;
            }
            $sum += ($unique_code[$i] * $starter);
            $starter--;
        }
        $sum += $unique_code[0];
        $sum = str_split($sum);

        // Create unique code character 20
        $unique_code .= (9 - $sum[count($sum) - 1]);

        // Change 20th and 15th unique code character
        $character15     = $unique_code[14];
        $character20     = $unique_code[19];
        $unique_code[14] = $character20;
        $unique_code[19] = $character15;

        // Separator
        $final_uniq_code = '';
        for ($i = 0; $i <= 19; $i++) {
            if ($i == 4 or $i == 9 or $i == 14) {
                $seperator = '-';
            } else {
                $seperator = '';
            }
            $final_uniq_code .= $unique_code[$i] . $seperator;
        }

        return $final_uniq_code;
    }



    /**
     * Checks the given code and returns:
     * <ul>
     * <li>
     * `false` if the code is not valid
     * </li>
     * <li>
     * `array` containing `price` and `date` if the code is valid
     * </li>
     * </ul>
     *
     * @param string|BigInteger $code
     *
     * @return array|bool
     */
    public static function checkCode($code)
    {
        if (strlen($code) != static::$code_length or !is_numeric($code)) {
            return false;
        }

        $code = str_split($code);
        $data = [];

        // Change 20th and 15th unique code character
        $character15 = $code[14];
        $character20 = $code[19];
        $code[14]    = $character20;
        $code[19]    = $character15;

        $controller = 9 - $code[19];

        // Controller number
        $starter = 9 - $code[0];
        if ($starter <= 2) {
            $starter = 8;
        }

        // Create controller number
        $sum = 0;
        for ($i = 18; $i > 0; $i--) {
            if ($starter < 2) {
                $starter = 8;
            }
            $sum += ($code[$i] * $starter);
            $starter--;
        }
        $sum += $code[0];
        $sum = str_split($sum);

        if ($sum[count($sum) - 1] != $controller) {
            return false;
        }

        // Change 1st and 5th unique code character
        $character1 = $code[0];
        $character5 = $code[4];
        $code[0]    = $character5;
        $code[4]    = $character1;

        $data['date'] = 1
             . $code[1]
             . $code[3]
             . $code[5]
             . $code[7]
             . $code[9]
             . $code[11]
             . $code[13]
             . $code[15]
             . $code[17];

        $price = $code[2]
             . $code[4]
             . $code[6]
             . $code[8]
             . $code[10]
             . $code[12]
             . $code[14]
             . $code[16]
             . $code[18];

        $price         = str_split($price);
        $data['price'] = '';
        for ($i = 0; $i < $code[0]; $i++) {
            $data['price'] .= (9 - $price[$i]);
        }

        return $data;
    }



    /**
     * Creates a receipt item.
     *
     * @param string|int|float $amount
     * @param string           $timestamp
     * @param int|null         $invoice_id
     *
     * @return Receipt
     */
    public static function storeReceipt($amount, $timestamp, ?Cart $cart = null)
    {
        $cart = ($cart ?? model('cart'));
        $user = ($cart->user->exists) ? $cart->user : model('user');

        return depoint()
             ->drawing()
             ->receiptModel()
             ->batchSave([
                  'purchased_at'     => Carbon::parse($timestamp)->toDateTimeString(),
                  'invoice_id'       => $cart->id,
                  'user_id'          => $user->id,
                  'purchased_amount' => $amount,
             ])
             ;
    }
}
