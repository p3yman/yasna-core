<?php

namespace Modules\Depoint\Providers;

use App\Models\DepointSparePart;
use Illuminate\Support\ServiceProvider;

class SparePartsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * return the slug of the posttype which should be created to manage files of spare parts.
     *
     * @return string
     */
    public static function posttypeSlug(): string
    {
        return 'spare-parts';
    }



    /**
     * return the link to browse spare parts.
     *
     * @return string
     */
    public static function browseLink($tab = '')
    {
        return route('depoint.manage.spare-parts', ['tab' => $tab]);
    }



    /**
     * return the link to create new spare part.
     *
     * @return string
     */
    public static function createLink()
    {
        return route('depoint.manage.spare-parts.single-action', [
             'model_id' => hashid(0),
             'act'      => 'create',
        ]);
    }



    /**
     * Return a link to refresh the grid part of the browsing page of spare parts.
     *
     * @return string
     */
    public static function refreshGridLink()
    {
        return route('depoint.manage.spare-parts.grid');
    }



    /**
     * Refresh grid URL with the current query string.
     *
     * @return string
     */
    public static function refreshGridFullUrl()
    {
        $query_string = request()->getQueryString();
        return static::refreshGridLink() . ($query_string ? ('?' . $query_string) : '');
    }



    /**
     * return the link to edit the given spare part.
     *
     * @return string
     */
    public static function editLink(DepointSparePart $model)
    {
        return route('depoint.manage.spare-parts.single-action', [
             'model_id' => $model->hashid,
             'act'      => 'edit',
        ]);
    }



    /**
     * return the link to edit the given spare part from the grid.
     *
     * @return string
     */
    public static function editRowLink(DepointSparePart $model)
    {
        $full_url    = static::editLink($model);
        $link_prefix = url('/') . '/';
        $link        = str_after($full_url, $link_prefix);

        return 'url:' . $link;
    }



    /**
     * Return the link to refresh a row in spare parts grid view.
     *
     * @param DepointSparePart $model
     *
     * @return string
     */
    public static function refreshRowLink(DepointSparePart $model)
    {
        return route('depoint.manage.spare-parts.single-action', [
             'model_id' => $model->hashid,
             'act'      => 'browse-row',
        ]);
    }



    /**
     * Return the link to delete an item in the grid view.
     *
     * @param DepointSparePart $model
     *
     * @return string
     */
    public static function deleteRowLink(DepointSparePart $model)
    {
        return 'modal:' . route('depoint.manage.spare-parts.single-action', [
                  'model_id' => $model->hashid,
                  'action'   => 'delete',
                  'option0'  => 'delete',
             ], false);
    }



    /**
     * Return the link to undelete an item in the grid view.
     *
     * @param DepointSparePart $model
     *
     * @return string
     */
    public static function undeleteRowLink(DepointSparePart $model)
    {
        return 'modal:' . route('depoint.manage.spare-parts.single-action', [
                  'model_id' => $model->hashid,
                  'action'   => 'delete',
                  'option0'  => 'undelete',
             ], false);
    }



    /**
     * return link of the form to add a spare part to cart.
     *
     * @param DepointSparePart $model
     *
     * @return string
     */
    public static function addToCartLink(DepointSparePart $model)
    {
        return 'modal:' . route('depoint.manage.spare-parts.single-action', [
                  'model_id' => $model->hashid,
                  'action'   => 'add-to-cart',
             ], false);
    }



    /**
     * Returns the link to refresh the products part in the spare part editor.
     *
     * @param DepointSparePart $model
     *
     * @return string
     */
    public static function editorProductsLink(DepointSparePart $model)
    {
        return route('depoint.manage.spare-parts.single-action', [
             'model_id' => $model->hashid,
             'action'   => 'editor-products',
        ]);
    }



    /**
     * return link to add multiple spare parts to cart at once.
     *
     * @return string
     */
    public static function massAddToCartLink()
    {
        return 'modal:' . route('depoint.manage.spare-parts.mass-action', [
                  'action' => 'add-to-cart',
             ], false);
    }



    /**
     * check if the logged in user can access to the cart environment or not.
     *
     * @return bool
     */
    public static function cartIsAccessible()
    {
        return model('depoint-cart')->canAccess();
    }



    /**
     * check if the logged in user can not access to the cart environment.
     *
     * @return bool
     */
    public static function cartIsNotAccessible()
    {
        return !static::cartIsAccessible();
    }



    /**
     * return an array of mass actions to be used in the spare parts browse page.
     *
     * @return array
     */
    public static function browseMassActions()
    {
        $actions = [];
        if (static::cartIsAccessible()) {
            $actions [] = [
                 'cart-plus',
                 trans_safe('depoint::spare-parts.spare-part.add-to-cart'),
                 static::massAddToCartLink(),
            ];
        }


        return $actions;
    }



    /**
     * Returns uploaders for spare parts.
     *
     * @return array
     */
    public static function sparePartUploaders()
    {
        return [
             'main'    => fileManager()
                  ->uploader(SparePartsServiceProvider::posttypeSlug())
                  ->posttypeConfig('main_files')
                  ->withoutAjaxRemove()
                  ->compressed()
                  ->onEachUploadComplete('mainFileAdded')
             ,
             'preview' => fileManager()
                  ->uploader(SparePartsServiceProvider::posttypeSlug())
                  ->posttypeConfig('preview_file')
                  ->withoutAjaxRemove()
                  ->image()
                  ->onEachUploadComplete('previewFileAdded')
             ,
        ];
    }
}
