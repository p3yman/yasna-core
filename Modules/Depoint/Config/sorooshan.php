<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/22/19
 * Time: 5:21 PM
 */

return [
     'wsdl'     => env('SOROOSHAN_WSDL'),
     'username' => env('SOROOSHAN_USERNAME'),
     'password' => env('SOROOSHAN_PASSWORD'),

     'service-type-codes' => [
          'SV00000001',
          'SV00000002',
     ],
];
