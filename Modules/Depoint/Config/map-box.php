<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/3/18
 * Time: 6:00 PM
 */

return [
     'token' => env(
          'MAPBOX_TOKEN',
          'pk.eyJ1IjoieWFzbmF0ZWFtIiwiYSI6ImNqazlib2gzcjJ1NGgzcW1sYXF5aDZoM3kifQ.BLoLXT33-GG6NwVp7SaNQQ'
     ),
];
