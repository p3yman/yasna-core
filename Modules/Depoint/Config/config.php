<?php

return [
     'name' => 'Depoint',


     'product-colors' => [
          'black'         => '#555',
          'gray'          => '#777',
          'white'         => '#fff',
          'leather-white' => '#fff',
     ],


     'map-box'   => include(__DIR__ . DIRECTORY_SEPARATOR . 'map-box.php'),
     'sorooshan' => include(__DIR__ . DIRECTORY_SEPARATOR . 'sorooshan.php'),
];
