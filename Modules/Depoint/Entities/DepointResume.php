<?php

namespace Modules\Depoint\Entities;

use Modules\Depoint\Entities\Traits\DepointResumeMetaTrait;
use Modules\Depoint\Entities\Traits\DepointResumeStatusTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @property string $name_first
 * @property string $name_last
 * @property string $marital_status
 * @property string $military_status
 */
class DepointResume extends YasnaModel
{
    use DepointResumeStatusTrait;
    use DepointResumeMetaTrait;
    use SoftDeletes;



    /**
     * get the full name of worker
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->name_first . " " . $this->name_last;
    }



    /**
     * get the marital status of worker
     *
     * @return string
     */
    public function getMaritalAttribute()
    {
        if ($this->marital_status == 1){
            return trans_safe('depoint::resume.info.single');
        }
        if ($this->marital_status == 2){
            return trans_safe('depoint::resume.info.married');
        }

        return "";
    }



    /**
     * get the military status
     *
     * @return string
     */
    public function getMilitaryAttribute()
    {
        if ($this->military_status == 1){
            return trans_safe('depoint::resume.info.did_not_do');
        }

        if ($this->military_status == 2){
            return trans_safe('depoint::resume.info.do_military');
        }

        if ($this->military_status == 3){
            return trans_safe('depoint::resume.info.exempt');
        }
    }
}
