<?php

namespace Modules\Depoint\Entities;

use App\Models\DepointOrderItem;
use App\Models\DepointProvider;
use App\Models\DepointSparePart;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Depoint\Entities\Traits\DepointOrderPermitTrait;
use Modules\Depoint\Entities\Traits\DepointOrderStatusTrait;
use Modules\Depoint\Observers\DepointOrderObserver;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepointOrder extends YasnaModel
{
    use SoftDeletes;
    use DepointOrderPermitTrait;
    use DepointOrderStatusTrait;
    protected $guarded = ['id', 'code'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
         'submitted_at' => 'datetime',
         'sent_at'      => 'datetime',
    ];



    /**
     * Standard Laravel One-to-Many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }



    /**
     * Standard Laravel One-to-Many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function depointProvider()
    {
        return $this->belongsTo(DepointProvider::class);
    }



    /**
     * Mirror Method for `depointProvider()`
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->depointProvider();
    }



    /**
     * Standard one-to-many relationship with spare parts.
     *
     * @return HasMany
     */
    public function items()
    {
        return $this->hasMany(DepointOrderItem::class);
    }



    /**
     * A Many-to-Many relationship with spare parts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function spareParts()
    {
        return $this->belongsToMany(DepointSparePart::class, 'depoint_order_items');
    }



    /**
     * Accessor to the `spareParts` relationship.
     *
     * @return Collection
     */
    public function getSparePartsAttribute()
    {
        return $this->getRelationValue('spareParts');
    }



    /**
     * Generates and returns a virtual title.
     *
     * @return string
     */
    public function getVirtualTitle()
    {
        return trans_safe('depoint::spare-parts.order.virtual-title', [
             'number'   => ad($this->spareParts()->count()),
             'provider' => $this->provider->name,
        ]);
    }



    /**
     * Returns the virtual title.
     *
     * @return string
     */
    public function getVirtualTitleAttribute()
    {
        return $this->getVirtualTitle();
    }



    /**
     * The "booting" tasks of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::observe(DepointOrderObserver::class);
    }



    /**
     * Checks if this order is accessible for the current client.
     *
     * @return bool
     */
    public function isAccessible()
    {
        return boolval($this->provider->managers()->where('users.id', user()->id)->first());
    }
}
