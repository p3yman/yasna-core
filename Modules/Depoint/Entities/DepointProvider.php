<?php

namespace Modules\Depoint\Entities;

use App\Models\User;
use App\Models\DepointOrder;
use App\Models\DepointSparePart;
use Illuminate\Database\Eloquent\Collection;
use Modules\Depoint\Entities\Traits\DepointProviderPermitTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepointProvider extends YasnaModel
{
    use SoftDeletes;
    use DepointProviderPermitTrait;



    /**
     * Standard laravel many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }



    /**
     * Standard laravel many-to-many relationship.
     * <br>
     * _Mirror of the `users()` method._
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managers()
    {
        return $this->users();
    }



    /**
     * Standard many-to-many relationship with spare parts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function spareParts()
    {
        return $this->belongsToMany(DepointSparePart::class);
    }



    /**
     * Accessor to the `spareParts` relationship.
     *
     * @return Collection
     */
    public function getSparePartsAttribute()
    {
        return $this->getRelationValue('spareParts');
    }



    /**
     * Standard many-to-many relationship with spare parts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function depointOrders()
    {
        return $this->hasMany(DepointOrder::class);
    }



    /**
     * Accessor to the `depointOrders` relationship.
     *
     * @return Collection
     */
    public function getDepointOrdersAttribute()
    {
        return $this->getRelationValue('depointOrders');
    }
}
