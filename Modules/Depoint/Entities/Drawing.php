<?php

namespace Modules\Depoint\Entities;

use App\Models\Post;
use App\Models\User;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Drawing extends YasnaModel
{
    use SoftDeletes;



    /**
     * One-to-Many Relationship with the `users` Table
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }



    /**
     * Effects all receipt while preparing database.
     */
    protected static function prepareDatabaseEffectReceipts()
    {
        model('receipt')
             ->withTrashed()
             ->update([
                  'operation_integer' => 0,
             ])
        ;
    }



    /**
     * Effect receipts of the specified post while preparing database.
     *
     * @param Post $post
     */
    protected static function prepareDatabaseEffectPostReceipts(Post $post)
    {
        $post->receipts()->groupBy('user_id')->update([
             'operation_integer' => 1,
        ])
        ;
    }



    /**
     * Returns the name of the session which is holding the drawing post.
     *
     * @return string
     */
    public static function drawingPostSessionName()
    {
        return 'drawing_post';
    }



    /**
     * Returns the name of the session which is holding the line number.
     *
     * @return string
     */
    public static function lineNumberSessionName()
    {
        return 'line_number';
    }



    /**
     * Put data to session while preparing database.
     *
     * @param Post $post
     */
    protected static function prepareDatabasePutSessions(Post $post)
    {
        session()->put(static::lineNumberSessionName(), '0');
        session()->put(static::drawingPostSessionName(), $post->id);
    }



    /**
     * Prepares the database for drawing.
     *
     * @param Post $post
     */
    public static function prepareDatabase(Post $post)
    {
        static::select()->delete();
        static::prepareDatabaseEffectReceipts();
        static::prepareDatabaseEffectPostReceipts($post);
        static::prepareDatabasePutSessions($post);
    }



    /**
     * Pulls the drawing record based on the specified number.
     *
     * @param int $number
     *
     * @return \App\Models\Drawing|null
     */
    public static function pull(int $number)
    {
        return static::where('lower_line', '<=', $number)
                     ->where('upper_line', '>=', $number)
                     ->first()
             ;
    }



    /**
     * Weather the drawing is ready or not.
     *
     * @param Post $post
     *
     * @return bool
     */
    public static function isReadyFor(Post $post)
    {
        $session_drawing_post = session()->get(static::drawingPostSessionName());
        $session_line_number  = session()->get(static::lineNumberSessionName());

        return (($session_drawing_post == $post->id) and ($session_line_number > 0));
    }
}
