<?php

namespace Modules\Depoint\Entities;

use App\Models\DepointSparePart;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection as BaseCollection;

class DepointCart extends YasnaModel
{
    use SoftDeletes;



    /**
     * Standard many-to-many relationship with spare parts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function spareParts()
    {
        return $this->belongsToMany(DepointSparePart::class);
    }



    /**
     * Accessor to the `spareParts` relationship.
     *
     * @return mixed
     */
    public function getSparePartsAttribute()
    {
        return $this->getRelationValue('spareParts');
    }



    /**
     * attach spare part(s) to cart.
     *
     * @param int|string|DepointSparePart|Model|Collection|BaseCollection $spare_parts
     */
    public function addSparePart($spare_parts)
    {
        $this->spareParts()->attach($spare_parts);
    }



    /**
     * attach spare part(s) to cart.
     *
     * @param int|string|DepointSparePart|Model|Collection|BaseCollection $spare_parts
     */
    public function addSpareParts($spare_parts)
    {
        $this->addSparePart($spare_parts);
    }



    /**
     * check if this cart has the specified spare part.
     *
     * @param int|string|DepointSparePart $spare_part
     *
     * @return bool
     */
    public function hasSparePart($spare_part)
    {
        if (!($spare_part instanceof DepointSparePart)) {
            $spare_part = model('depoint-spare-part', $spare_part);
        }

        return boolval($this->spareParts()->where('depoint_spare_parts.id', $spare_part->id)->first());
    }



    /**
     * check if this cart has not the specified spare part.
     *
     * @param int|string|DepointSparePart $spare_part
     *
     * @return bool
     */
    public function hasNotSparePart($spare_part)
    {
        return !$this->hasSparePart($spare_part);
    }



    /**
     * Standard Laravel One-to-Many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }



    /**
     * Scope a query to only include raised carts.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeRaisedOnly($query)
    {
        return $query->whereNotNull('raised_at');
    }



    /**
     * Scope a query to only include open carts.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeOpenOnly($query)
    {
        return $query->whereNull('raised_at');
    }



    /**
     * Scope a query to only include unraised carts.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeUnraisedOnly($query)
    {
        return $this->scopeOpenOnly($query);
    }



    /**
     * check if the logged in user can access to the cart environment or not.
     *
     * @return bool
     */
    public function canAccess()
    {
        return model('depoint-order')->can('create');
    }
}
