<?php

namespace Modules\Depoint\Entities;

use Modules\Depoint\Entities\Traits\DepointProductPermitTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepointProduct extends YasnaModel
{
    use SoftDeletes;
    use DepointProductPermitTrait;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['titles' => 'array'];



    /**
     * Return the title in the specified locale.
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function titleIn($locale)
    {
        return ($this->titles[$locale] ?? null);
    }



    /**
     * Return title in the current locale.
     *
     * @return null|string
     */
    public function titleInCurrentLocale()
    {
        return $this->titleIn(getLocale());
    }



    /**
     * Accessor for the `titleInCurrentLocale()` method.
     *
     * @return null|string
     */
    public function getTitleAttribute()
    {
        return $this->titleInCurrentLocale();
    }



    /**
     * Return a meaningful slug about activation of the product.
     *
     * @return string
     */
    public function getStatus()
    {
        return ($this->is_active ? 'activated' : 'deactivated');
    }



    /**
     * Accessor for the method `getStatus()`.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->getStatus();
    }



    /**
     * Return trans of the status.
     *
     * @return string
     */
    public function getStatusText()
    {
        $status = $this->status;

        return trans_safe("depoint::spare-parts.product.status.$status");
    }



    /**
     * Accessor for the method `getStatusText()`
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->getStatusText();
    }



    /**
     * Return icon of the status.
     *
     * @return string
     */
    public function getStatusIcon()
    {
        return ($this->is_active ? 'check' : 'times');
    }



    /**
     * Accessor for the method `getStatusIcon()`
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        return $this->getStatusIcon();
    }



    /**
     * Return color of the status.
     *
     * @return string
     */
    public function getStatusColor()
    {
        return ($this->is_active ? 'success' : 'danger');
    }



    /**
     * Accessor for the method `getStatusColor()`.
     *
     * @return string
     */
    public function getStatusColorAttribute()
    {
        return $this->getStatusColor();
    }



    /**
     * Standard many-to-many relationship with spare parts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function spareParts()
    {
        return $this->belongsToMany(DepointSparePart::class);
    }



    /**
     * Accessor to the `spareParts` relationship.
     *
     * @return mixed
     */
    public function getSparePartsAttribute()
    {
        return $this->getRelationValue('spareParts');
    }
}
