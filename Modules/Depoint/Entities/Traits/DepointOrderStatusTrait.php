<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/3/18
 * Time: 11:29 PM
 */

namespace Modules\Depoint\Entities\Traits;


use Carbon\Carbon;

trait DepointOrderStatusTrait
{
    /**
     * Checks if this order is verified.
     *
     * @return bool
     */
    public function isVerified()
    {
        return ($this->verified_at and now()->gt($this->verified_at));
    }



    /**
     * Checks if this order is not verified.
     *
     * @return bool
     */
    public function isNotVerified()
    {
        return !$this->isVerified();
    }



    /**
     * Checks if this order is sent.
     *
     * @return bool
     */
    public function isSent()
    {
        return ($this->sent_at and now()->gt($this->sent_at));
    }



    /**
     * Checks if this order is not sent.
     *
     * @return bool
     */
    public function isNotSent()
    {
        return !$this->isSent();
    }



    /**
     * Checks if this order is submitted.
     *
     * @return bool
     */
    public function isSubmitted()
    {
        return ($this->submitted_at and now()->gt($this->submitted_at));
    }



    /**
     * Checks if this order is not submitted.
     *
     * @return bool
     */
    public function isNotSubmitted()
    {
        return !$this->isSubmitted();
    }



    /**
     * Checks if this order has been preparing.
     *
     * @return bool
     */
    public function hasBeenPreparing()
    {
        return $this->isSubmitted();
    }



    /**
     * Checks if this order has not been preparing.
     *
     * @return bool
     */
    public function hasNotBeenPreparing()
    {
        return $this->isNotSubmitted();
    }



    /**
     * Returns the status.
     *
     * @return string
     */
    public function getStatus()
    {
        if ($this->isVerified()) {
            return 'verified';
        }

        if ($this->isSent()) {
            return 'sent';
        }

        if ($this->hasBeenPreparing()) {
            return 'preparing';
        }

        return 'new';
    }



    /**
     * Accessor for the `getStatus()` Method.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->getStatus();
    }



    /**
     * Returns the status text in the current locale.
     *
     * @return string
     */
    public function getStatusText()
    {
        $status = $this->getStatus();

        return $this->module()->getTrans("spare-parts.order.status.$status");
    }



    /**
     * Accessor for the `getStatusText()` Method.
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->getStatusText();
    }



    /**
     * Returns the name of the column which contains the last status change's log.
     *
     * @return string
     */
    public function getStatusDateColumn()
    {
        $map = [
             'new'       => 'created_at',
             'preparing' => 'submitted_at',
             'sent'      => 'sent_at',
             'verified'  => 'verified_at',
        ];

        return ($map[$this->status] ?? 'created_at');
    }



    /**
     * Accessor for the `getStatusDateColumn()` Method
     *
     * @return string
     */
    public function getStatusDateColumnAttribute()
    {
        return $this->getStatusDateColumn();
    }



    /**
     * Returns the date and time of the last status change.
     *
     * @return string|Carbon|null
     */
    public function getStatusDate()
    {
        $column = $this->status_date_column;

        return $this->$column;
    }



    /**
     * Accessor for the `getStatusDate()` Method
     *
     * @return Carbon|null|string
     */
    public function getStatusDateAttribute()
    {
        return $this->getStatusDate();
    }



    /**
     * Returns an icon of the status.
     *
     * @return string
     */
    public function getStatusIcon()
    {
        $map = [
             'new'       => 'hourglass-start',
             'preparing' => 'spinner',
             'sent'      => 'truck',
             'verified'  => 'check',
        ];

        return ($map[$this->status] ?? '');
    }



    /**
     * Accessor for the `getStatusIcon()` Method
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        return $this->getStatusIcon();
    }



    /**
     * Returns the color of the status to be used in the manage side.
     *
     * @return string
     */
    public function getStatusManageColor()
    {
        $colors_statuses_map = [
             'new'       => 'warning',
             'preparing' => 'info',
             'sent'      => 'primary',
             'verified'  => 'success',
        ];

        return ($colors_statuses_map[$this->status] ?? '');
    }



    /**
     * Accessor for the `getStatusManageColor()` Method
     *
     * @return string
     */
    public function getStatusManageColorAttribute()
    {
        return $this->getStatusManageColor();
    }



    /**
     * Returns the color of the status to be used in the front side.
     *
     * @return string
     */
    public function getStatusFrontColor()
    {
        $colors_statuses_map = [
             'new'       => 'yellow',
             'preparing' => 'blue',
             'sent'      => 'teal',
             'verified'  => 'green',
        ];

        return ($colors_statuses_map[$this->status] ?? '');
    }



    /**
     * Accessor for the `getStatusFrontColor()` Method
     *
     * @return string
     */
    public function getStatusFrontColorAttribute()
    {
        return $this->getStatusFrontColor();
    }



    /**
     * Checks if this order is finalized.
     *
     * @return bool
     */
    public function isFinalized()
    {
        return $this->isVerified();
    }



    /**
     * Checks if this order is not finalized.
     *
     * @return bool
     */
    public function isNotFinalized()
    {
        return !$this->isFinalized();
    }



    /**
     * Change the status to new.
     *
     * @return $this
     */
    public function applyNewStatus()
    {
        $this->batchSave([
             'submitted_at' => null,
             'submitted_by' => 0,
             'sent_at'      => null,
             'sent_by'      => 0,
             'verified_at'  => null,
             'verified_by'  => 0,
        ]);

        $this->fresh();

        return $this;
    }



    /**
     * Change the status to preparing.
     *
     * @return $this
     */
    public function applySubmittedStatus()
    {
        if ($this->status == 'preparing') {
            return $this;
        }

        $this->update([
             'submitted_at' => now()->toDateTimeString(),
             'submitted_by' => user()->id,
             'sent_at'      => null,
             'sent_by'      => 0,
             'verified_at'  => null,
             'verified_by'  => 0,
        ]);

        return $this;
    }



    /**
     * Change the status to preparing.
     *
     * @return $this
     */
    public function applyPreparingStatus()
    {
        return $this->applySubmittedStatus();
    }



    /**
     * Change the status to sent.
     *
     * @return $this
     */
    public function applySentStatus()
    {
        if ($this->status == 'sent') {
            return $this;
        }

        $this->update([
             'sent_at'     => now()->toDateTimeString(),
             'sent_by'     => user()->id,
             'verified_at' => null,
             'verified_by' => 0,
        ]);

        return $this;
    }



    /**
     * Change the status to sent.
     *
     * @return $this
     */
    public function applyVerifiedStatus()
    {
        if ($this->status == 'verified') {
            return $this;
        }

        $this->update([
             'verified_at' => now()->toDateTimeString(),
             'verified_by' => user()->id,
        ]);

        return $this;
    }



    /**
     * Loosens the verified status.
     *
     * @return $this
     */
    public function loosenVerifiedStatus()
    {
        if ($this->status != 'verified') {
            return $this;
        }

        $this->update([
             'verified_at' => null,
             'verified_by' => 0,
        ]);

        return $this;


    }



    /**
     * Returns an array of statuses which could be selected by the provider.
     *
     * @return array
     */
    public function selectableStatuses()
    {
        return ['preparing', 'sent'];
    }



    /**
     * Sets the specified status on this order.
     *
     * @param string $status
     *
     * @return bool
     */
    public function setStatus(string $status): bool
    {
        switch ($status) {
            case 'preparing':
                $this->applySubmittedStatus();
                return true;

            case 'sent':
                $this->applySentStatus();
                return true;

            case 'verified':
                $this->applyVerifiedStatus();
                return true;

            default:
                return false;
        }

    }
}
