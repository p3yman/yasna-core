<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/13/18
 * Time: 2:42 PM
 */

namespace Modules\Depoint\Entities\Traits;


trait DepointSparePartPermitTrait
{
    /**
     * Check if the logged in user has permission to make the specified action.
     *
     * @param string $action
     *
     * @return bool
     */
    public function can($action = '*')
    {
        return user()->asAdmin()->can("depoint_spare_parts.$action");
    }
}
