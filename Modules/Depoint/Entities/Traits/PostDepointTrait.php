<?php

namespace Modules\Depoint\Entities\Traits;

use App\Models\Ware;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Depoint\Providers\PostsServiceProvider;

trait PostDepointTrait
{
    /**
     * Returns a proper version of title to be shown in URLs.
     *
     * @return string
     */
    public function getUrlTitle()
    {
        $url_encoded  = urlencode($this->title);
        $with_dashesh = str_replace('+', '-', $url_encoded);

        return $with_dashesh;

    }



    /**
     * Returns direct URL for news posts.
     *
     * @return string
     */
    public function newsDirectUrl()
    {
        return route('depoint.front.news.single', [
             'hashid' => $this->hashid,
             'title'  => $this->getUrlTitle(),
             'lang'   => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for blog posts.
     *
     * @return string
     */
    public function blogDirectUrl()
    {
        return route('depoint.front.blog.single', [
             'hashid' => $this->hashid,
             'title'  => $this->getUrlTitle(),
             'lang'   => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for festival posts.
     *
     * @return string
     */
    public function festivalsDirectUrl()
    {
        return route('depoint.front.festivals.single', [
             'hashid' => $this->hashid,
             'title'  => $this->getUrlTitle(),
             'lang'   => $this->locale,
        ]);
    }



    /**]
     * Returns direct URL for image-gallery posts.
     *
     * @return string
     */
    public function imageGalleryDirectUrl()
    {
        return route('depoint.front.images.single', [
             'hashid' => $this->hashid,
             'title'  => $this->getUrlTitle(),
             'lang'   => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for products posts.
     *
     * @return string
     */
    public function videoGalleryDirectUrl()
    {
        return route('depoint.front.videos.single', [
             'hashid' => $this->hashid,
             'title'  => $this->getUrlTitle(),
             'lang'   => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for image-gallery posts.
     *
     * @return string
     */
    public function productsDirectUrl()
    {
        return route('depoint.front.product.single', [
             'hashid' => $this->hashid,
             'title'  => $this->getUrlTitle(),
             'lang'   => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for image-gallery posts.
     *
     * @return string
     */
    public function stocksDirectUrl()
    {
        return route('depoint.front.stock.single', [
             'hashid' => $this->hashid,
             'title'  => $this->getUrlTitle(),
             'lang'   => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for faq posts.
     *
     * @return string
     */
    public function faqDirectUrl()
    {
        $default_category = $this->default_category_model;

        if ($default_category->exists) {
            $slug = $default_category->slug;
        } else {
            $slug = 'all';
        }

        return route('depoint.front.faq.list', [
             "category" => $slug,
             'lang'     => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for product landing posts.
     *
     * @return string
     */
    public function productLandingDirectUrl()
    {
        return route('depoint.front.landing', [
             'landing_slug' => $this->slug,
             'title'        => $this->getUrlTitle(),
             'lang'         => $this->locale,
        ]);
    }



    /**
     * Returns direct URL for pages.
     *
     * @return string
     */
    public function pagesDirectUrl()
    {
        return route('depoint.front.page', [
             'slug'  => $this->slug,
             'title' => $this->getUrlTitle(),
             'lang'  => $this->locale,
        ]);
    }



    /**
     * Returns the direct URL of this post.
     *
     * @return string|null
     */
    public function getDirectUrl()
    {
        $posttype_slug   = $this->type;
        $specific_method = camel_case("$posttype_slug-direct-url");

        if (method_exists($this, $specific_method)) {
            return $this->$specific_method();
        }

        return null;
    }



    /**
     * get the direct link of the model.
     *
     * @return string
     */
    public function getDirectLinkAttribute()
    {
        return $this->getDirectUrlAttribute();
    }



    /**
     * Accessor for the `getDirectUrl()` method.
     *
     * @return null|string
     */
    public function getDirectUrlAttribute()
    {
        return $this->getDirectUrl();
    }



    /**
     * Returns similar posts to this post.
     *
     * @param int $number
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function similars(int $number = 4)
    {
        $select_array = [
             'type' => $this->type, // same post type
             'lang' => $this->locale, // same locale
        ];

        // same categories
        $categories = $this->categories;
        if ($categories->count()) {
            $select_array['category'] = $categories->pluck('slug')->toArray();
        }

        return PostsServiceProvider::select($select_array)
                                   ->orderByDesc('published_at')
                                   ->where('id', '<>', $this->id)
                                   ->limit($number)
                                   ->get()
             ;
    }



    /**
     * Check if this post has the feature for color images or not.
     *
     * @return bool
     */
    public function hasColorImages()
    {
        return $this->has('depoint_colors_image');
    }



    /**
     * Check if this post has not the feature for color images.
     *
     * @return bool
     */
    public function hasNotColorImage()
    {
        return !$this->hasColorImages();
    }



    /**
     * Returns an array of meta fields for color images.
     *
     * @return array
     */
    protected function depointColorImagesMetaFields()
    {
        if ($this->hasColorImages()) {
            return ['color_images'];
        } else {
            return [];
        }
    }



    /**
     * Returns an array of meta fields depending on the products details feature.
     *
     * @return array
     */
    protected function depointProductsDetailsMetaFields()
    {
        if ($this->has('depoint_products_details')) {
            return ['details_posts'];
        } else {
            return [];
        }
    }



    /**
     * Returns an array of color images.
     *
     * @return array|null
     */
    protected function getColorImages()
    {
        return ($this->getMeta('color_images') ?? null);
    }



    /**
     * Accessor fot the `getColorImages()` Method
     *
     * @return array|null
     */
    public function getColorImagesAttribute()
    {
        return $this->getColorImages();
    }



    /**
     * Returns a builder to select the wares which are defined for sellers.
     *
     * @return Builder
     */
    public function sellerWares()
    {
        return $this->wares()->forDepointSeller();
    }



    /**
     * Returns a builder to select the wares which are defined for customers.
     *
     * @return Builder
     */
    public function customerWares()
    {
        return $this->wares()->forDepointCustomers();
    }



    /**
     * Returns a builder to select the wares which are defined for the current client.
     *
     * @return HasMany
     */
    public function depointWares()
    {
        return $this->hasMany(Ware::class, 'sisterhood', 'sisterhood')
                    ->orderBy('order')
                    ->forCurrentDepointClient()
             ;
    }



    /**
     * Returns the name of the meta which is holding the winners IDs.
     *
     * @return string
     */
    public function drawWinnersMetaFieldName()
    {
        return 'draw_winners';
    }



    /**
     * Returns an array containing the meta fields for drawing process.
     *
     * @return array
     */
    public function drawingMetaFields()
    {
        return [$this->drawWinnersMetaFieldName()];
    }



    /**
     * Returns the winners array.
     *
     * @return array
     */
    public function getWinnersArray()
    {
        $winners = $this->getMeta($this->drawWinnersMetaFieldName());

        if (!$winners or !is_array($winners)) {
            $winners = [];
        }

        return $winners;
    }



    /**
     * Accessor for the `getWinnersArray()` Method.
     *
     * @return array
     */
    public function getWinnersArrayAttribute()
    {
        return $this->getWinnersArray();
    }



    /**
     * Returns the winner users.
     *
     * @return array
     */
    public function getWinnerUsers()
    {
        return array_map(function ($user_id) {
            return model('user', $user_id);
        }, $this->winners_array);
    }



    /**
     * Accessor for the `getWinnerUsers()` Method.
     *
     * @return array
     */
    public function getWinnerUsersAttribute()
    {
        return $this->getWinnerUsers();
    }



    /**
     * Weather has any winner.
     *
     * @return bool
     */
    public function hasAnyDrawingWinner()
    {
        return boolval(count($this->getWinnersArray()));
    }



    /**
     * Weather has not any winner.
     *
     * @return bool
     */
    public function hasNotAnyDrawingWinner()
    {
        return !$this->hasAnyDrawingWinner();
    }



    /**
     * Weather has possibility for drawing process.
     *
     * @return bool
     */
    public function hasDrawPossibility()
    {
        return $this->has('event');
    }



    /**
     * Accessor for the `hasDrawPossibility()` Method.
     *
     * @return bool
     */
    public function getHasDrawPossibilityAttribute()
    {
        return $this->hasDrawPossibility();
    }



    /**
     * Weather has not possibility for drawing process.
     *
     * @return bool
     */
    public function hasNotDrawPossibility()
    {
        return !$this->hasDrawPossibility();
    }



    /**
     * Accessor for the `hasNotDrawPossibility()` Method.
     *
     * @return bool
     */
    public function getHasNotDrawPossibilityAttribute()
    {
        return $this->hasNotDrawPossibility();
    }



    /**
     * Returns total winner's count.
     *
     * @return int
     */
    public function getTotalWinnersCount()
    {
        return count($this->winners_array);
    }



    /**
     * Accessor for the `getTotalWinnersCount()` Method.
     *
     * @return int
     */
    public function getTotalWinnersCountAttribute()
    {
        return $this->getTotalWinnersCount();
    }



    /**
     * Returns a query builder to select receipts which could be in this
     *
     * @return Builder
     */
    public function receipts()
    {
        return model('receipt')
             ->whereBetween('purchased_at', [$this->event_starts_at, $this->event_ends_at]);
    }



    /**
     * Returns a collection containing the receipts.
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getReceiptsAttribute()
    {
        return $this->receipts()->get();
    }



    /**
     * Returns the totoal count of receipts.
     *
     * @return int
     */
    public function getTotalReceiptsCount()
    {
        return $this->receipts()->count();
    }



    /**
     * Accessor for the `getTotalReceiptsCount()` Method.
     *
     * @return int
     */
    public function getTotalReceiptsCountAttribute()
    {
        return $this->getTotalReceiptsCount();
    }



    /**
     * Weather has any receipts.
     *
     * @return bool
     */
    public function hasAnyReceipt()
    {
        if ($this->hasNotDrawPossibility()) {
            return false;
        }

        return boolval($this->getTotalReceiptsCount());
    }



    /**
     * Weather has not any receipts.
     *
     * @return bool
     */
    public function hasNotAnyReceipt()
    {
        return !$this->hasAnyReceipt();
    }



    /**
     * Prepares the database for drawing with this post.
     */
    public function prepareForDrawing()
    {
        model('drawing')->prepareDatabase($this);
    }



    /**
     * Weather is drawable.
     *
     * @return bool
     */
    public function isDrawableNow()
    {
        if ($this->hasNotDrawPossibility()) {
            return false;
        }

        return (now()->toDateTimeString() > $this->event_ends_at);
    }



    /**
     * Weather is not drawable.
     *
     * @return bool
     */
    public function isNotDrawableNow()
    {
        return !$this->isDrawableNow();
    }



    /**
     * Returns the total amount of receipts.
     *
     * @return int
     */
    public function getTotalReceiptsAmount()
    {
        return $this->receipts()->sum('purchased_amount');
    }



    /**
     * Accessor for the `getTotalReceiptsAmount()` Method.
     *
     * @return int
     */
    public function getTotalReceiptsAmountAttribute()
    {
        return $this->getTotalReceiptsAmount();
    }



    /**
     * Weather is ready for drawing with this post.
     *
     * @return bool
     */
    public function isReadyToDraw()
    {
        return model('drawing')->isReadyFor($this);
    }



    /**
     * Weather is not ready for drawing with this post.
     *
     * @return bool
     */
    public function isNotReadyToDraw()
    {
        return !$this->isReadyToDraw();
    }
}
