<?php

namespace Modules\Depoint\Entities\Traits;

use App\Models\DepointCart;
use App\Models\DepointProvider;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Modules\Depoint\Services\Module\CurrentModuleHelper;
use Nwidart\Modules\Facades\Module;

trait UserDepointTrait
{
    /**
     * Standard many-to-many relationship with depoint providers.
     *
     * @return BelongsToMany
     */
    public function depointProviders()
    {
        return $this->belongsToMany(DepointProvider::class);
    }



    /**
     * Accessor for the `depointProviders` Relation
     *
     * @return Collection
     */
    public function getDepointProvidersAttribute()
    {
        return $this->getRelationValue('depointProviders');
    }



    /**
     * Standard Laravel One-to-Many Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function depointCarts()
    {
        return $this->hasMany(DepointCart::class);
    }



    /**
     * Accessor for the `depointCarts` relationship.
     *
     * @return Collection
     */
    public function getDepointCartsAttribute()
    {
        return $this->getRelationValue('depointCarts');
    }



    /**
     * return a query builder to access all raised carts
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function raisedDepointCarts()
    {
        return $this->depointCarts()
                    ->raisedOnly('raised_at')
             ;
    }



    /**
     * Accessor for the `raisedDepointCarts` relationship.
     *
     * @return Collection
     */
    public function getRaisedDepointCartsAttribute()
    {
        return $this->getRelationValue('raisedDepointCarts');
    }



    /**
     * return last open cart if it exists.
     *
     * @return DepointCart|null
     */
    public function openDepointCart()
    {
        return $this->depointCarts()
                    ->openOnly('raised_at')
                    ->first()
             ;
    }



    /**
     * Accessor for the `getOpenDepointCartAttribute() Method
     *
     * @return DepointCart|null
     */
    public function getOpenDepointCartAttribute()
    {
        return $this->openDepointCart();
    }



    /**
     * check if has any open carts.
     *
     * @return bool
     */
    public function hasOpenDepointCart()
    {
        return boolval($this->open_depoint_cart);
    }



    /**
     * check if has not any open carts.
     *
     * @return bool
     */
    public function hasNotOpenDepointCart()
    {
        return !$this->hasOpenDepointCart();
    }



    /**
     * add a new cart for this user.
     *
     * @param array $data
     *
     * @return DepointCart
     */
    public function addDepointCart($data = [])
    {
        $cart = model('depoint-cart');
        $cart->fill($data);

        $cart->user()->associate($this);

        $cart->save();

        return $cart;
    }



    /**
     * return the current cart.
     * <br>
     * If any open cart exists, return it and if not create one and return the new one.
     *
     * @return DepointCart|null
     */
    public function currentDepointCart()
    {
        $open_cart = $this->openDepointCart();

        if ($open_cart) {
            return $open_cart;
        }

        return $this->addDepointCart();
    }



    /**
     * Return the user gravatar account avatar associated with his/her email, or a default one if it is not set.
     *
     * @param string|null $default
     * @param int         $size
     *
     * @return null|string
     */
    public function gravatar(string $default = null, $size = 100)
    {
        $default = $default ?: CurrentModuleHelper::asset('images/user.png');

        return $this->email
             ? "https://www.gravatar.com/avatar/"
             . md5(strtolower(trim($this->email)))
             . "?d="
             . urlencode(trim($default, '/'))
             . "&s="
             . $size
             : $default;
    }



    /**
     * Accessor for the gravatar.
     *
     * @return null|string
     */
    public function getGravatarAttribute()
    {
        return $this->gravatar();
    }



    /**
     * Checks if this user is a provider for depoint.
     *
     * @return bool
     */
    public function hasDepointProviderRole()
    {
        return $this->is('depoint-provider');
    }



    /**
     * Checks if this user is not a provider for depoint.
     *
     * @return bool
     */
    public function hasNotDepointProviderRole()
    {
        return !$this->hasDepointProviderRole();
    }



    /**
     * Checks if this user has been assigned as manager of any depoint provider.
     *
     * @return bool
     */
    public function hasAnyDepointProvider()
    {
        return boolval($this->depointProviders()->first());
    }



    /**
     * Checks if this user been assigned as manager of any depoint provider.
     *
     * @return bool
     */
    public function hasNotAnyDepointProvider()
    {
        return !$this->hasAnyDepointProvider();
    }



    /**
     * Check if this user has access to the providers environment.
     *
     * @return bool
     */
    public function hasAccessToProvidersEnvironment()
    {
        return ($this->hasDepointProviderRole() and $this->hasAnyDepointProvider());
    }



    /**
     * check user role to see stokes
     *
     * @return bool
     */
    public function hasAccessToStocksEnvironment()
    {
        return user()->is(depoint()->shop()->stockerRoleSlug()) or posttype('stocks')->getMeta('all_access');
    }



    /**
     * Check if this user has not access to the providers environment.
     *
     * @return bool
     */
    public function hasNotAccessToProvidersEnvironment()
    {
        return !$this->hasAccessToProvidersEnvironment();
    }



    /**
     * Checks if user has not access to the stocks environment.
     *
     * @return bool
     */
    public function hasNotAccessToStocksEnvironment()
    {
        return !$this->hasAccessToStocksEnvironment();
    }



    /**
     * Checks if this user can buy anything with credit.
     *
     * @return bool
     */
    public function canBuyWithCredit()
    {
        return $this->is(depoint()->shop()->validatedBuyerRoleSlug());
    }



    /**
     * Returns the count of receipts of this user based on the given event post.
     *
     * @param Post $post
     *
     * @return int
     */
    public function totalReceiptsAmountInEvent(Post $post)
    {
        return $post->receipts()->where('user_id', $this->id)->sum('purchased_amount');
    }
}
