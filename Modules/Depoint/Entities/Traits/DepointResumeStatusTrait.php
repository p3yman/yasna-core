<?php


namespace Modules\Depoint\Entities\Traits;

/**
 * @property string $approved_at
 * @property string $rejected_at
 */
trait DepointResumeStatusTrait
{
    /**
     * get the resume status code
     *
     * @return string
     */
    public function resumeStatus()
    {
        if ($this->isPending()){
            return "pending";
        }

        if ($this->isApproved()){
            return "approved";
        }

        if ($this->isRejected()){
            return "rejected";
        }

        return "unknown";//<~~This is a strange thing.
    }



    /**
     * get status label
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function getResumeStatusLabelAttribute()
    {
        return trans("depoint::resume.statuses." . $this->resumeStatus());
    }



    /**
     * indicate if the resume is approved.
     *
     * @return bool
     */
    public function isApproved()
    {
        return boolval($this->approved_at);
    }



    /**
     * indicate if the resume is rejected.
     *
     * @return bool
     */
    public function isRejected()
    {
        return boolval($this->rejected_at);
    }



    /**
     * indicate if the resume is processing.
     *
     * @return bool
     */
    public function isPending()
    {
        return !$this->isApproved() and !$this->isRejected();
    }
}
