<?php


namespace Modules\Depoint\Entities\Traits;


trait DepointResumeMetaTrait
{
    /**
     * get the main meta fields of the table
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return[
             'dependants',
             'educational',
             'workplace',
             'specialized',
             'knowing',
             'software',
             'relatives',
        ];
    }
}
