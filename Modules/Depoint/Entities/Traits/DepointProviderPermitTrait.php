<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/12/18
 * Time: 1:29 PM
 */

namespace Modules\Depoint\Entities\Traits;


trait DepointProviderPermitTrait
{
    /**
     * Check if the logged in user has permission to make the specified action.
     *
     * @param string $action
     *
     * @return bool
     */
    public function can($action = '*')
    {
        return user()->asAdmin()->can("depoint_providers.$action");
    }
}
