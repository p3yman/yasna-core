<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/11/18
 * Time: 2:47 PM
 */

namespace Modules\Depoint\Entities\Traits;


trait DepointProductPermitTrait
{
    /**
     * Check if the logged in user has permission to make the specified action.
     *
     * @param string $action
     *
     * @return bool
     */
    public function can($action = '*')
    {
        return user()->asAdmin()->can("depoint_products.$action");
    }
}
