<?php

namespace Modules\Depoint\Entities\Traits;

use App\Models\Order;
use App\Models\Receipt;
use App\Models\Transaction;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Modules\Payment\Services\PaymentHandler\Search\SearchBuilder;

trait CartDepointTrait
{
    /**
     * Refreshes the specified order.
     *
     * @param Order $order
     */
    protected function refreshOrder(Order $order)
    {
        /** @var Ware $ware */
        $ware = ($order->ware ?? ware());

        if ($ware->not_exists or $ware->isNotAvailableForCurrentUser()) {
            $order->delete();
        }
    }



    /**
     * Refreshes orders.
     */
    public function depointRefreshOrders()
    {
        $orders = $this->orders;

        foreach ($orders as $order) {
            $this->refreshOrder($order);
        }
    }



    /**
     * Refreshes calculations based on the Depoint site rules.
     *
     * @return mixed
     */
    public function depointRefreshCalculations()
    {
        $this->depointRefreshOrders();

        return $this->refreshCalculations();
    }



    /**
     * One-to-Many Relationship with receipts.
     *
     * @return HasMany
     */
    public function receipts()
    {
        return $this->hasMany(Receipt::class, 'invoice_id');
    }



    /**
     * Checks if this cart has a usable address.
     *
     * @return bool
     */
    public function hasUsableAddress()
    {
        $address = $this->address;

        if (!$address or $address->not_exists) {
            return false;
        }

        return ($address->user_id == $this->user_id);
    }



    /**
     * Checks if this cart has not a usable address.
     *
     * @return bool
     */
    public function hasNotUsableAddress()
    {
        return !$this->hasUsableAddress();
    }



    /**
     * Checks if this cart has a usable shipping method.
     *
     * @return bool
     */
    public function hasUsableShippingMethod()
    {
        $shipping_method = $this->shipping_method;

        if (!$shipping_method or $shipping_method->not_exists) {
            return false;
        }

        return $this->canBeShippedBy($shipping_method->id);
    }



    /**
     * Checks if this cart has not a usable shipping method.
     *
     * @return bool
     */
    public function hasNotUsableShippingMethod()
    {
        return !$this->hasUsableShippingMethod();
    }



    /**
     * Checks if this cart is ready to be purchased in Depoint website.
     *
     * @return bool
     */
    public function isReadyForDepointPurchase()
    {
        return ($this->hasUsableAddress() and $this->hasUsableShippingMethod());
    }



    /**
     * Checks if this cart is not ready to be purchased in Depoint website.
     *
     * @return bool
     */
    public function isNotReadyForDepointPurchase()
    {
        return !$this->isReadyForDepointPurchase();
    }



    /**
     * @inheritdoc
     */
    protected function shouldInteractWithModelName()
    {
        return false;
    }
}
