<?php

namespace Modules\Depoint\Entities\Traits;

use Illuminate\Database\Eloquent\Builder;

trait WareDepointTrait
{
    /**
     * Scope for selecting the wares which are defined for sellers.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeForDepointSeller($query)
    {
        $prefix = depoint()->shop()->sellerWaresPrefix();
        return $query->where('custom_code', 'like', $prefix . '_%');
    }



    /**
     * Scope for selecting the wares which are not defined for sellers.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeNotForDepointSeller($query)
    {
        $prefix = depoint()->shop()->sellerWaresPrefix();
        return $query->where('custom_code', 'not like', $prefix . '_%');
    }



    /**
     * Scope for selecting the wares which are not defined for sellers.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeForDepointCustomers($query)
    {
        return $this->scopeNotForDepointSeller($query);
    }



    /**
     * Scope for selecting wares which could be shown for the current client.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeForCurrentDepointClient($query)
    {
        if (depoint()->shop()->sellerEnvironmentIsAccessible()) {
            return $this->scopeForDepointSeller($query);
        } else {
            return $this->scopeForDepointCustomers($query);
        }
    }



    /**
     * Checks if this ware is for sellers.
     *
     * @return bool
     */
    public function isForSellers()
    {
        return starts_with($this->custom_code, depoint()->shop()->sellerWaresPrefix());
    }



    /**
     * Checks if this ware is not for sellers.
     *
     * @return bool
     */
    public function isNotForSellers()
    {
        return !$this->isForSellers();
    }



    /**
     * Checks if this ware is stock.
     *
     * @return bool
     */
    public function isStock()
    {
        return ($this->post->type == 'stocks');
    }



    /**
     * Checks if this ware is not stock.
     *
     * @return bool
     */
    public function isNotStock()
    {
        return !$this->isStock();
    }



    /**
     * Checks if this ware is available for the current user.
     *
     * @return bool
     */
    public function isAvailableForCurrentUser()
    {
        if ($this->isForSellers()) {
            return depoint()->shop()->sellerEnvironmentIsAccessible();
        }

        if ($this->isStock()) {
            return user()->hasAccessToStocksEnvironment();
        }

        return true;
    }



    /**
     * Checks if this ware is not available for the current user.
     *
     * @return bool
     */
    public function isNotAvailableForCurrentUser()
    {
        return !$this->isAvailableForCurrentUser();
    }
}
