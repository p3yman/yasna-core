<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 9/12/18
 * Time: 10:02 AM
 */

namespace Modules\Depoint\Entities\Traits;


trait TicketDepointTrait
{

    /**
     * deface id to view in frontend
     *
     * @return int
     */
    public function getIdForView()
    {
        return $this->id + 3000;
    }



    /**
     * get changing trans of flags
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getChangingFlagTrans()
    {
        return trans("tickets::flags.changing." . $this->flag);
    }



    /**
     * get trans of flags
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getFlagTrans()
    {
        return trans("tickets::flags." . $this->flag);
    }
}