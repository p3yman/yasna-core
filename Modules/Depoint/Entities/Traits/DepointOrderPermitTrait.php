<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/20/18
 * Time: 11:23 AM
 */

namespace Modules\Depoint\Entities\Traits;


trait DepointOrderPermitTrait
{
    /**
     * Check if the logged in user has permission to make the specified action.
     *
     * @param string $action
     *
     * @return bool
     */
    public function can($action = '*')
    {
        return user()->asAdmin()->can("depoint_orders.$action");
    }
}
