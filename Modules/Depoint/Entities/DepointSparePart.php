<?php

namespace Modules\Depoint\Entities;

use App\Models\DepointProduct;
use App\Models\DepointProvider;
use App\Models\User;
use Modules\Depoint\Entities\Traits\DepointSparePartPermitTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepointSparePart extends YasnaModel
{
    use SoftDeletes;
    use DepointSparePartPermitTrait;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
         'main_files'         => 'array',
         'pending_main_files' => 'array',
    ];



    /**
     * return meta fields for files.
     *
     * @return array
     */
    public function filesMetaFields()
    {
        return ['main_files'];
    }



    /**
     * standard laravel many-to-many relationship between spare parts and providers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function providers()
    {
        return $this->belongsToMany(DepointProvider::class);
    }



    /**
     * standard laravel many-to-many relationship between spare parts and products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(DepointProduct::class);
    }



    /**
     * return an array of main files.
     *
     * @return array|null
     */
    public function mainFiles()
    {
        return ($this->getMeta('main_files') ?? []);
    }



    /**
     * Accessor for the `mainFiles()` method.
     *
     * @return array|null
     */
    public function getMainFilesAttribute()
    {
        return $this->mainFiles();
    }



    /**
     * Return a meaningful slug about activation of the product.
     *
     * @return string
     */
    public function getStatus()
    {
        return ($this->is_ok ? 'ok' : 'nok');
    }



    /**
     * Accessor for the method `getStatus()`.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->getStatus();
    }



    /**
     * Return trans of the status.
     *
     * @return string
     */
    public function getStatusText()
    {
        $status = $this->status;

        return trans_safe("depoint::spare-parts.spare-part.status.$status");
    }



    /**
     * Accessor for the method `getStatusText()`
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->getStatusText();
    }



    /**
     * Return icon of the status.
     *
     * @return string
     */
    public function getStatusIcon()
    {
        return ($this->is_ok ? 'check' : 'times');
    }



    /**
     * Accessor for the method `getStatusIcon()`
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        return $this->getStatusIcon();
    }



    /**
     * Return color of the status.
     *
     * @return string
     */
    public function getStatusColor()
    {
        return ($this->is_ok ? 'success' : 'danger');
    }



    /**
     * Accessor for the method `getStatusColor()`.
     *
     * @return string
     */
    public function getStatusColorAttribute()
    {
        return $this->getStatusColor();
    }



    /**
     * Checks if this spare part is running out.
     *
     * @return bool
     */
    public function isRunningOut()
    {
        $daily_use = intval($this->daily_use);

        if (!$daily_use) {
            return false;
        }

        $inventory      = intval($this->inventory);
        $alert_days     = intval($this->alert_days);
        $remaining_days = floor($inventory / $daily_use);

        return ($remaining_days <= $alert_days);
    }



    /**
     * Checks if this spare part is not running out.
     *
     * @return bool
     */
    public function isNotRunningOut()
    {
        return !$this->isRunningOut();
    }



    /**
     * Checks if this spare part is acceptable for the current user.
     *
     * @param User|null $user If not specified, the current user will be used.
     *
     * @return bool
     */
    public function isAccessibleForUser(?User $user = null)
    {
        if (is_null($user)) {
            $user = user();
        }

        $user_providers = $user->depoint_providers->pluck('id')->toArray();
        $providers      = $this->providers->pluck('id')->toArray();
        $intersect      = array_intersect($user_providers, $providers);

        return boolval(count($intersect));
    }



    /**
     * Checks if this spare part is not acceptable for the current user.
     *
     * @param User|null $user If not specified, the current user will be used.
     *
     * @return bool
     */
    public function isNotAccessibleForUser(?User $user = null)
    {
        return !$this->isAccessibleForUser($user);
    }



    /**
     * Returns an array containing meta fields for pending files.
     *
     * @return array
     */
    public function pendingFilesMetaFields()
    {
        return [
             'pending_main_files',
             'pending_preview_file',
        ];
    }



    /**
     * Weather has pending main files.
     *
     * @return bool
     */
    public function hasPendingMainFiles()
    {
        return boolval($this->getMeta('pending_main_files'));
    }



    /**
     * Weather has pending preview file.
     *
     * @return bool
     */
    public function hasPendingPreviewFile()
    {
        return boolval($this->getMeta('pending_preview_file'));
    }



    /**
     * Weather has pending main files or preview file.
     *
     * @return bool
     */
    public function hasPendingFiles()
    {
        return ($this->hasPendingMainFiles() or $this->hasPendingPreviewFile());
    }
}
