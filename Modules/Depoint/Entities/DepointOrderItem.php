<?php

namespace Modules\Depoint\Entities;

use App\Models\DepointSparePart;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepointOrderItem extends YasnaModel
{
    use SoftDeletes;



    /**
     * Standard One-to-Many Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function depointSparePart()
    {
        return $this->belongsTo(DepointSparePart::class);
    }



    /**
     * Accessor for the `depointSparePart()` Method
     *
     * @return DepointSparePart|null
     */
    public function getDepointSparePartAttribute()
    {
        return $this->getRelationValue('depointSparePart');
    }



    /**
     * Mirror for the `depointSparePart()` Method
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sparePart()
    {
        return $this->depointSparePart();
    }



    /**
     * Accessor fot the `sparePart()` Method
     *
     * @return DepointSparePart|null
     */
    public function getSparePartAttribute()
    {
        return $this->getRelationValue('sparePart');
    }
}
