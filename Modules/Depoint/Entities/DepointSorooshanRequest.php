<?php

namespace Modules\Depoint\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Services\YasnaModel;

class DepointSorooshanRequest extends YasnaModel
{
    use SoftDeletes;



    /**
     * Response Meta Fields
     *
     * @return array
     */
    public function responseMetaFields()
    {
        return ['response'];
    }
}
