<?php

namespace Modules\Depoint\Entities;

use App\Models\Cart;
use Modules\Depoint\Observers\ReceiptObserver;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receipt extends YasnaModel
{
    use SoftDeletes;



    /**
     * Returns dashed version of the receipt's code.
     *
     * @return string
     */
    public function getDashedCode()
    {
        $dashed_code = '';

        $unique_code = str_split($this->code);

        for ($i = 0; $i <= 19; $i++) {
            if ($i == 4 or $i == 9 or $i == 14) {
                $separator = '-';
            } else {
                $separator = '';
            }
            $dashed_code .= $unique_code[$i] . $separator;
        }

        return $dashed_code;
    }



    /**
     * Accessor for the `getDashedCode()` Method
     *
     * @return string
     */
    public function getDashedCodeAttribute()
    {
        return $this->getDashedCode();
    }



    /**
     * Returns the formatted version of the amount.
     *
     * @return int|string
     */
    public function getFormattedAmount()
    {
        if ($this->purchased_amount > 0) {
            return number_format($this->purchased_amount);
        } else {
            return 0;
        }
    }



    /**
     * Accessor for the `getFormattedAmount()` Method
     *
     * @return int|string
     */
    public function getFormattedAmountAttribute()
    {
        return $this->getFormattedAmount();
    }



    /**
     * @inheritdoc
     */
    public static function boot()
    {
        parent::boot();

        static::observe(ReceiptObserver::class);
    }



    /**
     * Returns the Cart model object.
     *
     * @return Cart
     */
    public function getCart()
    {
        return model('cart', $this->invoice_id);
    }



    /**
     * Accessor for the `getCart()` Method
     *
     * @return Cart
     */
    public function getCartAttribute()
    {
        return $this->getCart();
    }
}
