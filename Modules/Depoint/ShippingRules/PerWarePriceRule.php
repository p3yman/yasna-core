<?php

namespace Modules\Depoint\ShippingRules;

use Modules\Manage\Services\Form;
use Modules\Shipping\Services\PriceModifier;
use Modules\Shipping\ShippingRules\Abs\ShippingRule;


class PerWarePriceRule extends ShippingRule
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return module('depoint')->getTrans('cart.shipping.rule.per-price');
    }



    /**
     * @inheritdoc
     */
    protected static function makeForm(): Form
    {
        $form = form();

        $form->add('text')
             ->inForm()
             ->numberFormat()
             ->name('price')
             ->label(module('depoint')->getTrans('cart.shipping.rule.per-price'))
             ->purificationRules('ed|numeric')
             ->validationRules('required|numeric')
        ;

        return $form;
    }



    /**
     * @inheritdoc
     */
    public function isSuitable(): bool
    {
        return true;
    }



    /**
     * get the final price
     *
     * @return float
     */
    public function getPrice(): float
    {
        $per_ware_price = $this->para['price'];
        $wares_count    = $this->getWaresCount();

        return ($per_ware_price * $wares_count);
    }



    /**
     * Returns the count of wares in the cart.
     *
     * @return double
     */
    protected function getWaresCount()
    {
        return $this
             ->invoice
             ->getOriginalInstance()
             ->orders()
             ->sum('count')
             ;
    }



    /**
     * @inheritdoc
     */
    public function getPriceModifier(): string
    {
        return PriceModifier::ABSOLUTE;
    }
}
