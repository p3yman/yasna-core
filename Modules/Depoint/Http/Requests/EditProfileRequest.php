<?php

namespace Modules\Depoint\Http\Requests;

use Carbon\Carbon;
use Exception;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;
use Morilog\Jalali\jDate;
use Morilog\Jalali\jDateTime;

class EditProfileRequest extends YasnaRequest
{
    /**
     * get an array of fillable fields, which are supposed to appear in the request submit.
     *
     * @return array
     */
    public function fillableFields()
    {

        return [
             '_token',
             'id',
             'model',
             'hashid',
             'name_first',
             'name_last',
             'birth_date',
             'birthday_day',
             'birthday_month',
             'birthday_year',
             'email',
             'gender',
             'tel',
             'mobile',
             'province',
             'city',
             'home_address',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "name_first"   => "ad",
             "name_last"    => "ad",
             "email"        => "ed",
             "tel"          => "ed",
             "mobile"       => "ed",
             "home_address" => "ad",
             "city"         => "dehash",
             "province"     => "dehash",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "name_first"   => "required|string|min:2|max:20",
             "name_last"    => "required|string|min:2|max:20",
             "email"        => "required|email",
             "gender"       => "required|integer|min:1|max:3",
             "tel"          => "required|phone",
             "mobile"       => "required|phone:mobile",
             "province"     => [
                  "required",
                  Rule::exists('states', 'id')
                      ->where('parent_id', 0),
             ],
             "city"         => [
                  "required",
                  Rule::exists('states', 'id')
                      ->where('parent_id', $this->province),
             ],
             "home_address" => "string|min:10|max:40",
             "birth_date"   => "required|date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctBirthDate();
    }



    /**
     * Corrects the birth date.
     */
    protected function correctBirthDate()
    {
        try {
            $day   = $this->data['birthday_day'];
            $month = $this->data['birthday_month'];
            $year  = $this->data['birthday_year'];
            if (jDateTime::checkDate($year, $month, $day)) {
                $birthday_gregorian       = jDateTime::toGregorian($year, $month, $day);
                $this->data['birth_date'] = $birthday_gregorian[0] . '-' . $birthday_gregorian[1] . '-' . $birthday_gregorian[2];
                $this->data['birth_date'] = (new Carbon($this->data['birth_date']))->toDateString();
            } else {
                $this->data['birth_date'] = '-';
            }
        } catch (Exception $exception) {
            $this->data['birth_date'] = '';
        }

        unset($this->data['birthday_year']);
        unset($this->data['birthday_month']);
        unset($this->data['birthday_day']);
    }
}
