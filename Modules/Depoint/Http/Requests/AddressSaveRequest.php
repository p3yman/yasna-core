<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Address\Http\Requests\AddressSaveRequest as OriginalRequest;

class AddressSaveRequest extends OriginalRequest
{
    /**
     * @inheritdoc
     */
    public function purifier()
    {
        $parent   = parent::purifier();
        $changing = [
             'province_id' => 'ed|dehash',
             'city_id'     => 'ed|dehash',
        ];

        return array_merge($parent, $changing);
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return (
             (
                  $model->not_exists or
                  ($model->user_id == user()->id)
             ) and
             parent::authorize()
        );
    }
}
