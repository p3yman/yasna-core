<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CartEditItemCountRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = 'order';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'count' => 'required|numeric|min:1',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'count' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['count'];
    }
}
