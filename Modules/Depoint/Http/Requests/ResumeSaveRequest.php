<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class ResumeSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "depoint-resume";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
             "name_first"      => "required",
             "name_last"       => "required",
             "name_father"     => "required",
             "code_id"         => "required",
             "code_melli"      => "required|code_melli",
             "birth_date"      => "required",
             "birth_city"      => "required",
             "marital_status"  => "required",
             "mobile"          => "required|phone:mobile",
             "home_address"    => "required",
             "military_status" => "required",
        ];
    }



    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [
             "name_first.required"      => trans("depoint::resume.validation.name_first_required"),
             "name_last.required"       => trans("depoint::resume.validation.name_last_required"),
             "name_father.required"     => trans("depoint::resume.validation.name_father_required"),
             "code_id.required"         => trans("depoint::resume.validation.code_id_required"),
             "code_melli.required"      => trans("depoint::resume.validation.code_melli_required"),
             "code_melli.code_melli"    => trans("depoint::resume.validation.code_melli_code_melli"),
             "birth_date.required"      => trans("depoint::resume.validation.birth_date_required"),
             "birth_city.required"      => trans("depoint::resume.validation.birth_city_required"),
             "marital_status.required"  => trans("depoint::resume.validation.marital_status_required"),
             "mobile.required"          => trans("depoint::resume.validation.mobile_required"),
             "mobile.mobile"            => trans("depoint::resume.validation.mobile_mobile"),
             "home_address.required"    => trans("depoint::resume.validation.home_address_required"),
             "military_status.required" => trans("depoint::resume.validation.military_status_required"),
        ];
    }



    /**
     * @inheritDoc
     */
    protected function fillableFields()
    {
        return [
             "name_first",
             "name_last",
             "name_father",
             "code_id",
             "code_melli",
             "insurance_number",
             "birth_date",
             "birth_city",
             "marital_status",
             "nationality1",
             "nationality2",
             "email",
             "military_status",
             "exemption_type",
             "mobile",
             "home_address",
             "postal_code",
             "home_tel",
             "work_address",
             "work_tel",
             "dependants",
             "educational",
             "workplace",
             "specialized",
             "knowing",
             "software",
             "relatives",
        ];
    }

}
