<?php

namespace Modules\Depoint\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Depoint\Services\Sorooshan\Sorooshan;
use Modules\Yasna\Services\YasnaRequest;


class SorooshanCitiesRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->sorooshan()->isAavailable();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'state' => [
                  'integer',
                  Rule::in($this->acceptableStateIds()),
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function all($keys = null)
    {
        return array_merge(parent::all($keys), [
             'state' => $this->route('state'),
        ]);
    }



    /**
     * Returns an array of the IDs which are acceptable as the state.
     *
     * @return array
     * @throws \Exception
     */
    protected function acceptableStateIds()
    {
        return collect($this->sorooshan()->getStates())
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Returns the Sorooshan singleton instance.
     *
     * @return Sorooshan
     */
    protected function sorooshan()
    {
        return app('depoint.sorooshan');
    }
}
