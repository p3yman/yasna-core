<?php

namespace Modules\Depoint\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class DepointOrderChangeStatusRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = "depoint-order";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'status' => [
                  'required',
                  Rule::in($this->model->selectableStatuses()),
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        $contained = $model->isAccessible();
        return ($model->exists and $contained);
    }
}
