<?php

namespace Modules\Depoint\Http\Requests;

use App\Models\Post;
use App\Models\Posttype;
use Modules\Yasna\Services\YasnaRequest;

class CommentSaveRequest extends YasnaRequest
{
    /**
     * The post which comment will be saved for
     *
     * @var Post|null
     */
    protected $post;
    /**
     * The posttype which comment will be saved for
     *
     * @var Posttype|null
     */
    protected $posttype;



    /**
     * Returns an array of general rule (which are needed for all comments).
     *
     * @return array
     */
    protected function generalRules()
    {
        return [
             'post_id'   => 'exists:posts,id',
             'parent_id' => 'exists:comments,id',
             'email'     => 'email',
             'mobile'    => 'phone:mobile',
             'text'      => 'required_without:message',
        ];
    }



    /**
     * Returns an instance pointing to the Post model which the comment will be saved for.
     *
     * @return Post
     */
    public function getPost()
    {
        if (!$this->post) {
            $post_id = ($this->data['post_id'] ?? 0);

            $this->post = post($post_id);
        }

        return $this->post;
    }



    /**
     * Returns an instance pointing to the Posttype model which the comment will be saved for.
     *
     * @return Posttype
     */
    public function getPosttype()
    {
        if (!$this->posttype) {
            $this->posttype = $this->getPost()->posttype;
        }

        return $this->posttype;
    }



    /**
     * Returns rules based on the post
     *
     * @return array
     */
    protected function getPostRules()
    {
        $rules = [];

        $post = $this->post;

        if ($post->exists) {
            $this->post   = $post;
            $rules_string = $post->getMeta('rules');
            $rules        = $this->safeRulesArray(depoint()->commenting()->translateRules($rules_string));
        }

        return $rules;
    }



    /**
     * Returns rules based on the posttype.
     *
     * @return array
     */
    protected function getPosttypeRules()
    {
        $posttype = $this->getPosttype();

        return $this->safeRulesArray(depoint()->commenting()->posttypeRules($posttype));
    }



    /**
     * Return general rules.
     *
     * @return array
     */
    protected function getGeneralRules()
    {
        return $this->safeRulesArray($this->generalRules());
    }



    /**
     * Returns a safe version of the given rules.
     *
     * @param array $rules
     *
     * @return array
     */
    protected function safeRulesArray(array $rules)
    {
        return array_map(function ($item) {
            return is_string($item) ? explode('|', $item) : $item;
        }, $rules);
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(
             $this->getPostRules(),
             $this->getPosttypeRules(),
             $this->getGeneralRules()
        );
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->post) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'post_id'   => 'dehash',
             'parent_id' => 'dehash',
             'mobile'    => 'ed',
        ];
    }



    /**
     * Sets default data.
     */
    protected function setDefaultData()
    {
        $this->data['user_id']     = user()->id;
        $this->data['guest_ip']    = request()->ip();
        $this->data['posttype_id'] = $this->getPosttype()->id;

    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->setDefaultData();
    }



    /**
     * Returns translated post fields.
     *
     * @return array
     */
    public function getPostFields()
    {
        $result = [];

        $post = $this->getPost();

        if ($post->exists) {
            $this->post = $post;
            $string     = $post->getMeta('fields');
            $result     = depoint()->commenting()->translateFields($string);
        }

        return $result;
    }



    /**
     * Returns fillable fields based on the post.
     *
     * @return array
     */
    protected function getPostFillableFields()
    {
        return array_keys($this->getPostFields());
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_merge(['post_id'], $this->getPostFillableFields());
    }

}
