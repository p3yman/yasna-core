<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class ProviderSaveRequest extends YasnaRequest
{
    protected $model_name = "depoint-provider";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name'      => 'required',
             'telephone' => 'required|phone',
             'address'   => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'name'      => trans_safe('depoint::validation.attributes.name'),
             'telephone' => trans_safe('depoint::validation.attributes.telephone'),
             'address'   => trans_safe('depoint::validation.attributes.address'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name'      => 'ed',
             'telephone' => 'ed',
             'address'   => 'ed',
        ];
    }
}
