<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class OrderUpdateRequest extends YasnaRequest
{
    protected $model_name   = "depoint-order";
    public    $cached_items = [];



    /**
     * Corrects counts array.
     */
    protected function correctCounts()
    {
        $counts      = ($this->data['counts'] ?? []);
        $safe_counts = [];

        foreach ($counts as $item_hashid => $count) {
            $item = model('depoint-order-item', $item_hashid);

            if ($item->not_exists) {
                continue;
            }

            $this->cached_items[$item->id] = $item;
            $safe_counts[$item->id]        = ed($count);
        }

        $this->data['counts'] = $safe_counts;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctCounts();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'counts' => 'required|array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'counts.required' => trans_safe('depoint::spare-parts.order.unable-to-remove-all'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['counts', 'is_verified'];
    }
}
