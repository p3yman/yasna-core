<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class SparePartAddToCartRequest extends YasnaRequest
{
    protected $model_name   = "depoint-spare-part";
}
