<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class ProviderDeleteRequest extends YasnaRequest
{
    protected $model_name = "depoint-provider";
}
