<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class PostColorImagesRequest extends YasnaRequest
{
    protected $model_name = "post";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'color_images' => 'array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['color_images'];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->exists and $model->hasColorImages());
    }
}
