<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * @property string $_submit
 * @property string $id
 */
class ResumesRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = 'laravel';

    /**
     * @inheritdoc
     */
    protected $model_name = "depoint-resume";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;
}
