<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class DrawPrepareRequest extends YasnaRequest
{
    protected $model_name = "post";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->exists and $model->hasDrawPossibility());
    }
}
