<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class ProductDeleteRequest extends YasnaRequest
{
    protected $model_name = "depoint-product";
}
