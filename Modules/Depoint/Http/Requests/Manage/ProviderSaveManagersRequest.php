<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Providers\ValidationServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class ProviderSaveManagersRequest extends YasnaRequest
{
    protected $model_name = "depoint-provider";



    /**
     * Explode managers list and make an array.
     */
    protected function explodeManagersList()
    {
        if ($this->data['managers']) {
            $this->data['managers'] = explode(',', ($this->data['managers'] ?? ''));
        } else {
            $this->data['managers'] = [];
        }
    }



    /**
     * Rules to purify managers items.
     *
     * @return array
     */
    protected function purifierManagerRules()
    {
        $managers = $this->data['managers'];
        $keys     = array_keys($managers);

        return array_fill_keys($keys, 'dehash');
    }



    /**
     * Purify managers items.
     */
    protected function purifyManagersList()
    {
        $this->data['managers'] = ValidationServiceProvider::purifier(
             $this->data['managers'],
             $this->purifierManagerRules()
        );
    }



    /**
     * Correct managers list.
     */
    protected function correctManagersList()
    {
        $this->explodeManagersList();
        $this->purifyManagersList();
    }



    /**
     * Correct data.
     */
    public function corrections()
    {
        $this->correctManagersList();
    }
}
