<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Depoint\Providers\SparePartsServiceProvider;
use Modules\Yasna\Providers\ValidationServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class CartSaveRequest extends YasnaRequest
{
    protected $model_name         = "cart";
    public    $cached_providers   = [];
    public    $cached_spare_parts = [];



    /**
     * apply `ed` purification on all counts.
     *
     * @return void
     */
    protected function edCounts()
    {
        $counts = ($this->data['counts'] ?? []);

        foreach ($counts as $provider => $spare_parts) {
            $spare_parts    = is_array($spare_parts) ? $spare_parts : [];
            $keys           = array_keys($spare_parts);
            $purifier_rules = array_fill_keys($keys, 'ed');

            $counts[$provider] = ValidationServiceProvider::purifier($spare_parts, $purifier_rules);
        }

        $this->data['counts'] = $counts;
    }



    /**
     * clear empty counts.
     *
     * @return void
     */
    protected function clearCounts()
    {
        $counts = ($this->data['counts'] ?? []);

        foreach ($counts as $provider => $spare_parts) {
            $spare_parts = is_array($spare_parts) ? $spare_parts : [];

            $not_empty_data = array_filter($spare_parts, function ($count) {
                return !empty($count);
            });

            if (count($not_empty_data)) {
                $counts[$provider] = $not_empty_data;
            } else {
                unset($counts[$provider]);
            }
        }

        $this->data['counts'] = $counts;
    }



    /**
     * check and pass only valid hashids
     */
    protected function checkCounts()
    {
        $counts      = ($this->data['counts'] ?? []);
        $safe_counts = [];

        foreach ($counts as $provider_hashid => $spare_parts) {
            $provider = model('depoint-provider', $provider_hashid);

            if ($provider->not_exists) {
                continue;
            }

            $this->cached_providers[$provider->hashid] = $provider;

            $spare_parts = is_array($spare_parts) ? $spare_parts : [];

            foreach ($spare_parts as $spare_part_hashid => $count) {
                $spare_part = model('depoint-spare-part', $spare_part_hashid);

                if ($spare_part->not_exists) {
                    continue;
                }

                $this->cached_spare_parts[$spare_part->hashid] = $spare_part;

                $safe_counts[$provider_hashid][$spare_part_hashid] = $count;
            }
        }

        $this->data['counts'] = $safe_counts;
    }



    /**
     * correct values of `counts` array
     */
    protected function correctCounts()
    {
        $this->edCounts();
        $this->clearCounts();
        $this->checkCounts();
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctCounts();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'counts' => 'required|array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'counts.required' => trans_safe('depoint::spare-parts.cart.empty-request-note'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function loadRequestedModel($id_or_hashid = false)
    {
        $this->model = user()->currentDepointCart();
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return SparePartsServiceProvider::cartIsAccessible();
    }
}
