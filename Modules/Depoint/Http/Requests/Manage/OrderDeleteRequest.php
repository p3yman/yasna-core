<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class OrderDeleteRequest extends YasnaRequest
{
    protected $model_name = "depoint-order";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        
        return ($model->isNotTrashed() and $model->can('delete'));
    }
}
