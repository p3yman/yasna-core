<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Providers\ValidationServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class ProductSaveRequest extends YasnaRequest
{
    protected $model_name = "depoint-product";



    /**
     * Correct the titles.
     */
    public function correctTitle()
    {
        $title = ($this->data['titles'] ?? []);

        $this->data['titles'] = array_filter($title, function ($item) {
            return boolval($item);
        });
    }



    /**
     * Change value to boolean.
     */
    public function correctIsActive()
    {
        $this->data['is_active'] = boolval($this->data['is_active']);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctTitle();
        $this->correctIsActive();
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'titles.min' => trans('depoint::validation.at-least-one-required'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'code'   => trans('depoint::validation.attributes.code'),
             'titles' => trans('depoint::validation.attributes.titles'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'code'   => 'required',
             'titles' => 'required|array|min:1',

        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (hashid($this->data['id']) == 0) {
            return model('depoint-product')->can('create');
        } else {
            return model('depoint-product')->can('edit');
        }
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'code' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function all($keys = null)
    {
        parent::all($keys);

        $this->titlePurify();

        return $this->data;
    }



    /**
     * Return array of languages which title is specified in.
     *
     * @return array
     */
    protected function languageKeys()
    {
        return array_keys($this->data['titles']);
    }



    /**
     * Purifier array for titles.
     *
     * @return array
     */
    protected function titlePurifier()
    {
        return array_fill_keys($this->languageKeys(), 'ed');
    }



    /**
     * Purify titles.
     *
     * @return void
     */
    protected function titlePurify()
    {
        $this->data['titles'] = ValidationServiceProvider::purifier(
             $this->data['titles'],
             $this->titlePurifier()
        );
    }
}
