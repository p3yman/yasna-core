<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class DrawSelectRequest extends YasnaRequest
{
    protected $model_name = "post";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'number' => "required|numeric|min:1|max:" . session()->get('line_number'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return ['number' => 'ed'];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        return ($model->exists and $model->hasDrawPossibility());
    }
}
