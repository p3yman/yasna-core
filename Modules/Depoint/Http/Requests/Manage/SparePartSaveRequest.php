<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Modules\Yasna\Services\YasnaRequest;

class SparePartSaveRequest extends YasnaRequest
{
    protected $model_name   = "depoint-spare-part";
    public    $cached_files = [];



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name'            => 'required',
             'code'            => 'required',
             'inventory'       => 'required|numeric',
             'daily_use'       => 'required|numeric',
             'production_days' => 'required|numeric',
             'alert_days'      => 'required|numeric',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'english_name'    => 'ed',
             'code'            => 'ed',
             'inventory'       => 'ed',
             'daily_use'       => 'ed',
             'production_days' => 'ed',
             'alert_days'      => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'name'            => trans_safe('depoint::validation.attributes.name'),
             'code'            => trans_safe('depoint::validation.attributes.code'),
             'inventory'       => trans_safe('depoint::validation.attributes.inventory'),
             'daily_use'       => trans_safe('depoint::validation.attributes.daily_use'),
             'production_days' => trans_safe('depoint::validation.attributes.production_days'),
             'alert_days'      => trans_safe('depoint::validation.attributes.alert_days'),
        ];
    }



    /**
     * Correct the `is_ok` field value.
     */
    protected function correctStatus()
    {
        $this->data['is_ok'] = boolval($this->data['is_ok'] ?? false);
    }



    /**
     * check if the given hashid is for a valid file.
     *
     * @param string $hashid
     *
     * @return bool
     */
    protected function fileIsValid($hashid)
    {
        $doc = fileManager()->file($hashid)->resolve();

        if ($doc->getUrl()) {
            $this->cached_files[$hashid] = $doc;
            return true;
        } else {
            return false;
        }
    }



    /**
     * correct `main_files`
     */
    protected function correctMainFiles()
    {
        $array                    = (json_decode($this->data['main_files'] ?? '') ?? []);
        $safe_array               = array_filter($array, function ($hashid) {
            return $this->fileIsValid($hashid);
        });
        $this->data['main_files'] = $safe_array;
    }



    /**
     * correct `preview_file`
     */
    protected function correctPreviewFile()
    {
        $array  = (json_decode($this->data['preview_file'] ?? '') ?? []);
        $hashid = ($array[0] ?? '');
        if ($this->fileIsValid($hashid)) {
            $this->data['preview_file'] = $hashid;
        } else {
            $this->data['preview_file'] = '';
        }
    }



    /**
     * correct `products`
     */
    protected function correctProducts()
    {
        $safe_string = ($this->data['products'] ?? '');
        $array       = $safe_string ? explode(',', $safe_string) : [];

        $this->data['products'] = array_map(function ($hashid) {
            return hashid($hashid);
        }, $array);
    }



    /**
     * correct `providers`
     */
    protected function correctProviders()
    {
        $safe_string = ($this->data['providers'] ?? '');
        $array       = $safe_string ? explode(',', $safe_string) : [];

        $this->data['providers'] = array_map(function ($hashid) {
            return hashid($hashid);
        }, $array);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctStatus();
        $this->correctMainFiles();
        $this->correctPreviewFile();
        $this->correctProducts();
        $this->correctProviders();
    }
}
