<?php

namespace Modules\Depoint\Http\Requests\Manage;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaRequest;

class ReportRequest extends YasnaRequest
{

    /**
     * correction time data
     */
    public function corrections()
    {
        if (isset($this->data['birth_date'])) {
            $this->data['birth_date'] = empty($this->data['_birth_date']) ? null : $this->getTimeFromUnix($this->data['birth_date']);
        }
        if (isset($this->data['marriage_date'])) {
            $this->data['marriage_date'] = empty($this->data['marriage_date']) ? null : $this->getTimeFromUnix($this->data['marriage_date']);
        }
    }



    /**
     * get a valid date from unix time
     *
     * @param $time
     *
     * @return bool|string
     */
    private function getTimeFromUnix($time)
    {
        //return pre value without change
        if (!$this->isValidTimeStamp($time)) {
            return $time;
        }

        $time = substr($time, 0, strlen($time) - 3);
        $time = Carbon::parse(date('Y-m-d', $time))->toDateString();
        return $time;
    }



    /**
     * check that input is a valid timestamp
     *
     * @param $timestamp
     *
     * @return bool
     */
    public function isValidTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === $timestamp)
             && ($timestamp <= PHP_INT_MAX)
             && ($timestamp >= ~PHP_INT_MAX);
    }
}
