<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CartRemoveItemRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = 'order';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists;
    }
}
