<?php

namespace Modules\Depoint\Http\Requests;

use App\Models\Address;
use Modules\Yasna\Services\YasnaRequest;

/**
 * The Main to Delete The Specified Address
 *
 * @property Address $model
 */
class AddressDeleteRequest extends YasnaRequest
{
    protected $model_name         = "address";
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->isOwnedAddress();
    }



    /**
     * Checks if the given address is owned by current client.
     *
     * @return bool
     */
    protected function isOwnedAddress()
    {
        $model = $this->model;

        return $model->exists and ($model->user_id == user()->id);
    }
}
