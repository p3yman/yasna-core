<?php

namespace Modules\Depoint\Http\Requests;

use Closure;
use Illuminate\Contracts\Validation\Validator;
use Modules\Yasna\Services\YasnaRequest;

class EditPasswordRequest extends YasnaRequest
{
    protected $model_name = "user";



    /**
     * get an array of fillable fields, which are supposed to appear in the request submit.
     *
     * @return array
     */
    public function fillableFields()
    {
        return [
             'old_password',
             'password',
             'password2',
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "old_password" => "required",
             "password"     => "required|string|min:6|same:password2",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'old_password' => currentModule()->trans('validation.attributes.last_password'),
             'password'     => currentModule()->trans('validation.attributes.new_password'),
             'password2'    => currentModule()->trans('validation.attributes.new_password_repeat'),
        ];
    }
}
