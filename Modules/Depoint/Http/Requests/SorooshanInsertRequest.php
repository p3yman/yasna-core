<?php

namespace Modules\Depoint\Http\Requests;

use Exception;
use Illuminate\Validation\Rule;
use Modules\Depoint\Services\Sorooshan\Sorooshan;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use Modules\Yasna\Services\YasnaRequest;

class SorooshanInsertRequest extends YasnaRequest
{
    use ModuleRecognitionsTrait;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->sorooshan()->isAavailable();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name'    => 'required',
             'state'   => [
                  'required',
                  Rule::in($this->acceptableStatesIds()),
             ],
             'city'    => [
                  'required',
                  Rule::in($this->acceptableCitiesIds()),
             ],
             'tel'     => 'required|phone',
             'mobile'  => 'required|phone:mobile',
             'email'   => 'email',
             'address' => 'required',
             'serial'  => 'required',
             'sender'  => 'required',
             'action'  => [
                  'required',
                  Rule::in($this->acceptableActionValues()),
             ],
             'desc'    => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $module = $this->runningModule();

        return [
             'name'    => $module->getTrans('general.service.name'),
             'state'   => $module->getTrans('general.service.province'),
             'city'    => $module->getTrans('general.service.city'),
             'tel'     => $module->getTrans('general.service.tel'),
             'mobile'  => $module->getTrans('general.service.mobile'),
             'email'   => $module->getTrans('general.service.email'),
             'address' => $module->getTrans('general.service.address'),
             'serial'  => $module->getTrans('general.service.serial'),
             'sender'  => $module->getTrans('general.service.sender'),
             'action'  => $module->getTrans('general.service.action'),
             'desc'    => $module->getTrans('general.service.desc'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'tel'    => 'ed',
             'mobile' => 'ed',
             'email'  => 'ed',
             'serial' => 'ed',
        ];
    }



    /**
     * Returns an array of acceptable values of the `state` field.
     *
     * @return array
     * @throws Exception
     */
    protected function acceptableStatesIds()
    {
        return collect($this->acceptableStates())
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Returns an array of acceptable states.
     *
     * @return \Illuminate\Contracts\Cache\Repository
     * @throws Exception
     */
    protected function acceptableStates()
    {
        return $this->sorooshan()->getStates();
    }



    /**
     * Returns an array of acceptable values of the `city` field.
     *
     * @return array
     */
    protected function acceptableCitiesIds()
    {
        return collect($this->acceptableCities())
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * Returns an array of acceptable cities.
     *
     * @return array|\Illuminate\Contracts\Cache\Repository
     */
    protected function acceptableCities()
    {
        $requested_state = $this->getData('state');

        if (!$requested_state) {
            return [];
        }


        return $this
             ->sorooshan()
             ->getCitiesByStateId($requested_state)
             ;
    }



    /**
     * Returns an array of acceptable values of the `action` field.
     *
     * @return array
     */
    protected function acceptableActionValues()
    {
        return $this->sorooshan()->getServiceTypes();
    }



    /**
     * Returns the Sorooshan singleton instance.
     *
     * @return Sorooshan
     */
    protected function sorooshan()
    {
        return app('depoint.sorooshan');
    }
}
