<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CartAddCouponCodeRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ['code' => 'required'];
    }
}
