<?php

namespace Modules\Depoint\Http\Requests\Auth;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Modules\Depoint\Http\Controllers\Auth\ForgotPasswordController;
use Modules\Depoint\Services\Module\ModuleTrait;
use Modules\Yasna\Services\YasnaRequest;

class PasswordResetTokenRequest extends YasnaRequest
{
    use ModuleTrait;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'email'                => [
                  'required',
                  'email',
                  Rule::exists('users', 'email'),
             ],
             'token'                => 'required',
             'g-recaptcha-response' => !env('APP_DEBUG') ? 'required|captcha' : '',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'token' => currentModule()->trans('general.auth.reset_password_token'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'token' => 'ed',
        ];
    }



    /**
     * Returns the found or a new user.
     *
     * @return User
     */
    protected function getModel()
    {
        return ($this->model ?: model('user'));
    }



    /**
     * Returns the session's name.
     *
     * @return string
     */
    protected function getSessionName()
    {
        return app(ForgotPasswordController::class)
             ->getSessionName();
    }



    /**
     * Checks token value.
     */
    protected function checkTokenValue()
    {
        if (!Hash::check($this->data['token'], $this->getModel()->reset_token)) {
            unset($this->data['token']);
        }
    }



    /**
     * Checks token expiration.
     */
    protected function checkTokenExpiration()
    {
        if (now()->greaterThan($this->getModel()->reset_token_expire)) {
            unset($this->data['token']);
        }
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        $email = ($this->data['email'] ?? null);

        if (!$email) {
            return;
        }

        $this->model = user()->firstOrNew(['email' => $email]);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->checkTokenValue();
        $this->checkTokenExpiration();
    }



    /**
     * @inheritdoc
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new ValidationException($validator))
             ->withMessages([currentModule()->trans('general.auth.invalid_inputs')])
             ->redirectTo($this->getRedirectUrl())
        ;
    }
}
