<?php

namespace Modules\Depoint\Http\Requests\Auth;

use Modules\Yasna\Services\YasnaRequest;

class PasswordResetNewRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'new_password'         => 'required|same:new_password2|string|min:6',
             'g-recaptcha-response' => !env('APP_DEBUG') ? 'required|captcha' : '',
        ];
    }
}
