<?php

namespace Modules\Depoint\Http\Requests\Auth;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class PasswordResetRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'email'                => [
                  'required',
                  Rule::exists('users', 'email'),
             ],
             'g-recaptcha-response' => !env('APP_DEBUG') ? 'required|captcha' : '',
        ];
    }
}
