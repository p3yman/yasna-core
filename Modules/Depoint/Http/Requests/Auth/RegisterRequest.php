<?php

namespace Modules\Depoint\Http\Requests\Auth;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class RegisterRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name_first'           => 'required|string|max:255',
             'name_last'            => 'required|string|max:255',
             'email'                => 'required|string|email|max:255|unique:users',
             'mobile'               => 'required|phone:mobile',
             'password'             => 'required|same:password2|string|min:6',
             'terms_and_conditions' => 'required',
             'g-recaptcha-response' => !env('APP_DEBUG') ? 'required|captcha' : '',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'terms_and_conditions.required' => currentModule()
                  ->trans('general.terms-and-conditions-needed'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name_first' => 'ed',
             'name_last'  => 'ed',
             'email'      => 'ed',
             'mobile'     => 'ed',
        ];
    }
}
