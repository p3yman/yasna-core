<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class SparePartSaveFilesRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = "depoint-spare-part";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'main_files' => 'array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['main_files'] = ($this->data['main_files'] ?? []);
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->exists and $model->isAccessibleForUser());
    }
}
