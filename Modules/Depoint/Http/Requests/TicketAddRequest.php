<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketAddRequest extends YasnaRequest
{
    /**
     * set rule on add ticket form
     *
     * @return array
     */
    public function rules()
    {
        return [
             "priority" => 'required',
             "text"     => 'required',
             'subject'  => 'required',
        ];
    }

}
