<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class SupportTicketAddRequest extends YasnaRequest
{
    /**
     * set rule on support ticket form
     *
     * @return array
     */
    public function rules()
    {
        return [
             'subject'    => 'required',
             'text'       => 'required',
             'name_first' => 'required',
             'name_last'  => 'required',
             'mobile'     => 'required|phone:mobile',
             'email'      => 'required|email',
        ];
    }

}
