<?php

namespace Modules\Depoint\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class PurchaseSetDeliveryInfoRequest extends YasnaRequest
{
    /**
     * Checks if this request should save new address.
     *
     * @return bool
     */
    public function isSavingNewAddress()
    {
        return !boolval($this->data['selected_address'] ?? true);
    }



    /**
     * Checks if this request should not save new address.
     *
     * @return bool
     */
    public function isNotSavingNewAddress()
    {
        return !$this->isSavingNewAddress();
    }



    /**
     * Returns rules for new address fields.
     *
     * @return array
     */
    protected function newAddressRules()
    {
        if ($this->isSavingNewAddress()) {
            return [
                 'receiver'    => 'required|string',
                 'city_id'     =>
                      'required'
                      . '|'
                      . Rule::exists('states', 'id')->where(function ($query) {
                          $query->where('parent_id', '<>', 0);
                      })
                 ,
                 'address'     => 'required',
                 'telephone'   => 'required|phone',
                 'postal_code' => 'required|postal_code',
            ];
        }

        return [];
    }



    /**
     * Returns rules of address.
     *
     * @return array
     */
    protected function addressRules()
    {
        if ($this->isNotSavingNewAddress()) {
            return [
                 'selected_address' =>
                      'required'
                      . '|'
                      . Rule::exists('addresses', 'id', function ($query) {
                          $query->where('user_id', user()->id);
                      }),
            ];
        }
        return [];
    }



    /**
     * Returns an array of rules depending on the shipping method.
     *
     * @return array
     */
    protected function shippingMethodRules()
    {
        return [
             'shipping_method' =>
                  'required'
                  . '|'
                  . Rule::in(
                       array_column(
                            depoint()->cart()->findActiveCart()->availableShippingMethods(),
                            'id'
                       )
                  ),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'selected_address' => 'dehash',
             'telephone'        => 'ed',
             'postal_code'      => 'ed',
             'city_id'          => 'dehash',
             'shipping_method'  => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'selected_address.required' => currentModule()->trans('general.address.required'),
             'selected_address.in'       => currentModule()->trans('general.address.required'),
             'shipping_method.required'  => currentModule()->trans('validation.select-required'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'shipping_method' => $this->runningModule()->getTrans('cart.shipping.method.title.singular'),
        ];
    }
}
