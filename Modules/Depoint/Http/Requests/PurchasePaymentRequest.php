<?php

namespace Modules\Depoint\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class PurchasePaymentRequest extends YasnaRequest
{
    /**
     * Returns an array of acceptable accounts' ids.
     *
     * @return array
     */
    protected function acceptableAccountIds()
    {
        return depoint()
             ->shop()
             ->acceptablePaymentAccounts()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'account' => [
                  'required',
                  Rule::in($this->acceptableAccountIds()),
             ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'account' => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'account.required' => currentModule()->trans('validation.payment-type-required'),
        ];
    }
}
