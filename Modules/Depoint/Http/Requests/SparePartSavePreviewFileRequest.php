<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Depoint\Services\Module\ModuleTrait;
use Modules\Yasna\Services\YasnaRequest;

class SparePartSavePreviewFileRequest extends YasnaRequest
{
    use ModuleTrait;

    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = "depoint-spare-part";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'preview_file' => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'preview_file' => static::module()->trans('spare-parts.spare-part.preview-file'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->exists and $model->isAccessibleForUser());
    }
}
