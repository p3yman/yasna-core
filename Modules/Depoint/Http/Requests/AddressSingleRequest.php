<?php

namespace Modules\Depoint\Http\Requests;

use App\Models\Address;
use Modules\Yasna\Services\YasnaRequest;

/**
 * The Main Request to Render the Single Page of Current Client's Addresses
 *
 * @property Address $model
 */
class AddressSingleRequest extends YasnaRequest
{
    protected $model_name = "address";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->isOwnedAddress() or $this->isNewAddress()
        );
    }



    /**
     * Checks if the given address is owned by current client.
     *
     * @return bool
     */
    protected function isOwnedAddress()
    {
        $model = $this->model;

        return $model->exists and ($model->user_id == user()->id);
    }



    /**
     * Checks if this request is for a new address.
     *
     * @return bool
     */
    protected function isNewAddress()
    {
        return (request()->route()->parameter('hashid') == 'new');
    }
}
