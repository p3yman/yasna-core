<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CartAddItemRequest extends YasnaRequest
{
    protected $model_name = 'ware';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'count' => 'required|numeric|min:1',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'count' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->exists and $model->isAvailableForCurrentUser());
    }
}
