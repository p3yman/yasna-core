<?php

namespace Modules\Depoint\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class CartRepurchaseRequest extends YasnaRequest
{
    /**
     * The Name fo the Main Model
     *
     * @var string
     */
    protected $model_name = "cart";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'account' => [
                  'required',
                  Rule::in($this->acceptableAccountsIds()),
             ],
        ];
    }



    /**
     * Returns an array of acceptable values for the `account` field
     *
     * @return array
     */
    protected function acceptableAccountsIds()
    {
        return depoint()
             ->shop()
             ->repurchasePaymentAccounts()
             ->pluck('id')
             ->toArray()
             ;
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             'account' => currentModule()->trans('cart.gateway'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'account.required' => currentModule()->trans('cart.message.failure.gateway-required'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        return ($model->exists and ($model->user_id == user()->id));
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'account' => 'dehash',
        ];
    }
}
