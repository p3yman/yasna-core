<?php

namespace Modules\Depoint\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TicketReplyRequest extends YasnaRequest
{
    /**
     * set rule for reply ticket form
     *
     * @return array
     */
    public function rules()
    {
        return [
             'text'        => 'required',
             'ticket_hash' => 'required',
        ];
    }

}
