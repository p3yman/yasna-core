<?php

namespace Modules\Depoint\Http\Controllers;

use App\Models\Tag;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class TagController extends DepointFrontControllerAbstract
{
    const PAGINATION_LIMIT = 24;

    protected $base_model  = "tag";
    protected $view_folder = "tag";



    /**
     * Renders the index page of the requested tag.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(SimpleYasnaRequest $request)
    {
        $tag_slug = urldecode($request->tag);
        /** @var Tag $tag */
        $tag = $this->model($tag_slug);

        if ($tag->not_exists) {
            return $this->abort(404);
        }

        $models = PostsServiceProvider::select([
             'tag' => $tag->slug,
        ])->orderByDesc('published_at')
                                      ->paginate(static::PAGINATION_LIMIT)
        ;

        return $this->template('index', compact('tag', 'models'));
    }
}
