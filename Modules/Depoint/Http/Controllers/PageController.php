<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 6:09 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class PageController extends DepointFrontControllerAbstract
{
    /**
     * The folder of views
     *
     * @var string
     */
    protected $view_folder = 'about';



    /**
     * Finds the post object.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null|object
     */
    protected function findPostObject(SimpleYasnaRequest $request)
    {
        return PostsServiceProvider::select([
             'type' => $this->posttype_slugs['pages'],
             'slug' => $request->slug,
        ])->first()
             ;
    }



    /**
     * Renders and returns the page view for the specified slug.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(SimpleYasnaRequest $request)
    {
        $post = $this->findPostObject($request);

        if (!$post) {
            return $this->abort(404);
        }

        $this->setPostSisterhoodLinksParameters($post);
        $this->general_variables['model'] = $post;

        return $this->template('main');

    }



    /**
     * Specific action for the about page
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function about(SimpleYasnaRequest $request)
    {
        $request->merge(['slug' => 'about']);

        return $this->index($request);
    }
}
