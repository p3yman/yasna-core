<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 9:18 PM
 */

namespace Modules\Depoint\Http\Controllers\Auth;

use Modules\Depoint\Services\Controllers\Traits\ModuleControllerTrait;
use Modules\Depoint\Services\Controllers\Traits\TemplateControllerTrait;
use Modules\Depoint\Services\Controllers\Traits\YasnaControllerAlternativeTrait;
use Modules\Yasna\Http\Controllers\Auth\LoginController as YasnaLoginController;

class LoginController extends YasnaLoginController
{
    use YasnaControllerAlternativeTrait;
    use ModuleControllerTrait;
    use TemplateControllerTrait;

    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'auth.login';



    /**
     * @inheritdoc
     */
    public function showLoginForm()
    {
        return $this->template('main');
    }



    /**
     * Return an instance of `Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector` class
     * to redirect the client after login.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectAfterLogin()
    {
        if (user()->isAdmin()) {
            $target = url('manage');
        } else {
            $target = route_locale('depoint.front.dashboard');
        }

        return redirect()->intended($target);
    }
}
