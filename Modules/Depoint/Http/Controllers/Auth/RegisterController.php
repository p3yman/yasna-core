<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 10:26 PM
 */

namespace Modules\Depoint\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Modules\Depoint\Http\Requests\Auth\RegisterRequest;
use Modules\Depoint\Services\Controllers\Traits\ModuleControllerTrait;
use Modules\Depoint\Services\Controllers\Traits\TemplateControllerTrait;
use Modules\Depoint\Services\Controllers\Traits\YasnaControllerAlternativeTrait;
use Modules\Yasna\Http\Controllers\Auth\RegisterController as YasnaRegisterController;

class RegisterController extends YasnaRegisterController
{
    use YasnaControllerAlternativeTrait;
    use ModuleControllerTrait;
    use TemplateControllerTrait;

    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'auth.register';



    /**
     * @inheritdoc
     */
    public function showRegistrationForm()
    {
        return $this->template('main');
    }



    /**
     * The `register()` method with custom validation.
     *
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function customRegister(RegisterRequest $request)
    {
        return $this->register($request);
    }



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, []);
    }



    /**
     * Creates a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        return user()->batchSave([
             'name_first' => $data['name_first'],
             'name_last'  => $data['name_last'],
             'email'      => $data['email'],
             'mobile'     => $data['mobile'],
             'password'   => Hash::make($data['password']),
        ]);
    }
}
