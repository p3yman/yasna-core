<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/27/18
 * Time: 12:54 AM
 */

namespace Modules\Depoint\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Support\Facades\Hash;
use Modules\Depoint\Http\Requests\Auth\PasswordResetNewRequest;
use Modules\Depoint\Http\Requests\Auth\PasswordResetRequest;
use Modules\Depoint\Http\Requests\Auth\PasswordResetTokenRequest;
use Modules\Depoint\Notifications\PasswordResetNotification;
use Modules\Depoint\Services\Controllers\Traits\ModuleControllerTrait;
use Modules\Depoint\Services\Controllers\Traits\TemplateControllerTrait;
use Modules\Depoint\Services\Controllers\Traits\YasnaControllerAlternativeTrait;
use Modules\Yasna\Http\Controllers\Auth\ForgotPasswordController as YasnaForgotPasswordController;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class ForgotPasswordController extends YasnaForgotPasswordController
{
    use YasnaControllerAlternativeTrait;
    use ModuleControllerTrait;
    use TemplateControllerTrait;

    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'auth.password';


    protected $reset_token_length       = 6;
    public    $reset_token_lifetime     = 15;
    public    $reset_token_session_name = 'resettingPasswordId';



    /**
     * @inheritdoc
     */
    public function showTokenRequestForm()
    {
        $auth_title = currentModule()->trans('general.auth.forgot_password');
        $type       = "forgot";

        return $this->template('main', compact('type', 'auth_title'));
    }



    /**
     * Finds a an instance of User model with the given credentials.
     *
     * @param array $credentials
     *
     * @return User
     */
    protected function getUser(array $credentials)
    {
        return user()->where($credentials)->first();
    }



    /**
     * Generates and returns a new token.
     *
     * @return int
     */
    protected function generateNewToken()
    {
        return rand(str_repeat(1, $this->reset_token_length), str_repeat(9, $this->reset_token_length));
    }



    /**
     * Returns the lifetime of tokens.
     *
     * @return int
     */
    protected function getTokenLifeTime()
    {
        return (setting('password_token_expire_time')->gain() ?: $this->reset_token_lifetime);
    }



    /**
     * Returns the session's name.
     *
     * @return string
     */
    public function getSessionName()
    {
        return $this->reset_token_session_name;
    }



    /**
     * Clears the session.
     */
    protected function clearSession()
    {
        session()->forget($this->getSessionName());
    }



    /**
     * Saves the user's id in session.
     *
     * @param User $user
     */
    protected function saveSession(User $user)
    {
        session()->put($this->getSessionName(), $user->id);
    }



    /**
     * Generates token, send it to user and save the user's id in session.
     *
     * @param array $credentials
     */
    protected function saveToken(array $credentials)
    {
        $user  = $this->getUser($credentials);
        $token = $this->generateNewToken();

        $user->batchSave([
             'reset_token'        => Hash::make($token),
             'reset_token_expire' => now()->addMinute($this->getTokenLifeTime())->toDateTimeString(),
        ]);

        $this->saveSession($user);

        $user->notify(new PasswordResetNotification($token, now()));
    }



    /**
     * Handles the password token request.
     *
     * @param PasswordResetRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function request(PasswordResetRequest $request)
    {
        $this->saveToken($request->only('email'));

        return redirect()->route('password.token');
    }



    /**
     * Retrieve the user from the session.
     *
     * @return User|null
     */
    protected function sessionUser()
    {
        $session_name = $this->getSessionName();

        if (!session()->has($session_name)) {
            return null;
        }

        $session_value = session($session_name);

        if (!$session_value) {
            return null;
        }


        $user = user($session_value);

        if ($user->not_exists) {
            return null;
        }

        return $user;
    }



    /**
     * Handles email for token form.
     *
     * @param SimpleYasnaRequest $request
     */
    protected function tokenFormCheckSession(SimpleYasnaRequest $request)
    {
        $this->general_variables['email_value'] = null;

        if ($request->not_fresh) {
            return;
        }

        $user = $this->sessionUser();

        if (!$user) {
            return;
        }

        $this->general_variables['email_value'] = $user->email;
        session()->forget($this->getSessionName());
    }



    /**
     * Renders and returns the token form.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showTokenForm(SimpleYasnaRequest $request)
    {
        $this->tokenFormCheckSession($request);

        $auth_title = currentModule()->trans('general.auth.reset_password_token');
        $type       = "token";


        return $this->template('main', compact('type', 'auth_title'));
    }



    /**
     * Checks the sent token.
     *
     * @param PasswordResetTokenRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function checkToken(PasswordResetTokenRequest $request)
    {
        $user = $request->model;

        session()->put($this->getSessionName(), $user->id);

        return redirect()->route('password.new');
    }



    /**
     * Renders and returns the reset form.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showResetForm()
    {
        $session_user = $this->sessionUser();

        if (!$session_user) {
            return redirect()->route('password.request');
        }

        $auth_title = trans('depoint::general.auth.reset_password');
        $type       = "reset";

        return $this->template('main', compact('type', 'auth_title'));
    }



    /**
     * Saves the new password for user.
     *
     * @param User   $user
     * @param string $password
     */
    protected function updateUserPassword(User $user, $password)
    {
        $user->batchSave([
             'password'           => Hash::make($password),
             'reset_token'        => '',
             'reset_token_expire' => null,
        ]);
    }



    /**
     * Makes the user logged in.
     *
     * @param User $user
     */
    protected function loginUser(User $user)
    {
        login($user->id);
    }



    /**
     * Saves the new password for user.
     *
     * @param PasswordResetNewRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveNewPassword(PasswordResetNewRequest $request)
    {
        $user = $this->sessionUser();

        if (!$user) {
            return redirect()->route('password.request');
        }

        $this->updateUserPassword($user, $request->new_password);
        $this->loginUser($user);

        $this->clearSession();

        return redirect()->route('home');
    }
}
