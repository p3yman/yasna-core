<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/11/18
 * Time: 8:55 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Http\Requests\CartAddCouponCodeRequest;
use Modules\Depoint\Http\Requests\CartAddItemRequest;
use Modules\Depoint\Http\Requests\CartEditItemCountRequest;
use Modules\Depoint\Http\Requests\CartRemoveCouponCodeRequest;
use Modules\Depoint\Http\Requests\CartRemoveItemRequest;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class CartController extends DepointFrontControllerAbstract
{
    /**
     * The Folder's Name
     *
     * @var string
     */
    protected $view_folder = 'order.cart';



    /**
     * Reloads the cart link in the header.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reloadView()
    {
        $this->templateCart();
        return view($this->module()->bladePath('layout.header.cart'));
    }



    /**
     * Adds the specified ware with the given count to the active cart.
     *
     * @param CartAddItemRequest $request
     *
     * @return string
     */
    public function add(CartAddItemRequest $request)
    {
        $ware = $request->model;

        $done = depoint()->cart()->addToActiveCart($ware->id, $request->count);

        return $this->jsonAjaxSaveFeedback($done, [
             'success_message'      => $this->module()->trans('cart.message.success.item-added'),
             'success_callback'     => 'reloadCart()',
             'success_feed_timeout' => 5000,
        ]);
    }



    /**
     * The main page of the cart.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        return $this->template('main');
    }



    /**
     * Removes a specified item from current client's cart.
     *
     * @param CartRemoveItemRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function remove(CartRemoveItemRequest $request)
    {
        $cart = depoint()->cart()->findActiveCart();
        $cart->removeOrder($request->id);

        $this->templateCart();
        return view($this->module()->bladePath('order.cart.content'));
    }



    /**
     * Modifies the count of the specified order.
     *
     * @param CartEditItemCountRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editCount(CartEditItemCountRequest $request)
    {
        $order = $request->model;
        $order->alterCount($request->count);

        $this->templateCart();
        return view($this->module()->bladePath('order.cart.content'));
    }



    /**
     * Applies the specified code on the client's cart.
     *
     * @param CartAddCouponCodeRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function addCouponCode(CartAddCouponCodeRequest $request)
    {
        $cart = depoint()->cart()->findActiveCart();
        $code = $request->code;

        if (!$cart->applyCouponCode($code)) {
            return $this->abort(400, true, true, [
                 'error' => currentModule()->trans('cart.message.failure.coupon-code-is-not-valid'),
            ]);
        }

        $this->templateCart();
        return view($this->module()->bladePath('order.cart.content'));
    }



    /**
     * Removes the applied coupon code from the client's cart.
     *
     * @param CartRemoveCouponCodeRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function removeCouponCode(CartRemoveCouponCodeRequest $request)
    {
        $cart = depoint()->cart()->findActiveCart();
        $cart->clearAppliedDiscount();

        $this->templateCart();
        return view($this->module()->bladePath('order.cart.content'));
    }
}
