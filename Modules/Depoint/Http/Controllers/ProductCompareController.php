<?php

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;

use Modules\Posts\Entities\Post;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class ProductCompareController extends DepointFrontControllerAbstract
{
    protected $view_folder = "product.compare";
    protected $request;
    protected $view_data;



    /**
     * show compare of to product
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SimpleYasnaRequest $request)
    {
        $this->request = $request;
        if (count($this->getPosts()) < 2) {
            return $this->abort('404');
        }
        $this->view_data['posts'] = $this->getPosts();

        $this->getSpecsOfEachPost();
        return $this->template('main', $this->view_data);
    }



    /**
     * get posts to compare
     *
     * @return array
     */
    private function getPosts()
    {
        $posts      = [];
        $spec_group = null;
        for ($i = 1; $i <= 4; $i++) {
            if (!isset($this->request->{"hash$i"})) {
                continue;
            }
            $post = PostsServiceProvider::select(['hashid' => $this->request->{"hash$i"}])->first();
            if (!$post) {
                continue;
            }
            if (is_null($spec_group)) {
                $spec_group                = $post->getMeta('spec_group');
                $this->view_data['groups'] = model('spec', $post->getMeta('spec_group'))->children;
            }
            if ($this->isCompareAble($post, $spec_group)) {
                $posts[] = $post;
            }
        }
        return $posts;
    }



    /**
     * get specs of each post
     */
    private function getSpecsOfEachPost()
    {
        foreach ($this->view_data['posts'] as $post) {
            $this->view_data['posts_specs'][$post->id] = $post->specsPivots()->whereNull('hid_at')->get();
        }
    }



    /**
     * check that post spec group is equal to passed spec group
     *
     * @param Post $post
     * @param string $speck_group
     *
     * @return bool
     */
    private function isCompareAble($post, $speck_group)
    {
        if ($post->getMeta('spec_group') == $speck_group) {
            return true;
        }
        return false;
    }

}
