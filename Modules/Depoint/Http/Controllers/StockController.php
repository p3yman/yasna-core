<?php

namespace Modules\Depoint\Http\Controllers;

use App\Models\Post;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Nwidart\Modules\Collection;

class StockController extends DepointFrontControllerAbstract
{
    protected $view_folder = "stock";
    const LIMIT_ITEMS = 24;



    /**
     * ProductListController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->posttype = $this->posttype_slugs['stocks'];
    }



    /**
     * display list of stokes
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->general_variables['products'] = $this->getList();
        return $this->template("list.main");
    }



    /**
     * display single page of stokes
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(SimpleYasnaRequest $request)
    {
        $post = $this->singleFindPost($request);
        if (!$post) {
            return $this->abort(404);
        }

        $this->setPostSisterhoodLinksParameters($post);
        $this->general_variables['post_title']     = $post->title;
        $this->general_variables['posttype']       = $post->posttype;
        $this->general_variables['posttype_title'] = $post->posttype->titleIn(getLocale());
        $this->general_variables['features']       = empty($post->getMeta('summarized_feature')) ? "" : explode("\n",
             $post->getMeta('summarized_feature'));
        $this->general_variables['ware_hashid']    = $request->has('ware') ? $request->get('ware') : null;
        $this->general_variables['model']          = $post;
        $this->general_variables['specs_pivot_parent']   = $this->getSpecsPivotParent($post);

        return $this->template('single.main');
    }




    /**
     * get specs of product
     *
     * @param $post
     *
     * @return \Illuminate\Database\Eloquent\Collection|Collection
     */
    private function getSpecsPivotParent(Post $post)
    {
        return $post->specsPivots()->whereNull('hid_at')->get()->groupBy(function ($item) {
            return $item->spec->parent_id;
        });
    }

    /**
     * get Stoke items
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function getList()
    {
        return PostsServiceProvider::select([
             'type' => $this->posttype,
        ])
                                   ->orderByDesc('published_at')
                                   ->paginate(posttype($this->posttype)->getMeta('max_per_page') ?? self::LIMIT_ITEMS)
             ;
    }

}
