<?php

namespace Modules\Depoint\Http\Controllers;

use App\Models\Post;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class LandingController extends DepointFrontControllerAbstract
{
    protected $view_folder = 'product.landing.';



    /**
     * Finds and returns the landing post depending on its slug.
     *
     * @param string $slug
     *
     * @return Post|null
     */
    protected function findLandingPostWithSlug(string $slug)
    {
        return $this->getProductLandingsBuilder()
                    ->whereSlug($slug)
                    ->first()
             ;
    }



    /**
     * Sets the basic information.
     */
    protected function setBasicInformation()
    {
        $post = $this->general_variables['post'];

        $this->general_variables['features'] = empty($post->getMeta('summarized_feature'))
             ? []
             : preg_split('/(\r\n|\r|\n)+/', $post->abstract);
    }



    /**
     * Sets URL parameters for the given post.
     *
     * @param Post $post
     */
    protected function setPostSisterhoodLinksParameters(Post $post)
    {
        $locales    = depoint()->siteLocales();
        $parameters = [];

        foreach ($locales as $locale) {
            if ($locale == getLocale()) {
                continue;
            }

            $sister_post = $post->in($locale);

            if ($sister_post->exists and $sister_post->isPublished() and $sister_post->slug) {
                $parameters[$locale] = $sister_post->productLandingDirectUrl();
            } else {
                $parameters[$locale] = false;
            }
        }

        depoint()->routes()->forceUrlParameters($parameters);
    }



    /**
     * Sets the details posts.
     */
    protected function setDetailsPosts()
    {
        $post                 = $this->general_variables['post'];
        $posts_hashids_string = $post->getMeta('details_posts');
        if (!$posts_hashids_string) {
            return;
        }
        $posts_hashids = explode_not_empty(',', $posts_hashids_string);
        if (empty($posts_hashids)) {
            return;
        }
        $posts_ids = array_map('hashid', $posts_hashids);

        $details_posts = PostsServiceProvider::select([
             'type' => $this->posttype_slugs['products-details'],
        ])->whereIn('id', $posts_ids)->get()->sortBy(function (Post $post) use ($posts_ids) {
            return array_search($post->id, $posts_ids);
        })
        ;
        if (!$details_posts->count()) {
            return;
        }

        $this->general_variables['details_posts']             = $details_posts->groupBy(function (Post $post) {
            $group_name = $post->getMeta('minimal-preview')
                 ? 'minimal'
                 : 'wide';
            return $group_name;
        });
        $this->general_variables['products_details_posttype'] = posttype($this->posttype_slugs['products-details']);
    }



    /**
     * Landing Page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SimpleYasnaRequest $request)
    {
        $post = $this->findLandingPostWithSlug($request->landing_slug);

        if (!$post) {
            return $this->abort(404);
        }

        $this->setPostSisterhoodLinksParameters($post);
        $this->general_variables['post'] = $post;

        $this->setBasicInformation();
        $this->setDetailsPosts();

        return $this->template('main');
    }
}
