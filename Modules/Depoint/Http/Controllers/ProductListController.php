<?php

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Nwidart\Modules\Collection;

class ProductListController extends DepointFrontControllerAbstract
{
    protected $view_folder = "product";
    const LIMIT_ITEMS = 24;



    /**
     * ProductListController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->posttype = $this->posttype_slugs['products'];
    }



    /**
     * show list of products
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->general_variables['all_products'] = $this->getListBuilder()->get();
        return $this->template("list.main");
    }



    /**
     * action of single page
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function single(SimpleYasnaRequest $request)
    {
        $post = $this->singleFindPost($request);
        if (!$post) {
            return $this->abort(404);
        }

        $this->setPostSisterhoodLinksParameters($post);
        $this->general_variables['post_title']         = $post->title;
        $this->general_variables['posttype']           = $post->posttype;
        $this->general_variables['posttype_title']     = $post->posttype->titleIn(getLocale());
        $this->general_variables['features']           = empty($post->getMeta('summarized_feature')) ? "" : explode("\n",
             $post->getMeta('summarized_feature'));
        $this->general_variables['ware_hashid']        = $request->has('ware') ? $request->get('ware') : null;
        $this->general_variables['model']              = $post;
        $this->general_variables['specs_pivot_parent'] = $this->getSpecsPivotParent($post);
        return $this->template('single.main');
    }



    /**
     * get specs of product
     *
     * @param $post
     *
     * @return \Illuminate\Database\Eloquent\Collection|Collection
     */
    private function getSpecsPivotParent($post)
    {
        return $post->specsPivots()->whereNull('hid_at')->get()->groupBy(function ($item) {
            return $item->spec->parent_id;
        })
             ;
    }



    /**
     * Returns the builder to select product posts.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getListBuilder()
    {
        return PostsServiceProvider::select([
             'type' => $this->posttype_slugs['products'],
        ]);
    }



    /**
     * get Product items
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function getList()
    {
        return $this->getListBuilder()
                    ->orderByDesc('published_at')
                    ->paginate(posttype($this->posttype)->getMeta('max_per_page') ?? self::LIMIT_ITEMS)
             ;
    }
}
