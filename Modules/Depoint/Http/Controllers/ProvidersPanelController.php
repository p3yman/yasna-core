<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/4/18
 * Time: 4:10 PM
 */

namespace Modules\Depoint\Http\Controllers;


use App\Models\DepointSparePart;
use Modules\Depoint\Http\Controllers\Traits\PanelControllerTrait;
use Modules\Depoint\Http\Requests\DepointOrderChangeStatusRequest;
use Modules\Depoint\Providers\SparePartsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class ProvidersPanelController extends DepointFrontControllerAbstract
{
    use PanelControllerTrait;

    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'profile.panel';

    protected $base_model = 'depoint-order';



    /**
     * Sets default value of breadcrumb
     */
    protected function setProfileBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'] = [
             currentModule()->trans('spare-parts.provider.title.plural'),
        ];
    }



    /**
     * Sets the basic variables to be used in blades.
     */
    protected function setBasics()
    {
        $this->setProfileBreadcrumb();
        $this->setMenuItems();
    }



    /**
     * Adds orders to the basic breadcrumb.
     */
    protected function addOrdersToBreadCrumb()
    {
        $this->general_variables['profile_breadcrumb'][] = currentModule()
             ->trans('spare-parts.order.title.plural');
    }



    /**
     * Sets orders.
     */
    protected function setOrders()
    {
        $this->setOrdersBuilder();
        $builder = $this->general_variables['orders_builder'];

        $this->general_variables['orders'] = $builder->paginate(40);
    }



    /**
     * Renders and returns the orders page.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function orders()
    {
        $this->setBasics();
        $this->addOrdersToBreadCrumb();
        $this->setProviders();
        $this->setOrders();

        return $this->template('orders');
    }



    /**
     * Render and return the spare parts page.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function spareParts()
    {
        $this->setBasics();
        $this->addSparePartsToBreadCrumb();
        $this->setProviders();
        $this->setSpareParts();

        return $this->template('parts');
    }



    /**
     * Set spare parts.
     */
    protected function setSpareParts()
    {
        $providers = $this->general_variables['providers']->pluck('id')->toArray();

        $this->general_variables['parts'] = model('depoint_spare_part')
             ->whereHas('providers', function ($query) use ($providers) {
                 $query->whereIn('depoint_providers.id', $providers);
             })
             ->orderBy('created_at', 'desc')
             ->paginate(10)
        ;
    }



    /**
     * Adds orders to the basic breadcrumb.
     */
    protected function addSparePartsToBreadCrumb()
    {
        $this->general_variables['profile_breadcrumb'][] = currentModule()
             ->trans('spare-parts.spare-part.title.plural');
    }



    /**
     * Checks if the order is accessible for the current client.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return bool
     */
    protected function orderIsAccessible(SimpleYasnaRequest $request)
    {
        $order = $this->model()->grabHashid($request->hashid);

        if ($order->isAccessible()) {
            $this->general_variables['order'] = $order;

            if ($order->isNotSubmitted() and $order->isNotSent() and $order->isNotVerified()) {
                $order->applySubmittedStatus();
            }

            return true;
        } else {
            return false;
        }
    }



    /**
     * Checks if the order is not accessible for the current client.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return bool
     */
    protected function orderIsNotAccessible(SimpleYasnaRequest $request)
    {
        return !$this->orderIsAccessible($request);
    }



    /**
     * Renders and returns the single page for the specified order.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function orderSingle(SimpleYasnaRequest $request)
    {
        if ($this->orderIsNotAccessible($request)) {
            return $this->abort(410);
        }

        $this->setBasics();
        $this->addOrdersToBreadCrumb();
        $this->setMenuItems();

        return $this->template('order-single');
    }



    /**
     * Sets the specified status for the status of the specified order.
     *
     * @param DepointOrderChangeStatusRequest $request
     *
     * @return string
     */
    public function changeOrderStatus(DepointOrderChangeStatusRequest $request)
    {
        $ok = $request->model->setStatus($request->status);

        return $this->jsonAjaxSaveFeedback($ok);
    }



    /**
     * Finds spare part with hashid.
     *
     * @param string $hashid
     *
     * @return DepointSparePart
     */
    protected function findSparePartWithHashid($hashid)
    {
        return model('depoint-spare-part')->grabHashid($hashid);
    }



    /**
     * Adds items to bread crumb for spare part single page.
     *
     * @param DepointSparePart $spare_part
     */
    protected function addToBreadCrumbForSparePartSingle(DepointSparePart $spare_part)
    {
        $this->addSparePartsToBreadCrumb();
        $this->general_variables['profile_breadcrumb'][] = $spare_part->name;
    }



    /**
     * Sets uploader objects.
     */
    protected function setUploaders()
    {
        $this->general_variables['uploaders'] = SparePartsServiceProvider::sparePartUploaders();
    }



    /**
     * Renders the single page for spare part in the providers panel.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function sparePartSingle(SimpleYasnaRequest $request)
    {
        $spare_part = $this->findSparePartWithHashid($request->hashid);

        if ($spare_part->not_exists or $spare_part->isNotAccessibleForUser()) {
            return $this->abort(404);
        }

        $this->setBasics();
        $this->addToBreadCrumbForSparePartSingle($spare_part);
        $this->setUploaders();

        return $this->template('spare-part-single.main', compact('spare_part'));
    }
}
