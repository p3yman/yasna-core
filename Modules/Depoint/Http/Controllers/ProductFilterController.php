<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/21/18
 * Time: 9:41 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Services\ProductFilter\ProductFilter;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class ProductFilterController extends ProductListController
{
    protected $view_folder = "product.list";
    const LIMIT_ITEMS = 24;



    /**
     * Does filter on products.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function filter(SimpleYasnaRequest $request)
    {
        $this->general_variables['products'] = $this->filterPosts($request->hash);

        return $this->view('list-items', $this->general_variables);
    }



    /**
     * Filter product posts
     *
     * @param null|string $hash_string
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function filterPosts(?string $hash_string)
    {
        $hash_array = $this->translateHash($hash_string);

        if (count($hash_array)) {
            return $this->filterQuery($hash_array);
        } else {
            return $this->filterWithNoHash();
        }
    }



    /**
     * Returns the filtered products.
     *
     * @param array $hash_array
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function filterQuery(array $hash_array)
    {
        $filter   = new ProductFilter($hash_array);
        $products = $filter->paginate(static::LIMIT_ITEMS);

        return $products;
    }



    /**
     * Filters products with no hash string
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function filterWithNoHash()
    {
        return $this->getList();
    }



    /**
     * Translates hash string.
     *
     * @param string $hash
     *
     * @return array
     */
    protected function translateHash($hash)
    {
        $hashArray = [];

        $hash = explode_not_empty('!', $hash);
        foreach ($hash as $field) {
            $field = explode_not_empty('?', $field);
            if (count($field) == 2) {
                $hashArray[$field[1]] = explode_not_empty('/', $field[0]);
                $currentGroup         = &$hashArray[$field[1]];
                $currentGroup         = $this->arrayPrefixToIndex('_', $currentGroup);
            }
        }

        return $hashArray;
    }



    /**
     * Maps array and set prefixes as indexes.
     *
     * @param string       $delimiter
     * @param string|array $haystack
     *
     * @return array|string
     */
    protected function arrayPrefixToIndex($delimiter, $haystack)
    {
        if (is_array($haystack)) {
            foreach ($haystack as $index => $field) {
                $parts = explode($delimiter, $field, 2);
                if (count($parts) == 2) {
                    $key   = $parts[0];
                    $value = $parts[1];
                    $value = $this->arrayPrefixToIndex($delimiter, $value);

                    $target = &$haystack[$key];
                    if (isset($target)) {
                        if (!is_array($target)) {
                            $target = [$target];
                        }

                        if (!is_array($value)) {
                            $value = [$value];
                        }

                        $target = array_merge($target, $value);
                    } else {
                        $target = $value;
                    }
                }
                unset($haystack[$index]);
            }

            return $haystack;
        }

        if (is_string($haystack)) {
            $parts = explode($delimiter, $haystack, 2);
            if (count($parts) == 2) {
                $key   = $parts[0];
                $value = $parts[1];
                $value = $this->arrayPrefixToIndex($delimiter, $value);

                $haystack = [
                     $key => $value,
                ];

                return $haystack;
            }
        }

        return $haystack;
    }
}
