<?php

namespace Modules\Depoint\Http\Controllers;

use App\Models\Posttype;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class BlogController extends DepointFrontControllerAbstract
{
    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'blog';
    /**
     * An instance of the `Posttype` model
     * which is pointing to the `blog` post type.
     *
     * @var Posttype|null
     */
    protected $posttype;



    /**
     * Returns an instance of the `Posttype` model
     * which is pointing to the `blog` post type.
     *
     * @return Posttype
     */
    protected function getPosttype()
    {
        if (!$this->posttype) {
            $this->posttype = posttype($this->posttype_slugs['blog']);
        }

        return $this->posttype;
    }



    /**
     * Adds the `posttype` field to `general_variables`.
     */
    protected function posttypeVariable()
    {
        $this->general_variables['posttype'] = $this->getPosttype();
    }



    /**
     * Generates and returns a builder to select blog post.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function blogBuilder()
    {
        return PostsServiceProvider::select([
             'type' => $this->getPosttype()->slug,
        ]);
    }



    /**
     * Guesses or returns the limit for blog list.
     *
     * @return int
     */
    protected function listPaginationLimit()
    {
        $config_limit = $this->getPosttype()->getMeta('max_per_page');
        return ($config_limit ?: 24);
    }



    /**
     * Selects the models to be shown in the blog list view
     * and appends it to the `general_variables` array.
     */
    protected function selectListModels()
    {
        $this->general_variables['models'] = $this->blogBuilder()
                                                  ->orderByDesc('published_at')
                                                  ->paginate($this->listPaginationLimit())
        ;
    }



    /**
     * The list page of the blog.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if ($this->getPosttype()->not_exists) {
            return $this->abort(404);
        }

        $this->selectListModels();
        $this->posttypeVariable();

        return $this->template('list.main');
    }



    /**
     * Finds the post to be shown in the single page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    protected function singleFindPost(SimpleYasnaRequest $request)
    {
        return PostsServiceProvider::select([
             'type'   => $this->getPosttype()->slug,
             'hashid' => $request->hashid,
        ])->first()
             ;
    }



    /**
     * Renders and returns the response of the single view depending on the specified blog post.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(SimpleYasnaRequest $request)
    {
        $post = $this->singleFindPost($request);
        if (!$post) {
            return $this->abort(410);
        }

        $this->setPostSisterhoodLinksParameters($post);
        $this->general_variables['model'] = $post;
        $this->posttypeVariable();

        return $this->template('single.main');
    }
}
