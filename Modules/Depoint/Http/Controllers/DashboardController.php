<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/29/18
 * Time: 12:08 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Modules\Depoint\Http\Requests\EditPasswordRequest;
use Morilog\Jalali\jDateTime;
use Illuminate\Support\Collection;
use Modules\Depoint\Http\Requests\EditProfileRequest;
use Modules\Depoint\Http\Controllers\Traits\PanelControllerTrait;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Depoint\Services\Module\CurrentModuleHelper;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class DashboardController extends DepointFrontControllerAbstract
{
    use PanelControllerTrait;

    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'profile.customer';



    /**
     * Sets default value of breadcrumb
     */
    protected function setProfileBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'] = [
             currentModule()->trans('profile.user-profile'),
        ];
    }



    /**
     * Sets the basic variables to be used in blades.
     */
    protected function setBasics()
    {
        $this->setProfileBreadcrumb();
        $this->setMenuItems();
    }



    /**
     * Add dashboard to the basic breadcrumb.
     */
    protected function addDashboardToBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'][] = currentModule()->trans('profile.dashboard');
    }



    /**
     * Sets orders.
     */
    public function setOrders()
    {
        $this->setOrdersBuilder();
        $builder = $this->general_variables['orders_builder'];

        $this->general_variables['orders'] = $builder->limit(10)->get();
    }



    /**
     * Set data needed to fill dashboard of providers.
     */
    protected function setProvidersData()
    {
        if (user()->hasAccessToProvidersEnvironment()) {
            $this->setProviders();
            $this->setOrders();
            $this->general_variables['with_providers_data'] = true;
        } else {
            $this->general_variables['with_providers_data'] = false;
        }
    }



    /**
     * Sets latest orders.
     */
    protected function setLatestCartsAsOrders()
    {
        $this->general_variables['orders'] = user()
             ->carts()
             ->orderByDesc('created_at')
             ->limit(10)
             ->get()
        ;
    }



    /**
     * Add edit-profile to the basic breadcrumb.
     */
    protected function addEditProfileToBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'][] = currentModule()->trans('profile.edit-profile');
    }



    /**
     * Add edit-password to the basic breadcrumb.
     */
    protected function addEditPasswordToBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'][] = currentModule()->trans('profile.edit-password');
    }



    /**
     * Renders the main page of the user's dashboard.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->setBasics();
        $this->addDashboardToBreadcrumb();
        $this->setProvidersData();
        $this->setLatestCartsAsOrders();

        return $this->template('main');
    }



    /**
     * Renders the 'edit-profile' page of the user's dashboard.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function editProfile()
    {
        $this->setBasics();
        $this->addEditProfileToBreadcrumb();

        $gender               = $this->userGender();
        $provinces            = $this->allProvinces();
        $user_province_hashid = $this->userProvinceHashid();
        $cities               = $this->allCities();
        $user_city_hashid     = $this->userCityHashid();
        $days                 = $this->allDays();
        $user_birthday_day    = $this->userBirthdayDay();
        $months               = $this->allMonths();
        $user_birthday_month  = $this->userBirthdayMonth();
        $years                = $this->allYears();
        $user_birthday_year   = $this->userBirthdayYear();

        return $this->template('edit',
             compact('provinces', 'gender', 'provinces', 'user_province_hashid', 'user_city_hashid', 'cities', 'days',
                  'user_birthday_day',
                  'months', 'user_birthday_month', 'user_birthday_year', 'years'));
    }



    /**
     * Return user birth_date year.
     *
     * @return null|integer
     */
    protected function userBirthdayYear()
    {
        if (empty(user()->birth_date)) {
            return null;
        }

        $birth_date_carbon = new Carbon(user()->birth_date);
        $birth_date_jalali = jDateTime::toJalali(
             $birth_date_carbon->year, $birth_date_carbon->month,
             $birth_date_carbon->day);

        return $birth_date_jalali[0];
    }



    /**
     * Return user birth_date month.
     *
     * @return null|integer
     */
    protected function userBirthdayMonth()
    {
        if (empty(user()->birth_date)) {
            return null;
        }

        $birth_date_carbon = new Carbon(user()->birth_date);
        $birth_date_jalali = jDateTime::toJalali($birth_date_carbon->year, $birth_date_carbon->month,
             $birth_date_carbon->day);

        return $birth_date_jalali[1];
    }



    /**
     * Return user birth_date day.
     *
     * @return null|integer
     */
    protected function userBirthdayDay()
    {
        if (empty(user()->birth_date)) {
            return null;
        }

        $birth_date_carbon = new Carbon(user()->birth_date);
        $birth_date_jalali = jDateTime::toJalali($birth_date_carbon->year, $birth_date_carbon->month,
             $birth_date_carbon->day);

        return $birth_date_jalali[2];
    }



    /**
     * Return all day numbers in a month.
     *
     * @return array
     */
    protected function allDays()
    {
        $days = [
             [
                  'title' => '',
                  'value' => '',
             ],
        ];

        for ($i = 1; $i <= 31; $i++) {
            $days[$i] = [
                 'title' => $i,
                 'value' => $i,
            ];
        }

        return $days;
    }



    /**
     * Return all month numbers in a year.
     *
     * @return array
     */
    protected function allMonths()
    {
        $months = [
             [
                  'title' => '',
                  'value' => '',
             ],
        ];

        for ($i = 1; $i <= 12; $i++) {
            $months[$i] = [
                 'title' => $i,
                 'value' => $i,
            ];
        }

        return $months;
    }



    /**
     * Return all recent 130 year numbers.
     *
     * @return array
     */
    protected function allYears()
    {
        $years               = [
             [
                  'title' => '',
                  'value' => '',
             ],
        ];
        $today_jalali        = jDateTime::toJalali(Carbon::now()->year, Carbon::now()->month, Carbon::now()->day);
        $current_year_jalali = $today_jalali[0];

        for ($i = $current_year_jalali - 130; $i <= $current_year_jalali; $i++) {
            $years[$i] = [
                 'title' => $i,
                 'value' => $i,
            ];
        }
        return $years;
    }



    /**
     * Return user gender.
     *
     * @return int
     */
    protected function userGender()
    {
        return user()->gender;
    }



    /**
     * Return all provinces hashids and titles.
     *
     * @return array
     */
    protected function allProvinces()
    {
        return model('state')->allProvinces()->get()->map(function ($item) {
            return [
                 'value' => $item->hashid,
                 'title' => $item->title,
            ];
        })->toArray()
             ;
    }



    /**
     * Return all cities hashids and titles, according to the user province.
     *
     * @return array
     */
    protected function allCities()
    {
        return model('state', user()->province)->cities()->get()->map(function ($item) {
            return [
                 'value' => $item->hashid,
                 'title' => $item->title,
            ];
        })->toArray()
             ;
    }



    /**
     * Return user province hashid.
     *
     * @return string
     */
    protected function userProvinceHashid()
    {
        return model('state', user()->province)->hashid;
    }



    /**
     * Return user city hashid.
     *
     * @return string
     */
    protected function userCityHashid()
    {
        return model('state', user()->city)->hashid;
    }



    /**
     * Update the user profile information.
     *
     * @param EditProfileRequest $request
     *
     * @return string
     */
    public function editProfileSubmit(EditProfileRequest $request)
    {
        $is_saved = user()->batchSave($request);

        return $this->jsonAjaxSaveFeedback($is_saved->exists, [
             'success_message' => currentModule()->trans('general.dashboard_message.saved-successfully'),
        ]);
    }



    /**
     * Return related cities of the selected user province.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    protected function relatedCities(SimpleYasnaRequest $request)
    {
        $related_cities = model('state', $request->province)->cities()->get();

        return $this->template('selected-cities', compact('related_cities'));
    }



    /**
     * Return 'profile.customer.edit-password' view.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function editPassword()
    {
        $this->setBasics();
        $this->addEditPasswordToBreadcrumb();

        return $this->template('edit-password');
    }



    /**
     * Edit the user's password.
     *
     * @param EditPasswordRequest $request
     *
     * @return string
     */
    public function editPasswordSubmit(EditPasswordRequest $request)
    {
        if (Hash::check($request->old_password, user()->password)) {
            $is_saved = user()->batchSave(
                 [
                      'password' => Hash::make($request->password),
                 ]
            );

            return $this->jsonAjaxSaveFeedback($is_saved->exists, [
                 'success_message'  => currentModule()->trans('validation.new-password-saved-successfully'),
                 "success_redirect" => route_locale('depoint.front.dashboard'),
                 "redirectTime"     => 1500,
            ]);
        }

        return $this->jsonAjaxSaveFeedback(0, [
             'danger_message' => currentModule()->trans('validation.old-password-was-wrong'),
        ]);
    }



    /**
     * Adds receipts to the basic breadcrumb.
     */
    public function addReceiptsToBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'][] = currentModule()->trans('cart.receipt.title.plural');
    }



    /**
     * Sets codes.
     */
    public function setCodes()
    {
        $this->general_variables['codes'] = depoint()
             ->drawing()
             ->receiptModel()
             ->where('user_id', user()->id)
             ->orderByDesc('purchased_at')
             ->get()
        ;
    }



    /**
     * Renders codes page in the provider
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function codes()
    {
        $this->setBasics();
        $this->addReceiptsToBreadcrumb();
        $this->setCodes();

        return $this->template('lottery');
    }



    /**
     * Renders the list of orders.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function orders()
    {
        $this->setBasics();
        $this->addOrdersToBreadcrumb();
        $this->setCartOrders();

        return $this->template('orders');
    }



    /**
     * Adds orders title to the breadcrumb array.
     */
    protected function addOrdersToBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'][] = $this
             ->module()
             ->getTrans('cart.order.title.plural')
        ;
    }



    /**
     * Sets the raised carts of the client as orders in the general variable.
     */
    protected function setCartOrders()
    {
        $this->general_variables['orders'] = user()
             ->carts()
             ->whereNotNull('raised_at')
             ->where('raised_at', '<=', now())
             ->orderByDesc('raised_at')
             ->paginate(40)
        ;
    }

}
