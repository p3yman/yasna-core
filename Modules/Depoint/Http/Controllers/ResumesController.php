<?php 

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Http\Requests\ResumeSaveRequest;
use Modules\Yasna\Services\YasnaController;

class ResumesController extends YasnaController
{
    protected $base_model  = "depoint-resume";
    protected $view_folder = "depoint::jobs.apply";


    /**
     * save the new resume
     *
     * @param ResumeSaveRequest $request
     *
     * @return string
     */
    public function save(ResumeSaveRequest $request)
    {
        $model = $request->model->batchSaveBoolean($request);
        $message = trans('depoint::resume.done');

        return response()->json(['message'=>$message]);
    }
}
