<?php

namespace Modules\Depoint\Http\Controllers;

use App\Models\Category;
use App\Models\Posttype;
use Illuminate\Database\Eloquent\Collection;
use Modules\Depoint\Http\Requests\NewFaqRequest;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class FaqController extends DepointFrontControllerAbstract
{
    protected $base_model  = "depoint";
    protected $view_folder = "faq";
    /**
     * An instance of the `Posttype` model
     * which is pointing to the `faq` post type.
     *
     * @var Posttype|null
     */
    protected $posttype;



    /**
     * Returns an instance of the `Posttype` model
     * which is pointing to the `faq` post type.
     *
     * @return Posttype
     */
    protected function getPosttype()
    {
        if (!$this->posttype) {
            $this->posttype = posttype($this->posttype_slugs['faq']);
        }

        return $this->posttype;
    }



    /**
     * Adds the `posttype` field to `general_variables`.
     */
    protected function posttypeVariable()
    {
        $this->general_variables['posttype'] = $this->getPosttype();
    }



    /**
     * Set 'categories' and 'side_items'.
     */
    protected function categoriesQuestions()
    {
        $categories       = [];
        $side_items       = [];
        $categories_query = $this->categoriesQuery();

        foreach ($categories_query as $category) {
            $posts = $this->fillPosts($category);

            if (!count($posts)) {
                continue;
            }

            $categories[] = [
                 'id'    => $category->slug,
                 'title' => $category->titleIn(getLocale()),
                 'posts' => $posts,
            ];
            $side_items[] = [
                 'link'  => '#' . $category->slug,
                 'title' => $category->titleIn(getLocale()),
            ];
        }

        $this->general_variables['categories'] = $categories;
        $this->general_variables['side_items'] = $side_items;
    }



    /**
     * Set 'categories' and 'side_items' if no 'categories' are defined.
     */
    protected function handleNoCategory()
    {
        if (!empty($this->general_variables['categories'])) {
            return;
        }

        $side_items = [];

        $categories   = $this->defaultCategoriesContent();
        $side_items[] = $this->noCategorySideItems();

        $this->general_variables['categories'] = $categories;
        $this->general_variables['side_items'] = $side_items;
    }



    /**
     * Return the required fields for the categories.
     *
     * @return array
     */
    protected function defaultCategoriesContent()
    {
        return [
             [
                  'id'    => 'all',
                  'title' => trans('depoint::faq.all'),
                  'posts' => PostsServiceProvider::select([
                       'type' => $this->getPosttype()->slug,
                  ])->orderByDesc('published_at')->get(),
             ],
        ];
    }



    /**
     * Return the required fields for the side items.
     *
     * @return array
     */
    protected function noCategorySideItems()
    {
        return [
             'link'  => '#all',
             'title' => trans('depoint::faq.all'),
        ];
    }



    /**
     * Fill questions and posts with the desired content, if there's no category set.
     */
    protected function allFaqPosts()
    {
        $this->categoriesQuestions();
        $this->handleNoCategory();

        foreach ($this->general_variables['categories'] as $key => $value) {
            $posts     = $value['posts'];
            $questions = $this->fillQuestions($posts);

            $this->general_variables['categories'][$key]['questions'] = $questions;
        }
    }



    /**
     * Return 'faq.main' view.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(SimpleYasnaRequest $request)
    {
        if (!$this->posttpyeExists()) {
            return $this->abort(404);
        }

        $this->posttypeVariable();
        $this->allFaqPosts();
        $this->general_variables['category_slug'] = $request->category;

        return $this->template('main');
    }



    /**
     * Return the categories query.
     *
     * @return collection
     */
    protected function categoriesQuery()
    {
        return $this->getPosttype()
                    ->categories()
                    ->where('is_folder', 0)
                    ->where('parent_id', 0)
                    ->get()
             ;
    }



    /**
     * Checks whether the posttype exists or not.
     *
     * @return bool
     */
    protected function posttpyeExists()
    {
        return $this->getPosttype()->exists;
    }



    /**
     * Fill posts desired content.
     *
     * @param Category $category
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|Collection
     */
    protected function fillPosts(Category $category)
    {
        $posts = PostsServiceProvider::select([
             'type'     => $this->getPosttype()->slug,
             'category' => $category->id,
        ])->orderByDesc('published_at')
                                     ->get()
        ;

        return $posts;
    }



    /**
     * Fill questions desired content.
     *
     * @param Collection $posts
     *
     * @return array
     */
    protected function fillQuestions(Collection $posts)
    {
        $questions = $posts->map(function ($post) {
            return [
                 'q' => $post->title,
                 'a' => $post->abstract,
            ];
        })->toArray()
        ;

        return $questions;
    }



    /**
     * Finds the commenting post and sets it in variables.
     */
    protected function setCommentingPost()
    {
        $this->general_variables['commenting_post'] = PostsServiceProvider::select([
                  'type' => $this->posttype_slugs['commenting'],
                  'slug' => 'faq',
             ])->first() ?? post();
    }



    protected function newQuestionSimilars()
    {
        $this->categoriesQuestions();
        $this->handleNoCategory();

        $questions_array = array_column($this->general_variables['categories'], 'posts');
        $questions       = collect($questions_array)->flatten();

        $this->general_variables['similar_questions'] = $questions->map(function ($item) {
            return [
                 "link"  => $item->direct_url,
                 "title" => $item->title,
            ];
        })->toArray()
        ;

    }



    /**
     * Return 'faq.new' view.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function new()
    {
        $this->setCommentingPost();
        $this->newQuestionSimilars();

        return $this->template('new');
    }
}
