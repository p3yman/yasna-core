<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 11/27/18
 * Time: 4:18 PM
 */

namespace Modules\Depoint\Http\Controllers;


use App\Models\Address;
use Illuminate\Http\JsonResponse;
use Modules\Depoint\Http\Controllers\Traits\PanelControllerTrait;
use Modules\Depoint\Http\Requests\AddressDeleteRequest;
use Modules\Depoint\Http\Requests\AddressSaveRequest;
use Modules\Depoint\Http\Requests\AddressSingleRequest;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;

class AddressController extends DepointFrontControllerAbstract
{
    use PanelControllerTrait;

    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'profile.customer.addresses';



    /**
     * Renders the list of addresses.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->setBasics();
        $this->setAddresses();

        return $this->template('list');
    }



    /**
     * Sets the addresses to show in the list.
     */
    public function setAddresses()
    {
        $this->general_variables['addresses'] = addresses()
             ->user(user())
             ->orderByDesc('created_at')
             ->paginate(12)
        ;
    }



    /**
     * Renders the single page of an address.
     *
     * @param AddressSingleRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function single(AddressSingleRequest $request)
    {
        $model = $request->model;

        $this->setBasics();
        $this->singleAddToBreadcrumb($model);

        return $this->template('single', compact('model'));
    }



    /**
     * Sets the basic information of the actions of this controller.
     */
    protected function setBasics()
    {
        $this->setProfileBreadcrumb();
        $this->setMenuItems();
    }



    /**
     * Sets the profile breadcrumb with its basic items.
     */
    protected function setProfileBreadcrumb()
    {
        $this->general_variables['profile_breadcrumb'] = [
             $this->module()->getTrans('profile.user-profile'),
             $this->module()->getTrans('address.title.plural'),
        ];
    }



    /**
     * Adds a title to the breadcrumb for the address single page
     *
     * @param Address $model
     */
    protected function singleAddToBreadcrumb(Address $model)
    {
        if ($model->exists) {
            $title = $this->module()->getTrans('address.edit');
        } else {
            $title = $this->module()->getTrans('address.add');
        }


        $this->general_variables['profile_breadcrumb'][] = $title;
    }



    /**
     * Saves an address.
     *
     * @param AddressSaveRequest $request
     *
     * @return JsonResponse
     */
    public function save(AddressSaveRequest $request)
    {
        return module('address')->addressController()->save($request);
    }



    /**
     * Deletes the requested address if it exists and owned by the client.
     *
     * @param AddressDeleteRequest $request
     *
     * @return string
     */
    public function delete(AddressDeleteRequest $request)
    {
        $model  = $request->model;
        $hashid = $request->model_hashid;


        $ok = $model->delete();

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_message'  => $this->module()->getTrans('address.delete-success'),
             'success_callback' => "$('#addresses-table').find('tr#tr-$hashid').remove();",

             'danger_message'  => $this->module()->getTrans('address.delete-failure'),
             'danger_callback' => "$('#addresses-table').find('tr#tr-$hashid').removeClass('disabled-box');",
        ]);
    }
}
