<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/16/18
 * Time: 12:49 PM
 */

namespace Modules\Depoint\Http\Controllers;


use App\Models\State;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class GeneralController extends DepointFrontControllerAbstract
{
    /**
     * Returns cities of the specified province.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function cities(SimpleYasnaRequest $request)
    {
        $state = model('state', $request->province_id);

        if ($state->isNotProvince()) {
            return $this->abort(403);
        }

        $cities = $state->cities()
                        ->get()
                        ->map(function (State $city) {
                            return [
                                 'id'    => $city->hashid,
                                 'title' => $city->title,
                            ];
                        })
        ;

        return response()->json($cities);
    }
}
