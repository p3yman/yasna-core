<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use App\Models\DepointSparePart;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Depoint\Http\Requests\Manage\SparePartAddToCartRequest;
use Modules\Depoint\Http\Requests\Manage\SparePartDeleteRequest;
use Modules\Depoint\Http\Requests\Manage\SparePartPendingFilesRequest;
use Modules\Depoint\Http\Requests\Manage\SparePartSaveRequest;
use Modules\Depoint\Providers\SparePartsServiceProvider;
use Modules\Yasna\Services\YasnaController;

class SparePartsController extends YasnaController
{
    protected $base_model  = "depoint-spare-part";
    protected $view_folder = "depoint::manage.spare-parts";



    /**
     * return the basic part of page info which is common in some pages
     *
     * @return array
     */
    protected function basicPageInfo(): array
    {
        return [
             [
                  $url = str_after(route('depoint.manage.spare-parts'), url('manage/') . '/'),
                  trans_safe('depoint::spare-parts.spare-part.title.plural'),
                  $url,
             ],
        ];
    }



    /**
     * return uploaders to be loaded in create/edit page.
     *
     * @return array
     */
    protected function buildUploaders(): array
    {
        return SparePartsServiceProvider::sparePartUploaders();
    }



    /**
     * return the page info for the create page.
     *
     * @return array
     */
    protected function buildCreatePageInfo(): array
    {
        $basic       = $this->basicPageInfo();
        $url_prefix  = $basic[0][0] . '/';
        $create_link = SparePartsServiceProvider::createLink();

        $basic[] = [
             str_after($create_link, $url_prefix),
             trans_safe('depoint::spare-parts.spare-part.create-new'),
        ];

        return $basic;
    }



    /**
     * root form of create action.
     *
     * @param string $model_id
     * @param array  ...$options
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createRootForm($model_id, ...$options)
    {
        if (!$this->model()->can('create')) {
            return $this->abort(403);
        }

        $options[0] = $this->buildCreatePageInfo();
        $options[1] = $this->buildUploaders();

        return $this->singleAction($model_id, 'editor', ...$options);
    }



    /**
     * return the page info for the edit page.
     *
     * @return array
     */
    protected function buildEditPageInfo(): array
    {
        $basic       = $this->basicPageInfo();
        $url_prefix  = $basic[0][0] . '/';
        $create_link = SparePartsServiceProvider::createLink();

        $basic[] = [
             str_after($create_link, $url_prefix),
             trans_safe('depoint::spare-parts.spare-part.edit'),
        ];

        return $basic;
    }



    /**
     * root form of edit action.
     *
     * @param string $model_id
     * @param array  ...$options
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editRootForm($model_id, ...$options)
    {
        if (!$this->model()->can('edit')) {
            return $this->abort(403);
        }

        $options[0] = $this->buildEditPageInfo();
        $options[1] = $this->buildUploaders();

        return $this->singleAction($model_id, 'editor', ...$options);
    }



    /**
     * return a preview of a file with the specified hashid
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function fileItem(Request $request)
    {
        return $this->view('edit-main-file-item', ['file' => $request->file_hashid]);
    }



    /**
     * Return the root form of products part in the spare part editor.
     *
     * @param string $mode_hashid
     * @param array  ...$options
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editorProductsRootForm($mode_hashid, ... $options)
    {
        return $this->singleAction($mode_hashid, 'edit-side-products-selectize', ...$options);
    }



    /**
     * permanent save files received from request.
     *
     * @param SparePartSaveRequest $request
     *
     * @return void
     */
    protected function saveFiles(SparePartSaveRequest $request)
    {
        $files_hashids = $request->main_files;

        if ($request->preview_file) {
            $files_hashids[] = $request->preview_file;
        }

        foreach ($files_hashids as $hashid) {
            $request->cached_files[$hashid]->permanentSave();
        }
    }



    /**
     * save relations for saved model.
     *
     * @param SparePartSaveRequest $request
     * @param DepointSparePart     $model
     *
     * @return void
     */
    protected function saveRelations(SparePartSaveRequest $request, DepointSparePart $model)
    {
        $model->providers()->sync($request->providers);
        $model->products()->sync($request->products);
    }



    /**
     * save spare part.
     *
     * @param SparePartSaveRequest $request
     *
     * @return string
     */
    public function save(SparePartSaveRequest $request)
    {
        try {
            $saved = $request->model->batchSave($request->only([
                 'name',
                 'english_name',
                 'code',
                 'is_ok',
                 'inventory',
                 'daily_use',
                 'production_days',
                 'alert_days',
                 'main_files',
                 'preview_file',
            ]));

            $this->saveFiles($request);
            $this->saveRelations($request, $saved);

            $ok = true;
        } catch (QueryException $exception) {
            $ok = false;
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_redirect' => route('depoint.manage.spare-parts.single-action', [
                  'model_id' => $saved->hashid,
                  'act'      => 'edit',
             ]),
        ]);
    }



    /**
     * Delete or undelete a spare part.
     *
     * @param SparePartDeleteRequest $request
     *
     * @return string
     */
    public function deleteOrUndelete(SparePartDeleteRequest $request)
    {
        if ($request->_submit == 'delete') {
            $ok = $request->model->delete();
        } else {
            $ok = $request->model->restore();
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('auto','$request->model_hashid')",
        ]);
    }



    /**
     * add a spare part to cart.
     *
     * @param SparePartAddToCartRequest $request
     *
     * @return string
     */
    public function addToCart(SparePartAddToCartRequest $request)
    {
        $cart       = user()->currentDepointCart();
        $spare_part = $request->model;

        if ($spare_part->not_exists) {
            return $this->abort(410, true, true);
        }

        if (SparePartsServiceProvider::cartIsNotAccessible()) {
            return $this->abort(403, true, true);
        }

        if ($cart->hasNotSparePart($spare_part)) {
            $cart->addSparePart($spare_part);
        }

        return $this->jsonAjaxSaveFeedback(1, [
             'success_message' => trans_safe('depoint::spare-parts.spare-part.added-to-cart-something', [
                  'title' => $spare_part->name,
             ]),
        ]);
    }



    /**
     * return a collection of selected spare parts
     *
     * @param string $hashids
     *
     * @return Collection
     */
    private function getMassSelectedItems($hashids)
    {
        $hashid_array = explode_not_empty(',', $hashids);
        $id_array     = hashid($hashid_array);


        return $this->model()->whereIn("id", $id_array)->get();
    }



    /**
     * add multiple spare parts to cart at once.
     *
     * @param Request $request
     *
     * @return string
     */
    public function addToCartMass(Request $request)
    {
        if (SparePartsServiceProvider::cartIsNotAccessible()) {
            return $this->abort(403, true, true);
        }

        $models                  = $this->getMassSelectedItems($request->ids);
        $cart                    = user()->currentDepointCart();
        $current_spare_parts_ids = $cart->spare_parts->pluck('id')->toArray();
        $models_to_add           = $models->whereNotIn('id', $current_spare_parts_ids);

        $cart->addSpareParts($models_to_add);

        return $this->jsonAjaxSaveFeedback(1, [
             'success_message' => trans_safe('depoint::spare-parts.spare-part.added-to-cart-numbers', [
                  'number' => ad($models->count()),
             ]),
        ]);
    }



    /**
     * Applies the requested action on the specified pending field.
     *
     * @param SparePartPendingFilesRequest $request
     * @param string                       $field_name
     */
    protected function applyPendingFiles(SparePartPendingFilesRequest $request, string $field_name)
    {
        $model         = $request->model;
        $pending_name  = "pending_$field_name";
        $pending_value = $model->getMeta($pending_name);
        if (empty($pending_value)) {
            return;
        }

        $save_pending = boolval($request->$field_name);


        if ($save_pending) {
            $model->batchSave([
                 $field_name => $pending_value,
            ]);
        }

        $model->batchSave([
             $pending_name => null,
        ]);
    }



    /**
     * Applies the requested changes on the pending files.
     *
     * @param SparePartPendingFilesRequest $request
     *
     * @return string
     */
    public function pendingFiles(SparePartPendingFilesRequest $request)
    {
        $this->applyPendingFiles($request, 'main_files');
        $this->applyPendingFiles($request, 'preview_file');


        return $this->jsonAjaxSaveFeedback(1, [
             'success_callback' => "rowUpdate('auto','$request->model_hashid')",
        ]);
    }
}
