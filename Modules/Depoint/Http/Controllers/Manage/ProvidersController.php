<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Modules\Depoint\Http\Requests\Manage\ProviderDeleteRequest;
use Modules\Depoint\Http\Requests\Manage\ProviderSaveManagersRequest;
use Modules\Depoint\Http\Requests\Manage\ProviderSaveRequest;
use Modules\Depoint\Providers\SpareProvidersServicePrvider;
use Modules\Yasna\Services\YasnaController;

class ProvidersController extends YasnaController
{
    protected $base_model  = "depoint-provider";
    protected $view_folder = "depoint::manage.providers";



    /**
     * Root form of the create action.
     *
     * @param string $model_hashid
     * @param array  ...$options
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createRootForm($model_hashid, ... $options)
    {
        return $this->singleAction($model_hashid, 'edit', ...$options);
    }



    /**
     * Save a provider.
     *
     * @param ProviderSaveRequest $request
     *
     * @return string
     */
    public function save(ProviderSaveRequest $request)
    {
        $old_model = $request->model;
        $editing   = $old_model->exists;
        $model     = $old_model->batchSave($request);

        return $this->jsonAjaxSaveFeedback($model->exists, [
             "success_callback" => ($editing ? "rowUpdate('auto','$model->hashid')" : "divReload('main-grid')"),
        ]);
    }



    /**
     * Delete or undelete a provider.
     *
     * @param ProviderDeleteRequest $request
     *
     * @return string
     */
    public function deleteOrUndelete(ProviderDeleteRequest $request)
    {
        if ($request->_submit == 'delete') {
            $ok = $request->model->delete();
        } else {
            $ok = $request->model->restore();
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('auto','$request->model_hashid')",
        ]);
    }



    /**
     * Save managers of a specified provider.
     *
     * @param ProviderSaveManagersRequest $request
     *
     * @return string
     */
    public function saveManagers(ProviderSaveManagersRequest $request)
    {
        try {
            $request->model->managers()->sync($request->managers);
            $ok = true;
        } catch (QueryException $exception) {
            $ok = false;
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_message' => trans_safe('depoint::spare-parts.provider.managers-stored'),
        ]);
    }

}
