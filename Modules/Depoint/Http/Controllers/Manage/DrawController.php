<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/30/18
 * Time: 1:57 PM
 */

namespace Modules\Depoint\Http\Controllers\Manage;


use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Modules\Depoint\Http\Requests\Manage\DrawPrepareRequest;
use Modules\Depoint\Http\Requests\Manage\DrawSelectRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class DrawController extends YasnaController
{
    protected $base_model  = 'post';
    protected $view_folder = 'depoint::manage.draw';



    /**
     * Renders the draw modal.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function draw(SimpleYasnaRequest $request)
    {
        $model = post($request->hashid);

        if ($model->hasNotDrawPossibility()) {
            return $this->abort(403, true, true);
        }

        $model->prepareForDrawing();

        $posttype = $model->posttype;

        return $this->view('modal', compact('model', 'posttype'));
    }



    /**
     * Returns the query limit.
     *
     * @return int
     */
    protected function queryLimit()
    {
        return 300;
    }



    /**
     * Returns a builder to select receipt holders.
     *
     * @return Builder
     */
    protected function getReceiptHoldersBuilder()
    {
        return model('receipt')
             ->select('user_id')
             ->where('operation_integer', 1)
             ->limit($this->queryLimit())
             ;
    }



    /**
     * Returns the receipt holders.
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function getReceiptHolders()
    {
        return $this->getReceiptHoldersBuilder()->get();
    }



    /**
     * Returns the line number.
     *
     * @return int|string
     */
    protected function getLineNumber()
    {
        return session()->get(model('drawing')->lineNumberSessionName());
    }



    /**
     * Creates drawing records.
     *
     * @param Post $post
     *
     * @return bool
     */
    protected function createDrawingRecords(Post $post)
    {
        $receipt_holders = $this->getReceiptHolders();
        $line_number     = $this->getLineNumber();
        $point_rate      = $post->getMeta('point_rate');


        foreach ($receipt_holders as $receipt_holder) {
            $amount       = $post->receipts->where('user_id', $receipt_holder->user_id)
                                           ->sum('purchased_amount')
            ;
            $total_points = intval(floor($amount / $point_rate));

            if ($total_points) {
                model('drawing')->batchSave([
                     'user_id'    => $receipt_holder->user_id,
                     'post_id'    => $post->id,
                     'amount'     => $amount,
                     'lower_line' => $line_number + 1,
                     'upper_line' => $line_number += $total_points,
                ]);
            }
        }

        $this->getReceiptHoldersBuilder()->update(['operation_integer' => '2',]);
        session()->put('line_number', $line_number);

        return boolval($line_number == session()->get('line_number'));
    }



    /**
     * Prepares for drawing.
     *
     * @param DrawPrepareRequest $request
     *
     * @return string
     */
    public function prepare(DrawPrepareRequest $request)
    {
        $post  = $request->model;
        $limit = $this->queryLimit();

        $finished = $this->createDrawingRecords($post);

        return $this->jsonAjaxSaveFeedback($finished, [
             'redirectTime'       => 500,
             'success_message'    => trans('manage::forms.feed.done'),
             'success_callback'   => "masterModal('"
                  . route('depoint.manage.draw.winners', ['hashid' => $post->hashid]) .
                  "')",
             'success_modalClose' => "0",
             'danger_message'     => trans('manage::forms.feed.wait'),
             'danger_callback'    => <<<JS
drawingProgress($limit);
rowUpdate('tblPosts','$post->hashid');
JS
             ,
        ]);
    }



    /**
     * Renders the winners modal.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function winners(SimpleYasnaRequest $request)
    {
        $model = post($request->hashid);

        if ($model->hasNotDrawPossibility()) {
            return $this->abort(403, true, true);
        }

        $posttype = $model->posttype;

        return $this->view('winners', compact('model', 'posttype'));
    }



    /**
     * Finds user while the drawing process.
     *
     * @param int   $number
     * @param Post  $post
     * @param array $winners
     *
     * @return bool
     */
    protected function selectFindUser(int $number, Post $post, array $winners)
    {

        for ($try = 1; $try <= 10; $try++) {
            $drawing_row = model('drawing')->pull($number);

            if (!$drawing_row or $drawing_row->post_id != $post->id) {
                return false;
            }

            $user = $drawing_row->user;

            if ($user and $user->exists and !in_array($user->id, $winners)) {
                return $user;
            }

            $number = rand(1, session()->get('line_number'));
        }

        return false;
    }



    /**
     * Processes select.
     *
     * @param DrawSelectRequest $request
     *
     * @return bool
     */
    protected function selectProcess(DrawSelectRequest $request)
    {
        $post    = $request->model;
        $winners = $post->winners_array;
        $number  = $request->number;
        $user    = $this->selectFindUser($number, $post, $winners);

        if (!$user) {
            return false;
        }

        $winners[] = $user->id;
        $ok        = $post->batchSaveBoolean([model('post')->drawWinnersMetaFieldName() => $winners]);

        return ($ok ? $user->full_name : false);
    }



    /**
     * Returns the response for the selecting process.
     *
     * @param DrawSelectRequest $request
     *
     * @return string
     */
    public function select(DrawSelectRequest $request)
    {
        $user_full_name = $this->selectProcess($request);
        return $this->jsonSaveFeedback($user_full_name, [
             'success_message'    => trans('validation.attributes.number')
                  . " "
                  . ad($request->number)
                  . ": "
                  . $user_full_name,
             'success_modalClose' => false,
             'success_callback'   => <<<JS
divReload('divWinnersTable');
rowUpdate('tblPosts','$request->model_hashid')
JS
             ,
             'danger_message'     => trans_safe('manage::forms.feed.error'),
             'danger_callback'    => "masterModal('"
                  . route('depoint.manage.draw.winners', ['hashid' => $request->model_hashid]) .
                  "')",
        ]);
    }



    /**
     * Deletes the specified key from the winners list.
     *
     * @param SimpleYasnaRequest $request
     */
    public function delete(SimpleYasnaRequest $request)
    {
        $key     = $request->key;
        $post_id = session()->get('drawing_post');
        $post    = post($post_id);

        if ($post and $post->exists) {
            $winners = $post->winners_array;
            unset($winners[$key]);
            $post->batchSave([$post->drawWinnersMetaFieldName() => array_values($winners)]);
        }
    }
}
