<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Modules\Depoint\Events\DepointOrderDeleted;
use Modules\Depoint\Events\DepointOrderModified;
use Modules\Depoint\Http\Requests\Manage\OrderDeleteRequest;
use Modules\Depoint\Http\Requests\Manage\OrderUpdateRequest;
use Modules\Yasna\Services\YasnaController;

class OrdersController extends YasnaController
{
    protected $base_model  = "depoint-order";
    protected $view_folder = "depoint::manage.orders";
    protected $request;
    protected $is_modified = false;



    /**
     * Saves the items which should be deleted from the order.
     */
    protected function saveDeletion()
    {
        $request         = $this->request;
        $remaining_items = array_keys($request->cached_items);
        $deleting_items  = $request->model->items()->whereNotIn('id', $remaining_items)->get();

        if ($deleting_items->count()) {
            $deleting_items->delete();
            $this->is_modified = true;
        }
    }



    /**
     * Save changes on counts of items.
     */
    protected function saveModification()
    {
        $request = $this->request;
        $counts  = $request->counts;

        foreach ($counts as $item_id => $count) {
            $item = $request->cached_items[$item_id];

            if ($item->count != $count) {
                $item->batchSave(['count' => $count]);
                $this->is_modified = true;
            }

        }
    }



    /**
     * Raise the `DepointOrderModified` event if anything has been changed in the order.
     */
    protected function saveEvent()
    {
        if ($this->is_modified) {
            event(new DepointOrderModified($this->request->model));
        }
    }



    /**
     * Saves items and their changes.
     *
     * @param OrderUpdateRequest $request
     */
    protected function saveItems(OrderUpdateRequest $request)
    {
        $this->request = $request;
        $this->saveDeletion();
        $this->saveModification();
    }



    /**
     * Makes change on the order's status if needed.
     *
     * @param OrderUpdateRequest $request
     */
    protected function saveStatus(OrderUpdateRequest $request)
    {
        $order      = $request->model;
        $old_status = $order->status;

        if (boolval($request->is_verified)) {
            $order->applyVerifiedStatus();
        } else {
            $order->loosenVerifiedStatus();
        }

        if ($old_status != $order->status) {
            $this->is_modified = true;
        }
    }



    /**
     * Saves changes on an order.
     *
     * @param OrderUpdateRequest $request
     *
     * @return string
     */
    public function save(OrderUpdateRequest $request)
    {
        $this->saveItems($request);
        $this->saveStatus($request);
        $this->saveEvent();

        return $this->jsonAjaxSaveFeedback(true, [
             'success_callback' => "rowUpdate('auto','$request->model_hashid')",
        ]);
    }



    /**
     * Deletes the specified order.
     *
     * @param OrderDeleteRequest $request
     *
     * @return string
     */
    public function delete(OrderDeleteRequest $request)
    {
        $model = $request->model;
        $ok    = $model->delete();

        event(new DepointOrderDeleted($model));

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('auto','$request->model_hashid')",
        ]);
    }
}
