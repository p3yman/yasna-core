<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Carbon\Carbon;
use Modules\Depoint\Http\Requests\Manage\ReportRequest;
use Modules\Depoint\Providers\DepointServiceProvider;
use Modules\Yasna\Services\YasnaController;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends YasnaController
{
    private $view_data;
    private $request;
    private $elector_array = [];
    private $filter_query;
    private $limit         = 50;
    private $excel_limit   = 10000;
    private $excelTitles   = [];
    private $excelRow      = [];
    private $excel_col     = [];



    /**
     * filter users to show in report page
     *
     * @param ReportRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(ReportRequest $request)
    {
        $this->request              = $request;
        $this->view_data['search']  = empty($request->all()) ? false : true;
        $this->view_data['request'] = $request;
        if ($this->view_data['search'] == false) {
            return $this->showUI();
        }
        $this->callFilterMethods();
        if ($request->has('ajax')) {
            return $this->filter_query;
        }
        return $this->showUI();
    }



    /**
     * show excel modal
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function excelModal()
    {
        return view('depoint::manage.report.excel-modal');
    }



    /**
     * generate excel file
     *
     * @param ReportRequest $request
     */
    public function excel(ReportRequest $request)
    {

        $builder = $this->filter($request);
        Excel::create("Report-" . Carbon::now(), function ($excel) use ($builder, $request) {
            $excel->sheet('Sheetname', function ($sheet) use ($builder, $request) {
                $this->setExcelTitle(
                     $request->get('name_check'),
                     $this->module()->getTrans('report.name'),
                     'name_first');

                $this->setExcelTitle(
                     $request->get('family_check'),
                     $this->module()->getTrans('report.family'),
                     'name_last');

                $this->setExcelTitle(
                     $request->get('gender_check'),
                     $this->module()->getTrans('report.gender'),
                     'gender');

                $this->setExcelTitle($request->get('tel_check'), $this->module()->getTrans('report.tel'), 'tel');

                $this->setExcelTitle(
                     $request->get('mobile_check'),
                     $this->module()->getTrans('report.mobile'),
                     'mobile');

                $this->setExcelTitle($request->get('email_check'), $this->module()->getTrans('report.email'), 'email');

                $this->setExcelTitle(
                     $request->get('birth_day_check'),
                     $this->module()->getTrans('report.birth-date'),
                     'birth_date');

                $this->setExcelTitle($request->get('marriage_date_check'),
                     $this->module()->getTrans('report.marriage-date'), 'marriage_date');

                $this->setExcelTitle($request->get('city_check'), $this->module()->getTrans('report.city'), 'city');

                $this->setExcelTitle(
                     $request->get('province_check'),
                     $this->module()->getTrans('report.province'),
                     'province'
                );


                $sheet->appendRow($this->excelTitles);

                $dataCollect = $builder->limit($this->excel_limit)->get($this->excel_col);
                $genders     = depoint()->activeGendersTranses();
                foreach ($dataCollect as $item) {
                    $index = 1;
                    $this->setExcelRow($request->get('name_check'), $item->name_first, $index++);
                    $this->setExcelRow($request->get('family_check'), $item->name_last, $index++);
                    $this->setExcelRow($request->get('gender_check'), $genders[$item->gender], $index++);
                    $this->setExcelRow($request->get('tel_check'), $item->tel, $index++);
                    $this->setExcelRow($request->get('mobile_check'), $item->mobile, $index++);
                    $this->setExcelRow($request->get('email_check'), $item->email, $index++);
                    $this->setExcelRow($request->get('birth_day_check'), echoDate($item->birth_date, "y/m/d"),
                         $index++);
                    $this->setExcelRow($request->get('marriage_date_check'), echoDate($item->marriage_date, "y/m/d"),
                         $index++);
                    $this->setExcelRow($request->get('city_check'), model('state', $item->city)->title, $index++);
                    $this->setExcelRow($request->get('province_check'), model('state', $item->province)->title,
                         $index++);
                    $sheet->appendRow($this->excelRow);
                }
            });
        })->export('xls')
        ;
    }



    /**
     * set title of excel cols
     *
     * @param $flag
     * @param $title
     * @param $db_col_name
     */
    private function setExcelTitle($flag, $title, $db_col_name)
    {
        if ($flag != "1") {
            return;
        }
        $this->excel_col[]   = $db_col_name;
        $this->excelTitles[] = $title;
    }



    /**
     * set column of excel
     *
     * @param $flag
     * @param $data
     * @param $index
     */
    private function setExcelRow($flag, $data, $index)
    {
        if ($flag != "1") {
            return;
        }
        $this->excelRow[$index] = $data;
    }



    /**
     * show report page
     */
    private function showUI()
    {
        $this->view_data['genders'] = collect(depoint()->activeGendersTranses())->map(function ($val, $key) {
            return ['key' => $key, 'caption' => $val];
        })->toArray()
        ;
        $this->view_data['page']    = $this->setPage();
        return view('depoint::manage.report.index', $this->view_data);
    }



    /**
     * call filter methods to generate results
     */
    private function callFilterMethods()
    {
        $this->filter_query = model('user');
        $this->runElectors();
        $this->runPureQuery();
        $this->view_data['resCount'] = $this->filter_query->count();
        $this->view_data['models']   = $this->filter_query->simplePaginate($this->limit)
                                                          ->appends($this->request->all())
        ;
        $this->view_data['offset']   = (($this->request->page ?? 1) - 1) * $this->limit;
    }



    /**
     * run elector queries
     */
    private function runElectors()
    {
        $this->ageFilter();
        $this->emailFilter();
        $this->genderFilter();
        $this->filter_query = $this->filter_query->elector($this->elector_array);
    }



    /**
     * run filters that didn't use elector
     */
    private function runPureQuery()
    {
        $this->nameFilter();
        $this->familyFilter();
        $this->birthDateFilter();
        $this->marriageDateFilter();
        $this->homePositionFilter();
        $this->telFilter();
        $this->mobileFilter();
    }



    /**
     * set age filter on model
     */
    private function ageFilter()
    {
        if (!empty($this->request->age_min)) {
            $this->elector_array['age-min'] = $this->request->age_min;
        }
        if (!empty($this->request->age_max)) {
            $this->elector_array['age-max'] = $this->request->age_max;
        }
    }



    /**
     * set filter to name_first filed
     */
    private function nameFilter()
    {
        if (!empty($this->request->name)) {
            $this->filter_query = $this->filter_query->where('name_first', 'like', '%' . $this->request->name . '%');
        }
    }



    /**
     * set filter to name_last filed
     */
    private function familyFilter()
    {
        if (!empty($this->request->family)) {
            $this->filter_query = $this->filter_query->where('name_last', 'like', '%' . $this->request->family . '%');
        }
    }



    /**
     * set filter to birth_date filed
     */
    private function birthDateFilter()
    {
        if (!empty($this->request->birth_date)) {
            $this->filter_query = $this->filter_query->where('birth_date', $this->request->birth_date);
        }

    }



    /**
     * set filter to birth_date filed
     */
    private function marriageDateFilter()
    {
        if (!empty($this->request->marriage_date)) {
            $this->filter_query = $this->filter_query->where('marriage_date', $this->request->marriage_date);
        }

    }



    /**
     * set filter to city and province filed
     */
    private function homePositionFilter()
    {
        if (!empty($this->request->home_province)) {
            $this->filter_query = $this->filter_query->where('province', $this->request->home_province);

        }
        if (!empty($this->request->home_city)) {
            $this->filter_query = $this->filter_query->where('city', $this->request->home_city);

        }
    }



    /**
     * set filter to email filed
     */
    private function emailFilter()
    {
        if (!empty($this->request->email)) {
            $this->elector_array['email'] = $this->request->email;
        }
    }



    /**
     * set filter to tel filed
     */
    private function telFilter()
    {
        if (!empty($this->request->tel)) {
            $this->filter_query = $this->filter_query->where('tel', $this->request->tel);

        }
    }



    /**
     * set filter to mobile filed
     */
    private function mobileFilter()
    {
        if (!empty($this->request->mobile)) {
            $this->filter_query = $this->filter_query->where('mobile', $this->request->mobile);

        }
    }



    /**
     * set page route in title of page
     *
     * @return array
     */
    private function setPage()
    {
        return [
             ["depoint/report", $this->module()->getTrans('report.title')],
        ];
    }



    /**
     * set filter in gender field
     */
    private function genderFilter()
    {
        if ($this->request->has('gender')) {
            $this->elector_array['gender'] = $this->request->get('gender');
        }
    }
}
