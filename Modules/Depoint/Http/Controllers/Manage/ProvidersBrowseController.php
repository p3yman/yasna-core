<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Modules\Yasna\Services\YasnaController;

class ProvidersBrowseController extends YasnaController
{
    protected $base_model     = "depoint-provider";
    protected $view_folder    = "depoint::manage.providers";
    protected $builder;
    protected $view_variables = [];



    /**
     * Check if the `browse` is accessible
     *
     * @return boolean
     */
    protected function hasBrowsePermission()
    {
        return model('depoint-provider')->can('browse');
    }



    /**
     * Check if browse is not accessible.
     *
     * @return bool
     */
    protected function hasNotBrowsePermission()
    {
        return !$this->hasBrowsePermission();
    }



    /**
     * Build the `$page` variable.
     */
    protected function buildPageInfo()
    {
        $this->view_variables['page'] = [
             [
                  $url = str_after(request()->url(), url('manage/') . '/'),
                  trans_safe('depoint::spare-parts.provider.title.plural'),
                  $url,
             ],
        ];
    }



    /**
     * Make the main query to find all providers.
     */
    protected function makeQueryBuilder()
    {
        $this->builder = model('depoint-provider')->orderByDesc('created_at');
    }



    /**
     * Generate collection/paginator from query.
     */
    protected function makeCollection()
    {
        $this->view_variables['models'] = $this->builder->simplePaginate(20);
    }



    /**
     * Build heading of the grid.
     */
    protected function buildBrowseHeadings()
    {
        $this->view_variables['browse_headings'] = [
             trans_safe('depoint::validation.attributes.name'),
             trans_safe('depoint::validation.attributes.telephone'),
             trans_safe('depoint::validation.attributes.address'),
        ];
    }



    /**
     * Browse providers.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if ($this->hasNotBrowsePermission()) {
            return $this->abort(403);
        }

        $this->buildPageInfo();
        $this->makeQueryBuilder();
        $this->makeCollection();
        $this->buildBrowseHeadings();

        return $this->view('browse', $this->view_variables);
    }



    /**
     * Return the grid of browsing.
     * <br >
     * This method is being used while refreshing the grid.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function grid()
    {
        if ($this->hasNotBrowsePermission()) {
            return $this->abort(403);
        }
        $this->makeQueryBuilder();
        $this->makeCollection();
        $this->buildBrowseHeadings();

        return $this->view('browse-grid', $this->view_variables);
    }
}
