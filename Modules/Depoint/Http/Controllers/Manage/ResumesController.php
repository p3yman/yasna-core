<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Illuminate\Database\Eloquent\Builder;
use Modules\Depoint\Http\Requests\Manage\ResumesRequest;
use Modules\Depoint\Http\Requests\ResumeSaveRequest;
use Modules\Yasna\Services\YasnaController;

class ResumesController extends YasnaController
{
    protected $base_model  = "resume";
    protected $view_folder = "depoint::resume";



    /**
     * get models parameter and return view
     *
     * @param string $request_tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($request_tab = 'pending')
    {

        if ($this->tabIsNotAcceptable($request_tab)) {
            return $this->abort(404);
        }

        $page = $this->getPageInfo($request_tab);

        $builder = $this->builder($request_tab);

        $model = $builder->paginate(15);

        return $this->view('resume', compact('model', 'page', 'request_tab'));
    }



    /**
     * doing action on resume
     *
     * @param ResumesRequest $request
     *
     * @return string
     */
    public function action(ResumesRequest $request)
    {
        /**@var bool $done */
        if ($request->_submit == 'approve') {
            $done = $request->model->batchSaveBoolean([
                 'approved_at' => $this->now(),
                 'approved_by' => user()->id,
            ]);
        }

        if ($request->_submit == 'reject') {
            $done = $request->model->batchSaveBoolean([
                 'rejected_at' => $this->now(),
                 'rejected_by' => user()->id,
            ]);
        }

        return $this->jsonAjaxSaveFeedback($done, [
             'success_callback' => $request->id ? "rowHide('resumeTbl','$request->model_hashid')" : 0,
        ]);
    }



    /**
     * return view for refresh row
     *
     * @param string $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function refreshRow($hashid = null)
    {
        $model = model("depoint-resume")->grabHashid($hashid);

        return $this->view('row', compact('model'));
    }



    /**
     * show the detail of resume
     *
     * @param string|null $hashid
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function showResumeDetails($hashid = null)
    {
        $model = model("depoint-resume")->grabHashid($hashid)->spreadMeta();

        return $this->view('resume-detail-modal', compact('model'));
    }



    /**
     * get page info
     *
     * @param string $request_tab
     *
     * @return array
     */
    protected function getPageInfo($request_tab)
    {
        $page[0] = [
             $url = "depoint/resume",
             trans('depoint::resume.resumes'),
        ];
        $page[1] = [
             $request_tab,
             trans('depoint::resume.statuses.' . $request_tab),
             $url,
        ];

        return $page;
    }



    /**
     * get builder from page
     *
     * @param string $request_tab
     *
     * @return Builder
     */
    protected function builder($request_tab)
    {
        $builder = model('depoint-resume')
             ->orderByDesc('created_at');


        if ($request_tab == "pending") {
            $builder->whereNull('approved_at')->whereNull('rejected_at');
        }

        if ($request_tab == "approved") {
            $builder->whereNotNull("approved_at")->whereNull("rejected_at");
        }

        if ($request_tab == "rejected") {
            $builder->whereNotNull("rejected_at")->whereNull("approved_at");
        }

        return $builder;
    }



    /**
     * acceptable tabs
     *
     * @param string $request_tab
     *
     * @return bool
     */
    protected function tabIsAcceptable($request_tab): bool
    {

        if (is_null($request_tab)) {
            return true;
        }

        $allowed = [
             'pending',
             'approved',
             'rejected',
        ];
        if (!in_array($request_tab, $allowed)) {
            return false;
        }

        return true;
    }



    /**
     * illegal tabs
     *
     * @param string $request_tab
     *
     * @return bool
     */
    protected function tabIsNotAcceptable($request_tab): bool
    {
        return !$this->tabIsAcceptable($request_tab);
    }
}
