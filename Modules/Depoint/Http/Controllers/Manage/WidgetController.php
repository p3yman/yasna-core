<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Modules\Yasna\Services\YasnaController;
use Nwidart\Modules\Collection;

class WidgetController extends YasnaController
{
    const CACHE_TIME = 60 * 5;



    /**
     * get sale Statistics Data at today , yesterday , a week ago,a month ago
     */
    public function saleStatisticsData()
    {
        if (Cache::has('sales_statistics')) {
            return Cache::get('sales_statistics');
        }
        $view_data['today']      = $this->getStatisticsWithDate(Carbon::today());
        $view_data['yesterday']  = $this->getStatisticsWithDate(Carbon::yesterday(), true);
        $view_data['last_week']  = $this->getStatisticsWithDate(Carbon::today()->subWeek());
        $view_data['last_month'] = $this->getStatisticsWithDate(Carbon::today()->subMonth());
        Cache::set('sales_statistics', $view_data, self::CACHE_TIME);
        return $view_data;
    }



    /**
     * make data for register widget
     *
     * @return array
     */
    public function salesCountInMonth()
    {
        if (Cache::has('sales_in_month')) {
            return Cache::get('sales_in_month');
        }
        $day       = Carbon::today();
        $res_array = [];
        $model     = model('cart');
        for ($i = 0; $i < 30; $i++) {
            $res_array[] = $model->whereDate('raised_at', $day->toDateString())->count();
            $day->subDay();
        }
        Cache::set('sales_in_month', $res_array, self::CACHE_TIME);
        return $res_array;
    }



    /**
     * make data for gender widget
     *
     * @return array
     */
    public function genderData()
    {
        if (Cache::has('chart_gender')) {
            return Cache::get('chart_gender');
        }
        $men_count   = model('user')->elector(['gender' => '1'])->count();
        $women_count = model('user')->elector(['gender' => '2'])->count();
        $other_count = model('user')->elector(['gender' => '3'])->count();
        $result      = ['menCount' => $men_count, 'womenCount' => $women_count, 'otherCount' => $other_count];
        Cache::set('chart_gender', $result, self::CACHE_TIME);
        return $result;
    }



    /**
     * make data for age category widget
     *
     * @return array
     */
    public function ageCategoryData()
    {
        if (Cache::has('chart_age')) {
            return Cache::get('chart_age');
        }
        $res_array["0-10"]   = model('user')->elector(['age-max' => '10', 'age-min' => '0'])->count();
        $res_array["10-20"]  = model('user')->elector(['age-max' => '20', 'age-min' => '10'])->count();
        $res_array["20-30"]  = model('user')->elector(['age-max' => '30', 'age-min' => '20'])->count();
        $res_array["30-40"]  = model('user')->elector(['age-max' => '40', 'age-min' => '30'])->count();
        $res_array["40-50"]  = model('user')->elector(['age-max' => '50', 'age-min' => '40'])->count();
        $res_array["60-..."] = model('user')->elector(['age-min' => '60'])->count();
        Cache::set('chart_age', $res_array, self::CACHE_TIME);
        return $res_array;
    }



    /**
     * get latest discount code to show in widget
     *
     * @return Collection
     */
    public function latestDiscountCode()
    {
        $limit = 15;
        $codes = model('receipt')->orderBy('purchased_at', "DESC")->limit($limit)->get();
        return $codes;
    }



    /**
     * make data for register widget
     *
     * @return array
     */
    public function registerCountInMonth()
    {
        if (Cache::has('chart_register')) {
            return Cache::get('chart_register');
        }
        $day        = Carbon::today();
        $res_array  = [];
        $user_model = model('user');
        for ($i = 0; $i < 30; $i++) {
            $res_array[$day->timestamp] = $user_model->whereDate('created_at', $day->toDateTimeString())->count();
            $day->subDay();
        }
        Cache::set('chart_register', $res_array, self::CACHE_TIME);
        return $res_array;
    }



    /**
     * get today's sale info
     *
     * @param Carbon $date
     * @param bool   $equal
     *
     * @return array
     */
    private function getStatisticsWithDate(Carbon $date, $equal = false)
    {
        $query = model('cart')->whereNotNull('raised_at');
        if ($equal) {
            $count        = $query->whereDate('raised_at', $date->toDateString())->count();
            $total_amount = $query->whereDate('raised_at', $date->toDateString())->sum('invoiced_amount');
            return ["count" => $count, 'amount' => $total_amount];
        }
        $count        = $query->whereDate('raised_at', ">=", $date->toDateString())->count();
        $total_amount = $query->whereDate('raised_at', ">=", $date->toDateString())->sum('invoiced_amount');
        return ["count" => $count, 'amount' => $total_amount];
    }


}
