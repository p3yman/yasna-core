<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Depoint\Events\DepointOrderSaved;
use Modules\Depoint\Http\Requests\Manage\CartSaveRequest;
use Modules\Depoint\Providers\SparePartsOrdersServiceProviders;
use Modules\Depoint\Providers\SparePartsServiceProvider;
use Modules\Yasna\Services\YasnaController;

class CartController extends YasnaController
{
    protected $base_model     = "depoint-cart";
    protected $view_folder    = "depoint::manage.cart";
    protected $view_variables = [];
    protected $request;



    /**
     * check if the cart is accessible
     *
     * @return bool
     */
    protected function hasPermission()
    {
        return SparePartsServiceProvider::cartIsAccessible();
    }



    /**
     * check if cart is not accessible.
     *
     * @return bool
     */
    protected function hasNotPermission()
    {
        return !$this->hasPermission();
    }



    /**
     * build the `$page` variable.
     *
     * @return void
     */
    protected function buildPageInfo()
    {
        $this->view_variables['page'] = [
             [
                  $url = str_after(route('depoint.manage.spare-parts'), url('manage/') . '/'),
                  trans_safe('depoint::spare-parts.spare-part.title.plural'),
                  $url,
             ],
             [
                  $url2 = str_after(request()->url(), url('manage/') . '/'),
                  trans_safe('depoint::spare-parts.cart.title.singular'),
                  $url2,
             ],
        ];
    }



    /**
     * build groups to be separated shown in the cart page.
     *
     * @return  void
     */
    protected function buildSparePartsGroups()
    {
        $cart        = user()->currentDepointCart();
        $spare_parts = $cart->spare_parts;
        $groups      = [];

        foreach ($spare_parts as $spare_part) {
            $providers = $spare_part->providers;

            foreach ($providers as $provider) {
                $groups[$provider->hashid] = ($groups[$provider->hashid] ?? ['provider' => $provider]);

                $groups[$provider->hashid]['spare_parts'][] = $spare_part;
            }
        }

        $this->view_variables['spare_parts_groups'] = $groups;
    }



    /**
     * build heading of the grid.
     *
     * @return void
     */
    protected function buildBrowseHeadings()
    {
        $this->view_variables['browse_headings'] = [
             [trans_safe('depoint::spare-parts.headline.title'), 'col-xs-6'],
             [trans_safe('depoint::spare-parts.headline.code'), 'col-xs-3'],
             [trans_safe('depoint::validation.attributes.count'), 'col-xs-3'],
        ];
    }



    /**
     * render the index page of cart.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        if ($this->hasNotPermission()) {
            return $this->abort(410);
        }
        $this->request = $request;


        $this->buildPageInfo();
        $this->buildSparePartsGroups();
        $this->buildBrowseHeadings();

        return $this->view('index', $this->view_variables);
    }



    /**
     * save orders and their items.
     *
     * @param CartSaveRequest $request
     */
    public function checkoutSaveOrders(CartSaveRequest $request)
    {
        $counts = $request->counts;

        foreach ($counts as $provider_hashid => $spare_parts_count) {
            $provider = $request->cached_providers[$provider_hashid];
            $order    = model('depoint-order')->batchSave([
                 'user_id'             => user()->id,
                 'depoint_provider_id' => $provider->id,
            ]);

            foreach ($spare_parts_count as $spare_part_hashid => $count) {
                $spare_part = $request->cached_spare_parts[$spare_part_hashid];

                $order_item                        = model('depoint-order-item');
                $order_item->depoint_spare_part_id = $spare_part->id;
                $order_item->count                 = $count;

                $order->items()->save($order_item);
            }

            event(new DepointOrderSaved($order));
        }

    }



    /**
     * mark cart as raised.
     *
     * @param CartSaveRequest $request
     */
    protected function checkoutRaiseCart(CartSaveRequest $request)
    {
        $request->model->batchSave([
             'raised_at' => Carbon::now()->toDateTimeString(),
             'raised_by' => user()->id,
        ]);
    }



    /**
     * checkout a cart.
     *
     * @param CartSaveRequest $request
     */
    public function checkout(CartSaveRequest $request)
    {
        $this->checkoutSaveOrders($request);
        $this->checkoutRaiseCart($request);

        return $this->jsonFeedback([
             'ok'       => 1,
             'message'  => trans_safe('depoint::spare-parts.cart.raised'),
             'redirect' => SparePartsOrdersServiceProviders::browseLink()
        ]);
    }
}
