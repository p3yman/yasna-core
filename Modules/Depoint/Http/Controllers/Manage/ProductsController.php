<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Modules\Depoint\Http\Requests\Manage\ProductDeleteRequest;
use Modules\Depoint\Http\Requests\Manage\ProductSaveRequest;
use Modules\Yasna\Services\YasnaController;

class ProductsController extends YasnaController
{
    protected $base_model  = "depoint-product";
    protected $view_folder = "depoint::manage.products";



    /**
     * Root form of the create action.
     *
     * @param string $model_hashid
     * @param array  ...$options
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\Support\Facades\View|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createRootForm($model_hashid, ... $options)
    {
        return $this->singleAction($model_hashid, 'edit', ...$options);
    }



    /**
     * check if request has been sent from the browse page of products.
     *
     * @return bool
     */
    protected function requestedFromBrowse()
    {
        return (url()->previous() === route('depoint.manage.products'));
    }



    /**
     * check if request has not been sent from the browse page of products.
     *
     * @return bool
     */
    protected function notRequestedFromBrowse()
    {
        return !$this->requestedFromBrowse();
    }



    /**
     * return proper callback for actions.
     *
     * @param string $callback
     *
     * @return string
     */
    protected function properCallback($callback)
    {
        if ($this->requestedFromBrowse()) {
            return $callback;
        }

        return <<<JS
if (typeof productsCallbackRunner === 'function') {
    productsCallbackRunner("$callback");
} else {
    eval("$callback");
}
JS;

    }



    /**
     * Save a product.
     *
     * @param ProductSaveRequest $request
     *
     * @return string
     */
    public function save(ProductSaveRequest $request)
    {
        $old_model = $request->model;
        $editing   = $old_model->exists;
        $model     = $old_model->batchSave($request);

        $success_callback = ($editing ? "rowUpdate('tbl-products','$model->hashid')" : "divReload('main-grid')");
        return $this->jsonAjaxSaveFeedback($model->exists, [
             "success_callback" => $this->properCallback($success_callback),
        ]);
    }



    /**
     * Delete or undelete a product.
     *
     * @param ProductDeleteRequest $request
     *
     * @return string
     */
    public function deleteOrUndelete(ProductDeleteRequest $request)
    {
        if ($request->_submit == 'delete') {
            $ok = $request->model->delete();
        } else {
            $ok = $request->model->restore();
        }

        $success_callback = "rowUpdate('tbl-products','$request->model_hashid')";
        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => $this->properCallback($success_callback),
        ]);
    }
}
