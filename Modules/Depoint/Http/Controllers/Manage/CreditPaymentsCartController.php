<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\YasnaController;

class CreditPaymentsCartController extends YasnaController
{
    protected $base_model  = "cart";
    protected $view_folder = "cart::manage";
    protected $credit_transactions;



    /**
     * Returns a list of tabs slugs.
     *
     * @return array
     */
    protected function tabsArray()
    {
        return [
             'pending',
             'under_packing',
             'under_delivery',
             'under_payment',
             'not_raised',
             'support_request',
             'delivered',
             'archive',
             'search',
        ];
    }



    /**
     * Returns an array of browse tabs.
     *
     * @return array
     */
    protected function browserTabs()
    {
        $array        = $this->tabsArray();
        $counted_tabs = ['pending', 'under_packing'];
        $result       = [];

        foreach ($array as $item) {
            $result[] = [
                 'url'         => $item,
                 'caption'     => trans_safe("cart::status.$item"),
                 'badge_count' => not_in_array($item, $counted_tabs) ? null : $this->modelsBuilder($item)->count(),
            ];
        }

        return $result;
    }



    /**
     * Returns a collection of transactions with credit payment account.
     *
     * @return Collection
     */
    protected function creditTransactions()
    {
        if (!$this->credit_transactions) {
            $account                   = depoint()->shop()->creditAccount();
            $this->credit_transactions = payment()
                 ->search()
                 ->whereAccountId($account->id)
                 ->get()
            ;
        }

        return $this->credit_transactions;
    }



    /**
     * Returns a builder to select carts with the specified requested tab.
     *
     * @param string $request_tab
     *
     * @return Builder
     */
    protected function modelsBuilder($request_tab)
    {
        $transactions_ids = $this->creditTransactions()->pluck('id')->toArray();

        return $this->model()
                    ->selector(['criteria' => $request_tab])
                    ->whereHas('transactions', function ($query) use ($transactions_ids) {
                        $query->whereIn('transactions.id', $transactions_ids);
                    })
                    ->groupBy('carts.id')
             ;
    }



    /**
     * Returns paginated models with the specified requested tab.
     *
     * @param string $request_tab
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function findModels($request_tab)
    {
        return $this->modelsBuilder($request_tab)
                    ->orderBy('created_at', 'desc')
                    ->paginate(user()->preference('max_rows_per_page'))
             ;
    }



    /**
     * Returns page info.
     *
     * @param string $request_tab
     *
     * @return array
     */
    protected function pageInfo($request_tab)
    {
        $main_url = route('depoint.credit-payments.carts', [], false);
        $part_url = str_after($main_url, 'manage/');
        return [
             [$part_url, $this->module()->getTrans('cart.credit-purchases')],
             [$request_tab, trans("cart::status.$request_tab")],
        ];
    }



    /**
     * Renders the page for cart with requested tabs.
     *
     * @param string $request_tab
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index($request_tab = 'pending')
    {
        if (not_in_array($request_tab, $this->tabsArray())) {
            return $this->abort(410);
        }

        $models = $this->findModels($request_tab);

        $tabs = $this->browserTabs();


        $page = $this->pageInfo($request_tab);

        return $this->view("browse", compact('page', 'models', 'tabs', 'request_tab'));
    }
}
