<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;
use Nwidart\Modules\Collection;

class ReportAjaxController extends YasnaController
{
    /**
     * get province with ajax
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string (json result)
     *
     */
    public function getProvince(SimpleYasnaRequest $request)
    {
        $cityId = $request->get('city_id');
        $data   = model('state')::find($cityId)->province()->toArray();
        return json_encode($data);
    }



    /**
     * get city with ajax
     *
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string (json result)
     */
    public function getCity(SimpleYasnaRequest $request)
    {
        $province_id = $request->get('province_id');
        $data        = $this->findCities($province_id);
        //make empty option
        $data[] = [
             "id"    => "",
             "title" => trans('depoint::report.blank-value'),
        ];
        return json_encode(array_reverse($data));
    }



    /**
     * find cities from their province
     *
     *
     * @param $province_id
     *
     * @return Collection
     */
    private function findCities($province_id)
    {
        if (is_null($province_id)) {
            $data = model('state')::where('parent_id', '<>', '0')
                                  ->orderBy('title', 'desc')
                                  ->get(['id', 'title'])
                                  ->toArray()
            ;
        } else {
            $data = model('state')::find($province_id)
                                  ->cities()
                                  ->orderBy('title', 'desc')
                                  ->get(['id', 'title'])
                                  ->toArray()
            ;
        }
        return $data;
    }

}


