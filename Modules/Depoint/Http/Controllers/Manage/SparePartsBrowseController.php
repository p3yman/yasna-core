<?php

namespace Modules\Depoint\Http\Controllers\Manage;

use Illuminate\Http\Request;
use Modules\Depoint\Providers\SparePartsServiceProvider;
use Modules\Yasna\Services\YasnaController;

class SparePartsBrowseController extends YasnaController
{
    protected $base_model     = "depoint-spare-part";
    protected $view_folder    = "depoint::manage.spare-parts";
    protected $builder;
    protected $view_variables = [];
    protected $request;



    /**
     * Check if the `browse` is accessible
     *
     * @return boolean
     */
    protected function hasBrowsePermission()
    {
        return $this->model()->can('browse');
    }



    /**
     * Check if browse is not accessible.
     *
     * @return bool
     */
    protected function hasNotBrowsePermission()
    {
        return !$this->hasBrowsePermission();
    }



    /**
     * Return the default tab of the browse page.
     *
     * @return string
     */
    protected function defaultTab()
    {
        return 'all';
    }



    /**
     * check if the requested tab is search.
     *
     * @return bool
     */
    protected function isSearchForm()
    {
        return ($this->view_variables['tab'] == 'search');
    }



    /**
     * check if the requested tab is not search.
     *
     * @return bool
     */
    protected function isNotSearchForm()
    {
        return !$this->isSearchForm();
    }



    /**
     * guess the current tab.
     */
    protected function guessTab()
    {
        $this->view_variables['tab']            = ($this->request->tab ?? $this->defaultTab());
        $this->view_variables['is_search_form'] = $this->isSearchForm();
    }



    /**
     * Build the `$page` variable.
     */
    protected function buildPageInfo()
    {
        $tab        = $this->view_variables['tab'];
        $tab_string = trans_safe("depoint::spare-parts.browse-tab.$tab");

        $this->view_variables['page'][] = [
             $url = str_after(SparePartsServiceProvider::browseLink(), url('manage/') . '/'),
             trans_safe('depoint::spare-parts.spare-part.title.plural'),
             $url,
        ];

        $this->view_variables['page'][] = [
             $url,
             $tab_string,
             $tab,
        ];
    }



    /**
     * Make the main query to find all products.
     */
    protected function makeQueryBuilder()
    {
        $this->builder = $this->model()->withTrashed()->orderByDesc('created_at');
    }



    /**
     * apply products condition on query builder if specified.
     *
     * @return void
     */
    protected function applyProductsSearch()
    {
        $str = $this->request->products;
        if (!$str) {
            return;
        }
        $hashids = explode_not_empty(',', $str);
        $ids     = array_map('hashid', $hashids);

        $this->builder->whereHas('products', function ($query) use ($ids) {
            $query->whereIn('depoint_products.id', $ids);
        });
    }



    /**
     * apply providers condition on query builder if specified.
     *
     * @return void
     */
    protected function applyProvidersSearch()
    {
        $str = $this->request->providers;
        if (!$str) {
            return;
        }
        $hashids = explode_not_empty(',', $str);
        $ids     = array_map('hashid', $hashids);

        $this->builder->whereHas('providers', function ($query) use ($ids) {
            $query->whereIn('depoint_providers.id', $ids);
        });
    }



    /**
     * apply status (`is_ok`) condition on query builder if specified.
     *
     * @return void
     */
    protected function applyStatusSearch()
    {
        if (!$this->request->has('is_ok')) {
            return;
        }

        $value = boolval($this->request->is_ok);

        $this->builder->where('is_ok', $value);
    }



    /**
     * apply search conditions on query builder.
     */
    protected function applySearchOnQueryBuilder()
    {
        $this->applyProductsSearch();
        $this->applyProvidersSearch();
        $this->applyStatusSearch();
    }



    /**
     * Generate collection/paginator from query.
     */
    protected function makeCollection()
    {
        $this->view_variables['models'] = $this->builder->simplePaginate(20);
    }



    /**
     * Build heading of the grid.
     */
    protected function buildBrowseHeadings()
    {
        $this->view_variables['browse_headings'] = [
             trans_safe('depoint::validation.attributes.name'),
             trans_safe('depoint::validation.attributes.code'),
             trans_safe('depoint::validation.attributes.daily_use'),
             trans_safe('depoint::validation.attributes.inventory'),
             trans_safe('depoint::validation.attributes.alert_days'),
        ];
    }



    /**
     * run grid needs.
     */
    protected function runGrid()
    {
        $this->makeQueryBuilder();
        $this->applySearchOnQueryBuilder();
        $this->makeCollection();
        $this->buildBrowseHeadings();
    }



    /**
     * Browse spare parts.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        if ($this->hasNotBrowsePermission()) {
            return $this->abort(403);
        }
        $this->request = $request;

        $this->guessTab();
        $this->buildPageInfo();

        if ($this->isNotSearchForm()) {
            $this->runGrid();
        }

        return $this->view('browse', $this->view_variables);
    }



    /**
     * Return the grid of browsing.
     * <br >
     * This method is being used while refreshing the grid.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function grid(Request $request)
    {
        if ($this->hasNotBrowsePermission()) {
            return $this->abort(403);
        }
        $this->request = $request;

        $this->runGrid();

        return $this->view('browse-grid', $this->view_variables);
    }
}
