<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/2/18
 * Time: 12:41 PM
 */

namespace Modules\Depoint\Http\Controllers\Manage;


use Modules\Depoint\Http\Requests\Manage\PostColorImagesRequest;
use Modules\Depoint\Services\Controllers\DepointControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class PostsController extends DepointControllerAbstract
{
    protected $view_folder = 'manage.posts';
    protected $base_model  = 'post';



    /**
     * Returns and array containing data to be used while rendering uploaders for colors.
     *
     * @return array
     */
    protected function colorImagesUploadersInfo()
    {
        $colors            = depoint()->shop()->availableWareColors();
        $posttype          = depoint()->shop()->mainPosttype();
        $basic_uploader    = fileManager()
             ->uploader($posttype)
             ->posttypeConfig('colors_images')
             ->withAssets()
             ->withoutGeneralAssets()
             ->withoutAjaxRemove()
             ->onEachUploadComplete('imageUploaded')
        ;
        $uploaders         = [];
        $label_slug_prefix = $label_slug_prefix = depoint()->shop()->labelsSlugPrefix();

        foreach ($colors as $color) {
            $label             = label($label_slug_prefix . $color);
            $uploaders[$color] = [
                 'uploader' => (clone $basic_uploader)
                      ->jsVariableName(snake_case(camel_case($color)) . '_uploader')
                 ,
                 'title'    => $label->title,
            ];
        }

        return $uploaders;
    }



    /**
     * Returns a form to manage color images.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function colorImages(SimpleYasnaRequest $request)
    {
        $model = $this->model($request->hashid);

        if ($model->not_exists) {
            return $this->abort(404);
        }

        $uploaders_info = $this->colorImagesUploadersInfo();

        return $this->view('colors-image', compact('model', 'uploaders_info'));
    }



    /**
     * Returns an item for each ticket.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function itemPreview(SimpleYasnaRequest $request)
    {
        $file_hashid = $request->file_hashid;
        $color       = $request->color;

        if (!$file_hashid) {
            return $this->abort(410);
        }

        return $this->view('colors-image-item', compact('file_hashid', 'color'));
    }



    /**
     * Saves color images for a post.
     *
     * @param PostColorImagesRequest $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    protected function saveColorImages(PostColorImagesRequest $request)
    {
        if ($request->model->not_exists) {
            return $this->abort(410);
        }

        $colors_files = $request->only(['color_images']);
        foreach ($colors_files as $color => $files) {
            foreach ($files as $key => $file) {
                $save_result = fileManager()->file($file)->permanentSave();

                if (!$save_result) {
                    unset($colors_files[$color][$key]);
                }
            }
        }

        $ok = $request->model->batchSaveBoolean($colors_files);

        return $this->jsonSaveFeedback($ok, [
             'success_modalClose' => 1,
        ]);
    }
}
