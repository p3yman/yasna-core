<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/27/18
 * Time: 6:55 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;

class ContactController extends DepointFrontControllerAbstract
{
    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'contact';



    /**
     * Reads address from setting and set it in variables.
     */
    protected function setAddress()
    {
        $setting = get_setting('address');
        if ($setting) {
            $this->general_variables['contact_methods'][] = [
                 "image"       => currentModule()->asset('images/icon-contact-1.svg'),
                 "value_class" => "address",
                 "value"       => $setting,
            ];
        }
    }



    /**
     * Reads email(s) from setting and set it in variables.
     */
    protected function setEmail()
    {
        $setting = get_setting('email');
        if ($setting) {
            if (is_array($setting)) {
                $setting = implode('<br >', $setting);
            }

            $this->general_variables['contact_methods'][] = [
                 "image" => currentModule()->asset('images/icon-contact-2.svg'),
                 "value" => $setting,
            ];
        }
    }



    /**
     * Reads telephone number(s) from setting and set it in variables.
     */
    protected function setTelephone()
    {
        $setting = get_setting('telephone');
        if ($setting) {
            if (is_array($setting)) {
                $setting = implode('<br >', $setting);
            }

            $this->general_variables['contact_methods'][] = [
                 "image" => currentModule()->asset('images/icon-contact-3.svg'),
                 "value" => $setting,
            ];
        }
    }



    /**
     * Reads contact info from setting and set it in variables.
     */
    protected function setContactInfo()
    {
        $this->general_variables['contact_methods'] = [];
        $this->setAddress();
        $this->setEmail();
        $this->setTelephone();
    }



    /**
     * Finds the commenting post and sets it in variables.
     */
    protected function setCommentingPost()
    {
        $this->general_variables['commenting_post'] = PostsServiceProvider::select([
                  'type' => $this->posttype_slugs['commenting'],
                  'slug' => 'contact',
             ])->first() ?? post();
    }



    /**
     * Renders contact page.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->setContactInfo();
        $this->setCommentingPost();

        return $this->template('main');
    }
}
