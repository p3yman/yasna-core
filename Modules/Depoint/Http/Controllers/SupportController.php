<?php

namespace Modules\Depoint\Http\Controllers;

use App\Models\State;
use Exception;
use Modules\Depoint\Events\DepointInsertTracking;
use Modules\Depoint\Http\Requests\SorooshanCitiesRequest;
use Modules\Depoint\Http\Requests\SorooshanInsertRequest;
use Modules\Depoint\Http\Requests\SupportTicketAddRequest;
use Modules\Depoint\Notifications\SendTrackingCodeNotification;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Depoint\Services\Sorooshan\Sorooshan;
use Modules\Yasna\Services\V4\Request\SimpleYasnaFormRequest;


class SupportController extends DepointFrontControllerAbstract
{
    protected $view_folder = "support";



    /**
     * show ticket page of support
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showAddTicketPage()
    {
        return $this->template('single.main');
    }



    /**
     * save ticket in support section
     *
     * @param SupportTicketAddRequest $request
     *
     * @return string
     */
    public function saveTicket(SupportTicketAddRequest $request)
    {
        $ticket = ticket('main')->addRemark([
             "title"            => $request->subject,
             "text"             => $request->text,
             'guest_name_first' => $request->name_first,
             'guest_name_last'  => $request->name_last,
             'guest_email'      => $request->email,
             'guest_mobile'     => $request->mobile,
             'guest_ip'         => $request->ip(),

        ]);

        return $this->jsonAjaxSaveFeedback($ticket, [
             'success_message' => $this->module()->getTrans('general.ticket-saved'),
        ]);
    }



    /**
     * Renders the form for the Sorooshan service.
     *
     * @param string      $locale
     * @param null|string $trace_code
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function sorooshan($locale, $trace_code = null)
    {
        /** @var Sorooshan $sorooshan */
        $sorooshan = app('depoint.sorooshan');
        $message = trans('depoint::errors.service_not_available');

        if ($sorooshan->isNotAvailable()) {
            return $this->customError($message);
        }

        try {
            $this->setSorooshanSelects($sorooshan);
        } catch (Exception $exception) {
            return $this->customError($message);
        }

        if ($trace_code) {
            $status = $this->followTraceCode($locale, $trace_code);
        }


        return $this->template('service.main', compact('provinces', 'cities', 'trace_code', 'locale', 'status'));
    }



    /**
     * Sets the variables for the selects in the Sorooshan form.
     *
     * @param Sorooshan $sorooshan
     *
     * @throws Exception
     */
    protected function setSorooshanSelects(Sorooshan $sorooshan)
    {
        $default_option = [
             'value' => '',
             'title' => '',
        ];

        $this->general_variables['provinces'] = collect($sorooshan->getStates())
             ->map(function ($state) {
                 return [
                      'value' => $state['id'],
                      'title' => $state['title'],
                 ];
             })
             ->prepend($default_option)
             ->toArray()
        ;

        $this->general_variables['service_types'] = $sorooshan->getServiceTypesCombo();

        $this->general_variables['cities'] = [$default_option];
    }



    /**
     * Renders the combo to select the cities of a state.
     *
     * @param SorooshanCitiesRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws Exception
     */
    public function sorooshanCities(SorooshanCitiesRequest $request)
    {
        /** @var Sorooshan $sorooshan */
        $sorooshan = app('depoint.sorooshan');

        $state_order = $request->state;
        $cities      = $this->getCitiesComboWithStateOrder($sorooshan, $state_order);
        $disabled    = false;

        return view(
             $this->module()->getBladePath('support.service.cities-select'),
             compact('cities', 'disabled')
        );
    }



    /**
     * Returns the cities combo based on the given state order.
     *
     * @param Sorooshan $sorooshan
     * @param int       $state_order
     *
     * @return array
     * @throws Exception
     */
    public function getCitiesComboWithStateOrder(Sorooshan $sorooshan, int $state_order)
    {
        $state_item = $this->findStateWithOrder($sorooshan, $state_order);

        $state_name = $state_item['title'];
        return collect($sorooshan->getCities($state_name))
             ->map(function (array $city) {
                 return [
                      'value' => $city['id'],
                      'title' => $city['title'],
                 ];
             })->toArray()
             ;

    }



    /**
     * Returns the state item based on the given state order.
     *
     * @param Sorooshan $sorooshanserviceStatus
     * @param int       $state_order
     *
     * @return array
     * @throws Exception
     */
    protected function findStateWithOrder(Sorooshan $sorooshan, int $state_order)
    {
        /** @var Sorooshan $sorooshan */
        $sorooshan = app('depoint.sorooshan');

        return collect($sorooshan->getStates())->firstWhere('id', $state_order);
    }



    /**
     * Insert virtual reception on Sorooshan service..
     *
     * @param SorooshanInsertRequest $request
     *
     * @return string
     * @throws Exception
     */
    public function sorooshanInsert(SorooshanInsertRequest $request)
    {
        /** @var Sorooshan $sorooshan */
        $sorooshan = app('depoint.sorooshan');
        $model     = $sorooshan->insertVirtualReception($request->toArray());
        $code      = $model->warning_message;
        $ok        = boolval($model->sorooshan_status);
        $mobile    = $request->mobile;



        $user = user(1);
        $user->mobile = $mobile;

        $user->notify( (new SendTrackingCodeNotification($code)) );


        return $this->jsonAjaxSaveFeedback($ok, [
             'success_message'    => $sorooshan->getTrans('message.insert-success', ['code' => $code]),
             'success_form_reset' => 1,
             'danger_message'     => $sorooshan->getTrans('message.insert-failure'),
        ]);


    }



    /**
     * show following status form
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showStatusForm()
    {
        return $this->template('service.supportTracking');
    }



    /**
     * get request status via soap method
     *
     * @param string         $locale
     * @param string|integer $trace_code
     *
     * @return string
     */
    public function followTraceCode($locale, $trace_code)
    {
        $sorooshan = app('depoint.sorooshan');
        $status    = $sorooshan->virtualReceptionFollowUp($locale, (string)$trace_code);

        return $status;
    }



    /**
     * redirect the request to `followTraceCode` method
     *
     * @param SimpleYasnaFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function serviceStatus(SimpleYasnaFormRequest $request)
    {
        $locale     = getLocale();
        $trace_code = $request->trace_code;

        if (!$trace_code) {
            $route = route('depoint.support.sorooshan', [$locale]);
        } else {
            $route = route('depoint.support.sorooshan', [$locale, $trace_code]);
        }

        return redirect($route);
    }
}
