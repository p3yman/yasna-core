<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/26/18
 * Time: 11:13 AM
 */

namespace Modules\Depoint\Http\Controllers;

use App\Models\Posttype;
use App\Models\State;
use Exception;
use App\Models\Post;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;

class AgentsController extends DepointFrontControllerAbstract
{
    protected $view_folder = 'agents';



    /**
     * Sets posttype.
     */
    protected function setPosttype()
    {
        $this->general_variables['posttype'] = (
             $this->general_variables['posttype'] ??
             posttype($this->posttype_slugs['agents'])
        );
    }



    /**
     * Returns posttype.
     *
     * @return Posttype
     */
    protected function getPosttype()
    {
        $this->setPosttype();
        return $this->general_variables['posttype'];
    }



    /**
     * Returns all agents posts.
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    protected function getAgentPosts()
    {
        $posttype = $this->getPosttype();
        return PostsServiceProvider::select([
             'type' => $posttype->slug,
        ])->get()
             ;
    }



    /**
     * Sets states.
     */
    protected function setStates()
    {
        $this->general_variables['states'] = model('state')
             ->allProvinces()
             ->get()
        ;
    }



    /**
     * Sets states agents.
     */
    protected function setStatesAgents()
    {
        $this->general_variables['states_agents'] = $this
             ->getAgentPosts()
             ->map(function (Post $post) {
                 $post->cached_city = model('state', $post->getMeta('city'));

                 return $post;
             })
             ->groupBy(function (Post $post) {
                 try {
                     $city     = model('state', $post->getMeta('city'));
                     $province = $city->province;

                     return $province->id;
                 } catch (Exception $exception) {
                     return '';
                 }
             })
        ;
    }



    /**
     * Sets the default state.
     */
    public function setDefaultState()
    {
        $this->general_variables['default_state'] = 'tehran';
    }



    /**
     * Sets data for states.
     */
    protected function setStatesData()
    {
        $this->setStates();
        $this->setStatesAgents();
        $this->setDefaultState();
    }



    /**
     * Renders the states page.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->setStatesData();
        return $this->template('main');
    }
}
