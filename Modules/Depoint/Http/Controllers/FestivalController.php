<?php

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class FestivalController extends DepointFrontControllerAbstract
{
    protected $view_folder = "festivals";
    const  MAX_CURRENT_ITEM = 5;
    const  MAX_PASSED_ITEM  = 7;
    const  MAX_FUTURE_ITEM  = 7;



    /**
     * FestivalController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->posttype = $this->posttype_slugs['festivals'];
    }



    /**
     * show list page of festivals
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function lists()
    {
        $this->general_variables['current_festivals'] = $this->getCurrentFestivals();
        $this->general_variables['passed_festivals']  = $this->getPassedFestivals();
        $this->general_variables['future_festivals']  = $this->getFutureFestivals();
        return $this->template('list.main');
    }



    /**
     * action of single page
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function single(SimpleYasnaRequest $request)
    {
        $post                                      = $this->singleFindPost($request);
        $this->general_variables['post_title']     = $post->title;
        $this->general_variables['posttype']       = $post->posttype;
        $this->general_variables['posttype_title'] = $post->posttype->titleIn(getLocale());

        $this->general_variables['model'] = model('post', $request->hashid);

        return $this->template('single.main');
    }



    /**
     * get current festivals
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getCurrentFestivals()
    {
        return PostsServiceProvider::select([
             'type'         => $this->posttype_slugs['festivals'],
             'event_starts' => now()->toDateString(),
             'event_ends'   => now()->toDateString(),
        ])->orderByDesc('published_at')->limit(self::MAX_CURRENT_ITEM)->get()
             ;
    }



    /**
     * get passed festivals
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getPassedFestivals()
    {
        return PostsServiceProvider::select([
             'type' => $this->posttype_slugs['festivals'],
        ])
                                   ->orderByDesc('published_at')
                                   ->where('event_ends_at', "<", now()->toDateString())
                                   ->limit(self::MAX_PASSED_ITEM)
                                   ->get()
             ;
    }



    /**
     * get future festivals
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getFutureFestivals()
    {
        return PostsServiceProvider::select([
             'type' => $this->posttype_slugs['festivals'],
        ])
                                   ->orderByDesc('published_at')
                                   ->where('event_starts_at', ">", now()->toDateString())
                                   ->limit(posttype($this->posttype)->getMeta('max_per_page') ?? self::MAX_FUTURE_ITEM)
                                   ->get()
             ;
    }
}
