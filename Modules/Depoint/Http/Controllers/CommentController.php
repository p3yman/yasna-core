<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/28/18
 * Time: 1:19 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Http\Requests\CommentSaveRequest;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;

class CommentController extends DepointFrontControllerAbstract
{
    /**
     * Saves a comment from request.
     *
     * @param CommentSaveRequest $request
     *
     * @return string
     */
    public function save(CommentSaveRequest $request)
    {
        $this->request = $request;
        $data          = $this->changeIndexes($request->toArray());

        $saved_comment = model('comment')
             ->batchSaveBoolean($data);


        return $this->jsonAjaxSaveFeedback($saved_comment, [
             'success_message'      => currentModule()->trans('general.message-sent'),
             'success_form_reset'   => 1,
             'success_feed_timeout' => 5000,
        ]);
    }



    /**
     * Makes needed changes in saving data.
     *
     * @param array $data
     *
     * @return array
     */
    protected function changeIndexes(array $data)
    {
        $indexes_to_change = [
             'name'    => 'guest_name',
             'email'   => 'guest_email',
             'mobile'  => 'guest_mobile',
             'message' => 'text',
        ];
        foreach ($indexes_to_change as $from => $to) {
            if (array_key_exists($from, $data)) {
                $data[$to] = $data[$from];
                unset($data[$from]);
            }
        }

        return $data;
    }
}
