<?php

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Providers\SparePartsServiceProvider;
use Modules\Yasna\Services\YasnaController;

class ManageSidebarController extends YasnaController
{
    /**
     * All Submenus for the Spare Parts Supply Section
     *
     * @var array
     */
    protected $all_submenus = [];



    /**
     * generate a link with a route name,
     * change it to be used in sidebar
     * and return it.
     *
     * @param string $route_name
     *
     * @return string
     */
    protected function linkFromRoute($route_name)
    {
        $full_url   = route($route_name);
        $manage_url = url('manage') . '/';

        return str_after($full_url, $manage_url);
    }



    /**
     * submenu for products
     */
    protected function productsSubmenu()
    {
        if (!model('depoint-product')->can('browse')) {
            return;
        }

        $this->all_submenus[] = [
             'caption' => trans_safe('depoint::spare-parts.product.title.plural'),
             'link'    => $this->linkFromRoute('depoint.manage.products'),
        ];
    }



    /**
     * submenu for providers
     */
    protected function providersSubmenu()
    {
        if (!model('depoint-provider')->can('browse')) {
            return;
        }

        $this->all_submenus[] = [
             'caption' => trans_safe('depoint::spare-parts.provider.title.plural'),
             'link'    => $this->linkFromRoute('depoint.manage.providers'),
        ];
    }



    /**
     * submenu for spare parts
     */
    protected function sparePartSubmenu()
    {
        if (!model('depoint-spare-part')->can('browse')) {
            return;
        }

        $this->all_submenus[] = [
             'caption' => trans_safe('depoint::spare-parts.spare-part.title.plural'),
             'link'    => $this->linkFromRoute('depoint.manage.spare-parts'),
        ];
    }



    /**
     * provide submenu for cart
     */
    protected function cartSubmenu()
    {
        if (SparePartsServiceProvider::cartIsNotAccessible()) {
            return;
        }

        $this->all_submenus[] = [
             'caption' => trans_safe('depoint::spare-parts.cart.title.singular'),
             'link'    => $this->linkFromRoute('depoint.manage.cart'),
        ];
    }



    /**
     * provide submenu for orders.
     */
    protected function ordersSubmenu()
    {
        if (!(model('depoint-order'))->can('browse')) {
            return;
        }

        $this->all_submenus[] = [
             'caption' => trans_safe('depoint::spare-parts.order.title.plural'),
             'link'    => $this->linkFromRoute('depoint.manage.orders'),
        ];
    }



    /**
     * generate and return all submenus
     *
     * @return array
     */
    public function all()
    {
        $this->productsSubmenu();
        $this->providersSubmenu();
        $this->sparePartSubmenu();
        $this->cartSubmenu();
        $this->ordersSubmenu();

        return $this->all_submenus;
    }
}
