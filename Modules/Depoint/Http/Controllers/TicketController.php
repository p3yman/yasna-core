<?php

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Http\Requests\TicketAddRequest;
use Modules\Depoint\Http\Requests\TicketReplyRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class TicketController extends DashboardController
{
    protected $view_folder = 'profile.tickets';
    const PAGE_LIMIT = 10;



    /**
     * display list of tickets
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->setBasics();
        $this->general_variables['profile_breadcrumb'][] = currentModule()->trans('general.tickets');
        $this->general_variables['tickets']              = ticket('main')
             ->remarks()
             ->where('lang', getLocale())
             ->where('user_id', user()->id)
             ->where("parent_id", 0)
             ->paginate(self::PAGE_LIMIT)
        ;

        return $this->template('list');
    }



    /**
     * show details of a ticket
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function single(SimpleYasnaRequest $request)
    {
        $this->setBasics();
        $this->general_variables['profile_breadcrumb'][] = currentModule()->trans('general.ticket-show');
        $ticket = model('ticket')->elector([
             'lang'   => getLocale(),
             'hashid' => $request->hashid,
        ])->where('user_id', user()->id)->first()
        ;
        if (!$ticket) {
            return $this->abort('404');
        }
        $this->general_variables['ticket']  = $ticket;
        $this->general_variables['replies'] = $ticket->deep_replies;
        return $this->template('single');
    }



    /**
     * display add page of tickets
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function showAddPage()
    {
        $this->setBasics();
        $this->general_variables['profile_breadcrumb'][] = currentModule()->trans('general.ticket-add');
        $flags = model('ticketType')->remarkPossibleFlags();
        $this->remove_element($flags, 'new');
        $this->remove_element($flags, 'done');
        $this->general_variables['flags'] = $this->generateFlagsComboitem($flags);
        return $this->template('new');
    }



    /**
     * save new ticket
     *
     * @param TicketAddRequest $request
     *
     * @return string
     * @throws \Modules\Remark\Exceptions\ImpossibleFlagException
     */
    public function addNew(TicketAddRequest $request)
    {
        $ticket = ticket('main')->addRemark([
             "title" => $request->subject,
             "text"  => $request->text,
        ]);

        $ticket->markAs($request->priority);

        return $this->jsonAjaxSaveFeedback($ticket, [
             'success_message' => $this->module()->getTrans('general.ticket-saved'),
        ]);
    }



    /**
     * save reply for specify ticket
     *
     * @param TicketReplyRequest $request
     *
     * @return string
     */
    public function addReply(TicketReplyRequest $request)
    {
        $is_saved = model('ticket', $request->ticket_hash)->addReply([
             "text" => $request->text,
             "lang" => getLocale(),
        ]);

        return $this->jsonAjaxSaveFeedback($is_saved, [
             'success_refresh' => '1',
             'success_message' => $this->module()->getTrans('general.reply-saved'),
        ]);
    }



    /**
     * remove array item with value
     *
     * @param array  $array
     * @param string $value
     */
    private function remove_element(&$array, $value)
    {
        if (($key = array_search($value, $array)) !== false) {
            unset($array[$key]);
        }
    }



    /**
     * generate flags combo item
     *
     * @param array $flags
     *
     * @return array
     */
    private function generateFlagsComboitem($flags)
    {
        $item = [];
        foreach ($flags as $flag) {
            $item[] = [
                 'title' => trans('tickets::flags.' . $flag),
                 'value' => $flag,
            ];
        }
        return $item;
    }

}
