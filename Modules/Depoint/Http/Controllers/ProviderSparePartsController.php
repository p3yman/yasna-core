<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/21/18
 * Time: 3:49 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Http\Requests\SparePartSaveFilesRequest;
use Modules\Depoint\Http\Requests\SparePartSavePreviewFileRequest;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class ProviderSparePartsController extends DepointFrontControllerAbstract
{
    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = 'profile.panel';



    /**
     * Returns the preview of a file item in the spare part single page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function fileItem(SimpleYasnaRequest $request)
    {
        $file_hashid = $request->hashid;
        return $this->template('spare-part-single.file-item', compact('file_hashid'));
    }



    /**
     * Saves changes on the main files of a spare part.
     *
     * @param SparePartSaveFilesRequest $request
     *
     * @return string
     */
    public function saveFiles(SparePartSaveFilesRequest $request)
    {
        $spare_part         = $request->model;
        $new_main_files     = $request->main_files;
        $current_main_files = $spare_part->main_files;

        // Weather there is any difference between the new main files and current main files.
        if ($new_main_files == $current_main_files) {
            return $this->jsonAjaxSaveFeedback(0, [
                 'danger_message' => $this->module()->trans('spare-parts.spare-part.main-files-not-changed'),
            ]);
        }

        $ok = $spare_part->batchSaveBoolean([
             'pending_main_files' => $new_main_files,
        ]);
        return $this->jsonAjaxSaveFeedback($ok, [
             'success_message' => $this->module()->trans('spare-parts.spare-part.main-files-saved-pending'),
        ]);
    }



    /**
     * Returns the preview of preview file in the spare part single page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function imageView(SimpleYasnaRequest $request)
    {
        $file_hashid = $request->hashid;
        return $this->template('spare-part-single.image-preview', compact('file_hashid'));
    }



    /**
     * Saves change in the preview file of a spare part.
     *
     * @param SparePartSavePreviewFileRequest $request
     *
     * @return string
     */
    public function savePreviewFile(SparePartSavePreviewFileRequest $request)
    {
        $spare_part         = $request->model;
        $new_preview_file     = $request->preview_file;
        $current_preview_file = $spare_part->preview_file;

        // Weather there is any difference between the new preview file and current preview file.
        if ($new_preview_file == $current_preview_file) {
            return $this->jsonAjaxSaveFeedback(0, [
                 'danger_message' => $this->module()->trans('spare-parts.spare-part.preview-file-not-changed'),
            ]);
        }

        $ok = $spare_part->batchSaveBoolean([
             'pending_preview_file' => $new_preview_file,
        ]);
        return $this->jsonAjaxSaveFeedback($ok, [
             'success_message' => $this->module()->trans('spare-parts.spare-part.preview-file-saved-pending'),
        ]);
    }
}
