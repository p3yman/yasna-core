<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/16/18
 * Time: 10:06 AM
 */

namespace Modules\Depoint\Http\Controllers;


use App\Models\Address;
use App\Models\Cart;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;
use Modules\Depoint\Http\Middleware\MergeCartMiddleware;
use Modules\Depoint\Http\Requests\CartRepurchaseRequest;
use Modules\Depoint\Http\Requests\PurchasePaymentRequest;
use Modules\Depoint\Http\Requests\PurchaseSetDeliveryInfoRequest;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Payment\Services\PaymentHandler\Transaction\Transaction as TransactionHandler;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class PurchaseController extends DepointFrontControllerAbstract
{
    /**
     * The Folder's Name of Views
     *
     * @var string
     */
    protected $view_folder = 'order';



    /**
     * Provides a page to review the cart.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function review()
    {
        return $this->template('cart.main', ['review' => true]);
    }



    /**
     * Provides the delivery page of the order.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function delivery()
    {
        return $this->template('checkout.main');
    }



    /**
     * Saves a new address with the given data.
     *
     * @param PurchaseSetDeliveryInfoRequest $request
     *
     * @return Address
     */
    protected function saveNewAddress(PurchaseSetDeliveryInfoRequest $request)
    {
        return model('address')
             ->batchSave($request->only([
                  'receiver',
                  'city_id',
                  'address',
                  'telephone',
                  'postal_code',
             ]));
    }



    /**
     * Sets the address of the current client's cart.
     *
     * @param PurchaseSetDeliveryInfoRequest $request
     *
     * @return string
     */
    public function setDeliveryInfo(PurchaseSetDeliveryInfoRequest $request)
    {
        if ($request->isSavingNewAddress()) {
            $address_model = $this->saveNewAddress($request);
        } else {
            $address_model = model('address', $request->selected_address);
        }

        $cart = depoint()->cart()->findActiveCart();

        $address_success         = $cart->update(['address_id' => $address_model->id]);
        $shipping_method_success = $cart->addShippingMethod($request->shipping_method);
        $ok                      = ($address_success and $shipping_method_success);

        return $this->jsonSaveFeedback($ok, [
             'success_message'  => currentModule()->trans('general.address.saved-for-cart'),
             'success_redirect' => route_locale('depoint.cart.review'),
        ]);
    }



    /**
     * Provides a page to select payment method.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function paymentMethods()
    {
        return $this->template('payment-method.main');
    }



    /**
     * Effects cart before payment.
     *
     * @param Cart $cart
     */
    protected function paymentEffectCart(Cart $cart)
    {
        $cart->checkout();

        $cart->markAppliedCouponCodeAsUsed();
    }



    /**
     * Returns the response of the `fire()` method of the new transaction.
     *
     * @param Cart       $cart
     * @param string|int $account_id
     *
     * @return array
     */
    protected function fireTransaction(Cart $cart, $account_id)
    {
        $transaction = payment()
             ->transaction()
             ->invoiceId($cart->id)
             ->amount($cart->invoiced_amount)
             ->accountId($account_id)
             ->autoVerify(false)
             ->callbackUrl(route_locale('depoint.cart.payment-callback', ['cart' => $cart->hashid]))
        ;


        return $transaction->fire();
    }



    /**
     * Does the payment action.
     *
     * @param PurchasePaymentRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function doPayment(PurchasePaymentRequest $request)
    {
        $account_id = $request->account;
        $cart       = depoint()->cart()->findActiveCart();

        $this->paymentEffectCart($cart);

        $transaction_response = $this->fireTransaction($cart, $account_id);

        if ($transaction_response['slug'] == 'returnable-response') {
            return $transaction_response['response'];
        }

        return redirect()->back();
    }



    /**
     * Finds and returns a cart with its hashid.
     *
     * @param string $cart_hashid
     *
     * @return Cart|null
     */
    protected function findCartWithHashid($cart_hashid)
    {
        $cart = model('cart')->grabHashid($cart_hashid);

        return $cart->exists ? $cart : null;
    }



    /**
     * Finds and returns transactions of the specified cart.
     *
     * @param Cart $cart
     *
     * @return Collection
     */
    protected function findCartTransactions(Cart $cart)
    {
        return payment()->invoice($cart->id)->orderByDesc('created_at')->get();
    }



    /**
     * Returns a response to be returned in the callback page if the information are invalid.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function paymentCallbackInvalidResponse()
    {
        return redirect(route_locale('depoint.cart'));
    }



    /**
     * Checks the inventory of wares in the specified cart.
     *
     * @param Cart $cart
     *
     * @return bool Weather all wares have enough inventory or not.
     */
    protected function checkItemsInventory(Cart $cart)
    {
        $cart->depointRefreshCalculations();
        $orders = $cart->orders;
        $enough = true;

        foreach ($orders as $order) {
            $count = $order->count;
            $ware  = $order->ware;

            if ($ware->inventoryIsEnough($count)) {
                continue;
            }

            $cart->removeOrder($order->id);

            $enough = false;
        }

        return $enough;
    }



    /**
     * Handles the not-enough-inventory problem.
     *
     * @param Cart $cart
     *
     * @return void
     */
    protected function handleInventoryProblem(Cart $cart)
    {
        $cart->refreshCalculations();
        $cart->applyNotRaisedStatus();

        $this->general_variables['not_enough_inventory'] = true;
        $this->general_variables['redirect_url']         = route_locale('depoint.cart');
        $this->general_variables['redirect_errors']      = [
             'cart_error' => $this->module()->trans('cart.message.failure.items-has-been-changed'),
        ];
    }



    /**
     * Handles the verified transaction.
     *
     * @param Cart               $cart
     * @param TransactionHandler $transaction_handler
     *
     * @return void
     */
    protected function handleVerifiedTransaction(Cart $cart, TransactionHandler $transaction_handler)
    {
        $transaction = $transaction_handler->getModel();
        $transaction->refresh();

        $cart->applyPaidStatus();

        $this->general_variables['transaction_alert']   = $this->module()->trans('cart.message.success.paid');
        $this->general_variables['transaction_success'] = true;
    }



    /**
     * Handles new transaction.
     *
     * @param TransactionHandler $transaction_handler
     * @param Cart               $cart
     *
     * @return void
     */
    protected function handleNewTransaction(TransactionHandler $transaction_handler, Cart $cart)
    {
        if (!$this->checkItemsInventory($cart)) {
            $this->handleInventoryProblem($cart);
            return;
        }

        $verification_result = $transaction_handler->verify();
        if ($verification_result['code'] > 0) {
            $this->handleVerifiedTransaction($cart, $transaction_handler);
            return;
        }

        $this->general_variables['transaction_alert'] = $this->module()
                                                             ->trans('cart.message.failure.rejected-by-gateway')
        ;
    }



    /**
     * Handles online transaction.
     *
     * @param TransactionHandler $transaction_handler
     * @param Cart               $cart
     *
     * @return void
     */
    protected function handleOnlineTransaction(TransactionHandler $transaction_handler, Cart $cart)
    {
        if ($old_verified_transaction = $cart->verifiedTransactions()->first()) {
            $transaction_handler->makeCancelled(now(),
                 'Cart has been verified with another transaction.');
            $this->general_variables['transaction_alert'] = $this->module()
                                                                 ->trans('cart.message.failure.unable-to-verify')
            ;
        } else {
            $this->handleNewTransaction($transaction_handler, $cart);
        }
    }



    /**
     * Handles transaction specified in the url.
     *
     * @param Transaction $transaction
     * @param Cart        $cart
     *
     * @return void
     */
    protected function handleTransaction(Transaction $transaction, Cart $cart)
    {
        $transaction_handler                            = $transaction->handler;
        $this->general_variables['transaction_handler'] = $transaction_handler;
        if ($transaction_handler->type() != 'online') {
            return;
        }

        $this->handleOnlineTransaction($transaction_handler, $cart);
    }



    /**
     * Checks for the transaction specified in the url.
     *
     * @param Cart        $cart
     * @param Collection  $transactions
     * @param string|null $tracking_number
     *
     * @return void
     */
    protected function checkForTransaction(Cart $cart, Collection $transactions, $tracking_number)
    {
        if (!$tracking_number) {
            return;
        }

        $this->general_variables['tracking_number'] = $tracking_number;

        $query_transaction = $transactions
             ->where('tracking_no', $tracking_number)
             ->first()
        ;
        if (!$query_transaction) {
            return;
        }

        $this->handleTransaction($query_transaction, $cart);
    }



    /**
     * Applies the disbursement of the given cart if needed.
     *
     * @param Cart $cart
     *
     * @return void
     */
    protected function paymentCallbackApplyCartDisbursement(Cart $cart)
    {
        if ($cart->isDisbursed()) {
            return;
        }
        $declared_transaction = $cart->declaredTransactions()->offline()->first();

        if ($declared_transaction) {
            $cart->update([
                 'disbursed_at' => $declared_transaction->declarad_at,
                 'disbursed_by' => $declared_transaction->declarad_by,
            ]);
        }
    }



    /**
     * Applies the paid status on the given cart if needed.
     *
     * @param Cart $cart
     *
     * @return void
     */
    protected function paymentCallbackApplyCartPayment(Cart $cart)
    {
        if ($cart->isPaid()) {
            return;
        }

        $verified_transaction = $cart->verifiedTransactions()->first();
        if ($verified_transaction) {
            $cart->update([
                 'paid_at' => $verified_transaction->verified_at,
                 'paid_by' => $verified_transaction->verified_by,
            ]);
        }
    }



    /**
     * Modifies the given cart in the payment callback action.
     *
     * @param Cart $cart
     */
    protected function paymentCallbackModifyCart(Cart $cart)
    {
        $this->paymentCallbackApplyCartDisbursement($cart);
        $this->paymentCallbackApplyCartPayment($cart);
    }



    /**
     * Returns the response of the payment callback action.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    protected function paymentCallbackResponse()
    {
        if ($this->general_variables['not_enough_inventory'] ?? false) {
            return redirect($this->general_variables['redirect_url'])
                 ->withErrors($this->general_variables['redirect_errors']);
        } else {
            return $this->template('receipt.main');
        }

    }



    /**
     * Does the actions on the payment callback.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function paymentCallback(SimpleYasnaRequest $request)
    {
        $cart = $this->general_variables['cart'] = $this->findCartWithHashid($request->cart);
        if (!$cart or $cart->isNotRaised()) {
            return $this->paymentCallbackInvalidResponse();
        }

        $transactions = $this->general_variables['transactions'] = $this->findCartTransactions($cart);
        if (!$transactions->count()) {
            return $this->paymentCallbackInvalidResponse();
        }

        $this->checkForTransaction($cart, $transactions, $request->tracking_no);

        $this->paymentCallbackModifyCart($cart);

        $this->general_variables['transactions'] = $this->findCartTransactions($cart);

        return $this->paymentCallbackResponse();
    }



    /**
     * Fires the specified transaction.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function fire(SimpleYasnaRequest $request)
    {
        $transaction = model('transaction', $request->transaction);

        if ($transaction->not_exists) {
            return $this->abort(410);
        }

        $handler  = $transaction->handler;
        $response = $handler->fire();

        if ($response['slug'] == 'returnable-response') {
            return $response['response'];
        }

        return redirect(route_locale('depoint.cart.payment-callback', [
             'cart' => hashid($transaction->getModel()->invoice_id),
        ]));
    }



    /**
     * Returns the last transaction of the specified cart.
     *
     * @param Cart $cart
     *
     * @return Transaction|null
     */
    protected function findLastTransactionOfCart(Cart $cart)
    {
        return $cart->transactions()
                    ->orderByDesc('created_at')
                    ->first()
             ;
    }



    /**
     * Does the repurchase process fot the specified cart with the given account.
     *
     * @param CartRepurchaseRequest $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function repurchase(CartRepurchaseRequest $request)
    {
        $transaction = $this->findLastTransactionOfCart($request->model);

        if (!$transaction) {
            return $this->abort(410);
        }

        $handler = $transaction
             ->handler
             ->clone(['account_id' => $request->account]);

        return $this->jsonFeedback([
             'ok'           => 1,
             'redirect'     => route_locale('depoint.purchase.fire', [
                  'transaction' => $handler->getModel()->hashid,
             ]),
             'message'      => trans('manage::forms.feed.wait'),
             'redirectTime' => 3000,
        ]);
    }
}
