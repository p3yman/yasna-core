<?php

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Divisions\Entities\Division;
use Nwidart\Modules\Facades\Module;

class DepointTestController extends DepointFrontControllerAbstract
{


    /**
     * Displays index page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->view('depoint::index.main');
    }



    /**
     * Displays login  page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function login()
    {
        return $this->view('depoint::auth.login.main');
    }



    /**
     * Displays register page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function register()
    {
        return $this->view('depoint::auth.register.main');
    }



    /**
     * displays reset password page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function resetPassword()
    {
        $auth_title = trans('depoint::general.auth.reset_password');
        $type       = "reset";

        return $this->view('depoint::auth.password.main', compact('auth_title', 'type'));
    }



    /**
     * displays forgot password
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function forgotPassword()
    {
        $auth_title = trans('depoint::general.auth.forgot_password');
        $type       = "forgot";

        return $this->view('depoint::auth.password.main', compact('auth_title', 'type'));
    }



    /**
     * displays order cart
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function cart()
    {
        return $this->view('depoint::order.cart.main');
    }



    /**
     * displays review order cart
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function cartReview()
    {
        $review = true;
        return $this->view('depoint::order.cart.main', compact('review'));
    }



    /**
     * displays checkout options setting
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function checkout()
    {
        return $this->view('depoint::order.checkout.main');
    }



    /**
     * displays payment feedback
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function payment()
    {
        return $this->view('depoint::order.payment.main');
    }



    /**
     * displays news list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function newsList()
    {
        return $this->view('depoint::news.list.main');
    }



    /**
     * displays news single
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function newsSingle()
    {
        return $this->view('depoint::news.single.main');
    }



    /**
     * displays support list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function supportList()
    {
        return $this->view('depoint::support.list.main');
    }



    /**
     * displays support single
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function supportSingle()
    {
        return $this->view('depoint::support.single.main');
    }
    
    
    
    /**
     * displays support form
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function supportService()
    {
        $default = [
            'value' => '0',
            'title' => '- انتخاب کنید -'
        ];
        
        $provinces = model('division')->elector(['selectProvince'=>true])
                                      ->select('id as value', 'title_fa as title')
                                      ->get()
                                      ->toArray();
        
        $cities = model('division')->elector(['selectCity'=>true])
                                   ->select('id as value', 'title_fa as title', 'province_id as parent')
                                   ->get()
                                   ->toArray();
        
        array_unshift($provinces, $default);
        array_unshift($cities, $default);
        
        return $this->view('depoint::support.service.main', compact('provinces', 'cities'));
    }
    


    /**
     * displays contact page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function contact()
    {
        return $this->view('depoint::contact.main');
    }



    /**
     * displays contact page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function about()
    {
        return $this->view('depoint::about.main');
    }



    /**
     * displays board page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function board()
    {
        return $this->view('depoint::board.main');
    }



    /**
     * displays shareholders page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function shareholders()
    {
        return $this->view('depoint::shareholders.main');
    }



    /**
     * displays jobs page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function jobs()
    {
        return $this->view('depoint::jobs.main');
    }



    /**
     * displays jobs single page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function jobsSingle()
    {
        return $this->view('depoint::jobs.single.main');
    }



    /**
     * displays products landing page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function productLanding()
    {
        return $this->view('depoint::product.landing.main');
    }



    /**
     * displays products list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function productList()
    {
        return $this->view('depoint::product.list.main');
    }



    /**
     * displays products single
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function productSingle()
    {
        return $this->view('depoint::product.single.main', [
             'products' => model('post', 18)->get(),
        ]);
    }



    /**
     * displays product compare
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function productCompare()
    {
        return $this->view('depoint::product.compare.main');
    }



    /**
     * displays email template
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function email()
    {
        return $this->view('depoint::email.main');
    }



    /**
     * displays panel main page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function panel()
    {
        return $this->view('depoint::profile.panel.main');
    }



    /**
     * displays panel orders list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function panelOrders()
    {
        return $this->view('depoint::profile.panel.orders');
    }



    /**
     * displays panel order single
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function panelOrderSingle()
    {
        return $this->view('depoint::profile.panel.order-single');
    }



    /**
     * displays panel parts list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function panelParts()
    {
        return $this->view('depoint::profile.panel.parts');
    }



    /**
     * displays panel ticket list view
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function tickets()
    {
        return $this->view('depoint::profile.tickets.list');
    }



    /**
     * displays panel ticket single view
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function ticketSingle()
    {
        return $this->view('depoint::profile.tickets.single');
    }



    /**
     * displays panel new ticket page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function ticketNew()
    {
        return $this->view('depoint::profile.tickets.new');
    }



    /**
     * displays orders receipt
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function receipt()
    {
        return $this->view('depoint::order.receipt.main');
    }



    /**
     * displays orders invoice
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function invoice()
    {
        return $this->view('depoint::order.invoice.main');
    }



    /**
     * displays payment methods
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function paymentMethod()
    {
        return $this->view('depoint::order.payment-method.main');
    }



    /**
     * displays profile main page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function profile()
    {
        return $this->view('depoint::profile.customer.main');
    }



    /**
     * displays profile lottery code
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function profileLottery()
    {
        return $this->view('depoint::profile.customer.lottery');
    }



    /**
     * displays profile edit
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function profileEdit()
    {
        return $this->view('depoint::profile.customer.edit');
    }



    /**
     * displays profile page for account new value
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function profileNewValue()
    {
        return $this->view('depoint::profile.customer.new-value');
    }



    /**
     * displays profile page after updating value
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function profileValueResult()
    {
        $success = false;

        return $this->view('depoint::profile.customer.value-update-result', compact('success'));
    }



    /**
     * displays faq page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function faq()
    {
        return $this->view('depoint::faq.main');
    }



    /**
     * displays new faq page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function faqNew()
    {
        return $this->view('depoint::faq.new');
    }



    /**
     * displays where-to-buy single page
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function whereToBuy()
    {
        return $this->view('depoint::where-to-buy.main');
    }



    /**
     * displays image gallery list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function imageGalleryList()
    {
        $hover_icon = Module::asset('depoint:images/gallery.svg');
        $page_title = trans('depoint::general.image_gallery');
        return $this->view('depoint::gallery.list.main', compact('hover_icon', 'page_title'));
    }



    /**
     * displays image gallery single
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function imageGallerySingle()
    {
        return $this->view('depoint::gallery.single.main');
    }



    /**
     * displays video gallery list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function videoGalleryList()
    {
        $hover_icon = Module::asset('depoint:images/videos.svg');
        $page_title = trans('depoint::general.video_gallery');
        return $this->view('depoint::gallery.list.main', compact('hover_icon', 'page_title'));
    }



    /**
     * displays video gallery single
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function videoGallerySingle()
    {
        return $this->view('depoint::gallery.video-single.main');
    }



    /**
     * displays festivals list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function festivals()
    {
        return $this->view('depoint::festivals.main');
    }



    /**
     * displays blog list
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function blogList()
    {
        return $this->view('depoint::blog.list.main');
    }



    /**
     * displays blog single
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function blogSingle()
    {
        return $this->view('depoint::blog.single.main');
    }
}
