<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 1:30 PM
 */

namespace Modules\Depoint\Http\Controllers;


use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;

class HomeController extends DepointFrontControllerAbstract
{
    /**
     * The Path to the Views Folder
     *
     * @var string
     */
    protected $view_folder = 'index';
    const SLIDER_LIMIT          = 18;
    const PRODUCT_LANDING_LIMIT = 10;
    const BLOG_LIMIT            = 2;
    const INNOVATION_LIMIT      = 3;



    /**
     * Generates and returns the index page.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $this->getSliders();
        $this->getProductLandings();
        $this->setInnovationsFeatureA();
        $this->setInnovationsFeatureB();

        return $this->template('main');
    }



    /**
     * get slider items
     */
    private function getSliders()
    {
        $this->general_variables['slides'] = PostsServiceProvider::select([
             'type' => $this->posttype_slugs['slider'],
        ])->orderByDesc('published_at')->limit(self::SLIDER_LIMIT)->get()
        ;
    }



    /**
     * get product landing  items
     */
    private function getProductLandings()
    {
        $this->general_variables['products'] = $this->getProductLandingsBuilder()
                                                    ->orderByDesc('published_at')
                                                    ->limit(self::PRODUCT_LANDING_LIMIT)
                                                    ->get()
        ;
    }



    /**
     * Sets the posts to be shown in the feature A part.
     */
    private function setInnovationsFeatureA()
    {
        $this->general_variables['innovations_a'] = PostsServiceProvider::select([
             'type'     => $this->posttype_slugs['innovation'],
             'category' => 'significant-innovations',
        ])->orderByDesc('published_at')->limit(self::INNOVATION_LIMIT)->get()
        ;
    }



    /**
     * Sets the posts to be shown in the feature B part.
     */
    private function setInnovationsFeatureB()
    {
        $this->general_variables['innovations_b'] = PostsServiceProvider::select([
             'type'     => $this->posttype_slugs['innovation'],
             'category' => 'sophisticated-designs',
        ])->orderByDesc('published_at')->limit(self::INNOVATION_LIMIT)->get()
        ;
    }
}
