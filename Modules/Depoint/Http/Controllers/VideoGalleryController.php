<?php

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class VideoGalleryController extends DepointFrontControllerAbstract
{
    protected $view_folder = "gallery";
    const MAX_ITEM = 15;



    /**
     * VideoGalleryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->posttype = $this->posttype_slugs['video-gallery'];
    }



    /**
     * display list of gallery
     */
    public function index()
    {
        $this->general_variables['images'] = $this->getImageGallery();
        $this->general_variables['title']  = model('posttype')->grabSlug($this->posttype)->titleIn(getLocale());
        return $this->template('list.main');
    }



    /**
     * action of single page
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function single(SimpleYasnaRequest $request)
    {
        $post = $this->singleFindPost($request);
        if (!$post) {
            return $this->abort(404);
        }

        $this->general_variables['title']          = isset($request->title) ? $request->title : currentModule()->trans('general.image_gallery');
        $this->general_variables['post_title']     = $post->title;
        $this->general_variables['posttype']       = $post->posttype;
        $this->general_variables['posttype_title'] = $post->posttype->titleIn(getLocale());

        $this->general_variables['model']      = $post;
        $this->general_variables['video_hash'] = depointTools()->getAparatHash($post->getMeta('aparat_link'));
        return $this->template('video-single.main');
    }



    /**
     * get image-gallery items
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    private function getImageGallery()
    {
        return PostsServiceProvider::select([
             'type' => $this->posttype_slugs['video-gallery'],
        ])->orderByDesc('published_at')
                                   ->paginate(posttype($this->posttype)->getMeta('max_per_page') ?? self::MAX_ITEM)
             ;
    }


}
