<?php

namespace Modules\Depoint\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

class NewsController extends DepointFrontControllerAbstract
{
    /**
     * The folder of views for this controller.
     *
     * @var string
     */
    protected $view_folder = "news";
    /**
     * An instance of the `Posttype` model
     * which is pointing to the `news` post type.
     *
     * @var Posttype|null
     */
    protected $posttype;



    /**
     * Returns an instance of the `Posttype` model
     * which is pointing to the `news` post type.
     *
     * @return Posttype
     */
    protected function getPosttype()
    {
        if (!$this->posttype) {
            $this->posttype = posttype($this->posttype_slugs['news']);
        }

        return $this->posttype;
    }



    /**
     * Adds the `posttype` field to `general_variables`.
     */
    protected function posttypeVariable()
    {
        $this->general_variables['posttype'] = $this->getPosttype();
    }



    /**
     * Generates and returns a builder to select news.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function newsBuilder()
    {
        return PostsServiceProvider::select([
             'type' => $this->getPosttype()->slug,
        ]);
    }



    /**
     * Guesses and returns the limit for news list.
     *
     * @return int
     */
    protected function listPaginationLimit()
    {
        $config_limit = $this->getPosttype()->getMeta('max_per_page');
        return ($config_limit ?: 24);
    }



    /**
     * Selects the models to be shown in the news list view
     * and appends it to the `general_variables` array.
     */
    protected function selectListModels()
    {
        $this->general_variables['models'] = $this->newsBuilder()
                                                  ->orderByDesc('published_at')
                                                  ->paginate($this->listPaginationLimit())
        ;
    }



    /**
     * The list page of the news.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if ($this->getPosttype()->not_exists) {
            return $this->abort(404);
        }

        $this->selectListModels();
        $this->posttypeVariable();

        return $this->template('list.main');
    }



    /**
     * Finds the post to be shown in the single page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return Post|null
     */
    protected function singleFindPost(SimpleYasnaRequest $request)
    {
        return PostsServiceProvider::select([
             'type'   => $this->getPosttype()->slug,
             'hashid' => $request->hashid,
        ])->first()
             ;
    }



    /**
     * Renders and returns the response of the single view depending on the specified news.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function single(SimpleYasnaRequest $request)
    {
        $post = $this->singleFindPost($request);
        if (!$post) {
            return $this->abort(410);
        }

        $this->setPostSisterhoodLinksParameters($post);
        $this->general_variables['model'] = $post;
        $this->posttypeVariable();

        return $this->template('single.main');
    }
}
