<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/4/18
 * Time: 4:12 PM
 */

namespace Modules\Depoint\Http\Controllers\Traits;


trait PanelControllerTrait
{
    /**
     * Returns an array of general items for side menu.
     *
     * @return array
     */
    protected function generaMenuItems()
    {
        return [
             [
                  'link'  => route_locale('depoint.front.dashboard'),
                  'label' => currentModule()->trans('general.dashboard'),
             ],
             [
                  'link'  => route_locale('depoint.front.dashboard.edit-profile'),
                  'label' => currentModule()->trans('general.edit-profile'),
             ],
             [
                  'link'  => route_locale('depoint.front.dashboard.edit-password'),
                  'label' => currentModule()->trans('general.edit-password'),
             ],
             [
                  'link'  => route_locale('depoint.front.dashboard.orders'),
                  'label' => currentModule()->trans('cart.order.title.plural'),
             ],
             [
                  'link'  => route_locale('depoint.front.dashboard.codes'),
                  'label' => currentModule()->trans('cart.receipt.title.plural'),
             ],
             [
                  'link'  => route_locale('depoint.front.dashboard.addresses.list'),
                  'label' => currentModule()->trans('address.title.plural'),
             ],
             [
                  'link'  => route_locale('depoint.front.dashboard.tickets'),
                  'label' => currentModule()->trans('general.tickets'),
             ],
        ];
    }



    /**
     * Returns an array of side menu items for providers.
     *
     * @return array
     */
    protected function panelMenuItems()
    {
        if (user()->hasAccessToProvidersEnvironment()) {
            return [
                 [
                      'link'  => route_locale('depoint.front.provider-panel.spare-parts'),
                      'label' => currentModule()->trans('spare-parts.spare-part.title.plural'),
                 ],
                 [
                      'link'  => route_locale('depoint.front.provider-panel.orders'),
                      'label' => currentModule()->trans('spare-parts.order.title.plural'),
                 ],
            ];
        } else {
            return [];
        }
    }



    /**
     * Sets the menu items.
     */
    protected function setMenuItems()
    {
        $this->general_variables['sidebar_menu'] = array_merge($this->generaMenuItems(), $this->panelMenuItems());
    }



    /**
     * Sets providers.
     */
    protected function setProviders()
    {
        $providers = user()->depoint_providers;

        $this->general_variables['providers']          = $providers;
        $this->general_variables['multiple_providers'] = ($providers->count() > 1);
    }



    /**
     * Sets the builder for selecting orders.
     */
    protected function setOrdersBuilder()
    {
        $providers                                 = $this->general_variables['providers'];
        $providers_ids                             = $providers->pluck('id')->toArray();
        $this->general_variables['orders_builder'] = model('depoint-order')
             ->whereIn('depoint_provider_id', $providers_ids)
             ->orderByDesc('created_at')
        ;
    }
}
