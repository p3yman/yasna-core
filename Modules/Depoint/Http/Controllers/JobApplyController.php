<?php

namespace Modules\Depoint\Http\Controllers;

use Modules\Depoint\Services\Controllers\DepointFrontControllerAbstract;

class JobApplyController extends DepointFrontControllerAbstract
{
    protected $view_folder = 'jobs.apply';


    public function index()
    {
        return $this->template('index');
    }
}
