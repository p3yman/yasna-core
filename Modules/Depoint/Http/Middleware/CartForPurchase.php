<?php

namespace Modules\Depoint\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CartForPurchase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->cartIsNotReadyForPurchase()) {
            return redirect(route_locale('depoint.cart.delivery'));
        }

        return $next($request);
    }



    /**
     * Checks if the client's active cart is ready to be purchased.
     *
     * @return bool
     */
    protected function cartIsReadyForPurchase()
    {
        return depoint()
             ->cart()
             ->findActiveCart()
             ->isReadyForDepointPurchase()
             ;
    }



    /**
     * Checks if the client's active cart is not ready to be purchased.
     *
     * @return bool
     */
    protected function cartIsNotReadyForPurchase()
    {
        return !$this->cartIsReadyForPurchase();
    }
}
