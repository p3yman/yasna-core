<?php

namespace Modules\Depoint\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ProviderPanelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (user()->hasNotAccessToProvidersEnvironment()) {
            return redirect('/');
        }

        return $next($request);
    }
}
