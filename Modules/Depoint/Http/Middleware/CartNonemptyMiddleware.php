<?php

namespace Modules\Depoint\Http\Middleware;

use App\Models\Cart;
use Closure;
use Illuminate\Http\Request;

class CartNonemptyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cart         = $this->getCart();
        $orders_count = $cart->orders()->count();

        if (!$orders_count) {
            return redirect(route_locale('depoint.cart'));
        }


        return $next($request);
    }



    /**
     * Returns the Cart object to be checked.
     *
     * @return Cart
     */
    protected function getCart()
    {
        return depoint()->cart()->findActiveCart();
    }
}
