<?php

namespace Modules\Depoint\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class OwnCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cart = model('cart', $request->cart);

        if ($cart->not_exists) {
            return redirect(depoint()->homeUrl());
        }

        if ($cart->user_id != user()->id) {
            return redirect(depoint()->homeUrl());
        }


        return $next($request);
    }
}
