<?php

namespace Modules\Depoint\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MergeCartMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->handleCartAfterLogin();

        return $next($request);
    }



    /**
     * Handles carts in session and the database.
     */
    protected function handleCartAfterLogin()
    {
        if (auth()->guest()) {
            return;
        }

        $session_cart = depoint()->cart()->getSessionCart();

        if ($session_cart->not_exists) {
            return;
        }

        $user_cart = depoint()->cart()->getUsersCart(user());

        if ($user_cart->exists) {
            $user_cart->mergeWith($session_cart->id);
            $session_cart->delete();
        } else {
            $session_cart->update(['user_id' => user()->id]);
        }

        depoint()->cart()->clearSession();
    }
}
