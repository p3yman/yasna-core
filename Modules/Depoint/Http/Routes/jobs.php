<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Depoint')->getControllersNamespace(),
     'prefix'     => '/',
], function () {
    Route::get('jobs/apply', 'JobApplyController@index')->name('jobs.apply');
});


