<?php

Route::group([
     'middleware' => 'web',
     'prefix'     => 'depoint',
     'namespace'  => 'Modules\Depoint\Http\Controllers',
], function () {
    Route::get('/', 'DepointTestController@index');
    Route::get('/login', 'DepointTestController@login');
    Route::get('/register', 'DepointTestController@register');
    Route::get('/forgot-password', 'DepointTestController@forgotPassword');
    Route::get('/reset-password', 'DepointTestController@resetPassword');

    Route::group([
         "prefix" => "order",
    ], function () {
        Route::get('/cart', 'DepointTestController@cart');
        Route::get('/cart/review', 'DepointTestController@cartReview');
        Route::get('/checkout', 'DepointTestController@checkout');
        Route::get('/checkout/method', 'DepointTestController@paymentMethod');
        Route::get('/checkout/payment', 'DepointTestController@payment');
        Route::get('/receipt', 'DepointTestController@receipt');
        Route::get('/invoice', 'DepointTestController@invoice');
    });

    Route::get('/news/list', 'DepointTestController@newsList');
    Route::get('/news/single', 'DepointTestController@newsSingle');

    Route::get('/blog/list', 'DepointTestController@blogList');
    Route::get('/blog/single', 'DepointTestController@blogSingle');

    Route::get('/support/list', 'DepointTestController@supportList');
    Route::get('/support/single', 'DepointTestController@supportSingle');
    Route::get('/support/service', 'DepointTestController@supportService');

    Route::get('/contact', 'DepointTestController@contact');
    Route::get('/about', 'DepointTestController@about');

    Route::get('/board', 'DepointTestController@board');
    Route::get('/shareholders', 'DepointTestController@shareholders');

    Route::get('/jobs', 'DepointTestController@jobs');
    Route::get('/jobs/single', 'DepointTestController@jobsSingle');

    Route::get('/gallery/images/list', 'DepointTestController@imageGalleryList');
    Route::get('/gallery/images/single', 'DepointTestController@imageGallerySingle');

    Route::get('/gallery/videos/list', 'DepointTestController@videoGalleryList');
    Route::get('/gallery/videos/single', 'DepointTestController@videoGallerySingle');

    Route::group([
         "prefix" => "product",
    ], function () {
        Route::get('/landing', 'DepointTestController@productLanding');
        Route::get('/list', 'DepointTestController@productList');
        Route::get('/single', 'DepointTestController@productSingle');
        Route::get('/compare', 'DepointTestController@productCompare');
    });

    Route::get('/email', 'DepointTestController@email');
    Route::get('/faq', 'DepointTestController@faq');
    Route::get('/faq/new', 'DepointTestController@faqNew');
    Route::get('/where-to-buy', 'DepointTestController@whereToBuy');
    Route::get('/festivals', 'DepointTestController@festivals');

    Route::group([
         "prefix" => "panel",
    ], function () {

        Route::get('/', 'DepointTestController@panel');
        Route::get('/orders', 'DepointTestController@panelOrders');
        Route::get('/order-single', 'DepointTestController@panelOrderSingle');
        Route::get('/parts', 'DepointTestController@panelParts');

        Route::get('/tickets', 'DepointTestController@tickets');
        Route::get('/ticket/single', 'DepointTestController@ticketSingle');
        Route::get('/ticket/new', 'DepointTestController@ticketNew');
    });

    Route::group([
         "prefix" => "profile",
    ], function () {

        Route::get('/', 'DepointTestController@profile');
        Route::get('/new-value', 'DepointTestController@profileNewValue');
        Route::get('/value-result', 'DepointTestController@profileValueResult');
        Route::get('/lottery-code', 'DepointTestController@profileLottery');
        Route::get('/edit', 'DepointTestController@profileEdit');
    });

});

/**
 * Routes of the Manage Side
 */
Route::group([
     'prefix'     => 'manage/depoint',
     'middleware' => ['web', 'auth', 'is:admin'],
     'namespace'  => 'Modules\Depoint\Http\Controllers\Manage',
], function () {
    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductsBrowseController@index')->name('depoint.manage.products');
        Route::get('grid', 'ProductsBrowseController@grid')->name('depoint.manage.products.grid');
        Route::get('browse-modal', 'ProductsBrowseController@browseModal')
             ->name('depoint.manage.products.browse-modal')
        ;

        Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'ProductsController@singleAction')
             ->name('depoint.manage.products.single-action')
        ;

        Route::post('save', 'ProductsController@save')->name('depoint.manage.products.save');
        Route::post('delete-undelete', 'ProductsController@deleteOrUndelete')
             ->name('depoint.manage.products.delete-or-unndelete')
        ;

    });

    /**
     * widget route group
     */
    Route::group(['prefix' => 'widget'], function () {
        Route::get('gender', "WidgetController@genderData");
        Route::get('age', "WidgetController@ageCategoryData");
        Route::get('register', "WidgetController@registerCountInMonth");
        Route::get('sale', "WidgetController@saleStatisticsData");
        Route::get('sale-chart', "WidgetController@salesCountInMonth");
        Route::get('codes', "WidgetController@latestDiscountCode");
    });

    Route::group(['prefix' => 'providers'], function () {
        Route::get('/', 'ProvidersBrowseController@index')->name('depoint.manage.providers');
        Route::get('grid', 'ProvidersBrowseController@grid')->name('depoint.manage.providers.grid');

        Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'ProvidersController@singleAction')
             ->name('depoint.manage.providers.single-action')
        ;

        Route::post('save', 'ProvidersController@save')->name('depoint.manage.providers.save');
        Route::post('delete-undelete', 'ProvidersController@deleteOrUndelete')
             ->name('depoint.manage.providers.delete-or-unndelete')
        ;
        Route::post('save-managers', 'ProvidersController@saveManagers')
             ->name('depoint.manage.providers.save-managers')
        ;
    });


    Route::group(['prefix' => 'spare-parts'], function () {

        Route::get('grid', 'SparePartsBrowseController@grid')->name('depoint.manage.spare-parts.grid');

        Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'SparePartsController@singleAction')
             ->name('depoint.manage.spare-parts.single-action')
        ;
        Route::get('mass/{action}/{option0?}/{option1?}/{option2?}', 'SparePartsController@massAction')
             ->name('depoint.manage.spare-parts.mass-action')
        ;
        Route::get('file/{file_hashid}/view', 'SparePartsController@fileItem')
             ->name('depoint.manage.spare-parts.save.file.view')
        ;

        Route::get('{tab?}', 'SparePartsBrowseController@index')->name('depoint.manage.spare-parts');

        Route::post('save', 'SparePartsController@save')->name('depoint.manage.spare-parts.save');
        Route::post('delete-undelete', 'SparePartsController@deleteOrUndelete')
             ->name('depoint.manage.spare-parts.delete-or-unndelete')
        ;
        Route::post('add-to-cart', 'SparePartsController@addToCart')
             ->name('depoint.manage.spare-parts.add-to-cart')
        ;
        Route::post('mass-add-to-cart', 'SparePartsController@addToCartMass')
             ->name('depoint.manage.spare-parts.add-to-cart-mass')
        ;

        Route::post('pending-files', 'SparePartsController@pendingFiles')
             ->name('depoint.manage.spare-parts.pending-files')
        ;

    });

    Route::group(['prefix' => 'cart'], function () {
        Route::get('/', 'CartController@index')->name('depoint.manage.cart');

        Route::post('checkout', 'CartController@checkout')->name('depoint.manage.cart.checkout');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'OrdersBrowseController@index')->name('depoint.manage.orders');
        Route::get('grid', 'OrdersBrowseController@grid')->name('depoint.manage.orders.grid');

        Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'OrdersController@singleAction')
             ->name('depoint.manage.orders.single-action')
        ;
        Route::post('save', 'OrdersController@save')->name('depoint.manage.orders.save');
        Route::post('delete', 'OrdersController@delete')->name('depoint.manage.orders.delete');
    });


    Route::group(['prefix' => 'posts/color-images'], function () {
        Route::get('{hashid}', 'PostsController@colorImages')->name('depoint.manage.posts.color-images');
        Route::get('item-preview/{color}/{file_hashid}', 'PostsController@itemPreview')
             ->name('depoint.manage.posts.color-images.item-preview')
        ;
        Route::post('save', 'PostsController@saveColorImages')->name('depoint.manage.posts.color-images.save');
    });


    /**
     * Report
     */
    Route::group(['prefix' => 'report'], function () {
        Route::get("/", "ReportController@filter");
        Route::get("/excel/modal", "ReportController@excelModal");
        Route::get("/export", "ReportController@excel");
        Route::post("/city_ajax", "ReportAjaxController@getCity");
        Route::post("/province_ajax", "ReportAjaxController@getProvince");

    });


    /**
     * Credit Payments
     */
    Route::group(['prefix' => 'credit-payments/carts', 'middleware' => 'can:carts'], function () {
        Route::get("{request_tab?}", "CreditPaymentsCartController@index")->name('depoint.credit-payments.carts');
    });


    Route::group(['prefix' => 'draw'], function () {
        Route::get('{hashid}', 'DrawController@draw')
             ->name('depoint.manage.draw')
        ;
        Route::post('prepare', 'DrawController@prepare')
             ->name('depoint.manage.draw.prepare')
        ;
        Route::get('{hashid}/winners', 'DrawController@winners')
             ->name('depoint.manage.draw.winners')
        ;
        Route::post('select', 'DrawController@select')
             ->name('depoint.manage.draw.select')
        ;
        Route::get('act/{hashid}/{view_file}', 'DrawController@singleAction')
             ->name('depoint.manage.draw.act')
        ;
        Route::get('delete/{key}', 'DrawController@delete')
             ->name('depoint.manage.draw.delete-winner')
        ;
    });

    Route::group(['prefix' => 'resume' , 'middleware' => 'can:resume' ], function () {
        Route::post('/action/{id}', 'ResumesController@action')->name('depoint.manage.action');
        Route::get('/refresh/{id}', 'ResumesController@refreshRow')->name('depoint.manage.refresh');
        Route::get('/detail/{id}', 'ResumesController@showResumeDetails')->name('depoint.manage.resume.details');
        Route::get('/{request_tab?}', 'ResumesController@index')->name('depoint.manage.resume');
    });

});


/**
 * Routes of the Front Side
 */
Route::group([
     'middleware' => 'web',
     'namespace'  => 'Modules\Depoint\Http\Controllers',
], function () {

    Route::get('/', 'HomeController@index')
         ->name('depoint.front.home')
         ->middleware('locale')
    ;

    Route::group(['prefix' => '{lang}', 'middleware' => 'locale'], function () {
        Route::get('/', 'HomeController@index')->name('depoint.front.home-with-locale');

        /*
         * News
         */
        Route::group(['prefix' => 'news'], function () {
            Route::get('/', 'NewsController@index')->name('depoint.front.news.list');
            Route::get('{hashid}/{title?}', 'NewsController@single')->name('depoint.front.news.single');
        });

        /*
         * Blog
         */
        Route::group(['prefix' => 'blog'], function () {
            Route::get('/', 'BlogController@index')->name('depoint.front.blog.list');
            Route::get('{hashid}/{title?}', 'BlogController@single')->name('depoint.front.blog.single');
        });


        /*
         * Faq
         */
        Route::group(['prefix' => 'faq'], function () {
            Route::get('new', 'FaqController@new')->name('depoint.front.faq.new');
            Route::get('{category?}', 'FaqController@index')->name('depoint.front.faq.list');
        });

        Route::group(['prefix' => 'support'], function () {
            Route::get('service/{trace_code?}', 'SupportController@sorooshan')->name('depoint.support.sorooshan');
            Route::post('service', 'SupportController@sorooshanInsert');
            Route::get('service/{state}/cities', 'SupportController@sorooshanCities')
                 ->name('depoint.support.sorooshan.cities')
            ;
            Route::get('/ticket', 'SupportController@showAddTicketPage')->name('depoint.support.ticket');
            Route::post('/ticket/save', 'SupportController@saveTicket')->name('depoint.support.ticket.save');
            Route::get('form', 'SupportController@showStatusForm')->name('depoint.support.status.form');
            Route::post('status', 'SupportController@serviceStatus')->name('depoint.support.service.status');
        });

        /*
         * Pages
         */
        Route::get('page/{slug}/{title?}', 'PageController@index')->name('depoint.front.page');

        /**
         * About
         */
        Route::get('about/{title?}', 'PageController@about')->name('depoint.front.about');


        /**
         * Comments
         */
        Route::group(['prefix' => 'comments'], function () {
            Route::post('save', 'CommentController@save')->name('depoint.comments.save');
        });


        /**
         * Contact
         */
        Route::get('contact', 'ContactController@index')->name('depoint.front.contact');


        /**
         * User Dashboard
         */
        Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
            Route::get('/', 'DashboardController@index')->name('depoint.front.dashboard');
            Route::get('edit', 'DashboardController@editProfile')
                 ->name('depoint.front.dashboard.edit-profile')
            ;
            Route::post('edit', 'DashboardController@editProfileSubmit')
                 ->name('depoint.front.dashboard.edit-profile.submit')
            ;
            Route::get('related-cities/{province}', 'DashboardController@relatedCities')
                 ->name('depoint.front.dashboard.related-cities')
            ;
            Route::get('edit-password', 'DashboardController@editPassword')
                 ->name('depoint.front.dashboard.edit-password')
            ;
            Route::post('edit-password', 'DashboardController@editPasswordSubmit')
                 ->name('depoint.front.dashboard.edit-password.submit')
            ;
            Route::get('orders', 'DashboardController@orders')
                 ->name('depoint.front.dashboard.orders')
            ;
            Route::get('codes', 'DashboardController@codes')
                 ->name('depoint.front.dashboard.codes')
            ;

            Route::group(['prefix' => 'addresses'], function () {
                Route::get('/', 'AddressController@index')->name('depoint.front.dashboard.addresses.list');
                Route::get('{hashid}', 'AddressController@single')
                     ->name('depoint.front.dashboard.addresses.single')
                ;
                Route::post('save', 'AddressController@save')
                     ->name('depoint.front.dashboard.addresses.save')
                ;
                Route::post('delete/{hashid}', 'AddressController@delete')
                     ->name('depoint.front.dashboard.addresses.delete')
                ;
            });

            Route::group(['prefix' => 'tickets'], function () {
                Route::get('/', "TicketController@index")->name('depoint.front.dashboard.tickets');
                Route::get('/single/{hashid}', "TicketController@single")
                     ->name('depoint.front.dashboard.tickets.single')
                ;
                Route::get('/new', "TicketController@showAddPage")->name('depoint.front.dashboard.tickets.new');
                Route::post('/Add', "TicketController@addNew")->name('depoint.front.dashboard.tickets.add.new');
                Route::post('/Add/reply', "TicketController@addReply")
                     ->name('depoint.front.dashboard.tickets.add.new.reply')
                ;
            });


            Route::group(['prefix' => 'provider', 'middleware' => 'provider_panel'], function () {
                Route::get('orders', 'ProvidersPanelController@orders')->name('depoint.front.provider-panel.orders');

                Route::post('order/change-status', 'ProvidersPanelController@changeOrderStatus')
                     ->name('depoint.front.provider-panel.orders.change-status')
                ;
                Route::get('orders/{hashid}', 'ProvidersPanelController@orderSingle')
                     ->name('depoint.front.provider-panel.orders.single')
                ;

                Route::get('spare-parts', 'ProvidersPanelController@spareParts')
                     ->name('depoint.front.provider-panel.spare-parts')
                ;

                Route::get('spare-parts/{hashid}', 'ProvidersPanelController@sparePartSingle')
                     ->name('depoint.front.provider-panel.spare-parts.single')
                ;

                Route::group(['prefix' => 'spare-parts-single'], function () {
                    Route::get('file-item/{hashid}', 'ProviderSparePartsController@fileItem')
                         ->name('depoint.front.provider-panel.spare-parts.single.file-item')
                    ;
                    Route::post('save-files', 'ProviderSparePartsController@saveFiles')
                         ->name('depoint.front.provider-panel.spare-parts.single.save-files')
                    ;

                    Route::get('image-view/{hashid}', 'ProviderSparePartsController@imageView')
                         ->name('depoint.front.provider-panel.spare-parts.single.image-view')
                    ;
                    Route::post('save-preview-file', 'ProviderSparePartsController@savePreviewFile')
                         ->name('depoint.front.provider-panel.spare-parts.single.save-image')
                    ;
                });
            });
        });


        /**
         * products
         */
        Route::group(['prefix' => 'products'], function () {
            Route::get('/', 'ProductListController@index')->name('depoint.front.product.list');
            Route::get('/single/{hashid}/{title?}', 'ProductListController@single')
                 ->name('depoint.front.product.single')
            ;
            Route::get('filter', "ProductFilterController@filter")
                 ->name('depoint.front.product.filter')
            ;
            Route::get('compare/{hash1}/{hash2}/{hash3?}/{hash4?}', "ProductCompareController@index")
                 ->name('depoint.front.product.compare')
            ;
        });


        /**
         * stocks
         */
        Route::group([
             'prefix'     => 'stocks',
             'middleware' => \Modules\Depoint\Http\Middleware\CheckStocksAccessMiddleware::class,
        ], function () {
            Route::get('/', 'StockController@index')->name('depoint.front.stock.list');
            Route::get('/single/{hashid}/{title?}', 'StockController@single')->name('depoint.front.stock.single');
        });


        /**
         * festivals
         */
        Route::group(['prefix' => 'festivals'], function () {
            Route::get('/', 'FestivalController@lists')->name('depoint.front.festivals.list');
            Route::get('single/{hashid}/{title?}', 'FestivalController@single')->name('depoint.front.festivals.single');
        });


        /**
         * image gallery
         */
        Route::group(['prefix' => 'images'], function () {
            Route::get('/', 'ImageGalleryController@index')->name('depoint.front.images.list');
            Route::get('single/{hashid}/{title?}', 'ImageGalleryController@single')
                 ->name('depoint.front.images.single')
            ;
        });

        /**
         * video gallery
         */
        Route::group(['prefix' => 'videos'], function () {
            Route::get('/', 'VideoGalleryController@index')->name('depoint.front.videos.list');
            Route::get('single/{hashid}/{title?}', 'VideoGalleryController@single')
                 ->name('depoint.front.videos.single')
            ;
        });

        /**
         * Agents
         */
        Route::get('agents', 'AgentsController@index')->name('depoint.front.agents');


        /**
         * Products Landing
         */
        Route::get('landing/{landing_slug}/{landing_title?}', 'LandingController@index')->name('depoint.front.landing');

        /**
         * Tags
         */
        Route::get('tag/{tag}','TagController@index')->name('depoint.front.tag');

        Route::group(['prefix' => 'cart'], function () {
            Route::get('/', 'CartController@index')->name('depoint.cart');
            Route::get('reload', 'CartController@reloadView')->name('depoint.cart.reload-view');

            Route::post('add', 'CartController@add')->name('depoint.cart.add');
            Route::post('remove', 'CartController@remove')->name('depoint.cart.remove');
            Route::post('edit-count', 'CartController@editCount')->name('depoint.cart.edit-count');
            Route::post('add-coupon-code', 'CartController@addCouponCode')->name('depoint.cart.add-coupon-code');
            Route::post('remove-coupon-code', 'CartController@removeCouponCode')
                 ->name('depoint.cart.remove-coupon-code')
            ;


            Route::group([
                 'middleware' => [
                      'auth',
                      \Modules\Depoint\Http\Middleware\MergeCartMiddleware::class,
                      'cart_nonempty',
                 ],
            ], function () {
                Route::get('review', 'PurchaseController@review')
                     ->name('depoint.cart.review')
                     ->middleware('cart_for_purchase')
                ;
                Route::get('delivery', 'PurchaseController@delivery')->name('depoint.cart.delivery');

                Route::post('set-delivery-info', 'PurchaseController@setDeliveryInfo')
                     ->name('depoint.cart.set-delivery-info')
                ;

                Route::get('payment-methods', 'PurchaseController@paymentMethods')
                     ->name('depoint.cart.payment-methods')
                     ->middleware('cart_for_purchase')
                ;
                Route::post('payment-methods', 'PurchaseController@doPayment')
                     ->name('depoint.cart.do-payment')
                     ->middleware('cart_for_purchase')
                ;
            });
        });

        Route::group(['prefix' => 'purchase', 'middleware' => 'auth'], function () {
            Route::get('payment-callback/{cart}', 'PurchaseController@paymentCallback')
                 ->name('depoint.cart.payment-callback')
                 ->middleware('own_cart')
            ;
            Route::post('retry', 'PurchaseController@repurchase')
                 ->name('depoint.cart.repurchase')
            ;

            Route::get('fire/{transaction}', 'PurchaseController@fire')
                 ->name('depoint.purchase.fire')
            ;
        });


        Route::get('cities/{province_id}', 'GeneralController@cities')->name('depoint.province-cities');


        Route::group(['prefix' => 'resume'], function () {
            Route::post('/save', 'ResumesController@save')->name('depoint.resume.save');
        });

    });

});


/**
 * Auth Routes
 */
Route::group([
     'middleware' => 'web',
     'namespace'  => 'Modules\Depoint\Http\Controllers\Auth',
],
     function () {
         Route::get('login', 'LoginController@showLoginForm')->name('login');
         Route::get('home', 'LoginController@home')->name('home');

         Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
         Route::post('register', 'RegisterController@customRegister');

         Route::get('password/reset', 'ForgotPasswordController@showTokenRequestForm')->name('password.request');
         Route::post('password/reset', 'ForgotPasswordController@request');

         Route::get('password/token/{not_fresh?}', 'ForgotPasswordController@showTokenForm')->name('password.token');
         Route::post('password/token', 'ForgotPasswordController@checkToken');

         Route::get('password/new', 'ForgotPasswordController@showResetForm')->name('password.new');
         Route::post('password/new', 'ForgotPasswordController@saveNewPassword');

     }
);
