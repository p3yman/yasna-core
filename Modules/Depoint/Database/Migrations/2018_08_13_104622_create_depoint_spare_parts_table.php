<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepointSparePartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depoint_spare_parts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->index();
            $table->string('english_name')->index();
            $table->string('code')->index()->nullable();
            $table->integer('inventory')->index();
            $table->integer('daily_use')->index()->default(0);
            $table->integer('production_days')->index();
            $table->integer('alert_days')->index();
            $table->boolean('is_ok')->index()->default(0);

            $table->string('preview_file')->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depoint_spare_parts');
    }
}
