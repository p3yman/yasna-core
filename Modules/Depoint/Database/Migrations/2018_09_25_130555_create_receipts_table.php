<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('invoice_id')->nullable()->index();
            $table->integer('user_id')->index();
            $table->string('code', 40)->index();

            $table->timestamp('purchased_at')->nullable();
            $table->integer('purchased_amount')->default(0);

            $table->string('operation_string', 20)
                  ->nullable()
                  ->comment('Reserved column to be eventually used in drawing procedure')
            ;

            $table->integer('operation_integer')->default(0)
                  ->comment('reserved column to be eventually used in drawing procedure')
            ;

            $table->boolean('is_verified')->default(false)->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
