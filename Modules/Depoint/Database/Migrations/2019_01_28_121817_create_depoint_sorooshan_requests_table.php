<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepointSorooshanRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depoint_sorooshan_requests', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->default('')->nullable();
            $table->string('state')->default('')->nullable();
            $table->string('city')->default('')->nullable();
            $table->string('tel')->default('')->nullable();
            $table->string('mobile')->default('')->nullable();
            $table->string('email')->default('')->nullable();
            $table->string('address')->default('')->nullable();
            $table->string('serial')->default('')->nullable();
            $table->string('sender')->default('')->nullable();
            $table->string('action')->default('')->nullable();
            $table->string('desc')->default('')->nullable();

            $table->string('status')->default('')->nullable();
            $table->string('sorooshan_status')->default('')->nullable();

            $table->text('warning_message')->default('')->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depoint_sorooshan_requests');
    }
}
