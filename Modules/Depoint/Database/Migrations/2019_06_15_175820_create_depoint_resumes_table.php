<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepointResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('resumes');

        Schema::create('depoint_resumes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_first')->index();
            $table->string('name_last')->index();
            $table->string('name_father');
            $table->string('code_id');
            $table->string('code_melli', 20)->index();
            $table->string('insurance_number')->nullable();
            $table->string('birth_date')->index();
            $table->string('birth_city')->index();
            $table->boolean('marital_status');
            $table->string('mobile')->index();
            $table->string('nationality1')->nullable();
            $table->string('nationality2')->nullable();
            $table->string('military_status');
            $table->string('exemption_type')->nullable();
            $table->string('email')->nullable()->index();
            $table->text('home_address')->nullable();
            $table->string('home_tel')->nullable();
            $table->string('postal_code')->nullable();
            $table->text('work_address')->nullable();
            $table->string('work_tel')->nullable();


            $table->timestamp("approved_at")->nullable();
            $table->unsignedInteger("approved_by")->default(0);

            $table->timestamp("rejected_at")->nullable();
            $table->unsignedInteger("rejected_by")->default(0);

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}

