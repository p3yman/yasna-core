<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepointCartDepointSparePartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depoint_cart_depoint_spare_part', function (Blueprint $table) {
            $table->increments('id');


            $table->unsignedInteger('depoint_cart_id')->index();
            $table->foreign('depoint_cart_id', 'cart_spare_part_cart_id_foreign')
                  ->references('id')
                  ->on('depoint_carts')
            ;

            $table->unsignedInteger('depoint_spare_part_id')->index();
            $table->foreign('depoint_spare_part_id', 'cart_spare_part_spare_part_id_foreign')
                  ->references('id')
                  ->on('depoint_spare_parts')
            ;

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depoint_cart_depoint_spare_part');
    }
}
