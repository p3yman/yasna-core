<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepointOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depoint_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->index();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
            ;

            $table->unsignedInteger('depoint_provider_id')->index();
            $table->foreign('depoint_provider_id')
                  ->references('id')
                  ->on('depoint_providers')
            ;

            $table->string('code')->nullable()->index();


            $table->timestamp('submitted_at')->nullable()->index();
            $table->timestamp('sent_at')->nullable()->index();
            $table->timestamp('verified_at')->nullable()->index();


            $table->unsignedInteger('submitted_by')->default(0)->index();
            $table->unsignedInteger('sent_by')->default(0)->index();
            $table->unsignedInteger('verified_by')->default(0)->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depoint_orders');
    }
}
