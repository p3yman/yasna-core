<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepointProductDepointSparePartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depoint_product_depoint_spare_part', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('depoint_product_id')->index();
            $table->foreign('depoint_product_id', 'product_spare_part_product_id_foreign')
                  ->references('id')
                  ->on('depoint_products');

            $table->unsignedInteger('depoint_spare_part_id')->index();
            $table->foreign('depoint_spare_part_id', 'product_spare_part_spare_part_id_foreign')
                  ->references('id')
                  ->on('depoint_spare_parts');
            
            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depoint_product_depoint_spare_part');
    }
}
