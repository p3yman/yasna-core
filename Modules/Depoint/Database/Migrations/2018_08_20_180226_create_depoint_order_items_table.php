<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepointOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depoint_order_items', function (Blueprint $table) {
            $table->increments('id');


            $table->unsignedInteger('depoint_order_id')->index();
            $table->foreign('depoint_order_id')
                  ->references('id')
                  ->on('depoint_orders')
            ;

            $table->unsignedInteger('depoint_spare_part_id')->index();
            $table->foreign('depoint_spare_part_id')
                  ->references('id')
                  ->on('depoint_spare_parts')
            ;

            $table->unsignedInteger('count')->index();
            
            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depoint_order_items');
    }
}
