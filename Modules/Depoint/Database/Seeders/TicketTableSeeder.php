<?php

namespace Modules\Depoint\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class TicketTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles['fa'] = $this->runningModule()->getTrans('seeder.depoint-ticket-type');
        $titles['en'] = $this->runningModule()->getTrans('seeder.depoint-ticket-type', [], 'en');

        $data[] = [
             'slug'   => 'main',
             'titles' => json_encode($titles),
        ];

        yasna()->seed('ticket_types', $data);
    }
}
