<?php

namespace Modules\Depoint\Database\Seeders;

use App\Models\DepointSparePart;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DummyTableSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->depointSparePartsTable();
    }



    /**
     * Insert dummy data into 'depoint_spare_parts' table
     *
     * @param int $total
     */
    public function depointSparePartsTable($total = 20)
    {
        $counter = 0;

        while ($counter < $total) {
            $counter++;

            $attend = new DepointSparePart();

            $attend->batchSave([
                 'name'            => dummy()::persianWord(),
                 'code'            => dummy()::integer(),
                 'inventory'       => dummy()::integer(),
                 'daily_use'       => dummy()::integer(),
                 'production_days' => dummy()::integer(),
                 'alert_days'      => dummy()::integer(),
                 'is_ok'           => rand(0, 1),
                 'created_at'      => rand(2010, 2018) . '-' . rand(10, 12) . '-' . rand(10, 30) . ' ' . rand(10,
                           23) . ':' . rand(10, 59) . ':' . rand(10, 59),
            ]);
        }
    }
}
