<?php

namespace Modules\Depoint\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Depoint\Services\Module\ModuleTrait;

class InputsTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * return the data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        $module_handler = static::module();

        return [
             [
                  'slug'              => 'main_files_upload_configs',
                  'title'             => $module_handler->trans('seeder.main-files-upload-configs'),
                  'order'             => '50',
                  'type'              => 'upstream',
                  'data_type'         => 'combo',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => 'filemanager::getConfigsCombo',
             ],

             [
                  'slug'              => 'preview_file_upload_configs',
                  'title'             => $module_handler->trans('seeder.preview-file-upload-configs'),
                  'order'             => '51',
                  'type'              => 'upstream',
                  'data_type'         => 'combo',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => 'filemanager::getConfigsCombo',
             ],

             [
                  'slug'              => 'fields',
                  'title'             => $module_handler->trans('seeder.fields'),
                  'order'             => '52',
                  'type'              => 'editor',
                  'data_type'         => 'textarea',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',

             ],

             [
                  'slug'              => 'rules',
                  'title'             => $module_handler->trans('seeder.rules'),
                  'order'             => '53',
                  'type'              => 'editor',
                  'data_type'         => 'textarea',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
             [
                  'slug'              => 'colors_images_upload_configs',
                  'title'             => $module_handler->trans('seeder.colors-images-upload-configs'),
                  'order'             => '54',
                  'type'              => 'upstream',
                  'data_type'         => 'combo',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => 'filemanager::getConfigsCombo',
             ],
             [
                  'slug'              => 'aparat_link',
                  'title'             => $module_handler->trans('seeder.aparat-link'),
                  'order'             => '55',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
             ],
             [
                  'slug'              => 'summarized_feature',
                  'title'             => $module_handler->trans('seeder.summarized-feature'),
                  'order'             => '56',
                  'type'              => 'editor',
                  'data_type'         => 'textarea',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
             [
                  'slug'              => 'all_access',
                  'title'             => $module_handler->trans('seeder.all_access'),
                  'order'             => '58',
                  'type'              => 'downstream',
                  'data_type'         => 'boolean',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
             [
                  'slug'              => 'manuals',
                  'title'             => $module_handler->trans('seeder.manuals'),
                  'order'             => '59',
                  'type'              => 'editor',
                  'data_type'         => 'file',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
             [
                  'slug'              => 'catalog',
                  'title'             => $module_handler->trans('seeder.catalog'),
                  'order'             => '60',
                  'type'              => 'editor',
                  'data_type'         => 'file',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
             [
                  'slug'              => 'minimal-preview',
                  'title'             => $module_handler->trans('seeder.minimal-preview'),
                  'order'             => '61',
                  'type'              => 'editor',
                  'data_type'         => 'boolean',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => $module_handler->trans('seeder.minimal-preview-hint'),
                  'meta-options'      => '',

             ],

             [
                  'slug'              => 'address',
                  'title'             => $module_handler->trans('seeder.address'),
                  'order'             => '62',
                  'type'              => 'editor',
                  'data_type'         => 'textarea',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',

             ],

             [
                  'slug'              => 'telephone',
                  'title'             => $module_handler->trans('seeder.telephone'),
                  'order'             => '63',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => 'required|phone',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',

             ],

             [
                  'slug'              => 'postal_code',
                  'title'             => $module_handler->trans('seeder.postal-code'),
                  'order'             => '64',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => 'postal_code',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',

             ],

             [
                  'slug'              => 'city',
                  'title'             => $module_handler->trans('seeder.city'),
                  'order'             => '65',
                  'type'              => 'editor',
                  'data_type'         => 'combo',
                  'css_class'         => '',
                  'validation_rules'  => 'required',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => 'depoint::getCitiesCombo',
             ],

             [
                  'slug'              => 'point_rate',
                  'title'             => $module_handler->trans('seeder.point-rate'),
                  'order'             => '66',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => 'required|numeric|min:1',
                  'purifier_rules'    => '',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',
             ],
        ];
    }



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('inputs', $this->data());
    }
}
