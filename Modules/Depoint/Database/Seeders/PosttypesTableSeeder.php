<?php

namespace Modules\Depoint\Database\Seeders;

use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Depoint\Providers\SparePartsServiceProvider;
use Modules\Depoint\Services\Module\ModuleTrait;

class PosttypesTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * read upload configs from resources with the given file name.
     *
     * @param string $file_name
     *
     * @return string
     */
    protected function readUploadConfigFromJson(string $file_name): string
    {
        $file_path    = implode(DIRECTORY_SEPARATOR, [
             module('depoint')->getPath(),
             'Resources',
             'json',
             'upload-config',
             "$file_name.json",
        ]);
        $file_content = file_get_contents($file_path);
        $json_string  = json_encode(json_decode($file_content, true));

        return $json_string;
    }



    /**
     * Generates and returns a slug for upload config to be saved in the `settings` table.
     *
     * @param string $title
     *
     * @return string
     */
    protected function uploadConfigSlug(string $title): string
    {
        $title = snake_case(camel_case($title));

        return "{$title}_upload_configs";
    }



    /**
     * Generates and returns a slug for spare parts upload config to be saved in the `settings` table.
     *
     * @param string $title
     *
     * @return string
     */
    protected function sparePartsUploadConfigSlug(string $title): string
    {
        $title = snake_case($title);

        return $this->uploadConfigSlug("depoint_spare_parts_{$title}");
    }



    /**
     * return data to be saved in the `settings` table for upload configs.
     *
     * @return array
     */
    protected function uploadConfigsData(): array
    {
        $module_handler = static::module();

        return [
             [
                  'slug'          => $this->sparePartsUploadConfigSlug('main_files'),
                  'title'         => $module_handler->trans('seeder.spare-parts-main-files'),
                  'category'      => '',
                  'order'         => '20',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('spare-parts-main-files'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->sparePartsUploadConfigSlug('preview_file'),
                  'title'         => $module_handler->trans('seeder.spare-parts-preview-file'),
                  'category'      => '',
                  'order'         => '21',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('spare-parts-preview-file'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('news-featured-image'),
                  'title'         => $module_handler->trans('seeder.news-featured-image'),
                  'category'      => '',
                  'order'         => '22',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('news-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('slider-featured-image'),
                  'title'         => $module_handler->trans('seeder.slider-featured-image'),
                  'category'      => '',
                  'order'         => '23',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('slider-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('innovation-featured-image'),
                  'title'         => $module_handler->trans('seeder.innovation-featured-image'),
                  'category'      => '',
                  'order'         => '24',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('innovation-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('blog-featured-image'),
                  'title'         => $module_handler->trans('seeder.blog-featured-image'),
                  'category'      => '',
                  'order'         => '25',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('blog-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('products-featured-image'),
                  'title'         => $module_handler->trans('seeder.products-featured-image'),
                  'category'      => '',
                  'order'         => '26',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('products-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('products-details-attachments'),
                  'title'         => $module_handler->trans('seeder.products-details-attachments'),
                  'category'      => '',
                  'order'         => '27',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('products-details-attachments'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('festivals-featured-image'),
                  'title'         => $module_handler->trans('seeder.festivals-featured-image'),
                  'category'      => '',
                  'order'         => '29',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('festivals-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('gallery-featured-image'),
                  'title'         => $module_handler->trans('seeder.gallery-featured-image'),
                  'category'      => '',
                  'order'         => '30',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('gallery-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('image-gallery-attachments'),
                  'title'         => $module_handler->trans('seeder.image-gallery-attachments'),
                  'category'      => '',
                  'order'         => '31',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('image-gallery-attachments'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('products-colors-images'),
                  'title'         => $module_handler->trans('seeder.products-colors-images'),
                  'category'      => '',
                  'order'         => '32',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('products-colors-images'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('products-attachments'),
                  'title'         => $module_handler->trans('seeder.products-attachments'),
                  'category'      => '',
                  'order'         => '33',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('products-attachments'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],

             [
                  'slug'          => $this->uploadConfigSlug('stocks-colors-images'),
                  'title'         => $module_handler->trans('seeder.stocks-colors-images'),
                  'category'      => '',
                  'order'         => '34',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('stock-colors-images'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('stocks-attachments'),
                  'title'         => $module_handler->trans('seeder.stocks-attachments'),
                  'category'      => '',
                  'order'         => '35',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('stock-attachments'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => $this->uploadConfigSlug('stocks-featured-image'),
                  'title'         => $module_handler->trans('seeder.stocks-featured-image'),
                  'category'      => '',
                  'order'         => '36',
                  'data_type'     => '',
                  'default_value' => $this->readUploadConfigFromJson('stock-featured-image'),
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
        ];
    }



    /**
     * seed upload configs.
     *
     * @return void
     */
    protected function seedUploadConfigs()
    {
        yasna()->seed('settings', $this->uploadConfigsData());
    }



    /**
     * return data to be saved or posttypes and their upload configs.
     *
     * @return array
     */
    protected function posttypesData(): array
    {
        $module_handler = static::module();

        return [
             [
                  'posttype' => [
                       'slug'           => "pages",
                       'title'          => $module_handler->trans('seeder.pages-plural'),
                       'singular_title' => $module_handler->trans('seeder.pages-singular'),
                       'template'       => "post",
                       'icon'           => "file-o",
                       'order'          => "2",
                       'locales'        => 'fa,en',
                  ],
                  'features' => [
                       'locales',
                       'text',
                       'single_view',
                       'searchable',
                       'slug',
                       'manage_sidebar',
                  ],
             ],
             [
                  'posttype'       => [
                       'slug'           => SparePartsServiceProvider::posttypeSlug(),
                       'title'          => $module_handler->trans('seeder.spare-parts'),
                       'singular_title' => $module_handler->trans('seeder.spare-part'),
                       'order'          => '15',
                       'template'       => 'special',
                       'icon'           => 'cogs',
                  ],
                  'upload-configs' => [
                       'main_files_upload_configs'   => $this->sparePartsUploadConfigSlug('main_files'),
                       'preview_file_upload_configs' => $this->sparePartsUploadConfigSlug('preview_file'),
                  ],
             ],

             [
                  'posttype'       => [
                       'slug'           => 'news',
                       'order'          => '16',
                       'title'          => $module_handler->trans('seeder.news-plural'),
                       'singular_title' => $module_handler->trans('seeder.news-singular'),
                       'template'       => 'special',
                       'icon'           => 'newspaper-o',
                       'locales'        => 'fa,en',
                  ],
                  'features'       => [
                       "text",
                       "abstract",
                       "locales",
                       "single_view",
                       "list_view",
                       "searchable",
                       "category",
                       "featured_image",
                       "manage_sidebar",
                  ],
                  'inputs'         => ['max_per_page'],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('news-featured-image'),
                  ],
             ],

             [
                  'posttype'       => [
                       'slug'           => 'slider',
                       'order'          => '17',
                       'title'          => $module_handler->trans('seeder.slider-plural'),
                       'singular_title' => $module_handler->trans('seeder.slider-singular'),
                       'template'       => 'album',
                       'icon'           => 'slideshare',
                       'locales'        => "fa,en",
                  ],
                  'features'       => [
                       "abstract",
                       "locales",
                       "title2",
                       "featured_image",
                       "manage_sidebar",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('slider-featured-image'),
                  ],
             ],
             [
                  'posttype'       => [
                       'slug'           => 'blog',
                       'order'          => '18',
                       'title'          => $module_handler->trans('seeder.blog-plural'),
                       'singular_title' => $module_handler->trans('seeder.blog-singular'),
                       'template'       => 'special',
                       'icon'           => 'align-left',
                       'locales'        => 'fa,en',
                  ],
                  'features'       => [
                       "abstract",
                       "title2",
                       "text",
                       "locales",
                       "featured_image",
                       "manage_sidebar",
                       "category",
                       "long_title",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('blog-featured-image'),
                  ],

             ]
             ,
             [
                  'posttype'       => [
                       'slug'           => 'innovation',
                       'order'          => '19',
                       'title'          => $module_handler->trans('seeder.innovation-plural'),
                       'singular_title' => $module_handler->trans('seeder.innovation-singular'),
                       'template'       => 'special',
                       'icon'           => 'braille',
                       'locales'        => "fa,en",
                  ],
                  'features'       => [
                       "abstract",
                       "locales",
                       "featured_image",
                       "manage_sidebar",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('innovation-featured-image'),
                  ],

             ],
             [
                  'posttype'       => [
                       'slug'                   => 'products',
                       'title'                  => $module_handler->trans('seeder.products-plural'),
                       'singular_title'         => $module_handler->trans('seeder.products-singular'),
                       'order'                  => '20',
                       'template'               => 'special',
                       'icon'                   => 'shopping-bag',
                       'locales'                => "fa,en",
                       "meta-title-en"          => $module_handler->trans('seeder.products-plural', [], 'en'),
                       "meta-singular_title-en" => $module_handler->trans('seeder.products-singular', [], 'en'),
                  ],
                  'features'       => [
                       "abstract",
                       "locales",
                       "shop",
                       "text",
                       "title2",
                       "attachments",
                       "featured_image",
                       "manage_sidebar",
                       "depoint_colors_image",
                       "specs",
                       "category",
                       "slug",
                       "depoint_products_details",
                  ],
                  'inputs'         => [
                       $this->uploadConfigSlug('products-colors-images'),
                       "summarized_feature",
                       "manuals",
                       "catalog",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('products-featured-image'),
                       'colors_images_upload_configs'  => $this->uploadConfigSlug('products-colors-images'),
                       'attachments_upload_configs'    => $this->uploadConfigSlug('products-attachments'),
                  ],
             ],
             [
                  'posttype'       => [
                       'slug'                   => 'products-details',
                       'title'                  => $module_handler->trans('seeder.products-details-plural'),
                       'singular_title'         => $module_handler->trans('seeder.products-details-singular'),
                       'order'                  => '21',
                       'template'               => 'special',
                       'icon'                   => 'list',
                       'locales'                => "fa,en",
                       "meta-title-en"          => $module_handler->trans('seeder.products-plural', [], 'en'),
                       "meta-singular_title-en" => $module_handler->trans('seeder.products-singular', [], 'en'),
                  ],
                  'features'       => [
                       "text",
                       "locales",
                       "attachments",
                       "manage_sidebar",
                  ],
                  'inputs'         => ['minimal-preview'],
                  'upload-configs' => [
                       'attachments_upload_configs' => $this->uploadConfigSlug('products-details-attachments'),
                  ],
             ],
             [
                  'posttype'       => [
                       'slug'                   => 'stocks',
                       'title'                  => $module_handler->trans('seeder.stocks-plural'),
                       'singular_title'         => $module_handler->trans('seeder.stocks-singular'),
                       'order'                  => '22',
                       'template'               => 'special',
                       'icon'                   => 'shopping-cart',
                       'locales'                => "fa,en",
                       "meta-title-en"          => $module_handler->trans('seeder.stocks-plural', [], 'en'),
                       "meta-singular_title-en" => $module_handler->trans('seeder.stocks-singular', [], 'en'),
                  ],
                  'features'       => [
                       "abstract",
                       "locales",
                       "shop",
                       "text",
                       "title2",
                       "attachments",
                       "featured_image",
                       "manage_sidebar",
                       "depoint_colors_image",
                       "specs",
                  ],
                  'inputs'         => [
                       $this->uploadConfigSlug('stocks-colors-images'),
                       "summarized_feature",
                       "all_access",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('stocks-featured-image'),
                       'colors_images_upload_configs'  => $this->uploadConfigSlug('stocks-colors-images'),
                       'attachments_upload_configs'    => $this->uploadConfigSlug('stocks-attachments'),
                  ],
             ]
             ,
             [
                  'posttype'       => [
                       'slug'                   => 'festivals',
                       'title'                  => $module_handler->trans('seeder.festivals-plural'),
                       'singular_title'         => $module_handler->trans('seeder.festivals-singular'),
                       'order'                  => '23',
                       'template'               => 'special',
                       'icon'                   => 'percent',
                       'locales'                => "fa,en",
                       "meta-title-en"          => $module_handler->trans('seeder.festivals-plural', [], 'en'),
                       "meta-singular_title-en" => $module_handler->trans('seeder.festivals-singular', [], 'en'),
                  ],
                  'features'       => [
                       "abstract",
                       "locales",
                       "text",
                       "featured_image",
                       "event",
                       "manage_sidebar",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('festivals-featured-image'),
                  ],
             ],
             [
                  'posttype'       => [
                       'slug'                   => 'image-gallery',
                       'title'                  => $module_handler->trans('seeder.image-gallery-plural'),
                       'singular_title'         => $module_handler->trans('seeder.image-gallery-singular'),
                       'order'                  => '24',
                       'template'               => 'album',
                       'icon'                   => 'image',
                       'locales'                => "fa,en",
                       "meta-title-en"          => $module_handler->trans('seeder.image-gallery-plural', [], 'en'),
                       "meta-singular_title-en" => $module_handler->trans('seeder.image-gallery-singular', [], 'en'),
                  ],
                  'features'       => [
                       "locales",
                       "featured_image",
                       "manage_sidebar",
                       "attachments",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('gallery-featured-image'),
                       'attachments_upload_configs'    => $this->uploadConfigSlug('image-gallery-attachments'),
                  ],
             ],
             [
                  'posttype'       => [
                       'slug'                   => 'video-gallery',
                       'title'                  => $module_handler->trans('seeder.video-gallery-plural'),
                       'singular_title'         => $module_handler->trans('seeder.video-gallery-singular'),
                       'order'                  => '25',
                       'template'               => 'album',
                       'icon'                   => 'video',
                       'locales'                => "fa,en",
                       "meta-title-en"          => $module_handler->trans('seeder.video-gallery-plural', [], 'en'),
                       "meta-singular_title-en" => $module_handler->trans('seeder.video-gallery-singular', [], 'en'),
                  ],
                  'features'       => [
                       "text",
                       "locales",
                       "featured_image",
                       "manage_sidebar",
                  ],
                  'upload-configs' => [
                       'featured_image_upload_configs' => $this->uploadConfigSlug('gallery-featured-image'),
                  ],
                  'inputs'         => ['aparat_link'],
             ],
             [
                  'posttype' => [
                       'slug' => 'pages',
                  ],
                  'features' => [
                       "locales",
                  ],
             ],

             [
                  'posttype' => [
                       'slug'           => 'commenting',
                       'order'          => '26',
                       'title'          => $module_handler->trans('seeder.commenting-plural'),
                       'singular_title' => $module_handler->trans('seeder.commenting-singular'),
                       'template'       => 'special',
                       'icon'           => 'commenting',
                       'locales'        => 'fa,en',
                  ],
                  'features' => [
                       "text",
                       "manage_sidebar",
                       "locales",
                  ],
                  'inputs'   => [
                       "fields",
                       "rules",
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'faq',
                       'order'          => '27',
                       'title'          => $module_handler->trans('seeder.faq-plural'),
                       'singular_title' => $module_handler->trans('seeder.faq-singular'),
                       'template'       => 'special',
                       'icon'           => 'question-circle',
                       'locales'        => 'fa,en',
                  ],
                  'features' => [
                       "locales",
                       "manage_sidebar",
                       "long_title",
                       "abstract",
                       "category",
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'agents',
                       'order'          => '28',
                       'title'          => $module_handler->trans('seeder.agents-plural'),
                       'singular_title' => $module_handler->trans('seeder.agents-singular'),
                       'template'       => 'special',
                       'icon'           => 'user',
                       'locales'        => 'fa,en',
                  ],
                  'features' => [
                       "locales",
                       "manage_sidebar",
                       "title",
                  ],
                  'inputs'   => [
                       "address",
                       "telephone",
                       "postal_code",
                       "city",
                  ],
             ],
             [
                  'posttype' => [
                       'slug'           => 'events',
                       'order'          => '29',
                       'title'          => $module_handler->trans('seeder.events-plural'),
                       'singular_title' => $module_handler->trans('seeder.events-singular'),
                       'template'       => 'special',
                       'icon'           => 'calendar',
                       'locales'        => 'fa,en',
                  ],
                  'features' => [
                       "locales",
                       "manage_sidebar",
                       "event",
                  ],
                  'inputs'   => [],
             ],
        ];
    }



    /**
     * Seeds inputs for the specified posttype.
     *
     * @param Posttype $posttype
     * @param array    $inputs_slugs
     */
    protected function seedPosttypeInputs(Posttype $posttype, $inputs_slugs = [])
    {
        $inputs     = model('input')->whereIn('slug', $inputs_slugs)->get();
        $inputs_ids = $inputs->pluck('id')->toArray();

        $posttype->inputs()->syncWithoutDetaching($inputs_ids);
    }



    /**
     * Seeds the upload configs for the specified posttype.
     *
     * @param Posttype $posttype
     * @param array    $upload_configs
     */
    protected function seedPosttypeUploadConfigs(Posttype $posttype, $upload_configs = [])
    {
        $inputs_slugs = array_keys($upload_configs);

        $this->seedPosttypeInputs($posttype, $inputs_slugs);

        $save_data = [];
        foreach ($upload_configs as $input_key => $setting_slug) {
            if ($posttype->$input_key) {
                continue;
            }

            $save_data[$input_key] = setting($setting_slug)->id;
        }

        $posttype->fresh()->batchSave($save_data);
    }



    /**
     * Seeds features and required inputs for the specified posttype.
     *
     * @param Posttype $posttype
     * @param array    $features
     */
    protected function seedPosttypeFeatures(Posttype $posttype, $features = [])
    {
        $posttype->attachFeatures($features);
        $posttype->attachRequiredInputs();
        $posttype->cacheFeatures();
    }



    /**
     * Seeds posttypes and their info.
     *
     * @return void
     */
    protected function seedPosttypes()
    {
        $data = $this->posttypesData();

        yasna()->seed('posttypes', array_column($data, 'posttype'));

        foreach ($data as $datum) {
            $slug           = $datum['posttype']['slug'];
            $posttype       = model('posttype', $slug);
            $features       = ($datum['features'] ?? []);
            $inputs         = ($datum['inputs'] ?? []);
            $upload_configs = ($datum['upload-configs'] ?? []);

            $this->seedPosttypeFeatures($posttype, $features);
            $this->seedPosttypeInputs($posttype, $inputs);
            $this->seedPosttypeUploadConfigs($posttype, $upload_configs);
        }
    }



    /**
     * Removes seeded posttype which are not needed.
     */
    protected function removePosttypes()
    {
        $posttypes_to_remove = ['shop', 'fruits'];

        posttype()->whereIn('slug', $posttypes_to_remove)->forceDelete();
    }



    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedUploadConfigs();
        $this->seedPosttypes();
        $this->removePosttypes();
    }
}
