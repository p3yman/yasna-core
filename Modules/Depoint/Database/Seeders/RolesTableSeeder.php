<?php

namespace Modules\Depoint\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Depoint\Providers\SpareProvidersServicePrvider;
use Modules\Depoint\Services\Module\ModuleTrait;

class RolesTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * Return the data to seed.
     *
     * @return array
     */
    protected function data()
    {
        $module_handler = static::module();

        return [
             [
                  'slug'         => SpareProvidersServicePrvider::getRoleSlug(),
                  'title'        => $module_handler->trans('seeder.provider'),
                  'plural_title' => $module_handler->trans('seeder.providers'),
                  "meta-icon"    => "building",
             ],
             [
                  'slug'         => depoint()->shop()->sellerRoleSlug(),
                  'title'        => $module_handler->trans('seeder.seller'),
                  'plural_title' => $module_handler->trans('seeder.sellers'),
                  "meta-icon"    => "truck",
             ],
             [
                  'slug'         => depoint()->shop()->stockerRoleSlug(),
                  'title'        => $module_handler->trans('seeder.stocker'),
                  'plural_title' => $module_handler->trans('seeder.stockers'),
                  "meta-icon"    => "blind",
             ],
             [
                  'slug'         => depoint()->shop()->validatedBuyerRoleSlug(),
                  'title'        => $module_handler->trans('seeder.validated-buyer'),
                  'plural_title' => $module_handler->trans('seeder.validated-buyers'),
                  "meta-icon"    => "check",
             ],
        ];
    }



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('roles', $this->data());
    }
}
