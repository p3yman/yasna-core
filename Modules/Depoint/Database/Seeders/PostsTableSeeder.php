<?php

namespace Modules\Depoint\Database\Seeders;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Depoint\Services\Module\ModuleTrait;

class PostsTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert($this->safeData());
    }



    /**
     * Generates an array to be used while normalizing array of every record before insert.
     * If normalizing be omitted, it may occurring differences between records' keys and cause SQL error.
     *
     * @param array $data
     *
     * @return array
     */
    protected function generateNormalized(array $data)
    {
        return array_fill_keys(array_keys(array_merge(...$data)), null);
    }



    /**
     * Returns the safe data to be inserted.
     *
     * @return array
     */
    protected function safeData()
    {
        $data       = $this->data();
        $normalizer = $this->generateNormalized($data);

        foreach ($data as $key => $datum) {
            $conditions = array_only($datum, ['slug', 'locale', 'type']);

            if (post()->where($conditions)->first()) {
                unset($data[$key]);
            } else {
                $data[$key] = array_normalize($datum, $normalizer);
            }
        }

        return array_values($data);
    }



    /**
     * Returns all data to be inserted.
     *
     * @return array
     */
    protected function data()
    {
        return array_merge(
             $this->aboutPosts(),
             $this->termsPosts(),
             $this->contactPosts(),
             $this->faqPosts()
        );
    }



    /**
     * Return `about` posts to be inserted.
     *
     * @return array
     */
    protected function aboutPosts()
    {
        $sisterhood = hashid(time(), 'main');
        $now        = now()->toDateTimeString();
        $common     = [
             'type'         => 'pages',
             'slug'         => 'about',
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
        ];
        $data       = [];
        $locales    = availableLocales();

        foreach ($locales as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => static::module()->trans('general.about_us', [], $locale),
            ]);
        }

        return $data;
    }



    /**
     * Return `terms` posts to be inserted.
     *
     * @return array
     */
    protected function termsPosts()
    {
        $sisterhood = hashid(time(), 'main');
        $now        = now()->toDateTimeString();
        $common     = [
             'type'         => 'pages',
             'slug'         => 'terms-and-conditions',
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
        ];
        $data       = [];
        $locales    = availableLocales();

        foreach ($locales as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => static::module()->trans('general.terms-and-conditions', [], $locale),
            ]);
        }

        return $data;
    }



    /**
     * Return commenting posts for contact form.
     *
     * @return array
     */
    protected function contactPosts()
    {
        $sisterhood  = hashid(time(), 'main');
        $now         = now()->toDateTimeString();
        $common_meta = [
             'fields' => "name*,email*,mobile*,subject*,text*",
             'rules'  => "name=>required,\r\nmobile=>phone:mobile|required,\r\nemail=>email,\r\nsubject=>required,\r\ntext=>required",
        ];
        $common      = [
             'type'         => 'commenting',
             'slug'         => 'contact',
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
             'meta'         => json_encode($common_meta),
        ];
        $data        = [];
        $locales     = availableLocales();

        foreach ($locales as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => static::module()->trans('general.contact_us', [], $locale),
            ]);
        }

        return $data;
    }



    /**
     * Return `faq` posts to be inserted.
     *
     * @return array
     */
    protected function faqPosts()
    {
        $sisterhood  = hashid(time(), 'main');
        $now         = now()->toDateTimeString();
        $common_meta = [
             'fields' => "name*,email*,mobile*,subject*,text*",
             'rules'  => "name=>required,\r\nmobile=>phone:mobile|required,\r\nemail=>email,\r\nsubject=>required,\r\ntext=>required",
        ];
        $common      = [
             'type'         => 'commenting',
             'slug'         => 'faq',
             'is_draft'     => 0,
             'sisterhood'   => $sisterhood,
             'created_at'   => $now,
             'updated_at'   => $now,
             'published_at' => $now,
             'published_by' => 1,
             'meta'         => json_encode($common_meta),
        ];
        $data        = [];
        $locales     = availableLocales();

        foreach ($locales as $locale) {
            $data[] = array_merge($common, [
                 'locale' => $locale,
                 'title'  => static::module()->trans('general.faq', [], $locale),
            ]);
        }

        return $data;
    }

}
