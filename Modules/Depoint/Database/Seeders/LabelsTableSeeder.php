<?php

namespace Modules\Depoint\Database\Seeders;


use Illuminate\Database\Seeder;
use Modules\Depoint\Services\Module\ModuleTrait;


class LabelsTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->labelsMaster();
        $this->labelsSlaves();
    }



    /**
     * Seeds parent labels
     */
    private function labelsMaster()
    {
        $posttypes    = posttype()->having('shop')->get();
        $titles_array = [
             "fa" => static::module()->trans("colors.label", [], "fa"),
             "en" => static::module()->trans("colors.label", [], "en"),
        ];

        foreach ($posttypes as $posttype) {
            ware()->defineLabel($posttype->slug . ware()::LABEL_POSTTYPE_DIVIDER . "colors", $titles_array);
        }
    }



    /**
     * Seeds children label
     */
    private function labelsSlaves()
    {
        $parent_labels = ware()->definedLabels()->where('parent_id', 0)->get();
        foreach ($parent_labels as $parent_label) {
            $this->defineColorLabels($parent_label);
        }
    }


    /**
     * Returns titles of the specified color in all available locales.
     *
     * @param string $color
     *
     * @return array
     */
    protected function getColorTitles(string $color)
    {
        $locales = availableLocales();
        $result  = [];

        foreach ($locales as $locale) {
            $result[$locale] = static::module()->trans("colors.$color", [], $locale);
        }

        return $result;
    }

    /**
     * Seeds Colors Labels
     *
     * @param $parent
     */
    public function defineColorLabels($parent)
    {
        $module_handler = static::module();
        $colors         = $module_handler->config('product-colors');

        $colors_slugs   = array_keys($colors);

        foreach ($colors_slugs as $color) {
            $posttype_slug = str_before($parent->slug, ware()::LABEL_POSTTYPE_DIVIDER);
            $slug          = $posttype_slug . ware()::LABEL_POSTTYPE_DIVIDER . $color;
            model('ware')->defineLabel(
                 $slug,
                 $this->getColorTitles($color),
                 $parent->id
            );
        }
    }
}
