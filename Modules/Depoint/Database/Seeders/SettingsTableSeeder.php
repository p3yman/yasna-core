<?php

namespace Modules\Depoint\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Depoint\Services\Module\ModuleTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns an array to be seeded in the settings table.
     *
     * @return array
     */
    protected function data()
    {
        $module_handler = static::module();
        return [
             [
                  'slug'          => 'footer_logo',
                  'title'         => $module_handler->trans('seeder.footer-logo'),
                  'category'      => 'template',
                  'order'         => '10',
                  'data_type'     => 'photo',
                  'default_value' => '',
                  'hint'          => $module_handler->trans('seeder.footer-logo-hint'),
                  'css_class'     => '',
                  'is_localized'  => '1',
             ],
             [
                  'slug'          => 'linkedin_link',
                  'title'         => $module_handler->trans('seeder.linkedin-account'),
                  'category'      => 'socials',
                  'order'         => '31',
                  'data_type'     => 'text',
                  'default_value' => 'http://linkedin.com/account',
                  'hint'          => '',
                  'css_class'     => 'ltr',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => 'aparat_link',
                  'title'         => $module_handler->trans('seeder.aparat-account'),
                  'category'      => 'socials',
                  'order'         => '32',
                  'data_type'     => 'text',
                  'default_value' => 'https://www.aparat.com/DEPOINT',
                  'hint'          => '',
                  'css_class'     => 'ltr',
                  'is_localized'  => '0',
             ],
             [
                  'slug'          => 'yasna_team_name',
                  'title'         => $module_handler->trans('seeder.yasna-team-title'),
                  'category'      => 'upstream',
                  'order'         => '33',
                  'data_type'     => 'text',
                  'default_value' => 'YasnaWeb',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '1',
             ],
             [
                  'slug'          => 'yasna_team_url',
                  'title'         => $module_handler->trans('seeder.yasna-team-url'),
                  'category'      => 'upstream',
                  'order'         => '33',
                  'data_type'     => 'text',
                  'default_value' => 'http://yasna.team',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
        ];
    }
}
