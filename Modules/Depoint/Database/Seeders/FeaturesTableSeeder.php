<?php

namespace Modules\Depoint\Database\Seeders;

use App\Models\Feature;
use Illuminate\Database\Seeder;
use Modules\Depoint\Services\Module\ModuleTrait;

class FeaturesTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedFeatures();
        $this->seedFeaturesInputs();
    }



    /**
     * Seeds features.
     */
    protected function seedFeatures()
    {
        yasna()->seed('features', array_column($this->data(), 'feature'));
    }



    /**
     * Seeds relation between features and inputs.
     */
    protected function seedFeaturesInputs()
    {
        $features = $this->data();

        foreach ($features as $feature_info) {
            $feature = model('feature', $feature_info['feature']['slug']);

            if ($feature->not_exists) {
                continue;
            }

            $this->attachFeatureInputs($feature, $feature_info['inputs']);
        }
    }



    /**
     * Attach the given inputs to the specified feature.
     *
     * @param Feature $feature
     * @param array   $inputs
     */
    protected function attachFeatureInputs(Feature $feature, array $inputs)
    {
        if (empty($inputs)) {
            return;
        }

        $feature->attachInputs($inputs);
    }



    /**
     * Returns an array to be seeded on the `features` table.
     *
     * @return array
     */
    protected function data()
    {
        $module_handler = static::module();

        return [
             [
                  'feature' => [
                       'slug'      => 'depoint_colors_image',
                       'title'     => $module_handler->trans('seeder.depoint-colors-image'),
                       'order'     => '70',
                       'icon'      => 'picture-o',
                       'post_meta' => '',
                       'meta-hint' => '',
                  ],
                  'inputs'  => [],
             ],

             [
                  'feature' => [
                       'slug'      => 'depoint_products_details',
                       'title'     => $module_handler->trans('seeder.with-products-details-posts'),
                       'order'     => '71',
                       'icon'      => 'list',
                       'post_meta' => '',
                       'meta-hint' => '',
                  ],
                  'inputs'  => [],
             ],

             [
                  'feature' => [
                       'slug'      => 'event',
                       'title'     => '',
                       'order'     => '',
                       'icon'      => '',
                       'post_meta' => '',
                       'meta-hint' => '',
                  ],
                  'inputs'  => [
                       'point_rate',
                  ],
             ],

        ];
    }
}
