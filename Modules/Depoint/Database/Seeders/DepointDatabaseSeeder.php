<?php

namespace Modules\Depoint\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DepointDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(InputsTableSeeder::class);
        $this->call(FeaturesTableSeeder::class);
        $this->call(PosttypesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(LabelsTableSeeder::class);
        $this->call(TicketTableSeeder::class);
        $this->call(AccountsTableSeeder::class);
    }
}
