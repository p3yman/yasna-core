<?php

namespace Modules\Depoint\Database\Seeders;

use App\Models\Posttype;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Depoint\Services\Module\ModuleTrait;

class CategoriesTableSeeder extends Seeder
{
    use ModuleTrait;
    /**
     * Cached Posttype Objects
     *
     * @var array
     */
    protected $cached_posttypes = [];



    /**
     * Finds and returns a Posttype object with the given slug.
     *
     * @param string $posttype_slug
     *
     * @return Posttype
     */
    protected function findPosttypeObject(string $posttype_slug)
    {
        if (!array_key_exists($posttype_slug, $this->cached_posttypes)) {
            $this->cached_posttypes[$posttype_slug] = model('posttype', $posttype_slug);
        }

        return $this->cached_posttypes[$posttype_slug];
    }



    /**
     * Returns the trans of the given key and in the given locale.
     *
     * @param string $key
     * @param string $locale
     * @param array  $replace
     *
     * @return $this|array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    protected function getTransInLocale(string $key, string $locale, array $replace = [])
    {
        $module_handler = static::module();

        return $module_handler->trans("seeder.$key", $replace, $locale);
    }



    /**
     * Returns the trans of the given key in all the site locales.
     *
     * @param string $key
     * @param array  $replace
     *
     * @return array
     */
    protected function getTransInAllLocales(string $key, array $replace = [])
    {
        $locales = depoint()->siteLocales();
        $result  = [];

        foreach ($locales as $locale) {
            $result[$locale] = $this->getTransInLocale($key, $locale, $replace);
        }

        return $result;
    }



    /**
     * Returns the basic data to be seeded.
     *
     * @return array
     */
    protected function basicData()
    {
        return [
             [
                  'posttype_id' => $this->findPosttypeObject('products')->id,
                  'slug'        => 'with-landing',
                  'locales'     => '|fa|en|',
                  'meta'        => json_encode([
                       'titles' => $this->getTransInAllLocales("with-landing"),
                  ]),
             ],
        ];
    }



    /**
     * Returns safe version of data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        $basic_data = $this->basicData();
        $data       = [];

        foreach ($basic_data as $datum) {
            $current = model('category')
                 ->where('posttype_id', $datum['posttype_id'])
                 ->where('slug', $datum['slug'])
                 ->first()
            ;

            if ($current) {
                continue;
            }

            $data[] = $datum;
        }

        return $data;
    }



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert($this->data());
    }
}
