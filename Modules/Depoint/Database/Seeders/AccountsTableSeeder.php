<?php

namespace Modules\Depoint\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Depoint\Services\Module\ModuleTrait;

class AccountsTableSeeder extends Seeder
{
    use ModuleTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('accounts', $this->data());
    }



    /**
     * Returns an array of data to be seeded.
     *
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'slug'  => depoint()->shop()->creditAccountSlug(),
                  'title' => static::module()->trans('seeder.credit-payment'),
                  'type'  => 'cash',
             ],
        ];
    }
}
