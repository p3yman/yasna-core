@extends(currentModule()->bladePath('layout.plane'))

@php $list_title = $__module->getTrans('general.tag.some-tag', ['tag' => $tag->slug]) @endphp

@php depointTemplate()->appendToPageTitle($list_title) @endphp

@section('content')
	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"title" => $list_title ,
			])

			<div class="row">

				<div class="col-sm-10 col-center">

					@include(currentModule()->bladePath('blog.list.items'))

				</div>

			</div>

		</div>

	</div>
@stop
