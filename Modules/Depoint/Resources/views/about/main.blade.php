@php
	$post_title = $model->title;

	depointTemplate()->appendToPageTitle($post_title);

@endphp

@component(currentModule()->bladePath('post.single.main'))

	<!-- Post Title -->
	@slot('header')
		@include(currentModule()->bladePath('layout.widgets.part-title'),[
			"title" => $post_title ,
		])
	@endslot

	<!-- !END Post Title -->


	<!-- Post Content -->
	{!! $model->text !!}

	<!-- !END Post Content -->

@endcomponent
