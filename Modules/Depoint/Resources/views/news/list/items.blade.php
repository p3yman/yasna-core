@if ($models->count())

	@foreach($models as $model)
		@include('depoint::layout.widgets.news-item',[
			"title" => $model->title,
			"link" => ($model->direct_url ?? '#'),
			"abstract" => $model->abstract,
			"image" => fileManager()
				->file($model->featured_image)
				->posttype($posttype)
				->config('featured_image')
				->version('240x180')
				->getUrl() ,
		])
	@endforeach

	{!! $models->render() !!}

@else

	@include(currentModule()->bladePath('layout.widgets.alert'), [
		'class' => 'alert-default',
		'text' => currentModule()->trans('general.no-item-to-show')
	])

@endif
