@extends('manage::layouts.template')

@section('content')
	@include('depoint::resume.tabs')
	@include("manage::widgets.toolbar" , [
    'title'      => trans('depoint::resume.resumes_list'),
])

	@include("manage::widgets.grid" , [
			'table_id' => "resumeTbl" ,
			'row_view' =>'depoint::resume.row' ,
			'handle'   => "counter" ,
			'headings' => [
				trans('depoint::resume.worker_info'),
				trans('depoint::resume.status'),
				trans('depoint::resume.operation'),
			],
			'models' => $model,
		]     )
@endsection

