{!!
widget("modal")
	->label(trans("depoint::resume.worker_info"))
	->target(route('depoint.manage.action', ['id' => $model->hashid], false)) 
!!}

@include("depoint::resume.resume-detail-form")

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
	'save_label' => trans("depoint::resume.approve"),
	'save_value' => 'approve',
	],
	['extra_label' => trans("depoint::resume.reject"),
	'extra_shape' => 'danger',
	'extra_value' => 'reject',
	])



</div>

