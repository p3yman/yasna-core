@include("manage::widgets.grid-actionCol" , [

    "actions" => [
    	[
      	  'id-card-o',
      	   trans('depoint::resume.resume_display'),
       	  'modal:' . route('depoint.manage.resume.details', ['id' => $model->hashid] , false),
         ],

    ],
    "button_label" => trans('depoint::resume.operation'),
    "button_size"  => 'xs',
    "button_class" => 'default'
])
