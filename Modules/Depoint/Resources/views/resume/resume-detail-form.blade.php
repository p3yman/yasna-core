<div class="modal-body">
	{!!
		widget('hidden')
		->value($model->hashid)
		->name('hashid')
	!!}

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.name')}} </div>
		<div class="col-sm-9">{{$model->full_name}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.father')}} </div>
		<div class="col-sm-9">{{$model->name_father}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.code_id')}} </div>
		<div class="col-sm-9">{{ad($model->code_id)}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.code_melli')}} </div>
		<div class="col-sm-9">{{ad($model->code_melli)}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.insurance_number')}} </div>
		<div class="col-sm-9">{{ad($model->insurance_number)}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.birth_date')}} </div>
		<div class="col-sm-9">{{ad($model->birth_date)}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.birth_city')}} </div>
		<div class="col-sm-9">{{$model->birth_city}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.marital_status')}} </div>
		<div class="col-sm-9">{{$model->marital}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.mobile')}} </div>
		<div class="col-sm-9">{{ad($model->mobile)}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.nationality1')}} </div>
		<div class="col-sm-9">{{$model->nationality1}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.nationality2')}} </div>
		<div class="col-sm-9">{{$model->nationality2}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.military_status')}} </div>
		<div class="col-sm-9">{{$model->military}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.exemption_type')}} </div>
		<div class="col-sm-9">{{$model->exemption_type}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.email')}} </div>
		<div class="col-sm-9">{{$model->email}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.home_address')}} </div>
		<div class="col-sm-9">{{$model->home_address}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.home_tel')}} </div>
		<div class="col-sm-9">{{ad($model->home_tel)}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.postal_code')}} </div>
		<div class="col-sm-9">{{ad($model->postal_code)}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.work_address')}} </div>
		<div class="col-sm-9">{{$model->work_address}}</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.work_tel')}} </div>
		<div class="col-sm-9">{{ad($model->work_tel)}}</div>
	</div>

	@if($model->educational)
		<div class="row pv5">
			<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.educational_records')}}</div>
			<div class="col-sm-9">
				<table class="table">
					<thead>
						<tr>
							<th>{{trans_safe('depoint::resume.info.edu_major_short')}}</th>
							<th>{{trans_safe('depoint::resume.info.edu_institution_short')}}</th>
							<th>{{trans_safe('depoint::resume.info.edu_type_short')}}</th>
							<th>{{trans_safe('depoint::resume.info.edu_year_short')}}</th>
							<th>{{trans_safe('depoint::resume.info.edu_average')}}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($model->educational as $item)
							<tr>
								<th>{{$item['field_orientation_educational']??''}}</th>
								<th>{{$item['name_address_educational_institution']??''}}</th>
								<th>{{ad($item['type_degree'])??''}}</th>
								<th>{{$item['year_graduation']??''}}</th>
								<th>{{$item['average']??''}}</th>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	@endif

	@if($model->workplace)
		@foreach($model->workplace as $item)
			<div class="row pv5">
				<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_records')}} </div>
					<div class="col-sm-9  pv15">
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_institution')}} </div>
								<div class="col-sm-9">{{$item['workplace_name']??''}}</div>
							</div>
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_title')}} </div>
								<div class="col-sm-9">{{$item['work_type']??''}}</div>
							</div>
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_from_date')}} </div>
								<div class="col-sm-9">{{ad($item['from_date'])??''}}</div>
							</div>
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_to_date')}} </div>
								<div class="col-sm-9">{{ad($item['to_date'])??''}}</div>
							</div>
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_last_salary')}} </div>
								<div class="col-sm-9">{{ad($item['last_salary'])??''}}</div>
							</div>
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_cause_change')}} </div>
								<div class="col-sm-9">{{$item['why_change_job']??''}}</div>
							</div>
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_insurance_history')}} </div>
								<div class="col-sm-9">{{$item['insurance_record']??''}}</div>
							</div>
							<div class="row pv5">
								<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.job_reagent')}} </div>
								<div class="col-sm-9">{{ad($item['presenter_name_phone'])??''}}</div>
							</div>
					</div>
			</div>
		@endforeach
	@endif

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.dependants')}} </div>
		<div class="col-sm-9">
			<table class="table " id="dependants_table">
				<thead>
				<tr>
					<th>{{trans_safe('depoint::resume.info.name')}}</th>
					<th>{{trans_safe('depoint::resume.info.relation')}}</th>
					<th>{{trans_safe('depoint::resume.info.age')}}</th>
					<th>{{trans_safe('depoint::resume.info.job')}}</th>
				</tr>
				</thead>
				<tbody>
				@if($model->dependants)
					@foreach($model->dependants as $item)
						<tr>
							<th>{{$item['first_name']??''}}</th>
							<th>{{$item['relation']??''}}</th>
							<th>{{ad($item['age'])??''}}</th>
							<th>{{$item['job']??''}}</th>
						</tr>
					@endforeach
				@endif
				</tbody>
			</table>
		</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.courses')}} </div>
		<div class="col-sm-9">
			<table class="table " id="specialized_table">
				<thead>
				<tr>
					<th>{{trans_safe('depoint::resume.info.course_name')}}</th>
					<th>{{trans_safe('depoint::resume.info.course_duration')}}</th>
					<th>{{trans_safe('depoint::resume.info.course_institution')}}</th>
					<th>{{trans_safe('depoint::resume.info.course_year')}}</th>
				</tr>
				</thead>
				<tbody>
					@if($model->specialized)
						@foreach($model->specialized as $item)
							<tr>
								<th>{{$item['course_name']??''}}</th>
								<th>{{ad($item['course_duration'])??''}}</th>
								<th>{{$item['name_educational_institution']??''}}</th>
								<th>{{ad($item['course_year_graduation'])??''}}</th>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.lang_info')}} </div>
		<div class="col-sm-9">
			<table class="table ">
				<thead>
					<tr>
						<th>{{trans_safe('depoint::resume.info.lang_name')}}</th>
						<th>{{trans_safe('depoint::resume.info.lang_speaking')}}</th>
						<th>{{trans_safe('depoint::resume.info.lang_listening')}}</th>
						<th>{{trans_safe('depoint::resume.info.lang_writing')}}</th>
						<th>{{trans_safe('depoint::resume.info.lang_reading')}}</th>
					</tr>
				</thead>
				<tbody>
					@if($model->knowing)
						@foreach($model->knowing as $item)
							<tr>
								<th>{{$item['foreign_lang']??''}}</th>
								<th>{{$item['foreign_speaking']??''}}</th>
								<th>{{$item['foreign_listening']??''}}</th>
								<th>{{$item['foreign_writing']??''}}</th>
								<th>{{$item['foreign_reading']??''}}</th>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.software')}} </div>
		<div class="col-sm-9">
			<table class="table ">
				<thead>
					<tr>
						<th>{{trans_safe('depoint::resume.info.software_title')}}</th>
						<th>{{trans_safe('depoint::resume.info.software_level')}}</th>
					</tr>
				</thead>
				<tbody>
					@if($model->software)
						@foreach($model->software as $item)
							<tr>
								<th>{{$item['software']??''}}</th>
								<th>{{$item['level_knowing']??''}}</th>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

	<div class="row pv5">
		<div class="col-sm-3 text-right">{{trans_safe('depoint::resume.info.relative')}} </div>
		<div class="col-sm-9">
			<table class="table ">
				<thead>
					<tr>
						<th>{{trans_safe('depoint::resume.info.name')}}</th>
						<th>{{trans_safe('depoint::resume.info.relation')}}</th>
						<th>{{trans_safe('depoint::resume.info.tel')}}</th>
					</tr>
				</thead>
				<tbody>
					@if($model->relatives)
						@foreach($model->relatives as $item)
							<tr>
								<th>{{$item['full-name']??''}}</th>
								<th>{{$item['relation']??''}}</th>
								<th>{{ad($item['phone'])??''}}</th>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

</div>
