@include('manage::layouts.sidebar-link' , [
    'icon'       => 'list',
    'caption'    => trans('depoint::resume.resume'),
    'link'       => 'depoint/resume',
    'condition'  => user()->as('admin')->can('resume.manage'),
])
