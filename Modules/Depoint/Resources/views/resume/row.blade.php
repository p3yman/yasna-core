@include('manage::widgets.grid-row-handle', [
	'handle'      => "counter" ,
	'refresh_url' => '',
])

<td>

	@include('manage::widgets.grid-tiny', [
		'text'  	=> $model->full_name,
		'size'  => '14',
		'locale'    => "en" ,
		'link'      => 'modal:' . route('depoint.manage.resume.details', ['id' => $model->hashid], false) ,
		'color'     => "primary" ,
		"icon"	    => "id-card-o",
	])

	@include("manage::widgets.grid-text" , [
		'text'  => $model->mobile ,
		'icon'  => "mobile",
		'size'  => '11',
	])

</td>

<td>
	{{$model->resume_status_label}}
</td>
@include('depoint::resume.operations')
