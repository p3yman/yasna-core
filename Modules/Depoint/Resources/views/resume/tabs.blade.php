@include("manage::widgets.tabs", [
    'current' => $request_tab,
    'tabs' => [
        [
            'caption'     => trans('depoint::resume.statuses.pending'),
            'url'         => "pending",
        ],
        [
            'caption'     => trans('depoint::resume.statuses.approved'),
            'url'         => "approved",
        ],
        [
            'caption'     => trans('depoint::resume.statuses.rejected'),
            'url'         => "rejected",
        ],
    ],
])

