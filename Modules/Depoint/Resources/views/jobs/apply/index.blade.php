@extends(currentModule()->bladePath('layout.plane'))

@section('content')
	<div class="container" >
		@if (session('message'))
			<div class="alert alert-success">
				{{ session('message') }}
			</div>
		@endif


			@if($errors)
				@foreach ($errors->all() as $error)
					<div class="alert alert-danger">{{ $error }}</div>
				@endforeach
			@endif


        <form class="form-required"
              method="post"
              action="{{route('depoint.resume.save', ['lang' => getLocale()])}}"
              class="js"
              id="cvForm">

            {{csrf_field()}}
			<div class="panel">
				<article>
					<div class="mb20">
						<h4 class="mb20">{{ trans('depoint::form.cv-form-general-title') }}</h4>
					</div>
					<div class="row">
						<div class="col-md-4">
							@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
								"name"=>"name_first",
								"class"=>"form-required",
								"label" => currentModule()->trans('form.cv-form-first-name'),
								"required" => true
							])
						</div>
						<div class="col-md-4">
							@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
								"name"=>"name_last",
								"label" => currentModule()->trans('form.cv-form-last-name'),
								"required" => true

							])
						</div>
						<div class="col-md-4">
							@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
								"name"=>"name_father",
								"label" => currentModule()->trans('form.cv-form-father-name'),
								"required" => true

							])
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"code_id",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-code-id'),
								"required" => true
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"code_melli",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-code-melli'),
								"required" => true
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"insurance_number",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-code-insurance')
							])
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"id" => "birth_date",
								"name" => "birth_date",
								"type"=>"text",
								"label" => currentModule()-> trans('form.cv-form-birth-date'),
								"required" => true
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name" => "birth_city",
								"label" => currentModule()-> trans('form.cv-form-birth-location'),
								"required" => true
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.select'), [
								"name" => "marital_status",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-marriage-situation'),
								"required" => true,
								"options" => [
									[
										"value" => "",
										"title" => currentModule()-> trans('form.cv-form-selected'),
									],
									[
										"value" => 1,
										"title" => currentModule()-> trans('form.cv-form-single'),
									],
									[
										"value" => 2,
										"title" => currentModule()-> trans('form.cv-form-married'),
									]
								],
							])
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"nationality1",
								"label" => currentModule()-> trans('form.cv-form-nationality 1')
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"nationality2",
								"label" => currentModule()-> trans('form.cv-form-nationality 2')
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"email",
								"label" => currentModule()-> trans('form.cv-form-email')
							])
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.select'), [
							"name" => "military_status",
							"label" => currentModule()-> trans('form.cv-form-military-service'),
							"required" => true,
							"options" => [
								[
									"value" => "",
									"title" => currentModule()-> trans('form.cv-form-selected'),
								],
								[
									"value" => 1,
									"title" => currentModule()-> trans('form.cv-form-did-not-do'),
								],
								[
									"value" => 2,
									"title" => currentModule()-> trans('form.cv-form-end-service'),
								],
								[
									"value" => 3,
									"title" => currentModule()-> trans('form.cv-form-exempt'),
								]
							],
						])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"exemption_type",
								"label" => currentModule()-> trans('form.cv-form-type-exemption')
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"mobile",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-mobile'),
								"required" => true
							])
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"home_address",
								"label" => currentModule()-> trans('form.cv-form-home-address'),
								"required" => true
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"home_tel",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-home-phone')
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"postal_code",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-postal-code')
							])
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"work_address",
								"label" => currentModule()-> trans('form.cv-form-work-address')
							])
						</div>
						<div class="col-md-4">
							@include(currentModule() -> bladePath('layout.widgets.form.input-text'),[
								"name"=>"work_tel",
								"type"=>"number",
								"label" => currentModule()-> trans('form.cv-form-work-phone')
							])
						</div>
						<div class="col-md-4">
						</div>
					</div>
				</article>
			</div>
			<div class="panel">
				<article>
					<div id="depends-form">
						<div class="mb20">
							<h4>{{ trans('depoint::form.cv-form-dependants-title') }}</h4>
						</div>
						<div class="row">
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=first_name" ,
									"label" => currentModule()->trans('form.cv-form-first-name'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=relation" ,
									"label" => currentModule()->trans('form.cv-form-relation'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=age" ,
									"label" => currentModule()->trans('form.cv-form-age'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=job" ,
									"label" => currentModule()->trans('form.cv-form-job'),

								])
							</div>
						</div>

						<div class="ta-l mb20">
							<button type="button" class="button red" id="btn-add-depends" data-target="dependants_table">
								{{ currentModule()->trans('form.cv-form-add-column') }}
							</button>
						</div>
					</div>
					<span class="icon-cancel color-red"></span>
					<table class="table bordered" id="dependants_table">
						<thead>
						<tr>
							<th>{{currentModule()->trans('form.cv-form-first-name')}}</th>
							<th>{{currentModule()->trans('form.cv-form-relation')}}</th>
							<th>{{currentModule()->trans('form.cv-form-age')}}</th>
							<th>{{currentModule()->trans('form.cv-form-job')}}</th>
							<th>{{currentModule()->trans('form.cv-form-delete-column')}}</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</article>
			</div>
			<div class="panel">
				<article>
					<div id="educational-form">
						<div class="mb20">
							<h4>{{ trans('depoint::form.cv-form-education-records-title') }}</h4>
						</div>
						<div class="row">
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=field_orientation_educational" ,
									"label" => currentModule()->trans('form.cv-form-field-orientation-educational'),
								])
							</div>
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=name_address_educational_institution" ,
									"label" => currentModule()->trans('form.cv-form-name-address-educational-institution'),

								])
							</div>
							<div class="col-md-4">
								@include(currentModule() -> bladePath('layout.widgets.form.select'), [
									"attr" => "data-name=type_degree" ,
									"label" => currentModule()-> trans('form.cv-form-type-degree'),
									"options" => [
										[
											"value" => "",
											"title" => currentModule()-> trans('form.cv-form-selected'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-diploma'),
											"title" => currentModule()-> trans('form.cv-form-diploma'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-associate-degree'),
											"title" => currentModule()-> trans('form.cv-form-associate-degree'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-bachelor'),
											"title" => currentModule()-> trans('form.cv-form-bachelor'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-master'),
											"title" => currentModule()-> trans('form.cv-form-master'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-PHD'),
											"title" => currentModule()-> trans('form.cv-form-PHD'),
										]
									],
								])
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"id" => "year_graduation",
									"class" => "year_date",
									"attr" => "data-name=year_graduation" ,
									"label" => currentModule()->trans('form.cv-form-year-graduation'),

								])
							</div>
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=average" ,
									"type" => "number",
									"label" => currentModule()->trans('form.cv-form-average'),

								])
							</div>
						</div>

						<div class="ta-l mb20">
							<button class="button red" id="btn-add-educational" data-target="educational_table">
								{{ currentModule()->trans('form.cv-form-add-column') }}
							</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table bordered" id="educational_table">
								<thead>
								<tr>
									<th>{{currentModule()->trans('form.cv-form-field-orientation-educational')}}</th>
									<th>{{currentModule()->trans('form.cv-form-name-address-educational-institution')}}</th>
									<th>{{currentModule()->trans('form.cv-form-type-degree')}}</th>
									<th>{{currentModule()->trans('form.cv-form-year-graduation')}}</th>
									<th>{{currentModule()->trans('form.cv-form-average')}}</th>
									<th>{{currentModule()->trans('form.cv-form-delete-column')}}</th>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</article>
			</div>
			<div class="panel">
				<article>
					<div id="workplace-form">
						<div class="mb20">
							<h4 class="mb20">{{ trans('depoint::form.cv-form-resume-title') }}</h4>
						</div>
						<div class="row">
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=workplace_name" ,
									"label" => currentModule()->trans('form.cv-form-workplace-name'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=work_type" ,
									"label" => currentModule()->trans('form.cv-form-work-type'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"id" => "from_date" ,
									"class" => "range_date" ,
									"attr" => "data-name=from_date" ,
									"label" => currentModule()->trans('form.cv-form-from-date'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"id" => "to_date" ,
									"class" => "range_date" ,
									"attr" => "data-name=to_date" ,
									"label" => currentModule()->trans('form.cv-form-to-date'),

								])
							</div>
						</div>

						<div class="row">
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
								   "attr" => "data-name=last_salary" ,
								   "label" => currentModule()->trans('form.cv-form-last-salary'),

							   ])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=why_change_job" ,
									"label" => currentModule()->trans('form.cv-form-why-change-job'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule() -> bladePath('layout.widgets.form.select'), [
									"attr" => "data-name=insurance_record" ,
									"label" => currentModule()-> trans('form.cv-form-insurance-record'),
									"options" => [
										[
											"value" => "",
											"title" => currentModule()-> trans('form.cv-form-selected'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-yes'),
											"title" => currentModule()-> trans('form.cv-form-yes'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-no'),
											"title" => currentModule()-> trans('form.cv-form-no'),
										]
									],
								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=presenter_name_phone" ,
									"label" => currentModule()->trans('form.cv-form-presenter-name-phone'),

								])
							</div>
						</div>

						<div class="ta-l mb20">
							<button class="button red" id="btn-add-workplace" data-target="workplace_table">
								{{ currentModule()->trans('form.cv-form-add-column') }}
							</button>
						</div>
					</div>

					<table class="table bordered" id="workplace_table">
						<thead>
						<tr>
							<th>{{currentModule()->trans('form.cv-form-workplace-name')}}</th>
							<th>{{currentModule()->trans('form.cv-form-work-type')}}</th>
							<th>{{currentModule()->trans('form.cv-form-from-date')}}</th>
							<th>{{currentModule()->trans('form.cv-form-to-date')}}</th>
							<th>{{currentModule()->trans('form.cv-form-last-salary')}}</th>
							<th>{{currentModule()->trans('form.cv-form-why-change-job')}}</th>
							<th>{{currentModule()->trans('form.cv-form-insurance-record')}}</th>
							<th>{{currentModule()->trans('form.cv-form-presenter-name-phone')}}</th>
							<th>{{currentModule()->trans('form.cv-form-delete-column')}}</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</article>

			</div>
			<div class="panel">
				<article>
					<div id="specialized-form">
						<div class="mb20">
							<h4 class="mb20">{{ trans('depoint::form.cv-form-specialized-courses-title') }}</h4>
						</div>
						<div class="row">
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=course_name" ,
									"label" => currentModule()->trans('form.cv-form-course-name'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=course_duration" ,
									"label" => currentModule()->trans('form.cv-form-course-duration'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=name_educational_institution" ,
									"label" => currentModule()->trans('form.cv-form-name-educational-institution'),

								])
							</div>
							<div class="col-md-3">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"class" => "year_date" ,
									"attr" => "data-name=course_year_graduation" ,
									"label" => currentModule()->trans('form.cv-form-year-graduation'),

								])
							</div>
						</div>


						<div class="ta-l mb20">
							<button class="button red" id="btn-add-specialized" data-target="specialized_table">
								{{ currentModule()->trans('form.cv-form-add-column') }}
							</button>
						</div>
					</div>
					<table class="table bordered" id="specialized_table">
						<thead>
						<tr>
							<th>{{currentModule()->trans('form.cv-form-course-name')}}</th>
							<th>{{currentModule()->trans('form.cv-form-course-duration')}}</th>
							<th>{{currentModule()->trans('form.cv-form-name-educational-institution')}}</th>
							<th>{{currentModule()->trans('form.cv-form-year-graduation')}}</th>
							<th>{{currentModule()->trans('form.cv-form-delete-column')}}</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</article>
			</div>
			<div class="panel">
				<article>
					<div id="knowing-form">
						<div class="mb20">
							<h4 class="mb20">{{ trans('depoint::form.cv-form-knowing-foreign-lang-title') }}</h4>
						</div>
							<div class="row">
								<div class="col-md-2">
									@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
										"attr" => "data-name=foreign_lang" ,
										"label" => currentModule()->trans('form.cv-form-foreign-lang'),

									])
								</div>
								<div class="col-md-2">
									@include(currentModule() -> bladePath('layout.widgets.form.select'), [
										"attr" => "data-name=foreign_speaking" ,
										"label" => currentModule()-> trans('form.cv-form-foreign-speaking'),
										"options" => [
											[
												"value" => "",
												"title" => currentModule()-> trans('form.cv-form-selected'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
											],
										],
									])
								</div>
								<div class="col-md-2">
									@include(currentModule() -> bladePath('layout.widgets.form.select'), [
										"attr" => "data-name=foreign_listening" ,
										"label" => currentModule()-> trans('form.cv-form-foreign-listening'),
										"options" => [
											[
												"value" => "",
												"title" => currentModule()-> trans('form.cv-form-selected'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
											],
										],
									])
								</div>
								<div class="col-md-2">
									@include(currentModule() -> bladePath('layout.widgets.form.select'), [
										"attr" => "data-name=foreign_writing" ,
										"label" => currentModule()-> trans('form.cv-form-foreign-writing'),
										"options" => [
											[
												"value" => "",
												"title" => currentModule()-> trans('form.cv-form-selected'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
											],
										],
									])
								</div>
								<div class="col-md-2">
									@include(currentModule() -> bladePath('layout.widgets.form.select'), [
										"attr" => "data-name=foreign_reading" ,
										"label" => currentModule()-> trans('form.cv-form-foreign-reading'),
										"options" => [
											[
												"value" => "",
												"title" => currentModule()-> trans('form.cv-form-selected'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
											],
											[
												"value" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
												"title" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
											],
										],
									])
								</div>
							</div>
							<div class="ta-l mb20">
								<button class="button red" id="btn-add-knowing" data-target="knowing_table">
									{{ currentModule()->trans('form.cv-form-add-column') }}
								</button>
							</div>
					</div>
					<div class="row">
							<div class="col-md-12" id="knowing_table">
								<table class="table bordered">
									<thead>
									<tr>
										<th>{{currentModule()->trans('form.cv-form-foreign-lang')}}</th>
										<th>{{currentModule()->trans('form.cv-form-foreign-speaking')}}</th>
										<th>{{currentModule()->trans('form.cv-form-foreign-listening')}}</th>
										<th>{{currentModule()->trans('form.cv-form-foreign-writing')}}</th>
										<th>{{currentModule()->trans('form.cv-form-foreign-reading')}}</th>
										<th>{{currentModule()->trans('form.cv-form-delete-column')}}</th>
									</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
				</article>
			</div>
			<div class="panel">
				<article>
					<div id="software-form">
						<div class="mb20">
							<h4>{{ trans('depoint::form.cv-form-knowing-computer-software-title') }}</h4>
						</div>
						<div class="row">
							<div class="col-md-6">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"attr" => "data-name=software" ,
									"label" => currentModule()->trans('form.cv-form-software'),

								])
							</div>
							<div class="col-md-6">
								@include(currentModule() -> bladePath('layout.widgets.form.select'), [
									"attr" => "data-name=level_knowing" ,
									"label" => currentModule()-> trans('form.cv-form-level-knowing'),
									"options" => [
										[
											"value" => "",
											"title" => currentModule()-> trans('form.cv-form-selected'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
											"title" => currentModule()-> trans('form.cv-form-foreign-lang-excellent'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
											"title" => currentModule()-> trans('form.cv-form-foreign-lang-medium'),
										],
										[
											"value" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
											"title" => currentModule()-> trans('form.cv-form-foreign-lang-weak'),
										],
									],
								])
							</div>
						</div>

						<div class="ta-l mb20">
							<button class="button red" id="btn-add-software" data-target="software_table">
								{{ currentModule()->trans('form.cv-form-add-column') }}
							</button>
						</div>
					</div>
					<div class="row">
							<div class="col-md-12" id="software_table">
								<table class="table bordered">
									<thead>
									<tr>
										<th>{{currentModule()->trans('form.cv-form-software')}}</th>
										<th>{{currentModule()->trans('form.cv-form-level-knowing')}}</th>
										<th>{{currentModule()->trans('form.cv-form-delete-column')}}</th>
									</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
				</article>
			</div>
			<div class="panel">
				<article>
					<div class="mb20">
						<h4 class="mb20">{{ trans('depoint::form.cv-form-necessary-contacts-title') }}</h4>
					</div>
						<div class="row">
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"name"=>"relatives[0][full-name]",
									"label" => currentModule()->trans('form.cv-form-full-name'),
								])
							</div>
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"name"=>"relatives[0][relation]",
									"label" => currentModule()->trans('form.cv-form-relation'),

								])
							</div>
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"name"=>"relatives[0][phone]",
									"label" => currentModule()->trans('form.cv-form-mobile'),

								])
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"name"=>"relatives[1][full-name]",
									"label" => currentModule()->trans('form.cv-form-full-name'),
								])
							</div>
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"name"=>"relatives[1][relation]",
									"label" => currentModule()->trans('form.cv-form-relation'),

								])
							</div>
							<div class="col-md-4">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"name"=>"relatives[1][phone]",
									"label" => currentModule()->trans('form.cv-form-mobile'),

								])
							</div>
						</div>
				</article>
			</div>
			<div class="panel">
				<article class="ta-l">
                    <a href="{{ route('jobs.apply') }}" class="button">
                        {{ currentModule()->trans('form.cv-form-new') }}
                    </a>
                    <a href="#" class="button blue loader" id="submitJobForm">
                        {{ currentModule()->trans('form.cv-form-save') }}
                    </a>
				</article>
			</div>

            <div class="alert" style="display: none;"></div>

		</form>
	</div>

@endsection
