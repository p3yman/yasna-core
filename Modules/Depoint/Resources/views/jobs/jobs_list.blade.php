<div class="jobs-list">
	<h3 class="title color-gray">
		{{ $list_title }}
	</h3>
	@foreach($jobs as $job)
		<div class="item">
			<a href="{{ $job['link'] }}">
				{{ $job['title'] }}
			</a>
		</div>
	@endforeach
</div>
