@extends('depoint::layout.plane')

@php
	$job_group = "طراح و برنامه‌نویس";
	$job_opportunities_link = "#";
	$job_title = "توسعه دهنده‌ی UI";
	$description = '<p>به یک نفر برنامه نویس ارشد با توانایی و درجه مهارت بسیار بالا جهت تکمیل تیم و همکاری تمام وقت در شهر تهران (حوالی ظفر) نیازمدیم.</p>

						<br>

						<h3>الزامات و مهارت ها</h3>
						<ul>
							<li>مسلط به React,React-native</li>
							<li>مسلط به ES6 و ES5</li>
							<li>                            آشنا به نوشتن کد های Cross Platform</li>
							<li>آشنا به زبان های Java یا Swift</li>
							<li>                            آشنا به پیاده سازی طرح به صورت Pixel Perfect</li>
							<li>                            توانایی نوشتن تست نرم افزار Unit Test</li>
							<li>                            توانایی نوشتن کاستوم سرویس ها در React-native</li>
							<li>اشنا به مفاهیم UI &UX</li>
							<li>                            تجربه توسعه اپلیکیشن به صورت native مزیت میباشد</li>
							<li>توانایی نحوه داکیومنت کردن کد</li>
							<li>                            آشنا به Agile‌ و تکنیک های سریع توسعه نرم افزار</li>
							<li>مسلط به Git</li>
							<li>توانای و تجربه کار تیمی</li>
							<li>تسلط کافی به زبان انگلیسی</li>
						</ul>

						<br>';
@endphp

@section('content')
	<div class="container">

		<div class="page">

			<div class="part-title" data-aos="fade-down">
				<div class="subtitle">
					<a href="{{ $job_opportunities_link }}">
						{{ trans('depoint::general.job_opportunities') . ":" }}
					</a>
					{{ $job_group }}
				</div>
				<div class="title">
					{{ $job_title }}
				</div>
			</div>

			<div class="row">

				<div class="col-sm-10 col-center">

					<div class="page-content" data-aos="fade-down" data-aos-delay="500">

						<h3>
							{{ trans('depoint::general.job_description') }}
						</h3>

						{!! $description !!}

						<div class="ta-c mb50">
							<a href="#" class="button green lg">
								<span class="icon-send"></span>
								{{ trans('depoint::general.send_resume') }}
							</a>
						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
@stop
