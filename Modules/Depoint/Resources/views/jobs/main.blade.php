@extends('depoint::layout.plane')

@php
	$title = " متنی برای معرفی شرکت و محیط آن ";
	$desc = '<p>لورم ایپسوم یا طرح‌نما (به انگلیسی: <a href="#">Lorem ipsum</a>) به متنی آزمایشی و بی‌معنی در
							<a href="#">صنعت چاپ</a>، صفحه‌آرایی و طراحی گرافیک گفته می‌شود. طراح گرافیک از این متن به عنوان عنصری از ترکیب بندی برای پر کردن صفحه و ارایه اولیه شکل ظاهری و کلی طرح سفارش گرفته شده استفاده می نماید، تا از نظر گرافیکی نشانگر چگونگی نوع و اندازه فونت و ظاهر متن باشد. معمولا طراحان گرافیک برای صفحه‌آرایی، نخست از متن‌های آزمایشی و بی‌معنی استفاده می‌کنند تا صرفا به مشتری یا صاحب کار خود نشان دهند که صفحه طراحی یا صفحه بندی شده بعد از اینکه متن در آن قرار گیرد چگونه به نظر می‌رسد و قلم‌ها و اندازه‌بندی‌ها چگونه در نظر گرفته شده‌است. از آنجایی که طراحان عموما نویسنده متن نیستند و وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی و صفحه‌بندی را به پایان برند.</p>' ;

	$job_list = [
		[
			"list_title" => "طراح و برنامه‌نویس" , 
			"jobs" => [
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
			] , 
		],
		[
			"list_title" => "طراح و برنامه‌نویس" ,
			"jobs" => [
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
			] ,
		],
		[
			"list_title" => "طراح و برنامه‌نویس" ,
			"jobs" => [
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
			] ,
		],
		[
			"list_title" => "طراح و برنامه‌نویس" ,
			"jobs" => [
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
			] ,
		],
		[
			"list_title" => "طراح و برنامه‌نویس" ,
			"jobs" => [
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
				[
					"title" => "برنامه‌نویس Full-Stack" ,
					"link" => "#" ,
				],
				[
					"title" => "مدیر پروژه" ,
					"link" => "#" ,
				],
			] ,
		],
	];
@endphp

@section('content')

	<div class="container">

		<div class="page">
			@include('depoint::layout.widgets.part-title',[
				"title" => trans('depoint::general.job_opportunities') ,
			])

			<div class="row">

				<div class="col-sm-10 col-center">

					<div class="page-content" data-aos="fade-down" data-aos-delay="500">
						<h3>{{ $title }}</h3>

						{!! $desc !!}

						<img src="{{ Module::asset('depoint:images/sample/feature-b-1.jpg') }}">

						<h2 class="mt50">
							{{ trans('depoint::general.available_jobs') }}
						</h2>

						@foreach($job_list as $list)

							@if($loop->index % 2 === 0)
								@section('first_col')
									@include('depoint::jobs.jobs_list',[
										"list_title" => $list['list_title'] ,
										"jobs" => $list['jobs'] ,
									])
								@append
							@else
								@section('second_col')
									@include('depoint::jobs.jobs_list',[
										"list_title" => $list['list_title'] ,
										"jobs" => $list['jobs'] ,
									])
								@append
							@endif
						@endforeach

						<div class="row">

							<div class="col-sm-6">
								@yield('first_col')
							</div>

							<div class="col-sm-6">
								@yield('second_col')
							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

@stop
