@extends('depoint::layout.plane')

@php
	$states = [
		[
			"id" => "province-Tehran" ,
			"title" => "استان تهران" ,
			"agents" => [
				[
					"city" => "تهران" ,
					"manager" => "رضا حسین‌زاده" ,
					"address" => "میدان امام حسین، بلوار بهارستان، ساختمان بهار، طبقه سوم" ,
					"postal_code" => "72782901" ,
					"telephone" => "021-63697290" ,
				],
				[
					"city" => "تهران" ,
					"manager" => "رضا حسین‌زاده" ,
					"address" => "میدان امام حسین، بلوار بهارستان، ساختمان بهار، طبقه سوم" ,
					"postal_code" => "72782901" ,
					"telephone" => "021-63697290" ,
				],
				[
					"city" => "ملارد" ,
					"manager" => "رضا حسین‌زاده" ,
					"address" => "میدان امام حسین، بلوار بهارستان، ساختمان بهار، طبقه سوم" ,
					"postal_code" => "72782901" ,
					"telephone" => "021-63697290" ,
				],
				[
					"city" => "اسلام‌شهر" ,
					"manager" => "رضا حسین‌زاده" ,
					"address" => "میدان امام حسین، بلوار بهارستان، ساختمان بهار، طبقه سوم" ,
					"postal_code" => "72782901" ,
					"telephone" => "021-63697290" ,
				],
			] ,
		],
		[
			"id" => "province-Maazandaraan" ,
			"title" => "استان مازندران" ,
			"agents" => [
				[
					"city" => "آمل" ,
					"manager" => "رضا حسین‌زاده" ,
					"address" => "میدان امام حسین، بلوار بهارستان، ساختمان بهار، طبقه سوم" ,
					"postal_code" => "72782901" ,
					"telephone" => "021-63697290" ,
				],
				[
					"city" => "بابل" ,
					"manager" => "رضا حسین‌زاده" ,
					"address" => "میدان امام حسین، بلوار بهارستان، ساختمان بهار، طبقه سوم" ,
					"postal_code" => "72782901" ,
					"telephone" => "021-63697290" ,
				],
				[
					"city" => "بابلسر" ,
					"manager" => "رضا حسین‌زاده" ,
					"address" => "میدان امام حسین، بلوار بهارستان، ساختمان بهار، طبقه سوم" ,
					"postal_code" => "72782901" ,
					"telephone" => "021-63697290" ,
				]
			] ,
		],
	];
@endphp

@section('content')
	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"subtitle" => trans('depoint::general.introduction') ,
				"title" => trans('depoint::general.where-to-buy') ,
			])

			<div id="where">

				<div class="row">

					<div class="col-sm-5">

						<div id="iran-map">
							@include('depoint::where-to-buy.iran-map')
							<div class="province-title"></div>
						</div>

					</div>

					<div class="col-sm-7">

						<div id="places">

							@foreach($states as $state)
								@include('depoint::where-to-buy.state-table',[
									"id" => $state['id'] ,
									"title" => $state['title'] ,
									"agents" => $state['agents'] ,
								])
							@endforeach

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
@stop
