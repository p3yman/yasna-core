<table class="table bordered province-details" id="{{ $id }}" @if($id !== "province-Tehran") style="display: none;" @endif>
	<thead>
	<tr>
		<th colspan="100%" class="province-head">
			{{ $title }}
		</th>
	</tr>
	<tr>
		<th>
			{{ trans('depoint::general.table.city') }}
		</th>
		<th>
			{{ trans('depoint::general.table.agent') }}
		</th>
		<th>
			{{ trans('depoint::general.contact_info') }}
		</th>
	</tr>
	</thead>
	<tbody>
	@foreach($agents as $agent)
		<tr>
			<td>
				{{ $agent['city'] }}
			</td>
			<td>
				{{ $agent['manager'] }}
			</td>
			<td>
				<div>
					{{ trans('validation.attributes.address').": ".ad($agent['address'])." - " }}
					{{ trans('validation.attributes.postal_code').": ".ad($agent['postal_code']) }}
				</div>
				<div>
					{{ trans('validation.attributes.telephone').": " }}
					<span style="direction: ltr; display: inline-block;">
						{{ ad($agent['telephone']) }}
					</span>
				</div>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
