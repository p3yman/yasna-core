@php
	$days = module($__module->name)->callControllerMethod('Manage\WidgetController','registerCountInMonth');

	foreach ($days as $day=>$count){
	   $arrLabel[]=ad(jdate($day)->format('%d %B'));
	   $arrData[] = $count;
	 }
@endphp


@include('manage::widgets.charts.bar.chartjs-bar',[
	'id'=> 'month-register-bar',
	'labels'=> $arrLabel,
	'data' => [
		[
			'backgroundColor' => '#23b7e5',
			'borderColor' => '#23b7e5',
			'data' => $arrData
		]
	]
])

