@php
		$genderData = module($__module->name)->callControllerMethod('Manage\WidgetController','genderData') ;
		$ageData = module($__module->name)->callControllerMethod('Manage\WidgetController','ageCategoryData') ;
		   foreach ($ageData as $category=>$count){
			  $arrLabel[] = trans("depoint::widgets.$category");
			  $arrData[] = $count;
		   }
@endphp

<div class="col-md-4">

@include ('manage::widgets.charts.pie.chartjs-pie',[
	'id' => 'chart-gender',
	'type' => 'doughnut', // doughnut or pie
	'legend' => true,
	'labels'=> [trans("depoint::widgets.man"), trans("depoint::widgets.women")],
	'data' => [
		[
			'backgroundColor' => ['#f05050', '#7266ba'],
			'hoverBackgroundColor' => ['#f05050', '#7266ba'],
			'data' => [$genderData['menCount'],$genderData['womenCount']]
		]
	]
])
</div>

<div class="col-md-8">
@include('manage::widgets.charts.bar.chartjs-bar',[
	'id'=> 'age-bar',
	'labels'=> $arrLabel,
	'data' => [
		[
			'backgroundColor' => '#23b7e5',
			'borderColor' => '#23b7e5',
			'data' => $arrData
		]
	]
])
</div>

