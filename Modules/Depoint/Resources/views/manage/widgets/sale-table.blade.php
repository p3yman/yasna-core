@php
	$data = module($__module->name)->callControllerMethod('Manage\WidgetController','saleStatisticsData');
   $sales_in_month = module($__module->name)->callControllerMethod('Manage\WidgetController','salesCountInMonth');
@endphp
@if(!empty($data))
	<div>
		<table class="table">
			<thead>
			<tr>
				<th>{{trans('depoint::widgets.duration')}}</th>
				<th>{{trans('depoint::widgets.count')}}</th>
				<th>{{trans('depoint::widgets.total-amount')}}</th>
			</tr>
			</thead>
			<tbody>
			@foreach($data as $key=>$item)
				<tr>
					<td>{{trans("depoint::widgets.$key")}}</td>
					<td>{{ad($item['count'])}}</td>
					<td>{{ad(number_format(depoint()->shop()->convertToMainCurrency($item['amount']))) ." ".trans('depoint::general.currency.IRT')}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>

	@if(!empty($sales_in_month))
		<hr>
		<h4> {{ trans('depoint::widgets.sales-in-month')}}</h4>
		@include('manage::widgets.charts.line.sparkline-line',[
						'height'=> '80',
						'width'=> '100%',
						'lineWidth' => '2',
						'lineColor' => '#7266ba',
						'spotColor' => '#888',
						'minColor' => "#7266ba",
						'maxColor' => '#7266ba',
						'fillColor'=> '',
						'highlightLine' => 'transparent',
						'spotRad' => '3',
						'values' => implode(",",$sales_in_month),
						'class'=> 'pv-lg'
					])
	@endif


@else
	<h3>{{trans('depoint::widgets.empty')}}</h3>
@endif
<script>

    $(function () {
        $('[data-sparkline]').each(initSparkLine);

        function initSparkLine() {
            var $element = $(this),
                options  = $element.data(),
                values   = options.values && options.values.split(',');

            options.type               = options.type || 'bar'; // default chart is bar
            options.disableHiddenCheck = true;

            $element.sparkline(values, options);

            if (options.resize) {
                $(window).resize(function () {
                    $element.sparkline(values, options);
                });
            }
        }
    });
</script>


