@php
	$data = module($__module->name)->callControllerMethod('Manage\WidgetController','latestDiscountCode');
@endphp

<div>
	@if($data->count())
		<table class="table">
			<thead>
			<tr>
				<th>{{trans('depoint::widgets.index')}}</th>
				<th>{{trans('depoint::widgets.submit-by')}}</th>
				<th>{{trans('depoint::widgets.discount-amount')}}</th>
				<th>{{trans('depoint::widgets.discount-date')}}</th>
			</tr>
			</thead>
			<tbody>
			@foreach($data as $item)
				<tr>
					<td>{{ad($loop->index+1)}}</td>
					<td>{{model('user',$item->user_id)->full_name}}</td>
					<td>{{ad(number_format(depoint()->shop()->convertToMainCurrency($item->purchased_amount))) ." ".trans('depoint::general.currency.IRT')}}</td>
					<td>{{ad(echoDate($item->purchased_at,'j F Y'))}}</td>
				</tr>

			@endforeach
			</tbody>
		</table>
	@else
		<h3>{{trans('depoint::widgets.empty')}}</h3>
	@endif
</div>