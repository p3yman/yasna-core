<div class="row">
	<div class="col-xs-12" id="main-grid">
		<span class="refresh">{{ SpareProductsTools::refreshGridLink() }}</span>

		@include("manage::widgets.grid" , [
			'headings'          => $browse_headings,
			'row_view'          => 'depoint::manage.products.browse-row',
			'table_id'          => 'tbl-products',
			'handle'            => 'counter',
			'operation_heading' => true,
		])

	</div>
</div>
