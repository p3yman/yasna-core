{!!
	widget('modal')
    	->label(trans('depoint::spare-parts.product.title.plural'))
!!}


<div class="modal-body">
	@include('depoint::manage.products.browse-toolbar-mini')
	@include('depoint::manage.products.browse-grid')
</div>

<div class="modal-footer">
	@include('manage::forms.button' , [
		'label' => trans('manage::forms.button.cancel'),
		'id' => "btnCancel" ,
		'shape' => 'link',
		'link' => '$(".modal").modal("hide")'
	])
</div>
