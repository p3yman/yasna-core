@php
	$action = $option[0];
@endphp

@include("manage::layouts.modal-delete" , [
	'form_url' => route('depoint.manage.products.delete-or-unndelete'),
	'title_value' => $model->title ,
	'save_label' => trans($action == 'delete' ? "manage::forms.button.soft_delete" : "manage::forms.button.undelete" ),
	'save_value' => $action ,
	'save_shape' => $action == 'delete' ? 'danger' : 'primary' ,
])
