<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->code,
		'class' => "font-yekan" ,
		'size' => "12",
	])

	@include("manage::widgets.grid-badge" , [
		'text' => $model->status_text,
		'color' => $model->status_color,
		'icon' => $model->status_icon,
	])
</td>
