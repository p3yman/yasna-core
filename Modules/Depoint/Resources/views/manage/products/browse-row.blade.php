@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => SpareProductsTools::refreshRowLink($model),
])


@include('depoint::manage.products.browse-row-title')
@include('depoint::manage.products.browse-row-code')


@include("manage::widgets.grid-actionCol"  , [
	'actions' => [
		[
			'pencil',
			trans('manage::forms.button.edit'),
			SpareProductsTools::editRowLink($model),
			$model->can('edit'),
		],
		[
			'trash-o',
			trans('manage::forms.button.soft_delete'),
			SpareProductsTools::deleteRowLink($model),
			($model->can('delete') and !$model->trashed())
		],
		[
			'recycle',
			trans('manage::forms.button.undelete'),
			SpareProductsTools::undeleteRowLink($model),
			($model->can('bin') and $model->trashed())
		],
	],
])
