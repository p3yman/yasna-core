{!!
	widget('modal')
		->target(route('depoint.manage.products.save'))
    	->labelIf($model->exists, trans('depoint::spare-parts.product.edit-something', ['title' => $model->title]))
    	->labelIf(!$model->exists, trans('depoint::spare-parts.product.create-new'))
!!}


<div class="modal-body">
	{!! widget('hidden')->name('id')->value($model->hashid) !!}


	{!!
		widget('input')
			->inForm()
			->name('code')
			->label(trans_safe('depoint::validation.attributes.code'))
			->value($model->code)
	!!}

	@php $site_locales = get_setting('site_locales') @endphp
	@foreach($site_locales as $locale)
		@php $locale_title = trans("depoint::general.locales.$locale") @endphp
		{!!
			widget('input')
				->inForm()
				->name("titles[$locale]")
				->label(trans('depoint::form.title-in', ['locale' => $locale_title]))
				->value($model->titleIn($locale))
		!!}
	@endforeach


	{!!
		widget('toggle')
			->inForm()
			->name('is_active')
			->label(trans('depoint::validation.attributes.active'))
			->value($model->is_active)
	!!}
</div>

<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
