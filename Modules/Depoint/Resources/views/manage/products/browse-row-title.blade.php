<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->title,
		'class' => "font-yekan" ,
		'size' => "14",
		'link' => $model->can('edit') ? SpareProductsTools::editRowLink($model) : ''
	])

	<div class="mv10 f10 text-grey">
		{{ $model->creator->full_name }}
	</div>


	@include("manage::widgets.grid-text" , [
		'text' => $model->id . '|' . $model->hashid,
		'class' => "font-yekan" ,
		'size' => "10",
		'condition' => user()->isDeveloper(),
	])

	@include('manage::widgets.grid-date', [
		'date' => $model->created_at
	])
</td>
