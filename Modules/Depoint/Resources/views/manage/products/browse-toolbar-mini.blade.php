<div class="col-xs-12 text-right" style="padding-top: 10px; padding-bottom: 10px">
	@php $link = str_after(SpareProductsTools::createLink(), 'modal:') @endphp
	{!!
		widget('button')
			->onClick("masterModal('$link')")
			->icon('plus-circle')
			->shape('success')
			->class('btn-xs')
			->label(trans_safe('manage::forms.button.add_to') . ' ' . $page[0][1])
			->condition(model('depoint-product')->can())
	!!}
</div>
