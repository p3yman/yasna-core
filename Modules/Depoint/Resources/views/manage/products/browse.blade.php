@extends('manage::layouts.template')

@section('content')
	@include('depoint::manage.products.browse-toolbar')
	@include('depoint::manage.products.browse-grid')
@endsection
