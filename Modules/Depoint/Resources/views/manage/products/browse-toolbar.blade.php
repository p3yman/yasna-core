@include("manage::widgets.toolbar" , [
    'title'             => $page[0][1],
    'buttons'           => [
        [
			'target'  => SpareProductsTools::createLink(),
			'type'    => 'success',
			'caption' => trans_safe('manage::forms.button.add_to') . ' ' . $page[0][1],
			'icon' => "plus-circle",
			'condition' => model('depoint-product')->can(),
        ],
    ],
])
