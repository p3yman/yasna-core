<div class="row" style="margin-top: 30px">
	<div class="col-xs-12">
		{!!
			widget('selectize')
				->inForm()
				->searchable()
				->name('providers')
				->label(trans_safe('depoint::spare-parts.provider.title.plural'))
				->valueField('id')
				->options(SpareProvidersTools::combo())
				->value(implode(',', $model->providers->pluck('hashid')->toArray()))
		!!}
	</div>
</div>
