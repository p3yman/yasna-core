{!!
	widget('modal')
		->target(route('depoint.manage.spare-parts.pending-files'))
    	->label($__module->getTrans('spare-parts.spare-part.pending-files'))
!!}


<div class='modal-body'>
	{!! widget('hidden')->name('hashid')->value($model->hashid) !!}


	@php $main_files = ($model->getMeta('pending_main_files') ?? []) @endphp
	@if (!empty($main_files))
		<div class="col-xs-12 files" style="margin-bottom: 10px">
			{!!
				widget('checkbox')
					->label($__module->getTrans('general.confirm-something', [
						'something' => $__module->getTrans('spare-parts.spare-part.main-files')
					]))
					->name('main_files')
					->value(1)
			!!}
		</div>

		<div class="col-xs-12 files" style="margin-bottom: 30px">

			<ul class="media-list">
				@foreach($main_files as $file)
					@include('depoint::manage.spare-parts.edit-main-file-item')
				@endforeach
			</ul>
		</div>
	@endif

	@php $preview_file = $model->getMeta('pending_preview_file') @endphp
	@if ($preview_file)
		<div class="col-xs-12 files" style="margin-bottom: 10px">
			{!!
				widget('checkbox')
					->label($__module->getTrans('general.confirm-something', [
						'something' => $__module->getTrans('spare-parts.spare-part.preview-file')
					]))
					->name('preview_file')
					->value(1)
			!!}
		</div>

		<div class="col-xs-12 files">
			<ul class="media-list">
				@if ($preview_file)
					@include('depoint::manage.spare-parts.edit-main-file-item', ['file' => $preview_file])
				@endif
			</ul>
		</div>
	@endif
</div>


<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		'save_value' => 'submit' ,
		'save_shape' => 'primary' ,
	])
</div>

{!! Html::style($__module->getAsset('css/spare-part-editor.min.css')) !!}
