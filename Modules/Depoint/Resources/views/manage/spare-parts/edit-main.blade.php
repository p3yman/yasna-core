<div class="col-md-7" style="margin-bottom: 30px">

	{!!
		widget('input')
			->inForm()
			->name('name')
			->value($model->name)
			->label(trans_safe('depoint::validation.attributes.name'))
	!!}

	{!!
		widget('input')
			->inForm()
			->name('english_name')
			->value($model->english_name)
			->label(trans_safe('depoint::validation.attributes.english_name'))
	!!}


	{!!
		widget('input')
			->inForm()
			->name('code')
			->value($model->code)
			->label(trans_safe('depoint::validation.attributes.code'))
	!!}


	<div class="form-group">
		<label for="is_ok" class="col-sm-2 control-label ">
			{{ trans_safe('depoint::validation.attributes.status') }}
		</label>
		<div class="col-sm-10">
			{!!
				widget('toggle')
					->name('is_ok')
					->value($model->is_ok)
					->size('100px')
					->label(' ')
					->dataOn('OK')
					->dataOnStyle('success')
					->dataOff('NOK')
					->dataOffStyle('danger')
			!!}
		</div>
	</div>

	@include('depoint::manage.spare-parts.edit-main-numbers')
	@include('depoint::manage.spare-parts.edit-main-files')
</div>
