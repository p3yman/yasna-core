{!!
	widget('form-open')
		->target(route('depoint.manage.spare-parts.save'))
		->class('js')
!!}

{!! widget('hidden')->name('id')->value($model->hashid) !!}


@include('depoint::manage.spare-parts.edit-main')
@include('depoint::manage.spare-parts.edit-side')


{!! widget('form-close') !!}
