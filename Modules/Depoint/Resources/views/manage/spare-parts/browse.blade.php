@extends('manage::layouts.template')

@section('content')
	@include('depoint::manage.spare-parts.browse-tabs')
	@include('depoint::manage.spare-parts.browse-toolbar')
	@include('depoint::manage.spare-parts.browse-grid')
	@include('depoint::manage.spare-parts.browse-search')
@append
