{!!
	widget('modal')
		->target(route('depoint.manage.spare-parts.add-to-cart'))
    	->label(trans('depoint::spare-parts.spare-part.add-to-cart-something', ['title' => $model->name]))
!!}

<div class='modal-body'>
	{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

	{!!
		widget('input')
			->inForm()
			->label(trans_safe('depoint::validation.attributes.name'))
			->value($model->name)
			->disabled()
	!!}
</div>

<div class="modal-footer">
	@include('manage::forms.buttons-for-modal' , [
		'save_label' => trans_safe('depoint::spare-parts.spare-part.add-to-cart'),
		'save_value' => 'submit' ,
		'save_shape' => 'primary' ,
	])
</div>
