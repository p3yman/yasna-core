<hr>

<div class="row">
	<div class="col-xs-12">
		{!! widget('feed') !!}
	</div>

	<div class="col-xs-12 text-center">
		{!!
			widget('button')
				->type('submit')
				->shape('success')
				->label(trans_safe('manage::forms.button.save'))
		!!}
	</div>
</div>
