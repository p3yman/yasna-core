@if ($is_search_form)
	<div class="panel panel-default p20">

		{!!
			 widget('form-open')
			 	->target(SparePartsTools::browseLink())
			 	->method('get')
		!!}


		{!!
			widget('selectize')
				->inForm()
				->searchable()
				->name('products')
				->label(trans_safe('depoint::spare-parts.product.title.plural'))
				->valueField('id')
				->options(SpareProductsTools::combo())
		!!}


		{!!
			widget('selectize')
				->inForm()
				->searchable()
				->name('providers')
				->label(trans_safe('depoint::spare-parts.provider.title.plural'))
				->valueField('id')
				->options(SpareProvidersTools::combo())
		!!}


		<div class="form-group">
			<label for="is_ok" class="col-sm-2 control-label ">
				{{ trans_safe('depoint::validation.attributes.status') }}
			</label>
			<div class="col-sm-10">
				{!!
					widget('toggle')
						->name('is_ok')
						->size('100px')
						->label(' ')
						->dataOn('OK')
						->dataOnStyle('success')
						->dataOff('NOK')
						->dataOffStyle('danger')
				!!}
			</div>
		</div>


		{!!
			 widget('button')
			    ->inForm()
			    ->type('submit')
			    ->shape('success')
			    ->label(trans_safe('manage::forms.button.search'))
		!!}


		{!! widget('form-close') !!}
	</div>
@endif