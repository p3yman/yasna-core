@include("manage::layouts.modal-mass" , [
	'form_url' => route('depoint.manage.spare-parts.add-to-cart-mass') ,
	'modal_title' => trans_safe('depoint::spare-parts.spare-part.add-to-cart'),
	'save_label' => trans_safe('depoint::spare-parts.spare-part.add-to-cart'),
	'save_shape' => "primary" ,
])
