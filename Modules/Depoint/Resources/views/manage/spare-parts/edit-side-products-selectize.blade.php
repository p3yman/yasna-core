<span class="refresh">{{ SparePartsTools::editorProductsLink($model) }}</span>
{!!
	widget('selectize')
		->inForm()
		->searchable()
		->name('products')
		->label(trans_safe('depoint::spare-parts.product.title.plural'))
		->valueField('id')
		->options(SpareProductsTools::combo())
		->value(implode(',', $model->products->pluck('hashid')->toArray()))
!!}
