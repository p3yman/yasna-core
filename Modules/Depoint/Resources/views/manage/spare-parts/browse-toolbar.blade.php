@include("manage::widgets.toolbar" , [
    'title'             => $page[0][1],
    'buttons'           => [
        [
			'target'  => SparePartsTools::createLink(),
			'type'    => 'success',
			'caption' => trans_safe('manage::forms.button.add_to') . ' ' . $page[0][1],
			'icon'    => "plus-circle",
			'condition' => model('depoint-spare-part')->can(),
        ],
        [
			'target'  => route('depoint.manage.cart', [], false),
			'type'    => 'info',
			'caption' => trans_safe('depoint::spare-parts.cart.title.singular'),
			'icon'    => "shopping-cart",
			'condition' => SparePartsTools::cartIsAccessible(),
        ],
    ],
    'mass_actions' => SparePartsTools::browseMassActions()
])
