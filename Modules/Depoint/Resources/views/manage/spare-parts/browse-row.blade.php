@include('manage::widgets.grid-rowHeader' , [
	'handle' => SparePartsTools::cartIsAccessible() ? 'selector' : 'counter',
	'refresh_url' => SparePartsTools::refreshRowLink($model),
])

@include('depoint::manage.spare-parts.browse-row-name')
@include('depoint::manage.spare-parts.browse-row-code')
@include('depoint::manage.spare-parts.browse-row-numbers')

@include("manage::widgets.grid-actionCol"  , [
	'actions' => [
		[
			'pencil',
			trans('manage::forms.button.edit'),
			SparePartsTools::editRowLink($model),
			$model->can('edit'),
		],
		[
			'cart-plus',
			trans_safe('depoint::spare-parts.spare-part.add-to-cart'),
			SparePartsTools::addToCartLink($model),
			(!$model->trashed() and SparePartsTools::cartIsAccessible())
		],
		[
			'trash-o',
			trans('manage::forms.button.soft_delete'),
			SparePartsTools::deleteRowLink($model),
			($model->can('delete') and !$model->trashed())
		],
		[
			'recycle',
			trans('manage::forms.button.undelete'),
			SparePartsTools::undeleteRowLink($model),
			($model->can('bin') and $model->trashed())
		],
	],
])
