@php $is_search_form = ($is_search_form ?? false) @endphp

@if (!$is_search_form)
	<div class="row">
		<div class="col-xs-12" id="main-grid">
			<span class="refresh">{!! SparePartsTools::refreshGridFullUrl() !!}</span>

			@include("manage::widgets.grid" , [
				'headings'          => $browse_headings,
				'row_view'          => 'depoint::manage.spare-parts.browse-row',
				'table_id'          => 'tbl-providers',
				'handle'            => SparePartsTools::cartIsAccessible() ? 'selector' : 'counter',
				'operation_heading' => true,
			])
		</div>
	</div>
@endif

@section('html_footer')
	@if (!$is_search_form)
		<script>
            divReload = function (div_id, additive = '') {

                //Preparations...
                var $div       = $("#" + div_id);
                var reload_url = $("#" + div_id + " .refresh").html();
                reload_url     = $('<div>').html(reload_url).text();

                if (!reload_url) {
                    reload_url = $div.attr('data-src') + additive;
                    reload_url = reload_url.replaceAll("-id-", $div.attr('data-id'));

                    // Checks if reload_url starts with http
                    if (!/^http.+$/.test(reload_url)) {
                        reload_url = url(reload_url);
                    }
                }
                if (!reload_url) {
                    return;
                }

                //Loading Effect...
                has_loading = $div.attr('data-loading');

                if (has_loading != 'no') {
                    $div.addClass('loading');
                }
                forms_log('loading [' + reload_url + '] in [' + div_id + ']');

                //Ajax...
                $.ajax({
                    url  : reload_url,
                    cache: false
                }).done(function (html) {
                        if ($div.attr('data-type') == 'append') {
                            $div.append(html);
                        }
                        else {
                            $div.html(html);
                        }
                        $div.removeClass('loading');
                    }
                )
                ;
            }
		</script>
	@endif
@append
