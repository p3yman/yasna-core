@php
	$uploaders = $option[1];
	$main_uploader = $uploaders['main'];
	$preview_uploader = $uploaders['preview'];
@endphp

@section('html_header')
	{!! Html::style(Module::asset('depoint:css/spare-part-editor.min.css')) !!}
	{!! $main_uploader->renderCss() !!}
	{!! $preview_uploader->renderCss() !!}
@append

@section('html_footer')
	<script>
        let file_view_url = "{{ route('depoint.manage.spare-parts.save.file.view', ['file_hashid' => '__FILE__']) }}"
	</script>
	{!! Html::script(Module::asset('depoint:js/spare-part-editor.min.js')) !!}
	{!! $main_uploader->renderJs() !!}
	{!! $preview_uploader->renderJs() !!}
@append

<hr>

<div class="row">
	<div class="col-xs-12 upload-box" id="main-file-uploader">
		<h5>{{ trans_safe('depoint::spare-parts.spare-part.main-files') }}</h5>
		@php $current_main_files = $model->main_files @endphp
		<div class="input-container">
			{!!
				widget('hidden')
					->id('main-files')
					->name('main_files')
					->class('file-input')
					->value(json_encode($current_main_files))
			!!}
		</div>

		<div class="col-xs-12 files" style="margin-bottom: 20px">
			<ul class="media-list">
				@foreach($current_main_files as $file)
					@include('depoint::manage.spare-parts.edit-main-file-item')
				@endforeach
			</ul>
		</div>

		<div class="col-xs-12">
			{!! $main_uploader->render() !!}
		</div>

	</div>

	<div class="col-xs-12 upload-box" id="preview-file-uploader">
		<h5>{{ trans_safe('depoint::spare-parts.spare-part.preview-file') }}</h5>
		@php $current_preview_files_array = ($model->preview_file) ? [$model->preview_file] : [] @endphp
		<div class="input-container">
			{!!
				widget('hidden')
					->id('preview-files')
					->name('preview_file')
					->class('file-input')
					->value(json_encode($current_preview_files_array))
			!!}
		</div>

		<div class="col-xs-12 files" style="margin-bottom: 20px">
			<ul class="media-list">
				@foreach($current_preview_files_array as $file)
					@include('depoint::manage.spare-parts.edit-main-file-item')
				@endforeach
			</ul>
		</div>

		<div class="col-xs-12">
			{!! $preview_uploader->render() !!}
		</div>

	</div>
</div>
