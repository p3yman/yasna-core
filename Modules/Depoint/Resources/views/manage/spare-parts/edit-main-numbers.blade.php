<hr>

{!!
	widget('input')
		->inForm()
		->name('inventory')
		->value($model->inventory)
		->label(trans_safe('depoint::validation.attributes.inventory'))
		->addon(trans_safe('depoint::spare-parts.unit.numbers'))
		->help(trans_safe('depoint::spare-parts.spare-part.help.inventory'))
!!}

{!!
	widget('input')
		->inForm()
		->name('daily_use')
		->value($model->daily_use)
		->label(trans_safe('depoint::validation.attributes.daily_use'))
		->addon(trans_safe('depoint::spare-parts.unit.numbers'))
		->help(trans_safe('depoint::spare-parts.spare-part.help.daily_use'))
!!}

{!!
	widget('input')
		->inForm()
		->name('production_days')
		->value($model->production_days)
		->label(trans_safe('depoint::validation.attributes.production_days'))
		->addon(trans_safe('depoint::spare-parts.unit.days'))
		->help(trans_safe('depoint::spare-parts.spare-part.help.production_days'))
!!}

{!!
	widget('input')
		->inForm()
		->name('alert_days')
		->value($model->alert_days)
		->label(trans_safe('depoint::validation.attributes.alert_days'))
		->addon(trans_safe('depoint::spare-parts.unit.days'))
		->help(trans_safe('depoint::spare-parts.spare-part.help.alert_days'))
!!}
