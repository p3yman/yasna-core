<div class="row">
	<div class="col-xs-12" id="products-selectize">
		@include('depoint::manage.spare-parts.edit-side-products-selectize')
	</div>

	<div class="col-xs-12 text-right">
		@php $products_modal_url = route('depoint.manage.products.browse-modal') @endphp
		{!!
			widget('button')
				->label(trans_safe('depoint::spare-parts.product.manage'))
				->id('btn-products')
				->shape('info')
				->setExtra('data-url', $products_modal_url)
				->onClick('masterModal("' . $products_modal_url . '")')
				->condition(model('depoint-product')->can('*'))
		!!}
	</div>
</div>

