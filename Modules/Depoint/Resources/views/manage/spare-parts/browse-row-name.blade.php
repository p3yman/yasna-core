<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->name,
		'class' => "font-yekan" ,
		'size' => "14",
		'link' => $model->can('edit') ? SparePartsTools::editRowLink($model) : ''
	])


	@include("manage::widgets.grid-text" , [
		'text' => $model->english_name,
		'class' => "text-gray" ,
		"color" => 'gray',
		'size' => "12",
	])


	@include("manage::widgets.grid-text" , [
		'text' => $model->creator->full_name,
		'class' => "font-yekan text-gray" ,
		"color" => 'gray',
		'size' => "11",
	])


	@include("manage::widgets.grid-text" , [
		'text' => $model->id . '|' . $model->hashid,
		'class' => "font-yekan" ,
		'size' => "10",
		'condition' => user()->isDeveloper(),
	])

	@include('manage::widgets.grid-date', [
		'date' => $model->created_at
	])


	@include("manage::widgets.grid-badge" , [
		'text' => $__module->getTrans('spare-parts.spare-part.pending-files'),
		'color' => 'warning',
		'icon' => 'hourglass-half',
		'condition' => $model->hasPendingFiles(),
		'link' => 'modal:' . route_locale('depoint.manage.spare-parts.single-action', [
			'model_id' => $model->hashid,
			'action' => 'pending-files',
		], false),
	])

</td>
