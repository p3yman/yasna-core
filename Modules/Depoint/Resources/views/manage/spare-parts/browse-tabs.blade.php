@include("manage::widgets.tabs" , [
	'current' =>  $page[1][2] ,
	'tabs' => [
		['all', trans_safe('depoint::spare-parts.browse-tab.all')],
		['search', trans_safe('depoint::spare-parts.browse-tab.search')],
	] ,
])
