<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->daily_use . ' ' . trans_safe('depoint::spare-parts.unit.numbers'),
		'class' => "font-yekan" ,
		'size' => "14",
	])
</td>

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->inventory . ' ' . trans_safe('depoint::spare-parts.unit.numbers'),
		'class' => "font-yekan" ,
		'size' => "14",
	])
</td>

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->alert_days . ' ' . trans_safe('depoint::spare-parts.unit.days'),
		'class' => "font-yekan" ,
		'size' => "14",
	])
</td>
