@php $doc = fileManager()->file($file)->resolve() @endphp

@if ($doc->getUrl())
	<li class="media" data-hashid="{{ $file }}">
		<div class="media-left">
			<a href="{{ $doc->getUrl() }}">{!! $doc->elementWidth('60px')->show() !!}</a>
		</div>
		<div class="media-body">
			<h4 class="media-heading">{{ $doc->getTitle() }}</h4>
			<button class="btn  btn-danger btn-xs btn-outline file-item-remove" type="button">
				<span class="fa fa-times"></span>
			</button>
			<a class="btn btn-info btn-xs btn-outline" href="{{ $doc->getDownloadUrl() }}">
				<span class="fa fa-download"></span>
			</a>
		</div>
	</li>
@endif
