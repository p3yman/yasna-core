@include("manage::layouts.sidebar-link" , [
	'icon' => "comment",
	'caption' => trans_safe('depoint::spare-parts.section-title'),
	'link' => "#" ,
	'sub_menus' => $sub_menus = module('depoint')->ManageSidebarController()->all() ,
	'condition' => sizeof($sub_menus),
])
