{{
widget('modal')->label($__module->getTrans('report.excel'))
->id('excel-modal')
}}

<form method="post" action="" id="cols">
	<div class="row m">
		<h4>{{$__module->getTrans('report.excel-fields')}}</h4>
	</div>
	<div class="panel panel-default m">

		<div class="row m">
			<div class="col-md-2">
				{{widget('checkbox')->id('name')->name('name_check')->value(1)->label($__module->getTrans('report.name'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('family')->name('family_check')->value(1)->label($__module->getTrans('report.family'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('gender')->name('gender_check')->value(1)->label($__module->getTrans('report.gender'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('tel')->name('tel_check')->value(1)->label($__module->getTrans('report.tel'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('mobile')->name('mobile_check')->value(1)->label($__module->getTrans('report.mobile'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('email')->name('email_check')->value(1)->label($__module->getTrans('report.email'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('birth_day')->name('birth_day_check')->value(1)->label($__module->getTrans('report.birth-date'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('marriage_date')->name('marriage_date_check')->value(1)->label($__module->getTrans('report.marriage-date'))}}
			</div>

			<div class="col-md-2">
				{{widget('checkbox')->id('city')->name('city_check')->value(1)->label($__module->getTrans('report.city'))}}
			</div>
			<div class="col-md-2">
				{{widget('checkbox')->id('province')->name('province_check')->value(1)->label($__module->getTrans('report.province'))}}
			</div>
		</div>

	</div>
	{{widget('button')->id('excel-btn')->label($__module->getTrans('report.export'))->class('btn-primary m')}}
</form>

<script>

    $(document).ready(function () {

        $("#excel-btn").click(function () {
            var queryString = $("#report-form").serialize();
            var cols        = $("#cols").serialize();
            var allData     = queryString + "&" + cols;

            window.location = "{{url('manage/depoint/report/export')}}" + "?ajax=1&" + allData;
            $(".close").click()
        })
    });
</script>