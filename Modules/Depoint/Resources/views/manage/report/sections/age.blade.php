<div class="row">
	<div class="col-md-6">
		<div class="col-sm-8">
			{!!
			  widget('text')
			  ->id('age-min-input')
			  ->name('age_min')
			  ->value($request->get('age_min'))
			  ->label($__module->getTrans('report.from-age'))

			!!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="col-sm-8">
			{!!
			  widget('text')
			  ->id('age-max-input')
			  ->name('age_max')
			  ->value($request->get('age_max'))
				->label($__module->getTrans('report.to-age'))
			!!}
		</div>
	</div>
</div>
