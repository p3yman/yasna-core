@php
	$provinces = model('state')::where('parent_id','0')->orderBy('title')->get(['id','title'])->toArray();
		$cities = model('state')::where('parent_id','<>','0')->orderBy('title')->get(['id','title'])->toArray();

	if (!empty($request->get('home_province'))){
	$cities = model('state')::where('parent_id',$request->get('home_province'))->orderBy('title')->get(['id','title'])->toArray();
	}
    else{
	$cities = model('state')::where('parent_id','<>','0')->orderBy('title')->get(['id','title'])->toArray();
    }
@endphp

<div class="col-md-4">
	{!!
		   widget('combo')
			->id('home-province-input')
			->name('home_province')
			->options($provinces)
			->searchable()
			->valueField('id')
			->captionField('title')
			->blankValue('')
			->blankCaption($__module->getTrans('report.blank-value'))
			->value($request->get('home_province'))
			->label($__module->getTrans('report.home-province'))
		!!}
</div>

<div class="col-md-4">
	{!!
		 widget('combo')
		->id('home-city-input')
		->name('home_city')
		->options($cities)
		->searchable()
		->valueField('id')
		->captionField('title')
		->blankValue('')
		->blankCaption($__module->getTrans('report.blank-value'))
		->value($request->get('home_city'))
		->label($__module->getTrans('report.home-city'))
	!!}
</div>
@include($__module->getBladePath('manage.report.js.scripts'))
<script>

    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            }
        });

        $("#home-province-input").change(function (e) {

            getCities($("#home-city-input"), $(this).val(), "{{url('manage/depoint/report/city_ajax')}}")
        });

        $("#home-city-input").change(function (e) {
            getProvince($("#home-province-input"), $(this).val(), "{{url('manage/depoint/report/province_ajax')}}")
        });
    });
</script>