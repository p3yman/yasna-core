@extends('manage::layouts.template')

@section('content')

	<div class="container-fluid" id="report-parent">


		<div class="panel panel-dark">
			<div class="panel-heading">
				<h3 class="f20 mv-lg">
					{{$__module->getTrans('report.form-title')}}
				</h3>
			</div>
			<div class="tforms">
				{!! widget('form-open')->method('GET')->target('manage/depoint/report')->id('report-form') !!}
				<div class="panel-body">
					<div class="row">
						<div class="col-md-3">
							{!!
								 widget('input')
									->id('name')
									->name('name')
									->value($request->get('name'))
									->label($__module->getTrans('report.name'))
							!!}
						</div>
						<div class="col-md-3">
							{!!
								 widget('text')
									->id('family')
									->name('family')
									->value($request->get('family'))
									->label($__module->getTrans('report.family'))
							!!}
						</div>
						<div class="col-md-3">
							@include($__module->getBladePath('manage.report.sections.age'))
						</div>
						<div class="col-md-3">
							{!!
						   widget('combo')
							->id('gender-input')
							->name('gender')
							->options($genders)
							->searchable()
							->valueField('key')
							->captionField('caption')
							->blankValue('')
							->blankCaption($__module->getTrans('report.blank-value'))
							->value($request->get('gender'))
							->label($__module->getTrans('report.gender'))
							!!}
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							{!!
								 widget('input')
								    ->type('email')
									->id('email')
									->name('email')
									->value($request->get('email'))
									->label($__module->getTrans('report.email'))
							!!}
						</div>
						<div class="col-md-4">
							{!!
								 widget('input')
								 	->type('tel')
									->id('tel')
									->name('tel')
									->value($request->get('tel'))
									->label($__module->getTrans('report.tel'))
							!!}
						</div>
						<div class="col-md-4">
							{!!
								 widget('input')
								 ->type('tel')
									->id('mobile')
									->name('mobile')
									->value($request->get('mobile'))
									->label($__module->getTrans('report.mobile'))
							!!}
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							{!!
								 widget('PersianDatePicker')
									 ->label($__module->getTrans('report.birth-date'))
									 ->id('birth-date')
									 ->name('birth_date')
									 ->value($request->birth_date??"")
									  ->onlyDatePicker()
									  ->withoutTime()
								 !!}
						</div>
						<div class="col-md-6">
							{!!
								 widget('PersianDatePicker')
									 ->label($__module->getTrans('report.marriage-date'))
									 ->id('marriage-date')
									 ->name('marriage_date')
									 ->value($request->marriage_date??"")
									  ->onlyDatePicker()
									  ->withoutTime()
								 !!}
						</div>
					</div>

					<div class="row">
						@include($__module->getBladePath('manage.report.sections.home-city'))
					</div>
				</div>
			</div>

			<div class="panel-footer pv-lg">
				{!! widget('button')->id('submit')->type('submit')->label($__module->getTrans('report.submit'))->class('btn-primary')!!}
				{!! widget('button')->id('excel')->label($__module->getTrans('report.excel'))->class('btn-success')->icon('file-excel')!!}
			</div>
			{!! widget('form-close') !!}
		</div>
	</div>


	@if($search)
		<div class="row" style="margin-top:10px ">
			<div class="col-md-12">
				<p>
					{{$__module->getTrans('report.count')." ".pd($resCount)}}
				</p>
			</div>
			<div class="col-md-12">
				@include("manage::widgets.grid" , [
					'table_id' => "tblUsers" ,
					'row_view' => "depoint::manage.report.table-row" ,
					'headings' => [
					$__module->getTrans('report.number'),
					$__module->getTrans('report.fullname'),
					$__module->getTrans('report.mobile'),
					$__module->getTrans('report.email'),
					$__module->getTrans('report.birth-date'),
					] ,
				]     )
			</div>
		</div>
	@endif


	{{--excel export--}}
	@php($modalUrl=url('manage/depoint/report/excel/modal'))
	<script>
        //run excel modal with form data
        $("#excel").click(function () {
            var queryString = "?" + $("#report-form").serialize();
            masterModal("{{$modalUrl}}" + queryString)
        });
	</script>
@stop
