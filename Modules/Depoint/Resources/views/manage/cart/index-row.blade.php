@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => '#',
])

@include('depoint::manage.cart.index-row-title')
@include('depoint::manage.cart.index-row-code')
@include('depoint::manage.cart.index-row-count')
