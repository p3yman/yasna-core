{!!
	widget('form-open')
		->target(route('depoint.manage.cart.checkout'))
		->class('js checkout-form')
!!}

<div class="row">
	@foreach($spare_parts_groups as $group)
		@include('depoint::manage.cart.index-group')
	@endforeach

	@include('depoint::manage.cart.index-groups-buttons')
</div>

{!! widget('form-close') !!}
