@php $provider = $group['provider'] @endphp
<div class="col-xs-12" id="group-{{ $provider->hashid }}">
	<h4>{{ $provider->name }}</h4>
	@include("manage::widgets.grid" , [
		'headings'          => $browse_headings,
		'row_view'          => 'depoint::manage.cart.index-row',
		'table_id'          => 'tbl-spare-parts-' . $provider->hashid,
		'handle'            => 'counter',
		'models' 			=> collect($group['spare_parts']),
	])
</div>
