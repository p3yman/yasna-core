@if (count($spare_parts_groups))
	@include('depoint::manage.cart.index-groups-list')
@else
	@include('depoint::manage.cart.index-empty')
@endif
