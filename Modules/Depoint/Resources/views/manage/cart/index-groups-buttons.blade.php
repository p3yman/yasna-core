<div class="col-xs-12">
	{!! widget('feed')->containerId('checkout-feed') !!}

	<div class="alert alert-info verify-note" style="display: none">
		{{ trans_safe('depoint::spare-parts.cart.verify-note') }}
	</div>
</div>
<div class="col-xs-12 text-right buttons-container">

	{!!
		widget('button')
			->shape('success')
			->class('step1')
			->type('button')
			->label(trans_safe('depoint::spare-parts.cart.continue'))
			->onClick('toStep2()')
	!!}


	{!!
		widget('button')
			->shape('success')
			->class('step2')
			->type('submit')
			->label(trans_safe('depoint::spare-parts.cart.checkout'))
			->style('display:none')
	!!}


	{!!
		widget('button')
			->shape('danger')
			->class('step2')
			->type('button')
			->label(trans_safe('depoint::spare-parts.cart.no-it-was-wrong'))
			->style('display:none')
			->onClick('toStep1()')
	!!}
</div>
