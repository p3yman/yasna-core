@php $name = "counts[$provider->hashid][$model->hashid]" @endphp

<td>
	{!!
		widget('input')
			->name($name)
			->class('number-input')
			->style('max-width: 100px')
	!!}
</td>
