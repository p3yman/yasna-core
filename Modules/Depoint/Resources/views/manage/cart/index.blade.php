@extends('manage::layouts.template')

@section('content')
	@include('depoint::manage.cart.index-content')
@append

@section('html_footer')
	{!! Html::script(Module::asset('depoint:js/spare-part-cart.min.js')) !!}
	{!! Html::script(Module::asset('depoint:js/number-input.min.js')) !!}
@append
