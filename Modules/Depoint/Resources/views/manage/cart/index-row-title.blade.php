<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->name,
		'class' => "font-yekan" ,
		'size' => "14"
	])

	@include("manage::widgets.grid-text" , [
		'text' => $model->creator->full_name,
		'class' => "font-yekan" ,
		'size' => "10",
	])

	@include("manage::widgets.grid-text" , [
		'text' => $model->id . '|' . $model->hashid,
		'class' => "font-yekan" ,
		'size' => "10",
		'condition' => user()->isDeveloper(),
	])
</td>
