@include("manage::layouts.sidebar-link" , [
	'icon' => "shopping-cart",
	'caption' => $__module->getTrans('cart.credit-purchases'),
	'link' => str_after(route('depoint.credit-payments.carts'), url('manage') . '/'),
])
