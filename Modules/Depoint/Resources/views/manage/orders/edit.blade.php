{!!
	widget('modal')
		->target(route('depoint.manage.orders.save'))
    	->label(trans('depoint::spare-parts.order.edit'))
!!}


<div class="modal-body">
	{!! widget('hidden')->name('id')->value($model->hashid) !!}

	{!!
		widget('input')
			->inForm()
			->label(trans_safe('depoint::spare-parts.provider.title.singular'))
			->value($model->provider->name)
			->disabled()
	!!}

	@include("manage::widgets.grid" , [
		'headings' => [
			trans_safe('validation.attributes.title'),
			trans_safe('depoint::validation.attributes.count'),
		],
		'row_view' => 'depoint::manage.orders.edit-item',
		'table_id' => 'tbl-order-parts',
		'handle'   => 'counter',
		'models'   => $model->items,
	])

	{!!
		widget('toggle')
			->name('is_verified')
			->inForm()
			->label($__module->getTrans('spare-parts.order.status.verified'))
			->value($model->isVerified())
			->inForm()
	!!}
</div>


<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
