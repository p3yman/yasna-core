@if ($model->isNotTrashed())

	@include('manage::widgets.grid-rowHeader' , [
		'handle' => "counter" ,
		'refresh_url' => SparePartsOrdersTools::refreshRowLink($model),
	])


	@include('depoint::manage.orders.browse-row-provider')
	@include('depoint::manage.orders.browse-row-code')
	@include('depoint::manage.orders.browse-row-status')
	@include('depoint::manage.orders.browse-row-registrar')
	@include('depoint::manage.orders.browse-row-spare-parts')


	@include("manage::widgets.grid-actionCol"  , [
		'actions' => [
			[
				'pencil',
				trans('manage::forms.button.edit'),
				SparePartsOrdersTools::editLink($model),
				$model->can('edit'),
			],
			[
				'trash-o',
				trans('manage::forms.button.delete'),
				SparePartsOrdersTools::deleteLink($model),
				($model->can('delete') and !$model->trashed())
			],
		],
	])

@endif
