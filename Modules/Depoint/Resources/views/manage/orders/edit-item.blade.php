@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
])

@php $spare_part = $model->spare_part @endphp

<td>
	@include("manage::widgets.grid-text" , [
		'text' => $spare_part->name,
		'class' => "font-yekan" ,
		'size' => "12",
	])
</td>


<td>
	{!!
		widget('input')
			->name("counts[$model->hashid]")
			->class('number-input')
			->style('max-width: 100px')
			->value(ad($model->count))
	!!}
</td>

<td style="vertical-align: middle">
	<button class="btn btn-danger btn-xs btn-outline spare-part-remove" type="button">
		<span class="fa fa-times"></span>
	</button>
	<button class="btn btn-info btn-xs btn-outline spare-part-recycle" type="button">
		<span class="fa fa-recycle"></span>
	</button>
</td>
