@include("manage::layouts.modal-delete" , [
	'form_url'    => route('depoint.manage.orders.delete'),
	'title_value' => $model->virtual_title . ' (' . $model->code . ')',
	'title_label' => trans_safe('depoint::spare-parts.headline.summary') ,
	'save_label'  => trans("manage::forms.button.delete"),
	'save_shape'  => 'danger',
])
