<td>
	@include("manage::widgets.grid-text" , [
		'text' => $model->provider->name,
		'class' => "font-yekan" ,
		'size' => "14",
	])

	@include("manage::widgets.grid-text" , [
		'text' => $model->id . '|' . $model->hashid,
		'class' => "font-yekan" ,
		'size' => "10",
		'condition' => user()->isDeveloper(),
	])

	@include("manage::widgets.grid-text" , [
		'text' => $model->creator->full_name,
		'class' => "font-yekan" ,
		'size' => "10",
	])

	@include('manage::widgets.grid-date', [
		'date' => $model->created_at
	])
</td>
