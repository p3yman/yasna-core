@extends('manage::layouts.template')


@section('content')
	@include('depoint::manage.orders.browse-grid')
@append

@section('html_header')
	<style>
		#tbl-order-parts tr.grid:not(.deleted-content) .spare-part-recycle {
			display: none;
		}
		#tbl-order-parts tr.grid.deleted-content .spare-part-remove {
			display: none;
		}
	</style>
@append

@section('html_footer')
	{!! Html::script(Module::asset('depoint:js/number-input.min.js')) !!}
	{!! Html::script(Module::asset('depoint:js/spare-part-orders.min.js')) !!}
@append
