@php $spare_parts = $model->spare_parts @endphp
<td>
	@foreach($spare_parts as $spare_part)
		@include("manage::widgets.grid-badge" , [
			'text' => $spare_part->name,
			'class' => "font-yekan" ,
			'icons' => 'cogs'
		])
	@endforeach
</td>
