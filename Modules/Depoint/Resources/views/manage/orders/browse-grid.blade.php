<div class="row">
	<div class="col-xs-12" id="main-grid">
		<span class="refresh">{{ SparePartsOrdersTools::refreshGridLink() }}</span>

		@include("manage::widgets.grid" , [
			'headings'          => $browse_headings,
			'row_view'          => 'depoint::manage.orders.browse-row',
			'table_id'          => 'tbl-orders',
			'handle'            => 'counter',
			'operation_heading' => true,
		])
	</div>
</div>
