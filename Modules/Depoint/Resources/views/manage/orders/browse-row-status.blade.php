<td>
	@include('manage::widgets.grid-badge', [
		'text' => $model->status_text,
		'color' => $model->status_manage_color,
		'icon' => $model->status_icon,
	])

	@include('manage::widgets.grid-date', [
		'date' => $model->status_date,
	])
</td>
