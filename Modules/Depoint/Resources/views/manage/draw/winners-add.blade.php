<tr>
	<td style="vertical-align: middle"><span class="fa fa-plus"></span></td>
	<td colspan="1">
		{!! widget('hidden')->name('hashid')->value($model->id) !!}

		{!!
			widget('input')
				->id('txtDrawingGuess')
				->name('number')
				->placeholder(
					$__module->getTrans('cart.drawing.take-number-between', [
							'number1' => ad(1),
							'number2' => ad($max_possible_number = session()->get('line_number')),
						])
				)->class('text-center ltr')
		!!}
	</td>
	<td colspan="3">
		{!!
			widget('button')
				->id('btnSubmit')
				->label(trans_safe('manage::forms.general.submit'))
				->type('submit')
		!!}

		{!!
			widget('button')
				->label($__module->getTrans('cart.drawing.random-number'))
				->type('button')
				->onClick("drawingRandom($max_possible_number)")
				->shape('link')
		!!}
	</td>
</tr>
