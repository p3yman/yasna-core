{!!
	widget('modal')
		->target(route('depoint.manage.draw.prepare'))
		->label($__module->getTrans('cart.drawing.draw.singular'))
!!}

<div class='modal-body'>
	{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

	{!!
		widget('input')
			->inForm()
			->label($posttype->singularTitleIn(getLocale()))
			->value($model->title)
			->disabled()
	!!}


	{!! widget('group') !!}

	{!!
		widget('button')
			->id('btnPrepare')
			->label($__module->getTrans('cart.drawing.draw.prepare'))
			->shape('primary')
			->class('-progressHide')
			->type('submit')
	!!}

	{!! widget('group')->type('stop') !!}

	{!! widget('feed') !!}

	<div class="progress noDisplay">
		<div id="divProgress" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"
			 aria-valuenow="0" aria-valuemin="0" aria-valuemax="{{ $model->receipts->count() }}" style="width:0;">
			<span class="sr-only"></span>
		</div>
	</div>
</div>

<div class="modal-footer">
	@include('manage::forms.button' , [
	'label' => trans('manage::forms.button.cancel'),
	'id' => "btnCancel" ,
	'shape' => 'link',
	'link' => '$(".modal").modal("hide")'
])
</div>
