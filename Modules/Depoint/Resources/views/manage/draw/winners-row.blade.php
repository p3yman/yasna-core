<tr>
	<td>{{ ad($key + 1) }}</td>

	@if($winner and $winner->exists)
		<td>
			@include("manage::widgets.grid-text" , [
				'text' => $winner->full_name,
				'link' => "urlN:manage/users/browse/customer/search?id=".$winner->id."&searched=1" ,
			])
		</td>
		<td ondblclick="$(this).html('{{ pd($winner->mobile_formatted) }}')">
			{{ ad($winner->mobile_masked) }}
		</td>
		@php
			$total_purchased_amount = depoint()->shop()->convertToMainCurrency($winner->totalReceiptsAmountInEvent($model));
		@endphp
		<td>{{ ad(number_format($total_purchased_amount)) . ' ' . depoint()->shop()->currencyTrans() }}</td>
	@else
		<td colspan="3"><span class="noContent"> {{ trans('people.form.user_deleted') }}</span></td>
	@endif

	<td>
		@if($model->isReadyToDraw())
			<a class="text-danger f10" href="#"
			   onclick="drawingDelete('{{ route('depoint.manage.draw.delete-winner', ['key' => $key]) }}' , '{{$model->hashid}}')"
			>
				x
			</a>
		@endif
	</td>
</tr>
