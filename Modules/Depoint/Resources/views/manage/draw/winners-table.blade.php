{!! widget('feed')->containerClass('m10') !!}

<span class="refresh">
    {{
        route('depoint.manage.draw.act', [
            'hashid' => $model->hashid,
            'view_file' => 'winners-table'
        ])
    }}
</span>

<table class="table table-striped">

	<thead>
	<tr>
		<td>#</td>
		<td>{{ trans('validation.attributes.name_first') }}</td>
		<td>{{ trans('validation.attributes.mobile') }}</td>
		<td>{{ $__module->getTrans('general.purchase') }}</td>
		<td>&nbsp;</td>
	</tr>
	</thead>

	@if($model->isReadyToDraw())
		@include($__module->getBladePath('manage.draw.winners-add'))
	@endif

	@forelse($model->winner_users as $key => $winner)
		@include($__module->getBladePath('manage.draw.winners-row'))
	@empty
		<tr>
			<td colspan="5">
				<div class="no-results m20">
					{{ $__module->getTrans('cart.drawing.no-winner-so-far') }}
				</div>
			</td>
		</tr>
	@endforelse
</table>
