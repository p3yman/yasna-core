{!!
	widget('modal')
		->label($__module->getTrans('cart.drawing.winners-of', ['title' => $model->title]))
		->target(route('depoint.manage.draw.select'))
!!}

<div class='modal-body'>

	<div id="divWinnersTable" class="panel panel-default m10">
		@include($__module->getBladePath('manage.draw.winners-table'))
	</div>
</div>

@if($model->isNotReadyToDraw())
	<div class="modal-footer">
		@include("manage::forms.button" , [
			'label' => $__module->getTrans('cart.drawing.redraw'),
			'shape' => "primary",
			'link' => "masterModal('"
				. route('depoint.manage.draw', ['hashid' => $model->hashid])
				. "')",
		])
	</div>
@endif
