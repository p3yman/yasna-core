<div class="row">
	<div class="col-xs-12" id="main-grid">
		<span class="refresh">{{ SpareProvidersTools::refreshGridLink() }}</span>

		@include("manage::widgets.grid" , [
			'headings'          => $browse_headings,
			'row_view'          => 'depoint::manage.providers.browse-row',
			'table_id'          => 'tbl-providers',
			'handle'            => 'counter',
			'operation_heading' => true,
		])
	</div>
</div>
