{!!
	widget('modal')
		->target(route('depoint.manage.providers.save'))
    	->labelIf($model->exists, trans('depoint::spare-parts.provider.edit-something', ['title' => $model->title]))
    	->labelIf(!$model->exists, trans('depoint::spare-parts.provider.create-new'))
!!}

<div class="modal-body">
	{!! widget('hidden')->name('id')->value($model->hashid) !!}


	{!!
		widget('input')
			->inForm()
			->name('name')
			->label(trans_safe('depoint::validation.attributes.name'))
			->value($model->name)
	!!}


	{!!
		widget('input')
			->inForm()
			->name('telephone')
			->label(trans_safe('depoint::validation.attributes.telephone'))
			->value($model->telephone)
	!!}


	{!!
		widget('textarea')
			->inForm()
			->name('address')
			->label(trans_safe('depoint::validation.attributes.address'))
			->value($model->address)
	!!}
</div>


<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
