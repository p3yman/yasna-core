<td>
	@php $telephone = $model->telephone @endphp
	@if ($telephone)
		@include("manage::widgets.grid-text" , [
			'text' => $telephone,
			'class' => "font-yekan" ,
			'size' => "14",
			'link' => 'url:tel:' . $telephone
		])
	@endif
</td>
