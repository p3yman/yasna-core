@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => SpareProvidersTools::refreshRowLink($model),
])

@include('depoint::manage.providers.browse-row-name')
@include('depoint::manage.providers.browse-row-telephone')
@include('depoint::manage.providers.browse-row-address')


@include("manage::widgets.grid-actionCol"  , [
	'actions' => [
		[
			'pencil',
			trans('manage::forms.button.edit'),
			SpareProvidersTools::editRowLink($model),
			$model->can('edit'),
		],
		[
			'users',
			trans_safe('depoint::spare-parts.provider.managers'),
			SpareProvidersTools::managersRowLink($model),
			$model->can('edit'),
		],
		[
			'trash-o',
			trans('manage::forms.button.soft_delete'),
			SpareProvidersTools::deleteRowLink($model),
			($model->can('delete') and !$model->trashed())
		],
		[
			'recycle',
			trans('manage::forms.button.undelete'),
			SpareProvidersTools::undeleteRowLink($model),
			($model->can('bin') and $model->trashed())
		],
	],
])
