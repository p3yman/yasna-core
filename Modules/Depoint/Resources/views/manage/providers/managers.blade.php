@php $role_options = SpareProvidersTools::allProviderUsersCombo() @endphp

<style>
	.bootstrap-select.btn-group.show-tick .dropdown-menu li.selected a span.check-mark {
		right: auto;
		left: 15px;
	}
</style>


{!!
	widget('modal')
		->target(route('depoint.manage.providers.save-managers'))
    	->label(trans('depoint::spare-parts.provider.edit-managers-of', ['title' => $model->name]))
!!}


<div class="modal-body">
	{!! widget('hidden')->name('id')->value($model->hashid) !!}

	{!!
		widget('selectize')
			->inForm()
			->searchable()
			->name('managers')
			->label(trans_safe('depoint::spare-parts.provider.managers'))
			->valueField('id')
			->options($role_options)
			->value(implode(',', $model->managers->pluck('hashid')->toArray()))
	!!}
</div>


<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>
