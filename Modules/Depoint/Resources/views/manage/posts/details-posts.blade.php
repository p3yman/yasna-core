<div class="panel">
	<div class="panel-body">
		{!!
			widget('selectize')
				->inForm()
				->options(depoint()->getProductsDetailsCombo($model))
				->name('details_posts')
				->valueField('id')
				->value($model->getMeta('details_posts'))
				->label($__module->getTrans('general.details-posts'))
		!!}
	</div>
</div>
