{!!
	widget('modal')
		->target(route('depoint.manage.posts.color-images.save'))
		->label(currentModule()->trans('general.colors-images'))
!!}

<div class="modal-body">
	{!! widget('hidden')->name('id')->value($model->hashid) !!}

	@foreach ($uploaders_info as $color => $uploader_info)
		<fieldset class="color-box" data-color="{{ $color }}">
			<legend>
				{{ $uploader_info['title'] }}
			</legend>
			<div class="color-box-items row" style="margin-bottom: 20px">
				@php $current_files = ($model->getMeta('color_images')[$color] ?? []) @endphp

				@foreach($current_files as $file_hashid)
					@include(currentModule()->bladePath('manage.posts.colors-image-item'), [
						'file_hashid' => $file_hashid,
						'color' => $color,
					])
				@endforeach
			</div>

			<div class="mv-lg">
				{!! $uploader_info['uploader']->render() !!}
			</div>
		</fieldset>

	@endforeach

</div>

<div class="modal-footer">
	@include("manage::forms.buttons-for-modal")
</div>


<script>
    function imageUploaded(file) {
        let preview_element = file.previewElement;
        let response        = JSON.parse(file.xhr.response);

        if (!response.success) {
            return;
        }

        let file_hashid = response.file;

        if (!file_hashid) {
            return;
        }

        let upload_box   = $(preview_element.closest('.dropzone'));
        let uploader_var = eval(upload_box.data('var-name'));
        let color_box    = $(upload_box.closest('.color-box'));
        let color        = color_box.data('color');
        let items_box    = color_box.find('.color-box-items');
        let uri          = "{{
			route('depoint.manage.posts.color-images.item-preview', [
				'color' => '__COLOR__',
				'file_hashid' => '__FILE__',
			])
		}}";
        let url          = uri.replace('__COLOR__', color).replace('__FILE__', file_hashid);

        $.ajax({
            url    : url,
            success: function (rs) {
                items_box.append(rs);
                uploader_var.removeFile(file);
            }
        })
    }

    $(document).ready(function () {
        $(document).on({
            click: function () {
                $(this).closest('.js_media').remove();
            }
        }, '.remove-color-image-item')
    });
</script>
