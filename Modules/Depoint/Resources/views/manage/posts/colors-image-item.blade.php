@php $file = fileManager()->file($file_hashid)->resolve() @endphp
<div class="js_media well p0 mh" style="display: inline-block">
	{!! widget('hidden')->name("color_images[$color][]")->value($file_hashid) !!}
	<div style="position: relative;">
		<a href="{{ $file->getUrl() }}" target="_blank" style="width: 150px; height: 100px; display: inline-block;">
			{!! $file->elementStyle('width: 100%; height: 100%;object-fit: cover;')->show() !!}
		</a>
		<button type="button" class="remove-color-image-item btn btn-danger btn-xs" style="position: absolute; top: 3px; right: 3px;">
			<span class="fa fa-times"></span>
		</button>
	</div>
</div>

