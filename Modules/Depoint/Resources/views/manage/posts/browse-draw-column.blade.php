@include("manage::widgets.grid-text" , [
    'condition' => $model->hasAnyReceipt(),
    'text' => $__module->getTrans('cart.drawing.receipts-count-amount' , [
        'count' => ad(number_format($model->total_receipts_count)),
        'amount' => ad(number_format(depoint()->shop()->convertToMainCurrency($model->total_receipts_amount))),
        'currency' => depoint()->shop()->currencyTrans(),
    ])
])

@include("manage::widgets.grid-badge" , [
    'condition' => $model->isDrawableNow() and $model->hasNotAnyDrawingWinner(),
    'text' => $__module->getTrans('cart.drawing.draw.singular'),
    'icon' => "gift",
    'link' => 'modal:' . route('depoint.manage.draw', ['hashid' => $model->hashid], false),
    'color' => 'primary',
])

@include("manage::widgets.grid-tiny" , [
    'condition' => $model->hasAnyDrawingWinner(),
    'text' => $__module->getTrans('cart.drawing.some-winners', ['number' => ad($model->total_winners_count)]),
    'icon' => "smile-o",
    'color' => "primary",
    'link' => 'modal:' . route('depoint.manage.draw.winners', ['hashid' => $model->hashid], false),
])
