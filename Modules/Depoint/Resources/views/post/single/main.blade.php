@extends(currentModule()->bladePath('layout.plane'))

@section('content')
	<div class="container">

		<div class="page">
			{!! $header !!}
			<div class="row">

				<div class="col-sm-10 col-center">

					<div class="page-content" data-aos="fade-down" data-aos-delay="500">

						{!! $slot !!}

						@include(currentModule()->bladePath('post.single.page-footer'))

					</div>

					@if(isset($related_posts) and count($related_posts['posts']))
						@include(currentModule()->bladePath('post.single.related_posts'),[
							"title" => $related_posts['title'] ,
							"related_posts" => $related_posts['posts'] ,
						])
					@endif
				</div>

			</div>

		</div>

	</div>
@stop
