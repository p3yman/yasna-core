@if ($related_posts->count())
	@include(currentModule()->bladePath('layout.widgets.part-title'),[
		"title" => $title ,
		"text_class" => "small" ,
	])

	@foreach($related_posts as $post)
		@component(currentModule()->bladePath('layout.widgets.rows'),[
			"col_count" => "3" ,
			"loop" => $loop ,
		])

			<div class="col-sm-4">
				@include(currentModule()->bladePath('layout.widgets.news-item'),[
					"title" => $post->title ,
					"link" => $post->direct_url ,
					"abstract" => $post->abstract ,
					"image" => fileManager()
					 	->file($post->featured_image)
					 	->posttype($post->posttype)
					 	->config('featured_image')
					 	->version('thumb')
					 	->getUrl(),
					"class" => "style-2" ,
				])
			</div>

		@endcomponent
	@endforeach

@endif
