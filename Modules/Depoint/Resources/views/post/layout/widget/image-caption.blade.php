<div class="image">
	<img src="{{ $image }}">
	<p class="caption">
		{{ $caption }}
	</p>
</div>
