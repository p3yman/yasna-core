<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<!-- Load styles -->
	<style>
		body {
			padding: 0;
			margin: 0;
			direction: rtl;
			font-family: Vazir, Tahoma, Arial, sans-serif;
		}

		.head {
			padding: 30px 0;
			-webkit-box-shadow: 0 5px 15px 0 rgba(0, 0, 0, 0.1);
			box-shadow: 0 5px 15px 0 rgba(0, 0, 0, 0.1);
			border-bottom: 1px solid #e7e7e7;
		}

		.title {
			text-align: center;
			color: #009688;
			padding: 30px 0 30px;
			border-bottom: 1px dashed #e7e7e7;
			font-size: 24px;
			font-weight: bold;
		}

		p {
			font-size: 13px;
			color: #444;
			line-height: 2;
			margin: 0 0 20px;
		}

		.action {
			text-align: center;
		}

		button {
			cursor: pointer;
			display: inline-block;
			outline: 0;
			border: 0;
			vertical-align: middle;
			background: #61CEED;
			color: #fff;
			font-weight: normal;
			font-size: 16px;
			font-family: inherit;
			line-height: 1.3;
			padding: 20px 32px;
			text-transform: none;
			text-shadow: none;
			font-style: normal;
			text-align: center;
			text-decoration: none;
			border-radius: 3px;
			transition: all 0.2s ease-in-out;
			white-space: nowrap;
			text-align: center;
			margin: 10px 0;
		}

		footer {
			margin-top: 30px;
			border-top: 1px dashed #e7e7e7;
			padding: 30px;
			background: #fafafa;
			font-size: 12px;
			color: #777;
			text-align: center;
		}

		.line {
			margin: 0 0 5px;
		}

		h1, h2, h3, h4, h5, h6 {
			color: #333;
		}

		table.bordered {
			width: 100%;
			border: 1px solid #e7e7e7;
			border-collapse: collapse;
			margin: 20px 0;
		}

		table.bordered thead {
			background: #eff9fd;
		}

		table.bordered td, table.bordered th {
			border: 1px solid #e7e7e7;
			font-size: 13px;
			color: #777777;
			padding: 10px;
		}
	</style>

	@if (isLangRtl())
		<style>
			* {
				text-align: right;
				direction: rtl;
			}
		</style>
	@endif
</head>
<body>

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" class="head" colspan="100%">
			<img src="{{ Module::asset('depoint:images/logo.png') }}">
		</td>
	</tr>
	<tr>
		<td align="center">
			<table class="content" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="body" width="100%" cellpadding="0" cellspacing="0">
						<table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">
							<tr>
								<td class="title">@yield('title')</td>
							</tr>
							@yield('content')
							<tr>
								<td>
									<footer>
										<div class="copyright">
											{{ trans('depoint::general.copyright', [
												'title' => get_setting('site_title'),
											]) }}
										</div>
									</footer>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
