@extends('depoint::email.layouts.main')

@section('content')
	<tr>
		<td>
			{{ trans_safe('depoint::validation.attributes.code') }}:
			{{ $model->code}}
		</td>
	</tr>
	<tr>
		<td>
			{{ trans_safe('depoint::spare-parts.provider.title.singular') }}:
			{{ $model->provider->name }}
		</td>
	</tr>
	<tr>
		<td>
			{{ trans_safe('depoint::spare-parts.headline.registrar') }}:
			{{ $model->user->full_name}}
		</td>
	</tr>

	@yield('middle_content')

	{{--<tr>--}}
	{{--<td class="action">--}}
	{{--<button>--}}
	{{--{{ trans('depoint::general.view_order') }}--}}
	{{--</button>--}}
	{{--</td>--}}
	{{--</tr>--}}

	<tr>
		<td>

			<table class="bordered" border="1" cellpadding="0" cellspacing="0">
				<thead>
				<tr>
					<th>
						{{ trans_safe('depoint::spare-parts.spare-part.title.singular') }}
					</th>
					<th>
						{{ trans_safe('depoint::validation.attributes.count') }}
						({{ trans_safe('depoint::spare-parts.unit.numbers') }})
					</th>
				</tr>
				</thead>
				<tbody>
				@php $order_items = $model->items @endphp
				@foreach($order_items as $item)
					<tr class="product-row">
						<td>
							<div>
								{{ $item->spare_part->name  }}
							</div>
						</td>
						<td>
							{{ ad($item->count) }}
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</td>
	</tr>
@append
