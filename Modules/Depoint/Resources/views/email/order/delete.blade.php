@extends('depoint::email.order.plane')

@section('title')
	{{ trans_safe('depoint::spare-parts.order.deletion-alert') }}
@append

@section('middle_content')
	<tr>
		<td>
			{{ trans_safe('depoint::spare-parts.order.deleter') }}:
			{{ $user->full_name}}
		</td>
	</tr>

	<tr>
		<td>
			{{ ad(echoDate($time, 'j F Y - H:i')) }}
		</td>
	</tr>
@append
