@extends('depoint::email.order.plane')

@section('title')
	{{ trans_safe('depoint::spare-parts.order.creation-alert') }}
@append


@section('middle_content')
	<tr>
		<td>
			{{ ad(echoDate($time, 'j F Y - H:i')) }}
		</td>
	</tr>
@append
