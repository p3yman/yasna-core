@extends('depoint::email.layouts.main')

@section('title')
	{{ trans_safe('depoint::general.auth.password_recovery') }}
@append

@section('content')
	<tr>
		<td>
			{{ trans_safe('depoint::general.auth.reset_mail_message.part1', ['name' => $user->full_name ]) }}
		</td>
	</tr>
	<tr>
		<td>
			{{ trans_safe('depoint::general.auth.reset_mail_message.part2') }}
		</td>
	</tr>

	<tr>
		<td>
			{{ trans_safe('depoint::general.auth.reset_password_token') }}:
			{!! $token !!}
		</td>
	</tr>

	<tr>
		<td>
			{{ ad(echoDate($time, 'j F Y - H:i')) }}
		</td>
	</tr>
@append
