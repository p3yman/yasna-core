@extends('depoint::email.layouts.main')

@section('title')
	{{ trans_safe('depoint::spare-parts.spare-part.inventory-alert') }}
@append

@section('content')
	<tr>
		<td>
			{{ ad(echoDate($time, 'j F Y - H:i')) }}
		</td>
	</tr>

	<tr>
		<td>

			<table class="bordered" border="1" cellpadding="0" cellspacing="0">
				<thead>
				<tr>
					<th>
						{{ trans_safe('depoint::validation.attributes.name') }}
					</th>
					<th>
						{{ trans_safe('depoint::validation.attributes.inventory') }}
						({{ trans_safe('depoint::spare-parts.unit.numbers') }})
					</th>
					<th>
						{{ trans_safe('depoint::validation.attributes.daily_use') }}
						({{ trans_safe('depoint::spare-parts.unit.numbers') }})
					</th>
					<th>
						{{ trans_safe('depoint::validation.attributes.production_days') }}
						({{ trans_safe('depoint::spare-parts.unit.days') }})
					</th>
					<th>
						{{ trans_safe('depoint::validation.attributes.alert_days') }}
						({{ trans_safe('depoint::spare-parts.unit.days') }})
					</th>
				</tr>
				</thead>
				<tbody>
				@foreach($spare_parts as $spare_part)
					<tr class="product-row">
						<td>
							{{ $spare_part->name  }}
						</td>
						<td>
							{{ ad($spare_part->inventory) }}
						</td>
						<td>
							{{ ad($spare_part->daily_use) }}
						</td>
						<td>
							{{ ad($spare_part->production_days) }}
						</td>
						<td>
							{{ ad($spare_part->alert_days) }}
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</td>
	</tr>
@append
