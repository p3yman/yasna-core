@extends($__module->getBladePath('email.layouts.main'))


@section('content')
	<tr>
		<td>
			{{
				$__module->getTrans('cart.receipt.creation-text.part1', [
					'name' => $cart->user->full_name
				])
			}}
			<br>
			{{
				$__module->getTrans('cart.receipt.creation-text.part2', [
					'code' =>  $cart->code
				])
			}}
		</td>
	</tr>
	<tr>
		<td>
			{{ ad($receipt->code) }}
		</td>
	</tr>
@append
