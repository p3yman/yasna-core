@if($model->depointWares()->count())
	<div class="colors">
		<label>
			{{ trans('depoint::general.form.choosing_color') }}
		</label>
		@foreach($model->depointWares()->get() as $ware)
			@if(!$ware->checkAvailability())
				@continue
			@endif

			@if(is_null($ware_hashid) and $loop->iteration==1)
				@php $ware_hashid=$ware->hashid @endphp
			@endif

			@php $color = getWareColor($ware) @endphp
			@if(!empty($color))
				<div class="color_checkbox" id="{{ $ware->hashid }}">
					<input type="radio" id="color_{{ $ware->hashid }}" name="hashid"
						   value="{{  $ware->hashid }}"
						   @if($ware->hashid === $ware_hashid) checked @endif
					>
					<label for="color_{{  $ware->hashid }}">
						<span style="background: {{ $color['code'] }};"></span>
						{{ $color['title'] }}
					</label>
				</div>
			@endif
		@endforeach
	</div>
@endif
