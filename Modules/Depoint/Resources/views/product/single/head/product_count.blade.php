<div class="qty">
	<label for="product-count">
		{{ currentModule()->trans('general.form.count') }}
	</label>
	<input type="number" id="product-count" value="1" min="1" name="count">
</div>
