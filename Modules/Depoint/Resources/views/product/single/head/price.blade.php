@if($model->depointWares()->count())
	@foreach($model->depointWares()->get() as $ware)
		@if(!$ware->checkAvailability())
			@continue
		@endif

		@if(is_null($ware_hashid) and $loop->iteration==1)
			@php($ware_hashid=$ware->hashid)
		@endif

		<div class="price" id="price_{{$ware->hashid}}" @if($ware->hashid !== $ware_hashid) style="display: none" @endif>

			@if($ware->isOnSale())
				<del>
					{{ depointTools()->getWarePrice($ware) }}
				</del>
				<ins>
					{{ depointTools()->getWarePrice($ware,true) }}
				</ins>
			@else
				<ins>
					{{ depointTools()->getWarePrice($ware) }}
				</ins>
			@endif
		</div>
	@endforeach
@endif