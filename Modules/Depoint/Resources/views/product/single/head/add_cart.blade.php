<div class="actions mt40">
	<button data-aos="fade-down" data-aos-delay="250" class="button blue lg">
		{{ currentModule()->trans('general.add_to_cart') }}
	</button>
</div>

@section('end-body')
	<script>
        $(document).ready(function () {
            let form        = $('form[action="{{ route_locale('depoint.cart.add') }}"]');
            let ware_inputs = form.find('input[name=hashid]');
            let submit      = form.find(':submit');

            if (!ware_inputs.length) {
                submit.remove();
            }
        });
	</script>
@append
