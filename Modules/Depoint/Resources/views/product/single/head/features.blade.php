@if(!empty($features))
	<ul data-aos="fade-down" data-aos-delay="200">
		@foreach($features as $feature)
			<li>{{ $feature }}</li>
		@endforeach
	</ul>
@endif