@if($specs_pivot_parent->count())
	<div class="product-single-part">
		@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"title" => currentModule()->trans('general.technical_spec') ,
			])

		<div class="row">
			<div class="col-sm-10 col-center">
				<div class="specs">


					@foreach($specs_pivot_parent as $parent_id => $children)
						@php $parent_spec = model('spec', $parent_id); @endphp
						@include(currentModule()->bladePath('product.layout.table'),[
							"title" => $parent_spec->title ,
							"rows" => $children ,
						])
					@endforeach

				</div>
			</div>
		</div>

	</div>
@endif