@if($products->count())
	<div class="product-single-part">
			@include(currentModule()->bladePath('layout.widgets.part-title'),[
			"title" =>currentModule()-> trans('general.similar_products') ,
		])

		@foreach($products as $product)
			@component(currentModule()->bladePath('layout.widgets.rows'),[
				"col_count" => "2" ,
				"loop" => $loop ,
			])
				<div class="col-sm-6">
					@include(currentModule()->bladePath('layout.widgets.product-item'),[
     							"image" => fileManager()->file($product->featured_image)->posttype($product->posttype)->config('featured_image')->getUrl() ,
								"title" => $product->title ,
								"abstract" => $product->abstract ,
								"link"	 => $product->direct_url ,
							])
				</div>
			@endcomponent
		@endforeach

	</div>
@endif