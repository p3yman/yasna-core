@extends(currentModule()->bladePath('layout.plane'))

@php
	depointTemplate()->appendToPageTitle($posttype_title);
	depointTemplate()->appendToPageTitle($post_title);

	$post_image = fileManager()
		->file($model->featured_image)
		->getUrl();

	if ($post_image) {
		depointTemplate()->mergeWithOpenGraphs([
			'image' => $post_image
		]);
	}
@endphp


@section('content')

	<div class="container">

		<div class="product-single">

			@include(currentModule()->bladePath('product.single.head.main'))

			@include(currentModule()->bladePath('product.single.desc.main'),[
				"desc" => $model->text ,
			])

			@include(currentModule()->bladePath('product.single.technical-spec.main'),[
			"specs_pivot_parent" => $specs_pivot_parent ,
			])

			@include(currentModule()->bladePath('product.single.similar.main'),[
					"products" => $model->similars(2) ,
			])

		</div>

	</div>

@stop


@section('end-body')
	<script>

        $(document).ready(function () {
            $(".color_checkbox input[type=radio]").change(function () {
                console.log("test");
                var id = $(this).val();
                $(".price").hide();
                $("#price_" + id).show();
            });
        })
	</script>
@append
