@extends('depoint::layout.plane')


@section('content')

	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"title" => trans('depoint::general.compare_products') , 
			])

			<div class="products-compare">
				<div class="table-scroll">
					<table class="table bordered">

						<thead>
						<tr>
							<th class="empty"></th>
							@foreach($posts as $post)
								<th class="head">
									<img src="{{ fileManager()->file($post->featured_image)->posttype($post->posttype)->config('featured_image')->version('thumb')->getUrl() }}">
									<div class="title">{{ $post->title or "-" }}</div>
									@php
										if($post->posttype->slug=='stocks'){
										 	$min_price_ware = depointTools()->getMinimumPriceOfProduct($post,true);
										 	}
									    else{
									    	$min_price_ware = depointTools()->getMinimumPriceOfProduct($post);
										}
									@endphp
									<div class="price">
										@if(!is_null($min_price_ware))
											@if($min_price_ware->isOnSale())
												{{--<del>--}}
												{{--{{ depointTools()->getWarePrice($min_price_ware) }}--}}
												{{--</del>--}}
												{{--<br>--}}
												<ins>
													{{ depointTools()->getWarePrice($min_price_ware,true) }}
												</ins>
											@else
												<ins>
													{{ depointTools()->getWarePrice($min_price_ware) }}
												</ins>
											@endif
										@else
											{{ depointTools()->getWarePrice(null) }}
										@endif
									</div>
									<a href="{{ $post->direct_url }}" class="button blue block">
										{{ currentModule()->trans('general.view_and_purchase') }}
									</a>
								</th>
							@endforeach
						</tr>
						</thead>

						<tbody>



						@foreach($groups as $category)
							@php $children = $category->children; @endphp
							@if(!$children->count())
								@continue
							@endif

							<tr class="row-title">
								<td colspan="100%">
									{{ $category->title }}
								</td>
							</tr>
							@foreach($children as $child)
								<tr class="row-data">
									<td>
										{{ $child->titleIn(getLocale()) }}
									</td>

									@foreach($posts as $post)
										@php $spec = $posts_specs[$post->id]->where('spec_id', $child->id)->first() @endphp

										<td>
											@if($child->input_type == 'boolean')
												<span class="{{ $spec->value ? 'icon-check color-green': 'icon-close color-red' }} fz-lg"></span>
											@else
												{{ $spec->value }}
											@endif
										</td>
									@endforeach

								</tr>
							@endforeach
						@endforeach

						</tbody>

					</table>

				</div>


			</div>

		</div>

	</div>

@stop
