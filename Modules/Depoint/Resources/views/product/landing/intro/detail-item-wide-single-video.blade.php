@php
	$aparat_hash = depointTools()->getAparatHash($file['link']);
	$player_id = "video-player-" . $post->hashid;
@endphp


<div id="{{ $player_id }}">
	<script type="text/JavaScript"
			src="https://www.aparat.com/embed/{{ $aparat_hash }}?data[rnddiv]={{ $player_id }}&data[responsive]=yes"></script>
</div>

<div class="content-wrapper">
	<p>{!! $post->text !!}</p>
</div>
