@isset($details_posts)
	@if (!empty($details_posts['wide']))
		@foreach($details_posts['wide'] as $post)
			@include(currentModule()->bladePath('product.landing.intro.detail-item-wide', [
				'post' => $post,
			]))
		@endforeach
	@endif

	@if (!empty($details_posts['minimal']))
		@foreach($details_posts['minimal'] as $post)
			@include(currentModule()->bladePath('product.landing.intro.detail-item-minimal', [
				'post' => $post,
			]))
		@endforeach
	@endif
@endisset
