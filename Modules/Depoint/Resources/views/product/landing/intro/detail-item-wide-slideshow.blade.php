@include(currentModule()->bladePath('product.landing.gallery.main', [
	'files' => $files
]))


<div class="container">

	<div class="product-part box">

		<div class="content">

			<div class="content-wrapper">
				<p>{!! $post->text !!}</p>
			</div>

		</div>
	</div>
</div>
