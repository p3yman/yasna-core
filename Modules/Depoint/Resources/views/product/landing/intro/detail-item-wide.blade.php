@php
	$files = $post->files;
	$files_count = count($files);
@endphp

@if ($files_count)
	@if ($files_count > 1)
		@include(currentModule()->bladePath('product.landing.intro.detail-item-wide-slideshow'), [
			'post' => $post,
			'files' => $files,
		])
	@else
		@include(currentModule()->bladePath('product.landing.intro.detail-item-wide-single'), [
			'post' => $post,
			'file' => array_first($files),
		])
	@endif
@endif
