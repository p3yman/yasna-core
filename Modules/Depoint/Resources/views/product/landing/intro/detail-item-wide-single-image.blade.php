@php
	$image_url = fileManager()
		->file($file['src'])
		->posttype($products_details_posttype)
		->version('1440x1026')
		->config('attachments')
		->getUrl()
@endphp

<img src="{{ $image_url }}">

<div class="content-wrapper">
	<p>{!! $post->text !!}</p>
</div>
