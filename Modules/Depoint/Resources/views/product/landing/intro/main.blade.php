<div class="container">

	<div class="product-part box" id="product-intro">

		<div class="content">
			@include('depoint::layout.widgets.part-title',[
				"title" => trans('depoint::general.product_intro') ,
				"hide_title" => trans('depoint::general.product_intro') ,
			])

			<div class="content-wrapper">
				<p>{!! $post->text !!}</p>
			</div>

		</div>

	</div>

</div>

@include(currentModule()->bladePath('product.landing.intro.details'))
