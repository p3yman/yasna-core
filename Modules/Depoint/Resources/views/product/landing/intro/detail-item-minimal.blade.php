@php $files = $post->files @endphp

@if (!empty($files))

	@php
		$file = array_first($files);
		$image_url = fileManager()
			->file($file['src'])
			->posttype($products_details_posttype)
			->version('400x285')
			->config('attachments')
			->getUrl()
	@endphp

	<div class="container">

		<div class="product-part box">

			<div class="content">
				<div class="content-wrapper">
					<div class="row">
						@if($loop->index % 2 === 0)

							<div class="col-sm-6">
								<img src="{{ $image_url }}">
							</div>

							<div class="col-sm-6">
								<div class="part-title">
									<div class="subtitle">
										{{ $post->title }}
									</div>
								</div>
								{!! $post->text !!}
							</div>

						@else

							<div class="col-sm-6">
								<div class="part-title">
									<div class="subtitle">
										{{ $post->title }}
									</div>
								</div>
								{!! $post->text !!}
							</div>

							<div class="col-sm-6">
								<img src="{{ $image_url }}">
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

@endif
