<div class="container">

	<div class="product-part box">

		<div class="content">

			@if ($file['link'])
				@include(currentModule()->bladePath('product.landing.intro.detail-item-wide-single-video'), [
					'post' => $post,
					'file' => $file,
				])
			@else
				@include(currentModule()->bladePath('product.landing.intro.detail-item-wide-single-image'), [
					'post' => $post,
					'file' => $file,
				])
			@endif

		</div>
	</div>
</div>
