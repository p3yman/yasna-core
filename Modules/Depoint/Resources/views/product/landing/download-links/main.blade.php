@php
	$download_links = depoint()->getLandingDownloadLinks($post);
@endphp

@if(isset($download_links) and count($download_links))
	<div class="product-part box last" id="product-download">

		<div class="content">

			@include('depoint::layout.widgets.part-title',[
				"title" => trans('depoint::general.download') ,
				"hide_title" => trans('depoint::general.download') ,
			])

			<div class="content-wrapper downloads">

				<div class="d-ib w-100 ta-c mb50">

					@foreach($download_links as $link)
						<a href="{{ $link['link'] }}" class="download" target="_blank">
							<img src="{{ $link['image'] }}">
							<span>{{ $link['title'] }}</span>
						</a>
					@endforeach

				</div>

			</div>

		</div>

	</div>

@endif
