@if(isset($files) and count($files))
	<div class="product-gallery">

		<ul class="rslides">
			@foreach($files as $file)
				@php
					$image_url = fileManager()
						->file($file['src'])
						->getUrl()
				@endphp

				<li style="background-image: url('{{ $image_url }}');"></li>
			@endforeach
		</ul>

	</div>
@endif

