@extends(currentModule()->bladePath('layout.plane'))

@php
	$post_image = fileManager()
		->file($post->featured_image)
		->getUrl();

	if ($post_image) {
		depointTemplate()->mergeWithOpenGraphs([
			'image' => $post_image
		]);
	}
@endphp

@section('content')
	<!-- Head -->
	@include(currentModule()->bladePath('product.landing.head.main'))

	<!-- intro -->
	@include(currentModule()->bladePath('product.landing.intro.main'))

	<div class="container">

		<!-- Technical Info -->
		@include(currentModule()->bladePath('product.landing.technical-info.main'),[
			"specs_pivots_parents" => depoint()->getPostSpecsParentItems($post) ,
		])

		<!-- Download Links -->
		@include(currentModule()->bladePath('product.landing.download-links.main'))

	</div>

@stop


@include(currentModule()->bladePath('product.landing.scripts.main'))
