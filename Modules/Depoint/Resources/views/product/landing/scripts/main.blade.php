@section('end-body')
	<script>
        $(document).ready(function () {
            $('.js-actions').find('a[href^="#"]').each(function () {
                let that   = $(this);
                let href   = that.attr('href');
                let target = $(href);

                if (!target.length) {
                    that.remove();
                }
            });
        });
	</script>
@append
