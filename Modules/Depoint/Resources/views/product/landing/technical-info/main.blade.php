@if($specs_pivots_parents->count())
	<div class="product-part box" id="product-specs">

		<div class="content">
			@include('depoint::layout.widgets.part-title',[
				"title" => trans('depoint::general.technical_spec') ,
				"hide_title" => trans('depoint::general.technical_spec') ,
			])

			<div class="content-wrapper specs">
				@foreach($specs_pivots_parents as $parent_id => $children)
					@php $parent_spec = $children->first()->spec->parent; @endphp
					@include('depoint::product.layout.table',[
						"title" => $parent_spec->title ,
						"rows" => $children ,
					])
				@endforeach
			</div>

		</div>
	</div>
@endif
