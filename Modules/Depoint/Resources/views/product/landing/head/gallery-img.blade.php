@php
	$image_url = fileManager()
		->file($image_hashid)
		->getUrl()
@endphp
<img src="{{ $image_url }}" class="fluid">
