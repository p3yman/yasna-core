@php depointTemplate()->appendToPageTitle($post->title) @endphp

@include(currentModule()->bladePath('product.landing.head.colors-loop'))

<div class="product-head">

	<div class="container">

		<div class="head-wrapper">

			@include(currentModule()->bladePath('product.landing.head.images'))

			<div class="content">

				@include('depoint::layout.widgets.part-title',[
					"title" => $post->title,
					"subtitle" => $post->title2,
				])
				@if(count($features))
					<ul data-aos="fade-down" data-aos-delay="200">
						@foreach($features as $feature)
							<li>{{ $feature }}</li>
						@endforeach
					</ul>
				@endif

				@include(currentModule()->bladePath('product.landing.head.colors'))

				<div class="actions mt40 js-actions">
					<a href="{{ $post->direct_url }}" data-aos="fade-down" data-aos-delay="250" class="button blue">
						<span class="icon-search"></span>
						{{ trans('depoint::general.purchase') }}
					</a>
					<a href="#product-intro" data-aos="fade-down" data-aos-delay="300"
					   class="button gray-light inverted">
						<span class="icon-quote-right"></span>
						{{ trans('depoint::general.introduction') }}
					</a>
					<a href="#product-specs" data-aos="fade-down" data-aos-delay="350"
					   class="button gray-light inverted">
						<span class="icon-bars"></span>
						{{ trans('depoint::general.technical_spec') }}
					</a>
					<a href="#product-download" data-aos="fade-down" data-aos-delay="400"
					   class="button gray-light inverted">
						<span class="icon-angle-down"></span>
						{{ trans('depoint::general.download') }}
					</a>
				</div>

			</div>

		</div>

	</div>

</div>
