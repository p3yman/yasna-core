@section('images-item')
	<li class="color-gallery">
		<ul class="color-item-gallery">
			@foreach($images as $image)
				@include(currentModule()->bladePath('product.landing.head.gallery-img'), [
					'image_hashid' => $image,
				])
			@endforeach
		</ul>
	</li>
@append
