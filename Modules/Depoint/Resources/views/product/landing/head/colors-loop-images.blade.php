@php
	$color_slug = $color['slug'];
	$images = ($color_images[$color_slug] ?? []);
@endphp

@if (empty($images))
	@include(currentModule()->bladePath('product.landing.head.image-item-single'), [
		'image_hashid' => $post->featured_image
	])
@elseif (count($images) == 1)
	@include(currentModule()->bladePath('product.landing.head.image-item-single'), [
		'image_hashid' => $images[0]
	])
@else
	@include(currentModule()->bladePath('product.landing.head.image-item-gallery'), [
		'images' => $images
	])
@endif
