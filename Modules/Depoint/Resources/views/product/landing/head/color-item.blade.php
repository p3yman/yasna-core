@section('color-items')
	<a href="#" class="item simptip-position-top simptip-smooth simptip-fade aos-init aos-animate"
	   data-tooltip="{{ $color['title'] }}" data-aos="fade-down" data-aos-delay="350">
		<span style="background: {{ $color['code'] }};"></span>
	</a>
@append
