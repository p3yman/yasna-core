@php $color_images = $post->color_images @endphp

@foreach($post->depointWares()->get() as $ware)

	@php $ware_hashid = $ware->hashid @endphp

	@php
		$color = depoint()->shop()->getWareColorInfo($ware);
	@endphp

	@if(!empty($color))
		@include(currentModule()->bladePath('product.landing.head.color-item'))
	@endif

	@include(currentModule()->bladePath('product.landing.head.colors-loop-images'), [
		'color' => $color,
	])

@endforeach
