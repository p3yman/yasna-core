<div class="part-title">
	<div class="subtitle">{{ $title }}</div>
</div>
<table class="table bordered">
	<tbody>
	@foreach($rows as $row)
		<tr>
			<td>
				{{ $row->spec->titleIn(getLocale()) }}
			</td>
			<td>
				@if($row->spec->input_type=="boolean")
					@include(currentModule()->bladePath('product.layout.boolean_view'),['value'=>$row->value])
				@else
					{{ ad($row->value) }}
				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
