@if($products->count())

	@foreach($products as $product)
		@include(currentModule()->bladePath('layout.widgets.product-item'),[
			"image" => fileManager()->file($product->featured_image)->posttype($product->posttype)->config('featured_image')->getUrl() ,
			"title" => $product->title ,
			"abstract" => $product->abstract ,
			"link"	 => $product->direct_url ,
		])
	@endforeach

	{!! $products->render() !!}
@else
	@include(currentModule()->bladePath('layout.widgets.alert'), [
		'class' => 'alert-default',
		'text' => currentModule()->trans('general.no-item-to-show')
	])
@endif
