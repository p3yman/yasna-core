@extends(currentModule()->bladePath('layout.plane'))

@php $list_title = (posttype('products')->titleIn(getLocale()) ?: currentModule()->trans('general.products')) @endphp

@php depointTemplate()->appendToPageTitle($list_title) @endphp

@section('content')

	<div class="container">

		<div class="page">
			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"title" => currentModule()->trans('general.products') ,
			])
			@if ($all_products->count())
				<div class="row">
					@include(currentModule()->bladePath('product.list.filter'))
					@include(currentModule()->bladePath('product.list.list'))
				</div>
			@else

				@include(currentModule()->bladePath('layout.widgets.alert'), [
					'class' => 'alert-default',
					'text' => currentModule()->trans('general.no-item-to-show')
				])

			@endif
		</div>

	</div>

@stop


@section('end-body')
	{!! Html::script(currentModule()->asset('js/ajax-filter.min.js')) !!}
@append
