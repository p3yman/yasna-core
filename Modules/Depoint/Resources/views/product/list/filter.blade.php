<div class="col-sm-3 mb50 js-filters-panel" data-filter-url="{{ route_locale('depoint.front.product.filter') }}">

    <a href="#"
       class="button blue block mb10"
       id="filters-toggle"
       data-title-deactive="{{ currentModule()->trans('general.products-filter.toggle_active') }}"
       data-title-active="{{ currentModule()->trans('general.products-filter.toggle_deactive') }}"
    >
        {{ currentModule()->trans('general.products-filter.toggle_active') }}
    </a>

	<div class="filters-content">

        <!-- Filters -->
        <div class="product-filter search-filter filter-text" data-identifier="title">
            <div class="title">{{ currentModule()->trans('general.products-filter.search-name') }}</div>
            <div class="content">
                <div class="field">
                    <div class="icon">
                        <input type="text" placeholder="{{ currentModule()->trans('general.products-filter.search-name') }}">
                        <span class="icon-search"></span>
                    </div>
                </div>
            </div>
        </div>

        @php
            $min_price = depoint()->shop()->postsMinPrice($all_products);
            $min_price = depoint()->shop()->convertToMainCurrency($min_price);
            $max_price = depoint()->shop()->postsMaxPrice($all_products);
            $max_price = depoint()->shop()->convertToMainCurrency($max_price);
        @endphp

        @if ($max_price > $min_price)
            <div class="product-filter price-filter filter-slider" data-identifier="price">
                <div class="title">{{ currentModule()->trans('general.products-filter.price-range') }}</div>
                <div class="content">
                    <div id="price-slider" class="slider-self"
                         data-min="{{ $min_price }}"
                         data-max="{{ $max_price }}"
                    >

                    </div>
                    <div class="values">
                        <div class="from @if(isLangRtl()) vazir-fn @endif">{{ currentModule()->trans('general.products-filter.from') }}:
                            <span id="price-slider-lower"></span>
                            {{ depoint()->shop()->currencyTrans() }}
                        </div>
                        <div class="to @if(isLangRtl()) vazir-fn @endif">{{ currentModule()->trans('general.products-filter.to') }}:
                            <span id="price-slider-upper"></span>
                            {{ depoint()->shop()->currencyTrans() }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="product-filter switch-filter filter-switch-checkbox" data-identifier="available">
            <div class="content">
                <div class="field">
                    <div class="switch green">
                        <input id="switch" type="checkbox" checked="">
                        <label for="switch">
						<span>
							{{ currentModule()->trans('general.products-filter.only-available-wares') }}
						</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


