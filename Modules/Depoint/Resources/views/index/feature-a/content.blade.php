<div class="text">
	<div class="title">
		{{ $innovation->title }}
	</div>
	<p>
		{!! str_limit($innovation->abstract,700) !!}
	</p>
</div>
