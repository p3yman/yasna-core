@if ($innovations_a->count())
	<div id="features-a">

        <div class="divider">
            <img src="{{ depointTemplate()->module()->asset('images/gray-divider.svg') }}">
        </div>

		@include(currentModule()->bladePath('layout.widgets.big-title'),[
			"title" => $__module->getTrans('general.motto.innovation') ,
		])

		<div class="container">

			<div class="row">

				<div class="col-sm-8 col-center">
					@php($orderFlag=true)
					@foreach($innovations_a as $innovation)
						<div class="feature" data-aos="fade-down">
							@if($orderFlag)
								@include(currentModule()->bladePath('index.feature-a.image-first'))
							@else
								@include(currentModule()->bladePath('index.feature-a.content-first'))
							@endif
						</div>
						@php($orderFlag=!$orderFlag)
					@endforeach
				</div>

			</div>

		</div>

	</div>
@endif
