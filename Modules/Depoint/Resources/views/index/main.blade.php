@extends(currentModule()->bladePath('layout.plane'))

@section('content')

	@include(currentModule()->bladePath('index.main-slide.main'))

	@include(currentModule()->bladePath('index.products.main'))

	@include(currentModule()->bladePath('index.feature-a.main'))

	@include(currentModule()->bladePath('index.feature-b.main'))


@stop
