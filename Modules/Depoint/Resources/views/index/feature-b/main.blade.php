@if ($innovations_b->count())
	<div id="features-b">

		@include(currentModule()->bladePath('layout.widgets.big-title'),[
			"bg_title" => trans('depoint::general.motto.design') ,
			"title" => trans('depoint::general.motto.design') ,
		])

		<div class="container">

			<div class="row">

				<div class="col-sm-10 col-center">
					@php($fadeFlag=true)
					@foreach($innovations_b as $post)
						<div class="feature" data-aos="fade-{{($fadeFlag)?'right':'left'}}">
							@php($fadeFlag=!$fadeFlag)

							<div class="img">
								<img src="{{ fileManager()->file($post->featured_image)->posttype($post->posttype)->config('featured_image')->getUrl() }}"
									 class="fluid">
							</div>
							<div class="text">
								<div class="title">
									{{ $post->title }}
								</div>
								<div class="subtitle">
									{{ $post->title2 }}
								</div>
								<div class="row">
									<div class="col-sm-9 col-center mb30">
										{!! str_limit($post->abstract,700) !!}
									</div>
								</div>
								<div class="row">
									<div class="col-sm-5 col-center mb30 ta-c">
										<a href="{{ $post->direct_url }}" class="button red lg block shadow">
											{{ trans('depoint::general.show_more') }}
										</a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>

			</div>

		</div>

	</div>
@endif
