@if ($products->count())
	<div id="products">
		<div class="container">

			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"title" => trans('depoint::general.our_products') ,
			])


			@foreach($products as $product)
				@component(currentModule()->bladePath('layout.widgets.rows'),[
					"col_count" => "2" ,
					"extra" => "data-aos=fade-down" ,
					"loop" => $loop ,
				])

					<div class="col-sm-6">
						@include(currentModule()->bladePath('layout.widgets.product-item'),[
							"image" => fileManager()
									->file($product->featured_image)
									->posttype($product->posttype)
									->config('featured_image')
									->version('thumb')
									->getUrl() ,
							"title" => $product->title ,
							"abstract" => str_limit($product->abstract,200) ,
						//	"colors" => $product['colors'] ,
							"link" => $product->productLandingDirectUrl() ,
							"show_price" => false,
							"show_colors" => false,
						])
					</div>

				@endcomponent
			@endforeach

		</div>

	</div>
@endif
