

<div id="main-slide">

    <ul class="rslides">
        @foreach($slides as $slide)
            <li>
                <img src="{{ fileManager()->file($slide->featured_image)->posttype($slide->posttype)->config('featured_image')->getUrl() }}">
                <div class="text">
                    <div class="content">
                        <h1>
                            {!! $slide->title !!}
                        </h1>
                        <p>
                            {{ str_limit($slide->abstract,600) }}
                        </p>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>

</div>


