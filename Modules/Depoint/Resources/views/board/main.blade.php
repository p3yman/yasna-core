@extends('depoint::layout.plane')
@php
	$board = [
		[
			"image" => Module::asset('depoint:images/sample/person.jpg') , 
			"title" => "پیمان اسکندری" , 
			"position" => "رئیس هیئت مدیره" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/person.jpg') ,
			"title" => "پیمان اسکندری" ,
			"position" => "رئیس هیئت مدیره" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/person.jpg') ,
			"title" => "پیمان اسکندری" ,
			"position" => "رئیس هیئت مدیره" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/person.jpg') ,
			"title" => "پیمان اسکندری" ,
			"position" => "رئیس هیئت مدیره" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/person.jpg') ,
			"title" => "پیمان اسکندری" ,
			"position" => "رئیس هیئت مدیره" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/person.jpg') ,
			"title" => "پیمان اسکندری" ,
			"position" => "رئیس هیئت مدیره" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/person.jpg') ,
			"title" => "پیمان اسکندری" ,
			"position" => "رئیس هیئت مدیره" ,
		],
	];
@endphp
@section('content')
	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"subtitle" => trans('depoint::general.introduction') ,
				"title" => trans('depoint::general.board') ,
			])

			<div class="row">

				<div class="col-sm-10 col-center">

					@foreach($board as $person)
						@component('depoint::layout.widgets.rows',[
							"col_count" => "3" ,
							"loop" => $loop ,
						])
							<div class="col-sm-4">
								@include('depoint::layout.widgets.image-title-box',[
									"image" => $person['image'] , 
									"title" => $person['title'] , 
									"description" => $person['position'] , 
								])
							</div>
						@endcomponent
					@endforeach	

				</div>

			</div>

		</div>

	</div>
@stop
