@extends(currentModule()->bladePath('layout.plane'))

@php depointTemplate()->appendToPageTitle(currentModule()->trans('general.contact_us')) @endphp

@section('content')
	<div class="container">

		<div class="page">

			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"image" => currentModule()->asset('images/icon-contact.svg') ,
				"title" => currentModule()->trans('general.contact_us') ,
			])

			<div class="row">

				<div class="col-sm-10 col-center">

					<div class="page-desc">
						{{ currentModule()->trans('general.contact_methods').":" }}
					</div>

					<div class="mb50">
						@foreach($contact_methods as $method)
							@component(currentModule()->bladePath('layout.widgets.rows'),[
								"col_count" => "3" ,
								"loop" => $loop ,
							])

								<div class="col-sm-4">
									@include(currentModule()->bladePath('contact.contact_box'),[
										"image" => $method['image'] , 
										"value_class" => (isset($method['value_class']))? $method['value_class'] : "" , 
										"value" => $method['value'] , 
									])
								</div>

							@endcomponent
						@endforeach
					</div>


					<div class="row">
						@include(currentModule()->bladePath('contact.form'))
					</div>

				</div>

			</div>

		</div>

	</div>
@stop	
