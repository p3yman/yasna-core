@if ($commenting_post->exists)
	@php $fields = depoint()->commenting()->translateFields($commenting_post->getMeta('fields')) @endphp
	<div class="col-sm-6 col-center">

		{!!
			widget('form-open')
				->target(route_locale('depoint.comments.save'))
				->class('js')
				->id('contact-form')
		!!}

		<div class="page-desc">
			{{ currentModule()->trans('general.send_your_message'). ":" }}
		</div>

		{!! widget('hidden')->name('post_id')->value($commenting_post->hashid) !!}

		@foreach($fields as $field_name => $field_value)

			@switch($field_name)

				@case('name')

				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans("validation.attributes.$field_name") ,
					"field_class" => "lg" ,
					"id" => kebab_case($field_name) ,
					"name" => $field_name ,
					"value" => (user()->id ? user()->full_name : '') ,
				])

				@break


				@case('email')

				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans("validation.attributes.$field_name") ,
					"field_class" => "lg" ,
					"id" => kebab_case($field_name) ,
					"name" => $field_name ,
					"value" => (user()->id ? user()->email : '') ,
				])

				@break


				@case('mobile')

				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans("validation.attributes.$field_name") ,
					"field_class" => "lg" ,
					"id" => kebab_case($field_name) ,
					"name" => $field_name ,
					"value" => (user()->id ? user()->mobile : '') ,
				])

				@break


				@case('subject')

				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans("validation.attributes.$field_name") ,
					"field_class" => "lg" ,
					"id" => kebab_case($field_name) ,
					"name" => $field_name ,
				])

				@break

				@case('text')

				@include('depoint::layout.widgets.form.textarea',[
					"placeholder" => currentModule()->trans('general.form.your_message') ,
					"field_class" => "lg" ,
					"rows" => "10" ,
					"name" => $field_name ,
				])

				@break
			@endswitch
		@endforeach


		<div class="field ta-l">
			<button class="button blue lg">
				{{ currentModule()->trans('general.send_message') }}
			</button>
		</div>

		<div class="result">
			{!! widget('feed') !!}
		</div>

		{!! widget('form-close') !!}


	</div>

	@include(currentModule()->bladePath('layout.widgets.form.scripts'))
@endif
