<div class="contact-box item eq-item" data-aos="fade-down">
	<div class="icon-wrapper">
		<div class="icon">
			<img src="{{ $image }}" width="64">
		</div>
	</div>
	<div class="value {{ $value_class or ""}}">
		{{ ad($value) }}
	</div>
</div>
