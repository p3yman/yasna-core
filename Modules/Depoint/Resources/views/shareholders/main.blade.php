@extends('depoint::layout.plane')
@php
	$shareholders = [
		[
			"image" => Module::asset('depoint:images/sample/logo-1.jpg') , 
			"title" => "بانک ملي ايران" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/logo-2.jpg') ,
			"title" => "بانک ملي ايران" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/logo-3.jpg') ,
			"title" => "بانک ملي ايران" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/logo-1.jpg') ,
			"title" => "بانک ملي ايران" ,
		],
		[
			"image" => Module::asset('depoint:images/sample/logo-2.jpg') ,
			"title" => "بانک ملي ايران" ,
		],
	];
@endphp
@section('content')
	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"subtitle" => trans('depoint::general.introduction') , 
				"title" => trans('depoint::general.shareholders') ,
			])


			<div class="row">

				<div class="col-sm-10 col-center">

					@foreach($shareholders as $person)
						@component('depoint::layout.widgets.rows',[
							"col_count" => "3" ,
							"loop" => $loop ,
						])
							<div class="col-sm-4">
								@include('depoint::layout.widgets.image-title-box',[
									"image" => $person['image'] , 
									"title" => $person['title'] , 
								])
							</div>
						@endcomponent
					@endforeach

				</div>

			</div>

		</div>

	</div>
@stop
