@extends(currentModule()->bladePath('layout.plane'))

@php
	depointTemplate()->appendToPageTitle($title);
@endphp

@section('content')

	<div class="container">

		<div class="page">

			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"title" => $title,
			])

			@forelse($images as $image)
				@component(currentModule()->bladePath('layout.widgets.rows'),[
					"col_count" => 3 ,
					"loop" => $loop ,
				])

					<div class="col-sm-4">
						@include(currentModule()->bladePath('layout.widgets.gallery-item'),[
							"link" => $image->direct_url ,
							"image" =>  depointTools()->getListImage($image),
							"hover_icon" => currentModule()->asset('images/gallery.svg') ,
							"title" => $image->title ,
						])
					</div>

				@endcomponent
			@empty
				@include(currentModule()->bladePath('layout.widgets.alert'), [
				 'class' => 'alert-default',
		 			'text' => currentModule()->trans('general.no-item-to-show')
		 			])
			@endforelse

			{!! $images->render() !!}

		</div>

	</div>
@stop
