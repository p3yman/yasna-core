@extends('depoint::layout.plane')
@php
	depointTemplate()->appendToPageTitle($posttype_title);
	depointTemplate()->appendToPageTitle($post_title);
@endphp

@section('content')

	<div class="container">

		<div class="page">

			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"title" => $title,
			])

			@foreach($model->files as $image)
				@component(currentModule()->bladePath('layout.widgets.rows'),[
					"col_count" => 4 ,
					"loop" => $loop ,
				])

					<div class="col-sm-3">
						@include(currentModule()->bladePath('layout.widgets.image-item'),[
							"image_original" => fileManager()->file($image['src'])->posttype($model->posttype)->config('attachments')->getUrl() ,
							"image_thumb" => fileManager()->file($image['src'])->posttype($model->posttype)->config('attachments')->version("thumb")->getUrl() ,
							"hover_icon" => currentModule()->asset('images/zoom.svg') ,
							"title" => $image['label'] ,
						])
					</div>

				@endcomponent
			@endforeach

			@include(currentModule()->bladePath('post.single.page-footer'))

		</div>

	</div>
@stop
