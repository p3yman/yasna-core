@php
	depointTemplate()->appendToPageTitle($posttype_title);
	depointTemplate()->appendToPageTitle($post_title);
@endphp

@component(currentModule()->bladePath('post.single.main'))
	@slot('header')
		@include(currentModule()->bladePath('layout.widgets.part-title'),[
			"title" => $post_title ,
		])
	@endslot

	@if(!empty($model->abstract) or !empty($video_hash))
		{{$model->text}}

		<div id="15331336276341833">
			<script type="text/JavaScript"
					src="https://www.aparat.com/embed/{{$video_hash}}?data[rnddiv]=15331336276341833&data[responsive]=yes"></script>
		</div>
	@else
		@include(currentModule()->bladePath('layout.widgets.alert'), [
		  'class' => 'alert-default',
			  'text' => currentModule()->trans('general.no-item-to-show')
			  ])
	@endif

@endcomponent
