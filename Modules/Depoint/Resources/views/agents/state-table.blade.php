<table class="table bordered province-details" id="province-{{ $id }}"
	   @if($id !== $default_state) style="display: none;" @endif>
	<thead>
	<tr>
		<th colspan="100%" class="province-head">
			{{ $title }}
		</th>
	</tr>
	@if ($agents->count())
		<tr>
			<th>
				{{ currentModule()->trans('general.table.city') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.agent') }}
			</th>
			<th>
				{{ currentModule()->trans('general.contact_info') }}
			</th>
		</tr>
	</thead>
	<tbody>
	@foreach($agents as $agent)
		<tr>
			<td>
				{{ $agent->cached_city->title }}
			</td>
			<td>
				{{ $agent->title }}
			</td>
			<td>
				@if ($agent->getMeta('address'))
					<div>
						{{ trans('validation.attributes.address') }}:
						{{ $agent->getMeta('address') }}
					</div>
				@endif

				@if ($agent->getMeta('postal_code'))
					<div>
						{{ trans('validation.attributes.postal_code') }}:
						{{ ad($agent->getMeta('postal_code')) }}
					</div>
				@endif

				@if ($agent->getMeta('telephone'))
					<div>
						{{ trans('validation.attributes.telephone') }}:
						<span style="direction: ltr; display: inline-block;">
						{{ ad($agent->getMeta('telephone')) }}
					</span>
					</div>
				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
	@else
		<tr>
			<td>
				<div class="pt10 pb10">
					{{ currentModule()->trans('general.no-agents-for-province') }}
				</div>
			</td>
		</tr>
	@endif
</table>
