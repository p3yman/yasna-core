@extends(currentModule()->bladePath('layout.plane'))

@php $title = ($posttype->titleIn(getLocale()) ?: currentModule()->trans('general.servicers'))  @endphp
@php depointTemplate()->appendToPageTitle($title) @endphp

@section('content')
	<div class="container">

		<div class="page">


			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"subtitle" => currentModule()->trans('general.introduction') ,
				"title" => $title,
			])

			<div id="where">

				<div class="row">

					<div class="col-sm-5">

						<div id="iran-map">
							@include(currentModule()->bladePath('agents.iran-map'))
							<div class="province-title"></div>
						</div>

					</div>

					<div class="col-sm-7">

						<div id="places">
							@foreach($states as $state)
								@php
									$agents = ($states_agents[$state->id] ?? collect([]));
								@endphp
								@include(currentModule()->bladePath('agents.state-table'),[
									"id" => $state->slug ,
									"title" => $state->title ,
									"agents" => $agents ,
								])
							@endforeach

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
@stop


@section('end-body')
	<script>
        $(document).ready(function () {
            $("#iran-map path.{{ $default_state }}").attr("data-status", "active");
        });
	</script>
@append
