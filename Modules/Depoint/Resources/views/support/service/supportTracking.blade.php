		<div class="panel">
			<article class="p30">
				<form action="{{route('depoint.support.service.status' , ['lang' => getLocale()])}}" method="post">
					{{csrf_field()}}

					<div>
							@include('depoint::layout.widgets.form.input-text',[
								"label" => trans('depoint::sorooshan.service-tracking.trace-code') ,
								"id" => "trace_code" ,
								"name" => "trace_code" ,
								'value'=> $trace_code ?? "",
								'placeholder' => trans('depoint::general.insert-tracking-code') ,
							])
						<div class="ta-l ltr">
							<button class="button blue"  type="submit">
								{{ trans('depoint::general.follow_up_request') }}
							</button>
						</div>
					</div>
				</form>
					<div class="row">
						<div>
							@if(isset($status))
								<div class="alert mt20 alt gray" role="alert">
									{{$status}}
								</div>
							@endif
						</div>
					</div>
			</article>
		</div>