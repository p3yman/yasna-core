@include('depoint::layout.widgets.form.select-city',[
	 "label" => currentModule()->trans('general.service.city'),
	 'name'  => 'city',
	 'options' => $cities,
	 'disabled' => ($disabled ?? true),
	 'id' => 'select-city',
	 'field_class' => 'js-cities-select'
])
