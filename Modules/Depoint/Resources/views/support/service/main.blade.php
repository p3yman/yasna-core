@extends('depoint::layout.plane')

@php
    depointTemplate()->appendToPageTitle(
        $__module->getTrans('general.request_service')
    );
@endphp

@section('content')
	<div class="container">
		<div class="page">
			@include('depoint::layout.widgets.part-title',[
				"title" => trans('depoint::general.request_service') ,
			])

            <div class="row">
                <div class="col-sm-10 col-center">
                    @include('depoint::support.service.supportTracking')
                </div>
            </div>


            {!! widget('form-open')->target(route_locale('depoint.support.sorooshan'))->class('js') !!}

			<div class="row">
                <div class="col-sm-10 col-center">

                    <div class="support-form pr10 pl10">

                        @include('depoint::layout.widgets.part-title',[
                            "title" => trans('depoint::general.service.title.personal') ,
                            "class" => "right" ,
                            "text_class" => "small" ,
                        ])
                        <div class="row">

                            <div class="col-sm-4">
                                @include('depoint::layout.widgets.form.input-text',[
                                    "label" => currentModule()->trans('general.service.name'),
                                    'name'  => 'name'
                                ])
                            </div>

                            <div class="col-sm-4">
                                @include('depoint::layout.widgets.form.select',[
                                     "label" => currentModule()->trans('general.service.province'),
                                     'name'  => 'state',
                                     'options' => $provinces,
                                     'id' => 'state-city',
                                ])
                            </div>

                            <div class="col-sm-4">
                                @include($__module->getBladePath('support.service.cities-select'))
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                @include('depoint::layout.widgets.form.input-text',[
                                    "label" => currentModule()->trans('general.service.tel'),
                                    'name'  => 'tel'
                                ])
                            </div>

                            <div class="col-sm-4">
                                @include('depoint::layout.widgets.form.input-text',[
                                    "label" => currentModule()->trans('general.service.mobile'),
                                    'name'  => 'mobile'
                                ])
                            </div>

                            <div class="col-sm-4">
                                @include('depoint::layout.widgets.form.input-text',[
                                    "label" => currentModule()->trans('general.service.email'),
                                    'name'  => 'email'
                                ])
                            </div>

                        </div>

                        @include('depoint::layout.widgets.form.textarea',[
                            "label" => currentModule()->trans('general.service.address'),
                            "rows"  => 3,
                            'name'  =>'address'
                        ])

                        <br>

                        @include('depoint::layout.widgets.part-title',[
                            "title" => trans('depoint::general.service.title.product') ,
                            "class" => "right" ,
                            "text_class" => "small" ,
                        ])
                        <div class="row">

                            <div class="col-sm-6">
                                @include('depoint::layout.widgets.form.input-text',[
                                    "label" => currentModule()->trans('general.service.serial'),
                                    'name'  => 'serial'
                                ])
                            </div>

                            {{--<div class="col-sm-6">--}}
                            {{--@include('depoint::layout.widgets.form.input-text',[--}}
                            {{--"label" => currentModule()->trans('general.service.guarantee'),--}}
                            {{--'name'  => 'guarantee'--}}
                            {{--])--}}
                            {{--</div>--}}

                        </div>

                        <br>

                        @include('depoint::layout.widgets.part-title',[
                            "title" => trans('depoint::general.service.title.service') ,
                            "class" => "right" ,
                            "text_class" => "small" ,
                        ])
                        <div class="row">

                            <div class="col-sm-6">
                                @include('depoint::layout.widgets.form.input-text',[
                                    "label" => currentModule()->trans('general.service.sender'),
                                    'name'  => 'sender'
                                ])
                            </div>
                            <div class="col-sm-6">
                                @include('depoint::layout.widgets.form.select',[
                                    "label" => currentModule()->trans('general.service.action'),
                                    'name'  => 'action',
                                    'options' => $service_types,
                                ])
                            </div>

                        </div>
                        @include('depoint::layout.widgets.form.textarea',[
                            "label" => currentModule()->trans('general.service.desc'),
                            "rows"  => 5,
                            'name'  =>'desc'
                        ])


                        <div class="ta-l mt10 mb50">
                            <button class="button blue lg">
                                {{ trans('depoint::general.send_request') }}
                            </button>
                        </div>
                        {!! widget('feed') !!}

                    </div>

                </div>
            </div>

			{!! widget('form-close') !!}

		</div>

	</div>
@stop

@include(currentModule()->bladePath('layout.widgets.form.scripts'))
@include(currentModule()->bladePath('support.service.scripts'))
