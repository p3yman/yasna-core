@section('end-body')
	<script>
        $(document).ready(function () {
            let cities_url = "{{ route_locale('depoint.support.sorooshan.cities', ['state' => '__STATE__']) }}";
            $('#state-city').change(function () {
                let that = $(this);
				let state_id = that.val();

				if (!state_id) {
				    return;
				}

				let cities_box = $('.js-cities-select');

				$.ajax({
					url: cities_url.replace('__STATE__', state_id),
					success: function (rs) {
                        cities_box.replaceWith(rs);
                    },
					beforeSend:function (){
					    cities_box.find('select').prop('disabled', true);
					}
				})
            }).trigger('change');
        })
	</script>
@append
