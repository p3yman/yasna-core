@extends('depoint::layout.plane')


@section('content')
	<div class="container">

		<div class="page">
			@include('depoint::layout.widgets.part-title',[
				"image" => Module::asset('depoint:images/icon-support.svg') , 
				"title" => trans('depoint::general.request_support') ,
			])

			<div class="row">

				<div class="col-sm-10 col-center">

					<div class="support-form">

						<div class="page-desc">
							{{ trans('depoint::general.support_message') }}
						</div>
						{!! widget('form-open')->target(route_locale('depoint.support.ticket.save'))->class('js') !!}
						<div class="row">
							<div class="col-sm-8 col-center">

								@include('depoint::layout.widgets.part-title',[
									"title" => trans('depoint::general.support_type') , 
									"class" => "ta-r" , 
									"text_class" => "small" ,
								])

								@include('depoint::layout.widgets.form.input-text',[
									"label" => currentModule()->trans('general.table.subject') ,
									 'name'=>'subject'
								])


								@include('depoint::layout.widgets.form.textarea',[
									"label‌" => trans('validation.attributes.description') ,
									"rows" => 7 ,
									'name'=>'text'
								])

								@include('depoint::layout.widgets.part-title',[
									"title" => trans('depoint::general.personal_info') ,
									"class" => "ta-r" ,
									"text_class" => "small" ,
								])
								<div class="row">
									<div class="col-sm-6">
										@include('depoint::layout.widgets.form.input-text',[
											"label" => trans('validation.attributes.name_first') ,
											"id" => "name_first" ,
											"name" => "name_first" ,
											'value'=> user()->name_first ?? "",
										])
									</div>
									<div class="col-sm-6">
										@include('depoint::layout.widgets.form.input-text',[
											"label" => trans('validation.attributes.name_last') ,
											"id" => "name_last" ,
											"name" => "name_last" ,
											'value'=> user()->name_last ?? "",
										])
									</div>
								</div>

								@include('depoint::layout.widgets.part-title',[
									"title" => trans('depoint::general.contact_info') ,
									"class" => "ta-r" ,
									"text_class" => "small" ,
								])
								<div class="row">
									<div class="col-sm-4">
										@include('depoint::layout.widgets.form.input-text',[
						                	"label" => currentModule()->trans('general.table.mobile') ,
						                	"name"=>'mobile',
						                	'value'=> user()->mobile ?? "",
						                 ])
									</div>
									<div class="col-sm-4">
										@include('depoint::layout.widgets.form.input-text',[
										"label" => currentModule()->trans('general.table.email') ,
										"name"=>'email',
						                	'value'=> user()->email ?? "",
									 ])
									</div>
								</div>
								<div class="ta-l mt10 mb50">
									<button class="button blue lg">
										{{ trans('depoint::general.send_request') }}
									</button>
								</div>
								{!! widget('feed') !!}
							</div>
						</div>

					</div>

				</div>

			</div>
			{!! widget('form-close') !!}

		</div>

	</div>
@stop

@include(currentModule()->bladePath('layout.widgets.form.scripts'))
