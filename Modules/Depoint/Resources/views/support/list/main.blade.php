@extends('depoint::layout.plane')
@php
	$provinces = [
		[
			"id" => "bla1" , 
			"title" => "تهران" , 
			"cities" => [
				[
					"title" => "تهران" , 
					"technician" => "رضا حسین‌زاده" , 
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" , 
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "تهران" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "تهران" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "تهران" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
			] , 
		],
		[
			"id" => "bla2" ,
			"title" => "شیراز" ,
			"cities" => [
				[
					"title" => "شیراز" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "شیراز" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "شیراز" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "تهران" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
			] ,
		],
		[
			"id" => "bla3" ,
			"title" => "کرج" ,
			"cities" => [
				[
					"title" => "کرج" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "کرج" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "کرج" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
				[
					"title" => "کرج" ,
					"technician" => "رضا حسین‌زاده" ,
					"address" => "خیابان کریم‌خان، خیابان زرند، پلاک ۲۷۱" ,
					"telephone" => "۰۲۱-۳۴۴۶۱۱۳۵" ,
				],
			] ,
		],
	];

	$provinces_select = [];
	$count = 0;
	foreach ($provinces as $province){
		$provinces_select[$count]['value'] = $province['id'];
		$provinces_select[$count]['title'] = $province['title'];
		++$count;
	}

@endphp
@section('content')
	<div class="container">

		<div class="page">
			@include('depoint::layout.widgets.part-title',[
				"image" => Module::asset('depoint:images/icon-support.svg') ,
				"title" => trans('depoint::general.request_support') ,
			])

			<div class="row">

				<div class="col-sm-10 col-center">

					<div class="page-desc">
						{{ trans('depoint::general.select_support_province') }}
					</div>

					<div class="row">
						<div class="col-sm-8 col-center">
							@include('depoint::layout.widgets.form.select',[
								"class" => "lg" ,
								"id" => "provinces-select" , 
								"options" => $provinces_select ,
							])

							<div id="support-contents">
								@foreach($provinces as $province)
									@include('depoint::support.list.support-table',[
										"id" => $province['id'] ,
										"title" => $province['title'] ,
										"cities" => $province['cities'] ,
									])
								@endforeach

							</div>

						</div>
					</div>

				</div>

			</div>

		</div>

	</div>
@stop
