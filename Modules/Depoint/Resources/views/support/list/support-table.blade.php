<div class="province" id="province-{{ $id }}">
	<div class="title">
		{{ trans('depoint::general.province_agents') . " " . $title }}
	</div>
	<table class="table padded">
		<thead>
		<tr>
			<th>
				{{ trans('depoint::general.support_table.city') }}
			</th>
			<th>
				{{ trans('depoint::general.support_table.technician') }}
			</th>
			<th>
				{{ trans('depoint::general.support_table.address') }}
			</th>
			<th>
				{{ trans('depoint::general.support_table.telephone') }}
			</th>
		</tr>
		</thead>
		<tbody>

		@foreach($cities as $city)
			<tr>
				<td>
					{{ $city['title'] }}
				</td>
				<td>
					{{ $city['technician'] }}
				</td>
				<td>
					{{ $city['address'] }}
				</td>
				<td>
					{{ ad($city['telephone']) }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
