@php
	$related_posts = [
		"title" => trans('depoint::general.related_news') ,
		"posts" => $model->similars(4),
	];

	depointTemplate()->appendToPageTitle($posttype_title);
	depointTemplate()->appendToPageTitle($post_title);

@endphp

@component(currentModule()->bladePath('post.single.main'),[
	"related_posts" => $related_posts ,
	"title" => $post_title
])



<!-- Post Title -->
	@slot('header')
		@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"subtitle" => $posttype_title ,
				"subtitle_link" => "#" ,
				"title" => $post_title ,
			])
	@endslot
	<!-- !END Post Title -->

<h3 class="ta-c color-gray h2 mb50">{{depoint()->eventEchoDate($model->event_starts_at,$model->event_ends_at)}}</h3>


	<!-- Post Content -->
	{!! $model->text !!}

	<!-- !END Post Content -->

@endcomponent