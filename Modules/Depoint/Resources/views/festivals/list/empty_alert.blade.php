@include('depoint::layout.widgets.part-title',[
	"title" => currentModule()->trans('general.festivals')  ,
		])

@include(currentModule()->bladePath('layout.widgets.alert'), [
		'class' => 'alert-default',
			'text' => currentModule()->trans('general.no-item-to-show')
			])