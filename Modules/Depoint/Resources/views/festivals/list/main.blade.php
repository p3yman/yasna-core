@extends(currentModule()->bladePath('layout.plane'))

@section('content')
	<div class="festival-bg"></div>

	<div class="container">

		<div class="page festivals">


			@if($current_festivals->count() or $passed_festivals->count() or $future_festivals->count()))
			@include(currentModule()->bladePath("festivals.list.current_festivals"))

			@include(currentModule()->bladePath("festivals.list.passed_festivals"))

			@include(currentModule()->bladePath("festivals.list.future_festivals"))
			@else
				@include(currentModule()->bladePath('festivals.list.empty_alert'))
			@endif


		</div>

	</div>
@stop
