@php
	$class = "";
	$show_ribbon = false;
	
	if (isset($is_main) and  $is_main){
		$class = "style-1";
		$show_ribbon = true;
	}

	if (isset($status)){
		$class .= " ". $status;
	}
@endphp

<div class="festival-item {{ $class }}">
	<div class="head">
		<div class="title">
			{{ ad($title) }}
		</div>
		<div class="date">
			{{ ad($date) }}
		</div>
	</div>

	<hr>

	<div class="content">
		{!! $content or "" !!}
		<div class="ta-c">
			<a href="{{ $link }}" class="button teal lg wide">
				{{ trans('depoint::general.show_detail') }}
			</a>
		</div>
	</div>

	@if($show_ribbon)
		<div class="ribbons">
			<img src="{{ Module::asset('depoint:images/festival-ribbon-4.png') }}" class="ribbon-1" width="82">
			<img src="{{ Module::asset('depoint:images/festival-ribbon-2.png') }}" class="ribbon-2" width="76">
			<img src="{{ Module::asset('depoint:images/festival-ribbon-3.png') }}" class="ribbon-3" width="92">
			<img src="{{ Module::asset('depoint:images/festival-ribbon-4.png') }}" class="ribbon-4" width="143">
		</div>
	@endif

	@if(isset($status) and $status === "done")
		<div class="ribbon">
			<span>
				{{ trans('depoint::general.festival_is_done') }}
			</span>
		</div>
	@endif
</div>
