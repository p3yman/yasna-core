@if($future_festivals->count())
	@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"title" => currentModule()->trans('general.future_festivals')  ,
			])

	@foreach($future_festivals as $fest)
		@component(currentModule()->bladePath('layout.widgets.rows'),[
			"col_count" => 2 ,
			"loop" => $loop ,
		])
			<div class="col-sm-6">
				@include(currentModule()->bladePath('festivals.list.festival-item'),[
					"title" => $fest->title,
					"date" => depoint()->eventEchoDate($fest->event_starts_at,$fest->event_ends_at) ,
					"link" => $fest->direct_url ,
					"status" => "future" ,
				])
			</div>
		@endcomponent
	@endforeach
@endif