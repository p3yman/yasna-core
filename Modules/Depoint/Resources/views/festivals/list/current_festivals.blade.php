@if($current_festivals->count())

	@include(currentModule()->bladePath('layout.widgets.part-title'),[
		"title" => currentModule()->trans('general.festivals')  ,
			])

	@foreach($current_festivals as $current_festival)
		<div class="row">
			<div class="col-sm-8 col-center">
				@include(currentModule()->bladePath('festivals.list.festival-item'),[
				"is_main" => true ,
				"title" => $current_festival->title ,
				"date" => depoint()->eventEchoDate($current_festival->event_starts_at,$current_festival->event_ends_at) ,
				"link" => $current_festival->direct_url ,
				"content" => $current_festival->abstract,
				])

			</div>
		</div>
	@endforeach
@endif