@extends('depoint::layout.plane')

@php depointTemplate()->appendToPageTitle(trans('depoint::general.faq')) @endphp

@section('content')

	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"title" => trans('depoint::general.support'),
				"subtitle" => trans('depoint::general.faq'),
			])

			<div class="row">

				<div class="col-sm-3">
					@include('depoint::faq.sidebar.sidebar', [
						'items' => $side_items
					])
				</div>

				<div class="col-sm-9">

					<div class="faq-search">
						<div class="search" id="form">
						</div>
						<div class="ask ta-l mt20">
							<span class="color-gray-light ml10 xs-hide">
								{{ trans('depoint::general.ask_us') }}
							</span>
							<a href="{{route_locale('depoint.front.faq.new')}}" target="_blank">
								<button class="button blue" type="button"></button>
							</a>
						</div>
					</div>


					<div id="list">

						@foreach($categories as $category)
							<div class="faq-part" id="{{ $category['id'] }}">
								<div class="part-title inner">
									<div class="title">{{ $category['title'] }}</div>
								</div>
								<section class="panel questions">

									@foreach($category['questions'] as $question)
										<div class="q accordion">
											<a href="#" class="title">
												{{ $question['q'] }}
											</a>
											<div class="content">
												{!! $question['a'] !!}
											</div>
										</div>
									@endforeach
								</section>
							</div>
						@endforeach

						@if ((count($categories) == 1) and !count($category['questions']))
							@include(currentModule()->bladePath('layout.widgets.alert'), [
								'class' => 'alert-default',
								'text' => currentModule()->trans('general.no-item-to-show')
							])
						@endif

					</div>

				</div>

			</div>

		</div>

	</div>
@stop


@section('end-body')
	@if($category_slug)
		<script>
            $(document).ready(function () {
                let category_slug = "{{ $category_slug }}";

                $('a[href=#' + category_slug + ']').trigger('click');
            });
		</script>
	@endif
@append
