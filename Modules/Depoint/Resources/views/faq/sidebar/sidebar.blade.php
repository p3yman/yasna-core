<section class="panel p10 xs-hide">
	@foreach($items as $item)
		<div class="item">
			<a href="{{ $item['link'] }}">
				{{ $item['title'] }}
			</a>
		</div>

	@endforeach
</section>