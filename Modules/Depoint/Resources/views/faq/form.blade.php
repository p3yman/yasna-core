@if ($commenting_post->exists)
	@php $fields = depoint()->commenting()->translateFields($commenting_post->getMeta('fields')) @endphp
	<div class="col-sm-8 col-center">

		{!!
			widget('form-open')
				->target(route_locale('depoint.comments.save'))
				->class('js')
				->id('faq-form')
		!!}

		{!! widget('hidden')->name('post_id')->value($commenting_post->hashid) !!}

		@foreach($fields as $field_name => $field_value)

			@switch($field_name)

				@case('subject')
				<br><br><br><br><br>

				<div id="faq-title-wrapper">
					<div class="field lg">
						@include('depoint::layout.widgets.form.input-text',[
							"placeholder" => currentModule()->trans('validation.attributes.your-question') ,
							"field_class" => "lg" ,
							"id" => kebab_case($field_name) ,
							"name" => $field_name ,
							"id" => "faq-title",
						])
					</div>
					<div class="suggestions">
						<div class="title">
							{{ trans('depoint::general.similar_questions').": " }}
						</div>
						<ul class="list">
							@foreach($similar_questions as $question)
								<li>
									<a href="{{ $question['link'] }}" class="suggestion-title">
										{{ $question['title'] }}
									</a>
								</li>
							@endforeach
						</ul>
					</div>
				</div>

				@break

				@case('text')

				@include('depoint::layout.widgets.form.textarea',[
					"placeholder" => currentModule()->trans('validation.attributes.more-comments') ,
					"field_class" => "lg" ,
					"rows" => "10" ,
					"name" => $field_name ,
				])

				@break

				@case('name')

				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans("validation.attributes.$field_name") ,
					"field_class" => "lg" ,
					"id" => kebab_case($field_name) ,
					"name" => $field_name ,
					"value" => (user()->id ? user()->full_name : '') ,
				])

				@break


				@case('email')

				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans("validation.attributes.$field_name") ,
					"field_class" => "lg" ,
					"id" => kebab_case($field_name) ,
					"name" => $field_name ,
					"value" => (user()->id ? user()->email : '') ,
				])

				@break


				@case('mobile')

				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans("validation.attributes.$field_name") ,
					"field_class" => "lg" ,
					"id" => kebab_case($field_name) ,
					"name" => $field_name ,
					"value" => (user()->id ? user()->mobile : '') ,
				])

				@break
			@endswitch
		@endforeach


		<div class="field ta-l">
			<button class="button blue lg">
				{{ currentModule()->trans('general.send_message') }}
			</button>
		</div>

		<div class="result">
			{!! widget('feed') !!}
		</div>

		{!! widget('form-close') !!}


	</div>

	@include(currentModule()->bladePath('layout.widgets.form.scripts'))
@endif
