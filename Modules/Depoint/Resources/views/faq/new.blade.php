@extends('depoint::layout.plane')

@php
	depointTemplate()->appendToPageTitle(trans('depoint::general.faq'));
	depointTemplate()->appendToPageTitle(trans('depoint::general.new_question'));
@endphp


@include('depoint::layout.widgets.form.scripts')

@section('content')
	<div class="container">

		<div class="page">

			<div class="part-title" data-aos="Return 'faq.new' view.ade-down">
				<div class="subtitle">
					{{ trans('depoint::general.support') }} -
					<a href="#">
						{{ trans('depoint::general.faq') }}
					</a>
				</div>
				<div class="title">
					{{ trans('depoint::general.new_question') }}
				</div>
			</div>

			<div class="row">

				<div class="col-sm-10 col-center">

					<div class="faq-form">
						<div class="row">
							@include(currentModule()->bladePath('faq.form'))
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

	</div>
@stop
