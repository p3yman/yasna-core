<section class="panel">
	<article class="p0">
		<div class="payment-method">
			<div class="right">
				<div class="radio_style">
					@include(currentModule()->bladePath('order.checkout.address-radio'), [
						'input_id' => 'new-address',
						'input_value' => hashid(0)
					])
				</div>
			</div>
			<div class="left">
				<h5 class="color-blue">
					{{ currentModule()->trans('general.address.new') }}
				</h5>
				<div class="hide-content">
					<div class="info form-info">
						<div class="row sm">
							<div class="col-sm-12">
								@include('depoint::layout.widgets.form.input-text',[
									"label" => trans('validation.attributes.receiver') ,
									"name" => "receiver" ,
									"id" => "receiver" ,
									"required_sign" => true ,
								])
							</div>
						</div>

						<div class="row sm pt10">
							<div class="col-sm-6">
								@include('depoint::layout.widgets.form.input-text',[
									"label" => trans('validation.attributes.telephone') ,
									"name" => "telephone" ,
									"id" => "telephone" ,
									"required_sign" => true ,
								])
							</div>

							<div class="col-sm-6">
								@include('depoint::layout.widgets.form.input-text',[
									"label" => trans('validation.attributes.postal_code') ,
									"name" => "postal_code" ,
									"id" => "postal_code" ,
									"required_sign" => true ,
								])
							</div>
						</div>

						<div class="row sm pt10">
							<div class="col-sm-6">
								@include('depoint::layout.widgets.form.select',[
									"label" => trans('validation.attributes.province_id') ,
									"name" => "province_id" ,
									"id" => "province_id" ,
									"class" => "js-province-select",
									"required_sign" => true ,
									"options" => depoint()->provincesCombo('value')
								])
							</div>

							<div class="col-sm-6">
								@include('depoint::layout.widgets.form.select',[
									"label" => trans('validation.attributes.city_id') ,
									"name" => "city_id" ,
									"id" => "city_id" ,
									"class" => "js-city-select",
									"field_class" => "js-city-select-box",
									"required_sign" => true ,
									"options" => []
								])
							</div>
						</div>

						<div class="row sm pt10">
							<div class="col-sm-12">
								@include('depoint::layout.widgets.form.textarea',[
									"label" => trans('validation.attributes.address') ,
									"name" => "address" ,
									"id" => "address" ,
									"required_sign" => true ,
								])
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>

@section('end-body')
	<script>
        $(document).ready(function () {
            $('.js-province-select').change(function () {
                let that  = $(this);
                let value = that.val();
                let url   = "{{ route_locale('depoint.province-cities', ['province_id' => '__PROVINCE__']) }}"
                    .replace('__PROVINCE__', value);

                $.ajax({
                    url       : url,
                    dataType  : 'JSON',
                    beforeSend: function () {
                        $('.js-city-select-box').addClass('disabled-box');
                        $('.js-city-select').html('');
                    },
                    success   : function (cities) {
                        let city_select = $('.js-city-select');
                        $.each(cities, function (index, city) {
                            let option = $('<option></option>');
                            option.attr('value', city.id);
                            option.html(city.title);

                            city_select.append(option);
                        });

                        $('.js-city-select-box').removeClass('disabled-box');
                    }
                })
            }).trigger('change');
        });
	</script>
@append
