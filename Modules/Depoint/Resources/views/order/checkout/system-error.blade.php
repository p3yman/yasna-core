<div class="alert red">
	{{ $__module->getTrans('cart.message.failure.system-error') }}

	@if (user()->isDeveloper())
		({{ $__module->getTrans('cart.message.failure.system-error-dev') }})
	@endif
</div>
