<section class="panel order-auth">
	<article>
		<p>
			{{  currentModule()->trans('general.member_checkout') }}
		</p>
		<div class="row sm">
			<div class="col-sm-3">
				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans('validation.attributes.email') ,
					"type" => "email" ,
					"id" => "email" ,
					"name" => "email" ,
				])
			</div>
			<div class="col-sm-3">
				@include('depoint::layout.widgets.form.input-text',[
					"placeholder" => trans('validation.attributes.password') ,
					"type" => "password" ,
					"id" => "password" ,
					"name" => "password" ,
				])
			</div>
			<div class="col-sm-4">
				<a href="#" class="button blue">
					{{ currentModule()->trans('general.auth.login') }}
				</a>
			</div>
		</div>
		<a href="#" class="">
			{{ currentModule()->trans('general.auth.forgot_your_password') }}
		</a>
	</article>
</section>
