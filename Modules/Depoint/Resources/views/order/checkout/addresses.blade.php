<div class="row">
	<div class="col-sm-12 col-center">
		@foreach(depoint()->clientAddresses() as $address)
			@include(currentModule()->bladePath('order.checkout.address-item'), ['address' => $address])
		@endforeach

		@include(currentModule()->bladePath('order.checkout.new-address'))
	</div>
</div>
