@extends(currentModule()->bladePath('layout.plane'))

@php
	$title = currentModule()->trans('general.checkout_method');

	depointTemplate()->appendToPageTitle($title);

	$cart = depoint()->cart()->findActiveCart();
	$shipping_methods = $cart->availableShippingMethods();
@endphp

@section('content')
	<div class="container">

		<div class="page">

			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"subtitle" => currentModule()->trans('general.shop') ,
				"title" => $title ,
			])

			<div class="row">

				<div class="col-sm-12">

					<div class="row">
						<div class="col-sm-10 col-center">

							<div class="row">
								@if (count($shipping_methods))
									@include($__module->getBladePath('order.checkout.form'))
								@else
									@include($__module->getBladePath('order.checkout.system-error'))
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
