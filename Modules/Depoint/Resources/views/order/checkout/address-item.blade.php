<section class="panel">
	<article class="p0">
		<div class="payment-method">
			<div class="right">
				<div class="radio_style">
					@include(currentModule()->bladePath('order.checkout.address-radio'), [
						'input_id' => 'address-' . $address->hashid,
						'input_value' => $address->hashid,
						'checked' => ($cart->address_id == $address->id) ? 'checked' : '',
					])
				</div>
			</div>
			<div class="left">
				<p>{{ $address->address }}</p>
			</div>
		</div>
	</article>
</section>
