<section class="panel">
	<article>
		@include('depoint::layout.widgets.part-title',[
			"class" => "inner" ,
			"title" => currentModule()->trans('general.factor_details') ,
		])

		<div class="row">
			<div class="col-sm-6">
				@include('depoint::layout.widgets.form.input-text',[
					"label" => trans('validation.attributes.name_first') ,
					"name" => "name_first" ,
					"id" => "name_first" ,
					"required" => true ,
				])
			</div>
			<div class="col-sm-6">
				@include('depoint::layout.widgets.form.input-text',[
					"label" => trans('validation.attributes.name_last') ,
					"name" => "name_last" ,
					"id" => "name_last" ,
					"required" => true ,
				])
			</div>
		</div>

		@include('depoint::layout.widgets.form.input-text',[
			"label" => currentModule()->trans('general.form.company') ,
			"name" => "company" ,
			"id" => "company" ,
		])

		<div class="row">
			<div class="col-sm-6">
				@include('depoint::layout.widgets.form.select',[
					"label" => trans('validation.attributes.province_id') ,
					"options" => [
						[
							"value" => "bla" ,
							"title" => "تهران" ,
						],
						[
							"value" => "bla" ,
							"title" => "مازندران" ,
						],
						[
							"value" => "bla" ,
							"title" => "یزد" ,
						],
						[
							"value" => "bla" ,
							"title" => "کرمان" ,
						],
					] ,
				])
			</div>
			<div class="col-sm-6">
				@include('depoint::layout.widgets.form.input-text',[
					"label" => trans('validation.attributes.city') ,
					"name" => "city" ,
					"id" => "city" ,
				])
			</div>
		</div>
		@include('depoint::layout.widgets.form.textarea',[
			"label" => trans('validation.attributes.address') ,
			"rows" => "3" ,
			"id" => "address" ,
			"name" => "address" ,
		])
	</article>
</section>
