<h3>{{ $__module->getTrans('cart.shipping.method.title.singular') }}</h3>

<div class="shipping-row">
	@foreach($shipping_methods as $shipping_method)
		@php
			$is_selected = ($cart->shipping_method_id == $shipping_method->id);
			$price = depoint()->shop()->convertToMainCurrency($shipping_method['price']);
		@endphp
		<div class="group">
            <input type="radio" name="shipping_method"
                   id="{{ $shipping_method->hashid }}"
                   value="{{ $shipping_method->hashid }}"
                   @if($is_selected) checked @endif>
            <label for="{{ $shipping_method->hashid }}">
                <div class="title">
                    {{ $shipping_method->getTitleInCurrentLocale() }}
                </div>
                <div class="price">
                    {{ ad(number_format($price)) }}
                    {{ depoint()->shop()->currencyTrans() }}
                </div>
            </label>
		</div>
	@endforeach
</div>
