<section class="panel">
	<article>
		@include('depoint::layout.widgets.part-title',[
			"title" => currentModule()->trans('general.post_profile') ,
			"class" => "inner" ,
		])

		<div class="field">
			@include('depoint::layout.widgets.form.checkbox',[
				"label" => currentModule()->trans('general.post_to_new_address') ,
				"extra" => " data-disable-toggle=#disable-part" ,
				"id" => "check-2" ,
			])
		</div>
		<div class="disabled-area active" id="disable-part">
			<div class="row">
				<div class="col-sm-6">
					@include('depoint::layout.widgets.form.input-text',[
						"label" => trans('validation.attributes.name_first') ,
						"name" => "name_first2" ,
						"id" => "name_first2" ,
						"required" => true ,
					])
				</div>
				<div class="col-sm-6">
					@include('depoint::layout.widgets.form.input-text',[
						"label" => trans('validation.attributes.name_last') ,
						"name" => "name_last2" ,
						"id" => "name_last2" ,
						"required" => true ,
					])
				</div>
			</div>
			@include('depoint::layout.widgets.form.input-text',[
				"label" => currentModule()->trans('general.form.company') ,
				"name" => "company2" ,
				"id" => "company2" ,
			])
			<div class="row">
				<div class="col-sm-6">
					@include('depoint::layout.widgets.form.select',[
						"label" => trans('validation.attributes.province_id') ,
						"options" => [
							[
								"value" => "bla" ,
								"title" => "تهران" ,
							],
							[
								"value" => "bla" ,
								"title" => "مازندران" ,
							],
							[
								"value" => "bla" ,
								"title" => "یزد" ,
							],
							[
								"value" => "bla" ,
								"title" => "کرمان" ,
							],
						] ,
					])
				</div>
				<div class="col-sm-6">
					@include('depoint::layout.widgets.form.input-text',[
						"label" => trans('validation.attributes.city') ,
						"name" => "city" ,
						"id" => "city" ,
					])
				</div>
			</div>
			@include('depoint::layout.widgets.form.textarea',[
				"label" => trans('validation.attributes.address') ,
				"rows" => "3" ,
				"id" => "address2" ,
				"name" => "address2" ,
			])
		</div>
	</article>
</section>
