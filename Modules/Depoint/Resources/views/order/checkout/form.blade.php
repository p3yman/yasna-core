{!!
	widget('form-open')
		->target(route_locale('depoint.cart.set-delivery-info'))
		->class('js')
!!}

<div class="col-xs-12">
	@include(currentModule()->bladePath('order.checkout.addresses'))
</div>

<div class="col-sm-12 ">
	@include($__module->getBladePath('order.checkout.shipping-methods'))
</div>

<div class="col-sm-12 f-r">
	<div class="action ta-l">
		<a class="button gray lg" href="{{ route_locale('depoint.cart') }}">
			{{ currentModule()->trans('general.go-back') }}
		</a>
		<button class="button blue lg">
			{{ currentModule()->trans('general.save_checkout') }}
		</button>
	</div>
</div>

<div class="col-sm-12 pt15">
	{!! widget('feed') !!}
</div>

{!! widget('form-close') !!}
