<section class="panel">
	<article class="p0">
		<div class="payment-method">
			<div class="right">
				<div class="radio_style">
					<input type="radio" id="payment_method_online" name="payment_method" value="online">
					<label for="payment_method_online">
						<span></span>
					</label>
				</div>
			</div>
			<div class="left">
				<h5 class="color-blue">
					{{ currentModule()->trans('general.method_online') }}
				</h5>
				<div class="hide-content">
					<p>
						{{ currentModule()->trans('general.choose_bank_gate') }}
					</p>
					<div class="row">
						<div class="col-sm-7">
							@foreach($accounts as $account)
								@component(currentModule()->bladePath('layout.widgets.rows'),[
									"col_count" => 2 ,
									"loop" => $loop ,
								])

									<div class="col-sm-6">
										@include(currentModule()->bladePath('layout.widgets.form.radio'),[
											"class" => "blue" ,
											"id" => "radio-". $account->hashid ,
											"label" => $account->title ,
											"name" => "account" ,
											"value" => $account->hashid ,
										])
									</div>

								@endcomponent
							@endforeach

						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
