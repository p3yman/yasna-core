@php $cash_radio_id = 'payment_method_cash' @endphp

<section class="panel">
	<article class="p0">
		<div class="payment-method">
			<div class="right">
				<div class="radio_style">
					<input type="radio" id="{{ $cash_radio_id }}" name="payment_method" value="cash">
					<label for="{{ $cash_radio_id }}"><span></span></label>
				</div>
			</div>
			<div class="left">
				<h5 class="color-blue">
					{{ currentModule()->trans('general.method_cash') }}
				</h5>
			</div>
			<div style="display: none">
				@include(currentModule()->bladePath('layout.widgets.form.radio'), [
					'label' => '',
					'value' => $accounts->first()->hashid,
					'class' => 'js-cash-account-radio',
					'name' => 'account'
				])
			</div>
		</div>
	</article>
</section>

@section('end-body')
	<script>
        $(document).ready(function () {
            $("#{{ $cash_radio_id}}").change(function () {
                $('.js-cash-account-radio')
                    .find('input[type=radio]')
                    .prop('checked', true);
            });
        });
	</script>
@append
