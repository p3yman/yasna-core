<section class="panel">
	<article class="p0">
		<div class="payment-method">
			<div class="right">
				<div class="radio_style">
					<input type="radio" id="payment_method_shetab" name="payment_method" value="shetab">
					<label for="payment_method_shetab"><span></span></label>
				</div>
			</div>
			<div class="left">
				<h5 class="color-blue">
					{{ currentModule()->trans('general.method_shetab') }}
				</h5>
				<div class="hide-content">
					<p>
						{{ currentModule()->trans('general.money_transfer_message') }}
					</p>

					@foreach($accounts as $account)
						<div class="info">
							<div class="row">
								<div class="right">
									@include(currentModule()->bladePath('layout.widgets.form.radio'), [
										'label' => '',
										'name' => 'account',
										'id' => 'radio-' . $account->hashid,
										'value' => $account->hashid,
									])
								</div>

								<div class="left">
									<div class="row">
										<div class="col-sm-6">
											<span>
												{{ currentModule()->trans('general.bank').": " }}
											</span>
											<span>
												{{ $account->bank_name}}
											</span>
										</div>
										<div class="col-sm-6">
											<span>
												{{ currentModule()->trans('general.account_owner').": " }}
											</span>
											<span>
												{{ $account->getMeta('owner') }}
											</span>
										</div>
										<div class="col-sm-12">
											<span>
												{{ currentModule()->trans('general.bank_card_number').": " }}
											</span>
											<span>
												{{ ad(depoint()->shop()->accountNumberPreview($account)) }}
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach

					{{--<div class="info form-info">--}}
					{{--<div class="row sm">--}}
					{{--<div class="col-sm-3">--}}
					{{--@include('depoint::layout.widgets.form.input-text',[--}}
					{{--"placeholder" => currentModule()->trans('general.bank_tracking_number') ,--}}
					{{--"label" => currentModule()->trans('general.bank_tracking_number') ,--}}
					{{--])--}}
					{{--</div>--}}
					{{--<div class="col-sm-3">--}}
					{{--@include('depoint::layout.widgets.form.input-text',[--}}
					{{--"placeholder" => currentModule()->trans('general.settlement_date') ,--}}
					{{--"label" => currentModule()->trans('general.settlement_date') ,--}}
					{{--])--}}
					{{--</div>--}}
					{{--<div class="col-sm-6">--}}
					{{--<button class="button blue">--}}
					{{--{{ currentModule()->trans('general.submit') }}--}}
					{{--</button>--}}
					{{--</div>--}}
					{{--</div>--}}
					{{--</div>--}}
				</div>
			</div>
		</div>
	</article>
</section>
