@php
	$credit_account = depoint()->shop()->creditAccount();
	$credit_radio_id = 'payment_method_credit'
@endphp

@if (user()->canBuyWithCredit() and $credit_account->exists and $credit_account->isPublished())
	<section class="panel">
		<article class="p0">
			<div class="payment-method">
				<div class="right">
					<div class="radio_style">
						<input type="radio" id="{{ $credit_radio_id }}" name="payment_method" value="credit">
						<label for="{{ $credit_radio_id }}"><span></span></label>
					</div>
				</div>
				<div class="left">
					<h5 class="color-blue">
						{{ currentModule()->trans('general.method_credit') }}
					</h5>
					{{--<div class="hide-content">--}}
					{{--<p>--}}
					{{--{{ currentModule()->trans('general.your_credit').": " }}--}}
					{{--<span class="label red">--}}
					{{--{{ ad(number_format($credit))." ".$currency}}--}}
					{{--</span>--}}
					{{--{{ currentModule()->trans('general.not_enough_credit.part1') }}--}}
					{{--<a href="#">--}}
					{{--{{ currentModule()->trans('general.not_enough_credit.part2') }}--}}
					{{--</a>--}}
					{{--{{ currentModule()->trans('general.not_enough_credit.part3') }}--}}
					{{--</p>--}}
					{{--</div>--}}
				</div>
				<div style="display: none">
					@include(currentModule()->bladePath('layout.widgets.form.radio'), [
						'label' => '',
						'value' => $credit_account->hashid,
						'class' => 'js-credit-account-radio',
						'name' => 'account'
					])
				</div>
			</div>
		</article>
	</section>
@endif

@section('end-body')
	@if (user()->canBuyWithCredit() and $credit_account->exists and $credit_account->isPublished())
		<script>
            $(document).ready(function () {
                $("#{{ $credit_radio_id }}").change(function () {
                    $('.js-credit-account-radio')
                        .find('input[type=radio]')
                        .prop('checked', true);
                });
            });
		</script>
	@endif
@append
