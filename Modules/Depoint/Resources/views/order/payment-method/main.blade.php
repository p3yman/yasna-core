@extends('depoint::layout.plane')

@php
	$title = currentModule()->trans('general.payment_method');

	depointTemplate()->appendToPageTitle($title)
@endphp

@section('content')

	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"title" => currentModule()->trans('general.shop') ,
				"title" => $title ,
			])

			<div class="row">

				<div class="col-sm-12">

					<div class="row">
						<div class="col-sm-10 col-center">

							{!!
								widget('form-open')
									->target(route_locale('depoint.cart.do-payment'))
							!!}

							@php $grouped_accounts = depoint()->shop()->groupedPaymentAccounts() @endphp
							@foreach($grouped_accounts as $type => $accounts)
								@include(currentModule()->bladePath("order.payment-method.methods.$type"), [
									'accounts' => $accounts
								])
							@endforeach

							@include(currentModule()->bladePath('order.payment-method.methods.credit'))


							<div class="action ta-l">
								<a href="{{ route_locale('depoint.cart.review') }}" class="button gray-lighter lg">
									{{ currentModule()->trans('general.back') }}
								</a>
								<button class="button blue lg">
									{{ currentModule()->trans('general.confirm_checkout') }}
								</button>
							</div>

							@if($errors->all())
								<div class="row mt15">
									<div class="col-xs-12">
										<div class="alert danger">
											@foreach ($errors->all() as $error)
												{{ $error }}<br>
											@endforeach
										</div>
									</div>
								</div>
							@endif

							{!! widget('form-close') !!}

						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

@stop


@section('end-body')
	<script>
        $(document).ready(function () {
            $('input[name="payment_method"]').each(function (index, input) {
                let that = $(input);
                if (that.prop('checked')) {
                    that.trigger('change')
                }
            });
        })
	</script>
@append
