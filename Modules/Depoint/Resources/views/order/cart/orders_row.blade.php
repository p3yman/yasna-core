@php
	$ware = $order->ware;
	$post = $ware->post;
	$image = fileManager()
		->file($post->featured_image)
		->version('thumb')
		->getUrl();
	$unit_price = round(depoint()->shop()->convertToMainCurrency($order->single_original_price));
	$total_price = round(depoint()->shop()->convertToMainCurrency($order->original_price));
	$unit_price_to_show = ad(number_format($unit_price));
	$total_price_to_show = ad(number_format($total_price));
	$count =  $order->count;
@endphp

<tr class="product-row js-cart-item" data-item-key="{{ $order->hashid }}">
	<td>
		<div class="item">
			<div class="product-image">
				<img src="{{ $image }}">
			</div>
			<div class="details">
				<div class="title">
					{{ $ware->full_title }}
				</div>
				{{--<div class="options">--}}
				{{--{{ currentModule()->trans('general.cart_table.color'). ": ". $order['color'] }}--}}
				{{--</div>--}}
			</div>
		</div>
	</td>
	<td class="price">
		@if ($order->single_original_price != $order->single_sale_price)
			<del>
				{{ ad(number_format(round(depoint()->shop()->convertToMainCurrency($order->single_original_price)))) }}
				{{ $currency }}
			</del>
			<br>
		@endif
		{{ ad(number_format(round(depoint()->shop()->convertToMainCurrency($order->single_sale_price)))) }}
		{{ $currency }}
	</td>
	@if(isset($review) and $review)
		<td class="qty review">
			{{ ad($count) }}
		</td>
	@else
		<td class="qty">
			<input class="number-input js-cart-item-count" min="1" step="1" value="{{ ad($count) }}"
				   data-url="{{ route_locale('depoint.cart.edit-count') }}">
		</td>
	@endif
	<td class="price row-total">
		@if ($order->original_price != $order->sale_price)
			<del>
				{{ ad(number_format(round(depoint()->shop()->convertToMainCurrency($order->original_price)))) }}
				{{ $currency }}
			</del>
			<br>
		@endif
		{{ ad(number_format(round(depoint()->shop()->convertToMainCurrency($order->sale_price)))) }}
		{{ $currency }}
	</td>
	@if(!isset($review) or !$review)
		<td class="product-remove">
			<a href="#" class="icon-close js-cart-item-remove" data-url="{{ route_locale('depoint.cart.remove') }}"></a>
		</td>
	@endif
</tr>
