@if ($errors->count())
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-10 col-center js-cart-container">
				<div class="alert alert-danger">
					{!! implode("<br>", array_flatten($errors->toArray())) !!}
				</div>
			</div>
		</div>
	</div>
@endif
