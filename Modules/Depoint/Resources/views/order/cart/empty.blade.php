<section class="panel">
	<article class="p50">

		<div class="ta-c mt30 mb50"><img src="{{ Module::asset('depoint:images/shopping-cart-empty.svg') }}" width="128"></div>

		<div class="page-desc">
			{{ currentModule()->trans('general.cart_table.is_empty') }}
		</div>

		<div class="ta-c mt30">
			<a class="button blue lg" href="{{ route_locale('depoint.front.product.list') }}">
				{{ currentModule()->trans('general.cart_table.back_to_shop') }}
			</a>
		</div>

	</article>
</section>
