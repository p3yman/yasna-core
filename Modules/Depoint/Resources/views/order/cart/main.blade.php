@extends('depoint::layout.plane')

@php
	$tax = 9;

	$title = currentModule()->trans('general.cart');


	if(isset($review) and $review){
		$title = currentModule()->trans('general.cart_review');
	}

	depointTemplate()->appendToPageTitle($title)
@endphp

@section('content')

	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"subtitle" => currentModule()->trans('general.shop') ,
				"title" => $title ,
			])

			<div class="row">
				@include(currentModule()->bladePath('order.cart.error'))
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-10 col-center js-cart-container">
							@include(currentModule()->bladePath('order.cart.content'))
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop
