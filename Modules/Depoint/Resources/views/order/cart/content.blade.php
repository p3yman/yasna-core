@php
	$currency = depoint()->shop()->currencyTrans();

	$cart = depointTemplate()->cart();
	$orders = $cart->orders;
@endphp

@if(isset($orders) and count($orders))
	@include(currentModule()->bladePath('order.cart.orders'))
@else
	@include(currentModule()->bladePath('order.cart.empty'))
@endif
