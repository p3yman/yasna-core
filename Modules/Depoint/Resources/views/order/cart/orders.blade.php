<div class="table-scroll">
	<table class="table bordered padded">
		<thead>
		<tr>
			<th>
				{{ currentModule()->trans('general.cart_table.product') }}
			</th>
			<th>
				{{ currentModule()->trans('general.cart_table.price') }}
			</th>
			<th>
				{{ currentModule()->trans('general.cart_table.count') }}
			</th>
			<th>
				{{ currentModule()->trans('general.cart_table.total') }}
			</th>
			@if(!isset($review) or !$review)
				<th></th>
			@endif
		</tr>
		</thead>
		<tbody>
		@foreach($orders as $order)
			@include('depoint::order.cart.orders_row',[
				"order" => $order ,
			])
		@endforeach

		@if((!isset($review) or !$review) and $cart->hasNotAppliedCouponCode())
			<tr class="coupon-row">
				<td colspan="100%">
					<div class="result error js-coupon-code-result" style="display: none">
						{{ currentModule()->trans('general.cart_table.add_discount_coupon') }}
					</div>
					<input type="text" class="js-coupon-code"
						   placeholder="{{ currentModule()->trans('general.cart_table.discount_coupon') }}">
					<button class="button inverted gray-lightest js-submit-coupon-code"
							data-url="{{ route_locale('depoint.cart.add-coupon-code') }}">
						{{ currentModule()->trans('general.cart_table.use_coupon') }}
					</button>
				</td>
			</tr>
		@endif
		</tbody>
	</table>
</div>


<div class="row">
	<div class="col-sm-6"></div>
	<div class="col-sm-6">
		<table class="table bordered padded total">
			<tbody>

			@if ($cart->invoiced_amount != $cart->original_price)
				<tr>
					<td>
						{{ currentModule()->trans('general.cart_table.primary_sum') }}
					</td>
					<td class="price">
						{{ ad(number_format(depoint()->shop()->convertToMainCurrency($cart->original_price))) }}
						<small>{{ $currency }}</small>
					</td>
				</tr>
			@endif

			@php
				$shop_discount = $cart->original_price - $cart->sale_price;
				$added_discount = $cart->added_discount;
			@endphp
			@if ($shop_discount or $added_discount)
				<tr>
					<td>
						{{ currentModule()->trans('general.cart_table.discount') }}

						@if ($shop_discount)
							<br>
							<small>{{ currentModule()->trans('general.cart_table.shop_discount') }}</small>
						@endif

						@if ($added_discount)
							@php $coupon_code = $cart->getAppliedCouponCode() @endphp
							<br>
							<small>
								{{ currentModule()->trans('general.cart_table.discount_coupon') }}:
							</small>
							<small class="ltr">
								<span class="label gray-light sm d-ib"
									  style="padding-bottom: 4px;">{{ $coupon_code }}</span>
							</small>
							<a href="#"
							   class="inverted d-ib color-red simptip-fade simptip-position-bottom simptip-danger js-disable-code"
							   data-url="{{ route_locale('depoint.cart.remove-coupon-code') }}"
							   data-tooltip="{{ currentModule()->trans('cart.disable-code') }}">
								<i class="icon-trash-o"></i>
							</a>

						@endif
					</td>
					<td class="price coupon">
						{{ ad(number_format(depoint()->shop()->convertToMainCurrency($shop_discount + $added_discount))) }}
						<small>{{ $currency }}</small>

						@if ($shop_discount)
							<br>
							<small class="color-gray-light">{{ ad(number_format(depoint()->shop()->convertToMainCurrency($shop_discount))) }}</small>
							<small class="color-gray-light">{{ $currency }}</small>
						@endif

						@if ($added_discount)
							@php $code_discount = $cart->calculateDiscountByCouponCode($coupon_code) @endphp
							<br>
							<small class="color-gray-light">{{ ad(number_format(depoint()->shop()->convertToMainCurrency($code_discount))) }}</small>
							<small class="color-gray-light">{{ $currency }}</small>
						@endif
					</td>
				</tr>
			@endif

			@php $tax_amount = $cart->tax_amount @endphp
			@if ($tax_amount)
				<tr>
					<td>
						{{ currentModule()->trans('general.cart_table.tax') }}
					</td>
					<td class="price coupon">
						{{ ad(number_format(depoint()->shop()->convertToMainCurrency($tax_amount))) }}
						<small>{{ $currency }}</small>
					</td>
				</tr>
			@endif

			@php $delivery_price = $cart->delivery_price @endphp
			@if ($delivery_price)
				<tr>
					<td>
						{{ currentModule()->trans('cart.delivery-price') }}
					</td>
					<td class="price coupon">
						{{ ad(number_format(depoint()->shop()->convertToMainCurrency($delivery_price))) }}
						<small>{{ $currency }}</small>
					</td>
				</tr>
			@endif


			<tr>
				<td>
					{{ currentModule()->trans('general.cart_table.final_price') }}
				</td>
				<td class="price big-total">
					{{ ad(number_format(depoint()->shop()->convertToMainCurrency($cart->invoiced_amount))) }}
					<small>{{ $currency }}</small>
				</td>
			</tr>
			</tbody>
		</table>
		@if ($review ?? false)
			<div class="action ta-l">
				<a href="{{ route_locale('depoint.cart.delivery') }}" class="button gray lg">
					{{ currentModule()->trans('general.go-back') }}
				</a>
				<a href="{{ route_locale('depoint.cart.payment-methods') }}" class="button blue lg">
					{{ currentModule()->trans('general.cart_table.checkout') }}
				</a>
			</div>
		@else
			<div class="action ta-l">
				<a href="{{ route_locale('depoint.cart.delivery') }}" class="button blue lg">
					{{ currentModule()->trans('general.continue') }}
				</a>
			</div>
		@endif
	</div>
</div>
