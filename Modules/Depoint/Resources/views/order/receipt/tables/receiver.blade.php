@php $address = $cart->address @endphp
@if ($address)

	<table class="table bordered recipt-table">
		<thead>
		<tr>
			<th colspan="100%" class="p20">
				<div class="part-title panel-title">
					<div class="title">
						{{ $__module->getTrans('cart.delivery-info') }}
					</div>
				</div>
			</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td>
				{{ trans('depoint::general.table.receiver') }}
			</td>
			<td class="fw-b">
				{{ $address->receiver }}
			</td>
		</tr>
		<tr>
			<td>
				{{ trans('depoint::general.table.address') }}
			</td>
			<td class="fw-b">
				{{ $address->address }}
			</td>
		</tr>
		<tr>
			<td>
				{{ trans('depoint::general.table.telephone') }}
			</td>
			<td>
				{{ ad($address->telephone) }}
			</td>
		</tr>
		<tr>
			<td>
				{{ $__module->getTrans('cart.shipping.method.title.singular') }}
			</td>
			<td>
				@if ($cart->shipping_method and $cart->shipping_method->exists)
					{{ $cart->shipping_method->getTitleInCurrentLocale() }}
				@endif
			</td>
		</tr>
		</tbody>
	</table>

@endif
