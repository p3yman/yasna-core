<table class="table bordered recipt-table">
	<thead>
	<tr>
		<th colspan="100%" class="p20">
			<div class="part-title panel-title">
				<div class="title">
					{{ trans('depoint::general.order_abstract') }}
				</div>
			</div>
		</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td>
			{{ trans('depoint::general.table.receipt_number') }}
		</td>
		<td class="fw-b ta-c">
			{{ ad($cart->code) }}
		</td>
	</tr>
	<tr>
		<td>
			{{ trans('depoint::general.table.total_price') }}
		</td>
		<td class="fw-b color-blue-darker ta-c">
			{{ ad(number_format(depoint()->shop()->convertToMainCurrency($cart->invoiced_amount))) }}
			{{ $currency }}
		</td>
	</tr>
	<tr>
		<td>
			{{ trans('depoint::general.table.payment_status') }}
		</td>
		<td class="color-{{ $cart->isPaid() ? 'green' : 'red' }} ta-c">
			@if ($cart->isPaid())
				<i class="icon-check"></i>
			@else
				-
			@endif
			{{ $cart->payment_status_text }}

			@if ($cart->isNotPaid())
				-
			@endif
		</td>
	</tr>
	<tr>
		<td>
			{{ trans('depoint::general.table.order_status') }}
		</td>
		<td class="ta-c">
			<div class="label {{ depoint()->mapColor($cart->status_color) }}">
				{{ $cart->status_text }}
			</div>
		</td>
	</tr>


	@if ($cart->isDelivered())
		<tr>
			<td>
				{{ $__module->getTrans('cart.order.delivery-date') }}
			</td>
			<td class="ta-c">
				{{ ad(echoDate($cart->delivered_at, 'j F Y')) }}
			</td>
		</tr>
	@endif
	</tbody>
</table>
