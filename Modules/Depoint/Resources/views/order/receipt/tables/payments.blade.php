@if ($transactions->count())

	<div class="table-scroll">
		<table class="table bordered recipt-table">
			<thead>
			<tr>
				<th colspan="100%" class="p20">
					<div class="part-title panel-title">
						<div class="title">
							{{ trans('depoint::general.your_payments') }}
						</div>
					</div>
				</th>
			</tr>
			<tr>
				<th>
					{{ trans('depoint::general.table.row') }}
				</th>
				<th>
					{{ trans('depoint::general.table.payment_method') }}
				</th>
				<th>
					{{ trans('depoint::general.table.bank') }}
				</th>
				<th>
					{{ trans('depoint::general.table.tracking_number') }}
				</th>
				<th>
					{{ trans('depoint::general.table.date') }}
				</th>
				<th>
					{{ trans('depoint::general.table.price') }}
				</th>
				<th>
					{{ trans('depoint::general.table.status') }}
				</th>
			</tr>
			</thead>
			<tbody>
			@foreach($transactions as $transaction)
				@php $account = $transaction->account @endphp
				<tr>
					<td>
						{{ $loop->iteration }}
					</td>
					<td>
						{{ depoint()->shop()->mapTransactionTitle($transaction) }}
					</td>
					<td>
						{{ $account->bank_name or '-' }}
					</td>
					<td class="fw-b color-blue-darker">
						{{ ad($transaction->tracking_no) }}
					</td>
					<td>
						{{ ad(echoDate($transaction->created_at, 'j F Y')) }}
					</td>
					<td class="fw-b color-blue-darker">
						{{ ad(number_format(depoint()->shop()->convertToMainCurrency($transaction->payable_amount))) }}
						{{ $currency }}
					</td>
					<td>
						<div class="label {{ depoint()->mapColor($transaction->status_color) }}">
							{{ $transaction->status_text }}
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endif
