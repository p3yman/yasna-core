@extends('depoint::layout.plane')

@php
	$currency = depoint()->shop()->currencyTrans();
	depointTemplate()->appendToPageTitle(trans('depoint::general.order_receipt'));
@endphp

@section('content')
	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"subtitle" => trans('depoint::general.shop') ,
				"title" => trans('depoint::general.order_receipt') ,
			])

			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-sm-10 col-center">
							<div class="row">
								@include(currentModule()->bladePath('order.receipt.transaction-result'))
								@include(currentModule()->bladePath('order.receipt.repurchase'))

								<div class="col-sm-6">
									@include('depoint::order.receipt.tables.abstract')
								</div>

								<div class="col-sm-6">
									@include('depoint::order.receipt.tables.receiver')
								</div>
							</div>
							@include('depoint::order.receipt.tables.payments')

						</div>
					</div>

				</div>

			</div>

		</div>

	</div>
@stop
