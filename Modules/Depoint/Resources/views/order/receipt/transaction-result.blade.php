@isset($transaction_alert)
	@php $transaction_success = ($transaction_success ?? false) @endphp
	<div class="col-sm-12">
		<div class="alert alert-{{ $transaction_success ? 'success' : 'danger' }}">
			{{ $transaction_alert }}

			<br>

			{{ currentModule()->trans('general.table.tracking_number') }}:
			{{ ad($tracking_number) }}
		</div>
	</div>
@endisset
