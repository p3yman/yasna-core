@if ($cart->isNotPaid())

	{!!
		widget('form-open')
			->target(route_locale('depoint.cart.repurchase'))
			->class('js')
	!!}

	{!! widget('hidden')->name('hashid')->value($cart->hashid) !!}

	<div class="col-sm-12">
		@php
			$cells_in_row = 3;
		@endphp
		<table class="table recipt-table">
			<tr>
				<td colspan="{{ $cells_in_row }}">
					{{ currentModule()->trans('cart.message.hint.repurchase') }}
				</td>
			</tr>

			@php
				$accounts = depoint()->shop()->repurchasePaymentAccounts();
				$accounts_rows = $accounts->chunk($cells_in_row);
			@endphp
			<tr>
				<td colspan="100%">
					@foreach($accounts_rows as $accounts_row)

						<div class="row">
							@foreach($accounts_row as $account)

								<div class="col-sm-{{ 12 / $cells_in_row }}">
									@include(currentModule()->bladePath('layout.widgets.form.radio'),[
										"class" => "blue" ,
										"id" => "radio-". $account->hashid ,
										"label" => $account->title ,
										"name" => "account" ,
										"value" => $account->hashid ,
									])

								</div>

							@endforeach
						</div>

					@endforeach


				</td>
			</tr>

			<tr>
				<td colspan="{{ $cells_in_row }}">
					<div class="row">
						<div class="col-sm-12 ta-l">
							<button class="button blue sm">
								{{ currentModule()->trans('general.payment') }}
							</button>
						</div>
						<div class="col-sm-12 mt5">
							{!! widget('feed') !!}
						</div>
					</div>
				</td>
			</tr>

		</table>

	</div>

	{!! widget('form-close') !!}
@endif
