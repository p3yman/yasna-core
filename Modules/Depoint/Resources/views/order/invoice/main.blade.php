@extends('depoint::layout.plane')

@php
	$transactions = [
		[
			"method" => "آنلاین" ,
			"bank" => "بانگ سامان" ,
			"tracking_number" => "52782902" ,
			"date" => "4 آبان 1397" ,
			"price" => "28600000" ,
			"currency" => "ریال" ,
			"status" => [
				"title" => "در حال بررسی" ,
				"color" => "blue" ,
			] ,
		],
		[
			"method" => "آنلاین" ,
			"bank" => "بانگ سامان" ,
			"tracking_number" => "52782902" ,
			"date" => "4 آبان 1397" ,
			"price" => "28600000" ,
			"currency" => "ریال" ,
			"status" => [
				"title" => "در حال بررسی" ,
				"color" => "green" ,
			] ,
		],
	];

	$order_abstract = [
		"tracking_number" => "672829" ,
		"total_price" => "52600700" ,
		"currency" => "ریال" ,
		"payment_status" => [
			"color" => "green" ,
			"icon" => "check" ,
			"title" => "پرداخت شده" ,
		] ,
		"order_status" => [
			"color" => "blue" ,
			"title" => "پرداخت شده" ,
		] ,
	];

	$receiver = [
		"full_name" => "علی محمدزاده" ,
		"address" => "تهران، نارمک، خیابان دکتر آیت، پلاک ۳۱۸" ,
		"telephone" => "55288801" ,
	];

	$buyer = [
		"title" => "علی محمدزاده" ,
		"national_id_number" => "5272828" ,
		"registration_number" => "5272828" ,
		"economic_number" => "5272828" ,
		"address" => "تهران، نارمک، خیابان دکتر آیت، پلاک ۳۱۸" ,
		"telephone" => "55288801" ,
		"postal_code" => "663288282" ,
	];

	$seller = [
		"title" => "شرکت فناوران آنلاین رساساز" ,
		"national_id_number" => "5272828" ,
		"registration_number" => "5272828" ,
		"economic_number" => "5272828" ,
		"address" => "تهران، نارمک، خیابان دکتر آیت، پلاک ۳۱۸" ,
		"telephone" => "55288801" ,
		"postal_code" => "663288282" ,
	];

	$currency = "ریال";
	$tax = "9";

	$products = [
		[
			"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
			"code" => "67212" ,
			"color" => "سفید" ,
			"count" => "2" ,
			"single_price" => "1200000" ,
			"total_price" => "46290000" ,
			"discount" => "0" ,
			"discounted_price" => "62788800" ,
			"tax_price" => "628000" ,
			"final_price" => "529007300" ,
		],
		[
			"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
			"code" => "67212" ,
			"color" => "سفید" ,
			"count" => "2" ,
			"single_price" => "1200000" ,
			"total_price" => "46290000" ,
			"discount" => "0" ,
			"discounted_price" => "62788800" ,
			"tax_price" => "628000" ,
			"final_price" => "529007300" ,
		],
	];

@endphp

@section('content')
	<div class="container">

		<div class="container">

			<div class="page">

				<div class="recipt">

					@include('depoint::layout.widgets.part-title',[
						"title" => trans('depoint::general.product_invoice') ,
					])

					<table class="table bordered recipt-table">
						<tbody>
						<tr>
							<td>
								<strong>{{ trans('depoint::general.table.seller').": " }}</strong>
								{{ $seller['title'] }}
							</td>
							<td>{{ trans('depoint::general.table.national_id_number').": ".ad($seller['national_id_number']) }}</td>
							<td>{{ trans('depoint::general.table.registration_number').": ".ad($seller['registration_number']) }}</td>
							<td>{{ trans('depoint::general.table.economic_number').": ".ad($seller['economic_number']) }}</td>
						</tr>
						<tr>
							<td colspan="2">{{ trans('depoint::general.table.address').": ".ad($seller['address']) }}</td>
							<td>{{ trans('depoint::general.table.postal_code').": ".ad($seller['postal_code']) }}</td>
							<td>{{ trans('depoint::general.table.telephone').": ".ad($seller['telephone']) }}</td>
						</tr>
						</tbody>
					</table>

					<table class="table bordered recipt-table">
						<tbody>
						<tr>
							<td>
								<strong>{{ trans('depoint::general.table.seller').": " }}</strong>
								{{ $buyer['title'] }}
							</td>
							<td>{{ trans('depoint::general.table.national_id_number').": ".ad($buyer['national_id_number']) }}</td>
							<td>{{ trans('depoint::general.table.registration_number').": ".ad($buyer['registration_number']) }}</td>
							<td>{{ trans('depoint::general.table.economic_number').": ".ad($buyer['economic_number']) }}</td>
						</tr>
						<tr>
							<td colspan="2">{{ trans('depoint::general.table.address').": ".ad($buyer['address']) }}</td>
							<td>{{ trans('depoint::general.table.postal_code').": ".ad($buyer['postal_code']) }}</td>
							<td>{{ trans('depoint::general.table.telephone').": ".ad($buyer['telephone']) }}</td>
						</tr>
						</tbody>
					</table>

					<div class="invoice">

						<div class="table-scroll">
							<table class="table bordered">
								<thead>
									<tr>
										<th>
											{{ trans('depoint::general.table.row') }}
										</th>
										<th>
											{{ trans('depoint::general.table.product_code') }}
										</th>
										<th>
											{{ trans('depoint::general.table.product') }}
										</th>
										<th>
											{{  trans('depoint::general.table.count')  }}
										</th>
										<th>
											{{ trans('depoint::general.table.single_price')." ($currency)" }}
										</th>
										<th>
											{{ trans('depoint::general.table.total_price')." ($currency)" }}
										</th>
										<th>
											{{ trans('depoint::general.cart_table.discount')." ($currency)" }}
										</th>
										<th>
											{{ trans('depoint::general.table.price_after_discount')." ($currency)" }}
										</th>
										<th>
											{{ trans('depoint::general.table.total_tax')." ($currency)" }}
										</th>
										<th>
											{{ trans('depoint::general.table.total_price_tax_discount')." ($currency)" }}
										</th>
									</tr>
								</thead>
								<tbody>
								@foreach($products as $product)
									<tr class="product-row">
										<td>
											{{ $loop->iteration }}
										</td>
										<td>
											{{ ad($product['code']) }}
										</td>
										<td>
											<div class="item">
												<div class="details">
													<div class="title">
														{{ $product['title'] }}
													</div>
													<div class="options">
														{{ trans('depoint::general.cart_table.color').": ".$product['color'] }}
													</div>
												</div>
											</div>
										</td>
										<td class="qty review">
											{{ ad($product['count']) }}
										</td>
										<td class="fw-b ta-c">
											{{ ad(number_format($product['single_price'])) }}
										</td>
										<td class="fw-b ta-c">
											{{ ad(number_format($product['total_price'])) }}
										</td>
										<td class="fw-b ta-c">
											{{ ad(number_format($product['discount'])) }}
										</td>
										<td class="fw-b ta-c">
											{{ ad(number_format($product['discounted_price'])) }}
										</td>
										<td class="fw-b ta-c">
											{{ ad(number_format($product['tax_price'])) }}
										</td>
										<td class="fw-b ta-c">
											{{ ad(number_format($product['final_price'])) }}
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>

						<div class="row">
							<div class="col-sm-6"></div>
							<div class="col-sm-6">
								<table class="table bordered total">
									<tbody>
									<tr>
										<td>
											{{ trans('depoint::general.cart_table.primary_sum') }}
										</td>
										<td class="price">
											{{ ad(number_format("6276128000")) }}
											<small>{{ $currency }}</small>
										</td>
									</tr>
									<tr>
										<td>{{ trans('depoint::general.cart_table.tax')." (". ad($tax).trans('depoint::general.percent') .")" }}</td>
										<td class="price">
											{{ ad(number_format("6276128000")) }}
											<small>{{ $currency }}</small>
										</td>
									</tr>
									<tr>
										<td>
											{{ trans('depoint::general.cart_table.discount') }}
										</td>
										<td class="price">
											<span style="direction: ltr; display: inline-block;">
												{{ ad(number_format("-6276128000")) }}
											</span>
											<small>{{ $currency }}</small>
										</td>
									</tr>
									<tr>
										<td>
											{{ trans('depoint::general.cart_table.discount') }}
										</td>
										<td class="price big-total">
											{{ ad(number_format("6276128000")) }}
											<small>{{ $currency }}</small>
										</td>
									</tr>
									</tbody>
								</table>
								<div class="action ta-l">
									<button onclick="window.print();" class="button blue lg">
										<i class="icon-print"></i>
										{{ trans('depoint::general.print_invoice') }}
									</button>
								</div>
							</div>
						</div>

					</div>

				</div>

			</div>

		</div>

		</div>

@stop
