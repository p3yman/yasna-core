<div class="ta-c mt30 mb50">
	<img src="{{ Module::asset('depoint:images/error.svg') }}" width="128">
</div>

<div class="payment-result">
	<div class="title error">
		{{ trans('depoint::general.payment_message.error.title') }}
	</div>

	<p>
		{{ trans('depoint::general.payment_message.error.message') }}
	</p>

	<div class="action">
		<a href="#" class="button blue lg">
			{{ trans('depoint::general.cart_table.pay_again') }}
		</a>
	</div>
</div>
