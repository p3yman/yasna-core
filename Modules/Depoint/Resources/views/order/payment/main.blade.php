@extends('depoint::layout.plane')

@php

@endphp

@section('content')

	<div class="container">

		<div class="page">
			@include('depoint::layout.widgets.part-title',[
				"subtitle" => trans('depoint::general.shop') ,
				"title" => trans('depoint::general.payment')
			])

			<div class="row">

				<div class="col-sm-12">

					<div class="row">
						<div class="col-sm-10 col-center">

							<section class="panel">
								<article class="p50">
									@include('depoint::order.payment.error')
									{{--@include('depoint::order.payment.success',[--}}
										{{--"tracking_number" => "22336677" , --}}
									{{--])--}}
									{{--@include('depoint::order.payment.redirect')--}}
								</article>
							</section>

						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

@stop
