<div class="ta-c mt30 mb50">
	<img src="{{ Module::asset('depoint:images/credit-cards.svg') }}" width="128">
</div>

<div class="page-desc">
	{{ trans('depoint::general.payment_message.redirect') }}
</div>
