<div class="ta-c mt30 mb50">
	<img src="{{ Module::asset('depoint:images/ok.svg') }}" width="128">
</div>

<div class="payment-result">
	<div class="title">
		{{ trans('depoint::general.payment_message.success.title') }}
	</div>
	<p>
		{{ trans('depoint::general.payment_message.success.message') }}
	</p>

	<div class="code">{{ trans('depoint::general.tracking_number')." :". ad($tracking_number) }}</div>

	<div class="action">
		<a href="#" class="button blue lg">
			{{ trans('depoint::general.cart_table.back_to_shop') }}
		</a>
	</div>
</div>
