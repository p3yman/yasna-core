@extends('depoint::profile.layout.frame')

@php
	// Frame Values
	$profile_breadcrumb = ["تامین‌کنندگان", "پیشخوان"];

	// Parts Table
	$parts = [
		[
			"name" => "چرخ‌دنده‌ی ۲۵۱ A" ,
			"code" => "DA-76278" ,
			"stock_count" => "2665" ,
			"daily_usage" => "17" ,
			"alarm_date" => "17 آبان 1397" ,
			"alarm" => true ,
		],
		[
			"name" => "چرخ‌دنده‌ی ۲۵۱ A" ,
			"code" => "DA-76278" ,
			"stock_count" => "2665" ,
			"daily_usage" => "17" ,
			"alarm_date" => "17 آبان 1397" ,
			"alarm" => false ,
		],
		[
			"name" => "چرخ‌دنده‌ی ۲۵۱ A" ,
			"code" => "DA-76278" ,
			"stock_count" => "2665" ,
			"daily_usage" => "17" ,
			"alarm_date" => "17 آبان 1397" ,
			"alarm" => false ,
		],
		[
			"name" => "چرخ‌دنده‌ی ۲۵۱ A" ,
			"code" => "DA-76278" ,
			"stock_count" => "2665" ,
			"daily_usage" => "17" ,
			"alarm_date" => "17 آبان 1397" ,
			"alarm" => false ,
		],
		[
			"name" => "چرخ‌دنده‌ی ۲۵۱ A" ,
			"code" => "DA-76278" ,
			"stock_count" => "2665" ,
			"daily_usage" => "17" ,
			"alarm_date" => "17 آبان 1397" ,
			"alarm" => false ,
		],
		[
			"name" => "چرخ‌دنده‌ی ۲۵۱ A" ,
			"code" => "DA-76278" ,
			"stock_count" => "2665" ,
			"daily_usage" => "17" ,
			"alarm_date" => "17 آبان 1397" ,
			"alarm" => true ,
		],
		[
			"name" => "چرخ‌دنده‌ی ۲۵۱ A" ,
			"code" => "DA-76278" ,
			"stock_count" => "2665" ,
			"daily_usage" => "17" ,
			"alarm_date" => "17 آبان 1397" ,
			"alarm" => false ,
		],
	];

	$currency = "ریال";
	$tax = 9;

	// Orders table
	$orders = [
		[
			"code" => "19827" ,
			"date" => "4 آبان 1397" ,
			"total_price" => "25698000" ,
			"status" => [
				"title" => currentModule()->trans('general.status.processing') ,
				"color" => "blue" ,
			] ,
			"products" => [
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "2" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
			] ,
		],
		[
			"code" => "19827" ,
			"date" => "4 آبان 1397" ,
			"total_price" => "25698000" ,
			"status" => [
				"title" => currentModule()->trans('general.status.aborted') ,
				"color" => "red" ,
			] ,
			"products" => [
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "2" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
			] ,
		],
		[
			"code" => "19827" ,
			"date" => "4 آبان 1397" ,
			"total_price" => "25698000" ,
			"status" => [
				"title" => currentModule()->trans('general.status.done') ,
				"color" => "green" ,
			] ,
			"products" => [
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "2" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
			] ,
		],
		[
			"code" => "19827" ,
			"date" => "4 آبان 1397" ,
			"total_price" => "25698000" ,
			"status" => [
				"title" => currentModule()->trans('general.status.reviewing') ,
				"color" => "gray" ,
			] ,
			"products" => [
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "2" ,
				],
				[
					"title" => "۶۳۵ لیتر محصول فریزر بالا، با فناوری بدون برفک مدل T7" ,
					"image" => Module::asset('depoint:images/sample/product-cart.JPG') ,
					"color" => "سفید" ,
					"price" => "37350000" ,
					"currency" => "ریال" ,
					"count" => "1" ,
				],
			] ,
		],
	];
@endphp

@section('profile_content')
	@include('depoint::profile.tables.parts-list',[
		"parts" => $parts ,
		"mode" => "abstract" , 
	])

	@include('depoint::profile.tables.orders',[
		"orders" => $orders ,
		"mode" => "view" ,
	])
@stop
