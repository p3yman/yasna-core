@extends(currentModule()->bladePath('profile.layout.frame'))

@section('html-header')
	{!! fileManager()->uploader()->renderGeneralCss() !!}
@append

@section('end-body')
	{!! fileManager()->uploader()->renderGeneralJs() !!}
	{!! Html::script(currentModule()->asset('js/providers-panel.min.js')) !!}
@append

@section('profile_content')
	<div class="row">
		@include(currentModule()->bladePath('profile.panel.spare-part-single.files'))
		@include(currentModule()->bladePath('profile.panel.spare-part-single.image'))
	</div>
@append
