@php
	$file = fileManager()
		->file($file_hashid)
		->resolve();
	$image_url = $file->getUrl();
@endphp

@if ($image_url)
	<input type="hidden" name="preview_file" value="{{ $file_hashid }}">
	<a href="{{ $file->getDownloadUrl() }}" target="_blank">
		<img src="{{ $image_url }}" alt="" class="image spare-part-preview-image">
	</a>
@endif
