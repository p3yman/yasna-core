@php
	$main_uploader = $uploaders['main'];
@endphp

@section('html-header')
	{!! $main_uploader->renderCss() !!}
@append

@section('end-body')
	{!! $main_uploader->renderJs() !!}
@append

<div class="col-sm-6">
	<h5>{{ currentModule()->trans('spare-parts.spare-part.main-files') }}</h5>

	{!!
		widget('form-open')
			->target(route_locale('depoint.front.provider-panel.spare-parts.single.save-files'))
			->class('js')
	!!}

	{!! widget('hidden')->name('hashid')->value($spare_part->hashid) !!}

	@php $main_files = $spare_part->main_files @endphp

	<table class="table padded table-order-head" id="files-table"
		   data-item-url="{{ route_locale('depoint.front.provider-panel.spare-parts.single.file-item', ['hashid' => '__HASHID__']) }}">
		<tbody>
		@foreach($main_files as $file)
			@include(currentModule()->bladePath('profile.panel.spare-part-single.file-item'), ['file_hashid' => $file])
		@endforeach
		</tbody>
	</table>

	<div class="row">
		<div class="col-sm-12" id="main-file-uploader">
			{!!
				$main_uploader->render()
		 	!!}
		</div>

		<div class="col-sm-12 ta-l">
			<button class="button blue sm">
				{{ currentModule()->trans('general.update') }}
			</button>
		</div>

		<div class="col-sm-12 mt10">
			{!! widget('feed') !!}
		</div>
	</div>

	{!! widget('form-close') !!}
</div>
