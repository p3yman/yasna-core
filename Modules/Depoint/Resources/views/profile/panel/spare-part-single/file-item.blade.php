@php
	$file = fileManager()
		->file($file_hashid)
		->resolve();
@endphp

<tr>
	<td>{{ $file->getFileName() }}</td>
	<td>
		<input type="hidden" name="main_files[]" value="{{ $file_hashid }}">
		<a href="{{ $file->getDownloadUrl() }}" class="color-blue">
			<i class="icon-cloud-download"></i>
		</a>
	</td>
	<td class="text-left">
		<a href="#" class="color-red file-item-delete">
			<i class="icon-times"></i>
		</a>
	</td>
</tr>
