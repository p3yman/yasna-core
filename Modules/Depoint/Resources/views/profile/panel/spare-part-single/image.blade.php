@php
	$preview_uploader = $uploaders['preview'];
@endphp

@section('html-header')
	{!! $preview_uploader->renderCss() !!}
@append

@section('end-body')
	{!! $preview_uploader->renderJs() !!}
@append

<div class="col-sm-6">
	<h5>{{ currentModule()->trans('spare-parts.spare-part.preview-file') }}</h5>

	{!!
		widget('form-open')
			->target(route_locale('depoint.front.provider-panel.spare-parts.single.save-image'))
			->class('js')
	!!}

	{!! widget('hidden')->name('hashid')->value($spare_part->hashid) !!}

	<div class="row">

		<div class="col-sm-12 mb20" id="preview-image-container"
			 data-view-url="{{ route_locale('depoint.front.provider-panel.spare-parts.single.image-view', ['hashid' => '__HASHID__']) }}">
			@include(currentModule()->bladePath('profile.panel.spare-part-single.image-preview'), [
				'file_hashid' => $spare_part->preview_file,
			])
		</div>

		<div class="col-sm-12" id="preview-file-uploader">
			{!!
				$preview_uploader->render()
		 	!!}
		</div>

		<div class="col-sm-12 ta-l">
			<button class="button blue sm">
				{{ currentModule()->trans('general.update') }}
			</button>
		</div>

		<div class="col-sm-12 mt10">
			{!! widget('feed') !!}
		</div>
	</div>

	{!! widget('form-close') !!}
</div>
