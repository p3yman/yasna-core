@extends(currentModule()->bladePath('profile.layout.frame'))

@section('profile_content')
	{!!
		widget('form-open')
			->class('js')
			->target(route_locale('depoint.front.provider-panel.orders.change-status'))
			->id('status-form')
	!!}

	{!! widget('hidden')->name('hashid')->value($order->hashid) !!}

	<table class="table bordered padded table-order-head" id="form-table">
		<tbody>
		<tr>
			<td>
				<h4 class="mb0 p20 color-gray-light">
					{{ currentModule()->trans('general.order_code').": " }}
					<span class="color-blue-darker">
						{{ ad($order->code) }}
					</span>
				</h4>
			</td>
			<td><h4 class="mb0 p20 color-gray-light">
					{{ currentModule()->trans('general.order_date').": " }}
					<span class="color-blue-darker">
						{{ ad(echoDate($order->created_at, 'j F Y H:i'))}}
					</span>
				</h4>
			</td>
			<td class="pl50">
				@if ($order->isNotFinalized())
					<div class="field mb0">
						<label>
							{{ currentModule()->trans('general.order_status') }}
						</label>
						<div class="select">
							<select id="status" name="status">
								@foreach($order->selectableStatuses() as $status)
									<option value="{{ $status }}" @if($status == $order->status) selected @endif>
										{{ currentModule()->trans("spare-parts.order.status.$status") }}
									</option>
								@endforeach
							</select>
						</div>
					</div>
				@else
					<div class="label {{ $order->status_front_color }}">{{ $order->status_text }}</div>
				@endif
			</td>
		</tr>

		<tr style="display: none">
			<td colspan="3">
				{!! widget('feed') !!}
			</td>
		</tr>
		</tbody>
	</table>

	<button id="_submit" style="display: none"></button>

	{!! widget('form-close') !!}

	@include(currentModule()->bladePath('profile.tables.order-details'),[
		"products" => $order['products'] ,
	])
@stop

@include(currentModule()->bladePath('layout.widgets.form.scripts'))


@section('end-body')
	<script>
        function showFeedRow() {
            $('#form-table').find('.form-feed').closest('tr').show()
        }

        function hideFeedRow() {
            $('#form-table').find('.form-feed').closest('tr').hide()
        }

        $(document).ready(function () {
            $('select#status').change(function () {
                showFeedRow();

                $(this).closest('form').submit();
            })
        });
	</script>
@append
