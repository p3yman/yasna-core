@extends(currentModule()->bladePath('profile.layout.frame'))

@section('profile_content')
	<div class="ticket-scroll">
		@include(currentModule()->bladePath('profile.tables.orders'),[
			"orders" => $orders ,
			"mode" => "edit" ,
		])
	</div>

	{!! $orders->render() !!}

@stop
