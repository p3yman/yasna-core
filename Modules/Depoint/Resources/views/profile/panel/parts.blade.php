@extends(currentModule()->bladePath('profile.layout.frame'))

@section('profile_content')
	@include(currentModule()->bladePath('profile.tables.parts-list'),[
		"parts" => $parts ,
	])
@stop
