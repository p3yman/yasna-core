@php
	$profile_image = user()->gravatar;
	$user = [
		"image" => $profile_image ,
		"full_name" => user()->full_name ,
	];

	if(isset($user['image']) and $user['image']){
		$profile_image = $user['image'];
	}

@endphp
<section class="panel profile-head">
	<div class="avatar">
		<img src="{{ user()->gravatar }}" width="72">
		<a class="edit icon-pencil" href={{ route_locale('depoint.front.dashboard.edit-profile') }}></a>
	</div>

	<h3 class="name">
		{{ $user['full_name'] }}
	</h3>

	<div class="res-menu-toggle">
		<h3>
			{{ currentModule()->trans('general.menu') }}
		</h3>
		<span class="icon-angle-down"></span>
	</div>

	<ul class="profile-menu">
		@foreach($sidebar_menu as $menu_item)
			<li @if($menu_item['link'] == request()->url()) class="active" @endif>
				<a href="{{ $menu_item['link'] }}">{{ $menu_item['label'] }}</a>
			</li>
		@endforeach

		{{--todo: The following lines are commented to keep some hints for the future developments--}}

		{{--<li class="active">--}}
		{{--<a href="#">--}}
		{{--{{ currentModule()->trans('general.dashboard') }}--}}
		{{--</a>--}}
		{{--</li>--}}
		{{--<li>--}}
		{{--<a href="#">--}}
		{{--{{ currentModule()->trans('general.parts') }}--}}
		{{--</a>--}}
		{{--</li>--}}
		{{--<li>--}}
		{{--<a href="#">--}}
		{{--{{ currentModule()->trans('general.orders') }}--}}
		{{--</a>--}}
		{{--</li>--}}
		{{--<li>--}}
		{{--<a href="#">--}}
		{{--{{ currentModule()->trans('general.tickets') }}--}}
		{{--</a>--}}
		{{--</li>--}}
		{{--<li class="divider"></li>--}}
		{{--<li>--}}
		{{--<a href="#">--}}
		{{--{{ currentModule()->trans('general.profile_edit') }}--}}
		{{--</a>--}}
		{{--</li>--}}
		{{--<li>--}}
		{{--<a href="#">--}}
		{{--{{ currentModule()->trans('general.password_edit') }}--}}
		{{--</a>--}}
		{{--</li>--}}
		{{--<li>--}}
		{{--<a href="#">--}}
		{{--{{ currentModule()->trans('general.credit_increase') }}--}}
		{{--</a>--}}
		{{--</li>--}}
	</ul>
</section>
