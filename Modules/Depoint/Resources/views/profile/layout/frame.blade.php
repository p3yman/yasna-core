@extends(currentModule()->bladePath('layout.plane'))


@php depointTemplate()->mergeWithPageTitle($profile_breadcrumb) @endphp


@section('content')

	<div class="container">

		<div class="page">

			<div class="row">

				<div class="col-sm-3">
					@include(currentModule()->bladePath('profile.layout.panel-menu'))
				</div>


				<div class="col-sm-9">

					<div class="part-title panel-title" data-aos="fade-down">
						@foreach($profile_breadcrumb as $key => $item)
							@if($loop->iteration !== $loop->count)
								<div class="subtitle">
									{{ $item }}
								</div>
							@else
								<div class="title">
									{{ $item or "" }}
								</div>
							@endif
						@endforeach
					</div>

					@yield('profile_content')

				</div>

			</div>

		</div>

	</div>
@stop
