<div class="table-scroll">
	<table class="table bordered padded">
		<thead>
		<tr>
			<th>
				{{  currentModule()->trans('general.table.row') }}
			</th>
			<th>
				{{  currentModule()->trans('validation.attributes.code') }}
			</th>
			<th>
				{{  currentModule()->trans('general.cart_table.count') }}
			</th>
		</tr>
		</thead>
		<tbody>
		@foreach($order->items as $item)
			@php $spare_part = $item->spare_part @endphp
			<tr class="product-row">
				<td>
					{{ ad($loop->iteration) }}
				</td>
				<td>
					{{ $spare_part->name }}
				</td>
				<td class="qty review">
					{{ ad($item->count) }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
