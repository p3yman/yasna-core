

<section class="panel profile-value">
	<article>
		<h5>
			{{ currentModule()->trans('general.wallet_inventory') }}
		</h5>
		<h2 class="mb0">
			{{ ad(number_format($user_credit)) }}
			<small>
				{{ $currency }}
			</small>
		</h2>
		@if (isset($show_action) and $show_action)
			<a href="#" class="charge button red">
				<i class="icon-plus"></i>
				{{ currentModule()->trans('general.credit_increase') }}
			</a>
		@endif
	</article>
</section>
