<div class="table-scroll">
	<table class="table bordered @if($mode === "view") orders @endif">
		<thead>
		@if($mode === "view")
			<tr>
				<th colspan="100%" class="p20">
					<div class="part-title panel-title">
						<div class="title">
							{{ $table_title or currentModule()->trans('general.orders') }}
						</div>
						<a class="button action blue sm"
						   href="{{ route_locale('depoint.front.provider-panel.orders') }}">
							{{ currentModule()->trans('general.orders_archive') }}
						</a>
					</div>
				</th>
			</tr>
		@endif
		<tr>
			<th>
				{{ currentModule()->trans('general.table.row') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.order_number') }}
			</th>
			@if ($multiple_providers)
				<th>
					{{ currentModule()->trans('spare-parts.provider.title.singular') }}
				</th>
			@endif
			<th>
				{{ currentModule()->trans('general.table.date') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.status') }}
			</th>
		</tr>
		</thead>
		<tbody>
		@foreach($orders as $order)
			<tr class="order-row">
				<td>
					{{ ad($loop->iteration) }}
				</td>
				<td class="fw-b color-blue-darker">
					{{ ad($order->code) }}
				</td>


				@if ($multiple_providers)
					<th>{{ $order->provider->name }}</th>
				@endif

				<td class="fw-b">
					{{ ad(echoDate($order->created_at, 'j F Y - H:i')) }}
				</td>
				<td>
					<div class="label {{ $order->status_front_color }}">{{ $order->status_text }}</div>
				</td>
				<td class="toggle">
					@if($mode === "view")
						<button class="button icon-angle-down gray-lightest alt"></button>
					@elseif($mode === "edit")
						@php
							$order_link = route_locale('depoint.front.provider-panel.orders.single', [
								'hashid' => $order->hashid
							]);
						@endphp
						<a href="{{ $order_link }}" class="button icon-pencil gray-lightest alt"></a>
					@endif
				</td>
			</tr>
			@if($mode === "view")
				<tr class="order-detail">
					<td colspan="100%">
						@include('depoint::profile.tables.order-details',[
							"order" => $order,
						])
					</td>
				</tr>
			@endif
		@endforeach
		</tbody>
	</table>
</div>
