<div class="table-scroll">
	<table class="table bordered parts">
		<thead>
		@if(isset($mode) and $mode === "abstract")
			<tr>
				<th colspan="100%" class="p20">
					<div class="part-title panel-title">
						<div class="title">
							{{ currentModule()->trans('general.parts') }}
						</div>
						<button class="button action blue sm">
							{{ currentModule()->trans('general.parts_archive') }}
						</button>
					</div>
				</th>
			</tr>
		@endif
		<tr>
			<th>
				{{ currentModule()->trans('general.table.row') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.name') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.code') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.current_stock') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.daily_usage_count') }}
			</th>
			<th>
				{{ currentModule()->trans('general.table.alarm_time') }}
			</th>
			<th width="1"></th>
		</tr>
		</thead>
		<tbody>
		@foreach($parts as $part)
			<tr>
				<td>
					{{ $loop->iteration }}
				</td>
				<td class="fw-b color-blue-darker">
					{{ $part['name'] }}
				</td>
				<td class="fw-b color-blue-darker">
					{{ $part['code'] }}
				</td>
				<td class="fw-b color-blue-darker">
					{{ ad($part['inventory']) }}
				</td>
				<td class="fw-b color-blue-darker">
					{{ ad($part['daily_use']) }}
				</td>
				<td class="fw-b {{ (!$part['is_ok'])? "color-red":"" }}">
					{{ ad($part['alert_days']) }}
				</td>
				<td class="toggle">
					@php
						$url = route_locale('depoint.front.provider-panel.spare-parts.single', ['hashid' => $part->hashid])
					@endphp
					<a href="{{ $url }}" class="button icon-pencil gray-lightest alt"></a>
				</td>
			</tr>
		@endforeach

		</tbody>
	</table>

	{!! $parts->render() !!}
</div>
