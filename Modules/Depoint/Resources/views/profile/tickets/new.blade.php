@extends(currentModule()->bladePath('profile.layout.frame'))


@section('profile_content')
	<section class="panel">
		<article>
			{!! widget('form-open')->target(route_locale('depoint.front.dashboard.tickets.add.new'))->class('js') !!}

			@include('depoint::layout.widgets.form.input-text',[
				"label" => currentModule()->trans('general.table.subject') ,
				"name"=>'subject'
			])

			<div class="row">

				<div class="col-sm-6">
					@include(currentModule()->bladePath('layout.widgets.form.select'),[
						"label" => currentModule()->trans('general.table.priority') ,
						'name'=>'priority',
						"options" =>  $flags,
					])

				</div>

			</div>

			@include('depoint::layout.widgets.form.textarea',[
				"label" => currentModule()->trans('general.ticket_text') ,
				'name'=>'text',
				"rows" => "10" ,
			])


			<div class="ta-l mt10">
				<button type="submit" class="button blue lg">
					{{ currentModule()->trans('general.submit_ticket') }}
				</button>
			</div>

			<div class="mt-20">
				{!! widget('feed') !!}
			</div>
		</article>
	</section>
	{!! widget('form-close') !!}
@stop

@include(currentModule()->bladePath('layout.widgets.form.scripts'))


