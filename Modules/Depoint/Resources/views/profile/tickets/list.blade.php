@extends(currentModule()->bladePath('profile.layout.frame'))


@section('profile_content')

	<div class="mb10 ta-l">
		<a href="{{route_locale('depoint.front.dashboard.tickets.new')}}" class="button blue">
			{{ currentModule()->trans('general.ticket-add') }}
		</a>
	</div>

	@if($tickets->count())
		<div class="table-scroll">
			<table class="table bordered orders">
				<thead>
				<tr>
					<th>
						{{ currentModule()->trans('general.table.ticket_id') }}
					</th>
					<th>
						{{ currentModule()->trans('general.table.subject') }}
					</th>
					{{--<th>--}}
					{{--{{ currentModule()->trans('general.table.organization') }}--}}
					{{--</th>--}}
					<th>
						{{ currentModule()->trans('general.table.status') }}
					</th>
					<th>
						{{ currentModule()->trans('general.table.updated_at') }}
					</th>
					{{--<th>--}}
					{{--{{ currentModule()->trans('general.table.ticket_id') }}--}}
					{{--</th>--}}
					<th width="1"></th>
				</tr>
				</thead>
				<tbody>
				@foreach($tickets as $ticket)
					<tr>
						<td class="fw-b color-blue-darker">
							{{ ad($ticket->getIdForView()) }}
						</td>
						<td>
							{{ $ticket->title }}
						</td>
						{{--<td class="fw-b color-gray">--}}
						{{--{{ $ticket['organization'] }}--}}
						{{--</td>--}}
						<td>
							<div class="label {{ currentModule()->trans('colors.ticket-flags.'.$ticket->flag) }}">
								{{ $ticket->getFlagTrans() }}
							</div>
						</td>
						<td>
							{{ ad(echoDate(model('ticket',$ticket->id)->getLastUpdateTime(),"j F Y H:i")) }}
						</td>
						{{--<td>--}}
						{{--<div class="label {{ $ticket['status']['color'] }}">--}}
						{{--{{ $ticket['status']['title'] }}--}}
						{{--</div>--}}
						{{--</td>--}}
						<td>
							<a href="{{route_locale('depoint.front.dashboard.tickets.single',[$ticket->hashid])}}"
							   class="button icon-eye gray-lightest alt"></a>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>

		{!! $tickets->render() !!}
	@else
		@include(currentModule()->bladePath('layout.widgets.alert'), [
			 'class' => 'alert-default',
			 'text' => currentModule()->trans('general.no-item-to-show')
		 ])
	@endif
@stop
