@extends(currentModule()->bladePath('profile.layout.frame'))


@section('profile_content')
	<div class="row">

		<section class="panel ticket-info">
			<article>
				<div class="item">
					<small>
						{{ currentModule()->trans('general.table.updated_at') }}
					</small>
					<h5>
						{{ ad(echoDate($ticket->getLastUpdateTime(),"j F Y H:i")) }}
					</h5>
				</div>
				{{--<div class="item">--}}
				{{--<small>--}}
				{{--{{ currentModule()->trans('general.table.organization') }}--}}
				{{--</small>--}}
				{{--<h5>--}}
				{{--{{ $ticket['organization'] }}--}}
				{{--</h5>--}}
				{{--</div>--}}
				<div class="item">
					<small>
						{{ currentModule()->trans('general.table.status') }}
					</small>
					<div class="label {{ currentModule()->trans('colors.ticket-flags.'.$ticket->flag) }}">
						{{ $ticket->getFlagTrans() }}
					</div>
				</div>
				{{--<div class="item">--}}
				{{--<small>--}}
				{{--{{ currentModule()->trans('general.table.priority') }}--}}
				{{--</small>--}}
				{{--<div class="label {{ $ticket['priority']['color'] }}">--}}
				{{--{{ $ticket['priority']['title'] }}--}}
				{{--</div>--}}
				{{--</div>--}}
			</article>
		</section>

		<section class="panel ticket-content">
			<header>
				<div class="title">
					{{ $ticket->title }}
				</div>
				<div class="content">
					{{ $ticket->text }}
				</div>
			</header>
			<article>
				@foreach($replies as $reply)

					<div class="ticket-post @if($reply->user_id != user()->id) support @endif">
						<div class="head">
							<h5>
								{{ $reply->user->fullname }}
							</h5>
							<span class="date">
								{{ ad(echoDate($reply->created_at,"j F Y H:i")) }}
							</span>
						</div>
						<div class="content">
							@if($reply->type=='changing-flag')
								{{$reply->getChangingFlagTrans()}}
							@else
								{!! $reply->text !!}
							@endif
						</div>
					</div>
				@endforeach
			</article>
		</section>


		@if($ticket->flag!='done')
			<section class="panel">
				<article>

					{!! widget('form-open')->target(route_locale('depoint.front.dashboard.tickets.add.new.reply'))->class('js') !!}
					{!! widget('hidden')->name('ticket_hash')->value($ticket->hashid) !!}

					@include(currentModule()->bladePath('layout.widgets.form.textarea'),[
					"label" => currentModule()->trans('general.answer_ticket') ,
					"rows" => "10" ,
					'name'=> 'text'
					])

					<div class="ta-l mt10">
						<button class="button blue lg">
							{{ currentModule()->trans('general.send_answer') }}
						</button>
					</div>
					<div class="mt10">
						{!! widget('feed') !!}
					</div>
					{!! widget('form-close') !!}
				</article>
			</section>
		@endif
	</div>
@stop

@include(currentModule()->bladePath('layout.widgets.form.scripts'))
