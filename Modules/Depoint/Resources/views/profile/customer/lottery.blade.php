@extends('depoint::profile.layout.frame')

@php
	$currency = depoint()->shop()->currencyTrans();
@endphp

@section('profile_content')

	<section class="panel">
		<article>
			<div class="part-title panel-title">
				<div class="title">
					{{ currentModule()->trans('general.submitted_lottery_codes') }}
				</div>
				{{--<button class="button blue action" data-modal="#new-code-modal">--}}
					{{--{{ currentModule()->trans('general.new_lottery_code') }}--}}
				{{--</button>--}}
			</div>

			<div class="table-scroll">
				<table class="table bordered">
					<thead>
					<tr>
						<th>
							{{ currentModule()->trans('general.table.row') }}
						</th>
						<th>
							{{ currentModule()->trans('general.table.code') }}
						</th>
						<th>
							{{ currentModule()->trans('general.table.submitting_date') }}
						</th>
						<th>
							{{ currentModule()->trans('general.order_date') }}
						</th>
						<th>
							{{ currentModule()->trans('general.table.price') }}
						</th>
					</tr>
					</thead>
					<tbody>
					@foreach($codes as $code)
						<tr>
							<td>
								{{ ad($loop->iteration )  }}
							</td>
							<td class="fw-b color-blue-darker">
								{{ ad($code->code) }}
							</td>
							<td>
								{{ ad(echoDate($code->purchased_at, 'j F y')) }}
							</td>
							<td>
								{{ ad(echoDate($code->cart->created_at, 'j F y')) }}
							</td>
							<td class="fw-b">
								@php $amount = depoint()->shop()->convertToMainCurrency($code->purchased_amount) @endphp
								{{ ad(number_format($amount)) . " " . $currency }}
							</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</article>
	</section>

	<!-- Modal -->
	@component('depoint::layout.widgets.modal',[
		"id" => "new-code-modal" ,
		"size" => "sm" ,
	])

		<article>
			<div class="part-title panel-title">
				<div class="title">
					{{ currentModule()->trans('general.new_lottery_code') }}
				</div>
			</div>
			<div class="gift-inputs">
				<div class="wrap">
					<input type="text" class="gift-input vazir-fn" maxlength="5" placeholder="-----">
				</div>
				<div class="wrap">
					<input type="text" class="gift-input vazir-fn" maxlength="5" placeholder="-----">
				</div>
				<div class="wrap">
					<input type="text" class="gift-input vazir-fn" maxlength="5" placeholder="-----">
				</div>
				<div class="wrap">
					<input type="text" class="gift-input vazir-fn" maxlength="5" placeholder="-----">
				</div>
			</div>
			<div class="action ta-l">
				<button class="button blue">
					{{ currentModule()->trans('general.check_code') }}
				</button>
			</div>
			<div class="result">
				<div class="message error">
					{{ currentModule()->trans('general.lottery_code_message.error') }}
				</div>
				<div class="message success">
					{{ currentModule()->trans('general.lottery_code_message.success') }}
				</div>
			</div>
		</article>

	@endcomponent
	<!-- End! Modal -->

@stop
