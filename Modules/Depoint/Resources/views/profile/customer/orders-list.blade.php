@php
	$currency = depoint()->shop()->currencyTrans();
@endphp

<div class="table-scroll">
	<table class="table bordered">
		<thead>
		@isset($table_title)
			<tr>
				<th colspan="100%" class="p20">
					<div class="part-title panel-title">
						<div class="title">
							{{ $table_title }}
						</div>
					</div>
				</th>
			</tr>
		@endisset
		<tr>
			<th>
				{{ $__module->getTrans('general.table.row') }}
			</th>
			<th>
				{{ $__module->getTrans('cart.receipt-number') }}
			</th>
			<th>
				{{ $__module->getTrans('cart.order.date') }}
			</th>
			<th>
				{{ $__module->getTrans('cart.price.total') }}
			</th>
			<th>
				{{ $__module->getTrans('cart.receiver.title.singular') }}
			</th>
			<th>
				{{ $__module->getTrans('cart.order.status') }}
			</th>
		</tr>
		</thead>
		<tbody>
		@forelse($orders as $order)

			@php $order_url = route_locale('depoint.cart.payment-callback', ['cart' => $order->hashid]) @endphp

			<tr>
				<td>
					{{ ad($loop->iteration )  }}
				</td>
				<td class="fw-b color-blue-darker">
					<a href="{{ $order_url }}">
						{{ ad($order->code) }}
					</a>
				</td>
				<td>
					{{ ad(echoDate($order->created_at, 'j F Y')) }}
				</td>
				<td class="fw-b">
					@php
						$amount = depoint()->shop()->convertToMainCurrency($order->invoiced_amount)
					@endphp
					{{ ad(number_format($amount)) . " " . $currency }}
				</td>

				<td>
					{{ ($order->address->receiver ?? null ) }}
				</td>

				<td>
					<div class="label @if($order->isConfirmed()) green @else red @endif">
						{{ $order->status_text }}
					</div>
				</td>
			</tr>

		@empty
			<tr>
				<td colspan="6">
					<div style="padding: 40px 10px; text-align: center">
						{{ $__module->getTrans('cart.order.empty-alert') }}
					</div>
				</td>
			</tr>
		@endforelse
		</tbody>
	</table>

	@if (method_exists($orders, 'render'))
		{!! $orders->render() !!}
	@endif
</div>
