@extends('depoint::profile.layout.frame')

@php
	// Frame Values
	$profile_breadcrumb = ["پروفایل کاربری", "افزایش اعتبار"];

@endphp

@section('profile_content')

	@include('depoint::profile.tables.user_credit',[
			"user_credit" => "5379000" ,
			"currency" => "ریال" ,
			"show_action" => false ,
		])

	<section class="panel">
		<article>
			<div class="part-title panel-title">
				<div class="title">
					{{ currentModule()->trans('general.charge_account') }}
				</div>
			</div>
			@include('depoint::layout.widgets.form.input-text',[
				"field_class" => "lg" ,
				"label" => currentModule()->trans('general.table.price') ,
				"type" => "number" ,
			])

			<div class="ta-l mt10">
				<button class="button blue">
					{{ currentModule()->trans('general.checkout_price') }}
				</button>
			</div>
		</article>
	</section>

@stop
