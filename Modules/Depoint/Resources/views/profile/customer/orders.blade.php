@extends($__module->getBladePath('profile.layout.frame'))

@section('profile_content')

	<section class="panel">
		<article>
			<div class="part-title panel-title">
				<div class="title">
					{{ $__module->getTrans('cart.order.title.plural') }}
				</div>
			</div>

			@include($__module->getBladePath('profile.customer.orders-list'))
		</article>
	</section>

@stop
