@extends('depoint::profile.layout.frame')

@section('profile_content')

	<section class="panel">
		<article>
			{!! widget('form-open')
					->target(route_locale('depoint.front.dashboard.edit-password.submit'))
					->class('js')
					->id('edit-profile')
			!!}
			<div class="row">

				<div class="col-sm-5">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "old_password" ,
						"label" => currentModule()->trans('validation.attributes.last_password'),
						"type" => "password",
					])
				</div>

			</div>

			<br>

			<div class="row">

				<div class="col-sm-5">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "password" ,
						"label" => currentModule()->trans('validation.attributes.new_password'),
						"type" => "password",
					])
				</div>

			</div>

			<div class="row">

				<div class="col-sm-5">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "password2" ,
						"label" => currentModule()->trans('validation.attributes.new_password_repeat'),
						"type" => "password",
					])
				</div>

			</div>

			{!! widget('feed') !!}

			<div class="ta-l mt10">
				<button class="button blue lg">
					{{ currentModule()->trans('general.update') }}
				</button>
			</div>

			{!! widget('form-close') !!}
		</article>
	</section>

@stop

@include(currentModule()->bladePath('layout.widgets.form.scripts'))