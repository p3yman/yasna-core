@extends($__module->getBladePath('profile.layout.frame'))

@section('profile_content')

	<section class="panel">
		<article>
			<div class="part-title panel-title">
				<div class="title">
					{{ $__module->getTrans('address.title.plural') }}
				</div>

				<div class="ta-l">
					<a class="button blue"
					   href="{{ route_locale('depoint.front.dashboard.addresses.single', ['hashid' => 'new']) }}">
						{{ $__module->getTrans('address.add') }}
					</a>
				</div>
			</div>

			<div class="col-sm-12">
				{!!
					widget('form-open')
						->id('delete-address-form')
						->class('js')
				!!}

				{!! widget('feed') !!}

				{!! widget('form-close') !!}
			</div>

			<div class="table-scroll">
				<table class="table bordered" id="addresses-table">
					<thead>
					<tr>
						<th>
							{{ $__module->getTrans('general.table.row') }}
						</th>
						<th>
							{{ trans_safe('validation.attributes.receiver') }}
						</th>
						<th>
							{{ $__module->getTrans('address.details') }}
						</th>
					</tr>
					</thead>
					<tbody>
					@forelse($addresses as $address)
						<tr id="tr-{{ $address->hashid }}">
							<td>
								{{ ad($loop->iteration )  }}
							</td>

							<td>
								{{ $address->receiver }}
							</td>

							<td>
								<div class="row">
									<div class="col-sm-12">
										@php
											$city = $address->city;
											$province = ($city->province ?? null);
										@endphp

										@if ($city and $province)
											{{ $province->title }} / {{ $city->title }}
										@endif
									</div>

									<div class="col-sm-6">
										@if ($address->telephone)
											{{ trans_safe('validation.attributes.telephone') }}:
											{{ ad($address->telephone) }}
										@endif
									</div>

									<div class="col-sm-6">
										@if ($address->postal_code)
											{{ trans_safe('validation.attributes.postal_code') }}:
											{{ ad($address->postal_code) }}
										@endif
									</div>

									<div class="col-sm-12">
										{{ $address->address }}
									</div>

								</div>
							</td>

							<td>
								<a class="button icon-pencil gray-lightest alt"
								   href="{{ route_locale('depoint.front.dashboard.addresses.single', ['hashid' => $address->hashid]) }}"></a>
							</td>

							<td>
								<button class="button icon-trash-o gray-lightest alt js-remove-address"
										data-action="{{ route_locale('depoint.front.dashboard.addresses.delete', ['hashid' => $address->hashid]) }}"></button>
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="5">
								<div style="padding: 40px 10px; text-align: center">
									{{ $__module->getTrans('address.empty-text') }}
								</div>
							</td>
						</tr>
					@endforelse
					</tbody>
				</table>

				{!! $addresses->render() !!}
			</div>

		</article>
	</section>

@append

@section('end-body')
	<script>
        $(document).ready(function () {

            function resultForm() {
                return $('#delete-address-form');
            }


            $('.js-remove-address').click(function () {
                let sure = confirm("{{ $__module->getTrans('address.delete-alert') }}");

                if (!sure) {
                    return;
                }

                let that        = $(this);
                let action_url  = that.data('action');
                let result_form = resultForm();
                let tr          = that.closest('tr');

                tr.addClass('disabled-box');
                result_form.attr('action', action_url);
                result_form.trigger('submit');
            });
        });
	</script>
@append
