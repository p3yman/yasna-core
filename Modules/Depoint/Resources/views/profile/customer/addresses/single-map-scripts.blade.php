@php
	$site_location = get_setting('location');
@endphp

@section('end-body')
	<script>

        $(document).ready(function () {
            let main_map;
            let main_marker;

            function initMap() {
                main_map = L.map('map-box', {
                    doubleClickZoom: false,
                });

                L.tileLayer(leaflet_map.layer_uri, {
                    maxZoom    : 18,
                    id         : 'mapbox.streets',
                    accessToken: leaflet_map.token,
                }).addTo(main_map);

                return main_map;
            }


            function initMarker(latlng) {
                main_marker = L.marker(latlng, {
                    draggable: true,
                }).addTo(main_map);

                main_marker.on('move', function (e) {
                    setInputValue(e.latlng);
                });
            }


            function setInputValue(latlng) {
                let input_value = latlng.lat + ',' + latlng.lng;
                $('.map-container').find('#location').val(input_value);
            }


            function showCurrentMarker() {
                let input              = $('.map-container').find('#location');
                let coordinates_string = input.val();

                if (!coordinates_string) {
                    return;
                }

                let coordinates = coordinates_string.split(',');
                if (coordinates.length != 2) {
                    return;
                }


                let latlng = L.latLng(coordinates[0], coordinates[1]);


                initMap();
                main_map.setView(latlng, 13);
                initMarker(latlng);
                setInputValue(latlng);
                $('.map-in-form-cover').hide()
            }

            $('.map-in-form-cover').click(function () {
                let cover_element = $(this);

                let map = initMap();

                map.locate({
                    setView: true,
                    maxZoom: 16,
                });

                map.on('locationfound', function (e) {
                    initMarker(e.latlng);
                    setInputValue(e.latlng);
                });

                map.on('locationerror', function () {
                    let site_location = {!! json_encode($site_location ?? []) !!};
                    if (site_location.length == 2) {
                        let latlng = L.latLng(site_location[0], site_location[1]);
                        main_map.setView(latlng, 13);
                        initMarker(latlng);
                        setInputValue(latlng);
                    } else {
                        cover_element.siblings('.not-allowed-message').show();
                    }
                });

                cover_element.hide();
            });


            showCurrentMarker();
        });
	</script>
@append
