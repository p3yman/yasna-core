@extends($__module->getBladePath('profile.layout.frame'))

@section('profile_content')

	<section class="panel">
		<article>
			<div class="part-title panel-title">
				<div class="title">
					{{ $profile_breadcrumb[2] }}
				</div>
			</div>

			{!! widget('form-open')
					->target(route_locale('depoint.front.dashboard.addresses.save'))
					->class('js')
					->id('address-form')
			!!}

			{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

			<div class="row">

				<div class="col-sm-4">
					@include($__module->getBladePath('layout.widgets.form.input-text'),[
						"name" => "receiver" ,
						"label" => trans_safe('validation.attributes.receiver'),
						"value" => $model->receiver,
					])
				</div>

				<div class="col-sm-4">
					@include($__module->getBladePath('layout.widgets.form.input-text'),[
						"name" => "telephone" ,
						"label" => trans_safe('validation.attributes.telephone'),
						"value" => $model->telephone,
					])
				</div>

				<div class="col-sm-4">
					@include($__module->getBladePath('layout.widgets.form.input-text'),[
						"name" => "postal_code" ,
						"label" => trans_safe('validation.attributes.postal_code'),
						"value" => $model->postal_code,
					])
				</div>

			</div>

			<div class="row">
				<div class="col-sm-4">
					@php
						$current_province = ($model->province ?? model('state'));
						$current_city = ($model->city?? model('state'));
						$cities = depoint()->provinceCitiesCombo($current_province, 'value');
					@endphp

					@include($__module->getBladePath('layout.widgets.form.select'),[
						"name" => "province_id",
						"label" => trans('validation.attributes.province_id') ,
						"options" => depoint()->provincesCombo('value'),
						"id" => "province",
						"value" => $current_province->hashid,
					])
				</div>
				<div class="col-sm-4">
					@include($__module->getBladePath('layout.widgets.form.select'),[
						"name" => "city_id" ,
						"label" => trans('validation.attributes.city') ,
						"options" => ($cities ?? []),
						"id" => "city" ,
						"value" => $current_city->hashid,
					])
				</div>
			</div>


			<div class="row">
				<div class="col-sm-12">
					@include($__module->getBladePath('layout.widgets.form.textarea'),[
						"name" => "address" ,
						"row" => "3",
						"label" => trans('validation.attributes.address') ,
						"id" => "address" ,
						"value" => $model->address,
					])
				</div>
			</div>


			<div class="row">
				<div class="col-sm-12">
					<div class="map-container">
						{!!
							widget('hidden')
								->name('location')
								->id('location')
								->value($model->location)
						!!}

						<div id="map-box" class="map-box"></div>
						<div class="not-allowed-message" style="display: none;">
							<h1 class="ta-c"><i class="icon-times-circle color-red"></i></h1>
							<p>{{ $__module->getTrans('address.location.message.is-disabled-in-browser') }}</p>
							<p>
								{{ $__module->getTrans('general.browser.firefox') }}:
								{{ $__module->getTrans('address.location.guide.activation-firefox') }}
							</p>
							<p>
								{{ $__module->getTrans('general.browser.chrome') }}:
								{!! $__module->getTrans('address.location.guide.activation-chrome', [
									'button' => '<i class="fa fa-exclamation-circle"></i>'
								])  !!}
							</p>
						</div>
						<div class="map-in-form-cover">
							<div class="map-in-form-cover-alert">
								{{ $__module->getTrans('address.location.message.select-to-increase-precision') }}
							</div>
						</div>
					</div>
				</div>
			</div>

			{!! widget('feed') !!}

			<div class="ta-l mt10">
				<button class="button blue lg">
					{{ $__module->getTrans('address.save') }}
				</button>
			</div>

			{!! widget('form-close') !!}
		</article>
	</section>

@append


@section('html-header')
	@include($__module->getBladePath('layout.widgets.map.assets'))
@append

@section('end-body')
	<script>
        $(document).ready(function () {
            $("#province").change(function () {
                let value             = $(this).val();
                let city_combo        = $("#city");
                let before_city_value = city_combo.val();

                $.ajax({
                    url       : "{{ route_locale('depoint.front.dashboard.related-cities', ['province' => '__PROVINCE__']) }}"
                        .replace('__PROVINCE__', value),
                    beforeSend: function () {
                        city_combo.prop('disabled', true);
                    },
                    success   : function (response) {
                        city_combo.html(response).prop('disabled', false);

                        let option_to_select = city_combo.children('option[value=' + before_city_value + ']');
                        if (option_to_select.length) {
                            option_to_select.prop('selected', true);
                        }
                    }
                });
            }).trigger('change');

        });

        function addressAddedCallback(hashid) {
            let new_url = "{{ route_locale('depoint.front.dashboard.addresses.single', ['hashid' => '__HASHID__']) }}"
                .replace('__HASHID__', hashid);

            window.location.replace(new_url);
        }

	</script>
@append

@include($__module->getBladePath('profile.customer.addresses.single-map-styles'))
@include($__module->getBladePath('profile.customer.addresses.single-map-scripts'))
