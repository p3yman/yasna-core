@section('html-header')
	<style>
		.map-container {
			position: relative;
			box-shadow: 0 5px 15px rgba(0,0,0,0.1);
			margin-bottom: 15px;
		}

		.map-container .map-box {
			z-index: 1;
			height: 300px;
		}


		.map-container .not-allowed-message {
			position: absolute;
			top: 0;
			height: 100%;
			width: 100%;
			background-color: #fff;
			z-index: 3;
			padding: 10px 15px;
		}

		.map-container .not-allowed-message p {
			text-align: justify;
			font-size: .8125rem;
		}

		.map-container .map-in-form-cover {
			position: absolute;
			top: 0;
			height: 100%;
			width: 100%;
			background-color: #fff;
			z-index: 3;
			text-align: center;
			padding-top: 100px;
			cursor: pointer;
		}

		.map-container .map-in-form-cover .map-in-form-cover-alert {
			background-color: rgba(221, 244, 48, 0.6);
			height: 100px;
			padding: 40px 15px;
		}
	</style>
@append
