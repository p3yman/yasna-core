@extends(currentModule()->bladePath('profile.layout.frame'))

@section('profile_content')

	{{--todo: The following lines are commented to be dynamic in the future--}}

	{{--@include(currentModule()->bladePath('profile.tables.user_credit'),[--}}
	{{--"user_credit" => 226000 ,--}}
	{{--"currency" => "ریال" ,--}}
	{{--"show_action" => true ,--}}
	{{--])--}}

	@if ($with_providers_data)
		@include(currentModule()->bladePath('profile.tables.orders'),[
			"orders" => $orders ,
			"mode" => "view" ,
			"table_title" => $__module->getTrans('spare-parts.order.title.plural')
		])
	@endif

	@include(currentModule()->bladePath('profile.customer.orders-list'),[
		"orders" => $orders,
		"table_title" => $__module->getTrans('cart.order.title.plural')
	])
@stop
