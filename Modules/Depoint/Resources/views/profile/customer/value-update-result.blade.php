@extends('depoint::profile.layout.frame')

@php
	// Frame Values
	$profile_breadcrumb = ["پروفایل کاربری", "افزایش اعتبار"];

	$user_credit = 5379000;
	$currency= "ریال";
	$tracking_number = 27262522;

	$message_image = Module::asset('depoint:images/error.svg');

	if ($success){
		$message_image = Module::asset('depoint:images/ok.svg');
	}


@endphp

@section('profile_content')

	@include('depoint::profile.tables.user_credit',[
			"user_credit" => $user_credit ,
			"currency" => $currency ,
			"show_action" => true ,
		])

	<section class="panel">
		<article class="p50">
			<div class="ta-c mt30 mb50">
				<img src="{{ $message_image }}" width="128">
			</div>

			<div class="payment-result">
				@if ($success)

					<div class="title">
						{{ currentModule()->trans('general.recharge_message.success.part1') }}
						<span class="fw-b color-blue-darker">
							{{ ad(number_format($user_credit))." ".$currency }}
						</span>
						{{ currentModule()->trans('general.recharge_message.success.part2') }}
					</div>
					<div class="code">{{ currentModule()->trans('general.tracking_number')." :". ad($tracking_number) }}</div>

				@else
					<div class="payment-result">
						<div class="title error">
							{{ currentModule()->trans('general.payment_message.error.title') }}
						</div>

						<p>
							{{ currentModule()->trans('general.payment_message.error.message') }}
						</p>

						<div class="action">
							<a href="#" class="button blue lg">
								{{ currentModule()->trans('general.cart_table.pay_again') }}
							</a>
						</div>
					</div>
				@endif

			</div>
		</article>
	</section>

@stop
