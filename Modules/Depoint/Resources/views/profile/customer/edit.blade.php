@extends('depoint::profile.layout.frame')

@section('profile_content')

	<section class="panel">
		<article>
			{!! widget('form-open')
					->target(route_locale('depoint.front.dashboard.edit-profile.submit'))
					->class('js')
					->id('edit-profile')
			!!}
			<div class="row">

				<div class="col-sm-3">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "name_first" ,
						"label" => trans('validation.attributes.name_first'),
						"value" => user()->name_first ?: '',
					])
				</div>

				<div class="col-sm-3">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "name_last" ,
						"label" => trans('validation.attributes.name_last'),
						"value" => user()->name_last ?: '',
					])
				</div>

                <div class="col-sm-3">
                    @include('depoint::layout.widgets.form.radio-group',[
                        "label" => trans('validation.attributes.gender') ,
                        "radios" => [
                            [
                                "name" => "gender" ,
                                "label" => currentModule()->trans('general.form.male'),
                                "class" => "inline" ,
                                "id" => "gender-male",
                                "is_checked" => $gender==1 ? true : false ,
                                "value" => 1,
                            ],
                            [
                                "name" => "gender" ,
                                "label" => currentModule()->trans('general.form.female'),
                                "class" => "inline" ,
                                "id" => "gender-female",
                                "is_checked" => $gender==2 ? true : false ,
                                "value" => 2,
                            ],
                            [
                                "name" => "gender" ,
                                "label" => currentModule()->trans('general.form.other'),
                                "class" => "inline" ,
                                "id" => "gender-other",
                                "is_checked" => $gender==3 ? true : false ,
                                "value" => 3,
                            ],
                        ] ,
                    ])
                </div>

			</div>

            <label>تاریخ تولد</label>
            <div class="row">
                <div class="col-sm-2">
                    @include('depoint::layout.widgets.form.select',[
                        "name" => "birthday_day",
                        "label" => currentModule()->trans('validation.attributes.day') ,
                        "options" => $days,
                        "value" => $user_birthday_day ?: '',
                        "id" => "birthday-day",
                    ])
                </div>
                <div class="col-sm-2">
                    @include('depoint::layout.widgets.form.select',[
                        "name" => "birthday_month" ,
                        "label" => currentModule()->trans('validation.attributes.month') ,
                        "options" => $months,
                        "value" => $user_birthday_month ?: '',
                        "id" => "birthday-month" ,
                    ])
                </div>

                <div class="col-sm-2">
                    @include('depoint::layout.widgets.form.select',[
                        "name" => "birthday_year" ,
                        "label" => currentModule()->trans('validation.attributes.year') ,
                        "options" => $years,
                        "value" => $user_birthday_year ?: '',
                        "id" => "birthday-year" ,
                    ])
                </div>
            </div>

			<div class="row">

				<div class="col-sm-3">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "email" ,
						"label" => trans('validation.attributes.email'),
						"value" => user()->email ,
						"class" => "ltr",
					])
				</div>

				<div class="col-sm-3">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "tel" ,
						"label" => trans('validation.attributes.telephone'),
						"value" => user()->tel ?: '',
					])
				</div>
				<div class="col-sm-3">
					@include('depoint::layout.widgets.form.input-text',[
						"name" => "mobile" ,
						"label" => trans('validation.attributes.mobile'),
						"value" => user()->mobile ?: '',
					])
				</div>

			</div>

			<div class="row">
				<div class="col-sm-3">
					@include('depoint::layout.widgets.form.select',[
						"name" => "province",
						"label" => trans('validation.attributes.province_id') ,
						"options" => $provinces,
						"value" => $user_province_hashid ?: '',
						"id" => "user-province",
					])
				</div>
				<div class="col-sm-3">
					@include('depoint::layout.widgets.form.select',[
						"name" => "city" ,
						"label" => trans('validation.attributes.city') ,
						"options" => $cities ?: [],
						"value" => $user_city_hashid ?: '',
						"id" => "user-city" ,
					])
				</div>



			</div>

			@include('depoint::layout.widgets.form.textarea',[
				"name" => "home_address" ,
				"row" => "3",
				"label" => trans('validation.attributes.address') ,
				"id" => "address" ,
				"value" => user()->home_address ?: '',
			])

			{!! widget('feed') !!}

			<div class="ta-l mt10">
				<button class="button blue lg">
					{{ currentModule()->trans('general.update') }}
				</button>
			</div>

			{!! widget('form-close') !!}
		</article>
	</section>

@stop

@include(currentModule()->bladePath('layout.widgets.form.scripts'))

@section('end-body')
	<script>
        $("#user-province").change(function () {
            let value = $(this).val();
            $.ajax({
                url       : "{{ route_locale('depoint.front.dashboard.related-cities', ['province' => '__PROVINCE__']) }}"
                    .replace('__PROVINCE__', value),
                beforeSend: function () {
                    $("#user-city").prop('disabled', true);
                },
                success   : function (response) {
                    $("#user-city").html(response).prop('disabled', false);
                }
            });
        })
	</script>
@append
