<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>

<!-- Styles -->
@if(isLangRtl())
	{!! Html::style(Module::asset('depoint:css/front-style.min.css')) !!}
@else
	{!! Html::style(Module::asset('depoint:css/front-style-ltr.min.css')) !!}
@endif

{!! Html::style(Module::asset('depoint:css/additives.min.css')) !!}
{!! Html::style(Module::asset('depoint:css/persian-datepicker.min.css')) !!}
<meta name="csrf-token" content="{{ csrf_token() }}">

@yield('html-header')

<title>@yield('page_title', depointTemplate()->implodePageTitle(' - '))</title>
<!-- !END Styles -->

@include(currentModule()->bladePath('layout.open-graphs'))
