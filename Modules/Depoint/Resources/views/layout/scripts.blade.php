<!-- Scripts -->

<!-- Vendor -->
{!! Html::script(Module::asset('depoint:libs/jquery.js')) !!}
{!! Html::script(Module::asset('depoint:libs/aos.js')) !!}
{!! Html::script(Module::asset('depoint:libs/jquery.matchHeight.js')) !!}
{!! Html::script(Module::asset('depoint:libs/responsiveslides.min.js')) !!}
{!! Html::script(Module::asset('depoint:libs/lightbox.min.js')) !!}
{!! Html::script(Module::asset('depoint:libs/slick.min.js')) !!}
{!! Html::script(Module::asset('depoint:libs/persian.js')) !!}
{!! Html::script(Module::asset('depoint:libs/list.min.js')) !!}
{!! Html::script(Module::asset('depoint:libs/nouislider.js')) !!}
{!! Html::script(Module::asset('depoint:libs/persian-date.min.js')) !!}
{!! Html::script(Module::asset('depoint:libs/persian-datepicker.min.js')) !!}
<!-- !END Vendor -->

<!-- App Scripts -->
{!! Html::script(Module::asset('depoint:js/tools.min.js')) !!}
{!! Html::script(Module::asset('depoint:js/app.min.js')) !!}
{!! Html::script(Module::asset('yasna:libs/timer-js/dist/timer.min.js')) !!}
{!! Html::script(currentModule()->asset('js/number-input.min.js')) !!}
{!! Html::script(currentModule()->asset('js/jquery.form.min.js')) !!}
{!! Html::script(currentModule()->asset('js/forms.min.js')) !!}
{!! Html::script(currentModule()->asset('js/cart.min.js')) !!}
{!! Html::script(currentModule()->asset('js/jobs.js')) !!}
<!-- !END App Scripts -->

<!-- !END Scripts -->
