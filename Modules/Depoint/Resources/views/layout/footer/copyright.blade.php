<div class="copyright">
	{!!
		currentModule()->trans('general.copyright', [
			'title' => depointTemplate()->siteTitle()
		])
	!!}
</div>
