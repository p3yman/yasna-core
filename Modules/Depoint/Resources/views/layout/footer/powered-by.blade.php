@if ($title = depointTemplate()->yasnaTeamTitle())
	<div class="copyright mt10">
		{{ currentModule()->trans('general.powered-by.part1') }}
		<a href="{{ depointTemplate()->yasnaTeamUrl() ?: '#' }}" target="_blank">
			{{ $title  }}
		</a>
	</div>
@endif
