@php $socials = depointTemplate()->socialLinks() @endphp
<div class="socials">
	@foreach($socials as $social)
		<a
				href="{{ $social['link'] }}"
				class="{{ $social['icon'] }}"
				data-aos="fade-down" data-aos-delay="{{ 100 + 50*($loop->index) }}"></a>
	@endforeach
</div>
