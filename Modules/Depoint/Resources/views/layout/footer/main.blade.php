<footer id="main-footer">

	<div class="container">

		<a href="{{ url('/') }}" id="logo-footer" data-aos="flip-left">
			<img src="{{ depointTemplate()->footerLogo() }}">
		</a>

		@include(currentModule()->bladePath('layout.footer.socials'))

		@include(currentModule()->bladePath('layout.footer.copyright'))

		@include(currentModule()->bladePath('layout.footer.powered-by'))

	</div>

</footer>
