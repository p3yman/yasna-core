<a href="{{ $link }}" class="box news-item {{ $class or "" }}">
	<div class="cover">
		<img src="{{ $image }}" class="fluid">
	</div>
	<div class="details">
		<h2 class="title">
			{{ $title }}
		</h2>
		<p>
			{{ $abstract }}
		</p>
	</div>
</a>
