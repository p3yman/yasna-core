<div class="container">
    <div class="big-title" data-aos="fade-down">
        @if( is_array($title) )
            <span>{{ $title['first'] or '' }}</span>
            <span>{{ $title['second'] or '' }}</span>
            <i class="lines"></i>
        @else
            {{ $title }}
        @endif
    </div>
</div>
