<div class="modal-wrapper">
	<section class="modal panel {{ $size or 'md' }}" id="{{ $id or "" }}">
		{{ $slot }}
	</section>
</div>