<div class="product-item">
	<div class="img">
		<img src="{{ $image }}" style="max-width: 100%;">
	</div>
	<div class="content">
		<div class="title">
			{{ $title }}
		</div>
		<p class="excerpt">
			{{ $abstract }}
		</p>

		<div class="colors">
			@foreach($product->wares as $ware)
				@php $color = getWareColor($ware) @endphp
				@if(!empty($color))
					<div class="item simptip-position-top simptip-smooth simptip-fade"
						 data-tooltip="{{ $color['title'] }}" data-aos="fade-down"
						 data-aos-delay="{{ 250 + 50*($loop->index) }}">
						<span style="background: {{ $color['code'] }};"></span>
					</div>
				@endif
			@endforeach
		</div>


		@if($product->wares->count())
			@php($min_price_ware=depointTools()->getMinimumPriceOfProduct($product,true))
			<div class="price">
				@if($min_price_ware->isOnSale())
					<del>
						{{ depointTools()->getWarePrice($min_price_ware) }}
					</del>
				<br>
					<ins>
						{{ depointTools()->getWarePrice($min_price_ware,true) }}
					</ins>
				@else
					<ins>
						{{ depointTools()->getWarePrice($min_price_ware) }}
					</ins>
				@endif
			</div>
		@endif


		<a href="{{ $link }}" class="button blue">
			{{ trans('depoint::general.show_detail') }}
		</a>
	</div>
</div>