<div class="product-item shop-item">
	<div class="img">
		<img src="{{ $image }}">
	</div>
	<div class="content">
		<div class="title">
			{{ $title }}
		</div>
		<p class="excerpt">
			{{ $abstract }}
		</p>

		@php $show_colors = ($show_colors ?? true ) @endphp
		@if ($show_colors)
			<div class="colors">
				@foreach($product->depointWares()->get() as $ware)
					@php $color = getWareColor($ware) @endphp
					@if(!empty($color))
						<div class="item simptip-position-top simptip-smooth simptip-fade"
							 data-tooltip="{{ $color['title'] }}" data-aos="fade-down"
							 data-aos-delay="{{ 250 + 50*($loop->index) }}">
							<span style="background: {{ $color['code'] }};"></span>
						</div>
					@endif
				@endforeach
			</div>
		@endif

		@php $show_price = ($show_price ?? true) @endphp
		@if($show_price and $product->depointWares()->count())
			@php($min_price_ware=depointTools()->getMinimumPriceOfProduct($product))
			<div class="price">
				@if($min_price_ware->isOnSale())
					<del>
						{{ depointTools()->getWarePrice($min_price_ware) }}
					</del>
					<ins>
						{{ depointTools()->getWarePrice($min_price_ware,true) }}
					</ins>
				@else
					<ins>
						{{ depointTools()->getWarePrice($min_price_ware) }}
					</ins>
				@endif
			</div>
		@endif
		<a href="{{ $link }}" class="button red rounded">
			{{ trans('depoint::general.show_detail') }}
		</a>
	</div>
</div>
