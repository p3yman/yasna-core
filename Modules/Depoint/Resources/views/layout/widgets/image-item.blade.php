<a href="{{ $image_original }}" class="box-item" data-lightbox="image-gallery" data-title="{{ $title }}">
	<img src="{{ $image_thumb }}" class="fluid">
	<span class="hover"></span>
	<img src="{{ $hover_icon }}" class="icon">
</a>
