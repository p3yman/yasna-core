<div class="part-title {{ $class or "" }}" data-aos="fade-down">
	@isset($image)
		<div class="icon">
			<img src="{{ $image }}" width="128">
		</div>
	@endisset

	@isset($subtitle)
		<div class="subtitle">
			@if(isset($subtitle_link))
				<a href="{{ $subtitle_link }}">
					{{ $subtitle }}
				</a>
			@else
				{{ $subtitle }}
			@endif

		</div>

	@endisset

	@isset($hide_title)
			<div class="hide-title">
				{{ $hide_title }}
			</div>
	@endisset

	<div class="title {{ $text_class or "" }}">
		{{ $title }}
	</div>
</div>
