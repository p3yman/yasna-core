<div class="person-item eq-item">
	<img src="{{ $image }}">
	@if(isset($description))
		<h2 class="name">
			{{ $title }}
		</h2>
		<span class="position">
			{{ $description }}
		</span>
	@else
		<h3 class="name">
			{{ $title }}
		</h3>
	@endif
</div>
