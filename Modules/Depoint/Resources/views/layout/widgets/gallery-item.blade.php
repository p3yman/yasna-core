<a href="{{ $link }}" class="box-item with-title">
	<img src="{{ $image }}" class="fluid">
	<span class="hover"></span>
	<img src="{{ $hover_icon }}" class="icon">
	@isset($title)
	<span class="title">
		{{ $title }}
	</span>
	@endisset
</a>
