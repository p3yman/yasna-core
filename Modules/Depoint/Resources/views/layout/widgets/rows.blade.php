{{--
	`$col_cont` is the numbers of your desired colums
--}}
@if($loop->iteration % $col_count === 1)
	<div class="row" {{ $extra or "" }}>
		@endif

		{{ $slot }}

		@if(($loop->iteration % $col_count === 0) or ($loop->iteration === $loop->count))
	</div>
@endif