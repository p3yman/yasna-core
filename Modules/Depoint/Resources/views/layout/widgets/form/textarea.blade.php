@php $required = ($required ?? false) @endphp
@if ($required)
	@php $required_sign = ($required_sign ?? true) @endphp
@endif

<div class="field {{ $field_class or "" }} {{ ($required_sign ?? false) ? "required-label" : ""  }}">
	@isset($label)
		<label for="{{ $id or "" }}">
			{{ $label }}
		</label>
	@endisset

	<textarea
			id="{{ $id or "" }}"
			class="{{ $class or "" }}"
			name="{{ $name or "" }}"
			placeholder="{{ $placeholder or "" }}"
			rows="{{ $rows or "" }}"
			@if($required) required @endif
			cols="{{ $cols or "" }}">{{ $value or "" }}</textarea>
</div>
