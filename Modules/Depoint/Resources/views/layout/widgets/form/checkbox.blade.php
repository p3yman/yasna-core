<div class="field">
	<div class="checkbox">
		<input
				id="{{ $id or "" }}"
				type="checkbox"
				class="{{ $class or "" }}"
				name="{{ $name or "" }}"
				{{ (isset($is_checked) and $is_checked)? "checked" : "" }}
				{{ $extra or "" }}>
		<label for="{{ $id or "" }}">
			{!! $label or "" !!}
		</label>
	</div>
</div>
