<div class="radio {{ $class or "" }}">
	<input
			id="{{ $id or "" }}"
			type="radio"
			name="{{ $name or "" }}"
			value="{{ $value or "" }}"
			{{ (isset($is_checked) and $is_checked)? "checked" : "" }}>
	<label for="{{ $id or "" }}">
		@if ($label ?? false)
			{{ $label }}
		@else
			&nbsp;
		@endif
	</label>
</div>
