@php $required = ($required ?? false) @endphp
@if ($required)
	@php $required_sign = ($required_sign ?? true) @endphp
@endif

<div class="field {{ $field_class or "" }} {{ ($required_sign ?? false) ? "required-label" : ""  }}">
	@isset($label)
		<label for="{{ $id or "" }}">{{ $label }}</label>
	@endisset
	<input
			id="{{ $id or "" }}"
			class="{{ $class or "" }}"
			name="{{ $name or "" }}"
			placeholder="{{ $placeholder or "" }}"
			value="{{ $value or "" }}"
			type="{{ $type or "text" }}"
			{{ $attr or "" }}
			@if($required) required @endif>
</div>
