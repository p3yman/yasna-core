@php
	$required = ($required ?? false);
	$disabled = ($disabled ?? false);
@endphp
@if ($required)
	@php $required_sign = ($required_sign ?? true) @endphp
@endif

@php $value = ($value ?? '') @endphp
<div class="field {{ $field_class or "" }} {{ ($required_sign ?? false) ? "required-label" : ""  }}">
	@isset($label)
		<label for="{{ $id or "" }}">{{ $label }}</label>
	@endisset
	<div class="select">
		<select id="{{ $id or "" }}" name="{{ $name or "" }}" class="{{ $class or "" }}"
				@if($required) required @endif @if($disabled) disabled @endif>
			@foreach($options as $option)
				<option value="{{ $option['value'] }}"
						@if($option['value'] == $value) selected @endif
						@if(array_has($option, 'parent')) data-parent="{{ $option['parent'] }}" @endif
				>
					{{ $option['title'] }}
				</option>
			@endforeach
		</select>
	</div>
</div>
