<div class="field">
	@isset($label)
		<label>
			{{ $label }}
		</label>
	@endisset

	@foreach($radios as $radio)
		@include(currentModule()->bladePath('layout.widgets.form.radio'),[
			"name" => $radio['name'] ,
			"class" => isset($radio['class'])? $radio['class']: "" ,
			"id" => isset($radio['id'])? $radio['id']: "" ,
			"value" => isset($radio['value'])? $radio['value']: "" ,
			"is_checked" => isset($radio['is_checked'])? $radio['is_checked']: false ,
			"label" => $radio['label'] ,
		])
	@endforeach	
</div>
