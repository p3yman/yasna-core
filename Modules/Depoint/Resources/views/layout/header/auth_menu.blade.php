@if (auth()->guest())
	<li>
		<a href="{{ route('login') }}">
			{{ currentModule()->trans('general.auth.login') }}
		</a>
	</li>
@else
	<li class="has-child">
		<a href="#">{{ user()->full_name }}</a>
		<ul>
			<li>
				<a href="{{ route_locale('depoint.front.dashboard') }}">
					{{ currentModule()->trans('profile.user-profile') }}
				</a>
			</li>

			<li>
				<a href="{{ route('logout') }}">
					{{ currentModule()->trans('general.auth.logout') }}
				</a>
			</li>
		</ul>
	</li>
@endif
