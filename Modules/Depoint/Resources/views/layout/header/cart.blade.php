<!-- START: CART -->
@php $orders_count = depoint()->cart()->findActiveCart()->orders()->count() @endphp
<a href="{{ route_locale('depoint.cart') }}" id="cart-link" class="simptip-position-bottom simptip-smooth simptip-fade"
   {{--data-tooltip=""--}} data-reload-url="{{ route_locale('depoint.cart.reload-view') }}">
	@if($orders_count)
		<span class="badge blue count">{{ ad($orders_count) }}</span>
	@endif
	<span class="icon icon-shopping-cart"></span>
</a>
<!-- END: CART -->
