<div class="menu-overlay"></div>

<header id="main-header">

	<div class="container">

        <div id="main-header-content">

            <div class="head-col right ta-r xs-hide">
                @php $socials = depointTemplate()->socialLinks() @endphp
                <div class="socials">
                    @foreach($socials as $social)
                        <a
                            href="{{ $social['link'] }}"
                            class="{{ $social['icon'] }}"
                            data-aos="fade-down" data-aos-delay="{{ 100 + 50*($loop->index) }}"></a>
                    @endforeach
                </div>

            </div>

            <div class="head-col center ta-c logo-section">
                <a href="{{ url('/') }}" id="logo" data-aos="fade-down">
                    <img src="{{ depointTemplate()->siteLogo() }}">
                </a>
            </div>

            <div class="head-col left ta-l left-section">
                <button class="hamburger hamburger--collapse" type="button">
                    <span class="hamburger-box"><span class="hamburger-inner"></span></span>
                </button>

                @include(currentModule()->bladePath('layout.header.langs'))
                @include(currentModule()->bladePath('layout.header.cart'))
            </div>

        </div>

	</div>

</header>
<nav id="nav">
    <ul class="menu" id="menu">
        @include(currentModule()->bladePath('layout.header.menu'))
    </ul>
</nav>
