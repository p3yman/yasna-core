<!-- START: LANGUAGES -->
@php
	$all_locales = depointTemplate()->siteLocales();
	$current_locale = getLocale();
	$other_locales = array_filter($all_locales, function ($locale) use ($current_locale) {
		return ($current_locale != $locale);
	});
	$other_locales = value($other_locales);
@endphp

<div id="langs">
	<div class="current">
		<a href="{{ request()->fullUrl() }}">
			<img src="{{ depoint()->getLocaleFlagUrl($current_locale) }}">
		</a>
	</div>

	<div class="others">
		@foreach($other_locales as $locale)
			<a href="{{ depoint()->getLocaleLink($locale) }}">
				<img src="{{ depoint()->getLocaleFlagUrl($locale) }}">
				<span>{{ depoint()->getLocaleTitle($locale) }}</span>
			</a>
		@endforeach
	</div>
</div>

<div id="langs-res" class="xs-show">
	@foreach($all_locales as $locale)
		<a href="{{ depoint()->getLocaleLink($locale) }}" @if($locale == $current_locale) class="current" @endif>
			<img src="{{ depoint()->getLocaleFlagUrl($locale) }}">
		</a>
	@endforeach
</div>

<!-- END: LANGUAGES -->
