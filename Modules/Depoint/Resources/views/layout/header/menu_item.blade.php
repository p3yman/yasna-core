@if (!$menu_item->isEnabled(getLocale()))
	@php return @endphp
@endif

@php
	$url = $menu_item->linkIn(getLocale());
	$link = $url ? url($url) : '#';
@endphp

<li class="{{ (url()->current() === $link) ? "active" : "" }}">
	<a href="{{ $link }}">
		{{ $menu_item->titleIn(getLocale()) }}
	</a>
</li>
