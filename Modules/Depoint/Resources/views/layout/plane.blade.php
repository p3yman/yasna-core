<!DOCTYPE html>
<html lang="{{ getLocale() }}">
<head>
	@include(currentModule()->bladePath('layout.head'))
</head>
<body>
@include(currentModule()->bladePath('layout.body'))
</body>
</html>
