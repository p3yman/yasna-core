@foreach(depointTemplate()->openGraphs() as $tag_title =>  $tag_content)
	@php $value = depoint()->getValue($tag_content) @endphp
	@if (is_array($value))
	    @php $value = array_last($value) @endphp
	@endif
	<meta property="og:{{ $tag_title }}" content="{{ $value }}"/>
@endforeach
