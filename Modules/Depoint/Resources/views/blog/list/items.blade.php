@if ($models->count())

	@foreach($models as $model)
		@component('depoint::layout.widgets.rows',[
							"col_count" => 2 ,
							"loop" => $loop ,
						])
			<div class="col-sm-6">
				@include('depoint::blog.list.blog-item',[
					"title" => $model->title,
					"link" => ($model->direct_url ?? '#'),
					"abstract" => $model->abstract,
					"image" => fileManager()
						->file($model->featured_image)
						->version('thumb')
						->getUrl() ,
				])
			</div>
		@endcomponent
	@endforeach

	{!! $models->render() !!}

@else

	@include(currentModule()->bladePath('layout.widgets.alert'), [
		'class' => 'alert-default',
		'text' => currentModule()->trans('general.no-item-to-show')
	])

@endif
