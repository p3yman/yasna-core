@php
	$related_posts = [
		"title" => trans('depoint::general.related_posts') ,
		"posts" => $model->similars(4),
	];

	$posttype_title = $posttype->titleIn(getLocale());
	$post_title = $model->title;

	depointTemplate()->appendToPageTitle($posttype_title);
	depointTemplate()->appendToPageTitle($post_title);

	$post_image = fileManager()
		->file($model->featured_image)
		->getUrl();

	if ($post_image) {
		depointTemplate()->mergeWithOpenGraphs([
			'image' => $post_image
		]);
	}
@endphp

@component(currentModule()->bladePath('post.single.main'),[
	"related_posts" => $related_posts ,
	"title" => $post_title
])

	<!-- Post Title -->
	@slot('header')
		@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"subtitle" => $posttype_title ,
				"subtitle_link" => "#" ,
				"title" => $post_title ,
			])
	@endslot
	<!-- !END Post Title -->


	<!-- Post Content -->
	{!! $model->text !!}

	@foreach ($model->tags as $tag)
		<a href="{{ route_locale('depoint.front.tag', ['tag' => urlencode($tag->slug)]) }}" class="label blue">{{ $tag->slug }}</a>
	@endforeach

	<!-- !END Post Content -->

@endcomponent

