@extends(currentModule()->bladePath('auth.layout.frame'))

@php
	$auth_title = currentModule()->trans('general.login_to_site');
	$links = [
		[
			"link" => route('password.request'),
			"title" => currentModule()->trans('general.auth.forgot_your_password') ,
		],
		[
			"link" => route('register') ,
			"title" => currentModule()->trans('general.auth.register') ,
		],
	];

	depointTemplate()->appendToPageTitle(currentModule()->trans('general.login_to_site'))
@endphp

@section('auth-content')
	<div class="auth-form box">

		{!!
			widget('form-open')
				->target(route('login'))
		!!}

		@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
			"label" => trans('validation.attributes.email') ,
			"type" => "email" ,
			"id" => "email" ,
			"name" => "email" ,
			"value" => old('email'),
		])

		@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
			"label" => trans('validation.attributes.password') ,
			"type" => "password" ,
			"id" => "password" ,
			"name" => "password" ,
		])

		@include(currentModule()->bladePath('layout.widgets.form.checkbox'),[
			"label" => currentModule()->trans('general.auth.remember_me') ,
			"id" => "remember_me" ,
			"name" => "remember" ,
			"is_checked" => old('remember'),
		])

		<div class="ta-l">
			<button class="button blue block">
				{{ currentModule()->trans('general.auth.login') }}
			</button>
		</div>

		@if (!env('APP_DEBUG'))
			{!! app('captcha')->render(getLocale()) !!}
		@endif

		@if($errors->all())
			<div class="alert danger" style="margin-top: 10px;">
				@foreach ($errors->all() as $error)
					{{ $error }}<br>
				@endforeach
			</div>
		@endif

		{!! widget('form-close') !!}

	</div>

	@include(currentModule()->bladePath('auth.layout.links'),[
		"links" => $links ,
	])
@stop
