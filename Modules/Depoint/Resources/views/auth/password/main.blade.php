@extends(currentModule()->bladePath('auth.layout.frame'))

@php
	$links = [
		[
			"link" => route('login') ,
			"title" => currentModule()->trans('general.auth.login') ,
		],
		[
			"link" => route('register') ,
			"title" => currentModule()->trans('general.auth.register') ,
		],
	];

	depointTemplate()->appendToPageTitle($auth_title);
@endphp

@section('auth-content')
	<div class="auth-form box">
		@if($type === "forgot")
			@include(currentModule()->bladePath('auth.password.forgot'))
		@elseif($type === "token")
			@include(currentModule()->bladePath('auth.password.token'))
		@elseif($type === "reset")
			@include(currentModule()->bladePath('auth.password.reset'))
		@endif
	</div>

	@include('depoint::auth.layout.links',[
		"links" => $links ,
	])
@stop
