<p>
	{{ currentModule()->trans('general.auth.forgot_password_message') }}
</p>

{!!
	widget('form-open')
		->target(route('password.request'))
!!}

@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
	"label" => trans('validation.attributes.email') ,
	"type" => "email" ,
	"id" => "email" ,
	"name" => "email" ,
])


<div class="ta-l">
	<button class="button blue block">
		{{ currentModule()->trans('general.auth.send_reset_token') }}
	</button>
</div>

@if (!env('APP_DEBUG'))
	{!! app('captcha')->render(getLocale()) !!}
@endif

@if($errors->all())
	@include(currentModule()->bladePath('layout.widgets.alert'),[
		"class" => "danger mt20" ,
		"text" => implode('<br>', array_flatten($errors->all())) ,
	])
@endif

{!! widget('form-close') !!}
