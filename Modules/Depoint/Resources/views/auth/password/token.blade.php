{!!
	widget('form-open')
		->target(route('password.token'))
!!}

@if($email_value)
	@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
		"type" => "hidden" ,
		"id" => "email" ,
		"name" => "email" ,
		"value" => $email_value,
	])
@else
	@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
		"label" => trans('validation.attributes.email') ,
		"type" => "email",
		"id" => "email" ,
		"name" => "email" ,
		"value" => old("email"),
	])
@endif

@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
	"label" => currentModule()->trans('general.auth.reset_password_token') ,
	"type" => "text" ,
	"id" => "token" ,
	"name" => "token" ,
	"value" => old("token"),
])


<div class="ta-l">
	<button class="button blue block">
		{{ currentModule()->trans('general.auth.check_token') }}
	</button>
</div>

@if (!env('APP_DEBUG'))
	{!! app('captcha')->render(getLocale()) !!}
@endif

@if($errors->all())
	@include(currentModule()->bladePath('layout.widgets.alert'),[
		"class" => "danger mt20" ,
		"text" => implode('<br>', array_flatten($errors->all())) ,
	])
@endif

{!! widget('form-close') !!}
