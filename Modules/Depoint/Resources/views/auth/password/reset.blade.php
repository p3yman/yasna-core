<p>
	{{ currentModule()->trans('general.auth.reset_password_message') }}
</p>

{!!
	widget('form-open')
		->target(route('password.new'))
!!}

@include('depoint::layout.widgets.form.input-text',[
	"label" => trans('validation.attributes.new_password') ,
	"type" => "password" ,
	"id" => "new_password" ,
	"name" => "new_password" ,
])

@include('depoint::layout.widgets.form.input-text',[
	"label" => trans('validation.attributes.new_password2') ,
	"type" => "password" ,
	"id" => "new_password2" ,
	"name" => "new_password2" ,
])


<div class="ta-l">
	<button class="button blue block">
		{{ currentModule()->trans('general.auth.change_password') }}
	</button>
</div>

@if (!env('APP_DEBUG'))
	{!! app('captcha')->render(getLocale()) !!}
@endif

@if($errors->all())
	@include(currentModule()->bladePath('layout.widgets.alert'),[
		"class" => "danger mt20" ,
		"text" => implode('<br>', array_flatten($errors->all())) ,
	])
@endif

{!! widget('form-close') !!}
