<div class="auth-links">
	@foreach($links as $link)
		<a href="{{ $link['link'] }}">{{ $link['title'] }}</a>
		@if($loop->iteration !== $loop->count)
		<span>|</span>
		@endif
	@endforeach
</div>