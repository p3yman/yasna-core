@extends('depoint::layout.plane')

@section('content')
	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"title" => $auth_title ,
			])

			<div class="row">

				<div class="col-sm-5 col-md-4 col-center">

					@yield('auth-content')

				</div>

			</div>

		</div>

	</div>
@stop