@php
	$genders = depoint()->genders();
	$radios_array = [];
	$value = ($value ?? null);
@endphp

@foreach($genders as $gender)
	@php
		$radios_array[] = [
			"name" => "gender" ,
			"label" => currentModule()->trans("people.gender.$gender") ,
			"id" => "gender-$gender" ,
			"class" => "inline" ,
			"value" => $gender,
			"is_checked" => ($value == $gender),
		];
	@endphp
@endforeach

@include(currentModule()->bladePath('layout.widgets.form.radio-group'),[
	"label" => trans('validation.attributes.gender') ,
	"radios" => $radios_array,
])
