@extends(currentModule()->bladePath('layout.plane'))

@php
	$auth_title = currentModule()->trans('general.auth.register');
	$links = [
		[
			"link" => route('login') ,
			"title" => currentModule()->trans('general.auth.login') ,
		],
	];

	depointTemplate()->appendToPageTitle($auth_title);
@endphp

@section('content')
	<div class="container">

		<div class="page">

			@include('depoint::layout.widgets.part-title',[
				"title" => $auth_title ,
			])

			<div class="row">

				<div class="col-sm-8 col-center">

					<div class="auth-form box">

						{!!
							widget('form-open')
								->target(route('register'))
						!!}

						<div class="row">

							<div class="col-sm-6">
								@include('depoint::layout.widgets.form.input-text',[
									"label" => trans('validation.attributes.name_first') ,
									"id" => "name_first" ,
									"name" => "name_first" ,
									"value" => old("name_first"),
								])
							</div>

							<div class="col-sm-6">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"label" => trans('validation.attributes.name_last') ,
									"id" => "name_last" ,
									"name" => "name_last" ,
									"value" => old("name_last"),
								])
							</div>

						</div>

						<div class="row">

							<div class="col-sm-6">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"label" => trans('validation.attributes.email') ,
									"type" => "email" ,
									"id" => "email" ,
									"name" => "email" ,
									"value" => old("email"),
								])
							</div>

							<div class="col-sm-6">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"label" => trans('validation.attributes.mobile') ,
									"type" => "text" ,
									"id" => "mobile" ,
									"name" => "mobile" ,
									"value" => old("mobile"),
								])
							</div>

						</div>


						<div class="row">

							<div class="col-sm-6">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"label" => trans('validation.attributes.password') ,
									"type" => "password" ,
									"id" => "password" ,
									"name" => "password" ,
								])
							</div>

							<div class="col-sm-6">
								@include(currentModule()->bladePath('layout.widgets.form.input-text'),[
									"label" => trans('validation.attributes.password2') ,
									"type" => "password" ,
									"id" => "password2" ,
									"name" => "password2" ,
								])
							</div>

						</div>

						@include(currentModule()->bladePath('layout.widgets.form.checkbox'),[
							"label" => currentModule()->trans('general.auth.accept_policy.part1')
								. "<a href='"
								. depoint()->termsAndConditionsUrl()
								."'>"
								. currentModule()->trans('general.auth.accept_policy.part2')
								. "</a>"
								. currentModule()->trans('general.auth.accept_policy.part3') ,
							"id" => "policy" ,
							"name" => "terms_and_conditions" ,
							"is_checked" => old("terms_and_conditions"),
						])

						<div class="ta-l">
							<button class="button blue block">
								{{ currentModule()->trans('general.auth.register') }}
							</button>
						</div>

						@if($errors->all())
							<div class="alert danger" style="margin-top: 10px;">
								@foreach ($errors->all() as $error)
									{{ $error }}<br>
								@endforeach
							</div>
						@endif

						@if (!env('APP_DEBUG'))
							{!! app('captcha')->render(getLocale()) !!}
						@endif

						{!! widget('form-close') !!}

					</div>

					@include('depoint::auth.layout.links',[
						"links" => $links ,
					])

				</div>

			</div>

		</div>

	</div>
@stop
