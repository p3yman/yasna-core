@extends(currentModule()->bladePath('layout.plane'))

@php $list_title = (posttype('stokes')->titleIn(getLocale()) ?: currentModule()->trans('general.products')) @endphp

@php depointTemplate()->appendToPageTitle($list_title) @endphp

@section('content')

	<div class="container">

		<div class="page">
			@include(currentModule()->bladePath('layout.widgets.part-title'),[
				"title" => currentModule()->trans('general.products') ,
			])
			@if ($products->count())
				<div class="row">

					<div class="col-sm-10 col-center">

						@foreach($products as $product)
							@include(currentModule()->bladePath('layout.widgets.stoke-item'),[
								"image" => fileManager()->file($product->featured_image)->posttype($product->posttype)->config('featured_image')->getUrl() ,
								"title" => $product->title ,
								"abstract" => $product->abstract ,
								"link"	 => $product->direct_url ,
							])
						@endforeach

						{!! $products->render() !!}

					</div>

				</div>
			@else

				@include(currentModule()->bladePath('layout.widgets.alert'), [
					'class' => 'alert-default',
					'text' => currentModule()->trans('general.no-item-to-show')
				])

			@endif
		</div>

	</div>

@stop
