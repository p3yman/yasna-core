<div class="product-single-gallery">

	<ul class="gallery-items">
		@foreach($slides as $slide)
			<li>
				<img src="{{ fileManager()->file($slide['src'])->posttype($model->posttype)->config('attachments')->version("416x416")->getUrl() }}">
			</li>
		@endforeach
	</ul>

	<div class="pager-wrapper">
		<ul class="gallery-pager">
			@foreach($slides as $slide)
				<li>
					<a href="#">
						<img src="{{ fileManager()->file($slide['src'])->posttype($model->posttype)->config('attachments')->version("thumb")->getUrl() }}">
					</a>
				</li>
			@endforeach
		</ul>
	</div>

</div>
