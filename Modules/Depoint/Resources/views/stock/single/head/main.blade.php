<div class="product-single-part">

	<div class="row">

		<div class="col-sm-5">

			@include(currentModule()->bladePath('product.single.head.slider'),[
				"slides" => $model->files ,
			])

		</div>

		<div class="col-sm-7">

			<div class="product-intro">
				@include(currentModule()->bladePath('layout.widgets.part-title'),[
					"title" => $post_title ,
					"subtitle" =>$model->title2,
				])


				@include(currentModule()->bladePath('product.single.head.features'))

				{!!
					widget('form-open')
						->target(route_locale('depoint.cart.add'))
						->class('js')
				!!}

				@include(currentModule()->bladePath('stock.single.head.color'))

				@include(currentModule()->bladePath('product.single.head.product_count'))


				@include(currentModule()->bladePath('stock.single.head.price'))

				@include(currentModule()->bladePath('product.single.head.add_cart'))

				<div class="actions mt20">
					{!! widget('feed') !!}
				</div>

				{!! widget('form-close') !!}

			</div>

		</div>

	</div>

</div>
