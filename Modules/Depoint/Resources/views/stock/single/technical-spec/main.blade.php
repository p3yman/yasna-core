<div class="product-single-part">
	@include('depoint::layout.widgets.part-title',[
			"title" => trans('depoint::general.technical_spec') ,
		])

	<div class="row">
		<div class="col-sm-10 col-center">
			<div class="specs">

				@foreach($specs as $spec)
					@include('depoint::product.layout.table',[
						"title" => $spec['group_title'] ,
						"rows" => $spec['rows'] ,
					])
				@endforeach

			</div>
		</div>
	</div>

</div>
