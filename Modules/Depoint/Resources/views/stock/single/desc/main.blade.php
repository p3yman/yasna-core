@if(!empty($desc))
<div class="product-single-part">

	@include(currentModule()->bladePath('layout.widgets.part-title'),[
		"title" => currentModule()->trans('general.product_intro') ,
	])

	<div class="row">
		<div class="col-sm-10 col-center">
			{!! $desc !!}
		</div>
	</div>

</div>
@endif