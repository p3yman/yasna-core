@extends(currentModule()->bladePath('layout.plane'))

@php
	depointTemplate()->appendToPageTitle($posttype_title);
	depointTemplate()->appendToPageTitle($post_title);
@endphp

@section('content')

	<div class="container">

		<div class="product-single">

			@include(currentModule()->bladePath('stock.single.head.main'))

			@include(currentModule()->bladePath('product.single.desc.main'),[
				"desc" => $model->text ,
			])

			@include(currentModule()->bladePath('product.single.technical-spec.main'),[
				"specs_pivot_parent" => $specs_pivot_parent ,
				])

			@include(currentModule()->bladePath('product.single.similar.main'),[
					"products" => $model->similars(2) ,
			])

		</div>

	</div>

@stop


@section('end-body')
	<script>
        $(document).ready(function () {
            $(".color_checkbox").on('change', 'input[type=radio]', function () {
                var id = $(this).val();
                $(".price").hide();
                $("#price_" + id).show();
            })
        })
	</script>
@append