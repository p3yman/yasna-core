$.fn.scrollToView = function (extra, duration) {
    var item = $(this);

    if (!$.isNumeric(duration)) {
        duration = 1000;
    }

    if (!$.isNumeric(extra)) {
        extra = 0;
    }

    $('html, body').animate({
        scrollTop: item.offset().top + extra
    }, duration);
};

String.prototype.splitNotEmpty = function (delimiter) {
    return this.match(new RegExp("[^" + delimiter + "]+", "gi"));
};

String.prototype.limitedSplit = function (delimiter, limit, notEmpty) {
    if ((typeof notEmpty == typeof undefined) || !notEmpty) {
        var arr = this.split(delimiter);
    } else {
        var arr = this.splitNotEmpty(delimiter);
    }

    if ((typeof limit == typeof undefined) || (arr.length <= limit)) {
        return arr;
    }

    var result = arr.splice(0, (limit - 1));

    result.push(arr.join(delimiter));

    return result;
};

function getHashUrl(url) {
    if (url) {
        var hashIndex  = url.indexOf('#');
        var hashString = "";
        if (hashIndex != -1) {
            hashString = url.substring(hashIndex + 1);
        }
        return hashString;
    }

    return decodeURIComponent(window.location.hash.substr(1));
}

function setHashUrl(hashString, url) {
    if (url) {
        var hashIndex = url.indexOf('#');
        if (hashIndex == -1) {
            url = url + '#' + hashString;
        } else {
            url = url.substring(0, hashIndex + 1) + hashString;
        }
        return url;
    }

    window.location.hash = hashString;
}

function getPageUrl() {
    return window.location.href.replace(getHashUrl(), '').replace('#', '');
}


function isDefined(variable) {
    return (typeof variable != typeof undefined);
}

function prefixToIndex(delimiter, haystack) {
    switch (typeof haystack) {
        case 'object':
            var output = {};
            $.each(haystack, function (index, field) {
                var parts = field.limitedSplit(delimiter, 2);
                if (parts.length == 2) {
                    var key   = parts[0];
                    var value = parts[1];
                    value     = prefixToIndex(delimiter, value);
                    if (isDefined(output[key])) {
                        if (typeof output[key] != 'object') {
                            output[key] = [output[key]];
                        }

                        if (typeof value != 'object') {
                            value = [value];
                        }

                        output[key] = $.smartMerge(output[key], value);
                    } else {
                        output[key] = value;
                    }
                }
            });
            return output;
            break;

        case 'string':
            var parts = haystack.limitedSplit(delimiter, 2);
            if (parts.length == 2) {
                var key   = parts[0];
                var value = parts[1];
                value     = prefixToIndex(delimiter, value);

                haystack      = {};
                haystack[key] = value;

                return haystack;
            }
            break;
    }
    return haystack;
}

function ksort(obj) {
    if (obj) {
        var sorted = {};
        var keys   = [];

        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
    }

    keys.sort();

    for (i = 0; i < keys.length; i++) {
        sorted[keys[i]] = obj[keys[i]];
    }

    return sorted;
}

$.smartMerge = function () {
    var allArray = true;
    var checking = arguments;
    $.each(checking, function (i, argument) {
        if (!$.isArray(argument)) {
            allArray = false;
            return false;
        }
    });

    if (allArray) {
        return $.merge.apply(this, checking);
    }

    $.each(checking, function (i, argument) {
        if ($.isArray(argument)) {
            checking[i] = $.extend({}, argument);
        }
    });

    return $.extend.apply(this, checking);
};
