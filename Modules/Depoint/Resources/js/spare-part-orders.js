$(document).ready(function () {
    $(document).on({
        click: function () {
            let that = $(this);
            let tr = that.closest('tr');

            tr.find('.number-input').prop('disabled', true);
            tr.addClass('deleted-content');
        }
    }, '.spare-part-remove');

    $(document).on({
        click: function () {
            let that = $(this);
            let tr = that.closest('tr');

            tr.find('.number-input').prop('disabled', false);
            tr.removeClass('deleted-content');
        }
    }, '.spare-part-recycle');
});
