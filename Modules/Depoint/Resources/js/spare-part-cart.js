function toStep2() {
    let form        = $('.checkout-form');
    let buttons_box = form.find('.buttons-container');

    form.find('.number-input').prop('readonly', true);

    buttons_box.find('button.step1').hide();
    buttons_box.find('button.step2').show();

    $('.verify-note').slideDown();
}


function toStep1() {
    let form        = $('.checkout-form');
    let buttons_box = form.find('.buttons-container');

    form.find('.number-input').prop('readonly', false);

    buttons_box.find('button.step2').hide();
    buttons_box.find('button.step1').show();

    $('.verify-note').slideUp();
    $('#checkout-feed').slideUp();
}

$(document).ready(function () {

    $(document).on({
        submit: function () {
            $('.verify-note').slideUp();
        }
    }, '.checkout-form');

});
