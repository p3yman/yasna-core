let hidden_input_selector = '.input-container input[type=hidden]';

function addFileToInput(input, file_hashid) {
    let added_files;

    try {
        added_files = $.parseJSON(input.val());
    } catch (e) {
        added_files = [];
    }

    added_files.push(file_hashid);
    input.val(JSON.stringify(added_files));
}


function fileView(box, file_hashid) {
    let files_box = box.find('.files .media-list');

    $.ajax({
        url    : file_view_url.replace('__FILE__', file_hashid),
        success: function (rs) {
            files_box.append(rs);
        }
    })
}


function addFile(file, box) {
    let input       = box.find(hidden_input_selector);
    let response    = $.parseJSON(file.xhr.response);
    let file_hashid = response.file;

    addFileToInput(input, file_hashid);
    fileView(box, file_hashid);
}

function removeFileFromBox(file, box) {
    let var_name     = box.find('.dropzone').data('varName');
    let dropzone_obj = eval(var_name);

    dropzone_obj.removeFile(file);
}

function flushBox(box) {
    let file_items = box.find('.files').find('.media');

    file_items.each(function (key, file_item) {
        $(file_item).find('.file-item-remove').trigger('click');
    })
}


function fileUploaded(file, box, flush) {
    flush = (typeof flush !== 'undefined') ? flush : false;

    if (file.status == 'success') {
        if (flush) {
            flushBox(box);
        }
        
        addFile(file, box);
        removeFileFromBox(file, box);
    }

}

function mainFileAdded(file) {
    fileUploaded(file, $('#main-file-uploader'));
}

function previewFileAdded(file) {
    fileUploaded(file, $('#preview-file-uploader'), true);
}


function productsCallbackRunner(callback) {
    divReload('products-selectize');
    setTimeout(function () {
        $('#btn-products').trigger('click');
    }, 1000);
}


$(document).ready(function () {
    $(document).on({
        click: function () {
            let that        = $(this);
            let item        = that.closest('.media');
            let file_hashid = item.attr('data-hashid');
            let box         = that.closest('.upload-box');
            let input       = box.find(hidden_input_selector);
            let added_files;

            try {
                added_files = $.parseJSON(input.val());
            } catch (e) {
                added_files = [];
            }

            added_files = jQuery.grep(added_files, function (value) {
                return value != file_hashid;
            });

            input.val(JSON.stringify(added_files));
            item.remove();
        }
    }, '.file-item-remove');
});
