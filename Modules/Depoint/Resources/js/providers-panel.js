function filesTable() {
    return $('#files-table');
}

function fileItemUrl() {
    return filesTable().data('itemUrl');
}

function removeFileFromBox(file, box) {
    let var_name     = box.find('.dropzone').data('varName');
    let dropzone_obj = eval(var_name);

    dropzone_obj.removeFile(file);
}


function fileView(file) {
    let response    = $.parseJSON(file.xhr.response);
    let file_hashid = response.file;

    $.ajax({
        url       : fileItemUrl().replace('__HASHID__', file_hashid),
        beforeSend: function () {
            filesTable().addClass('disabled-box');
        },
        success   : function (rs) {
            filesTable().append(rs);
            filesTable().removeClass('disabled-box');
        }
    })
}

function mainFileAdded(file) {
    if (file.status == 'success') {
        fileView(file);
        removeFileFromBox(file, $('#main-file-uploader'));
    }
}

function previewImageContainer() {
    return $('#preview-image-container');
}


function imageViewUrl() {
    return previewImageContainer().data('viewUrl');
}


function imageView(file) {
    let response    = $.parseJSON(file.xhr.response);
    let file_hashid = response.file;

    console.log(imageViewUrl());

    $.ajax({
        url       : imageViewUrl().replace('__HASHID__', file_hashid),
        beforeSend: function () {
            previewImageContainer().addClass('disabled-box');
        },
        success   : function (rs) {
            previewImageContainer().html(rs);
            previewImageContainer().removeClass('disabled-box');
        }
    })
}

function previewFileAdded(file) {
    if (file.status == 'success') {
        imageView(file);
        removeFileFromBox(file, $('#preview-file-uploader'));
    }
}

$(document).ready(function () {
    $(document).on({
        click: function (event) {
            event.preventDefault();

            $(this).closest('tr').remove();
        }
    }, '.file-item-delete')
});
