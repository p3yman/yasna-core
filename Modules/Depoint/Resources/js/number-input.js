$(document).ready(function () {

    function escapeNonNumeric(text) {
        let result = text.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        result     = ed(result);
        result     = result.replace(/\D/g, '');
        result     = ad(result);

        result = result ? result : "";

        return result;
    }

    function filterNumberInputValue(input) {
        let original_value = input.val();

        if (!original_value) {
            return;
        }

        let that_self = input[0];
        let start     = that_self.selectionStart;
        let end       = that_self.selectionEnd;
        let value;


        value = escapeNonNumeric(original_value);

        let diff = original_value.length - value.length;
        start -= diff;
        end -= diff;

        input.val(value);
        that_self.setSelectionRange(start, end);
    }

    $(document).on({
        input: function () {
            filterNumberInputValue($(this));
        }
    }, '.number-input');

    $('.number-input').each(function (key, input) {
        filterNumberInputValue($(input));
    });

});
