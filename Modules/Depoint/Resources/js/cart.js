/**
 * Reloads the cart link in the header
 */
function reloadCart() {
    let cart_link = $('#cart-link');
    let url       = cart_link.data('reloadUrl');
    $.ajax({
        url    : url,
        success: function (rs) {
            cart_link.replaceWith(rs);
        }
    })
}


$(document).ready(function () {
    let count_edition_timers = {};

    setInterval(function () {
        reloadCart();
    }, 60000);

    function getCsrfToken() {
        return $('meta[name=csrf-token]').attr('content');
    }

    function cartContainer() {
        return $('.js-cart-container');
    }

    function lockCart() {
        cartContainer().addClass('disabled-box');
    }

    function unlockCart() {
        cartContainer().removeClass('disabled-box');
    }


    function changeCartContent(rs) {
        cartContainer().html(rs);
    }


    function getTimerItem(timer_identifier) {
        if (!count_edition_timers[timer_identifier]) {
            count_edition_timers[timer_identifier] = new Timer();
        }
        return count_edition_timers[timer_identifier];
    }

    $(document).on({
        click: function (event) {
            event.preventDefault();

            let that   = $(this);
            let row    = that.closest('.js-cart-item');
            let hashid = row.data('itemKey');
            let url    = that.data('url');

            $.ajax({
                type      : 'POST',
                url       : url,
                beforeSend: lockCart,
                data      : {
                    hashid: hashid,
                    _token: getCsrfToken(),
                },
                success   : function (rs) {
                    changeCartContent(rs);
                    unlockCart();
                },
            })
        }
    }, '.js-cart-item-remove');

    $(document).on({
        input: function (event) {
            let that   = $(this);
            let row    = that.closest('.js-cart-item');
            let hashid = row.data('itemKey');
            let url    = that.data('url');
            let count  = that.val();

            if (!count) {
                return;
            }

            let timer = getTimerItem(hashid);
            if ((ed(count) < 1)) {
                timer.stop();
                return;
            }

            timer.delay(function () {
                $.ajax({
                    type      : 'POST',
                    url       : url,
                    beforeSend: lockCart,
                    data      : {
                        hashid: hashid,
                        count : count,
                        _token: getCsrfToken(),
                    },
                    success   : function (rs) {
                        changeCartContent(rs);
                        unlockCart();
                    },
                });
            }, 1);

        }
    }, '.js-cart-item-count');

    $(document).on({
        change: function () {
            let that = $(this);
            let item = that.closest('.payment-method');

            $('.payment-method .hide-content').slideUp('fast');

            item.find('.hide-content').slideDown('fast');
        }
    }, '.js-address-item');

    $('.js-address-item').each(function (key, input) {
        let that = $(input);
        if (that.prop('checked')) {
            that.trigger('change');
        }
    });

    $(document).on({
        click: function () {
            let that             = $(this);
            let url              = that.data('url');
            let result_container = $('.js-coupon-code-result');
            result_container.hide();

            let code = $('.js-coupon-code').val();
            if (!code) {
                return;
            }
            $.ajax({
                type      : 'POST',
                url       : url,
                beforeSend: lockCart,
                data      : {
                    _token: getCsrfToken(),
                    code  : code,
                },
                success   : function (rs) {
                    changeCartContent(rs);
                    unlockCart();
                },
                error     : function (xhr) {
                    if (xhr.status == 400) {
                        result_container.html(xhr.responseJSON.error)
                        result_container.show();
                        unlockCart();
                    }
                }
            });
        }
    }, '.js-submit-coupon-code');

    $(document).on({
        click: function (event) {
            event.preventDefault();
            let that = $(this);
            let url  = that.data('url');

            $.ajax({
                type      : 'POST',
                url       : url,
                beforeSend: lockCart,
                data      : {
                    _token: getCsrfToken(),
                },
                success   : function (rs) {
                    changeCartContent(rs);
                    unlockCart();
                },
            });
        }
    }, '.js-disable-code');

});
