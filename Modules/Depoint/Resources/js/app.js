jQuery(document).ready(function($) {

    var direction = $('body').css('direction');

    // Loading
    $('#loading').hide();

    // AOS
    AOS.init({
        once: true,
        offset: 50
    });

    // Match Height
    $('.eq-item').matchHeight();

    // Support
    $('#provinces-select').on('change', function () {

        var selected = $(this).val();

        $('.province').slideUp('fast');
        $('#province-'+selected).slideDown('fast');

    });

    // Home Slider
    $("#main-slide .rslides").responsiveSlides({
        auto: true,
        speed: 500,
        timeout: 4000,
        pager: true,
    });

    // Slider
    $(".product-gallery .rslides").responsiveSlides({
        auto: true,
        speed: 500,
        timeout: 4000,
        pager: true,
        manualControls: "",
    });

    // Smooth Scrolling
    $('a[href*="#"]')
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function() {
                        var $target = $(target);
                        if ($target.is(":focus")) {
                            return false;
                        } else {
                            $target.attr('tabindex','-1');
                        };
                    });
                }
            }
        });

    // Disable toggle area
    $("[data-disable-toggle]").change(function() {
        if(this.checked) {
            $($(this).attr('data-disable-toggle')).removeClass('active');
        } else {
            $($(this).attr('data-disable-toggle')).addClass('active');
        }
    });

    // Accordion
    var allPanels = $('.accordion');
    allPanels.first().addClass('active').find('.content').slideDown();
    $('.accordion > a.title').click(function() {
        allPanels.removeClass('active').find('.content').slideUp();
        $(this).parent().addClass('active').find('.content').slideDown();
        return false;
    });

    $('input[name="payment_method"]').on('change', function () {
        var method = $(this).parents().eq(2);
        $('.payment-method .hide-content').slideUp('fast');
        $(method).find('.hide-content').slideDown('fast');
    })

    // Res panel menu
    $('.res-menu-toggle').on('click', function () {
        $(this).toggleClass('active').next('ul').slideToggle('fast');
    })

    // Lightbox
    lightbox.option({
        'albumLabel': '',
    });

    // Map hover
    $("#iran-map path.Tehran").attr("data-status", "active");
    $("#iran-map svg path").hover(function () {
        var Name = $(this).data("value");
        $(".province-title").text(Name);
        $("svg .Provinces path").mousemove(function (event) {
            if (!event) {
                event = window.event;
            }
            var X = event.pageX;
            var Y = event.pageY;
            $(".province-title").css({
                'display': 'inline-block',
                'top': Y + -300,
                'left': X + -70
            });
        });
    }, function () {
        $(".province-title").css({
            'display': 'none'
        });
    });

    $("svg .Provinces path").on('click', function (event) {
        var target = '#province-' + $(this).attr('class');
        $("#iran-map path").attr("data-status", "deactive");
        $(this).attr("data-status", "active");
        $('.province-details').hide();
        $(target).show();
        console.log(target);
    });

    // Product gallery
    $(".product-single-gallery .gallery-items").responsiveSlides({
        auto: false,
        speed: 500,
        timeout: 4000,
        pager: true,
        manualControls: $('.product-single-gallery .gallery-pager'),
    });

    // Product gallery
    var left = direction == 'rtl' ? 'left' : 'right';
    var right = direction == 'rtl' ? 'right' : 'left';
    $('.product-single-gallery .gallery-pager').slick({
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        rtl: direction == 'ltr' ? true : false,
        prevArrow: '<button class="icon-angle-'+left+' prev" type="button"></button>',
        nextArrow: '<button class="icon-angle-'+right+' next" type="button"></button>'
    });

    // Table toggle
    $('table.orders .toggle button').on('click', function () {
        var tr = $(this).closest('tr').next('tr');
        $(this).parent().toggleClass('toggled').closest('tr').toggleClass('focus');
        $(tr).toggleClass('open');
    });

    /*-----------------------------------------------------------------
     - Modal
     -----------------------------------------------------------------*/
    $('[data-modal]').on('click', function(e) {
        e.preventDefault();
        var target = $(this).attr('data-modal');
        var file_elm = $(this).attr('data-index');
        if (!target.match("^#")) {
            target = '#' + target;
        }
        $('body').addClass('modal-open');
        $('.modal-wrapper').addClass('open');
        $(target).addClass('open').attr('data-file', file_elm);

        $( 'body' ).append( '<div class="overlay"></div>' );
        setTimeout(function(){
            $('.overlay').addClass('open');
            $('.blurable').addClass('open');
        }, 10);
    });
    $(document).on('click', '.modal-wrapper, .close-modal', function(e) {

        if($(e.target).is('.modal *')){
            return;
        }

        e.preventDefault();

        $('body').removeClass('modal-open');
        $('.modal').removeClass('open');
        $('.modal-wrapper').removeClass('open');

        $('.overlay').removeClass('open');
        $('.blurable').removeClass('open');
        setTimeout(function(){
            $('.overlay').remove();
        }, 300);
    });

    // Menu
    $('button.hamburger').on('click', function (e) {
        $(this).toggleClass('is-active');
        $('.menu-overlay').toggleClass('is-active');
        $('body').toggleClass('menu-open');
        $('ul.menu').toggleClass('is-active');
        $('#langs-res').toggleClass('is-active');
    })

    // Faq title select2
    var userList = new List('faq-title-wrapper', {
        valueNames: [ 'suggestion-title' ]
    });
    $("input#faq-title").keyup(function(){
        if (this.value.length >= 1 && userList.update().matchingItems.length > 0) {
            $('.suggestions').addClass('active');
        } else {
            $('.suggestions').removeClass('active');
        }
    });
    $('input#faq-title').blur(function() {
        $('.suggestions').removeClass('active');
    });
    $('input#faq-title').focus(function() {
        if (this.value.length >= 3 && userList.update().matchingItems.length > 0) {
            $('.suggestions').addClass('active');
        }
    });

    /*-----------------------------------------------------------------
    - Landing products colors gallery
    -----------------------------------------------------------------*/
    $(".colors-gallery ul.main-slider").responsiveSlides({
        auto: false,
        speed: 500,
        timeout: 4000,
        pager: true,
        manualControls: $('.product-landing-colors'),
    });
    $('.color-item-gallery').each(function (index) {
        $(this).responsiveSlides({
            auto: true,
            speed: 500,
            timeout: 4000,
            pager: true,
        });
    });
    $('.product-landing-colors a').on('click', function () {
        var list = $(this).parent();
        $(list).find('a').removeClass('active');
        $(this).addClass('active');
    })

    /*-----------------------------------------------------------------
    - Filters
    -----------------------------------------------------------------*/
    // Price slider
    console.log($('body').css('direction'));
    if( typeof noUiSlider == 'object' && $('#price-slider').length ) {
        var slider = document.getElementById('price-slider');
        var slider_min = $('#price-slider').data('min');
        var slider_max = $('#price-slider').data('max');
        var sliderValues = [
            document.getElementById('price-slider-lower'),
            document.getElementById('price-slider-upper')
        ];
        noUiSlider.create(slider, {
            start: [slider_min, slider_max],
            connect: true,
            step: 100000,
            range: {
                'min': slider_min,
                'max': slider_max
            },
            direction: $('body').css('direction'),
        });
        slider.noUiSlider.on('update', function (values, handle) {
            sliderValues[handle].innerHTML = toCommas(Math.round(values[handle]));
        });
    }
    function toCommas(value) {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    /*-----------------------------------------------------------------
    - Gift input
    -----------------------------------------------------------------*/
    $("input.gift-input").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("input.gift-input").keyup(function(){
        if (this.value.length >= this.attributes["maxlength"].value) {
            $(this).parent().next().find('input').focus();
        }
    });

    /*-----------------------------------------------------------------
    - Filters
    -----------------------------------------------------------------*/
    $('a#filters-toggle').on('click', function (e) {
        e.preventDefault();

        var content = $(this).hasClass('active') ? $(this).data('title-active') : $(this).data('title-deactive');

        $(this).toggleClass('active').html(content);
        $('.filters-content').slideToggle();

    });


    /*-----------------------------------------------------------------
    - CV Form
    -----------------------------------------------------------------*/

    //depends panel

    $('#btn-add-depends').on('click', function(e){
        e.preventDefault();

        let target = '#' + $(this).data('target');

        let _first_name = $('input[data-name="first_name"]').val();
        let _relation   = $('input[data-name="relation"]').val();
        let _age        = $('input[data-name="age"]').val();
        let _job        = $('input[data-name="job"]').val();

        let row = $(target).find('tbody tr').length;

        let _tr = '';
        _tr += '<td>'+_first_name+' <input type="hidden" name="dependants['+row+'][first_name]" value="'+_first_name+'"> </td>';
        _tr += '<td>'+_relation+' <input type="hidden" name="dependants['+row+'][relation]" value="'+_relation+'"></td>';
        _tr += '<td>'+_age+' <input type="hidden" name="dependants['+row+'][age]" value="'+_age+'"></td>';
        _tr += '<td>'+_job+' <input type="hidden" name="dependants['+row+'][job]" value="'+_job+'"></td>';
        _tr += '<td><a href="#" class="delete-row">حذف</a></td>';

        _tr = '<tr>'+_tr+'</tr>';

        $(target).find('tbody').append(_tr);

        $('#depends-form input').val('');


    });

    $('tbody').on('click','.delete-row', function (e) {
        e.preventDefault();

        $(this).closest('tr').remove();

    });



    //educational panel

    $('#btn-add-educational').on('click', function(e){
        e.preventDefault();

        let target = '#' + $(this).data('target');

        let _field_orientation_educational = $('input[data-name="field_orientation_educational"]').val();
        let _name_address_educational_institution   = $('input[data-name="name_address_educational_institution"]').val();
        let _type_degree       = $('select[data-name="type_degree"]').val();
        let _year_graduation        = $('input[data-name="year_graduation"]').val();
        let _average        = $('input[data-name="average"]').val();

        let row = $(target).find('tbody tr').length;

        let _tr = '';
        _tr += '<td>'+_field_orientation_educational+' <input type="hidden" name="educational['+row+'][field_orientation_educational]" value="'+_field_orientation_educational+'"> </td>';
        _tr += '<td>'+_name_address_educational_institution+' <input type="hidden" name="educational['+row+'][name_address_educational_institution]" value="'+_name_address_educational_institution+'"></td>';
        _tr += '<td>'+_type_degree +' <input type="hidden" name="educational['+row+'][type_degree]" value="'+_type_degree+'"></td>';
        _tr += '<td>'+_year_graduation+' <input type="hidden" name="educational['+row+'][year_graduation]" value="'+_year_graduation+'"></td>';
        _tr += '<td>'+_average+' <input type="hidden" name="educational['+row+'][average]" value="'+_average+'"></td>';
        _tr += '<td><a href="#" class="delete-row">حذف</a></td>';

        _tr = '<tr>'+_tr+'</tr>';

        $(target).find('tbody').append(_tr);

        $('#educational-form input').val('');


    });

    $('tbody').on('click','.delete-row', function (e) {
        e.preventDefault();

        $(this).closest('tr').remove();

    });


    //workplace panel

    $('#btn-add-workplace').on('click', function(e){
        e.preventDefault();

        let target = '#' + $(this).data('target');

        let _workplace_name = $('input[data-name="workplace_name"]').val();
        let _work_type   = $('input[data-name="work_type"]').val();
        let _from_date       = $('input[data-name="from_date"]').val();
        let _to_date        = $('input[data-name="to_date"]').val();
        let _last_salary        = $('input[data-name="last_salary"]').val();
        let _why_change_job        = $('input[data-name="why_change_job"]').val();
        let _insurance_record        = $('select[data-name="insurance_record"]').val();
        let _presenter_name_phone        = $('input[data-name="presenter_name_phone"]').val();

        let row = $(target).find('tbody tr').length;

        let _tr = '';
        _tr += '<td>'+_workplace_name+' <input type="hidden" name="workplace['+row+'][workplace_name]" value="'+_workplace_name+'"> </td>';
        _tr += '<td>'+_work_type+' <input type="hidden" name="workplace['+row+'][work_type]" value="'+_work_type+'"></td>';
        _tr += '<td>'+_from_date +' <input type="hidden" name="workplace['+row+'][from_date]" value="'+_from_date+'"></td>';
        _tr += '<td>'+_to_date+' <input type="hidden" name="workplace['+row+'][to_date]" value="'+_to_date+'"></td>';
        _tr += '<td>'+_last_salary+' <input type="hidden" name="workplace['+row+'][last_salary]" value="'+_last_salary+'"></td>';
        _tr += '<td>'+_why_change_job+' <input type="hidden" name="workplace['+row+'][why_change_job]" value="'+_why_change_job+'"></td>';
        _tr += '<td>'+_insurance_record+' <input type="hidden" name="workplace['+row+'][insurance_record]" value="'+_insurance_record+'"></td>';
        _tr += '<td>'+_presenter_name_phone+' <input type="hidden" name="workplace['+row+'][presenter_name_phone]" value="'+_presenter_name_phone+'"></td>';
        _tr += '<td><a href="#" class="delete-row">حذف</a></td>';

        _tr = '<tr>'+_tr+'</tr>';

        $(target).find('tbody').append(_tr);

        $('#workplace-form input').val('');


    });

    $('tbody').on('click','.delete-row', function (e) {
        e.preventDefault();

        $(this).closest('tr').remove();

    });



    //specialized  panel

    $('#btn-add-specialized').on('click', function(e){
        e.preventDefault();

        let target = '#' + $(this).data('target');

        let _course_name = $('input[data-name="course_name"]').val();
        let _course_duration   = $('input[data-name="course_duration"]').val();
        let _name_educational_institution  = $('input[data-name="name_educational_institution"]').val();
        let _year_graduation        = $('input[data-name="course_year_graduation"]').val();

        let row = $(target).find('tbody tr').length;

        let _tr = '';
        _tr += '<td>'+_course_name+' <input type="hidden" name="specialized['+row+'][course_name]" value="'+_course_name+'"> </td>';
        _tr += '<td>'+_course_duration+' <input type="hidden" name="specialized['+row+'][course_duration]" value="'+_course_duration+'"></td>';
        _tr += '<td>'+_name_educational_institution +' <input type="hidden" name="specialized['+row+'][name_educational_institution]" value="'+_name_educational_institution+'"></td>';
        _tr += '<td>'+_year_graduation+' <input type="hidden" name="specialized['+row+'][course_year_graduation]" value="'+_year_graduation+'"></td>';
        _tr += '<td><a href="#" class="delete-row">حذف</a></td>';

        _tr = '<tr>'+_tr+'</tr>';

        $(target).find('tbody').append(_tr);

        $('#specialized-form input').val('');


    });

    $('tbody').on('click','.delete-row', function (e) {
        e.preventDefault();

        $(this).closest('tr').remove();

    });


    //knowing  panel

    $('#btn-add-knowing').on('click', function(e){
        e.preventDefault();

        let target = '#' + $(this).data('target');

        let _foreign_lang = $('input[data-name="foreign_lang"]').val();
        let _foreign_speaking   = $('select[data-name="foreign_speaking"]').val();
        let _foreign_listening  = $('select[data-name="foreign_listening"]').val();
        let _foreign_writing        = $('select[data-name="foreign_writing"]').val();
        let _foreign_reading        = $('select[data-name="foreign_reading"]').val();

        let row = $(target).find('tbody tr').length;

        let _tr = '';
        _tr += '<td>'+_foreign_lang+' <input type="hidden" name="knowing['+row+'][foreign_lang]" value="'+_foreign_lang+'"> </td>';
        _tr += '<td>'+_foreign_speaking+' <input type="hidden" name="knowing['+row+'][foreign_speaking]" value="'+_foreign_speaking+'"></td>';
        _tr += '<td>'+_foreign_listening +' <input type="hidden" name="knowing['+row+'][foreign_listening]" value="'+_foreign_listening+'"></td>';
        _tr += '<td>'+_foreign_writing+' <input type="hidden" name="knowing['+row+'][foreign_writing]" value="'+_foreign_writing+'"></td>';
        _tr += '<td>'+_foreign_reading+' <input type="hidden" name="knowing['+row+'][foreign_reading]" value="'+_foreign_reading+'"></td>';
        _tr += '<td><a href="#" class="delete-row">حذف</a></td>';

        _tr = '<tr>'+_tr+'</tr>';

        $(target).find('tbody').append(_tr);

        $('#knowing-form input').val('');


    });

    $('tbody').on('click','.delete-row', function (e) {
        e.preventDefault();

        $(this).closest('tr').remove();

    });


    // Software panel

    $('#btn-add-software').on('click', function(e){
        e.preventDefault();

        let target = '#' + $(this).data('target');

        let _software = $('input[data-name="software"]').val();
        let _level_knowing   = $('select[data-name="level_knowing"]').val();

        let row = $(target).find('tbody tr').length;

        let _tr = '';
        _tr += '<td>'+_software+' <input type="hidden" name="software['+row+'][software]" value="'+_software+'"> </td>';
        _tr += '<td>'+_level_knowing+' <input type="hidden" name="software['+row+'][level_knowing]" value="'+_level_knowing+'"></td>';

        _tr += '<td><a href="#" class="delete-row">حذف</a></td>';

        _tr = '<tr>'+_tr+'</tr>';

        $(target).find('tbody').append(_tr);

        $('#software-form input').val('');


    });

    $('tbody').on('click','.delete-row', function (e) {
        e.preventDefault();

        $(this).closest('tr').remove();

    });



});


// Filter
(function ($) {
    jQuery.expr[':'].Contains = function(a,i,m){
        return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
    };

    function filterList(header, list) {
        var form = $("<form>").attr({"class":"filterform","action":"#"}),
            input = $("<input>").attr({"class":"filterinput lg","type":"text", "placeholder":"جستجوی پرسش..."});
        $(form).append(input).appendTo(header);

        $(input)
            .change( function () {
                var filter = $(this).val();
                if(filter) {

                    $matches = $(list).find('a.title:Contains(' + filter + ')').parent();
                    $('.q', list).not($matches).slideUp();
                    $matches.slideDown();

                } else {
                    $(list).find("li").slideDown();
                }
                return false;
            })
            .keyup( function () {
                $(this).change();
            });
    }

    $(function () {
        filterList($("#form"), $("#list"));
    });
}(jQuery));
