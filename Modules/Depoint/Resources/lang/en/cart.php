<?php
return [
     "disable-code"     => "Remove Coupon Code",
     "credit-purchases" => "Credit Payments",
     "gateway"          => "Bank Gateway",
     "receipt-number"   => "Order Number",

     "receipt" => [
          "title"          => [
               "plural"   => "Drawing Codes",
               "singular" => "Drawing Code",
          ],
          "creation-alert" => "Drawing Code Submission",
          "creation-text"  => [
               "part1" => "Dear Customer :name,",
               "part2" => "Due to the delivery of your order with code :code, "
                    . "the bellow drawing code has been saved and registered in your panel automatically."
               ,
          ],
     ],

     "order" => [
          "title"         => [
               "plural" => "Orders",
          ],
          "date"          => "Order Date",
          "status"        => "Order Status",
          "delivery-date" => "Delivery Date",
          "empty-alert"   => "You have no raised order.",
     ],

     "drawing" => [
          "draw"                  => [
               "singular" => "Draw",
               "prepare"  => "Prepare for Draw",
          ],
          "some-winners"          => ":number Winners",
          "receipts-count-amount" => ":count Receipts (:amount :currency in Total)",
          "winners-of"            => "Winners of :title",
          "redraw"                => "Redraw",
          "no-winner-so-far"      => "No winner So Far.",
          "take-number-between"   => "Select a number from :number1 to :number2.",
          "random-number"         => "Random Number",
     ],

     "message" => [
          "success" => [
               "item-added" => "The product has been added to your cart.",
               "paid"       => "The purchase has been ended successfully.",
          ],
          "failure" => [
               "items-has-been-changed"   => "The items in your cart has been changed.",
               "rejected-by-gateway"      => "The transaction has not been verified by the gateway.",
               "unable-to-verify"         => "Unable to verify the transaction.",
               "coupon-code-is-not-valid" => "The code is invalid.",
               "gateway-required"         => "Please select on of the gateways.",
               "system-error"             => "Due to a system error, it is not possible to complete the purchase process at this time.",
               "system-error-dev"         => "There is no shipping method available.",
          ],
          "hint"    => [
               "repurchase" => "Your order has not been paid. You can try to pay it with one the following gateways.",
          ],
     ],

     "delivery-price" => "Delivery Price",
     "delivery-info"  => "Delivery Information",


     "price" => [
          "unit"                => "Unit Price",
          "total"               => "Total Price",
          "your-purchase-total" => "Your Total Purchased Amount",
          "purchasable"         => "Purchasable",
          "delivery"            => "Delivery Price",
          "free"                => "Free",
     ],

     "receiver" => [
          "name"  => "Receiver Name",
          "title" => [
               "singular" => "Receiver",
          ],
     ],


     "shipping" => [
          "method" => [
               "title" => [
                    "plural"   => "Shipping Methods",
                    "singular" => "Shipping Method",
               ],
          ],

          "rule" => [
               "per-price" => "Price per Ware",
          ],
     ],
];
