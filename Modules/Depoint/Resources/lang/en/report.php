<?php


return [
     'title'         => 'Report',
     'form-title'    => 'Report form',
     'number'        => 'index',
     'fullname'      => 'Fullname',
     'mobile'        => 'Mobile',
     'email'         => "Email",
     'submit'        => 'Result',
     'count'         => "Total",
     'family'        => "Family",
     'name'          => "Name",
     'excel'         => "Export",
     'from-age'      => "from age",
     'to-age'        => 'to age',
     'tel'           => 'Tel',
     'marriage-date' => 'Marriage date',
     'birth-date'    => 'Birth date',
     'city'          => 'city',
     'province'      => 'Province',
     'home-province' => 'Province',
     'home-city'     => 'City',
     'gender'        => 'Gender',
     'blank-value'   => 'Select',
     'excel-fields'  => 'choose Excel fields',
     'export'        => 'Export',
];