<?php
return [
     "attributes" => [
          "code"                => "Code",
          "active"              => "Active",
          "titles"              => 'Titles',
          "name"                => "Name",
          "english_name"        => "English Name",
          "telephone"           => "Telephone",
          "mobile"              => "Mobile",
          "address"             => "Address",
          "inventory"           => "Inventory",
          "daily_use"           => "Daily Use",
          "production_days"     => "Production Days",
          "alert_days"          => "Alert Days",
          "status"              => "Status",
          "count"               => "Count",
          "your-question"       => "Your Question",
          "more-comments"       => "More Comments",
          "day"                 => "Day",
          "month"               => "Month",
          "year"                => "Year",
          "last_password"       => "Old Password",
          "new_password"        => "New Password",
          "new_password_repeat" => "New Password Repeat",
     ],

     "new-password-saved-successfully" => "New password has been saved successfully.",
     "old-password-was-wrong"          => "The old password is wrong.",

     "at-least-one-required" => "At least one of :attributes is required.",

     "payment-type-required" => "Select a payment type.",

     "select-required" => "Selecting :attribute is required.",
];
