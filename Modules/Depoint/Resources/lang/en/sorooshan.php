<?php
return [
     'service-types' => [
          'SV00000001' => 'Repair',
          'SV00000002' => 'Installation and Training',
          'SV00000004' => 'Movement',
          'SV00000019' => 'Production Line Repair',
          'SV00000020' => 'Cancellation',
          'SV00000021' => 'Exchange',
     ],

     'message' => [
          'insert-success' => 'Your request has been submitted successfully. Tracking Code: :code',
          'insert-failure' => 'A problem occurred in the submission process.',
     ],

     'service-tracking' =>[
          'trace-code'              => 'If you have a tracking code, you can check the status of your application through the form below',
          'status-server-error'     => 'System error occurred. Please try again later!',
     ],
];
