<?php
return [
     'main' => [
          "index"      => "Home",
          "support"    => "Support",
          "products"   => "Products",
          "stock"      => "Stock Shop",
          "contact_us" => "Contact Us",
     ],
];
