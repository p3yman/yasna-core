<?php
return [
     "locale-title" => "English",

     "currency" => [
          "IRT" => "Tomans",

     ],

     "our_products"  => "Our Products",
     "about_us"      => "About Us",
     "contact_us"    => "Contact Us",
     "login_to_site" => "Login to Site",
     "colors-images" => "Colors Images",
     "edit-profile"  => "Edit Profile",
     "edit-password" => "Edit Password",

     "shop"                    => "Shop",
     "cart"                    => "Cart",
     "cart_review"             => "Cart Review",
     "checkout_method"         => "Checkout Method",
     "factor_details"          => "Invoice Details",
     "post_profile"            => "Post Profile",
     "payment"                 => "Payment",
     "news"                    => "News",
     "products"                => "Products",
     "related_news"            => "Related News",
     "request_support"         => "Request Support",
     "request_service"         => "Installation and service request",
     "support_type"            => "Support Type",
     "personal_info"           => "Personal Information",
     "contact_info"            => "Contact Information",
     "province_agents"         => "Province Agents",
     "related_posts"           => "Related Posts",
     "introduction"            => "Introduction",
     "board"                   => "Board of Directors",
     "shareholders"            => "Shareholders",
     "job_opportunities"       => "Job Opportunities",
     "available_jobs"          => "Available Jobs",
     "job_description"         => "Job Description",
     "purchase"                => "Purchase",
     "technical_spec"          => "Technical Specifications",
     "download"                => "Download",
     "product_intro"           => "Product Introduction",
     "similar_products"        => "Similar Products",
     "compare_products"        => "Compare Products",
     "menu"                    => "Menu",
     "dashboard"               => "Dashboard",
     "parts"                   => "Parts",
     "parts_archive"           => "Parts Archive",
     "orders"                  => "Orders",
     "orders_archive"          => "Orders Archive",
     "tickets"                 => "Tickets",
     "profile_edit"            => "Edit Profile",
     "password_edit"           => "Edit Password",
     "credit_increase"         => "Increase Credit",
     "order_status"            => "Order Status",
     "order_date"              => "Order Date",
     "order_code"              => "Order Code",
     "answer_ticket"           => "Answer Ticket",
     "ticket_text"             => "Ticket Text",
     "send_answer"             => "Send Answer",
     "submit_ticket"           => "Submit Ticket",
     "order_receipt"           => "Order Receipt",
     "your_payments"           => "Your Payments",
     "order_abstract"          => "Order Abstract",
     "product_invoice"         => "Product Invoice",
     "payment_method"          => "Choose Payment Method",
     "method_online"           => "Pay it online!",
     "method_cash"             => "Pay it in cash, after delivery!",
     "method_credit"           => "Compensate by your site credit.",
     "method_deposit"          => "Settle into our accounts.",
     "method_shetab"           => "Card to Card",
     "your_credit"             => "Your Credit",
     "bank_account_number"     => "Bank Account Number",
     "bank_card_number"        => "Bank Card Number",
     "account_owner"           => "Account Owner",
     "bank"                    => "Bank",
     "bank_tracking_number"    => "Tracking Number",
     "settlement_date"         => "Settlement Date",
     "wallet_inventory"        => "Wallet Inventory",
     "new_lottery_code"        => "Submit New Lottery Code",
     "submitted_lottery_codes" => "Submitted Lottery Codes",
     "charge_account"          => "Charge with the desired amount",
     "support"                 => "Support",
     "faq"                     => "FAQ",
     "commenting"              => "Commenting",
     "ask_us"                  => "Ask Us",
     "new_question"            => "Submit a New Question",
     "your_question"           => "Your Question",
     "question_title"          => "Your Question Title",
     "similar_questions"       => "Question May Have Your Answer",
     "where-to-buy"            => "Where to Buy?",
     "image_gallery"           => "Image Gallery",
     "video_gallery"           => "Video Gallery",
     "festivals"               => "Festivals",
     "passed_festivals"        => "Passed Festivals",
     "future_festivals"        => "Future Festivals",
     "festival_is_done"        => "Held",
     "blog"                    => "Blog",
     "until"                   => "until",
     "go-back"                 => "Go Back",
     "continue"                => "Continue",
     "confirm"                 => "Confirm",
     "confirm-something"       => "Confirm :something",
     "agents"                  => "Agents",
     "servicers"               => "Servicers",
     "follow_up_request"       => "Follow Up Request",
     "insert-tracking-code"    => "Insert tracking code.",
     "no-agents-for-province"  => "No agents registered for this province.",


     "nav" => [
          "index"      => "Home Page",
          "support"    => "Support",
          "products"   => "Products",
          "contact_us" => "Contact Us",
     ],

     "lottery_code_message" => [
          "error"   => "The code is not valid.",
          "success" => "The code submitted successfully.",
     ],

     "service" => [

          "title" => [
               "personal" => "Personal Information",
               "product"  => "Product Information",
               "service"  => "Service",
          ],

          "name"     => "Customer Name",
          "province" => "State",
          "city"     => "City",
          "tel"      => "Phone Number",
          "mobile"   => "Mobile Number",
          "email"    => "Email Address",
          "address"  => "Address",

          "serial"    => "Serial Number",
          "guarantee" => "Guarantee Card Number",

          "sender" => "Sender Name",
          "action" => "Requesting Action Name",
          "desc"   => "Description",

     ],

     "form" => [
          "male"                 => "Male",
          "female"               => "Female",
          "other"                => "Other",
          "company"              => "Company",
          "specify_support_type" => "Specify your support type.",
          "your_message"         => "Your Message",
          "choosing_color"       => "Choosing Color",
          "count"                => "Count",
          "description"          => "Description",

     ],

     "auth"  => [
          "forgot_your_password"    => "Forgot your password?",
          "forgot_password"         => "Forgot Password",
          "reset_password"          => "New Password",
          "reset_password_message"  => "Enter your new password.",
          "forgot_password_message" => "Enter your email to recover your password.",
          "send_reset_link"         => "Send Password Recovery Link",
          "send_reset_token"        => "Send Reset Token",
          "reset_password_token"    => "ًReset Password Token",
          "password_recovery"       => "Password Recovery",
          "change_password"         => "Change Password",
          "login"                   => "Login",
          "register"                => "Register",
          "remember_me"             => "Remember Me",
          "logout"                  => "Logout",
          "accept_policy"           => [
               "part1" => "I agree to the ",
               "part2" => "terms",
               "part3" => ".",
          ],
          "reset_mail_message"      => [
               "part1" => "Hello :name,",
               "part2" => "A reset password request has been submitted for you.",
          ],
          "invalid_inputs"          => "Invalid inputs.",
          "check_token"             => "Check Token",

     ],
     "motto" => [
          "innovation" => [
               "first"  => "Significant",
               "second" => "Innovations",
          ],
          "design"     => [
               "first"  => "Sophisticated",
               "second" => "Design",
          ],
     ],

     "table" => [
          "row"                      => "Row",
          "name"                     => "Name",
          "code"                     => "Code",
          "current_stock"            => "Current Stock",
          "daily_usage_count"        => "Daily Usage Count",
          "alarm_time"               => "Alarm Time",
          "order_number"             => "Order Number",
          "date"                     => "Date",
          "total_price"              => "Total Price",
          "status"                   => "Status",
          "ticket_id"                => "Number", //@TODO: Check if it's meaningful on the view.
          "subject"                  => "Subject",
          "organization"             => "Organization",
          "priority"                 => "Priority",
          "updated_at"               => "Updated at",
          "payment_method"           => "Payment Method",
          "bank"                     => "Bank",
          "tracking_number"          => "Tracking Number",
          "price"                    => "Price",
          "receipt_number"           => "Receipt Number",
          "payment_status"           => "Payment Status",
          "order_status"             => "Order Status",
          "receiver"                 => "Receiver",
          "address"                  => "Address",
          "telephone"                => "Telephone",
          "seller"                   => "Seller",
          "national_id_number"       => "National ID Number",
          "registration_number"      => "Registration Number",
          "economic_number"          => "Economic Number",
          "postal_code"              => "Postal Code",
          "buyer"                    => "Buyer",
          "product_code"             => "Product Code",
          "product"                  => "Product",
          "count"                    => "Count",
          "single_price"             => "Single Price",
          "price_after_discount"     => "Sale Price",
          "total_tax"                => "Total Tax",
          "total_price_tax_discount" => "Total Price after Discount, Taxes and Duties",
          "submitting_date"          => "Submitting Date",
          "city"                     => "City",
          "agent"                    => "Agent",
          'mobile'                   => 'Mobile',
          'email'                    => 'Email',

     ],


     "cart_table" => [
          "review"              => "Review",
          "product"             => "Product",
          "price"               => "Price",
          "count"               => "Count",
          "total"               => "Total",
          "color"               => "Color",
          "add_discount_coupon" => "Discount coupon successfully added.",
          "discount_coupon"     => "Discount Coupon",
          "use_coupon"          => "Use Coupon",
          "primary_sum"         => "Lump Sum",
          "tax"                 => "Tax",
          "discount"            => "Discount",
          "shop_discount"       => "Shop Discount",
          "final_price"         => "Final Price",
          "checkout"            => "Checkout",
          "is_empty"            => "Your cart is empty. Start shopping now!",
          "back_to_shop"        => "Back to the Shop",
          "pay_again"           => "Try Again",


     ],

     "status" => [
          "processing" => "Processing",
          "aborted"    => "Aborted",
          "done"       => "Done",
          "reviewing"  => "Reviewing",
     ],


     "payment_message" => [
          "redirect" => "Redirecting to the online bank payment page...",
          "error"    => [
               "title"   => "Your payment could not be successfully completed.",
               "message" => "There was a problem with the payment, please try again through the button below.",
          ],
          "success"  => [
               "title"   => "Your payment has been successfully completed.",
               "message" => "You will be notified via email and SMS. Also, you may track your order from your profile page on the site.",
          ],
     ],


     "dashboard_message" => [
          "saved-successfully" => "Your information has been submitted successfully.",
     ],


     "recharge_message" => [
          "success" => [
               "part1" => "Increase the credit up to",
               //TODO: //@TODO: Check if it's meaningful on the view. Does it need a space or not?
               "part2" => "Done successfully.",
          ],
     ],

     "support_table" => [
          "city"       => "City",
          "technician" => "Technician",
          "address"    => "Address",
          "telephone"  => "Telephone",
     ],


     "save_checkout"           => "Order Registration and Payment",
     //@TODO: Check if it's meaningful on the view.
     "member_checkout"         => "If you have already bought from us, you can use your address and previous profile to register a new order by logging in to the panel.",
     "post_to_new_address"     => "Send to an address other than the one invoiced?",
     "tracking_number"         => "Tracking Number",
     "support_message"         => "By filling out the form below, our colleagues will contact you to provide support services.",
     "select_support_province" => "Select your province to view the list of dealers and their contact information.",
     "contact_methods"         => "You can contact us by using the following methods.",
     //@TODO: Check if it's meaningful on the view. The DOT.
     "send_your_message"       => "Or submit your message through the form below.",
     //@TODO: Check if it's meaningful on the view. The DOT.
     "choose_bank_gate"        => "Select your gateway. You may do the payment via all SHETAB debit/credit cards.",
     "not_enough_credit"       => [
          "part1" => "and insufficient for the payment. Please go through the section",
          "part2" => "Buy Credit",
          "part3" => "Raising your credibility.",
     ],


     "money_transfer_message" => "You can register the deposit specifications by depositing the invoice amount to the account with the following specifications.",


     "percent" => "%",

     "show_detail"       => "Show Details",
     "show_more"         => "Show more",
     "share_article"     => "Share",
     "send_request"      => "Send Request",
     "send_message"      => "Send Message",
     "send_resume"       => "Send Resume",
     "add_to_cart"       => "Add to Cart",
     "view_and_purchase" => "View and Purchase Product",
     "view_order"        => "View the Order",
     "print_invoice"     => "Print Invoice",
     "confirm_checkout"  => "Confirm and Pay",
     "back"              => "Back",
     "submit"            => "Submit",
     "check_code"        => "Check Code",
     "update"            => "Update",
     "checkout_price"    => "Checkout Price",
     "submit_question"   => "Submit Question",

     "copyright" => "All rights are reserved for :title.", //@TODO: Check if it's meaningful on the view.

     "locales" => [
          "fa" => "Persian",
          "en" => "English",
     ],

     "no-item-to-show"             => "No item to show.",
     "terms-and-conditions"        => "Terms and Conditions",
     "terms-and-conditions-needed" => "You didn't accept the terms and conditions.",
     "message-sent"                => "Your message has been sent successfully.",
     "faq-sent"                    => "Your question has been sent successfully.",

     "download-manuals" => "Download Manuals",
     "download-catalog" => "Download Catalog",
     "details-posts"    => "Details Posts",
     "boolean"          => [
          'on'  => "Yes",
          "off" => "No",
     ],
     "reply-saved"      => "Reply Saved",
     "ticket-saved"     => "Ticket Saved",
     'ticket-show'      => 'Show Ticket',
     'ticket-add'       => 'Add Ticket',

     'address' => [
          'new'            => 'New Address',
          'saved-for-cart' => 'Address has been saved for the cart.',
          'required'       => 'Selecting address is required.',
     ],

     "products-filter" => [
          "search-name"          => "Search Title",
          "price-range"          => "Target Price Range",
          "from"                 => "From",
          "to"                   => "To",
          "only-available-wares" => "Only Available Wares",
          "toggle_active"        => "Hide filters",
          "toggle_deactive"      => "Show filters",
     ],


     "powered-by" => [
          "part1" => "Proudly Powered by",
          "part2" => "",
     ],

     "browser" => [
          "firefox" => "Firefox",
          "chrome"  => "Google Chrome",
     ],

     "tag" => [
          "some-tag" => "Tag \":tag\"",
     ],
];
