<?php
return [
     "section-title" => "Spare Parts Supply Department",

     "product" => [
          "title"          => [
               "plural"   => "Products",
               "singular" => "Product",
          ],
          "create-new"     => "Create new product",
          "edit-something" => "Edit :title",
          "status"         => [
               "activated"   => "Activated",
               "deactivated" => "Deactivated",
          ],
          "manage"         => "Products Managing",
     ],

     "provider" => [
          "title"            => [
               "plural"   => "Providers",
               "singular" => "Provider",
          ],
          "create-new"       => "Create new provider",
          "edit-something"   => "Edit :title",
          "managers"         => "Managers",
          "edit-managers-of" => "Edit managers of «:title»",
          "managers-stored"  => "Managers list has been saved.",
     ],


     "spare-part" => [
          "title"                      => [
               "plural"   => "Spare Parts",
               "singular" => "Spare Part",
          ],
          "create-new"                 => "Create New Spare Part",
          "edit-something"             => "Edit :title",
          "edit"                       => "Edit Spare Part",
          "main-files"                 => "Main Files",
          "preview-file"               => "Preview File",
          "add-to-cart"                => "Add to Cart",
          "add-to-cart-something"      => "Add \":title\" to Cart",
          "added-to-cart-something"    => "The \":title\" spare part has been added to cart.",
          "added-to-cart-numbers"      => ":number spare parts have been added to cart.",
          "main-files-not-changed"     => "You have not made any change in the main files.",
          "main-files-saved-pending"   => "The changes in the main files have been saved and will be applied after the confirmation by admins.",
          "preview-file-not-changed"   => "You have not make any change in the preview file.",
          "preview-file-saved-pending" => "The change in the preview file has been saved and will be applied after the confirmation by admins.",
          "pending-files"              => "Unconfirmed Files",

          "help" => [
               "inventory"       => "How many is available?",
               "daily_use"       => "How many is using daily?",
               "production_days" => "How many days it takes to reproduce?",
               "alert_days"      => "How many days before ending alert should be sent?",
          ],

          "status" => [
               "ok"  => "OK",
               "nok" => "NOK",
          ],

          "inventory-alert" => "Spare Parts Inventory Alert",
     ],

     "cart" => [
          "title"              => [
               "singular" => "Cart",
          ],
          "continue"           => "Continue",
          "checkout"           => "Checkout",
          "no-it-was-wrong"    => "No. It was wrong.",
          "empty-request-note" => "At least one spare part should be ordered.",
          "verify-note"        => "Please check the order's details and hit the \"Checkout\" button. "
               . "Leaving count value of each spare part means that you don't need that.",
          "raised"             => "Order has been saved.",
     ],

     "order" => [
          "title"                => [
               "plural"   => "Providing Orders",
               "singular" => "Providing Order",
          ],
          "creation-alert"       => "Order Submission Notification",
          "modification-alert"   => "Order Modification Notification",
          "deletion-alert"       => "Order Deletion Notification",
          "edit"                 => "Edit Order",
          "unable-to-remove-all" => "You can not remove all items from an order but you can remove the order's self.",
          "virtual-title"        => "Order of :number Spare Parts from :provider",
          "last-modifier"        => "Last Modifier",
          "deleter"              => "Deleter",
          "status"               => [
               "new"       => "New",
               "preparing" => "Preparing",
               "sent"      => "Sent",
               "verified"  => "Verified",
          ],
     ],

     "headline" => [
          "title"     => "Title",
          "code"      => "Code",
          "registrar" => "Registrar",
          "summary"   => "Summary",
     ],

     "browse-tab" => [
          "all"    => "All",
          "search" => "Search",
     ],

     "unit" => [
          "numbers" => "Numbers",
          "days"    => "Days",
     ],
];
