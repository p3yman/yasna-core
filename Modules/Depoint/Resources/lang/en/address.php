<?php
return [
     "title" => [
          "plural" => "Addresses",
     ],

     "add"     => "Add Address",
     "edit"    => "Edit Address",
     "save"    => "Save Address",
     "details" => "Address Details",

     "delete-alert"   => "Do not need this address anymore?",
     "delete-success" => "The address has been deleted successfully.",
     "delete-failure" => "An error occurred int while deleting the address.",

     "empty-text" => "You have no stored address.",

     "location" => [
          "message" => [
               "is-disabled-in-browser"         => "Your browser's location service is disabled.",
               "select-to-increase-precision"   => "For more accuracy, select the address on the map.",
               "thanks-for-specifying-location" => "Thank you for helping us to provide the best possible service by specifying the address on the map.",
          ],
          "guide"   => [
               "activation-firefox" => "Press Alt+t+i to enter settings and select the Permission tab, then remove Access your location from the block.",
               "activation-chrome"  => "Click the :button button or green lock icon at the beginning Address Bar, then in setting, remove Location from the block.",
          ],
     ],
];
