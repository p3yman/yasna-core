<?php
return [
     "title" => [
          "plural" => "نشانی‌ها",
     ],

     "add"     => "افزودن نشانی",
     "edit"    => "ویرایش نشانی",
     "save"    => "ذخیره‌ی نشانی",
     "details" => "جزئیات نشانی",

     "delete-alert"   => "دیگر به این نشانی نیاز ندارید؟",
     "delete-success" => "نشانی با موفقیت حذف شد.",
     "delete-failure" => "مشکلی در فرایند حذف نشانی رخ داد.",

     "empty-text" => "شما نشانی ثبت‌شده‌ای ندارید.",


     "location" => [
          "message" => [
               "is-disabled-in-browser"         => "سرویس Location (موقعیت یاب) مرورگر شما، غیرفعال است.",
               "select-to-increase-precision"   => "  برای دقت بیشتر، آدرس را روی نقشه انتخاب کنید.",
               "thanks-for-specifying-location" => "از اینکه با تعیین آدرس بر روی نقشه، ما را در ارائه هرچه بهتر خدمات یاری نمودید، سپاسگزاریم.",
          ],
          "guide"   => [
               "activation-firefox" => "با فشردن Alt+t+i وارد تنظیمات شوید و تب Permission را انتخاب کنید، سپس Access your location را از حالت بلاک خارج کنید.",
               "activation-chrome"  => "روی دکمه :button یا آیکن قفل سبز، در ابتدای Address Bar کلیک کنید، سپس در تنظیمات، Location را از حالت بلاک خارج کنید.",
          ],
     ],
];
