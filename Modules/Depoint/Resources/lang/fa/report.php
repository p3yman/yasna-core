<?php


return [
     'title'         => 'گزارش گیری',
     'form-title'    => 'فرم گزارش گیری',
     'number'        => 'ردیف',
     'fullname'      => 'نام و نام خانوادگی',
     'mobile'        => 'شماره موبایل',
     'email'         => "آدرس ایمیل",
     'submit'        => 'مشاهده نتیجه',
     'count'         => "تعداد کل",
     'family'        => "نام خانوادگی",
     'name'          => "نام",
     'excel'         => "خروجی اکسل",
     'from-age'      => "از سن",
     'to-age'        => 'تا سن',
     'tel'           => 'تلفن',
     'marriage-date' => 'تاریخ ازدواج',
     'birth-date'    => 'تاریخ تولد',
     'city'          => 'شهر',
     'province'      => 'استان',
     'home-province' => 'استان محل سکونت',
     'home-city'     => 'شهر محل سکونت',
     'gender'        => 'جنسیت',
     'blank-value'   => 'انتخاب کنید',
     'excel-fields'  => 'انتخاب فیلدهای خروجی',
     'export'        => 'دریافت فایل',
];