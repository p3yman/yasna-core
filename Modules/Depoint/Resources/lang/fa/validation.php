<?php
return [
     "attributes" => [
          "code"                => "کد",
          "active"              => "فعال",
          "titles"              => 'عنوان‌ها',
          "name"                => "نام",
          "english_name"        => "نام لاتین",
          "telephone"           => "تلفن",
          "mobile"              => "تلفن همراه",
          "address"             => "آدرس",
          "inventory"           => "موجودی",
          "daily_use"           => "مصرف روزانه",
          "production_days"     => "مدت زمان تولید",
          "alert_days"          => "زمان هشدار",
          "status"              => "وضعیت",
          "count"               => "تعداد",
          "your-question"       => "پرسش شما",
          "more-comments"       => "توضیحات بیش‌تر",
          "day"                 => "روز",
          "month"               => "ماه",
          "year"                => "سال",
          "last_password"       => "رمز عبور قبلی",
          "new_password"        => "رمز عبور جدید",
          "new_password_repeat" => "تکرار رمز عبور جدید",
     ],

     "new-password-saved-successfully" => "رمز عبور جدید، با موفقیت ذخیره شد.",
     "old-password-was-wrong"          => "رمز عبور قبلی اشتباه است.",

     "at-least-one-required" => "حداقل یکی از :attribute الزامی است.",

     "payment-type-required" => "یکی از روش‌های پرداخت را انتخاب کنید.",

     "select-required" => "انتخاب :attribute الزامی است.",
];
