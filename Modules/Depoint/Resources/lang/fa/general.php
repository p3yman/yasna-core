<?php
return [

     "locale-title" => "فارسی",

     "currency"                => [
          "IRT" => "تومان",

     ],
     "our_products"            => "محصولات ما",
     "login_to_site"           => "ورود به سایت",
     "shop"                    => "فروشگاه",
     "cart"                    => "سبد خرید",
     "cart_review"             => "بازبینی سبد",
     "checkout_method"         => "مشخصات و نحوه‌ی ارسال",
     "factor_details"          => "مشخصات فاکتور",
     "post_profile"            => "مشخصات ارسال",
     "payment"                 => "پرداخت",
     "news"                    => "اخبار",
     "products"                => "محصولات",
     "related_news"            => "اخبار مرتبط",
     "request_support"         => "درخواست پشتیبانی",
     "request_service"         => "درخواست نصب و سرویس",
     "support_type"            => "نوع پشتیبانی",
     "personal_info"           => "اطلاعات شخصی",
     "contact_info"            => "اطلاعات تماس",
     "province_agents"         => "نمایندگان استان",
     "contact_us"              => "تماس با ما",
     "related_posts"           => "پست‌های مرتبط",
     "about_us"                => "درباره ما",
     "introduction"            => "معرفی",
     "board"                   => "هیئت مدیره",
     "shareholders"            => "سهامداران",
     "job_opportunities"       => "فرصت‌های شغلی",
     "available_jobs"          => "فرصت‌های شغلی موجود",
     "job_description"         => "شرح موقعیت شغلی",
     "purchase"                => "خرید",
     "technical_spec"          => "مشخصات فنی",
     "download"                => "دانلود",
     "product_intro"           => "معرفی محصول",
     "similar_products"        => "محصولات مشابه",
     "compare_products"        => "مقایسه محصولات",
     "menu"                    => "منو",
     "dashboard"               => "پیشخوان",
     "parts"                   => "قطعات",
     "parts_archive"           => "آرشیو قطعات",
     "orders"                  => "سفارش‌ها",
     "orders_archive"          => "آرشیو سفارش‌ها",
     "tickets"                 => "تیکت‌ها",
     "profile_edit"            => "ویرایش پروفایل",
     "password_edit"           => "ویرایش گذرواژه",
     "credit_increase"         => "افزایش اعتبار",
     "order_status"            => "وضعیت سفارش",
     "order_date"              => "تاریخ سفارش",
     "order_code"              => "کد سفارش",
     "answer_ticket"           => "پاسخ به تیکت‌",
     "ticket_text"             => "متن تیکت",
     "send_answer"             => "ارسال پاسخ",
     "submit_ticket"           => "ثبت تیکت",
     "order_receipt"           => "رسید سفارش",
     "your_payments"           => "پرداخت‌های شما",
     "order_abstract"          => "خلاصه وضعیت سفارش",
     "product_invoice"         => "صورتحساب فروش کالا و خدمات",
     "payment_method"          => "انتخاب روش پرداخت",
     "method_online"           => "پرداخت آنلاین",
     "method_cash"             => "پرداخت نقدی",
     "method_credit"           => "پرداخت از اعتبار",
     "method_deposit"          => "واریز به حساب",
     "method_shetab"           => "کارت به کارت",
     "your_credit"             => "اعتبار شما",
     "bank_account_number"     => "شماره حساب",
     "bank_card_number"        => "شماره کارت",
     "account_owner"           => "صاحب حساب",
     "bank"                    => "بانک",
     "bank_tracking_number"    => "شماره پیگیری",
     "settlement_date"         => "تاریخ واریز",
     "wallet_inventory"        => "موجودی کیف پول",
     "new_lottery_code"        => "ثبت کد جدید",
     "submitted_lottery_codes" => "کدهای ثبت‌شده",
     "charge_account"          => "شارژ با مبلغ دلخواه",
     "support"                 => "پشتیبانی",
     "faq"                     => "پرسش‌های متداول",
     "commenting"              => "ثبت دیدگاه",
     "ask_us"                  => "پرسش خود را نیافتید؟ از ما بپرسید",
     "new_question"            => "ثبت پرسش جدید",
     "your_question"           => "پرسش شما",
     "question_title"          => "عنوان پرسش شما",
     "similar_questions"       => "پرسش‌هایی که ممکن هست پاسخ شما را داشته باشند",
     "where-to-buy"            => "از کجا بخریم؟",
     "image_gallery"           => "گالری تصاویر",
     "video_gallery"           => "گالری ویدئو",
     "festivals"               => "جشنواره‌ها",
     "passed_festivals"        => "جشنواره‌های پیشین",
     "future_festivals"        => "جشنواره‌های پیش رو",
     "festival_is_done"        => "برگزار‌شده",
     "blog"                    => "وبلاگ",
     "until"                   => "تا",
     "colors-images"           => "تصاویر رنگ‌ها",
     "edit-profile"            => "ویرایش پروفایل",
     "edit-password"           => "ویرایش رمز عبور",
     "go-back"                 => "بازگشت",
     "continue"                => "ادامه",
     "confirm"                 => "تأیید",
     "confirm-something"       => "تأیید :something",
     "agents"                  => "نمایندگان",
     "servicers"               => "سرویس‌کاران",
     "follow_up_request"       => "پیگیری درخواست",
     "insert-tracking-code"    => "کد پیگیری خود را وارد کنید.",
     "no-agents-for-province"  => "نماینده‌ای برای این استان ثبت نشده‌است.",

     "nav" => [
          "index"      => "صفحه نخست",
          "support"    => "پشتیبانی",
          "products"   => "محصولات",
          "contact_us" => "تماس با ما",
     ],


     "motto" => [
          "innovation" => [
               'first'  => 'خلاقیتی',
               'second' => 'متمایز',
          ],
          "design"     => [
               'first'  => 'طراحی',
               'second' => 'مدرن',
          ],
     ],

     "auth" => [
          "forgot_your_password"    => "گذرواژه‌ی خود را فراموش کرده‌اید؟",
          "forgot_password"         => "فراموشی گذرواژه",
          "reset_password"          => "گذرواژه‌ی جدید",
          "reset_password_message"  => "گذرواژه‌ی جدید خود را وارد کنید.",
          "forgot_password_message" => "برای بازیابی گذرواژه، لطفاً ایمیل خود را وارد کنید.",
          "send_reset_link"         => "ارسال لینک بازیابی",
          "send_reset_token"        => "ارسال کد بازیابی",
          "reset_password_token"    => "کد بازیابی گذرواژه",
          "password_recovery"       => "بازیابی گذرواژه",
          "change_password"         => "تغییر گذرواژه",
          "login"                   => "ورود",
          "register"                => "ثبت نام",
          "remember_me"             => "مرا به خاطر بسپار",
          "logout"                  => "خروج",
          "accept_policy"           => [
               "part1" => "",
               "part2" => "قوانین و مقررات",
               "part3" => " را به طور کامل می‌پذیرم.",
          ],
          "reset_mail_message"      => [
               "part1" => "سلام :name،",
               "part2" => "یک درخواست بازیابی گذرواژه برای شما ثبت شده است.",
          ],
          "invalid_inputs"          => "اطلاعات وارد شده معتبر نیستند.",
          "check_token"             => "بررسی کد",

     ],

     "lottery_code_message" => [
          "error"   => "کد وارد شده معتبر نیست",
          "success" => "کد با موفقیت ثبت شد.",


     ],

     "service" => [

          "title" => [
               "personal" => "مشخصات فردی",
               "product"  => "مشخصات محصول",
               "service"  => "سرویس",
          ],

          "name"     => "نام مشتری",
          "province" => "استان",
          "city"     => "شهر",
          "tel"      => "شماره تماس",
          "mobile"   => "شماره همراه",
          "email"    => "نشانی پست الکترونیکی",
          "address"  => "نشانی",

          "serial"    => "شماره سریال",
          "guarantee" => "شماره کارت گارانتی",

          "sender" => "نام اقدام کننده",
          "action" => "نوع عملیات مورد درخواست",
          "desc"   => "شرح درخواست سرویس",

     ],

     "form" => [
          "male"                 => "آقا",
          "female"               => "خانم",
          "other"                => "سایر",
          "company"              => "شرکت",
          "specify_support_type" => "نوع درخواست پشتیبانی خود را مشخص کنید.",
          "your_message"         => "پیام شما",
          "choosing_color"       => "انتخاب رنگ",
          "count"                => "تعداد",
          "description"          => "توضیحات",

     ],

     "table" => [
          "row"                      => "ردیف",
          "name"                     => "نام",
          "code"                     => "کد",
          "current_stock"            => "موجودی فعلی",
          "daily_usage_count"        => "تعداد مصرف روزانه",
          "alarm_time"               => "زمان هشدار",
          "order_number"             => "شماره سفارش",
          "date"                     => "تاریخ",
          "total_price"              => "مبلغ کل",
          "status"                   => "وضعیت",
          "ticket_id"                => "شماره", //@TODO: Check if it's meaningful on the view.
          "subject"                  => "موضوع",
          "organization"             => "بخش",
          "priority"                 => "اولویت",
          "updated_at"               => "بروز‌رسانی شده در",
          "payment_method"           => "روش پرداخت",
          "bank"                     => "بانک",
          "tracking_number"          => "کد رهگیری",
          "price"                    => "مبلغ",
          "receipt_number"           => "شماره رسید",
          "payment_status"           => "وضعیت پرداخت",
          "order_status"             => "وضعیت سفارش",
          "receiver"                 => "دریافت‌کننده",
          "address"                  => "آدرس",
          "telephone"                => "تلفن",
          "seller"                   => "فروشنده",
          "national_id_number"       => "شناسه ملی",
          "registration_number"      => "شماره ثبت",
          "economic_number"          => "شماره اقتصادی",
          "postal_code"              => "کد پستی",
          "buyer"                    => "خریدار",
          "product_code"             => "کد کالا",
          "product"                  => "محصول",
          "count"                    => "تعداد",
          "single_price"             => "مبلغ واحد",
          "price_after_discount"     => "مبلغ پس از تخفیف",
          "total_tax"                => "جمع مالیات و عوارض",
          "total_price_tax_discount" => "جمع مبلغ کل پس از تخفیف و مالیات و عوارض",
          "submitting_date"          => "تاریخ ثبت",
          "city"                     => "شهر",
          "agent"                    => "نماینده",
          'mobile'                   => 'موبایل',
          'email'                    => 'ایمیل',
     ],


     "cart_table" => [
          "review"              => "بازبینی",
          "product"             => "محصول",
          "price"               => "قیمت",
          "count"               => "تعداد",
          "total"               => "مجموع",
          "color"               => "رنگ",
          "add_discount_coupon" => "کد تخفیف با موفقیت اضافه شد",
          "discount_coupon"     => "کد تخفیف",
          "use_coupon"          => "اعمال کد",
          "primary_sum"         => "مجموع اولیه",
          "tax"                 => "مالیات",
          "discount"            => "تخفیف",
          "shop_discount"       => "تخفیف فروشگاهی",
          "final_price"         => "قیمت نهایی",
          "checkout"            => "تسویه حساب",
          "is_empty"            => "سبد خرید شما خالی‌ست! خرید را آغاز کنید!",
          "back_to_shop"        => "بازگشت به فروشگاه",
          "pay_again"           => "تلاش مجدد",


     ],


     "status" => [
          "processing" => "در حال انجام",
          "aborted"    => "لغو شده",
          "done"       => "انجام شده",
          "reviewing"  => "در حال بررسی",
     ],


     "dashboard_message" => [
          "saved-successfully" => "اطلاعات شما با موفقیت ذخیره شد.",
     ],

     "payment_message" => [
          "redirect" => "در حال انتقال به صفحه‌ی پرداخت آنلاین بانکی...",
          "error"    => [
               "title"   => "پرداخت شما با موفقیت انجام نشد.",
               "message" => "مشکلی در پرداخت به وجود آمده، لطفا مجددا از طریق دکمه‌ی زیر امتحان کنید.",
          ],
          "success"  => [
               "title"   => "پرداخت شما با موفقیت انجام شد.",
               "message" => "اطلاعات ارسال از طریق ایمیل و پیامک به اطلاع شما خواهد رسید. همچنین می‌توانید از طریق پروفایل کاربری خود در سایت پیگیری سفارش خود را انجام دهید.",
          ],

     ],

     "recharge_message" => [
          "success" => [
               "part1" => "شارژ اعتبار شما به مبلغ",
               "part2" => "با موفقیت انجام شد.",
          ],

     ],


     "support_table" => [
          "city"       => "شهرستان",
          "technician" => "متخصص فنی",
          "address"    => "آدرس",
          "telephone"  => "تلفن",
     ],


     "save_checkout"           => "ثبت سفارش و پرداخت",
     "member_checkout"         => "اگر قبلا از ما خرید داشته‌اید، با ورود به پنل می‌توانید از آدرس و مشخصات قبلی خود برای ثبت سفارش جدید استفاده کنید.",
     "post_to_new_address"     => "به آدرسی متفاوت از آدرس فاکتور ارسال شود؟",
     "tracking_number"         => "کد رهگیری",
     "support_message"         => "با پر کردن فرم زیر، همکاران ما با شما جهت ارائه‌ی خدمات پشتیبانی تماس خواهند گرفت.",
     "select_support_province" => "استان مورد نظر خود را برای مشاهده لیست نمایندگی‌ها و اطلاعات تماس آن‌ها، انتخاب کنید.",
     "contact_methods"         => "از روش‌های زیر می‌توانید با ما تماس بگیرید.",
     //@TODO: Check if it's meaningful on the view. The DOT
     "send_your_message"       => "یا پیام خود را از طریق فرم زیر ارسال کنید.",
     //@TODO: Check if it's meaningful on the view. The DOT
     "choose_bank_gate"        => " درگاه مورد نظر خود را انتخاب کنید. از طریق تمام کارت‌های بانکی عضو شتاب می‌توانید پرداخت خود را انجام دهید.",
     "not_enough_credit"       => [
          "part1" => "و برای پرداخت ناکافی‌ست. لطفا از طریق بخش",
          "part2" => "خرید اعتبار",
          "part3" => "نسبت به افزایش اعتبار خود اقدام کنید.",
     ],
     "money_transfer_message"  => " با واریز مبلغ فاکتور به حساب با مشخصات زیر می‌تواند مشخصات واریز را ثبت کنید.",


     "percent" => "٪",


     "show_detail"       => "مشاهده جزئیات",
     "show_more"         => "مشاهده بیشتر",
     "share_article"     => "به اشتراک بگذارید",
     "send_request"      => "ارسال درخواست",
     "send_message"      => "ارسال پیام",
     "send_resume"       => "ارسال رزومه",
     "add_to_cart"       => "افزودن به سبد خرید",
     "view_and_purchase" => "مشاهده و خرید محصول",
     "view_order"        => "مشاهده سفارش",
     "print_invoice"     => "چاپ فاکتور",
     "confirm_checkout"  => "تایید و پرداخت",
     "back"              => "بازگشت",
     "submit"            => "ثبت",
     "check_code"        => "بررسی کد",
     "update"            => "به‌روزرسانی",
     "checkout_price"    => "پرداخت مبلغ",
     "submit_question"   => "ثبت پرسش",


     "copyright" => "تمام حقوق برای :title محفوظ است.",

     "locales" => [
          "fa" => "فارسی",
          "en" => "انگلیسی",
     ],

     "no-item-to-show"             => "موردی برای نمایش وجود ندارد.",
     "terms-and-conditions"        => "قوانین و مقررات",
     "terms-and-conditions-needed" => "قوانین و مقررات را نپذیرفته‌اید.",
     "message-sent"                => "پیام شما با موفقیت ارسال شد.",
     "faq-sent"                    => "پرسش شما با موفقیت ارسال شد.",

     "download-manuals" => "دانلود دفترچه‌ی راهنما",
     "download-catalog" => "دانلود کاتالوگ",
     "details-posts"    => "پست‌های جزئیات",
     "boolean"          => [
          'on'  => "بله",
          "off" => "خیر",
     ],
     "reply-saved"      => "پاسخ شما ذخیره شد",
     "ticket-saved"     => "تیکت شما ذخیره شد",
     'ticket-show'      => 'نمایش تیکت',
     'ticket-add'       => 'اضافه کردن تیکت',

     'address' => [
          'new'            => 'نشانی جدید',
          'saved-for-cart' => 'نشانی ثبت شد.',
          'required'       => 'انتخاب آدرس الزامی است.',
     ],

     "products-filter" => [
          "search-name"          => "جستجوی نام",
          "price-range"          => "محدوده‌ی قیمت موردنظر",
          "from"                 => "از",
          "to"                   => "تا",
          "only-available-wares" => "فقط کالاهای موجود",
          "toggle_active"        => "مخفی کردن فیلترها",
          "toggle_deactive"      => "نمایش فیلترها",
     ],


     "powered-by" => [
          "part1" => "با افتخار قدرت گرفته از",
          "part2" => "",
     ],

     "browser" => [
          "firefox" => "فایرفاکس",
          "chrome"  => "گوگل کروم",
     ],

     "tag" => [
          "some-tag" => "تگ «:tag»",
     ],
];
