<?php
return [
     "disable-code"     => "غیرفعال کردن کد",
     "credit-purchases" => "خریدهای اعتباری",
     "gateway"          => "درگاه پرداخت",
     "receipt-number"   => "شماره رسید",

     "receipt" => [
          "title"          => [
               "plural"   => "کدهای قرعه‌کشی",
               "singular" => "کد قرعه‌کشی",
          ],
          "creation-alert" => "ثبت کد قرعه‌کشی",
          "creation-text"  => [
               "part1" => "مشتری گرامی :name،",
               "part2" => "با توجه به تحویل سفارش شما با کد :code، "
                    . "کد قرعه‌کشی زیر براش شما ثبت، به صورت خودکار به حساب شما اضافه شد."
               ,
          ],
     ],

     "order" => [
          "title"         => [
               "plural" => "سفارشات",
          ],
          "date"          => "تاریخ سفارش",
          "status"        => "وضعیت سفارش",
          "delivery-date" => "تاریخ تحویل",
          "empty-alert"   => "شما سفارش ثبت‌شده‌ای ندارید.",
     ],

     "drawing" => [
          "draw"                  => [
               "singular" => "قرعه‌کشی",
               "prepare"  => "آماده‌سازی برای قرعه‌کشی",
          ],
          "some-winners"          => ":number برنده",
          "receipts-count-amount" => ":count رسید خرید (در کل :amount :currency)",
          "winners-of"            => "برندگان :title",
          "redraw"                => "قرعه‌کشی دوباره",
          "no-winner-so-far"      => "فعلاً هیچ کس به عنوان برنده انتخاب نشده است.",
          "take-number-between"   => "عددی را بین :number1 تا :number2 انتخاب کنید.",
          "random-number"         => "عدد شانسی",
     ],

     "message" => [
          "success" => [
               "item-added" => "محصول به سبد خرید شما افزوده شد.",
               "paid"       => "پرداخت شما با موفقیت انجام شد.",
          ],
          "failure" => [
               "items-has-been-changed"   => "موارد موجود در سبد خرید شما تغییر کرده است.",
               "rejected-by-gateway"      => "تراکنش از طرف درگاه بانکی تایید نشد.",
               "unable-to-verify"         => "امکان تایید تراکنش وجود ندارد.",
               "coupon-code-is-not-valid" => "کد تخفیف معتبر نیست.",
               "gateway-required"         => "لطفاً یکی از درگاه‌های بانکی را انتخاب کنید.",
               "system-error"             => "به دلیل خطای سیستمی امکان ادامه‌ی فرایند خرید وجود ندارد.",
               "system-error-dev"         => "هیچ روش تحویلی وجود ندارد.",
          ],
          "hint"    => [
               "repurchase" => "سفارش شما پرداخت نشده است. می‌توانید از یکی از درگاه‌های زیر آن را به صورت آنلاین پرداخت کنید.",
          ],
     ],

     "delivery-price" => "هزینه‌ی حمل",
     "delivery-info"  => "اطلاعات ارسال",


     "price" => [
          "unit"                => "قیمت واحد",
          "total"               => "قیمت کل",
          "your-purchase-total" => "جمع خرید شما",
          "purchasable"         => "قابل پرداخت",
          "delivery"            => "هزینه‌ی تحویل",
          "free"                => "رایگان",
     ],

     "receiver" => [
          "name"  => "نام تحویل‌گیرنده",
          "title" => [
               "singular" => "تحویل‌گیرنده",
          ],
     ],


     "shipping" => [
          "method" => [
               "title" => [
                    "plural"   => "روش‌های ارسال",
                    "singular" => "روش ارسال",
               ],
          ],

          "rule" => [
               "per-price" => "قیمت به ازای هر کالا",
          ],
     ],
];
