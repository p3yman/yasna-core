<?php
return [
     'main' => [
          "index"      => "صفحه نخست",
          "support"    => "پشتیبانی",
          "products"   => "محصولات",
          "stock"      => "فروشگاه استوک",
          "contact_us" => "تماس با ما",
     ],
];
