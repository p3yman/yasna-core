<?php
return [
     "user-profile"  => "پروفایل کاربری",
     "dashboard"     => "پیشخوان",
     "edit-profile"  => "ویرایش پروفایل",
     "edit-password" => "ویرایش رمز عبور",
     "first-name"    => "نام",
     "last-name"     => "نام خانوادگی",
     "code-melli"    => "کد ملی",
     "father-name"   => "نام پدر",
     "birthday"      => "تاریخ تولد",
];
