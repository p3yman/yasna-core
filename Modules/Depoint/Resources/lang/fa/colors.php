<?php
return [
     "black"         => "سیاه",
     "gray"          => "خاکستری",
     "white"         => "سفید",
     "leather-white" => "سفید چرمی",
     "label"         => "برچسب‌های‌ رنگ",

     "ticket-flags" => [
          'new'             => "blue",
          "high-priority"   => "red",
          "medium-priority" => "orange",
          "low-priority"    => "gray",
          "done"            => "green",
     ],
];
