<?php
return [
     "section-title" => "بخش تأمین قطعات",

     "product" => [
          "title"          => [
               "plural"   => "محصولات",
               "singular" => "محصول",
          ],
          "create-new"     => "افزودن محصول",
          "edit-something" => "ویرایش :title",
          "status"         => [
               "activated"   => "فعال",
               "deactivated" => "غیرفعال",
          ],
          "manage"         => "مدیریت محصولات",
     ],

     "provider" => [
          "title"            => [
               "plural"   => "تأمین‌کنندگان",
               "singular" => "تأمین‌کننده",
          ],
          "create-new"       => "افزودن تأمین‌کننده",
          "edit-something"   => "ویرایش :title",
          "managers"         => "مدیران",
          "edit-managers-of" => "ویرایش مدیران «:title»",
          "managers-stored"  => "لیست مدیران ذخیره شد.",
     ],


     "spare-part" => [
          "title"                      => [
               "plural"   => "قطعات یدکی",
               "singular" => "قطعه یدکی",
          ],
          "create-new"                 => "افزودن قطعه یدکی",
          "edit-something"             => "ویرایش :title",
          "edit"                       => "ویرایش قطعه‌ یدکی",
          "main-files"                 => "فایل‌های اصلی",
          "preview-file"               => "فایل نمایشی",
          "add-to-cart"                => "افزودن به سبد سفارش",
          "add-to-cart-something"      => "افزودن «:title» به سبد سفارش",
          "added-to-cart-something"    => "قطعه «:title» یدکی به سبد سفارش افزوده شدند.",
          "added-to-cart-numbers"      => ":number قطعه یدکی به سبد سفارش افزوده شدند.",
          "main-files-not-changed"     => "شما تغییری در فایل‌های اصلی ایجاد نکرده‌اید.",
          "main-files-saved-pending"   => "تغییرات فایل‌های اصلی ذخیره شدند و پس از تأیید مدیران در سامانه اعمال خواهند شد.",
          "preview-file-not-changed"   => "شما تغییری در فایل نمایشی ایجاد نکرده‌اید.",
          "preview-file-saved-pending" => "تغییر در فایل نمایشی ذخیره شد و پس از تأیید مدیران در سامانه اعمال خواهد شد.",
          "pending-files"              => "فایل‌های تأیید نشده",

          "help" => [
               "inventory"       => "موجودی فعلی انبار چند عدد است؟",
               "daily_use"       => "روزانه چند عدد استفاده می‌شود؟",
               "production_days" => "چند روز طول می‌کشد تا دوباره تولید شود؟",
               "alert_days"      => "چند روز قبل از تمام شدن هشدار داده شود؟",
          ],

          "status" => [
               "ok"  => "OK",
               "nok" => "NOK",
          ],

          "inventory-alert" => "هشدار کسر موجودی قطعات",
     ],

     "cart" => [
          "title"              => [
               "singular" => "سبد سفارش",
          ],
          "continue"           => "ادامه",
          "checkout"           => "ثبت نهایی",
          "no-it-was-wrong"    => "نه. اشتباه شد.",
          "empty-request-note" => "حداقل یک قطعه باید سفارش داده شود.",
          "verify-note"        => "لطفاً پس از بررسی جزئیات سفارش دکمه‌ی «ثبت نهایی» را بزنید. "
               . "خالی بودن تعداد برای هر یک از قطعات به معنی عدم نیاز به آن قطعه است.",
          "raised"             => "سفارش ثبت شد.",
     ],

     "order" => [
          "title"                => [
               "plural"   => "سفارشات تأمین",
               "singular" => "سفارش تأمین",
          ],
          "creation-alert"       => "اعلان ثبت سفارش جدید",
          "modification-alert"   => "اعلان تغییرات در سفارش",
          "deletion-alert"       => "اعلان حذف سفارش",
          "edit"                 => "ویرایش سفارش",
          "unable-to-remove-all" => "نمی‌توانید همه‌ی قطعات را از یک سفارش حذف کنید. اما می‌توانید خود سفارش را حذف کنید.",
          "virtual-title"        => "سفارش :number قطعه از :provider",
          "last-modifier"        => "آخرین تغییردهنده",
          "deleter"              => "حذف‌کننده",
          "status"               => [
               "new"       => "جدید",
               "preparing" => "در حال آماده‌سازی",
               "sent"      => "ارسال شده",
               "verified"  => "تایید شده",
          ],
     ],

     "headline" => [
          "title"     => "عنوان",
          "code"      => "کد",
          "registrar" => "ثبت‌کننده",
          "summary"   => "خلاصه",
     ],

     "browse-tab" => [
          "all"    => "همه",
          "search" => "جست‌وجو",
     ],

     "unit" => [
          "numbers" => "عدد",
          "days"    => "روز",
     ],
];
