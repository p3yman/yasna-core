<?php

namespace Modules\Depoint\Schedules;

use App\Models\DepointSparePart;
use Modules\Depoint\Notifications\SparePartsRunningOutNotification;
use Modules\Yasna\Services\YasnaSchedule;

class SparePartReduceSchedule extends YasnaSchedule
{
    /**
     * List of the Running Out Spare Part
     *
     * @var array
     */
    protected $running_out_spare_parts = [];



    /**
     * @inheritdoc
     */
    protected function job()
    {
        $this->processSpareParts();
        $this->sendAlerts();
    }



    /**
     * @inheritdoc
     */
    protected function frequency()
    {
        return 'dailyAt:18:00';
    }



    /**
     * Checks if reducing is needed.
     *
     * @return bool
     */
    protected function reduceIsNeeded()
    {
        $now = now();

        if (workdays()->isNotDefined($now)) {
            return false;
        }

        if (workdays()->isClosed($now)) {
            return false;
        }


        return true;
    }



    /**
     * Checks if reducing is not needed.
     *
     * @return bool
     */
    protected function reduceIsNotNeeded()
    {
        return !$this->reduceIsNeeded();
    }



    /**
     * Processes inventory for all spare parts.
     *
     * @return void
     */
    protected function processSpareParts()
    {
        if ($this->reduceIsNotNeeded()) {
            return;
        }

        $spare_parts = model('depoint-spare-part')->get();

        foreach ($spare_parts as $spare_part) {
            $this->processOneSparePart($spare_part);
        }
    }



    /**
     * Processes the inventory of the given spare part.
     *
     * @param DepointSparePart $spare_part
     *
     * @return void
     */
    protected function processOneSparePart(DepointSparePart $spare_part)
    {
        $current_inventory = intval($spare_part->inventory);
        $daily_use         = intval($spare_part->daily_use);
        $new_inventory     = max(($current_inventory - $daily_use), 0);

        $updated_spare_part = $spare_part->batchSave([
             'inventory' => $new_inventory,
        ]);

        $this->checkRemaining($updated_spare_part);
    }



    /**
     * Adds the given spare part to the $running_out_spare_parts array property
     * if that spare part is running out.
     *
     * @param DepointSparePart $spare_part
     */
    protected function checkRemaining(DepointSparePart $spare_part)
    {
        if ($spare_part->isNotRunningOut()) {
            return;
        }

        $this->running_out_spare_parts[] = $spare_part;
    }



    /**
     * Sends notifications if needed.
     */
    protected function sendAlerts()
    {
        if (!count($this->running_out_spare_parts)) {
            return;
        }

        $providing_managers = user()->elector(['permit' => 'depoint_orders.create'])->get();

        foreach ($providing_managers as $user) {
            $user->notify(new SparePartsRunningOutNotification($this->running_out_spare_parts));
        }
    }
}
