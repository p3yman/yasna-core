<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/22/19
 * Time: 5:15 PM
 */

namespace Modules\Depoint\Services\Sorooshan;

use Exception;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;
use SoapClient;
use SoapHeader;
use SoapFault;

class SorooshanClient
{
    use ModuleRecognitionsTrait;

    /**
     * @var SoapClient
     */
    protected $soap;



    /**
     * SorooshanClient constructor.
     */
    public function __construct()
    {
        try {
            $this->makeSoap();
            $this->setHeaders();
        } catch (SoapFault $exception) {
            // this condition will be checked in the `isActive()` method
        }
    }



    /**
     * If the client is ready.
     *
     * @return bool
     */
    public function isActive()
    {
        return boolval($this->soap);
    }



    /**
     * If the client is not ready
     *
     * @return bool
     */
    public function isNotActive()
    {
        return !$this->isActive();
    }



    /**
     * Calls the specified method with the specified arguments from the SOAP client.
     *
     * @param string $method
     * @param array  $arguments
     *
     * @return array|string|null
     */
    public function callMethod(string $method, array $arguments = [])
    {
        $response = $this->soap->__soapCall($method, $arguments);
        return $this->responseToArray($response);
    }



    /**
     * Makes the SOAP client.
     */
    protected function makeSoap()
    {
        $this->soap = new SoapClient($this->getConfig('wsdl'));
    }



    /**
     * Sets the default headers of the SOAP client.
     */
    protected function setHeaders()
    {
        $header = new SoapHeader(
             'http://Service.Sorooshan.Com/',
             'SorooshanCredential',
             [
                  'Username' => $this->getConfig('username'),
                  'Password' => $this->getConfig('password'),
             ]
        );
        $this->soap->__setSoapHeaders($header);
    }



    /**
     * Returns the config of the Sorooshan service.
     *
     * @param string $config
     *
     * @return mixed
     */
    protected function getConfig(string $config)
    {
        return $this->runningModule()->getConfig("sorooshan.$config");
    }



    /**
     * Converts the given SOAP response to an array if it is possible.
     *
     * @param object|string $response
     *
     * @return array|string|null
     */
    protected function responseToArray($response)
    {
        if (is_object($response)) {
            $response = json_decode(json_encode($response), true);
        }

        if (is_array($response)) {
            return collect($response)
                 ->map(function ($part) {
                     return $this->responseToArray($part);
                 })
                 ->toArray()
                 ;
        }


        if (is_string($response)) {
            return $this->xmlToArray($response);
        }

        return $response;
    }



    /**
     * Converts a XML string to an array or returns its original value if it's not convertible.
     *
     * @param string $xml
     *
     * @return string|array|null
     */
    protected function xmlToArray(string $xml)
    {
        try {
            // SimpleXML seems to have problems with the colon ":" in the <xxx:yyy> response tags, so take them out
            $xml = preg_replace('/(<\/?)(\w+):([^>]*>)/', '$1$2$3', $xml);
            $xml = simplexml_load_string($xml);
        } catch (Exception $exception) {
            return $xml;
        }

        $json = json_encode($xml);

        return json_decode($json, true);
    }
}
