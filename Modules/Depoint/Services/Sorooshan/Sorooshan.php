<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/23/19
 * Time: 9:43 AM
 */

namespace Modules\Depoint\Services\Sorooshan;

use App\Models\DepointSorooshanRequest;
use Closure;
use Exception;
use Illuminate\Contracts\Cache\Repository;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class Sorooshan
{
    use ModuleRecognitionsTrait;


    /**
     * @var SorooshanClient
     */
    protected $client;



    /**
     * Sorooshan constructor.
     *
     * @param SorooshanClient $client
     */
    public function __construct(SorooshanClient $client)
    {
        $this->client = $client;
    }



    /**
     * If the service is available
     *
     * @return bool
     */
    public function isAavailable()
    {
        return $this->client->isActive();
    }



    /**
     * If the service is not available.
     *
     * @return bool
     */
    public function isNotAvailable()
    {
        return !$this->isAavailable();
    }



    /**
     * Finds and returns a states with its id.
     *
     * @param int $state_id
     *
     * @return \Illuminate\Support\Collection
     * @throws Exception
     */
    public function findStateById(int $state_id)
    {
        $states = $this->getStates();

        return collect($states)->firstWhere('id', $state_id);
    }



    /**
     * Returns the states.
     *
     * @return Repository
     * @throws Exception;
     */
    public function getStates()
    {
        return $this->readFromCache('states', function () {
            return $this->getFreshStates();
        });
    }



    /**
     * Returns the fresh version of the states.
     *
     * @return array|null
     */
    public function getFreshStates()
    {
        $response = $this->client->callMethod('BindState');
        $states   = ($response['BindStateResult']['xml'] ?? []);

        return collect($states)
             ->sortBy('order')
             ->map(function ($item, $key) {
                 return [
                      'id'    => $key + 1,
                      'title' => $item['StateName'],
                 ];
             })
             ->toArray()
             ;
    }



    /**
     * Returns the city with the given id in a state with the given id.
     *
     * @param int $state_id
     * @param int $city_id
     *
     * @return \Illuminate\Support\Collection
     * @throws Exception
     */
    public function findCityByIds(int $state_id, int $city_id)
    {
        $cities = $this->getCitiesByStateId($state_id);

        return collect($cities)->firstWhere('id', $city_id);
    }



    /**
     * Returns an array of the cities with a state with the given id.
     *
     * @param int $state_id
     *
     * @return array|Repository
     * @throws Exception
     */
    public function getCitiesByStateId(int $state_id)
    {
        $state = $this->findStateById($state_id);

        if (!$state) {
            return [];
        }

        $state_name = $state['title'];

        return $this->getCities($state_name);
    }



    /**
     * Returns the cities based on the specified state.
     *
     * @param string $state_name
     *
     * @return Repository
     * @throws Exception
     */
    public function getCities(string $state_name)
    {
        return $this->readFromCache("cities.$state_name", function () use ($state_name) {
            return $this->getFreshCities($state_name);
        });
    }



    /**
     * Returns the fresh version of the cities based on the specified state.
     *
     * @param string $state_name
     *
     * @return array|null
     */
    public function getFreshCities(string $state_name)
    {
        $response = $this->client->callMethod('BindCity', [
             'parameters' => [
                  'StateName' => $state_name,
             ],
        ]);

        $cities = ($response['BindCityResult']['xml'] ?? []);
        return collect($cities)
             ->sortBy('order')
             ->map(function ($item, $key) {
                 return [
                      'id'    => $key + 1,
                      'title' => $item['CityName'],
                 ];
             })
             ->toArray()
             ;
    }



    /**
     * Calls the `InsertVirtualReception2` method.
     *
     * @param array $data
     *
     * @return DepointSorooshanRequest
     * @throws Exception
     */
    public function insertVirtualReception(array $data)
    {
        $model     = $this->generateModelForInsertVirtualReception($data);
        $arguments = $this->generateInsertVirtualReceptionArguments($model);

        try {
            $response = $this->client->callMethod('InsertVirtualReception2', $arguments);

            $model->response         = $response;
            $model->sorooshan_status = ($response['InsertVirtualReception2Result'] ?? 0);
            $model->status           = $this->guessInsertVirtualReceptionStatus($response);
            $model->warning_message  = ($response['warningMessage'] ?? null);

        } catch (Exception $exception) {
            $model->status = 'exception';
        }

        $model->batchSave($model->toArray());
        return $model;
    }



    /**
     * call the `VirtualReceptionFollowUp` method
     *
     * @param string $trace_code
     *
     * @return DepointSorooshanRequest
     * @throws Exception
     */
    public function virtualReceptionFollowUp($locale, $trace_code)
    {
        $arguments = [
             'WarningMessage' => '',
             'WebTraceCode'   => $trace_code,
             'SortField'      => '',
        ];

        $arguments = [
             'parameters' => $arguments,
        ];
        $response  = $this->client->callMethod('VirtualReceptionFollowUp', $arguments);
        
        try {
            $response = ($response['VirtualReceptionFollowUpResult']['xml']['FullStatusMessage'] ?? $response['warningMessage']);
        } catch (Exception $exception) {
            $response = trans('depoint::sorooshan.service-tracking.status-server-error');
        }
        return $response;
    }



    /**
     * Returns a combo array of the service types.
     *
     * @param string $value_field
     * @param string $caption_field
     *
     * @return array
     */
    public function getServiceTypesCombo(string $value_field = 'value', string $caption_field = 'title')
    {
        return collect($this->getServiceTypes())
             ->map(function (string $type) use ($value_field, $caption_field) {
                 return [
                      $value_field   => $type,
                      $caption_field => $this->getTrans("service-types.$type"),
                 ];
             })->toArray()
             ;
    }



    /**
     * Returns an array of the service types.
     *
     * @return array
     */
    public function getServiceTypes()
    {
        return $this->getConfig('service-type-codes');
    }



    /**
     * Returns the value of a Sorooshan config.
     *
     * @param string $config_path
     *
     * @return mixed
     */
    public function getConfig(string $config_path)
    {
        return $this->runningModule()->getConfig("sorooshan.$config_path");
    }



    /**
     * Returns the value of a Sorooshan trans.
     *
     * @param string      $trans_path
     * @param array       $replacements
     * @param string|null $locale
     *
     * @return array|string|null
     */
    public function getTrans(string $trans_path, array $replacements = [], ?string $locale = null)
    {
        return $this->runningModule()->getTrans("sorooshan.$trans_path", $replacements, $locale);
    }



    /**
     * Reads an item from the cache and stores its value if not exists.
     *
     * @param string  $key
     * @param Closure $value
     *
     * @return Repository
     * @throws Exception
     */
    protected function readFromCache(string $key, Closure $value)
    {
        $cache_key = $this->generateCacheKey($key);

        if (!cache()->has($cache_key)) {
            cache()->put($cache_key, $value(), 24 * 60);
        }

        return cache()->get($cache_key, $value);
    }



    /**
     * Generates the cache key based on the specified key.
     *
     * @param string $key
     *
     * @return string
     */
    protected function generateCacheKey(string $key)
    {
        return $this->cacheKeyPrefix() . $key;
    }



    /**
     * Returns the prefix of the cache keys for the Sorooshan service.
     *
     * @return string
     */
    protected function cacheKeyPrefix()
    {
        return 'depoint.sorooshan.';
    }



    /**
     * Fills a new instance of the DepointSorooshanRequest model and returns it.
     *
     * @param array $data
     *
     * @return DepointSorooshanRequest
     * @throws Exception
     */
    protected function generateModelForInsertVirtualReception(array $data)
    {
        $collection = collect($data);
        $state      = $this->findStateById($collection->get('state'));
        $city       = $this->findCityByIds(
             $collection->get('state'),
             $collection->get('city')
        );

        $data['state'] = ($state['title'] ?? null);
        $data['city']  = ($city['title'] ?? null);

        return model('DepointSorooshanRequest')->fill($data);
    }



    /**
     * Returns an array of the arguments for the `InsertVirtualReception2` method.
     *
     * @param DepointSorooshanRequest $model
     *
     * @return array
     * @throws Exception
     */
    protected function generateInsertVirtualReceptionArguments(DepointSorooshanRequest $model)
    {
        $parameters   = $this->generateInsertVirtualReceptionParameters($model);
        $empty_fields = $this->generateInsertVirtualReceptionEmptyFields();


        return [
             'parameters' => array_default($parameters, $empty_fields),
        ];
    }



    /**
     * Returns an array of the parameters for the `InsertVirtualReception2` method based on the given data.
     *
     * @param DepointSorooshanRequest $model
     *
     * @return array
     * @throws Exception
     */
    protected function generateInsertVirtualReceptionParameters(DepointSorooshanRequest $model)
    {
        return [
             '_CustomerName'      => $model->name,
             '_StateName'         => $model->state,
             '_CityName'          => $model->city,
             '_TelNo'             => $model->tel,
             '_MobileNo'          => $model->mobile,
             '_EmailAddress'      => $model->email,
             '_Address'           => $model->address,
             '_SerialNo'          => $model->serial,
             '_ContactPersonName' => $model->sender,
             '_ServiceType'       => $model->action,
             '_VrDes'             => $model->desc,
             '_Brand'             => 'Depoint',
             '_Gaurantee'         => 0,
        ];
    }



    /**
     * Returns an array of the empty fields for the `InsertVirtualReception2` method.
     *
     * @return array
     */
    protected function generateInsertVirtualReceptionEmptyFields()
    {
        $keys = [
             '_App',
             '_Model',
             '_CustomerName',
             '_CustomerPrCode',
             '_GauranteePrCode',
             '_GauranteeCode',
             '_GauranteeAccountName',
             '_S_GExpDate',
             '_M_GExpDate',
             '_SvCode',
             '_ServiceType',
             '_VrDes',
             '_ContactPersonName',
             '_NationalCode',
             '_PostalCode',
        ];

        return array_fill_keys($keys, '');
    }



    /**
     * Guesses the status based on the given response array and returns it.
     *
     * @param array $response_array
     *
     * @return string
     */
    protected function guessInsertVirtualReceptionStatus(array $response_array)
    {
        $success = ($response_array['InsertVirtualReception2Result'] ?? 0);

        if ($success) {
            return 'success';
        }

        $warning_message = ($response_array['warningMessage'] ?? null);

        $errors_map = $this->getTrans('server-errors', [], 'fa');

        $index = array_search($warning_message, $errors_map);

        if ($index === false) {
            return 'unknown';
        }

        return $index;
    }
}
