<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 9/2/18
 * Time: 3:44 PM
 */

namespace Modules\Depoint\Services\Utilities;


use Modules\Posts\Entities\Post;
use Modules\Shop\Entities\Ware;

class DepointTools
{

    /**
     * try to find hashid of video in aparat
     *
     * @param string $link
     *
     * @return string this is hash of video in aparat
     */
    public static function getAparatHash($link)
    {
        $is_url = strstr($link, '/v/');
        if ($is_url) {
            return str_replace("/v/", "", $is_url);
        } else {
            return $link;
        }
    }



    /**
     * return image that use in image gallery list
     *
     * @param Post $post this post must be a image gallery type
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|null|string
     */
    public static function getListImage($post)
    {
        $file = fileManager()
             ->file($post->featured_image)
             ->posttype($post->posttype)
             ->config('featured_image')
             ->version("340x226")
             ->getUrl()
        ;
        return empty($file) ? currentModule()->asset('images/no-image.png') : $file;
    }



    /**
     * get price of a ware to view it
     *
     * @param   Ware $ware
     * @param bool   $sale_price
     *
     * @return string
     */
    public static function getWarePrice($ware, $sale_price = false)
    {
        $price = self::getWarePurePrice($ware, $sale_price);
        return ad(number_format($price)) . " " . depoint()->shop()->currencyTrans();
    }



    /**
     * find minimum price of post wares
     *
     * @param Post $product
     *
     * @return Ware|null
     */
    public static function getMinimumPriceOfProduct($product, $in_stocks = false)
    {
        $min_price = null;
        $min_ware  = null;
        $wares     = $in_stocks ? $product->wares : $product->depointWares()->get();
        foreach ($wares as $ware) {
            $ware_price = self::getWarePriceBasOnSale($ware);

            if (is_null($min_price)) {
                $min_price = self::getWarePriceBasOnSale($ware);
                $min_ware  = $ware;
                continue;
            }

            if (intval($min_price) == 0) {
                $min_price = $ware_price;
                $min_ware  = $ware;
                continue;
            }


            if (intval($ware_price) < intval($min_price)) {
                if (intval($ware_price) != 0) {
                    $min_price = $ware_price;
                    $min_ware  = $ware;
                }
            }
        }
        return $min_ware;
    }



    /**
     *  get price of a ware
     *
     * @param    Ware $ware
     * @param bool    $sale_price
     *
     * @return int
     */
    public static function getWarePurePrice($ware, $sale_price = false)
    {
        if (is_null($ware)) {
            return 0;
        }
        if ($sale_price) {
            $price = $ware->inLocale(getLocale())
                          ->salePrice()
                          ->withPackage()
                          ->in(depoint()->shop()->mainCurrency())
                          ->get()
            ;
        } else {
            $price = $ware->inLocale(getLocale())
                          ->originalPrice()
                          ->withPackage()
                          ->in(depoint()->shop()->mainCurrency())
                          ->get()
            ;
        }
        return intval($price);
    }



    /**
     * return ware price base on sale attribute
     *
     * @param Ware $ware
     *
     * @return int
     */
    private static function getWarePriceBasOnSale($ware)
    {
        if ($ware->isOnSale()) {
            $ware_price = self::getWarePurePrice($ware, true);
        } else {
            $ware_price = self::getWarePurePrice($ware);
        }
        return $ware_price;
    }
}