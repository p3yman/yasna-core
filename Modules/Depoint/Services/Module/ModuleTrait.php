<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 10:16 AM
 */

namespace Modules\Depoint\Services\Module;

/**
 * Trait ModuleTrait
 * <p>
 * This trait could be attached to any class/trait
 * and be used for accessing all information of the module which that class/trait belongs to.
 * </p>
 *
 * @package Modules\Depoint\Services\Module
 */
trait ModuleTrait
{
    /**
     * Returns an instance of the `Modules\Depoint\Services\Module\ModuleHandler`
     * to be used while accessing the module's information.
     *
     * @return ModuleHandler
     */
    public static function module()
    {
        return ModuleHandler::createFromNamespace(static::class);
    }
}
