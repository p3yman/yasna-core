<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 12:29 PM
 */

namespace Modules\Depoint\Services\Module;


class CurrentModuleHelper
{
    /**
     * The Current Produced Module
     *
     * @var ModuleHandler
     */
    protected static $module;



    /**
     * Assigns the specified module as the current module.
     *
     * @param ModuleHandler $module
     */
    public static function assign(ModuleHandler $module)
    {
        static::$module = $module;
    }



    /**
     * Returns the current produced module.
     *
     * @return ModuleHandler|null
     */
    public static function getCurrentModule()
    {
        return static::$module ?: null;
    }



    /**
     * Returns the path of a blade in the current module.
     *
     * @param string $sub_path
     *
     * @return null|string
     */
    public static function bladePath(string $sub_path)
    {
        return ($module = static::getCurrentModule()) ? $module->bladePath($sub_path) : null;
    }



    /**
     * Returns the path of an asset in the current module.
     *
     * @param string $sub_path
     *
     * @return null|string
     */
    public static function assetPath(string $sub_path)
    {
        return ($module = static::getCurrentModule()) ? $module->assetPath($sub_path) : null;
    }



    /**
     * Returns the asset in the current module.
     *
     * @param string $sub_path
     *
     * @return null|string
     */
    public static function asset(string $sub_path)
    {
        return ($module = static::getCurrentModule()) ? $module->asset($sub_path) : null;
    }



    /**
     * Returns the path of a trans in the current module.
     *
     * @param string $sub_path
     *
     * @return null|string
     */
    public static function transPath(string $sub_path)
    {
        return ($module = static::getCurrentModule()) ? $module->transPath($sub_path) : null;
    }



    /**
     * Returns the requested trans of the current module
     *
     * @param string $sub_path
     * @param array  ...$other_parameters
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string|null
     */
    public static function trans($sub_path, ...$otherParameters)
    {
        return ($module = static::getCurrentModule())
             ? $module->trans($sub_path, ...$otherParameters)
             : null;
    }



    /**
     * Returns the path of a config in the current module.
     *
     * @param string $sub_path
     *
     * @return null|string
     */
    public static function configPath(string $sub_path)
    {
        return ($module = static::getCurrentModule()) ? $module->configPath($sub_path) : null;
    }



    /**
     * Return the config in the current module.
     *
     * @param string $sub_path
     * @param array  ...$other_parameters
     *
     * @return mixed|\Illuminate\Config\Repository
     */
    public static function config(string $sub_path, ...$other_parameters)
    {
        return ($module = static::getCurrentModule())
             ? $module->config($sub_path, ...$other_parameters)
             : null;
    }
}
