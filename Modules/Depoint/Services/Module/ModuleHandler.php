<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 12:24 PM
 */

namespace Modules\Depoint\Services\Module;

use Modules\Yasna\Services\ModuleHelper;
use Nwidart\Modules\Facades\Module;

class ModuleHandler extends ModuleHelper
{
    /**
     * Creates a new instance with the given namespace.
     *
     * @param string $namespace
     *
     * @return $this
     */
    public static function createFromNamespace($namespace)
    {
        $array = explode("\\", $namespace);
        return new static($array[1] ?: null);
    }



    /**
     * Returns the path of a blade in this module.
     *
     * @return string
     */
    public function bladePath($sub_path)
    {
        return $this->getAlias() . '::' . $sub_path;
    }



    /**
     * Returns the path of an asset in this module.
     *
     * @param string $sub_path
     *
     * @return string
     */
    public function assetPath($sub_path)
    {
        return $this->getAlias() . ':' . $sub_path;
    }



    /**
     * Returns the asset in this module.
     *
     * @param string $sub_path
     *
     * @return string
     */
    public function asset($sub_path)
    {
        return Module::asset(static::assetPath($sub_path));
    }



    /**
     * Returns the path of a trans in this module.
     *
     * @param string $sub_path
     *
     * @return string
     */
    public function transPath($sub_path)
    {
        return $this->getAlias() . '::' . $sub_path;
    }



    /**
     * Returns the requested trans of this module
     *
     * @param string $sub_path
     * @param array  ...$other_parameters
     *
     * @return $this|array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function trans($sub_path, ... $other_parameters)
    {
        return trans($this->transPath($sub_path), ...$other_parameters);
    }



    /**
     * Returns the path of a config in this module.
     *
     * @param string $sub_path
     *
     * @return string
     */
    public function configPath($sub_path)
    {
        return $this->getAlias() . '.' . $sub_path;
    }



    /**
     * Return the config in this module.
     *
     * @param string $sub_path
     * @param array  ...$other_parameters
     *
     * @return mixed|\Illuminate\Config\Repository
     */
    public function config($sub_path, ...$other_parameters)
    {
        return config($this->configPath($sub_path), ...$other_parameters);
    }
}
