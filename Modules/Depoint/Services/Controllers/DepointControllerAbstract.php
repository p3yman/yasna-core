<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 11:38 AM
 */

namespace Modules\Depoint\Services\Controllers;


use Modules\Depoint\Services\Controllers\Traits\ModuleControllerTrait;
use Modules\Yasna\Services\YasnaController;

abstract class DepointControllerAbstract extends YasnaController
{
    use ModuleControllerTrait;
}
