<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/26/18
 * Time: 9:32 PM
 */

namespace Modules\Depoint\Services\Controllers\Traits;

use Illuminate\Support\Facades\View;
use Symfony\Component\HttpFoundation\Response;

trait YasnaControllerAlternativeTrait
{
    /**
     * Returns the save view depending on the view folder.
     *
     * @param string $view_file
     * @param bool   $arguments
     *
     * @return View|Response
     */
    protected function safeView($view_file, $arguments = false)
    {
        if ($this->view_folder and !str_contains($view_file, $this->view_folder)) {
            $view_file = "$this->view_folder.$view_file";
        }
        if (!View::exists($view_file)) {
            if (config('app.debug')) {
                return ss("view file [$view_file] not found!");
            } else {
                return $this->abort(404, true);
            }
        }

        return view($view_file, $arguments);
    }



    /**
     * Returns the save view depending on the view folder.
     *
     * @param string $view_file
     * @param array  $arguments
     *
     * @return View|Response
     */
    protected function view($view_file, $arguments = [])
    {
        return $this->safeView($view_file, $arguments);
    }
}
