<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 12:43 PM
 */

namespace Modules\Depoint\Services\Controllers\Traits;


use Modules\Depoint\Services\Module\ModuleHandler;

trait ModuleControllerTrait
{
    /**
     * ModuleControllerTrait constructor.
     * <br>
     * Sets dependencies.
     */
    public function __construct()
    {
        $this->assignCurrentModule();
        $this->makeViewFolderSafe();

        parent::__construct();
    }



    /**
     * Returns an instance of the ModuleHandler class.
     *
     * @return ModuleHandler
     */
    protected function module()
    {
        return ModuleHandler::createFromNamespace(get_class($this));
    }



    /**
     * Returns the name of the module.
     *
     * @return string
     */
    protected function moduleName()
    {
        return $this->module()->getName();
    }



    /**
     * Assigns the current module from the running controller's namespace.
     */
    protected function assignCurrentModule()
    {
        currentModule()->assign($this->module());
    }



    /**
     * Makes sure that the `$view_folder` property contains the module's alias.
     */
    protected function makeViewFolderSafe()
    {
        $view_folder = ($this->view_folder ?? null);
        if (is_string($view_folder) and !str_contains($view_folder, '::')) {
            $this->view_folder = currentModule()->bladePath($this->view_folder);
        }
    }
}
