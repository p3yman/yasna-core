<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 12:23 PM
 */

namespace Modules\Depoint\Services\Controllers\Traits;


use App\Models\Menu;
use Illuminate\Http\Response;
use Illuminate\View\View;

trait TemplateControllerTrait
{
    /**
     * An array to be passed to views as default.
     *
     * @var array
     */
    protected $general_variables = [];



    /**
     * Reads settings from database and sets them in the template properties.
     */
    protected function templateSettings()
    {
        $site_title = get_setting('site_title');

        if ($site_title) {
            depointTemplate()->appendToPageTitle($site_title);
            depointTemplate()->setSiteTitle($site_title);
        }
    }



    /**
     * Set site locales.
     */
    public function templateSiteLocales()
    {
        depointTemplate()->setSiteLocales(depoint()->siteLocales());
    }



    /**
     * Returns some temporary items for main menu.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function templateMainMenuItems()
    {
        $menu_items = menu('main')->items()->get();

        if (user()->hasAccessToStocksEnvironment()) {
            $menu_items->push(
                 new Menu([
                      'titles'  => [
                           getLocale() => currentModule()->trans('menu.main.stock'),
                      ],
                      'links'   => [
                           getLocale() => route_locale('depoint.front.stock.list'),
                      ],
                      'locales' => [getLocale()],
                 ])
            );
        }

        return $menu_items;
    }



    /**
     * Sets the items in of the main menu in the template properties.
     */
    protected function templateMainMenu()
    {
        $main_menu = $this->templateMainMenuItems();

        depointTemplate()->setMainMenu($main_menu);
    }



    /**
     * Sets the items of menus in the template properties.
     */
    protected function templateMenus()
    {
        $this->templateMainMenu();
    }



    /**
     * Returns an array of all the social links to be checked and shown.
     *
     * @return array
     */
    protected function templateSocialItems()
    {
        return [
             [
                  'setting' => 'twitter_link',
                  'icon'    => 'icon-tw',
             ],
             [
                  'setting' => 'instagram_link',
                  'icon'    => 'icon-in',
             ],
             [
                  'setting' => 'linkedin_link',
                  'icon'    => 'icon-li',
             ],
             [
                  'setting' => 'aparat_link',
                  'icon'    => 'icon-ap',
             ],
        ];
    }



    /**
     * Checks and adds available and needed social links to the template properties.
     */
    protected function templateSocialLinks()
    {
        $items = $this->templateSocialItems();

        foreach ($items as $item) {
            $setting = get_setting($item['setting']);
            if ($setting) {
                depointTemplate()->appendToSocialLinks([
                     'link' => $setting,
                     'icon' => $item['icon'],
                ]);
            }
        }
    }



    /**
     * Sets the cart of the current client.
     */
    public function templateCart()
    {
        $cart = depoint()->cart()->findActiveCart();

        if ($cart->exists) {
            $cart->depointRefreshCalculations();
        }

        depointTemplate()->setCart($cart);
    }



    /**
     * Set open graphs.
     *
     * @return $this
     */
    protected function setOpenGraphs()
    {
        depointTemplate()->setOpenGraphs(array_default(depointTemplate()->openGraphs(), [
             'title'       => function () {
                 return depointTemplate()->siteTitle();
             },
             'url'         => request()->url(),
             'image'       => depointTemplate()->siteLogo(),
             'description' => function () {
                 return depointTemplate()->implodePageTitle(' - ');
             },
        ]));
        return $this;
    }



    /**
     * Sets information about Yasna team in template.
     */
    protected function templateYasnaInformation()
    {
        depointTemplate()->setYasnaTeamTitle(get_setting('yasna_team_name'));
        depointTemplate()->setYasnaTeamUrl(get_setting('yasna_team_url'));
    }



    /**
     * Sets needed values in the template properties
     * and then returns the view object to be rendered and shown.
     *
     * @param string $view_name  The path to the view which will be rendered as the response.
     * @param array  $parameters The parameters to be passed to the view
     *
     * @return View|Response
     */
    protected function template($view_name, array $parameters = [])
    {
        $this->templateSettings();
        $this->templateSiteLocales();
        $this->templateMenus();
        $this->templateSocialLinks();
        $this->templateCart();
        $this->setOpenGraphs();
        $this->templateYasnaInformation();

        $parameters = array_merge($this->general_variables, $parameters);

        return $this->view($view_name, $parameters);
    }
}
