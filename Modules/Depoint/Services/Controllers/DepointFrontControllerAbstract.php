<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 1:13 PM
 */

namespace Modules\Depoint\Services\Controllers;


use App\Models\Post;
use Illuminate\Pagination\Paginator;
use Modules\Depoint\Providers\PostsServiceProvider;
use Modules\Depoint\Services\Controllers\Traits\DepointFeedbackControllerTrait;
use Modules\Depoint\Services\Controllers\Traits\TemplateControllerTrait;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;

abstract class DepointFrontControllerAbstract extends DepointControllerAbstract
{
    use TemplateControllerTrait;
    use DepointFeedbackControllerTrait;

    protected $posttype = null;

    /**
     * An array to map post types with their slugs in the database.
     *
     * @var array
     */
    protected $posttype_slugs = [
         'news'             => 'news',
         'pages'            => 'pages',
         'commenting'       => 'commenting',
         'slider'           => 'slider',
         "products-details" => 'products-details',
         "blog"             => 'blog',
         "innovation"       => 'innovation',
         "products"         => "products",
         "stocks"           => "stocks",
         "faq"              => "faq",
         "festivals"        => "festivals",
         "image-gallery"    => "image-gallery",
         "video-gallery"    => "video-gallery",
         "agents"           => "agents",

    ];



    /**
     * DepointFrontControllerAbstract constructor.
     * <br>
     * Sets dependencies.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setPaginationTemplate();
    }



    /**
     * Sets the pagination blade(s)
     */
    protected function setPaginationTemplate()
    {
        Paginator::defaultView(currentModule()->bladePath('pagination.default'));
    }



    /**
     * Finds the post to be shown in the single page.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    protected function singleFindPost(SimpleYasnaRequest $request)
    {
        $select_data = ['hashid' => $request->hashid];

        if (!is_null($this->posttype)) {
            $select_data['type'] = $this->posttype;
        }

        return PostsServiceProvider::select($select_data)->first();
    }



    /**
     * Sets URL parameters for the given post.
     *
     * @param Post $post
     */
    protected function setPostSisterhoodLinksParameters(Post $post)
    {
        $locales    = depoint()->siteLocales();
        $parameters = [];

        foreach ($locales as $locale) {
            if ($locale == getLocale()) {
                continue;
            }

            $sister_post = $post->in($locale);

            if ($sister_post->exists and $sister_post->isPublished()) {
                $parameters[$locale] = $sister_post->direct_url;
            } else {
                $parameters[$locale] = false;
            }
        }

        depoint()->routes()->forceUrlParameters($parameters);
    }



    /**
     * Returns a builder to select product landings.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getProductLandingsBuilder()
    {
        return PostsServiceProvider::select([
             'type'     => $this->posttype_slugs['products'],
             'category' => 'with-landing',
        ])->whereNotNull('slug')
                                   ->where('slug', '<>', '')
             ;
    }
}
