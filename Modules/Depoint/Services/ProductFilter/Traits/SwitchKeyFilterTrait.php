<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 03:42 PM
 */

namespace Modules\Depoint\Services\ProductFilter\Traits;

trait SwitchKeyFilterTrait
{
    protected $switches = [];



    /**
     * Does switch key filter.
     *
     * @param array $filter_conditions
     *
     * @return $this
     */
    public function switchKeyFilter(array $filter_conditions)
    {
        foreach ($filter_conditions as $condition_type => $condition) {
            $this->addSwitch($condition_type, $condition);
        }

        return $this;
    }



    /**
     * Adds switch.
     *
     * @param string $switch_name
     * @param bool   $switch_value
     *
     * @return $this
     */
    protected function addSwitch(string $switch_name, bool $switch_value)
    {
        $this->switches[camel_case($switch_name)] = $switch_value;

        return $this;
    }



    /**
     * Applies switches.
     *
     * @return $this
     */
    protected function applySwitches()
    {
        if (($switches = $this->switches) and count($switches)) {
            foreach ($switches as $switchTitle => $switch) {
                $this->callMethod($switchTitle, [$switch]);
            }
        }

        return $this;
    }



    /**
     * Applies available condition.
     *
     * @param bool $switch
     *
     * @return $this
     */
    protected function available(bool $switch)
    {
        $builder = $this->getBuilder();
        $builder->where('wares.is_available', '=', $switch);
        $this->setBuilder($builder);

        return $this;
    }



    /**
     * Applies special sale condition.
     *
     * @param bool $switch
     *
     * @return $this
     */
    protected function specialSale(bool $switch)
    {
        $builder = $this->getBuilder();
        $builder->whereColumn('wares.sale_price', '<', 'wares.original_price');
        $this->setBuilder($builder);

        return $this;
    }



    /**
     * Weather availability is checked.
     *
     * @return bool
     */
    protected function availabilityIsChecked()
    {
        return $this->switchChecked('available');
    }



    /**
     * Weather special sale is checked.
     *
     * @return bool
     */
    protected function specialSaleIsChecked()
    {
        return $this->switchChecked('special-sale');
    }



    /**
     * Weather the specified switch is checked.
     *
     * @param string $switch
     *
     * @return bool
     */
    protected function switchChecked($switch)
    {
        if (array_key_exists($switch = camel_case($switch), $this->switches) and $this->switches[$switch]) {
            return true;
        } else {
            return false;
        }
    }
}
