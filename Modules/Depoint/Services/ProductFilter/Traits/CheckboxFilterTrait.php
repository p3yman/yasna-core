<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:07 PM
 */

namespace Modules\Depoint\Services\ProductFilter\Traits;

use App\Models\Category;

trait CheckboxFilterTrait
{
    protected $categories = [];
    protected $packages   = [];

    protected $category_folder;



    /**
     * Does the checkbox filter.
     *
     * @param array $filter_conditions
     *
     * @return $this
     */
    public function checkboxFilter(array $filter_conditions)
    {
        foreach ($filter_conditions as $condition_type => $condition) {
            $this->callMethod($condition_type, [$condition]);
        }

        return $this;
    }



    /**
     * Does categories filter.
     *
     * @param array|string $categories
     *
     * @return $this
     */
    public function category($categories)
    {
        if (!is_array($categories)) {
            $categories = [$categories];
        }

        foreach ($categories as $category) {
            $this->setCategory($category);
        }

        return $this;
    }



    /**
     * Sets categories.
     *
     * @param array|string $category
     *
     * @return $this
     */
    protected function setCategory($category)
    {
        $category_id = hashid($category);
        if ($category_id) {
            $this->categories = array_unique(array_merge($this->categories, [$category_id]));
        }

        return $this;
    }



    /**
     * Does packages filter.
     *
     * @param array|string $packages
     *
     * @return $this
     */
    public function package($packages)
    {
        if (!is_array($packages)) {
            $packages = [$packages];
        }

        foreach ($packages as $package) {
            $this->setPackage($package);
        }

        return $this;
    }



    /**
     * Sets packages.
     *
     * @param array|string $package
     *
     * @return $this
     */
    protected function setPackage($package)
    {
        $package_id = hashid($package);
        if ($package_id) {
            $this->packages = array_unique(array_merge($this->packages, [$package_id]));
        }

        return $this;
    }



    /**
     * Applies the checkboxes.
     *
     * @return $this
     */
    protected function applyCheckboxes()
    {
        return $this->applyCategories()
                    ->applyPackages()
             ;
    }



    /**
     * Applies categories.
     *
     * @return $this
     */
    protected function applyCategories()
    {
        $categories = $this->categories;
        if ($this->category_folder) {
            $folder_categories = $this->category_folder->children->pluck('id')->toArray();
            if (count($categories)) {
                $categories = array_intersect($folder_categories, $categories);
            } else {
                $categories = $folder_categories;
            }
        }
        if (count($categories)) {
            $builder = $this->getBuilder();
            $builder->join('category_post', 'posts.id', '=', 'category_post.post_id')
                    ->whereIn('category_post.category_id', $categories)
            ;
            $this->setBuilder($builder);
        }
        return $this;
    }



    /**
     * Applies packages.
     *
     * @return $this
     */
    protected function applyPackages()
    {
        if (($packages = $this->packages) and count($packages)) {
            $builder = $this->getBuilder();
            $builder->whereIn('wares.package_id', $packages);
            $this->setBuilder($builder);
        }

        return $this;
    }



    /**
     * Checks if package has been specified.
     *
     * @return bool
     */
    protected function packageSpecified()
    {
        return boolval(count($this->packages));
    }



    /**
     * Does folder filter.
     *
     * @param Category $folder
     *
     * @return $this
     */
    public function folder(Category $folder)
    {
        if ($folder->exists and $folder->is_folder) {
            $this->category_folder = $folder;
        }

        return $this;
    }



    /**
     * Returns categories folders.
     *
     * @param array ...$parameters
     *
     * @return CheckboxFilterTrait
     */
    public function categoryFolder(...$parameters)
    {
        return $this->folder(...$parameters);
    }



    /**
     * Returns categories folders.
     *
     * @param array ...$parameters
     *
     * @return CheckboxFilterTrait
     */
    public function categoriesFolder(...$parameters)
    {
        return $this->categoryFolder(...$parameters);
    }
}
