<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:12 PM
 */

namespace Modules\Depoint\Services\ProductFilter\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

trait OutputTrait
{
    /**
     * Runs the `get()` method on the builder.
     *
     * @return Collection
     */
    public function run()
    {
        $this->makeQueryReady();

        return $this->getBuilder()->get();
    }



    /**
     * Runs the `paginate()` method on the builder.
     *
     * @param int $limit
     *
     * @return LengthAwarePaginator
     */
    public function paginate($limit)
    {
        $obj = $this;
        Paginator::currentPageResolver(function () use ($obj) {
            return $obj->page;
        });

        $this->makeQueryReady();

        return $this->getBuilder()->paginate($limit);
    }



    /**
     * Returns the raw version of the builder.
     *
     * @return string
     */
    public function raw()
    {
        $this->makeQueryReady();

        $builder  = $this->getBuilder();
        $sql      = $builder->toSql();
        $bindings = $builder->getBindings();

        foreach ($bindings as $val) {
            $sql = str_replace_first('?', is_numeric($val) ? $val : "'$val'", $sql);
        }

        return $sql;
    }



    /**
     * Throws a `Symfony\Component\HttpKernel\Exception\BadRequestHttpException`
     *
     * @throws BadRequestHttpException
     */
    protected function badRequest()
    {
        throw new BadRequestHttpException();
    }
}
