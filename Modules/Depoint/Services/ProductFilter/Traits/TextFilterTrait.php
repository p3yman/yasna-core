<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:06 PM
 */

namespace Modules\Depoint\Services\ProductFilter\Traits;

trait TextFilterTrait
{
    protected $search = [];

    protected static $search_columns_map = [
         'title' => 'posts.title',
    ];



    /**
     * Does text filter.
     *
     * @param array $filter_conditions
     *
     * @return $this
     */
    public function textFilter(array $filter_conditions)
    {
        foreach ($filter_conditions as $condition_type => $condition) {
            $this->addTextFilter($condition_type, $condition);
        }

        return $this;
    }



    /**
     * Adds text filter.
     *
     * @param string $title
     * @param string $value
     *
     * @return $this
     */
    protected function addTextFilter($title, $value)
    {
        $map = self::getSearchColumnsMap();
        if (array_key_exists($title, $map)) {
            $this->search[$map[$title]] = $value;
        }

        return $this;
    }



    /**
     * Applies search condition.
     *
     * @return $this
     */
    protected function applySearch()
    {
        if (($search = $this->search) and count($search)) {
            $builder = $this->getBuilder();
            foreach ($search as $column => $value) {
                $builder->where($column, 'like', '%' . $value . '%');
            }

            $this->setBuilder($builder);
        }

        return $this;
    }



    /**
     * Returns the search columns map.
     *
     * @return array
     */
    public static function getSearchColumnsMap()
    {
        return self::$search_columns_map;
    }
}
