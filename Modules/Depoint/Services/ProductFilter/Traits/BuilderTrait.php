<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 09/12/2017
 * Time: 01:13 PM
 */

namespace Modules\Depoint\Services\ProductFilter\Traits;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Modules\Depoint\Providers\PostsServiceProvider;

trait BuilderTrait
{
    protected $builder = null;

    protected $conditions = [];

    protected static $products_posttype              = 'products';
    protected static $selecting_columns_instructions = [
         'posts' => [
              '*',
         ],
    ];



    /**
     * Adds condition.
     *
     * @param mixed $data
     *
     * @return $this
     */
    public function addCondition($data)
    {
        $this->conditions[] = $data;

        return $this;
    }



    /**
     * Resets the builder.
     *
     * @return Builder|null
     */
    protected function resetBuilder()
    {
        $joinType      = $this->wareImportance() ? 'inner' : '';
        $locale        = getLocale();
        $this->builder = PostsServiceProvider::select([
             'type' => static::$products_posttype,
        ])->select(static::getSelectingColumns())
                                             ->whereHas('depointWares', function ($query) use ($locale) {
                                                 $query->where('wares.locales', 'like', "%$locale%");
                                             })
                                             ->join('wares', function ($query) use ($locale) {
                                                 $query->whereColumn('wares.sisterhood', '=', 'posts.sisterhood');
                                             }, null, null, $joinType)
                                             ->groupBy('posts.id')
        ;

        return $this->builder;
    }



    /**
     * Returns the builder
     *
     * @return Builder|null
     */
    protected function getBuilder()
    {
        return $this->builder ?: $this->resetBuilder();
    }



    /**
     * Sets the builder.
     *
     * @param Builder $builder
     *
     * @return $this
     */
    protected function setBuilder(Builder $builder)
    {
        $this->builder = $builder;

        return $this;
    }



    /**
     * Returns the selecting columns instructions.
     *
     * @return array
     */
    public static function getSelectingColumnsInstructions()
    {
        return self::$selecting_columns_instructions;
    }



    /**
     * Returns the selecting columns.
     *
     * @return array
     */
    protected static function getSelectingColumns()
    {
        $instructions  = self::$selecting_columns_instructions;
        $resultColumns = [];
        foreach ($instructions as $table => $columns) {
            if (!is_array($columns)) {
                $columns = [$columns];
            }

            foreach ($columns as $key => $val) {
                $appending = '';
                if (is_numeric($key)) {
                    $column = $val;
                } else {
                    $column    = $key;
                    $appending = ' as ' . $val;
                }
                $resultColumns[] = $table . '.' . $column . $appending;
            }
        }

        return $resultColumns;
    }
}
