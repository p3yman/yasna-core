<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/5/18
 * Time: 9:12 PM
 */

namespace Modules\Depoint\Services\ProductFilter\Traits;

trait SortTrait
{
    protected $order_column    = 'posts.created_at';
    protected $order_direction = 'desc';



    /**
     * Does sort filter.
     *
     * @param array $filter_conditions
     *
     * @return $this
     */
    public function sortFilter(array $filter_conditions)
    {
        foreach ($filter_conditions as $condition_type => $condition) {
            $this->callMethod($condition_type, [$condition]);
        }

        return $this;
    }



    /**
     * Applies sort condition.
     *
     * @param string $sort
     *
     * @return $this
     */
    public function price($sort)
    {
        $this->order_column    = 'wares.sale_price';
        $this->order_direction = $sort;

        return $this;
    }



    /**
     * Applies publishing condition.
     *
     * @param string $sort
     *
     * @return $this
     */
    public function publishing($sort)
    {
        $this->order_column    = 'published_at';
        $this->order_direction = $sort;

        return $this;
    }



    /**
     * Applies sort.
     *
     * @return $this
     */
    protected function applySort()
    {
        $builder = $this->getBuilder();
        $builder->orderBy($this->order_column, $this->order_direction);

        $this->setBuilder($builder);

        return $this;
    }
}
