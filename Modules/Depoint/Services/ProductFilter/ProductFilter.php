<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 05/12/2017
 * Time: 01:12 PM
 */

namespace Modules\Depoint\Services\ProductFilter;

use App\Models\Post;
use Modules\Depoint\Services\ProductFilter\Traits\BuilderTrait;
use Modules\Depoint\Services\ProductFilter\Traits\CheckboxFilterTrait;
use Modules\Depoint\Services\ProductFilter\Traits\OutputTrait;
use Modules\Depoint\Services\ProductFilter\Traits\PaginationTrait;
use Modules\Depoint\Services\ProductFilter\Traits\RangeFilterTrait;
use Modules\Depoint\Services\ProductFilter\Traits\SortTrait;
use Modules\Depoint\Services\ProductFilter\Traits\SwitchKeyFilterTrait;
use Modules\Depoint\Services\ProductFilter\Traits\TextFilterTrait;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductFilter
{
    use BuilderTrait;
    use OutputTrait;
    use TextFilterTrait;
    use CheckboxFilterTrait;
    use RangeFilterTrait;
    use SwitchKeyFilterTrait;
    use PaginationTrait;
    use SortTrait;



    /**
     * ProductFilter constructor.
     *
     * @param array $filter_data
     */
    public function __construct(array $filter_data)
    {
        $this->assignFilterData($filter_data);
    }



    /**
     * Assigns the filter data
     *
     * @param array $filter_data
     *
     * @return $this
     */
    public function assignFilterData(array $filter_data)
    {
        foreach ($filter_data as $filter_type => $filter_values) {
            $this->callMethod($filter_type . 'Filter', [$filter_values]);
        }

        return $this;
    }



    /**
     * Calls the specified method with the given parameters.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     *
     * @throws BadRequestHttpException
     */
    protected function callMethod(string $method, array $parameters)
    {
        if (method_exists($this, $method)) {
            return $this->$method(...$parameters);
        } else {
            $this->badRequest();
        }
    }



    /**
     * Makes the query builder ready.
     */
    protected function makeQueryReady()
    {
        $this->resetBuilder();
        $this->applyCheckboxes()
             ->applySearch()
             ->applyRanges()
             ->applySwitches()
             ->applySort()
        ;;
    }



    /**
     * Checks if the wares are important or not.
     *
     * @return bool
     */
    protected function wareImportance()
    {
        return $this->availabilityIsChecked() or $this->specialSaleIsChecked() or $this->packageSpecified();
    }
}
