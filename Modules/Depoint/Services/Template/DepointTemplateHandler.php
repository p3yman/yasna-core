<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 8/25/18
 * Time: 11:46 AM
 */

namespace Modules\Depoint\Services\Template;


use App\Models\Cart;
use Modules\Depoint\Services\Module\ModuleHandler;

class DepointTemplateHandler extends TemplateHandler
{
    /**
     * Parts of the Page's Title
     *
     * @var array
     */
    protected static $page_title = [];
    /**
     * The Title of the Site
     *
     * @var string|null
     */
    protected static $site_title;
    /**
     * Array of Available Locales
     *
     * @var array
     */
    protected static $site_locales = [];
    /**
     * The Items of the Main Menu
     *
     * @var array
     */
    protected static $main_menu = [];
    /**
     * The URL of the Site's Logo
     *
     * @var string|null
     */
    protected static $site_logo;
    /**
     * The URL of the Site's Logo to Be Shown in the Footer
     *
     * @var string|null
     */
    protected static $footer_logo;
    /**
     * List of all social links to be shown in the site's footer.
     *
     * @var array
     */
    protected static $social_links = [];
    /**
     * The Cart of the Current Client
     *
     * @var Cart|null
     */
    protected static $cart;

    /**
     * An Instance of the `Modules\Depoint\Services\Module\ModuleHandler` Class
     * to be used while getting information of the module.
     *
     * @var ModuleHandler|null
     */
    protected static $module;

    /**
     * An Array of the Open Graphs and their Values
     *
     * @var array
     */
    protected static $open_graphs = [];
    /**
     * The Title of Yasna Team
     *
     * @var string|null
     */
    protected static $yasna_team_title;
    /**
     * The URL of Yasna Team's Website
     *
     * @var string|null
     */
    protected static $yasna_team_url;



    /**
     * Returns an instance of the `Modules\Depoint\Services\Module\ModuleHandler`
     * to be used while getting information of the module.
     *
     * @return ModuleHandler
     */
    public static function module()
    {
        if (!static::$module) {
            static::$module = ModuleHandler::createFromNamespace(static::class);
        }

        return static::$module;
    }



    /**
     * Guesses the site logo.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|null|string
     */
    public static function guessSiteLogo()
    {
        $setting = get_setting('site_logo');
        if ($setting) {
            $doc = fileManager()
                 ->file($setting)
                 ->resolve()
            ;
            if ($doc->getUrl()) {
                return $doc->getUrl();
            }
        }

        return static::module()->asset('images/logo.png');
    }



    /**
     * Returns the URL of the Site's Logo
     *
     * @return null|string
     */
    public static function siteLogo()
    {
        if (!static::$site_logo) {
            static::$site_logo = static::guessSiteLogo();
        }

        return static::$site_logo;
    }



    /**
     * Guesses the site's logo to be shown in the site's footer.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|null|string
     */
    public static function guessFooterLogo()
    {
        $setting = get_setting('footer_logo');
        if ($setting) {
            $doc = fileManager()
                 ->file($setting)
                 ->resolve()
            ;
            if ($doc->getUrl()) {
                return $doc->getUrl();
            }
        }

        return static::module()->asset('images/logo-footer.png');
    }



    /**
     * Returns the site's logo to be shown in the site's footer.
     *
     * @return null|string
     */
    public static function footerLogo()
    {
        if (!static::$footer_logo) {
            static::$footer_logo = static::guessFooterLogo();
        }

        return static::$footer_logo;
    }
}
