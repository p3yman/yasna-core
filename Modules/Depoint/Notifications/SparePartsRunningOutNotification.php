<?php

namespace Modules\Depoint\Notifications;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SparePartsRunningOutNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    /**
     * The running out spare parts.
     *
     * @var Collection
     */
    protected $spare_parts;
    /**
     * The time which the the notification has been requested at.
     *
     * @var Carbon
     */
    protected $time;



    /**
     * SparePartsRunningOutNotification constructor.
     *
     * @param $spare_parts
     */
    public function __construct($spare_parts)
    {
        $this->spare_parts = $spare_parts;
        $this->time        = now();
    }



    /**
     * Returns the channel(s) of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * Returns an instance `Illuminate\Notifications\Messages\MailMessage` class
     * which will be sent to user.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = trans_safe('depoint::spare-parts.spare-part.inventory-alert');

        return (new MailMessage)
             ->subject($subject)
             ->view('depoint::email.spare-part.running-out', [
                  'user'        => $notifiable,
                  'spare_parts' => $this->spare_parts,
                  'time'        => $this->time,
             ])
             ;
    }



    /**
     * Returns an array version of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             '',
        ];
    }
}
