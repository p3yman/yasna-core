<?php

namespace Modules\Depoint\Notifications;

use App\Models\User;
use Carbon\Carbon;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordResetNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    /**
     * The token to be sent via email.
     *
     * @var string
     */
    protected $token;
    /**
     * The tile which the token is requested.
     *
     * @var Carbon
     */
    protected $time;



    /**
     * PasswordResetNotification constructor.
     *
     * @param string $token
     * @param Carbon $time
     */
    public function __construct(string $token, Carbon $time)
    {
        $this->token = $token;
        $this->time  = $time;
    }



    /**
     * Returns the channel(s) of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * Returns an instance `Illuminate\Notifications\Messages\MailMessage` class
     * which will be sent to user.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $subject = trans_safe('depoint::general.auth.password_recovery');

        return (new MailMessage)
             ->subject($subject)
             ->view('depoint::email.password.token', [
                  'user'  => $notifiable,
                  'token' => $this->token,
                  'time'  => $this->time,
             ])
             ;
    }



    /**
     * Returns an array version of the notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'user_id'        => $notifiable->id,
             'user_full_name' => $notifiable->full_name,
             'token'          => $this->token,
             'time'           => $this->time->toDateTimeString(),
        ];
    }
}
