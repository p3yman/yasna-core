<?php

namespace Modules\Depoint\Notifications;

use App\Models\DepointOrder;
use App\Models\User;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderCreateNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    protected $order;
    protected $time;



    /**
     * OrderCreatedNotification constructor.
     *
     * @param DepointOrder $order
     */
    public function __construct(DepointOrder $order)
    {
        $this->order = $order;
        $this->time  = now();
    }



    /**
     * return the channels which notification should be sent through
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * create the mail message
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order   = $this->order;
        $subject = trans_safe('depoint::spare-parts.order.creation-alert')
             . ' '
             . '('
             . $order->code
             . ')';

        return (new MailMessage)
             ->subject($subject)
             ->view('depoint::email.order.create', [
                  'model' => $this->order,
                  'time'  => $this->time,
             ])
             ;
    }



    /**
     * return array version of this notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'user_id'  => $notifiable->id,
             'order_id' => $this->order->id,
        ];
    }
}
