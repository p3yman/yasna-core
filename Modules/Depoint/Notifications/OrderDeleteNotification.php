<?php

namespace Modules\Depoint\Notifications;

use App\Models\DepointOrder;
use App\Models\User;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderDeleteNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    protected $order;
    protected $user;
    protected $time;



    /**
     * OrderCreatedNotification constructor.
     *
     * @param DepointOrder $order
     */
    public function __construct(DepointOrder $order)
    {
        $this->order = $order;
        $this->user  = user();
        $this->time  = now();
    }



    /**
     * Returns the channels which notification should be sent through.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * Creates the mail message.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order   = $this->order;
        $subject = trans_safe('depoint::spare-parts.order.deletion-alert')
             . ' '
             . '('
             . $order->code
             . ')';

        return (new MailMessage)
             ->subject($subject)
             ->view('depoint::email.order.delete', [
                  'model' => $this->order,
                  'user'  => $this->user,
                  'time'  => $this->time,
             ])
             ;
    }



    /**
     * return array version of this notification.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
             'user_id'  => $notifiable->id,
             'order_id' => $this->order->id,
        ];
    }
}
