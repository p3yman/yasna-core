<?php

namespace Modules\Depoint\Notifications;

use App\Models\Cart;
use App\Models\Receipt;
use App\Models\User;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SendReceiptCreateNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use ModuleRecognitionsTrait;

    protected $receipt;
    protected $cart;



    /**
     * SendReceiptCreteNotification constructor.
     *
     * @param Receipt $receipt
     */
    public function __construct(Receipt $receipt, Cart $cart)
    {
        $this->receipt = $receipt;
        $this->cart    = $cart;
    }



    /**
     * @inheritdoc
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaMailChannel(),
        ];
    }



    /**
     * Returns the mail version of the notification.
     *
     * @param User $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $receipt = $this->receipt;
        $cart    = $this->cart;

        $subject = $this->runningModule()->getTrans('cart.receipt.creation-alert');

        return (new MailMessage)
             ->subject($subject)
             ->view(
                  $this
                       ->runningModule()
                       ->getBladePath('email.receipt.create'), compact('receipt', 'cart')
             )
             ;
    }



    /**
     * @inheritdoc
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
