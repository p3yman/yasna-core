<?php

namespace Modules\Depoint\Notifications;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Notifier\Messages\BrowserMessage;
use Modules\Notifier\Messages\Parts\BrowserWebButton;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class SendTrackingCodeNotification extends YasnaNotificationAbstract //implements ShouldQueue
{
    protected $trace_code;



    /**
     * SendTrackingCodeNotification constructor.
     *
     * @param $trace_code
     */
    public function __construct($trace_code)
    {
        $this->trace_code = $trace_code;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }

    /**
     * Send the message to user when tracking is saved
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms($notifiable)
    {
        return trans('depoint::notification.your-request-has-been-registered' ,
             ['Tracking'=> $this->trace_code]
        );
    }
}
