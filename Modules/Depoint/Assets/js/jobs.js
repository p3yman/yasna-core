jQuery(document).ready(function($) {

    $("#birth_date").pDatepicker({
        initialValue: false,
        format: 'YYYY/MM/DD',
        viewMode: 'year',
        maxDate: new Date(),
        toolbox: {
            enabled: false
        }
    });

    $(".year_date").pDatepicker({
        initialValue: false,
        format: 'YYYY',
        maxDate: new Date(),
        viewMode: 'year',
        toolbox: {
            enabled: false
        },
        dayPicker: {
            enabled: false
        },
        monthPicker: {
            enabled: false
        },
        yearPicker: {
            enabled: true
        }
    });

    $(".range_date").pDatepicker({
        initialValue: false,
        format: 'MMMM YYYY',
        maxDate: new Date(),
        viewMode: 'year',
        toolbox: {
            enabled: false
        },
        dayPicker: {
            enabled: false
        },
        monthPicker: {
            enabled: true
        },
        yearPicker: {
            enabled: true
        }
    });

    $('#submitJobForm').on('click', function (e) {
        e.preventDefault();

        var form = $('#cvForm').serialize();
        var url  = $('#cvForm').attr('action');

        $('input').removeClass('error');
        showInfoAlert('در حال ارسال...');

        $.ajax({
            type      : 'POST',
            url       : url,
            beforeSend: addLoading,
            data      : form,
            success   : function (rs) {
                showSuccess(rs);
            },
            error: function(jqXHR, textStatus, errorThrown){
                if( jqXHR.status === 422 ) {
                    var fields = Object.keys(jqXHR.responseJSON.errors);
                    $.each(fields, function (index, value) {
                        $('input[name="' + value + '"]').addClass('error');
                        $('select[name="' + value + '"]').addClass('error');
                    });
                    showErrorAlert('لطفا مقادیر معتبری وارد کنید.');
                } else {
                    showErrorAlert('مشکلی در ارسال بوجود آمده است. لطفا مجددا تلاش کنید.');
                }
            }
        }).always(function () {
            removeLoading();
        })
    });

    function addLoading() {
        $('#submitJobForm').addClass('loading')
    }

    function removeLoading() {
        $('#submitJobForm').removeClass('loading')
    }

    function showInfoAlert(text) {
        $('.alert').slideUp().removeClass('red blue green').addClass('blue').html(text).slideDown();
    }

    function showSuccessAlert(text) {
        $('.alert').slideUp().removeClass('red blue green').addClass('green').html(text).slideDown();
    }

    function showErrorAlert(text) {
        $('.alert').slideUp().removeClass('red blue green').addClass('red').html(text).slideDown();
    }

    function showSuccess(rs) {
        showSuccessAlert('درخواست شما با موفقیت ثبت شد.');
        removeLoading();
    }

});