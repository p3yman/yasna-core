<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/25/18
 * Time: 2:41 PM
 */

namespace Modules\Depoint\Observers;


use App\Models\Receipt;
use Carbon\Carbon;

class ReceiptObserver
{
    /**
     * Adds values to a creating object of receipt model.
     *
     * @param Receipt $receipt
     */
    public function creating(Receipt $receipt)
    {
        $receipt->code = depoint()
             ->drawing()
             ->createCode(
                  Carbon::parse($receipt->purchased_at)->getTimestamp(),
                  $receipt->purchased_amount
             )
        ;
    }
}
