<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/2/18
 * Time: 10:34 AM
 */

namespace Modules\Depoint\Observers;


use App\Models\DepointOrder;

class DepointOrderObserver
{
    /**
     * Generates and returns a unique code for the specified order
     * based on current month and year and the order's id.
     *
     * @param DepointOrder $order
     *
     * @return string
     */
    protected function generateCode(DepointOrder $order)
    {
        return date('ym') . (1548 + $order->id);
    }



    /**
     * Saves the code for the given order.
     *
     * @param DepointOrder $order
     */
    protected function saveCode(DepointOrder $order)
    {
        $code = $this->generateCode($order);

        $order->code = $code;
        $order->save();
    }



    /**
     * Saves the code for the order after creating the order.
     *
     * @param DepointOrder $order
     */
    public function created(DepointOrder $order)
    {
        $this->saveCode($order);
    }



    /**
     * Saves the code for the order after updating the order if needed.
     *
     * @param DepointOrder $order
     */
    public function updated(DepointOrder $order)
    {
        if (!$order->code) {
            $this->saveCode($order);
        }
    }
}
