<?php

namespace Modules\Depoint\Events;

use Modules\Yasna\Services\YasnaEvent;

class DepointOrderSaved extends YasnaEvent
{
    public $model_name = 'depoint-order';
}
