<?php

namespace Modules\Depoint\Events;

use Modules\Yasna\Services\YasnaEvent;

class DepointOrderDeleted extends YasnaEvent
{
    public $model_name = 'depoint-order';
}
