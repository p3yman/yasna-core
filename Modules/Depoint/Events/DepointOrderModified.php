<?php 

namespace Modules\Depoint\Events;

use Modules\Yasna\Services\YasnaEvent;

class DepointOrderModified extends YasnaEvent
{
    public $model_name = 'depoint-order';
}
