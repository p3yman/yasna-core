<?php

namespace Modules\EhdaFront\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Services\YasnaModel;

class FileDownload extends YasnaModel
{
    use SoftDeletes;

    protected $casts = [
         'meta'        => 'array',
         'expire_date' => 'datetime',
    ];

    protected $fillable = [];
}
