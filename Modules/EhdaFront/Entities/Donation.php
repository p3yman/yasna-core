<?php namespace Modules\EhdaFront\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donation extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ["id"];
}
