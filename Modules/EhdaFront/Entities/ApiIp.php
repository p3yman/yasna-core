<?php

namespace Modules\EhdaFront\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Yasna\Services\YasnaModel;

class ApiIp extends YasnaModel
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
