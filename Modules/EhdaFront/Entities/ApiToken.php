<?php namespace Modules\EhdaFront\Entities;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaModel;

class ApiToken extends YasnaModel
{
    protected $guarded = ['id'];
    protected $casts   = [
        'expired_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public static function delete_expired()
    {
        self::where('expired_at', '<=', Carbon::now()->toDateTimeString())->delete();
    }

    public static function find_token($token = null)
    {
        if (!$token) {
            return false;
        }

        $token = self::where('api_token', $token)->first();
        if ($token) {
            return $token;
        } else {
            return false;
        }
    }
}
