<?php namespace Modules\EhdaFront\Entities;

use App\Models\User;
use Modules\Posts\Entities\Post;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Printing extends YasnaModel
{
    use SoftDeletes;



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'User');
    }



    /**
     * @return User
     */
    public function getUserAttribute()
    {
        $user = $this->user()->first();
        if (!$user or !$user->id) {
            $user = model('user');
        }

        return $user;
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Post');
    }



    /**
     * @return Post|bool
     */
    public function getEventAttribute()
    {
        if ($this->event_id) {
            $event = cache()->remember("post-$this->event_id", 10, function () {
                return $this->event()->first();
            });
        }

        if (isset($event) and $event and $event->exists) {
            return $event;
        } else {
            return false;
        }
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function printing()
    {
        return $this->hasOne(MODELS_NAMESPACE . 'Printing');
    }



    /**
     * @param array $switches
     * @param bool  $overriding_criteria
     *
     * @return mixed
     */
    public static function counter($switches = [], $overriding_criteria = false)
    {
        if ($overriding_criteria) {
            $switches['criteria'] = $overriding_criteria;
        }

        return self::selector($switches)->count();
    }



    /**
     * @param array $switches (accepts 'domain' , 'criteria' , 'user_id' , 'event_id' , 'volunteer_id' )
     */
    public static function selector($parameters = [])
    {
        $switch = array_normalize($parameters, [
             'user_id'    => false,
             'event_id'   => false,
             'domain'     => false,
             'criteria'   => false,
             'created_by' => false,
        ]);

        $table = self::where('id', '>', '0');

        /*-----------------------------------------------
        | Simple Things ...
        */
        if ($switch['user_id']) {
            $table->where('user_id', $switch['user_id']);
        }
        if ($switch['event_id'] and $switch['event_id'] > 0) {
            $table->where('event_id', $switch['event_id']);
        }
        if ($switch['created_by']) {
            $table->where('created_by', $switch['created_by']);
        }

        /*-----------------------------------------------
        | Domain ...
        */
        if ($switch['domain'] == 'auto') {
            if (user()->is_a('manager')) {
                $switch['domain'] = false;
            } else {
                $switch['domain'] = user()->domainsArray('users-card-holder.print'); //@TODO: Vulnerability: What if a user() has access to 'print-excel' but not to 'print-direct'?
            }
        }

        if ($switch['domain'] !== false) {
            $switch['domain'] = (array)$switch['domain'];

            $table = self::whereIn('domain', $switch['domain']);
        }


        /*-----------------------------------------------
        | Criteria ...
        */
        switch ($switch['criteria']) {
            case 'all':
                break;

            case 'under_any_action':
                $table->whereNull('delivered_at');
                break;

            case 'all_with_trashed':
                $table->withTrashed();
                break;

            case 'pending':
                $table->whereNull('queued_at');
                break;

            case 'under_direct_printing':
                $table->whereNotNull('queued_at')->whereNull('printed_at');
                break;

            case 'under_excel_printing':
                $table->whereNotNull('printed_at')->whereNull('verified_at');
                break;

            //case 'under_print' : // means direct
            //	$table->whereNotNull('queued_at')->whereNull('printed_at');
            //	break;
            //
            //case 'under_verification' : // means excel
            //	$table->whereNotNull('printed_at')->whereNull('verified_at');
            //	break;
            //
            //case 'under_dispatch' :
            //	$table->whereNotNull('verified_at')->whereNull('dispatched_at');
            //	break;
            //
            //case 'under_delivery' :
            //	$table->whereNotNull('dispatched_at')->whereNull('delivered_at');
            //	break;
            //
            //case 'archive' :
            //	$table->where('delivered_at');
            //	break;
            //
            //case 'bin' :
            //	$table->onlyTrashed();
            //	break;

        }

        /*-----------------------------------------------
        | Return ...
        */

        return $table;
    }



    /**
     * @return string
     */
    public function getStatusAttribute()
    {
        if ($this->delivered_at) {
            return 'archive';
        }
        if ($this->dispatched_at and !$this->delivered_at) {
            return 'under_delivery';
        }
        if ($this->verified_at and !$this->dispatched_at) {
            return 'under_dispatch';
        }
        if ($this->printed_at and !$this->verified_at) {
            return 'under_verification';
        }
        if ($this->queued_at and !$this->printed_at) {
            return 'under_print';
        }
        if (!$this->queued_at) {
            return 'under_print';
        } else {
            return 'unknown';
        }
    }



    /**
     * @return string
     */
    public function getStatusColorAttribute()
    {
        switch ($this->status) {
            case 'pending':
                return 'danger';

            case 'under_print':
                return 'warning';

            case 'under_verification':
                return 'warning';

            case 'under_dispatch':
                return 'warning';

            case 'under_delivery':
                return 'primary';

            case 'archive':
                return 'success';

            default:
                return 'black';

        }
    }



    /**
     * @param $event
     * @param $user
     *
     * @return int
     */
    public static function addTo($event, $user)
    {
        /*-----------------------------------------------
        | Delete current rows where a pending request is already in the queue ...
        */
        self::selector([
             'user_id'  => $user->id,
             'criteria' => "under_any_action",
        ])->delete()
        ;

        /*-----------------------------------------------
        | Add New Row ...
        */
        return static::instance()->batchSaveId([
             'user_id'  => $user->id,
             'event_id' => $event->id,
        ])
             ;
    }



    /**
     * @param int $event_id
     * @param int $user_id
     *
     * @return $this
     */
    public function addSingleCard(int $event_id, int $user_id)
    {
        $this->selector([
             'user_id'  => $user_id,
             'criteria' => "under_any_action",
        ])->delete()
        ;

        return $this->batchSave([
             "user_id"  => $user_id,
             "event_id" => $event_id,
        ]);
    }



    /**
     * @return array
     */
    public function requiredDataForExcelPrinting()
    {
        return [
             'printed_at'    => $this->now(),
             'queued_at'     => $this->now(),
             'verified_at'   => null,
             'dispatched_at' => null,
             'delivered_at'  => null,
             'printed_by'    => user()->id,
             'queued_by'     => user()->id,
             'verified_by'   => 0,
             'dispatched_by' => 0,
             'delivered_by'  => 0,
        ];
    }



    /**
     * @return array
     */
    public function requiredDataForReversion()
    {
        return [
             'printed_at'    => null,
             'queued_at'     => null,
             'verified_at'   => null,
             'dispatched_at' => null,
             'delivered_at'  => null,
             'printed_by'    => 0,
             'queued_by'     => 0,
             'verified_by'   => 0,
             'dispatched_by' => 0,
             'delivered_by'  => 0,
        ];
    }



    /**
     * @return array
     */
    public function requiredDataForCancellation()
    {
        return [
             'deleted_at' => $this->now(),
             'deleted_by' => user()->id,
        ];
    }



    /**
     * @return array
     */
    public function requiredDataForQualityConfirmation()
    {
        $now      = $this->now();
        $admin_id = user()->id;

        return [
             'printed_at'    => $now,
             'verified_at'   => $now,
             'dispatched_at' => $now,
             'delivered_at'  => $now,
             'printed_by'    => $admin_id,
             'verified_by'   => $admin_id,
             'dispatched_by' => $admin_id,
             'delivered_by'  => $admin_id,
        ];
    }
}
