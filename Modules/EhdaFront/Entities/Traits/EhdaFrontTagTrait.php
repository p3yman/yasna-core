<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/3/18
 * Time: 11:28 AM
 */

namespace Modules\EhdaFront\Entities\Traits;

use Modules\EhdaFront\Providers\RouteServiceProvider;

trait EhdaFrontTagTrait
{
    public function getIndexUrlAttribute()
    {
        return RouteServiceProvider::actionLocale('TagController@index', ['tag' => urlencode($this->slug)]);
    }
}
