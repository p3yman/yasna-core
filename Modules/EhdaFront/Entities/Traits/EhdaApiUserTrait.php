<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 04/03/2018
 * Time: 07:13 PM
 */

namespace Modules\EhdaFront\Entities\Traits;

use App\Models\ApiIp;
use App\Models\ApiToken;

trait EhdaApiUserTrait
{

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------
    |
    */
    public function ips()
    {
        return $this->hasMany(ApiIp::class);
    }


    /*
    |--------------------------------------------------------------------------
    | Meta Fields
    |--------------------------------------------------------------------------
    |
    */

    public static function apiMetaFields()
    {
        return ['portable_printer_configs'];
    }
}
