<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/23/18
 * Time: 3:57 PM
 */

namespace Modules\EhdaFront\Entities\Traits;

trait EhdaFrontCommentTrait
{
    /**
     * @return array
     */
    public function uploadingFilesMetaFields()
    {
        return ['files'];
    }



    /**
     * @return array
     */
    public function sendWorkMetaFields()
    {
        return ['description', 'text_content'];
    }
}
