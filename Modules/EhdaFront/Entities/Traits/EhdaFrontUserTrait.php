<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 27/02/2018
 * Time: 04:55 PM
 */

namespace Modules\EhdaFront\Entities\Traits;

use Illuminate\Notifications\Notifiable;
use Modules\EhdaFront\Providers\RouteServiceProvider;

trait EhdaFrontUserTrait
{


    /**
     * Route notifications for the Aanak channel.
     *
     * @param  \Illuminate\Notifications\Notification $notification
     *
     * @return string
     */
    public function routeNotificationForSms()
    {
        return $this->mobile;
    }



    /**
     * @param string $type
     * @param string $mode
     *
     * @return string
     */
    public function cards($type = 'mini', $mode = 'show')
    {
        return $this->newCards($type, $mode);
    }



    /**
     * @return string
     */
    public function getCardShortUrlAttribute()
    {
        return RouteServiceProvider::action('UserController@shortCard', ['identifier' => $this->hashid]);
    }



    /**
     * @param string $type
     * @param string $mode
     *
     * @return string
     */
    public function newCards($type = 'mini', $mode = 'show')
    {
        $card_type = ['mini', 'single', 'social', 'full'];
        $card_mode = ['show', 'download', 'print'];

        if (!in_array($type, $card_type)) {
            $type = 'mini';
        }

        if (!in_array($mode, $card_mode)) {
            $mode = 'show';
        }

        return RouteServiceProvider::action('NewCardFileController@generateCard', [
             'type'             => $type,
             'user_type_hashid' => $this->cardTypeHashid($type),
             'mode'             => $mode,
        ]);
    }



    /**
     * @param $type
     *
     * @return mixed
     */
    public function cardTypeHashid($type)
    {
        return hashid_encrypt($this->id, 'ehda_card_' . $type);
    }



    /**
     * @return $this
     */
    public function setCardNumber()
    {
        $this->update([
             'card_no'    => $this->id + 5000,
             'updated_by' => user()->id,
        ]);

        return $this;
    }
}
