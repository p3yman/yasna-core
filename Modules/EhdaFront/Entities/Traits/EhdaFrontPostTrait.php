<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 11/03/2018
 * Time: 11:10 AM
 */

namespace Modules\EhdaFront\Entities\Traits;

use Carbon\Carbon;
use Modules\EhdaFront\Providers\EhdaFrontServiceProvider;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;

trait EhdaFrontPostTrait
{
    /**
     * @return string
     */
    public function getShortUrlAttribute()
    {
        return RouteServiceProvider::action('PostController@shortLink', [
             'identifier' => config('ehdafront.prefix.routes.post.short') . $this->hashid,
        ]);
    }



    /**
     * @return string
     */
    protected function directUrl()
    {
        if (app()->runningInConsole()) {
            return $this->consoleDirectUrl();
        } else {
            return $this->pureDirectUrl();
        }
    }



    /**
     * @return string
     */
    public function getDirectUrlAttribute()
    {
        return $this->directUrl();
    }



    /**
     * get the direct link of the model.
     *
     * @return string
     */
    public function getDirectLinkAttribute()
    {
        return $this->getDirectUrlAttribute();
    }



    /**
     * @return string
     */
    public function pureDirectUrl()
    {
        return RouteServiceProvider::action('PostController@showWithFullUrl', [
             'identifier' => $this->hashid,
             'url'        => EhdaFrontServiceProvider::urlHyphen($this->title),
             'lang'       => $this->locale,
        ]);
    }



    /**
     * @return string
     */
    public function getPureDirectUrlAttribute()
    {
        return $this->pureDirectUrl();
    }



    /**
     * @return string
     */
    protected function consoleDirectUrl()
    {
        $url               = $this->pureDirectUrl();
        $parts             = parse_url($url);
        $domain            = $this->domain;
        $omissible_domains = ['global', config('ehdafront.default-domain')];

        if ($domain and !in_array($domain->slug, $omissible_domains)) {
            $parts['host'] = $domain->alias . '.' . $parts['host'];
        }

        return build_url($parts);
    }



    /**
     * @return string
     */
    public function getConsoleDirectUrlAttribute()
    {
        return $this->consoleDirectUrl();
    }



    /**
     * @return null|string
     */
    public function getEventStatusAttribute()
    {
        if (($startTime = Carbon::parse($this->event_starts_at)) and ($endTime = Carbon::parse($this->event_ends_at))) {
            if (($now = Carbon::now())->lessThan($startTime)) {
                return 'upcoming';
            } elseif ($now->greaterThan($startTime) and $now->lessThan($endTime)) {
                return 'running';
            } elseif ($now->greaterThan($endTime) and $now->greaterThan($startTime)) {
                return 'expired';
            }
        }
        return null;
    }



    /**
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     */
    public function getEventStatusTextAttribute()
    {
        return ($status = $this->event_status) ? trans("ehdafront::general.event.type.$status") : '';
    }



    /**
     * @param null|int $number
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function similars($number = null)
    {
        $this->spreadMeta();

        $selectArray = [
             'type'       => $this->type, // similar post type
             'conditions' => [
                  ['id', '<>', $this->id],        // not self post
             ],
             'locale'     => $this->locale,
             'domain'     => EhdaFrontServiceProvider::usableDomains(),
        ];

        // similar categories
        $categories = $this->categories;
        if ($categories->count()) {
            $selectArray['category'] = $categories->pluck('slug')->toArray();
        }

        // check availability for "products"
        if ($this->has('price')) {
            $selectArray[] = ['is_available' => true];
        }

        if ($number and is_int($number)) {
            // sort
            //			$posts->orderBy('published_at', 'DESC');
            $selectArray['limit'] = $number;
        }

        if ($this->posttype->hasFeature('domains')) {
            $selectArray['domains'] = $this->domains->pluck('slug')->toArray();
        }

        return PostsServiceProvider::collectPosts($selectArray);
    }
}
