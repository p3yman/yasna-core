<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/9/18
 * Time: 1:10 PM
 */

namespace Modules\EhdaFront\Entities\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait EhdaFrontStateTrait
{
    /**
     * Relationship with domains table.
     *
     * @return BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Domain');
    }
}
