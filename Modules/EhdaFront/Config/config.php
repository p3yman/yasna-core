<?php

return [
     'name' => 'EhdaFront',

     'prefix' => [
          'routes' => [
               'post' => [
                    'short' => 'P',
               ],
          ],
     ],

     'default-domain' => 'tehran',
];
