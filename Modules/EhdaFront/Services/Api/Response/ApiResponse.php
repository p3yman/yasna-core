<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 04/03/2018
 * Time: 05:01 PM
 */

namespace Modules\EhdaFront\Services\Api\Response;

use Illuminate\Http\Exceptions\HttpResponseException;

class ApiResponse
{
    /**
     * Status of the Response
     *
     * @var integer
     */
    protected $status;

    /**
     * Other data that will be passed with response
     *
     * @var array
     */
    protected $data = [];

    /**
     * @var array All Possible Statuses
     */
    protected static $statusesSlugs = [

         'information-contact-success'   => 15,
         'information-about-success'     => 14,
         'details-news-success'          => 13,
         'list-news-success'             => 12,
         'password-reset-success'        => 11,
         'card-print-success'            => 10,
         'list-events-success'           => 9,
         'list-slideshow-success'        => 8,
         'list-configs-success'          => 7,
         'list-education-levels-success' => 6,
         'list-cities-success'           => 5,
         'list-provinces-success'        => 4,
         'card-process-success'          => 3,
         'card-exists'                   => 2,
         'token-created'                 => 1,

         'credentials-missed'                => 0,
         'credentials-invalid'               => -1,
         'permission-denied'                 => -2,
         'unacceptable-ip'                   => -3,
         'unable-to-create-token'            => -4,
         'token-and-code-melli-needed'       => -5,
         'token-invalid'                     => -6,
         'token-expired'                     => -7,
         'invalid-code-melli'                => -8,
         'user-has-no-card'                  => -9,
         'user-not-found'                    => -10,
         'card-get-invalid-parameters'       => -11,
         'birth-date-or-mobile-not-match'    => -12,
         'token-missed'                      => -13,
         'list-cities-province-missed'       => -14,
         'card-register-invalid-parameters'  => -15,
         'card-register-failed'              => -16,
         'list-configs-unavailable'          => -17,
         'list-slideshow-unavailable'        => -18,
         'list-education-levels-unavailable' => -19,
         'card-print-invalid-parameters'     => -20,
         'card-print-unable-to'              => -21,
         'list-events-unavailable'           => -22,
         'card-register-unable-to'           => -23,
         'card-edit-invalid-parameters'      => -24,
         'card-edit-unable-to'               => -25,
         'password-reset-invalid-parameters' => -26,
         'list-news-unavailable'             => -27,
         'details-news-invalid-news'         => -28,
         'information-about-unavailable'     => -29,
         'information-contact-unavailable'   => -30,

         'unknown-error' => -100,
    ];



    public function __construct($statusSlug)
    {
        $this->status($statusSlug);
    }



    /**
     * Set the value of response's status.
     *
     * @param integer $status
     *
     * @return $this
     */
    public function status($status)
    {
        $this->status = array_key_exists($status, self::$statusesSlugs) ? self::$statusesSlugs[$status] : $status;

        return $this;
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function appendData($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function append(...$parameters)
    {
        return $this->appendData(...$parameters);
    }



    /**
     * @param array $data
     *
     * @return $this
     */
    public function merge(array $data)
    {
        $this->data = array_merge($this->data, $data);

        return $this;
    }



    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge([
             'status' => $this->status,
        ], $this->data);
    }



    /**
     * Throw needed exception
     */
    public function throw()
    {
        throw new HttpResponseException(response()->json($this->toArray(), $this->failureHeaderStatus()));
    }



    /**
     * @return int
     */
    protected function failureHeaderStatus()
    {
        return 200;
    }



    /**
     * @param string $statusSlug
     *
     * @return $this
     */
    public static function create(...$parameters)
    {
        return new static(...$parameters);
    }



    /**
     * @param string $exceptionSlug
     */
    public static function throwException($exceptionSlug)
    {
        static::create($exceptionSlug)->throw();
    }
}
