<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 12:22 PM
 */

namespace Modules\EhdaFront\Services\Template;

use Nwidart\Modules\Facades\Module;

class EhdaFrontTemplateHandler extends TemplateHandler
{
    protected static $pageTitle          = [];
    protected static $siteTitle;
    protected static $stateLogo;
    protected static $pageTitleSeparator = ' | ';
    protected static $contactInfo        = [];
    protected static $socialLinks        = [];
    protected static $location;
    protected static $yasnaTitle;
    protected static $yasnaUrl;
    protected static $openGraphs         = [];
    protected static $showHeartIcon      = true;
    protected static $statesComboOptions;
    protected static $mainMenu;
    protected static $footerMenu;

    protected static $siteLogoUrl;
    protected static $miniSiteLogoUrl;

    protected static $moduleName = 'EhdaFront';



    public static function siteLogoUrl()
    {
        if (is_null(static::$siteLogoUrl)) {
            if (
                 ($settingLogo = get_setting('site_logo')) and
                 ($logoFile = doc($settingLogo)->getUrl())
            ) {
                static::$siteLogoUrl = $logoFile;
            } else {
                static::$siteLogoUrl = Module::asset(
                     strtolower(static::$moduleName)
                     . ":images/template/header-logo-"
                     . getLocale()
                     . ".png"
                );
            }
        }

        return static::$siteLogoUrl;
    }



    public static function siteLogo()
    {
        return static::siteLogoUrl();
    }



    public static function tinySiteLogoUrl()
    {
        if (is_null(static::$miniSiteLogoUrl)) {
            if (
                 ($settingLogo = get_setting('site_logo_tiny')) and
                 ($logoFile = doc($settingLogo)->getUrl())
            ) {
                static::$miniSiteLogoUrl = $logoFile;
            } else {
                static::$miniSiteLogoUrl = Module::asset(strtolower(static::$moduleName) . ":images/template/logo.png");
            }
        }

        return static::$miniSiteLogoUrl;
    }



    public static function tinySiteLogo()
    {
        return static::tinySiteLogoUrl();
    }
}
