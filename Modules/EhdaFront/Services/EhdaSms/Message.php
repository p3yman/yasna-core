<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/18/18
 * Time: 12:07 PM
 */

namespace Modules\EhdaFront\Services\EhdaSms;

use Modules\EhdaFront\Services\AsanakSms\Facade\AsanakSms;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;

class Message
{
    protected static $sms_providers = [
         'fanap',
         'asanak',
    ];



    /**
     * @param $to
     * @param $message
     *
     * @return bool
     */
    public static function send($to, $message)
    {
        foreach (static::$sms_providers as $provider) {
            $sending_response = static::sendMessageVia($provider, $to, $message);
            if ($sending_response) {
                return $sending_response;
                return true;
            }
        }

        return false;
    }



    /**
     * @param string $provider
     * @param string $to
     * @param string $message
     *
     * @return bool
     */
    protected static function sendMessageVia(string $provider, $to, $message)
    {
        $method = __FUNCTION__ . studly_case($provider);
        if (method_exists(static::class, $method)) {
            return static::$method($to, $message);
        }
        return false;
    }



    /**
     * @param string $to
     * @param string $message
     *
     * @return bool
     */
    protected static function sendMessageViaFanap($to, $message)
    {
        return FanapServiceProvider::sendMessage($to, $message);
    }



    /**
     * @param string $to
     * @param string $message
     *
     * @return bool|string
     */
    protected static function sendMessageViaAsanak($to, $message)
    {
        $asanak_response = AsanakSms::send($to, $message);
        if (
             $asanak_response and (
                  is_array($asanak_response) or
                  (
                       is_string($asanak_response) and
                       is_array($asanak_response_array = json_decode($asanak_response, true)) and
                       (count($asanak_response_array) == 1)
                  )
             )
        ) {
            return true;
        } else {
            return false;
        }
    }
}
