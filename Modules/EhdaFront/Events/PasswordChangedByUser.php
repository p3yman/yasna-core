<?php namespace Modules\EhdaFront\Events;

use Modules\Yasna\Services\YasnaEvent;

class PasswordChangedByUser extends YasnaEvent
{
    public $model_name = 'User';
}
