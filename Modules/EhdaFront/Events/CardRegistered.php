<?php

namespace Modules\EhdaFront\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Yasna\Services\YasnaEvent;

class CardRegistered extends YasnaEvent
{
    public $model_name = 'User';
}
