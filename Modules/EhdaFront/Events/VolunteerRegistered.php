<?php

namespace Modules\EhdaFront\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Yasna\Services\YasnaEvent;

class VolunteerRegistered extends YasnaEvent
{
    public $model_name = 'User';
}
