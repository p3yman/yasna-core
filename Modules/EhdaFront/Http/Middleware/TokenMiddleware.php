<?php

namespace Modules\EhdaFront\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\EhdaFront\Providers\Api\ApiIpServiceProvider;
use Modules\EhdaFront\Providers\ApiServiceProvider;
use Modules\EhdaFront\Services\Api\Response\ApiResponse;

class TokenMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($token = $request->token) {
            if ($row = model('api-token')->where('api_token', $token)->first()) {
                if (Carbon::now()->lessThan($row->expired_at)) {
                    if (($user = user($row->user_id))->hasRole(ApiServiceProvider::apiClientRoleSlug())) {
                        if (ApiIpServiceProvider::userIsPermittedWithIp($user)) {
                            Auth::login($user);
                            $request->replace($request->except('token'));
                            return $next($request);
                        } else {
                            $exceptionKey = 'unacceptable-ip';
                        }
                    } else {
                        $exceptionKey = 'token-invalid';
                    }
                } else {
                    $exceptionKey = 'token-expired';
                }
            } else {
                $exceptionKey = 'token-invalid';
            }
        } else {
            $exceptionKey = 'token-missed';
        }
        ApiResponse::create($exceptionKey)->throw();
    }
}
