<?php

namespace Modules\EhdaFront\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DonationPaymentRequest extends YasnaRequest
{
    protected $model_name = "";

    protected $minimum_amount = 2000;



    public function rules()
    {
        return [
             'mobile'     => 'phone:mobile',
             'code_melli' => 'code_melli',
             'ware_id'    => 'exists:wares,id',
             'amount'     => 'required|numeric|integer|min:' . $this->getMinimumAmount(),
        ];
    }



    public function purifier()
    {
        return [
             'mobile'     => 'ed',
             'code_melli' => 'ed',
             'amount'     => 'ed',
             'ware_id'    => 'dehash',
        ];
    }



    public function corrections()
    {
        $this->data['user_id'] = user()->id;
    }



    protected function getMinimumAmount()
    {
        return $this->minimum_amount;
    }



    public function messages()
    {
        return [
             'ware_id.exists' => trans('ehdafront::general.donation.invalid_type'),
        ];
    }



    public function attributes()
    {
        return [
             'ware_id' => trans('ehdafront::general.donation.type'),
        ];
    }
}
