<?php

namespace Modules\EhdaFront\Http\Requests;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class VolunteerRegisterFinalStepRequest extends YasnaRequest
{
    protected $model_name = "";
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (true and // @TODO: check for exam passed if needed
             (auth()->guest() or !user()->withDisabled()->is_admin()) // If isn't admin
        ) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
             'name_first'  => 'required|persian:60',
             'name_last'   => 'required|persian:60',
             'gender'      => 'required|numeric|min:1|max:3',
             'name_father' => 'required|persian:60',
             'code_id'     => 'required|numeric',
             // unique where not soft deleted
             'code_melli'  => [
                  'code_melli',
                  Rule::unique('users')
                      ->whereNull('deleted_at')// Not soft deleted
                      ->whereNotNull('id'), // Not logged in
             ],
             'birth_date'  => 'required|date|min:6|before_or_equal:'
                  . Carbon::now()->toDateString()
                  . '|after_or_equal:'
                  . Carbon::now()->subYears(100)->toDateString(),
             'birth_city'  => 'required|numeric|min:1',
             'marital'     => 'required|numeric|min:1|max:2',
             'email'       => 'email',

             'edu_level' => 'required|numeric|min:1|max:6',
             'edu_field' => 'required|persian:60',
             'edu_city'  => 'required|numeric|min:1',

             'mobile'        => 'required|phone:mobile',
             'tel_emergency' => 'required|phone:fixed',

             'home_city'        => 'required|numeric|min:1',
             'home_address'     => 'required|persian:60',
             'home_tel'         => 'required|phone:fixed|min:10',
             'home_postal_code' => 'digits:10',

             'job'              => 'required|persian:60',
             'work_city'        => 'numeric',
             'work_address'     => 'persian:60',
             'work_tel'         => 'phone:fixed',
             'work_postal_code' => 'digits:10',

             'familiarization' => 'required|numeric|min:1|max:4',
             'motivation'      => 'required|persian:60',
             'alloc_time'      => 'required',

             'activity' => 'array',
        ];

        if (auth()->guest()) {
            $rules['password'] = 'required|same:password2|min:8|max:50';
        }

        return $rules;
    }



    public function corrections()
    {
        if (auth()->guest()) {
            if (isset($this->data['id'])) {
                unset($this->data['id']);
            }
        } else {
            unset($this->data['code_melli']);
            $this->data['id'] = user()->id;
        }
    }



    public function purifier()
    {
        return [

             'name_first'  => 'pd',
             'name_last'   => 'pd',
             'gender'      => 'ed',
             'name_father' => 'pd',
             'code_id'     => 'ed',
             'birth_date'  => 'gDate',
             'birth_city'  => 'dehash',
             'marital'     => 'ed',
             'email'       => 'ed',

             'edu_level' => 'ed',
             'edu_field' => 'pd',
             'edu_city'  => 'dehash',

             'tel_mobile'    => 'ed',
             'tel_emergency' => 'ed',

             'home_city'        => 'dehash',
             'home_address'     => 'pd',
             'home_tel'         => 'ed',
             'home_postal_code' => 'ed',

             'job'              => 'pd',
             'work_city'        => 'dehash',
             'work_address'     => 'pd',
             'work_tel'         => 'ed',
             'work_postal_code' => 'ed',

             'familization' => 'ed',
             'motivation'   => 'pd',
             'alloc_time'   => 'pd',
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $home_state = model('state', $this->getData('home_city'));
        $work_state = model('state', $this->getData('work_city'));
        $domain     = $home_state->domain;

        $this->setData('home_province', $home_state->parent_id);
        $this->setData('work_province', $work_state->parent_id);
        $this->setData('domain', $domain);
        $this->setData('activities', implode(',', $this->getData('activity')));
        $this->setData('password', Hash::make($this->getData('password')));
        $this->setData('volunteer_registered_at', Carbon::now()->toDateTimeString());
    }
}
