<?php
/**
 * Created by PhpStorm.
 * User: yasna
 * Date: 5/19/18
 * Time: 4:48 PM
 */

namespace Modules\EhdaFront\Http\Requests;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Modules\EhdaFront\Providers\ToolsServiceProvider;
use Modules\Yasna\Services\YasnaRequest;
use Morilog\Jalali\jDateTime;

/**
 * Class PassargadRegisterRequest
 *
 * @package Modules\EhdaFront\Http\Requests
 */
class PassargadRegisterRequest extends YasnaRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
             'code_melli'  => 'required|code_melli|unique:users,code_melli',
             'mobile'      => 'required|phone:mobile',
             'birth_date'  => [
                  'required',
                  'date',
                  //'before:' . Carbon::now(),
                  //'after:' . $this->getMinimumAcceptableBirthDate(),
             ],
             'gender'      => [
                  'required',
                  Rule::in(ToolsServiceProvider::getActiveGendersCodes()),
             ],
             'name_first'  => 'required|persian:60',
             'name_last'   => 'required|persian:60',
             'name_father' => 'required|persian:60',
             'home_city'   => $this->cityValidationRule(),
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'code_melli'  => 'ed',
             'tel_mobile'  => 'ed',
             'birth_date'  => 'ed',
             'gender'      => 'ed',
             'name_first'  => 'pd',
             'name_last'   => 'pd',
             'name_father' => 'pd',
             'home_city'   => 'ed',
        ];
    }



    /**
     * @return \Illuminate\Validation\Rules\Exists
     */
    protected function cityValidationRule()
    {
        return Rule::exists('states', 'id')->where('parent_id', '!=', 0);
    }



    /**
     * @return \Illuminate\Validation\Rules\Exists
     */
    protected function provinceValidationRule()
    {
        return Rule::exists('states', 'id')->where('parent_id', 0);
    }



    /**
     * @return string
     */
    protected function getMinimumAcceptableBirthDate()
    {
        return jDateTime::createCarbonFromFormat('Y/m/d', '1300/01/01')->toDateTimeString();
    }



    /**
     * @return void
     */
    public function corrections()
    {
        $birth_date = $this->data['birth_date'];
        $birth_date = explode('/', $birth_date);
        $gregorian  = jDateTime::toGregorian($birth_date[0], $birth_date[1], $birth_date[2]);
        $gregorian  = Carbon::createFromDate($gregorian[0], $gregorian[1], $gregorian[2])
                            ->toDateString();

        $this->data['birth_date'] = $gregorian;
    }



    /**
     * @return array
     */
    public function messages()
    {
        return
             [
                  'code_melli.unique' => trans('ehdafront::passargad.code_melli.registered')
             ];
    }
}
