<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/23/18
 * Time: 3:33 PM
 */

namespace Modules\EhdaFront\Http\Requests;

class WorksCommentSaveRequest extends CommentSaveRequest
{
    /**
     * @return array
     */
    public function attributes()
    {
        return [
             'subject'      => trans('ehdafront::validation.attributes.submission_work_subject'),
             'name'         => trans('ehdafront::validation.attributes.submission_work_owner_name'),
             'mobile'       => trans('ehdafront::validation.attributes.submission_work_owner_mobile'),
             'email'        => trans('ehdafront::validation.attributes.submission_work_owner_email'),
             'description'  => trans('ehdafront::validation.attributes.description'),
             'text_content' => trans('ehdafront::validation.attributes.text_content'),
             '_files'        => trans('ehdafront::general.send_works.files'),
        ];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return array_merge(parent::messages(), [
             '_files.required' => trans('ehdafront::general.send_works.message.failure.files_required'),
        ]);
    }



    /**
     * @return array
     */
    public function rules()
    {
        return array_except(parent::rules(), ['text']);
    }
}
