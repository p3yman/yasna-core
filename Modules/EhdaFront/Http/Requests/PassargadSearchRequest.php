<?php
/**
 * Created by PhpStorm.
 * User: yasna
 * Date: 5/19/18
 * Time: 4:35 PM
 */

namespace Modules\EhdaFront\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

/**
 * Class PassargadSearchRequest
 *
 * @package Modules\EhdaFront\Http\Requests
 */
class PassargadSearchRequest extends YasnaRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
             'code_melli' => 'required|code_melli',
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'code_melli' => 'ed',
        ];
    }
}
