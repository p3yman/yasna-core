<?php

namespace Modules\EhdaFront\Http\Requests;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class FontPurchaseTrackRequest extends YasnaRequest
{
    protected $possible_carts = [];



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'tracking_number' => [
                  'required',
                  Rule::exists('transactions', 'tracking_no')
                      ->whereIn('invoice_id', $this->possible_carts),
             ],
             'email'           => 'required|email|exists:carts,email',
        ];
    }



    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
             'tracking_number' => trans('ehdafront::validation.attributes.tracking_number'),
        ];
    }



    /**
     * Find ids of carts with specified email.
     */
    public function corrections()
    {
        $this->possible_carts = model('cart')
             ->whereEmail($this->data['email'])
             ->pluck('id')
             ->toArray()
        ;
    }



    /**
     * Throw customized error.
     *
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
             'errors' => [
                  'tracking_number' => [
                       trans('ehdafront::cart.messages.tracking.invalid_information'),
                  ],
             ],
        ], 422));
    }
}
