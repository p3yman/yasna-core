<?php

namespace Modules\EhdaFront\Http\Requests;

use App\Models\Post;
use App\Models\Posttype;
use Modules\EhdaFront\Providers\CommentServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class CommentSaveRequest extends YasnaRequest
{
    protected $model_name = "";
    protected $post;
    protected $posttype;

    protected $generalRules = [
         'post_id'   => 'exists:posts,id',
         'parent_id' => 'exists:comments,id',
         'email'     => 'email',
         'mobile'    => 'phone:mobile',
         'text'      => 'required_without:message',
    ];



    /**
     * @return bool
     */
    public function authorize()
    {
        if ($this->post) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'post_id'   => 'dehash',
             'parent_id' => 'dehash',
             'mobile'    => 'ed',
        ];
    }



    /**
     * @return array
     */
    public function rules()
    {
        return array_merge_recursive($this->getPostRules(), $this->getPosttypeRules(), $this->getGeneralRules());
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             'text.required_without' => trans('validation.required'),
        ];
    }



    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post ?: new Post;
    }



    /**
     * @return Posttype
     */
    public function getPosttype()
    {
        if (!$this->posttype) {
            $this->posttype = $this->getPost()->posttype;
        }

        return $this->posttype;
    }



    /**
     * @return array
     */
    protected function getPostRules()
    {
        if (
             array_key_exists('post_id', $data = $this->data) and
             ($post = model('post')->grab($data['post_id'])) and
             $post->exists
        ) {
            $this->post = $post->spreadMeta();
            return self::safeRulesArray(CommentServiceProvider::translateRules($this->post->rules));
        }

        return [];
    }



    /**
     * @return array
     */
    protected function getPosttypeRules()
    {
        return self::safeRulesArray(CommentServiceProvider::posttypeRules($this->getPosttype()));
    }



    /**
     * @return array
     */
    protected function getGeneralRules()
    {
        return self::safeRulesArray($this->generalRules);
    }



    /**
     * @param array $rules
     *
     * @return array
     */
    protected static function safeRulesArray(array $rules)
    {
        return array_map(function ($item) {
            return is_string($item) ? explode('|', $item) : $item;
        }, $rules);
    }
}
