<?php


namespace Modules\EhdaFront\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

/**
 * Class PassargadLoginRequest
 *
 * @package Modules\EhdaFront\Http\Requests
 */
class PassargadLoginRequest extends YasnaRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return
             [
                  'code_melli' => 'required|exists:users,code_melli',
                  'password'   => 'required|string',
             ];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return
             [
                  'code_melli.exists' => trans('ehdafront::passargad.username_not_valid'),
             ];
    }
}
