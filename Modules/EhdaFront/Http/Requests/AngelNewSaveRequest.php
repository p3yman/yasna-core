<?php

namespace Modules\EhdaFront\Http\Requests;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\Rule;
use Modules\Posts\Http\Requests\PostSaveRequest;

class AngelNewSaveRequest extends PostSaveRequest
{

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * @return array
     */
    public function rules()
    {
        $rules                           = parent::rules();
        $rules['donor_name_and_surname'] = $rules['title'];
        unset($rules['title']);
        return array_merge($rules, $this->customizedRules(), $this->fileRules(), $this->cityRules());
    }



    /**
     * @return array
     */
    protected function customizedRules()
    {
        return [
             'donation_date'              => 'required',
             'submitter_name_and_surname' => 'required',
             'hospital_name'              => 'required',
        ];
    }



    /**
     * @return array
     */
    protected function fileRules()
    {
        return [
             'featured_image' => 'required',
        ];
    }



    /**
     * @return array
     */
    protected function cityRules()
    {
        return [
             'city_and_province' => [
                  'required',
                  Rule::exists('states', 'id')
                      ->whereNot('parent_id', 0),
             ],
        ];
    }



    /**
     * Make data ready to be saved
     */
    public function corrections()
    {
        $this->data['type']       = 'angels';
        $this->_posttype          = model('posttype', $this->data['type']);
        $this->data['locale']     = getLocale();
        $this->data['slug']       = '';
        $this->data['sisterhood'] = hashid(time(), 'main') . str_random(5);
        $this->fetchFeaturedImage();
        $this->enrichNewModels();
    }



    /**
     * Fetch featured image hashid
     */
    protected function fetchFeaturedImage()
    {
        if (
             array_key_exists('_donor_image', $this->data) and
             ($imagesJson = $this->data['_donor_image']) and
             (
                  is_array($array = $imagesJson) or
                  is_array($array = json_decode($imagesJson, true))
             )
        ) {
            $this->data['featured_image'] = $array[0];
        }
    }



    /**
     * Enrich New Model
     */
    protected function enrichNewModels()
    {
        $this->model->type   = $this->_posttype->slug;
        $this->model->locale = $this->data['locale'];
    }



    /**
     * @return array
     */
    public function attributes()
    {
        return [
             'donor_name_and_surname'     => trans('ehdafront::validation.attributes.donor_name_and_surname'),
             'donation_date'              => trans('ehdafront::validation.attributes.donation_date'),
             'city_and_province'          => trans('ehdafront::validation.attributes.city_and_province'),
             'submitter_name_and_surname' => trans('ehdafront::validation.attributes.submitter_name_and_surname'),
             'hospital_name'              => trans('ehdafront::validation.attributes.hospital_name'),
        ];
    }



    /**
     * @return array
     */
    public function messages()
    {
        return [
             'featured_image.required' => trans('ehdafront::general.message.error.upload_one_image'),
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return array_except(parent::purifier(), 'type');
    }



    /**
     * @return array
     */
    public function toArray()
    {
        return $this->except(['model']);
    }
}
