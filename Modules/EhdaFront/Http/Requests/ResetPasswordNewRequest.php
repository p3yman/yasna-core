<?php

namespace Modules\EhdaFront\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Modules\Yasna\Services\YasnaRequest;

class ResetPasswordNewRequest extends YasnaRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'new_password' => 'required|same:new_password2|min:8|max:50|',
        ];
    }
}
