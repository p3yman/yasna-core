<?php

namespace Modules\EhdaFront\Http\Requests;

use Modules\EhdaFront\Http\Controllers\Auth\ForgotPasswordController;
use Modules\Yasna\Services\YasnaRequest;

class ResetPasswordTokenRequest extends YasnaRequest
{
    protected $model_name = "";
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_melli'           => 'required_if:direct_request,true|code_melli|exists:users,code_melli',
            'password_reset_token' => 'required|numeric',
            'g-recaptcha-response' => !env('APP_DEBUG') ? 'required|captcha' : '',
        ];
    }

    public function corrections()
    {
        if (session()->get($sessionName = ForgotPasswordController::getResettingSessionName())) {
            $this->data['code_melli'] = session()->get($sessionName);
        } else {
            $this->data['direct_request'] = true;
        }
    }

    public function purifier()
    {
        return [
            'code_melli'           => 'ed',
            'password_reset_token' => 'ed',
        ];
    }
}
