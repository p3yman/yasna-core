<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;

class CardSearchRequest extends ApiRequestAbstract
{
    protected $model_name = "";

    public function rules()
    {
        return [
            'code_melli' => 'required|code_melli',
        ];
    }

    public function purifier()
    {
        return [
            'code_melli' => 'ed',
        ];
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        $failed = $validator->failed();
        if (isset($failed['code_melli']['Required'])) {
            return 'token-and-code-melli-needed';
        } else {
            return parent::getErrorStatusSlug($validator);
        }
    }

    protected function acceptableFields()
    {
        return ['code_melli'];
    }
}
