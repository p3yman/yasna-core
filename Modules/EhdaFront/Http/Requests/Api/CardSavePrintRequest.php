<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;

class CardSavePrintRequest extends ApiRequestAbstract
{
    public function rules()
    {
        return [
            'code_melli' => 'required|code_melli',
            'event'      => [$this->eventValidationRule()],
        ];
    }

    public function purifier()
    {
        return [
            'code_melli' => 'ed',
            'event'      => 'dehash',
        ];
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        $failed = $validator->failed();

        if (isset($failed['code_melli']['CodeMelli'])) {
            return 'invalid-code-melli';
        }

        return 'card-print-invalid-parameters';
    }

    protected function acceptableFields()
    {
        return ['code_melli', 'event'];
    }
}
