<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;

class CardGetRequest extends ApiRequestAbstract
{
    protected $model_name = "";

    public function rules()
    {
        return [
            'code_melli' => 'required|code_melli',
            'birth_date' => 'required|date',
            'mobile'     => 'phone:mobile',
        ];
    }

    public function corrections()
    {
        $this->timeToDateString('birth_date')
            ->changeDataKey('tel_mobile', 'mobile');
    }

    public function purifier()
    {
        return [
            'code_melli' => 'ed',
            'birth_date' => 'ed',
            'tel_mobile' => 'ed',
        ];
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        $failed = $validator->failed();

        if (isset($failed['code_melli']['CodeMelli'])) {
            return 'invalid-code-melli';
        }

        return 'card-get-invalid-parameters';
    }

    protected function acceptableFields()
    {
        return ['code_melli', 'birth_date', 'mobile'];
    }
}
