<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/29/18
 * Time: 4:03 PM
 */

namespace Modules\EhdaFront\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Modules\EhdaFront\Providers\ApiServiceProvider;

class ListNewsDetailsRequest extends ApiRequestAbstract
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
             'news_id' => [
                  'required',
                  'integer',
                  'min:1',
                  Rule::in([$this->model->id]),
             ],
        ];
    }



    /**
     * Correct data to be passed to controller
     */
    public function corrections()
    {
        $this->model = model('post')
             ->elector([
                  'type'     => ApiServiceProvider::POSTTYPES_SLUGS['news'],
                  'category' => ApiServiceProvider::CATEGORIES_SLUGS['news']['broadcast'],
                  'id'       => intval($this->data['news_id']) ?? 0,
             ])->first()
             ?: model('post');
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'news_id' => 'dehash',
        ];
    }



    /**
     * @param Validator $validator
     *
     * @return string
     */
    protected function getErrorStatusSlug(Validator $validator)
    {
        return 'details-news-invalid-news';
    }
}
