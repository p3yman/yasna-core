<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Modules\EhdaFront\Providers\ToolsServiceProvider;

class CardEditRequest extends ApiRequestAbstract
{
    public function rules()
    {
        return [
            'code_melli'         => 'required|code_melli',
            'current_birth_date' => [
                'required',
                'date',
            ],
            'birth_date'         => [
                'date',
                'before:' . Carbon::now(),
                'after:' . $this->getMinimumAcceptableBirthDate(),
            ],
            'mobile'             => 'phone:mobile',
            'gender'             => [
                'required',
                Rule::in(ToolsServiceProvider::getActiveGendersCodes()),
            ],
            'name_first'         => 'persian:60',
            'name_last'          => 'persian:60',
            'name_father'        => 'persian:60',
            'code_id'            => 'numeric',
            'birth_city'         => $this->cityValidationRule(),
            'edu_level'          => [
                Rule::in(ToolsServiceProvider::activeEduLevelsCodes()),
            ],
            'home_city'          => $this->cityValidationRule(),
        ];
    }

    public function corrections()
    {
        $this->changeDataKey('birth_date', 'current_birth_date')
            ->changeDataKey('new_birth_date', 'birth_date')
            ->timeToString('birth_date')
            ->timeToString('current_birth_date')
            ->changeDataKey('tel_mobile', 'mobile');
    }

    public function purifier()
    {
        return [
            'code_melli'     => 'ed',
            'tel_mobile'     => 'ed',
            'birth_date'     => 'ed',
            'new_birth_date' => 'ed',
            'gender'         => 'ed',
            'name_first'     => 'pd',
            'name_last'      => 'pd',
            'name_father'    => 'pd',
            'code_id'        => 'ed',
            'birth_city'     => 'ed',
            'edu_level'      => 'ed',
            'home_city'      => 'ed',
        ];
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        $failed = $validator->failed();
        if (isset($failed['code_melli']['CodeMelli'])) {
            return 'invalid-code-melli';
        }

        return 'card-edit-invalid-parameters';
    }

    protected function acceptableFields()
    {
        return [
            'code_melli',
            'mobile',
            'birth_date',
            'current_birth_date',
            'gender',
            'name_first',
            'name_last',
            'name_father',
            'code_id',
            'birth_city',
            'edu_level',
            'home_city',
        ];
    }
}
