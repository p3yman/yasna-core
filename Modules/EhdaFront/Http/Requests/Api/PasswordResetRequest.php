<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;

class PasswordResetRequest extends ApiRequestAbstract
{
    public function rules()
    {
        return [
            'code_melli'      => 'required|code_melli',
            'birth_date'      => 'required|date',
            'return_password' => 'boolean',
        ];
    }

    public function corrections()
    {
        $this->timeToString('birth_date');
    }

    public function purifier()
    {
        return [
            'code_melli' => 'ed',
            'birth_date' => 'ed',
        ];
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        $failed = $validator->failed();
        if (isset($failed['code_melli']['CodeMelli'])) {
            return 'invalid-code-melli';
        }

        return 'password-reset-invalid-parameters';
    }

    protected function acceptableFields()
    {
        return ['code_melli', 'birth_date', 'return_password'];
    }
}
