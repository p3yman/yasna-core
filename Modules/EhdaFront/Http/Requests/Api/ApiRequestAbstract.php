<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 04/03/2018
 * Time: 04:16 PM
 */

namespace Modules\EhdaFront\Http\Requests\Api;

use App\Models\ApiToken;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Modules\EhdaFront\Services\Api\Response\ApiResponse;
use Modules\Yasna\Services\YasnaRequest;
use Morilog\Jalali\Facades\jDateTime;

class ApiRequestAbstract extends YasnaRequest
{
    protected function failedValidation(Validator $validator)
    {
        ApiResponse::create($this->getErrorStatusSlug($validator))->throw();
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        $failed = $validator->failed();
        if (isset($failed['code_melli']['CodeMelli'])) {
            return 'invalid-code-melli';
        }

        return 'unknown-error';
    }

    public function all($key = null)
    {
        return method_exists($this, 'acceptableFields')
            ? array_only(parent::all($key), $this->acceptableFields())
            : parent::all($key);
    }

    protected function timeToString($fieldKey)
    {
        if (array_key_exists($fieldKey, $this->data)) {
            if (intval($this->data[$fieldKey]) == $this->data[$fieldKey]) {
                $this->data[$fieldKey] = Carbon::createFromTimestamp($this->data[$fieldKey])->toDateTimeString();
            } else {
                $this->data[$fieldKey] = '';
            }
        }

        return $this;
    }



    /**
     * @param $fieldKey
     *
     * @return $this
     */
    protected function timeToDateString($fieldKey)
    {
        if (array_key_exists($fieldKey, $this->data)) {
            if (intval($this->data[$fieldKey]) == $this->data[$fieldKey]) {
                $this->data[$fieldKey] = Carbon::createFromTimestamp($this->data[$fieldKey])->toDateString();
            } else {
                $this->data[$fieldKey] = '';
            }
        }

        return $this;
    }

    protected function changeDataKey($fromKey, $toKey)
    {
        if (array_key_exists($fromKey, $this->data)) {
            $this->data[$toKey] = $this->data[$fromKey];
            unset($this->data[$fromKey]);
        }

        return $this;
    }

    protected function eventValidationRule()
    {
        return Rule::exists('posts', 'id')
                   ->where('type', 'event')
                   ->whereNull('deleted_at')
                   ->whereNotNull('published_at')
                   //->where('posts.published_by', '>', "0")
                   ->where(function ($q) {
                       $q->where('published_at', '<=', Carbon::now()->toDateTimeString())
                         ->where('event_starts_at', '<=', Carbon::now())
                         ->where('event_ends_at', '>=', Carbon::now())
                       ;
                   })
             ;
    }

    protected function cityValidationRule()
    {
        return Rule::exists('states', 'id')->where('parent_id', '!=', 0);
    }

    protected function provinceValidationRule()
    {
        return Rule::exists('states', 'id')->where('parent_id', 0);
    }

    protected function getMinimumAcceptableBirthDate()
    {
        return jDateTime::createCarbonFromFormat('Y/m/d', '1300/01/01')->toDateTimeString();
    }
}
