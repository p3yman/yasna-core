<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;

class ListCitiesRequest extends ApiRequestAbstract
{
    protected $model_name = "";

    public function rules()
    {
        return [
            'province' => ['required', $this->provinceValidationRule()],
        ];
    }

    public function purifier()
    {
        return ['province' => 'ed'];
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        return 'list-cities-province-missed';
    }
}
