<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;

class GetTokenRequest extends ApiRequestAbstract
{
    protected $model_name = "";

    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required',
        ];
    }

    public function purifier()
    {
        return [
            'username' => 'ed',
            'password' => 'ed',
        ];
    }

    protected function getErrorStatusSlug(Validator $validator)
    {
        return 'credentials-missed';
    }

    protected function acceptableFields()
    {
        return ['username', 'password'];
    }
}
