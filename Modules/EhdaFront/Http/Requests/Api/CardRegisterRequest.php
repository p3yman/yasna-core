<?php

namespace Modules\EhdaFront\Http\Requests\Api;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Rule;
use Modules\EhdaFront\Providers\ToolsServiceProvider;

class CardRegisterRequest extends ApiRequestAbstract
{
    protected $producer;



    /**
     * @return array
     */
    public function rules()
    {
        return [
             'code_melli'          => 'required|code_melli',
             'mobile'              => 'required|phone:mobile',
             'birth_date'          => [
                  'required',
                  'date',
                  'before:' . Carbon::now(),
                  'after:' . $this->getMinimumAcceptableBirthDate(),
             ],
             'gender'              => [
                  'required',
                  Rule::in(ToolsServiceProvider::getActiveGendersCodes()),
             ],
             'name_first'          => 'required|persian:60',
             'name_last'           => 'required|persian:60',
             'name_father'         => 'required|persian:60',
             'code_id'             => 'numeric',
             'birth_city'          => $this->cityValidationRule(),
             'edu_level'           => [
                  Rule::in(ToolsServiceProvider::activeEduLevelsCodes()),
             ],
             'home_city'           => $this->cityValidationRule(),
             'from_event_id'       => [$this->eventValidationRule()],
             'producer_code_melli' => [
                  'code_melli',
                  Rule::in([$this->producer->code_melli]),
             ],
        ];
    }



    /**
     * @return void
     */
    protected function findProducerModel()
    {
        if ($this->producer) {
            return;
        }

        $this->producer      = model('user');
        $producer_code_melli = $this->get('producer_code_melli');

        if (!$producer_code_melli) {
            return;
        }

        $producer_model = model('user')
             ->whereCodeMelli($producer_code_melli)
             ->first()
        ;

        if ($producer_model) {
            $this->producer = $producer_model;
        }
    }



    /**
     * Correct Data
     */
    public function corrections()
    {
        $this->timeToDateString('birth_date')
             ->changeDataKey('tel_mobile', 'mobile')
             ->changeDataKey('event', 'from_event_id')
        ;

        $this->findProducerModel();
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'code_melli'          => 'ed',
             'tel_mobile'          => 'ed',
             'birth_date'          => 'ed',
             'gender'              => 'ed',
             'name_first'          => 'pd',
             'name_last'           => 'pd',
             'name_father'         => 'pd',
             'code_id'             => 'ed',
             'birth_city'          => 'ed',
             'edu_level'           => 'ed',
             'home_city'           => 'ed',
             'event'               => 'dehash',
             'producer_code_melli' => 'ed',
        ];
    }



    /**
     * @param Validator $validator
     *
     * @return string
     */
    protected function getErrorStatusSlug(Validator $validator)
    {
        $failed = $validator->failed();

        if (isset($failed['code_melli']['CodeMelli'])) {
            return 'invalid-code-melli';
        }

        return 'card-register-invalid-parameters';
    }



    /**
     * @return array
     */
    protected function acceptableFields()
    {
        return [
             'code_melli',
             'mobile',
             'birth_date',
             'gender',
             'name_first',
             'name_last',
             'name_father',
             'code_id',
             'birth_city',
             'edu_level',
             'home_city',
             'from_event_id',
        ];
    }



    /**
     * @return User
     */
    public function getProducer()
    {
        return $this->producer;
    }
}
