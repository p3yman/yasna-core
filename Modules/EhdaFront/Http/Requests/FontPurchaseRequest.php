<?php

namespace Modules\EhdaFront\Http\Requests;

use Modules\EhdaFront\Providers\CurrencyServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class FontPurchaseRequest extends YasnaRequest
{
    protected $model_name = "ware";



    public function authorize()
    {
        return $this->model->exists;
    }



    public function rules()
    {
        return [
             'id'     => 'exists:wares,id',
             'name'   => 'required|persian:60',
             'email'  => 'required|email',
             'mobile' => 'required|phone:mobile',
             'price'  => [
                  'required',
                  'numeric',
                  'min:' . CurrencyServiceProvider::convertToPreviewCurrency(($this->model->price()->get())),
             ],
        ];
    }



    public function purifier()
    {
        return [
             'id'     => 'dehash',
             'name'   => 'pd',
             'mobile' => 'ed',
        ];
    }
}
