<?php

namespace Modules\EhdaFront\Http\Requests;

use Carbon\Carbon;
use Modules\Yasna\Services\YasnaRequest;

class ProfileSaveRequest extends YasnaRequest
{
    protected $model_name = "";
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return user()->exists;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_first'  => 'required|persian:60',
            'name_last'   => 'required|persian:60',
            'gender'      => 'required|numeric|min:1|max:3',
            'name_father' => 'required|persian:60',
            'code_id'     => 'numeric',
            'birth_date'  => 'required|date|min:6|before_or_equal:'
                . Carbon::now()->toDateString()
                . '|after_or_equal:'
                . Carbon::now()->subYears(100)->toDateString(),
            'birth_city'  => 'numeric|min:1',
            'edu_level'   => 'numeric|min:1|max:6',
            'job'         => 'persian:60',
            'mobile'      => 'required|phone:mobile',
            'home_tel'    => 'phone:fixed',
            'home_city'   => 'required|numeric|min:1',
            'email'       => 'email',
            'password'    => 'same:password2|min:8|max:50|',
            'password2'   => 'required_with:password',
//            'chRegisterAll' => 'required_without_all:chRegisterHeart,chRegisterLung,chRegisterLiver,chRegisterKidney,chRegisterPancreas,chRegisterTissues'

        ];
    }

    public function purifier()
    {
        return [
            'name_first'    => 'pd',
            'name_last'     => 'pd',
            'name_father'   => 'pd',
            'birth_date'    => 'gDate',
            'birth_city'    => 'dehash',
            'marriage_date' => 'gDate',
            'home_city'     => 'dehash',
            'mobile'        => 'ed',
            'password'      => 'ed',
            'password2'     => 'ed',
        ];
    }
}
