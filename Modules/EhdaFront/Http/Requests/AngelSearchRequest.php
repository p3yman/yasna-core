<?php

namespace Modules\EhdaFront\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class AngelSearchRequest extends YasnaRequest
{
    protected $model_name = "";



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'angel_name' => 'required|persian:60',
        ];
    }
}
