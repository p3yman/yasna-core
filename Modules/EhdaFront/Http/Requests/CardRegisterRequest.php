<?php

namespace Modules\EhdaFront\Http\Requests;

use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class CardRegisterRequest extends YasnaRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->stepIsValid();
    }



    /**
     * @return bool
     */
    protected function stepIsValid()
    {
        return $this->_step and $this->_step >= 1 and $this->_step <= 3;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->_step) {
            case 1: // Step 1
                return [
                     'name_first' => 'required|persian:60',
                     'name_last'  => 'required|persian:60',
                     'code_melli' => 'required|code_melli',
                     //             'security' => 'required|captcha:'.$input['key'], @TODO: new human validation
                ];
                break;
            case 2: // Step 2
                return [
                     'code_melli'  => 'required|code_melli',
                     'name_first'  => 'required|persian:60',
                     'name_last'   => 'required|persian:60',
                     'gender'      => 'required|numeric|min:1|max:3',
                     'name_father' => 'required|persian:60',
                     //                    'code_id'     => 'required|numeric',
                     'birth_date'  => 'required|date|min:6|before_or_equal:'
                          . Carbon::now()->toDateString()
                          . '|after_or_equal:'
                          . Carbon::now()->subYears(100)->toDateString(),
                     'birth_city'  => [
                          'numeric',
                          Rule::exists('states', 'id')
                              ->whereNot('parent_id', 0),
                     ],
                     'mobile'      => 'required|phone:mobile',
                     'home_tel'    => 'phone:fixed',
                     'home_city'   => 'required|numeric|min:1',
                     'email'       => 'email',
                     'password'    => 'required|same:password2|min:8|max:50',
                     //            'chRegisterAll' => 'required_without_all:chRegisterHeart,chRegisterLung,chRegisterLiver,chRegisterKidney,chRegisterPancreas,chRegisterTissues'

                ];
                break;
            case 3:
                return [
                     'code_melli' => 'required|code_melli',
                ];
                break;

            default:
                return [];
        }
    }



    /**
     * @return array
     */
    public function purifier()
    {
        switch ($this->data['_step']) {
            case 1:
                return [
                     'security'   => 'ed',
                     'code_melli' => 'ed',
                     'name_first' => 'pd',
                     'name_last'  => 'pd',
                ];

            case 2:
                return [
                     'code_melli'  => 'ed',
                     'mobile'      => 'ed',
                     'name_first'  => 'pd',
                     'name_last'   => 'pd',
                     'gender'      => 'ed',
                     'name_father' => 'pd',
                     'code_id'     => 'ed',
                     'birth_date'  => 'gDate',
                     'birth_city'  => 'dehash',
                     'edu_level'   => 'ed',
                     'job'         => 'pd',
                     'tel_mobile'  => 'ed',
                     'home_tel'    => 'ed',
                     'home_city'   => 'dehash',
                     'email'       => 'ed',
                     'password'    => 'ed',
                     'password2'   => 'ed',
                ];

            case 3:
                return [
                     'code_melli' => 'ed',
                     'db-check'   => 'decrypt',
                ];

            default:
                return [];
        }
    }



    /**
     * Correct Data
     */
    public function corrections()
    {
        if (array_key_exists('birth_city', $this->data) and !$this->data['birth_city']) {
            unset($this->data['birth_city']);
        }
    }
}
