<?php

namespace Modules\EhdaFront\Http\Requests\Manage;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class UserSaveRequest extends YasnaRequest
{
    protected $model_name = "User";

    public function rules()
    {
        return [
            'name_first' => 'required',
            'code_melli' => [
                'required',
                "unique:users,code_melli,$this->id,id",
                "alpha_num_dash",
                "max:20",
            ],
            'password'   => 'required_without:hashid',
            'home_city'  => [
                'required',
                Rule::exists('states', 'id')->where('parent_id', '!=', 0),
            ],
        ];
    }

    public function purifier()
    {
        return ['code_melli' => 'ed'];
    }

    public function attributes()
    {
        return [
            'code_melli' => trans('validation.attributes.username'),
            'home_city'  => trans('validation.attributes.city'),
        ];
    }

    public function messages()
    {
        return [
            'password.required_without' => trans('ehdafront::api.manage.alert.error.password-needed-for-new-client'),
        ];
    }
}
