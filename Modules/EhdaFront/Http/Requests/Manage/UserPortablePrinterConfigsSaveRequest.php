<?php

namespace Modules\EhdaFront\Http\Requests\Manage;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class UserPortablePrinterConfigsSaveRequest extends YasnaRequest
{
    protected $model_name = 'User';

    public function rules()
    {
        return [
            'portable_printer_configs' => 'json',
        ];
    }

    public function attributes()
    {
        return [
            'portable_printer_configs' => trans("ehdafront::api.manage.headline.portable-printer-configs"),
        ];
    }

    public function purifier()
    {
        return [
            'user_id' => 'dehash',
        ];
    }
}
