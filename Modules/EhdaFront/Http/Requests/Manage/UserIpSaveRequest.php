<?php

namespace Modules\EhdaFront\Http\Requests\Manage;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class UserIpSaveRequest extends YasnaRequest
{
    public function rules()
    {
        return [
            'slug' => 'required|ip',
        ];
    }

    public function attributes()
    {
        return [
            'slug' => trans('ehdafront::api.manage.headline.ip'),
        ];
    }

    public function purifier()
    {
        return [
            'user_id' => 'dehash',
        ];
    }
}
