<?php

namespace Modules\EhdaFront\Http\Requests;

use Modules\EhdaFront\Http\Controllers\Auth\ForgotPasswordController;
use Modules\Yasna\Services\YasnaRequest;

class ResetPasswordRequest extends YasnaRequest
{
    protected $model_name = "";
    //Feel free to define purifier(), rules(), authorize() and messages() methods, as appropriate.



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'code_melli'           => 'required|code_melli|exists:users,code_melli',
             'type'                 => 'required|in:' . implode(',', ForgotPasswordController::RECOVERY_METHODS),
             'email'                => 'required_if:type,email|email',
             'mobile'               => 'required_if:type,mobile|phone:mobile',
             'g-recaptcha-response' => !env('APP_DEBUG') ? 'required|captcha' : '',
        ];
    }



    /**
     * @return array
     */
    public function purifier()
    {
        return [
             'code_melli' => 'ed',
             'mobile'     => 'ed',
             'email'      => 'ed',
        ];
    }



    /**
     * Correct Data
     */
    public function corrections()
    {
        if (count(ForgotPasswordController::RECOVERY_METHODS) == 1) {
            $this->data['type'] = array_first(ForgotPasswordController::RECOVERY_METHODS);
        }
    }
}
