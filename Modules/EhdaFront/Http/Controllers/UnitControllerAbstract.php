<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/8/18
 * Time: 1:01 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\EhdaFront\Providers\PostsServiceProvider;

abstract class UnitControllerAbstract extends EhdaFrontControllerAbstract
{
    public function contact()
    {
        return $this
             ->findContactStaticPost()
             ->findContactCommentingPost()
             ->appendToVariables('unit', $this->unit)
             ->template('unit-contact.main', $this->variables)
             ;
    }



    protected function findContactStaticPost()
    {
        return $this->appendToVariables('infoPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'contact-' . $this->unit,
        ])->first() ?: new Post);
    }



    protected function findContactCommentingPost()
    {
        return $this->appendToVariables('commentingPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['commenting'],
             'slug' => 'contact-' . $this->unit,
        ])->first() ?: new Post);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resources(Request $request)
    {
        $request->merge(['postType' => $this->posttype_slugs[$this->unit . '-resources']]);
        return app(PostController::class)
             ->callAction('appendToInnerVariables', ['folders', $this->resourcesSideFolder($request)])
             ->callAction('appendToInnerVariables', ['content_categories', $this->resourcesContentCategories($request)])
             ->callAction('appendToListOptions', ['feature', false])
             ->callAction('appendToListOptions', ['is_base_page', true])
             ->callAction('appendToInnerVariables', [
                  'filterUrl',
                  action_locale('\\' . static::class . '@resourcesFilter'),
             ])
             ->archive($request)
             ;
    }



    /**
     * Return the slug of the content categories' folder.
     *
     * @return string
     */
    protected function resourcesContentCategoriesSlug()
    {
        return 'content-categories';
    }



    /**
     * Find the folders to show their categories in the filter box.
     *
     * @param Request $request
     *
     * @return Collection
     */
    protected function resourcesSideFolder(Request $request)
    {
        return posttype($request->postType)
             ->folders()
             ->where('slug', '<>', $this->resourcesContentCategoriesSlug())
             ->get()
             ;
    }



    /**
     * Find the categories to be shown in top
     *
     * @param Request $request
     *
     * @return Collection
     */
    protected function resourcesContentCategories(Request $request)
    {
        return posttype($request->postType)
             ->folders()
             ->where('slug', $this->resourcesContentCategoriesSlug())
             ->get()
             ->pluck('children')
             ->flatten()
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function resourcesFilter(Request $request)
    {
        $request->merge(['postType' => $this->posttype_slugs[$this->unit . '-resources']]);
        return app(PostController::class)
             ->callAction('appendToListOptions', ['feature', false])
             ->callAction('galleryFilter', [$request])
             ;
    }
}
