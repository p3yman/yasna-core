<?php

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Http\Requests\ProfileSaveRequest;

class UserController extends EhdaFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->selectDashboardInfoPost()
                    ->hideHeartIcon()
                    ->template('user.dashboard.main', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function selectDashboardInfoPost()
    {
        $this->variables['profilePost'] = Post::selector([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'my-card-detail',
        ])->first()
        ;
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function profile()
    {
        // If user is volunteer should proceed to edit profile from admin panel (not here)
        if (user()->is_admin()) {
            return redirect('manage/account');
        }

        user()->spreadMeta();

        return $this->selectStatesCombo()
                    ->template('ehdafront::user.profile.edit.main')
             ;
    }



    /**
     * @param ProfileSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProfileSaveRequest $request)
    {
        // If user is volunteer should proceed to edit profile from admin panel (not here)
        if (user()->is_admin()) {
            return $this->jsonFeedback('', [
                 'redirect' => url('manage/account/profile'),
            ]);
        }

        $request = $request->all();
        if ($request['new_password']) {
            $request['password'] = Hash::make($request['new_password']);
        }
        $request['id'] = user()->id;

        if (isset($request['code_melli'])) {
            // If the form has been manipulated
            // Because we've set "code_melli" disabled in the form and it should't have been posted
            unset($request['code_melli']);
        }

        $ok = user()->batchSaveBoolean($request, ['new_password', 'new_password2']);
        return $this->jsonAjaxSaveFeedback($ok, [
             'success_refresh' => 1,
        ]);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function shortCard(Request $request)
    {
        return (
             ($user = user()->grabHashid($request->identifier))->exists and
             $user->hasRole('card-holder')
        ) ? redirect($user->cards())
             : $this->abort(404);
    }
}
