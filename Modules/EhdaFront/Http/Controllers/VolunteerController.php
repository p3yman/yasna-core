<?php

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Activity;
use App\Models\Post;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Events\VolunteerRegistered;
use Modules\EhdaFront\Http\Requests\VolunteerRegisterFinalStepRequest;
use Modules\EhdaFront\Http\Requests\VolunteerRegisterFirstStepRequest;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;

class VolunteerController extends EhdaFrontControllerAbstract
{
    private $registerSessionName           = 'register_volunteer';
    private $currentRegisteringSessionName = 'current_registering';



    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        return $this->findInformationPost()
                    ->findVolunteerActivities()
                    ->indexResult()
             ;
    }



    /**
     * @return $this
     */
    protected function findInformationPost()
    {
        $this->variables['post'] = PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'volunteers-detail',
        ])->first()
        ;
        return $this;
    }



    /**
     * @return $this
     */
    protected function findVolunteerActivities()
    {
        $this->appendToVariables('activities', PostsServiceProvider::collectPosts([
             'type'       => 'volunteers-activities',
             'pagination' => false,
             'limit'      => 12,
        ]));
        return $this;
    }



    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function indexResult()
    {
        return ($this->variables['post'] and $this->variables['post']->exists)
             ? $this->template('volunteers.volunteers_info.main', $this->variables)
             : redirect(RouteServiceProvider::actionLocale('EhdaFrontController@index'));
    }



    /**
     * @param VolunteerRegisterFirstStepRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerFirstStep(VolunteerRegisterFirstStepRequest $request)
    {
        $checkResult = $this->checkCodeMelli($request->code_melli);
        if (!$checkResult['canRegister']) {
            return $checkResult['response'];
        }

        // @TODO: verify "code_melli" with "name_first" and "name_last"
        session()->put(implode('.', [$this->registerSessionName, $request->code_melli]), [
             'verified' => true,
             'step'     => 1,
             'formData' => $request->except('_token', '_submit'),
        ]);
        session()->put($this->currentRegisteringSessionName, $request->code_melli);

        return $this->jsonFeedback(null, [
             'ok'       => 1,
             'message'  => trans('ehdafront::forms.feed.wait'),
             'redirect' => get_setting('volunteer_exam')
                  ? null // @todo: redirect to exam page
                  : RouteServiceProvider::actionLocale('VolunteerController@registerFinalStep'),
        ]);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function registerFinalStep()
    {
        if (auth()->guest()) {
            // If user isn't logged in, form data should be read from session
            if (
                 session()->has($this->currentRegisteringSessionName) and
                 session()->has($formDataSessionKey = implode('.', [
                      $this->registerSessionName,
                      $currentRegistering = session()->get($this->currentRegisteringSessionName),
                      'formData',
                 ]))
            ) {
                // @TODO: check exam passed if needed

                $this->variables['currentValues'] = collect(session()->get($formDataSessionKey));
            } else {
                return redirect(RouteServiceProvider::actionLocale('VolunteerController@index'));
            }
        } else {
            if (user()->is_admin()) {
                // If user has logged in as a volunteer, redirect to manage panel
                return redirect('manage');
            }
            if (user()->withDisabled()->is_admin()) {
                // If user has logged in as a disabled volunteer, redirect to first step
                return redirect(RouteServiceProvider::actionLocale('VolunteerController@index'));
            }

            // If user is logged in , form data should be read from user info
            $this->variables['currentValues'] = collect(user()->spreadMeta()->attributesToArray());
        }

        return $this->findInformationPost()
                    ->selectActivityOptions()
                    ->registerFormResult()
             ;
    }



    /**
     * @return $this
     */
    protected function selectActivityOptions()
    {
        $this->variables['activitiesOptions'] = Activity::all();
        return $this;
    }



    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function registerFormResult()
    {
        return ($this->variables['post'] and $this->variables['post']->exists)
             ? $this->selectStatesCombo()
                    ->template('volunteers.volunteer_register.main', $this->variables)
             : redirect(RouteServiceProvider::actionLocale('EhdaFrontController@index'));
    }



    /**
     * @param VolunteerRegisterFinalStepRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function registerFinalStepSubmit(VolunteerRegisterFinalStepRequest $request)
    {
        $homeState  = model('state', $request->home_city);
        $workState  = model('state', $request->work_city);
        $domainSlug = ($domain = $homeState->domain) ? $domain->slug : null;

        if (
             (
                  !$domainSlug or
                  !($volunteerRole = model('role')->grabSlug('volunteer-' . $domainSlug)) or
                  !$volunteerRole->exists
             ) and
             // If there is not roles that matches with domain, we can set default volunteer role from settings
             (
                  !($volunteerRole = model('role')->grabSlug(get_setting('default_volunteer_role'))) or
                  !$volunteerRole->exists
             )
        ) {
            // If there is not roles matches with default volunteer role in setting, we can not register volunteer
            return $this->jsonFeedback(trans('ehdafront::validation.http.Error500'), ['ok' => 0]);
        }


        //$request->merge([
        //     'home_province'           => $homeState,
        //     'work_province'           => $workState,
        //     'domain'                  => $domain,
        //     'activities'              => implode(',', $request->activity),
        //     'password'                => Hash::make($request->password),
        //     'volunteer_registered_at' => Carbon::now()->toDateTimeString(),
        //]);

        $userId = model('user')->batchSaveId($request, ['activity', 'password2']);

        if ($userId) {
            $user = user()->grabId($userId);
            $user->attachRole($volunteerRole->slug, 1); // 1 status points to inactive volunteer

            event(new VolunteerRegistered($user));

            $return = $this->jsonFeedback(null, [
                 'ok'       => 1,
                 'message'  => trans('ehdafront::general.volunteer_section.register_success'),
                 'callback' => 'afterRegisterVolunteer(' . (auth()->guest() ? 'false' : 'true') . '); ',
            ]);
        } else {
            $return = $this->jsonFeedback(null, [
                 'ok'      => 0,
                 'message' => trans('ehdafront::forms.feed.un_save'),
            ]);
        }

        return $return;
    }



    /**
     * @param $codeMelli
     *
     * @return array
     * @throws \Throwable
     */
    private function checkCodeMelli($codeMelli)
    {
        $user = user()->findByUsername($codeMelli);

        if ($user->exists) { // A user with the given "code_melli" exists.
            return [
                 'canRegister' => false,
                 'response'    => $this->jsonFeedback(null, [
                      'ok'         => 0,
                      'message'    => $this->safeTemplate(
                           'volunteers.registration_response.user_existance_message',
                           compact('user')
                      )->render(),
                      'feed_class' => 'blue',
                 ]),
            ];
        }

        return ['canRegister' => true];
    }
}
