<?php

namespace Modules\EhdaFront\Http\Controllers\Api\Manage;

use App\Models\Post;
use App\Models\Printing;
use App\Models\State;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Events\CardRegistered;
use Modules\EhdaFront\Http\Controllers\EhdaFrontModuleControllerAbstract;
use Modules\EhdaFront\Http\Requests\Api\CardGetRequest;
use Modules\EhdaFront\Http\Requests\Api\CardRegisterRequest;
use Modules\EhdaFront\Http\Requests\Api\CardSavePrintRequest;
use Modules\EhdaFront\Http\Requests\Api\CardSearchRequest;
use Modules\EhdaFront\Http\Requests\Api\ListCitiesRequest;
use Modules\EhdaFront\Http\Requests\Manage\UserPortablePrinterConfigsSaveRequest;
use Modules\EhdaFront\Http\Requests\Manage\UserSaveRequest;
use Modules\EhdaFront\Providers\ApiServiceProvider;
use Modules\EhdaFront\Providers\ToolsServiceProvider;
use Modules\EhdaFront\Services\Api\Response\ApiResponse;
use Modules\Yasna\Services\YasnaController;

class UsersController extends YasnaController
{
    public function create(Request $request)
    {
        $model = new User();
        return $this->safeView($this->module()->getAlias() . "::manage.api.user.edit", compact("model"));
    }

    public function edit(Request $request)
    {
        if (
            ($model = model('user')->grabHashid($request->hashid)) and
            $model->exists and
            $model->hasRole(ApiServiceProvider::apiClientRoleSlug())
        ) {
            return $this->safeView($this->module()->getAlias() . "::manage.api.user.edit", compact("model"));
        } else {
            return $this->abort(410);
        }
    }

    public function save(UserSaveRequest $request)
    {
        /*-----------------------------------------------
        | Model
        */
        if ($request->id) {
            $model = User::withTrashed()->find($request->id);
            if (!$model or !$model->exists) {
                return $this->jsonFeedback(trans('validation.http.Error410'));
            }
            if (!$model->canEdit() or !$model->hasRole(ApiServiceProvider::apiClientRoleSlug())) {
                return $this->jsonFeedback(trans('validation.http.Error403'));
            }
        }

        /*-----------------------------------------------
        | Hash Password
        */
        if ($request->password) {
            $request->merge([
                'password' => Hash::make($request->password),
            ]);
        } else {
            $request->replace($request->except(['password']));
        }


        /*-----------------------------------------------
        | Save
        */
        $saved = model('user')->batchSaveId($request->all(), ['model']);

        /*-----------------------------------------------
        | Role Attachment ...
        */
        if ($saved and !$request->id) {
            $model = User::find($saved);
            $model->attachRole(ApiServiceProvider::apiClientRoleSlug());
        }

        /*-----------------------------------------------
        | Feedback
        */
        return $this->jsonAjaxSaveFeedback($saved, [
            'success_callback' => $request->id ? "rowUpdate('tblUsers','$request->hashid')" : 0,
            'success_refresh'  => $request->id ? 0 : 1,
        ]);
    }

    public function ips(Request $request)
    {
        if (
            ($model = model('user')->grabHashid($request->hashid)) and
            $model->exists and
            $model->hasRole(ApiServiceProvider::apiClientRoleSlug())
        ) {
            return $this->safeView($this->module()->getAlias() . "::manage.api.user.ips", compact("model"));
        } else {
            return $this->abort(410);
        }
    }

    public function portablePrinterConfigsForm(Request $request)
    {
        if (
            ($model = model('user')->grabHashid($request->hashid)) and
            $model->exists and
            $model->hasRole(ApiServiceProvider::apiClientRoleSlug())
        ) {
            return $this->safeView(
                $this->module()->getAlias() . "::manage.api.user.portable-printer-configs",
                compact("model")
            );
        } else {
            return $this->abort(410);
        }
    }

    public function portablePrinterConfigs(UserPortablePrinterConfigsSaveRequest $request)
    {
        if (
            ($model = $request->model) and
            $model->exists and
            $model->hasRole(ApiServiceProvider::apiClientRoleSlug())
        ) {
            $request->merge([
                'portable_printer_configs' => json_decode($request->portable_printer_configs, true),
            ]);
            return $this->jsonAjaxSaveFeedback(model('user')->store($request->only(['id', 'portable_printer_configs'])), [
                'success_message' => trans('ehdafront::api.manage.alert.success.portable-printer-configs-save'),
            ]);
        } else {
            return $this->abort(410);
        }
    }
}
