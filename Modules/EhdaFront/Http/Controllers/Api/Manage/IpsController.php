<?php

namespace Modules\EhdaFront\Http\Controllers\Api\Manage;

use App\Models\ApiIp;
use App\Models\User;
use Illuminate\Http\Request;
use Modules\EhdaFront\Http\Requests\Manage\UserIpSaveRequest;
use Modules\EhdaFront\Providers\ApiServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;
use Modules\Yasna\Services\YasnaController;

class IpsController extends YasnaController
{
    protected $base_model = 'ApiIp';

    public function __construct()
    {
        $this->view_folder = $this->module()->getAlias() . "::manage.api.ips";
    }

    public function singleAction($model_hashid, $view_file, ... $option)
    {
        /*
        | Model ...
        */
        $model = model($this->base_model);

        if ($model_hashid != '0' or $model_hashid != hashid(0)) {
            $model = $model->grabber($model_hashid, true);

            if (!$model or !$model->exists) {
                return $this->abort(410);
            }

            $client = $model->user;

            if (!$client->canEdit()) {
                return $this->abort(403);
            }

            $model->spreadMeta();
        }

        /*
        | View ...
        */
        return $this->safeView(
            $this->view_folder . "." . $view_file,
            compact('model', 'option', 'client')
        );
    }

    public function delete(Request $request)
    {
        /*
        | Delete
        */
        $model = ApiIp::grab($request->hashid);
        if (!$model or !$model->exists) {
            return $this->jsonFeedback(trans('validation.http.Error410'));
        }
        if (!($client = $model->user) or !$client->canEdit()) {
            return $this->jsonFeedback(trans('validation.http.Error403'));
        }
        $ok = $model->delete();

        /*
        | Feedback
        */

        return $this->jsonAjaxSaveFeedback($ok, [
            'success_callback'   => 'masterModal("'
                . RouteServiceProvider::action('Api\Manage\UsersController@ips', ['hashid' => $client->hashid])
                . '")',
            'success_modalClose' => 0,
        ]);
    }

    public function save(UserIpSaveRequest $request)
    {
        if (
            ($client = User::find($request->user_id)) and
            $client->exists and
            $client->hasRole(ApiServiceProvider::apiClientRoleSlug())
        ) {
            return $this->jsonAjaxSaveFeedback(
                (($row = model('ApiIp')->batchSave($request->all())) and $row->exists),
                [
                    'success_callback' => 'masterModal("' . RouteServiceProvider::action(
                            'Api\Manage\UsersController@ips',
                            ['hashid' => $client->hashid]
                        ) . '")',
                    'success_modalClose' => 0,
                ]
            );
        } else {
            return $this->abort(410);
        }
    }
}
