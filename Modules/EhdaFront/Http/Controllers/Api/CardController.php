<?php

namespace Modules\EhdaFront\Http\Controllers\Api;

use App\Models\Printing;
use App\Models\Role;
use App\Models\State;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Events\CardRegistered;
use Modules\EhdaFront\Http\Requests\Api\CardEditRequest;
use Modules\EhdaFront\Http\Requests\Api\CardGetRequest;
use Modules\EhdaFront\Http\Requests\Api\CardRegisterRequest;
use Modules\EhdaFront\Http\Requests\Api\CardSavePrintRequest;
use Modules\EhdaFront\Http\Requests\Api\CardSearchRequest;
use Modules\EhdaFront\Http\Requests\Api\PasswordResetRequest;
use Modules\EhdaFront\Notifications\PasswordResetRequestedViaApi;
use Modules\EhdaFront\Services\Api\Response\ApiResponse;

class CardController extends ApiControllerAbstract
{
    protected static $newPasswordLength = 8;



    /**
     * Search for existed card with a specified code melli
     *
     * @param \Modules\EhdaFront\Http\Requests\Api\CardSearchRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(CardSearchRequest $request)
    {
        if (($user = $this->findUser($request->code_melli)) and $user->hasRole($this->cardHolderRoleSlug())) {
            $statusKey = 'card-exists';
        } else {
            $statusKey = 'user-not-found';
        }
        return response()->json(ApiResponse::create($statusKey)->toArray());
    }



    /**
     * @param CardRegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(CardRegisterRequest $request)
    {
        if ($user = $this->findUser($request->code_melli)) {
            if ($user->hasRole($this->cardHolderRoleSlug())) {
                $response = ApiResponse::create('card-exists');
            } else {
                $response = ApiResponse::create('card-register-unable-to');
            }
        } else {
            if (Role::grab($this->cardHolderRoleSlug())->exists) {
                $request->merge([
                     'password'              => Hash::make($request->mobile),
                     'home_province'         => ($homeProvince = State::find($request->home_city)->province)
                          ? $homeProvince->id
                          : null,
                     'domain'                => ($domain = $homeProvince->domain) ? $domain->id : null,
                     'password_force_change' => 1,
                     'card_registered_at'    => Carbon::now(),
                ]);

                if (($user = model('user')->batchSave($request->all()))->exists) {
                    $user->update([
                         'created_by' => $request->getProducer()->id ?: user()->id,
                         'card_no' => $user->id + 5000,
                    ]);
                    $user->attachRole($this->cardHolderRoleSlug());
                    event(new CardRegistered($user));

                    $response = ApiResponse::create('card-process-success')
                                           ->merge($this->cardsLinks($user))
                                           ->merge($this->cardDetails($user))
                    ;
                } else {
                    $response = ApiResponse::create('card-register-failed');
                }
            } else {
                $response = ApiResponse::create('card-register-failed');
            }
        }
        return response()->json($response->toArray());
    }



    /**
     * @param CardEditRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(CardEditRequest $request)
    {
        if ($user = $this->findUser($request->code_melli)) {
            if ($user->hasRole($this->cardHolderRoleSlug())) {
                if ($user->birth_date->equalTo(Carbon::parse($request->current_birth_date))) {
                    if (model('user')->batchSaveId(
                         array_merge(['id' => $user->id], $request->all()),
                         ['current_birth_date', 'code_melli'])
                    ) {
                        return response()->json(ApiResponse::create('card-process-success')
                                                           ->merge($this->cardsLinks($user->refresh()))
                                                           ->merge($this->cardDetails($user))
                                                           ->toArray());
                    } else {
                        $statusKey = 'card-edit-unable-to';
                    }
                } else {
                    $statusKey = 'birth-date-or-mobile-not-match';
                }
            } else {
                $statusKey = 'user-has-no-card';
            }
        } else {
            $statusKey = 'user-not-found';
        }
        return response()->json(ApiResponse::create($statusKey)->toArray());
    }



    /**
     * @param CardGetRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(CardGetRequest $request)
    {
        if ($user = $this->findUser($request->code_melli)) {
            if ($user->hasRole($this->cardHolderRoleSlug())) {
                if (
                    //$user->birth_date->equalTo(Carbon::parse($request->birth_date)) and
                     ($user->birth_date->toDateString() == $request->birth_date) and
                     (!$request->mobile or ($request->mobile == $user->mobile))
                ) {
                    return response()->json(ApiResponse::create('card-process-success')
                                                       ->merge($this->cardsLinks($user))
                                                       ->merge($this->cardDetails($user))
                                                       ->toArray());
                } else {
                    $statusKey = 'birth-date-or-mobile-not-match';
                }
            } else {
                $statusKey = 'user-has-no-card';
            }
        } else {
            $statusKey = 'user-not-found';
        }
        return response()->json(ApiResponse::create($statusKey)->toArray());
    }



    /**
     * @param PasswordResetRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function passwordReset(PasswordResetRequest $request)
    {
        if ($user = $this->findUser($request->code_melli)) {
            if ($user->hasRole($this->cardHolderRoleSlug())) {
                if (
                     $user->birth_date->equalTo(Carbon::parse($request->birth_date)) and
                     (!$request->mobile or ($request->mobile == $user->mobile))
                ) {
                    $newPassword = $this->setNewPassword($user);
                    $response    = ApiResponse::create('password-reset-success');
                    if ($request->return_password) {
                        $response->append('new_password', $newPassword);
                    }
                    return response()->json($response->toArray());
                } else {
                    $statusKey = 'birth-date-or-mobile-not-match';
                }
            } else {
                $statusKey = 'user-has-no-card';
            }
        } else {
            $statusKey = 'user-not-found';
        }
        return response()->json(ApiResponse::create($statusKey)->toArray());
    }



    /**
     * @param User $user
     *
     * @return string
     */
    protected function setNewPassword(User $user)
    {
        $user->update([
             'password'              => Hash::make($newPassword = $this->generateNewPassword()),
             'password_force_change' => 1,
        ]);
        $user->notify(new PasswordResetRequestedViaApi($newPassword));
        return $newPassword;
    }



    /**
     * @return string
     */
    protected function generateNewPassword()
    {
        return rand(str_repeat(1, self::$newPasswordLength), str_repeat(9, self::$newPasswordLength));
    }



    /**
     * @param CardSavePrintRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function savePrintRequest(CardSavePrintRequest $request)
    {
        if (($user = $this->findUser($request->code_melli)) and $user->hasRole($this->cardHolderRoleSlug())) {
            $insert = [
                 'user_id'       => $user->id,
                 'event_id'      => $request->event,
                 'queued_at'     => $now = Carbon::now(),
                 'queued_by'     => ($client = user())->id,
                 'printed_at'    => $now,
                 'printed_by'    => $client->id,
                 'verified_at'   => $now,
                 'verified_by'   => $client->id,
                 'dispatched_at' => $now,
                 'dispatched_by' => $client->id,
                 'delivered_at'  => $now,
                 'delivered_by'  => $client->id,
                 'created_by'    => $client->id,
                 'updated_by'    => $client->id,
            ];

            model('Printing')->batchSaveId($insert);

            $statusKey = 'card-print-success';
        } else {
            $statusKey = 'card-print-unable-to';
        }
        return response()->json(ApiResponse::create($statusKey)->toArray());
    }



    /**
     * Find user with code melli
     *
     * @param $codeMelli
     *
     * @return User|null
     */
    protected function findUser($codeMelli)
    {
        return model('user')->where('code_melli', $codeMelli)->first();
    }



    /**
     * @return string
     */
    protected function cardHolderRoleSlug()
    {
        return 'card-holder';
    }



    /**
     * @param User $user
     *
     * @return array
     */
    protected function cardsLinks(User $user)
    {
        return $user->exists
             ? [
                  'ehda_card_mini'     => $user->newCards('mini'),
                  'ehda_card_single'   => $user->newCards('single'),
                  'ehda_card_social'   => $user->newCards('social'),
                  'ehda_card_print'    => $user->newCards('full', 'print'),
                  'ehda_card_download' => $user->newCards('mini', 'download'),
             ]
             : [];
    }



    /**
     * @param User $user
     *
     * @return array
     */
    protected function cardDetails(User $user)
    {
        return $user->exists
             ? [
                  'ehda_card_details' => [
                       'register_no'   => $user->card_no,
                       'full_name'     => $user->full_name,
                       'father_name'   => $user->name_father,
                       'code_melli'    => $user->code_melli,
                       'birth_date'    => echoDate($user->birth_date, 'Y/m/d', 'fa', false),
                       'registered_at' => echoDate($user->card_registered_at, 'Y/m/d', 'fa', false),
                       'full_data'     => $user->full_name . ' - ' . $user->card_no,
                       'unique_id'     => $user->hashid,
                  ],
             ]
             : [];
    }
}
