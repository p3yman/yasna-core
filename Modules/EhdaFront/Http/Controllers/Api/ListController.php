<?php

namespace Modules\EhdaFront\Http\Controllers\Api;

use App\Models\Post;
use App\Models\Printing;
use App\Models\State;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Events\CardRegistered;
use Modules\EhdaFront\Http\Requests\Api\CardGetRequest;
use Modules\EhdaFront\Http\Requests\Api\CardRegisterRequest;
use Modules\EhdaFront\Http\Requests\Api\CardSavePrintRequest;
use Modules\EhdaFront\Http\Requests\Api\CardSearchRequest;
use Modules\EhdaFront\Http\Requests\Api\ListCitiesRequest;
use Modules\EhdaFront\Http\Requests\Api\ListNewsDetailsRequest;
use Modules\EhdaFront\Providers\ApiServiceProvider;
use Modules\EhdaFront\Providers\ToolsServiceProvider;
use Modules\EhdaFront\Services\Api\Response\ApiResponse;

class ListController extends ApiControllerAbstract
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function provinces(Request $request)
    {
        return response()->json(ApiResponse::create('list-provinces-success')
                                           ->append('province', State::getProvinces()->select('id', 'title')->get())
                                           ->toArray());
    }



    /**
     * @param ListCitiesRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cities(ListCitiesRequest $request)
    {
        return response()->json(ApiResponse::create('list-cities-success')
                                           ->append('cities',
                                                model('state')
                                                     ->where('parent_id', $request->province)
                                                     ->select('id', 'title')->get())
                                           ->toArray());
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function educationLevels(Request $request)
    {
        if (($levels = ToolsServiceProvider::activeEduLevelsTranses()) and is_array($levels) and count($levels)) {
            $response = ApiResponse::create('list-education-levels-success')
                                   ->append('education', array_values(array_map(function ($key, $value) {
                                       return ['id' => $key, 'title' => $value];
                                   }, array_keys($levels), array_values($levels))))
            ;
        } else {
            $response = ApiResponse::create('list-education-levels-unavailable');
        }

        return response()->json($response->toArray());
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function printerConfigs(Request $request)
    {
        if (
             (
                  ($configs = user()->spreadMeta()->portable_printer_configs) and
                  is_array($configs) and
                  count($configs)
             ) or
             (
                  ($configs = get_setting('prepare_portable_printer_config')) and
                  isJson(get_setting('prepare_portable_printer_config')) and
                  is_array($configs = json_decode($configs, true)) and
                  count($configs)
             )
        ) {
            $response = ApiResponse::create('list-configs-success')
                                   ->append('config', $configs)
            ;
        } else {
            $response = ApiResponse::create('list-configs-unavailable');
        }

        return response()->json($response->toArray());
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function printerSlideShow(Request $request)
    {
        if (count($items = $this->printerSlideShowItems())) {
            $response = ApiResponse::create('list-slideshow-success')
                                   ->append('slideshow', $items)
            ;
        } else {
            $response = ApiResponse::create('list-slideshow-unavailable');
        }

        return response()->json($response->toArray());
    }



    /**
     * @return array
     */
    protected function printerSlideShowItems()
    {
        return Post::selector([
             'category' => 'printers-slideshow',
             'type'     => 'slideshows',
        ])->orderByDesc('published_at')
                   ->limit(12)
                   ->get()
                   ->map(function ($item) {
                       return ['picture' => doc($item->featured_image)->getUrl(), 'title' => $item->title];
                   })->filter(function ($item) {
                       return $item['picture'];
                   })->values()
                   ->toArray()
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function activeEvents(Request $request)
    {
        if (count($items = $this->activeEventsItems())) {
            $response = ApiResponse::create('list-events-success')
                                   ->append('events_list', $items)
            ;
        } else {
            $response = ApiResponse::create('list-events-unavailable');
        }

        return response()->json($response->toArray());
    }



    /**
     * @return array
     */
    protected function activeEventsItems()
    {
        return Post::selector([
             'type' => ApiServiceProvider::POSTTYPES_SLUGS['events'],
        ])->where('event_starts_at', '<=', Carbon::now())
                   ->where('event_ends_at', '>=', Carbon::now())
                   ->orderByDesc('published_at')
                   ->limit(50)
                   ->get()
                   ->map(function ($item) {
                       return ['title' => $item->title, 'unique_id' => $item->hashid];
                   })->values()
                   ->toArray()
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function broadcastNewsList(Request $request)
    {
        if (count($items = $this->broadcastNewsItems())) {
            $response = ApiResponse::create('list-news-success')
                                   ->append('news_list', $items)
            ;
        } else {
            $response = ApiResponse::create('list-news-unavailable');
        }

        return response()->json($response->toArray());
    }



    /**
     * @return array
     */
    protected function broadcastNewsItems()
    {
        return model('post')
             ->elector([
                  'type'     => ApiServiceProvider::POSTTYPES_SLUGS['news'],
                  'category' => ApiServiceProvider::CATEGORIES_SLUGS['news']['broadcast'],
             ])
             ->orderByDesc('published_at')
             ->limit(50)
             ->get()
             ->map(function ($item) {
                 return [
                      'title'          => $item->title,
                      'unique_id'      => $item->hashid,
                      'abstract'       => $item->abstract,
                      'published_at'   => strtotime($item->published_at),
                      'featured_image' => fileManager()->file($item->featured_image)->getUrl() ?: "",
                 ];
             })->values()
             ->toArray()
             ;
    }



    /**
     * @param ListNewsDetailsRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function broadcastNewsSingle(ListNewsDetailsRequest $request)
    {
        return response()->json(ApiResponse::create('details-news-success')
                                           ->append('details', [
                                                'title'          => ($news = $request->model)->title,
                                                'unique_id'      => $news->hashid,
                                                'abstract'       => $news->abstract,
                                                'text'           => $news->text,
                                                'published_at'   => strtotime($news->published_at),
                                                'featured_image' => fileManager()
                                                     ->file($news->featured_image)
                                                     ->getUrl() ?: "",
                                           ])
                                           ->toArray());
    }
}
