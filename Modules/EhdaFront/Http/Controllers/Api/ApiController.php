<?php

namespace Modules\EhdaFront\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Http\Requests\Api\GetTokenRequest;
use Modules\EhdaFront\Providers\Api\ApiIpServiceProvider;
use Modules\EhdaFront\Providers\Api\TokenServiceProvider;
use Modules\EhdaFront\Providers\ApiServiceProvider;
use Modules\EhdaFront\Services\Api\Response\ApiResponse;

class ApiController extends ApiControllerAbstract
{
    /**
     * Generate and return token depending on received credentials.
     *
     * @param \Modules\EhdaFront\Http\Requests\Api\GetTokenRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getToken(GetTokenRequest $request)
    {
        if ($client = $this->findClient($request)) {
            if (ApiIpServiceProvider::userIsPermittedWithIp($client)) {
                $response = ApiResponse::create('token-created')
                                       ->append(
                                            'token',
                                            ($token = TokenServiceProvider::newToken($client))->api_token
                                       )
                                       ->append('created_at', $token->created_at->tz('UTC')->toW3cString())
                ;
            } else {
                $response = ApiResponse::create('unacceptable-ip');
            }
        } else {
            $response = ApiResponse::create('credentials-invalid');
        }
        return response()->json($response->toArray());
    }



    /**
     * Find client with its credentials
     *
     * @param $request
     *
     * @return User|bool
     */
    protected function findClient($request)
    {
        if (
             ($client = user()->findByUsername($request->username)) and
             Hash::check($request->password, $client->password) and
             $client->hasRole($this->apiRoleSlug())
        ) {
            return $client;
        } else {
            return false;
        }
    }



    /**
     * Name of the column that client's username stored in
     *
     * @return string
     */
    protected function username()
    {
        return 'code_melli';
    }



    /**
     * Slug of the role that an API client must have
     *
     * @return string
     */
    protected function apiRoleSlug()
    {
        return ApiServiceProvider::apiClientRoleSlug();
    }
}
