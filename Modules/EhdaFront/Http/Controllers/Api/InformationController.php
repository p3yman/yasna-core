<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/29/18
 * Time: 4:52 PM
 */

namespace Modules\EhdaFront\Http\Controllers\Api;

use Modules\EhdaFront\Providers\ApiServiceProvider;
use Modules\EhdaFront\Services\Api\Response\ApiResponse;

class InformationController extends ApiControllerAbstract
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function about()
    {
        if ($post = model('post')->elector([
             'type' => ApiServiceProvider::POSTTYPES_SLUGS['statics'],
             'slug' => 'about',
        ])->first()) {
            $response = ApiResponse::create('information-about-success')
                                   ->append('about_us', $post->text)
            ;
        } else {
            $response = ApiResponse::create('information-about-unavailable');
        }

        return response()->json($response->toArray());
    }



    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function contact()
    {
        if (count($data = $this->findContactData())) {
            $response = ApiResponse::create('information-contact-success')
                                   ->append('contact_information', $data)
            ;
        } else {
            $response = ApiResponse::create('information-contact-unavailable');
        }

        return response()->json($response->toArray());
    }



    /**
     * @return array
     */
    public function findContactData()
    {
        return array_merge(
             $this->findContactInfo(),
             $this->findSocialNetworks(),
             $this->findLocation()
        );
    }



    /**
     * @return array
     */
    protected function findContactInfo()
    {
        return array_filter([
             'address'   => get_setting('address'),
             'telephone' => get_setting('telephone'),
             'email'     => get_setting('email'),
        ], function ($item) {
            return !is_null($item);
        });
    }



    /**
     * @return array
     */
    protected function findSocialNetworks()
    {
        $socialLinks = array_filter([
             'telegram'  => get_setting('telegram_link'),
             'twitter'   => get_setting('twitter_link'),
             'facebook'  => get_setting('facebook_link'),
             'instagram' => get_setting('instagram_link'),
             'aparat'    => get_setting('aparat_link'),
        ], function ($item) {
            return !is_null($item);
        });

        return count($socialLinks) ? ['social_links' => $socialLinks] : [];
    }



    /**
     * @return array
     */
    protected function findLocation()
    {
        if (($location = get_setting('location')) and is_array($location) and (count($location) == 2)) {
            return [
                 'location' => [
                      'latitude'  => $location[0],
                      'longitude' => $location[1],
                 ],
            ];
        } else {
            return [];
        }
    }
}
