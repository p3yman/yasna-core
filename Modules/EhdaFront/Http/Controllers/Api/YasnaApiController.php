<?php

namespace Modules\EhdaFront\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;
use Modules\Yasna\Services\YasnaController;

class YasnaApiController extends YasnaController
{
    protected $token = 'gjglkk87YYD12RDFHd8878gdss2SWSTYGSDSsSDHgbdkkdASfbksd87377t73ge';
    protected $response;



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function sendSms(Request $request)
    {
        $validator = Validator::make($request->all(), [
             'token'  => 'required|in:' . $this->token,
             'text'   => 'required',
             'mobile' => 'required',
        ]);

        if ($validator->fails()) {
            $this->response['status'] = false;
        } else {
            $this->response['status'] = true;
            $this->response['send']   = FanapServiceProvider::sendMessage($request->mobile, $request->text);
        }
        return api()->successRespond($this->response);
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function setLogs(Request $request)
    {
        $data['body']   = $request->all();
        $data['header'] = $request->headers->all();
        setLog('GitHub', 'meysampg', $data);
        return 'BALA BALA';
    }
}
