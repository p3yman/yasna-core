<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 04/03/2018
 * Time: 05:43 PM
 */

namespace Modules\EhdaFront\Http\Controllers\Api;

use Illuminate\Routing\Controller;

abstract class ApiControllerAbstract extends Controller
{
}
