<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/17/18
 * Time: 3:40 PM
 */

namespace Modules\EhdaFront\Http\Controllers\Api;

use App\Models\Post;
use Modules\EhdaFront\Http\Controllers\EhdaFrontControllerAbstract;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class ApiDocumentationController extends EhdaFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $post = $this->findApiDocumentationPost();
        if ($post->exists) {
            return redirect($post->direct_url);
        }

        return $this->abort(404);
    }



    /**
     * @return Post
     */
    protected function findApiDocumentationPost()
    {
        return PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'api-documentation',
        ])->first() ?: post();
    }
}
