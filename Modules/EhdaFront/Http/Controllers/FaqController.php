<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/8/18
 * Time: 4:29 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class FaqController extends EhdaFrontControllerAbstract
{
    public function index(Request $request)
    {
        $this->findPosttype()->findCommentingPost();
        if ($this->variables['posttype']->exists or ($this->variables['commentingPost']->exists)) {
            return $this->findPosts($request)
                        ->template('faq.main', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    protected function findPosttype()
    {
        return $this->appendToVariables('posttype', model('posttype')->grab($this->posttype_slugs['faq']));
    }



    protected function findPosts($request)
    {
        $this->variables['postGroups'] = [];
        foreach ($this->variables['posttype']->categories as $category) {
            $posts = PostsServiceProvider::collectPosts(array_merge([
                 'type'       => $this->posttype_slugs['faq'],
                 'category'   => $category->slug,
                 'pagination' => false,
                 'limit'      => 50,
            ], $this->searchFields($request)));

            if ($posts->count()) {
                $this->variables['postGroups'][] = [
                     'category' => $category,
                     'posts'    => $posts,
                ];
            }
        }

        return $this;
    }



    protected function findCommentingPost()
    {
        return $this->appendToVariables('commentingPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['commenting'],
             'slug' => 'faq',
        ])->first() ?: new Post());
    }



    protected function searchFields($request)
    {
        if ($search = $request->search) {
            $this->appendToVariables('searchValue', $search);
            return ['search' => $request->search];
        }

        return [];
    }
}
