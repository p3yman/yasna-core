<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/9/18
 * Time: 12:15 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\EhdaFront\Http\Requests\DonationPaymentRequest;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;

class DonationController extends EhdaFrontControllerAbstract
{
    protected $offline_account_types = ['shetab', 'deposit', 'sheba'];

    protected $alert_session_key = 'alert';



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        if ($this->findStaticPost()->variables['staticPost']) {
            return $this
                 ->findPaymentData()
                 ->generateAlert()
                 ->template('donation.main', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return $this
     */
    protected function findStaticPost()
    {
        return $this->appendToVariables('staticPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'donation',
        ])->first());
    }



    /**
     * @return $this
     */
    protected function findPaymentData()
    {
        return $this->findAccounts();
    }



    /**
     * @return $this
     */
    protected function findAccounts()
    {
        return $this->findOfflineAccounts()
                    ->findOnlineAccounts()
                    ->findWares()
             ;
    }



    /**
     * @return $this
     */
    protected function findOfflineAccounts()
    {
        $offlineAccountGroups = [];
        foreach ($this->offline_account_types as $type) {
            $accounts = payment()->accounts()->$type()->get();
            if ($accounts->count()) {
                $offlineAccountGroups[$type] = [
                     'method'   => $this->accountConvertMethod($type),
                     'accounts' => $accounts,
                ];
            }
        }
        return $this->appendToVariables('offlineAccountGroups', $offlineAccountGroups);
    }



    /**
     * @return $this
     */
    protected function generateAlert()
    {
        if (session()->has($this->alert_session_key)) {
            $sessionValue = session($this->alert_session_key);
            session()->forget($this->alert_session_key);
        }
        return $this->appendToVariables('alert', $sessionValue ?? null);
    }



    /**
     * @param $type
     *
     * @return null|string
     */
    protected function accountConvertMethod($type)
    {
        switch ($type) {
            case 'shetab':
                return 'toCardNumber';

            default:
                return null;
        }
    }



    /**
     * @return $this
     */
    protected function findOnlineAccounts()
    {
        return $this->appendToVariables('onlineAccount', payment()->accounts()->online()->first());
    }



    /**
     * @return $this
     */
    protected function findWares()
    {
        return $this->appendToVariables('wares', (PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['donation'],
             'slug' => 'donation',
        ])->first() ?: model('post'))->waresIn(getLocale())->get());
    }



    /**
     * @param DonationPaymentRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pay(DonationPaymentRequest $request)
    {
        $trackingNo = payment()
             ->transaction()
             ->amount($request->amount)
             ->invoiceId(0)
             ->callback(RouteServiceProvider::actionLocale('DonationController@paymentResult'))
             ->getTracking()
        ;

        if ($trackingNo and !is_array($trackingNo)) {
            $request->merge(['tracking_no' => $trackingNo]);
            $donationRow = model('donation')->batchSave($request->all());
            if ($donationRow->exists) {
                return $this->jsonAjaxSaveFeedback(1, [
                     'success_redirect' => RouteServiceProvider::actionLocale('DonationController@sendToGateway', [
                          'donation' => $donationRow->hashid,
                     ]),
                ]);
            }
        }

        return $this->jsonSaveFeedback(0);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function sendToGateway(Request $request)
    {
        if (($donation = model('donation')->grab($request->donation))->exists) {
            if (
                 ($fireResult = payment()->transaction()->trackingNumber($donation->tracking_no)->fire()) and
                 is_array($fireResult) and
                 ($fireResult['code'] == 100)
            ) {
                return $fireResult['response'];
            } else {
                return $this->abort(403);
            }
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function paymentResult(Request $request)
    {
        if ($trackingNo = $request->tracking_no) {
            switch ($statusCode = payment()->transaction()->trackingNumber($trackingNo)->statusCode()) {
                case 1: // Verified
                    model('donation')->whereTrackingNo($trackingNo)->update([
                         'paid_at' => Carbon::now(),
                         'paid_by' => user()->id,
                    ])
                    ;
                    $message = trans('ehdafront::general.donation.message.success.payment')
                         . '<br>'
                         . trans('ehdafront::general.payment.message.general.tracking_no', [
                              'trackingNo' => ad($trackingNo),
                         ]);
                    break;

                case -3: // Canceled
                    $message = trans('ehdafront::general.payment.message.failure.cancelled');
                    break;

                case -4: // Not Verified / Rejected
                    $message = trans('ehdafront::general.payment.message.failure.rejected')
                         . '<br>'
                         . trans('ehdafront::general.payment.message.general.tracking_no', [
                              'trackingNo' => ad($trackingNo),
                         ]);
                    break;

                default: // Unhandled Status
                    $message = trans('ehdafront::general.payment.message.failure.rejected')
                         . '<br>'
                         . trans('ehdafront::general.payment.message.general.tracking_no', [
                              'trackingNo' => ad($trackingNo),
                         ]);
                    break;
            }

            $messageType = ($statusCode > 0) ? 'success' : 'danger';

            session()->put($this->alert_session_key, compact('message', 'messageType'));
        }

        return redirect(RouteServiceProvider::actionLocale('DonationController@index'));
    }
}
