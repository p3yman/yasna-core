<?php

namespace Modules\EhdaFront\Http\Controllers;

use Modules\Yasna\Http\Controllers\YasnaController;

class VolunteerConvertController extends YasnaController
{
    /**
     * Convert dismissed volunteer data from role record
     */
    public function index()
    {
        // find all admin roles
        $roles = role()->adminRoles();

        for ($i = 0; $i < count($roles); $i++) {
            $role = $roles[$i];
            echo $role . '<br>';

            // check the role is an volunteer
            if (strpos($role, 'olunteer')) {
                // find users with the current volunteer role where volunteer registered in null
                $users = role($role)
                     ->users()
                     ->where('volunteer_registered_at', null)
                     ->limit(10)
                     ->get()
                ;

                // loop on users list
                foreach ($users as $user) {
                    $user = $user->toArray();

                    echo $user['id'] . ' - ' . $user['pivot']['created_at'] . '<br>';

                    // check the volunteer register date time
                    if ($user['volunteer_registered_at'] < $user['pivot']['created_at']) {
                        // update user volunteer registered at column
                        model('user', $user['id'])->batchSave([
                             'volunteer_registered_at' => $user['pivot']['created_at'],
                        ]);
                        echo $user['id'] . ' Updated! <hr>';
                    }
                }
            }
        }

        // automatically refresh page
        echo '<script>
             location.reload(); 
        </script>';


    }
}
