<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 28/02/2018
 * Time: 04:23 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\EhdaFront\Http\Controllers\Traits\EhdaFrontFeedbackControllerTrait;
use Nwidart\Modules\Facades\Module;

class CardFileControllerAbstract extends EhdaFrontModuleControllerAbstract
{
    use EhdaFrontFeedbackControllerTrait;
    protected $cardTypes           = ['mini', 'single', 'social', 'full'];
    protected $defaultCardType     = 'mini';
    protected $cardModes           = ['show', 'download', 'print'];
    protected $defaultCardMode     = 'show';
    protected $cardHashTypes       = ['id', 'st'];
    protected $defaultCardHashType = 'id';

    public function generateCard(Request $request)
    {
        $type     = in_array($request->type, $this->cardTypes) ? $request->type : $this->defaultCardType;
        $mode     = in_array($request->mode, $this->cardModes) ? $request->mode : $this->defaultCardMode;
        $hashType = in_array($request->hash_type, $this->cardHashTypes) ? $request->hash_type : $this->defaultCardHashType;

        switch ($hashType) {
            case  'id':
                $connection = 'ehda_card_' . $type;
                $id         = intval(hashid($request->user_type_hashid, $connection));
                $user       = model('user')->grabId($id);
                if (!$user or !$user->is_a('card-holder')) {
                    return $this->abort(404);
                }
                break;

            case 'st':
                $user = decrypt($request->user_type_hashid);
                break;

            default:
                return $this->abort(404);
        }

        if ($mode == 'print') {
            if ($hashType == 'st') {
                $user = json_decode(json_encode($user), false);
            }

            // print card
            return $this->printBlade(compact('type', 'user', 'mode'));
        } else {
            // show and download card
            switch ($type) {
                case "mini":
                    return $this->cardMini($user, $mode);
                    break;
                case "single":
                    return $this->cardSingle($user, $mode);
                    break;
                case "social":
                    return $this->cardSocial($user, $mode);
                    break;
                case "full":
                    return $this->cardFull($user, $mode);
                    break;
                default:
                    return $this->cardMini($user, $mode);
            }
        }
    }

    protected function preUser($user)
    {
        if (is_object($user)) {
            return $user->toArray();
        } else {
            return $user;
        }
    }

    protected function getPersianFont()
    {
        return Module::getAssetsPath() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, [
                self::getModuleAlias(),
                'fonts',
                'php',
                'BNazanin.ttf',
            ]);
    }

    protected function getEnglishFont()
    {
        return Module::getAssetsPath() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, [
                self::getModuleAlias(),
                'fonts',
                'php',
                'calibri.ttf',
            ]);
    }

    protected function setHeader($mode, $file_name)
    {
        header("Content-type: image/png");
        if ($mode == 'print') {
            header('Content-Disposition: filename=' . 'کارت_اهدای_عضو_' . $file_name . '.png');
        } elseif ($mode == 'download') {
            header('Content-Description: File Transfer');
            header('Content-Type: image/png');
            header('Content-Disposition: attachment; filename="' . $file_name . '.png"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
        } else {
            header('Content-Disposition: filename=' . 'کارت_اهدای_عضو_' . $file_name . '.png');
        }
    }

    protected function absoluteModuleAssetUrl($url)
    {
        return str_before(url('/'), '//') . $url;
    }
}
