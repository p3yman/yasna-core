<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/8/18
 * Time: 5:57 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Http\Request;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class ActivitiesController extends EhdaFrontControllerAbstract
{
    public function index(Request $request)
    {
        if (
             $this->findPosttype()->findCurrentCategory($request->category)->variables['posttype']->exists and
             (
                  !$request->category or
                  $this->variables['currentCategory']->exists
             )
        ) {
            return $this
                 ->findCategories()
                 ->findPosts($request)
                 ->template('activities.list.main', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    protected function findPosttype()
    {
        return $this->appendToVariables('posttype', model('posttype')->grab($this->posttype_slugs['activities']));
    }



    protected function findCategories()
    {
        return $this->appendToVariables('categories', $this->variables['posttype']->categories);
    }



    protected function findPosts($request)
    {
        return $this->appendToVariables('posts', PostsServiceProvider::collectPosts(array_merge(
             [
                  'type'         => $this->posttype_slugs['activities'],
             ],
             $this->categorySelectData($request),
             $this->searchSelectData($request)
        )));
    }



    protected function categorySelectData($request)
    {
        return $request->category ? ['category' => $request->category] : [];
    }



    protected function searchSelectData($request)
    {
        if ($search = $request->search) {
            $this->appendToVariables('searchValue', $search);
            return [
                 'search'       => $request->search,
                 'paginate_url' => $request->fullUrl(),
            ];
        }

        return [];
    }



    protected function findCurrentCategory($categorySlug)
    {
        return $this->appendToVariables('currentCategory',
             (
                  $categorySlug and
                  ($category = $this->variables['posttype']->categories()->whereSlug($categorySlug)->first())
             )
                  ? $category
                  : model('category')
        );
    }
}
