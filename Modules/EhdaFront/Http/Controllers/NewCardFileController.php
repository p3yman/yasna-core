<?php

namespace Modules\EhdaFront\Http\Controllers;

use Modules\EhdaFront\Providers\FaGDServiceProvider;
use Nwidart\Modules\Facades\Module;

class NewCardFileController extends CardFileControllerAbstract
{
    protected function printBlade($data)
    {
        return view('ehdafront::members.print_my_card.new', $data);
    }

    protected function cardMini($user, $mode)
    {
        ini_set("error_reporting", "E_ALL & ~E_NOTICE & ~E_STRICT");

        $user = $this->preUser($user);

        // load persian font
        $font = $this->getPersianFont();

        // set header
        $this->setHeader($mode, $user['card_no']);

        // orginal image
        $img = imagecreatefrompng($this->getDefaultImage('cardMini.png'));

        // data
        $name_full = FaGDServiceProvider::fagd($user['name_first'] . ' ' . $user['name_last'], 'fa', 'nastaligh');

        $data = $user['card_no'] . ' - ' . $name_full;

        // font size
        $font_size = 25;

        // position
        $data_position = imagettfbbox($font_size, 0, $font, $data);
        $data_position = $data_position[2] - $data_position[0];
        $data_position = (800 - $data_position) / 2;

        // Create some colors
        $black = imagecolorallocate($img, 66, 66, 66);

        // Add the text
        imagettftext($img, $font_size, 0, $data_position, 910, $black, $font, $data);

        // Using imagepng() results in clearer text compared with imagejpeg()
        imagepng($img);
        imagedestroy($img);
    }

    protected function cardSingle($user, $mode)
    {
        ini_set("error_reporting", "E_ALL & ~E_NOTICE & ~E_STRICT");

        $user = $this->preUser($user);

        // load persian font
        $font = $this->getPersianFont();

        // set header
        $this->setHeader($mode, $user['card_no']);

        // orginal image
        $img = imagecreatefrompng($this->getDefaultImage('cardSingle.png'));

        // data
        $name_full = FaGDServiceProvider::fagd($user['name_first'] . ' ' . $user['name_last'], 'fa', 'nastaligh');

        $data = $user['card_no'] . ' - ' . $name_full;

        // font size
        $font_size = 25;

        // position
        $data_position = imagettfbbox($font_size, 0, $font, $data);
        $data_position = $data_position[2] - $data_position[0];
        $data_position = (800 - $data_position) / 2;

        // Create some colors
        $black = imagecolorallocate($img, 66, 66, 66);

        // Add the text
        imagettftext($img, $font_size, 0, $data_position, 380, $black, $font, $data);


        // Using imagepng() results in clearer text compared with imagejpeg()
        imagepng($img);
        imagedestroy($img);
    }

    protected function cardSocial($user, $mode)
    {
        ini_set("error_reporting", "E_ALL & ~E_NOTICE & ~E_STRICT");

        $user = $this->preUser($user);

        // load persian font
        $font = $this->getPersianFont();

        // set header
        $this->setHeader($mode, $user['card_no']);

        // orginal image
        $img = imagecreatefrompng($this->getDefaultImage('cardSocial.png'));

        // data
        $name_full = FaGDServiceProvider::fagd($user['name_first'] . ' ' . $user['name_last'], 'fa', 'nastaligh');
//        $name_father = FaGDServiceProvider::fagd($user['name_father'], 'fa', 'nastaligh');
//        $register_date = echoDate($user['card_registered_at'], 'Y/m/d', 'fa', true);

        // font size
        $font_size      = 21;
        $name_font_size = 34;

        // position
        $name_position = imagettfbbox($name_font_size, 0, $font, $name_full);
        $name_position = $name_position[2] - $name_position[0];
        $name_position = ((750 - $name_position) / 2) + 150;


        $card_no_position = imagettfbbox($font_size, 0, $font, $user['card_no']);
        $card_no_position = $card_no_position[2] - $card_no_position[0];
        $card_no_position = ((750 - $card_no_position) / 2) + 150;

//        $register_date_position = imagettfbbox($font_size, 0, $font, $register_date);
//        $register_date_position = $register_date_position[2] - $register_date_position[0];

        // Create some colors
        $black = imagecolorallocate($img, 0, 0, 0);
        $blue  = imagecolorallocate($img, 1, 174, 240);

        // Add the text
        imagettftext($img, $name_font_size, 0, $name_position, 1080, $blue, $font, $name_full);
        imagettftext($img, $name_font_size, 0, $card_no_position, 1140, $blue, $font, $user['card_no']);
//        imagettftext($img, $font_size, 0, (597 - $register_date_position), 318, $black, $font, $register_date);

        // Using imagepng() results in clearer text compared with imagejpeg()
        imagepng($img);
        imagedestroy($img);
    }

    protected function cardFull($user, $mode)
    {
        ini_set("error_reporting", "E_ALL & ~E_NOTICE & ~E_STRICT");

        $user = $this->preUser($user);

        // load persian font
        $font   = $this->getPersianFont();
        $enFont = $this->getEnglishFont();

        // set header
        $this->setHeader($mode, $user['card_no']);

        // orginal image
        $img = imagecreatefrompng($this->getDefaultImage('finalCart.png'));

        // data
        $name_full     = FaGDServiceProvider::fagd($user['name_first'] . ' ' . $user['name_last'], 'fa', 'nastaligh');
        $data          = $user['card_no'] . ' - ' . $name_full;
        $name_father   = FaGDServiceProvider::fagd($user['name_father'], 'fa', 'nastaligh');
        $birth_date    = echoDate($user['birth_date'], 'Y/m/d', 'fa', true);
        $register_date = echoDate($user['card_registered_at'], 'Y/m/d', 'fa', true);

        // font size
        $font_size = 30;

        // position
        $email_position = imagettfbbox(40, 0, $font, $user['email']);
        $email_position = $email_position[2] - $email_position[0];

        $mobile_position = imagettfbbox(40, 0, $enFont, $user['mobile']);
        $mobile_position = $mobile_position[2] - $mobile_position[0];

        $data_position = imagettfbbox($font_size, 0, $font, $data);
        $data_position = $data_position[2] - $data_position[0];
        $data_position = ((800 - $data_position) / 2) + 1272;

        // Create some colors
        $black = imagecolorallocate($img, 66, 66, 66);

        // Add the text
        imagettftext($img, $font_size, 0, $data_position, 825, $black, $font, $data);
        imagettftext($img, 40, 0, (1850 - $mobile_position), 2115, $black, $font, $user['mobile']);
        imagettftext($img, 40, 0, (1850 - $email_position), 2190, $black, $enFont, $user['email']);

        // Using imagepng() results in clearer text compared with imagejpeg()
        imagepng($img);
        imagedestroy($img);
    }

    protected function getDefaultImage($imageName)
    {
        return $this->absoluteModuleAssetUrl(Module::asset('ehdafront:' . implode('/', [
                'images',
                'template',
                'card-new',
                $imageName,
            ])));
    }
}
