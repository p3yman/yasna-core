<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/7/18
 * Time: 1:43 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class OrganizationController extends EhdaFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this
             ->sectionVolunteers()
             ->sectionSponsors()
             ->sectionMembers()
             ->sectionCommittees()
             ->template('organization.main', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function sectionVolunteers()
    {
        return $this->sectionVolunteersStaticPost()
                    ->sectionVolunteersCelebrities()
             ;
    }



    /**
     * @return $this
     */
    protected function sectionVolunteersStaticPost()
    {
        return $this->appendToVariables('volunteersPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'organization-volunteers',
        ])->first());
    }



    /**
     * @return $this
     */
    protected function sectionVolunteersCelebrities()
    {
        return $this->appendToVariables('celebrities', PostsServiceProvider::collectPosts([
             'type'       => $this->posttype_slugs['celebrities'],
             'category'   => 'foundations-page',
             'random'     => true,
             'limit'      => 6,
             'pagination' => false,
        ]))->appendToVariables('celebrities_posttype', posttype()->grabSlug($this->posttype_slugs['celebrities']))
             ;
    }



    /**
     * @return $this
     */
    protected function sectionSponsors()
    {
        if ($this->sectionSponsorsPosttype()->variables['sponsorsPosttype']->exists) {
            $this->sectionSponsorsSpecial()
                 ->sectionSponsorsOther()
            ;
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function sectionSponsorsPosttype()
    {
        return $this->appendToVariables('sponsorsPosttype', Posttype::firstOrNew([
             'slug' => $this->posttype_slugs['sponsors'],
        ]));
    }



    /**
     * @return $this
     */
    protected function sectionSponsorsSpecial()
    {
        $categorySlug = 'special';
        $this->appendToVariables('sponsorsSpecialCategory',
             $this->variables['sponsorsPosttype']->categories()->whereSlug($categorySlug)->first());
        if ($this->variables['sponsorsSpecialCategory']) {
            $this->appendToVariables('sponsorsSpecial', PostsServiceProvider::collectPosts([
                 'type'       => $this->posttype_slugs['sponsors'],
                 'category'   => $categorySlug,
                 'pagination' => false,
                 'limit'      => 100,
            ]));
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function sectionSponsorsOther()
    {
        $categorySlug = 'others';
        $this->appendToVariables('sponsorsOtherCategory',
             $this->variables['sponsorsPosttype']->categories()->whereSlug($categorySlug)->first());
        if ($this->variables['sponsorsSpecialCategory']) {
            $this->appendToVariables('sponsorsOther', PostsServiceProvider::collectPosts([
                 'type'       => $this->posttype_slugs['sponsors'],
                 'category'   => $categorySlug,
                 'pagination' => false,
                 'limit'      => 100,
            ]));
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function sectionMembers()
    {
        if ($this->sectionMembersPosttype()->variables['membersPosttype']->exists) {
            $this->sectionMembersGroups();
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function sectionMembersPosttype()
    {
        return $this->appendToVariables('membersPosttype', Posttype::firstOrNew([
             'slug' => $this->posttype_slugs['members'],
        ]));
    }



    /**
     * @return $this
     */
    protected function sectionMembersGroups()
    {
        return $this->sectionMembersCategory('directors')
                    ->sectionMembersCategory('trustees')
                    ->sectionMembersCategory('founders')
             ;
    }



    /**
     * @param string $categorySlug
     *
     * @return $this
     */
    protected function sectionMembersCategory($categorySlug)
    {
        if (
             ($category = $this->variables['membersPosttype']->categories()->whereSlug($categorySlug)->first()) and
             ($posts = PostsServiceProvider::collectPosts([
                  'type'       => $this->posttype_slugs['members'],
                  'category'   => $categorySlug,
                  'pagination' => false,
                  'limit'      => 100,
             ]))->count()
        ) {
            $this->variables['membersGroups'][] = collect([
                 'category' => $category,
                 'members'  => $posts,
            ]);
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function sectionCommittees()
    {
        return $this->appendToVariables('committees_static_post', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'committees',
        ])->first() ?: post());
    }
}
