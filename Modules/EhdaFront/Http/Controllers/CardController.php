<?php

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use App\Models\Role;
use App\Models\State;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Events\CardRegistered;
use Modules\EhdaFront\Http\Requests\CardRegisterRequest;
use Modules\EhdaFront\Providers\RouteServiceProvider;

class CardController extends EhdaFrontControllerAbstract
{
    public function index()
    {
        return $this->hideHeartIcon()
            ->findInformationPost()
            ->indexResult();
    }

    protected function findInformationPost()
    {
        $this->variables['post'] = Post::selector([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'organ-donation-card',
        ])->first();
        return $this;
    }

    protected function indexResult()
    {
        return ($this->variables['post'] and $this->variables['post']->exists)
            ? $this->selectStatesCombo()->template('card.card_info.main', $this->variables)
            : redirect(RouteServiceProvider::actionLocale('EhdaFrontController@index'));
    }

    public function register(CardRegisterRequest $request)
    {
        switch ($request->_step) {
            case 1:
            case 2:
            case 3:
                $method = 'registerStep' . $request->_step;
                return $this->$method($request);

            default:
                return $this->abort(403);
        }
    }

    protected function registerStep1(CardRegisterRequest $request)
    {
        $checkResult = $this->checkCodeMelli($request->code_melli);
        if (!$checkResult['canRegister']) {
            return $checkResult['response'];
        }

        // @TODO: verify "code_melli" with "name_first" and "name_last"
        session()->put('register_card.' . $request->code_melli, ['verified' => true, 'step' => 1]);

        return $this->jsonFeedback(null, [
            'ok'       => 1,
            'callback' => <<<JS
                upToStep(2);
JS
            ,
            'message'  => trans('ehdafront::forms.feed.wait'),
        ]);
    }

    protected function registerStep2(CardRegisterRequest $request)
    {
        if (!session()->has("register_card.$request->code_melli")) { // This "code_melli" hasn't been submitted
            // Achieving this condition means that "code_melli" has been manipulated in read only mode by client
            return $this->jsonFeedback(null, [
                'ok'      => 0,
                'refresh' => 1,
            ]);
        }

        $checkResult = $this->checkCodeMelli($request->code_melli);
        if (!$checkResult['canRegister']) {
            return $checkResult['response'];
        }


        $state         = State::find($request->home_city);
        $modifyingData = [
            'password'              => Hash::make($request->password),
            'home_province'         => $state->province()->id,
//            'domain'                => $state->domain->slug,
            'domain'                => null, // @todo: domain should be set properly
            'password_force_change' => 0,
        ];

        $request->merge($modifyingData);

        $input = $request->all();
        unset(
            $input['_token'],
            $input['_submit'],
            $input['password2']
        );
        session()->put("register_card.$request->code_melli.step", 2);
        session()->put("register_card.$request->code_melli.data", $input);

        return $this->jsonFeedback(trans('ehdafront::forms.feed.register_check_data_step_second'), [
            'ok'       => 1,
            'callback' => <<<JS
                upToStep(3);
JS
            ,
        ]);
    }

    protected function registerStep3(CardRegisterRequest $request)
    {
        if (!session()->has("register_card.$request->code_melli")
            // This "code_melli" hasn't been submitted
            or
            !session()->has("register_card.$request->code_melli.data")
            // This client didn't pass step 2 successfully
        ) {
            // Achieving this condition means that "code_melli" has been manipulated in read only mode by client
            return $this->jsonFeedback(null, [
                'ok'      => 0,
                'refresh' => 1,
            ]);
        }

        $checkResult = $this->checkCodeMelli($request->code_melli);
        if (!$checkResult['canRegister']) {
            return $checkResult['response'];
        }

        $data                       = session()->get("register_card.$request->code_melli.data");
        $data['card_registered_at'] = Carbon::now()->toDateTimeString();

        $userId = model('user')->batchSaveId($data);


        if ($userId) {
            $user = user()->grabId($userId);
            $user->setCardNumber();

            // @todo: maybe we should make another action if role doesn't exist
            if (role('card-holder')->exists) {
                $user->attachRole('card-holder');
            }

            event(new CardRegistered($user));

            Auth::loginUsingId($userId);

            return $this->jsonFeedback(null, [
                'redirect'     => RouteServiceProvider::actionLocale('UserController@index'),
                'ok'           => 1,
                'message'      => trans('ehdafront::forms.feed.register_success'),
                'redirectTime' => 2000,
            ]);
        } else {
            return $this->jsonFeedback(null, [
                'redirect'     => RouteServiceProvider::actionLocale('CardController@index'),
                'ok'           => 0,
                'message'      => trans('ehdafront::forms.feed.register_not_complete'),
                'redirectTime' => 2000,
            ]);
        }
    }

    private function checkCodeMelli($codeMelli)
    {
        $user = user()->findByUsername($codeMelli);

        if ($user->exists) { // A user with the given "code_melli" exists.
            return [
                'canRegister' => false,
                'response'    => $this->jsonFeedback(null, [
                    'ok'         => 0,
                    'message'    => $this->safeTemplate(
                        'card.registration_response.user_existance_message',
                        compact('user')
                    )->render(),
                    'feed_class' => 'blue',
                ]),
            ];
        }

        return ['canRegister' => true];
    }
}
