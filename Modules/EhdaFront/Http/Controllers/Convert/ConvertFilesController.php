<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/14/18
 * Time: 10:53 PM
 */

namespace Modules\EhdaFront\Http\Controllers\Convert;

use App\Models\File;
use Modules\EhdaFront\Entities\Convert\OldFile;
use Modules\EhdaFront\Entities\Convert\OldPosttype;
use Modules\Filemanager\Providers\UploadServiceProvider;
use Symfony\Component\HttpFoundation\File\File as SymfonyFile;
use Illuminate\Support\Facades\File as FileFacades;

class ConvertFilesController extends ConvertControllerAbstract
{
    protected $model = OldFile::class;

    protected $posttypes = [];

    protected $configs_slugs = [
         'featured_image_upload_configs',
         'attached_files_upload_configs',
    ];

    protected $limit = 100;



    protected function additionalConditions($queryBuilder)
    {
        return $queryBuilder->whereNotNull('posttype')
                            ->whereIn('posttype', $this->convertingPosttypesIds())
             ;
    }



    protected function convertModel($model)
    {
        if ($newPosttype = $this->newPosttype($model)) {
            $newFile = $this->updateFile($model, $newPosttype);

            if (
                 ($configsDirectory = $this->getConfigsDirectory($newFile)) and
                 //(($mainDirectory = $this->getFileMainDirectory($model)) != $configsDirectory) and
                 ($newFile->directory != ($newDir = $this->fileDirectoryToBe($newFile, $configsDirectory)))
            ) {
                $this->moveFile($newFile, $newDir);
            } else {
                $newFile->update(['converted' => 1]);
                $this->logs[] = "File #$newFile->id No Need To Move";
            }

            $this->updateOldFile($model);
        } else {
            dd($model, $this->newPosttype($model), __FILE__ . ' - ' . __LINE__);
        }
    }



    protected function updateFile($oldFile, $posttype = null)
    {
        $newRow = (new File)
             ->fill(array_merge(
                  array_except($oldFile->getAttributes(), ['id', 'folder', 'category', 'posttype', 'type']),
                  ['posttype' => ($posttype ?: $this->newPosttype($oldFile))->id]
             ));

        $newRow->id   = $oldFile->id;
        $newRow->type = $oldFile->type ?: explode('//', $oldFile->mime_type)[0];

        if (
             $oldFile->meta and
             (
                  is_array($metaArray = $oldFile->meta) or
                  is_array($metaArray = json_decode($oldFile->meta, true))
             )
        ) {
            $newRow->meta = $metaArray;
        }
        $newRow->save();

        return $newRow;
    }



    protected function newPosttype($oldFile)
    {
        return (
             ($oldPosttype = $oldFile->posttype()->first()) and
             ($newPosttypeSlug = $this->mapPosttype($oldPosttype->slug)) and
             ($newPosttype = $this->getPosttype($newPosttypeSlug)) and
             $newPosttype->exists
        )
             ? $newPosttype
             : null;
    }



    protected function getConfigsDirectory($model)
    {
        $posttype = $this->getFilePosttype($model)->spreadMeta();
        foreach ($this->configs_slugs as $slug) {
            if (
                 $posttype->$slug and
                 ($configs = $posttype->getUploadConfigs($slug)) and
                 is_array($configs) and
                 count($configs)
            ) {
                return $configs['uploadDir'];
            }
        }

        return null;
        return UploadServiceProvider::getSectionRule('default', 'uploadDir');
    }



    protected function getFileMainDirectory($file)
    {
        $innerDirectory = str_after($file->directory, UploadServiceProvider::getRootUploadDir() . DIRECTORY_SEPARATOR);
        if ($inTemp = $file->is_in_temporary_folder) {
            $innerDirectory = str_after($file->directory,
                 UploadServiceProvider::getTemporaryFolderName() . DIRECTORY_SEPARATOR);
        }
        return str_before($innerDirectory, DIRECTORY_SEPARATOR . $file->type);
    }



    protected function getFilePosttype($file)
    {
        if (!array_has($this->posttype_slugs, $posttypeId = $file->posttype)) {
            $this->posttypes[$posttypeId] = model('posttype', $posttypeId);
        }

        return $this->posttypes[$posttypeId];
    }



    protected function fileDirectoryToBe($file, $configDirectory)
    {
        $newDirParts = [UploadServiceProvider::getRootUploadDir()];
        if ($inTemp = $file->is_in_temporary_folder) {
            $newDirParts[] = UploadServiceProvider::getTemporaryFolderName();
        }
        $newDirParts[] = $configDirectory;
        $newDirParts[] = $file->type;
        $newDirParts   = array_map(function ($part) {
            return trim($part, DIRECTORY_SEPARATOR);
        }, $newDirParts);
        return implode(DIRECTORY_SEPARATOR, $newDirParts);
    }



    protected function moveFile($file, $newDir, $mainDirectory = null)
    {
        $newFile = new SymfonyFile($file->pathname);
        if ($file->related_files) {
            foreach ($file->related_files as $key => $relatedFileName) {
                $relatedFilePathname = implode(DIRECTORY_SEPARATOR, [
                     $file->directory,
                     $relatedFileName,
                ]);
                if (FileFacades::exists($relatedFilePathname)) {
                    $relatedFile = new SymfonyFile($relatedFilePathname);
                    $relatedFile->move($newDir);
                }
            }
        }

        $newFile->move($newDir);
        $file->directory = $newDir;
        $file->status    = $file->is_in_temporary_folder ? 1 : 2;
        $file->converted = 1;

        $file->save();

        $this->logs[] = "File #$file->id Moved";
    }



    protected function updateOldFile($oldFile)
    {
        $oldFile->update(
             [
                  'converted' => 1,
             ]
        );
    }



    protected function convertingPosttypesIds()
    {
        return array_map(function ($slug) {
            return OldPosttype::whereSlug($slug)->first()->id;
        }, array_keys($this->posttypesMap()));
    }
}
