<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/14/18
 * Time: 10:53 PM
 */

namespace Modules\EhdaFront\Http\Controllers\Convert;

use App\Models\State;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Modules\EhdaFront\Providers\RouteServiceProvider;

class ConvertUsersController extends ConvertControllerAbstract
{
    protected $model = 'user';

    protected $limit = 1000;



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function eduProvince()
    {
        return $this->template(
             $this->index_view,
             [
                  'ajaxCallsUrl' => RouteServiceProvider::action(
                       'Convert\ConvertUsersController@convertEduProvince',
                       ['offset' => '__OFFSET__']
                  ),
             ]
        );
    }



    /**
     * @param Request $request
     *
     * @return string
     */
    public function convertEduProvince(Request $request)
    {
        //return '';

        if (!$this->convertEduProvinceQuery()->limit(1)->count()) {
            return '';
        }

        $models = $this->convertEduProvinceQuery()
                       ->limit($this->limit)
                       //->offset($request->offset * $this->limit)
                       ->get()
        ;

        foreach ($models as $model) {
            $this->convertModelEduProvince($model);
        }

        return (implode('<br>', $this->logs)
             ?: 'Nothing to Change from '
             . $models->first()->id
             . ' to '
             . $models->last()->id
             . '.'
        );
    }



    /**
     * @return Builder
     */
    protected function convertEduProvinceQuery()
    {
        return model($this->model)->withTrashed()
                                  ->where('edu_city', '<>', 0)
                                  ->whereNotNull('edu_city')
                                  ->whereNull('edu_province')
             ;
    }



    /**
     * @param User $user
     *
     * @return void
     */
    protected function convertModelEduProvince(User $user)
    {
        $city_model = $this->findCityModel($user);
        if ($city_model) {
            $user->update([
                 'edu_province' => $city_model->province->id,
            ]);
            $this->logs[] = "User #$user->id converted!";
        }
    }



    /**
     * @param User $user
     *
     * @return null|State
     */
    protected function findCityModel(User $user)
    {
        $city_id = $user->edu_city;
        if ($city_id) {
            return model('state')->where('parent_id', '<>', 0)->whereId($city_id)->first();
        }

        return null;
    }
}
