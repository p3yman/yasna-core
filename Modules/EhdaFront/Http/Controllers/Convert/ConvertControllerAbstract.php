<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/10/18
 * Time: 11:43 AM
 */

namespace Modules\EhdaFront\Http\Controllers\Convert;

use Illuminate\Http\Request;
use Modules\EhdaFront\Http\Controllers\EhdaFrontControllerAbstract;

abstract class ConvertControllerAbstract extends EhdaFrontControllerAbstract
{
    protected $limit = 10;
    protected $logs  = [];

    protected $index_view = 'convert.main';



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this->template($this->index_view);
    }



    public function convert(Request $request)
    {
        $queryBuilder = $this->model::withTrashed()
                                    ->limit($this->limit)
                                    ->whereConverted(false)
            //->offset($request->offset)
        ;
        $models       = $this->additionalConditions($queryBuilder)->get();

        foreach ($models as $model) {
            $this->convertModel($model);
        }

        return implode('<br>', $this->logs);
    }



    protected function additionalConditions($queryBuilder)
    {
        return $queryBuilder;
    }



    protected function getPosttype($posttypeSlug)
    {
        if (!isset($this->fetched_posttypes[$posttypeSlug])) {
            $this->fetched_posttypes[$posttypeSlug] = model('posttype')
                 ->whereSlug($posttypeSlug)
                 ->first()
                 ?: model('posttype')
                      ?: model('posttype');
        }

        return $this->fetched_posttypes[$posttypeSlug];
    }



    protected function getCategory($posttypeSlug, $categorySlug)
    {
        if (!isset($this->fetched_categories[$posttypeSlug][$categorySlug])) {
            $this->fetched_categories[$posttypeSlug][$categorySlug] =
                 $this->getPosttype($posttypeSlug)
                      ->categories()
                      ->whereSlug($categorySlug)
                      ->whereIsFolder(false)
                      ->first()
                      ?: model('category');
        }

        return $this->fetched_categories[$posttypeSlug][$categorySlug];
    }



    protected function getFolderCategory($posttypeSlug, $folderSlug, $categorySlug)
    {
        if (!isset($this->fetched_categories[$posttypeSlug][$folderSlug][$categorySlug])) {
            $this->fetched_categories[$posttypeSlug][$folderSlug][$categorySlug] =
                 $this->getFolder($posttypeSlug, $folderSlug)->children()
                      ->whereSlug($categorySlug)
                      ->whereIsFolder(false)
                      ->first()
                      ?: model('category');
        }

        return $this->fetched_categories[$posttypeSlug][$folderSlug][$categorySlug];
    }



    protected function getFolder($posttypeSlug, $folderSlug)
    {
        if (!isset($this->fetched_categories[$posttypeSlug][$folderSlug])) {
            $this->fetched_categories[$posttypeSlug][$folderSlug] =
                 $this->getPosttype($posttypeSlug)
                      ->categories()
                      ->whereSlug($folderSlug)
                      ->whereIsFolder(true)
                      ->first()
                      ?: model('category');
        }

        return $this->fetched_categories[$posttypeSlug][$folderSlug];
    }



    protected function mapPosttype($slug)
    {
        return (array_key_exists($slug, $map = $this->posttypesMap()))
             ? $map[$slug]
             : null;
    }



    protected function posttypesMap()
    {
        return [
             'faq'          => $this->posttype_slugs['faq'],
             'statics'      => $this->posttype_slugs['statics'],
             'event'        => $this->posttype_slugs['events'],
             'slideshows'   => $this->posttype_slugs['slideshows'],
             'gallery'      => $this->posttype_slugs['gallery-image'],
             'iran-news'    => $this->posttype_slugs['news'],
             'word-news'    => $this->posttype_slugs['news'],
             'angels'       => $this->posttype_slugs['angels'],
             'celebs'       => $this->posttype_slugs['celebrities'],
             'states-logos' => $this->posttype_slugs['states-logos'],
        ];
    }
}
