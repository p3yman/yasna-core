<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/10/18
 * Time: 11:42 AM
 */

namespace Modules\EhdaFront\Http\Controllers\Convert;

use App\Models\File;
use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Http\UploadedFile;
use Modules\EhdaFront\Entities\Convert\OldFile;
use Modules\EhdaFront\Entities\Convert\OldPost;
use Modules\EhdaFront\Entities\Convert\OldPosttype;
use Modules\Filemanager\Providers\FileServiceProvider;
use Modules\Filemanager\Providers\UploadServiceProvider;

class ConvertPostsController extends ConvertControllerAbstract
{
    protected $model                = '\Modules\EhdaFront\Entities\Convert\OldPost';
    protected $posttypes_to_convert = [
         'faq',
         'statics',
         'event',
         'slideshows',
         'gallery',
         'iran-news',
         'word-news',
         'angels',
         'celebs',
         'states-logos',
    ];
    protected $limit                = 50;

    protected $fetched_categories        = [];
    protected $fetched_posttypes         = [];
    protected $fetched_folder_categories = [];
    protected $fetched_folders           = [];
    protected $meta_seeded_prefix        = 'meta-';
    protected $default_domain            = 'tehran';

    protected $featured_image_variables = [];



    public function __construct()
    {
        UploadServiceProvider::disableTemporaryUpload();
        UploadServiceProvider::setCurrentConfigSlug('featured_image');
        $typeString                                    = 'posttype__products.image';
        $typeStringParts                               = explode('.', $typeString);
        $this->featured_image_variables['sectionName'] = implode('.',
             array_slice($typeStringParts, 0, count($typeStringParts) - 1));
        $this->featured_image_variables['folderName']  = array_last($typeStringParts);
        UploadServiceProvider::setConfigsSection($typeStringParts[0]);
        parent::__construct();
    }



    protected function additionalConditions($queryBuilder)
    {
        return $queryBuilder->whereIn('type', $this->posttypes_to_convert);
    }



    protected function convertModel($model)
    {
        if (method_exists($this, $methodName = 'convert' . studly_case($model->type))) {
            $newModel = $this->$methodName($model);
        } else {
            $newModel = $this->convertModelGeneral($model);
        }
        $newModel = $this->convertDomains($newModel, $model);

        $model->update(['converted' => 1]);

        if ($newModel->exists) {
            $this->logs[] = "Post #$model->id in type\"$newModel->type\" converted successfully";
        }
    }



    protected function convertDomains($newModel, $oldModel)
    {
        if ($oldModel->domains and ($oldDomains = $oldModel->domains_array) and count($oldDomains)) {
            $domainsSlugs = array_unique(array_map(function ($domainSlug) {
                return $this->mapDomains($domainSlug);
            }, $oldDomains));

            if (in_array($this->default_domain, $domainsSlugs)) {
                $mainDomainSlug = $this->default_domain;
            }


            foreach ($domainsSlugs as $domainsSlug) {
                if (($domainRow = model('domain', $domainsSlug))->exists) {
                    if (isset($mainDomainSlug)) {
                        if (($domainRow->slug == $mainDomainSlug)) {
                            $isMain = true;
                        } else {
                            $isMain = false;
                        }
                    } else {
                        $mainDomainSlug = $domainRow->slug;
                        $isMain         = true;
                    }
                    if ($isMain) {
                        $mainDomain = $domainRow;
                    }
                    $newModel->reflect($domainRow->id, $isMain);
                }
            }

            if (isset($mainDomain)) {
                $newModel->update(['domain_id' => $mainDomain->id]);
            }
        }

        return $newModel;
    }



    protected function mapDomains($domain)
    {
        switch ($domain) {
            case 'global':
                $domain = 'tehran';
                break;
        }

        return $domain;
    }



    protected function convertFeaturedImage($newModel, $oldModel)
    {
        if ($oldPath = $oldModel->featured_image) {
            if (($oldRow = $this->findFileByPathname($oldPath)) and $oldRow->exists) {
                //$newRow = $this->migrateFileRow($oldRow, $this->getPosttype($newModel->type));
                $newRow = model('file', $oldRow->id);
                if ($newRow->exists) {
                    $newModel->update(['featured_image' => $newRow->hashid]);
                }
            } else {
                $posttype = model('posttype', $newModel->type);

                if ($uploadedFile = $this->uploadFile($oldPath, $posttype)) {
                    $newModel->update(['featured_image' => $uploadedFile->hashid]);
                }
            }
        }

        return $newModel;
    }



    protected function uploadFile($oldPath, $posttype)
    {
        $externalFields = ['posttype' => $posttype->id];

        $uploadDir = implode(DIRECTORY_SEPARATOR, [
             UploadServiceProvider::getSectionRule($this->featured_image_variables['sectionName'], 'uploadDir'),
             $this->featured_image_variables['folderName'],
        ]);

        $pathname  = starts_with($oldPath, '/')
             ? str_after($oldPath, '/')
             : $oldPath;
        $imageFile = FileServiceProvider::getFileObject($pathname);

        if ($imageFile) {
            $name = str_random(8) . '.' . $imageFile->getExtension();
            $path = sys_get_temp_dir() . '/' . $name;
            copy($pathname, $path);
            $file           = new UploadedFile($path, $name, $imageFile->getMimeType(), filesize($path), null,
                 true);
            $uploadedFileId = UploadServiceProvider::uploadFile($file, $uploadDir, $externalFields);
            $uploadedFile   = File::find($uploadedFileId);
            return $uploadedFile;
        }

        return null;
    }



    protected function findFileByPathname($pathname)
    {
        if (starts_with($pathname, '/')) {
            $pathname = str_after($pathname, '/');
        }

        $conditions['directory']     = pathinfo($pathname, PATHINFO_DIRNAME);
        $conditions['physical_name'] = pathinfo($pathname, PATHINFO_BASENAME);
        $file                        = OldFile::where($conditions)->first();

        if ($file and $file->exists) {
            return $file;
        }

        return null;
    }



    protected function migrateFileRow($oldRow, $posttype)
    {
        if (
             $oldRow->converted and
             ($newId = $oldRow->meta('new_id')) and
             ($newRow = File::find($newId))
        ) {
            return $newRow;
        } else {
            $newRow = (new File)
                 ->fill(array_merge(
                      array_except($oldRow->getAttributes(), ['id', 'folder', 'category', 'posttype']),
                      ['posttype' => $this->newFilePosttype($oldRow, $posttype)->id]
                 ));
            if (
                 $oldRow->meta and
                 (
                      is_array($metaArray = $oldRow->meta) or
                      is_array($metaArray = json_decode($oldRow->meta, true))
                 )
            ) {
                $newRow->meta = $metaArray;
            }
            $newRow->save();
            $this->updateOldFile($oldRow, $newRow->id);

            return $newRow;
        }
    }



    protected function updateOldFile($oldFile, $newId)
    {
        $meta =
             (
                  $oldFile->meta and (
                       is_array($decoded = $oldFile->meta) or
                       is_array($decoded = json_decode($oldFile->meta, true))
                  )
             )
                  ? $decoded
                  : [];

        $oldFile->update(
             [
                  'meta'      => array_merge($decoded, [
                       'new_id' => $newId,
                  ]),
                  'converted' => 1,
             ]
        );
    }



    protected function newFilePosttype($oldFile, $defaultPosttype)
    {
        return (
             ($oldPosttype = $oldFile->posttype()->first()) and
             ($newPosttypeSlug = $this->mapPosttype($oldPosttype->slug)) and
             ($newPosttype = $this->getPosttype($newPosttypeSlug)) and
             $newPosttype->exists
        )
             ? $newPosttype
             : $defaultPosttype;
    }



    protected function convertModelGeneral($model)
    {
        dd($model, __FILE__ . ' - ' . __LINE__);
    }



    protected function convertFaq($model)
    {
        $model = $this->insert(array_merge(
             $this->insertingMetaFields($model, ['option_true', 'option_wrong_1', 'option_wrong_2', 'option_wrong_3']),
             $this->generalConvertData($model),
             ['type' => $this->posttype_slugs['faq']]
        ));

        $model->categories()->sync([$this->getCategory('faq', 'general')->id]);

        return $model;
    }



    protected function convertStatics($model)
    {
        $newModel = $this->insert(array_merge(
             $this->insertingMetaFields($model, ['option_true', 'option_wrong_1', 'option_wrong_2', 'option_wrong_3']),
             $this->generalConvertData($model),
             ['type' => $this->posttype_slugs['statics']]
        ));

        return $this->convertFeaturedImage($newModel, $model);
    }



    protected function convertEvent($model)
    {
        $newModel = $this->insert(array_merge(
             $this->insertingMetaFields($model, [
                  'option_true',
                  'option_wrong_1',
                  'option_wrong_2',
                  'option_wrong_3',
                  'can_register_card',
             ]),
             $this->generalConvertData($model),
             ['type' => $this->posttype_slugs['events']]
        ));

        $newModel = $this->convertFeaturedImage($newModel, $model);
        $newModel = $this->manageEventRegisterCategory($newModel, $model);

        return $this->saveCategories($newModel, $model);
    }



    protected function manageEventRegisterCategory($newModel, $oldModel)
    {
        if ($oldModel->meta('can_register_card')) {
            $newModel->categories()->attach(
                 $this->getCategory($this->posttype_slugs['events'], 'registrable')->id
            )
            ;
        }

        return $newModel;
    }



    protected function convertSlideshows($model)
    {
        $newModel = $this->insert(array_merge(
             $this->insertingMetaFields($model, ['option_true', 'option_wrong_1', 'option_wrong_2', 'option_wrong_3']),
             $this->generalConvertData($model),
             ['type' => $this->posttype_slugs['slideshows']]
        ));

        $newModel = $this->convertFeaturedImage($newModel, $model);

        return $this->saveCategories($newModel, $model);
    }



    protected function convertGallery($model)
    {
        $newModel = $this->insert(array_merge(
             $this->generalConvertData($model),
             $this->insertingMetaFields($model, [
                  'option_true',
                  'option_wrong_1',
                  'option_wrong_2',
                  'option_wrong_3',
                  'post_files',
                  'externalFields',
             ]),
             $this->attachedFilesConvertData($model, $this->getPosttype($this->posttype_slugs['gallery-image'])),
             ['type' => $this->posttype_slugs['gallery-image']]
        ));

        $newModel = $this->convertFeaturedImage($newModel, $model);

        return $this->saveCategories($newModel, $model);
    }



    protected function convertIranNews($model)
    {
        $newModel = $this->insert(array_merge(
             $this->generalConvertData($model),
             $this->insertingMetaFields($model, [
                  'option_true',
                  'option_wrong_1',
                  'option_wrong_2',
                  'option_wrong_3',
             ]),
             ['type' => $this->posttype_slugs['news']]
        ));

        $newModel = $this->convertFeaturedImage($newModel, $model);

        return $this->saveNewsCategories($newModel, $model, 'iran');
    }



    protected function convertWordNews($model)
    {
        $newModel = $this->insert(array_merge(
             $this->generalConvertData($model),
             $this->insertingMetaFields($model, [
                  'option_true',
                  'option_wrong_1',
                  'option_wrong_2',
                  'option_wrong_3',
             ]),
             ['type' => $this->posttype_slugs['news']]
        ));

        $newModel = $this->convertFeaturedImage($newModel, $model);

        return $this->saveNewsCategories($newModel, $model, 'world');
    }



    protected function convertAngels($model)
    {
        $newModel = $this->insert(array_merge(
             $this->generalConvertData($model),
             $this->insertingMetaFields($model, [
                  'option_true',
                  'option_wrong_1',
                  'option_wrong_2',
                  'option_wrong_3',
             ]),
             ['type' => $this->posttype_slugs['angels']]
        ));

        return $this->convertFeaturedImage($newModel, $model);
    }



    protected function convertCelebs($model)
    {
        $newModel = $this->insert(array_merge(
             $this->generalConvertData($model),
             $this->insertingMetaFields($model, [
                  'option_true',
                  'option_wrong_1',
                  'option_wrong_2',
                  'option_wrong_3',
             ]),
             ['type' => $this->posttype_slugs['celebrities']]
        ));

        return $this->convertFeaturedImage($newModel, $model);
    }



    protected function convertStatesLogos($model)
    {
        $newModel = $this->insert(array_merge(
             $this->generalConvertData($model),
             $this->insertingMetaFields($model, [
                  'option_true',
                  'option_wrong_1',
                  'option_wrong_2',
                  'option_wrong_3',
             ]),
             ['type' => $this->posttype_slugs['states-logos']]
        ));

        return $this->convertFeaturedImage($newModel, $model);
    }



    protected function saveNewsCategories($newModel, $oldModel, $folder)
    {
        foreach ($oldModel->categories as $sourceCategory) {
            $targetCategory = $this->getFolderCategory($newModel->type, $folder, $sourceCategory->slug);
            if (
                 $targetCategory and
                 $targetCategory->slug and
                 !$newModel->categories()->whereCategoryId($targetCategory->id)->count()
            ) {
                $newModel->categories()->attach($targetCategory->id);
            }
        }
        $newModel->updateCategoryCache($newModel->default_category_model->id);

        return $newModel;
    }



    protected function attachedFilesConvertData($model, $posttype)
    {
        $files = [];
        if ($model->files and count($model->files)) {
            foreach ($model->files as $file) {
                $oldRow = OldFile::findByHashid($file['src']);

                if (
                     $oldRow->exists and
                     //($newRow = $this->migrateFileRow($oldRow, $posttype))->exists
                     ($newRow = model('file', $oldRow->id))->exists
                ) {
                    $files[] = array_merge($file, ['src' => $newRow->hashid]);
                    //} else {
                    //    $files[] = $file;
                }
            }
            return ['meta-post_files' => $files];
        } else {
            return [];
        }
    }



    protected function saveCategories($newModel, $oldModel)
    {
        foreach ($oldModel->categories as $sourceCategory) {
            $targetCategory = $this->getCategory($newModel->type, $sourceCategory->slug);
            if (
                 $targetCategory and
                 $targetCategory->slug and
                 !$newModel->categories()->whereCategoryId($targetCategory->id)->count()
            ) {
                $newModel->categories()->attach($targetCategory->id);
            }
        }
        $newModel->updateCategoryCache($newModel->default_category_model->id);

        return $newModel;
    }



    protected function insertingMetaFields($model, $except = [])
    {
        $insertingData = [];
        if (
             $model->meta and
             (
                  is_array($metaArray = $model->meta) or
                  is_array(($metaArray = json_decode($model->meta, true)))
             )
        ) {
            foreach ($metaArray as $key => $value) {
                if ($value and !in_array($key, $except)) {
                    $insertingData[$this->meta_seeded_prefix . $key] = $value;
                }
            }
        }

        return $insertingData;
    }



    protected function generalConvertData($model)
    {
        return [
             'id'              => $model->id,
             'slug'            => $model->slug,
             'title'           => $model->title,
             'locale'          => $model->locale,
             'is_draft'        => $model->is_draft,
             'copy_of'         => $model->copy_of,
             'sisterhood'      => $model->sisterhood,
             'text'            => $this->convertPostText($model->text,
                  $this->getPosttype($this->mapPosttype($model->type))),
             'abstract'        => $model->abstract,
             'event_starts_at' => $model->starts_at,
             'event_ends_at'   => $model->ends_at,
             'title2'          => $model->title2,
             'long_title'      => $model->long_title,
             'pinned_at'       => $model->pinned_at,
             'pinned_by'       => $model->pinned_by,
             'created_at'      => $model->created_at,
             'created_by'      => $model->created_by,
             'updated_at'      => $model->updated_at,
             'updated_by'      => $model->updated_by,
             'published_at'    => $model->published_at,
             'published_by'    => $model->published_by,
             'deleted_at'      => $model->deleted_at,
             'deleted_by'      => $model->deleted_by,
             'moderated_at'    => $model->moderated_at,
             'moderated_by'    => $model->moderated_by,
             'owned_by'        => $model->owned_by,
        ];
    }



    protected function insert($data)
    {
        $metaFields = array_filter($data, function ($key) {
            return starts_with($key, $this->meta_seeded_prefix);
        }, ARRAY_FILTER_USE_KEY);

        $otherFields = array_except($data, array_keys($metaFields));

        $model = model('post');

        if (array_key_exists('id', $data)) {
            $model->id = $data['id'];
        }

        $meta = [];
        foreach ($metaFields as $key => $value) {
            $meta[str_after($key, $this->meta_seeded_prefix)] = $value;
        }

        $model->fill(array_merge($otherFields, ['meta' => $meta]))->save();

        return $model;
    }



    protected function convertPostText($text, $posttype)
    {
        preg_match('/src=\"[^">]+\"/', $text, $result);
        $srcs = array_map(function ($img) {
            return str_before(str_after($img, 'src="'), '"');
        }, $result);

        //$doc = new \DOMDocument();
        //$doc->loadHTML($text);
        //foreach ($doc->getElementsByTagName('img') as $img) {
        //    $srcs[] = $img->getAttribute("src");
        //}

        foreach ($srcs as $src) {
            $clearSrc = trim($src, '/');
            if (starts_with($clearSrc, 'photos')) {
                if ($newFile = $this->uploadFile($clearSrc, $posttype)) {
                    $text = str_replace(
                         $src,
                         '/' . $newFile->pathname,
                         $text
                    );
                }
            } else {
                if (starts_with($clearSrc, 'uploads')) {
                    $srcParts            = explode('/', $clearSrc);
                    $fileName            = array_last($srcParts);
                    $originalVersionName = UploadServiceProvider::changeFileNameVersion($fileName, 'original');
                    if ($fileRow = model('file')->wherePhysicalName($originalVersionName)->first()) {
                        $text = str_replace(
                             $src,
                             '/' . $fileRow->pathname,
                             $text
                        );
                    }
                }
            }
        }

        return $text;
    }
}
