<?php

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Http\Request;
use Modules\EhdaFront\Providers\GalleryServiceProvider;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class GalleryController extends EhdaFrontControllerAbstract
{
    public function visual(Request $request)
    {
        return $this->findVisualPosttypes()
             ? $this->template('gallery.categories.categories', $this->variables)
             : $this->abort(404);
    }



    protected function findVisualPosttypes()
    {
        $visualGalleryExists = false;
        foreach (GalleryServiceProvider::getVisualGalleries() as $gallery) {
            $posttype = PostsServiceProvider::smartFindPosttype(GalleryServiceProvider::typeToPosttype($gallery));
            if ($posttype->exists) {
                $this->variables['posttypes'][$gallery] = $posttype;
                $visualGalleryExists                    = true;
            }
        }

        return $visualGalleryExists;
    }
}
