<?php

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Yasna\Http\Controllers\YasnaController;

class EhdaFrontTestController extends EhdaFrontControllerAbstract
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function galleryCat()
    {
        return $this->template('ehdafront::gallery.categories.categories');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function galleryList()
    {
        return $this->template('ehdafront::gallery.list.list');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function galleryVideoSingle()
    {
        return $this->template('ehdafront::gallery.single.video.single');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function galleryImageSingle()
    {
        return $this->template('ehdafront::gallery.single.image.single');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function audioSingle()
    {
        return $this->template('ehdafront::gallery.single.audio.single');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newsList()
    {
        return $this->template('ehdafront::news.list.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function newsSingle()
    {
        return $this->template('ehdafront::news.single.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function eventsList()
    {
        return $this->template('ehdafront::events.list.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function eventsSingle()
    {
        return $this->template('ehdafront::events.single.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function galleryListText()
    {
        return $this->template('ehdafront::gallery.list.text.list');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return $this->template('ehdafront::test.contact.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function volunteerRegister()
    {
        return $this->template('ehdafront::register.volunteer.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function faq()
    {
        return $this->template('ehdafront::test.faq.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function support()
    {
        return $this->template('ehdafront::test.donation.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function organization()
    {
        return $this->template('ehdafront::test.organization.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generalInfo()
    {
        return $this->template('ehdafront::test.general-information.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function activities()
    {
        return $this->template('ehdafront::test.activities.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contractsList()
    {
        return $this->template('ehdafront::contracts.list.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function annualReports()
    {
        return $this->template('ehdafront::annual-reports.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function researchProjects()
    {
        return $this->template('ehdafront::research-projects.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function celebritiesList()
    {
        return $this->template('ehdafront::test.celebrities.list.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function celebritiesSingle()
    {
        return $this->template('ehdafront::test.celebrities.single.main');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pasargadLogin()
    {
        return $this->template('ehdafront::test.pasargad.login');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pasargadInquiry()
    {
        return $this->template('ehdafront::test.pasargad.inquiry');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pasargadRegister()
    {
        return $this->template('ehdafront::test.pasargad.register');
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function eduList()
    {
        return $this->template('ehdafront::test.education.main');
    }
}
