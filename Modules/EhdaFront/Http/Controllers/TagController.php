<?php

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends EhdaFrontControllerAbstract
{
    public function index(Request $request)
    {
        return $this->template('tag.posts.index', [
             'posts' => Tag::firstOrNew(['slug' => $tagSlug = $this->makeQueryTagReady($request->tag)])
                           ->posts()
                           ->paginate(24),
             'tag'   => $tagSlug,
        ]);
    }



    protected function makeQueryTagReady($tagSlug)
    {
        return trim(urldecode($tagSlug));
    }
}
