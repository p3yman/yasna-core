<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/03/2018
 * Time: 03:46 PM
 */

namespace Modules\EhdaFront\Http\Controllers\Traits;

trait EhdaFrontFeedbackControllerTrait
{
    use FeedbackControllerTrait {
        abort as protected traitAbort;
    }

    protected function abort($errorCode, $minimal = false, $data = null)
    {
        if (is_null($data)) {
            $data = $this->template(
                'errors' . ($minimal ? '.m' : '') . '.' . $errorCode,
                $this->variables
            );
        }
        return $this->traitAbort($errorCode, $minimal, $data);
    }
}
