<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 24/02/2018
 * Time: 17:30 AM
 */

namespace Modules\EhdaFront\Http\Controllers\Traits;

use App\Models\State;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\EhdaFront\Providers\ToolsServiceProvider;
use Modules\EhdaFront\Services\Template\EhdaFrontTemplateHandler as Template;

trait EhdaFrontTemplateTrait
{
    /**
     * Set needed template data and then get the evaluated view contents for the given view.
     *
     * @param  string $view
     * @param  array  $data
     * @param  array  $mergeData
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function template(...$parameters)
    {
        $this->readSettings()
             ->setOpenGraphs()
             ->readStateLogo()
             ->readMainMenuItems()
             ->readFooterMenuItems()
        ;

        return parent::template(...$parameters);
    }



    /**
     * @return $this
     */
    protected function readSettings()
    {
        return $this
             ->readSiteTitle()
             ->readContactInfo()
             ->readSocialLinks()
             ->readLocation()
             ->readYasnaInfo()
             ;
    }



    /**
     * @return $this
     */
    protected function readSiteTitle()
    {
        if ($siteTitle = get_setting('site_title')) {
            Template::setSiteTitle($siteTitle);
            Template::appendToPageTitle($siteTitle);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function readContactInfo()
    {
        if ($address = get_setting('address')) {
            Template::mergeWithContactInfo(['address' => $address]);
        }
        if (($telephone = get_setting('telephone')) and is_array($telephone) and count($telephone)) {
            Template::mergeWithContactInfo(['telephone' => $telephone]);
        }
        if (($email = get_setting('email')) and is_array($email) and count($email)) {
            Template::mergeWithContactInfo(['email' => $email]);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function readSocialLinks()
    {
        if ($telegram = get_setting('telegram_link')) {
            Template::mergeWithSocialLinks([
                 'telegram' => [
                      'link' => $telegram,
                      'icon' => 'telegram',
                 ],
            ]);
        }
        if ($twitter = get_setting('twitter_link')) {
            Template::mergeWithSocialLinks([
                 'twitter' => [
                      'link' => $twitter,
                      'icon' => 'twitter',
                 ],
            ]);
        }
        if ($fb = get_setting('facebook_link')) {
            Template::mergeWithSocialLinks([
                 'facebook' => [
                      'link' => $fb,
                      'icon' => 'facebook',
                 ],
            ]);
        }
        if ($instagram = get_setting('instagram_link')) {
            Template::mergeWithSocialLinks([
                 'instagram' => [
                      'link' => $instagram,
                      'icon' => 'instagram',
                 ],
            ]);
        }
        if ($aparat = get_setting('aparat_link')) {
            Template::mergeWithSocialLinks([
                 'aparat' => [
                      'link' => $aparat,
                      'icon' => 'aparat',
                 ],
            ]);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function readLocation()
    {
        if (($location = get_setting('location')) and is_array($location) and (count($location) == 2)) {
            Template::setLocation($location);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function readYasnaInfo()
    {
        Template::setYasnaTitle(
             get_setting('yasna-group-title')
                  ?: trans('ehdafront::general.footer.yasna_team')
        );
        Template::setYasnaUrl(get_setting('yasna-group-url') ?: '#');
        return $this;
    }



    /**
     * @return $this
     */
    protected function hideHeartIcon()
    {
        Template::setShowHeartIcon(false);
        return $this;
    }



    /**
     * @return $this
     */
    protected function setOpenGraphs()
    {
        Template::setOpenGraphs(array_default(Template::openGraphs(), [
             'title'       => Template::siteTitle(),
             'url'         => request()->url() .
                  (($queryString = request()->getQueryString()) ? ('?' . $queryString) : ''),
             'image'       => function () {
                 return Template::siteLogoUrl();
             },
             'description' => function () {
                 return get_setting('open_graph_default_description');
             },
        ]));
        return $this;
    }



    /**
     * @return $this
     */
    protected function selectStatesCombo()
    {
        Template::setStatesComboOptions(ToolsServiceProvider::getStatesComboHashid());
        return $this;
    }



    /**
     * @return $this
     */
    protected function readMainMenuItems()
    {
        Template::setMainMenu(menumaker()->get('main'));
        return $this;
    }



    /**
     * @return $this
     */
    protected function readFooterMenuItems()
    {
        Template::setFooterMenu(menumaker()->get('footer'));
        return $this;
    }



    /**
     * @return $this
     */
    protected function readStateLogo()
    {
        $state = getDomain();
        if (
             ($post = PostsServiceProvider::selectPosts([
                  'type' => $this->posttype_slugs['states-logos'],
                  'slug' => $state . '-logo',
             ])->first()) and
             ($image = doc($post->featured_image)->getUrl())
        ) {
            Template::setStateLogo($image);
        }

        return $this;
    }
}
