<?php
/**
 * Created by PhpStorm.
 * User: yasna
 * Date: 5/19/18
 * Time: 4:33 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Events\CardRegistered;
use Modules\EhdaFront\Http\Requests\PassargadRegisterRequest;
use Modules\EhdaFront\Http\Requests\PassargadSearchRequest;
use Modules\Yasna\Services\YasnaController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PassargadController
 *
 * @package Modules\EhdaFront\Http\Controllers
 */
class PassargadController extends YasnaController
{
    /**
     * @var string
     */
    protected $view_folder = 'ehdafront::';



    /**
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function inquiryForm()
    {
        if (!auth()->check()) {
            return redirect()->route('passargad_login');
        }

        if (session()->has('passargad_save_success')) {
            $saved_message = session()->get('passargad_save_success');
            $card_no       = session()->get('passargad_saved_card_no');
            session()->remove('passargad_save_success');
            session()->remove('passargad_saved_card_no');
        }
        return $this->view('passargad.inquiry', compact('saved_message', 'card_no'));
    }



    /**
     * Search for existed card with a specified code melli
     *
     * @param PassargadSearchRequest $request
     *
     * @return string
     */
    public function inquiry(PassargadSearchRequest $request)
    {
        if (!auth()->check()) {
            return redirect()->route('passargad_login');
        }

        $user = $this->findUser($request->code_melli);

        $callback = 'undefinedUser()';
        $ok       = 0;
        $message  = trans('ehdafront::passargad.code_melli.not_registered');
        if (isset($user->exists)) {
            $user_card_role = $user->hasRole($this->cardHolderRoleSlug());

            if ($user_card_role) {
                $callback = 'showUser(' . $user->card_no . ')';
                $ok       = 1;
                $message  = trans('ehdafront::passargad.code_melli.registered');
            }
        } else {
            session()->put('passargad_code_melli', $request->code_melli);
        }

        return $this->jsonFeedback('', compact('callback', 'ok', 'message'));
    }



    /**
     * @param null $code_melli
     *
     * @return \Illuminate\Support\Facades\View|Response
     */
    public function registerForm($code_melli = null)
    {
        if (!auth()->check()) {
            return redirect()->route('passargad_login');
        }

        $states = model('state')->where('parent_id', '!=', 0)->get();

        return $this->view('passargad.register', compact('states', 'code_melli'));
    }



    /**
     * Register user
     *
     * @param PassargadRegisterRequest $request
     *
     * @return string
     */
    public function register(PassargadRegisterRequest $request)
    {
        if (!auth()->check()) {
            return redirect()->route('passargad_login');
        }

        $homeProvince = null;
        $domain       = null;

        $message  = trans('ehdafront::passargad.registered_fail');
        $redirect = '';

        $request = $this->mergeRequest($request);

        $user = model('user')->batchSave($request->all());

        if ($user->exists) {
            $this->userExists($user);

            $redirect = route('passargad_inquiry');
            $message  = trans('ehdafront::passargad.registered_success');

            session()->put('passargad_save_success', $message);
            session()->put('passargad_saved_card_no', $user->card_no);
        }

        $ok = 1;
        return $this->jsonFeedback($message, compact('ok', 'redirect'));
    }



    /**
     * Find user with code melli
     *
     * @param $codeMelli
     *
     * @return object|null
     */
    protected function findUser($codeMelli)
    {
        return model('user')->where('code_melli', $codeMelli)->first();
    }



    /**
     * @return string
     */
    protected function cardHolderRoleSlug()
    {
        return 'card-holder';
    }



    /**
     * @param $user
     */
    private function userExists($user)
    {
        $user->update([
             'created_by' => user()->id,
        ]);

        $user->attachRole($this->cardHolderRoleSlug());
        $user->setCardNumber();
        event(new CardRegistered($user));

        session()->remove('passargad_code_melli');
    }



    /**
     * @param $request
     *
     * @return mixed
     */
    private function mergeRequest($request)
    {
        $state = model('State', $request->data['home_city']);
        if (is_object($state)) {
            $homeProvince = $state->province ?? null;
            if (is_object($homeProvince)) {
                $domain       = $homeProvince->domain->id ?? null;
                $homeProvince = $homeProvince->id;
            }
        }

        $request->merge([
             'password'              => Hash::make($request->data['mobile']),
             'home_province'         => $homeProvince,
             'domain'                => $domain,
             'password_force_change' => 1,
             'card_registered_at'    => Carbon::now(),
        ]);

        return $request;
    }
}
