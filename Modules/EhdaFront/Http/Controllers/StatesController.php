<?php

namespace Modules\EhdaFront\Http\Controllers;

use Modules\EhdaFront\Providers\PostsServiceProvider;

class StatesController extends EhdaFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function map()
    {
        return $this->selectStates()
                    ->findMapStaticPost()
                    ->generatePositionInfo()
                    ->hideHeartIcon()
                    ->template('map.iran.main', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function selectStates()
    {
        return $this->appendToVariables('states', model('domain')->all()->map(function ($domain) {
            $informationPost = PostsServiceProvider::selectPosts([
                 'type' => $this->posttype_slugs['states-logos'],
                 'slug' => $domain->slug . '-logo',
            ])->first() ?: post();
            $domain->info    = [
                 'address'   => $informationPost->spreadMeta()->address,
                 'telephone' => explode_not_empty(HTML_LINE_BREAK, $informationPost->telephone),
                 'email'     => explode_not_empty(HTML_LINE_BREAK, $informationPost->email),
            ];

            return $domain;
        }));
    }



    /**
     * @return $this
     */
    protected function findMapStaticPost()
    {
        return $this->appendToVariables('staticPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'states-info',
        ])->first() ?: model('post'));
    }



    /**
     * @return $this
     */
    protected function generatePositionInfo()
    {
        if (($infoPost = $this->variables['staticPost'])->exists) {
            $infoPost->spreadMeta();
            return $this->appendToVariables('positionInfo', [
                 'group'    => $infoPost->header_title,
                 'category' => $infoPost->category_title,
                 'title'    => $infoPost->title,
            ]);
        } else {
            return $this->appendToVariables('positionInfo', [
                 'group' => trans('ehdafront::genral.states_entrance'),
            ]);
        }
    }
}
