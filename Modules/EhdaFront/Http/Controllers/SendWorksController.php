<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/23/18
 * Time: 12:35 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Modules\EhdaFront\Providers\PostsServiceProvider;

class SendWorksController extends EhdaFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return count($this->findCommentingPosttype()->findCommentingPosts()->variables['commentingPostsInfo'])
             ? $this->template('send-works.main', $this->variables)
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function findCommentingPosttype()
    {
        return $this->appendToVariables('posttype', model('posttype', $this->posttype_slugs['commenting']));
    }



    /**
     * @return $this
     */
    protected function findCommentingPosts()
    {
        $commentingPosts = PostsServiceProvider::selectPosts([
             'type'    => $this->posttype_slugs['commenting'],
             'domains' => null, // @todo: remove this line after fixing bugs in elector
        ])->where('slug', 'like', 'send-works-_%')
                                               ->get()
        ;

        $commentingPostsInfo = [];
        foreach ($commentingPosts as $commentingPost) {
            $commentingPost->spreadMeta();
            if (
                 ($settingId = $commentingPost->post_upload_configs) and
                 ($settingRow = model('setting', $settingId))
            ) {
                $commentingPostsInfo[] = [
                     'post'      => $commentingPost,
                     'config'    => $settingRow->slug,
                     'file_type' => $commentingPost->file_type,
                ];
            }
        }

        return $this->appendToVariables('commentingPostsInfo', $commentingPostsInfo);
    }
}
