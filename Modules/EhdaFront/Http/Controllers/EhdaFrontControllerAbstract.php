<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 24/02/2018
 * Time: 17:30 AM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Pagination\Paginator;
use Modules\EhdaFront\Http\Controllers\Traits\EhdaFrontFeedbackControllerTrait;
use Modules\EhdaFront\Http\Controllers\Traits\EhdaFrontTemplateTrait;
use Modules\EhdaFront\Providers\EhdaFrontServiceProvider;

abstract class EhdaFrontControllerAbstract extends EhdaFrontModuleControllerAbstract
{
    use EhdaFrontFeedbackControllerTrait;
    use EhdaFrontTemplateTrait;



    /**
     * EhdaFrontControllerAbstract constructor.
     */
    public function __construct()
    {
        $this->setPaginationTemplate();
    }



    /**
     * Assign the view that will be used in rendering pagination links.
     */
    protected function setPaginationTemplate()
    {
        Paginator::defaultView('ehdafront::vendor.pagination.default');
    }



    /**
     * @return $this
     */
    public function findUsableDomains()
    {
        $this->variables['usableDomains'] = EhdaFrontServiceProvider::getUsableDomains();
        return $this;
    }
}
