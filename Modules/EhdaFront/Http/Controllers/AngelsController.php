<?php

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use Modules\EhdaFront\Http\Requests\AngelNewSaveRequest;
use Modules\EhdaFront\Http\Requests\AngelSearchRequest;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\Posts\Http\Requests\PostSaveRequest;

class AngelsController extends EhdaFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this->findUsableDomains()
                    ->findAngelsPosttype()
                    ->appendToVariables('posts', $this->findPosts([
                         'type'         => 'angels',
                         'random'       => true,
                         'max_per_page' => 19,
                    ]))
                    ->indexResult()
             ;
    }



    /**
     * @param array $switches
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    protected function findPosts($switches = [])
    {
        $posts = PostsServiceProvider::collectPosts(array_merge($switches, [
             'domains' => $this->variables['usableDomains'],
        ]));
        if (!$posts->count()) {
            $posts = PostsServiceProvider::collectPosts($switches);
        }
        return $posts;
    }



    /**
     * @return $this
     */
    protected function findAngelsPosttype()
    {
        return $this->appendToVariables('posttype',
             PostsServiceProvider::smartFindPosttype($this->posttype_slugs['angels']));
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response|\Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    protected function indexResult()
    {
        return ($this->variables['posttype']->exists and $this->variables['posts']->count())
             ? $this->template('angels.index.main', $this->variables)
             : $this->abort(404);
    }



    /**
     * @param AngelSearchRequest $request
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function search(AngelSearchRequest $request)
    {
        list($textToSearch) = explode('،', $request->angel_name);
        return $this->findUsableDomains()
                    ->appendToVariables('foundAngels', $this->findPosts([
                         'type'       => 'angels',
                         'pagination' => false,
                         'conditions' => [
                              ['title', 'LIKE', "%{$textToSearch}%"],
                         ],
                         'limit'      => 100,
                    ]))->searchResult()
             ;
    }



    /**
     * @return \Illuminate\Http\JsonResponse|null
     */
    protected function searchResult()
    {
        return $this->variables['foundAngels']->count()
             ? response()->json($this->variables['foundAngels']->map(function ($angel) {
                 return [
                      'id'            => $angel->id,
                      'name'          => $angel->title,
                      'label'         => $angel->title . ($angel->city ? '، ' . $angel->city : ''),
                      'picture_url'   => doc($angel->featured_image)->getUrl(),
                      'donation_date' => ad(echoDate($angel->donation_date, 'j F Y')),
                 ];
             })->toArray())
             : null;
    }



    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function newForm()
    {
        return $this->findAngelsPosttype()
                    ->newFormFindStaticPost()
                    ->newFormGenerateUploader()
                    ->selectStatesCombo()
                    ->template('angels.new.main', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function newFormFindStaticPost()
    {
        return $this->appendToVariables('staticPost', PostsServiceProvider::selectPosts([
             'type'    => $this->posttype_slugs['statics'],
             'slug'    => 'new-angel',
             'domains' => null,
        ])->first() ?: model('post'));
    }



    /**
     * @return $this
     */
    protected function newFormGenerateUploader()
    {
        return $this->appendToVariables('uploader', fileManager()
             ->uploader($this->posttype_slugs['angels'])
             ->posttypeConfig('featured_image')
             ->title('(' . trans('ehdafront::general.angels.picture.title.singular') . ')')
             ->image()
        );
    }



    /**
     * @param AngelNewSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function newSubmit(AngelNewSaveRequest $request)
    {
        fileManager()->file($request->featured_image)->permanentSave();
        $request->merge(['title' => $request->donor_name_and_surname]);
        unset($request['donor_name_and_surname']);
        $saveResponse = json_decode(module('posts')
             ->postsSaveController()
             ->save($request), true);
        return $this->jsonAjaxSaveFeedback($saveResponse['ok'], [
             'success_message'    => $saveResponse['message'],
             'success_callback'   => "eval($('.uploader-container').first().data('varName') + '.removeAllFiles()')",
             'success_form_reset' => 1,
        ]);
    }
}
