<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 24/02/2018
 * Time: 17:30 AM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Routing\Controller;

abstract class ModuleControllerAbstract extends Controller
{
    public static function getModuleAlias()
    {
        return property_exists($className = static::class, 'moduleAlias')
            ? static::$moduleAlias
            : strtolower(str_before(str_after(static::class, 'Modules\\'), '\\'));
    }

    protected function template(...$parameters)
    {
        return $this->safeTemplate(...$parameters);
    }

    protected function safeTemplate(...$parameters)
    {
        if (is_string($parameters[0]) and !str_contains($parameters[0], '::')) {
            $parameters[0] = self::getModuleAlias() . '::' . $parameters[0];
        }

        return view(...$parameters);
    }
}
