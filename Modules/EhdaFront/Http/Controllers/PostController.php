<?php

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Modules\EhdaFront\Providers\EhdaFrontServiceProvider;
use Modules\EhdaFront\Providers\FilterServiceProvider;
use Modules\EhdaFront\Providers\GalleryServiceProvider;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;

class PostController extends EhdaFrontControllerAbstract
{
    protected $innerVariables = [];
    protected $listOptions    = [];



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function showWithFullUrl(Request $request)
    {
        return $this->show($request->identifier);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Symfony\Component\HttpFoundation\Response
     */
    public function shortLink(Request $request)
    {
        return ($post = post()->grabHashid(str_after($request->identifier,
             config('ehdafront.prefix.routes.post.short'))))->exists
             ? redirect($post->direct_url)
             : $this->abort(404);
    }



    /**
     * @param $identifier
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function show($identifier)
    {
        return $this->singleFindPost($identifier)
                    ->singleRenderView()
             ;
    }



    /**
     * @param string|int $identifier
     *
     * @return $this
     */
    protected function singleFindPost($identifier)
    {
        if (count($dehashed = hashid_decrypt($identifier, 'ids')) and
             is_numeric($id = $dehashed[0])
        ) {
            $post = model('post')->whereId($id)->first() ?: model('post');
        } else {
            $post = model('post')
                 ->whereSlug($identifier)
                 ->whereType($this->posttype_slugs['statics'])
                 ->first() ?: model('post');
        }

        return $this->appendToVariables('post', $post->in(getLocale()));
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function singleRenderView()
    {
        if (($post = $this->variables['post']) and $post->exists) {
            $this->appendToVariables('posttype', $postType = ($post = $this->variables['post'])->posttype)
                 ->appendToVariables('categories', $categories = $post->categories)
                 ->singleViewSetPaymentInfo()
                 ->appendToVariables('innerHTML', PostsServiceProvider::showPost($post, [
                      'variables' => $this->innerVariables,
                 ]))
                 ->appendToVariables('pageTitle', $post->title)
                 ->appendToVariables('metaTags', [
                      'image' => doc($post->featured_image)->getUrl(),
                 ])
                 ->singleSisterhoodUrlParameters()
            ;
            return (
                 array_key_exists('posttype', $this->variables) and
                 (method_exists($this,
                      $methodName = 'single' . studly_case($this->variables['posttype']->slug) . 'Result'))
            ) ? $this->$methodName()
                 : $this->singleGeneratePositionInfo()
                        ->template('posts.general.frame.main', $this->variables)
                 ;
        } else {
            return $this->abort('404');
        }
    }



    /**
     * @return $this
     */
    protected function singleViewSetPaymentInfo()
    {
        return $this->appendToInnerVariables('paymentInfo', session()->get('paymentInfo'));

        //        @todo: check all of this method's content while doing educational posts task
        $post = $this->variables['post'];
        $this->appendToInnerVariables('showSideBar', false)
             ->appendToInnerVariables('postFiles', $files = $post->files)
        ;

        if (session()->exists($orderSessionName = 'product-order-' . $post->hashid)) {
            $orderId = session($orderSessionName);
            $order   = Order::find($orderId);
            if ($order->exists) {
                $innerHTMLVars['trackingNumber'] = $order->tracking_number;
            }

            if (count($files)) {
                foreach ($files as $file) {
                    $file = UploadServiceProvider::smartFindFile($file['src']);
                    if ($file->exists) {
                        $filesIds[] = $file->id;
                    }
                }

                $undownloadeds = FileDownloads::where([
                     'order_id' => $orderId,
                ])->get()
                ;
                if ($undownloadeds and $undownloadeds->count()) {
                    $undownloadedIds = $undownloadeds->pluck('file_id')->toArray();
                    foreach ($files as $key => $file) {
                        $fileObj = model('file')->grabHashid($file['src']);
                        if ($fileObj->exists and in_array($fileObj->id, $undownloadedIds) === false) {
                            unset($files[$key]);
                        } else {
                            $files[$key]['hashString'] = $undownloadeds->where('file_id', $fileObj->id)
                                                                       ->last()
                                 ->hashid
                            ;
                        }
                    }
                    $innerHTMLVars['files'] = $files;
                }
            }

            if (session()->exists('paymentSucceeded')) {
                $paymentSucceeded                  = session('paymentSucceeded');
                $innerHTMLVars['paymentSucceeded'] = session('paymentSucceeded');

                if ($paymentSucceeded) {
                    $messagePost = post()->grabSlug('payment-success')->in(getLocale());
                    if ($messagePost->exists) {
                        $innerHTMLVars['paymentMsg'] = $messagePost->text;
                    } else {
                        $innerHTMLVars['paymentMsg'] = trans('cart.messages.payment.succeeded');
                    }
                } else {
                    $messagePost = post()->grabSlug('payment-canceled')->in(getLocale());
                    if ($messagePost->exists) {
                        $innerHTMLVars['paymentMsg'] = $messagePost->text;
                    } else {
                        $innerHTMLVars['paymentMsg'] = trans('cart.messages.payment.canceled');
                    }
                }
            }
        }

        return $this;
    }



    /**
     * @return $this
     */
    protected function singleGeneratePositionInfo()
    {
        $post       = $this->variables['post'];
        $postType   = $this->variables['posttype'];
        $categories = $this->variables['categories'];
        $this
             ->appendToVariables('positionInfo', [
                  'group'    => $post->header_title ?: $postType->header_title,
                  'category' => $post->category_title ?: $postType->titleIn(getLocale()),
                  'title'    => $categories->first() ? $categories->first()->title : '',
             ]);


        return $this;
    }



    /**
     * @return $this
     */
    protected function singleSisterhoodUrlParameters()
    {
        $urlParametersForForce = [];
        foreach (availableLocales() as $locale) {
            $sisterPost = $this->variables['post']->in($locale);

            if ($sisterPost->exists) {
                $localeValue        = [];
                $localeValue['url'] = EhdaFrontServiceProvider::urlHyphen($sisterPost->title);
            } else {
                $localeValue = false;
            }
            $urlParametersForForce[$locale] = $localeValue;
        }
        RouteServiceProvider::forceUrlParameters($urlParametersForForce);

        return $this;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleNewsResult()
    {
        return $this->template('news.single.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleEventResult()
    {
        return $this->template('events.single.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleGalleryImageResult()
    {
        return $this->template('gallery.single.frame.frame', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleGalleryVideoResult()
    {
        return $this->template('gallery.single.frame.frame', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleGalleryAudioResult()
    {
        return $this->template('gallery.single.frame.frame', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleVolunteersActivitiesResult()
    {
        return $this->singleNewsResult();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleActivitiesResult()
    {
        return $this->template('activities.single.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleCulturalProductsResult()
    {
        return $this->template('news.single.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleEducationResourcesResult()
    {
        return $this->template('news.single.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleResearchResourcesResult()
    {
        return $this->template('news.single.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleFontsResult()
    {
        return $this
             ->appendToVariables('disable_similars', true)
             ->template('news.single.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleInteractionsResult()
    {
        return $this->singleNewsResult();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleInformingCoursesResult()
    {
        return $this->singleNewsResult();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleInformingCongressesResult()
    {
        return $this->singleNewsResult();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleResearchProjectsResult()
    {
        return $this->singleNewsResult();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function singleCelebritiesResult()
    {
        return $this->template('celebrities.single.frame', $this->variables);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function archive(Request $request)
    {
        return $this
             ->findUsableDomains()
             ->archiveGenerateInnerHtml($request)
             ->archiveRenderResult()
             ;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function archiveGenerateInnerHtml(Request $request)
    {
        return $this->appendToInnerVariables('twoColumns', false)
                    ->appendToListOptions('showError', false)
                    ->archiveGuessPosttype($request)
                    ->appendToListOptions('domains',
                         $this->variables['postType']->hasFeature('domains')
                              ? $this->variables['usableDomains']
                              : null
                    )
                    ->archiveGuessCategory($request)
                    ->archiveSearch($request)
                    ->appendToListOptions('variables', $this->innerVariables)
                    ->appendToVariables('innerHTML', PostsServiceProvider::showList($this->listOptions))
             ;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function archiveGuessPosttype(Request $request)
    {
        if ($postTypeSlug = $request->postType) {
            // If "postType" is specified, posts in the specified postType will be shown
            $this->appendToListOptions('type', $postTypeSlug);

            $this->appendToVariables('postType', $posttype = posttype($postTypeSlug));
            $this->appendToInnerVariables('postType', $posttype);
        } else {
            // If "postType" isn't specified, "listable" posts will be shown
            $this->appendToVariables('postType', model('posttype'));
            $this->appendToListOptions('type', 'feature:listable');
        }

        return $this;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function archiveGuessCategory(Request $request)
    {
        if ($folder = $request->folder) {
            // If "folder" is specified, posts in specified folder will be shown
            $this->appendToListOptions('folder', $folder);

            if ($category = $request->category) {
                // If "category" is specified, posts in specified category will be shown
                $this->appendToListOptions('category', $category);
            } // If "category" isn't specified, posts in all categories will be shown
        } // If "folder" isn't specified, posts in all folders will be shown

        $this->appendToVariables('folder', $folderModel = model('category')->firstOrNew([
             'posttype_id' => $this->variables['postType']->id,
             'is_folder'   => 1,
             'slug'        => $request->folder,
        ]));
        $this->appendToInnerVariables('folder', $folderModel);
        $this->appendToVariables('category', $categoryModel = model('category')->firstOrNew([
             'parent_id'   => $this->variables['folder']->id,
             'posttype_id' => $this->variables['postType']->id,
             'is_folder'   => 0,
             'slug'        => $request->category,
        ]));
        $this->appendToInnerVariables('category', $categoryModel);

        return $this;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function archiveSearch(Request $request)
    {
        if ($search = $request->get('search')) {
            $this->appendToVariables('searchValue', $search);
            $this->appendToListOptions('search', $search);
            $this->appendToListOptions('paginate_url',
                 request()->url() . "?search=$search");
        }

        return $this;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveRenderResult()
    {
        return (
             array_key_exists('postType', $this->variables) and
             (method_exists($this,
                  $methodName = 'archive' . studly_case($this->variables['postType']->slug) . 'Result'))
        ) ? $this->$methodName()
             : $this->template('posts.general.frame.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveNewsResult()
    {
        return $this
             ->archiveSelectSpecialNews()
             ->template('news.list.main', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function archiveSelectSpecialNews()
    {
        if (array_key_exists('folder', $this->listOptions) and $this->listOptions['folder']) {
            $categoryPrefix                 = ($this->listOptions['folder'] == 'world') ? 'world-' : '';
            $this->variables['specialNews'] = PostsServiceProvider::collectPosts(array_merge(
                 array_only($this->listOptions, ['folder', 'domains', 'type']),
                 [
                      'category'   => $categoryPrefix . 'special-news',
                      'pagination' => false,
                      'limit'      => 12,
                 ]
            ));
        } else {
            $this->variables['specialNews'] = collect([]);
        }

        return $this;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveEventResult()
    {
        return $this
             ->archiveSelectImportantEvents()
             ->template('events.list.main', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function archiveSelectImportantEvents()
    {
        return $this->appendToVariables('importantEvents',
             PostsServiceProvider::selectPosts([
                  'type'     => 'event',
                  'category' => 'important-events',
                  'sort_by'  => 'pinned_at',
                  'limit'    => 5,
             ])->orderBy('published_at', 'desc')
                                 ->get()
        );
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveInteractionsResult()
    {
        return $this->template('interactions.list.main', $this->variables);
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveInformingCoursesResult()
    {
        return $this->archiveProjects();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveInformingCongressesResult()
    {
        return $this->archiveProjects();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveResearchProjectsResult()
    {
        return $this->archiveProjects();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveProjects()
    {
        return $this->archiveProjectsStaticPost($this->variables['postType']->slug)
                    ->template('projects.list.main', $this->variables)
             ;
    }



    /**
     * @param string $posttype_slug
     *
     * @return $this
     */
    protected function archiveProjectsStaticPost($posttype_slug)
    {
        return $this->appendToVariables(
             'static_post',
             PostsServiceProvider::selectPosts([
                  'type' => $this->posttype_slugs['statics'],
                  'slug' => $posttype_slug,
             ])->first() ?: post()
        );
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    protected function appendToInnerVariables($key, $value)
    {
        $this->innerVariables[$key] = $value;

        return $this;
    }



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    protected function appendToListOptions($key, $value)
    {
        $this->listOptions[$key] = $value;

        return $this;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function gallery(Request $request)
    {
        $request->postType = GalleryServiceProvider::typeToPosttype($request->type);
        return $this
             ->appendToListOptions('is_base_page', true)
             ->archive($request)
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function galleryFilter(Request $request)
    {
        if ($request->ajax()) {
            $request->postType =
                 $request->postType ?:
                      GalleryServiceProvider::typeToPosttype($request->type);
            $hashArray         = FilterServiceProvider::translateFilterHash($hashString = $request->hash);
            $posttype          = PostsServiceProvider::smartFindPosttype($request->postType);

            if (isset($hashArray['checkbox']['category'])) {
                $this->appendToListOptions(
                     'category',
                     $posttype->categories()
                              ->whereIsFolder(0)
                              ->whereIn('slug', (array)$hashArray['checkbox']['category'])
                              ->pluck('id')
                              ->toArray()
                );
            }

            if (isset($hashArray['text']['search'])) {
                $this->appendToListOptions('search', $hashArray['text']['search']);
            }

            if (isset($hashArray['sort']['publishing'])) {
                $this->appendToListOptions('sort', $hashArray['sort']['publishing']);
                $this->appendToListOptions('sort_by', 'published_at');
            }

            if (isset($hashArray['pagination']['page']) and is_numeric($page = $hashArray['pagination']['page'])) {
                $this->appendToListOptions('paginate_current', $page);
            }


            return $this
                 ->appendToListOptions('paginate_hash', $hashString)
                 ->appendToListOptions('paginate_url', url()->previous())
                 ->appendToInnerVariables('hash_array', $hashArray)
                 ->appendToInnerVariables('posttype', $posttype)
                 ->archive($request)
                 ;
        } else {
            return $this->abort(403);
        }
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveGalleryImageResult()
    {
        return $this->archiveGalleryAllResult();
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveGalleryVideoResult()
    {
        return $this->archiveGalleryAllResult();
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveGalleryAudioResult()
    {
        return $this->archiveGalleryAllResult();
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveGalleryTextResult()
    {
        return $this->archiveGalleryAllResult();
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveGalleryAllResult()
    {
        return $this->template(
             'gallery.list.frame.frame' . (request()->ajax() ? '-filter' : ''),
             $this->variables
        );
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveCulturalProductsResult()
    {
        return $this->archiveGalleryAllResult();
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveEducationResourcesResult()
    {
        return $this->archiveGalleryAllResult();
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function archiveResearchResourcesResult()
    {
        return $this->archiveGalleryAllResult();
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function archiveCelebritiesResult()
    {
        return $this->template('celebrities.list.frame', $this->variables);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $searchFieldName = PostsServiceProvider::getMainSearchFieldName();
        if ($search = $request->$searchFieldName) {
            return $this->appendToVariables('innerHTML', PostsServiceProvider::showList([
                 'search'       => $search,
                 'showError'    => false,
                 'paginate_url' => request()->url() . "?" . $searchFieldName . "=" . $search,
            ]))->appendToVariables('searchValue', $search)
                        ->appendToVariables('searchFieldName', $searchFieldName)
                        ->template('posts.general.frame.search.main', $this->variables)
                 ;
        } else {
            return redirect()->back();
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function culturalProducts(Request $request)
    {
        $request->merge(['postType' => $this->posttype_slugs['cultural-products']]);
        return $this
             ->appendToListOptions('feature', false)
             ->appendToListOptions('is_base_page', true)
             ->appendToInnerVariables('filterUrl',
                  RouteServiceProvider::actionLocale('PostController@culturalProductsFilter'))
             ->archive($request)
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function culturalProductsFilter(Request $request)
    {
        $request->merge(['postType' => $this->posttype_slugs['cultural-products']]);
        return $this
             ->appendToListOptions('feature', false)
             ->galleryFilter($request)
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function interactions(Request $request)
    {
        $request->postType = $this->posttype_slugs['interactions'];
        return $this->archive($request);
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function informingCourses(Request $request)
    {
        $request->merge([
             'postType' => $this->posttype_slugs['informing-courses'],
        ]);

        return $this
             ->appendToListOptions('feature', null)
             ->archive($request)
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function informingCongresses(Request $request)
    {
        $request->merge([
             'postType' => $this->posttype_slugs['informing-congresses'],
        ]);

        return $this
             ->appendToListOptions('feature', null)
             ->archive($request)
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function researchProjects(Request $request)
    {
        $request->merge([
             'postType' => $this->posttype_slugs['research-projects'],
        ]);

        return $this
             ->appendToListOptions('feature', null)
             ->archive($request)
             ;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function celebrities(Request $request)
    {
        $request->postType = $this->posttype_slugs['celebrities'];
        return $this
             ->appendToListOptions('feature', null)
             ->appendToListOptions('max_per_page', -1)
             ->archive($request)
             ;
    }
}
