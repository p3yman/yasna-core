<?php

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class EhdaFrontController extends EhdaFrontControllerAbstract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return $this
             ->selectPosttypes()
             ->findUsableDomains()
             ->selectMainSlideShow()
             ->selectEventsSlideShow()
             ->selectSpecialNews()
             ->selectTopParagraph()
             ->getDeadlinesHtml()
             ->selectEquationPost()
             ->selectEvents()
             ->selectWorldNews()
             ->selectTransplantNews()
             ->template('home.main', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function selectPosttypes()
    {
        return $this
             ->appendToVariables('slideshow_posttype', posttype()->grabSlug($this->posttype_slugs['slideshows']))
             ->appendToVariables('events_posttype', posttype()->grabSlug($this->posttype_slugs['events']))
             ->appendToVariables('news_posttype', posttype()->grabSlug($this->posttype_slugs['news']))
             ;
    }



    /**
     * @return $this
     */
    protected function selectMainSlideShow()
    {
        return $this->appendToVariables('mainSlideShow', PostsServiceProvider::collectPosts([
             'type'       => $this->posttype_slugs['slideshows'],
             'category'   => 'main-slideshow',
             'domains'    => $this->variables['usableDomains'],
             'pagination' => false,
             'limit'      => 10,
        ]));
    }



    /**
     * @return $this
     */
    protected function selectEventsSlideShow()
    {
        $this->variables['eventsSlideShow'] = PostsServiceProvider::collectPosts([
             'type'       => 'event',
             'category'   => 'main-page',
             'domains'    => $this->variables['usableDomains'],
             'pagination' => false,
             'limit'      => 10,
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectSpecialNews()
    {
        $this->variables['specialNews'] = PostsServiceProvider::collectPosts([
             'type'       => 'news',
             'folder'     => 'iran',
             'category'   => 'special-news',
             'domains'    => $this->variables['usableDomains'],
             'pagination' => false,
             'limit'      => 6,
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectTopParagraph()
    {
        $this->variables['homePageTopParagraph'] = Post::firstOrNew([
             'type'   => 'static-paragraph',
             'slug'   => 'home-page-top-paragraph',
             'locale' => getLocale(),
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function getDeadlinesHtml()
    {
        $this->variables['deadlinesHTML'] = PostsServiceProvider::showList([
             'type'         => 'deadlines',
             'max_per_page' => -1,
             'sort'         => 'asc',
             'showError'    => false,
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectEquationPost()
    {
        $this->variables['equationPost'] = Post::firstOrNew([
             'type' => 'static-paragraph',
             'slug' => 'home-page-fix-background-parag',
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectEvents()
    {
        $this->variables['events'] = PostsServiceProvider::collectPosts([
             'type'       => 'event',
             'domains'    => $this->variables['usableDomains'],
             'limit'      => 5,
             'pagination' => false,
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectWorldNews()
    {
        $this->variables['worldNews'] = PostsServiceProvider::collectPosts([
             'type'    => 'news',
             'folder'  => 'world',
             'domains' => $this->variables['usableDomains'],
             'limit'   => 5,
        ]);

        return $this;
    }



    /**
     * @return $this
     */
    protected function selectTransplantNews()
    {
        $this->variables['transplantNews'] = PostsServiceProvider::collectPosts([
             'type'     => 'news',
             'folder'   => 'iran',
             'category' => [
                  'iran-opu-transplant',
                  'internal-ngo',
             ],
             'domains'  => $this->variables['usableDomains'],
             'limit'    => 5,
        ]);

        return $this;
    }
}
