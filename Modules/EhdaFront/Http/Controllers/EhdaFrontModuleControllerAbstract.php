<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 24/02/2018
 * Time: 17:30 AM
 */

namespace Modules\EhdaFront\Http\Controllers;

abstract class EhdaFrontModuleControllerAbstract extends ModuleControllerAbstract
{
    protected $posttype_slugs = [
         'statics'              => 'statics',
         'angels'               => 'angels',
         'sponsors'             => 'sponsors',
         'members'              => 'members',
         'commenting'           => 'commenting',
         'general-information'  => 'general-information',
         'faq'                  => 'faq',
         'activities'           => 'activities',
         'donation'             => 'donation',
         'events'               => 'event',
         'slideshows'           => 'slideshows',
         'gallery-image'        => 'gallery-image',
         'news'                 => 'news',
         'celebrities'          => 'celebrities',
         'states-logos'         => 'states-logos',
         'cultural-products'    => 'cultural-products',
         'education-resources'  => 'education-resources',
         'research-resources'   => 'research-resources',
         'fonts'                => 'fonts',
         'interactions'         => 'interactions',
         'informing-courses'    => 'informing-courses',
         'informing-congresses' => 'informing-congresses',
         'research-projects'    => 'research-projects',
    ];

    protected $variables = [];



    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    protected function appendToVariables($key, $value)
    {
        $this->variables[$key] = $value;

        return $this;
    }
}
