<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/8/18
 * Time: 2:59 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Http\Request;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;

class GeneralInformationController extends EhdaFrontControllerAbstract
{
    public function index(Request $request)
    {
        if (($this->findPosttype()->variables['posttype']->exists and $this->findPosts()->variables['posts']->count())) {
            if ($this->indexGuessCurrentPost($request)->variables['currentPost']) {
                return $this
                     ->sisterhoodUrlParameters($request)
                     ->template('general-information.main', $this->variables)
                     ;
            } else {
                return redirect(action_locale('\\' . $request->route()->getActionName()));
            }
        } else {
            return $this->abort(404);
        }
    }



    protected function findPosts()
    {
        return $this->appendToVariables('posts', PostsServiceProvider::collectPosts([
             'type'       => $this->posttype_slugs['general-information'],
             'pagination' => false,
             'limit'      => 20,
        ]));
    }



    protected function findPosttype()
    {
        return $this->appendToVariables(
             'posttype',
             model('posttype')->grab($this->posttype_slugs['general-information'])
        );
    }



    protected function indexGuessCurrentPost($request)
    {
        return $this->appendToVariables('currentPost', ($slug = $request->post)
             ? $this->variables['posts']->where('slug', $slug)->first()
             : $this->variables['posts']->first());
    }



    protected function sisterhoodUrlParameters($request)
    {
        if ($request->post) {
            $urlParametersForForce = [];
            foreach (availableLocales() as $locale) {
                $sisterPost = $this->variables['currentPost']->in($locale);

                if (!$sisterPost or !$sisterPost->exists) {
                    $urlParametersForForce[$locale] = false;
                }
            }
            RouteServiceProvider::forceUrlParameters($urlParametersForForce);
        }

        return $this;
    }
}
