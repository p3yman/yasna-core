<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/24/18
 * Time: 12:13 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Cart;
use App\Models\FileDownload;
use App\Models\Post;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\EhdaFront\Http\Requests\FontPurchaseRequest;
use Modules\EhdaFront\Http\Requests\FontPurchaseTrackRequest;
use Modules\EhdaFront\Providers\CurrencyServiceProvider;
use Modules\EhdaFront\Providers\EhdaFrontServiceProvider;
use Modules\EhdaFront\Providers\PostsServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;
use Modules\Filemanager\Services\Tools\Doc;

class FontsController extends EhdaFrontControllerAbstract
{
    protected $default_locale        = 'fa';
    protected $important_fonts_slugs = ['font-daftar', 'font-studio', 'font-daftar-and-studio'];

    protected static $download_valid_no  = 10;
    protected static $download_valid_min = 15;



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return $this
             ->indexFindStaticPost()
             ->indexRenderResult()
             ;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function indexRenderResult()
    {
        return $this->variables['staticPost']->exists
             ? $this
                  ->indexFindPurchaseFormText()
                  ->indexFindFontPosts()
                  ->template('fonts.main-page.main', $this->variables)
             : $this->abort(404);
    }



    /**
     * @return $this
     */
    protected function indexFindStaticPost()
    {
        return $this->appendToVariables('staticPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'fonts-daftar-and-studio-static',
        ])->first() ?: model('post'));
    }



    /**
     * @return $this
     */
    protected function indexFindPurchaseFormText()
    {
        return $this->appendToVariables('purchaseFormText', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'purchase-form',
        ])->first() ?: model('post'));
    }



    /**
     * @return $this
     */
    protected function indexFindFontPosts()
    {
        return $this->appendToVariables(
             'fontPosts',
             PostsServiceProvider::selectPosts([
                  'type'    => $this->posttype_slugs['fonts'],
                  'domains' => null, // @todo: remove this line after fixing bugs in elector
             ])->whereIn('slug', $this->important_fonts_slugs)
                                 ->where('total_wares', '>', '0')
                                 ->get()
        )->indexSortPosts()
             ;
    }



    /**
     * @return $this
     */
    protected function indexSortPosts()
    {
        $posts = $this->variables['fontPosts'];
        $slugs = $this->important_fonts_slugs;
        for ($i = 0; $i < ($posts->count() - 1); $i++) {
            for ($j = ($i + 1); $j < ($posts->count()); $j++) {
                $post1 = $posts[$i];
                $post2 = $posts[$j];
                if (array_search($post1->slug, $slugs) > array_search($post2->slug, $slugs)) {
                    list($posts[$i], $posts[$j]) = [$posts[$j], $posts[$i]];
                }
            }
        }

        return $this->appendToVariables('fontPosts', $posts);
    }



    /**
     * @param mixed ...$parameters
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function notLocaleIndex(...$parameters)
    {
        app()->setlocale($this->default_locale);
        return $this->callAction('index', $parameters);
    }



    /**
     * @param FontPurchaseRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function purchase(FontPurchaseRequest $request)
    {
        $tracking_no = payment()
             ->transaction()
             ->invoiceId(($cart = $this->purchaseCreateCart($request))->id)
             ->amount($cart->invoiced_amount)
             ->account($this->purchaseFindOnlineAccount()->id)
             ->callbackUrl(RouteServiceProvider::actionLocale('FontsController@callback'))
             ->getTracking()
        ;

        if (is_array($tracking_no)) { // Creating transaction unsuccess
            return $this->jsonAjaxSaveFeedback(0, [
                 'danger_message' => trans('ehdafront::cart.messages.payment.problem_occurred'),
            ]);
        } else {
            return $this->jsonAjaxSaveFeedback(1, [
                 'success_redirect' => RouteServiceProvider::actionLocale('FontsController@fire', [
                      'tracking_no' => $tracking_no,
                 ]),
                 'redirectTime'     => 3000,
                 'success_message'  => trans('ehdafront::cart.messages.payment.sending_to_gateway'),
            ]);
        }
    }



    /**
     * @param FontPurchaseRequest $request
     *
     * @return Cart
     */
    protected function purchaseCreateCart($request)
    {
        $ware_price           = $request->model->in(getLocale())
                                               ->in(CurrencyServiceProvider::getCurrency())
                                               ->price()
                                               ->get()
        ;
        $payment_price        = $request->price / CurrencyServiceProvider::getPreviewCurrencyRatio();
        $discount             = $ware_price - $payment_price;
        $cart                 = $this->purchaseCreateNewCart();
        $cart->added_discount = $discount;
        $cart->name           = $request->name;
        $cart->email          = $request->email;
        $cart->mobile         = $request->mobile;
        $cart->job            = $request->job;
        $cart->addOrder($request->model_id, 1);
        $cart->refreshCalculations();
        return $cart;
    }



    /**
     * @return \App\Models\Account|null
     */
    protected function purchaseFindOnlineAccount()
    {
        return payment()
             ->accounts()
             ->online()
             ->first()
             ;
    }



    /**
     * @return Cart
     */
    protected function purchaseCreateNewCart()
    {
        $cart           = model('cart');
        $cart->currency = CurrencyServiceProvider::getCurrency();
        $cart->locale   = getLocale();
        $cart->user_id  = user()->id;
        $cart->save();

        return $cart;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function fire(Request $request)
    {
        if (
             $this->trackingNumberIsAcceptable($trackingNumber = $request->tracking_no) and
             ($result = payment()->transaction()->trackingNumber($trackingNumber)->fire()) and
             ($result['status'] == 'returnable-response')
        ) {
            return $result['response'];
        } else {
            return redirect(RouteServiceProvider::actionLocale('FontsController@notLocaleIndex'));
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callback(Request $request)
    {
        if (
             ($this->trackingNumberIsAcceptable($tracking_number = $request->tracking_no)) and
             ($transaction = payment()->transaction()->trackingNumber($tracking_number)) and
             ($transaction_model = $transaction->getModel()) and
             $transaction_model->exists and
             ($cart = $this->findCart($transaction_model->invoice_id))->exists and
             ($order = $cart->orders()->first()) and
             ($post = $order->post)
        ) {
            $paymentInfo = compact('tracking_number');
            switch ($transaction->statusSlug()) {
                case "verified":
                    $paymentInfo['success'] = true;
                    $paymentInfo['message'] = $this->findPaymentMessageResponse('success')
                         ?: trans('ehdafront::cart.messages.payment.succeeded');
                    $this->makeCartVerified($cart);
                    $paymentInfo['served_files'] = $this->serveDownload($post, $tracking_number, $cart);
                    break;

                case "cancelled":
                    $paymentInfo['success'] = false;
                    $paymentInfo['message'] = $this->findPaymentMessageResponse('canceled')
                         ?: trans('ehdafront::cart.messages.payment.canceled');
                    break;

                case "rejected":
                    $paymentInfo['success'] = false;
                    $paymentInfo['message'] = $this->findPaymentMessageResponse('rejected')
                         ?: trans('ehdafront::cart.messages.payment.rejected');
                    break;

                default:
                    $paymentInfo['success'] = false;
                    $paymentInfo['message'] = trans('ehdafront::cart.messages.payment.problem_occurred');
                    break;
            }

            return redirect($post->direct_url)->with('paymentInfo', $paymentInfo);
        } else {
            return redirect(RouteServiceProvider::actionLocale('FontsController@notLocaleIndex'));
        }
    }



    /**
     * @param string $tracking_number
     *
     * @return Cart
     */
    protected function findCart(string $tracking_number)
    {
        return model('cart', $tracking_number);
    }



    /**
     * @param Cart $cart
     *
     * @return  void
     */
    protected function makeCartVerified(Cart $cart)
    {
        if (!$cart->isDelivered()) {
            $cart->checkout();
            $cart->confirm();
            $cart->applyDeliveredStatus();
        }
    }



    /**
     * @param $status
     *
     * @return string
     */
    protected function findPaymentMessageResponse($status)
    {
        return ($post = PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'payment-' . $status,
        ])->first())
             ? $post->text
             : "";
    }



    /**
     * @param Post   $post
     * @param string $tracking_number
     *
     * @return array
     */
    protected function serveDownload(Post $post, string $tracking_number, Cart $cart)
    {
        $post->spreadMeta();

        $files         = $post->files;
        $serving_files = [];
        if ($files and is_array($files) and count($files)) {
            foreach ($files as $fileInfo) {
                if ($hashid = $this->serveSingleFileDownload(
                     ($fileHashid = $fileInfo['src']),
                     ($file = fileManager()->file($fileHashid)),
                     $tracking_number
                )) {
                    $serving_files[] = [
                         'download_link' => RouteServiceProvider::action('FileController@disposableDownload', [
                              'hashid'     => $hashid,
                              'file_title' => EhdaFrontServiceProvider::urlHyphen($title = $file->getTitle()),
                              't'          => time(),
                         ]),
                         'title'         => $title,
                         'size'          => $file->getSizeText(),
                    ];
                }
            }
        }

        return $serving_files;
    }



    /**
     * @param string $fileHashid
     * @param Doc    $file
     * @param string $tracking_number
     *
     * @return null
     */
    protected function serveSingleFileDownload(string $fileHashid, Doc $file, $tracking_number)
    {
        if ($file->getUrl()) {
            $similarRow = FileDownload::where([
                 'file_code'       => $fileHashid,
                 'tracking_number' => $tracking_number,
            ])
                                      ->where('expire_date', '>', Carbon::now())
                                      ->whereRaw(DB::raw('downloaded_count < downloadable_count'))
                                      ->orderByDesc('created_at')
                                      ->first()
            ;

            if ($similarRow) {
                return $similarRow->hashid;
            } else {
                return model('file-download')->batchSave([
                     'user_id'            => (auth()->guest()) ? null : user()->id,
                     'file_code'          => $fileHashid,
                     'tracking_number'    => $tracking_number,
                     'downloadable_count' => self::$download_valid_no,
                     'expire_date'        => Carbon::now()->addMinutes(self::$download_valid_min),
                ])->hashid
                     ;
            }
        } else {
            return null;
        }
    }



    /**
     * @param FontPurchaseTrackRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function tracking(FontPurchaseTrackRequest $request)
    {
        $transaction = payment()
             ->transaction()
             ->trackingNumber($tracking_number = $request->tracking_number)
        ;
        if (($transaction_model = $transaction->getModel()) and $transaction_model->exists) {
            switch ($transaction->statusSlug()) {
                case "verified":
                    return $this->jsonFeedback([
                         'ok'       => 1,
                         'message'  => trans('ehdafront::forms.feed.wait'),
                         'redirect' => RouteServiceProvider::actionLocale('FontsController@callback', [
                              'tracking_no' => $tracking_number,
                         ]),
                    ]);

                case "cancelled":
                    return $this->jsonFeedback([
                         'ok'      => 0,
                         'message' => trans('ehdafront::cart.messages.payment.has_been_canceled'),
                    ]);
                    break;

                default:
                    return $this->jsonFeedback([
                         'ok'      => 0,
                         'message' => trans('ehdafront::cart.messages.payment.problem_occurred'),
                    ]);
                    break;
            }
        } else {
            return $this->jsonFeedback([
                 'ok'      => 0,
                 'message' => trans('validation.exists', [
                      'attribute' => trans('ehdafront::validation.attributes.tracking_number'),
                 ]),
            ]);
        }
    }



    /**
     * @param static|null $trackingNumber
     *
     * @return bool
     */
    protected function trackingNumberIsAcceptable($trackingNumber)
    {
        $validator = Validator::make(['tracking_number' => $trackingNumber], self::trackingNumberRules());
        return !$validator->fails();
    }



    /**
     * @return array
     */
    protected static function trackingNumberRules()
    {
        return [
             'tracking_number' => [
                  'required',
                  'numeric',
             ],
        ];
    }
}
