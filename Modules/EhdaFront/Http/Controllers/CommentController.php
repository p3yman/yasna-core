<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/8/18
 * Time: 11:27 AM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Http\Request;
use Modules\EhdaFront\Http\Requests\CommentSaveRequest;
use Modules\EhdaFront\Http\Requests\WorksCommentSaveRequest;

class CommentController extends EhdaFrontControllerAbstract
{
    protected $indexes_to_change = [
         'name'    => 'guest_name',
         'email'   => 'guest_email',
         'mobile'  => 'guest_mobile',
         'message' => 'text',
    ];

    protected $save_callback;

    protected $request;



    /**
     * @param CommentSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(CommentSaveRequest $request)
    {
        $this->request = $request;

        $savedComment = model('comment')
             ->batchSave($this->changeIndexes($request->all()));


        return $this->jsonAjaxSaveFeedback($savedComment->exists, [
             'success_message'      => trans('ehdafront::general.message.success.send_message'),
             'success_form_reset'   => 1,
             'success_feed_timeout' => 5000,
             'success_callback'     => $this->save_callback,

             'danger_message' => trans('ehdafront::general.message.error.send_message'),
        ]);
    }



    /**
     * @param array $data
     *
     * @return array
     */
    protected function changeIndexes(array $data)
    {
        foreach ($this->indexes_to_change as $from => $to) {
            if (array_key_exists($from, $data)) {
                $data[$to] = $data[$from];
                unset($data[$from]);
            }
        }

        return array_default($data, $this->additionalIndexes());
    }



    /**
     * @return array
     */
    protected function additionalIndexes()
    {
        return array_merge([
             'guest_ip'    => \request()->ip(),
             'user_id'     => user()->id,
             'posttype_id' => $this->request->getPost()->posttype->id,
        ], $this->departmentData());
    }



    /**
     * @return array
     */
    protected function departmentData()
    {
        if (
             ($departmentHashid = $this->request->getPost()->department) and
             ($department = model('role')->grab($departmentHashid)) and
             $department->exists
        ) {
            return [
                 'initial_department_id' => $department->id,
                 'current_department_id' => $department->id,
            ];
        }

        return [];
    }



    /**
     * @param WorksCommentSaveRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveWorks(WorksCommentSaveRequest $request)
    {
        $this->indexes_to_change['description'] = 'text';
        $this->save_callback                    = 'uploader' . $request->getPost()->hashid . '.removeAllFiles()';
        $this->permanentSaveFiles($request);

        if (count($request->get('files'))) {
            return $this->save($request);
        } else {
            return $this->abort(422, true, [
                 'errors' => [
                      'files' => trans('ehdafront::general.send_works.message.failure.files_required'),
                 ],
            ]);
        }
    }



    /**
     * @param $request
     *
     * @return $this
     */
    protected function permanentSaveFiles($request)
    {
        if (
             ($filesString = $request->_files) and
             (is_array($filesArray = json_decode($filesString, true)))
        ) {
            $files = [];
            foreach ($filesArray as $file) {
                if (fileManager()->file($file)->permanentSave()) {
                    $files[] = $file;
                }
            }

            $request->merge(['files' => $files]);
        }

        return $this;
    }
}
