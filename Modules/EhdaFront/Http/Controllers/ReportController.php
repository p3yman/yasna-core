<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 5/8/18
 * Time: 12:50 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Illuminate\Http\Request;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class ReportController extends EhdaFrontControllerAbstract
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function showReportsPage(Request $request)
    {
        if (
             $this->reportPosttype($request->report_type)->variables['posttype']->exists and
             $this->indexPosts()->variables['posts']->count()
        ) {
            return $this->template('reports.main', $this->variables);
        } else {
            return $this->abort('404');
        }
    }



    /**
     * @param string $report_type
     *
     * @return $this
     */
    protected function reportPosttype($report_type)
    {
        return $this->appendToVariables('posttype', model('posttype')->grabSlug($report_type . '-reports'));
    }



    /**
     * @return $this
     */
    protected function indexPosts()
    {
        return $this->appendToVariables(
             'posts',
             PostsServiceProvider::collectPosts([
                  'type' => $this->variables['posttype']->slug,
             ])
        );
    }
}
