<?php

namespace Modules\EhdaFront\Http\Controllers\Auth;

use Modules\EhdaFront\Http\Controllers\Traits\EhdaFrontTemplateTrait;
use Modules\Yasna\Http\Controllers\Auth\LoginController as Controller;
use Modules\EhdaFront\Providers\RouteServiceProvider;

class LoginController extends Controller
{
    use EhdaFrontTemplateTrait;

    protected $username_field = 'code_melli';

    public function showLoginForm()
    {
        $this->readSettings()
            ->setOpenGraphs();
        $query = \request()->query();

        if (array_key_exists('redirect', $query)) {
            switch (strtolower($query['redirect'])) {
                case 'referer':
                    session(['url.intended' => url()->previous()]);
                    break;
                default:
                    if (!Validator::make([
                        'url' => $query['redirect'],
                    ], [
                        'url' => 'required|url',
                    ])->fails()
                    ) {
                        session(['url.intended' => $query['redirect']]);
                    }
                    break;
            }
            session()->save();
        }

        return view('ehdafront::auth.login');
    }

    public function home()
    {
        return $this->redirectAfterLogin();
    }

    public function redirectAfterLogin()
    {
        //@TODO: Complete this based on the user roles

        if (user()->is_an('admin')) {
            $defaultUrl = url('/manage');
        } else {
            $defaultUrl = RouteServiceProvider::actionLocale('UserController@index');
        }

        return redirect()->intended($defaultUrl);
    }

    protected function validationRules()
    {
        return array_merge(parent::validationRules(), [
            'code_melli' => 'code_melli',
        ]);
    }
}
