<?php
/**
 * Created by PhpStorm.
 * User: yasna
 * Date: 5/19/18
 * Time: 6:00 PM
 */

namespace Modules\EhdaFront\Http\Controllers\Auth;

use Modules\EhdaFront\Http\Requests\PassargadLoginRequest;
use Modules\Yasna\Events\UserLoggedIn;
use Modules\Yasna\Events\UserLoggedOut;
use Modules\Yasna\Services\YasnaController;

/**
 * Class PassargadLoginController
 *
 * @package Modules\EhdaFront\Http\Controllers\Auth
 */
class PassargadLoginController extends YasnaController
{
    /**
     * @var string
     */
    protected $view_folder = 'ehdafront::';



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loginForm()
    {
        return view('ehdafront::passargad.login');
    }



    /**
     * @param PassargadLoginRequest $request
     *
     * @return string
     */
    public function login(PassargadLoginRequest $request)
    {
        $username = $request->get('code_melli');
        $password = $request->get('password');

        $redirect = '';
        $message = trans('ehdafront::passargad.user-not-found');

        // check user exists
        $user = user()->whereCodeMelli($username)->first();
        // check user role
        if ($user and $user->is('bpi')) {
            if (auth()->attempt(['code_melli' => $username, 'password' => $password])) {
                event(new UserLoggedIn(user()->id));

                $redirect = route('passargad_inquiry');
                $message  = trans('ehdafront::passargad.redirect');
            }
        } else {
            $redirect = '';
            $message  = trans('ehdafront::passargad.user-not-found');
        }


        return $this->jsonFeedback($message, compact('redirect'));
    }



    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        event(new UserLoggedOut());

        return redirect()->route('passargad_login');
    }
}
