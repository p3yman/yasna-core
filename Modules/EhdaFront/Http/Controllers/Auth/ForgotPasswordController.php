<?php

namespace Modules\EhdaFront\Http\Controllers\Auth;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\EhdaFront\Events\PasswordChangedByUser;
use Modules\EhdaFront\Http\Controllers\Traits\EhdaFrontFeedbackControllerTrait;
use Modules\EhdaFront\Http\Controllers\Traits\EhdaFrontTemplateTrait;
use Modules\EhdaFront\Http\Requests\ResetPasswordNewRequest;
use Modules\EhdaFront\Http\Requests\ResetPasswordRequest;
use Modules\EhdaFront\Http\Requests\ResetPasswordTokenRequest;
use Modules\EhdaFront\Notifications\PasswordResetRequested;
use Modules\EhdaFront\Providers\EmailServiceProvider;
use Modules\EhdaFront\Providers\RouteServiceProvider;
use Modules\EhdaFront\Services\AsanakSms\Facade\AsanakSms;

class ForgotPasswordController extends Controller
{
    const RECOVERY_METHODS = [
        /*'email',*/
        'mobile',
    ];

    use EhdaFrontFeedbackControllerTrait;
    use EhdaFrontTemplateTrait;

    public static $resetTokenLength = 6;

    protected static $resettingPasswordNationalIdSessionName = 'resettingPasswordNationalId';



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLinkRequestForm()
    {
        return $this->returnTemplate('ehdafront::auth.passwords.email', [
             'recovery_methods' => static::RECOVERY_METHODS,
        ]);
    }



    /**
     * @param ResetPasswordRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(ResetPasswordRequest $request)
    {
        if (!($user = $this->findUser($request))) {
            return redirect()->back()->withErrors([
                 'general' => trans('ehdafront::passwords.user'),
            ])->withInput($request->all())
                 ;
        }

        $user->batchSave([
             'reset_token'        => Hash::make($resetToken = $this->generateResetToken()),
             'reset_token_expire' => Carbon::now()
                                           ->addMinute(get_setting('password_token_expire_time'))
                                           ->toDateTimeString(),
        ]);

        session()->put(self::getResettingSessionName(), $user->code_melli);

        $user->notify(new PasswordResetRequested($request->type, $resetToken));
        return redirect(RouteServiceProvider::action('Auth\ForgotPasswordController@getToken'));
    }



    /**
     * @param $request
     *
     * @return User
     */
    protected function findUser($request)
    {
        return User::where([
             'code_melli'   => $request->code_melli,
             $request->type => $request[$request->type],
        ])->first()
             ;
    }



    /**
     * @return int
     */
    protected function generateResetToken()
    {
        return rand(str_repeat(1, self::$resetTokenLength), str_repeat(9, self::$resetTokenLength));
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getToken(Request $request)
    {
        if (session()->get(self::getResettingSessionName()) or ($request->haveCode == 'code')) {
            return $this->returnTemplate('ehdafront::auth.passwords.token', [
                 'haveCode'    => $request->haveCode,
                 'sessionName' => self::getResettingSessionName(),
            ]);
        } else {
            return redirect(RouteServiceProvider::action('Auth\ForgotPasswordController@showLinkRequestForm'));
        }
    }



    /**
     * @param ResetPasswordTokenRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function checkToken(ResetPasswordTokenRequest $request)
    {
        if ($request->has('code_melli')) {
            $user = User::where([
                 'code_melli' => $request->code_melli,
            ])->first()
            ;

            $user->spreadMeta();

            if (Hash::check($request->password_reset_token, $user->reset_token)) {
                session()->put(self::getResettingSessionName(), $user->code_melli);

                if (Carbon::now()->lte(Carbon::parse($user->reset_token_expire))) {
                    session(['resetPasswordVerifiedUser' => $user->code_melli]);

                    return redirect(RouteServiceProvider::action('Auth\ForgotPasswordController@newPassword'));
                } else {
                    return redirect()->back()->withErrors([
                         'general' => view('ehdafront::auth.passwords.responses.token_expired')->render(),
                    ])->withInput($request->all())
                         ;
                }
            } else {
                return redirect()->back()->withErrors([
                     'general' => trans('ehdafront::passwords.token'),
                ])->withInput($request->all())
                     ;
            }
        }

        return redirect(RouteServiceProvider::action('Auth\ForgotPasswordController@showLinkRequestForm'));
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function newPassword()
    {
        $current_session_value = session()->get(self::getResettingSessionName());
        if ($current_session_value) {
            return $this->returnTemplate('ehdafront::auth.passwords.new');
        } else {
            return redirect(RouteServiceProvider::action('Auth\ForgotPasswordController@showLinkRequestForm'));
        }
    }



    /**
     * @param ResetPasswordNewRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(ResetPasswordNewRequest $request)
    {
        $session_name          = self::getResettingSessionName();
        $current_session_value = session()->get($session_name);

        if ($current_session_value) {
            session()->forget($session_name);

            $user = user()->where([
                 'code_melli' => $current_session_value,
            ])->first()
            ;


            if ($user) {
                $saving_data = [
                     'password'           => Hash::make($request['new_password']),
                     'reset_token'        => null,
                     'reset_token_expire' => null,
                ];


                Auth::loginUsingId($user->id);

                event(new PasswordChangedByUser($user));

                return $this->jsonAjaxSaveFeedback($user->batchSave($saving_data)->exists, [
                     'success_message'  => trans('ehdafront::passwords.reset'),
                     'success_redirect' => route('home'),
                     'redirectTime'     => 3000,
                ]);
            }
        }

        return $this->jsonAjaxSaveFeedback(false, [
             'danger_redirect' => RouteServiceProvider::action('Auth\ForgotPasswordController@showLinkRequestForm'),
        ]);
    }



    /**
     * @param mixed ...$parameters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function returnTemplate(...$parameters)
    {
        $this->readSettings()
             ->setOpenGraphs()
        ;

        return view(...$parameters);
    }



    /**
     * @return string
     */
    public static function getResettingSessionName()
    {
        return self::$resettingPasswordNationalIdSessionName;
    }
}
