<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/8/18
 * Time: 9:47 AM
 */

namespace Modules\EhdaFront\Http\Controllers;

use App\Models\Post;
use Modules\EhdaFront\Providers\PostsServiceProvider;

class ContactController extends EhdaFrontControllerAbstract
{
    public function index()
    {
        return $this
             ->findStaticPost()
             ->findCommentingPost()
             ->template('contact.main', $this->variables)
             ;
    }



    protected function findStaticPost()
    {
        return $this->appendToVariables('infoPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'contact-us',
        ])->first() ?: new Post);
    }



    protected function findCommentingPost()
    {
        return $this->appendToVariables('commentingPost', PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['commenting'],
             'slug' => 'contact-us',
        ])->first() ?: new Post);
    }
}
