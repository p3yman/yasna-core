<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 4/25/18
 * Time: 5:02 PM
 */

namespace Modules\EhdaFront\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class FileController extends EhdaFrontControllerAbstract
{
    public function disposableDownload(Request $request)
    {
        if (
             ($row = model('file-download')->grabHashid($request->hashid)) and
             $row->exists and
             $row->downloaded_count <= $row->downloadable_count and
             Carbon::now()->lessThan($row->expire_date) and
             ($file = fileManager()->file($row->file_code)->resolve())->getUrl()
        ) {
            $row->downloaded_count++;
            $row->save();
            if ($row->downloaded_count >= $row->downloadable_count) {
                $row->delete();
            }

            if ($fileName = $request->file_title) {
                if (!ends_with($fileName, '.' . ($extension = $file->getExtension()))) {
                    $fileName = $fileName . '.' . $extension;
                }
            } else {
                $fileName = $file->getTitle();
            }

            // Set header
            $headers = [
                 'Content-Type: ' . $file->getMimeType(),
            ];

            // Return response
            return response()->download($file->getPathName(), $fileName, $headers);
        } else {
            return $this->abort(404);
        }
    }
}
