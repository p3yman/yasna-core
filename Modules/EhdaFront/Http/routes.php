<?php

// Volunteer convert route
Route::group(['namespace' => 'Modules\EhdaFront\Http\Controllers', 'prefix' => 'convert'], function () {
    Route::get('volunteer', 'VolunteerConvertController@index');
});


Route::group(['namespace' => 'Modules\EhdaFront\Http\Controllers'], function () {
    /*
    | Passargad
    */
    Route::group(['middleware' => 'web', 'prefix' => 'bpi'], function () {
        Route::get('/login', 'Auth\PassargadLoginController@loginForm')
             ->name('passargad_login')
        ;
        Route::post('/login', 'Auth\PassargadLoginController@login');

        // bpi users converter route
        //Route::group(['namespace' => '\Convert'], function () {
        //    Route::get('/convert/roles', 'ConvertBpiUsersController@index');
        //});

        Route::get('/', 'PassargadController@inquiryForm');
        Route::post('/', 'PassargadController@inquiry')
             ->name('passargad_inquiry')
        ;

        Route::get('/logout', 'Auth\PassargadLoginController@logout')
             ->name('passargad_logout')
        ;


        Route::get('/register', 'PassargadController@registerForm')
             ->name('passargad_register')
        ;
        Route::post('/register', 'PassargadController@register');
    });

    /*
     * API
     */
    Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
        Route::get('/', 'ApiDocumentationController@index');

        /*
         * Api Routes
         */
        Route::group(['prefix' => 'ehda'], function () {
            Route::post('getToken', 'ApiController@getToken');

            Route::group(['middleware' => 'api-token'], function () {
                Route::group(['prefix' => 'card'], function () {
                    Route::post('search', 'CardController@search');
                    Route::post('register', 'CardController@register');
                    Route::post('edit', 'CardController@edit');
                    Route::post('get', 'CardController@get');
                    Route::post('print', 'CardController@savePrintRequest');
                });

                Route::group(['prefix' => 'password'], function () {
                    Route::post('reset', 'CardController@passwordReset');
                });


                Route::post('province/get', 'ListController@provinces');
                Route::post('cities/get', 'ListController@cities');
                Route::post('education/get', 'ListController@educationLevels');
                Route::post('config/get', 'ListController@printerConfigs');
                Route::post('slideshow/get', 'ListController@printerSlideShow');
                Route::post('events/list', 'ListController@activeEvents');
                Route::post('news', 'ListController@broadcastNewsList');
                Route::post('news/details', 'ListController@broadcastNewsSingle');


                Route::post('about-us', 'InformationController@about');
                Route::post('contact-us', 'InformationController@contact');
            });
        });


        /*
         * Yasna Api Routes
         */
        Route::group(['prefix' => 'yasna'], function () {
            Route::post('/sms/send', 'YasnaApiController@sendSms');
            Route::any('/set/log', 'YasnaApiController@setLogs');
        });
    });


    /*
     * Api Manage Routes
     */

    Route::group([
         'prefix'     => 'ehda/api/manage',
         'namespace'  => 'Api\Manage',
         'middleware' => ['web', 'auth', 'is:admin'],
    ], function () {
        Route::group(['prefix' => 'users'], function () {
            Route::get('create', 'UsersController@create');
            Route::get('edit/{hashid}', 'UsersController@edit');
            Route::post('create', 'UsersController@save');
            Route::get('ips/{hashid}', 'UsersController@ips');
            Route::get('portable-printer-configs/{hashid}', 'UsersController@portablePrinterConfigsForm');
            Route::post('portable-printer-configs', 'UsersController@portablePrinterConfigs');

            Route::group(['prefix' => 'ip'], function () {
                Route::get('{hashid}/{action}', 'IpsController@singleAction');
                Route::post('delete', 'IpsController@delete');
                Route::post('save', 'IpsController@save');
            });
        });
    });

    /*
     * Webs
     */
    Route::group(['middleware' => 'web'], function () {
        Route::group(['namespace' => 'Auth', 'middleware' => 'stats'], function () {

            /*
            | Login
            */
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::post('login', 'LoginController@login');
            Route::get('home', 'LoginController@home')->name('home');


            /*
            | Password Reset
            */
            Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
            Route::post('password/reset', 'ForgotPasswordController@sendResetLinkEmail');
            Route::get('password/token/{haveCode?}', 'ForgotPasswordController@getToken');
            Route::post('password/token', 'ForgotPasswordController@checkToken');
            Route::get('password/new', 'ForgotPasswordController@newPassword');
            Route::post('password/new', 'ForgotPasswordController@changePassword');
        });

        /*
         * Organ Donation Card File
         */
        Route::get('/card/show_card/{type}/{user_hash_id}/{mode?}/{hash_type?}', 'CardFileController@generateCard');
        Route::get('/card/show_card_new/{type}/{user_hash_id}/{mode?}/{hash_type?}',
             'NewCardFileController@generateCard');

        Route::get('card/process/{type}/{user_type_hashid}/{mode?}/{hash_type?}', 'CardFileController@generateCard');
        Route::get('card_new/process/{type}/{user_type_hashid}/{mode?}/{hash_type?}',
             'NewCardFileController@generateCard');

        /*
         * Website Front
         */
        Route::group(['middleware' => 'sub-domain'], function () {
            Route::get('/', 'EhdaFrontController@index')->middleware('stats');

            Route::get('fonts', 'FontsController@notLocaleIndex')
                 ->middleware('stats')
            ;


            Route::get('disposable-download/{hashid}/{file_title?}', 'FileController@disposableDownload')
                 ->middleware('stats')
            ;


            /*
             * Post Short Url
             */
            Route::get('{identifier}', 'PostController@shortLink')
                 ->name('post.single.very-short')
                 ->where(
                      'identifier',
                      '^' . \Modules\EhdaFront\Providers\EhdaFrontServiceProvider::shortLinksHashidPrefix() . '(\w|)+$'
                 )->middleware('stats')
            ;
            /*
             * Card Short Url
             */
            Route::get('C{identifier}', 'UserController@shortCard')->middleware('stats');

            // If identifier is string and starts with value of config('ehdafront.prefix.routes.card.short')

            Route::group(['prefix' => '{lang?}', 'middleware' => ['locale', 'stats']], function () {
                Route::get('/', 'EhdaFrontController@index');

                /*
                 * Organ Donation Card
                 */
                Route::group(['prefix' => 'organ_donation_card'], function () {
                    Route::get('/', 'CardController@index');
                    Route::post('register', 'CardController@register');
                });

                /*
                 * Volunteer
                 */
                Route::group(['prefix' => 'volunteers'], function () {
                    Route::get('/', 'VolunteerController@index');
                    Route::post('first-step', 'VolunteerController@registerFirstStep');
                    Route::get('final-step', 'VolunteerController@registerFinalStep');
                    Route::post('final-step', 'VolunteerController@registerFinalStepSubmit');
                    //            Route::get('exam', 'members\VolunteerController@exam');
                    Route::get('celebrities', 'PostController@celebrities');
                });

                /*
                 * User
                 */
                Route::group(['prefix' => 'user', 'middleware' => ['auth', 'is:card-holder']], function () {
                    Route::get('dashboard', 'UserController@index');
                    Route::get('profile', 'UserController@profile');
                    Route::post('update', 'UserController@update');
                });

                /*
                 * States
                 */
                Route::group(['prefix' => 'states'], function () {
                    Route::get('/', 'StatesController@map');
                });

                /*
                 * Cultural Products
                 */
                Route::group(['prefix' => 'cultural-products'], function () {
                    Route::get('/', 'PostController@culturalProducts');
                    Route::post('filter', 'PostController@culturalProductsFilter');
                });

                /*
                 * Posts
                 */
                Route::get('page/{identifier}/{url?}', 'PostController@showWithFullUrl');
                Route::get('previewPost/{id}/{url?}', 'PostController@show');
                Route::get('archive/{postType?}/{folder?}/{category?}', 'PostController@archive');
                Route::get('search', 'PostController@search');
                Route::get('tag/{tag}', 'TagController@index');

                /*
                 * Gallery
                 */
                Route::group(['prefix' => 'gallery'], function () {
                    Route::get('/', 'GalleryController@visual');

                    Route::group(['prefix' => '{type}/{folder?}'], function () {
                        Route::get('', 'PostController@gallery');
                        Route::post('', 'PostController@galleryFilter');
                    });
                });

                /*
                 * Organization
                 */
                Route::get('ngo-foundations', 'OrganizationController@index');

                /**
                 * Interactions
                 */
                Route::get('interactions', 'PostController@interactions');

                /*
                 * Contact
                 */
                Route::get('contact', 'ContactController@index');

                /*
                 * FAQ
                 */
                Route::get('faq', 'FaqController@index');

                /*
                 * Comment
                 */
                Route::group(['prefix' => 'comment'], function () {
                    Route::post('save', 'CommentController@save');
                    Route::post('save-work', 'CommentController@saveWorks');
                });

                /*
                 * Education
                 */
                Route::group(['prefix' => 'education'], function () {
                    Route::get('contact', 'EducationController@contact');
                    Route::get('resources', 'EducationController@resources');
                    Route::post('resources/filter', 'EducationController@resourcesFilter');
                    Route::get('informing-courses', 'PostController@informingCourses');
                });

                /*
                 * Research
                 */
                Route::group(['prefix' => 'research'], function () {
                    Route::get('contact', 'ResearchController@contact');
                    Route::get('resources', 'ResearchController@resources');
                    Route::post('resources/filter', 'ResearchController@resourcesFilter');
                    Route::get('informing-congresses', 'PostController@informingCongresses');
                });

                /*
                 * Social Work
                 */
                Route::group(['prefix' => 'social-work'], function () {
                    Route::get('contact', 'SocialWorkController@contact');
                });

                /*
                 * General Information
                 */
                Route::group(['prefix' => 'general-information'], function () {
                    Route::get('{post?}', 'GeneralInformationController@index');
                });

                /*
                 * Activities
                 */
                Route::get('activities/{category?}', 'ActivitiesController@index');

                /*
                 * Angels
                 */
                Route::group(['prefix' => 'angels'], function () {
                    Route::get('/', 'AngelsController@index');
                    Route::post('search', 'AngelsController@search');
                    Route::get('new', 'AngelsController@newForm');
                    Route::post('new', 'AngelsController@newSubmit');
                });

                /*
                 * Donation
                 */
                Route::group(['prefix' => 'donation'], function () {
                    Route::get('/', 'DonationController@index');
                    Route::post('pay', 'DonationController@pay');
                    Route::group(['prefix' => 'payment'], function () {
                        Route::get('gateway/{donation}', 'DonationController@sendToGateway');
                        Route::get('result', 'DonationController@paymentResult');
                    });
                });

                /*
                 * Send Works
                 */
                Route::get('send-works', 'SendWorksController@index');

                /**
                 * Fonts
                 */
                Route::group(['prefix' => 'fonts'], function () {
                    Route::post('purchase', 'FontsController@purchase');
                    Route::get('fire/{tracking_no}', 'FontsController@fire');
                    Route::get('purchase/callback', 'FontsController@callback');
                    Route::post('tracking', 'FontsController@tracking');
                });


                /**
                 * Report
                 */
                Route::group(['prefix' => 'reports'], function () {
                    Route::get('{report_type}', 'ReportController@showReportsPage');
                });

                /**
                 * Research Projects
                 */
                Route::get('research-projects', 'PostController@researchProjects');

                /*
                *  Test
                */
                Route::group(['prefix' => 'test'], function () {
                    Route::get('contact', 'EhdaFrontTestController@contact');

                    Route::get('volunteer/register', 'EhdaFrontTestController@volunteerRegister');

                    Route::get('faq', 'EhdaFrontTestController@faq');

                    Route::get('support/financial', 'EhdaFrontTestController@support');

                    Route::get('organization', 'EhdaFrontTestController@organization');

                    Route::get('activities', 'EhdaFrontTestController@activities');

                    Route::get('contracts/list', 'EhdaFrontTestController@contractsList');

                    Route::get('annual-reports', 'EhdaFrontTestController@annualReports');

                    Route::get('research-projects', 'EhdaFrontTestController@researchProjects');

                    Route::get('celebrities/list', 'EhdaFrontTestController@celebritiesList');

                    Route::get('celebrities/single', 'EhdaFrontTestController@celebritiesSingle');

                    Route::get('edu/list', 'EhdaFrontTestController@eduList');

                    Route::group(['prefix' => 'pasargad'], function () {
                        Route::get('login', 'EhdaFrontTestController@pasargadLogin');
                        Route::get('inquiry', 'EhdaFrontTestController@pasargadInquiry');
                        Route::get('register', 'EhdaFrontTestController@pasargadRegister');
                    });

                    Route::group(['prefix' => 'information'], function () {
                        Route::get('general', 'EhdaFrontTestController@generalInfo');
                    });

                    Route::group(['prefix' => 'news'], function () {
                        Route::get('list', 'EhdaFrontTestController@newsList');
                        Route::get('single', 'EhdaFrontTestController@newsSingle');
                    });

                    Route::group(['prefix' => 'events'], function () {
                        Route::get('list', 'EhdaFrontTestController@eventsList');
                        Route::get('single', 'EhdaFrontTestController@eventsSingle');
                    });

                    Route::group(['prefix' => 'gallery'], function () {
                        Route::get('categories', 'EhdaFrontTestController@galleryCat');
                        Route::get('list', 'EhdaFrontTestController@galleryList');
                        Route::get('list/text', 'EhdaFrontTestController@galleryListText');
                        Route::get('video/single', 'EhdaFrontTestController@galleryVideoSingle');
                        Route::get('image/single', 'EhdaFrontTestController@galleryImageSingle');
                        Route::get('audio/single', 'EhdaFrontTestController@audioSingle');
                        //                Route::get('single', 'UserController@profile');
                    });
                });
            });


            /*
             * Convert
             */
            Route::group(['prefix' => 'convert', 'namespace' => 'Convert'], function () {
                /*
                 * Post
                 */
                Route::group(['prefix' => 'posts'], function () {
                    Route::get('/', 'ConvertPostsController@index');
                    Route::get('convert/{offset}', 'ConvertPostsController@convert');
                });

                /*
                 * File
                 */
                Route::group(['prefix' => 'file'], function () {
                    Route::get('/', 'ConvertFilesController@index');
                    Route::get('convert/{offset}', 'ConvertFilesController@convert');
                });


                Route::group(['prefix' => 'users'], function () {
                    Route::get('edu-province', 'ConvertUsersController@eduProvince');
                    Route::get('convert/edu-province/{offset}', 'ConvertUsersController@convertEduProvince');
                });
            });
        });
    });
});
