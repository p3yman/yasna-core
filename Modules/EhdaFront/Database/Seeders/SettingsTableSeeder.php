<?php

namespace Modules\EhdaFront\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->mainData());
    }

    public function mainData()
    {
        return
            [
                [
                    'slug'          => "showing_posts_domains",
                    'title'         => "دامنه‌های پست‌های قابل نمایش",
                    'order'         => "150",
                    'category'      => "upstream",
                    'data_type'     => "text",
                    'default_value' => "{{currentDomain}}|global",
                    'is_localized'  => "",
                    'css_class'     => "",
                    'hint'          => "",
                    'created_at'    => \Carbon\Carbon::now()->toDateTimeString(),
                ],
                [
                    'slug'          => "default_domain",
                    'title'         => "دامنه‌ی پیش‌فرض",
                    'order'         => "151",
                    'category'      => "upstream",
                    'data_type'     => "text",
                    'default_value' => "tehran",
                    'is_localized'  => "",
                    'css_class'     => "",
                    'hint'          => "",
                    'created_at'    => \Carbon\Carbon::now()->toDateTimeString(),
                ],
            ];
    }
}
