<?php

namespace Modules\EhdaFront\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EhdaFrontDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(SettingsTableSeeder::class);
        $this->call(PassargadUsersSeeder::class);
    }
}
