<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');

            /*-----------------------------------------------
            | Personal Info ...
            */
            $table->string('code_melli', 20)->nullable()->index();
            $table->string('name')->nullable();

            /*-----------------------------------------------
            | Contact Details ...
            */
            $table->string('mobile')->nullable()->index();

            /*-----------------------------------------------
            | Tracking Details ...
            */
            $table->string('tracking_no')->nullable()->index();

            /*-----------------------------------------------
            | Amount ...
            */
            $table->float('amount', 15, 2)->default(0)->index();

            /*-----------------------------------------------
            | User ...
            */
            $table->unsignedInteger('user_id')->default(0)->nullable()->index();

            /*-----------------------------------------------
            | Ware ...
            */
            $table->unsignedInteger('ware_id')->index();

            /*-----------------------------------------------
            | Meta ...
            */
            $table->longText('meta')->nullable();

            /*-----------------------------------------------
            | Change Logs ...
            */
            $table->timestamps();
            $table->softDeletes();
            $table->timestamp('paid_at')->nullable()->index();
            $table->unsignedInteger('created_by')->default(0)->index();
            $table->unsignedInteger('updated_by')->default(0)->index();
            $table->unsignedInteger('deleted_by')->default(0)->index();
            $table->unsignedInteger('paid_by')->default(0)->index();
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
