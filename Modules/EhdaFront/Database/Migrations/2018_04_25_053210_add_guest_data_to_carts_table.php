<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGuestDataToCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->string('job')->nullable()->index()->after('user_id');
            $table->string('mobile')->nullable()->index()->after('user_id');
            $table->string('email')->nullable()->index()->after('user_id');
            $table->string('name')->nullable()->index()->after('user_id');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('email');
            $table->dropColumn('mobile');
            $table->dropColumn('job');
        });
    }
}
