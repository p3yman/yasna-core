<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('tel');
            $table->dropColumn('province');
            $table->dropColumn('city');
            $table->dropColumn('postal_code');

            // Card Info
            $table->unsignedBigInteger('card_no')->index()->after('id');

            $table->timestamp('published_at')->nullable()->index()->after('deleted_at');

            // Default Role
            $table->timestamp('default_role_deleted_at')->nullable()->after('from_event_id');
            $table->tinyInteger('default_role_status')->default(0)->after('from_event_id');
            $table->string('default_role_key')->nullable()->after('from_event_id');

            $table->boolean('newsletter')->default(0)->after('from_domain');

            $table->string('activities')->nullable()->after('motivation')->index();
            $table->string('alloc_time')->nullable()->after('motivation');

            // Exam
            $table->integer('exam_result')->nullable()->after('meta');
            $table->longText('exam_sheet')->nullable()->after('meta');
            $table->timestamp('exam_passed_at')->nullable()->index()->after('meta');

            // Temporary
            $table->tinyInteger('unverified_flag')->nullable()->after('meta');
            $table->longText('unverified_changes')->nullable()->after('meta');

            // Roles Registered Time
            $table->timestamp('card_registered_at')->nullable()->index()->after('marriage_date');
            $table->timestamp('volunteer_registered_at')->nullable()->index()->after('marriage_date');


            $table->string('work_postal')->nullable()->index()->after('home_address');
            $table->string('work_tel')->nullable()->index()->after('home_address');
            $table->unsignedInteger('work_city')->nullable()->index()->after('home_address');
            $table->unsignedInteger('work_province')->nullable()->index()->after('home_address');
            $table->longText('work_address')->nullable()->after('home_address');
            $table->string('home_postal')->nullable()->index()->after('home_address');
            $table->string('home_tel')->nullable()->index()->after('home_address');
            $table->unsignedInteger('home_city')->nullable()->index()->after('home_address');
            $table->unsignedInteger('home_province')->nullable()->index()->after('home_address');

            $table->timestamp('reset_token_expire')->after('reset_token')->default(null);
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->unsignedInteger('city')->nullable()->index()->after('tel_emergency');
            $table->unsignedInteger('province')->nullable()->index()->after('tel_emergency');
            $table->string('tel')->nullable()->after('tel_emergency');
            $table->string('postal_code')->nullable()->after('home_address');


            $table->dropColumn('card_no');
            $table->dropColumn('published_at');
            $table->dropColumn('default_role_deleted_at');
            $table->dropColumn('default_role_status');
            $table->dropColumn('default_role_key');
            $table->dropColumn('newsletter');
            $table->dropColumn('activities');
            $table->dropColumn('alloc_time');
            $table->dropColumn('exam_result');
            $table->dropColumn('exam_sheet');
            $table->dropColumn('exam_passed_at');
            $table->dropColumn('unverified_flag');
            $table->dropColumn('unverified_changes');
            $table->dropColumn('card_registered_at');
            $table->dropColumn('volunteer_registered_at');
            $table->dropColumn('work_postal');
            $table->dropColumn('work_tel');
            $table->dropColumn('work_city');
            $table->dropColumn('work_province');
            $table->dropColumn('work_address');
            $table->dropColumn('home_postal');
            $table->dropColumn('home_tel');
            $table->dropColumn('home_city');
            $table->dropColumn('home_province');
            $table->dropColumn('reset_token_expire');
        });
    }
}
