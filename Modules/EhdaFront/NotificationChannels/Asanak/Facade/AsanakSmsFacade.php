<?php

namespace Modules\EhdaFront\NotificationChannels\Asanak\Facades;

use Illuminate\Support\Facades\Facade;

class AsanakSmsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AsanakSms';
    }
}
