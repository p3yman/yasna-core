<?php

namespace Modules\EhdaFront\NotificationChannels\Asanak\Facade;

use http\Exception;

class AsanakSms
{
    public function __construct()
    {
    }

    public static function send($destination, $msgBody)
    {
        $destination = self::purifier_destination($destination);
        if (!$destination) {
            return false;
        }
        
        $URL = config('AsanakSmsConfig.url');
        $msg = urlencode(trim($msgBody));

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query([
                'username'    => config('AsanakSmsConfig.username'),
                'password'    => config('AsanakSmsConfig.password'),
                'source'      => config('AsanakSmsConfig.source'),
                'destination' => $destination,
                'message'     => $msg,
            ]));

//      receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close($ch);

        try {
            if (($return = $server_output)) {
                return $return;
            }
        } catch (Exception $ex) {
            return $ex->errorMessage();
        }
    }

    private static function purifier_destination($dis)
    {
        $destination = '';
        if (is_array($dis)) {
            for ($i = 0; $i < count($dis); $i++) {
                if (is_numeric($dis[$i]) and strlen($dis[$i]) == 11) {
                    $destination .= $dis[$i] . ',';
                }
            }
        } else {
            if (is_numeric($dis) and strlen($dis) == 11) {
                $destination = $dis;
            }
        }

        return $destination;
    }
}
