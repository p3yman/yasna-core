<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 04/03/2018
 * Time: 10:24 AM
 */

namespace Modules\EhdaFront\NotificationChannels\Asanak;

use Illuminate\Notifications\Notification;
use Modules\EhdaFront\NotificationChannels\Asanak\Exceptions\CouldNotSendNotification;
use Modules\EhdaFront\NotificationChannels\Asanak\Facade\AsanakSms;
use Modules\EhdaManage\Services\Fanap\FanapServiceProvider;

class AsanakChannel
{
    protected $asanakSms;

    /**
     * @return  void
     */
    public function __construct(AsanakSms $asanakSms)
    {
        $this->asanakSms = $asanakSms;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed                                  $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     *
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$to = $notifiable->routeNotificationFor('asanak')) {
            return;
        }

        if (is_string($message = $notification->toAsanak($notifiable))) {
            $message = new AsanakMessage($message);
        }

        // @todo: temporary fixed attempts problem
        FanapServiceProvider::sendMessage($to, $message->content);

        return true;

        $response = $this->asanakSms->send($to, $message->content);

        if (!$response) {
            throw CouldNotSendNotification::serviceRespondedWithAnError($response);
        }

        return $response;
    }
}
