<?php

return [

    'username' => env('ASANAK_SMS_USERNAME'),
    'password' => env('ASANAK_SMS_PASSWORD'),
    'source'   => env('ASANAK_SMS_SOURCE'),
    'url'      => env('ASANAK_SMS_URL'),

];
