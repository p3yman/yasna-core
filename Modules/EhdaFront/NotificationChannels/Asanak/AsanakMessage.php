<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 04/03/2018
 * Time: 10:44 AM
 */

namespace Modules\EhdaFront\NotificationChannels\Asanak;

class AsanakMessage
{

    /**
     * The message content.
     *
     * @var string
     */
    public $content;

    /**
     * Create a new message instance.
     *
     * @param  string $content
     */
    public function __construct($content = '')
    {
        $this->content($content);
    }

    /**
     * Set the message content.
     *
     * @param  string $content
     *
     * @return $this
     */
    public function content($content)
    {
        $this->content = $content;

        return $this;
    }
}
