<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 04/03/2018
 * Time: 11:18 AM
 */

namespace Modules\EhdaFront\NotificationChannels\Asanak\Exceptions;

class CouldNotSendNotification extends \Exception
{
    public static function serviceRespondedWithAnError($response)
    {
        return new static("Notification was not sent. Asanak responded with `$response`");
    }
}
