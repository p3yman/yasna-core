<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/18/18
 * Time: 12:24 PM
 */

namespace Modules\EhdaFront\NotificationChannels\EhdaSms\Exceptions;

use Exception;

class CouldNotSendNotification extends Exception
{
    /**
     * @param array $providers
     * @param       $response
     *
     * @return CouldNotSendNotification
     */
    public static function serviceRespondedWithAnError($response)
    {
        return new static("Notification was not sent. Sending process' response was `$response`");
    }
}
