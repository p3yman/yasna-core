<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/18/18
 * Time: 10:21 AM
 */

namespace Modules\EhdaFront\NotificationChannels\EhdaSms;

class EhdaSmsMessage
{

    /**
     * The message content.
     *
     * @var string
     */
    public $content;

    /**
     * Create a new message instance.
     *
     * @param  string $content
     */
    public function __construct($content = '')
    {
        $this->content($content);
    }

    /**
     * Set the message content.
     *
     * @param  string $content
     *
     * @return $this
     */
    public function content($content)
    {
        $this->content = $content;

        return $this;
    }
}
