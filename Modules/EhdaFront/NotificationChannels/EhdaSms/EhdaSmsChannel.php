<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 6/17/18
 * Time: 6:08 PM
 */

namespace Modules\EhdaFront\NotificationChannels\EhdaSms;

use App\Models\User;
use Illuminate\Notifications\Notification;
use Modules\EhdaFront\NotificationChannels\EhdaSms\Exceptions\CouldNotSendNotification;
use Modules\EhdaFront\Services\EhdaSms\Message;

class EhdaSmsChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed                                  $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     *
     * @return bool
     * @throws CouldNotSendNotification
     */
    public function send(User $notifiable, Notification $notification)
    {
        if (!$to = $notifiable->routeNotificationForSms()) {
            return false;
        }

        $message = $notification->toSms($notifiable);

        $response = $this->sendMessage($to, $message->content);
        
        try {
            if ($response) {
                return $response;
            } else {
                throw CouldNotSendNotification::serviceRespondedWithAnError($response);
            }
        } catch (CouldNotSendNotification $exception) {
            //@TODO: This situation must be handled
        }
    }



    /**
     * @param string $to
     * @param string $message
     *
     * @return bool
     */
    protected function sendMessage($to, $message)
    {
        return Message::send($to, $message);
    }
}
