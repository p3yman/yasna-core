<?php

namespace Modules\EhdaFront\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\InteractsWithQueue;
use Modules\EhdaFront\NotificationChannels\EhdaSms\EhdaSmsChannel;
use Modules\EhdaFront\NotificationChannels\EhdaSms\EhdaSmsMessage;
use Modules\EhdaFront\Providers\ToolsServiceProvider;

class PasswordResetRequested extends Notification implements ShouldQueue
{
    use Queueable, InteractsWithQueue;

    /**
     * Type of the Notification
     *
     * @var string
     */
    protected $type;

    /**
     * Default Notification Type
     *
     * @var string
     */
    protected $defaultType = 'mail';

    /**
     * List of Notification Types an Related Channels for Each One
     *
     * @var array
     */
    protected $typeChannelMap = [
         'mobile' => EhdaSmsChannel::class,
         'email'  => 'mail',
    ];

    /**
     * Token that Will be Sent
     *
     * @var string
     */
    protected $token;


    /**
     * Try to run listener again if not processed
     *
     * @var integer
     */
    public $tries = 5;



    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($type, $token)
    {
        $this->type  = $this->typeIsAccepted($type) ? $type : $this->defaultType;
        $this->token = $token;
    }



    /**
     * Check if specified type is an acceptable type
     *
     * @param $type
     *
     * @return bool
     */
    protected function typeIsAccepted($type)
    {
        return array_key_exists($type, $this->typeChannelMap);
    }



    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [$this->typeChannelMap[$this->type]];
    }



    /**
     * Get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return EhdaSmsMessage
     */
    public function toSms($notifiable)
    {
        return (new EhdaSmsMessage)
             ->content($this->getSmsText());
    }



    /**
     * Generate text that should be sent in sms
     *
     * @return string
     */
    protected function getSmsText()
    {
        return trans('ehdafront::passwords.reset_sms', ['token' => $this->token])
             . "\n\r"
             . config('ehdamanage.public-domain');
    }



    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
             ->markdown(
                  ToolsServiceProvider::getEmailTemplatePath('default_email'),
                  ['text' => $this->getEmailText()]
             )
             ->subject(trans('ehdafront::people.form.recover_password'))
             ;
    }



    /**
     * Generate text that should be sent by email
     *
     * @return string
     */
    protected function getEmailText()
    {
        return nl2br(
             trans('ehdafront::passwords.reset_mail', ['token' => $this->token])
             . "\n\r"
             . config('ehdamanage.public-domain')
        );
    }



    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
