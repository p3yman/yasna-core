<?php

namespace Modules\EhdaFront\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\EhdaFront\NotificationChannels\EhdaSms\EhdaSmsChannel;
use Modules\EhdaFront\NotificationChannels\EhdaSms\EhdaSmsMessage;
use Modules\EhdaFront\Providers\ToolsServiceProvider;

class PasswordResetRequestedViaApi extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Password that Will be Sent
     *
     * @var string
     */
    protected $password;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [/*'mail',*/ EhdaSmsChannel::class];
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return EhdaSmsMessage
     */
    public function toSms($notifiable)
    {
        return (new EhdaSmsMessage)
            ->content($this->getSmsText());
    }

    /**
     * Generate text that should be sent in sms
     *
     * @return string
     */
    protected function getSmsText()
    {
        return trans('ehdafront::api.password.reset.notification.sms', ['password' => $this->password])
            . "\n\r"
            . config('ehdamanage.public-domain');
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown(
                ToolsServiceProvider::getEmailTemplatePath('default_email'),
                ['text' => $this->getEmailText()]
            )
            ->subject(trans('ehdafront::people.form.recover_password'));
    }

    /**
     * Generate text that should be sent by email
     *
     * @return string
     */
    protected function getEmailText()
    {
        return nl2br(
            trans('ehdafront::api.password.reset.notification.email', ['password' => $this->password])
            . "\n\r"
            . config('ehdamanage.public-domain')
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
