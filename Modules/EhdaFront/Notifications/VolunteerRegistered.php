<?php

namespace Modules\EhdaFront\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\EhdaFront\NotificationChannels\EhdaSms\EhdaSmsChannel;
use Modules\EhdaFront\NotificationChannels\EhdaSms\EhdaSmsMessage;
use Modules\EhdaFront\Providers\ToolsServiceProvider;

class VolunteerRegistered extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        // @todo: temporary disabled `mail` channel
        return [/*'mail',*/ EhdaSmsChannel::class];
    }

    /**
     * Get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return EhdaSmsMessage
     */
    public function toSms($notifiable)
    {
        return (new EhdaSmsMessage)
            ->content($this->getSmsText($notifiable));
    }

    /**
     * Generate the message that should be sent by sms
     *
     * @param $notifiable
     *
     * @return string
     */
    protected function getSmsText($notifiable)
    {
        return trans('ehdafront::general.volunteer_section.register_success_message.sms', [
            'name' => $notifiable->full_name,
            'site' => config('ehdamanage.public-domain'),
        ]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown($this->getEmailTemplatePath(), $this->getViewData($notifiable))
            ->subject($this->getEmailSubject());
    }

    /**
     * Generate data that should be sent to view blade
     *
     * @param User $notifiable
     *
     * @return array
     */
    protected function getViewData($notifiable)
    {
        return ['text' => $this->getEmailText($notifiable)];
    }

    /**
     * Generate text message that should be sent in email
     *
     * @param User $notifiable
     *
     * @return string
     */
    protected function getEmailText($notifiable)
    {
        return nl2br(trans('ehdafront::general.volunteer_section.register_success_message.email', [
            'name' => $notifiable->full_name,
            'site' => config('ehdamanage.public-domain'),
        ]));
    }

    /**
     * Get full path of the blade that will be used as email template
     *
     * @return string
     */
    protected function getEmailTemplatePath()
    {
        return ToolsServiceProvider::getEmailTemplatePath('default_email');
    }

    /**
     * Get subject of the email
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return trans('ehdafront::general.volunteer_section.register');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
