<div class="share col-xs-12 pt15">
    @include('ehdafront::layouts.widgets.add-to-any', [
        'shareUrl' => $post->short_url,
        'shortUrl' => $post->short_url,
    ])
</div>
