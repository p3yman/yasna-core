<div class="audio-player">
	<!-- Top Info -->
	<div id="title">
		<span id="track"></span>
		<div id="timer">0:00</div>
		<div id="duration">0:00</div>
	</div>

	<!-- Controls -->
	<div class="controlsOuter">
		<div class="controlsInner">
			<div id="loading"></div>
			<div class="btn" id="playBtn"></div>
			<div class="btn" id="pauseBtn"></div>
			<div class="btn" id="prevBtn"></div>
			<div class="btn" id="nextBtn"></div>
		</div>
		{{--<div class="btn" id="playlistBtn"></div>--}}
		{{--<div class="btn" id="volumeBtn"></div>--}}
	</div>

	<!-- Progress -->
	<div id="waveform"></div>
	<div id="bar"></div>
	<div id="progress"></div>

	<!-- Playlist -->
{{--<div id="playlist">--}}
{{--<div id="list"></div>--}}
{{--</div>--}}

<!-- Volume -->
	{{--<div id="volume" class="fadeout">
		<div id="barFull" class="bar"></div>
		<div id="barEmpty" class="bar"></div>
		<div id="sliderBtn"></div>
	</div>--}}

</div>


@section('endOfBody')
	{!! Html::script(Module::asset('ehdafront:libs/howler/dist/howler.core.min.js')) !!}
	{!! Html::script(Module::asset('ehdafront:libs/howler/siriwave.js')) !!}
	<script>
        var musicList = {!! json_encode(array_map(function ($file) {
        	return ['title' => $file['title'], 'file' => $file['url']];
        }, $musics)) !!};
	</script>
	{!! Html::script(Module::asset('ehdafront:libs/howler/player.js')) !!}
@append