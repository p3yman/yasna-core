@php

	$title = $post->title;
	$caption = $post->abstract;

	$cover = doc($post->featured_image)->version('thumb')->getUrl() ?: Module::asset('ehdafront:images/demo/events/img3.jpg');

	$musics = [];
	foreach ($post->files as $file) {
		$musicObject = doc($file['src'])->resolve();
		if ($music = $musicObject->getUrl()) {
			$musics[] = [
				'title' => $file['label'] ?: $musicObject->getTitle(),
				'url' => $music,
				'downloadUrl' => $musicObject->getDownloadUrl(),
				'size' => $musicObject->getSizeText(),
			];
		}
	};

@endphp

<div class="container-fluid bg-gradient mt50">
	<div class="container">
		<div class="row">
			@include($viewFolder . '.audio-player')
		</div>
	</div>
</div>
<div class="container">
	<div class="row mb40">
		<div class="audio-detail">
			<div class="album-art">
				<img src="{{ $cover }}" alt="cover">
			</div>
			<div class="audio-info">
				<h1>{{ $title }}</h1>
				<div class="caption">
					{{ $caption }}
				</div>
			</div>
		</div>
		@foreach($musics as $music)
			<div class="audio-detail">
				<div class="audio-info">
					<h1>{{ $music['title'] }}</h1>
					<div class="caption">
					</div>
					<span class="download">
						<span class="download-size">
							{{ $music['size'] }}
						</span>
						<a href="{{ $music['downloadUrl'] }}" class="btn btn-link text-green">
							<i class="fa fa-download"></i>
							{{ trans('ehdafront::general.download') }}
						</a>
					</span>
				</div>
			</div>
		@endforeach

	</div>

	<div class="row mb40">
		@if(($tags = $post->tags)->count())
			@include('ehdafront::layouts.meta_tags', ['metaTags' => ['keywords' => $tags->pluck('slug')->toArray()]])
			<div class="col-sm-12">
				<div class="tags">
                    <span class="text mr10">
                        {{ trans('ehdafront::posts.features.tags').":" }}
                    </span>
					@foreach($tags as $tag)
						<a class="btn btn-sm btn-info f12 mb5"
						   href="{{ $tag->index_url }}">{{ $tag->slug }}</a>
					@endforeach
				</div>
			</div>
		@endif
	</div>
	<hr>
</div>

