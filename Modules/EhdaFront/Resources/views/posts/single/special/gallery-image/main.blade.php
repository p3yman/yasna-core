@php
	$posttype = $post->posttype;
	$pageName = $posttype->titleIn(getLocale());
	$sub_title = $post->default_category_model->titleIn(getLocale());
	$gallery_title = $post->title;
	$bg_img = doc($post->featured_image)->getUrl() ?: Module::asset('ehdafront:images/gallery.jpg');
	$image_set = "example1";
@endphp

@include('ehdafront::layouts.widgets.title-with-image',[
	"title" => $pageName,
	"bg_img" => $bg_img,
	"blur" => $post->spreadMeta()->single_cover_blur,
])
<div class="container-fluid">
	<div class="col-md-12">
		@include($viewFolder . '.gallery-images',[
				"gallery_title" => $gallery_title ,
			])

		<div class="col-sm-6">
			@if(($tags = $post->tags)->count())
				@include('ehdafront::layouts.meta_tags', ['metaTags' => ['keywords' => $tags->pluck('slug')->toArray()]])
				<div class="tags">
                    <span class="text mr10">
                        {{ trans('ehdafront::posts.features.tags').":" }}
                    </span>
					@foreach($tags as $tag)
						<a class="btn btn-sm btn-info f12 mb5"
						   href="{{ $tag->index_url }}">{{ $tag->slug }}</a>
					@endforeach
				</div>
			@endif
		</div>
		<div class="col-sm-6">
			@include('ehdafront::layouts.widgets.add-to-any',[
				"shortUrl" => $post->short_url,
			])
		</div>
	</div>
</div>

@section('head')
	{!! Html::style(Module::asset('ehdafront:libs/lightbox/dist/css/lightbox.min.css')) !!}
@append

@section('endOfBody')
	{!! Html::script(Module::asset('ehdafront:libs/shufflejs/shufflejs.js')) !!}
	{!! Html::script(Module::asset('ehdafront:libs/lightbox/dist/js/lightbox.min.js')) !!}

	<script>
        jQuery(function ($) {

            var Shuffle = window.Shuffle;

            var myShuffle = new Shuffle(document.querySelector('.my-shuffle'), {
                itemSelector: '.js-item',
                sizer       : '.my-sizer-element',
                buffer      : 1,
            });

            lightbox.option({
                albumLabel: "",
            })

        }); //End Of siaf!

	</script>

@append