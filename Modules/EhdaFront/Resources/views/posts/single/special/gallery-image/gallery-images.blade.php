<h3 class="gallery-title">{{ $gallery_title }}</h3>
<div class="gallery js-grid shuffle my-shuffle mb50">
	@foreach($post->files as $image)
		@php $originalImage = doc($image['src'])->resolve() @endphp
		@if ($originalImageUrl = $originalImage->getUrl())
			@php
				$thumbImage = doc($image['src'])->version('thumb')->getUrl();
			@endphp
			<figure class="tile-column my-sizer-element js-item" {{--data-groups='["first"]'--}}>
				<div class="aspect aspect--16x9">
					<div class="aspect__inner">
						<a href="{{ $originalImageUrl }}" data-lightbox="{{ $image_set }}"
						   data-title="{{ $image['label'] }}">
							<img src="{{ $thumbImage }}" alt="{{ $originalImage->getTitle() }}">
						</a>
					</div>
				</div>
			</figure>
		@endif
	@endforeach
</div>
<!-- !END Gallery -->