@php
	$videos = [];
	foreach ($post->files as $file){
		if ($aparatHash = GalleryTools::getAparatId($file['link'])) {
			$videos[] = [
				'hash' => $aparatHash,
				'imageHashid' => $file['src'],
				'title' => $file['label'],
			];
		}
	}
	$posttype = $post->posttype;
@endphp

@if (count($videos))
	<div class="video-wrapper mb60">
		<div class="col-lg-11 col-centered pt30">
			<button class="btn btn-link text-gray theme-mode js-themeToggle">
                <span class="day mode">
                    <i class="fa fa-sun-o"></i>
					{{ trans('ehdafront::general.theme.day') }}
                </span>
				<span class="night mode noDisplay">
                    <i class="fa fa-moon-o"></i>
					{{ trans('ehdafront::general.theme.night') }}
                </span>
			</button>
			<div class="row">
				@php $firstVideo = $videos[0] @endphp
				@section('endOfBody')
					<script type="text/JavaScript" id="video-script"
							src="https://www.aparat.com/embed/{{ $firstVideo['hash'] }}?data[rnddiv]=player-div&data[responsive]=yes">
					</script>
				@append
				<div class="col-md-8 @if(count($videos) == 1) col-centered @endif ">
					@include($viewFolder . '.video-player',[
						"title" => $post->title ,
						"caption" => $post->abstract ,
						//"views" => "1500" ,
						//"comments" => "300" ,
						"tags" => $post->tags,
					])

				</div>
				@if(count($videos) > 1)
					<div class="col-md-4">
						<div class="video-playlist">
							<div class="episodes">
								@foreach($videos as $video)
									@include($viewFolder . '.video-episode-row',[
										"title"=> $video['title'],
										"img"=> doc($video['imageHashid'])
											->posttype($posttype)
											->config('attachments')
											->version('150x85')
											->getUrl(),
										"hash" => $video['hash'],
										"class" => $loop->index ? "" : "current",
									])
								@endforeach

								@section('endOfBody')
									<script>
                                        $(document).ready(function () {
                                            let videoRows = $('.video-row');
                                            videoRows.click(function () {
                                                let that   = $(this);
                                                let hashid = that.data('hashid');
                                                if (hashid) {
                                                    let newSrc        = "https://www.aparat.com/embed/" +
                                                        hashid +
                                                        "?data[rnddiv]=player-div&data[responsive]=yes";
                                                    let currentScript = $('#video-script');

                                                    let newScript  = document.createElement("script");
                                                    newScript.type = "text/javascript";
                                                    newScript.id   = 'video-script';
                                                    newScript.src  = newSrc;
                                                    currentScript.after(newScript);
                                                    currentScript.remove();

                                                    videoRows.removeClass('current');
                                                    that.addClass('current');
                                                }
                                            });
                                        });
									</script>
								@append
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
@endif