@if (($files = $post->files) and is_array($files) and count($files))
{{--	@if ((user()->is_a("$posttype->slug-subscriber")))--}}
		<div class="row mb40">
			@foreach($post->files as $file)
				@php $doc = doc($file['src'])->resolve() @endphp
				@if ($url = $doc->getUrl())
					<div class="audio-detail">
						<div class="audio-info">
							<h1>{{ $file['label'] ?: $doc->getTitle() }}</h1>
							<div class="caption">
							</div>
							<span class="download">
								<span class="download-size">
									{{ $doc->getSizeText() }}
								</span>
								<a href="{{ $doc->getDownloadUrl() }}" class="btn btn-link text-green">
									<i class="fa fa-download"></i>
									{{ trans('ehdafront::general.download') }}
								</a>
							</span>
						</div>
					</div>
				@endif
			@endforeach
		</div>
	{{--@else--}}
		{{--@if ($alertPost = PostTools::getDynamicMessage("$posttype->slug-no-access-files"))--}}
			{{--<div class="row">--}}
				{{--<div class="col-xs-12">--}}
					{{--<div class="alert alert-danger">--}}
						{{--{!! $alertPost !!}--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--@endif--}}
	{{--@endif--}}
@endif
