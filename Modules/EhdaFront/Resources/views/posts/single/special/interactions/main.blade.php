@include('ehdafront::layouts.single-post-layout',[
    "title" => $post->title ,
    "tags" => $post->tags ,
    "short_url" => $post->short_url ,
    "time" => ad(echoDate($post->published_at, 'H:i / j F Y')),
    "content" => $post->text ,
])