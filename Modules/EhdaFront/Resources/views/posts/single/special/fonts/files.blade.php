@if (count($files))
	<div class="row mb40">
		@foreach($files as $file)
			<div class="audio-detail">
				<div class="audio-info">
					<h1>{{ $file['title'] }}</h1>
					<div class="caption">
					</div>
					<span class="download">
						<span class="download-size">
							{{ $file['size'] }}
						</span>
						<a href="{{ $file['download_link'] }}" class="btn btn-link text-green">
							<i class="fa fa-download"></i>
							{{ trans('ehdafront::general.download') }}
						</a>
					</span>
				</div>
			</div>
		@endforeach
	</div>
@endif
