@if($paymentInfo)
	<div class="row" id="payment-result">
		<div class="alert alert-{{ $paymentInfo['success'] ? 'success' : 'danger' }}">
			{!! $paymentInfo['message'] !!}

			<br/>

			{{ trans('ehdafront::validation.attributes.tracking_number') }}:
			{{ ad($paymentInfo['tracking_number']) }}
		</div>

		@if (array_key_exists('served_files', $paymentInfo))
			@include($viewFolder . '.files', ['files' => $paymentInfo['served_files']])
		@endif
	</div>
@endif