@section('additionalContent')
	@include('ehdafront::posts.single.special.event.related-files', [
		'title' => trans('ehdafront::general.activities.follow')
	])
@append

@include('ehdafront::layouts.single-post-layout',[
    "title" => $post->title ,
    //"subtitle" => $subtitle ,
    "tags" => $post->tags ,
    "short_url" => $post->short_url ,
    "time" => ad(echoDate($post->published_at, 'H:i / j F Y')),
    "content" => $post->text ,
])