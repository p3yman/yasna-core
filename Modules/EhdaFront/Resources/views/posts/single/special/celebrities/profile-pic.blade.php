@php
	$medal_class = (isset($medal) and $medal)? "text-gold" : "text-silver";
@endphp
<div class="celebrity-pic-container">
	<div class="celebrity-pic">
		<img src="{{ $pic }}" alt="{{ $alt }}">
		<span class="celebrity_medal {{ $medal_class }}">
			<i class="fa fa-trophy"></i>
		</span>
	</div>

</div>