<hr>

<div class="row">
	<div class="col-sm-12">
		<div class="celebrity-info-title">
			{{ trans('ehdafront::general.volunteer_section.attributes.related_activities') }}
		</div>
		<br>
		{!! $post['related_activities'] !!}
	</div>
</div>