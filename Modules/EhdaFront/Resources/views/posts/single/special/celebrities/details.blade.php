<div class="row">
	<div class="col-sm-5">
		@php
			$image = fileManager()
				->file($post->featured_image)
				->posttype($posttype)
				->config('featured_image')
				->version('350x350')
				->resolve()
		@endphp
		@include($viewFolder . '.profile-pic',[
			"pic" => $image->getUrl() ,
			"alt" => $image->getTitle() ,
			"medal" => $post->has_medal ,
		])
	</div>
	<div class="col-sm-7">
		@include($viewFolder . '.profile-info')
	</div>
</div>