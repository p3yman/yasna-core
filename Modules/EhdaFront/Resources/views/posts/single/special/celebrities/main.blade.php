@php
	$post->spreadMeta();
	$posttype = $post->posttype;
@endphp

<div class="celebrity">
	@include($viewFolder . '.details')

	@if($post->text)
		@include($viewFolder . '.explanation')
	@endif
</div>