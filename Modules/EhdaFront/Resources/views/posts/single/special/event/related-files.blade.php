@if ($post->report_link_picture or $post->report_link_video or $post->report_link_text)
	<div class="row mt30">
		@include('ehdafront::layouts.underlined_heading', [
			'text' => $title ?? trans('ehdafront::general.event.title.follow_event'),
			'color' => 'green',
		])
		<div class="col-md-12 col-centered">
			<div class="well linked-file-wrapper">
				@if ($post->report_link_picture)
					<div class="linked-file">
						<a href="{{ $post->report_link_picture }}">
							<div class="file-icon">
								<i class="fa fa-{{ 'image' }}"></i>
							</div>
							<div class="title">
								{{ trans('ehdafront::general.report.picture') }}
							</div>
						</a>
					</div>
				@endif
				@if ($post->report_link_video)
					<div class="linked-file">
						<a href="{{ $post->report_link_video }}">
							<div class="file-icon">
								<i class="fa fa-{{ 'file-video-o' }}"></i>
							</div>
							<div class="title">
								{{ trans('ehdafront::general.report.video') }}
							</div>
						</a>
					</div>
				@endif
				@if ($post->report_link_text)
					<div class="linked-file">
						<a href="{{ $post->report_link_text }}">
							<div class="file-icon">
								<i class="fa fa-{{ 'file-text-o' }}"></i>
							</div>
							<div class="title">
								{{ trans('ehdafront::general.report.text') }}
							</div>
						</a>
					</div>
				@endif
			</div>
		</div>
	</div>
@endif