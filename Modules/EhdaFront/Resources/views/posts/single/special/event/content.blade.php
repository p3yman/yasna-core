@php $zeroDate = '-' @endphp
@php $zeroTime = ad('00:00') @endphp
<div class="row">
	<div class="col-md-12">
		<div class="event-content clearfix">
			<div class="details">
				<div class="image-wrapper">
					<img src="{{ $featuredImageUrl }}" alt="{{ $featuredImageTitle }}">
				</div>
				<div class="status-tag mb5 {{ $post->event_status }}">
					{{ $post->event_status_text }}
				</div>

				@php $dateParts = [] @endphp
				@if (
					$post->event_starts_at and
					($startDate = ad(echoDate($post->event_starts_at, 'j F'))) and
					($startDate != $zeroDate)
				)
					@php $dateParts[] = $startDate @endphp
				@endif
				@if (
					$post->event_ends_at and
					($endDate = ad(echoDate($post->event_ends_at, 'j F'))) and
					($endDate != $zeroDate)
				)
					@php $dateParts[] = $endDate @endphp
				@endif
				@if ($dateString = implode(' - ', $dateParts))
					<div class="dates mb5 ">
						<i class="fa fa-calendar text-gold"></i>
						{{ $dateString }}
					</div>
				@endif

				@php $timeParts = [] @endphp
				@if (
					$post->event_starts_at and
					($startTime = ad(echoDate($post->event_starts_at, 'H:i'))) and
					($startTime != $zeroTime) and
					($startTime != $zeroDate)
				)
					@php $timeParts[] = $startTime @endphp
				@endif
				@if (
					$post->event_ends_at and
					($endTime = ad(echoDate($post->event_ends_at, 'H:i'))) and
					($endTime != $zeroTime) and
					($endTime != $zeroDate)
				)
					@php $timeParts[] = $endTime @endphp
				@endif
				@if ($timeString = implode(' - ', $timeParts))
					<div class="times mb5 ">
						<i class="fa fa-clock-o text-gold"></i>
						<span>
							{{ $timeString }}
						</span>
					</div>
				@endif

				@if($post->location)
					<div class="location mb5 ">
						<i class="fa fa-map-marker text-gold"></i>
						{{ $post->location }}
					</div>
				@endif
			</div>
			{!! $post->text !!}

			@if(($tags = $post->tags)->count())
				@include('ehdafront::layouts.meta_tags', ['metaTags' => ['keywords' => $tags->pluck('slug')->toArray()]])
				<div class="tags">
                    <span class="text mr10">
                        {{ trans('ehdafront::posts.features.tags').":" }}
                    </span>
					@foreach($tags as $tag)
						<a class="btn btn-sm btn-info f12 mb5"
						   href="{{ $tag->index_url }}">{{ $tag->slug }}</a>
					@endforeach
				</div>
			@endif
		</div>
		@include($viewFolder . '.related-files')
	</div>
	<div class="col-xs-12">
		<div class="col-xs-12">
			@include('ehdafront::layouts.widgets.add-to-any',[
				"shortUrl" => $post->short_url ,
			])
		</div>
	</div>
</div>

