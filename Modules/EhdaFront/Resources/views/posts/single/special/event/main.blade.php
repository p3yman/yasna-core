@php
	$post->spreadMeta();
	$startCarbon = $post->event_starts_at ? \Carbon\Carbon::parse($post->event_starts_at) : null;
	$endCarbon = $post->event_ends_at ? \Carbon\Carbon::parse($post->event_ends_at) : null;
@endphp

@include($viewFolder . '.heading',[
	"title" => $post->title ,
	"src" => $featuredImageUrl = ($featuredImage = doc($post->featured_image)->resolve())->getUrl() ,
	"alt" => $featuredImageTitle = $featuredImage->getTitle(),
	"attendees" => EhdaFront::getEventRegistrations($post) ,
	"js_input" => ($startCarbon and $endCarbon)
		? [
			"start" => $startCarbon->format('M j, Y H:i:s') ,
			"end" => $endCarbon->format('M j, Y H:i:s') ,
		]: null,
	"start_date" => $startDate = ad(echoDate($startCarbon, 'j F')) ,
	"end_date" => $endDate = ad(echoDate($endCarbon, 'j F')) ,
	"start_time" => $startTime = ad(echoDate($startCarbon, 'H:i')) ,
	"end_time" => $endTime = ad(echoDate($endCarbon, 'H:i')) ,
	"location" => $post->location ,
	"blur" => $post->spreadMeta()->single_cover_blur,
])

<div class="container mt60">
	<!-- Event Content -->
@include($viewFolder . '.content')
<!-- !END Event Content -->

	@include('ehdafront::layouts.underlined_heading', [
		'text' => trans('ehdafront::posts.features.dont_miss'),
		'color' => 'green',
	])

	<div class="row">
		@foreach($post->similars(4) as $similar)
			<div class="col-xxs col-xs-6 col-md-4 col-lg-3">
				@include('ehdafront::layouts.widgets.box',[
					"title" => $similar->title ,
					"image" => doc($similar->featured_image)->getUrl(),
					"link" => [
						"link" => $similar->direct_url ,
						"title" => "+ " . trans('ehdafront::general.continue') ,
					] ,
				])
			</div>
		@endforeach
	</div>
</div>