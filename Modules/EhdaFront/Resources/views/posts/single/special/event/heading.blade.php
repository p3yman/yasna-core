@php
    $textsArray = [
        "day" => trans('ehdafront::general.event.timer.day') ,
        "hours" => trans('ehdafront::general.event.timer.hours')  ,
        "minutes" => trans('ehdafront::general.event.timer.minutes')  ,
        "seconds" => trans('ehdafront::general.event.timer.seconds') ,
        "expired" => trans('ehdafront::general.event.timer.phrase.expired')  ,
        "running" => trans('ehdafront::general.event.timer.phrase.running')  ,
        "upcoming" => trans('ehdafront::general.event.timer.phrase.upcoming')  ,
        "status" => [
            "expired" => trans('ehdafront::general.event.type.expired') ,
            "running" => trans('ehdafront::general.event.type.running') ,
            "upcoming" => trans('ehdafront::general.event.type.upcoming') ,
        ] ,
    ];
    $transJson = json_encode($textsArray);

@endphp
<div class="event-header top-event-full">
    <div class="image-wrapper @if($blur ?? false) blur @endif">
        <img src="{{ $src }}" alt="{{ $alt or "" }}">
    </div>
    <div class="content">
        <h1 class="title">{{ $title }}</h1>
        @isset($js_input)
            <div class="status">
                <div class="timer" id="timer">
                    <Timer starttime="{{ $js_input['start'] }}" endtime="{{ $js_input['end'] }}" trans="{{ $transJson }}"></Timer>
                </div>
            </div>
        @endisset
        @if(isset($attendees) and $attendees > 0)
            <div class="attendees">
                <div class="format">
                    {{ ($attendees === "1")? trans('ehdafront::general.event.attendees.singular') : trans('ehdafront::general.event.attendees.plural') }}
                </div>
                <i class="fa fa-id-card"></i>
                {{ ad($attendees) }}
            </div>
        @endif
        
    </div>
</div>

@section('endOfBody')
    
    {!! Html::script (Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
    {!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
    
    {{--<script>--}}
        {{--InitTimer("{!! json_encode($textsArray) !!}")--}}
    {{--</script>--}}
@endsection