@section('additionalContent')
	@if (($files = $post->files) and is_array($files) and count($files))
		<div class="row mb40">
			@foreach($post->files as $file)
				@php $doc = doc($file['src'])->resolve() @endphp
				@if ($url = $doc->getUrl())
					<div class="audio-detail">
						<div class="audio-info">
							<h1>{{ $file['label'] ?: $doc->getTitle() }}</h1>
							<div class="caption">
							</div>
							<span class="download">
								<span class="download-size">
									{{ $doc->getSizeText() }}
								</span>
								<a href="{{ $doc->getDownloadUrl() }}" class="btn btn-link text-green">
									<i class="fa fa-download"></i>
									{{ trans('ehdafront::general.download') }}
								</a>
							</span>
						</div>
					</div>
				@endif
			@endforeach
		</div>
	@endif
@append
