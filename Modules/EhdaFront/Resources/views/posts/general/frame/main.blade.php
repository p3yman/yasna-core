@extends('ehdafront::layouts.frame')

@if(!isset($positionInfo))
	@php $positionInfo = [] @endphp
@endif

@section('content')
	<div class="container-fluid">
		@isset($post)
			@php $posttype = ($posttype ?? $post->posttype) @endphp
			@if ($post->type == 'statics')
				<div class="row">
					@include('ehdafront::layouts.widgets.title-with-image', [
						"title_link" => request()->url(),
						"title" => $post->title,
						"bg_img" => doc($post->featured_image)->getUrl(),
						"blur" => false,
					])
				</div>
			@else
				@php Template::appendToPageTitle($posttype->titleIn(getLocale())) @endphp
			@endif

			@php Template::appendToPageTitle($post->title) @endphp

			@if ($postImage = doc($post->featured_image)->getUrl())
				@php Template::mergeWithOpenGraphs(['image' => $postImage]) @endphp
			@endif

		@endisset

		{{--@include('ehdafront::layouts.position_info', $positionInfo + [--}}
		{{--'groupColor' => 'green',--}}
		{{--'categoryColor' => 'green',--}}
		{{--])--}}
		<div class="container content">
			@if ($innerHTML)
				{!! $innerHTML !!}
			@else
				@include('ehdafront::layouts.widgets.error-box',[
					"message" => trans('ehdafront::general.no_result_found') ,
				])
			@endif
		</div>
	</div>
@endsection

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')