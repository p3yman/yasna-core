@extends('ehdafront::layouts.frame')

@php $subTitle = [] @endphp

@isset($postType)
	@php Template::appendToPageTitle($posttypeTitle = $postType->titleIn(getLocale())) @endphp

	@isset($folder)
		@if ($folder->exists)
			@php Template::appendToPageTitle($folderTitle = $folder->titleIn(getLocale())) @endphp
			@php $subTitle[] = $folderTitle @endphp
		@endif
	@endisset

	@isset($category)
		@if ($category->exists)
			@php Template::appendToPageTitle($categoryTitle = $category->titleIn(getLocale())) @endphp
			@php $subTitle[] = $categoryTitle @endphp
		@endif
	@endisset

	@php
		$header_background_image = ($cover_hashid = $postType->spreadMeta()->list_cover_image)
			? fileManager()->file($cover_hashid)->getUrl()
			: null
	@endphp
	@php $blur_header = $postType->spreadMeta()->list_cover_blur @endphp
@endisset

@php $headerTitle = $posttypeTitle ?? $headerTitle ?? '' @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop
@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title_link" => action(
			'\\' . ($route = request()->route())->getActionName(),
			array_except($route->parameters(), ['category' , 'folder'])
		),
		"title" => $headerTitle,
		"sub_title" => implode(' / ', $subTitle) ,
		"subtitle_link" => request()->url(),
		"bg_img" => $header_background_image ?? null,
		"blur" => $blur_header ?? false,
	])
	@include('ehdafront::layouts.widgets.slide-row-box',[
		"boxes" => $specialNews ?? collect([]) ,
	])

	<div class="container">
		<div class="row">
			{{--@TODO: "col-centered" class should be removed if special news column is exist. --}}
			<div class="col-md-9 col-centered {{--col-md-pull-3--}}">
				<div class="news-list">
					@include('ehdafront::layouts.underlined_heading',[
						"color" => "green" ,
						"text" => $headerTitle ,
					])
					<div class="search-news">
						<div class="row">
							<div class="col-md-12">
								@include(
									'ehdafront::layouts.widgets.search-form' ,
									isset($searchFieldName) ? ['name' => $searchFieldName] : []
								)
							</div>
						</div>
					</div>
					<div class="list">
						@if ($innerHTML)
							{!! $innerHTML !!}
						@else
							@include('ehdafront::layouts.widgets.error-box',[
								"message" => trans('ehdafront::general.no_result_found') ,
							])
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-3 {{--col-md-push-9--}}">
				<div class="top-news">
					{{--@include('ehdafront::layouts.underlined_heading',[--}}
					{{--"color" => "green" ,--}}
					{{--"text" => "جدید‌ترین اخبار" ,--}}
					{{--])--}}
					{{--@include('ehdafront::news.list.special-news')--}}
				</div>
			</div>
		</div>
	</div>
@endsection