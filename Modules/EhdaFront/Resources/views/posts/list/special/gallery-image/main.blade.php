@php
	$postType->spreadMeta();
	if(!isset($filterUrl)) {
		$filterRouteParameters = ['type' => GalleryTools::posttypeToType($postType->slug)];
		if ($folder and $folder->exists){
			$filterRouteParameters['folder'] = $folder->slug;
		}
		$filterUrl = EhdaFrontRouteTools::actionLocale('PostController@galleryFilter', $filterRouteParameters);
	}
@endphp
@component('ehdafront::layouts.filter-grid-layout', compact('isBasePage', 'filterUrl'))
	@slot('filters')
		<!-- Search Box -->
		<div class="filter_search search-form filter-text" data-identifier="search">
			<label for="search">
				{{ trans('ehdafront::general.search') }}
			</label>
			<input class="form-control" id="{{ $id or "search" }}" type="text"
				   placeholder="{{ trans('ehdafront::general.search') }}" name="search"
				   value="{{ $searchValue or '' }}">
			<button class="search-button" type="button">
				<i class="fa fa-search"></i>
			</button>
		</div>
		<!-- !END Search Box -->
		<!-- Checkboxes -->
		<div class="filter_checkboxes">
			@if ($allFolders ?? false)
				@foreach ($postType->folders as $folder)
					@if (($categories = $folder->children)->count())
						<div class="checkbox-group filter-checkbox" data-identifier="category">
							<div class="title">
								<h1>{{ $folder->titleIn(getLocale()) }}</h1>
							</div>
							<div class="body">
								@foreach($categories as $category)
									@include('ehdafront::forms.new-form.checkbox',[
										"title" => $category->titleIn(getLocale()) ,
										"value" => $category->slug ,
										"id" => "category-$category->slug",
									])
								@endforeach
							</div>
						</div>
					@endif
				@endforeach
			@else
				@if ($folder->exists)
					@php $categories = $folder->children @endphp
				@else
					@php $categories = $postType->categories()->whereIsFolder(false)->whereParentId(0)->get() @endphp
				@endif
				@if ($categories->count())
					<div class="checkbox-group filter-checkbox" data-identifier="category">
						<div class="title">
							<h1>{{ trans('ehdafront::posts.features.category') }}</h1>
						</div>
						<div class="body">
							@foreach($categories as $category)
								@include('ehdafront::forms.new-form.checkbox',[
									"title" => $category->titleIn(getLocale()) ,
									"value" => $category->slug ,
									"id" => "category-$category->slug",
								])
							@endforeach
						</div>
					</div>
				@endif
			@endif
		</div>
		<!-- !END Checkboxes -->
	@endslot

	@slot('header_filters')
		<div class="col-xs-6">
			<div class="form-group pull-start">
				<div class="col-sm-12">
					<select class="ajax-sort form-control">
						<option data-identifier="publishing" data-value="desc" selected>
							{{ trans('ehdafront::general.publishing_time') }}
							>
							{{ trans('ehdafront::general.descending') }}
						</option>
						<option data-identifier="publishing" data-value="asc">
							{{ trans('ehdafront::general.publishing_time') }}
							>
							{{ trans('ehdafront::general.ascending') }}
						</option>
					</select>
					<span class="help-block {{$hint_class or ''}}">
					</span>

				</div>
			</div>
		</div>
	@endslot

	@if (!$isBasePage)
		@slot('content')
			@foreach($posts as $post)
				@php
					$postImage = doc($post->featured_image)->resolve();
					$postUrl = $post->direct_url;
					$cardData = [
						'title' => $post->title,
						'image' => $postImage->getUrl(),
						'alt' => $postImage->getTitle(),
					];
				@endphp
				@switch($postType->list_card_type)
					@case('uncertain-ratio')
					<div class="col-xxs col-xs-6 col-md-4 col-lg-3">
						@include('ehdafront::layouts.widgets.image-box',array_merge($cardData, [
							"caption" => $post->abstract ,
							"link" => $postUrl,
						]))
					</div>
					@break

					@case('horizontal')
					<div class="col-sm-6 col-xs-6 col-xxs">
						@include('ehdafront::layouts.widgets.row-box', array_merge($cardData, [
						 "caption" => $post->abstract ,
						 "author" => (isset($newsItem['author']))? $newsItem['author'] : false ,
						 "date" => ad(echoDate($post->published_at, 'j F')) ,
						 "src" => $cardData['image'] ,
						"link" => $postUrl,
					 ]))
					</div>
					@break

					@default
					<div class="col-xxs col-xs-6 col-md-4 col-lg-3">
						@include('ehdafront::layouts.widgets.box', array_merge($cardData, [
							"link" => [
								"link" => $postUrl,
								"title" => "+ " . trans('ehdafront::general.continue') ,
							] ,
						]))
					</div>
					@break
				@endswitch
				@if ($postType->list_card_type == 'uncertain-ratio')
				@else
				@endif
			@endforeach
			<div class="col-xxs col-xs-12 text-center">
				{!! $posts->render() !!}
			</div>
		@endslot
	@endif
@endcomponent