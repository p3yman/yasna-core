@foreach($posts as $post)
	@php
		$post->spreadMeta();
		$eventInfoParts = [];
		if ($post->event_starts_at) {
			$startTimeStamp = ad(echoDate($post->event_starts_at, 'j F'));
			if (($startTime = echoDate($post->event_starts_at, 'H:i')) and ($startTime != '00:00')) {
				$startTimeStamp .= ' - ' . ad($startTime);
			}
			$eventInfoParts[] = $startTimeStamp;
		}
		if ($post->event_ends_at) {
			$endTimeStamp = ad(echoDate($post->event_ends_at, 'j F'));
			if (($endTime = echoDate($post->event_ends_at, 'H:i')) and ($endTime != '00:00')) {
				$endTimeStamp .= ' - ' . ad($endTime);
			}
			$eventInfoParts[] = $endTimeStamp;
		}
		if ($post->location) {
			$eventInfoParts[] = $post->location;
		}
	@endphp

	@include('ehdafront::layouts.widgets.row-box',[
		"link" => $post->direct_url ,
		"title" => $post->title ,
		"src" => doc($post->featured_image)->getUrl() ,
		"date" => implode(' | ', $eventInfoParts) ,
		"caption" => $post->event_status_text ,
	])
@endforeach

<div class="row">
	<div class="col-xs-12 text-center">
		{!! $posts->render() !!}
	</div>
</div>
