@php
	$class = "";
	if(isset($active) and $active){
		$class = "active";
	}
@endphp
<div class="category-box {{ $class }}" data-slug="{{ $slug }}">
	<div class="panel widget">
		<div class="row row-table">
			<div class="icon-container col-xs-4 text-center">
				<em class="fa fa-{{ $icon }}"></em>
			</div>
			<div class="title-container col-xs-8">
				<div class="h2 mt0 mb0 ">
					{{ $title }}
				</div>
			</div>
		</div>
	</div>
</div>