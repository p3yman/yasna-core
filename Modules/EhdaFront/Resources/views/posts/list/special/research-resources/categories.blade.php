@php
	$icons_map = [
		'video' => 'film',
		'slide' => 'file-powerpoint-o',
		'text' => 'file-text-o',
	];
@endphp

<div style="margin-top: -20px;" id="category_boxes">
	@foreach ($content_categories as $category)
		@php
			$slug = $category->slug;
			$icon = ($icons_map[$slug] ?? false);
		@endphp


		<div class="col-sm-4">
			@include($viewFolder . '.category-box',[
				"icon" => $icon ,
				"title" => $category->titleIn(getLocale()) ,
				"active" => false ,
				"slug" => $slug,
			])
		</div>
	@endforeach
</div>

@section('endOfBody')
	<script>
        $(document).ready(function () {
            var boxes             = $('#category_boxes .category-box');
            let hidden_checkboxes = $('.checkbox-group#content-categories#content-categories')
                .find('input[type=checkbox]');

            boxes.on('click', function () {
                let that              = $(this);
                let slug              = that.data('slug');
                let party_checkbox    = $('input[type=checkbox]#category-' + slug);
                let sister_checkboxes = hidden_checkboxes.not('#category-' + slug);

                boxes.removeClass('active');
                that.addClass('active');

                sister_checkboxes.prop('checked', false);
                party_checkbox.prop('checked', true).change();

                modifyUrl();
            });


            hidden_checkboxes.change(function () {
                let selected_content_categories = hidden_checkboxes.filter(':checked');
                let current_content_category    = null;

                if (selected_content_categories.length) {
                    current_content_category = selected_content_categories.first();
                } else {
                    current_content_category = hidden_checkboxes.first();
                }

                selected_content_categories.not(current_content_category).prop('checked', false);

                let current_content_category_slug = current_content_category.attr('id').substr('category-'.length);
                let current_box                   = boxes.filter('[data-slug="' + current_content_category_slug + '"]');

                boxes.not(current_box).removeClass('active');
                current_box.addClass('active');
            });

            boxes.filter('.active').trigger('click');
        });
	</script>
@append