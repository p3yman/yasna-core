<div class="contracts-list mt40 mb40">
	@foreach($posts as $post)
		@php
			$image = fileManager()
				->file($post->featured_image)
				->posttype($postType)
				->config('featured_image')
				->version('thumb')
				->resolve()
		@endphp

		@include('ehdafront::layouts.widgets.row-box',[
			"title" => $post->title ,
			"date" => ad(echoDate($post->published_at, 'j F y')) ,
			"caption" => $post->abstract ,
			"link" => $post->direct_url ,
			"src" => $image->getUrl() ,
			"alt" => $image->getTitle() ,
		])
	@endforeach
</div>

<div class="col-xs-12 mb40 text-center">
	{!! $posts->render() !!}
</div>