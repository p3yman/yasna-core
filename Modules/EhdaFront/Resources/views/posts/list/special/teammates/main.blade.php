@php
	$employees = array_values($posts->filter(function ($post) {
            $post->spreadMeta();
            return $post->short_view;
        })->all());

		$leaders = array_values($posts->diff($employees)->all());
@endphp

@include($viewFolder . '.leaders')
@include($viewFolder . '.employees')