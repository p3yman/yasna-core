<div class="row">
    @foreach($posts as $post)
        @php $featuredImage = doc($post->featured_image)->resolve() @endphp
        <div class="col-sm-12 col-xs-6 col-xxs">
            @include('ehdafront::layouts.widgets.row-box',[
                 "title" => $post->title ,
                 "caption" => $post->abstract ,
                 "date" => trans("ehdafront::posts.form.publish") . ": " . ad(echoDate($post->published_at, 'j F Y')) ,
                 "src" => $featuredImage->getUrl() ,
                 "alt" => $featuredImage->getTitle() ,
                 "link" => $post->direct_url ?: '#' ,
            ])
        </div>
    @endforeach
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        {!! $posts->render() !!}
    </div>
</div>
