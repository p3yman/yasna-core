{!!
    widget('FormOpen')
		->class('donation-from js')
		->target(EhdaFrontRouteTools::actionLocale('DonationController@pay'))
!!}

{!!
    widget('input')
    	->label(trans('ehdafront::validation.attributes.first_and_last_name'))
    	->name('name')
    	->value(user()->exists ? user()->full_name : '')
!!}
{!!
    widget('input')
    	->name('mobile')
    	->label(trans('ehdafront::validation.attributes.mobile'))
    	->value(user()->exists ? user()->mobile : '')
!!}
{!!
    widget('input')
    	->name('code_melli')
    	->label(trans('ehdafront::validation.attributes.code_melli'))
    	->value(user()->exists ? user()->code_melli : '')
!!}

@if ($wares->count() > 1)
	<div id="widget-input-29267387-container" class="">
		<label for="_widget-input-29267387" class="control-label text-gray "
			   style="margin-top:10px;">{{ trans('ehdafront::general.donation.type') ."..." }}</label>

		<select class="form-control" id="widget-input-29267387" name="ware_id">
			@foreach($wares as $ware)
				<option value="{{ $ware->hashid }}">{{ $ware->titleIn(getLocale()) }}</option>
			@endforeach
		</select>

	</div>
@else
	{!! widget('hidden')->name('ware_id')->value($wares->first()->hashid) !!}
@endif

{!!
    widget('input')
    	->name('amount')
    	->label(
    		trans('ehdafront::validation.attributes.amount')
    		. ' ('
    		. trans('ehdafront::general.rials')
    		. ')')
!!}

<div class="form-row mt15">
	{!!
		widget('feed')
	!!}
</div>


{!!
    widget('button')
		->type('submit')
		->label(trans('ehdafront::general.payment.redirect_to_gateway'))
		->class('btn-green mt25')
!!}

{!!
    widget('FormClose')

!!}



@section('endOfBody')
	{!! Html::script (Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:libs/jquery-ui/jquery-ui.min.js')) !!}
@append