<div class="abstract">
	<div class="container">
		@if ($title)
			<h1>{{ $title }}</h1>
		@endif
		@if ($abstract)
			<div class="content">
				{!! $abstract !!}
			</div>
		@endif
		@include('ehdafront::donation.call-to-action',[
			"onclick" => "$('#donationSection').scrollToView(-100,1000)" ,
		])
	</div>
</div>