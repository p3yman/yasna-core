<div class="account-info">
	{{--<h2>{{ trans('ehdafront::general.financial_support.account_info') }}</h2>--}}
	<div class="account-data">
		@foreach($offlineAccountGroups as $group => $groupInfo)
			@php
				$accounts = $groupInfo['accounts'];
				$method = $groupInfo['method'];
			@endphp

			<div class="mt10 mb20" id="{{ $group }}">
				<div class="title">
					{{ trans("ehdafront::general.payment.types.$group") . ":" }}
				</div>
				@foreach($accounts as $account)
					@php
						$account->spreadMeta();
						$accountNo = $account->account_no;
					@endphp
					<div class="subtitle">
						{!! $account->bank_name !!}
					</div>
					<div class="data">
						{{ ad($method ? EhdaFrontPaymentTools::$method($accountNo) : $accountNo ) }}
						<span class="copy-icon js-copy"
							  data-toggle="tooltip"
							  title="{{ trans('ehdafront::general.financial_support.donation.accounts.copy') }}"
							  data-clipboard-text="{{ $accountNo }}"
						>
							<i class="fa fa-files-o"></i>
						</span>
					</div>
				@endforeach
			</div>
		@endforeach
		<div class="owner">
			{{ trans('ehdafront::general.payment.account.owner.name') }}:
			{{ array_values($offlineAccountGroups)[0]['accounts']->first()->spreadMeta()->owner }}
		</div>
	</div>
</div>
