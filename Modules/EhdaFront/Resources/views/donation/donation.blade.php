<div class="donation" id="donationSection">
	<div class="container">

		@if ($title2)
			<h1>{{ $title2 }}</h1>
		@endif

		@if ($alert)
			<div class="col-xs-12">
				<div class="alert alert-{{ $alert['messageType'] }}">{!! $alert['message'] !!}</div>
				@section('endOfBody')
					<script>
						$(document).ready(function () {
                            $('.call-to-action').find('button').first().trigger('click');
                        });
					</script>
				@append
			</div>
		@endif

		<div class="row">
			@if (count($offlineAccountGroups))
				<div class="col-md-6">
					@include('ehdafront::layouts.underlined_heading',[
						"text" => trans('ehdafront::general.financial_support.donation.type.offline') ,
						"color" => "green" ,
					])
					@include('ehdafront::donation.bank-accounts')
				</div>

			@endif
			@if ($onlineAccount and $wares->count())
				<div class="col-md-6">
					@include('ehdafront::layouts.underlined_heading',[
						"text" => trans('ehdafront::general.financial_support.donation.type.online') ,
						"color" => "green" ,
					])
					<div class="form-wrapper">
						@include('ehdafront::donation.form')
					</div>
				</div>
			@endif
		</div>

	</div>
</div>