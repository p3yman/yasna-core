<div class="call-to-action">
	<button class="btn btn-lg btn-green" onclick="{{ $onclick or "" }}">
		{{ trans('ehdafront::general.financial_support.call_to_action') }}
	</button>
</div>