@extends('ehdafront::layouts.frame')
@php $staticPost->spreadMeta() @endphp

@php Template::appendToPageTitle($title = $staticPost->title) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	<div class="financial-support">
		@php
			$coverData = [
				'title' => $title,
				'title_link' => request()->url(),
				'bg_img' => doc($staticPost->featured_image)->getUrl(),
			]
		@endphp
		@if ($title2 = $staticPost->title2)
		    @php $coverData['sub_title'] = $title2 @endphp
		@endif
		@include('ehdafront::layouts.widgets.title-with-image', $coverData)
		@include('ehdafront::donation.abstract',[
			"title" => $title3 = $staticPost->title3 ,
			"abstract" => $staticPost->abstract ,
		])
		@include('ehdafront::donation.donation')
		@include('ehdafront::donation.details',[
			"description" => $staticPost->text ,
		])
	</div>
@endsection

@section('endOfBody')
	{!! Html::script(Module::asset('ehdafront:libs/clipboard/clipboard.min.js')) !!}

	<script>
        $(document).ready(function () {
            var clipboard = new ClipboardJS('.js-copy');
            clipboard.on('success', function (e) {
                $('body > textarea[readonly]').css({
                    opacity: 0,
                    width  : 0,
                    height : 0
                });
                $('body > .ui-tooltip ').css({
                    boxShadow: "none"
                });
                e.clearSelection();
                let target = $(e.trigger);
                target.addClass('text-success');
                setTimeout(function () {
                    target.removeClass('text-success');
                }, 1200);
            });

        })
	</script>
@append


