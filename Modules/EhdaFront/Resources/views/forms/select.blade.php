<?php
if (!isset($extra))
    $extra = '';
if (!isset($name))
    $name = '';

if (!isset($class)) {
    $class = '';
}

if (isset($class) && str_contains($class, 'form-required')) {
    $required = true;
}

if (isset($value) and is_object($value))
    $value = $value->$name;

if (isset($disabled) and $disabled) {
    $required = false;
    $extra    .= ' disabled ';
}
?>
@if(!isset($condition) or $condition)

	<div class="form-group pull-start {{ $div_class or '' }}">

		@if(!isset($label))
			@php
				$label = Lang::has("ehdafront::validation.attributes.$name") ? trans("ehdafront::validation.attributes.$name") : $name;
			@endphp
		@elseif($label)
			@php
				$label = is_string($label) ? $label : trans("ehdafront::validation.attributes.$name");
			@endphp
		@endif

		@if($label)
			<label
					for="{{$name}}"
					class="col-sm-12 control-label {{$label_class or ''}}"
			>
				{{$label or trans("ehdafront::validation.attributes.$name")}}
				@if(isset($required) and $required)
					<span class="fa fa-star required-sign " title="{{trans('ehdafront::forms.logic.required')}}"></span>
				@endif
			</label>
		@endif

		<div class="col-sm-12">
			@include('ehdafront::forms.select_self')
			<span class="help-block {{$hint_class or ''}}">
				{{ $hint or '' }}
			</span>

		</div>
	</div>
@endif