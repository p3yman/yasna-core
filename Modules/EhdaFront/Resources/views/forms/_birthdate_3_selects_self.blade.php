@if(!isset($condition) or $condition)
	@php
		if (!isset($extra)) {
			$extra = '';
		}

		if (!isset($name)) {
			$name = 'birth_date';
		}

		if (isset($value) and is_object($value))
			$value = $value->$name;

		if(!isset($value)){
			$value = '';
		}

		if($value) {
			list($yearValue, $monthValue, $dayValue) = explode('/', $value);
		} else {
			$yearValue = '';
			$monthValue = '';
			$dayValue = '';
		}

		if (isset($class)) {
			if (str_contains($class, 'form-required')) {
				$required = true;
			}
		} else {
			$class = '';
		}

		if (isset($disabled) and $disabled) {
			$required = false;
			$extra .= ' disabled ';
		}

		$thisDatePickerId = 'date-' . str_random(5) . time();
	@endphp

	@php $dayOptions = [['id' => '', 'title' => trans('ehdafront::general.day')]] @endphp
	@php $monthOptions = [['id' => '', 'title' => trans('ehdafront::general.month')]] @endphp
	@php $yearOptions = [['id' => '', 'title' => trans('ehdafront::general.year')]] @endphp

	@php
		$currentYear = echoDate(\Carbon\Carbon::now(), 'Y');
	@endphp

	@foreach(range(1, 31) as $n)
		@php $dayOptions[] = ['id' => $n, 'title' => ad($n)] @endphp
	@endforeach
	@foreach(range(1, 12) as $n)
		@php $monthOptions[] = ['id' => $n, 'title' => ad($n)] @endphp
	@endforeach
	@foreach(range($currentYear - 99, $currentYear) as $n)
		@php $yearOptions[] = ['id' => $n, 'title' => ad($n)] @endphp
	@endforeach
	<div class="datepicker-box" id="{{ $thisDatePickerId }}">
		<div class="col-md-4 col-xs-12 mb15 ">
			@include('ehdafront::forms.select_self',[
				'options' => $dayOptions,
				'name' => '',
				'id' => $thisDatePickerId . '-day',
				'value' => $dayValue,
			])
		</div>
		<div class="col-md-4 col-xs-12 mb15">
			@include('ehdafront::forms.select_self',[
				'options' => $monthOptions,
				'name' => '',
				'id' => $thisDatePickerId . '-month',
				'value' => $monthValue,
			])
		</div>
		<div class="col-md-4 col-xs-12 mb15">
			@include('ehdafront::forms.select_self',[
				'options' => $yearOptions,
				'name' => '',
				'id' => $thisDatePickerId . '-year',
				'value' => $yearValue,
			])
		</div>
	</div>
	@include('ehdafront::forms.hidden', ['id' => $thisDatePickerId . '-val'])
@endif

@section('endOfBody')
	@if(!isset($condition) or $condition)
		<script>
            $(document).ready(function () {
                $('#{{ $thisDatePickerId }}').find('select').change(function () {
                    let year  = $('#{{ $thisDatePickerId . '-year' }}').val();
                    let month = $('#{{ $thisDatePickerId . '-month' }}').val();
                    let day   = $('#{{ $thisDatePickerId . '-day' }}').val();
                    if (year && month && day) {
                        let val = year + '/' + month + '/' + day;
                        $('#{{ $thisDatePickerId . '-val' }}').val(val);
                    }
                }).last().change();
            });

		</script>
	@endif
@append