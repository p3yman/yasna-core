<?php
if (!isset($class)) {
    $class = '';
}

if (!isset($blank_value)) {
    $blank_value = '0';
}

if (!($options = Template::statesComboOptions())) {
    Template::setStatesComboOptions($options = EhdaTools::getStatesComboHashid());
}
?>

@include('ehdafront::forms.select-picker' , [
    'blank_value' => $blank_value,
    'options' => $options,
    'search' => true,
    'class' => $class,
])