@if(!isset($condition) or $condition)
	@include('ehdafront::formsgroup-start')

	@include('ehdafront::formscheck' , [
		'label' => $self_label
	])

	@include('ehdafront::formsgroup-end')

@endif