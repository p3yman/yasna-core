@if(isset($currentValues))
    @php $currentActivities = explode(',', $currentValues->get('activities')) @endphp
@else
    @php $currentActivities = [] @endphp
@endif

@if($activitiesOptions)
    @foreach($activitiesOptions as $act)
        <div class="checkbox">
            <label>
                <input name="activity[]" class="volunteer_activity" type="checkbox" value="{{ $act->slug }}"
                       style="display: block;" @if(in_array($act->slug, $currentActivities)) checked="checked" @endif>
                {{ $act->title }}
            </label>
        </div>
    @endforeach
@endif