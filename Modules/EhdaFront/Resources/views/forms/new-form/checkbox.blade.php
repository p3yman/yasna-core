
<label class="control control--checkbox {{ $class or "" }}">
    {{ $title }}
    <input type="checkbox"
           id="{{ $id or "" }}"
           name="{{ $name or "" }}"
           value="{{ $value }}"
           @if(isset($disabled) and $disabled) disabled="disabled" @endif
           @if(isset($is_checked) and $is_checked) checked="checked" @endif/>
    <div class="control__indicator"></div>
</label>