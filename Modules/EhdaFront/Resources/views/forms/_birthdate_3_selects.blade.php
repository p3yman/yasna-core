@php
    if (!isset($name))
        $name = 'birth_date';

    if (!isset($in_form))
        $in_form = true;

@endphp
@if(!isset($condition) or $condition)
    <div class="form-group {{ (isset($container['class']) ? $container['class'] : '') }}"
    {{ isset($container['id']) ? "id=$container[id]" : '' }}
    @if(isset($container['other']))
        @foreach($container['other'] as $attrName => $attrValue)
            {{ $attrName }}="{{ $attrValue }}"
        @endforeach
    @endif
    >
        @if(!isset($label))
            @php $label = Lang::has("ehdafront::validation.attributes.$name") ? trans("ehdafront::validation.attributes.$name") : $name @endphp
        @elseif($label)
            @php $label = is_string($label) ? $label : trans("ehdafront::validation.attributes.$name") @endphp
        @endif
        
        @if($label)
            <label
                    for="{{$name}}"
                    class="col-sm-12 control-label {{$label_class or ''}}"
            >
                {{ $label }}
                @if(isset($required) and $required)
                    @include('ehdafront::forms.require-sign')
                @endif
            </label>
        
        @endif
        <div class="col-sm-12">
            <div class="row">
                @include('ehdafront::forms._birthdate_3_selects_self')
            </div>
            <span class="help-block persian {{$hint_class or ''}}" style="{{$hint_style or ''}}">
                    {{ $hint or '' }}
                </span>
            
            @if ($errors->has($name))
                <span class="help-block error">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
            @endif
        </div>
    </div>

@endif