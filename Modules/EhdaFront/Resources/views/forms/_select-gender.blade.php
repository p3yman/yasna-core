<?php
if (!isset($name)) {
    $name = 'gender';
}
?>

@include('ehdafront::forms.buttonset', [
	'options' => EhdaTools::activeGendersTranses('ehdaforms.gender'),
])
<div class="col-xs-12">
    <span class="help-block persian {{$hint_class or ''}}" style="{{$hint_style or ''}}">
        {{ $hint or '' }}
    </span>
</div>