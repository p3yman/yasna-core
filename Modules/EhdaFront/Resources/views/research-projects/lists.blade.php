<div class="container mt30">
	@include('ehdafront::layouts.underlined_heading',[
					"text" => "طرح‌‌های تحقیقاتی" ,
					"color" => "green" ,
				])
	<div class="list">
		@include('ehdafront::layouts.widgets.search-form')
		@foreach($future_projects as $project)
			@include('ehdafront::layouts.widgets.row-box',[
				"link" => $project['link'] ,
				"title" => $project['title'] ,
				"src" => $project['src'] ,
				"alt" => $project['alt'] ,
				"caption" => $project['caption'] ,
			])
		@endforeach
	</div>
</div>