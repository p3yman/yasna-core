<div class="container">
	<div class="description">
		@isset($title)
			@include('ehdafront::layouts.underlined_heading',[
				"text" => $title ,
				"color" => "green" ,
			])
		@endisset
		{!! $description !!}
	</div>
</div>