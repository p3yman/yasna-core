@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($post->spreadMeta()->title) @endphp

@section('page-title')
    {{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
    @include('ehdafront::layouts.widgets.title-with-image', [
            "title" => $post->category_title ,
            "bg_img" => doc($post->featured_image)->getUrl() ,
        ])
    <div class="container-fluid pt40">
        @include('ehdafront::card.card_info.card_info_content')
    </div>
@endsection