@if ($user->is_admin()) {{--This user is a volunteer--}}
{{ trans('ehdafront::general.messages.you_are_volunteer') }}
<br>
{{ trans('ehdafront::general.messages.login_to_complete_volunteer_registration.before_link') }}
@include('ehdafront::card.registration_response.login_link', [
    'text' => trans('ehdafront::general.messages.login_to_complete_volunteer_registration.link_text'),
])
{{ trans('ehdafront::general.messages.login_to_complete_volunteer_registration.after_link') }}

@elseif ($user->withDisabled()->is_admin()) {{--This user is a blocked volunteer--}}
{{ trans('ehdafront::general.messages.unable_to_register_card') }}

@elseif ($user->is_a('card-holder')) {{--This user has card--}}
{{ trans('ehdafront::general.messages.card_exists_with_this_code_melli') }}
<br>
{{ trans('ehdafront::general.messages.please_login.before_link') }}
@include('ehdafront::card.registration_response.login_link', [
    'text' => trans('ehdafront::general.messages.please_login.link_text'),
])
{{ trans('ehdafront::general.messages.please_login.after_link') }}

@endif