@include('ehdafront::layouts.widgets.link', [
    'anchor' => [
        'text' => $text,
        'attributes' => [
            'href'  => route('login'),
            'class' => 'link-green',
        ],
    ],
])