<p style="text-align: justify">
    {!! trans('ehdafront::general.organ_donation_card_section.register_success_message.email.line1', [
                'name' => $user->full_name,
                'membershipNumber' => $user->card_no,
            ]
    ) !!}
    <br/>
    {!! trans('ehdafront::general.organ_donation_card_section.register_success_message.email.line2', [
                'membershipNumber' => $user->card_no,
            ]
    ) !!}
</p>
<a href="{{ config('ehdamanage.public-domain') }}" target="_blank">
    {{ setting()->ask('site_title')->gain() }}
</a>
<div style="text-align: center;">
    <img src="{{ $user->cards('social') }}" style="width: 80%;padding: 5px;border: 1px solid #01aef0;margin-top: 20px;">
</div>