@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle(trans('ehdafront::general.states')) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image', [
		"title_link" => "#",
		"title" => $positionInfo['category'],
		"sub_title" => $positionInfo['title'],
	])
	<div class="container-fluid pt40 pb40">

		<div class="row">
			<div class="col-sm-6">
				@include('ehdafront::map.iran.state-info')
			</div>
			<div class="col-sm-6">
				@include('ehdafront::map.iran.map')
			</div>
		</div>
	</div>
@endsection

@include('ehdafront::map.iran.scripts')