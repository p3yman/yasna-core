<style>
    .states-list li a[disabled]::after {
        content: "({{ trans('ehdafront::general.this_state_is_disabled') }})";
    }

</style>