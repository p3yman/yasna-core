@section('endOfBody')
    {!! Html::script (Module::asset('ehdafront:js/iranmap.js')) !!}
    <script type="text/javascript">
        var states = [
                @foreach($states as $state)
            {
                title: '{{ $state->title }}',
                slug: '{{ $state->slug }}',
                active: {{ $state->alias ? 'true' : 'false' }},
                link: "{{ EhdaFrontTools::addSubDomain(url(''), $state->alias) }}",
                tel: {!! json_encode(array_map('ad', $state->info['telephone'])) !!},
                address: "{{ $state->info['address'] }}",
                email: {!! json_encode($state->info['email']) !!}

            },
            @endforeach
        ];

        var labels = {
            selectState: '{{ trans('ehdafront::general.select_your_intended_state') }}',
        };
        $(function () {
            $('.iran-map .province path').attr('disabled', 'disabled');
            $.each(states, function (index, state) {
                var target = $('.iran-map .province path[data-name="' + state.slug + '"]');
                if (target.length) {
                    if (state.active) {
                        target.removeAttr('disabled');
                        target.attr('data-src', state.link);
                        target.attr('data-tel', state.tel);
                        target.attr('data-address', state.address);
                        target.attr('data-email', state.email);
                    }
                }
            });

            $('.iran-map .province path').click(function () {
                var that = $(this);
                var province = that.attr('data-name');

                if (!that.attr('disabled')) {
                    selectProvince(province);
                }

            }).each(function () {
                var that = $(this);
                var province = that.attr('data-name');

                if (that.attr('disabled')) {
                    $('.states-list li.' + province + ' a').attr('disabled', 'disabled');
                }
            });

            $('.states-list a').click(function (e) {
                e.preventDefault();
                var province = $(this).parent().attr('class');
                var provinceName = $(this).html();
                if (!$('.iran-map path.' + province).attr('disabled')) {
                    selectProvince(province);
                    $('.states-list').find('.active').removeClass('active');
                    $(this).addClass('active');
                    $('.states-list').first().fadeOut();
                    $('.choose-state').removeClass('active');
                } else {
                    $(this).attr('disabled', 'disabled');

                }
            });

            $('.choose-state').click(function () {
                if ($(this).is('.active')) {
                    $('.states-list').stop(true, false).slideUp();
                    $(this).removeClass('active');
                    $(this).style('height', '')
                } else {
                    $('.states-list').stop(true, false).slideDown();
                    $(this).addClass('active');
                }
            });
        });

        function selectProvince(slug) {
            let infoContainer = $('.state-contact-info');
            let path = $('.iran-map .province path[data-name=' + slug + ']');
            if (path.length) {
                let provinceName = path.attr('data-persian-name');
                $('.state-name').html(provinceName).attr('data-state', slug);
            }
            path.parents('svg').find('.hover').removeClass('hover');
            path.addClass('hover');

            infoContainer.hide();

            changeProvinceLink(path.attr('data-src'));

            checkIfShow(path, infoContainer, "address",  appendAddress);

            checkIfShow(path, infoContainer, "tel",  appendTel);

            checkIfShow(path, infoContainer, "email",  appendEmail);


            $('.browser-window').slideDown();
        }

        function checkIfShow(el, container, attr, appendFunc) {
            let data_attr = 'data-' + attr;

            if(el.attr(data_attr).length){
                container.show();
                $('.state-' + attr).show();
                appendFunc(el.attr(data_attr));
            }else {
                $('.state-' + attr).hide();
            }
        }

        function clearSelectedProvince() {
            $('.states-list li a.active').removeClass('active');
            $('.state-name').html(labels.selectState).attr('data-state', '');
            $('.state-name').removeClass('disabled');
        }

        function changeProvinceLink(url) {
            let clearUrl = url.replace(/(^\w+:|^)\/\//, '');
            let linkElements = $('.state-website');
            linkElements.each(function (i) {
                $(this).find('span.state-text').html(clearUrl);
                $(this).attr('href', url);
            });
        }

        function clearProvinceLink() {
            let linkElements = $('.state-website');
            linkElements.each(function (i) {
                if (!$(this).hasClass('btn')) {
                    $(this).html('');
                }
                $(this).removeAttr('href');
            });
        }

        function appendAddress(address) {
            if(!address.length){
                return
            }

            $('.state-address .state-address-item').remove();

            $('<span class="state-address-item"></span>')
                .appendTo('.state-address')
                .text(address);
        }

        function appendTel(tel) {
            if(!tel.length){
                return
            }

            $('.state-tel .single-tel').remove();

            let phoneArray = tel.split(',');

            $(phoneArray).each(function () {
                $('<div class="single-tel"><span class="state-contact-icon"><i class="fa fa-phone"></i></span></div>')
                    .appendTo('.state-tel')
                    .append(this);
            })

        }
        
        function appendEmail(email) {
            if(!email.length){
                return
            }

            $('.state-email .single-email').remove();

            let mailArray = email.split(',');

            $(mailArray).each(function () {
                $('<div class="single-email"><span class="state-contact-icon"><i class="fa fa-envelope"></i></span></div>')
                    .appendTo('.state-email')
                    .append(this);
            })
        }
    </script>
@append