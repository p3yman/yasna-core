<div class="states-info">
	<div class="row">
		<div class="col-lg-10 col-lg-offset-2">
			<h3>
				<span class="state-name">
					{{ trans('ehdafront::general.select_your_intended_state') }}
				</span>
			</h3>
			<div class="info browser-window" style="display: none;">
				<div class="browser-top">
				<span class="browser-buttons">
					<span class="browser-btn"></span>
					<span class="browser-btn"></span>
					<span class="browser-btn"></span>
				</span>
				</div>
				<div class="browser-body">
					<div class="state-website-container">
						<h2 class="state-title">
							{{ trans('ehdafront::general.states_info.website_address') }}
						</h2>
						<a href="" class="state-btn state-website btn-primary" target="_blank">
						<span class="state-text">

						</span>
							<span class="state-icon">
							<span class="state-icon-inner">
								{{ trans('ehdafront::general.states_info.enter_website') }}
								<i class="fa fa-long-arrow-left"></i>
							</span>
						</span>
						</a>
					</div>

					<div class="state-contact-info" style="display: none">
						<hr>
						<h2 class="state-title">
							{{ trans('ehdafront::general.states_info.contact_info') }}
						</h2>

						<div class="state-address" style="display:none;">
								<span class="state-contact-icon">
									<i class="fa fa-map-marker"></i>
								</span>
						</div>


						<div class="state-tel" style="display:none;">
							<div class="single-tel">
									<span class="state-contact-icon">
										<i class="fa fa-phone"></i>
									</span>

							</div>

						</div>


						<div class="state-email" style="display:none;">
							<div class="single-email">
								<span class="state-contact-icon">
									<i class="fa fa-envelope"></i>
								</span>

							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>

</div>
