<!DOCTYPE html>
<html lang="{{ getLocale() }}">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
	<link rel="shortcut icon" href="{{ asset('assets/images/template/favicon.ico') }}">
	<title>@yield('page_title')</title>
	<!-- Bootstrap -->
{{ Html::style(Module::asset('ehdafront:libs/bootstrap/css/bootstrap.min.css')) }}
{{ Html::style(Module::asset('ehdafront:libs/bootstrap/css/bootstrap-rtl.min.css')) }}
<!-- !END Bootstrap -->

	<!-- Fonts -->
{{ Html::style(Module::asset('ehdafront:libs/font-awesome/css/font-awesome.min.css')) }}
{{ Html::style(Module::asset('ehdafront:css/fontiran.css')) }}
<!-- !END Fonts -->

	<!-- Custom Style -->
{{ Html::style(Module::asset('ehdafront:css/front-style.min.css')) }}
{{ Html::style(Module::asset('ehdafront:css/pasargad.min.css')) }}
<!-- !END Custom Style -->

	<!-- Scripts -->
{!! Html::script(Module::asset('ehdafront:js/jquery.min.js')) !!}
{!! Html::script(Module::asset('ehdafront:libs/bootstrap/js/bootstrap.min.js')) !!}
<!-- !END Scripts -->

</head>

<body>
<div class="wrapper">
	@yield('body')
</div>

{!! Html::script (Module::asset('ehdafront:/libs/jquery.form.min.js')) !!}
{!! Html::script (Module::asset('ehdafront:/js/forms.min.js')) !!}
{!! Html::script (Module::asset('ehdafront:/libs/jquery-ui/jquery-ui.min.js')) !!}
@yield('scripts')
</body>
</html>