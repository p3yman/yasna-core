@extends('ehdafront::passargad.layout.frame')

@section('body')

	<div class="full-screen">
		<div class="credit">
			{{ trans('ehdafront::passargad.credit') }}
			<a href="http://yasna.team" target="_blank">{{ trans('ehdafront::passargad.yasna_team') }}</a>
		</div>
		@if(Route::current()->getName() != 'passargad_login')
			@if(Route::current()->getName() != 'passargad_inquiry')
				<a href="{{ route('passargad_inquiry') }}" class="btn btn-success" id="inquiry" style="position: fixed;z-index: 10;bottom: 20px;right: 40px;">
					{{ trans('ehdafront::passargad.inquiry_page') }}
				</a>
			@endif
			<a href="{{ route('passargad_logout') }}" class="btn btn-danger" id="logout">
				<i class="fa fa-sign-out"></i>
			</a>
		@endif


		<div class="dual-logo-header">
			<div class="logo-container">
				<div class="pasargad">
					<img src="{{ Module::asset('ehdafront:images/Bank_Pasargad_logo.png') }}" alt="logo Pasargad Bank">
				</div>
				<div class="ehda">
					<img src="{{ Module::asset('ehdafront:images/template/header-logo-fa.png') }}" alt="logo Ehda">
				</div>
			</div>
		</div>
		@yield('content')
	</div>
@yield('endOfBody')
@endsection