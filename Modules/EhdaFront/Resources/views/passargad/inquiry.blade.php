@extends('ehdafront::passargad.layout.container')
@php
	session()->remove('passargad_code_melli');
@endphp
@section('content')
	<style>
		.passargad-alert-danger {
			margin-top: 2em;
		}

		.passargad-alert-danger li {
			list-style-type: none;
		}
	</style>


	<div class="container-inner">
		<div class="inquiry-box">
			<h4>{{ trans('ehdafront::passargad.inquiry') }}</h4>
			<form id="passargad_inquiry" action="{{ route('passargad_inquiry') }}" class="js" method="post">
				<label for="inputCodemelli" class="sr-only">
					{{ trans('ehdafront::validation.attributes.codemelli') }}
				</label>
				<input name="code_melli" type="text" id="inputCodemelli" class="form-control mb5"
					   placeholder="{{ trans('ehdafront::validation.attributes.code_melli') }}"
					   required="" autofocus="">
				{{ csrf_field() }}
				<button class="btn btn-lg btn-primary btn-block" type="submit">
					{{ trans('ehdafront::forms.button.Inquiry') }}
				</button>
				<div class="passargad-alert-danger">
					{!!
						widget('feed');
					 !!}
				</div>
			</form>

			<a class="btn btn-success hidden" id="register-button" href="{{ route('passargad_register') }}">
				{{ trans('ehdafront::passargad.register_btn') }}
			</a>
			<div id="user-found-message" class="hidden">

				<div class="panel panel-success">
					<div class="panel-heading">
						{{ trans('ehdafront::passargad.card_no') }}
					</div>
					<div class="panel-body" id="show-card-no">
					</div>
				</div>
			</div>

			@if(isset($saved_message) && isset($card_no))
			<div id="user-saved-message">
				<div class="panel panel-success">
					<div class="panel-heading">
						{{ trans('ehdafront::passargad.registered_success') }}
					</div>
					<div class="panel-body" id="show-saved-card-no">
						{{ $card_no }}
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
@endsection
@section('scripts')
	<script>
        function undefinedUser() {
            $('#user-found-message').addClass('hidden');
            $('#user-saved-message').addClass('hidden');
            $('#register-button').removeClass('hidden');
        }

        function showUser($card_no) {
            $('#user-saved-message').addClass('hidden');
            var html = '<p>' + $card_no + '</p>';
            $('#show-card-no').html(html);
            $('#user-found-message').removeClass('hidden');
            $('#register-button').addClass('hidden');
        }
	</script>
@endsection