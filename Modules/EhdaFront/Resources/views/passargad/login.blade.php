@extends('ehdafront::passargad.layout.container')

@section('content')

	<div class="container-inner">
		<div class="login-box">
			<h3>{{ trans('ehdafront::passargad.login_panel') }}</h3>
			<form id="passargad_login" action="{{ route('passargad_login') }}" method="post" class="js">
				<div class="form-group">
					<label for="inputUsername" class="sr-only">{{ trans('ehdafront::validation.attributes.username') }}</label>
					<input type="text" name="code_melli" id="inputUsername" class="form-control mb5" placeholder="{{ trans('ehdafront::validation.attributes.username') }}" required="" autofocus="">
					<label for="inputPassword" class="sr-only">{{ trans('ehdafront::validation.attributes.password') }}</label>
					<input type="password" name="password" id="inputPassword" class="form-control mb5" placeholder="{{ trans('ehdafront::validation.attributes.password') }}" required="">
					<button class="btn btn-lg btn-primary btn-block" type="submit">
						{{ trans('ehdafront::forms.button.login') }}
					</button>

					<div class="mt20">
						{!!
							widget('feed')
						 !!}
					</div>
				</div>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
@endsection
