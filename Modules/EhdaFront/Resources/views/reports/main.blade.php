@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = $posttype->titleIn(getLocale())) @endphp


@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => $title,
		"title_links" => request()->url() ,
	])

	@include('ehdafront::reports.body')
@append