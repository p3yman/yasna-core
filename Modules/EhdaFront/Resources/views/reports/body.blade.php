<div class="container-fluid">
	<div class="row">
		@foreach($posts as $post)
			@include('ehdafront::reports.item', [
				"side_title" => $post->title2 ,
				"title" => $post->title ,
				"abstract" => $post->abstract ,
				"dl_link" => fileManager()->file(array_values($post->files)[0]['src'] ?? null)->getDownloadUrl() ,
			])
		@endforeach
	</div>
</div>
