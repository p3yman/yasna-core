<div class="annual-report">
	<div class="year-container">
		<span class="year">
			{{ $side_title }}
		</span>
	</div>
	<div class="report-detail">
		<h1>{{ $title }}</h1>
		<div class="abstract">
			{{ $abstract }}
		</div>
		@if ($dl_link)
			<div class="link-wrapper">
				<a href="{{ $dl_link }}" class="btn btn-success">
					<i class="fa fa-download"></i>
					{{ trans('ehdafront::general.annual_reports.download') }}
				</a>
			</div>
		@endif
	</div>
</div>