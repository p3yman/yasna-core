@extends('ehdafront::layouts.frame')

@section('content')
	<div class="container-fluid">
		<div class="col-xs-12 pt50 pb50 pr50 pl50 text-left" id="log">
		</div>
	</div>
@stop

@php
	$ajaxCallsUrl = $ajaxCallsUrl ?? action(
		'\\' . str_replace('index', 'convert', request()->route()->getAction()['uses']),
		['offset' => '__OFFSET__']
	);
@endphp

@section('endOfBody')
	<script>
        $(document).ready(function () {
            convertStep();

            function convertStep(offset = 0) {
                $.ajax({
                    url    : '{{ $ajaxCallsUrl }}'.replace('__OFFSET__', offset),
                    success: function (rs) {
                        if (rs) {
                            $('#log').append(rs + '<br>');
                            convertStep(offset + 1);
                        } else {
                            $('#log').append('Done :)' + '<br>');
						}

                    }
                })
            }
        });
	</script>
@append