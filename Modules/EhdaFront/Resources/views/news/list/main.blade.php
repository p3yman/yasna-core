@extends('ehdafront::posts.general.frame.list.main')

@php $bg_image_config_slug = 'list_header_image_' . $postType->slug @endphp


@if (isset($folder) and $folder->exists)
	@php $bg_image_config_slug .= '_' . $folder->slug @endphp
@endif

@if ($configImage = doc($postType->spreadMeta()->$bg_image_config_slug)->getUrl())
	@php $bg_img = $configImage @endphp
@endif