@php
    $newsList = [
        [
            "title" => "اهداکنندگان زندگی میزبان انجمن اهدای عضو ایرانیان" ,
            "date" => "۲۰ مهر" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "alt" => "event" ,
        ],
        [
            "title" => "فونت های اهدای عضو" ,
            "date" => "۲۰ مهر" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img4.jpg') ,
            "alt" => "event" ,
        ],
        [
            "title" => "۳۶۴ پیوند عضو در خراسان رضوی طی یک سال" ,
            "date" => "۲۵ اردیبهشت" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img3.jpg') ,
            "alt" => "event" ,
        ],
        [
            "title" => "اهداکنندگان زندگی میزبان انجمن اهدای عضو ایرانیان" ,
            "date" => "۲۰ مهر" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "alt" => "event" ,
        ],
        [
            "title" => "فونت های اهدای عضو" ,
            "date" => "۲۰ مهر" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img4.jpg') ,
            "alt" => "event" ,
        ],
        [
            "title" => "۳۶۴ پیوند عضو در خراسان رضوی طی یک سال" ,
            "date" => "۲۵ اردیبهشت" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img3.jpg') ,
            "alt" => "event" ,
        ],
    ];
@endphp
<div class="row">
    @foreach($newsList as $newsItem)
        <div class="col-sm-12 col-xs-6 col-xxs">
            @include('ehdafront::layouts.widgets.row-box',[
             "title" => $newsItem['title'] ,
             "caption" => $newsItem['caption'] ,
             "date" => trans("ehdafront::posts.form.publish").": ".$newsItem['date'] ,
             "src" => $newsItem['src'] ,
             "alt" => $newsItem['alt'] ,
             "link" => $newsItem['link'] ,
         ])
        </div>
    @endforeach
</div>
