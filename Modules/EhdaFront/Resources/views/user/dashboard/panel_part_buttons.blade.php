<div class="col-xs-12">
    <div class="row align-horizontal-center">
        <div class="col-xs-11 align-vertical-center">
            <div class="col-xs-6">
                <a href="{{ EhdaFrontRouteTools::actionLocale('UserController@profile') }}"
                   class="btn btn-primary btn-block">
                    {{ trans('ehdafront::general.member_section.profile_edit') }}
                </a>
            </div>
            <div class="col-xs-6">
                {{-- @TODO: should make its link to go to suitable link for voluteer --}}
                <a href="{{ EhdaFrontRouteTools::actionLocale('VolunteerController@registerFinalStep') }}"
                   class="btn btn-success btn-block">
                    {{ trans('ehdafront::general.volunteer_section.plural') }}
                </a>
            </div>
        </div>
    </div>
</div>