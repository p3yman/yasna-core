@extends('ehdafront::user.frame.main')

@section('head')
    {!! Html::style(Module::asset('ehdafront:libs/bootstrap-select/bootstrap-select.min.css')) !!}
@append

@section('inner-content')
    <div class="row">
        <div class="col-md-8 col-xs-12">
            @include('ehdafront::user.profile.edit.form')
        </div>
        <div class="col-md-4 col-xs-12 pt30">
            @include('ehdafront::user.profile.edit.card')
        </div>
    </div>
@endsection

@section('endOfBody')
    @include('ehdafront::user.profile.edit.scripts')
@append