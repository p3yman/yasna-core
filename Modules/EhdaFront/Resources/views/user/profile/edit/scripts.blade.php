<script>
    var bootstrapTooltip = $.fn.tooltip.noConflict(); // return $.fn.tooltip to previously assigned value

    $(document).on({
        click: function (e) {
            return false;
        }
    }, '.dropdown-toggle');
</script>
{!! Html::script (Module::asset('ehdafront:libs/bootstrap-select/bootstrap-select.min.js')) !!}
{!! Html::script (Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
{!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
{!! Html::script (Module::asset('ehdafront:libs/jquery-ui/jquery-ui.min.js')) !!}
@include('ehdafront::layouts.datepicker_assets')
<script>
    $.fn.tooltip = bootstrapTooltip; // give $().bootstrapTooltip the Bootstrap functionality
</script>