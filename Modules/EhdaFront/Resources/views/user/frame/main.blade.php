@extends('ehdafront::layouts.frame')


@php
	Template::appendToPageTitle(trans('ehdafront::general.profile_phrases.user_profile', ['user' => user()->full_name]))
@endphp
@php
	$pageTitle = Template::implodePageTitle(Template::pageTitleSeparator())
@endphp
@section('page-title')
	{{ $pageTitle }}
@append
@php $metaTags = [ 'title' => $pageTitle ] @endphp
@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title_link" => request()->url(),
		"title" => user()->full_name,
		"sub_title" => trans('ehdafront::general.profile_phrases.profile') ,
		"subtitle_link" => '#'
	])
	<div class="container-fluid">
		<div class="container content pb20">
			@yield('inner-content')
		</div>
	</div>
@append