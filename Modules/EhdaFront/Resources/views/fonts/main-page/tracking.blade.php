<div class="col-xs-12">
	<div class="col-xs-12 mb30">
		<div class="col-xs-12 border-blue rounded-corners-5 border-1 text-align-start pt20 pb10">
			{!! Form::open([
				'url'	=> EhdaFrontRouteTools::actionLocale('FontsController@tracking') ,
				'method'=> 'post',
				'class' => 'clearfix ehda-card-form js',
				'novalidate' => 'novalidate',
				'id' => 'tracking-form'
			]) !!}


			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						@include('ehdafront::forms.input', [
							'name' => 'tracking_number',
							'class' => 'form-required'
						])
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						@include('ehdafront::forms.input', [
							'name' => 'email',
							'class' => 'form-email form-required'
						])
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 pt15 text-center">
					@include('ehdafront::forms.button', [
						'shape' => 'success',
						'label' => trans('ehdafront::cart.tracking'),
						'type' => 'submit',
					])
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 pt15">
					@include('ehdafront::forms.feed')
				</div>
			</div>

			{!! Form::close() !!}

		</div>
	</div>

</div>
