@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = $staticPost->title) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image', [
		"title_link" => request()->url(),
		"title" => $title,
		"blur" => true,
	])

	<div class="container mt40 mb40">
		@include('ehdafront::fonts.main-page.content')
		@include('ehdafront::layouts.widgets.add-to-any',[
			"shortUrl" => request()->url() ,
		])
	</div>
@append

@include('ehdafront::fonts.main-page.scripts')