@section('endOfBody')
	<script>
        let purchaseForm = $('#purchase-form');
        $(document).ready(function () {
            $('.btn-buy-post').click(function (e) {
                e.preventDefault();
                let key          = $(this).data('postKey');
                let targetFormId = 'purchase-form-' + key;
                let targetForm   = $('.purchase-form#' + targetFormId);
                let anyVisible = $('.purchase-form:visible').length;

                $('.purchase-form').not('#' + targetFormId).hide();
                if (anyVisible) {
                    targetForm.show()
                } else {
                    targetForm.slideDown()
                }


            });
        });
	</script>
@append