@if(!isset($purchaseFormIsHidden))
	@php $purchaseFormIsHidden = true @endphp
@endif

<div class="row">
	<div class="col-xs-12 col-xs-offset-0 purchase-form mt20 mb20" id="purchase-form-{{ $postHashid }}"
		 @if($purchaseFormIsHidden) style="display: none;" @endif>
		<div class="row">
			<div class="col-xs-12">
				{!! Form::open([
					'url'	=> EhdaFrontRouteTools::actionLocale('FontsController@purchase') ,
					'method'=> 'post',
					'class' => 'clearfix ehda-card-form js',
					'id' => "form-$postHashid",
					'novalidate' => 'novalidate',
				]) !!}

				@include('ehdafront::forms.hidden', [
					'id' => 'id',
					'name' => 'id',
					'value' => $wareHashid,
				])

				<div class="row">
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							@include('ehdafront::forms.input', [
								'name' => 'name',
								'class' => 'form-persian form-required',
								'placeholder' => trans('ehdafront::validation.attributes.first_and_last_name'),
								'value' => (!auth()->guest() ? user()->full_name : '')
							])
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							@include('ehdafront::forms.input', [
								'name' => 'email',
								'class' => 'form-email form-required',
								'value' => (!auth()->guest() ? user()->email : '')
							])
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							@include('ehdafront::forms.input', [
								'name' => 'mobile',
								'class' => 'form-mobile form-required',
								'value' => (!auth()->guest() ? user()->mobile : '')
							])
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							@include('ehdafront::forms.input', [
								'name' => 'job',
								'value' => (!auth()->guest() ? user()->job : '')
							])
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							@include('ehdafront::forms.input', [
								'name' => 'title',
								'class' => 'form-number',
								'value' => $title,
								'disabled' => true,
							])
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							@include('ehdafront::forms.input', [
								'name' => 'price',
								'class' => 'form-number',
								'value' => EhdaFrontCurrencyTools::convertToPreviewCurrency($price)
							])
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 pt15">
						@include('ehdafront::forms.button', [
							'shape' => 'success',
							'label' => trans('ehdafront::cart.pay'),
							'type' => 'submit',
						])
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 pt15">
						@include('ehdafront::forms.feed')
					</div>
				</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

{{--@section('endOfBody')--}}
{{--{!! Html::script ('assets/libs/jquery.form.min.js') !!}--}}
{{--{!! Html::script ('assets/js/forms.js') !!}--}}

{{--<script>--}}
{{--$(document).ready(function () {--}}
{{--$('#btn-purchase').click(function () {--}}
{{--$('#purchase-form').slideDown();--}}
{{--$(this).hide()--}}
{{--});--}}
{{--});--}}
{{--</script>--}}
{{--@append--}}