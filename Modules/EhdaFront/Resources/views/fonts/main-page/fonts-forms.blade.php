@foreach($fontPosts as $post)
	@php
		$post->spreadMeta();
		$ware = $post->wares()->first();
		$price = $ware->price()->get()
	@endphp
@section('fontButtons')
	@include('ehdafront::fonts.main-page.button-with-image', [
		'buttonOptions' => [
			'buttonText' => trans('ehdafront::cart.buy_thing', [
				'thing' => $post->title
					. ' '
					. ad(number_format(EhdaFrontCurrencyTools::convertToPreviewCurrency($price)))
					. ' '
					. trans('ehdafront::general.toman')
			]),
			'buttonClass' => 'btn-buy-post',
			'dataAttributes' => [
				'post-key' => $post->hashid,
				'post-price' => $price,
				'post-title' => $post->title,
			]
		]
	])
@append

@if(
	!isset($guidanceFile) and
	$post->guidance_file and
	(($guidanceFileTemp = fileManager()->file($post->guidance_file)->resolve())->getUrl())
	)
	@php $guidanceFile = $guidanceFileTemp @endphp
@endif

@section('fontForms')
	@include('ehdafront::fonts.main-page.purchase-form', [
		'title' => $post->title,
		'wareHashid' => $ware->hashid,
		'postHashid' => $post->hashid,
		'price' => $price,
	])
@append
@endforeach

@section('fontButtons')
	@isset($guidanceFile)
		@include('ehdafront::fonts.main-page.button-with-image', [
				'buttonOptions' => [
					'buttonText' => trans('ehdafront::ehda.get_fonts_catalog'),
					'buttonColor' => 'info',
					'buttonIcon' => 'pdf',
					'buttonLink' => $guidanceFile->getDownloadUrl()
				]
			])
	@endisset
@append


@yield('fontButtons')


@yield('fontForms')

@section('endOfBody')
	{!! Html::script (Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
@append