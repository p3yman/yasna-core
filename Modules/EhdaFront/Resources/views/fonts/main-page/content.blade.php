<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<div class="col-md-4 col-xs-12 text-center pull-end">
				@if ($standImage = fileManager()->file($staticPost->featured_image)->getUrl())
					<img class="post-cover" src=" {{ $standImage }}" alt="">
				@endif
				@include('ehdafront::fonts.main-page.tracking')
			</div>

			<div class="col-md-8 col-xs-12 pull-end">
				<div class="row">
					<div class="col-xs-12 post-text text-justify border-bottom-1 border-bottom-darkGray mb20">
						{!! $staticPost->text !!}
					</div>
				</div>
				@if (isset($purchaseFormText))
				    <div class="col-xs-12">
						{!! $purchaseFormText->text !!}
					</div>
				@endif
				@include('ehdafront::fonts.main-page.fonts-forms')
			</div>
		</div>
	</div>
</div>