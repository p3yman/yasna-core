@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($posttypeTitle = $posttype->titleIn(getLocale())) @endphp
@php Template::appendToPageTitle($post->title) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@if (($postImage = fileManager()->file($post->featured_image))->getUrl())
	@php Template::mergeWithOpenGraphs(['image' => $postImage->getUrl()]) @endphp
@endif

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => trans('ehdafront::general.volunteer_section.special.plural') ,
		"title_link" => EhdaFrontRouteTools::actionLocale('PostController@celebrities') ,
		"sub_title" => $post->title ,
		"subtitle_link" => "#" ,
		"bg_img" => ($cover_hashid = $posttype->spreadMeta()->single_cover_image)
			? fileManager()->file($cover_hashid)->getUrl()
			: null,
		"blur" => $post->spreadMeta()->single_cover_blur,
	])

	<div class="container">
		{!! $innerHTML !!}
	</div>
@endsection