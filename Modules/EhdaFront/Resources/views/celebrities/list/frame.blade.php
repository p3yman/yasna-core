@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = $postType->titleIn(getLocale())) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => $title,
		"bg_img" => ($cover_hashid = $postType->spreadMeta()->list_cover_image)
			? fileManager()->file($cover_hashid)->getUrl()
			: null,
		"blur" => $postType->spreadMeta()->list_cover_blur,
	])

	<div class="container celebrities mt50 mb50">
		@if ($innerHTML)
			{!! $innerHTML !!}
		@else
			@include('ehdafront::layouts.widgets.error-box',[
				"message" => trans('ehdafront::general.no_result_found') ,
			])
		@endif
	</div>

@endsection