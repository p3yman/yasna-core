@include('ehdafront::layouts.widgets.link', [
    'anchor' => [
        'text' => trans('ehdafront::general.messages.login'),
        'attributes' => [
            'href'  => route('login'),
            //'class' => 'link-green',
        ],
    ]
])