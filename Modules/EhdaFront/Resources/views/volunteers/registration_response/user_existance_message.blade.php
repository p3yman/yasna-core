@if ($user->is_admin()) {{--This user is a volunteer--}}
{{ trans('ehdafront::general.messages.you_are_volunteer') }}
<br>
@include('ehdafront::volunteers.registration_response.login_link')

@elseif ($user->withDisabled()->is_admin()) {{--This user is a blocked volunteer--}}
{{ trans('ehdafront::general.messages.unable_to_register_volunteer') }}

@elseif ($user->is_a('card-holder')) {{--This user has card--}}
{{ trans('ehdafront::general.messages.you_are_card_holder') }}
<br>
@include('ehdafront::volunteers.registration_response.login_link')

@endif