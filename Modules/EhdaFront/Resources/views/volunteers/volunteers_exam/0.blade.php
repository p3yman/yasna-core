@extends('ehdafront::layouts.frame')
<title>{{ trans('global.siteTitle') }} | {{ $volunteer->title }}</title>
@section('content')
    <div class="container-fluid">
        @include('ehdafront::layouts.page_title', [
        'category' => $volunteer->say('header'),
        'parent' => $volunteer->say('category_name'),
        'sub' => $volunteer->title
        ])
        @include('ehdafront::volunteers.volunteers_exam.exam_content')
    </div>
@endsection