@section('head')
    {!! Html::style(Module::asset('ehdafront:libs/bootstrap-select/bootstrap-select.min.css')) !!}
@append

<div class="row article">
    <div class="container">
        <div class="col-xs-12">
            <div class="row border-2 border-blue pt15 pb15 rounded-corners-5">
                {!! Form::open([
                    'url'	=> EhdaFrontRouteTools::actionLocale('VolunteerController@registerFinalStepSubmit') ,
                    'method'=> 'post',
                    'class' => 'clearfix ehda-card-form js',
                    'name' => 'register_form',
                    'id' => 'volunteer_final_step',
                    'novalidate' => 'novalidate',
                ]) !!}
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <h5 class="form-heading">{{ trans('ehdafront::general.personal_information') }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'name_first',
                            'value' => $currentValues['name_first'],
                            'class' => 'form-persian form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.name_first'),
                                'minlength' => 2,
                            ]
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'name_last',
                            'value' => $currentValues['name_last'],
                            'class' => 'form-persian form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.name_last'),
                                'minlength' => 2,
                            ]
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms._select-gender', [
                            'class' => 'form-select form-required',
                            'required' => 1,
                            'value' => $currentValues->get('gender'),
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'name_father',
                            'value' => $currentValues->get('name_father'),
                            'class' => 'form-persian form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.name_father'),
                            ]
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'code_id',
                            'value' => $currentValues->get('code_id'),
                            'class' => 'form-number form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                            'title' => trans('ehdafront::validation.attributes_example.code_id'),
                                'minlength' => 1,
                                'maxlength' => 10,
                            ]
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'code_melli',
                            'value' => $currentValues->get('code_melli'),
                            'class' => 'form-national form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.code_melli'),
                                'readonly' => 'readonly',
                                'minlength' => 10,
                                'maxlength' => 10,
                            ]
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @if($birthDate = $currentValues->get('birth_date'))
                            @if(getLocale() == 'fa')
                                @php $bd = \Morilog\Jalali\jDate::forge($birthDate) @endphp
                            @else
                                @php $bd = user()->birth_date @endphp
                            @endif
                            @php $bdVal = $bd->format('Y/m/d') @endphp
                        @else
                            @php $bdVal = '' @endphp
                        @endif
                        {{--@include('ehdafront::forms._birthdate-datepicker', [--}}
                        {{--'name' => 'birth_date',--}}
                        {{--'class' => 'form-datepicker form-required',--}}
                        {{--'value' => $bdVal,--}}
                        {{--'dataAttributes' => [--}}
                        {{--'toggle' => 'tooltip',--}}
                        {{--'placement' => 'top',--}}
                        {{--],--}}
                        {{--'otherAttributes' => [--}}
                        {{--'title' => trans('ehdafront::validation.attributes_example.birth_date'),--}}
                        {{--]--}}
                        {{--])--}}
                        @include('ehdafront::forms._birthdate_3_selects', [
                            'name' => 'birth_date',
                            'class' => 'form-required',
                            'value' => $bdVal,
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.birth_date'),
                            ]
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms._states-selectpicker', [
                            'name' => 'birth_city' ,
                            'value' => hashid_encrypt($currentValues->get('birth_city'), 'ids'),
                            'required' => 1,
                            'class' => 'form-required',
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms._select-marital', [
                            'class' => 'form-select form-required',
                            'required' => 1,
                            'value' => $currentValues->get('marital'),
                        ])
                    </div>
                </div>
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <h5 class="form-heading">{{ trans('ehdafront::general.educational_information') }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.select-picker', [
                            'name' => 'edu_level',
                            'value' => $currentValues->get('edu_level'),
                            'class' => 'form-select form-required',
                            'required' => 1,
                            'options' =>
                                collect(EhdaTools::activeEduLevelsTranses())
                                ->map(function ($item, $key) {
                                    return ['id' => $key, 'title' => $item];
                                }),
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'edu_field',
                            'value' => $currentValues->get('edu_field'),
                            'class' => 'form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.edu_field'),
                            ]
                        ])
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms._states-selectpicker', [
                            'name' => 'edu_city' ,
                            'value' => hashid_encrypt($currentValues->get('edu_city'), 'ids'),
                            'required' => 1,
                            'class' => 'form-required',
                        ])
                    </div>
                </div>
                
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <h5 class="form-heading">{{ trans('ehdafront::general.contact_info') }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'email',
                            'value' => $currentValues->get('email'),
                            'class' => 'form-email form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.email'),
                            ]
                        ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'mobile',
                            'value' => $currentValues->get('mobile'),
                            'class' => 'form-mobile form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.mobile'),
                                'minlength' => 11,
                                'maxlength' => 11,
                            ]
                        ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'tel_emergency',
                            'value' => $currentValues->get('tel_emergency'),
                            'class' => 'form-phone form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.tel_emergency'),
                                'minlength' => 11,
                                'maxlength' => 11,
                            ]
                        ])
                    </div>
                </div>
                
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <h5 class="form-heading">{{ trans('ehdafront::general.home_info') }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms._states-selectpicker', [
                            'name' => 'home_city' ,
                            'value' => hashid_encrypt($currentValues->get('home_city'), 'ids'),
                            'required' => 1,
                            'class' => 'form-required',
                        ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'home_address',
                            'value' => $currentValues->get('home_address'),
                            'class' => 'form-persian form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.home_address'),
                                'minlength' => 10,
                            ]
                         ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'home_tel',
                            'value' => $currentValues->get('home_tel'),
                            'class' => 'form-phone form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.home_tel'),
                                'minlength' => 11,
                                'maxlength' => 11,
                            ]
                        ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'home_postal',
                            'value' => $currentValues->get('home_postal'),
                            'class' => 'form-number',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.home_postal'),
                                'minlength' => 10,
                                'maxlength' => 10,
                            ]
                        ])
                    </div>
                </div>
                
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <h5 class="form-heading">{{ trans('ehdafront::general.job_info') }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'job',
                            'value' => $currentValues->get('job'),
                            'class' => 'form-persian form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.job'),
                                'minlength' => 2,
                            ]
                         ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms._states-selectpicker', [
                            'name' => 'work_city' ,
                            'value' => hashid_encrypt($currentValues->get('work_city'), 'ids'),
                        ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'work_address',
                            'value' => $currentValues->get('work_address'),
                            'class' => 'form-persian',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.work_address'),
                            ]
                         ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'work_tel',
                            'value' => $currentValues->get('work_tel'),
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.work_tel'),
                                'minlength' => 11,
                                'maxlength' => 11,
                            ]
                        ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'work_postal',
                            'value' => $currentValues->get('work_postal'),
                            'class' => 'form-number',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.work_postal'),
                                'minlength' => 10,
                                'maxlength' => 10,
                            ]
                        ])
                    </div>
                </div>
                
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <h5 class="form-heading">{{ trans('ehdafront::general.supplementary_info') }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.select-picker', [
                            'name' => 'familiarization',
                            'label' => trans('ehdafront::validation.attributes.familiarization'),
                            'value' => $currentValues->get('familiarization'),
                            'class' => 'form-required',
                            'blank_value' => '0' ,
                            'required' => 1,
                            'options' =>
                                collect(EhdaTools::activeFamiliarizationTranses())
                                ->map(function ($item, $key) {
                                    return ['id' => $key, 'title' => $item];
                                }),
                        ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'motivation',
                            'value' => $currentValues->get('motivation'),
                            'class' => 'form-persian form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.motivation'),
                            ]
                         ])
                    </div>
                </div>
                
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="row">
                        @include('ehdafront::forms.input', [
                            'name' => 'alloc_time',
                            'value' => $currentValues->get('alloc_time'),
                            'class' => 'form-number form-required',
                            'dataAttributes' => [
                                'toggle' => 'tooltip',
                                'placement' => 'top',
                            ],
                            'otherAttributes' => [
                                'title' => trans('ehdafront::validation.attributes_example.alloc_time'),
                            ]
                         ])
                    </div>
                </div>
                
                @if(auth()->guest())
                    <div class="col-xs-12">
                        <div class="form-group">
                            <h5 class="form-heading">{{ trans('ehdafront::general.login_info') }}</h5>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="row">
                            @include('ehdafront::forms.input', [
                                'name' => 'password',
                                'class' => 'form-password form-required',
                                'type' => 'password',
                                'dataAttributes' => [
                                    'toggle' => 'tooltip',
                                    'placement' => 'top',
                                ],
                                'otherAttributes' => [
                                    'title' => trans('ehdafront::validation.attributes_example.password'),
                                    'minlength' => 8,
                                    'maxlength' => 64,
                                ]
                            ])
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="row">
                            @include('ehdafront::forms.input', [
                                'name' => 'password2',
                                'class' => 'form-required',
                                'type' => 'password',
                                'dataAttributes' => [
                                    'toggle' => 'tooltip',
                                    'placement' => 'top',
                                ],
                                'otherAttributes' => [
                                    'title' => trans('ehdafront::validation.attributes_example.password2'),
                                    'minlength' => 8,
                                    'maxlength' => 64,
                                ]
                            ])
                        </div>
                    </div>
                @endif
                
                
                <div class="col-xs-12">
                    <div class="form-group">
                        <h5 class="form-heading">{{ trans('ehdafront::general.your_activity') }}</h5>
                    </div>
                </div>
                
                <div class="col-xs-12 col-sm-12">
                    {{-- @TODO: read logged in user info --}}
                    @include('ehdafront::forms.activity-checkboxes')
                </div>
                
                
                <div class="col-xs-12 pt15 align-horizontal-center">
                    <div class="col-md-8 col-xs-12">
                        @include('ehdafront::forms.feed')
                    </div>
                </div>
                <div id="form-buttons" class="col-xs-12 text-center">
                    @include('ehdafront::forms.button', [
                        'id' => 'btn-submit',
                        'shape' => 'success',
                        'label' => trans('ehdafront::forms.button.send'),
                        'type' => 'submit',
                    ])
                    @include('ehdafront::layouts.widgets.link', [
                        'anchor' => [
                            'text' => trans('ehdafront::general.back_to_first_page'),
                            'attributes' => [
                                'id' => 'home-link',
                                'class' => 'btn btn-info',
                                'href' => EhdaFrontRouteTools::actionLocale('EhdaFrontController@index'),
                                'style' => 'display: none;',
                            ],
                        ]
                    ])
                    @include('ehdafront::layouts.widgets.link', [
                        'anchor' => [
                            'text' => trans('ehdafront::general.profile_phrases.profile'),
                            'attributes' => [
                                'id' => 'profile-link',
                                'class' => 'btn btn-info',
                                'href' => EhdaFrontRouteTools::actionLocale('UserController@index'),
                                'style' => 'display: none;',
                            ],
                        ]
                    ])
                </div>
                
                
                {!! Form::close() !!}
            
            </div>
        </div>
    </div>
</div>

@section('endOfBody')
    <script>
        var bootstrapTooltip = $.fn.tooltip.noConflict(); // return $.fn.tooltip to previously assigned value

        $(document).on({
            click: function (e) {
                return false;
            }
        }, '.dropdown-toggle');
    </script>
    
    {!! Html::script (Module::asset('ehdafront:libs/bootstrap-select/bootstrap-select.min.js')) !!}
    {!! Html::script (Module::asset('ehdafront:/libs/jquery.form.min.js')) !!}
    {!! Html::script (Module::asset('ehdafront:/js/forms.min.js')) !!}
    {!! Html::script (Module::asset('ehdafront:/libs/jquery-ui/jquery-ui.min.js')) !!}
    @include('ehdafront::layouts.datepicker_assets')
    
    <script>
        $.fn.tooltip = bootstrapTooltip; // give $().bootstrapTooltip the Bootstrap functionality

        function volunteer_final_step_validate() {
            $check = $("input[name='activity[]']:checked").length;

            if ($check > 0) {
                return 0;
            }
            else {
                return '{{ trans('ehdafront::general.messages.select_at_least_one_activity') }}';
            }
        }

        /**
         * Thing to do after finish registration volunteer
         */
        function afterRegisterVolunteer(isLogin) {
            $('#volunteer_final_step').find(':input').prop('disabled', true);
            $('#btn-submit').hide();
            if (typeof isLogin != 'undefined' && isLogin) {
                $('#profile-link').show();
            } else {
                $('#home-link').show();
            }
        }
    </script>
@append

