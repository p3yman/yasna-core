@extends('ehdafront::layouts.frame')

@php $post->spreadMeta() @endphp

@php $pageTitle = setting()->ask('site_title')->gain() . ' | ' . $post->title @endphp
@section('head')
    <title>{{ $pageTitle }}</title>
@append
@php $metaTags = [ 'title' => $pageTitle ] @endphp
@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
    @include('ehdafront::layouts.widgets.title-with-image',[
		"title_link" => request()->url(),
		"title" => trans('ehdafront::general.volunteer_section.register'),
	])
    <div class="container-fluid mt20">
        @include('ehdafront::volunteers.volunteer_register.form')
    </div>
@endsection