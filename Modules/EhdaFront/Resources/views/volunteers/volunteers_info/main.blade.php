@extends('ehdafront::layouts.frame')

@php $post->spreadMeta() @endphp

@php Template::appendToPageTitle($post->title) @endphp

@section('page-title')
    {{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')


@section('content')
    @include('ehdafront::layouts.widgets.title-with-image',[
		"title" => $post->title ,
		"bg_img" => doc($post->featured_image)->getUrl() ,
	])

    <div class="container-fluid pt40">
        @include('ehdafront::volunteers.volunteers_info.volunteers_content')
    </div>
@endsection