@include('ehdafront::layouts.underlined_heading',[
	"text" => trans('ehdafront::general.volunteer_section.activities') ,
	"color" => "green" ,
])

<div class="list">
	@foreach($activities as $activity)
		@include('ehdafront::layouts.widgets.row-box',[
			"link" => $activity->direct_url ?: '#' ,
			"src" => ($activityImage = doc($activity->featured_image)->resolve())->getUrl() ,
			"alt" => $activityImage->getTitle() ,
			"title" => $activity->title ,
			"date" => ad(echoDate($activity->published_at, 'j F Y')) ,
			"caption" => $activity->abstract ,
		])
	@endforeach
</div>