@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = trans('ehdafront::general.ngo-foundations')) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => $title ,
		//"bg_img" => Module::asset('ehdafront:images/donation.jpg') ,
	])

	<div class="container">
		<div class="row mt100 mb50">
			<div class="col-md-3">
				@include('ehdafront::organization.box-title',[
					"title" => trans('ehdafront::general.volunteer_section.plural_short') ,
					"colors" => ['#A6CE39','#688719'] ,
					"id" => "volunteers" ,
					"icon" => Module::asset('ehdafront:images/donation.png') ,
				])
			</div>
			@if ($sponsorsPosttype->exists)
				<div class="col-md-3">
					@include('ehdafront::organization.box-title',[
						"title" => $sponsorsPosttype->titleIn(getLocale()) ,
						"colors" => ["#FFD700","#F18432"] ,
						"id" => "sponsors" ,
						"icon" => Module::asset('ehdafront:images/charity.png') ,
					])
				</div>
			@endif
			@if ($membersPosttype->exists)
				<div class="col-md-3">
					@include('ehdafront::organization.box-title',[
						"title" => $membersPosttype->titleIn(getLocale()) ,
						"colors" => ["#44c8f5","#1683A7"] ,
						"id" => "members" ,
						"icon" => Module::asset('ehdafront:images/team.png') ,
					])
				</div>
			@endif
				<div class="col-md-3">
					@include('ehdafront::organization.box-title',[
						"title" => $committees_static_post->title ,
						"colors" => ["#f54444","#a71616"] ,
						"id" => "committees" ,
						"icon" => Module::asset('ehdafront:images/teamwork.png') ,
					])
				</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="organizations">
					<div class="organization" id="volunteers">
						@include('ehdafront::organization.volunteers.section',[
							"colors" => ['#A6CE39','#688719'] ,
						])
					</div>
					@if ($sponsorsPosttype->exists)
						<div class="organization" id="sponsors">
							@include('ehdafront::organization.sponsors.section')
						</div>
					@endif
					@if ($membersPosttype->exists)
						<div class="organization" id="members">
							@include('ehdafront::organization.members.section')
						</div>
					@endif

					<div class="organization" id="committees">
						@include('ehdafront::organization.committees.section')
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

