<div class="sponsors-section">
	<div class="section-title">
		<h2 class="">{{ $sponsorsPosttype->titleIn(getLocale()) }}</h2>
	</div>

	@php
		$anySpecial = (isset($sponsorsSpecial) and $sponsorsSpecial->count());
		$anyOther = (isset($sponsorsOther) and $sponsorsOther->count());
	@endphp

	@if ($anySpecial or $anyOther)
		<div class="col-md-10 col-centered">
			@include('ehdafront::organization.sponsors.special')

			@if ($anySpecial and $anyOther)
				<hr>
			@endif

			@if ($anyOther)
				<h3>{{ $sponsorsOtherCategory->titleIn(getLocale()) }}</h3>
				<div class="others">
					<ul class="clearfix other-sponsors">
						@foreach($sponsorsOther as $sponsor)
							<li class="col-sm-6 other-sponsor">
								<a @if($sponsor->spreadMeta()->link) href="{{ $sponsor->link }}" @endif>
									{{ $sponsor->title }}
								</a>
							</li>
						@endforeach
					</ul>
				</div>
			@endif
		</div>
	@else
		@include('ehdafront::layouts.widgets.error-box',[
			"message" => trans('ehdafront::general.no_result_found') ,
		])
	@endif
</div>