@if ($anySpecial)
	<h3>{{ $sponsorsSpecialCategory->titleIn(getLocale()) }}</h3>
	<div class="important">
		@foreach($sponsorsSpecial as $sponsor)
			<div class="sponsor-card">
				<a @if($sponsor->spreadMeta()->link) href="{{ $sponsor->link }}" @endif>
					<div class="col-sm-12">
						<div class="logo-title">
							<h1 class="text-blue">{{ $sponsor->title }}</h1>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="logo-wrapper">
							{{-- Square ratio is prefered--}}
							@php $image = doc($sponsor->featured_image)->resolve() @endphp
							<img src="{{ $image->getUrl() }}" alt="{{ $image->getTitle() }}">
						</div>
					</div>
					<div class="col-sm-8">
						<div class="content text-justify">
							{!! $sponsor->abstract !!}
						</div>
					</div>
				</a>
			</div>
		@endforeach
	</div>
@endif
