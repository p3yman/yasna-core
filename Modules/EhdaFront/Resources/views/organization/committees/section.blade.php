<div class="committees-section">
	<div class="section-title">
		<h2 class="">{{ $committees_static_post->title }}</h2>
	</div>

	<div class="col-sm-12 mb30">
		{!! $committees_static_post->text !!}
	</div>

</div>