@if($celebrities->count())
	<div class="random-celebrities">
		<div class="celebrities-heading clearfix">
			<div class="col-xs-8">
				<h3>{{ trans('ehdafront::general.volunteer_section.special.plural') }}</h3>
			</div>
			<div class="col-xs-4">
				<a href="{{ EhdaFrontRouteTools::actionLocale('PostController@celebrities') }}"
				   class="read-more">
					{{ trans('ehdafront::general.continue') }}
					@if(isLangRtl())
						<i class="fa fa-arrow-left"></i>
					@else
						<i class="fa fa-arrow-right"></i>
					@endif
				</a>
			</div>
		</div>
		<div class="random-celebrities-body">
			@foreach($celebrities as $celebrity)
				<div class="random-celebrity-col">
					<a href="{{ $celebrity->direct_url }}">
						<div class="random-celebrity-image">
							@php
								$celebrity_image = fileManager()
									->file($celebrity->featured_image)
									->posttype($celebrities_posttype)
									->config('featured_image')
									->version('150x150')
									->getUrl()
							@endphp
							<img src="{{ $celebrity_image }}" alt="{{ $celebrity->title }}">
						</div>
						{{--<div class="random-celebrity-name">--}}
						{{--{{ $celebrity['title'] }}--}}
						{{--</div>--}}
					</a>
				</div>
			@endforeach
		</div>
	</div>
@endif