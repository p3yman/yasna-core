<div class="organization-box"
	 data-id="{{ $id }}"
	style="background: linear-gradient({{ $colors[0] }}, {{ $colors[1] }})">
		<div class="image-wrapper">
			<img src="{{ $icon }}" alt="icon">
		</div>
		<div class="title">
			{{ $title }}
		</div>
</div>