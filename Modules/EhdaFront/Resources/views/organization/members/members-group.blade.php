@php
	$category = $group->get('category');
	$groupSlug = $category->slug
@endphp

@foreach($group->get('members') as $member)
	@php $member->spreadMeta() @endphp
	@if ($imageUrl = ($imageFile = doc($member->featured_image)->resolve())->getUrl())
		@php
			$image = $imageFile->getUrl();
			$alt = $imageFile->getTitle();
		@endphp
	@else
		@php
			$image = Module::asset('ehdafront:images/placeholder-avatar.png');
			$alt = '';
		@endphp
	@endif

	@section(($member->special ? "special_members_" : "other_members_") . $groupSlug)
		@include('ehdafront::organization.members.member',[
			"src" => $image ,
			"alt" => $alt ,
			"name" => $member->title ,
			"position" => $member->seat ,
		])
	@append
@endforeach

<div class="organization-group">
	<div class="title">
		<h2>{{ $category->titleIn(getLocale()) }}</h2>
	</div>
	<div class="members">
		<div class="specials">
			@yield("special_members_$groupSlug")
		</div>
		<div class="normals">
			@yield("other_members_$groupSlug")
		</div>
	</div>
</div>