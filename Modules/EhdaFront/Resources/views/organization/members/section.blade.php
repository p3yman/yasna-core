<div class="members-section">
	<div class="section-title">
		<h2 class="">{{ $membersPosttype->titleIn(getLocale()) }}</h2>
	</div>

	@if (isset($membersGroups))
		@foreach($membersGroups as $group)
			@include('ehdafront::organization.members.members-group')
		@endforeach
	@else
		@include('ehdafront::layouts.widgets.error-box',[
			"message" => trans('ehdafront::general.no_result_found') ,
		])
	@endif
</div>