@extends('ehdafront::auth.frame')

@section('body')
    <div class="login-form bg-lightGray">

        @if(isset($showLogo) and $showLogo)
            @if($logo = doc(get_setting('site_logo_tiny'))->getUrl())
                <div class="text-center mb10">
                    <img src="{{ $logo }}" style="width: 100px">
                </div>
            @endif
        @endif

        @yield('formBox')
    </div>
@endsection
