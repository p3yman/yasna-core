@extends('ehdafront::auth.form_frame', ['showLogo' => true])

@php Template::appendToPageTitle($titlePart = trans('ehdafront::people.form.recover_password')) @endphp
@php Template::mergeWithOpenGraphs(['description' => $titlePart]) @endphp
@include('ehdafront::layouts.open_graph_meta_tags')

@section('formBox')
    {!! Form::open([
        'url' => EhdaFrontRouteTools::action('Auth\ForgotPasswordController@changePassword'),
        'method'=> 'post',
        'class' => 'js',
        'name' => 'editForm',
        'id' => 'editForm',
    ]) !!}

    <div class="row">
        @include('ehdafront::forms.input',[
            'name' => 'new_password',
            'type' => 'password',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.new_password'),
            'containerClass' => 'field',
            'class' => 'input-lg',
            'icon' => 'key',
        ])
    </div>

    <div class="row">
        @include('ehdafront::forms.input',[
            'name' => 'new_password2',
            'type' => 'password',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.new_password2'),
            'containerClass' => 'field',
            'class' => 'input-lg',
            'icon' => 'key',
        ])
    </div>

    <div class="pb15 text-center">
        <button class="btn btn-green btn-lg"> {{ trans('ehdafront::forms.button.save') }} </button>
    </div>

    @include('ehdafront::forms.feed')
    {!! Form::close() !!}
@endsection

@section('endOfBody')
    {!! Html::script (Module::asset('ehdafront:libs/jquery.form.js')) !!}
    {!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
@endsection