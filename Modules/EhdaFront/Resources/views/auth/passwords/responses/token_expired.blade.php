{{ trans('ehdafront::passwords.token_expired') }}
<br>
<a href="{{ EhdaFrontRouteTools::action('Auth\ForgotPasswordController@showLinkRequestForm') }}">
    {{ trans('ehdafront::passwords.get_new_token') }}
</a>