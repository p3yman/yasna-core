@extends('ehdafront::auth.form_frame', ['showLogo' => true])

@php Template::appendToPageTitle($titlePart = trans('ehdafront::people.form.recover_password')) @endphp
@php Template::mergeWithOpenGraphs(['description' => $titlePart]) @endphp
@include('ehdafront::layouts.open_graph_meta_tags')

@php $multi_recovery_methods = (count($recovery_methods) > 1) @endphp
@php $default_recovery_methods = array_first($recovery_methods) @endphp

@section('formBox')
	{!! Form::open([
			'url' => EhdaFrontRouteTools::action('Auth\ForgotPasswordController@sendResetLinkEmail'),
		]) !!}

	<div class="row">
		@include('ehdafront::forms.input',[
			'name' => 'code_melli',
			'label' => false,
			'placeholder' => trans('ehdafront::validation.attributes.code_melli'),
			'containerClass' => 'field',
			'class' => 'input-lg',
			'icon' => 'id-card',
			'value' => old('code_melli'),
			'show_input_error' => false,
		])

		@if ($multi_recovery_methods)
			@include('ehdafront::layouts.widgets.radio',[
				'name' => 'type',
				'value' => $errors->has('mobile') ? 'mobile' :'email',
				'options' => [
					'email' => trans('ehdafront::validation.attributes.email'),
					'mobile' => trans('ehdafront::validation.attributes.mobile'),
				],
				'label' => false,
			])
		@endif

		@if (in_array('email', $recovery_methods))
			@include('ehdafront::forms.input',[
				'name' => 'email',
				'label' => false,
				'placeholder' => trans('ehdafront::validation.attributes.email'),
				'class' => 'input-lg',
				'container' => [
					'class' => 'field',
					'id' => 'email-container',
					'other' => [
						'style' => ($default_recovery_methods == 'email') ? '' : 'display:none',
					],
				],
				'icon' => 'envelope',
				'value' => old('email'),
				'show_input_error' => false,
			])
		@endif

		@if (in_array('mobile', $recovery_methods))
			@include('ehdafront::forms.input',[
				'name' => 'mobile',
				'label' => false,
				'placeholder' => trans('ehdafront::validation.attributes.mobile'),
				'class' => 'input-lg',
				'container' => [
					'class' => 'field',
					'id' => 'mobile-container',
					'other' => [
						'style' => ($default_recovery_methods == 'mobile') ? '' : 'display:none',
					],
				],
				'icon' => 'phone',
				'value' => old('mobile'),
				'show_input_error' => false,
			])
		@endif

	</div>

	<div class="tal pb15">
		<button class="btn btn-green btn-block btn-lg"> {{ trans('ehdafront::people.form.send_password_reset_code') }} </button>
	</div>

	<div class="tal pb15">
		<a href="{{ EhdaFrontRouteTools::action('Auth\ForgotPasswordController@getToken', ['haveCode' => 'code']) }}">
			<button type="button" class="btn btn-blue btn-block">
				{{ trans('ehdafront::people.form.have_a_code') }}
			</button>
		</a>
	</div>

	@if (!env('APP_DEBUG'))
		{!! app('captcha')->render(getLocale()); !!}
	@endif

	@if ($errors->all())
		<div class="for-feed alert alert-danger">
			{!! implode('<br>', $errors->all()) !!}
		</div>
	@endif
	{!! Form::close() !!}
@endsection

@section('endOfBody')
	@if ($multi_recovery_methods)
		<script>
            $(document).ready(function () {
                $('input[type=radio][name=type]').change(function () {
                    if ($(this).is(':checked')) {
                        if ($(this).val() == 'email') {
                            $('#email-container').show();
                            $('#mobile-container').hide();
                        } else if ($(this).val() == 'mobile') {
                            $('#mobile-container').show();
                            $('#email-container').hide();
                        }
                    }
                }).change();
            });
		</script>
	@endif
@endsection