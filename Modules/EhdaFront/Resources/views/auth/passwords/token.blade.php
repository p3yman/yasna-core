@extends('ehdafront::auth.form_frame', ['showLogo' => true])

@php Template::appendToPageTitle($titlePart = trans('ehdafront::people.form.recover_password')) @endphp
@php Template::mergeWithOpenGraphs(['description' => $titlePart]) @endphp
@include('ehdafront::layouts.open_graph_meta_tags')

@section('formBox')
    {!! Form::open([
           'url' => EhdaFrontRouteTools::action('Auth\ForgotPasswordController@checkToken'),
           'method'=> 'post',
           'name' => 'editForm',
           'id' => 'editForm',
       ]) !!}
    
    @if($haveCode or !session()->get($sessionName))
        <div class="row">
            @include('ehdafront::forms.input',[
                'name' => 'code_melli',
                'label' => false,
                'placeholder' => trans('ehdafront::validation.attributes.code_melli'),
                'class' => 'form-required form-national input-lg',
                'containerClass' => 'field',
                'error_value' => trans('ehdafront::validation.javascript_validation.code_melli'),
                'icon' => 'id-card',
            ])
        </div>
    @endif
    
    <div class="row">
        @include('ehdafront::forms.input',[
            'name' => 'password_reset_token',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.password_reset_token'),
            'class' => 'form-required input-lg',
            'containerClass' => 'field',
            'error_value' => trans('ehdafront::validation.javascript_validation.password_reset_token'),
            'icon' => 'key',
            'show_input_error' => false,
        ])
    </div>
    
    <div class="pb15 text-center">
        <button class="btn btn-green btn-lg"> {{ trans('ehdafront::people.form.check_password_token') }} </button>
    </div>

{{--    @if (!env('APP_DEBUG'))--}}
        {!! app('captcha')->render(getLocale()); !!}
    {{--@endif--}}



    @if ($errors->all())
        <div class="for-feed alert alert-danger">
            {!! implode('<br>', $errors->all()) !!}
        </div>
    @endif
    {!! Form::close() !!}
@endsection