<div class="field">
    <input type="{{ $type or '' }}" name="{{ $name or '' }}" id="{{ $name or '' }}" class="{{ $class or '' }}"
           placeholder="{{ trans('ehdafront::validation.attributes.' . $name) }}"
           title="{{ trans('ehdafront::validation.attributes_example.' . $name)}}"
           minlength="{{ $min or '' }}"
           maxlength="{{ $max or '' }}"
           value="{{ $value or '' }}"
           error-value="{{ trans('ehdafront::validation.javascript_validation.' . $name) }}"
            {{ $other or '' }}>
</div>