@section('head')
    {{ Html::style(Module::asset('ehdafront:css/login.min.css')) }}
@append

@include('ehdafront::layouts.header')

<body @if($background = doc(get_setting('login_page_background'))->getUrl()) style="background-image: url('{{ $background }}')" @endif>
@yield('body')

@yield('endOfBody')
</body>
