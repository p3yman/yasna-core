@extends('ehdafront::auth.form_frame', ['showLogo' => true])

@section('head')
    @parent
    <title>
        {{ setting()->ask('site_title')->gain() }} | {{ trans('ehdafront::general.register') }}
    </title>
@append
@include('ehdafront::layouts.open_graph_meta_tags', ['description' => trans('ehdafront::general.register')])

@section('formBox')

    {!! Form::open([
        'url'	=> '/register/new' ,
        'method'=> 'post',
        'class' => 'form-horizontal js',
        'name' => 'registerForm',
        'id' => 'registerForm',
    ]) !!}

    <div class="row">
        @include('ehdafront::forms.input',[
            'name' => 'name_first',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.name_first'),
            'value' => old('name_first'),
            'containerClass' => 'field',
            'class' => 'input-lg form-persian form-required',
            'icon' => 'user',
            'extra' => 'autofocus',
        ])

        @include('ehdafront::forms.input',[
            'name' => 'name_last',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.name_last'),
            'value' => old('name_last'),
            'containerClass' => 'field',
            'class' => 'input-lg form-persian form-required',
            'icon' => 'user',
        ])

        @include('ehdafront::forms.input',[
            'name' => 'code_melli',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.code_melli'),
            'value' => old('code_melli'),
            'containerClass' => 'field',
            'class' => 'input-lg form-national form-required',
            'icon' => 'id-card',
        ])

        @include('ehdafront::forms.input',[
            'name' => 'mobile',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.mobile'),
            'value' => old('mobile'),
            'containerClass' => 'field',
            'class' => 'input-lg form-mobile form-required',
            'icon' => 'phone',
        ])

        @include('ehdafront::forms.input',[
            'name' => 'email',
            'label' => false,
            'placeholder' => trans('ehdafront::validation.attributes.email'),
            'value' => old('email'),
            'containerClass' => 'field',
            'class' => 'input-lg form-email',
            'icon' => 'envelope',
        ])

        @include('ehdafront::forms.input',[
            'name' => 'password',
            'id' => 'password',
            'label' => false,
            'type' => 'password',
            'placeholder' => trans('ehdafront::validation.attributes.password'),
            'value' => old('password'),
            'containerClass' => 'field',
            'class' => 'input-lg form-password form-required',
            'icon' => 'key',
        ])

        @include('ehdafront::forms.input',[
            'name' => 'password2',
            'id' => 'password2',
            'label' => false,
            'type' => 'password',
            'placeholder' => trans('ehdafront::validation.attributes.password2'),
            'value' => old('password2'),
            'containerClass' => 'field',
            'class' => 'input-lg form-required',
            'icon' => 'key',
        ])
    </div>

    <div class="row text-center">
        <div class="col-xs-12 mb10">
            <div class="row">
                <button class="btn btn-lg btn-block btn-green"> {{ trans('ehdafront::general.register') }} </button>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="row">
                @include('ehdafront::forms.feed')
            </div>
        </div>
        <div class="col-xs-12 mb10">
            <div class="row">
                <a type="button" href="{{ route('login') }}"
                   class="btn btn-block btn-blue"> {{ trans('ehdafront::general.member_login') }} </a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('endOfBody')
    {!! Html::script ('assets/libs/jquery.form.js') !!}
    {!! Html::script ('assets/js/forms.js') !!}
@endsection