@extends('ehdafront::layouts.frame')

@php
	Template::appendToPageTitle($title = ($staticPost->title ?: trans('ehdafront::general.angels.new')))
@endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@if ($pageImageUrl = doc($staticPost->featured_image)->getUrl())
	@php Template::mergeWithOpenGraphs(['image' => $pageImageUrl]) @endphp
@endif

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image', [
		"title_link" => request()->url(),
		"title" => $title,
		"blur" => true,
		"bg_img" => $pageImageUrl,
	])

	<div class="container mt40 mb40">

		@if ($text = $staticPost->text)
			<div class="row">
				<div class="col-xs-12">{!! $text !!}</div>
			</div>
		@endif
		@include('ehdafront::angels.new.form')

	</div>

@append