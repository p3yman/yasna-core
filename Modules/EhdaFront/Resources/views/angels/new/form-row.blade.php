@php
	$extra = '';
	if (isset($class)) {
		if (str_contains($class, 'form-required')) {
			$required = true;
		}
	} else
		$class = '';

	if (isset($disabled) and $disabled) {
		$required = false;
		$extra .= ' disabled ';
	}

	$title = Lang::has($titleTransPath = "ehdafront::validation.attributes.$name") ? trans($titleTransPath) : "";
	$placeholder = Lang::has($placeholderTransPath = "ehdafront::validation.attributes_placeholder.$name") ? trans($placeholderTransPath) : "";
	$errorValue = Lang::has($errorValueTransPath = "ehdafront::validation.javascript_validation.$name") ? trans($errorValueTransPath) : "";
	$id = $id ?? $name;
@endphp

<div class="form-row">
	<label for="{{ $id }}">
		{{ $title }}
		@if(isset($required) and $required)
			@include('ehdafront::forms.require-sign')
		@endif
	</label>
	@if($name === "message")
		<textarea id="{{ $id }}" class="form-control {{ $class or "" }}" name="{{ $name }}"
				  placeholder="{{ $placeholder or $title }}"
				  @if($errorValue) error-value="{{ $errorValue }}" @endif {!! $extra !!}>{{ $value or '' }}</textarea>
	@elseif($name === 'donation_date')
		<div class="row">
			@include('ehdafront::forms._birthdate_3_selects_self')
		</div>
	@else
		<input type="text" id="{{ $id }}" class="form-control {{ $class or "" }}" name="{{ $name }}"
			   placeholder="{{ $placeholder or $title }}"
			   @if($errorValue) error-value="{{ $errorValue }}" @endif {!! $extra !!} value="{{ $value or '' }}">
	@endif
</div>