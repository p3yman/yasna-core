{!!
	widget('form-open')
		->class('js contact-form')
		->id('new-angel-form')
		->target(EhdaFrontRouteTools::actionLocale('AngelsController@newSubmit'))
!!}

{!!
	widget('hidden')
		->name('_meta_inputs')
		->value(implode(',', $posttype->editor_inputs->pluck('hashid')->toArray()))
!!}

<div class="row">
	<div class="col-sm-6">
		@include('ehdafront::angels.new.form-row', [
			'name' => 'donor_name_and_surname',
			'required' => 1,
		])
	</div>
	<div class="col-sm-6">
		<div class="form-row">
			@include('ehdafront::angels.new.form-row', [
				'name' => 'donation_date',
				'dataAttributes' => [
					'toggle' => 'tooltip',
					'placement' => 'top',
				],
				'otherAttributes' => [
					'title' => trans('ehdafront::validation.attributes_example.donation_date'),
				],
				'required' => 1,
			])
		</div>
	</div>
	<div class="col-sm-6">
		@include('ehdafront::angels.new.form-row', [
			'name' => 'submitter_name_and_surname',
			'required' => 1,
			'value' => (auth()->guest() ? '' : user()->full_name)
		])
	</div>
	<div class="col-sm-6">
		@include('ehdafront::forms._states-selectpicker', [
			'name' => 'city_and_province' ,
			'blank_value' => '',
			'required' => 1,
		])
	</div>
	<div class="col-sm-6">
		@include('ehdafront::angels.new.form-row', [
			'name' => 'hospital_name',
			'required' => 1,
		])
	</div>

	<div class="col-xs-12">
		<input type="hidden" id="donor_image" name="_donor_image">
		{!!
			$uploader
				->resultInputSelector('#donor_image')
				->render()
		!!}
	</div>

	<div class="col-xs-12">
		<div class="form-row">
			{!!
				widget('feed')
			!!}
		</div>
	</div>

	<div class="col-xs-12 text-end">
		<button type="submit" class="btn btn-primary" value="approval" name="_submit">
			{{ trans('ehdafront::forms.general.submit') }}
		</button>
	</div>
</div>


{!!
	widget('form-close')
!!}

@section('head')
	{!! Html::style(Module::asset('ehdafront:libs/bootstrap-select/bootstrap-select.min.css')) !!}
	{!! $uploader->renderCss() !!}
@append

@section('endOfBody')
	{!! Html::script (Module::asset('ehdafront:libs/bootstrap-select/bootstrap-select.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:/libs/jquery.form.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:/js/forms.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:/libs/jquery-ui/jquery-ui.min.js')) !!}
	{!! $uploader->renderJs() !!}
@append