<div class="alert alert-warning mt10" style="display:none;" id="alertNotFound">
	{{ trans('ehdafront::general.angels.not_found') }}
	<br/>
	{{ trans('ehdafront::general.angels.register_your_angel.part1') }}
	<a href="{{ EhdaFrontRouteTools::actionLocale('AngelsController@newForm') }}"
	   class="link-green {{--open-new-angel-form--}}">
		{{ trans('ehdafront::general.angels.register_your_angel.part2') }}
	</a>
	{{ trans('ehdafront::general.angels.register_your_angel.part3') }}
</div>