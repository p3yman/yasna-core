@section('content')
	<div class="container-fluid angels-main-container">
		<div class="row angels-main-container-inner">
			@include('ehdafront::angels.index.title')
			@include('ehdafront::angels.index.stars-box')
		</div>
	</div>
@append