@section('endOfBody')

	@foreach($posts as $post)
		@php
			$post->spreadMeta();
			$jsAngles[] = [
					'id' => $post->id,
					'name' => $post->title,
					'picture_url' => doc($post->featured_image)->getUrl(),
					'donation_date' => ad(echoDate($post->donation_date, 'j F Y')),
				]
		@endphp
	@endforeach


	{!! Html::script (Module::asset('ehdafront:libs/jquery-ui/jquery-ui.min.js')) !!}
	<script>
        var searchUrl = "{{ EhdaFrontRouteTools::actionLocale('AngelsController@search') }}";
        var angels = {!! json_encode($jsAngles) !!};
        $(document).ready(function () {
            random_angles(angels);
        })
	</script>
	{!! Html::script (Module::asset('ehdafront:js/angels.min.js')) !!}
@append