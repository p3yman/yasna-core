@if(getLocale() == 'fa')
	<div class="search-angel col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
		<button class="btn-search btn btn-primary" type="button">
			<i class="fa fa-search"></i>
		</button>
		<input type="search" name="angels_name" id="angels_name" value=""
			   placeholder="{{ trans('ehdafront::general.angels.search') }}"
			   autocomplete="off"
			   style="width: 80%">

		<div class="search-status">
			<div class="loading" style="display: none" id="angleSearchLoading">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
			@include('ehdafront::angels.index.not-fount-alert')
		</div>
	</div>
@endif