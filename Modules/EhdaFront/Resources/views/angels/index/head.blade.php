@php Template::appendToPageTitle($posttypeTitle = $posttype->titleIn(getLocale())) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop
@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('head')
	{!! Html::style(Module::asset('ehdafront:libs/bootstrap-select/bootstrap-select.min.css')) !!}
	<style>
		.ui-menu.ui-autocomplete {
			max-height: 100px;
			overflow-y: auto;
			overflow-x: hidden;
		}
	</style>
@append