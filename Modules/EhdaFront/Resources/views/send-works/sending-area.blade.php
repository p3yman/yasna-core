@if (count($commentingPostsInfo))
	@if ($multipleUploader = (count($commentingPostsInfo) > 1))
		@php $selectOptions = [] @endphp
		@foreach($commentingPostsInfo as $postInfo)
			@php
				$selectOptions[] = [
					'id' => 'post-' . $postInfo['post']->hashid . '-form',
					'title' => trans(
						"filemanager::base.file-types." . $postInfo['file_type'] . ".title"
					)
				]
			@endphp
		@endforeach
		<div class="col-xs-12">
			@include('ehdafront::forms.select', [
				'id' => 'file-type',
				'name' => 'file_type',
				'options' => $selectOptions
			])
		</div>

		@section('endOfBody')
			<script>
				$(document).ready(function () {
					$('#file-type').change(function () {
						let formId = $(this).val();
						$('.contact-form').not('#' + formId).hide();
                        $('.contact-form#' + formId).show();
                    }).change();
                });
			</script>
		@append

	@endif


	@foreach($commentingPostsInfo as $postInfo)
		@php try { @endphp
		@php
			$post = $postInfo['post'];
			$fields = CommentTools::translateFields($post->fields);
			$rules = CommentTools::translateRules($post->rules);
			$fileType = $postInfo['file_type'];
			$filesInputId = $post->hashid . '-' . 'files';
			$uploader = fileManager()
					->uploader($posttype)
					->config($postInfo['config'])
					->resultInputSelector('#' . $filesInputId)
					->jsVariableName('uploader' . $post->hashid)
					->$fileType();
		@endphp

		{!!
			widget('form-open')
				->class('js contact-form')
				->id('post-' . $post->hashid . '-form')
				->style($multipleUploader ? 'display:none' : '')
				->target(EhdaFrontRouteTools::actionLocale('CommentController@saveWorks'))
		!!}

		<div class="col-xs-12">
			<input type="hidden" id="{{ $filesInputId }}" name="_files">
			{!! $uploader->render() !!}
		</div>

		@section('head')
			{!! $uploader->renderCss() !!}
		@append

		@section('endOfBody')
			{!! $uploader->renderJs() !!}
		@append

		{!!
			widget('hidden')
				->name('post_id')
				->value($post->hashid)
		!!}

		@foreach($fields as $field => $fieldInfo)
			@php $field_value = '' @endphp

			@if (!auth()->guest())
				@switch($field)
					@case('name')
					@case('full_name')
					@php $field_value = user()->full_name @endphp
					@break

					@case('email')
					@php $field_value = user()->email @endphp
					@break

					@case('mobile')
					@php $field_value = user()->mobile @endphp
					@break
				@endswitch
			@endif

			@include('ehdafront::send-works.form-row', [
				'name' => $field,
				'id' => $post->hashid . '-' . $field,
				'size' => $fieldInfo['size'],
				'required' => (!empty($rules[$field]) and in_array('required', $rules[$field])),
				'value' => $field_value
			])
		@endforeach

		<div class="col-xs-12">
			{!! widget('feed') !!}
		</div>

		<div class="col-xs-12 text-end">
			<button class="btn btn-primary">
				{{ trans('ehdafront::general.send_works.title.singular') }}
			</button>
		</div>


		{!!
			widget('form-close')
		!!}

		@php } catch (Exception $e) { @endphp
			@continue
		@php } @endphp

	@endforeach



	@section('endOfBody')
		{!! Html::script (Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
		{!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
		{!! Html::script (Module::asset('ehdafront:libs/jquery-ui/jquery-ui.min.js')) !!}
	@append
@endif
