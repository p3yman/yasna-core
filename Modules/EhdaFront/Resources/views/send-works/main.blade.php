@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = trans('ehdafront::general.send_works.title.plural')) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image', [
		"title_link" => request()->url(),
		"title" => $title,
	])

	<div class="container mt40 mb40">
		@include('ehdafront::send-works.sending-area')
	</div>
@append