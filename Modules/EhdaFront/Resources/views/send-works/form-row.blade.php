@php
	$titles = [
		'subject' => trans('ehdafront::validation.attributes.submission_work_subject'),
		'name' => trans('ehdafront::validation.attributes.submission_work_owner_name'),
		'mobile' => trans('ehdafront::validation.attributes.submission_work_owner_mobile'),
		'email' => trans('ehdafront::validation.attributes.submission_work_owner_email'),
		'description' => trans('ehdafront::validation.attributes.description'),
		'text_content' => trans('ehdafront::validation.attributes.text_content'),
	];
	$js_errors = [
		'subject' => trans('ehdafront::validation.javascript_validation.submission_work_subject'),
		'name' => trans('ehdafront::validation.javascript_validation.submission_work_owner_name'),
		'mobile' => trans('ehdafront::validation.javascript_validation.submission_work_owner_mobile'),
		'email' => trans('ehdafront::validation.javascript_validation.submission_work_owner_email'),
		'description' => trans('ehdafront::validation.javascript_validation.description'),
		'text_content' => trans('ehdafront::validation.javascript_validation.text_content'),
	];
@endphp

@if (array_has($titles, $name))
	@php
		$title = $titles[$name];
		$js_error = ($js_errors[$name] ?? null);
	@endphp

	<div class="col-xs-{{ $size ?: '12' }}">
		<div class="form-row">
			<label for="{{ $name }}">
				{{ $title }}
				@if($required)
					@include('ehdafront::forms.require-sign')
				@endif
			</label>
			@if(in_array($name, ['message', 'description', 'text_content']))
				<textarea id="{{ $id }}" class="form-control {{ $class or "" }} @if($required) form-required @endif"
						  name="{{ $name }}" placeholder="{{ $placeholder or $title }}"
						  error-value="{{ $js_error }}">{{ $value or '' }}</textarea>
			@else
				<input type="text" id="{{ $id }}"
					   class="form-control {{ $class or "" }} @if($required) form-required @endif" name="{{ $name }}"
					   placeholder="{{ $placeholder or $title }}" error-value="{{ $js_error }}"
					   value="{{ $value or '' }}">
			@endif
		</div>
	</div>
@endif
