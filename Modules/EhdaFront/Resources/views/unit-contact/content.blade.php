<div class="container mt40">
	<div class="row">
		<div class="col-md-6 mb30">
			@include('ehdafront::layouts.underlined_heading',[
				"text" => $title ,
				"color" => "blue" ,
			])
			@if ($infoPost->exists)
				{!! $infoPost->text !!}
			@endif
		</div>
		<div class="col-md-6">
			@include('ehdafront::contact.contact-form')
		</div>
	</div>
</div>