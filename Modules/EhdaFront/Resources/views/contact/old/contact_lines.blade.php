<div class="col-md-6 col-sm-12">
	<p>
	</p>
	@php
		$address = setting()->ask('address')->in(getLocale())->gain();
	@endphp
	@if($address)
		<h5>{{ trans('validation.attributes.address') }}:</h5>
		{{ $address }}
	@endif
	<p></p>
	<p>
	</p>
	@php
		$tels = setting()->ask('telephone')->gain();
	@endphp
	@if($tels)
		@if(!is_array($tels))
			@php
				$tels = [$tels ];
			@endphp
		@endif
		<h5>
			{{ trans('validation.attributes.tel') }}:
			@foreach($tels as $key => $tel)
				@if($key)
					,
				@endif
				<a href="tel:{{ $tel }}">
					{{ ad($tel) }}
				</a>
			@endforeach
		</h5>
	@endif
	@php
		$emails = setting()->ask('email')->gain();
	@endphp
	@if($emails)
		@if(!is_array($emails))
			@php
				$emails = [$emails];
			@endphp
		@endif
		<h5>
			{{ trans('validation.attributes.email') }}:
			@foreach($emails as $key => $email)
				@if($key)
					,
				@endif
				<a href="mailto:{{ $email }}">
					{{ $email }}
				</a>
			@endforeach
		</h5>
	@endif
	<p></p>
	<p>
	<ul class="social-links list-inline">
		<h5>در شبکه&zwnj;های اجتماعی ما را دنبال کنید</h5>
		@php
			$telegram = setting()->ask('telegram_link')->gain();
			$twitter = setting()->ask('twitter_link')->gain();
			$facebook = setting()->ask('facebook_link')->gain();
			$instagram = setting()->ask('instagram_link')->gain();
			$aparat = setting()->ask('aparat_link')->gain();
		@endphp
		@if($telegram)
			<li><a href="{{ $telegram }}" target="_blank"><i class="icon icon-telegram dark"></i></a></li>
		@endif
		@if($twitter)
			<li><a href="{{ $twitter }}" target="_blank"><i class="icon icon-twitter dark"></i></a></li>
		@endif
		@if($facebook)
			<li><a href="{{ $facebook }}" target="_blank"><i class="icon icon-facebook dark"></i></a></li>
		@endif
		@if($instagram)
			<li><a href="{{ $instagram }}" target="_blank"><i class="icon icon-instagram dark"></i></a></li>
		@endif
		@if($aparat)
			<li><a href="{{ $aparat }}" target="_blank"><i class="icon icon-aparat dark"></i></a></li>
		@endif
	</ul>
	</p>

</div>