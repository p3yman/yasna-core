@php
	$title = Lang::has($titleTransPath = "ehdafront::general.contact.form.field.$id.title") ? trans($titleTransPath) : "";
	$placeholder = Lang::has($placeholderTransPath = "ehdafront::general.contact.form.field.$id.placeholder") ? trans($placeholderTransPath) : "";
	$errorValue = Lang::has($errorValueTransPath = "ehdafront::general.contact.form.field.$id.js_error") ? trans($errorValueTransPath) : "";
@endphp

@if($id=== "message")
	<div class="col-md-12 mb5">
		<label for="{{ $id }}">{{ $title }}</label>
		<textarea id="{{ $id }}" class="form-control {{ $class or "" }}" name="{{ $id }}"
				  placeholder="{{ $placeholder or $title }}" rows="5"
				  @if($errorValue) error-value="{{ $errorValue }}" @endif>{{ $value or '' }}</textarea>
	</div>
@else
	<div class="col-md-6 mb5">
		<label for="{{ $id }}">{{ $title }}</label>
		<input type="text" id="{{ $id }}" class="form-control {{ $class or "" }}" name="{{ $id }}"
			   value="{{ $value or '' }}" placeholder="{{ $placeholder or $title }}"
			   @if($errorValue) error-value="{{ $errorValue }}" @endif>
	</div>
@endif