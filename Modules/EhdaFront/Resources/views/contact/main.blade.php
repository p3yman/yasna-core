@extends('ehdafront::layouts.frame')

@php
	$title = $infoPost->exists ? $infoPost->title : trans('ehdafront::general.contact_us');
	Template::appendToPageTitle($title);
@endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::contact.content')
	@include('ehdafront::contact.map')
	<div class="container mt40 mb40">
		@include('ehdafront::contact.contact-form')
	</div>
@endsection