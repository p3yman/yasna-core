@if ($commentingPost->exists)
	@php $commentingPost->spreadMeta() @endphp
	@include('ehdafront::layouts.underlined_heading',[
		"text" => $commentingPost->title2 ?: trans('ehdafront::general.contact.title.write_for_us') ,
		"color" => "blue" ,
	])

	@php $fields = CommentTools::translateFields($commentingPost->fields) @endphp

	<div class="contact-form">
		{!! $commentingPost->text !!}
		{!!
			widget('form-open')
				->class('js')
				->target(EhdaFrontRouteTools::actionLocale('CommentController@save'))
		!!}

		{!!
			widget('hidden')
				->name('post_id')
				->value($commentingPost->hashid)
		!!}
		<div class="row">
			@foreach($fields as $fieldName => $fieldInfo)
				@php $field_value = '' @endphp

				@if (!auth()->guest())
					@switch($fieldName)
					@case('name')
					@case('full_name')
					@php $field_value = user()->full_name @endphp
					@break

					@case('email')
					@php $field_value = user()->email @endphp
					@break

					@case('mobile')
					@php $field_value = user()->mobile @endphp
					@break
					@endswitch
				@endif

				@if (CommentTools::isAcceptableField($fieldName))
					@include('ehdafront::contact.form-row',[
						"id" => $fieldName ,
						"class" => $fieldInfo['required'] ? "form-required" : "",
						'value' => $field_value
					])
				@endif
			@endforeach
		</div>


		<div class="form-row">
			{!!
				widget('feed')
			!!}
		</div>

		<div class="form-row">
			<button type="submit" class="btn btn-primary">
				{{ trans('ehdafront::general.contact.form.button') }}
			</button>
		</div>

		{!!
			widget('form-close')
		!!}
	</div>

@endif

@section('endOfBody')
	@if ($commentingPost->exists)
		{!! Html::script (Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
		{!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
		{!! Html::script (Module::asset('ehdafront:libs/jquery-ui/jquery-ui.min.js')) !!}
	@endif
@append
