<div class="contact-info">


	<div class="info-wrapper col-md-6">
		@include('ehdafront::layouts.underlined_heading',[
			"text" => trans('ehdafront::general.contact_us') ,
			"color" => "blue" ,
		])
		@if ($text)
			{!! $text !!}
		@endif
		@if ($address)
			<div class="info-row">
				<h5 class="title">
					{{ trans('ehdafront::general.contact.info.address').": " }}
				</h5>
				<div class="info">
					{{ $address }}
				</div>
			</div>
		@endif

		@if ($phone)
			@php $phoneNumbers = is_array($phone) ? $phone : (array) $phone @endphp
			<div class="info-row">
				<h5 class="title">
					{{ trans('ehdafront::general.contact.info.phone').": " }}
				</h5>
				<div class="info" style="direction: ltr">
					@foreach($phoneNumbers as $number)
						@if ($loop->index)
							,
						@endif
						<a href="tel:{{ $number }}">
							{{ ad($number) }}
						</a>
					@endforeach
				</div>
			</div>
		@endif

		@if ($email)
			@php $emails = is_array($email) ? $email : (array) $email @endphp
			<div class="info-row">
				<h5 class="title">
					{{ trans('ehdafront::general.contact.info.email').": " }}
				</h5>
				<div class="info">
					@foreach($emails as $email)
						@if ($loop->index)
							,
						@endif
						<a href="mailto:{{ $email }}">
							{{ $email }}
						</a>
					@endforeach
				</div>
			</div>
		@endif

		{{--<hr>--}}
	</div>
	<div class="info-wrapper col-md-6">
		<div class="info-row social">
			@include('ehdafront::layouts.underlined_heading',[
				"text" => trans('ehdafront::general.contact.title.follow_us') ,
				"color" => "blue" ,
			])
			{{--<h5 class="title">--}}
				{{--{{ trans('ehdafront::general.contact.title.follow_us') }}--}}
			{{--</h5>--}}
			<div class="social-wrapper">
				@if($telegram)
					<span class="social-link">
                        <a href="{{ $telegram['link'] }}" target="_blank"><i class="icon icon-telegram dark"></i></a>
                    </span>
				@endif
				@if($twitter)
					<span class="social-link">
                        <a href="{{ $twitter['link'] }}" target="_blank"><i class="icon icon-twitter dark"></i></a>
                    </span>
				@endif
				@if($instagram)
					<span class="social-link">
                        <a href="{{ $instagram['link'] }}" target="_blank"><i class="icon icon-instagram dark"></i></a>
                    </span>
				@endif
				@if($aparat)
					<span class="social-link">
                        <a href="{{ $aparat['link'] }}" target="_blank"><i class="icon icon-aparat dark"></i></a>
                    </span>
				@endif
				@if($facebook)
					<span class="social-link">
                        <a href="{{ $facebook['link'] }}" target="_blank"><i class="icon icon-facebook dark"></i></a>
                    </span>
				@endif
			</div>
		</div>
	</div>
</div>
