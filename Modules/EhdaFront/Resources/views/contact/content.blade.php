<div class="container mb40 mt80">
    <div class="row">
        <div class="col-md-12 col-centered mb30">
            @include('ehdafront::contact.contact-info',[
                "address" => ($contactInfo = collect(Template::contactInfo()))->get('address') ,
                "phone" => $contactInfo->get('telephone') ,
                "email" => $contactInfo->get('email') ,
                "twitter" => ($socialLinks = collect(Template::socialLinks()))->get('twitter') ,
                "telegram" => $socialLinks->get('telegram') ,
                "instagram" => $socialLinks->get('instagram') ,
                "facebook" => $socialLinks->get('facebook') ,
                "aparat" => $socialLinks->get('aparat') ,
                "text" => $infoPost->exists ? $infoPost->text : ""
            ])
        </div>
    </div>
</div>