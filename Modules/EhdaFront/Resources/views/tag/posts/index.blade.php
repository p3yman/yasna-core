@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle(trans('ehdafront::general.tag.title.singular')) @endphp
@php Template::appendToPageTitle($tag) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop
@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title_link" => request()->url(),
		"title" => trans('ehdafront::general.tag.title.singular'),
	])
	@include('ehdafront::layouts.widgets.slide-row-box',[
		"boxes" => $specialNews ?? collect([]) ,
	])

	<div class="container">
		<div class="row">
			<div class="col-md-9 {{--col-md-pull-3--}}">
				<div class="news-list">
					@include('ehdafront::layouts.underlined_heading',[
						"color" => "green" ,
						"text" => trans('ehdafront::general.tag.search') ,
					])
					<div class="search-news">
						<div class="row">
							<div class="col-md-12">
								@include('ehdafront::layouts.widgets.search-form', [
									'disabledSearch' => true,
									'searchValue' => '# ' . $tag,
								])
							</div>
						</div>
					</div>
					<div class="list">
						@if ($posts->count())
							@include('ehdafront::posts.list.special.news.main')
						@else
							@include('ehdafront::layouts.widgets.error-box',[
								"message" => trans('ehdafront::general.no_result_found') ,
							])
						@endif
					</div>
				</div>
			</div>
			<div class="col-md-3 {{--col-md-push-9--}}">
				<div class="top-news">
					{{--@include('ehdafront::layouts.underlined_heading',[--}}
						{{--"color" => "green" ,--}}
						{{--"text" => "جدید‌ترین اخبار" ,--}}
					{{--])--}}
{{--					@include('ehdafront::news.list.special-news')--}}
				</div>
			</div>
		</div>
	</div>
@endsection