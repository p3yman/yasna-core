@extends('ehdafront::layouts.frame')

@php
	$reports = [
		[
			"year" => "۱۳۹۷" ,
			"title" => "گزارش سالانه سال ۱۳۹۷" ,
			"abstract" => "چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
			"dl_link" => "#" ,
		],
		[
			"year" => "۱۳۹۶" ,
			"title" => "گزارش سالانه سال ۱۳۹۶" ,
			"abstract" => "چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
			"dl_link" => "#" ,
		],
		[
			"year" => "۱۳۹۵" ,
			"title" => "گزارش سالانه سال ۱۳۹۵" ,
			"abstract" => "چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
			"dl_link" => "#" ,
		],
		[
			"year" => "۱۳۹۴" ,
			"title" => "گزارش سالانه سال ۱۳۹۴" ,
			"abstract" => "" ,
			"dl_link" => "#" ,
		],
	];
@endphp

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => "گزارش‌های سالانه" ,
		"title_links" => "#" ,
	])
	@include('ehdafront::annual-reports.body')
@endsection