<div class="container-fluid">
	<div class="row">
		@foreach($reports as $report)
			@include('ehdafront::annual-reports.report',[
				"year" => $report['year'] ,
				"title" => $report['title'] ,
				"abstract" => $report['abstract'] ,
				"dl_link" => $report['dl_link'] ,
			])
		@endforeach
	</div>
</div>