<div class="gallery-cat_row">
    <div class="gallery-cat_heading">
        <div class="gallery-cat_title">
            <h1>
                <a href="{{ $main_link or "#" }}">{{ $main_category or "" }}</a>
            </h1>
            @if(isset($categories) and count($categories) and $categories)
                <ul class="gallery-cat_subtitle">
                    @foreach($categories as $category)
                        <li><a href="{{ $category['link'] }}">{{ $category['title'] }}</a></li>
                    @endforeach
                </ul>
            @endif
        </div>
        <div class="gallery-cat_more">
            <a href="{{ $main_link }}" class="read-more">
                {{ trans('ehdafront::general.read_more') }}
                <span class="arrow @if(isLangRtl()) -rtl @endif"></span>
            </a>
        </div>
    </div>
    <div class="gallery-cat_body">
        <div class="gallery-cat_container js-gallery-object-slider owl-carousel">
            @foreach($row_items as $row_item)
            <div class="gallery-cat_item">
                <a href="{{ $row_item['link'] }}">
                    <div class="wrapper">
                        <div class="item_thumbnail">
                            <img src="{{ $row_item['image'] }}" alt="{{ $row_item['alt'] }}">
                        </div>
                        <div class="item_info">
                            <h2>{{ $row_item['title'] }}</h2>
                            <div class="date">{{ $row_item['date'] }}</div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>