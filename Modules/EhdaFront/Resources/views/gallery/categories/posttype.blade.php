@foreach($posttype->folders as $folder)
	@php
		$categories = $folder->children;
		$posts = PostTools::collectPosts([
			'type' => $posttype->slug,
			'category' => $categories->pluck('id')->toArray(),
			'limit' => 15,
			'pagination' => false,
			'domains' => EhdaFront::usableDomains(),
		]);
		$folderUrl = EhdaFrontRouteTools::actionLocale('PostController@gallery', [
			'type' => GalleryTools::posttypeToType($posttype->slug),
			'folder' => $folder->slug,
		])
	@endphp
	@if ($posts->count())
		@include('ehdafront::gallery.categories.gallery-row',[
			 "main_category" => $folder->titleIn(getLocale()) ,
			 "main_link" => $folderUrl ,
			 "categories" => $categories->map(function ($category) use ($folderUrl) {
			 	return [
			 		'title' => $category->titleIn(getLocale()),
			 		'link' => $folderUrl . '#' . FilterTools::encryptFilterHash([
			 			'checkbox' => [
			 				'category' => $category->slug
			 			]
			 		])
				];
			 }),
			 "row_items" => $posts->map(function ($post) {
			 	return [
					"title" => $post->title ,
					"image" => ($image = doc($post->featured_image)->resolve())->getUrl() ,
					"alt" => $image->getTitle() ,
					"link" => $post->direct_url ,
					"date" => ad(echoDate($post->published_at, 'j F Y')) ,
				];
			 }) ,
		])
	@endif
@endforeach