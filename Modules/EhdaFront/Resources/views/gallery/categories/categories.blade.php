@extends('ehdafront::gallery.general.frame.main')

@php
	$pageName = trans('ehdafront::general.visual_gallery');
	$bg_img = Module::asset('ehdafront:images/gallery.jpg');
@endphp

@php Template::appendToPageTitle($pageName) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop


@section('inner-content')
	<div class="gallery-rows mt80 mb80">
		@foreach($posttypes as $posttype)
			@php $posttype->spreadMeta() @endphp
			@if ($posttype->header_image and ($posttypeHeaderImage = doc($posttype->header_image)->getUrl()))
			    @php $bg_img = $posttypeHeaderImage @endphp
			@endif
			@include('ehdafront::gallery.categories.posttype')
		@endforeach
	</div>
@endsection

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp
@php Template::mergeWithOpenGraphs(['image' => $bg_img]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')



@section('endOfBody')
	{!! Html::script (Module::asset('ehdafront:libs/owl.carousel/owl.carousel.min.js')) !!}
	<script>
        $('document').ready(function () {
            $('.js-gallery-object-slider').owlCarousel({
                items     : 4,
                margin    : 10,
				@if(isLangRtl())
                rtl       : true,
				@endif
                nav       : true,
                navText   : @if(isLangRtl()) ['<img src="{{ Module::asset('ehdafront:images/arrow-right.png') }}" alt="arrow">', '<img src="{{ Module::asset('ehdafront:images/arrow-left.png') }}" alt="arrow">'] @else ['<img src="{{ Module::asset('ehdafront:images/arrow-left.png') }}" alt="arrow">', '<img src="{{ Module::asset('ehdafront:images/arrow-right.png') }}" alt="arrow">'] @endif,
                responsive: {
                    0  : {
                        items: 1
                    },
                    424: {
                        items: 2
                    },
                    767: {
                        items: 3
                    },
                    991: {
                        items: 4
                    }
                }
            })
        });
	</script>
@append