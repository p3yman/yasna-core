<div class="box-container result-container">
	<div class="row">
		@if ($innerHTML)
			{!! $innerHTML !!}
		@else
			@include('ehdafront::layouts.widgets.error-box',[
				"message" => trans('ehdafront::general.no_result_found') ,
			])
		@endif
	</div>
</div>