@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($posttypeTitle = $postType->titleIn(getLocale())) @endphp
@php $subTitle = [] @endphp
@if ($folder->exists)
	@php Template::appendToPageTitle($folderTitle = $folder->titleIn(getLocale())) @endphp
@endif

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop
@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	{!! $innerHTML !!}
@endsection

@include('ehdafront::gallery.list.frame.filter-scripts')