@section('endOfBody')
	{!! Html::script(Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
	{!! Html::script(Module::asset('ehdafront:js/forms.min.js')) !!}
	{!! Html::script(Module::asset('ehdafront:js/timer.min.js')) !!}
	{!! Html::script(Module::asset('ehdafront:js/ajax-filter.min.js')) !!}

	<script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
	</script>
@append