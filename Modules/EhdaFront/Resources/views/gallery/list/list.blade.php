@extends('ehdafront::layouts.frame')

@section('content')
    @component('ehdafront::layouts.filter-grid-layout')
        
        @slot('filters')
            <!-- Search Box -->
            <div class="filter_search">
                @include('ehdafront::layouts.widgets.search-form')
            </div>
            <!-- !END Search Box -->
            <!-- Checkboxes -->
            <div class="filter_checkboxes">
                <div class="checkbox-group">
                    <div class="title">
                        <h1>گرووه اول</h1>
                    </div>
                    <div class="body">
                        <form action="">
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه اول" ,
                                "value" => "test1" ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه دوم" ,
                                "value" => "test2" ,
                                "is_checked" => true ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه سوم" ,
                                "value" => "test3" ,
                                "disabled" => true ,
                            ])
                        </form>
                    </div>
                </div>
                <div class="checkbox-group">
                    <div class="title">
                        <h1>گرووه دوم</h1>
                    </div>
                    <div class="body">
                        <form action="">
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه اول" ,
                                "value" => "test1" ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه دوم" ,
                                "value" => "test2" ,
                                "is_checked" => true ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه سوم" ,
                                "value" => "test3" ,
                                "disabled" => true ,
                            ])
                        </form>
                    </div>
                </div>
            </div>
            <!-- !END Checkboxes -->
        @endslot
        
        @slot('header_filters')
            <div class="col-xs-6">
                @include('ehdafront::forms.select',[
                    "id" => "selecttest1" ,
                    "options" => [
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر اول" ,
                        ],
                    ] ,
                ])
            </div>
            <div class="col-xs-6">
                @include('ehdafront::forms.select',[
                    "div_class" => "pull-end" ,
                    "id" => "selecttest1" ,
                    "options" => [
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر اول" ,
                        ],
                    ] ,
                ])
            </div>
        @endslot
        
        @slot('content')
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
    
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
    
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
    
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
    
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.image-box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "caption" => "اولین‌بار بود که انجمن اهدای عضو ایرانیان همراه با سفیران و همراهان همیشگی‌اش آمده بود." ,
                    "link" =>"#",
                ])
            </div>
        @endslot
    @endcomponent
@endsection