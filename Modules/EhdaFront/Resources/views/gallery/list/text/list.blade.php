@extends('ehdafront::layouts.frame')

@php
    $newsList = [
        [
            "title" => "اهداکنندگان زندگی میزبان انجمن اهدای عضو ایرانیان" ,
            "date" => "۲۰ مهر" ,
            "author" => "خانوم نگار" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "alt" => "event" ,
        ],
        [
            "title" => "فونت های اهدای عضو" ,
            "date" => "۲۰ مهر" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img4.jpg') ,
            "alt" => "event" ,
        ],
        [
            "title" => "۳۶۴ پیوند عضو در خراسان رضوی طی یک سال" ,
            "date" => "۲۵ اردیبهشت" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img3.jpg') ,
            "alt" => "event" ,
        ],
        [
            "title" => "اهداکنندگان زندگی میزبان انجمن اهدای عضو ایرانیان" ,
            "date" => "۲۰ مهر" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "alt" => "event" ,
        ],
        [
            "title" => "فونت های اهدای عضو" ,
            "date" => "۲۰ مهر" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img4.jpg') ,
            "alt" => "event" ,
        ],
        [
            "title" => "۳۶۴ پیوند عضو در خراسان رضوی طی یک سال" ,
            "date" => "۲۵ اردیبهشت" ,
            "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است." ,
            "link" => "#" ,
            "src" => Module::asset('ehdafront:images/demo/events/img3.jpg') ,
            "alt" => "event" ,
        ],
    ];
@endphp

@section('content')
    @include('ehdafront::layouts.widgets.title-with-image',[
        "title" => "نوشتاری" ,
    ])
    @component('ehdafront::layouts.filter-grid-layout')
        
        @slot('filters')
            <!-- Search Box -->
            <div class="filter_search">
                @include('ehdafront::layouts.widgets.search-form')
            </div>
            <!-- !END Search Box -->
            <!-- Checkboxes -->
            <div class="filter_checkboxes">
                <div class="checkbox-group">
                    <div class="title">
                        <h1>گرووه اول</h1>
                    </div>
                    <div class="body">
                        <form action="">
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه اول" ,
                                "value" => "test1" ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه دوم" ,
                                "value" => "test2" ,
                                "is_checked" => true ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه سوم" ,
                                "value" => "test3" ,
                                "disabled" => true ,
                            ])
                        </form>
                    </div>
                </div>
                <div class="checkbox-group">
                    <div class="title">
                        <h1>گرووه دوم</h1>
                    </div>
                    <div class="body">
                        <form action="">
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه اول" ,
                                "value" => "test1" ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه دوم" ,
                                "value" => "test2" ,
                                "is_checked" => true ,
                            ])
                            @include('ehdafront::forms.new-form.checkbox',[
                                "title" => "گزینه سوم" ,
                                "value" => "test3" ,
                                "disabled" => true ,
                            ])
                        </form>
                    </div>
                </div>
            </div>
            <!-- !END Checkboxes -->
        @endslot
        
        @slot('header_filters')
            <div class="col-xs-6">
                @include('ehdafront::forms.select',[
                    "id" => "selecttest1" ,
                    "options" => [
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر اول" ,
                        ],
                    ] ,
                ])
            </div>
            <div class="col-xs-6">
                @include('ehdafront::forms.select',[
                    "div_class" => "pull-end" ,
                    "id" => "selecttest1" ,
                    "options" => [
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر تست اول" ,
                        ],
                        [
                            "value_field" => "test1" ,
                            "id" => "test1" ,
                            "title" => "فیلتر اول" ,
                        ],
                    ] ,
                ])
            </div>
        @endslot
        
        @slot('content')
            @foreach($newsList as $newsItem)
                <div class="col-sm-6 col-xs-6 col-xxs">
                    @include('ehdafront::layouts.widgets.row-box',[
                     "title" => $newsItem['title'] ,
                     "caption" => $newsItem['caption'] ,
                     "author" => (isset($newsItem['author']))? $newsItem['author'] : false ,
                     "date" => $newsItem['date'] ,
                     "src" => $newsItem['src'] ,
                     "alt" => $newsItem['alt'] ,
                     "link" => $newsItem['link'] ,
                 ])
                </div>
            @endforeach
        @endslot
    @endcomponent
@endsection