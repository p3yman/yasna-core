@extends('ehdafront::layouts.frame')

@php
    // Test data
    $episodes =[
        [
            "title"=>"  قسمت بعدی فیلمی که الان می‌بینید.",
            "visits"=>"1200 بازدید",
        ],
        [
            "title"=>"  قسمت بعدی فیلمی که الان می‌بینید.",
            "visits"=>"1200 بازدید",
        ],
        [
            "title"=>"  قسمت بعدی فیلمی که الان می‌بینید.",
            "visits"=>"1200 بازدید",
        ],
        [
            "title"=>"  قسمت بعدی فیلمی که الان می‌بینید.",
            "visits"=>"1200 بازدید",
        ],
        [
            "title"=>"  قسمت بعدی فیلمی که الان می‌بینید.",
            "visits"=>"1200 بازدید",
        ],
        [
            "title"=>"  قسمت بعدی فیلمی که الان می‌بینید.",
            "visits"=>"1200 بازدید",
        ],
    ];
@endphp

@section('content')
    <div class="video-wrapper mb60">
        <div class="col-lg-11 col-centered pt30">
            <button class="btn btn-link text-gray theme-mode js-themeToggle">
                <span class="day mode">
                    <i class="fa fa-sun-o"></i>
                    {{ trans('ehdafront::general.theme.day') }}
                </span>
                <span class="night mode noDisplay">
                    <i class="fa fa-moon-o"></i>
                    {{ trans('ehdafront::general.theme.night') }}
                </span>
            </button>
            <div class="row">
                <div class="col-md-8 @if(!isset($episodes) or count($episodes) < 1) col-centered @endif ">
                    @include('ehdafront::gallery.single.video.video-player',[
                        "title" => "ویدئوی اول تست" , 
                        "caption" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است" ,
                        "views" => "1500" , 
                        "comments" => "300" ,
                    ])
                    
                </div>
                @if(isset($episodes) and count($episodes) > 1)
                <div class="col-md-4">
                    <div class="video-playlist">
                        <div class="episodes">
                            @foreach($episodes as $episode)
                                @include('ehdafront::gallery.single.video.video-episode-row',[
                                    "title"=> $episode['title'],
                                    "visits"=> $episode['visits'],
                                ])
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="container">
        @include('ehdafront::layouts.underlined_heading', [
                        'text' => trans('ehdafront::posts.features.dont_miss'),
                        'color' => 'green',
                    ])

        <div class="row">
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>

            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>

            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>

            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
        </div>
    </div>

@endsection