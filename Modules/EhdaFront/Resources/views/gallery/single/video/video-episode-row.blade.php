@php
    // Images should be 150 * 85
    $img = (isset($img))? $img : Module::asset('ehdafront:images/img-placeholder.png');
@endphp

<div class="video-row">
    <a href="{{ $link or "#" }}">
        <div class="video-thumb">
            <img src="{{ $img }}" alt="{{ $alt or "" }}">
        </div>
        <div class="video-info">
            <div class="title">
                {{ $title }}
            </div>
            @isset($visits)
                <div class="visits">
                    {{ $visits }}
                </div>
            @endisset
        </div>
    </a>
</div>