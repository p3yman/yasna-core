<div class="player-wrapper">
    <img src="{{ Module::asset('ehdafront:images/video-placeholder.png') }}" alt="video"
         class="img-responsive">
</div>

<!-- Video Info -->
<div class="video-info">
    <h1 class="title">{{ $title }}</h1>
    <div class="caption">
        {{ $caption }}
    </div>
    @if(isset($views) or isset($comments))
    <div class="statics">
        @isset($views)
        <span class="statics_views">
            <i class="fa fa-eye"></i>
            {{ ad($views) }}
        </span>
        @endisset
        @isset($comments)
        <span class="statics_comments">
            <i class="fa fa-comments"></i>
            {{ ad($comments) }}
        </span>
        @endisset
    </div>
    @endif
    <hr>
    <div class="row">
        <div class="col-sm-6">
            <div class="tags">
                <span class="text">
                    {{ trans('ehdafront::posts.features.tags').":" }}
                </span>
                <a class="btn btn-sm btn-info f12 mb5"
                   href="https://ehda.center/fa/archive/films/clips">کلیپ‌ها</a>
                <a class="btn btn-sm btn-danger f12 mb5"
                   href="https://ehda.center/fa/archive/films/clips">کلیپ‌ها</a>
            </div>
        </div>
        <div class="col-sm-6">
            @include('ehdafront::layouts.widgets.add-to-any',[
                "shortUrl" => "https://ehda.center/PNpQkW" ,
            ])
        </div>
    </div>
</div>
<!-- !END Video Info -->