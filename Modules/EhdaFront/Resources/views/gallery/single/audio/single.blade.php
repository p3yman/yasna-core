@extends('ehdafront::layouts.frame')

@if(isset($pageTitle))
    @php $pageTitle = setting()->ask('site_title')->gain() . ' | ' . $pageTitle @endphp
@else
    @php $pageTitle = setting()->ask('site_title')->gain()  @endphp
@endif
@section('head')
    <title>{{ $pageTitle }}</title>
@append

@php

    $title = "فایل صوتی فلان";
    $caption = "صدای فلانی که مثلا یه موضوع خیلی جالبی رو تعریف کرده.";
    $download = [
        "link" => "#" ,
        "size" => "40 MB" ,
    ];

    $cover = Module::asset('ehdafront:images/demo/events/img3.jpg');

@endphp


@section('content')
    <div class="container-fluid bg-gradient mt50">
        <div class="container">
            <div class="row">
                @include('ehdafront::gallery.single.audio.audio-player')
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mb40">
            <div class="audio-detail">
                <div class="album-art">
                    <img src="{{ $cover }}" alt="cover">
                </div>
                <div class="audio-info">
                    <h1>{{ $title }}</h1>
                    <div class="caption">
                        {{ $caption }}
                    </div>
                    <span class="download">
                        <span class="download-size">
                            {{ $download['size'] }}
                        </span>
                        <a href="{{ $download['link'] }}" class="btn btn-link text-green">
                            <i class="fa fa-download"></i>
                            {{ trans('ehdafront::general.download') }}
                        </a>
                    </span>
                </div>
            </div>
        </div>
        <hr>
        @include('ehdafront::layouts.underlined_heading', [
                       'text' => trans('ehdafront::posts.features.dont_miss'),
                       'color' => 'green',
                   ])
    
        <div class="row">
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.image-box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "caption" => false ,
                    "link" => "#",
                    "image" => Module::asset('ehdafront:images/audio-placeholder.png') ,
                ])
            </div>
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.image-box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "caption" => false ,
                    "link" => "#",
                    "image" => Module::asset('ehdafront:images/audio-placeholder.png') ,
                ])
            </div>
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.image-box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "caption" => false ,
                    "link" => "#",
                    "image" => Module::asset('ehdafront:images/audio-placeholder.png') ,
                ])
            </div>
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.image-box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "caption" => false ,
                    "link" => "#",
                    "image" => Module::asset('ehdafront:images/audio-placeholder.png') ,
                ])
            </div>
            
        </div>
    </div>
@endsection

