@extends('ehdafront::layouts.frame')

@php
    $pageName = "گالری تصاویر";
    $sub_title = "جشن نفس ۹۶";
    $gallery_title = "جشن نفس در بوشهر";
    //$bg_img = Module::asset('ehdafront:images/gallery.jpg');
    $image_set = "example1";
    $images = [
        [
            "thumb" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "original" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
       [
            "thumb" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "original" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "original" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "original" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "original" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "original" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "original" => Module::asset('ehdafront:images/img-placeholder.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "original" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "original" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "original" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "original" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "original" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        [
            "thumb" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "original" => Module::asset('ehdafront:images/demo/events/img1.png') ,
            "title" => "کپشن الکی زیر عکس برای نمایش" ,
            "alt" => "image" ,
        ],
        
    ];
@endphp

@section('content')
    @include('ehdafront::layouts.widgets.title-with-image',[
        "title" => $pageName,
        //"bg_img" => $bg_img
    ])
    {{--<div class="container">--}}
        {{--@yield('inner-content')--}}
    {{--</div>--}}
    <div class="col-md-12">
        @include('ehdafront::posts.single.special.gallery-image.gallery-images',[
                "gallery_title" => $gallery_title ,
                "images" => $images ,
                "image_set" => $image_set ,
            ])
        @include('ehdafront::layouts.widgets.add-to-any',[
                "shortUrl" => "https://ehda.center/PNpQkW" ,
            ])
    </div>
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{----}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container">
        @include('ehdafront::layouts.underlined_heading', [
                       'text' => trans('ehdafront::posts.features.dont_miss'),
                       'color' => 'green',
                   ])
    
        <div class="row">
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
        
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
        
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
        
            <div class="col-xxs col-xs-6 col-md-4 col-lg-3">
                @include('ehdafront::layouts.widgets.box',[
                    "title" => "باکس تست برای فیلتر" ,
                    "link" => [
                        "link" => "#" ,
                        "title" => "+ بیشتر" ,
                    ] ,
                ])
            </div>
        </div>
    </div>
    
@endsection

@section('head')
    {!! Html::style(Module::asset('ehdafront:libs/lightbox/dist/css/lightbox.min.css')) !!}
@append

@section('endOfBody')
    {!! Html::script(Module::asset('ehdafront:libs/shufflejs/shufflejs.js')) !!}
    {!! Html::script(Module::asset('ehdafront:libs/lightbox/dist/js/lightbox.min.js')) !!}
    
    <script>
        jQuery(function ($) {

            var Shuffle = window.Shuffle,
                filterVal = "";


            var myShuffle = new Shuffle(document.querySelector('.my-shuffle'), {
                itemSelector: '.js-item',
                sizer: '.my-sizer-element',
                buffer: 1,
            });

            lightbox.option({
                albumLabel: "",
            })

//            $('.js-filter').on('click',function () {
//                var $this = $(this);
//
//                $('.filters .active').removeClass('active');
//                $this.addClass('active');
//
//                filterVal = $this.attr("data-filter-name");
//                if(filterVal === "all"){
//                    filterVal = "";
//                }
//
//                galleryFiltering(myShuffle,filterVal);
//
//            });


        }); //End Of siaf!
    
    </script>

@append