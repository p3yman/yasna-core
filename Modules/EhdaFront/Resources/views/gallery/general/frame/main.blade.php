@extends('ehdafront::layouts.frame')

@if(isset($pageTitle))
    @php $pageTitle = setting()->ask('site_title')->gain() . ' | ' . $pageTitle @endphp
@else
    @php $pageTitle = setting()->ask('site_title')->gain()  @endphp
@endif
@section('head')
    <title>{{ $pageTitle }}</title>
@append


@section('content')
    @include('ehdafront::layouts.widgets.title-with-image',[
        "title" => $pageName,
        "bg_img" => $bg_img
    ])
    <div class="container">
        @yield('inner-content')
    </div>
@endsection
