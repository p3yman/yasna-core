@include('ehdafront::layouts.underlined_heading',[
					"text" => trans('ehdafront::general.volunteer_section.activities') ,
					"color" => "green" ,
				])

<div class="list">
	@foreach($activities as $activity)
		@include('ehdafront::layouts.widgets.row-box',[
			"link" => $activity['link'] ,
			"src" => $activity['src'] ,
			"alt" => $activity['alt'] ,
			"title" => $activity['title'] ,
			"date" => $activity['date'] ,
			"caption" => $activity['caption'] ,
		])
	@endforeach
</div>