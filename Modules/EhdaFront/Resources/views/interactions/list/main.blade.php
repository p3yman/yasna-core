@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = $postType->titleIn(getLocale())) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')


@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title_link" => request()->url(),
		"title" => $title,
		"bg_img" => ($cover_hashid = $postType->spreadMeta()->list_cover_image)
			? fileManager()->file($cover_hashid)->getUrl()
			: null,
		"blur" => $postType->spreadMeta()->list_cover_blur,
	])

	@include('ehdafront::interactions.list.list')
@append