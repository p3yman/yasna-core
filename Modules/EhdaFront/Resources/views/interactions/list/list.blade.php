<div class="container">
	@if ($innerHTML)
		{!! $innerHTML !!}
	@else
		@include('ehdafront::layouts.widgets.error-box',[
			"message" => trans('ehdafront::general.no_result_found') ,
		])
	@endif
</div>