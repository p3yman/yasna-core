@extends('ehdafront::test.pasargad.layout.container')

@section('content')

	<div class="container-inner">
		<div class="inquiry-box">
			<h4>استعلام کارت اهدای عضو</h4>
			<form action="">
				<label for="inputCodemelli" class="sr-only">{{ trans('ehdafront::validation.attributes.codemelli') }}</label>
				<input type="text" id="inputCodemelli" class="form-control mb5"
					   placeholder="{{ trans('ehdafront::validation.attributes.code_melli') }}"
					   required="" autofocus="">
				<button class="btn btn-lg btn-primary btn-block" type="submit">
					{{ trans('ehdafront::forms.button.Inquiry') }}
				</button>
			</form>
			{{--<div class="alert alert-danger">--}}
				{{--کد ملی نادرست است.--}}
			{{--</div>--}}
			{{--<div class="alert alert-success">--}}
				{{--کارت اهدای عضو برای این کدملی ثبت شده است.--}}
			{{--</div>--}}

			@include('ehdafront::test.pasargad.result',[
				"type" => "success" ,
				"title" => "کد ملی ثبت شده است" ,
				"message" => "
					<p>شماره عضویت: 22544775588</p>
				" ,
			])

		</div>
	</div>

@endsection