@extends('ehdafront::test.pasargad.layout.frame')

@section('body')

	<div class="full-screen">
		<div class="credit">
			طراحی و اجرا توسط
			<a href="http://yasna.team" target="_blank">یسنا‌تیم</a>
		</div>
		<a href="#" class="btn btn-danger" id="logout"> {{-- @TODO: hide it in login page --}}
			<i class="fa fa-sign-out"></i>
		</a>
		<div class="dual-logo-header">
			<div class="logo-container">
				<div class="pasargad">
					<img src="{{ Module::asset('ehdafront:images/Bank_Pasargad_logo.png') }}" alt="logo Pasargad Bank">
				</div>
				<div class="ehda">
					<img src="{{ Module::asset('ehdafront:images/template/header-logo-fa.png') }}" alt="logo Ehda">
				</div>
			</div>
		</div>
		@yield('content')
	</div>

@endsection