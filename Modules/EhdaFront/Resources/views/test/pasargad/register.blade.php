@extends('ehdafront::test.pasargad.layout.container')

@section('content')

	<div class="container-inner">
		<div class="register-box">
			<h4>ثبت‌ نام کارت اهدای عضو</h4>
			<form action="">
				<!-- First Name -->
				@include('ehdafront::forms.input', [
					'name' => 'name_first',
					'class' => 'form-persian form-required',
					'dataAttributes' => [
						'toggle' => 'tooltip',
						'placement' => 'top',
					],
					'otherAttributes' => [
						'title' => trans('ehdafront::validation.attributes_example.name_first'),
						'minlength' => 2,
					]
				])
				<!-- !END First Name -->

				<!-- Last Name -->
				@include('ehdafront::forms.input', [
					'name' => 'name_last',
					'class' => 'form-persian form-required',
					'dataAttributes' => [
						'toggle' => 'tooltip',
						'placement' => 'top',
					],
					'otherAttributes' => [
						'title' => trans('ehdafront::validation.attributes_example.name_last'),
						'minlength' => 2,
					]
				])
				<!-- !END Last Name -->

				<!-- Father Name -->
				@include('ehdafront::forms.input', [
					'name' => 'name_father',
					'class' => 'form-persian form-required',
					'dataAttributes' => [
						'toggle' => 'tooltip',
						'placement' => 'top',
					],
					'otherAttributes' => [
						'title' => trans('ehdafront::validation.attributes_example.name_father'),
					]
				])
				<!-- !END Father Name -->

				@include('ehdafront::forms._birthdate_3_selects', [
					'class' => 'form-required',
					'dataAttributes' => [
						'toggle' => 'tooltip',
						'placement' => 'top',
					],
					'otherAttributes' => [
						'title' => trans('ehdafront::validation.attributes_example.birth_date'),
					],
				])

				{{--@include('ehdafront::forms._states-selectpicker', [--}}
				   {{--'name' => 'birth_city' ,--}}
				   {{--'required' => 1,--}}
				   {{--'class' => 'form-required',--}}
			   {{--])--}}

				@include('ehdafront::forms.input', [
					'name' => 'mobile',
					'class' => 'form-mobile form-required',
					'dataAttributes' => [
						'toggle' => 'tooltip',
						'placement' => 'top',
					],
					'otherAttributes' => [
						'title' => trans('ehdafront::validation.attributes_example.mobile'),
						'minlength' => 11,
						'maxlength' => 11,
					]
				])

				<!-- Gender -->
				@include('ehdafront::forms._select-gender', [
					'class' => 'form-select form-required',
					'required' => 1,
				])
				<!-- !END Gender -->

				<label for="inputCodemelli" class="sr-only"
					   >{{ trans('ehdafront::validation.attributes.code_melli') }}</label>
				<input type="text" id="inputCodemelli" class="form-control hidden mb5"
					   placeholder="{{ trans('ehdafront::validation.attributes.code_melli') }}"
					   required="">
				<button class="btn btn-lg btn-primary btn-block" type="submit">
					{{ trans('ehdafront::forms.button.send_and_save') }}
				</button>
			</form>
			{{--<div class="alert alert-danger">--}}
			{{--کد ملی نادرست است.--}}
			{{--</div>--}}
			{{--<div class="alert alert-success">--}}
			{{--کارت اهدای عضو برای این کدملی ثبت شده است.--}}
			{{--</div>--}}
			@include('ehdafront::test.pasargad.result',[
				"type" => "success" ,
				"title" => "کد ملی ثبت شده است" ,
				"message" => "
					<p>شماره عضویت: 22544775588</p>
				" ,
			])
		</div>
	</div>

@endsection

@section('script')
	<script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
	</script>
@endsection