@php( $class = (isset($type)) ? $type : "default" )

<div class="panel panel-{{ $class }}">
	<div class="panel-heading">
		{{ $title }}
	</div>
	<div class="panel-body">
		{!! $message !!}
	</div>
</div>