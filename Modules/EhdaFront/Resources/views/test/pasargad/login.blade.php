@extends('ehdafront::test.pasargad.layout.container')

@section('content')

	<div class="container-inner">
		<div class="login-box">
			<h3>ورود به پنل</h3>
			<form action="">
				<div class="form-group">
					<label for="inputUsername" class="sr-only">{{ trans('ehdafront::validation.attributes.username') }}</label>
					<input type="email" id="inputUsername" class="form-control mb5" placeholder="{{ trans('ehdafront::validation.attributes.username') }}" required="" autofocus="">
					<label for="inputPassword" class="sr-only">{{ trans('ehdafront::validation.attributes.password') }}</label>
					<input type="password" id="inputPassword" class="form-control mb5" placeholder="{{ trans('ehdafront::validation.attributes.password') }}" required="">
					<button class="btn btn-lg btn-primary btn-block" type="submit">
						{{ trans('ehdafront::forms.button.login') }}
					</button>
				</div>
			</form>
		</div>
	</div>

@endsection