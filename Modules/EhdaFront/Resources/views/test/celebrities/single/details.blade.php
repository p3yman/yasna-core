<div class="row">
	<div class="col-sm-5">
		@include('ehdafront::test.celebrities.single.profile-pic',[
			"pic" => $celebrity['pic'] ,
			"alt" => $celebrity['alt'] ,
			"medal" => $celebrity['medal'] ,
		])
	</div>
	<div class="col-sm-7">
		@include('ehdafront::test.celebrities.single.profile-info')
	</div>
</div>