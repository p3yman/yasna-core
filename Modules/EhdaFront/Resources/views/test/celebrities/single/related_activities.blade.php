<hr>

<div class="row">
	<div class="col-sm-12">
		<div class="celebrity-info-title">
			{{ trans('ehdafront::general.volunteer_section.attributes.related_activities') }}
		</div>
		<br>
		{!! $celebrity['related_activities'] !!}
	</div>
</div>