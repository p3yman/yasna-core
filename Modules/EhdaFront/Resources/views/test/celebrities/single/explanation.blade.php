<hr>

<div class="row">
	<div class="col-sm-12">
		<div class="celebrity-info-title">
			{{ trans('ehdafront::general.volunteer_section.attributes.explanation') }}
		</div>
		<br>
		{!! $celebrity['explanation'] !!}
	</div>
</div>