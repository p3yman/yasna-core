<div class="celebrity-info">
	@isset($celebrity['first_name'])
		<div class="mb10">
			<span class="celebrity-info-title">
				{{ trans('ehdafront::general.volunteer_section.attributes.first_name').':' }}
			</span>
			<span class="celebrity-info-data">
				{{ $celebrity['first_name'] }}
			</span>
		</div>
	@endisset

	@isset($celebrity['last_name'])
		<div class="mb10">
		<span class="celebrity-info-title">
			{{ trans('ehdafront::general.volunteer_section.attributes.last_name').':' }}
		</span>
			<span class="celebrity-info-data">
			{{ $celebrity['last_name'] }}
		</span>
		</div>
	@endisset

	@isset($celebrity['birthday'])
		<div class="mb10">
		<span class="celebrity-info-title">
			{{ trans('ehdafront::general.volunteer_section.attributes.birth_date').':' }}
		</span>
			<span class="celebrity-info-data">
			{{ $celebrity['birthday'] }}
		</span>
		</div>
	@endisset

	@isset($celebrity['hometown'])
		<div class="mb10">
		<span class="celebrity-info-title">
			{{ trans('ehdafront::general.volunteer_section.attributes.hometown').':' }}
		</span>
			<span class="celebrity-info-data">
			{{ $celebrity['hometown'] }}
		</span>
		</div>
	@endisset

	@isset($celebrity['occupation'])
		<div class="mb10">
		<span class="celebrity-info-title">
			{{ trans('ehdafront::general.volunteer_section.attributes.occupation').':' }}
		</span>
			<span class="celebrity-info-data">
			{{ $celebrity['occupation'] }}
		</span>
		</div>
	@endisset

</div>
