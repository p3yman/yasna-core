@php
	$posts=[
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/1.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/2.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/3.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/4.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/5.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/6.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/1.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/2.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/3.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/4.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/5.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/6.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/1.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/2.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/3.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/4.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => false ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/5.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
		[
			"url" => "#" ,
			"has_medal" => true ,
		   "src" => Module::asset('ehdafront:images/demo/celebrities/6.jpg') ,
		   "title" => "آدم خیلی معروف" ,
		],
	];
@endphp

<div class="celebrities">
	<div class="yt-gallery">
		<ul id="waterfall" class="waterfall">
			@foreach($posts as $post)
				@include('ehdafront::test.celebrities.list.yt-gallery.item',[
					"url" => $post['url'] ,
					"has_medal" => $post['has_medal'] ,
					"src" => $post['src'] ,
					"title" => $post['title'] ,
				])
			@endforeach
		</ul>
	</div>
</div>

@include('ehdafront::test.celebrities.list.yt-gallery.scripts')