<li class="yt-gallery-item">
    <div class="yt-gallery-item-container">
        <a href="{{ $url }}">
            @php $thumbnailClass = $has_medal ? 'text-gold' : 'text-silver' @endphp
            <img class="yt-gallery-item-img lazy"
                 data-original="{{ $src }}"
                 alt="">

            <div class="yt-gallery-item-detail">
                <div class="yt-gallery-item-inner">
                    <span class="celeb-medal {{ $thumbnailClass }}">
                        <i class="fa fa-trophy"></i>
                    </span>
                    <div class="celeb-name">
                        {{ $title }}
                    </div>
                </div>
            </div>
        </a>
    </div>
</li>