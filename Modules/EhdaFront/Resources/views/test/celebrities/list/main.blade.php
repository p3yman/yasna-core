@extends('ehdafront::layouts.frame')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => trans('ehdafront::general.volunteer_section.special.plural')  ,
	])

	<div class="container celebrities mt50 mb50">
		@include('ehdafront::test.celebrities.list.yt-gallery.body')
	</div>

@endsection