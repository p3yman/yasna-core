<div class="activities-tree">
	@foreach($activity_types as $activity)
		@include('ehdafront::test.activities.activity-box',[
			"title" => $activity['title'] ,
			"link" => $activity['link'] ,
		])
	@endforeach
</div>