@php
	$activity_types = [
		[
			"title" => "فرهنگی" ,
			"link" => "#" ,
		],
		[
			"title" => "آموزشی" ,
			"link" => "#" ,
		],
		[
			"title" => "پژوهشی" ,
			"link" => "#" ,
		],
		[
			"title" => "مددکاری" ,
			"link" => "#" ,
		],
	];

	$activities =[
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
	];
@endphp


<div class="activities">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-sm-5">
				@include('ehdafront::test.activities.tree-view')
			</div>
			<div class="col-md-8 col-sm-7">
				<div class="activity-list">
					@include('ehdafront::layouts.underlined_heading',[
						"text" => "فعالیت‌های فرهنگی" , 
						"color" => "green" , 
					])
					<div class="list">
						@include('ehdafront::layouts.widgets.search-form')
						@foreach($activities as $activity)
							@include('ehdafront::layouts.widgets.row-box',[
								"link" => $activity['link'] , 
								"src" => $activity['src'] , 
								"alt" => $activity['alt'] , 
								"title" => $activity['title'] , 
								"caption" => $activity['caption'] ,
							])
						@endforeach	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
