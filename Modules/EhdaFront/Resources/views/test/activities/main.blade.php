@extends('ehdafront::layouts.frame')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => "فعالیت‌ها" ,
	])

	@include('ehdafront::test.activities.body')
@endsection