<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-push-4">
			<div class="page-content mt40">
				{!! $content !!}
			</div>
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			@include('ehdafront::test.general-information.list',[
				"lists" => [
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "current" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],
					[
						"title" => "تاریخچه اهدا و پیوند" ,
						"link" => "#" ,
						"class" => "" ,
					],

				] ,
			])
		</div>
	</div>
</div>