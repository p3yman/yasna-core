<div style="margin-top: -20px;" id="category_boxes">
	<div class="col-sm-4">
		@include('ehdafront::test.education.category-box',[
			"icon" => "film" ,
			"title" => "ویديو" ,
			"active" => true ,
		])
	</div>
	<div class="col-sm-4">
		@include('ehdafront::test.education.category-box',[
			"icon" => "file-powerpoint-o" ,
			"title" => "اسلاید" ,
		])
	</div>
	<div class="col-sm-4">
		@include('ehdafront::test.education.category-box',[
			"icon" => "file-text-o" ,
			"title" => "کتاب و مقاله" ,
		])
	</div>
</div>

@section('endOfBody')
	<script>
		$(document).ready(function () {
		    var boxes = $('#category_boxes .category-box');

            boxes.on('click', function () {
				boxes.removeClass('active');
				$(this).addClass('active');
            })
        })
	</script>
@append