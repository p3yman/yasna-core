<div class="abstract">
	<div class="container">
		<h1>{{ $title }}</h1>
		<div class="content">
			{!! $abstract !!}
		</div>
		@include('ehdafront::test.donation.call-to-action',[
			"onclick" => "$('#donationSection').scrollToView(-100,1000)" ,
		])
	</div>
</div>