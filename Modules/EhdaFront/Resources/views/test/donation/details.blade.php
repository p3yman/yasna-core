<div class="support-details">
	<div class="container">
		<div class="description">
			{!! $description !!}
		</div>
		@include('ehdafront::test.donation.call-to-action',[
			"onclick" => "$('#donationSection').scrollToView(-100,1000)" ,
		])
	</div>
</div>