<div class="donation" id="donationSection">
	<div class="container">
		<h1>{{ trans('ehdafront::general.financial_support.donation.title') }}</h1>
		<div class="row">
			<div class="col-md-6">
				@include('ehdafront::layouts.underlined_heading',[
					"text" => trans('ehdafront::general.financial_support.donation.type.offline') ,
					"color" => "green" ,
				])
				@include('ehdafront::test.donation.bank-accounts',[
					"accounts" => [
						[
							"id" => "cardAccount" ,
							"title" => "شماره کارت" ,
							"data" => "5022-2970-0000-5566" ,
						],
						[
							"id" => "bankAccount" ,
							"title" => "شماره حساب کوتاه مدت" ,
							"data" => "2498100125701741" ,
						],
						[
							"id" => "shabaAccount" ,
							"title" => "شماره شبا" ,
							"data" => "IR 630570024911012570174001" ,
						],
					] ,
				])
			</div>
			<div class="col-md-6">
				@include('ehdafront::layouts.underlined_heading',[
					"text" => trans('ehdafront::general.financial_support.donation.type.online') ,
					"color" => "green" ,
				])
				<div class="form-wrapper">
					@include('ehdafront::test.donation.form')
				</div>
			</div>
		</div>

	</div>
</div>