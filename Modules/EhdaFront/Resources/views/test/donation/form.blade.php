{!!
    widget('FormOpen')
    ->class('donation-from')
 !!}

{!!
    widget('input')
    ->label('نام و نام خانوادگی')
 !!}
{!!
    widget('input')
    ->label('شماره تماس')
 !!}
{!!
    widget('input')
    ->label('کد ملی')
 !!}

<div id="widget-input-29267387-container" class="">
	<label for="_widget-input-29267387" class="control-label text-gray " style="margin-top:10px;">{{ "نوع کمک"."..." }}</label>

	<select class="form-control" id="widget-input-29267387">
		<option>
			{{ "نوع اول" }}
		</option>
		<option>
			{{ "نوع دوم" }}
		</option>
	</select>

</div>



{!!
    widget('input')
    ->label('مبلغ')
 !!}


{!!
    widget('button')
    ->type('submit')
	->label('انتقال به درگاه')
	->class('btn-green mt25')
 !!}

{!!
    widget('FormClose')

 !!}