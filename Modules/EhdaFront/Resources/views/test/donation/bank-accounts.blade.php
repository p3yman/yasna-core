<div class="account-info">
	{{--<h2>{{ trans('ehdafront::general.financial_support.account_info') }}</h2>--}}
	<div class="account-data">
		@foreach($accounts as $account)
			<div class="mt5 mb10" id="{{ $account['id'] }}">
				<div class="title">
					{{ $account['title'].":" }}
				</div>
				<div class="data">
					{{ pd($account['data']) }}
					<span class="copy-icon js-copy"
						  data-toggle="tooltip"
						  title="{{ trans('ehdafront::general.financial_support.donation.accounts.copy') }}"
						  data-clipboard-text="{{ ed(str_replace("-","",$account['data'])) }}"
					>
						<i class="fa fa-files-o"></i>
					</span>
				</div>
			</div>
		@endforeach
	</div>
</div>