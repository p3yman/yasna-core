@php

    $title =  trans("ehdafront::general.contact.form.field.$id.title");
    $placeholder =  trans("ehdafront::general.contact.form.field.$id.placeholder");

@endphp

<div class="form-row">
    <label for="{{ $id }}">{{ $title }}</label>
    @if($id=== "message")
        <textarea id="{{ $id }}" class="form-control {{ $class or "" }}" placeholder="{{ $placeholder or $title }}"></textarea>
    @else
        <input type="text" id="{{ $id }}" class="form-control {{ $class or "" }}" placeholder="{{ $placeholder or $title }}">
    @endif
</div>