<div class="contact-info">
    @include('ehdafront::layouts.underlined_heading',[
                "text" => trans('ehdafront::general.contact_us') ,
                "color" => "blue" ,
          ])
    <div class="info-wrapper">
        <div class="info-row">
            <h5 class="title">
                {{ trans('ehdafront::general.contact.info.address').": " }}
            </h5>
            <div class="info">
                {{ $address }}
            </div>
        </div>
        <div class="info-row">
            <h5 class="title">
                {{ trans('ehdafront::general.contact.info.phone').": " }}
            </h5>
            <div class="info">
                <a href="tel:{{ $phone }}">
                    {{ $phone }}
                </a>
            </div>
        </div>
        <div class="info-row">
            <h5 class="title">
                {{ trans('ehdafront::general.contact.info.email').": " }}
            </h5>
            <div class="info">
                <a href="mailto:{{ $email }}">
                    {{ $email }}
                </a>
            </div>
        </div>
        <hr>
        <div class="info-row social">
            <h5 class="title">
                {{ trans('ehdafront::general.contact.title.follow_us') }}
            </h5>
            <div class="social-wrapper">
                @if($telegram)
                    <span class="social-link">
                        <a href="{{ $telegram }}" target="_blank"><i class="icon icon-telegram dark"></i></a>
                    </span>
                @endif
                @if($twitter)
                    <span class="social-link">
                        <a href="{{ $twitter }}" target="_blank"><i class="icon icon-twitter dark"></i></a>
                    </span>
                @endif
                @if($instagram)
                    <span class="social-link">
                        <a href="{{ $instagram }}" target="_blank"><i class="icon icon-instagram dark"></i></a>
                    </span>
                @endif
                @if($aparat)
                    <span class="social-link">
                        <a href="{{ $aparat }}" target="_blank"><i class="icon icon-aparat dark"></i></a>
                    </span>
                @endif
                @if($facebook)
                    <span class="social-link">
                        <a href="{{ $facebook }}" target="_blank"><i class="icon icon-facebook dark"></i></a>
                    </span>
                @endif
            </div>
        </div>
    </div>
</div>
