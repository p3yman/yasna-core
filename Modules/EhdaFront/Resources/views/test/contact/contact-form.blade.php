@include('ehdafront::layouts.underlined_heading',[
    "text" => trans('ehdafront::general.contact.title.write_for_us') ,
     "color" => "blue" ,
])

<div class="contact-form">
    {!!
        widget('form-open')
     !!}
    
    @include('ehdafront::test.contact.form-row',[
        "id" => "name" ,
    ])
    
    @include('ehdafront::test.contact.form-row',[
        "id" => "email" ,
    ])
    
    @include('ehdafront::test.contact.form-row',[
        "id" => "phone" ,
    ])
    
    @include('ehdafront::test.contact.form-row',[
        "id" => "title" ,
    ])
    
    @include('ehdafront::test.contact.form-row',[
        "id" => "message" ,
    ])

    <div class="form-row">
        {!!
            widget('feed')
         !!}
    </div>

    <div class="form-row">
        <button type="submit" class="btn btn-primary">
            {{ trans('ehdafront::general.contact.form.button') }}
        </button>
    </div>
    
    {!!
        widget('form-close')
     !!}
</div>