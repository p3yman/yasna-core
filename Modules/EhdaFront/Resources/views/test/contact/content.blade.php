<div class="container mt40">
    <div class="row">
        <div class="col-md-6 mb30">
            @include('ehdafront::test.contact.contact-info',[
                "address" => "تهران، خیابان ولیعصر، بعد از پل همت، روبروی خیابان دهم، ساختمان ۷۹۰، واحد ۱۲" , 
                "phone" => " ۹۸۲۱۸۸۱۹۰۱۸۰+" , 
                "email" => "info@ehda.center" , 
                "twitter" => "#" , 
                "telegram" => "#" , 
                "instagram" => "#" , 
                "facebook" => "#" , 
                "aparat" => "#" , 
            ])
        </div>
        <div class="col-md-6">
            @include('ehdafront::test.contact.contact-form')
        </div>
    </div>
</div>