@foreach($groups as $group)

	<div class="faq-group">
		<h2>{{ $group['group_title'] }}</h2>
		@include('ehdafront::faq.question-list',[
			"parent_id" => $group['parent_id'] ,
			"questions" => $group['questions'] ,
		])
	</div>

@endforeach

<div class="faq-group">
	<h2>
		<i class="fa fa-lg fa-question-circle-o"></i>
		{{ trans('ehdafront::general.faq_not_found_ask_yours') }}
	</h2>
	<div class="form-wrapper clearfix">
		@include('ehdafront::faq.form',[
			"fields" => [
				[
					"title" => "name" ,
					"required" => true ,
				],
				[
					"title" => "email" ,
					"required" => true ,
				],
				[
					"title" => "mobile" ,
					"required" => false ,
				],
				[
					"title" => "subject" ,
					"required" => false ,
				],
			] ,
		])
	</div>
</div>

