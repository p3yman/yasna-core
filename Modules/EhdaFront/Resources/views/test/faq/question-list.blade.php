<div class="faq-list">
	<div class="panel-group" id="{{ $parent_id }}">
		@foreach($questions as $question)
			@php
				$iteration = $loop->iteration;
			@endphp
			{{--Question Component--}}
			@component('ehdafront::test.faq.question')
				@slot('parent_id') {{ $parent_id }} @endslot
				@slot('id') {{ $parent_id.$iteration }}  @endslot
				@slot('question')
					{{ $question['question'] }}
				@endslot

				{!! $question['answer'] !!}

			@endcomponent
			{{--Question Component--}}
		@endforeach
	</div>
</div>