<div class="col-sm-12">
	{!!
    	widget('form-open')
 	!!}
	<div class="row">
		@foreach($fields as $field)
			@php
				$type = $field['title'];
			@endphp
			<div class="col-sm-6">
				<div class="form-group ">
					<label for="{{ $type }}" class="col-sm-12 control-label text-align-start">
						{{ trans("ehdafront::general.faq.form.field.$type.title") }}
						@if($field['required'])
							<span class="required-sign text-red"
								  title="{{ trans("ehdafront::form.logic.required") }}">*</span>
						@endif
					</label>

					<div class="col-sm-12">
						<input type="text" id="{{ $type }}" name="{{ $type }}"
							   value=""
							   class="form-control
											{{ ($field['required'])? "form-required" : "" }}
							   {{ ($type === "mobile" or $type === "email")? 'from-'.$type : "" }}"
							   style=""
							   placeholder="{{ trans("ehdafront::general.faq.form.field.$type.placeholder") }}"
							   onkeyup="" onblur="" onfocus="" aria-valuenow=""
							   error-value="{{ trans("ehdafront::general.faq.form.field.$type.error") }}"
							   autocomplete="off">

						<span class="help-block" style=""></span>
					</div>
				</div>
			</div>
		@endforeach

		@php
			$type = "text";
			$field['required'] = true;
		@endphp

		<div class="col-sm-12">
			<div class="form-group">
				<label for="{{ $type }}" class="col-sm-12 control-label text-align-start">
					{{ trans("ehdafront::general.faq.form.field.$type.title") }}
					@if($field['required'])
						<span class="required-sign text-red"
							  title="{{ trans("ehdafront::form.logic.required") }}">*</span>
					@endif
				</label>

				<div class="col-sm-12">
					<textarea id="{{ $type }}" name="{{ $type }}"
							  class="form-control {{ ($field['required'])? "form-required" : "" }}"
							  placeholder="{{ trans("ehdafront::general.faq.form.field.$type.placeholder") }}"
							  rows="4"
							  error-value="{{ trans("ehdafront::general.faq.form.field.$type.error") }}"></textarea>
					<span class="help-block" style=""></span>
				</div>
			</div>
		</div>


		<div class="col-sm-12 pt15">
			<div class="action tal">
				<button class="btn btn-primary pull-left">
					{{ trans('ehdafront::general.faq.form.button') }}
				</button>
			</div>
		</div>

		<div class="col-sm-12 pt15">
			<div class="form-feed alert " style="display:none">
				{{ trans('ehdafront::forms.feed.wait') }}
			</div>
			<div class="d-n hide">
        <span class=" form-feed-wait" style="color: black;">
            <div style="width: 100%; text-align: center;">
				{{ trans('ehdafront::forms.feed.wait') }}
            </div>
        </span>
				<span class=" form-feed-error">
					{{ trans('ehdafront::forms.feed.error') }}
				</span>
				<span class=" form-feed-ok">
					{{ trans('ehdafront::forms.feed.done') }}
				</span>
			</div>
		</div>
	</div>
	{!!
		widget('form-close')
	 !!}
</div>