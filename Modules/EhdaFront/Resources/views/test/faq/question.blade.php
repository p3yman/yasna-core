<div class="panel ">
	<div class="panel-heading">
		<a data-toggle="collapse" data-parent="#{{ $parent_id }}" href="#{{ $id }}">
			<h4 class="panel-title">
				{{ $question }}
			</h4>
			<i class="panel-icon fa fa-plus"></i>
		</a>
	</div>
	<div id="{{ $id }}" class="panel-collapse collapse">
		<div class="panel-body">
			{{ $slot }}
		</div>
	</div>
</div>