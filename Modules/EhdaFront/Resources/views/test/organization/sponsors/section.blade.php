<div class="sponsors-section">
	<div class="section-title">
		<h2 class="">{{ $section_title }}</h2>
	</div>
	<div class="col-md-10 col-centered">
		@include('ehdafront::test.organization.sponsors.special')
		<hr>
		<h3>دیگر حامیان</h3>
		<div class="others">
			<ul class="clearfix other-sponsors">
				@foreach($sponsors['other'] as $sponsor)
					<li class="col-sm-6 other-sponsor">
						<a href="{{ $sponsor['link'] }}">
							{{ $sponsor['title'] }}
						</a>
					</li>
				@endforeach
			</ul>
		</div>
	</div>
</div>