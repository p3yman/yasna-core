<h3>حامیان ویژه</h3>
<div class="important">
	@foreach($sponsors['important'] as  $sponsor)
		<div class="sponsor-card">
			<div class="col-sm-12">
				<div class="logo-title">
					<h1 class="text-blue">{{ $sponsor['title'] }}</h1>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="logo-wrapper">
					{{-- Square ratio is prefered--}}
					<img src="{{ $sponsor['src'] }}" alt="{{ "logo" }}">
				</div>
			</div>
			<div class="col-sm-8">
				<div class="content">
					{!! $sponsor['content'] !!}
				</div>
			</div>
		</div>
	@endforeach
</div>