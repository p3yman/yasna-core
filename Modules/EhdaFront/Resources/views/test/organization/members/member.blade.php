<div class="member">
	<div class="image-wrapper">
		{{-- Square ratio is prefered --}}
		<img src="{{ $src }}" alt="{{ $alt }}">
	</div>
	<div class="text">
		<div class="name">
			{{ $name }}
		</div>
		<div class="position">
			{{ $position }}
		</div>
	</div>
</div>