<div class="members-section">
	<div class="section-title">
		<h2 class="">{{ $section_title }}</h2>
	</div>
	@foreach($groups as $group)
		@include('ehdafront::test.organization.members.members-group',[
			"group_title" => $group['title'] ,
			"members" => $group['members'] ,
		])
	@endforeach
</div>