@foreach($members as $member)
	@php
		$image = (isset($member['src']))? $member['src'] : Module::asset('ehdafront:images/placeholder-avatar.png')
	@endphp
	@if($member['special'])
		@section("special_members_$group_title")
			@include('ehdafront::test.organization.members.member',[
				"src" => $image ,
				"alt" => $member['alt'] ,
				"name" => $member['name'] ,
				"position" => $member['position'] ,
			])
		@append
	@else
		@section("other_members_$group_title")
			@include('ehdafront::test.organization.members.member',[
				"src" => $image ,
				"alt" => $member['alt'] ,
				"name" => $member['name'] ,
				"position" => $member['position'] ,
			])
		@append
	@endif
@endforeach

<div class="organization-group">
	<div class="title">
		<h2>{{ $group_title }}</h2>
	</div>
	<div class="members">
		<div class="specials">
			@yield("special_members_$group_title")
		</div>
		<div class="normals">
			@yield("other_members_$group_title")
		</div>
	</div>
</div>