@extends('ehdafront::layouts.frame')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title_link" => "#" ,
		"title" => "تشکیلات سازمانی" ,
		//"bg_img" => Module::asset('ehdafront:images/donation.jpg') ,
	])

	<div class="container">
		<div class="row mt100 mb50">
			<div class="col-md-4">
				@include('ehdafront::test.organization.box-title',[
					"title" => "سفیران" ,
					"colors" => ['#A6CE39','#688719'] ,
					"id" => "volunteers" ,
					"icon" => Module::asset('ehdafront:images/donation.png') ,
				])
			</div>
			<div class="col-md-4">
				@include('ehdafront::test.organization.box-title',[
					"title" => "حامیان" ,
					"colors" => ["#FFD700","#F18432"] ,
					"id" => "sponsors" ,
					"icon" => Module::asset('ehdafront:images/donation2.png') ,
				])
			</div>
			<div class="col-md-4">
				@include('ehdafront::test.organization.box-title',[
					"title" => "اعضا" ,
					"colors" => ["#44c8f5","#1683A7"] ,
					"id" => "members" ,
					"icon" => Module::asset('ehdafront:images/teamwork.png') ,
				])
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="organizations">
					<div class="organization" id="volunteers">
						@include('ehdafront::test.organization.volunteers.section',[
							"section_title" => "سفیران" ,
							"colors" => ['#A6CE39','#688719'] ,
							"content" => "
								<p>
	لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
</p>
<p>
	لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
</p>
<p>
	لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
</p>
							" ,
						])
					</div>
					<div class="organization" id="sponsors">
						@include('ehdafront::test.organization.sponsors.section',[
							"section_title" => "سفیران" ,
							"sponsors" => [
								"important" => [
									[
										"link" => "#" ,
										"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
										"title" => "بانک سامان" ,
										"content" => "
											<p>
												۱۰ معاونت سلامت شهرداری

												الف. تامین هزینه پذیرایی از خانواده های اهداکننده، بیماران ، هنرمندان، تیم های درمانی و مسئولین در جشن نفس 1395

												ب. تامین اتوبوس های منتقل کننده خانواده های اهدا کننده و بیماران به میادین اصلی تهران بعد از اتمام مراسم جشن نفس 1395

												ج. تامین هزینه اتوبوس و ون های منتقل کننده خانواده های اهدا کننده و بیماران در مراسم جشن نفس ۱۳۹۶
											</p>
										" ,
									],
									[
										"link" => "#" ,
										"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
										"title" => "بانک سامان" ,
										"content" => "
											<p>
												۱۰ معاونت سلامت شهرداری

												الف. تامین هزینه پذیرایی از خانواده های اهداکننده، بیماران ، هنرمندان، تیم های درمانی و مسئولین در جشن نفس 1395

												ب. تامین اتوبوس های منتقل کننده خانواده های اهدا کننده و بیماران به میادین اصلی تهران بعد از اتمام مراسم جشن نفس 1395

												ج. تامین هزینه اتوبوس و ون های منتقل کننده خانواده های اهدا کننده و بیماران در مراسم جشن نفس ۱۳۹۶
											</p>
										" ,
									],
									[
										"link" => "#" ,
										"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
										"title" => "بانک سامان" ,
										"content" => "
											<p>
												۱۰ معاونت سلامت شهرداری

												الف. تامین هزینه پذیرایی از خانواده های اهداکننده، بیماران ، هنرمندان، تیم های درمانی و مسئولین در جشن نفس 1395

												ب. تامین اتوبوس های منتقل کننده خانواده های اهدا کننده و بیماران به میادین اصلی تهران بعد از اتمام مراسم جشن نفس 1395

												ج. تامین هزینه اتوبوس و ون های منتقل کننده خانواده های اهدا کننده و بیماران در مراسم جشن نفس ۱۳۹۶
											</p>
										" ,
									],
									[
										"link" => "#" ,
										"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
										"title" => "بانک سامان" ,
										"content" => "
											<p>
												۱۰ معاونت سلامت شهرداری

												الف. تامین هزینه پذیرایی از خانواده های اهداکننده، بیماران ، هنرمندان، تیم های درمانی و مسئولین در جشن نفس 1395

												ب. تامین اتوبوس های منتقل کننده خانواده های اهدا کننده و بیماران به میادین اصلی تهران بعد از اتمام مراسم جشن نفس 1395

												ج. تامین هزینه اتوبوس و ون های منتقل کننده خانواده های اهدا کننده و بیماران در مراسم جشن نفس ۱۳۹۶
											</p>
										" ,
									],
									[
										"link" => "#" ,
										"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
										"title" => "بانک سامان" ,
										"content" => "
											<p>
												۱۰ معاونت سلامت شهرداری

												الف. تامین هزینه پذیرایی از خانواده های اهداکننده، بیماران ، هنرمندان، تیم های درمانی و مسئولین در جشن نفس 1395

												ب. تامین اتوبوس های منتقل کننده خانواده های اهدا کننده و بیماران به میادین اصلی تهران بعد از اتمام مراسم جشن نفس 1395

												ج. تامین هزینه اتوبوس و ون های منتقل کننده خانواده های اهدا کننده و بیماران در مراسم جشن نفس ۱۳۹۶
											</p>
										" ,
									],
									[
										"link" => "#" ,
										"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
										"title" => "بانک سامان" ,
										"content" => "
											<p>
												۱۰ معاونت سلامت شهرداری

												الف. تامین هزینه پذیرایی از خانواده های اهداکننده، بیماران ، هنرمندان، تیم های درمانی و مسئولین در جشن نفس 1395

												ب. تامین اتوبوس های منتقل کننده خانواده های اهدا کننده و بیماران به میادین اصلی تهران بعد از اتمام مراسم جشن نفس 1395

												ج. تامین هزینه اتوبوس و ون های منتقل کننده خانواده های اهدا کننده و بیماران در مراسم جشن نفس ۱۳۹۶
											</p>
										" ,
									],
									[
										"link" => "#" ,
										"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
										"title" => "بانک سامان" ,
										"content" => "
											<p>
												۱۰ معاونت سلامت شهرداری

												الف. تامین هزینه پذیرایی از خانواده های اهداکننده، بیماران ، هنرمندان، تیم های درمانی و مسئولین در جشن نفس 1395

												ب. تامین اتوبوس های منتقل کننده خانواده های اهدا کننده و بیماران به میادین اصلی تهران بعد از اتمام مراسم جشن نفس 1395

												ج. تامین هزینه اتوبوس و ون های منتقل کننده خانواده های اهدا کننده و بیماران در مراسم جشن نفس ۱۳۹۶
											</p>
										" ,
									],
								] ,
								"other" => [
									[
										"link" => "#" , 
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
									[
										"link" => "#" ,
										"title" => "حامی اول الکی" ,
									],
								] , 
							] ,
						])
					</div>
					<div class="organization" id="members">
						@include('ehdafront::test.organization.members.section',[
							"section_title" => "اعضا" ,
							"groups" => [
								[
									"title" => "هیات مدیره" ,
									"members" => [
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar3.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "مدیر کل" ,
											"special" => true ,
										],
										[
											//"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "معاون" ,
											"special" => true ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
									] ,
								],
								[
									"title" => "هیات امنا" ,
									"members" => [
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar3.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "مدیر کل" ,
											"special" => true ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "معاون" ,
											"special" => true ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
									] ,
								],
								[
									"title" => "هیات موسس" ,
									"members" => [
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar3.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "مدیر کل" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "معاون" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar2.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
										[
											"src" => Module::asset('ehdafront:images/demo/avatar/avatar1.jpg') ,
											"alt" => "avatar" ,
											"name" => "نگار خانوم جمالی‌فرد" ,
											"position" => "کارمند" ,
											"special" => false ,
										],
									] ,
								],
							] , 
						])
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

