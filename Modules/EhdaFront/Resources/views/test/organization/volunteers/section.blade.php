<div class="volunteers-section">
	<div class="section-title">
		<h2 class="">{{ $section_title }}</h2>
	</div>
	<div class="content">
		{!! $content !!}
	</div>
	<div class="mt40">
		<div class="row">
			<div class="col-sm-6 mb10">
				<a class="btn btn-lg btn-block btn-black" href="#">سفیران مشاهیر</a>
			</div>
			<div class="col-sm-6 mb10">
				<a class="btn btn-gold btn-lg btn-block" href="#">سفیر شوید</a>
			</div>
		</div>
	</div>
</div>