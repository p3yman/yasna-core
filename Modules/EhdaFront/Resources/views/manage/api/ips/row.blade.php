@if (!isset($client))
    @php $client = $model->user @endphp
@endif

@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => EhdaFrontRouteTools::action('Api\Manage\IpsController@singleAction', [
	    'model_hashid' => $model->hashid,
	    'view_file' => 'row',
    ]),
])

<td>
    @include("manage::widgets.grid-text" , [
        'text' => $model->slug,
        'size' => "16" ,
        'class' => "font-yekan text-bold" ,
    ])
    @include("manage::widgets.grid-date" , [
        'date' => $model->created_at ,
        'default' => "fixed" ,
        'format' => 'j F Y' ,
        'color' => '#1b72e2',
    ])
</td>
{{--
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
|
--}}

@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		[
		    'trash-o',
		    trans('manage::forms.button.delete'),
		    "modal:" . EhdaFrontRouteTools::action('Api\Manage\IpsController@singleAction', [
                'model_hashid' => $model->hashid,
                'view_file' => 'delete',
            ], false),
		    $client->canEdit(),
        ],
	]
])