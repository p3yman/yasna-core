{!!
    widget('modal')->target(EhdaFrontRouteTools::action('Api\Manage\IpsController@delete', [], false))
        ->label(trans('ehdafront::api.manage.headline.delete-ip-of-client', [
            'ip' => $model->slug,
            'client' => $client->full_name
        ]))
        ->noValidation()
!!}
<div class='modal-body'>
    
    {!! widget('hidden')->name('hashid')->value($model->spreadMeta()->hashid) !!}
    
    {!! widget('input')->inForm()->value($model->slug)->label(trans('ehdafront::api.manage.headline.ip'))->disabled() !!}

</div>
<div class="modal-footer">
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.delete'))
            ->shape('danger')
            ->value('delete')
            ->type('submit')
    !!}
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click(
                'masterModal("'
                . EhdaFrontRouteTools::action('Api\Manage\UsersController@ips', ['hashid' => $client->hashid])
                .'")')
    !!}
    
    {!! widget('feed') !!}
</div>
