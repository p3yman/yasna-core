@include('manage::widgets.grid', [
    'headings' => [
        trans('ehdafront::api.manage.headline.ip'),
        trans('manage::forms.button.action'),
    ],
    'handle' => 'counter',
    'row_view' => "ehdafront::manage.api.ips.row",
    'table_id' => 'tbl-ips'
])