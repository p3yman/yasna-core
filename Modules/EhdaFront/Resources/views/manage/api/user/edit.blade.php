@include('manage::layouts.modal-start' , [
	'form_url' => EhdaFrontRouteTools::action('Api\Manage\UsersController@save'),
	'modal_title' => trans( $model->id? "manage::forms.button.edit_info" : "users::forms.create_new_person" ),
])
<div class='modal-body'>
    
    {!! widget('hidden')->name('hashid')->value($model->hashid)->condition($model->exists) !!}
    
    {!!
        widget('input')
            ->name('name_first')
            ->value($model->name_first)
            ->inForm()
            ->required()
    !!}
    
    {!!
        widget('input')
            ->name('code_melli')
            ->label(trans('validation.attributes.username'))
            ->value($model->code_melli)
            ->inForm()
            ->required()
    !!}
    
    {!!
        widget('input')
            //->type('password')
            ->valueIf(!$model->exists, str_random(8))
            ->name('password')
            ->inForm()
            ->requiredIf(!$model->exists)
            ->helpIf(!$model->exists, trans('users::forms.password_hint'))
            ->helpIf($model->exists, trans('ehdafront::api.manage.message.api-user-edit-password-hint'))
    !!}
    
    {!!
        widget('combo')
            ->name('home_city')
            ->options(EhdaTools::getStatesCombo())
            ->label(trans('validation.attributes.city'))
            ->value($model->home_city)
            ->searchable(true)
            ->blankValue(true)
            ->inForm()
            ->required()
    !!}
    
    @include('manage::forms.buttons-for-modal')

</div>
@include('manage::layouts.modal-end')