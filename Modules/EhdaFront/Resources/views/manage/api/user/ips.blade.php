@include('manage::layouts.modal-start' , [
	'form_url' => EhdaFrontRouteTools::action('Api\Manage\IpsController@save'),
	'modal_title' => trans('ehdafront::api.manage.headline.acceptable-ips-of-client', ['client' => $model->full_name]),
])
<div class='modal-body'>
    
    {!! widget('hidden')->name('user_id')->value($model->hashid)->condition($model->exists) !!}
    
    @include('ehdafront::manage.api.ips.browse', [
        'models' => $model->ips,
        'client' => $model,
    ])
    
    <div class="panel panel-default">
        <div class="panel-body">
            
            <div class="col-sm-8">
                {!!
                    widget('input')
                        ->name('slug')
                        ->label(trans('ehdafront::api.manage.headline.ip'))
                        ->inForm()
                        ->style('direction:ltr')
                !!}
            </div>
            <div class="col-sm-4">
                {!!
                    widget('button')
                        ->label(trans('manage::forms.button.add'))
                        ->shape('link')
                        ->type('submit')
                        ->shape('success')
                        ->inForm()
                !!}
            </div>
            
            <div class="col-sm-12">
                {!! widget('feed') !!}
            </div>
        </div>
    </div>


</div>

<div class="modal-footer">
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click('$(".modal").modal("hide")')
    !!}
</div>
@include('manage::layouts.modal-end')