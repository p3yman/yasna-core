{!!
    widget('modal')
        ->target(EhdaFrontRouteTools::action('Api\Manage\UsersController@portablePrinterConfigs'))
        ->label(trans('ehdafront::api.manage.headline.acceptable-ips-of-client', ['client' => $model->full_name]))
!!}

<div class='modal-body'>
    
    {!! widget('hidden')->name('hashid')->value($model->hashid) !!}
    
    {!!
        widget('textarea')
            ->name('portable_printer_configs')
            ->label(trans("ehdafront::api.manage.headline.portable-printer-configs"))
            ->value(json_encode($model->spreadMeta()->portable_printer_configs))
            ->inForm()
            ->help(trans('ehdafront::api.manage.message.portable-printer-configs-hint'))
    !!}
    
    {!! widget('feed') !!}
    
</div>

<div class="modal-footer">
    {!!
        widget('button')
            ->label(trans('manage::forms.button.save'))
            ->shape('success')
            ->type('submit')
    !!}
    
    {!!
        widget('button')
            ->label(trans('manage::forms.button.cancel'))
            ->shape('link')
            ->on_click('$(".modal").modal("hide")')
    !!}
</div>