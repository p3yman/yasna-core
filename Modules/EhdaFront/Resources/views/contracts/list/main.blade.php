@extends('ehdafront::layouts.frame')
@php
	$contracts = [
		[
			"title" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم" ,
			"date" => "در تاریخ ۲۴ مهر ۹۷" ,
			"caption" => "بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها" ,
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
		],
		[
			"title" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم" ,
			"date" => "در تاریخ ۲۴ مهر ۹۷" ,
			"caption" => "بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها" ,
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
		],
		[
			"title" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم" ,
			"date" => "در تاریخ ۲۴ مهر ۹۷" ,
			"caption" => "بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها" ,
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
		],
		[
			"title" => "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم" ,
			"date" => "در تاریخ ۲۴ مهر ۹۷" ,
			"caption" => "بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها بخش مجوز ها, تفاهم نامه ها, قرارداد ها" ,
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/logo/logo1.png') ,
		],


	];
@endphp

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => "بخش مجوز ها، تفاهم نامه ها، قرارداد ها" ,
		"title_link" => "#" , 
	])
	@include('ehdafront::contracts.list.list')
@endsection