<div class="container">
	<div class="contracts-list mt40 mb40">
		@foreach($contracts as $contract)
			@include('ehdafront::layouts.widgets.row-box',[
				"title" => $contract['title'] ,
				"date" => $contract['date'] ,
				"caption" => $contract['caption'] ,
				"link" => $contract['link'] ,
				"src" => $contract['src'] ,
				"alt" => "" ,
			])
		@endforeach
	</div>
</div>