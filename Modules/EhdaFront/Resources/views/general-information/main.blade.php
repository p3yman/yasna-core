@extends('ehdafront::layouts.frame')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => $posttype->titleIn(getLocale()) ,
		"sub_title" => $currentPost->title ,
		"bg_img" => doc($currentPost->featured_image)->getUrl(),
		"blur" => $currentPost->spreadMeta()->single_cover_blur,
	])
	@include('ehdafront::general-information.content')
@endsection