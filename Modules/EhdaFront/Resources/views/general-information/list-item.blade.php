<div class="info-list-item {{ $class or "" }}">
	<a href="{{ $link }}">
		<div class="bg">
			{{ $bg }}
		</div>
		<div class="title">
			{{ $title }}
		</div>
	</a>
</div>