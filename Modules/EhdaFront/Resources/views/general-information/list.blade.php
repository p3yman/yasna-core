<div class="info-list @if(!isLangRtl()) ltr @endif">
	@foreach($lists as $list)
		@php
			$counter = $loop->iteration;
			$class = (count($lists) === $counter)? $list['class']." last" : $list['class'];
		@endphp
		@include('ehdafront::general-information.list-item',[
			"title" => $list['title'] ,
			"link" => $list['link'] ,
			"bg" => ad($counter) ,
			"class" => $class ,
		])
	@endforeach
</div>
