<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-push-4">
			<div class="page-content mt40">
				{!! $currentPost->text !!}
			</div>
		</div>
		<div class="col-sm-4 col-sm-pull-8">
			@php $active = true; @endphp
			@include('ehdafront::general-information.list',[
				"lists" => $posts->map(function ($post) use ($currentPost, &$active) {
					$result = [
						'title' => $post->title,
						'link' => action_locale('\\' . request()->route()->getActionName(), ['post' => $post->slug]),
						'class' => $active ? 'current' : '',
					];
					if ($currentPost->id == $post->id) {
						$active = false;
					}
					return $result;
				}) ,
			])
		</div>
	</div>
</div>