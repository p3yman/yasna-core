<footer class="bg-darkBlue text-white-deep">
    <div class="container">
        <div class="row">
            @include('ehdafront::layouts.footer_menu')
            @include('ehdafront::layouts.footer_contact')
            @include('ehdafront::layouts.useful_links')
        </div>
        
    </div>
    <div class="credit mt50" style="text-align: center; color: #FFFAAA; font-size: 13px; /*padding-top: 20px;*/">
        <hr>
        <a href="{{ Template::yasnaUrl() }}" target="_blank" {{--style="color: #FFFAAA;"--}}>
            {{ trans('ehdafront::general.powered_by_someone', ['someone' => Template::yasnaTitle()]) }}
        </a>
        {{-- @todo: ucommect following line --}}
        <img src="//sstatic1.histats.com/0.gif?3905330&101" alt="site stats" border="0">
    </div>
</footer>
@include('ehdafront::layouts.iweb')
@include('ehdafront::layouts.jui_scripts')