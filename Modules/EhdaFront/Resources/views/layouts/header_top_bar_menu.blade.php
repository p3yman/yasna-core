<ul class="pull-start list-inline no-margin">
	@if(EhdaFront::isNotIndex())
		<li class="pull-start">
			<a href="{{ EhdaFrontRouteTools::actionLocale('EhdaFrontController@index') }}"
			   title="{{ trans('ehdafront::general.home') }}">
				<i class="fa fa-home f22" style="line-height: 18px"></i>
			</a>
		</li>
	@endif
	@if(user()->exists)
		<li class="has-child pull-start js-profileList">
			<a href="#">
				@php
					$userWelcomeText = trans('ehdafront::general.profile_phrases.welcome_user', [
						'user' => user()->name_first
					])
				@endphp
				<span class="menu-text">
                    {{ $userWelcomeText }}
                </span>
				<span class="menu-icon">
                    <i class="fa fa-user"></i>
                </span>
			</a>
			<ul class="list-unstyled bg-blue">
				<li class="visible-xs">
					<a>{{ $userWelcomeText }}</a>
				</li>
				@if(user()->is_admin())  {{--This user is a volunteer--}}
				<li>
					<a href="{{ url('/manage') }}">
						{{ trans('ehdafront::general.volunteer_section.section') }}
					</a>
				</li>
				@endif
				@if(user()->is_an('card-holder'))  {{--This user has card--}}
				<li>
					<a href="{{ EhdaFrontRouteTools::actionLocale('UserController@index') }}">
						{{ trans('ehdafront::general.organ_donation_card_section.singular') }}
					</a>
				</li>
				<li>
					<a href="{{ EhdaFrontRouteTools::actionLocale('UserController@profile') }}">
						{{ trans('ehdafront::general.member_section.profile_edit') }}
					</a>
				</li>
				@endif
				<li><a href="{{ url('/logout') }}">{{ trans('ehdafront::general.member_section.sign_out') }}</a></li>
			</ul>
		</li>
	@else
		<li class="pull-start">
			<a href="{{ route('login') }}">
                <span class="menu-text">
                {{ trans('ehdafront::general.member_section.sign_in') }}
                </span>
				<span class="menu-icon">
                    <i class="fa fa-user"></i>
                </span>
			</a>
		</li>
	@endif

	@if (getLocale() == 'fa')
		<li>
			<a href="{{ EhdaFrontRouteTools::actionLocale('StatesController@map') }}">
            <span class="menu-text">
                {{ trans('ehdafront::general.provinces_portals') }}
            </span>
				<span class="menu-icon">
                <i class="fa fa-map-marker"></i>
            </span>
			</a>
		</li>
	@endif
</ul>