<div class="col-xs-12 col-md-4">
    <h5>{{ trans('ehdafront::general.notes.follow_us_in_social') }}</h5>
    <ul class="social-links list-inline">
        @foreach(Template::socialLinks() as $link)
            <li><a href="{{ $link['link'] }}" target="_blank"><i class="icon {{ "icon-".$link['icon'] }}"></i></a></li>
        @endforeach
    </ul>
    <p class="address">
        @php $contactInfo = Template::contactInfo() @endphp
        @if(array_key_exists('address', $contactInfo))
            <h5>{{ trans('ehdafront::validation.attributes.address') }}</h5>
            {{ $contactInfo['address'] }}
        @endif
        @if(Template::location())
            <br>
            {{--        <a href="{{ route_locale('contact') }}#map" class="link-white">{{ trans('ehdafront::general.view_on_map') }}</a>--}}
            <a href="{{ EhdaFrontRouteTools::actionLocale('ContactController@index') }}#map" class="link-white mt10 mb10">
                <i class="fa fa-map-marker"></i>
                {{ trans('ehdafront::general.view_on_map') }}</a>
        @endif
        @if(array_key_exists('telephone', $contactInfo))
            <br>
            @if(!is_array($contactInfo['telephone']))
                @php $contactInfo['telephone'] = [$contactInfo['telephone']] @endphp
            @endif
            @foreach($contactInfo['telephone'] as $tel)
                @if($loop->index)
                    ،
                @endif
                <a href="tel:{{ $tel }}">
                    <i class="fa fa-phone-square"></i>
                    {{ ad($tel) }}
                </a>
            @endforeach
        @endif
        
        @if(array_key_exists('email', $contactInfo))
            @if(!is_array($contactInfo['email']))
                @php $contactInfo['email'] = [$contactInfo['email']] @endphp
            @endif
            @foreach($contactInfo['email'] as $email)
                <br/>
                <a href="mailto:{{ $email }}">
                    <i class="fa fa-envelope"></i>
                    {{ $email }}
                </a>
            @endforeach
        @endif
    </p>
</div>