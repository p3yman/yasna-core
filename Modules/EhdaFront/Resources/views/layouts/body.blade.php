<body>
@include('ehdafront::layouts.header_content')
@yield('content')
@include('ehdafront::layouts.footer')
@yield('endOfBody')
</body>
</html>