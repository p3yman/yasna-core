@if ($boxes->count())
    <div class="slide-row-box js-rowBoxSlider owl-carousel mb30">
        @foreach($boxes as $post)
            <div class="box" style="background-image: url('{{ doc($post->featured_image)->getUrl() }}'); width: 250px;">
                <a href="{{ $post->direct_url ?: '#' }}">
                    <div class="content">
                        <h3>{{ $post->title }}</h3>
                        <div class="date">
                            {{ ad(echoDate($post->published_at, 'j F')) }}
                        </div>
                    </div>
                </a>
            </div>
        
        @endforeach
    </div>
    
    @section('endOfBody')
        {!! Html::script(Module::asset('ehdafront:libs/owl.carousel/owl.carousel.min.js')) !!}
        <script>
            jQuery(function ($) {
                $('.js-rowBoxSlider').owlCarousel({
                    items: {{ count($boxes) + 10 }},
                    @if(isLangRtl())
                    rtl: true,
                    @endif
                    loop: true,
                    margin: 10,
                    autoWidth: true,
                    nav: false,
                    dots: false,
                    autoplay: true,
                    autoplayTimeout: 2000,
                    autoplayHoverPause: true
                });
            }); //End Of siaf!
        </script>
    @endsection
@endif
