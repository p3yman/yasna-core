{{--
    Searchin form
--}}
@php
	$disabledSearch = $disabledSearch ?? false;
	$canSearch = !$disabledSearch;
@endphp
{!!
    widget('form-open')
        ->class('form-inline search-form')
        ->method('get')
        ->target(request()->url())
        ->condition($canSearch)
!!}
@if ($canSearch)
	<label for="search">
		{{ trans('ehdafront::general.search') }}
	</label>
@endif
<input class="form-control" id="{{ $id or "search" }}" type="text" @if($disabledSearch) disabled @endif
placeholder="{{ trans('ehdafront::general.search') }}" name="{{ $name or "search" }}" value="{{ $searchValue or '' }}">
@if ($canSearch)
	<button class="search-button" type="submit">
		<i class="fa fa-search"></i>
	</button>
@endif
{!!
    widget('form-close')
    	->condition($canSearch)
!!}