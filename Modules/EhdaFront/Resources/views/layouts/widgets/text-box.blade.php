<div class="text-box-row">
	<h1 class="title">
		<a href="{{ $link }}">
			{{ $title }}
		</a>
	</h1>
	<div class="abstract">
		{{ $abstract }}
	</div>
	<div class="pull-end">
		<a class="read-more" href="{{ $link }}">
			{{ trans('ehdafront::general.read_more') }}
			<span class="arrow -long @if(isLangRtl()) -rtl @endif"></span>
		</a>
	</div>
</div>
<hr>