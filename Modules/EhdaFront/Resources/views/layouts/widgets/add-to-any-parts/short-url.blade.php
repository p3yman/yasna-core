@if(isset($shortUrl))
    <div class="form-group text-left pull-end mt10" style="position: inherit">
        <label class="ml10" for="short_link" style="font-size: 10px;">{{ trans('ehdafront::general.short_link') }}: </label>
        <input id="short_link" value="{{ $shortUrl }}" style="float: left; width: 200px; direction: ltr;" class="form-control">
    </div>
@endif