<div class="simple-list-item">
	<h1 class="title">
		{{ $title }}
	</h1>
	@isset($subtitle)
		<div class="subtitle">
			{{ $subtitle }}
		</div>
	@endisset
	<div class="abstract">
		{{ $abstract }}
	</div>
	<a href="{{ $link }}" class="read_more">
		{{ trans('ehdafront::general.read_more') }}
	</a>
</div>