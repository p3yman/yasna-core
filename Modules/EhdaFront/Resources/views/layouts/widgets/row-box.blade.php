{{--
    Row Box
--}}
<div class="row-box">
    <a href="{{ $link }}">
        <div class="image-wrapper">
            <img src="{{ $src }}" alt="{{ $alt or "" }}">
        </div>
        <div class="content">
            @isset($subtitle)
                <h2 class="subtitle">{{ $subtitle }}</h2>
            @endisset
            <h1 class="title">{{ $title }}</h1>
            @if(isset($author) and $author)
                <span class="author">
                    {{ $author }}
                </span>
            @endif
            @if(isset($author) and isset($date) and $date and $author)
                <span class="text-silver"> | </span>
            @endif
            @isset($date)
                <span class="date">
                    {{ $date }}
                </span>
            @endisset
            
            @isset($caption)
                <div class="caption">
                    {{ $caption }}
                </div>
            @endisset
        
        </div>
    </a>
</div>