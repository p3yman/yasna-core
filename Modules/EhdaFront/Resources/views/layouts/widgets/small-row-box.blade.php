{{--
    Small Row Boxes
--}}

<div class="small-row-box @if(!isset($src)) text-only @endif">
    <a href="{{ $link }}">
        @isset($src)
        <div class="image-wrapper">
            {{-- Image should be 60*60 --}}
            <img src="{{ $src }}" alt="{{ $alt or "" }}">
        </div>
        @endisset
        <div class="content">
            <h1 class="title">{{ $title }}</h1>
            @isset($date)
            <div class="date">
                {{ $date }}
            </div>
            @endisset
        </div>
        @isset($bg)
            <span class="bg">
                {{ $bg }}
            </span>
        @endisset
    </a>
</div>