@php
    // Desirable image size is 400*300
    $image = (!isset($image))? Module::asset('ehdafront:images//img-placeholder.png') : $image;
@endphp

<div class="media-box">
    <a href="{{ $link or "#" }}">
        <div class="box_image">
            <img class="img-responsive" src="{{ $image }}" alt="{{ $alt or "" }}">
            <div class="content-wrapper">
                <div class="content">
                    <h2>{{ $title }}</h2>
                    @if(isset($caption) or $caption)
                        <p>{{ $caption }}</p>
                    @endif
                </div>
            </div>
        </div>
    </a>
</div>