@php
    $image = (!isset($image))? Module::asset('ehdafront:images//img-placeholder.png') : $image;
@endphp

<div class="box">
    <a href="{{ $link['link'] }}">
        <div class="box_image">
            <img src="{{ $image }}" alt="{{ $alt or "" }}">
        </div>
        <div class="box_caption">
            <h1 class="title">
                {{ $title or "" }}
            </h1>
            @isset($link['link'])
                <div class="footer">
                    {{ $link['title'] }}
                </div>
            @endisset
        </div>
    </a>
</div>