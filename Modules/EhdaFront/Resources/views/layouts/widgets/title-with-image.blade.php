{{-- Page Title with background image--}}
{{-- Accepts "blur" class to blur the background image --}}
<style>
    .background::before {background: none !important;}
</style>
<div class="title-with-image">
    <div class="background @if(isset($blur) and $blur) blur @endif" style="-webkit-transform:scale(1.1);-moz-transform:scale(1.1);-ms-transform:scale(1.1);-o-transform:scale(1.1);transform:scale(1.1); background-image: url('@if(isset($bg_img)) {{ $bg_img }} @else {{ Module::asset('ehdafront:images/12.jpg') }} @endif')"></div>
    <h1>
        <a href="{{ $title_link or "#" }}">
            {{ $title }}
        </a>
    </h1>
    @isset($sub_title)
    <h2>
        <a href="{{ $subtitle_link or "#" }}">
            {{ $sub_title }}
        </a>
    </h2>
    @endisset
</div>