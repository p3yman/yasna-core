<div class="copyright">
    {{ str_replace('::site', setting()->ask('site_title')->gain(), trans('ehdafront::general.footer.copy_right')) }}
    <br />
    {{ trans('ehdafront::general.footer.created_by') }} <a class="text-gray" href="https://yasnateam.com/" target="_blank">{{ trans('ehdafront::general.footer.yasna_team') }}</a>
</div>