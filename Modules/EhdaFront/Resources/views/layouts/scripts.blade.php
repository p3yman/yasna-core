<!-- Load Scripts -->
{!! Html::script(Module::asset('ehdafront:/libs/jquery/jquery-3.2.1.min.js')) !!}
{!! Html::script(Module::asset('ehdafront:/libs/bootstrap/js/bootstrap.min.js')) !!}
{!! Html::script(Module::asset('ehdafront:/libs/vue/vue.min.js')) !!}
{!! Html::script(Module::asset('ehdafront:/js/front.min.js')) !!}
{!! Html::script(Module::asset('ehdafront:/js/main.min.js')) !!}
{!! Html::script(Module::asset('ehdafront:/js/general.min.js')) !!}

{{--<script>--}}
    {{--(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){--}}
            {{--(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),--}}
        {{--m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)--}}
    {{--})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');--}}

    {{--ga('create', 'UA-85883345-1', 'auto');--}}
    {{--ga('send', 'pageview');--}}

{{--</script>--}}