<div class="col-xs-12 col-md-4">
	<ul class="list-unstyled clearfix">
		@foreach(Template::footerMenu() as $menuColumn)
			@php $children = $menuColumn->subMenus @endphp
			@if (!count($children))
				@continue
			@endif
			<div class="col-xs-6">
				@foreach($children as $menu)
					<li>
						<a href="{{ url($menu->linkIn(getLocale())) }}">
							{{ $menu->titleIn(getLocale()) }}
						</a>
					</li>
				@endforeach
			</div>
		@endforeach
	</ul>
</div>