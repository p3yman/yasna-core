<div class="btn-group select-lang-btn pb10">
    <a href="#" class="globe-list-btn">
        <i class="fa fa-globe"></i>
    </a>
    @if (count($availableLocales = availableLocales()) > 1)
        <ul class="list-inline globe-list">
            @foreach($availableLocales as $locale)
                <li class="pull-start">
                    <a href="{{ ($isCurrent = (getLocale() == $locale)) ? request()->url() : EhdaFront::localeLink($locale) }}"
                       @if($isCurrent) class="selected" @endif>
                        {{ $locale }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endif
</div>