{{--@if(isset($text) and $text)
    {{ null, $class = (isset($color) and $color) ? ("text-$color border-bottom-$color") : '' }}
    <h3 class="underlined-heading">
        <span class="{{ $class }}">{{ $text }}</span>
    </h3>
@endif--}}

@if(isset($text) and $text)
	@php
		$class = (isset($color) and $color) ? ("text-darkGray border-start-$color") : '';
	@endphp
	<h3 class="underlined-heading">
		<span class="{{ $class }}">{{ $text }}</span>
	</h3>
@endif
