<a href="{{ url_locale() }}" class="pull-start">
    <h1 class="main-logo" style="display: inline-block">
        @php $siteTitle = Template::siteTitle() @endphp
        <img src="{{ Template::siteLogo() }}" alt="{{ $siteTitle }}" id="logo">
        <img src="{{ Template::tinySiteLogo() }}" alt="{{ $siteTitle }}" id="logo-small">
        
        @if($stateLogo = Template::stateLogo())
            <img src="{{ $stateLogo }}" style="max-height: 45px">
        @endif
        <span class="hidden">{{ $siteTitle }}</span>
    </h1>
</a>