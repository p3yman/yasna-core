<header class="clearfix container-fluid">
    @include('ehdafront::layouts.header_top_bar')
    @include('ehdafront::layouts.header_bottom_bar')
    @include('ehdafront::layouts.register_card_btn_side')
</header>