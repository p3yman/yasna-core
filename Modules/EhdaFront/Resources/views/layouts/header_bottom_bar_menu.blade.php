@include('ehdafront::layouts.register_card_btn_top')
<span class="text-blue f30 mt15 toggle-menu js-openMenu pull-end"><i class="fa fa-bars"></i></span>

<nav class="menu js-submenu-toggle">
	<ul class="list-inline menu-list">
		<li class="has-child">
            <span class="search-icon js-openSearch" role="button">
                <i class="fa fa-search"></i>
            </span>
			<div class="search-box">
                <span class="close-icon js-closeSearch" role="button">
                    <i class="fa fa-times"></i>
                </span>
				{{--@TODO: Search Form Needs route and controller--}}
				{!!
					widget('form-open')
						->class('form-inline')
						->target(EhdaFrontRouteTools::actionLocale('PostController@search'))
						->method('GET')
				 !!}
				<input type="text" class="form-control" name="{{ PostTools::getMainSearchFieldName() }}">
				<button class="btn btn-success" type="submit">
					<i class="fa fa-search"></i>
				</button>
				{!! Form::close() !!}
			</div>
		</li>
		@if ($mainMenu = Template::mainMenu())
			@foreach ($mainMenu as $menu)
				@if (!($menuTitle =  $menu->titleIn(getLocale())))
				    @continue
				@endif
				@php
					$hasChild = boolval($menu->subMenus->count());
					$multiColumn = $menu->subMenus->pluck('subMenus')->filter(function ($children) {
										return boolval($children->count());
									})->count()
				@endphp
				<li @if ($hasChild) class="has-child" @endif>
					<a @if($menu->link) href="{{ url($menu->link) }} @endif">{{ $menuTitle }}</a>
					@if ($hasChild)
						<ul class="sub-menu @if($multiColumn) multi-col @endif bg-white text-darkGray-deep mega-menu border-top-3 border-top-green">
							@if($multiColumn)
								@foreach($menu->subMenus as $subMenu)
									@if (!($subMenuTitle =  $subMenu->titleIn(getLocale())))
										@continue
									@endif
									<li class="menu-col">
										<a @if($subMenu->link) href="{{ url($subMenu->link) }}" @endif>
											{{ $subMenuTitle }}
										</a>
										@if($subMenu->subMenus->count())
											<ul>
												@foreach($subMenu->subMenus as $child)
													@if (!($childTitle =  $child->titleIn(getLocale())))
														@continue
													@endif
													<li>
														<a href="{{ $child->link ? url($child->link) : '#' }}">
															{{ $childTitle }}
														</a>
													</li>
												@endforeach
											</ul>
										@endif
									</li>
								@endforeach
							@else
								<li class="menu-col">
									<ul>
										@foreach($menu->subMenus as $subMenu)
											<li>
												<a href="{{ $subMenu->link ? url($subMenu->link) : '#' }}">
													{{ $subMenu->titleIn(getLocale()) }}
												</a>
											</li>
										@endforeach
									</ul>
								</li>
							@endif
						</ul>
					@endif
				</li>
			@endforeach
		@endif
	</ul>
</nav>


<span class="close-menu js-closeMenu">
    <i class="fa fa-times"></i>
</span>