{{--
    Single Page Layout
--}}

<div class="row">
	<div class="col-sm-12">
		@isset($subtitle)
			<h4 class="post-subtitle text-gray mb0">
				{{ $subtitle }}
			</h4>
		@endisset
		<h3 class="post-title @isset($subtitle) mt0 @endisset">
			{{ $title }}
		</h3>

		<div class="content">
			{!! $content !!}
		</div>

		@yield('additionalContent')

		<div class="time">
			{{ trans('ehdafront::posts.form.publish').": " }}
			{{ $time }}
		</div>
		<div class="row">
			<div class="col-sm-6">
				@if(isset($tags) and $tags->count())
					@include('ehdafront::layouts.meta_tags', ['metaTags' => ['keywords' => $tags->pluck('slug')->toArray()]])
					<div class="tags">
                    <span class="text mr10">
                        {{ trans('ehdafront::posts.features.tags').":" }}
                    </span>
						@foreach($tags as $tag)
							<a class="btn btn-sm btn-info f12 mb5"
							   href="{{ $tag->index_url }}">{{ $tag->slug }}</a>
						@endforeach
					</div>
				@endif
			</div>
			<div class="col-sm-6">
				@include('ehdafront::layouts.widgets.add-to-any',[
					"shortUrl" => $short_url ,
				])
			</div>
		</div>
	</div>
</div>