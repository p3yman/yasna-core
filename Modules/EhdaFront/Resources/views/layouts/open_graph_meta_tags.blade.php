@section('head')

	@foreach(Template::openGraphs() as $tagTitle =>  $tagContent)
		<meta property="og:{{ $tagTitle }}" content="{{ EhdaFrontTools::getValue($tagContent) }}"/>
	@endforeach

@append
