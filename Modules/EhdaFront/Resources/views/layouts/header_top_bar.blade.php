<div class="row top-bar bg-blue text-white-deep clearfix" style="position: relative">
    @include('ehdafront::layouts.header_top_bar_menu')
    @include('ehdafront::layouts.header_top_bar_select_lang')
    @include('ehdafront::layouts.header_top_bar_organ_donation')
</div>