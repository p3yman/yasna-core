<!DOCTYPE html>
<html lang="{{ getLocale() }}">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" href="{{ Module::asset('ehdafront:images/template/favicon.ico') }}">

	<title>@yield('page-title', Template::implodePageTitle(Template::pageTitleSeparator()))</title>
	@if(isLangRtl())
		{{ Html::style(Module::asset('ehdafront:css/front-style.min.css')) }}
		{{ Html::style(Module::asset('ehdafront:css/style-upgrade.min.css')) }}
	@else
		{{ Html::style(Module::asset('ehdafront:css/front-style-ltr.min.css')) }}
		{{ Html::style(Module::asset('ehdafront:css/style-upgrade-ltr.min.css')) }}
	@endif
	@include('ehdafront::layouts.scripts')
	@include('ehdafront::layouts.meta_tags', ['metaTags' => ['robots' => '__DEFAULT__']])
	@yield('head')
</head>