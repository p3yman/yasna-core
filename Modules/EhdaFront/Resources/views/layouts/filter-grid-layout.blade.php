{{--
---- Use this blade as "component" and fill the gaps with "slots"
--}}

@if ($isBasePage)
	<div class="container-fluid mt50 mb30">
		<div class="row">
			<div class="col-md-11 col-centered">
				<div class="row">
					<button class="btn btn-primary filter-opener js-filter-opener">
						<i class="fa fa-filter"></i>
						{{ trans('ehdafront::general.filter.plural') }}
					</button>
					<div class="col-md-3 col-sm-4 filter-sidebar">
						<div class="filter_wrapper">
                        <span class="close-filter js-close-filter" role="button">
                            <i class="fa fa-times"></i>
                        </span>
							<div class="filter_inner filters-panel" data-filter-url="{{ $filterUrl }}">
								{{ $filters }}

								<hr>

								<button class="btn btn-link btn-block js-refreshFilter mb10" onclick="resetFilters()">
									<i class="fa fa-refresh"></i>
									{{ trans('ehdafront::general.filter.discard') }}
								</button>

								{{--<button class="btn btn-block btn-primary js-applyFilter">--}}
								{{--{{ trans('ehdafront::general.filter.apply') }}--}}
								{{--</button>--}}
							</div>
						</div>
					</div>
					<div class="col-md-9 col-sm-8">
						@isset($header_filters)
							<div class="header-filter mb50">
								<div class="row">
									{{ $header_filters }}
								</div>
							</div>
						@endisset
						<div class="box-container result-container">
							<div class="row">
								@endif

								{{ $content or "" }}

								@if ($isBasePage)
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endif
