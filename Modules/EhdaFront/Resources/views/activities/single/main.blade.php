@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($posttypeTitle = $posttype->titleIn(getLocale())) @endphp
@if ($categoryTitle = $post->guessDefaultCategory()->titleIn(getLocale()))
	@php Template::appendToPageTitle($categoryTitle) @endphp
@endif
@php Template::appendToPageTitle($post->title) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@if ($postImage = doc($post->featured_image)->getUrl())
	@php Template::mergeWithOpenGraphs(['image' => $postImage]) @endphp
@endif

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image', [
		"title_link" => EhdaFrontRouteTools::actionLocale("PostController@archive", ["postType" => $posttype->slug]),
		"title" => $posttypeTitle,
		"sub_title" => $categoryTitle ,
		"bg_img" => doc($post->featured_image)->getUrl(),
		"blur" => $post->spreadMeta()->single_cover_blur,
	])

	<div class="container mt40">
		{!! $innerHTML !!}

		{{--@include('ehdafront::layouts.underlined_heading', [--}}
			{{--'text' => trans('ehdafront::posts.features.dont_miss'),--}}
			{{--'color' => 'green',--}}
		{{--])--}}

		{{--<div class="row">--}}
			{{--@foreach($post->similars(4) as $similar)--}}
				{{--<div class="col-xxs col-xs-6 col-md-4 col-lg-3">--}}
					{{--@include('ehdafront::layouts.widgets.box',[--}}
						{{--"title" => $similar->title ,--}}
						{{--"image" => doc($similar->featured_image)->getUrl(),--}}
						{{--"link" => [--}}
							{{--"link" => $similar->direct_url ,--}}
							{{--"title" => "+ " . trans('ehdafront::general.continue') ,--}}
						{{--] ,--}}
					{{--])--}}
				{{--</div>--}}
			{{--@endforeach--}}
		{{--</div>--}}
	</div>
@endsection