@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = $posttype->titleIn(getLocale())) @endphp

@php
	$coverData = [
		"title" => $title ,
		"title_link" => EhdaFrontRouteTools::currentActionLocale()
	];
@endphp

@if ($currentCategory->exists)
	@php
		Template::appendToPageTitle($currentCategory->titleIn(getLocale())) ;
		$coverData['sub_title'] = $currentCategory->titleIn(getLocale());
		$coverData['subtitle_link'] = request()->url();
	@endphp
@endif

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',$coverData)

	@include('ehdafront::activities.list.body')
@endsection