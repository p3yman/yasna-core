<div class="activity-box @if($active) active @endif">
	<a href="{{ $link }}">
		<img class="leaf leaf-inactive" src="{{ Module::asset('ehdafront:images/sprout.svg') }}" alt="leaf">
		<img class="leaf leaf-active" src="{{ Module::asset('ehdafront:images/sprout-active.svg') }}" alt="leaf">
		<div class="content">
			<h1 class="title">{{ $title or "" }}</h1>
		</div>
	</a>
</div>