<div class="activities-tree">
	@foreach($activity_types as $activity)
		@include('ehdafront::activities.list.activity-box',[
			"title" => $activity['title'] ,
			"link" => $activity['link'] ,
			"active" => $activity['active'],
		])
	@endforeach
</div>