@php
	$activity_types = $categories->map(function ($category) use ($currentCategory) {
		return [
			'title' => $category->titleIn(getLocale()),
			'link' => EhdaFrontRouteTools::currentActionLocale(['category' => $category->slug]),
			'active' => $category->id == $currentCategory->id
		];
	});

	$activities =[
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
		[
			"link" => "#" ,
			"src" => Module::asset('ehdafront:images/demo/events/img2.jpg') ,
			"alt" => "" ,
			"title" => "فعالیت اول" ,
			"caption" => "توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود. توضیحات این فعالیت در اینجا نوشته می‌شود." ,
		],
	];
@endphp


<div class="activities">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-sm-5">
				@include('ehdafront::activities.list.tree-view')
			</div>
			<div class="col-md-8 col-sm-7">
				<div class="activity-list">
					@include('ehdafront::layouts.underlined_heading',[
						"text" => $posttype->titleIn(getLocale()) ,
						"color" => "green" , 
					])
					<div class="list">
						@include('ehdafront::layouts.widgets.search-form')

						@if ($posts->count())
							@foreach($posts as $activity)
								@include('ehdafront::layouts.widgets.row-box',[
									"link" => $activity->direct_url ,
									"src" => ($imageFile = doc($activity->featured_image)->resolve())->getUrl() ,
									"alt" => $imageFile->getTitle() ,
									"title" => $activity->title ,
									"caption" => $activity->abstract ,
								])
							@endforeach

							<div class="col-xs-12 text-center">
								{!! $posts->render() !!}
							</div>
						@else
							@include('ehdafront::layouts.widgets.error-box',[
								"message" => trans('ehdafront::general.no_result_found') ,
							])
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
