<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        body{
            width: 100%;
            background: #ffffff !important;
            margin: 0 auto;
            text-align: center;
        }
        @if($type == 'full')
            img{
                width: 21cm;
                height: 29.7cm;
            }
        @endif
    </style>

    <title>{{ trans('ehdafront::forms.button.card_print') }}</title>
</head>
<body onload="window.print();">
{{--@TODO: server address should set dynamic --}}
<img src="{{ EhdaFrontRouteTools::action('NewCardFileController@generateCard', [
            'type'             => $type,
            'user_type_hashid' => $user->cardTypeHashid($type),
        ]) }}" alt="{{ trans('ehdafront::forms.button.card_print') }}">

</body>
</html>