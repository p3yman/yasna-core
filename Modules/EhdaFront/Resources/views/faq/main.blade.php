@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($title = $posttype->titleIn(getLocale())) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => trans('ehdafront::general.faqs') ,
		"bg_img" => Module::asset('ehdafront:images/faq2.jpg') ,
	])
	<div class="container mt40 mb40">
		<div class="mb20">
			@include('ehdafront::layouts.widgets.search-form')
		</div>
		@include('ehdafront::faq.groups')
	</div>
@endsection