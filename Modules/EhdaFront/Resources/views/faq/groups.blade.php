@if (count($postGroups))
	@foreach($postGroups as $group)
		@php
			$category = $group['category'];
		@endphp
		<div class="faq-group">
			<h2>{{ $category->titleIn(getLocale()) }}</h2>
			@include('ehdafront::faq.question-list',[
				"parent_id" => $category->hashid ,
				"questions" => $group['posts'] ,
			])
		</div>

	@endforeach
@else
	@include('ehdafront::layouts.widgets.error-box',[
		"message" => trans('ehdafront::general.no_result_found') ,
	])
@endif

@if ($commentingPost->exists)
	@php $commentingPost->spreadMeta() @endphp
	<div class="faq-group">
		<h2>
			<i class="fa fa-lg fa-question-circle-o"></i>
			{{ $commentingPost->title }}
		</h2>
		<div class="form-wrapper clearfix">
			@include('ehdafront::faq.form', [
				"fields" => [
					[
						"title" => "name" ,
						"required" => true ,
					],
					[
						"title" => "email" ,
						"required" => true ,
					],
					[
						"title" => "mobile" ,
						"required" => false ,
					],
					[
						"title" => "subject" ,
						"required" => false ,
					],
				] ,
				"fields" => CommentTools::translateFields($commentingPost->fields),
			])
		</div>
	</div>
@endif
