<div class="col-sm-12">
	{!!
    	widget('form-open')
    		->class('js')
			->target(EhdaFrontRouteTools::actionLocale('CommentController@save'))
 	!!}

	{!!
		widget('hidden')
			->name('post_id')
			->value($commentingPost->hashid)
	!!}

	<div class="row">
		@foreach($fields as $fieldName => $fieldInfo)
			@php //dd($fieldName, $fieldInfo, __FILE__ . ' - ' . __LINE__); @endphp
			@if ($fieldName == 'message' or $fieldName == 'text')
				<div class="col-sm-12">
					<div class="form-group">
						<label for="{{ $fieldName }}" class="col-sm-12 control-label text-align-start">
							{{ trans("ehdafront::general.faq.form.field.$fieldName.title") }}
							@if($fieldInfo['required'])
								<span class="required-sign text-red"
									  title="{{ trans("ehdafront::form.logic.required") }}">*</span>
							@endif
						</label>

						<div class="col-sm-12">
						<textarea id="{{ $fieldName }}" name="{{ $fieldName }}"
								  class="form-control {{ ($fieldInfo['required'])? "form-required" : "" }}"
								  placeholder="{{ trans("ehdafront::general.faq.form.field.$fieldName.placeholder") }}"
								  rows="4"
								  error-value="{{ trans("ehdafront::general.faq.form.field.$fieldName.error") }}"></textarea>
							<span class="help-block" style=""></span>
						</div>
					</div>
				</div>
			@else

				<div class="col-sm-6">
					<div class="form-group ">
						<label for="{{ $fieldName }}" class="col-sm-12 control-label text-align-start">
							{{ trans("ehdafront::general.faq.form.field.$fieldName.title") }}
							@if($fieldInfo['required'])
								<span class="required-sign text-red"
									  title="{{ trans("ehdafront::form.logic.required") }}">*</span>
							@endif
						</label>

						<div class="col-sm-12">
							<input type="text" id="{{ $fieldName }}" name="{{ $fieldName }}"
								   value=""
								   class="form-control
											{{ ($fieldInfo['required']) ? "form-required" : "" }}
								   {{ ($fieldName === "mobile" or $fieldName === "email")? 'from-'.$fieldName : "" }}"
								   style=""
								   placeholder="{{ trans("ehdafront::general.faq.form.field.$fieldName.placeholder") }}"
								   onkeyup="" onblur="" onfocus="" aria-valuenow=""
								   error-value="{{ trans("ehdafront::general.faq.form.field.$fieldName.error") }}"
								   autocomplete="off">

							<span class="help-block" style=""></span>
						</div>
					</div>
				</div>

			@endif

		@endforeach


		<div class="col-sm-12 pt15">
			<div class="action tal">
				<button class="btn btn-primary pull-left">
					{{ trans('ehdafront::general.faq.form.button') }}
				</button>
			</div>
		</div>

		<div class="col-sm-12 pt15">
			<div class="form-feed alert " style="display:none">
				{{ trans('ehdafront::forms.feed.wait') }}
			</div>
			<div class="d-n hide">
        <span class=" form-feed-wait" style="color: black;">
            <div style="width: 100%; text-align: center;">
				{{ trans('ehdafront::forms.feed.wait') }}
            </div>
        </span>
				<span class=" form-feed-error">
					{{ trans('ehdafront::forms.feed.error') }}
				</span>
				<span class=" form-feed-ok">
					{{ trans('ehdafront::forms.feed.done') }}
				</span>
			</div>
		</div>
	</div>
	{!!
		widget('form-close')
	 !!}
</div>

@if ($commentingPost->exists)
	{!! Html::script (Module::asset('ehdafront:libs/jquery.form.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:js/forms.min.js')) !!}
	{!! Html::script (Module::asset('ehdafront:libs/jquery-ui/jquery-ui.min.js')) !!}
@endif