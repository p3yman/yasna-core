@extends('ehdafront::layouts.frame')


@php Template::appendToPageTitle($posttypeTitle = $postType->titleIn(getLocale())) @endphp
@php $subTitle = [] @endphp
@if ($folder->exists)
	@php Template::appendToPageTitle($folderTitle = $folder->titleIn(getLocale())) @endphp
	@php $subTitle[] = $folderTitle @endphp
@endif
@if ($category->exists)
	@php Template::appendToPageTitle($categoryTitle = $category->titleIn(getLocale())) @endphp
	@php $subTitle[] = $categoryTitle @endphp
@endif

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop
@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
	@php $pin = $importantEvents->first() @endphp
	@if ($pin)
		@include('ehdafront::events.list.top-event',[
			"title" => $pin->title ,
			"start_date" => ad(echoDate($pin->event_starts_at, 'j F')) ,
			"end_date" => ad(echoDate($pin->event_ends_at, 'j F')) ,
			"start_time" => ad(echoDate($pin->event_starts_at, 'H:i')) ,
			"end_time" => ad(echoDate($pin->event_ends_at, 'H:i')),
			"location" => $pin->spreadMeta()->location ,
			"link" => $pin->direct_url ,
			"src" => doc($pin->featured_image)->getUrl(),
		])

		@include('ehdafront::events.list.pinned-events',[
			"events" => $importantEvents->except($pin->id),
		])
	@endif


	@include('ehdafront::events.list.all-events')
@endsection
