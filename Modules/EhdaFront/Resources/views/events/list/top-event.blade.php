@php
	$src = (isset($src) and $src) ? $src : Module::asset('ehdafront:images/image-1.jpg');
@endphp

<div class="top-event-full">
	<div class="image-wrapper blur">
		<img src="{{ $src }}" alt="{{ $alt or "" }}">
	</div>
	<div class="content">
		<h1 class="title">
			{{ $title }}
		</h1>
		@php $date_parts = [] @endphp
		@php $zero_date = '-' @endphp
		@if (isset($start_date) and $start_date and ($start_date != $zero_date))
			@php $date_parts[] = $start_date @endphp
		@endif
		@if (isset($end_date) and $end_date and ($end_date != $zero_date))
			@php $date_parts[] = $end_date @endphp
		@endif
		@if ($date_string = implode(' - ', $date_parts))
			<div class="dates mb5">
				<i class="fa fa-calendar text-gold"></i>
				{{ $date_string }}
			</div>
		@endif
		@php $time_parts = [] @endphp
		@php $zero_time = ad('00:00') @endphp
		@if (isset($start_time) and $start_time and ($start_time != $zero_time) and ($start_time != $zero_date))
			@php $time_parts[] = $start_time @endphp
		@endif
		@if (isset($end_time) and $end_time and ($end_time != $zero_time) and ($end_time != $zero_date))
			@php $time_parts[] = $end_time @endphp
		@endif
		@if ($time_string = implode(' - ', $time_parts))
			<div class="times mb5">
				<i class="fa fa-clock-o text-gold"></i>
				<span>
				{{ $time_string }}
            </span>
			</div>
		@endif
		@if(isset($location) and $location)
			<div class="location mb5">
				<i class="fa fa-map-marker text-gold"></i>
				{{ $location }}
			</div>
		@endif
		<div class="link mt20">
			<a href="{{ $link }}" class="btn btn-gold btn-lg">
				{{ "مشاهده جزئیات" }}
			</a>
		</div>
	</div>
</div>