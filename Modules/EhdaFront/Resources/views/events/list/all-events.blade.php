<div class="container">
	<div class="row">
		<div class="col-md-12" id="list-box">
			@include('ehdafront::layouts.underlined_heading',[
				"color" => "blue" ,
				"text" => trans('ehdafront::general.event.all') ,
			])
			@include('ehdafront::layouts.widgets.search-form')
			<div class="lis">
				@if ($innerHTML)
					{!! $innerHTML !!}
				@else
					@include('ehdafront::layouts.widgets.error-box',[
						"message" => trans('ehdafront::general.no_result_found') ,
					])
				@endif
			</div>
		</div>
	</div>
</div>

@section('endOfBody')
	@if (isset($searchValue) and $searchValue)
		<script>
            $(document).ready(function () {
                $('#list-box').scrollToView(-$('.main-menu').outerHeight());
            });
		</script>
	@endif
@stop