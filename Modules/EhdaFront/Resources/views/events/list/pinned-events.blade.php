<div class="pinned-events">
	<div class="events-container">
		@php $zero_date = '-' @endphp
		@php $zero_time = ad('00:00') @endphp
		@foreach($events as $event)
			@switch($event->event_status)
				@case("upcoming")
				@php $class = "future" @endphp
				@break
				@case("running")
				@php $class = "running" @endphp
				@break
				@case("expired")
				@php $class = "past" @endphp
				@break
			@endswitch
			@php $event->spreadMeta() @endphp
			<div class="event-card">
				<div class="status {{ $class or "" }}">

					{{ $event->event_status_text }}

				</div>
				@php $featuredImage = doc($event->featured_image)->resolve() @endphp
				<div class="card_image">
					<img src="{{ $featuredImage->getUrl() }}" alt="{{ $featuredImage->getTitle() }}">
				</div>
				<div class="card_caption">
					<h1 class="title">
						{{ $event->title }}
					</h1>
					@php $date_parts = [] @endphp
					@if (
						$event->event_starts_at and
						($start_date = ad(echoDate($event->event_starts_at, 'j F'))) and
						($start_date != $zero_date)
					)
						@php $date_parts[] = $start_date @endphp
					@endif
					@if (
						$event->event_ends_at and
						($end_date = ad(echoDate($event->event_ends_at, 'j F'))) and
						($end_date != $zero_date)
					)
						@php $date_parts[] = $end_date @endphp
					@endif
					@if ($date_string = implode(' - ', $date_parts))
						<div class="dates">
							<i class="fa fa-calendar text-gold"></i>
							{{ $date_string }}
						</div>
					@endif

					@php $time_parts = [] @endphp
					@if (
						$event->event_starts_at and
						($start_time = ad(echoDate($event->event_starts_at, 'H:i'))) and
						($start_time != $zero_time) and
						($start_time != $zero_date)
					)
						@php $time_parts[] = $start_time @endphp
					@endif
					@if (
						$event->event_ends_at and
						($end_time = ad(echoDate($event->event_ends_at, 'H:i'))) and
						($end_time != $zero_time) and
						($end_time != $zero_date)
					)
						@php $time_parts[] = $end_time @endphp
					@endif
					@if ($time_string = implode(' - ', $time_parts))
						<div class="times">
							<i class="fa fa-calendar text-gold"></i>
							{{ $time_string }}
						</div>
					@endif

					@if($event->location)
						<div class="location">
							<i class="fa fa-map-marker text-gold"></i>
							{{ $event->location }}
						</div>
					@endif
					<div class="footer">
						<a href="{{ $event->direct_url }}">
							@if(isLangRtl())
								<i class="fa fa-chevron-left"></i>
							@else
								<i class="fa fa-chevron-right"></i>
							@endif
						</a>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>