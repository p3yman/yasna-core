@extends('ehdafront::layouts.frame')

@php Template::appendToPageTitle($posttypeTitle = $posttype->titleIn(getLocale())) @endphp
@if ($categoryTitle = $post->guessDefaultCategory()->titleIn(getLocale()))
	@php Template::appendToPageTitle($categoryTitle) @endphp
@endif
@php Template::appendToPageTitle($post->title) @endphp


@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@if ($postImage = doc($post->featured_image)->getUrl())
	@php Template::mergeWithOpenGraphs(['image' => $postImage]) @endphp
@endif

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
    {!! $innerHTML !!}
@endsection