<div class="research-projects">
	@if ($static_post->exists)
		@include('ehdafront::projects.list.description',[
			"title" => $static_post->title ,
			"description" => $static_post->text,
		])
	@endif

	@include('ehdafront::projects.list.lists')
</div>