@extends('ehdafront::layouts.frame')

@php $pageImage = $static_post->exists ? fileManager()->file($static_post->featured_image)->getUrl() : null @endphp

@if ($pageImage)
	@php Template::mergeWithOpenGraphs(['image' => $pageImage]) @endphp
@endif

@php Template::appendToPageTitle($title = $postType->titleIn(getLocale())) @endphp

@section('page-title')
	{{ $pageTitle = Template::implodePageTitle(Template::pageTitleSeparator()) }}
@stop

@php Template::mergeWithOpenGraphs(['title' => $pageTitle]) @endphp

@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')


@section('content')
	@include('ehdafront::layouts.widgets.title-with-image',[
		"title" => $title ,
		"title_link" => request()->url() ,
		"bg_img" => $pageImage,
		"blur" => $static_post->spreadMeta()->single_cover_blur,
	])
	@include('ehdafront::projects.list.body')
@endsection