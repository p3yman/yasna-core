<div class="container mt30">
	@include('ehdafront::layouts.underlined_heading',[
					"text" => $postType->titleIn(getLocale()) ,
					"color" => "green" ,
				])

	<div class="list">
		@include('ehdafront::layouts.widgets.search-form')
		@if ($innerHTML)
			{!! $innerHTML !!}
		@else
			@include('ehdafront::layouts.widgets.error-box',[
				"message" => trans('ehdafront::general.no_result_found') ,
			])
		@endif
	</div>
</div>