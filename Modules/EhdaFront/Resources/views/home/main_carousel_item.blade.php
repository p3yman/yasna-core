@php $post->spreadMeta() @endphp
@php
    $image = fileManager()
    	->file($post->featured_image)
    	->posttype($slideshow_posttype)
    	->config('featured_image')
    	->version('1600x600')
    	->getUrl()
@endphp
@if($image)
    <div class="slide">
        {{-- If the link is specified, the item will behave like a link. --}}
        <a @if($post->link) href="{{ $post->link }}" target="_blank" @endif>
            <div class="image-wrapper">
                <img src="{{ $image }}">
            </div>
            @if($post->title2 or $post->abstract)
                <div class="slide-text">
                    @if($post->title2)
                        <h3>{{ ad($post->title2) }}</h3>
                    @endif
                    @if($post->abstract)
                        <span class="slide-text-description">
                    {{ ad($post->abstract) }}
                </span>
                    @endif
                </div>
            @endif
        </a>
    </div>
@endif
