@php
    $color = (isset($color) and $color) ? $color : "black"; // default color
    $showMoreColorClass = "link-$color";
    $itemsColorClass = "border-start-$color"
@endphp

@if(isset($title) and $title)
    @include('ehdafront::layouts.underlined_heading', [
        'text' => $title,
        'color' => $color
    ])
@endif
@if(isset($moreLink) and $moreLink)
    <a href="{{ $moreLink }}"
       class="floating-top-25 floating-end-15 {{ $showMoreColorClass }}">{{ trans('ehdafront::general.continue') }}</a>
@endif
<div class="media-list">
    @foreach($posts as $post)
        @include('ehdafront::home.posts_list_view_item')
    @endforeach
</div>