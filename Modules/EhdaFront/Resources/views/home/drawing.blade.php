<section id="gift">
    <div class="container">
        <div class="row">
            @include('ehdafront::home.drawing_event_image')
            <div class="col-sm-4">
                @include('ehdafront::home.drawing_form_section')
                @include('ehdafront::home.drawing_event_info')
            </div>
        </div>
    </div>
    <div class="event" style="background: transparent;"></div>
</section>