<div class="media-list-item {{ $itemsColorClass }}">
    <a href="{{ $post->direct_url ?: '#' }}">
        <div class="media-list-item-image">
            @php
                $thumbImage = fileManager()
                    ->file($post->featured_image)
                    ->posttype($posttype)
                    ->config('featured_image')
                    ->version('72x72')
                    ->getUrl();
            @endphp
            <img src="{{ $thumbImage }}" class="media-object">
        </div>
        <div class="media-list-item-body">
            <h5 class="media-list-item-heading">
                {{ ad($post->title) }}
            </h5>
            <p class="text-gray text-end">{{ ad(echoDate($post->published_at, 'j F y')) }}</p>
        </div>
    </a>
</div>