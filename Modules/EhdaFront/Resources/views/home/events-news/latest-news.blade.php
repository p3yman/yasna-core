@if ($specialNews->count())
    @include('ehdafront::layouts.underlined_heading', [
                        'text' => trans('ehdafront::general.special_news'),
                        'color' => 'blue',
                    ])
    
    <div {{--id="newsTabs"--}} class="latest-news owl-carousel fadeOut mt20 mb20">
        @foreach($specialNews as $post)
            @php
                $featuredImage = fileManager()
                    ->file($post->featured_image)
                    ->posttype($news_posttype)
                    ->config('featured_image')
                    ->version('960x360')
                    ->resolve()
            @endphp
            <div class="slide">
                @include('ehdafront::home.events-news.news-detail',[
                    "title" => $post->title ,
                    "date" => ad(echoDate($post->published_at, 'j F')) ,
                    "abstract" => $post->abstract ,
                    "link" => $post->direct_url ?: '#' ,
                    "src" => $featuredImage->getUrl() ,
                    "alt" => $featuredImage->getTitle() ,
                ])
            </div>
        @endforeach
    </div>
    
    @section('endOfBody')
        <script>
            $('document').ready(function () {
                $('.latest-news').owlCarousel({
                    @if(isLangRtl())
                    rtl: true,
                    @endif
                    items: 1,
                    loop: true,
                    nav: true,
                    navText: @if(isLangRtl()) ['<img src="{{ Module::asset('ehdafront:images/arrow-right.png') }}" alt="arrow">', '<img src="{{ Module::asset('ehdafront:images/arrow-left.png') }}" alt="arrow">'] @else ['<img src="{{ Module::asset('ehdafront:images/arrow-left.png') }}" alt="arrow">', '<img src="{{ Module::asset('ehdafront:images/arrow-right.png') }}" alt="arrow">'] @endif,
                    dots: false,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    autoplayHoverPause: true,
                    margin: 10,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 1
                        },
                        1200: {
                            items: 1
                        }
                    }
                })
            });
        </script>
    @append
@endif
