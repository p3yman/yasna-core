<div class="event">
	<div class="event-image">
		<img src="{{ $src }}" alt="{{ $alt }}">
	</div>
	<div class="event-detail">
		<h1>{{ $title }}</h1>
		<div class="date">
			<i class="fa fa-calendar"></i>
			@php $dateParts = [] @endphp
			@if(isset($start_date) and $start_date and ($start_date != '-'))
				@php $dateParts[] = $start_date @endphp
			@endif
			@if(isset($end_date) and $end_date and ($end_date != '-'))
				@php $dateParts[] = $end_date @endphp
			@endif
			{{ implode(' - ', $dateParts) }}
		</div>
		@if ($location)
			<div class="location">
				<i class="fa fa-map-marker"></i>
				{{ $location }}
			</div>
		@endif
		<a class="read-more" href="{{ $link }}">
			{{ trans('ehdafront::general.read_more') }}
			<span class="arrow -long @if(isLangRtl()) -rtl @endif"></span>
		</a>
	</div>
</div>