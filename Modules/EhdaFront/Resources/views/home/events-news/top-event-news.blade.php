 <!-- Top Events And News -->
<div class="row bg-semiWhite mb30 mt30">
    <div class="col-md-6">
        @include('ehdafront::home.events-news.latest-events')
    </div>
    <div class="col-md-6">
        @include('ehdafront::home.events-news.latest-news')
    </div>
</div>
 <!-- !END Top Events And News -->
