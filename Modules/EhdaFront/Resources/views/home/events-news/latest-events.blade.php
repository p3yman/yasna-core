@if ($eventsSlideShow->count())
	@include('ehdafront::layouts.underlined_heading', [
						'text' => trans('ehdafront::general.running_events'),
						'color' => 'green',
					])
	<div id="eventsTabs" class="latest-events mt20 mb20">
		<tabs>
			@foreach($eventsSlideShow as $event)
				@php
					$featuredImage = fileManager()
						->file($event->featured_image)
						->posttype($events_posttype)
						->config('featured_image')
						->version('850x360')
						->resolve();
					$event->spreadMeta();
					$eventStart = ad(echoDate($event->event_starts_at, 'j F'))
				@endphp
				<tab tabid="{{ $event->hashid }}"
					 name="{{ ($eventStart and ($eventStart != '-'))
					 	? $eventStart
					 	: ad(echoDate($event->published_at, 'j F'))
					}}"
					 @if($loop->iteration === 1) :selected="true" @endif>
					@include('ehdafront::home.events-news.event-detail',[
						"title" => $event->title,
						"start_date" => $eventStart ,
						"end_date" => ad(echoDate($event->event_ends_at, 'j F')) ,
						"abstract" => $event->abstract ,
						"link" => $event->direct_url ?: '#' ,
						"src" => $featuredImage->getUrl() ,
						"alt" => $featuredImage->getTitle() ,
						"location" => $event->location ,
					])
				</tab>
			@endforeach
		</tabs>
	</div>
@endif
