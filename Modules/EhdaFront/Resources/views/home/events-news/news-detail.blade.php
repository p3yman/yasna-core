<div class="news">
    <div class="news-image">
        <img src="{{ $src }}" alt="{{ $alt }}">
    </div>
    <div class="news-detail">
        <h1>{{ $title }}</h1>
        <div class="date">
            <i class="fa fa-calendar"></i>
            {{ $date }}
        </div>
        <div class="abstract">
            <p>
                {{ $abstract }}
            </p>
        </div>
        <a class="read-more" href="{{ $link }}">
            {{ trans('ehdafront::general.read_more') }}
            <span class="arrow -long @if(isLangRtl()) -rtl @endif"></span>
        </a>
    </div>
</div>