@if($mainSlideShow and $mainSlideShow->count())
    @section('endOfBody')
        <script>
            $(document).ready(function () {
                $('.home-slider-new').owlCarousel({
                    @if(isLangRtl())
                    rtl: true,
                    @endif
                    items: 1,
                    loop:true,
                    nav:true,
                    navText: @if(isLangRtl()) ['<img src="{{ Module::asset('ehdafront:images/arrow-right.png') }}" alt="arrow">','<img src="{{ Module::asset('ehdafront:images/arrow-left.png') }}" alt="arrow">'] @else ['<img src="{{ Module::asset('ehdafront:images/arrow-left.png') }}" alt="arrow">','<img src="{{ Module::asset('ehdafront:images/arrow-right.png') }}" alt="arrow">'] @endif,
                    dots: false,
                    autoplay:false,
                    autoplayTimeout:5000,
                    autoplaySpeed: 1000,
                    autoplayHoverPause:true,
                    margin: 0,
                    responsive: {
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1200:{
                            items:1
                        }
                    }
                });
            })
        </script>
    @append
    
    <div class="row">
        <div class="owl-carousel home-slider-new">
            @foreach($mainSlideShow as $post)
                @include('ehdafront::home.main_carousel_item')
            @endforeach
        </div>
    </div>
@endif