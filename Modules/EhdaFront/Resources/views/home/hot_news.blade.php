<div class="owl-carousel news-slider" dir="ltr">
    @foreach($hotNews as $post)
        @include('ehdafront::home.hot_news_item')
    @endforeach
</div>
@include('ehdafront::home.hot_news_scripts')