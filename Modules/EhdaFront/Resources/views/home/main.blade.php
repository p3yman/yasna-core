@extends('ehdafront::layouts.frame')

@php $metaTags = [ 'title' => Template::implodePageTitle(Template::pageTitleSeparator()) ] @endphp
@include('ehdafront::layouts.meta_tags')
@include('ehdafront::layouts.open_graph_meta_tags')

@section('content')
    <div class="container-fluid">
        @include('ehdafront::home.main_carousel')
        @include('ehdafront::home.pending_organ_transplant')
        @include('ehdafront::home.events-news.top-event-news')
        @include('ehdafront::home.home_notes')
        @include('ehdafront::home.equation')
        @include('ehdafront::home.hot_links')
        @include('ehdafront::home.carousel_scripts')
    </div>
@endsection