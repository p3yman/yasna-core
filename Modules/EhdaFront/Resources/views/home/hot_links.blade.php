<div class="container">
    <div class="row mt15 mb20">
        <div class="col-xs-12 col-md-4">
            @if($events and $events->count())
                @include('ehdafront::home.posts_list_view', [
                    'title' => trans('ehdafront::general.events'),
                    'color' => 'darkGray',
                    'posts' => $events,
                    'posttype' => $events_posttype,
                    'moreLink' => EhdaFrontRouteTools::actionLocale('PostController@archive', [
                        'postType' => $events_posttype->slug,
                    ]),
                ])
            @endif
        </div>
        <div class="col-xs-12 col-md-4">
            @if($transplantNews and $transplantNews->count())
                @include('ehdafront::home.posts_list_view', [
                    'title' => trans('ehdafront::general.ehda_news'),
                    'color' => 'blue',
                    'posts' => $transplantNews,
                    'posttype' => $news_posttype,
                    'moreLink' => EhdaFrontRouteTools::actionLocale('PostController@archive', [
                        'postType' => $news_posttype->slug,
                        'folder' => 'iran',
                    ]),
                ])
            @endif
        </div>
        <div class="col-xs-12 col-md-4">
            @if($worldNews and $worldNews->count())
                @include('ehdafront::home.posts_list_view', [
                    'title' => trans('ehdafront::general.world_news'),
                    'color' => 'green',
                    'posts' => $worldNews,
                    'posttype' => $news_posttype,
                    'moreLink' => EhdaFrontRouteTools::actionLocale('PostController@archive', [
                        'postType' => $news_posttype->slug,
                        'folder' => 'world',
                    ]),
                ])
            @endif
        </div>
    </div>
</div>

