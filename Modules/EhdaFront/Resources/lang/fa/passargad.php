<?php
/**
 * Created by PhpStorm.
 * User: yasna
 * Date: 5/20/18
 * Time: 4:22 PM
 */

return
     [
          'inquiry'     => 'استعلام کارت اهدای عضو',
          'login_panel' => 'ورود به پنل کاربری',
          'code_melli'  =>
               [
                    'not_valid'      => 'کد ملی نادرست است.',
                    'not_registered' => 'کارت اهدای عضو برای این کدملی ثبت نشده است.',
                    'registered'     => 'کد ملی ثبت شده است',
               ],

          'register_title' => 'ثبت‌ نام کارت اهدای عضو',
          'register_btn'   => 'ثبت‌ نام کارت',
          'card_no'        => 'شماره عضویت',
          'redirect'       => 'صبر، در حال انتقال',
          'user-not-found' => 'کاربر مورد نظر پیدا نشد.',

          'credit'     => 'طراحی و اجرا توسط',
          'yasna_team' => 'یسنا‌تیم',

          'registered_success' => 'با موفقیت ثبت شد',
          'registered_fail'    => 'اشکال در ثبت',
          'inquiry_page'       => 'استعلام',

          'username_not_valid' => 'نام کاربری معتبر نیست.',
     ];
