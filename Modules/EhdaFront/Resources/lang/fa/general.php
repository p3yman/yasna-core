<?php

return [
     'persian'                                  => 'فارسی',
     'english'                                  => 'انگلیسی',
     'arabic'                                   => 'عربی',
     'login_register'                           => 'ورود / ثبت‌نام',
     'orders'                                   => 'سفارشات',
     'profile'                                  => 'ویرایش پروفایل',
     'setting'                                  => 'تنظیمات',
     'log_out'                                  => 'خروج',
     'home'                                     => 'خانه',
     'back_to_first_page'                       => 'بازگشت به صفحه‌ی نخست',
     'about'                                    => 'درباره ما',
     'products'                                 => 'محصولات',
     'posts'                                    => 'مطالب',
     'contact_us'                               => 'تماس با ما',
     'categories'                               => 'دسته‌ها',
     'categories_of'                            => 'دسته‌های',
     'show_products'                            => 'مشاهده محصولات',
     'drawing_code_register'                    => 'ثبت کد قرعه‌کشی',
     'check_code'                               => 'بررسی کد',
     'drawing_check_code_fail'                  => 'کد وارد شده معتبر نیست',
     'login'                                    => 'ورود',
     'not_member_register_now'                  => 'عضو نیستید؟ ثبت‌نام کنید',
     'register'                                 => 'ثبت نام',
     'member_login'                             => 'عضو سایت هستید؟ وارد شوید',
     'code_melli_already_exists'                => 'این کد ملی قبلا ثبت شده است.',
     'relogin'                                  => 'اطلاعات شما در سایت موجود است، لطفا وارد شوید.',
     'register_failed'                          => 'خطایی در ثبت نام رخ داده است، دوباره تلاش کنید.',
     'register_success'                         => 'ثبت نام با موفقیت انجام شد، منتظر بمانید.',
     'register_success_sms'                     => 'ثبت نام شما در «::site» با موفقیت انجام شد.',
     'register_success_email'                   => 'ثبت نام شما در «::site» با موفقیت انجام شد.',
     'register_code_success_sms'                => "سلام ::name جان،\n\rکد قرعه‌کشی شما با موفقیت ثبت شد.",
     'all_user_score'                           => 'امتیاز جمع‌آوری شده:',
     'edit_profile'                             => 'ویرایش پروفایل',
     'accepted_codes'                           => 'کدهای ثبت شده',
     'events'                                   => 'رویدادها',
     'running_events'                           => 'رویدادهای در جریان',
     'expired_events'                           => 'رویدادهای پیشین',
     'soon'                                     => 'به زودی',
     'add_comment'                              => 'ثبت نظر',
     'sex'                                      => 'جنسیت',
     'marital'                                  => 'وضعیت تاهل',
     'single'                                   => 'مجرد',
     'married'                                  => 'متاهل',
     'drawing_code_success_receive_please_wait' => 'کد مورد پذیرش است، چند لحظه تامل بفرمایید.',
     'drawing_code_fail_receive'                => 'کد تکراری می باشد.',
     'add_code'                                 => 'افزودن کد',
     'code'                                     => 'کد',
     'created_at'                               => 'تاریخ ثبت',
     'purchased_at'                             => 'تاریخ فاکتور',
     'price'                                    => 'مبلغ',
     'drawing_code_not_found'                   => 'تاکنون کدی ثبت نشده است.',
     'rials'                                    => 'ریال',
     'sort'                                     => 'مرتب‌سازی',
     'price_max_to_min'                         => 'قیمت زیاد » کم',
     'price_min_to_max'                         => 'قیمت کم » زیاد',
     'best_seller'                              => 'پرفروش‌ترین‌ها',
     'favorites'                                => 'محبوب‌ترین‌ها',
     'search'                                   => 'جست و جو',
     'search_for'                               => 'جست و جو برای «:text»',
     'toman'                                    => 'تومان',
     'from'                                     => 'از',
     'to'                                       => 'تا',
     'states'                                   => 'استان‌ها',
     'this_state_is_disabled'                   => 'این استان غیرفعال است',
     'states_entrance'                          => 'ورود استان‌ها',
     'select_state'                             => 'انتخاب استان',
     'select_your_intended_state'               => 'استان مورد نظر خود را انتخاب کنید',
     'gallery'                                  => 'گالری',
     'visual_gallery'                           => 'گالری تصویری',
     'news'                                     => 'اخبار',
     'hot_news'                                 => 'اخبار داغ',
     'special_news'                             => 'اخبار ویژه',
     'ehda_news'                                => 'اخبار اهدا',
     'world_news'                               => 'اخبار جهان',
     'faqs'                                     => 'سوالات متداول',
     'faq_not_found_ask_yours'                  => 'پاسخ سوال خود را پیدا نکرده اید؟ از ما بپرسید.',
     'read_more'                                => 'ادامه مطلب',
     'continue'                                 => 'ادامه',
     'more'                                     => 'بیشتر',
     'teammates'                                => 'کارمندان مجموعه',
     'no_result_found'                          => 'نتیجه‌ای یافت نشد.',
     'view_on_map'                              => 'مشاهده بر روی نقشه',
     'volunteers'                               => 'سفیران',
     'special_volunteers'                       => 'سفیران ویژه',
     'personal_information'                     => 'اطلاعات فردی',
     'educational_information'                  => 'اطلاعات تحصیلی',
     'welcome_message'                          => 'خوش آمدید',
     'archive'                                  => 'آرشیو',
     'short_link'                               => 'لینک کوتاه',
     'contact_info'                             => 'اطلاعات تماس',
     'home_info'                                => 'اطلاعات محل سکونت',
     'job_info'                                 => 'اطلاعات شغل',
     'supplementary_info'                       => 'اطلاعات تکمیلی',
     'your_activity'                            => 'درچه زمینه ای می توانید فعالیت کنید؟',
     'login_info'                               => 'اطلاعات ورود به سامانه',
     'download'                                 => 'دانلود',
     'print'                                    => 'چاپ',
     'share'                                    => 'اشتراک',
     'powered_by_yasna'                         => 'طراحی و اجرا: گروه یسنا',
     'powered_by_someone'                       => 'با افتخار قدرت گرفته از :someone',
     'community_branches'                       => 'شعب انجمن',
     'provinces_portals'                        => 'پرتال استان‌ها',
     'email_has_been_sent_automatically'        => 'توجه : این ایمیل به صورت خودکار فرستاده شده است ، بنابراین به آن پاسخ ندهید. ',
     'unnamed'                                  => 'بی‌نام',
     'download_links'                           => 'لینک‌های دانلود',
     'invoice_payment'                          => 'پرداخت فاکتور',
     'follow_us_in_social'                      => 'ما را در شبکه‌های اجتماعی دنبال کنید',
     'day'                                      => 'روز',
     'month'                                    => 'ماه',
     'year'                                     => 'سال',
     'useful_links'                             => 'لینک‌های مفید',
     'descending'                               => 'نزولی',
     'ascending'                                => 'صعودی',
     'publishing_time'                          => "زمان انتشار",
     'organization'                             => 'تشکیلات سازمانی',
     'ngo-foundations'                          => 'ارکان انجمن',

     "filter" => [
          'plural'   => 'فیلتر‌ها',
          'singular' => 'فیلتر',
          'discard'  => 'خالی‌کردن فیلتر‌ها',
          'apply'    => 'فیلتر کردن',
     ],

     "theme" => [
          "day"   => "قالب روز",
          "night" => "قالب شب",
     ],

     'tutorials' => [
          'singular' => 'آموزش',
          'plural'   => 'آموزش‎ها',
          'all'      => 'همه‌ی آموزش‎ها',
     ],

     "event" => [
          "singular"  => "رویداد",
          "plural"    => "رویداد‌ها",
          "all"       => "همه رویداد‌‌ها",
          "type"      => [
               "upcoming" => "آینده",
               "running"  => "جاری",
               "expired"  => "پایان‌یافته",
          ],
          "attendees" => [
               "singular" => "شرکت‌کننده",
               "plural"   => "شرکت‌کننده",
          ],
          "title"     => [
               "follow_event" => "این رویداد را دنبال کنید",
          ],
          "timer"     => [
               "day"     => "روز",
               "hours"   => "ساعت",
               "minutes" => "دقیقه",
               "seconds" => "ثانیه",
               "phrase"  => [
                    "expired"  => "رویداد به پایان رسیده است.",
                    "running"  => "تا پایان رویداد زمان باقیست.",
                    "upcoming" => "تا آغاز رویداد زمان باقیست.",
               ],
          ],
     ],

     "contact" => [
          "info"  => [
               "address" => "نشانی پستی",
               "phone"   => "تلفن",
               "email"   => "ایمیل",
               "socials" => "شبکه‌‌های اجتماعی",
          ],
          "title" => [
               "follow_us"    => "ما را در شبکه‌های اجتماعی دنبال کنید",
               "write_for_us" => "برای ما بنویسید",
          ],
          "form"  => [
               "field"  => [
                    "name"    => [
                         "title"       => "نام و نام‌ خانوادگی",
                         "placeholder" => "نام کامل خود را وارد کنید",
                         "js_error"    => "نام و نام خانوادگی را وارد کنید.",
                    ],
                    "email"   => [
                         "title"       => "ایمیل",
                         "placeholder" => "ایمیل خود را وارد کنید",
                         "js_error"    => "ایمیل را وارد کنید.",
                    ],
                    "mobile"  => [
                         "title"       => "شماره موبایل",
                         "placeholder" => "",
                         "js_error"    => "شماره موبایل ۱۱ رقمی را وارد کنید.",
                    ],
                    "subject" => [
                         "title"       => "عنوان",
                         "placeholder" => "عنوان پیام",
                         "js_error"    => "عنوان را وارد کنید.",
                    ],
                    "message" => [
                         "title"       => "متن پیام",
                         "placeholder" => "پیام خود را بنویسید...",
                         "js_error"    => "متن پیام را را وارد کنید.",
                    ],
               ],
               "button" => "ارسال پیام",
          ],
     ],

     "faq" => [
          "form" => [
               "field"  => [
                    "name"    => [
                         "title"       => "نام و نام‌ خانوادگی",
                         "placeholder" => "نام و نام‌ خانوادگی",
                         "error"       => "نام و نام خانوادگی را با حروف فارسی وارد کنید.",
                    ],
                    "email"   => [
                         "title"       => "ایمیل",
                         "placeholder" => "ایمیل",
                         "error"       => "ایمیل را به درستی وارد نمایید.",
                    ],
                    "mobile"  => [
                         "title"       => "تلفن همراه",
                         "placeholder" => "تلفن همراه",
                         "error"       => "تلفن همراه ۱۱ رقمی خود را وارد نمایید.",

                    ],
                    "subject" => [
                         "title"       => "موضوع",
                         "placeholder" => "موضوع",
                         "error"       => "عنوان را وارد نمایید.",
                    ],
                    "text"    => [
                         "title"       => "متن",
                         "placeholder" => "سوال خود را بپرسید...",
                         "error"       => "متن را به درستی وارد نمایید.",
                    ],
               ],
               "button" => "ارسال",
          ],
     ],

     "financial_support" => [
          "account_info"   => "شماره حساب انجمن اهدای عضو ایرانیان",
          "form-title"     => "برای حمایت از اهدا، این فرم را پر کنید.",
          "donation"       => [
               "title"    => "راه‌های حمایت از اهدا",
               "type"     => [
                    "offline" => "آفلاین",
                    "online"  => "آنلاین",
               ],
               "accounts" => [
                    "copy" => "کپی کن!",
               ],
          ],
          "call_to_action" => "حمایت از ما",
     ],

     'volunteer_section' => [
          'singular'                 => 'سفیر اهدای عضو',
          'plural'                   => 'سفیران اهدای عضو',
          'plural_short'             => 'سفیران',
          'section'                  => 'بخش سفیران اهدای عضو',
          'go_section'               => 'ورود به بخش سفیران اهدای عضو',
          'register'                 => 'ثبت نام سفیر',
          'register_success'         => 'ثبت نام سفیر با موفقیت انجام شد. به زودی جهت شرکت در جلسه‌ی توجیحی سفیران، با شما تماس گرفته خواهد شد.',
          'be'                       => 'سفیر شوید',
          'ask'                      => 'می‌خواهید سفیر شوید؟',
          'partial'                  => [
               'part1' => 'سفیر',
               'part2' => 'اهدای',
               'part3' => 'عضو',
          ],
          'special'                  => [
               'singular' => 'سفیر ویژه',
               'plural'   => 'سفیران مشاهیر',
          ],
          'register_success_message' => [
               'sms'   => "سلام :name، \n\r
ثبت نام سفیر با موفقیت انجام شد. به زودی جهت شرکت در جلسه‌ی توجیحی سفیران، با شما تماس گرفته خواهد شد.\n\r
:site",
               'email' => "سلام :name، \n\r
ثبت نام سفیر با موفقیت انجام شد. به زودی جهت شرکت در جلسه‌ی توجیحی سفیران، با شما تماس گرفته خواهد شد.\n\r
:site",
          ],

          "activities" => "فعالیت سفیران",

          "attributes" => [
               "first_name"         => "نام",
               "last_name"          => "نام خانوادگی",
               "hometown"           => "محل تولد",
               "birth_date"         => "تاریخ تولد",
               "occupation"         => "حوزه فعالیت",
               "explanation"        => "توضیحات",
               "related_activities" => "فعالیت‌های مرتبط",
          ],

     ],

     "annual_reports" => [
          "download" => "دانلود گزارش",
     ],

     "states_info" => [
          "website_address" => "آدرس سایت",
          "enter_website"   => "ورود به سایت",
          "contact_info"    => "اطلاعات تماس",
     ],


     'organ_donation_card_section' => [
          'singular'                 => 'کارت اهدای عضو',
          'print'                    => 'چاپ کارت اهدای عضو',
          'download'                 => 'ذخیره کارت اهدای عضو',
          'preview'                  => 'نمایش کارت اهدای عضو',
          'card'                     => 'کارت',
          'register'                 => 'ثبت نام کارت اهدای عضو',
          'user_card'                => 'کارت اهدای عضو :user',
          'partial'                  => [
               'part1' => 'کارت',
               'part2' => 'اهدای',
               'part3' => 'عضو',
          ],
          'types'                    => [
               'mini'   => [
                    'title'       => 'همراه',
                    'description' => 'فایل کم‌حجم پشت و روی کارت به آسانی قابل دریافت خواهد بود. اما برای چاپ گزینه‌ی مناسبی نیست.',
               ],
               'single' => [
                    'title'       => 'فقط رو',
                    'description' => 'فقط روی کارت با وضوح تصویری قابل قبول برای چاپ در این نسخه مهیا است.',
               ],
               'social' => [
                    'title'       => 'قابل اشتراک',
                    'description' => 'در این نسخه از کارت اهدای عضو اطلاعات محرمانه‌ی شما حذف شده و آماده‌ی هم‌رسانی در شبکه‌های اجتماعی است.',
               ],
               'full'   => [
                    'title'       => 'قابل چاپ',
                    'description' => 'این نسخه از کارت اهدای عضو، با وضوح تصویری بالا، بهترین گزینه برای چاپ با چاپ‌گرهای خانگی است.',
               ],
          ],
          'register_success_message' => [
               'sms'   => "سلام :name، \n\r
کارت اهدای عضو شما با شماره عضویت :membershipNumber در سایت اهدا ثبت گردید.\n\r
:site",
               'email' => [
                    'line1' => 'سلام :name، ',
                    'line2' => '    کارت اهدای عضو شما با شماره عضویت :membershipNumber در سایت اهدا ثبت گردید.',
               ],
          ],
     ],

     'angels' => [
          'plural'              => 'فرشتگان ماندگار',
          'singular'            => 'فرشته ماندگار',
          'not_found'           => 'فرشته‌ای با این نام یافت نشد.',
          'search'              => 'جست و جوی فرشته',
          'new'                 => 'ثبت نام خانواده‌های اهداکننده',
          'register_your_angel' => [
               'part1' => 'فرشته‌ی خود را از',
               'part2' => 'اینجا',
               'part3' => ' ثبت کنید.',
          ],
          'picture'             => [
               'title' => [
                    'plural'   => 'تصاویر اهداکنندگان',
                    'singular' => 'تصویر اهداکننده',
               ],
          ],
     ],

     'member_section' => [
          'profile_edit' => 'ویرایش پروفایل',
          'sign_in'      => 'ورود',
          'sign_out'     => 'خروج',
     ],

     'profile_phrases' => [
          'not_enough_information'   => 'اطلاعات پروفایل شما کافی نیست.',
          'complete_to_join_drawing' => 'اکنون کافی‌ست با تکمیل اطلاعات خود وارد فهرست قرعه‌کشی شود.',
          'welcome_user'             => ':user خوش آمدید.',
          'profile'                  => 'پروفایل',
          'user_profile'             => 'پروفایل :user',
     ],

     'file_types' => [
          'file'       => [
               'title'         => 'فایل',
               'dropzone_text' => 'فایل خود را اینجا رها کنید',
          ],
          'image'      => [
               'title'         => 'تصویری',
               'dropzone_text' => 'تصویر خود را اینجا رها کنید',
          ],
          'text'       => [
               'title'         => 'متنی',
               'dropzone_text' => 'فایل متنی خود را اینجا رها کنید',
          ],
          'video'      => [
               'title'         => 'ویدیویی',
               'dropzone_text' => 'ویدیو خود را اینجا رها کنید',
          ],
          'audio'      => [
               'title'         => 'صوتی',
               'dropzone_text' => 'فایل صوتی خود را اینجا رها کنید',
          ],
          'compressed' => [
               'title'         => 'فشرده',
               'dropzone_text' => 'فایل فشرده‌‌ی خود را اینجا رها کنید',
          ],
     ],

     'byte_units' => [
          'B'  => 'بایت',
          'KB' => 'کیلوبایت',
          'MB' => 'مگابایت',
          'GB' => 'گیگابایت',
          'TB' => 'ترابایت',
     ],

     'upload' => [
          'errors'   => [
               'size'   => 'حجم فایل بسیاز زیاد است.',
               'type'   => 'نوع فایل قابل قبول نیست.',
               'server' => 'فرایند آپلود فایل در سرور با ارور {{statusCode}} متوقف شد.',
               'limit'  => 'نمی‌توانید فایل دیگری آپلود کنید.',
          ],
          'statuses' => [
               'uploading' => 'در حال بارگزاری',
               'failed'    => 'ناموفق',
               'success'   => 'موفق',
          ],
     ],

     'footer' => [
          'copy_right' => 'تمامی حقوق برای ::site محفوظ است.',
          'created_by' => 'طراحی و اجرا: ',
          'yasna_team' => 'گروه یسنا',
     ],

     'messages' => [
          'you_are_volunteer'                        => 'شما هم‌اکنون سفیر اهدای عضو هستید.',
          'login_to_complete_volunteer_registration' => [
               'before_link' => 'جهت ثبت نام کارت اهدای عضو',
               'link_text'   => 'وارد',
               'after_link'  => 'شوید.',
          ],
          'you_are_card_holder'                      => 'شما کارت اهدای عضو دارید. لطفا از پروفایل خود جهت ثبت نام به عنوان سفیر اهدای عضو اقدام کنید.',
          'card_exists_with_this_code_melli'         => 'برای این کد ملی کارت اهدای عضو صادر شده است.',
          'please_login'                             => [
               'before_link' => 'لطفاً',
               'link_text'   => 'وارد',
               'after_link'  => 'شوید.',
          ],
          'unable_to_register_card'                  => 'امکان ثبت کارت وجود ندارد.',
          'unable_to_register_volunteer'             => 'امکان ثبت سفیر وجود ندارد.',
          'select_at_least_one_activity'             => 'لطفا حداقل یکی از فعالیت ها را انتخاب نمائید.',
          'login'                                    => 'وارد شوید.',
     ],

     'notes' => [
          'moments_are_important_to_save_life' => [
               'part1' => 'برای نجات',
               'part2' => 'زندگی',
               'part3' => 'لحظه‌ها مهم است!',
          ],
          'one_brain_dead_can_save_8_lives'    => 'هر فرد مرگ مغزی می‌تواند جان ۸ نفر را نجات دهد.',
          'organ_donation'                     => 'اهدای عضو',
          'life_donation'                      => 'اهدای زندگی',
          'follow_us_in_social'                => 'ما را در شبکه‌های اجتماعی دنبال کنید',
     ],

     "main-menu" => [
          'items'     => [
               'learn'   => 'دانستن',
               'will'    => 'خواستن',
               'achieve' => 'توانستن',
               'join'    => 'پیوستن',
          ],
          'sub_menus' => [
               "learn" => [
                    'general_educations'                => 'آموزش‌های عمومی',
                    'brain_death'                       => 'مرگ مغزی',
                    'organ_donation'                    => 'اهدای عضو',
                    'allocation'                        => 'تخصیص اعضا',
                    'organ_transplant'                  => 'پیوند اعضا',
                    'organ_transplant_history'          => 'تاریخچه پیوند',
                    'statistics'                        => 'آمار',
                    'professional_educations'           => 'آموزش‌های تخصصی',
                    'educations_courses'                => 'دوره‌ها',
                    'educations_video'                  => 'فیلم‌ها',
                    'educations_text'                   => 'متن‌ها',
                    'cultural'                          => 'فرهنگی',
                    'organ_donation_in_religion'        => 'اهدای عضو در دین',
                    'organ_donation_in_another_country' => 'اهدای عضو در کشور های دیگر',
                    'organ_donation_card'               => 'کارت اهدای عضو',
                    'faq'                               => 'سوالات رایج',
                    'iran_news'                         => 'اخبار ایران',
                    'iran_procurement'                  => 'فراهم آوری و پیوند',
                    'iran_transplant'                   => 'پیوند',
                    'provinces'                         => 'استان ها',
                    'internal-ngo'                      => 'داخلی انجمن',
                    'world_news'                        => 'اخبار جهان',
                    'world_procurement'                 => 'فراهم آوری و پیوند',
                    'world_transplant'                  => 'پیوند',

               ],

               "will" => [
                    'donations'                         => 'کمک های مالی',
                    'volunteers'                        => 'سفیران',
                    'organ_donation_volunteers'         => 'سفیران اهدای عضو',
                    'angels'                            => 'فرشتگان ماندگار',
                    'special_volunteers'                => 'سفیران ویژه',
                    'participation_in_the_notification' => 'مشارکت در اطلاع رسانی',
                    'supporters'                        => 'حامیان',
                    'you_say'                           => 'شما هم بگویید',
                    'your_works'                        => 'آثار شما',
                    'your_memories'                     => 'خاطرات شما',
                    'suggestions'                       => 'پیشنهادات و انتقادات',
               ],

               "achieve" => [
                    'about_us'             => 'درباره ما',
                    'activities'           => 'فعالیت ها',
                    'ngo_history'          => 'تاریخچه انجمن',
                    'board_of_directories' => 'هیات مدیره',
                    'board_of_trustees'    => 'هیات امنا',
                    'founding'             => 'هیات موسس',
                    'organizational_chart' => 'چارت سازمانی',
                    'statute'              => 'اساسنامه',
                    'tasks_goals'          => 'شرح وظایف، اهداف',
                    'committees'           => 'کمیته ها',
                    'gallery'              => 'گالری',
                    'pictures'             => 'تصویر',
                    'films'                => 'فیلم',
                    'photo_donors'         => 'فرشتگان ماندگار',
                    'shop'                 => 'فروشگاه',
                    'fonts'                => 'فونت‌ها',
                    'purchase_tracking'    => 'پیگیری خرید',
                    'contact_us'           => 'تماس با ما',
               ],
          ],
     ],

     'media' => [
          'playing' => 'در حال پخش',
     ],

     'gateway' => [
          'disabled' => 'درگاه غیر فعال است.',
     ],

     'report' => [
          "picture" => "گزارش تصویری",
          "video"   => "گزارش ویدیویی",
          "text"    => "گزارش متنی",
     ],

     'tag' => [
          'title'  => [
               'singular' => 'برچسب',
               'plural'   => 'برچسب‌ها',
          ],
          'posts'  => 'پست‌های برچسب',
          'search' => 'جست و جوی برچسب',
     ],

     "message" => [
          "success" => [
               "send_message" => "پیام شما با موفقیت ارسال شد.",
          ],
          "error"   => [
               "send_message"     => "پیام ارسال نشد.",
               "upload_one_image" => 'یک تصویر آپلود کنید.',
          ],
     ],

     "education" => [
          'contact' => 'تماس با بخش آموزش',
     ],

     "research" => [
          'contact' => 'تماس با بخش پژوهش',
     ],

     "social-work" => [
          'contact' => 'ارتباط با واحد مددکاری',
     ],

     "activities" => [
          "follow" => "این فعالیت را دنبال کنید.",
     ],

     "payment" => [
          "types" => [
               "shetab"  => "کارت به کارت",
               "deposit" => "شماره حساب",
               "sheba"   => "شماره شبا",
          ],

          "account" => [
               "owner" => [
                    "name" => "نام صاحب حساب ",
               ],
          ],

          "redirect_to_gateway" => "انتقال به درگاه",

          "message" => [
               "success" => [],
               "failure" => [
                    "cancelled" => "پرداخت از طرف کاربر لغو شد.",
                    "rejected"  => "پرداخت انجام نشد. در صورتی که طی این فرایند مبلغی از حساب شما کسر شده است، طی ۷۲ ساعت آینده به حساب شما باز خواهد گشت.",
               ],
               "general" => [
                    "tracking_no" => "شماره پیگیری شما: :trackingNo",
               ],
          ],
     ],

     "donation" => [
          "type"         => "نوع کمک",
          "invalid_type" => "نوع کمک به درستی وارد نشده است.",

          "message" => [
               "success" => [
                    "payment" => "پرداخت شما با موفقیت انجام شد. از حمایت شما متشکریم.",
               ],
               "failure" => [],
          ],
     ],


     "send_works" => [
          "title" => [
               "plural"   => "ارسال آثار",
               "singular" => "ارسال اثر",
          ],

          "files" => "فایل‌ها",

          "message" => [
               "failure" => [
                    "files_required" => "آپلود فایل الزامی است.",
               ],
          ],
     ],
];
