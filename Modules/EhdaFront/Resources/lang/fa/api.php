<?php
return [
    "manage" => [
        "message" => [
            "api-user-edit-password-hint"   => "در صورت وارد کردن جایگزین گذرواژه‌ی قبلی خواهد شد.",
            "portable-printer-configs-hint" => "به صورت json قابل ذخیره است.",
        ],

        "alert" => [
            "error"   => [
                "password-needed-for-new-client" => "برای تعریف رابط برنامه‌نویسی جدید گذرواژه الزامی است.",
            ],
            "success" => [
                "portable-printer-configs-save" => "تنظیمات چاپگر سیار با موفقیت ذخیر شدند.",
            ],
        ],

        "headline" => [
            "acceptable-ips"           => "آی‌پی‌های مجاز",
            "acceptable-ips-of-client" => "آی‌پی‌های مجاز :client",
            "delete-ip-of-client"      => "حذف آی‌پی «:ip» رابط برنامه‌نویسی «:client»",
            "ip"                       => "آی‌پی",
            "portable-printer-configs" => "تنظیمات چاپگر سیار",
        ],
    ],


    "password" => [
        "reset" => [
            "notification" => [
                "sms"   => "سلام،\n\rگذرواژه‌ی جدید شما: :password",
                "email" => "سلام،\n\rگذرواژه‌ی جدید شما: :password",
            ],
        ],
    ],
];
