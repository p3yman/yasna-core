<?php
return [
     "card" => [
          "type" => [
               "vertical"        => "عمودی",
               "horizontal"      => "افقی",
               "uncertain-ratio" => "به نسبت تصویر",
          ],
     ],
];
