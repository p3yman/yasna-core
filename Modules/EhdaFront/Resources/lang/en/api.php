<?php
return [
    "password" => [
        "reset" => [
            "notification" => [
                "sms"   => "Hello,\n\rYour New Password: :password",
                "email" => "Hello,\n\rYour New Password: :password",
            ],
        ],
    ],
];
