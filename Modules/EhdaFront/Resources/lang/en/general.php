<?php

return [
     'persian'                                  => 'Persian',
     'english'                                  => 'English',
     'arabic'                                   => 'Arabic',
     'login_register'                           => 'Register / Login',
     'orders'                                   => 'Orders',
     'profile'                                  => 'Edit profile',
     'setting'                                  => 'Setting',
     'log_out'                                  => 'Logout',
     'home'                                     => 'Home',
     'back_to_first_page'                       => 'Back to First Page',
     'about'                                    => 'About',
     'products'                                 => 'Products',
     'posts'                                    => 'Posts',
     'contact_us'                               => 'Contact us',
     'categories'                               => 'Categories',
     'categories_of'                            => 'Categories of',
     'show_products'                            => 'See Products',
     'drawing_code_register'                    => 'Register Drawing Code',
     'check_code'                               => 'Check Code',
     'drawing_check_code_fail'                  => 'The code you entered is not valid',
     'login'                                    => 'Login',
     'not_member_register_now'                  => 'Not a member? Register now',
     'register'                                 => 'Register',
     'member_login'                             => 'Are you member? Please Login',
     'code_melli_already_exists'                => 'The National Code already exist',
     'relogin'                                  => 'Your information is available on the site, please log in.',
     'register_failed'                          => 'Registration error occurred, please try again.',
     'register_success'                         => 'Registration was successful, wait.',
     'register_success_sms'                     => 'You have successfully registered in ::site',
     'register_success_email'                   => 'You have successfully registered in ::site',
     'register_code_success_sms'                => "Hello ::name,\n\rYour drawing code has successfully registered.",
     'all_user_score'                           => 'Points collected:',
     'edit_profile'                             => 'Edit profile',
     'accepted_codes'                           => 'Registered codes',
     'events'                                   => 'Events',
     'running_events'                           => 'Running Events',
     'expired_events'                           => 'Expired Events',
     'soon'                                     => 'Soon',
     'add_comment'                              => 'Add Comment',
     'sex'                                      => 'Gender',
     'marital'                                  => 'Marital status',
     'single'                                   => 'Single',
     'married'                                  => 'married',
     'drawing_code_success_receive_please_wait' => 'Code is accepted, think for a moment.',
     'drawing_code_fail_receive'                => 'Code is repeated.',
     'add_code'                                 => 'Add Code',
     'code'                                     => 'Code',
     'created_at'                               => 'Register date',
     'purchased_at'                             => 'Invoice date',
     'price'                                    => 'price',
     'drawing_code_not_found'                   => 'The code has not been registered so far.',
     'rials'                                    => 'Rials',
     'sort'                                     => 'Sort',
     'price_max_to_min'                         => 'Price Many > Low',
     'price_min_to_max'                         => 'Price Low > Many',
     'best_seller'                              => 'Best seller',
     'favorites'                                => 'Favorites',
     'search'                                   => 'Search',
     'search_for'                               => 'Search For ":text"',
     'toman'                                    => 'Toman',
     'from'                                     => 'from',
     'to'                                       => 'to',
     'states'                                   => 'States',
     'this_state_is_disabled'                   => 'This state is disabled.',
     'states_entrance'                          => 'States Entrance',
     'select_state'                             => 'Select State',
     'select_your_intended_state'               => 'Select your intended state.',
     'gallery'                                  => 'Gallery',
     'visual_gallery'                           => 'Visual Gallery',
     'news'                                     => 'News',
     'hot_news'                                 => 'Hot News',
     'special_news'                             => 'Special News',
     'ehda_news'                                => 'Organ Donation News',
     'world_news'                               => 'World News',
     'faqs'                                     => 'Faqs',
     'faq_not_found_ask_yours'                  => 'Didn\'t find your answer? Ask your question.',
     'read_more'                                => 'Read More',
     'continue'                                 => 'Continue',
     'more'                                     => 'More',
     'teammates'                                => 'Teammates',
     'no_result_found'                          => 'No Results Found',
     'view_on_map'                              => 'View on Map',
     'volunteers'                               => 'Volunteers',
     'special_volunteers'                       => 'Celebrities',
     'personal_information'                     => 'Personal Information',
     'educational_information'                  => 'Educational Information',
     'welcome_message'                          => 'Welcome',
     'archive'                                  => 'Archive',
     'short_link'                               => 'Short Link',
     'contact_info'                             => 'Contact Info',
     'home_info'                                => 'Home Info',
     'job_info'                                 => 'Job Info',
     'supplementary_info'                       => 'Supplementary Info',
     'your_activity'                            => 'What field can you do?',
     'login_info'                               => 'Login Info',
     'download'                                 => 'Download',
     'print'                                    => 'Print',
     'share'                                    => 'Share',
     'powered_by_yasna'                         => 'Powered by Yasna Team',
     'powered_by_someone'                       => 'Powered by :someone',
     'community_branches'                       => 'Community Branches',
     'provinces_portals'                        => 'Provinces Portals',
     'email_has_been_sent_automatically'        => 'Note: This email has been sent automatically. So don\'t reply to it.',
     'unnamed'                                  => 'Unnamed',
     'download_links'                           => 'Download Links',
     'invoice_payment'                          => 'Invoice Payment',
     'follow_us_in_social'                      => 'Follow us in social networks',
     'day'                                      => 'Day',
     'month'                                    => 'Month',
     'year'                                     => 'Year',
     'useful_links'                             => 'Useful Links',
     'descending'                               => 'Descending',
     'ascending'                                => 'Ascending',
     'publishing_time'                          => "Publishing Time",
     'organization'                             => 'Organization',
     'ngo-foundations'                          => 'NGO Foundations',

     "filter" => [
          'plural'   => 'Filters',
          'singular' => 'Filter',
          'discard'  => 'Discard filter',
          'apply'    => 'Apply filter',
     ],

     "theme" => [
          "day"   => "Day Theme",
          "night" => "Night Theme",
     ],

     'tutorials' => [
          'singular' => 'Tutorial',
          'plural'   => 'Tutorials',
          'all'      => 'All Tutorials',
     ],

     "event" => [
          "singular"  => "event",
          "plural"    => "events",
          "all"       => "All Events",
          "type"      => [
               "upcoming" => "Upcoming",
               "running"  => "Running",
               "expired"  => "Finished",
          ],
          "attendees" => [
               "singular" => "attendee",
               "plural"   => "attendees",
          ],
          "title"     => [
               "follow_event" => "Follow this event",
          ],
          "timer"     => [
               "day"     => "Day",
               "hours"   => "Hours",
               "minutes" => "Minutes",
               "seconds" => "Seconds",
               "phrase"  => [
                    "expired"  => "Event has finished.",
                    "running"  => "Time remaining to the end of the event.",
                    "upcoming" => "Time remaining to the start of the event.",
               ],
          ],
     ],

     "contact" => [
          "info"  => [
               "address" => "Postal Address",
               "phone"   => "Phone Number",
               "email"   => "Email",
               "socials" => "Social Networks",
          ],
          "title" => [
               "follow_us"    => "Follow us on social networks",
               "write_for_us" => "Write for us",
          ],
          "form"  => [
               "field"  => [
                    "name"    => [
                         "title"       => "Full Name",
                         "placeholder" => "Enter your full name",
                         "js_error"    => "Insert name.",
                    ],
                    "email"   => [
                         "title"       => "Email",
                         "placeholder" => "Enter your email",
                         "js_error"    => "Insert email.",
                    ],
                    "mobile"  => [
                         "title"       => "Mobile Number",
                         "placeholder" => "",
                         "js_error"    => "Insert mobile in 11 digits.",
                    ],
                    "subject" => [
                         "title"       => "Subject",
                         "placeholder" => "Message Subject",
                         "js_error"    => "Insert subject.",
                    ],
                    "message" => [
                         "title"       => "Message",
                         "placeholder" => "Write your message...",
                         "js_error"    => "Insert message.",
                    ],
               ],
               "button" => "Send Message",
          ],
     ],

     "faq" => [
          "form" => [
               "field"  => [
                    "name"    => [
                         "title"       => "Full Name",
                         "placeholder" => "Full Name",
                         "error"       => "Enter your name with Persian letters.",
                    ],
                    "email"   => [
                         "title"       => "Email",
                         "placeholder" => "Email",
                         "error"       => "Enter your email correctly.",
                    ],
                    "mobile"  => [
                         "title"       => "Mobile Number",
                         "placeholder" => "Mobile Number",
                         "error"       => "Enter you 11 digits mobile number.",
                    ],
                    "subject" => [
                         "title"       => "Subject",
                         "placeholder" => "Subject",
                         "error"       => "Enter a subject for your question.",
                    ],
                    "text"    => [
                         "title"       => "Question",
                         "placeholder" => "Ask your question...",
                         "error"       => "Enter the text correctly.",
                    ],
               ],
               "button" => "Send",
          ],
     ],

     "financial_support" => [
          "account_info"   => "Bank accounts of Iranian Society of Organ Donation",
          "form-title"     => "For donation fill this form.",
          "donation"       => [
               "title"    => "Supporting Methods",
               "type"     => [
                    "offline" => "Offline",
                    "online"  => "Online",
               ],
               "accounts" => [
                    "copy" => "Copy!",
               ],
          ],
          "call_to_action" => "Support Us",
     ],

     "annual_reports" => [
          "download" => "Download Report",
     ],

     "states_info" => [
          "website_address" => "Website Address",
          "enter_website"   => "Enter Website",
          "contact_info"    => "Contact Information",
     ],

     'volunteer_section' => [
          'singular'                 => 'Organ Donation Volunteer',
          'plural'                   => 'Organ Donation Volunteers',
          'plural_short'             => 'Volunteers',
          'section'                  => 'Organ Donation Volunteers Section',
          'go_section'               => 'Go to Organ Donation Volunteers Section',
          'register'                 => 'Register Volunteer',
          'register_success'         => 'Volunteer registered successfully.',
          'be'                       => 'Be Volunteer',
          'ask'                      => 'Would you like to become a volunteer?',
          'partial'                  => [
               'part1' => 'Organ',
               'part2' => 'Donation',
               'part3' => 'Volunteer',
          ],
          'special'                  => [
               'singular' => 'Celebrity',
               'plural'   => 'Celebrities',
          ],
          'register_success_message' => [
               'sms'   => "Hello :name، \n\r
Volunteer has registered successfully. You'll be called soon for briefing.\n\r
:site",
               'email' => "Hello :name، \n\r
Volunteer has registered successfully. You'll be called soon for briefing.\n\r
:site",
          ],
          "activities"               => "Volunteers's Activities",

          "attributes" => [
               "first_name"         => "Name",
               "last_name"          => "Last Name",
               "hometown"           => "Hometown",
               "birth_date"         => "Birth Date",
               "occupation"         => "Occupation",
               "explanation"        => "Explanation",
               "related_activities" => "Related Activities",
          ],
     ],

     'organ_donation_card_section' => [
          'singular'                 => 'Organ Donation Card',
          'print'                    => 'Print Organ Donation Card',
          'download'                 => 'Save Organ Donation Card',
          'preview'                  => 'Preview Organ Donation Card',
          'card'                     => 'Card',
          'register'                 => 'Register Organ Donation Card',
          'user_card'                => 'Organ Donation Card of :user',
          'partial'                  => [
               'part1' => 'Organ',
               'part2' => 'Donation',
               'part3' => 'Card',
          ],
          'types'                    => [
               'mini'   => [
                    'title'       => 'Mini',
                    'description' => 'Small Size File',
               ],
               'single' => [
                    'title'       => 'Single',
                    'description' => 'Single Side',
               ],
               'social' => [
                    'title'       => 'Social',
                    'description' => 'Shareable File',
               ],
               'full'   => [
                    'title'       => 'Print',
                    'description' => 'Ready for Print',
               ],
          ],
          'register_success_message' => [
               'sms'   => "Hello :name، \n\r
Your Oragn Donation Card has been registered with :membershipNumber membership number.\n\r
:site",
               'email' => [
                    'line1' => 'Hello :name,',
                    'line2' => 'Your Oragn Donation Card has been registered with :membershipNumber membership number.',
               ],
          ],
     ],

     'angels' => [
          'plural'              => 'Angels',
          'singular'            => 'Angel',
          'not_found'           => 'Angel not found.',
          'search'              => 'Search Angel',
          'new'                 => 'Register Donor Families',
          'register_your_angel' => [
               'part1' => 'Register your angel',
               'part2' => 'here',
               'part3' => '.',
          ],
          'picture'             => [
               'title' => [
                    'plural'   => 'Donors\' Pictures',
                    'singular' => 'Donor\'s Picture',
               ],
          ],
     ],

     'member_section' => [
          'profile_edit' => 'Edit Profile',
          'sign_in'      => 'Sing In',
          'sign_out'     => 'Sign Out',
     ],

     'profile_phrases' => [
          'not_enough_information'   => 'Not Enough Profile Information',
          'complete_to_join_drawing' => 'Now you can join the drawing by completing your profile.',
          'welcome_user'             => 'Welcome :user.',
          'profile'                  => 'Profile',
          'user_profile'             => ':user Profile',
     ],

     'file_types' => [
          'file'       => [
               'title'         => 'File',
               'dropzone_text' => 'Drop your file here.',
          ],
          'image'      => [
               'title'         => 'Image',
               'dropzone_text' => 'Drop your image here.',
          ],
          'text'       => [
               'title'         => 'Text',
               'dropzone_text' => 'Drop your text file here.',
          ],
          'video'      => [
               'title'         => 'Video',
               'dropzone_text' => 'Drop your video file here.',
          ],
          'audio'      => [
               'title'         => 'Audio',
               'dropzone_text' => 'Drop your audio file here.',
          ],
          'compressed' => [
               'title'         => 'Compressed',
               'dropzone_text' => 'Drop your compressed file here.',
          ],
     ],

     'byte_units' => [
          'B'  => 'B',
          'KB' => 'KB',
          'MB' => 'MB',
          'GB' => 'GB',
          'TB' => 'TB',
     ],

     'upload' => [
          'errors'   => [
               'size'   => 'File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.',
               'type'   => 'You can\'t upload files of this type.',
               'server' => 'Server responded with {{statusCode}} code.',
               'limit'  => 'You can not upload any more files.',
          ],
          'statuses' => [
               'uploading' => 'Uploading',
               'failed'    => 'Failed',
               'success'   => 'Success',
          ],
     ],

     'footer' => [
          'copy_right' => 'All rights reserved for ::site.',
          'created_by' => 'Prepared by: ',
          'yasna_team' => 'Yasna Team',
     ],

     'messages' => [
          'you_are_volunteer'                        => 'You are volunteer.',
          'login_to_complete_volunteer_registration' => [
               'before_link' => '',
               'link_text'   => 'Login',
               'after_link'  => 'to register as organ donation volunteer.',
          ],
          'you_are_card_holder'                      => 'You have organ donation card. Please try to register as Organ Donation Volunteer from your profile.',
          'card_exists_with_this_code_melli'         => 'There is an Organ Donation Card for this code melli.',
          'please_login'                             => [
               'before_link' => 'Please',
               'link_text'   => 'Login',
               'after_link'  => '.',
          ],
          'unable_to_register_card'                  => 'Unable to Register Card',
          'unable_to_register_volunteer'             => 'Unable to Register Volunteer',
          'select_at_least_one_activity'             => 'Please select at least one activity.',
          'login'                                    => 'Login.',
     ],

     'notes' => [
          'moments_are_important_to_save_life' => [
               'part1' => 'Every moment is important to save',
               'part2' => 'life',
               'part3' => '',
          ],
          'one_brain_dead_can_save_8_lives'    => 'One brain dead can save 8 lives.',
          'organ_donation'                     => 'Organ Donation',
          'life_donation'                      => 'Life Donation',
          'follow_us_in_social'                => 'Follow us in social networks.',
     ],

     "main-menu" => [
          'items' => [
               'learn'   => 'to Know',
               'will'    => 'to Desire',
               'achieve' => 'to Do',
               'join'    => 'to Join',
          ],

          'sub_menus' => [
               "learn" => [
                    'general_educations'                => 'General Educations',
                    'brain_death'                       => 'Brain Death',
                    'organ_donation'                    => 'Organ Donation',
                    'allocation'                        => 'Organ Allocation',
                    'organ_transplant'                  => 'Organ Transplant',
                    'organ_transplant_history'          => 'Organ Transplant History',
                    'statistics'                        => 'Statistics',
                    'professional_educations'           => 'Professional Educations',
                    'educations_courses'                => 'Courses',
                    'educations_video'                  => 'Videos',
                    'educations_text'                   => 'Texts',
                    'cultural'                          => 'Cultural',
                    'organ_donation_in_religion'        => 'Organ Donation and Religion',
                    'organ_donation_in_another_country' => 'Organ Donation in Other Countries',
                    'organ_donation_card'               => 'Organ Donation Card',
                    'faq'                               => 'FAQ',
                    'iran_news'                         => 'Iran News',
                    'iran_procurement'                  => 'Organ Donation and Transplant',
                    'iran_transplant'                   => 'Transplant',
                    'provinces'                         => 'States',
                    'internal-ngo'                      => 'Internal',
                    'world_news'                        => 'World News',
                    'world_procurement'                 => 'Organ Donation and Transplant',
                    'world_transplant'                  => 'Transplant',

               ],

               "will" => [
                    'donations'                         => 'Funding',
                    'volunteers'                        => 'Volunteers',
                    'organ_donation_volunteers'         => 'Organ Donation Volunteers',
                    'angels'                            => 'Angels',
                    'special_volunteers'                => 'Celebrities',
                    'participation_in_the_notification' => 'Awareness Rising',
                    'supporters'                        => 'Supporters',
                    'you_say'                           => 'You Say',
                    'your_works'                        => 'Your Works',
                    'your_memories'                     => 'Your Memories',
                    'suggestions'                       => 'Suggestions',
               ],

               "achieve" => [
                    'about_us'             => 'About Us',
                    'activities'           => 'Activities',
                    'ngo_history'          => 'Background',
                    'board_of_directories' => 'Board of Directors',
                    'board_of_trustees'    => 'Board of Trustees',
                    'founding'             => 'Funding Board',
                    'organizational_chart' => 'Chart',
                    'statute'              => 'Statue',
                    'tasks_goals'          => 'Duties',
                    'committees'           => 'Committees',
                    'gallery'              => 'Gallery',
                    'pictures'             => 'Pictures',
                    'films'                => 'Videos',
                    'photo_donors'         => 'Lasting Angels',
                    'shop'                 => 'Shop',
                    'fonts'                => 'Fonts',
                    'purchase_tracking'    => 'Purchase Tracking',
                    'contact_us'           => 'Contact Us',

               ],
          ],
     ],

     'media' => [
          'playing' => 'Playing',
     ],

     'gateway' => [
          'disabled' => 'Gateway disabled.',
     ],

     'report' => [
          "picture" => "Picture Report",
          "video"   => "Video Report",
          "text"    => "Text Report",
     ],

     'tag' => [
          'title'  => [
               'singular' => 'Tag',
               'plural'   => 'Tags',
          ],
          'posts'  => 'Tag Posts',
          'search' => 'Search Tag',
     ],

     "message" => [
          "success" => [
               "send_message" => "Message sent successfully.",
          ],
          "error"   => [
               "send_message"     => "Unable to send message.",
               "upload_one_image" => 'Upload an image.',
          ],
     ],

     "education" => [
          'contact' => 'Contact Education Uni',
     ],

     "research" => [
          'contact' => 'Contact Research Uni',
     ],

     "social-work" => [
          'contact' => 'Contact Social Work Unit',
     ],

     "activities" => [
          "follow" => "Follow this activity.",
     ],

     "payment" => [
          "types" => [
               "shetab"  => "Shetab",
               "deposit" => "Deposit",
               "sheba"   => "IBAN",
          ],

          "account" => [
               "owner" => [
                    "name" => "Owner Name",
               ],
          ],

          "redirect_to_gateway" => "Redirect to Gateway",

          "message" => [
               "success" => [],
               "failure" => [
                    "cancelled" => "Payment has been canceled by client.",
                    "rejected"  => "Payment failed. If during this process your account has lost any credit, it will be refunded in 72 hours.",
               ],
               "general" => [
                    "tracking_no" => "Your Tracking Number: :trackingNo",
               ],
          ],
     ],

     "donation" => [
          "type"         => "Donation Type",
          "invalid_type" => "Invalid Donation Type.",

          "message" => [
               "success" => [
                    "payment" => "Payment succeeded. Thanks for your donation.",
               ],
               "failure" => [],
          ],
     ],


     "send_works" => [
          "title" => [
               "plural"   => "Send Works",
               "singular" => "Send Work",
          ],

          "message" => [
               "failure" => [
                    "files_required" => "Uploading file(s) is required.",
               ],
          ],
     ],
];
