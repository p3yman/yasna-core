<?php
return [
     "card" => [
          "type" => [
               "vertical"        => "Vertical",
               "horizontal"      => "Horizontal",
               "uncertain-ratio" => "Image Ratio",
          ],
     ],
];
