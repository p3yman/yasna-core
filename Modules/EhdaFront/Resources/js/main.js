function farsiDigits(txt) {
    for (var i = 0; i < 10; i++)
        txt = (txt + '').replace(new RegExp(i + '', 'g'), String.fromCharCode((i + '').charCodeAt(0) + (0x06f0 - 0x0030)));
    return txt;
}

function twoDigits(number) {
    return number = ('0' + number).slice(-2);
}

function selectCity() {
    var ci       = '<option value="0">انتخاب کنید...</option>';
    var selectSt = $('#cbRegisterState');
    var selectCi = $('#cbRegisterCity');
    selectCi.html(ci);
    var city = window['city'];
    if (selectSt.val() > 0 && selectSt.val() < 32) {
        city = city[selectSt.val()];
        for (var c in city) {
            ci += '<option value="' + city[c].id + '">' + city[c].name + '</option>';
        }
        selectCi.html(ci);
    }
}

function selectEditCity(id) {
    var ci       = '<option value="0">انتخاب کنید...</option>';
    var selectSt = $('#cbRegisterState');
    var selectCi = $('#cbRegisterCity');
    selectCi.html(ci);
    var city = window['city'];
    //console.log(city);
    if (selectSt.val() > 0 && selectSt.val() < 32) {
        city = city[selectSt.val()];
        for (var c in city) {
            if (city[c].id == id) {
                ci += '<option value="' + city[c].id + '" selected="selected">' + city[c].name + '</option>';
            }
            else {
                ci += '<option value="' + city[c].id + '">' + city[c].name + '</option>';
            }
        }
        selectCi.html(ci);
    }
}

function moveHeartBeat() {
    if (!$('.heart-line').length) {
        return false;
    }
    var pointsArray = $('.heart-line polyline').attr('points').split(" ");
    var xArray      = [];
    var yArray      = [];
    var arrayLength = pointsArray.length;
    var points      = "";
    var x           = [];

    heartBeatInit();
    heartBeatIncrease();

    function heartBeatInit() {
        xArray = [];
        for (i = 0; i < arrayLength; i++) {
            var cordinate = pointsArray[i].split(",");
            xArray.push(eval(cordinate[0]));
            yArray.push(eval(cordinate[1]));
        }
        for (i = 0; i < arrayLength - 1; i++) {
            xArray[i] = xArray[i] - 110.473;
        }
    }

    function heartBeatIncrease() {

        for (i = 1; i < arrayLength - 1; i++) {
            xArray[i] = xArray[i] + 1;
        }

        if (xArray[2] >= 150) {
            heartBeatInit();
        }

        // Updating SVG tag
        for (i = 0; i < arrayLength; i++) {
            points = points + " " + xArray[i] + "," + yArray[i];
        }
        $('.heart-line polyline').attr('points', points);
        points = "";
        setTimeout(heartBeatIncrease, 10);
    }
}

function closeFilter() {
    $('.filter-sidebar').removeClass('opened');
}

function eventTimer(start, end, texts) {
// Set the date we're counting down to

    var days;
    var hours;
    var minutes;
    var seconds;
    var startDate = new Date(start).getTime();
    var endDate   = new Date(end).getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {
        TimerCount(startDate, endDate, texts);
    }, 1000);

    function TimerCount(start, end, texts) {
        var domEl   = "";
        var message = "";

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = start - now;
        var passTime = end - now;

        if (distance < 0 && passTime < 0) {
            message                                    = '<div calss="message">' + texts.expired + '</div>';
            document.getElementById("timer").innerHTML = message;
            clearInterval(x);
            return;

        } else if (distance < 0 && passTime > 0) {
            calcTime(passTime);
            message = '<div class="message">' + texts.running + '</div>';

        } else if (distance > 0 && passTime > 0) {
            calcTime(distance);
            message = '<div class="message">' + texts.upcoming + '</div>';
        }

        // Output the result in an element with id="demo"
        domEl = '<div class="day">' + ad(days.toString()) + '<span class="format" id="dayTime">' + texts.day + '</span></div><div class="hour">' + ad(hours.toString()) + '<span class="format" id="hourTime">' + texts.hours + '</span></div><div class="min">' + ad(minutes.toString()) + '<span class="format" id="minTime">' + texts.minutes + '</span></div><div class="sec">' + ad(seconds.toString()) + '<span class="format" id="secTime">' + texts.seconds + '</span></div>' + message;


        document.getElementById("timer").innerHTML = domEl;

    }

    function calcTime(dist) {
        // Time calculations for days, hours, minutes and seconds
        days    = Math.floor(dist / (1000 * 60 * 60 * 24));
        hours   = Math.floor((dist % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        minutes = Math.floor((dist % (1000 * 60 * 60)) / (1000 * 60));
        seconds = Math.floor((dist % (1000 * 60)) / 1000);
    }
}

/*var openMenu = null;
 $(document).on('click', function (event) {
 if (openMenu && $(event.target) != openMenu && !$(event.target).parents('.has-child').length) {
 openMenu.slideUp(500, function () {
 openMenu.parents('.has-child').first().removeClass('active');
 openMenu = null;
 })
 }
 });
 $(document).on('click', '.has-child', function (event) {
 var elm = null;
 let target = $(event.target);
 if (target.is('.has-child')) {
 elm = target;
 } else {
 if (target.closest('a').length) {
 target = target.closest('a');
 }
 }
 if (target.is('a') && target.parent().is('.has-child')) {
 elm = target.parent();
 }
 if (elm) {
 event.preventDefault();
 elm.addClass('active');
 elm.siblings().removeClass('active').find('>ul').slideUp();
 elm.find('>ul').stop(true, false).slideToggle({
 done: function (e) {
 if ($(e.elem).css('display') != 'none') {
 openMenu = $(e.elem);
 openMenu.parents('.has-child').first().addClass('active');
 } else {
 openMenu.parents('.has-child').first().removeClass('active');
 openMenu = null;
 }
 }
 });
 }
 });*/


$(document).ready(function () {
    moveHeartBeat();

    $('[data-toggle="tooltip"]').tooltip();
    $(window).on('ready scroll',function () {
        var winTop         = $(window).scrollTop();
        var mainMenuHeight = $('.main-menu').outerHeight();
        if (winTop >= $('.top-bar').outerHeight()) {
            // $('.main-menu').height(mainMenuHeight);
            $('body').css('margin-top', mainMenuHeight);
            $("body").addClass("sticky-header");
        } else {
            $("body").removeClass("sticky-header");
            $('body').css('margin-top', 0);
            // $('.main-menu').css('height', 'auto');
        }
        if (winTop >= 300) {
            if ($('.go-to-top').length == 0) {
                $('body').append('<a href="#" class="go-to-top"><i class="icon icon-down rotate-180"></i></a>');
            }
            $('.go-to-top').stop(true, false).fadeIn();
        } else {
            $('.go-to-top').stop(true, false).fadeOut(200, function () {
                $(this).remove()
            })
        }
    });
    $(document).on('click', '.go-to-top', function (event) {
        event.preventDefault();
        $("html, body").animate({scrollTop: 0});
    });
    $('#current-members h3').fitText(2.5, {maxFontSize: '50px'});
    $('#home-notes h3').fitText(2.5, {maxFontSize: '30px'});
    // $('body').css('margin-bottom', $('body>footer').outerHeight());
    $(window).resize(function () {
        $('.timers .timer span').css('line-height', $('.timers .circle').first().height() + 'px');
        // $('body').css('margin-bottom', $('body>footer').outerHeight());
    });
    $('#chRegisterAll').on('change', function () {
        if (this.checked) {
            $(this).parent().parent().find('[type="checkbox"]').prop('checked', true);
        } else {
            $(this).parent().parent().find('[type="checkbox"]').prop('checked', false);
        }
    });
    $('.body-items [type="checkbox"]:not(#chRegisterAll)').on('change', function () {
        if ($('.body-items [type="checkbox"]:not(#chRegisterAll)').length == $('.body-items [type="checkbox"]:not(#chRegisterAll):checked').length) {
            $('#chRegisterAll').prop('checked', true);
        } else {
            $('#chRegisterAll').prop('checked', false);
        }
    });
    // $(window).trigger('resize');
    // $(window).on('resize', function () {
    //     $(window).trigger('scroll');
    //     if ($(window).width() > 999) {
    //         $('#menu-tree').removeAttr('style');
    //     }
    // });

    /*$('.toggle-menu').click(function () {
     let menuEl = $('#menu-tree');

     if ($('.main-menu')[0].style.height != 'auto') {
     $('.main-menu').css('height', 'auto');
     }

     if (menuEl.is(':visible')) {
     menuEl.slideUp();
     } else {
     menuEl.slideDown();
     }
     });*/

    $('.globe-list-btn').click(function (e) {
        e.preventDefault();
        let listEl = $('.globe-list');
        if (listEl.is(':visible')) {
            listEl.slideUp(function () {
                listEl.removeAttr('style');
            });
        } else {
            listEl.slideDown();
        }
    });

    $('.globe-list-btn').blur(function () {
        let listEl = $('.globe-list');
        if (listEl.is(':visible')) {
            listEl.slideUp(function () {
                listEl.removeAttr('style');
            });
        }
    });

    /*
     *-------------------------------------------------------
     * Js Updates
     *-------------------------------------------------------
     */

    // Open User Profile List
    $('.js-profileList').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('active');
        $(this).children('ul').slideToggle();
    });
    $('.js-profileList ul').on('click', function (e) {
        e.stopPropagation();
    });
    // Opens Menu (sidebar mode)
    $('.js-openMenu').on('click', function (e) {
        e.stopPropagation();
        $('body').addClass('menu-opened');
    });
    // Closes Menu (sidebar mode)
    $('.js-closeMenu').on('click', function () {
        $('body').removeClass('menu-opened');
    });
    // Add active Class to selected menu -> css opens submenu
    $('.sub-menu').on('click',function (e) {
        e.stopPropagation();
    });
    var openMenu = null;
    $('.js-submenu-toggle .has-child').on('click', function (e) {
        e.stopPropagation();
        if($(e.target).is('a')){
            e.preventDefault();
        }
        var $this = $(this);
        if (openMenu && !$this.is(openMenu)) {
            $('.js-submenu-toggle .active').removeClass('active');
        }
        $this.toggleClass('active');
        openMenu = $this;
    });
    // Show Search box
    $('.js-openSearch').on('click', function () {
        $('.search-box').fadeIn();
    });
    // Hide Search box
    $('.js-closeSearch').on('click', function (e) {
        e.stopPropagation();
        $('.search-box').fadeOut();
    });
    // Closes menu and drop downs on blur
    $(document).on('click', function () {
        if ($('.js-profileList.active').length) {
            $('.js-profileList').removeClass('active')
                .children('ul').slideUp();
        }
        if($('.js-submenu-toggle .active').length){
            $('.js-submenu-toggle .active').removeClass('active');
        }
    });

    if ($('.js-filter-opener').length) {
        $('.js-filter-opener').on('click', function () {
            $('.filter-sidebar').addClass('opened');
        });
        $('.js-close-filter').on('click', closeFilter);
        $('.js-refreshFilter').on('click', closeFilter);
        $('.js-applyFilter').on('click', closeFilter);
    }

    if ($('tabs').length) {
        new Vue({
            el: "#eventsTabs",
        });
    }

    $('.js-themeToggle').on('click', function () {
        $('.video-wrapper').toggleClass('light-theme');
        $(this).find('.mode').toggleClass('noDisplay');
    });

    if ($('Timer').length) {
        new Vue({
            el: "#timer"
        });
    }

    $('.organization-box').on('click',function () {
        let id = $(this).data('id');
        let content = $("#"+id);
        window.location.hash = id;
        $('.organization').removeClass('active');
        content.addClass('active');
        content.scrollToView(-150,1000);
    });

    if (window.location.hash) {
        let organizationBox = $('.organization-box[data-id=' + window.location.hash.substr(1) + ']');
        if (organizationBox.length) {
            organizationBox.click();
        }
    }
});
(function ($) {
    $.fn.fitText = function (kompressor, options) {
        // Setup options
        var compressor = kompressor || 1,
            settings   = $.extend({
                'minFontSize': Number.NEGATIVE_INFINITY,
                'maxFontSize': Number.POSITIVE_INFINITY
            }, options);
        return this.each(function () {
            // Store the object
            var $this   = $(this);
            // Resizer() resizes items based on the object width divided by the compressor * 10
            var resizer = function () {
                $this.css('font-size', Math.max(Math.min($this.width() / (compressor * 10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
            };
            // Call once to set.
            resizer();
            // Call on resize. Opera debounces their resize by default.
            $(window).on('resize.fittext orientationchange.fittext', resizer);
        });
    };
})(jQuery);


/*
 *-------------------------------------------------------
 * Vue Js
 *-------------------------------------------------------
 */

// Tabs component
Vue.component('tabs', {
    template: '<div class="tabs-container"><div class="tabs"><ul><li v-for="tab in tabs" :class="{\'is-active\': tab.isActive }"><a href="javascript:void(0);" @click="selectNewTab(tab)">{{ tab.name }}</a></li></ul></div><div class="tab-data-view"><slot></slot></div></div>',
    data    : function () {
        return {
            tabs: [],
            i   : 0,
            cacheInterval: ""
        }
    },

    created: function () {
        this.tabs = this.$children;
    },
    mounted: function () {
        let tabsVue = this;
        tabsVue.cacheInterval = setInterval(function () {
            tabsVue.changeToNextTab();
        }, 5000);
    },
    methods: {
        changeToNextTab: function () {
            if (!this.tabs.length) {
                return;
            }

            let activeIndex = this.tabs.findIndex(function (tab) {
                return tab.isActive;
            });

            if (activeIndex === -1) {
                activeIndex = 0;
            }

            // add one to the index for the next tab
            activeIndex++;

            if (activeIndex >= this.tabs.length) {
                // Reset to first tab if on the last tab
                activeIndex = 0;
            }

            this.selectNewTab(this.tabs[activeIndex]);

        },
        selectNewTab   : function (slectedTab) {
            this.tabs.forEach(function (tab) {
                tab.isActive = (tab.tabid === slectedTab.tabid)
            });
            let tabsVue = this;
            clearInterval(tabsVue.cacheInterval);
            tabsVue.cacheInterval = setInterval(function () {
                tabsVue.changeToNextTab();
            }, 5000);
        },

    }
});

Vue.component('tab', {
    template: '<transition name="fade" mode="out-in"><div class="data-box" v-show="isActive"><slot></slot></div></transition>',
    props   : {
        name    : {required: true},
        tabid   : {required: true},
        selected: {defaultValue: false}
    },

    data: function () {
        return {
            isActive: false
        }
    },

    mounted : function () {
        this.isActive = this.selected;
    },
    computed: {
        href: function () {
            return "#" + this.tabid.toLowerCase().replace(/ /g, '-');
        }
    }

});

Vue.component("Timer", {
    template: '<div><div v-show="statusType !== \'expired\'"><div class="day"><span class="number">{{ days.toString() }}</span><div class="format">{{ wordString.day }}</div></div><div class="hour"><span class="number">{{ hours }}</span><div class="format">{{ wordString.hours }}</div></div><div class="min"><span class="number">{{ minutes }}</span><div class="format">{{ wordString.minutes }}</div></div><div class="sec"><span class="number">{{ seconds }}</span><div class="format">{{ wordString.seconds }}</div></div></div><div class="message">{{ message }}</divdiv><div class="status-tag" :class="statusType">{{ statusText }}</div></div>',
    props   : ["starttime", "endtime", "trans"],
    data    : function data() {
        return {
            wordString: {},
            timer     : "",
            start     : "",
            end       : "",
            interval  : "",
            days      : "",
            minutes   : "",
            hours     : "",
            seconds   : "",
            message   : "",
            statusType: "",
            statusText: ""
        };
    },
    created : function () {
        this.wordString = JSON.parse(this.trans);
    },

    mounted: function mounted() {
        var _this = this;

        this.start = new Date(this.starttime).getTime();
        this.end   = new Date(this.endtime).getTime();

        _this.timerCount(_this.start, _this.end);
        // Update the count down every 1 second
        this.interval = setInterval(function () {
            _this.timerCount(_this.start, _this.end);
        }, 1000);
    },

    methods: {
        timerCount: function timerCount(start, end) {
            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = start - now;
            var passTime = end - now;

            if (distance < 0 && passTime < 0) {
                this.message    = this.wordString.expired;
                this.statusType = "expired";
                this.statusText = this.wordString.status.expired;
                clearInterval(this.interval);
                return;
            } else if (distance < 0 && passTime > 0) {
                this.calcTime(passTime);
                this.message    = this.wordString.running;
                this.statusType = "running";
                this.statusText = this.wordString.status.running;
            } else if (distance > 0 && passTime > 0) {
                this.calcTime(distance);
                this.message    = this.wordString.upcoming;
                this.statusType = "upcoming";
                this.statusText = this.wordString.status.upcoming;
            }
        },
        calcTime  : function calcTime(dist) {
            // Time calculations for days, hours, minutes and seconds
            this.days    = ad(Math.floor(dist / (1000 * 60 * 60 * 24)).toString());
            this.hours   = ad(Math.floor((dist % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)).toString());
            this.minutes = ad(Math.floor((dist % (1000 * 60 * 60)) / (1000 * 60)).toString());
            this.seconds = ad(Math.floor((dist % (1000 * 60)) / 1000).toString());
        }
    }
});