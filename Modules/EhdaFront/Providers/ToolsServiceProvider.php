<?php

namespace Modules\EhdaFront\Providers;

use Illuminate\Support\ServiceProvider;

class ToolsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }



    public static function getValue($variable, ...$closureParameters)
    {
        return is_closure($variable) ? $variable(...$closureParameters) : $variable;
    }



    public static function activeGendersCodes()
    {
        return ['1', '2', '3'];
    }



    public static function getActiveGendersCodes()
    {
        return self::activeGendersCodes();
    }



    public static function activeGendersTranses()
    {
        $codes   = self::activeGendersCodes();
        $transes = [];
        foreach ($codes as $code) {
            $transes[$code] = trans("ehdafront::forms.gender.$code");
        }
        return $transes;
    }



    public static function getActiveGendersTranses()
    {
        return self::activeGendersTranses();
    }



    public static function activeEduLevelsCodes()
    {
        return ['0', '1', '2', '3', '4', '5', '6'];
    }



    public static function getActiveEduLevelsCodes()
    {
        return self::activeEduLevelsCodes();
    }



    public static function activeEduLevelsTranses($transSlug = 'edu_level_full')
    {
        $codes   = self::activeEduLevelsCodes();
        $transes = [];
        foreach ($codes as $code) {
            $transes[$code] = trans("ehdafront::people.$transSlug.$code");
        }
        return $transes;
    }



    public static function getActiveEduLevelsTranses()
    {
        return self::activeEduLevelsTranses();
    }



    public static function activeFamiliarizationCodes()
    {
        return ['0', '1', '2', '3', '4'];
    }



    public static function getActiveFamiliarizationCodes()
    {
        return self::activeFamiliarizationCodes();
    }



    public static function activeFamiliarizationTranses()
    {
        $codes   = self::activeFamiliarizationCodes();
        $transes = [];
        foreach ($codes as $code) {
            $transes[$code] = trans("ehdafront::people.familiarization.$code");
        }
        return $transes;
    }



    public static function getStatesCombo($empty = false)
    {
        $data = array_map(function ($item) {
            return [
                 'id'    => $item[0],
                 'title' => $item[1],
            ];
        }, model('state')->combo());
        if ($empty) {
            array_unshift($data, ['id' => '', 'title' => '']);
        }
        return $data;
    }



    public static function getStatesComboHashid()
    {
        foreach (($states = self::getStatesCombo()) as $key => $state) {
            $states[$key]['originalId'] = $state['id'];
            $states[$key]['id']         = hashid_encrypt($state['id'], 'ids');
        }
        return $states;
    }



    public static function getEmailTemplatePath($templateName)
    {
        return "ehdafront::templates.email.$templateName";
    }



    /**
     * Checks if $code is a valid color code
     *
     * @param string $code
     *
     * @return bool
     */
    public static function validateColorCode($code)
    {
        if (is_string($code)) {
            if (!starts_with($code, '#')) {
                $code = '#' . $code;
            }


            if (preg_match('/^#[a-fA-F0-9]{6}$/i', $code)) {
                return $code;
            }
        }

        return false;
    }



    /**
     * Adds sub domain to a given url string.
     *
     * @param string $url
     * @param string $subDomain
     *
     * @return string
     */
    public static function addSubDomain($url, $subDomain)
    {
        $parts     = parse_url($url);
        $domain    = preg_replace('/^www\./', '', $parts['host']);
        $hotsParts = explode('.', $domain);
        $hotsParts = array_splice($hotsParts, -2); // Remove current sub domain
        array_unshift($hotsParts, $subDomain);
        $parts['host'] = implode('.', $hotsParts);
        return self::unparse_url($parts);
    }



    /**
     * Unparse url and returns a url string from a parsed url
     *
     * @param array $parsed_url
     * @param array $ommit
     *
     * @return string
     */
    public static function unparse_url($parsed_url, $ommit = [])
    {
        //From Open Web Analytics owa_lib.php

        $url = '';

        $p = [];

        $p['scheme'] = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';

        $p['host'] = isset($parsed_url['host']) ? $parsed_url['host'] : '';

        $p['port'] = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';

        $p['user'] = isset($parsed_url['user']) ? $parsed_url['user'] : '';

        $p['pass'] = isset($parsed_url['pass']) ? ':' . $parsed_url['pass'] : '';

        $p['pass'] = ($p['user'] || $p['pass']) ? $p['pass'] . "@" : '';

        $p['path'] = isset($parsed_url['path']) ? $parsed_url['path'] : '';

        $p['query'] = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';

        $p['fragment'] = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';

        if ($ommit) {
            foreach ($ommit as $key) {
                if (isset($p[$key])) {
                    $p[$key] = '';
                }
            }
        }

        return $p['scheme'] . $p['user'] . $p['pass'] . $p['host'] . $p['port'] . $p['path'] . $p['query'] . $p['fragment'];
    }
}
