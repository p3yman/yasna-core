<?php

namespace Modules\EhdaFront\Providers;

use Illuminate\Support\ServiceProvider;

class GalleryServiceProvider extends ServiceProvider
{
    protected static $visual_galleries = ['video', 'image'];
    protected static $posttypes_prefix = 'gallery-';


    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public static function getVisualGalleries()
    {
        return self::$visual_galleries;
    }



    public static function getPosttypesPrefix()
    {
        return self::$posttypes_prefix;
    }



    public static function typeToPosttype($type)
    {
        return self::getPosttypesPrefix() . $type;
    }



    public static function posttypeToType($posttype)
    {
        return str_after($posttype, self::getPosttypesPrefix());
    }



    public static function cardsTypes()
    {
        return ['vertical', 'horizontal', 'uncertain-ratio'];
    }



    public static function cartTypesCombo()
    {
        return array_map(function ($type) {
            return ['title' => trans("ehdafront::gallery.card.type.$type"), 'id' => $type];
        }, self::cardsTypes());
    }



    /**
     * If the $link belongs to a video of aparat, returns its hash id.
     * Else if it is any other string, returns the string self.
     * Else returns null.
     *
     * @param string $link
     *
     * @return null|string
     */
    public static function getAparatId($link)
    {
        if (is_string($link)) {
            $lower = strtolower($link);
            if (
                 starts_with($lower, 'https://www.aparat.com/v/') or
                 starts_with($lower, 'http://www.aparat.com/v/') or
                 starts_with($lower, 'https://aparat.com/v/') or
                 starts_with($lower, 'http://aparat.com/v/') or
                 starts_with($lower, 'www.aparat.com/v/') or
                 starts_with($lower, 'aparat.com/v/')
            ) {
                return str_after($link, 'v/');
            }
            return $link;
        }

        return null;
    }
}
