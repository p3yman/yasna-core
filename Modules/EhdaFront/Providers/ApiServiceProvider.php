<?php

namespace Modules\EhdaFront\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    const POSTTYPES_SLUGS = [
         'news'    => 'news',
         'events'  => 'event',
         'statics' => 'statics',
    ];

    const CATEGORIES_SLUGS = [
         'news' => [
              'broadcast' => 'broadcast',
         ],
    ];


    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    public function boot(Router $router)
    {
        Validator::extend('alpha_num_dash', function ($attribute, $value) {
            if (!is_string($value) && !is_numeric($value)) {
                return false;
            }

            return preg_match('/^[\pL\pM\pN\-]+$/u', $value) > 0;
        }, trans('ehdafront::validation.alpha_num_dash'));
    }



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * Slug of Api Clients' Default Role
     *
     * @return string
     */
    public static function apiClientRoleSlug()
    {
        return 'api';
    }
}
