<?php

namespace Modules\EhdaFront\Providers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;

class EmailServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function send($text, $to, $name, $subject, $template)
    {
        Mail::send('ehdafront::templates.email.' . $template, compact('text'), function ($m) use ($to, $name, $subject) {
            $m->from(env('MAIL_FROM'), get_setting('site_title'));

            $m->to($to, $name)
                ->subject($subject);
        });
    }
}
