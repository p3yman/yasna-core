<?php

namespace Modules\EhdaFront\Providers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Posttype;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Morilog\Jalali\Facades\jDateTime;

class PostsServiceProvider extends ServiceProvider
{
    protected static $searchTemplate = 'post';
    protected static $defaultData    = [
         'selectPosts'  => [],
         'collectPosts' => [],
         'showList'     => [],
         'showPost'     => [],
    ];

    protected static $moduleAlias = 'ehdafront';

    protected static $mainSearchFieldName = 'q';

    private $runningMethod;



    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }



    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * @param array $data
     *
     * @return Builder
     */
    public static function selectPosts($data = [])
    {
        // Normalize data
        $data = self::postSelectorFixTypeAndCategories(array_normalize($data, self::getDefaultValues(__FUNCTION__)));
        if ($search = $data['search']) {
            unset($data['search']);
        }

        // Select posts
        $posts = model('post')->elector($data)->where($data['conditions']);
        $posts = self::postSelectorSearch($posts, $search);
        $posts = self::postSelectorOrder($posts, $data);
        $posts = self::postSelectorLimit($posts, $data);

        return $posts;
    }



    protected static function postSelectorFixTypeAndCategories($data)
    {
        $folder   = $data['folder'];
        $category = $data['category'];
        if (
             ($posttypes = $data['type']) and
             !(is_string($posttypes) and str_contains($posttypes, ':'))
             and ($folder or $category)
        ) {
            $posttypes = (array)$posttypes;

            $posttypesIds = model('posttype')
                 ->whereIn('slug', $posttypes)
                 ->get(['id'])
                 ->map(function ($item) {
                     return $item->id;
                 })->toArray()
            ;

            if ($folder) {
                $folders = model('category')
                     ->whereIn('posttype_id', $posttypesIds)
                     ->whereIsFolder(true)
                     ->where(function ($q) use ($folder) {
                         $q->whereIn('slug', (array)$folder);
                         $q->orWhereIn('id', (array)$folder);
                     })
                     ->get(['id'])
                ;
            } else {
                $folders = false;
            }

            $categoriesQuery = model('category')
                 ->whereIn('posttype_id', $posttypesIds)
                 ->whereIsFolder(false)
            ;

            if ($category) {
                $categoriesQuery->where(function ($q) use ($category) {
                    $q->whereIn('slug', (array)$category);
                    $q->orWhereIn('id', (array)$category);
                });

                if ($folders) {
                    $categoriesQuery->whereIn('parent_id', $folders->toArray());
                }
            } else {
                if ($folders) {
                    $categoriesQuery->whereIn('parent_id', $folders->toArray());
                }
            }

            $data['category'] = $categoriesQuery->get(['id'])->map(function ($item) {
                return $item->id;
            })->toArray()
            ;
        }

        return $data;
    }



    protected static function postSelectorSearch($builder, $search)
    {
        if ($search) {
            $builder->where(function ($q) use ($search) {
                $q->where('title', 'like', '%' . $search . '%')
                  ->orWhere('text', 'like', '%' . $search . '%')
                ;
            });
        }

        return $builder;
    }



    protected static function postSelectorOrder($builder, $data)
    {
        // Randomize list if needed
        if ($data['random']) {
            $builder = $builder->inRandomOrder();
        } else {
            $builder = $builder->orderBy($data['sort_by'], $data['sort']);
        }

        return $builder;
    }



    protected static function postSelectorLimit($builder, $data)
    {
        // Limit number of posts if needed
        if ($data['limit']) {
            $builder = $builder->limit($data['limit']);
        }

        return $builder;
    }



    /**
     * Collects posts
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public static function collectPosts($data = [])
    {
        $methodName    = __FUNCTION__;
        $defaultValues = self::getDefaultValues($methodName);
        // normalize data
        $data = array_normalize($data, $defaultValues);

        // Set current pagination page if needed
        if ($data['paginate_current']) {
            Paginator::currentPageResolver(function () use ($data) {
                return $data['paginate_current'];
            });
        }

        $posts = self::selectPosts($data);

        // Paginate lists if needed
        if (!$data['pagination'] or ($data['max_per_page'] == -1)) {
            if ($data['limit'] > 0) {
                return $posts->limit($data['limit'])->get();
            } else {
                if ($data['max_per_page'] > 0) {
                    return $posts->limit($data['max_per_page'])->get();
                } else {
                    return $posts->get();
                }
            }
        } else {
            // Limit number of posts if needed
            if ($data['limit']) {
                $posts = $posts->paginate($data['limit']);
            } else {
                $posts = $posts->paginate($data['max_per_page']);
            }

            if ($data['paginate_hash']) {
                $posts->fragment($data['paginate_hash'])->links();
            }

            if ($data['paginate_url']) {
                $posts->withPath($data['paginate_url']);
            }

            return $posts;
        }
    }



    /**
     * Returns a view including list view of post with specified filters and conditions
     *
     * @param array $data Filters and Conditions
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function showList($data = [])
    {
        // Check received to be listable
        $methodName    = __FUNCTION__;
        $defaultValues = self::getDefaultValues($methodName);
        // normalize data
        $data         = array_normalize($data, $defaultValues);
        $data['type'] = self::filterPosttypesByFeature($data['type'], $data['feature']);


        $showFilter  = $data['show_filter'];
        $ajaxRequest = $data['ajax_request'];
        $isBasePage  = $data['is_base_page'];

        if (!$data['is_base_page']) {
            $posts = self::collectPosts($data);

            if (!$posts->count()) {
                if ($data['showError']) {
                    return self::showError(trans('front.no_result_found'), $ajaxRequest);
                } else {
                    return false;
                }
            }
        }

        // $allPosts = self::allPostsOfType($data['type'], $data['locale']);
        $allPosts = self::selectPosts($data)->get();
        if (!$allPosts->count()) {
            if ($data['showError']) {
                return self::showError(trans('front.no_result_found'), $ajaxRequest);
            } else {
                return false;
            }
        }

        // set an array for sending data to view
        $viewData = [];

        // specify template
        $postType = model('posttype', $data['type']);
        if (self::$defaultData[$methodName]['type'] != $data['type']) {
            $viewData['postType'] = $postType;
            $template             = $postType
                 ->spreadMeta()
                 ->template
            ;
        } else {
            $template = self::$searchTemplate;
        }

        // render view
        if ($template == 'special') {
            $viewFolder = self::postsViewPath("list.special.$postType->slug");
        } else {
            $viewFolder = self::postsViewPath("list.$template");
        }

        return self::generateView($viewFolder . '.main', compact(
                  'posts',
                  'viewFolder',
                  'showFilter',
                  'ajaxRequest',
                  'isBasePage',
                  'allPosts'
             ) + $data['variables']);
    }



    /**
     * Returns a view including single view of a post with specified filters and conditions
     *
     * @param Post|string|integer $identifier
     * @param array               $data
     *
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function showPost($identifier, $data = [])
    {
        $methodName    = __FUNCTION__;
        $defaultValues = self::getDefaultValues($methodName);
        // normalize data
        $data = array_normalize($data, $defaultValues);

        // Find posts
        $post = self::smartFindPost($identifier);

        // Make action if post doesn't exist
        if (!$post->exists) {
            if ($data['showError']) {
                return view(self::errorViewPath(410, true));
            } else {
                return false;
            }
        }

        // Find posttype
        $postType = $post->posttype()->spreadMeta();

        // Find related alert and messages
        $messagesPosts['noAccessFiles'] = self::smartFindPost($postType->slug . '-no-access-files');
        if (!$messagesPosts['noAccessFiles']->exists) {
            $messagesPosts['noAccessFiles'] = self::smartFindPost('no-access-files');
        }

        // Find template of post
        $template = $postType->template;
        if ($template == 'special') {
            $viewFolder = self::postsViewPath("single.$template.$postType->slug");
        } else {
            $viewFolder = self::postsViewPath("single.$template");
        }
        // render view
        $externalBlade = $data['externalBlade'];
        return self::generateView($viewFolder . '.main', compact('post',
                  'viewFolder',
                  'messagesPosts',
                  'externalBlade'
             ) + $data['variables']);
    }



    /**
     * Returns a view to show error
     *
     * @param string $errorMessage Error message to be shown
     * @param bool   $ajaxRequest  If true, error page will not be shown
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function showError($errorMessage, $ajaxRequest = false)
    {
        return view('front.posts.error', [
             'errorMessage' => $errorMessage,
             'ajaxRequest'  => $ajaxRequest,
        ]);
    }



    /**
     * Returns an array of categories of given posts
     *
     * @param \Illuminate\Database\Eloquent\Collection $posts
     * @param string                                   $slug
     *
     * @return array
     */
    public static function postsCategories($posts, $slug = 'id')
    {
        $cats = Category::whereHas('posts', function ($query) use ($posts) {
            $query->whereIn('posts.id', $posts->pluck('id')->toArray());
        })->get()
                        ->pluck('title', $slug)
                        ->toArray()
        ;

        ksort($cats);

        return $cats;
    }



    /**
     * Returns all posts of the specified post type
     *
     * @param string      $type
     * @param null|string $locale
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function allPostsOfType($type, $locale = null)
    {
        $data = [
             'locale' => ($locale) ? $locale : getLocale(),
             'type'   => $type,
        ];

        return Post::selector($data)->get();
    }



    /**
     * Returns Collection of comments for a post
     *
     * @param \App\Models\Postt $post
     * @param array             $parameters
     *
     * @return mixed
     */
    public static function getPostComments($post, $parameters = [])
    {
        $post = self::smartFindPost($post);
        if ($post->exists) {
            $parameters = array_normalize($parameters, [
                 'user_id' => '0',
            ]);

            $selectorRules = [
                 'type'     => $post->type,
                 'post_id'  => $post->id,
                 'criteria' => 'all',
            ];

            if ($parameters['user_id']) {
                $selectorRules['user_id'] = $parameters['user_id'];
            }

            return Comment::selector($selectorRules)
                          ->orderBy('created_at', 'DESC')
                          ->get()
                 ;
        }
    }



    /**
     * Find post with multiple types of identifiers
     *
     * @param         $identifier
     * @param boolean $checkDirectId If false, post will not be searched with id of db table
     *
     * @return \App\Models\Post
     */
    public static function smartFindPost($identifier, $checkDirectId = false)
    {
        if ($identifier instanceof Post) {
            $post = $identifier;
        } else {
            if (is_numeric($identifier) and $checkDirectId) {
                $post = Post::find($identifier);
            } else {
                if (count($dehashed = hashid_decrypt($identifier, 'ids')) and
                     is_numeric($id = $dehashed[0])
                ) {
                    $post = Post::find($id);
                } else {
                    $post = post()->grabSlug($identifier);
                }
            }
        }

        if ($post and
             $post->exists and
             (
                  $post->posttype->hasNot('domains') or
                  !count($postDomains = $post->domains()->pluck('slug')->toArray()) or
                  // Domain isn't specified
                  count(array_intersect($postDomains, EhdaFrontServiceProvider::usableDomains()))
                 // Domain is specified and it is one of usable domains
             )
        ) {
            // Find sister of found post in current locale
            $post = $post->in(getLocale());

            return $post;
        }

        return new Post();
    }



    /**
     * Find posttype with multiple types of identifiers
     *
     * @param         $identifier
     * @param boolean $checkDirectId If false, posttype will not be searched with id of db table
     *
     * @return \App\Models\Posttype
     */
    public static function smartFindPosttype($identifier, $checkDirectId = false)
    {
        if ($identifier instanceof Posttype) {
            $posttype = $identifier;
        } else {
            if (is_numeric($identifier) and $checkDirectId) {
                $posttype = Posttype::find($identifier);
            } else {
                if (count($dehashed = hashid_decrypt($identifier, 'ids')) and
                     is_numeric($id = $dehashed[0])
                ) {
                    $posttype = Posttype::find($id);
                } else {
                    $posttype = posttype($identifier);
                }
            }
        }

        if ($posttype and $posttype->exists) {
            return $posttype;
        }

        return new Posttype();
    }



    public static function filterPosttypesByFeature($posttypes, $feature = '__DEFAULT__')
    {
        /*
         * Decide about feature
         */
        $feature = $feature ? (($feature == '__DEFAULT__') ? 'list_view' : $feature) : null;

        /*
         *  Decide about posttype
         */
        if ($feature) {
            if (is_array($posttypes)) {
                foreach ($posttypes as $key => $posttype) {
                    if (is_null(self::filterPosttypesByFeature($posttype, $feature))) {
                        unset($posttypes[$key]);
                    }
                }
                if (empty($posttypes)) {
                    return self::getDefaultValues('selectPosts')['type'];
                }
                return $posttypes;
            } else {
                $obj = self::smartFindPosttype($posttypes);
                if ($obj->exists and !$obj->has($feature)) {
                    return null;
                } else {
                    return $posttypes;
                }
            }
        }
        return $posttypes;
    }



    public static function filterActiveCategories($categories)
    {
        foreach ($categories as $key => $category) {
            if (!$category->posts()->count()) {
                $categories->forget($key);
            }
        }
        return $categories;
    }



    private static function generateView($view, $data = [])
    {
        if (View::exists($view)) {
            return view($view, $data);
        }

        return view(self::errorViewPath(404, true));
    }



    protected static function getDefaultValues($method = null)
    {
        if (empty(self::$defaultData['selectPosts'])) {
            self::$defaultData['selectPosts'] = [
                 'id'         => null,
                 'slug'       => null,
                 'role'       => "",
                 'criteria'   => "published",
                 'locale'     => getLocale(),
                 'owner'      => null,
                 'type'       => "features:searchable",
                 'category'   => null,
                 'keyword'    => "",
                 'search'     => "",
                 'from'       => "",
                 'to'         => "",
                 'folder'     => "",
                 'domains'    => null,
                 'limit'      => false, // If this is set as "false" list will not be limited
                 'random'     => false,
                 'sort'       => 'DESC',
                 'sort_by'    => 'published_at',
                 'conditions' => [], // additional conditions to be used in "where" clause
            ];
        }
        if ($method == 'selectPosts') {
            return self::$defaultData[$method];
        }

        if (empty(self::$defaultData['collectPosts'])) {
            self::$defaultData['collectPosts'] = array_merge(self::$defaultData['selectPosts'], [
                 'max_per_page'     => 12, // If this is set as "-1" pagination will not be applied
                 'pagination'       => true,
                 'paginate_current' => '',
                 'paginate_hash'    => '', // the fragment that should be added to links in pagination
                 'paginate_url'     => '',
            ]);
        }
        if ($method == 'collectPosts') {
            return self::$defaultData[$method];
        }

        if (empty(self::$defaultData['showList'])) {
            self::$defaultData['showList'] = array_merge(self::$defaultData['collectPosts'], [
                 'show_filter'  => true,
                 'ajax_request' => false,
                 'is_base_page' => false,
                 'showError'    => true,
                 'feature'      => 'list_view',
                 'variables'    => [],
            ]);
        }
        if ($method == 'showList') {
            return self::$defaultData[$method];
        }

        if (empty(self::$defaultData['showPost'])) {
            self::$defaultData['showPost'] = [
                 'lang'          => getLocale(),
                 'externalBlade' => '',
                 'preview'       => false,
                 'showError'     => true,
                 'feature'       => 'single_view',
                 'variables'     => [],
            ];
        }
        if ($method == 'showPost') {
            return self::$defaultData[$method];
        }

        return self::$defaultData;
    }



    protected static function postsViewPath($viewPath)
    {
        return self::moduleView('posts.' . $viewPath);
    }



    protected static function errorViewPath($errorCode, $minimal)
    {
        return self::moduleView('errors.' . ($minimal ? 'm' : '') . $errorCode);
    }



    protected static function moduleView($viewPath)
    {
        return self::$moduleAlias . '::' . $viewPath;
    }



    public static function getMainSearchFieldName()
    {
        return self::$mainSearchFieldName;
    }



    public static function getDynamicMessage($postSlug, $replacements = [])
    {
        if ($post = self::selectPosts(['type' => 'site-messages', 'slug' => $postSlug])->first()) {
            $text = $post->text ?: '';

            foreach ($replacements as $key => $value) {
                $text = str_replace($key, $value, $text);
            }

            return $text;
        }
        return null;
    }
}
