<?php

namespace Modules\EhdaFront\Providers;

use App\Models\Post;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\EhdaFront\Events\CardRegistered;
use Modules\EhdaFront\Events\PasswordChangedByUser;
use Modules\EhdaFront\Events\VolunteerRegistered;
use Modules\EhdaFront\Http\Middleware\TokenMiddleware;
use Modules\EhdaFront\Listeners\SendVerificationsOnCardRegistered;
use Modules\EhdaFront\Listeners\SendVerificationsOnVolunteerRegistered;
use Modules\EhdaFront\Listeners\TurnOffPasswordForceChangeOnPasswordChangedByUser;
use Modules\EhdaFront\NotificationChannels\Asanak\AsanakServiceProvider;
use Modules\EhdaFront\Providers\Api\ApiIpServiceProvider;
use Modules\EhdaFront\Providers\Api\TokenServiceProvider;
use Modules\EhdaFront\Services\AsanakSms\AsanakSmsProvider;
use Modules\EhdaFront\Services\Template\EhdaFrontTemplateHandler;
use Modules\Filemanager\Providers\FileManagerToolsServiceProvider;
use Modules\Yasna\Services\YasnaProvider;

class EhdaFrontServiceProvider extends YasnaProvider
{
    protected $providers        = [
         PostsServiceProvider::class,
         ToolsServiceProvider::class,
         RouteServiceProvider::class,
         EmailServiceProvider::class,
         AsanakSmsProvider::class,
         ApiIpServiceProvider::class,
         TokenServiceProvider::class,
         ApiServiceProvider::class,
         GalleryServiceProvider::class,
         FilterServiceProvider::class,
         CommentServiceProvider::class,
         PaymentServiceProvider::class,
         CurrencyServiceProvider::class,
    ];
    protected $aliases          = [
         'EhdaFront'              => self::class,
         'Template'               => EhdaFrontTemplateHandler::class,
         'PostTools'              => PostsServiceProvider::class,
         'EhdaFrontTools'         => ToolsServiceProvider::class,
         'EhdaFrontRouteTools'    => RouteServiceProvider::class,
         'GalleryTools'           => GalleryServiceProvider::class,
         'FilterTools'            => FilterServiceProvider::class,
         'CommentTools'           => CommentServiceProvider::class,
         'EhdaFrontPaymentTools'  => PaymentServiceProvider::class,
         'EhdaFrontCurrencyTools' => CurrencyServiceProvider::class,
    ];
    protected $middlewares      = [
         'api-token' => TokenMiddleware::class,
    ];
    protected static $globalDomainSlug = 'global';

    protected $cardHashidConnections = [
         'ehda_card_mini' => [
              'salt'     => "o,ah-dfdfd-;55-ldfdf-45fgc-md-dfgkzdfdf-kvdfrtgn",
              'length'   => "5",
              'alphabet' => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
         ],

         'ehda_card_single' => [
              'salt'     => "o,ah-dfdfhjkd-;55-kgvh-45fgc-md-iutrmnbvv-kvdfrtgn",
              'length'   => "5",
              'alphabet' => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
         ],

         'ehda_card_social' => [
              'salt'     => "o,ah-zxcqwer-;442-kgvh-45fgc-md-iutrmnbvv-kvdfrtgn",
              'length'   => "5",
              'alphabet' => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
         ],

         'ehda_card_full' => [
              'salt'     => "o,ah-zxcqwer-;442-kgvh-55454gvf-md-iutrmnbvv-p;lkbdfs",
              'length'   => "5",
              'alphabet' => "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
         ],
    ];



    public function index()
    {
        $this->addProviders();
        $this->addAliases();
        $this->addMiddlewares();
        $this->registerModelTraits();
        $this->defineConfigs();
        $this->registerEventListeners();
        $this->registerApiThings();
    }



    protected function addProviders()
    {
        foreach ($this->providers as $provider) {
            $this->addProvider($provider);
        }
    }



    protected function addAliases()
    {
        foreach ($this->aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }
    }



    protected function addMiddlewares()
    {
        foreach ($this->middlewares as $alias => $middleware) {
            $this->addMiddleware($alias, $middleware);
        }
    }



    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait("EhdaFront:EhdaFrontUserTrait")->to("User")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait("EhdaFront:EhdaApiUserTrait")->to("User")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait("EhdaFront:EhdaFrontPostTrait")->to("Post")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait("EhdaFront:EhdaFrontTagTrait")->to("Tag")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait("EhdaFront:EhdaFrontCommentTrait")->to("Comment")
        ;
        module("yasna")
             ->service("traits")
             ->add()->trait("EhdaFront:EhdaFrontStateTrait")->to("State")
        ;
    }



    protected function defineConfigs()
    {
        foreach ($this->cardHashidConnections as $key => $config) {
            config(['hashids.connections.' . $key => $config]);
        }
    }



    protected function registerEventListeners()
    {
        $this->listen(CardRegistered::class, SendVerificationsOnCardRegistered::class);
        $this->listen(VolunteerRegistered::class, SendVerificationsOnVolunteerRegistered::class);
        $this->listen(
             PasswordChangedByUser::class,
             TurnOffPasswordForceChangeOnPasswordChangedByUser::class
        );
    }



    protected function registerApiThings()
    {
        return $this->registerUserHandlersForApi();
    }



    protected function registerUserHandlersForApi()
    {
        module('users')
             ->service('browse_button_handlers')
             ->add()
             ->method("EhdaFront:handleBrowseButtons")
             ->order(500)
        ;

        module('users')
             ->service('row_action_handlers')
             ->add()
             ->method("EhdaFront:handleRowActions")
             ->order(500)
        ;

        return $this;
    }



    public static function isIndex()
    {
        return strpos(request()->route()->getActionName(), 'EhdaFrontController@index') !== false;
    }



    public static function isNotIndex()
    {
        return !self::isIndex();
    }



    public static function getDomain()
    {
        return (($currentDomain = getDomain()) == self::$globalDomainSlug)
             ? self::getDefaultDomain()
             : $currentDomain;
    }



    public static function getDefaultDomain()
    {
        return get_setting('default_domain') ?: 'global';
    }



    /**
     * Returns an array including "global" and current domain (in specified)
     *
     * @return array
     */
    public static function usableDomains()
    {
        $domains = ['global'];

        if ($currentDomain = self::getDomain()) {
            // If setting has been set in manage panel
            if ($adminChoice = get_setting('showing_posts_domains')) {
                return explode_not_empty('|', str_replace('{{currentDomain}}', $currentDomain, $adminChoice));
            } else {
                $domains[] = $currentDomain;
            }
        }

        return $domains;
    }



    public static function localeLink($locale)
    {
        $currentRoute = request()->route();
        if (
             !$currentRoute->hasParameter('lang') or
             (($forcedParameters = RouteServiceProvider::getForcedUrlParameters($locale)) === false)
        ) {
            return RouteServiceProvider::action('EhdaFrontController@index', ['lang' => $locale]);
        }
        $parameters = $currentRoute->parameters();

        return action(
             '\\' . $currentRoute->getActionName(),
             array_merge($parameters, $forcedParameters, ['lang' => $locale])
        );
    }



    public static function getUsableDomains()
    {
        return self::usableDomains();
    }



    public static function handleRowActions($arguments)
    {
        $model   = $arguments['model'];
        $service = 'users:row_actions';

        if ($model->withDisabled()->hasRole(ApiServiceProvider::apiClientRoleSlug())) {
            /*-----------------------------------------------
            | Editor
            */
            service($service)
                 ->add('edit')
                 ->icon('pencil')
                 ->trans("manage::forms.button.edit_info")
                 ->link("modal:" . RouteServiceProvider::action('Api\Manage\UsersController@edit', [
                           'hashid' => $model->hashid,
                      ], false))
                 ->condition($model->canEdit())
                 ->order(12)
            ;


            /*-----------------------------------------------
            | IPs
            */
            service($service)
                 ->add('ips')
                 ->icon('cogs')
                 ->trans("ehdafront::api.manage.headline.acceptable-ips")
                 ->link("modal:" . RouteServiceProvider::action('Api\Manage\UsersController@ips', [
                           'hashid' => $model->hashid,
                      ], false))
                 ->condition($model->canEdit())
                 ->order(15)
            ;


            /*-----------------------------------------------
            | Portable Printer Config
            */
            service($service)
                 ->add('portablePrinterConfigs')
                 ->icon('print')
                 ->trans("ehdafront::api.manage.headline.portable-printer-configs")
                 ->link(
                      "modal:" .
                      RouteServiceProvider::action('Api\Manage\UsersController@portablePrinterConfigsForm', [
                           'hashid' => $model->hashid,
                      ], false))
                 ->condition($model->canEdit())
                 ->order(16)
            ;
        }
    }



    public static function handleBrowseButtons($arguments)
    {
        $role = $arguments['role'];
        if ($role->exists and ($role->slug == ApiServiceProvider::apiClientRoleSlug())) {
            $service = "users:browse_buttons";

            service($service)
                 ->add('create')
                 ->icon('plus-circle')
                 ->caption(trans('manage::forms.button.add_to') . SPACE . $role->plural_title)
                 ->link("modal:" . RouteServiceProvider::action('Api\Manage\UsersController@create', [], false))
                 ->set('type', 'success')
                 ->condition(user()->as('admin')->can("users-$role->slug.create"))
            ;
        }
    }



    public static function urlHyphen($str)
    {
        return str_replace('+', '-', urlencode($str));
    }



    public static function getEventRegistrations(Post $event)
    {
        if ($event->type == 'event') {
            return model('user')
                 ->where('from_event_id', $event->id)
                 ->count()
                 ;
        }

        return null;
    }



    public static function getGalleryCardCombo()
    {
        return GalleryServiceProvider::cartTypesCombo();
    }



    public static function getDepartmentsCombo()
    {
        return CommentServiceProvider::getDepartmentsCombo(true);
    }



    /**
     * Array to be use in a combo to selecting from file types
     *
     * @return array
     */
    public static function getFileTypesCombo()
    {
        $result = [];
        foreach (FileManagerToolsServiceProvider::getAcceptableFileTypes() as $type) {
            $result[] = ['id' => $type, 'title' => trans("filemanager::base.file-types.$type.title")];
        }

        return $result;
    }



    /**
     * @return string
     */
    public static function shortLinksHashidPrefix()
    {
        return (config('ehdafront.prefix.routes.post.short') ?: 'P');
    }
}
