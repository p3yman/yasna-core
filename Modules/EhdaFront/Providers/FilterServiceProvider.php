<?php

namespace Modules\EhdaFront\Providers;

use Illuminate\Support\ServiceProvider;

class FilterServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public static function translateFilterHash($hash)
    {
        $hashArray = [];

        $hash = explode_not_empty('!', $hash);
        foreach ($hash as $field) {
            $field = explode_not_empty('?', $field);
            if (count($field) == 2) {
                $hashArray[$field[1]] = explode_not_empty('/', $field[0]);
                $currentGroup         = &$hashArray[$field[1]];
                $currentGroup         = self::arrayPrefixToIndex('_', $currentGroup);
            }
        }

        return $hashArray;
    }



    protected static function arrayPrefixToIndex($delimiter, $haystack)
    {
        if (is_array($haystack)) {
            foreach ($haystack as $index => $field) {
                $parts = explode($delimiter, $field, 2);
                if (count($parts) == 2) {
                    $key   = $parts[0];
                    $value = $parts[1];
                    $value = self::arrayPrefixToIndex($delimiter, $value);

                    $target = &$haystack[$key];
                    if (isset($target)) {
                        if (!is_array($target)) {
                            $target = [$target];
                        }

                        if (!is_array($value)) {
                            $value = [$value];
                        }

                        $target = array_merge($target, $value);
                    } else {
                        $target = $value;
                    }
                }
                unset($haystack[$index]);
            }

            return $haystack;
        }

        if (is_string($haystack)) {
            $parts = explode($delimiter, $haystack, 2);
            if (count($parts) == 2) {
                $key   = $parts[0];
                $value = $parts[1];
                $value = self::arrayPrefixToIndex($delimiter, $value);

                $haystack = [
                     $key => $value,
                ];

                return $haystack;
            }
        }

        return $haystack;
    }



    public static function encryptFilterHash(array $hashArray, bool $hashid = true)
    {
        $result = '';

        foreach ($hashArray as $key => $group) {
            if (is_array($group) and count($group)) {
                $result .= '!';
                $parts  = [];

                foreach ($group as $identifier => $data) {
                    $groupPrefix = $identifier . '_';

                    if (is_bool($data)) {
                        $parts[] = $groupPrefix . ($data ? 1 : 0);
                    } else {
                        if (is_string($data)) {
                            $parts[] = $groupPrefix . $data;
                        } else {
                            if (is_array($data)) {
                                foreach ($data as $index => $value) {
                                    $prefix  = is_string($index) ? ($groupPrefix . $index . '_') : $groupPrefix;
                                    $parts[] = self::filterHashMapValue($hashid, $prefix, $value);
                                }
                            }
                        }
                    }
                }

                $result .= implode('/', $parts);
                $result .= '?' . $key;
            }
        }

        $result .= (($result) ? '!' : '');

        return $result;
    }



    protected static function filterHashMapValue($hashid, $prefix, $value = null)
    {
        if (is_bool($value)) {
            return $prefix . (($value) ? 1 : 0);
        } else {
            if (is_array($value) and array_key_exists('val', $value)) {
                if ($hashid and array_key_exists('hashid', $value)) {
                    return $prefix . $value['hashid'];
                } else {
                    return $prefix . $value['val'];
                }
            } else {
                return $prefix . $value;
            }
        }
    }
}
