<?php

namespace Modules\EhdaFront\Providers;

use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var string Namespace of Current Module's Controllers
     */
    protected static $controllersNamespace = '\Modules\EhdaFront\Http\Controllers\\';
    /**
     * @var array Forced values to be used in sisterhood links
     */
    private static $forcedUrlParameters = [];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public static function action(string $action, ...$otherParameters)
    {
        return action(self::$controllersNamespace . $action, ...$otherParameters);
    }



    public static function actionLocale(string $action, ...$otherParameters)
    {
        return action_locale(self::$controllersNamespace . $action, ...$otherParameters);
    }



    public static function currentAction(...$parameters)
    {
        return action('\\' . request()->route()->getActionName(), ...$parameters);
    }



    public static function currentActionLocale(...$parameters)
    {
        return action_locale('\\' . request()->route()->getActionName(), ...$parameters);
    }



    /**
     * <h3>Forces variables to be used while generating url of sister pages.</h3>
     * <p>
     *     <i>
     *         Note: Language should be specified in parameters.
     *         <ul>
     *             <li>Correct Example: ['en' => ['name' => 'John']];</li>
     *             <li>Wrong Example: ['name' => 'John'];</li>
     *         </ul>
     *     </i>
     * </p>
     *
     * @param array $parameters
     */
    public static function forceUrlParameters($parameters = [])
    {
        $siteLocales = availableLocales();
        foreach ($parameters as $locale => $fields) {
            if (in_array($locale, $siteLocales) !== false) {
                self::$forcedUrlParameters = array_merge(self::$forcedUrlParameters, [
                     $locale => $fields,
                ]);
            }
        }
    }



    /**
     * Returns forced variables to be used while generating url of sister pages.
     *
     * @param string $locale
     *
     * @return array
     */
    public static function getForcedUrlParameters($locale = '')
    {
        if ($locale) {
            if (array_key_exists($locale, self::$forcedUrlParameters)) {
                return self::$forcedUrlParameters[$locale];
            } else {
                return [];
            }
        }

        return self::$forcedUrlParameters;
    }
}
