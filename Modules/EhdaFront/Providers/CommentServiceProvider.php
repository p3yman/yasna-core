<?php

namespace Modules\EhdaFront\Providers;

use App\Models\Posttype;
use App\Models\Role;
use Illuminate\Support\ServiceProvider;

class CommentServiceProvider extends ServiceProvider
{
    public static $acceptable_comment_fields = [
         "name",
         "email",
         "mobile",
         "subject",
         "message",
    ];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public static function isAcceptableField($field)
    {
        return in_array($field, self::$acceptable_comment_fields);
    }



    /**
     * Translate standard "fields" value of a "commenting" post
     *
     * @param $fieldsString string
     *
     * @return array
     */
    public static function translateFields($fieldsString)
    {
        /**
         * translation rules
         * 1. separate fields description with comma (,)
         * 2. start each field description with its "name" (ex: "subject,first_name")
         * 3. if you want that field to has a label add "-label" to the field description (ex: "subject-label")
         * 4. if you want that field to has a column size (bootstrap columns) add ":size" (ex: "subject:6")
         * 5. put no "next-line" in fields string
         */

        if ($fieldsString and is_string($fieldsString)) {
            $fields = explode_not_empty(',', $fieldsString);
            foreach ($fields as $fieldIndex => $fieldValue) {
                unset($fields[$fieldIndex]);
                $fieldValue = trim($fieldValue);

                if (str_contains($fieldValue, '*')) {
                    $fieldValue           = str_replace('*', '', $fieldValue);
                    $tmpField['required'] = true;
                } else {
                    $tmpField['required'] = false;
                }

                if (str_contains($fieldValue, '-label')) {
                    $fieldValue        = str_replace('-label', '', $fieldValue);
                    $tmpField['label'] = true;
                } else {
                    $tmpField['label'] = false;
                }

                if (str_contains($fieldValue, ':')) {
                    $fieldValueParts  = explode_not_empty(':', $fieldValue);
                    $fieldValue       = $fieldValueParts[0];
                    $tmpField['size'] = $fieldValueParts[1];
                } else {
                    $tmpField['size'] = '';
                }
                $fields[$fieldValue] = $tmpField;
                unset($tmpField);
            }
            return $fields;
        }

        return [];
    }



    /**
     * Translate standard "rules" value of a "commenting" post
     *
     * @param string  $rulesString
     * @param boolean $detailed If "false" this function will return rules of any field in a single item (request
     *                          standard rule)
     *
     * @return array
     */
    public static function translateRules($rulesString, $detailed = true)
    {
        /**
         * translation rules
         * 1. separate fields rules with comma (,)
         * 2. start each field description with its "name" (ex: "subject,first_name")
         * 3. after name of the field, put "=>" and enter rules in the pipeline separated format
         * 4. each rule should be one of laravel standard rules
         * 5. put no "next-line" in rules string
         */

        if ($rulesString and is_string($rulesString)) {
            $rulesString = str_replace("\n", '', $rulesString);
            $rules       = explode_not_empty(',', $rulesString);

            foreach ($rules as $index => $rule) {
                $rule = trim($rule);
                unset($rules[$index]);

                $parts = explode_not_empty('=>', $rule);

                if ($parts and is_array($parts) and (count($parts) == 2)) {
                    $fieldName = trim($parts[0]);
                    if ($detailed) {
                        $fieldRules = explode_not_empty('|', $parts[1]);
                        if (count($fieldRules)) {
                            $rules[$fieldName] = $fieldRules;
                        }
                    } else {
                        $fieldRules = $parts[1];
                        if ($fieldRules) {
                            $rules[$fieldName] = $fieldRules;
                        }
                    }
                }
            }

            return $rules;
        }

        return [];
    }



    public static function posttypeRules(Posttype $posttype)
    {
        $rules = [];
        $posttype->spreadMeta();

        if (auth()->guest()) {
            $rules['name'] = ['required'];

            if ($posttype->commenting_enter_email == 'must') {
                $rules['email'] = ['required'];
            }

            if ($posttype->commenting_enter_mobile == 'must') {
                $rules['mobile'] = ['required'];
            }
        }

        return $rules;
    }



    public static function departments()
    {
        return Role::supportRoles();
    }



    public static function getDepartments()
    {
        return self::departments();
    }



    public static function departmentsCombo($blankItem = false)
    {
        $items = Role::supportRoles()->map(function ($role) {
            return ['id' => $role->hashid, 'title' => $role->title];
        })
        ;
        if ($blankItem) {
            $items = $items->prepend(['id' => '', 'title' => '']);
        }

        return $items->toArray();
    }



    public static function getDepartmentsCombo($blankItem = false)
    {
        return self::departmentsCombo($blankItem);
    }
}
