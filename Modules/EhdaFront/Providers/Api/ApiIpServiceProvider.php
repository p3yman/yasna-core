<?php

namespace Modules\EhdaFront\Providers\Api;

use App\Models\ApiIp;
use App\Models\User;
use Illuminate\Support\ServiceProvider;

class ApiIpServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function userIsPermittedWithIp(User $user, $ip = null)
    {
        $ip = $ip ?: request()->ip();
        switch ($user->as('api')->status()) {
            case 1:
                /*
                 * Requested IP should exist in user's acceptable IPs
                 */
                return boolval(ApiIp::where('user_id', $user->id)
                    ->where('slug', $ip)
                    ->first());
                break;

            case 2:
                /*
                 * User doesn't need validation with IP address
                 */
                return true;
                break;

            case 3:
                /*
                 * User hasn't permission
                 */
                return false;
                break;
        }
    }
}
