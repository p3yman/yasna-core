<?php

namespace Modules\EhdaFront\Providers\Api;

use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;
use Modules\Woice\Services\Api\Response\ApiResponse;

class TokenServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    public static function newToken($client)
    {
        do {
            $token = self::makeNewToken();
        } while (model('api_token')->where('api_token', $token)->first());

        if (($tokenRow = model('api_token')->batchSave([
                  'user_id'    => $client->id,
                  'api_token'  => $token,
                  'expired_at' => self::tokenExpirationTime(),
             ])) and $tokenRow->exists) {
            return $tokenRow;
        }

        ApiResponse::create('unable-to-create-token')->throw();
    }



    protected static function makeNewToken()
    {
        return str_random(100);
    }



    protected static function tokenExpirationTime()
    {
        return Carbon::now()->addHour()->toDateTimeString();
    }
}
