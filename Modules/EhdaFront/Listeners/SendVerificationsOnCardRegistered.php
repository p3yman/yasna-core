<?php

namespace Modules\EhdaFront\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaFront\Notifications\CardRegistered;
use Modules\EhdaFront\Providers\EmailServiceProvider;

class SendVerificationsOnCardRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\EhdaFront\Events\CardRegistered $event
     *
     * @return void
     */
    public function handle(\Modules\EhdaFront\Events\CardRegistered $event)
    {
        //$event->model->notify(new CardRegistered);
    }
}
