<?php

namespace Modules\EhdaFront\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\EhdaFront\Notifications\VolunteerRegistered;
use Modules\EhdaFront\Providers\EmailServiceProvider;
use Modules\EhdaFront\Services\AsanakSms\Facade\AsanakSms;

class SendVerificationsOnVolunteerRegistered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\EhdaFront\Events\VolunteerRegistered $event
     *
     * @return void
     */
    public function handle(\Modules\EhdaFront\Events\VolunteerRegistered $event)
    {
        $event->model->notify(new VolunteerRegistered);
    }
}
