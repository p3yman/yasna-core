<?php

namespace Modules\EhdaFront\Listeners;

use Modules\EhdaFront\Events\PasswordChangedByUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TurnOffPasswordForceChangeOnPasswordChangedByUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\EhdaFront\Events\PasswordChangedByUser $event
     *
     * @return void
     */
    public function handle(\Modules\EhdaFront\Events\PasswordChangedByUser $event)
    {
        $event->model->update(['password_force_change' => 0]);
    }
}
