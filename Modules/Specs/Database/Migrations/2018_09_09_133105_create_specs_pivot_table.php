<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecsPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specs_pivot', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('spec_id')->index();
            $table->foreign('spec_id')
                  ->references('id')
                  ->on('specs')
            ;

            $table->string('model_name')->index();
            $table->unsignedInteger('model_id')->index();

            $table->text('value')->nullable();

            $table->timestamp('hid_at')->nullable();


            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specs_pivot');
    }
}
