<?php

namespace Modules\Specs\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class FeaturesTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('features', $this->data());
    }



    /**
     * Returns an array to be seeded in the `features` table.
     *
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'slug'      => 'specs',
                  'title'     => $this->runningModule()->getTrans('seeder.specs'),
                  'order'     => '40',
                  'icon'      => 'list',
                  'post_meta' => '',
                  'meta-hint' => '',

             ],
        ];
    }
}
