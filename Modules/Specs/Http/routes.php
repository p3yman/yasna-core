<?php

Route::group([
     'middleware' => ['web', 'is:super'],
     'prefix'     => 'manage/specs',
     'namespace'  => 'Modules\Specs\Http\Controllers',
], function () {
    Route::group(['prefix' => 'groups'], function () {

        Route::get('act/{model_id}/{action}/{option0?}/{option1?}/{option2?}', 'GroupController@singleAction')
             ->name('specs.groups.single-action')
        ;

        Route::post('save', 'GroupController@save')->name('specs.groups.save');
        Route::post('delete-or-restore', 'GroupController@deleteOrRestore')->name('specs.groups.delete-or-restore');
        Route::post('hard-delete', 'GroupController@hardDelete')->name('specs.groups.hard-delete');
    });

    Route::get('posts/{post_hashid}/group/{group_hashid}/children', 'HandleController@specsEditor')
         ->name('specs.posts.group-children')
    ;
});
