<?php

namespace Modules\Specs\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Specs\Http\Controllers\V1\PostSpecsController;

/**
 * @api                {GET}
 *                    /api/modular/v1/specs-list
 *                    List
 * @apiDescription     Post Specs List
 * @apiVersion         1.0.0
 * @apiName            List
 * @apiGroup           Specs
 * @apiPermission      user
 * @apiParam {String} hashid  hashid of the post
 * @apiSuccessExample  Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" => hashid(1),
 *      },
 *      "results": {
 *            "136" => [
 *               "title" => dummy()::persianTitle(),
 *               "rows"  => [
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianName(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText()
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText()
 *               ],
 *               ],
 *               ],
 *               ],
 *            "140" => [
 *               "title" => dummy()::persianTitle(),
 *               "rows"  => [
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText()
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianTitle(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               "yes",
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               "no",
 *               ],
 *               ],
 *               ],
 *             ],
 *            "145" => [
 *               "title" => dummy()::persianTitle(),
 *               "rows"  => [
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               "",
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianWord(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText(),
 *               ],
 *               ],
 *               [
 *               "title" => dummy()::persianTitle(),
 *               "cols"  => [
 *               dummy()::persianText(),
 *               ],
 *               ],
 *               ],
 *            ],
 *           }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method PostSpecsController controller() //
 */
class ListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Specs\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PostSpecsController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "136" => [
                  "title" => dummy()::persianTitle(),
                  "rows"  => [
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianName(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText()
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText()
                            ],
                       ],
                  ],
             ],
             "140" => [
                  "title" => dummy()::persianTitle(),
                  "rows"  => [
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText()
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianTitle(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 "yes",
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 "no",
                            ],
                       ],
                  ],
             ],
             "145" => [
                  "title" => dummy()::persianTitle(),
                  "rows"  => [
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 "",
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianWord(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText(),
                            ],
                       ],
                       [
                            "title" => dummy()::persianTitle(),
                            "cols"  => [
                                 dummy()::persianText(),
                            ],
                       ],
                  ],
             ],
        ], [
             "hashid" => hashid(109),
        ]);
    }
}
