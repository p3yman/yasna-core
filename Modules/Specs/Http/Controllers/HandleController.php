<?php

namespace Modules\Specs\Http\Controllers;

use App\Models\Post;
use App\Models\Spec;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class HandleController extends YasnaController
{
    /**
     * The Base Model
     *
     * @var string
     */
    protected $base_model = 'spec';



    /**
     * Handles services for editor
     *
     * @param Post $model
     */
    public static function editor(Post $model)
    {
        $module = app(static::class)->module();

        service('posts:editor_main')
             ->add('specs')
             ->blade($module->getBladePath('posts.editor-main'))
             ->condition($model->has('specs'))
             ->order(40)
        ;
    }



    /**
     * Finds and returns an object of the Spec model with the specified hashid.
     *
     * @param string $group_hashid
     *
     * @return Spec
     */
    protected function findSpecsGroup(string $group_hashid)
    {
        $id = hashid($group_hashid);

        return $this->model()
                    ->groups()
                    ->whereId($id)
                    ->first()
             ?? $this->model();
    }



    /**
     * Returns product's spec form
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function specsEditor(SimpleYasnaRequest $request)
    {
        $group = $this->findSpecsGroup($request->group_hashid);
        $model = post($request->post_hashid);

        if ($group->not_exists) {
            return $this->abort(410, true, true);
        }

        return $this->view('specs::posts.specs-form', compact('group', 'model'));
    }



    /**
     * Saves specs after saving a post.
     *
     * @param Post $model
     * @param array $data
     */
    public static function saveSpecs(Post $model, $data)
    {
        if ($model->hasNotSpecs()) {
            return;
        }

        $model->specsPivots()->forceDelete();

        $specs = ($data['_specs'] ?? []);

        foreach ($specs as $spec_hashid => $spec_data) {
            $value      = $spec_data['value'];
            $is_visible = boolval($spec_data['visible']);
            $is_hidden  = !$is_visible;

            $model->saveSpecPivot($spec_hashid, $value, $is_hidden);
        }
    }
}
