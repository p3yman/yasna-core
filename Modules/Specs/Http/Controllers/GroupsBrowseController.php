<?php

namespace Modules\Specs\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Modules\Yasna\Services\YasnaController;

class GroupsBrowseController extends YasnaController
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $base_model = "spec";
    /**
     * The Folder of Views
     *
     * @var string
     */
    protected $view_folder = "specs::groups";
    /**
     * The Builder of Groups
     *
     * @var null|Builder
     */
    protected $builder;
    /**
     * The General Variables
     *
     * @var array
     */
    protected $general_variables = [];



    /**
     * Returns a builder to find spec groups.
     *
     * @return Builder
     */
    protected function groupsQueryBuilder()
    {
        if (!$this->builder) {
            $this->builder = $this->model()->groups();
        }

        return $this->builder;
    }



    /**
     * Finds groups to show in the grid.
     */
    protected function findGroups()
    {
        $this->general_variables['models'] = $this->groupsQueryBuilder()
                                                  ->orderByDesc('created_at')
                                                  ->paginate(20)
        ;
    }



    /**
     * Sets an array of the browse grid's headings.
     */
    protected function browseHeadings()
    {
        $this->general_variables['browse_headings'] = [
             $this->module()->getTrans('validation.attributes.title'),
        ];
    }



    /**
     * Sets the view file for the downstream page.
     */
    protected function downstreamViewFile()
    {
        $this->general_variables['view_file'] = $this->view_folder . '.downstream';
    }



    /**
     * Returns an array to be used while rendering the downstream page.
     *
     * @return array
     */
    public static function downstream()
    {
        $controller = app(static::class);
        $controller->findGroups();
        $controller->browseHeadings();

        $controller->downstreamViewFile();

        return $controller->general_variables;
    }
}
