<?php

namespace Modules\Specs\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Modules\Specs\Entities\Spec;
use Modules\Specs\Http\Requests\GroupDeleteRequest;
use Modules\Specs\Http\Requests\GroupHardDeleteDeleteRequest;
use Modules\Specs\Http\Requests\GroupSaveRequest;
use Modules\Yasna\Services\YasnaController;

class GroupController extends YasnaController
{
    protected $base_model  = "spec";
    protected $view_folder = "specs::groups";

    /**
     * List of IDs of children which has been saved in the current save requeset
     *
     * @var array
     */
    protected $saved_ids = [];
    /**
     * List of IDs of children which has been saved before.
     *
     * @var array
     */
    protected $all_old_children_ids = [];



    /**
     * Registers assets for the spec group editor.
     */
    protected function registerEditorAssets()
    {
        // Nested Sortable
        module('manage')
             ->service('template_bottom_assets')
             ->add('nested_sortable')
             ->link("specs:libs/Sortable.min.js")
             ->order(43)
        ;

        // Specs js
        module('manage')
             ->service('template_bottom_assets')
             ->add('specs_js')
             ->link("specs:js/specs.min.js")
             ->order(44)
        ;

        // Specs css
        module('manage')
             ->service('template_assets')
             ->add('specs_css')
             ->link("specs:css/specs-style.min.css")
             ->order(44)
        ;
    }



    /**
     * Returns an array of general editor page info.
     *
     * @return array
     */
    protected function generalEditorPageInfo()
    {
        return [
             [
                  $url = 'downstream/specs-groups',
                  $this->module()->getTrans('general.groups'),
                  $url,
             ],
        ];
    }



    /**
     * Returns and array of create page info.
     *
     * @return array
     */
    protected function createPageInfo()
    {
        $create_page_info = [
             [
                  $url2 = str_after(request()->url(), url('manage') . '/'),
                  trans_safe('manage::forms.button.add'),
                  $url2,
             ],
        ];

        return array_merge($this->generalEditorPageInfo(), $create_page_info);
    }



    /**
     * Returns a collection of last 10 groups.
     *
     * @return Collection
     */
    public function sidebarGroups($current_hashid): Collection
    {
        return $this->model()
                    ->groups()
                    ->where('id', '<>', hashid($current_hashid))
                    ->orderByDesc('created_at')
                    ->limit(10)
                    ->get()
             ;
    }



    /**
     * The Route Form of the Create Action
     *
     * @param string $model_hashid
     * @param array  $option
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createRootForm($model_hashid, ... $option)
    {
        $this->registerEditorAssets();
        $page         = $this->createPageInfo();
        $other_groups = $this->sidebarGroups($model_hashid);

        return $this->singleAction($model_hashid, 'editor', $page, $other_groups, ...$option);
    }



    /**
     * Returns and array of edit page info.
     *
     * @return array
     */
    protected function editPageInfo()
    {
        $edit_page_info = [
             [
                  $url2 = str_after(request()->url(), url('manage') . '/'),
                  trans_safe('manage::forms.button.edit'),
                  $url2,
             ],
        ];

        return array_merge($this->generalEditorPageInfo(), $edit_page_info);
    }



    /**
     * The Route Form of the edit Action
     *
     * @param string $model_hashid
     * @param array  $option
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function editRootForm($model_hashid, ... $option)
    {
        $this->registerEditorAssets();
        $page         = $this->editPageInfo();
        $other_groups = $this->sidebarGroups($model_hashid);

        return $this->singleAction($model_hashid, 'editor', $page, $other_groups, ...$option);
    }



    /**
     * Saves children in of the given spec from the given data.
     *
     * @param Spec  $parent
     * @param array $data
     */
    protected function saveSpecChildren(Spec $parent, array $data)
    {
        $old_children               = $parent->children->pluck('id')->toArray();
        $this->all_old_children_ids = array_merge($this->all_old_children_ids, $old_children);

        $children = ($data['children'] ?? []);

        foreach ($children as $child_data) {
            $model            = $child_data['model'];
            $model->parent_id = $parent->id;
            $saving_data      = $model->toArray();

            if ($model->exists) {
                $model = $model->fresh();
            }

            $saved_child       = $model->batchSave($saving_data);
            $this->saved_ids[] = $saved_child->id;

            $this->saveSpecChildren($saved_child, $child_data);
        }
    }



    /**
     * Removes old children which are not saved as a current child.
     */
    protected function removeOldChildren()
    {
        $removable_ids = array_diff($this->all_old_children_ids, $this->saved_ids);
        $this->model()
             ->whereIn('id', $removable_ids)
             ->delete()
        ;
    }



    /**
     * Saves a spec group and its children.
     *
     * @param GroupSaveRequest $request
     *
     * @return string
     */
    public function save(GroupSaveRequest $request)
    {
        $model       = $request->model;
        $is_new      = $model->not_exists;
        $saved_group = $model->batchSave([
             'titles' => [
                  getLocale() => $request->title,
             ],
        ]);
        $this->saveSpecChildren($saved_group, $request->only('children'));
        $this->removeOldChildren();

        $redirect = $is_new
             ? route('specs.groups.single-action', [
                  'model_id' => $saved_group->hashid,
                  'action'   => 'edit',
             ]) : '';

        return $this->jsonAjaxSaveFeedback($saved_group->exists, [
             'success_redirect' => $redirect,
        ]);
    }



    /**
     * Deletes or restores the specified spec group.
     *
     * @param GroupDeleteRequest $request
     *
     * @return string
     */
    public function deleteOrRestore(GroupDeleteRequest $request)
    {
        if ($request->_submit == 'delete') {
            $ok = $request->model->delete();
        } else {
            $ok = $request->model->restore();
        }

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_callback' => "rowUpdate('tbl-groups','$request->model_hashid')",
        ]);
    }



    /**
     * Deletes the specified spec group hardly.
     *
     * @param GroupHardDeleteDeleteRequest $request
     *
     * @return string
     */
    public function hardDelete(GroupHardDeleteDeleteRequest $request)
    {
        $ok = $request->model->hardDelete();

        return $this->jsonAjaxSaveFeedback($ok, [
             'success_refresh' => 1,
        ]);
    }
}
