<?php

namespace Modules\Specs\Http\Controllers\V1;

use Illuminate\Contracts\Routing\ResponseFactory;
use Modules\Specs\Http\Requests\V1\PostSpecsRequest;
use Modules\Yasna\Services\YasnaApiController;
use Symfony\Component\HttpFoundation\Response;

class PostSpecsController extends YasnaApiController
{

    /**
     * list of post specs
     *
     * @param PostSpecsRequest $request
     *
     * @return array|ResponseFactory|Response
     */
    public function list(PostSpecsRequest $request)
    {
        $result = $request->model->toListSpecResource(); //TODO: To be moved to the new resource system.

        return $this->success($result);

    }
}
