<?php

namespace Modules\Specs\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class GroupHardDeleteDeleteRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = "spec";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->exists and $model->is_group and $model->trashed());
    }
}
