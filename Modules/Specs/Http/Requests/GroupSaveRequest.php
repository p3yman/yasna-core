<?php

namespace Modules\Specs\Http\Requests;

use App\Models\Spec;
use Modules\Specs\Providers\SpecsServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class GroupSaveRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = "spec";
    /**
     * Weather the any error has been occurred in the list of children.
     *
     * @var bool
     */
    protected $children_error = false;
    /**
     * All Site Locales
     *
     * @var null|array|string
     */
    protected $available_locales;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'title'    => 'required',
             'children' => 'array',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'children.array' => $this->runningModule()
                                      ->getTrans('messages.failure.not-enough-children-data'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;

        // Weather model is a new one or an existed group.
        return ($model->not_exists or $model->isGroup());
    }



    /**
     * Returns a list of site locales.
     *
     * @return array|bool|mixed|null|string
     */
    protected function availableLocales()
    {
        if (!$this->available_locales) {
            $this->available_locales = SpecsServiceProvider::getAvailableLocales();
        }

        return $this->available_locales;
    }



    /**
     * Checks if an id is a new one.
     *
     * @param string $id
     *
     * @return bool
     */
    protected function isNewId(string $id): bool
    {
        return starts_with($id, 'new');
    }



    /**
     * Creates a child model object.
     *
     * @param string $id
     *
     * @return Spec
     */
    protected function makeChildModelObject(string $id)
    {
        if ($this->isNewId($id)) {
            return model($this->model_name);
        } else {
            return model($this->model_name, $id);
        }
    }



    /**
     * Returns a list of categories from the given `specs` array.
     *
     * @param array $array
     *
     * @return array
     */
    protected function filterSpecsCategories(array $array)
    {
        return array_filter($array, function ($item) {
            $parent_id = ($item['parent_id'] ?? null);
            return !$parent_id;
        });
    }



    /**
     * Returns a list of children of a category from the given specs array.
     *
     * @param array  $array
     * @param string $category_id
     *
     * @return array
     */
    protected function filterSpecsCategoryChildren(array $array, string $category_id)
    {
        return array_filter($array, function ($item) use ($category_id) {
            $parent_id = ($item['parent_id'] ?? null);
            return ($parent_id == $category_id);
        });
    }



    /**
     * Reverse of the `array_dot()` Method.
     *
     * @param array $dotted
     *
     * @return array
     */
    protected function arrayUndot(array $dotted)
    {
        $array = [];

        foreach ($dotted as $key => $value) {
            array_set($array, $key, $value);
        }

        return $array;

    }



    /**
     * Fills a spec model as a spec category with the specified data.
     *
     * @param Spec  $model
     * @param array $data
     */
    protected function fillCategoryModel(Spec &$model, array $data)
    {
        $ready_data = $this->arrayUndot($data);
        $titles     = $ready_data['titles'];
        $titles     = array_only($titles, $this->availableLocales());
        $titles     = array_filter($titles, 'boolval');

        if (empty($titles)) {
            $this->children_error = true;
        } else {
            $model->titles = $titles;
        }
    }



    /**
     * Fills the titles of a child spec with the specified data.
     *
     * @param Spec  $model
     * @param array $data
     */
    protected function fillChildModelTitles(Spec &$model, array $data)
    {
        $titles = $data['titles'];
        $titles = array_only($titles, $this->availableLocales());
        $titles = array_filter($titles, 'boolval');

        if (empty($titles)) {
            $this->children_error = true;
        } else {
            $model->titles = $titles;
        }
    }



    /**
     * Fills the input type of a child spec with the specified data.
     *
     * @param Spec  $model
     * @param array $data
     */
    protected function fillChildModelInputType(Spec &$model, array $data)
    {
        $available_input_types = $this->model->availableInputTypes();
        $input_type            = $data['input_type'];

        if (in_array($input_type, $available_input_types)) {
            $model->input_type = $input_type;
        } else {
            $this->children_error = true;
        }
    }



    /**
     * Fills the options of a child spec with the specified data.
     *
     * @param Spec  $model
     * @param array $data
     */
    protected function fillChildModelOptions(Spec &$model, array $data)
    {
        $options = $data['options'];

        if ($model->input_type == 'select') {
            $model->options = $options;
        }
    }



    /**
     * Fills the default values of a child spec with the specified data.
     *
     * @param Spec  $model
     * @param array $data
     */
    protected function fileChildModelDefaultValues(Spec &$model, array $data)
    {
        $default_values = $data['default_values'];
        $default_values = array_only($default_values, $this->availableLocales());

        $model->default_values = $default_values;
    }



    /**
     * Fills a spec model as a child spec with the specified data.
     *
     * @param Spec  $model
     * @param array $data
     */
    protected function fillChildModel(Spec &$model, array $data)
    {
        $ready_data = $this->arrayUndot($data);
        $this->fillChildModelTitles($model, $ready_data);
        $this->fillChildModelInputType($model, $ready_data);
        $this->fillChildModelOptions($model, $ready_data);
        $this->fileChildModelDefaultValues($model, $ready_data);
    }



    /**
     * Build the information array of a spec category.
     *
     * @param string $category_id
     * @param array  $category_data
     * @param array  $specs
     *
     * @return array
     */
    protected function buildCategoryInfoArray(string $category_id, array $category_data, array $specs)
    {
        $category       = ['model' => null, 'children' => []];
        $children       = $this->filterSpecsCategoryChildren($specs, $category_id);
        $category_model = $this->makeChildModelObject($category_id);
        $this->fillCategoryModel($category_model, $category_data);

        $category['model'] = $category_model;

        foreach ($children as $child_id => $child_data) {
            $child_model = $this->makeChildModelObject($child_id);
            $this->fillChildModel($child_model, $child_data);
            $category['children'][] = ['model' => $child_model];
        }

        return $category;
    }



    /**
     * Corrects the specs field an saves the result as the children.
     */
    protected function correctSpecs()
    {
        $string = ($this->data['specs'] ?? '');
        $array  = (json_decode($string, true) ?? []);

        if (empty($array)) {
            $this->data['children'] = '';
            return;
        }

        $categories = $this->filterSpecsCategories($array);
        $result     = [];

        foreach ($categories as $category_id => $category_data) {
            $result[] = $this->buildCategoryInfoArray($category_id, $category_data, $array);
        }

        if ($this->children_error) {
            $this->data['children'] = '-';
        } else {
            $this->data['children'] = $result;
        }

        unset($this->data['specs']);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctSpecs();
    }
}
