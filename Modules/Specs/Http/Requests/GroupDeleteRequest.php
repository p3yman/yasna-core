<?php

namespace Modules\Specs\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class GroupDeleteRequest extends YasnaRequest
{
    /**
     * The Main Model's Name
     *
     * @var string
     */
    protected $model_name = "spec";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             '_submit' => 'required|in:delete,restore',
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $model = $this->model;
        return ($model->exists and $model->is_group);
    }
}
