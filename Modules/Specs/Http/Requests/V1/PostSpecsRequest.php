<?php

namespace Modules\Specs\Http\Requests\V1;

use App\Models\Post;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class CommentListRequest
 * <br>
 * The Request to Get Comments of a Post
 *
 * @property Post $model
 */
class PostSpecsRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "Post";


    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->checkPermission();
    }



    /**
     * Check out the terms to see specs
     *
     * @return bool
     */
    private function checkPermission()
    {
        if ($this->model->isPublished()) {
            return true;
        }
        if (!$this->model->isPublished() and $this->model->canEdit()) {
            return true;
        }
        return false;
    }
}
