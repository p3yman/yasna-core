@php
	$categories = $group->children;
@endphp


@foreach($categories as $category)
	<fieldset>
		<legend>
			{{ $category->title }}
		</legend>

		@foreach($category->children as $spec)
			@php
				$spec_hashid = $spec->hashid;
				$spec_pivot = $model->getSpecPivot($spec->id);
			@endphp

			@if($loop->iteration !== 1)
				<fieldset class="{{ ($loop->iteration === $loop->count)? "last-child" : "" }}">
					@endif

					<div class="row">
						<div class="col-xs-2 text-right">
							{!!
								widget('checkbox')
									->name("_specs[$spec_hashid][visible]")
									->value($spec_pivot->isNotHidden())
							 !!}
						</div>

						<div class="col-xs-10">
							{!!
								$spec->getWidget()
									->label($spec->title)
									->inForm()
									->name("_specs[$spec_hashid][value]")
									->value($spec_pivot->value ?? $spec->defaultValueIn($model->locale))
							!!}
						</div>
					</div>
				</fieldset>
				@endforeach

				@if (false)
	</fieldset>
	@endif

@endforeach

