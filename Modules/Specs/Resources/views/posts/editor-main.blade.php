@php
	$options = $__module->getProvider()->groupsCombo();
@endphp


@if (!$options[0]['id']) {{-- Only an empty option --}}
<div class="alert alert-danger">
	{{ $__module->getTrans('messages.failure.no-specs-group-available') }}
</div>
@php return @endphp
@endif

<div class="panel">
	<div class="panel-body" id="js_form_panel">
		<div class="mb-xl">
			{!!
				widget('combo')
					->name('spec_group')
					->options($options)
					->inForm()
					->label($__module->getTrans('general.group'))
					->id('js_spec_group_selector')
					->blankValue('')
					->value($model->spec_group)
			 !!}
		</div>

		<div id="js_product_specs_form"></div>
	</div>
</div>


<script>
    $(document).ready(function () {
        // Changes the from in form selector of editor
        let group_selector = $('#js_spec_group_selector');

        function refreshForm() {

            if (!group_selector.val().length) {
                $('#js_product_specs_form').empty();
                return;
            }

            let target  = group_selector.val();
            let formUrl = "{{ route('specs.posts.group-children', [
            	'post_hashid' => ($model->hashid ?: hashid(0)),
            	'group_hashid' => '__GROUP__',
			]) }}"
                .replace('__GROUP__', target)
            ;


            // Add loading
            $('#js_form_panel').addClass('whirl');
            $('#js_product_specs_form').load(formUrl, function (responseTxt, textStatus, xhr) {
                // Remove loading when done
                $('#js_form_panel').removeClass('whirl');

                // Shows error message
                if (textStatus === "error") {
                    $.notify("{{ trans('specs::general.loading_error') }}", {"status": "danger"});
                }
            })
        }

        group_selector.on('change', function () {
            refreshForm()
        });

        refreshForm();
    })
</script>
