@extends('manage::layouts.template')


@section('content')
	@include("manage::downstream.tabs")
	@include($__module->getBladePath('groups.browse-toolbar'))
	@include($__module->getBladePath('groups.browse-grid'))
@append

