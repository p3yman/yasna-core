@include('manage::widgets.grid-rowHeader' , [
	'handle' => "counter" ,
	'refresh_url' => route('specs.groups.single-action', ['act' => 'browse-row', 'model_id' => $model->hashid]),
])

@php
	$edit_url = route('specs.groups.single-action', [
					  'model_id' => $model->hashid,
					  'action'   => 'edit',
				 ])
@endphp


@include($__module->getBladePath('groups.browse-row-title'))

@include("manage::widgets.grid-actionCol"  , [
	'actions' => [
		[
			'pencil',
			trans('manage::forms.button.edit'),
			$edit_url,
		],
		[
			'trash-o',
			trans('manage::forms.button.soft_delete'),
			'modal:' . route('specs.groups.single-action', [
				'act' => 'delete',
				'model_id' => $model->hashid,
				'option0' => 'delete'
			], false),
			!$model->trashed(),
		],
		[
			'recycle',
			trans('manage::forms.button.undelete'),
			'modal:' . route('specs.groups.single-action', [
				'act' => 'delete',
				'model_id' => $model->hashid,
				'option0' => 'restore'
			], false),
			$model->trashed(),
		],
		[
			'times',
			trans('manage::forms.button.hard_delete'),
			'modal:' . route('specs.groups.single-action', [
				'act' => 'hard-delete',
				'model_id' => $model->hashid,
			], false),
			$model->trashed(),
		],
	],
])
