@extends('manage::layouts.template')

@php
	$page = $option[0];
	$locales = $__module->getProvider()->getAvailableLocales();
	$current_locale = getLocale();
@endphp

@section('content')
	<div class="row">
		<div class="col-md-9">
			@include('specs::groups.specs-section.main')
		</div>
		<div class="col-md-3">
			@include('specs::groups.links')
		</div>
	</div>

	@include('specs::groups.templates.category')
	@include('specs::groups.templates.field')
@append
