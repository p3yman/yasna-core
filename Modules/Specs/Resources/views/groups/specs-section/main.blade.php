<div class="panel reset-tform">
	<div class="panel-heading">
		<h4>
			{{ $__module->getTrans('general.title') }}
		</h4>

	</div>
	{!!
		widget('form-open')
			->target(route('specs.groups.save'))
			->class('js')
	!!}

	{!! widget('hidden')->name('hashid')->value($model->hashid) !!}

	<div class="panel-body">


		{!!
		    widget('input')
		    ->label('tr:specs::general.product_title')
		    ->inForm()
		    ->name('title')
		    ->value($model->title)
		 !!}

		<hr>

		@include('specs::groups.specs-section.grouping')

		{!!
		    widget('textarea')
		    ->inForm()
		    ->value('')
		    ->name('specs')
		    ->containerClass('hidden')
		    ->id('js_specs_result')
		 !!}

	</div>
	<div class="panel-footer">
		{!!
			widget('button')
				->label('tr:specs::general.save')
				->class('btn-success btn-taha')
				->id('js_submit')
				->type('submit')
		!!}

		{!! widget('feed') !!}

	</div>


	{!! widget('form-close') !!}
</div>
