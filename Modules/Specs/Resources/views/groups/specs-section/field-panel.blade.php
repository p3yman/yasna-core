<li class="js_spec_draggable_item">
	<div class="panel panel-default">
		<div class="panel-heading clearfix">
			<h4 class="panel-title pull-left">
				<em class="fa fa-bars js_sortable_handle"></em>
				<a data-toggle="collapse" class="mh" href="#{!! $id or "@{{id}}" !!}" data-title-getter>
					{{ isset($title) ? $title : $__module->getTrans('general.new_spec') }}
				</a>
			</h4>

			<div class="pull-right js_control-btns">
				<button class="btn btn-danger btn-xs js_delete_field" title="{{ $__module->getTrans('general.remove') }}">
					<em class="fa fa-trash"></em>
				</button>
			</div>

		</div>
		<div id="{!! $id or "@{{id}}" !!}" class="panel-collapse collapse">
			<div class="panel-body">
				@foreach ($locales as $locale)
					{!!
						widget('input')
						->label($__module->getTrans('general.title_in')." ".trans("yasna::lang.$locale"))
						->value($field->titleIn($locale))
						->name("titles.$locale")
						->class(($locale == $current_locale) ? 'js_title_setter' : '')
						->inForm()
					 !!}
				@endforeach

				{!!
					widget('combo')
					->label('tr:specs::general.data_type')
					->class('js_data_type')
					->options(model('spec')->availableInputTypesCombo())
					->valueField('value')
					->value($field->input_type)
					->name('input_type')
					->inForm()
				 !!}

				<div class="js_select_options hidden">
					{!!
						widget('textarea')
						->label('tr:specs::general.options')
						->name('options')
						->value($field->spreadMeta()->options)
						->inForm()
					 !!}
				</div>


				@foreach ($locales as $locale)
					{!!
						widget('input')
						->label($__module->getTrans('general.default_value_in', ['locale' => trans("yasna::lang.$locale")]))
						->name("default_values.$locale")
						->value($field->defaultValueIn($locale))
						->inForm()
					 !!}
				@endforeach


				{!!
					widget('hidden')
					->name('parent_id')
					->value(isset($parentId) ? $parentId : '@{{parentId}}')

				 !!}

			</div>
		</div>
	</div>
</li>
