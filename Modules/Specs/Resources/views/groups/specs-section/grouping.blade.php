{!!
    widget('button')
    ->label('tr:specs::general.add_group')
    ->icon('plus')
    ->class('btn-primary btn-lg')
    ->id('js_add_new_spec_cat')
 !!}


@include('specs::groups.specs-section.list')
