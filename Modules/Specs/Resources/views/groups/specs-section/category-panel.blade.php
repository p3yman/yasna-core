<li class="js_spec_item @if(!isset($fields) or !count($fields)) js_no_child @endif">
	<div class="panel panel-inverse mt0">
		<div class="panel-heading clearfix">
			<h4 class="panel-title pull-left">
				<em class="fa fa-bars js_sortable_handle"></em>
				<a data-toggle="collapse" class="mh" href='#{!! $id or "@{{id}}" !!}' data-title-getter>
					{{ isset($title) ? $title : $__module->getTrans('general.new_category') }}
				</a>
			</h4>


			<div class="pull-right js_control-btns">
				<button class="btn btn-primary btn-xs js_add_field" title="{{ $__module->getTrans('general.add_subgroup') }}"
						data-toggle="tooltip">
					<em class="fa fa-plus"></em>
				</button>

				<button class="btn btn-danger btn-xs js_delete_cat" title="{{ $__module->getTrans('general.remove') }}"
						data-toggle="tooltip">
					<em class="fa fa-trash"></em>
				</button>
			</div>
		</div>

		<div id="{!! $id or '@{{id}}' !!}" class="panel-collapse collapse">
			<div class="panel-body">
				@foreach ($locales as $locale)
					@php $title = $category->titleIn($locale); @endphp
					{!!
						widget('input')
							->label($__module->getTrans('general.title_in')." ".trans("yasna::lang.$locale"))
							->value($title)
							->name("titles.$locale")
							->class(($locale == $current_locale) ? 'js_title_setter' : '')
							->inForm()
					 !!}
				@endforeach
			</div>
		</div>
	</div>

	<ol class="p0 panel-group js_spec_draggable" data-parent-id="{!! $id or "@{{id}}" !!}">
		@if(isset($fields) and count($fields))
			@foreach($fields as $field)
				@include('specs::groups.specs-section.field-panel',[
					"id" => $field->hashid ,
					"title" => $field->title,
					"parentId" => $id,
					"field" => $field,
				])
			@endforeach
		@endif
	</ol>
</li>
