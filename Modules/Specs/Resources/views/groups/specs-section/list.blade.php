@php
	$categories = $model->children;
@endphp

<div class="specs-list mt-xl" >
	<ol class="p0 panel-group" id="js_specs_sortable">
		@foreach ($categories as $keeeey =>  $category)
			@include('specs::groups.specs-section.category-panel',[
				"category" => $category,
				"id" => $category->hashid ,
				"title" => $category->title ,
				"fields" => $category->children ,
			])
		@endforeach
	</ol>
</div>
