@include("manage::layouts.modal-delete" , [
	'form_url' => route('specs.groups.hard-delete'),
	'title_value' => $model->title ,
	'save_label' => trans("manage::forms.button.hard_delete"),
	'save_shape' => 'danger' ,
])
