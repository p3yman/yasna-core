@include("manage::widgets.toolbar" , [
    'title'             => $page[0][1] . ' / ' . $page[1][1],
    'buttons'           => [
        [
			'target'  => route('specs.groups.single-action', ['model_id' => hashid(0), 'action' => 'create']),
			'type'    => 'success',
			'caption' => trans_safe('manage::forms.button.add_to') . ' ' . $page[0][1],
			'icon' => "plus-circle",
        ],
    ],
])
