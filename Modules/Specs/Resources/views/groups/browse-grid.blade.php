<div class="row">
	<div class="col-xs-12" id="main-grid">

		<div class="col-xs-12">
			@include("manage::widgets.grid" , [
				'table_id' => "tbl-groups",
				'row_view' => $__module->getBladePath('groups.browse-row'),
				'handle' => "counter",
				'models' => $models,
				'headings' => $browse_headings,
				'operation_heading' => true,
			])

		</div>
	</div>
</div>
