@php $other_groups = $option[1] @endphp

<div class="panel">
	<div class="panel-heading">
		<h4>
			{{ $__module->getTrans('general.related_links') }}
		</h4>
	</div>

	<div class="panel-body">
		<div class="list-group">
			@if($other_groups->count())
				@foreach($other_groups as $group)

					@php
						$link = route('specs.groups.single-action', [
							'model_id' => $group->hashid,
							'action' => 'edit'
						]);
					@endphp
					<a href="{{ $link }}" class="list-group-item">
						{{ $group->titleIn(getLocale()) }}
					</a>
				@endforeach
			@else
				<div class="box-placeholder">
					<p class="text-center">
						{{ $__module->getTrans('general.no_related_link') }}
					</p>
				</div>
			@endif
		</div>
	</div>
</div>
