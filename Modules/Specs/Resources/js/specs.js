/**
 * Specs js functions
 * -------------------------
 * Created by Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2018-09-04
 */


jQuery(function($){


    // Document Ready
    $(document).ready(function () {
        let sortable_container = document.getElementById('js_specs_sortable');

        // Disable enter in inputs (enter creates new field)
        $('.specs-list :input').on('keypress',function (e) {
            if(e.keyCode === 13){
                e.preventDefault();
            }
        });

        // Makes categories sortable
        Sortable.create(sortable_container, {
            animation: 150,
            draggable: '.js_spec_item',
            handle: '.js_sortable_handle',
            sort: true
        });

        // Makes sub-categories sortable
        makeSortable();

        // Remove Category
        $(document).on('click','.js_delete_cat',function (e) {
            e.preventDefault();
            $(this).parents('.js_spec_item').remove();
        });

        // Remove Fields
        $(document).on('click','.js_delete_field',function (e) {
            e.preventDefault();
            let parent = $(this).parents('.js_spec_draggable_item');
            let catList = parent.parent('.js_spec_draggable');
            // Remove First, so that checking the children
            // finds correct children count
            parent.remove();
            checkCategoryHasChild(catList);
        });

        // Create new category
        $(document).on('click','#js_add_new_spec_cat',function (e) {
            e.preventDefault();
            addNewCategory();
            makeSortable();
        });
        
        // Create new field
        $(document).on('click','.js_add_field',function (e) {
            e.preventDefault();
            let catList = $(this).parents('.js_spec_item').find('.js_spec_draggable');
            let parentId = catList.data('parent-id');
            addNewField(parentId);
            checkCategoryHasChild(catList);
        });

        // Dynamic panel title setter
        $(document).on('input','.js_title_setter', function () {
            let heading = $(this).closest('.panel').find('[data-title-getter]');
            heading.text($(this).val());
        });

        // Data type watcher
        $(document).on('change', '.js_data_type', function () {

            if(typeof $(this).attr('id') === "undefined"){
                return;
            }

            let select = $(this);

            if($(this).val() === "select"){
                select.siblings('.js_select_options').removeClass('hidden');
            }else{
                select.siblings('.js_select_options').addClass('hidden');
            }
        });

        // Calls data type event once
        $('.js_data_type').change();

        // Gets form categories info and place in a textarea
        $('#js_submit').on('click', function (e) {
            setParents();
            let formData = dataGetter();
            $('#js_specs_result').val(JSON.stringify(formData));

        });
    });



    /**
     * Checks if category has child or not
     * Add "js_no_child" class to categories with no child
     *
     * @param categoryList
     */
    function checkCategoryHasChild(categoryList) {
        let catList = $(categoryList);
        let cat = catList.parent('.js_spec_item');

        // Check Existence
        if(!cat.length){
            return;
        }

        if(catList.children('.js_spec_draggable_item').length){
            cat.removeClass('js_no_child');
        }else {
            cat.addClass('js_no_child');
        }
    }



    /**
     * Creates String Id
     * @param prefix
     * @param postfix
     * @return {string}
     */
    function generateId(prefix, postfix) {
        prefix = (prefix) ? prefix : "";
        postfix = (postfix) ? postfix : "";

        let hashId = Date.now().toString(36);

        return prefix + hashId + postfix;
    }



    /**
     * Adds new category to the list
     */
    function addNewCategory() {
        let id = generateId('new_');
        let el = createCategoryEl(id);

        $('#js_specs_sortable').append(el);
    }



    /**
     * Creates new category of template
     * @param id
     */
    function createCategoryEl(id) {
       let temp = $.trim($('#js_category_template').html());
       return temp.replace(/{{id}}/ig, id)
    }



    /**
     * Adds new field to category
     * @param parentId
     */
    function addNewField(parentId) {
        let id = generateId('new_');
        let el = createFieldEl(id,parentId);

        $("[data-parent-id="+ parentId +"]").append(el);
    }



    /**
     * Creates new field of template
     * @param id
     * @param parentId
     */
    function createFieldEl(id, parentId) {
        let temp = $.trim($('#js_field_template').html());
        return temp.replace(/{{id}}/ig, id)
                    .replace(/{{id}}/ig, parentId);
    }



    /**
     * Makes elements sortable
     */
    function makeSortable() {
        let sortable_fields = document.getElementsByClassName('js_spec_draggable');
        [].forEach.call(sortable_fields, function (el){
            Sortable.create(el, {
                group: 'specs',
                draggable: '.js_spec_draggable_item',
                handle: '.js_sortable_handle',
                animation: 150,
                sort: true,
                onEnd: function (e) {
                    if(e.to !== e.from){
                        checkCategoryHasChild(e.to);
                        checkCategoryHasChild(e.from);
                    }
                },
            });
        });
    }


    /**
     * Sets parent id for children based on resent changes
     */
    function setParents() {
        $('.js_spec_draggable').each(function (index, group) {
            let parentId = $(group).data('parent-id');

            $(group).find('input[name="parent_id"]').val(parentId);
        })
    }


    /**
     * Gets categories data
     */
    function dataGetter() {
        let data = {};

        $('.js_spec_item .js_spec_draggable').each(function (index, cat) {
            let catId = $(cat).data('parent-id');
            let panelBody = $('#'+ catId + ' .panel-body');
            let fields = $(cat).children('.js_spec_draggable_item');

            data[catId] = {};

            // Get category form inputs
            panelBody.find(' :input').each(function (index, input) {
               let name = $(input).attr('name');
               data[catId][name] = $(input).val();
            });

            // Check for fields existence
            if(fields.length){
                fields.each(function (index, field) {
                    let fieldId = $(field).find('.panel-collapse').attr('id');
                    data[fieldId] = {};

                    $("#"+ fieldId + ' .panel-body :input').each(function (index, input) {
                        let name = $(input).attr('name');
                        if(typeof name !== "undefined"){
                            data[fieldId][name] = $(input).val();
                        }
                    });
                })
            }


        });
        return data;
    }
}); //End Of siaf!
