<?php
return [
     "title"            => "Specs",
     "related_links"    => "Related Links",
     "no_related_link"  => "No related links was found.",
     "no_field"         => "No Field Exist.",
     "default_value_in" => "Default Value in :locale",


     "product_title" => "Product Title",
     "add_group"     => "Add Group",
     "add_subgroup"  => "Add Subgroup",
     "title_in"      => "Title in",
     "category"      => "Categories",
     "data_type"     => "Data Type",
     "remove"        => "Remove",
     "new_category"  => "New Category",
     "new_spec"      => "New Spec",
     "options"       => "Options",
     "save"          => "Save",


     "groups" => "Specs Groups",
     "group"  => "Specs Group",

     "loading_error" => "An Error occurred!",
];
