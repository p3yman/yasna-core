<?php
return [
     "failure" => [
          "not-enough-children-data" => "At least one of groups or subgroups have not enough information.",
          "no-specs-group-available" => "No specs group available.",
     ],
];
