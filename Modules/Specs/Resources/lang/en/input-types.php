<?php
return [
     "text"      => "Short Text",
     "paragraph" => "Long Text",
     "boolean"   => "Yes or No",
     "select"    => "Select Box",
];
