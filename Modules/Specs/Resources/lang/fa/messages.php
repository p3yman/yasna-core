<?php
return [
     "failure" => [
          "not-enough-children-data" => "برای حداقل یکی از گروه‌ها یا زیرگروه‌ها اطلاعات کافی پر نشده‌است.",
          "no-specs-group-available" => "هیچ گروهی از مشخصات در دسترس نیست.",
     ],
];
