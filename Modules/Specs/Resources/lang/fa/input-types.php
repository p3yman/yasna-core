<?php
return [
     "text"      => "متن کوتاه",
     "paragraph" => "متن بلند",
     "boolean"   => "بله یا خیر",
     "select"    => "فهرست کرکره‌ای",
];
