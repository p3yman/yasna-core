<?php
return [

     "title"            => "مشخصات",
     "related_links"    => "لینک‌های مشابه",
     "no_related_link"  => "لینک مشابهی یافت نشد",
     "no_field"         => "مشخصات ثبت نشده.",
     "default_value_in" => "مقدار پیش‌فرض به :locale",


     "product_title" => "نام محصول",
     "add_group"     => "افزودن گروه جدید",
     "add_subgroup"  => "افزودن زیرگروه",
     "title_in"      => "عنوان در",
     "category"      => "دسته‌بندی",
     "data_type"     => "نوع داده",
     "remove"        => "حذف",
     "new_category"  => "گروه جدید",
     "new_spec"      => "مورد جدید",
     "options"       => "گزینه‌ها",
     "save"          => "ذخیره",


     "groups" => "گروه‌های مشخصات",
     "group"  => "گروه مشخصات",

     "loading_error" => "در سیستم خطایی رخ داده است.",


];
