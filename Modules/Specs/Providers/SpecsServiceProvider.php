<?php

namespace Modules\Specs\Providers;

use App\Models\Spec;
use Modules\Specs\Http\Endpoints\V1\ListEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class SpecsServiceProvider
 *
 * @package Modules\Specs\Providers
 */
class SpecsServiceProvider extends YasnaProvider
{


    /**
     * Registers handlers for posts editor page.
     */
    protected function registerPostsEditorHandlers()
    {
        service('posts:editor_handlers')
             ->add('specs')
             ->method('specs:HandleController@editor')
        ;
    }



    /**
     * Register the downstream part.
     *
     * @return void
     */
    protected function registerDownstream()
    {
        module('manage')
             ->service('downstream')
             ->link('specs-groups')
             ->trans($this->module()->getTrans('general.groups'))
             ->method($this->module()->getAlias() . ':GroupsBrowseController@downstream')
             ->condition(function () {
                 return user()->isSuperadmin();
             })
             ->order(31)
        ;
    }



    /**
     * Registers model traits.
     */
    protected function registerModelTraits()
    {
        $this->addModelTrait('PostSpecTrait', 'Post');
        $this->addModelTrait('PostSpecsResourcesTrait', 'Post');
    }



    /**
     * Registers post save services.
     */
    protected function registerPostSave()
    {
        module('posts')
             ->service('after_save')
             ->add('specs')
             ->method($this->moduleName() . ":HandleController@saveSpecs")
        ;
    }



    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerPostsEditorHandlers();
        $this->registerDownstream();
        $this->registerModelTraits();
        $this->registerPostSave();
        $this->registerEndpoints();
    }



    /**
     * Returns all available site locales.
     *
     * @return array|bool|mixed
     */
    public static function getAvailableLocales()
    {
        return (get_setting('site_locales') ?? [getLocale()]);
    }



    /**
     * Returns an array of groups to be used in a Combo widget.
     *
     * @return array
     */
    public static function groupsCombo()
    {
        $groups = model('spec')
             ->groups()
             ->orderByDesc('created_at')
             ->get()
        ;

        if (!$groups->count()) {
            return [['id' => '', 'title' => '']];
        }

        return $groups->map(function (Spec $group) {
            return [
                 'title' => $group->title,
                 'id'    => $group->hashid,
            ];
        })->toArray()
             ;
    }



    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }

        endpoint()->register(ListEndpoint::class);
    }
}
