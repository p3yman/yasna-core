<?php

namespace Modules\Specs\Services\Traits;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Modules\Specs\Entities\Spec;

/**
 * Trait SpecsCompareColumnsAndHeadersTrait is a set of methods for preparing
 * information about compare headers and columns
 *
 * @package Modules\Specs\Services\Traits
 */
trait SpecsCompareColumnsAndHeadersTrait
{
    /**
     * Return the columns of the specs group. As we supposed that all specs
     * groups are same, so for getting this columns, we used the specs group of
     * the first added product.
     *
     * @return null|EloquentCollection
     */
    private function getSpecsColumns(): ?EloquentCollection
    {
        $post = $this->getPosts()->first();
        if (!$post) {
            return null;
        }

        return model('spec', $post->getMeta('spec_group'))->children;
    }



    /**
     * Return an array version of specs columns
     *
     * @param EloquentCollection $specs
     *
     * @see SpecsCompare::getSpecsColumns()
     * @return array
     */
    private function getSpecsColumnsArray(EloquentCollection $specs): array
    {
        return $specs->map(function (Spec $spec) {
            return [
                 $spec->titleIn(getLocale()) => $spec->children
                      ->map(function (Spec $childSpec) {
                          return $childSpec->titleIn(getLocale());
                      })
                      ->toArray(),
            ];
        })
                     ->toArray()
             ;
    }



    /**
     * Return an array of all headers which must be used on a compare table
     *
     * @param array $specs_columns Array of a specs group columns
     *
     * @return array
     */
    private function getCompareHeaders(array $specs_columns): array
    {
        $headers = $this->getProductMetaInformation();

        foreach ($specs_columns as $specs_column) {
            $key           = array_keys($specs_column)[0];
            $headers[$key] = $specs_column[$key];
        }

        return $headers;
    }
}
