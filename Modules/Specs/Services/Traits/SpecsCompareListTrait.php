<?php

namespace Modules\Specs\Services\Traits;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Modules\Posts\Entities\Post;
use Modules\Specs\Entities\Spec;

/**
 * Trait SpecsCompareListTrait is a set of methods for generating rows of a
 * compare table.
 *
 * @package Modules\Specs\Services\Traits
 */
trait SpecsCompareListTrait
{
    use SpecsCompareValueNormalizerTrait;



    /**
     * Return a compare list for a product. It contains all spec lists of a
     * product.
     *
     * @param Post               $post
     * @param array              $headers
     * @param EloquentCollection $specs
     *
     * @return array
     */
    private function getCompareListOfAnItem(
         Post $post,
         array $headers,
         EloquentCollection $specs
    ): array {
        $results = [];

        foreach ($headers as $header) {
            $results[$header] = $post->{$header};
        }

        foreach ($specs as $spec) {
            $results[$spec->titleIn(getLocale())] = $this->getSpecsListOfAnItem(
                 $post,
                 $spec
            );
        }

        return $results;
    }



    /**
     * Return a complete compare table of the given products.
     *
     * @param EloquentCollection $columns Column of specs of the product
     *
     * @return array
     */
    private function getCompareItems(EloquentCollection $columns): array
    {
        $posts   = $this->getPosts();
        $headers = $this->getProductMetaInformation();

        return $posts->map(function ($post) use ($headers, $columns) {
            return $this->getCompareListOfAnItem($post, $headers, $columns);
        })
                     ->toArray()
             ;
    }



    /**
     * Return a spec list of a product. A spec list is a set of rows on compare
     * list of a product.
     *
     * @param Post $post
     * @param Spec $spec
     *
     * @return array
     */
    private function getSpecsListOfAnItem(Post $post, Spec $spec): array
    {
        $results = [];

        foreach ($post->specs_pivots as $spec_pivot) {
            if ($spec->titleIn(getLocale()) == $spec_pivot->spec->parent->titleIn(getLocale())) {
                $results[$spec_pivot->spec->titleIn(getLocale())] = [
                     'type'  => $spec_pivot->spec->input_type,
                     'value' => $this->normalizeSpecPivotValues(
                          $spec_pivot->value,
                          $spec_pivot->spec->input_type
                     ),
                ];
            }
        }

        return $results;
    }
}
