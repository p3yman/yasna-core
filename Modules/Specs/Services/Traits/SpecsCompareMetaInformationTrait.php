<?php

namespace Modules\Specs\Services\Traits;


use Modules\Specs\Services\SpecsCompare;

/**
 * Trait SpecsCompareMetaInformationTrait is a set of functions for generating
 * and getting the meta information of a product. A meta information is every
 * thing which must be received from the product and doesn't save on the specs
 * information.
 *
 * @package Modules\Specs\Services\Traits
 */
trait SpecsCompareMetaInformationTrait
{
    /**
     * Add meta information to specs table. Meta information are a set of data
     * which are not on spec tables and must be fetched from product itself.
     * They can be elements like abstracts, product title or something like
     * them.
     *
     * @param string $column Column which must be added to specs table
     *
     * @return SpecsCompare
     */
    public function addProductMetaInformation(string $column): SpecsCompare
    {
        $column = trim($column);
        if (strlen($column)) {
            static::$product_meta_information[$column] = $column;
        }

        return $this;
    }



    /**
     * Return all product meta information.
     *
     * @see SpecsCompare::addProductMetaInformation()
     * @return array
     */
    public function getProductMetaInformation(): array
    {
        return array_values(static::$product_meta_information);
    }



    /**
     * Remove a product meta information from being on compare table
     *
     * @param string $column
     *
     * @see SpecsCompare::addProductMetaInformation()
     * @return SpecsCompare
     */
    public function removeProductMetaInformation(string $column): SpecsCompare
    {
        if (isset(static::$product_meta_information[$column])) {
            unset(static::$product_meta_information[$column]);
        }

        return $this;
    }
}
