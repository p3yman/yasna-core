<?php

namespace Modules\Specs\Services\Traits;

/**
 * Trait SpecsCompareValueNormalizerTrait is a set of methods to make the
 * values representation in a unified way. Each normalizer must be a method
 * with the name in `typeNormalizer` format (camelCase). For example
 * `booleanNormalizer` or `shortTextNormalizer`. The signature of normalizer is
 * `function typeNormalizer(mixed $value): mixed $normalizedValue`.
 *
 * @package Modules\Specs\Services\Traits
 */
trait SpecsCompareValueNormalizerTrait
{
    /**
     * Normalize spec pivot values to show all values in a unified manner.
     *
     * @todo Add more normalizer cases.
     *
     * @param mixed  $value
     * @param string $type
     *
     * @return mixed
     */
    private function normalizeSpecPivotValues($value, string $type)
    {
        // if value is null, it's OK and all is done!
        if (is_null($value)) {
            return null;
        }

        $method = camel_case($type . "Normalizer");
        if (method_exists($this, $method)) {
            return $this->{$method}($value);
        }

        return $value;
    }



    /**
     * Normalize the given value for evaluating it as a boolean.
     *
     * @param mixed $value
     *
     * @return bool
     */
    private function booleanNormalizer($value)
    {
        return boolval($value);
    }
}
