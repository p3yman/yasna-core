<?php

namespace Modules\Specs\Services\Traits;


use Modules\Specs\Services\SpecsCompare;

/**
 * Trait SpecsCompareErrorsTrait is a set of methods for setting and getting
 * errors occurred on the process of making compare table.
 *
 * @package Modules\Specs\Services\Traits
 */
trait SpecsCompareErrorsTrait
{
    /**
     * Return a list of all errors
     *
     * @return array
     */
    public function getErrors(): array
    {
        return static::$errors;
    }



    /**
     * Checks compare bag has some errors or not
     *
     * @return bool
     */
    public function hasErrors(): bool
    {
        return !empty(static::$errors);
    }



    /**
     * Add an error to the errors list
     *
     * @param string|int $error
     *
     * @return SpecsCompare
     */
    private function addError($error): SpecsCompare
    {
        static::$errors[$error] = $error;

        return $this;
    }
}
