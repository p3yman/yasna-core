<?php

namespace Modules\Specs\Services\Traits;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;

/**
 * Trait SpecsComparePostsTrait is a set of methods for working with posts
 * which in fact act as the backbone of a product.
 *
 * @package Modules\Specs\Services\Traits
 */
trait SpecsComparePostsTrait
{
    use SpecsCompareErrorsTrait;



    /**
     * Get all post which are related to the current saved products
     *
     * @return EloquentCollection
     */
    private function getPosts(): EloquentCollection
    {
        if (static::$posts_must_be_refreshed) {
            $post_ids                        = $this->getProducts();
            static::$posts                   = model('post')
                 ->with('specsPivots')
                 ->whereIn('id', $post_ids)
                 ->whereHas('specsPivots', function ($query) {
                     $query->whereNull('hid_at');
                 })
                 ->get()
            ;
            static::$posts_must_be_refreshed = false;
        }

        if (is_null(static::$posts)) {
            $this->addError(static::ERROR_NO_PRODUCTS_FOUND);
            static::$posts = new EloquentCollection();
        }

        return static::$posts;
    }
}
