<?php

namespace Modules\Specs\Services\Traits;


use Modules\Specs\Services\SpecsCompare;

/**
 * Trait SpecsCompareProductsTrait is a set of methods for working with
 * products. In fact this trait add ability of adding, getting and removing
 * products from the compare bag.
 *
 * @package Modules\Specs\Services\Traits
 */
trait SpecsCompareProductsTrait
{
    /**
     * With this function we can add product(s) to the compare list. It can be
     * used in form of addProduct(2) or addProduct(1,2,3,"xWRGA"). The number of
     * arguments are optional and they expected to be ID or HashID of products.
     *
     * @param int|string $product_id [, $...]
     *
     * @return SpecsCompare
     */
    public function addProduct(): SpecsCompare
    {
        foreach (func_get_args() as $product) {
            if (is_string($product)) {
                $product = hashid($product);
            }

            static::$products[]              = $product;
            static::$posts_must_be_refreshed = true;
        }

        return $this;
    }



    /**
     * Clear the bag which contain added products. Empty the Compare list!
     *
     * @return SpecsCompare
     */
    public function clearProducts(): SpecsCompare
    {
        static::$products = null;
        static::$posts    = null;
        static::$errors   = null;

        return $this;
    }



    /**
     * Getter of products (all products which must be compared).
     *
     * @return array
     */
    public function getProducts(): array
    {
        // remove duplicate records
        $products = array_flip(array_flip(static::$products));

        return array_values($products);
    }
}
