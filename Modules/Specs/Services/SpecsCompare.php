<?php

namespace Modules\Specs\Services;


use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Modules\Posts\Entities\Post;
use Modules\Specs\Services\Traits\SpecsCompareColumnsAndHeadersTrait;
use Modules\Specs\Services\Traits\SpecsCompareListTrait;
use Modules\Specs\Services\Traits\SpecsCompareMetaInformationTrait;
use Modules\Specs\Services\Traits\SpecsComparePostsTrait;
use Modules\Specs\Services\Traits\SpecsCompareProductsTrait;

/**
 * Class SpecsCompare prepare a service for comparing some products. In fact
 * this class prepare a table in array product which is a combination of
 * products specs and products attributes (like abstract, direct url, ...). If
 * there is no products or products don't have a same specs group, no result
 * will be return.
 *
 * @package Modules\Specs\Services
 */
class SpecsCompare
{
    use SpecsCompareListTrait;
    use SpecsComparePostsTrait;
    use SpecsCompareProductsTrait;
    use SpecsCompareMetaInformationTrait;
    use SpecsCompareColumnsAndHeadersTrait;

    public const ERROR_PRODUCTS_ARE_NOT_COMPARABLE = 0;
    public const ERROR_NO_PRODUCTS_FOUND           = 1;

    /**
     * List of all products which must be compared
     *
     * @var array $products
     */
    private static $products = [];

    /**
     * List of all posts related to current save products
     *
     * @var EloquentCollection $posts
     */
    private static $posts;

    /**
     * List of all errors occurred on whole process of comparing
     *
     * @var array $errors
     */
    private static $errors = [];

    /**
     * This flag indicate that the list of posts must be refreshed from DB or
     * not. This flag will be checked on getPosts method.
     *
     * @var bool $posts_must_be_refreshed
     */
    private static $posts_must_be_refreshed = false;

    /**
     * Array contains all meta information -means are not in specs table and
     * must be retrieved from product information. They actually must be column
     * names in post table.
     *
     * @var array $product_meta_information
     */
    private static $product_meta_information = [
         'abstract',
         'hashid',
         'direct_url',
    ];



    /**
     * SpecsCompare constructor.
     *
     * @param array $product_ids Array of all products which must be compared.
     */
    public function __construct(array $product_ids)
    {
        if (empty(static::$products)) {
            static::$products = $product_ids;

            static::$posts_must_be_refreshed = true;
        } elseif (!empty($product_ids)) {
            static::$products = array_merge(static::$products, $product_ids);

            static::$posts_must_be_refreshed = true;
        }
    }



    /**
     * Checks that given product are comparable or not
     *
     * @return bool
     */
    public function productsAreComparable(): bool
    {
        $posts = $this->getPosts();
        if (is_null($posts)) {
            $this->addError(static::ERROR_PRODUCTS_ARE_NOT_COMPARABLE);

            return false;
        }

        $first_post = $posts->first();
        if (is_null($first_post)) {
            $this->addError(static::ERROR_PRODUCTS_ARE_NOT_COMPARABLE);

            return false;
        }

        $spec_group = $first_post->getMeta('spec_group');
        if (is_null($spec_group)) {
            $this->addError(static::ERROR_PRODUCTS_ARE_NOT_COMPARABLE);

            return false;
        }

        $can_compare = $posts->every(function (Post $post) use ($spec_group) {
            return $post->getMeta('spec_group') == $spec_group;
        });
        if (!$can_compare) {
            $this->addError(static::ERROR_PRODUCTS_ARE_NOT_COMPARABLE);

            return false;
        }

        return true;
    }



    /**
     * Create a compare array for given products. Format of this array is like
     * this:
     * ```php
     *      [
     *          'headers' => [
     *              'meta' => [
     *                  'hashid',
     *                  'abstract',
     *              ],
     *              'prices' => [
     *                  'sold-out',
     *                  'price',
     *              ],
     *              'sizes' => [
     *                  'weight',
     *                  'height',
     *              ],
     *          ],
     *          'items' => [
     *              [
     *                  'meta' => [
     *                      'hashid'   => 'xWRGA',
     *                      'abstract' => 'That is an abstract...',
     *                  ]
     *                  'prices' => [
     *                      'sold-out' => [
     *                          'type'  => 'boolean',
     *                          'value' => true,
     *                      ],
     *                      'price'    => [
     *                          'type'  => 'short-text',
     *                          'value' => null,
     *                      ],
     *                  ],
     *                  'sizes' => [
     *                      'weight'   => [
     *                          'type'  => 'short_text',
     *                          'value' => '123',
     *                      ],
     *                      'height'   => [
     *                          'type'  => 'long_text',
     *                          'value' => '456',
     *                      ],
     *                  ]
     *              ],
     *          ],
     *      ]
     * ```
     *
     * @return array
     */
    public function getCompareArray(): array
    {
        if (!$this->productsAreComparable()) {
            $this->addError(static::ERROR_PRODUCTS_ARE_NOT_COMPARABLE);

            return [];
        }

        $columns    = $this->getSpecsColumns();
        $columnsArr = $this->getSpecsColumnsArray($columns);
        $headers    = $this->getCompareHeaders($columnsArr);
        $items      = $this->getCompareItems($columns);
        $results    = [
             'headers' => $headers,
             'items'   => $items,
        ];

        return $results;
    }
}
