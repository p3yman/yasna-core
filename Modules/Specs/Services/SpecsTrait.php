<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/9/18
 * Time: 1:48 PM
 */

namespace Modules\Specs\Services;


use App\Models\Spec;
use App\Models\SpecPivot;
use Illuminate\Database\Eloquent\Collection;

trait SpecsTrait
{
    /**
     * A Many-to-Many Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specs()
    {
        return $this->belongsToMany(Spec::class, 'specs_pivot', 'model_id')
                    ->whereModelName(static::className())
             ;
    }



    /**
     * A One-to-Many Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specsPivots()
    {
        return $this->hasMany(SpecPivot::class, 'model_id')
                    ->whereModelName(static::className())
             ;
    }



    /**
     * Accessor fot the `specsValues` Relationship
     *
     * @return Collection
     */
    public function getSpecsPivotsAttribute()
    {
        return $this->getRelationValue('specsPivots');
    }



    /**
     * Returns the relation record between this entity and the given spec id.
     *
     * @param int $spec_id
     *
     * @return SpecPivot
     */
    public function getSpecPivot($spec_id)
    {
        $value_record = $this->specsPivots()
                             ->whereSpecId($spec_id)
                             ->first()
        ;

        return ($value_record ?? model('spec-pivot'));
    }



    /**
     * Returns the value of the relation between this entity and the given spec id.
     *
     * @param int $spec_id
     *
     * @return string|null
     */
    public function getSpecValue($spec_id)
    {
        return $this->getSpecPivot($spec_id);
    }



    /**
     * Saves a relation between this model and a spec.
     *
     * @param int|string|Spec $spec
     * @param mixed           $value
     * @param bool            $hidden
     *
     * @return bool|false|\Illuminate\Database\Eloquent\Model
     */
    public function saveSpecPivot($spec, $value, $hidden = false)
    {
        if (!($spec instanceof Spec)) {
            $spec = model('spec', $spec);
        }

        if ($spec->not_exists) {
            return false;
        }

        $pivot = model('spec-pivot');

        $pivot->model_name = $this->className();
        $pivot->spec_id    = $spec->id;
        $pivot->value      = $value;
        $pivot->hid_at     = ($hidden ? now()->toDateTimeString() : null);

        return $this->specsPivots()->save($pivot);

    }
}
