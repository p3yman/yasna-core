<?php

namespace Modules\Specs\Entities;

use Illuminate\Database\Eloquent\Builder;
use Modules\Specs\Entities\Traits\SpecPivotResourcesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpecPivot extends YasnaModel
{
    use SoftDeletes;
    use SpecPivotResourcesTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'specs_pivot';



    /**
     * A One-to-Many Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function spec()
    {
        return $this->belongsTo(Spec::class);
    }



    /**
     * Returns the related entity object.
     *
     * @return YasnaModel
     */
    public function getEntity()
    {
        return model($this->model_name, $this->model_id);
    }



    /**
     * Accessor for the `getEntity()` Method
     *
     * @return YasnaModel
     */
    public function getEntityAttribute()
    {
        return $this->getEntity();
    }



    /**
     * Returns the related model object.
     *
     * @return YasnaModel
     */
    public function getRelatedModel()
    {
        return $this->getEntity();
    }



    /**
     * Accessor for the `getRelatedModel()` Method
     *
     * @return YasnaModel
     */
    public function getRelatedModelAttribute()
    {
        return $this->getRelatedModel();
    }



    /**
     * Checks if is hidden.
     *
     * @return bool
     */
    public function isHidden()
    {
        return boolval($this->hid_at);
    }



    /**
     * Checks if is not hidden.
     *
     * @return bool
     */
    public function isNotHidden()
    {
        return !$this->isHidden();
    }



    /**
     * Filters query result to visible items.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeVisible(Builder $builder)
    {
        return $builder->where(function ($query) {
            $query->whereNull('hid_at')
                  ->orWhere('hid_at', '>', now()->toDateTimeString())
            ;
        });
    }



    /**
     * Filters query result to visible items.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeNotHidden(Builder $builder)
    {
        return $this->scopeVisible($builder);
    }


    /**
     * Filters query result to hidden items.
     *
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeHidden(Builder $builder)
    {
        return $builder->whereNotNull('hid_at')
                       ->where('hid_at', '<=', now()->toDateTimeString())
             ;
    }
}
