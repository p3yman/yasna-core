<?php

namespace Modules\Specs\Entities;

use Modules\Specs\Entities\Traits\SpecDefaultValuesTrait;
use Modules\Specs\Entities\Traits\SpecInputTypeTrait;
use Modules\Specs\Entities\Traits\SpecSelfReferenceTrait;
use Modules\Specs\Entities\Traits\SpecsScopesTrait;
use Modules\Specs\Entities\Traits\SpecTitlesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Spec extends YasnaModel
{
    use SoftDeletes;
    use SpecsScopesTrait;
    use SpecTitlesTrait;
    use SpecInputTypeTrait;
    use SpecSelfReferenceTrait;
    use SpecDefaultValuesTrait;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
         'titles'         => 'array',
         'default_values' => 'array',
    ];



    /**
     * Checks if this spec is a group.
     *
     * @return bool
     */
    public function isGroup()
    {
        return ($this->parent_id == 0);
    }



    /**
     * Checks if this spec is not a group.
     *
     * @return bool
     */
    public function isNotGroup()
    {
        return !$this->isGroup();
    }



    /**
     * Accessor for the `isGroup()` Method.
     *
     * @return bool
     */
    public function getIsGroupAttribute()
    {
        return $this->isGroup();
    }



    /**
     * Accessor for the `isNotGroup()` Method.
     *
     * @return bool
     */
    public function getIsNotGroupAttribute()
    {
        return $this->isNotGroup();
    }



    /**
     * Returns an array of general meta fields.
     *
     * @return array
     */
    public function generalValueMetaFields()
    {
        return ['default_values'];
    }
}
