<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/4/18
 * Time: 12:44 PM
 */

namespace Modules\Specs\Entities\Traits;


use Illuminate\Database\Eloquent\Builder;

trait SpecsScopesTrait
{
    /**
     * Scope for Selecting Groups.
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeGroups(Builder $query): Builder
    {
        return $query->where('parent_id', 0);
    }
}
