<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/4/18
 * Time: 1:26 PM
 */

namespace Modules\Specs\Entities\Traits;


trait SpecTitlesTrait
{
    /**
     * Returns the title in the specified locale.
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function titleIn(string $locale)
    {
        return ($this->titles[$locale] ?? null);
    }



    /**
     * Returns the title in current locale.
     *
     * @return null|string
     */
    public function getTitle()
    {
        return $this->titleIn(getLocale());
    }



    /**
     * Accessor for the `getTitle()` Method.
     *
     * @return null|string
     */
    public function getTitleAttribute()
    {
        return $this->getTitle();
    }



    /**
     * Returns the first title stored.
     *
     * @return string|null
     */
    public function getFirstTitle()
    {
        $titles        = ($this->titles ?? []);
        $titles_values = array_values($titles);

        return ($titles_values[0] ?? null);
    }



    /**
     * Accessor fot the `getFirstTitle()` Method
     *
     * @return null|string
     */
    public function gitFirstTitleAttribute()
    {
        return $this->getFirstTitle();
    }
}
