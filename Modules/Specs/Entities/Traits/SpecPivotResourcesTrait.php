<?php

namespace Modules\Specs\Entities\Traits;

use App\Models\Spec;

/**
 * @property Spec $spec
 */
trait SpecPivotResourcesTrait
{
    /**
     * get Title resource.
     *
     * @return string
     */
    protected function getTitleResource()
    {
        $spec = $this->spec;

        return $spec ? $this->spec->title : null;
    }



    /**
     * get InputType resource.
     *
     * @return string
     */
    protected function getInputTypeResource()
    {
        $spec = $this->spec;

        return $spec ? $this->spec->input_type : null;
    }



    /**
     * get Value resource.
     *
     * @return string
     */
    protected function getValueResource()
    {
        $spec = $this->spec;

        if (!$spec) {
            return null;
        }

        if ($spec->input_type == "boolean") {
            return boolval($this->value);
        }

        return $this->value;
    }
}
