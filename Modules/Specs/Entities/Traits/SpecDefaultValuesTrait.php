<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/8/18
 * Time: 5:00 PM
 */

namespace Modules\Specs\Entities\Traits;


trait SpecDefaultValuesTrait
{
    /**
     * Sets the proper array as the default values in the attributes array.
     */
    protected function setDefaultValuesInAttributesArray()
    {
        $current = ($this->attributes['default_values'] ?? null);
        if ($current) {
            $value = $current;
        } else {
            $value = $this->getMeta('default_values');
        }

        $this->attributes['default_values'] = is_array($value) ? $value : json_decode($value,
             true);
    }



    /**
     * Returns the default values array.
     *
     * @return array|null
     */
    public function defaultValues()
    {
        $current = $this->attributes['default_values'] ?? null;
        if (!$current) {
            $this->setDefaultValuesInAttributesArray();
        }

        return $this->attributes['default_values'];
    }



    /**
     * Accessor for the `defaultValues()` Method
     *
     * @return array|null
     */
    public function getDefaultValuesAttribute()
    {
        return $this->defaultValues();
    }



    /**
     * Returns the default value in the specified locale.
     *
     * @param string $locale
     *
     * @return string|null
     */
    public function defaultValueIn(string $locale)
    {
        return ($this->default_values[$locale] ?? null);
    }



    /**
     * Returns the default value in current locale.
     *
     * @return string|null
     */
    public function getDefaultValueInCurrentLocale()
    {
        return $this->defaultValueIn(getLocale());
    }



    /**
     * Accessor for the `getDefaultValueInCurrentLocale()` Method
     *
     * @return null|string
     */
    public function getDefaultValueAttribute()
    {
        return $this->getDefaultValueInCurrentLocale();
    }
}
