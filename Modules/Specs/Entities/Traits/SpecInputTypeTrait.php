<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/8/18
 * Time: 12:48 PM
 */

namespace Modules\Specs\Entities\Traits;


use Modules\Manage\Services\Widget;
use Modules\Manage\Services\Widgets\Combo;
use Modules\Manage\Services\Widgets\Input;
use Modules\Manage\Services\Widgets\Textarea;
use Modules\Manage\Services\Widgets\Toggle;

trait SpecInputTypeTrait
{
    /**
     * Returns an array of available input types.
     *
     * @return array
     */
    public function availableInputTypes()
    {
        return ['text', 'paragraph', 'boolean', 'select'];
    }



    /**
     * Returns an array of available input types and their titles
     * which is ready to be used in a combo widget.
     *
     * @return array
     */
    public function availableInputTypesCombo()
    {
        $types  = $this->availableInputTypes();
        $result = [];

        foreach ($types as $type) {
            $result[] = [
                 'value' => $type,
                 'title' => $this->module()->getTrans("input-types.$type"),
            ];
        }

        return $result;
    }



    /**
     * Returns an array of meta fields depending on input type field.
     *
     * @return array
     */
    public function inputTypeMetaFields()
    {
        return ['options'];
    }



    /**
     * Accessor for the `options` property
     *
     * @param string|null $original
     *
     * @return string|null
     */
    public function getOptionsAttribute($original)
    {
        return ($original ?? $this->getMeta('options'));
    }



    /**
     * Returns an array of options.
     *
     * @return array|array[]|false|string[]
     */
    public function getOptionsArray()
    {
        $options = $this->options;
        if (empty($options)) {
            return [];
        }

        return preg_split('/\r\n|\r|\n/', $options);
    }



    /**
     * Accessor for the `getOptionsArray()` Method.
     *
     * @return array|array[]|false|string[]
     */
    public function getOptionsArrayAttribute()
    {
        return $this->getOptionsArray();
    }



    /**
     * Get an array of options to be used in a Combo widget.
     *
     * @param string $value_field
     * @param string $caption_field
     *
     * @return array
     */
    protected function getOptionsForCombo(string $value_field = 'id', string $caption_field = "title")
    {
        $options_array = $this->getOptionsArray();

        return array_map(function ($option) use ($value_field, $caption_field) {
            return [
                 $value_field   => $option,
                 $caption_field => $option,
            ];
        }, $options_array);
    }



    /**
     * Returns an Input widget object
     * to be used when the `input_typ` has been set as `text`.
     *
     * @return Input
     */
    protected function getTextWidget()
    {
        return widget('input');
    }



    /**
     * Returns a Textarea widget object
     * to be used when the `input_typ` has been set as `paragraph`.
     *
     * @return Textarea
     */
    protected function getParagraphWidget()
    {
        return widget('textarea');
    }



    /**
     * Returns a Toggle widget object
     * to be used when the `input_typ` has been set as `boolean`.
     *
     * @return Toggle
     */
    protected function getBooleanWidget()
    {
        return widget('toggle');
    }



    /**
     * Returns a Combo widget object with the proper options
     * to be used when the `input_typ` has been set as `select`.
     *
     * @return Combo
     */
    protected function getSelectWidget()
    {
        $options = $this->getOptionsForCombo();

        if (empty($options)) {
            $options = [['id' => '', 'title' => '']];
        }

        return widget('combo')
             ->options($options);
    }



    /**
     * Returns an Input widget object
     * to be used when the value of the `input_typ` fields has not been set as a known value.
     *
     * @return Input
     */
    protected function getDefaultWidget()
    {
        return widget('input');
    }



    /**
     * Returns an object of the `Modules\Manage\Services\Widget` class
     * which is proper for this spec.
     *
     * @return Widget
     */
    public function getWidget()
    {
        $input_type      = $this->input_type;
        $specific_method = camel_case("get-$input_type-widget");

        if (method_exists($this, $specific_method)) {
            return $this->$specific_method();
        }

        return $this->getDefaultWidget();
    }
}
