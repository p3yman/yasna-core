<?php

namespace Modules\Specs\Entities\Traits;

use App\Models\SpecPivot;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property Collection $specs_pivots
 */
trait PostSpecsResourcesTrait
{

    /**
     * get specs resource
     *
     * @return array
     */
    public function getSpecsResource()
    {
        if ($this->hasNotSpecs()) {
            return [];
        }

        $mapped = $this->specs_pivots->map(function (SpecPivot $pivot) {
            return $pivot->toResource();
        });

        return $mapped->toArray();
    }



    /**
     * return post specs by group name
     * TODO: To be refactored to the new resource system. Also used in:
     * Modules\Specs\Http\Controllers\V1\PostSpecsController
     *
     * @return array
     */
    public function toListSpecResource() //TODO!
    {
        $groups_items = $this->getSpecsPivotParents();

        return $groups_items->map(function ($items, $parent_id) {
            $parent_spec = model('spec', $parent_id);
            return [
                 'title' => $parent_spec->title,
                 'rows'  => $this->mapSpecs($items),
            ];
        })->toArray()
             ;
    }



    /**
     * Returns a collection of specs pivot grouped by their parent IDs.
     *
     * @return Collection
     */
    public function getSpecsPivotParents()
    {
        return $this
             ->specsPivots()
             ->whereNull('hid_at')
             ->get()
             ->groupBy(function ($item) {
                 return $item->spec->parent_id;
             })
             ;
    }



    /**
     * map post spec
     *
     * @param spec $items
     *
     * @return array
     */
    public function mapSpecs($items)
    {
        return $items->map(function ($item) {
            if ($item->spec->input_type == 'boolean') {
                $value = $item->value ? true : false;
            } else {
                $value = ad($item->value);
            }
            return [
                 'title' => $item->spec->title,
                 'cols'  => [
                      $value,
                 ],
            ];
        })->toArray()
             ;
    }
}
