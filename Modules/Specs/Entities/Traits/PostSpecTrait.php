<?php

namespace Modules\Specs\Entities\Traits;

use Modules\Specs\Services\SpecsTrait;

trait PostSpecTrait
{
    use SpecsTrait;



    /**
     * Checks if has spec feature.
     *
     * @return bool
     */
    public function hasSpecs()
    {
        return $this->has('specs');
    }



    /**
     * Checks if has not spec feature.
     *
     * @return bool
     */
    public function hasNotSpecs()
    {
        return !$this->hasSpecs();
    }



    /**
     * Returns an array to add meta fields depending on specs.
     *
     * @return array
     */
    public function specMetaFields()
    {
        if ($this->hasSpecs()) {
            return ['spec_group'];
        } else {
            return [];
        }
    }
}
