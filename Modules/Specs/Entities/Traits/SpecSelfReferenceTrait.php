<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 9/8/18
 * Time: 4:26 PM
 */

namespace Modules\Specs\Entities\Traits;


use App\Models\Spec;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait SpecSelfReferenceTrait
{
    /**
     * Returns a query builder to select children.
     *
     * @return Builder
     */
    public function children()
    {
        return static::instance()
             ->whereParentId($this->id ?: null);
    }



    /**
     * Returns a collection of the children.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getChildrenAttribute()
    {
        return $this->children()->get();
    }



    /**
     * Standard One-to-Many Relationship.
     *
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Spec::class, 'parent_id');
    }
}
