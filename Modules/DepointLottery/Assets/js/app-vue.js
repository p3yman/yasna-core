/*-----------------------------------------------------------------
- Number item component
-----------------------------------------------------------------*/
var NumberItem = Vue.component('number-item', {
    name: 'number-item',
    props: ['number', 'index'],
    data: function () {
        return {
            selected: false
        }
    },
    template: `
        <a href="#" class="number-item" data-aos="flip-left" :data-aos-delay="index*100"
           @click.prevent="select"
           :class="[ selected ? 'selected aos-init aos-animate' : 'aos-init aos-animate' ]"
           :data-gift-left="number.second"
           :data-gift-right="number.third"
           :data-gift-middle="number.first"><span>{{ index+1 }}</span></a>
    `,
    methods: {

        select: function (event) {

            if( this.selected ){
                alert('go next!');
            } else {
                this.selected = true;
            }

        }

    }

});


var app = new Vue({

    el: '#app',

    data: {
        state: 'home',

        classes: {
            empty: '',
            aos: 'aos-init aos-animate',
            selected: 'selected aos-init aos-animate',
        },

        selected: false,
        selected_classes: '',
        selected_gift: false,
        selected_gift_classes: '',
        showing: false,

        gifts: {
            first: null,
            second: null,
            third: null,
        },

        items: [
            {id: 1, width: 173, icon_width: 120},
            {id: 2, width: 175, icon_width: 119},
            {id: 3, width: 168, icon_width: 109},
            {id: 4, width: 175, icon_width: 70}
        ],

        level_1: [
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
            {first: '200,000', second: '150,000', third: '0'},
        ]

    },

    components: {
        NumberItem: NumberItem
    },

    created: function () {
        AOS.init({
            once: true,
        });
    },

    methods: {

        getClass: function(){

            return 'selected';

        },

        showLevel: function(level){

            this.state = 'level-' + level;

        },

        selectNumber: function (number) {

            if( this.selected && this.selected == number ){

                this.selected = false;
                this.state = 'gifts';

                return true;

            }

            this.selected = number;

            this.gifts.first = number.first;
            this.gifts.second = number.second;
            this.gifts.third = number.third;

        },

        selectGift: function (gift) {

            if( this.selected_gift && this.selected_gift == gift ){

                this.showing = true;

                return true;

            }

            this.selected_gift = gift;

        }

    },

});