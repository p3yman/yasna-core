jQuery(document).ready(function ($) {

    // AOS
    AOS.init({
        once: true,
    });


    // Select home card
    $('#home-cards .card-item a').on('click', function (e) {
        e.preventDefault();

        var cat = $(this).attr('data-cat');

        $('.white-overlay').addClass('active');

        axios.get(url('depoint/lottery/data/' + cat))
            .then(function (response) {

                if (response.data[0] != undefined)
                    $("#level-intro p").html(response.data[0].cat_describe);
                else
                    $("#level-intro p").html("");

                $('a#back-to-home').show();

                // Inject data on #level-numbers #level-numbers

                var data = response.data;
                var count = 0;
                var row_begin = '<div class="flex-single-row" data-aos="flip-left" data-aos-delay="';
                var row_end = '</div>';
                var chunk = 7;
                var row;
                var output = '';

                if( data.length > 42 )
                    chunk = 10;
                if( data.length > 70 )
                    chunk = 12;

                console.log(chunk);
                console.log(data.length);

                while(data.length) {

                    row = data.splice( 0, chunk );

                    output += row_begin + count*200 + '">';

                    for( i = 0; i < row.length; i++ ){


                        output += '<a href="#" id="'+row[i].post_hashid+'" class="number-item '+row[i].use_class+'" data-gift-left="'+row[i].third_gift+'" data-gift-right="'+row[i].second_gift+'" data-gift-middle="'+row[i].main_gift+'"><span>' + row[i].label + '</span></a>';

                    }

                    count++;

                    output += row_end;

                }

                $('#level-numbers .flex-column').html(output);


                $('#home-cards').fadeOut();

                $('#level-intro [data-aos]').hide(0).removeClass('aos-init').removeClass('aos-animate');

                $('#level-intro').delay(800).fadeIn(300, function () {
                    $('#level-intro [data-aos]').show(0);
                    AOS.init();
                });

                setTimeout(function () {
                    $('.white-overlay').removeClass('active');
                }, 1000);

            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {

            });

    });

    // Select gift number
    $(document).on('click', '.levels .number-item', function (e) {

        e.preventDefault();

        if (!$(this).hasClass('selected')) {

            $('.number-item').removeClass('selected');
            $(this).addClass('selected');

        } else {

            $('a#back-to-numbers').show();

            var level = $(this).parents().eq(2);
            var gifts = $('#gifts');
            var id    = $(this).attr('id');

            $(level).fadeOut();
            $(gifts).attr('data-id', id);
            $(gifts).find('.card-item').hide(0).removeClass('aos-init').removeClass('aos-animate');
            $(gifts).find('.card-item .gift').hide(0).removeClass('aos-init').removeClass('aos-animate');
            $(gifts).find('.card-item').hide(0).removeClass('aos-init').removeClass('aos-animate');

            $(gifts).find('.card-item .gift.middle').html($(this).data('gift-middle')).addClass( giftSize( $(this).data('gift-middle') ) );

            var c = $(this).data('gift-left') != '' ? '' : 'empty';
            var gift = c == 'empty' ? 'پوچ' : $(this).data('gift-left');
            c = c + ' ' + giftSize( $(this).data('gift-left') );
            $(gifts).find('.card-item .gift.left span.hide').html(gift).addClass(c);

            c = $(this).data('gift-right') != '' ? '' : 'empty';
            gift = c == 'empty' ? 'پوچ' : $(this).data('gift-right');
            c = c + ' ' + giftSize( $(this).data('gift-right') );
            $(gifts).find('.card-item .gift.right span.hide').html(gift).addClass(c);

            $(gifts).delay(500).fadeIn(300, function () {
                $('.card-item').show(0);
                $('.card-item .gift').show(0);
                AOS.init();
            });

            $('a.toggle-show')
                .attr('data-in', '#level-numbers')
                .attr('data-out', '#gifts')
                .slideDown();

            $(this).removeClass('selected');

        }


    });

    // Select gift
    $('#gifts .card-item a').on('click', function (e) {

        e.preventDefault();

        var gifts = $('#gifts');
        var item  = $(this).parents().eq(1);

        if( $('#gifts').hasClass('used') ){

            $(item).addClass('showing');

            if( $(item).find('span.hide').hasClass('empty') ){

                setTimeout(function () {
                    var audio = new Audio(url('modules/depointlottery/audio/loser.mp3'));
                    audio.play();

                }, 4000);

            }

        } else if (!$(item).hasClass('selected')) {

            $(gifts).find('.card-item').removeClass('selected');
            $(item).addClass('selected');

        } else {

            $('a.toggle-show').addClass('disabled');
            $('#gifts').addClass('used');

            $(this).fadeOut();

            $(item).addClass('showing');

            $(item).removeClass('selected');

            if( $(item).find('span.hide').hasClass('empty') ){

                setTimeout(function () {
                    var audio = new Audio(url('modules/depointlottery/audio/loser.mp3'));
                    audio.play();

                }, 4000);

            }

            var selected = $(this).attr('data-field');
            var id       = $('#gifts').attr('data-id');

            // if won == 0 -> class=used lost : class=used
            var c = $(item).find('span.hide').hasClass('empty') ? 'used lost' : 'used';
            $('#' + id).addClass(c);

            // Send Ajax request to save used
            axios.post(url('depoint/lottery/choose'), {
                id   : id,
                field: selected
            })
                .then(function (response) {

                    $('a.toggle-show').removeClass('disabled');

                })
                .catch(function (error) {
                    console.log(error);
                })
                .then(function () {
                    $('.white-overlay').removeClass('active');
                });

        }

    });

    // Back to home
    $('a#back-to-home').on('click', function (e) {

        e.preventDefault();

        $('#home-cards').slideDown();
        $('#level-numbers').slideUp();

        $('#back-to-home').hide();

    });
    $('a#back-to-numbers').on('click', function (e) {

        e.preventDefault();

        $('#level-numbers').slideDown();
        $('#gifts').slideUp();

        $('#gifts .card-item').removeClass('selected showing disable');
        $('#gifts .card-item span.hide').removeClass('empty');
        $('#gifts').removeClass('used');
        $('#gifts a').delay(500).fadeIn();

        $('#back-to-home').show();
        $('#back-to-numbers').hide();

    });
    $('a#go-to-numbers').on('click', function (e) {

        e.preventDefault();

        $('#level-numbers').slideDown();
        $('#level-intro').slideUp();

        $('#level-numbers [data-aos]').hide(0).removeClass('aos-init').removeClass('aos-animate');
        $('#level-numbers').delay(800).fadeIn(300, function () {
            $('#level-numbers [data-aos]').show(0);
            AOS.init();
        });

    });

    function giftSize(gift) {

        var count = gift.length;

        if( count < 10 ){
            return 'size-lg';
        } else if ( count < 15 ){
            return 'size-md'
        } else if ( count < 20 ){
            return 'size-sm';
        } else {
            return 'size-xs';
        }

    }

    // Generate number
    if( $('#random-number').length ){

        var numbers = prepare();

        $('#generate-number').on('click', function (e) {
            e.preventDefault();

            $(this).attr('disabled', 'true');

            $('#random-number').removeClass('selected');

            var item = parseInt(numbers.splice(Math.floor(Math.random() * numbers.length), 1));

            var counter = 0;
            var random = 0;
            var i = setInterval(function () {

                random = getRandomArbitrary(0, 999);

                $('#random-number').text(format_number(String(parseInt(random))));

                counter++;
                if (counter === 50) {
                    clearInterval(i);
                    $('#random-number').text(format_number(String(parseInt(item)))).addClass('selected');
                    $('#generate-number').attr('disabled', 'false')
                }
            }, 50);

            if( !numbers.length ){
                $('#generate-number').slideUp();
            }

            Cookies.set('depoint_numbers', numbers);

        })

    }

    $('#reset-random').on('click', function (e) {
        e.preventDefault();
        Cookies.remove('depoint_numbers');
        location.reload();
    })

    function prepare() {

        var array=[1,2,3,4,5,6,8,9,11,12,13,14,15,16,17,18,19,20,21,22,24,25,26,27,28,29,30,31,33,35,36,38,39,40,41,42,43,44,45,46,47,48,49,50,51,53,54,55,56,57,58,60,61,62,63,64,65,66,67,68,69,72,73,74,75,76,77,79,80,81,82,83,84,85,86,87,88,90,91,97,98,99,100,101,102,104,107,108,109,110,111,112,113,114,116,118,122,123,124,126,127,128,134,136,137,138,139,140,141,145,146,148,149,150,152,153,154,159,161,165,167,168,169,170,172,174,175,176,177,178,179,180,181,182,183,185,186,187,189,194,195,197,198,199,200,202,203,207,211,215,216,217,218,223,224,225,226,227,228,230,231,233,239,240,241,247,248,249,250,251,252,253,256,260,262,263,265,266,267,268,269,270,271,273,272,278,280,282,283,284,285,286,291,295,296,297,298,299,300,301,302,303,305,308,309,310,311,315,316,317,318,319,321,324,326,328,329,330,331,333,334,340,342,343,346,347,348,349,352,353,356,358,360,361];

        var value = Cookies.get('depoint_numbers');

        if( !value ){
            Cookies.set('depoint_numbers', array);
            value = array;
        } else {
            value = JSON.parse("[" + value + "]")[0];
        }

        console.log( value );

        return value;
    }

    function format_number(text){
        return text.replace(/(d)(?=(ddd)+(?!d))/g, "$1,").toFa();
    };

    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }

    String.prototype.toFa= function() {
        return this.replace(/\d/g, d => '۰۱۲۳۴۵۶۷۸۹'[d]);
    }
});

window.onbeforeunload = function() {
    return "با رفرش صفحه قرعه‌کشی به حالت اولیه برخواهد گشت. مطمئنید؟";
}