<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 10/2/18
 * Time: 3:36 PM
 */

namespace Modules\DepointLottery\Database\Seeders;


use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Modules\Yasna\Providers\DummyServiceProvider;

class DummyTableSeeder extends Seeder
{

    /**
     * make dummy data
     */
    public function run()
    {
        $this->seedCat(31,'d5i');
        $this->seedCat(61,'t7');
        $this->seedCat(81,'d4');
        $this->seedCat(81,'c5');
    }



    /**
     * seed category A
     *
     * @param int $count
     * @param string $cat_slug
     */
    private function seedCat($count,$cat_slug)
    {
        $model = post();
        $model->type = 'depoint-lottery';
        for ($i = 1; $i < $count; $i++) {
            $is_saved=$model->batchSave([
                 'type'         => "depoint-lottery",
                 'title'        => DummyServiceProvider::persianWord(3),
                 'title2'       => $i,
                 'second-gift'  => rand(0, 3) ? "" : DummyServiceProvider::persianWord(3),
                 'third-gift'   => rand(0, 3) ? "" : DummyServiceProvider::persianWord(3),
                 'published_at' => Carbon::now()->toDateTimeString(),
            ]);

            $is_saved->attachCategories(posttype('depoint-lottery')->categories()->where('slug',$cat_slug)->first()->id);
        }
    }
}