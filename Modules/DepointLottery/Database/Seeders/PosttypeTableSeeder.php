<?php

namespace Modules\DepointLottery\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PosttypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedInputs();
        $this->seedPosttype();
        $this->attachInputs();
        $this->seedCategories();
    }



    /**
     * seed posttype inputs
     */
    private function seedInputs()
    {
        $data = [
             [
                  'slug'              => "second-gift",
                  'title'             => trans("depointlottery::general.second-gift"),
                  'type'              => "editor",
                  'order'             => "11",
                  'data_type'         => "text",
                  'validation_rules'  => "",
                  'purifier_rules'    => "",
                  'default_value'     => "",
                  'default_value_set' => "",
                  'hint'              => "",
             ],
             [
                  'slug'              => "third-gift",
                  'title'             => trans("depointlottery::general.third-gift"),
                  'type'              => "editor",
                  'order'             => "11",
                  'data_type'         => "text",
                  'validation_rules'  => "",
                  'purifier_rules'    => "",
                  'default_value'     => "",
                  'default_value_set' => "",
                  'hint'              => "",
             ],
             [
                  'slug'              => "selected",
                  'title'             => trans("depointlottery::general.selected"),
                  'type'              => "editor",
                  'order'             => "11",
                  'data_type'         => "text",
                  'validation_rules'  => "",
                  'purifier_rules'    => "",
                  'default_value'     => "",
                  'default_value_set' => "",
                  'hint'              => "",
             ],
        ];
        yasna()->seed('inputs', $data);
    }



    /**
     * seed posttype
     */
    private function seedPosttype()
    {
        $slug     = 'depoint-lottery';
        $data[]   = [
             'slug'           => $slug,
             'title'          => trans("depointlottery::general.posttype"),
             'order'          => '10',
             'singular_title' => trans("depointlottery::general.posttype"),
             'template'       => 'album',
             'icon'           => 'shopping-bag',
        ];
        $features = [
             "category",
        ];
        yasna()->seed('posttypes', $data);
        $posttype = posttype($slug);
        $posttype->attachFeatures($features);
        $posttype->attachRequiredInputs();
        $posttype->cacheFeatures();
    }



    /**
     * attach inputs to posttype
     */
    private function attachInputs()
    {
        $ids   = [];
        $ids[] = model('input')->grabSlug('second-gift')->id;
        $ids[] = model('input')->grabSlug('third-gift')->id;
        $ids[] = model('input')->grabSlug('selected')->id;
        posttype('depoint-lottery')->inputs()->syncWithoutDetaching($ids);
    }



    /**
     * seed posttype categories
     */
    private function seedCategories()
    {
        $this->seedProductsCategories();
    }



    /**
     * Seed Categories
     */
    private function seedProductsCategories()
    {
        $posttype = posttype()->grabSlug('depoint-lottery');
        $data     = $this->getProductCategoriesData();

        $general_data = array_fill(0, count($data), [
             'posttype_id' => $posttype->id,
             'is_folder'   => 0,
             'parent_id'   => 0,
             'locales'     => '|fa|en|',
        ]);
        $modifiedData = array_replace_recursive($general_data, $data);

        yasna()->seed('categories', $modifiedData);
    }



    /**
     * @return array
     */
    protected function getProductCategoriesData()
    {
        return [

             [
                  'slug'        => 'd5i',
                  'meta-titles' => [
                       'fa' => 'D5i',
                       'en' => 'D5i',
                  ],
             ],
             [
                  'slug'        => 't7',
                  'meta-titles' => [
                       'fa' => 'T7',
                       'en' => 'T7',
                  ],
             ],
             [
                  'slug'        => 'd4',
                  'meta-titles' => [
                       'fa' => 'D4',
                       'en' => 'D4',
                  ],
             ],
             [
                  'slug'        => 'c5',
                  'meta-titles' => [
                       'fa' => 'C5',
                       'en' => 'C5',
                  ],
             ],
        ];
    }
}
