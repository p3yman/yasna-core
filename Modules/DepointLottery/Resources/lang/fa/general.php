<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 9/30/18
 * Time: 1:20 PM
 */


return [
     'sidebar'             => 'قرعه‌کشی',
     "lottery-number"      => "شماره قرعه‌کشی",
     "main-gift"           => "جایزه اصلی",
     "second-gift"         => "جایزه دوم",
     "third-gift"          => "جایزه سوم",
     "category"            => "دسته‌بندی",
     'grid-title'          => "لیست آیتم‌های قرعه‌کشی",
     "add"                 => "افزودن",
     "add-modal"           => "اضافه کردن آیتم جدید به قرعه‌کشی",
     'save'                => "ذخیره",
     "cancel"              => "انصراف",
     "shit"                => "پوچ",
     "miss-lottery-number" => "وارد کردن «شماره قرعه‌کشی» الزامی است.",
     "miss-main-gift"      => "وارد کردن «جایزه اصلی» الزامی است.",
     "type-lottery-number" => "مقدار «شماره قرعه‌کشی» باید به صورت عددی وارد شود",
     "exist-number"        => "این شماره قرعه‌کشی قبلا استفاده شده است",
     "delete"              => "حذف آیتم قرعه‌کشی",
     'used'                => 'استفاده‌شده',
     'unused'              => 'استفاده‌نشده',
     "state"               => 'وضعیت',
     'reset'               => 'بازگشت به تنظیمات اصلی',
     'choose'              => 'انتخاب کنید',
     'select'              => 'انتخاب',
     'return'              => 'بازگشت',
     'home'                => 'خانه',
     'posttype'            => 'قرعه‌کشی',
     'selected'            => 'انتخاب‌شده',
     "cat-a"               => "D5i",
     "cat-b"               => "T7",
     "cat-c"               => "D4",
     'cat-d'               => "C5",
     'empty'               => 'پوچ',
];