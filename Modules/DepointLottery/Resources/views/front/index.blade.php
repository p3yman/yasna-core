<!DOCTYPE html>
<html lang="fa">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
	<title>{{trans(('depointlottery::general.home'))}}</title>
	<link rel="stylesheet" href="{{Module::asset("depointlottery:css/front-style.min.css")}}">
	<link rel="stylesheet" href="{{Module::asset("depointlottery:css/addon.css")}}">
</head>


<body class="">
<canvas id="canv"></canvas>
<div id="logo">
	<img src="{{Module::asset("depointlottery:images/logo.png")}}">
</div>
<a href="#" class="button inverted toggle-show lg" id="back-to-home" data-out="#home-cards" data-in="#level-numbers"
   style="display: none;">{{trans(('depointlottery::general.return'))}}</a>
<a href="#" class="button inverted toggle-show lg" id="back-to-numbers" data-out="#home-cards" data-in="#level-numbers"
   style="display: none;">{{trans(('depointlottery::general.return'))}}</a>
<!-- Start: Home cards -->
<div class="h100vh" id="home-cards">
	<div class="flex-row">
		<div class="card-item" data-aos="flip-left" data-aos-delay="0">
			<article>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/card-image-bg-1.png")}}" class="card-bg"
						 width="173" data-aos="flip-up" data-aos-delay="200"></div>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/star-1.png")}}" class="star" width="120"
						 data-aos="zoom-in" data-aos-delay="350" data-aos-duration="1000"></div>
				<span data-aos="fade" data-aos-delay="200">
                            <div class="title">{{$cats[0]->title}}</div>
                        </span>
			</article>
			<div data-aos="flip-up" data-aos-delay="200">
				<a href="#level-1" data-cat="{{$cats[0]->slug}}">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
		<div class="card-item" data-aos="flip-left" data-aos-delay="300">
			<article>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/card-image-bg-2.png")}}" class="card-bg"
						 width="175" data-aos="flip-up" data-aos-delay="500"></div>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/star-2.png")}}" class="star" width="119"
						 data-aos="zoom-in" data-aos-delay="650" data-aos-duration="1000"></div>
				<span data-aos="fade" data-aos-delay="200">
                            <div class="title">{{$cats[1]->title}}</div>
                        </span>
			</article>
			<div data-aos="flip-up" data-aos-delay="500">
				<a href="#level-2" data-cat="{{$cats[1]->slug}}">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
		<div class="card-item" data-aos="flip-left" data-aos-delay="600">
			<article>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/card-image-bg-3.png")}}" class="card-bg"
						 width="168" data-aos="flip-up" data-aos-delay="800"></div>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/star-3.png")}}" class="star" width="109"
						 data-aos="zoom-in" data-aos-delay="950" data-aos-duration="1000"></div>
				<span data-aos="fade" data-aos-delay="200">
                            <div class="title">{{$cats[2]->title}}</div>
                        </span>
			</article>

			<div data-aos="flip-up" data-aos-delay="800">
				<a href="#level-3" data-cat="{{$cats[2]->slug}}">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
		<div class="card-item" data-aos="flip-left" data-aos-delay="900">
			<article>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/card-image-bg-4.png")}}" class="card-bg"
						 width="175" data-aos="flip-up" data-aos-delay="1100"></div>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/star-4.png")}}" class="star" width="70"
						 data-aos="zoom-in" data-aos-delay="1250" data-aos-duration="1000"></div>
				<span data-aos="fade" data-aos-delay="200">
                            <div class="title">{{$cats[3]->title}}</div>
                        </span>
			</article>
			<div data-aos="flip-up" data-aos-delay="1100">
				<a href="#level-4" data-cat="{{$cats[3]->slug}}">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- Start: End -->
<div class="levels" id="level-intro">
	<div class="flex-column">
		<div class="card-intro" data-aos="flip-left" data-aos-delay="0">
			<article>
				<p data-aos="zoom-in" data-aos-delay="300" class="ta-c"></p>
			</article>
			<div data-aos="flip-up" data-aos-delay="800">
				<a href="#" id="go-to-numbers" data-cat="">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- Start: LEVELS -->
<div class="levels" id="level-numbers">
	<div class="flex-column">
		<div class="flex-single-row" data-aos="flip-left" data-aos-delay="0">
			<a href="#" id="1" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" id="2" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" id="3" class="number-item used" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" id="4" class="number-item used lost" data-gift-left="۱" data-gift-right="۲"
			   data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" id="5" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" id="6" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" id="7" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
		</div>
		<div class="flex-single-row" data-aos="flip-left" data-aos-delay="200">
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
		</div>
		<div class="flex-single-row" data-aos="flip-left" data-aos-delay="400">
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
		</div>
		<div class="flex-single-row" data-aos="flip-left" data-aos-delay="600">
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
		</div>
		<div class="flex-single-row" data-aos="flip-left" data-aos-delay="800">
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
			<a href="#" class="number-item" data-gift-left="۱" data-gift-right="۲" data-gift-middle="۳">
				<span>۱</span>
			</a>
		</div>
	</div>
</div>
<!-- End: LEVELS -->
<!-- Start: Gifts -->
<div id="gifts" class="h100vh" data-id="0" data-empty="">
	<div class="flex-row">
		<div class="card-item" data-aos="zoom-in" data-aos-delay="0">
			<article>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/card-image-bg-1.png")}}" class="card-bg"
						 width="173" data-aos="flip-up" data-aos-delay="200"></div>
				<div class="img">
					<div class="gift right q" data-aos="flip-up" data-aos-delay="200">
						<span class="show">?</span>
						<span class="hide"></span>
					</div>
				</div>
			</article>
			<div data-aos="flip-up" data-aos-delay="200">
				<a href="#" data-field="second">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
		<div class="card-item main" data-aos="zoom-in" data-aos-delay="2000">
			<article>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/card-image-bg-2.png")}}" class="card-bg"
						 width="175" data-aos="flip-up" data-aos-delay="200"></div>
				<div class="img">
					<div class="gift middle" data-aos="flip-up" data-aos-delay="3000"></div>
				</div>
			</article>
			<div data-aos="flip-up" data-aos-delay="500">
				<a href="#" data-field="first">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
		<div class="card-item" data-aos="zoom-in" data-aos-delay="0">
			<article>
				<div class="img">
					<img src="{{Module::asset("depointlottery:images/card-image-bg-3.png")}}" class="card-bg"
						 width="168" data-aos="flip-up" data-aos-delay="200"></div>
				<div class="img">
					<div class="gift left q" data-aos="flip-up" data-aos-delay="200">
						<span class="show">?</span>
						<span class="hide"></span>
					</div>
				</div>
			</article>
			<div data-aos="flip-up" data-aos-delay="200">
				<a href="#" data-field="third">{{trans(('depointlottery::general.select'))}}
					<i class="icon-arrow-left"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- End: Gifts -->

<div class="white-overlay"></div>
<script>
    function url($additive) {
        if (!$additive) $additive = '';
        return '{{ url('-additive-') }}'.replace('-additive-', $additive);
    }

    function emptyItemTrans() {
        return "{{trans(('depointlottery::general.empty'))}}";
    }
</script>
<script src="{{Module::asset("depointlottery:js/vendor/jquery.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/vendor/aos.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/vendor/axios.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/snow.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/app.js")}}"></script>
</body>
</html>