<!DOCTYPE html>
<html lang="fa">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
	<title>{{trans(('depointlottery::general.home'))}}</title>
	<link rel="stylesheet" href="{{Module::asset("depointlottery:css/front-style.min.css")}}">
	<link rel="stylesheet" href="{{Module::asset("depointlottery:css/addon.css")}}">
</head>


<body class="">
<canvas id="canv"></canvas>
<div id="logo">
	<img src="{{Module::asset("depointlottery:images/logo.png")}}">
</div>
<a href="#" class="button inverted toggle-show lg" id="reset-random">بازنشانی</a>

<!-- Start: Random cards -->
<div class="h100vh" id="random-cards">
	<div class="flex-row">

        <div class="card-item" data-aos="flip-left" data-aos-delay="0">
            <article class="pos-r">
                <h1 id="random-number">-</h1>
            </article>
            <div data-aos="flip-up" data-aos-delay="200">
                <a href="#level-1" id="generate-number">{{trans(('depointlottery::general.select'))}}
                    <i class="icon-arrow-left"></i>
                </a>
            </div>
        </div>

    </div>
</div>
<!-- End: Random cards -->

<div class="white-overlay"></div>
<script>
    function url($additive) {
        if (!$additive) $additive = '';
        return '{{ url('-additive-') }}'.replace('-additive-', $additive);
    }

    function emptyItemTrans() {
		return  "{{trans(('depointlottery::general.empty'))}}";
    }
</script>
<script src="{{Module::asset("depointlottery:js/vendor/jquery.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/vendor/js.cookie.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/vendor/aos.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/snow.js")}}"></script>
<script src="{{Module::asset("depointlottery:js/app.js")}}"></script>
</body>
</html>