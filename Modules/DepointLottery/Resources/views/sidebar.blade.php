@if(user()->isSuperadmin())
	<li id="depoint-lottery">
		<a href="{{url("manage/depoint/lottery")}}">
			<em class="fa fa-gift"></em>
			<span>{{trans('depointlottery::general.sidebar')}}</span>
		</a>
	</li>
@endif