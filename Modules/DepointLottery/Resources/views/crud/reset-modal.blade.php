{{widget('modal')->label(trans('depointlottery::general.reset'))->target(route('reset-lottery'))->method('POST')}}

<div class="modal-body">
	{!! widget('text')
		 ->id('slug')
		->name("slug")
		->type('text')
		->inForm()
		->value($model->title2)
		->disabled()
		 ->label('tr:depointlottery::general.lottery-number')
	 !!}
	<input type="hidden" value="{{$model->hashid}}" name="post_hashid">


</div>


<div class="modal-footer pv-lg">
	{{widget('feed')}}
	{!! widget('button')->type('submit')->id('delete-btn')->label(trans('trans::downstream.accept'))->class('btn-danger')!!}
	{!! widget('button')->id('cancel-btn')->label(trans('trans::downstream.cancel'))->class(' btn-link btn-taha ')->onClick('$(".modal").modal("hide")')!!}

</div>