@extends('manage::layouts.template')

@section('content')
	<div class="col-xs-12">
		@include("manage::widgets.toolbar" , [
		'buttons' => [
						[
						'type' => "primary",
						'caption' => trans('depointlottery::general.add'),
						'icon' => "plus",
						'target' => "modal:" . route('lottery-add-modal'),
						'condition' => user()->isSuperadmin()
						],
					],
			'title' => trans('depointlottery::general.grid-title')
		])
	</div>
	{{--@include("manage::widgets.toolbar")--}}
	<div class="row">
		<div class="col-md-12">
			@include("manage::widgets.grid" , [
				'table_id' => "lotteries" ,
				'row_view' => "depointlottery::crud.row" ,
				'handle'=>'selector',
				'headings' => [
				trans('depointlottery::general.lottery-number'),
				trans('depointlottery::general.main-gift'),
					trans('depointlottery::general.second-gift'),
					trans('depointlottery::general.third-gift'),
					trans('depointlottery::general.category'),
					trans('depointlottery::general.state'),
				] ,
			]     )

		</div>
	</div>
@stop