{{widget('modal')->label(trans('depointlottery::general.add-modal'))->target(route('add-lottery'))->validation(false)->id('add_lottery')}}

<div class="modal-body">
	{!! widget('text')
		->name("lottery_number")
		->type('text')
		->inForm()
		 ->label('tr:depointlottery::general.lottery-number')
	 !!}

	{!! widget('text')
	->name("main_gift")
	->type('text')
	->inForm()
	 ->label('tr:depointlottery::general.main-gift')
 !!}

	{!! widget('text')
	->name("second_gift")
	->type('text')
	->inForm()
 	->label('tr:depointlottery::general.second-gift')
	!!}

		{!! widget('text')
		->name("third_gift")
		->type('text')
		->inForm()
		 ->label('tr:depointlottery::general.third-gift')
		!!}

	{!!
	 widget('combo')
	  ->name('category')
	  ->inForm()
	  ->label('tr:depointlottery::general.category')
	  ->valueField('key')
	  ->captionField('caption')
	  ->options($categories)
	 !!}
</div>

<div class="modal-footer pv-lg">
	{!! widget('feed') !!}
	{!! widget('button')->type('submit')->id('add-btn')->label(trans('depointlottery::general.save'))->class('btn-success')!!}
	{!! widget('button')->id('cancel-btn')->label(trans('depointlottery::general.cancel'))->class(' btn-link btn-taha ')->onClick('$(".modal").modal("hide")')!!}

</div>