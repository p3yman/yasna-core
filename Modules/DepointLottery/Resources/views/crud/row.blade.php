@include('manage::widgets.grid-row-handle', [
	'handle' => "selector" ,
    'refresh_url' => url('manage/depoint/lottery/update/row/'.$model->hashid),
])

@php($selected=$model->getMeta('selected'))

<td>{{ad($model->title2)}}</td>
<td class="{{$selected=='first'?'bg-primary':''}}">{{!empty($model->title) ?$model->title: trans("depointlottery::general.shit")}}</td>
<td class="{{$selected=='second'?'bg-primary':''}}">{{!empty($model->getMeta('second-gift')) ?$model->getMeta('second-gift'): trans("depointlottery::general.shit")}}</td>
<td class="{{$selected=='third'?'bg-primary':''}}">{{!empty($model->getMeta('third-gift'))  ?$model->getMeta('third-gift'): trans("depointlottery::general.shit")}}</td>
<td>{{$model->categories->first()->title ?? "-"}}</td>
<td class="{{!empty($selected)?'bg-red':''}}">{{!empty($selected)?trans("depointlottery::general.used"):trans("depointlottery::general.unused")}}</td>


@include("manage::widgets.grid-actionCol" , [
"actions" => [
 ['edit',trans('trans::downstream.list.edit'), 'modal:manage/depoint/lottery/update-modal/-hashid-'],
  ['trash',trans('trans::downstream.list.delete'), 'modal:manage/depoint/lottery/delete-modal/-hashid-'],
    ['refresh',trans('depointlottery::general.reset'), 'modal:manage/depoint/lottery/reset-modal/-hashid-'],
    ],
"button_label" =>  trans('trans::downstream.list.actions'),
"button_size"  => "xs" ,  //default: 'xs'
"button_class" => 'default'  //default: 'default'
])