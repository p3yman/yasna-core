<?php


/**
 * backend routes (manage)
 */
Route::group([
     'middleware' => ['web', 'auth', 'is:admin'],
     'prefix'     => 'manage/depoint/lottery',
     'namespace'  => 'Modules\DepointLottery\Http\Controllers',
], function () {
    Route::get('/', "LotteryCRUDController@showLotteryPage");
    Route::get('/add-modal', "LotteryCRUDController@showAddModal")->name('lottery-add-modal');
    Route::post('/add', "LotteryCRUDController@add")->name('add-lottery');
    Route::get("/update-modal/{hash_id}", "LotteryCRUDController@showUpdateModal");
    Route::post('/update', "LotteryCRUDController@updateLottery")->name('update-lottery');
    Route::get('update/row/{hashid}', 'LotteryCRUDController@update');
    Route::get("/delete-modal/{hash_id}", "LotteryCRUDController@showDeleteModal");
    Route::post('/delete', "LotteryCRUDController@delete")->name('delete-lottery');
    Route::get('/reset-modal/{hash_id}', 'LotteryCRUDController@showResetModal');
    Route::post('/reset', 'LotteryCRUDController@reset')->name('reset-lottery');
});

/**
 * frontend routes
 */
Route::group([
     'middleware' => 'web',
     'prefix'     => 'depoint/lottery',
     'namespace'  => 'Modules\DepointLottery\Http\Controllers',
], function () {
    Route::get('/',"DepointLotteryFrontController@index");
    Route::get("/data/{category}","DepointLotteryFrontController@getData");
    Route::post("/choose","DepointLotteryFrontController@choose");
	
	Route::get('/random',"DepointLotteryFrontController@random");
 
});