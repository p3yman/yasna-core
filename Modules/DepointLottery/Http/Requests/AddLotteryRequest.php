<?php

namespace Modules\DepointLottery\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class AddLotteryRequest extends YasnaRequest
{
    protected $model_name = "";



    /**
     * set rules of request
     *
     * @return array
     */
    public function rules()
    {
        return [
             'category'       => 'required',
             'lottery_number' => 'required|integer',
             'main_gift'      => 'required',
        ];
    }



    /**
     * set rules message
     *
     * @return array
     */
    public function messages()
    {
        return [
             'lottery_number.required' => trans('depointlottery::general.miss-lottery-number'),
             'main_gift.required'      => trans('depointlottery::general.miss-main-gift'),
             'lottery_number.integer'  => trans('depointlottery::general.type-lottery-number'),
        ];
    }


}
