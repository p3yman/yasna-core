<?php

namespace Modules\DepointLottery\Http\Controllers;

use Modules\Posts\Entities\Post;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class DepointLotteryFrontController extends YasnaController
{
    /**
     * show index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $view_data['cats'] = posttype('depoint-lottery')->categories;
        return view('depointlottery::front.index', $view_data);
    }



    /**
     * get item of specific category
     *
     * @param string $category
     *
     * @return string
     */
    public function getData($category)
    {
        $data = post()->whereHas('categories', function ($qu) use ($category) {
            return $qu->where("slug", $category);
        })->get()->map(function ($item) {
            return [
                 'post_hashid'    => $item->hashid,
                 'main_gift'      => $item->title,
                 'lottery_number' => (int)$item->title2,
                 'label'          => ad((int)$item->title2),
                 'second_gift'    => $item->getMeta('second-gift'),
                 'third_gift'     => $item->getMeta('third-gift'),
                 'selected'       => $item->getMeta('selected'),
                 'cat_describe'   => $item->categories->first()->texts[getLocale()],
                 'use_class'      => $this->getUsedClass($item, $item->getMeta('selected')),
            ];
        })->sortBy('lottery_number')
        ;

        if ($data->count() == 0) {
            abort(404);
        }

        return json_encode(array_values($data->toArray()));
    }



    /**
     * return correct class for view
     *
     * @param Post   $item
     * @param string $selected
     *
     * @return string|void
     */
    private function getUsedClass($item, $selected)
    {
        if (empty($selected)) {
            return;
        }

        $lost_class = "used lost";
        $used_class = "used";

        if ($selected == "first") {
            if (empty($item->title)) {
                return $lost_class;
            } else {
                return $used_class;
            }
        }

        if ($selected == "second") {
            if (empty($item->getMeta('second-gift'))) {
                return $lost_class;
            } else {
                return $used_class;
            }
        }

        if ($selected == 'third') {
            if (empty($item->getMeta('third-gift'))) {
                return $lost_class;
            } else {
                return $used_class;
            }
        }

        return "undefind";
    }



    /**
     * choose item of lottery
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function choose(SimpleYasnaRequest $request)
    {
        $post       = post($request->get('id'));
        $post->type = 'depoint-lottery';
        $is_saved   = $post->batchSave([
             'selected' => $request->get('field'),
        ]);

        return json_encode(['state' => 'success']);
    }
	
	/**
	 * show random number page
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function random()
	{
		return view('depointlottery::front.random');
	}
}
