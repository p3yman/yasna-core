<?php

namespace Modules\DepointLottery\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Modules\DepointLottery\Http\Requests\AddLotteryRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class LotteryCRUDController extends YasnaController
{
    protected $base_model  = 'Post';
    protected $view_folder = 'depointlottery::crud';
    protected $row_view    = 'row';



    /**
     * show lottery grid
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLotteryPage(SimpleYasnaRequest $request)
    {
        if (!user()->isSuperadmin()) {
            abort(403);
        }
        $view_data['models']  = model('post')->where('type', 'depoint-lottery')->orderBy('id', 'DESC');
        $limit                = 30;
        $view_data['models']  = $view_data['models']->paginate($limit)->appends($request->all());
        $view_data['offset']  = (($request->page ?? 1) - 1) * $limit;
        $view_data['request'] = $request;
        return view("depointlottery::crud.index", $view_data);
    }



    /**
     * show lottery add modal
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAddModal()
    {
        $view_data['categories'] = posttype('depoint-lottery')->categories->map(function ($item) {
            return ['key' => $item->id, "caption" => $item->title];
        })->toArray()
        ;
        return view('depointlottery::crud.add-modal', $view_data);
    }



    /**
     * show update modal
     *
     * @param $hashid
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUpdateModal($hashid)
    {
        $view_data['categories'] = posttype('depoint-lottery')->categories->map(function ($item) {
            return ['key' => $item->id, "caption" => $item->title];
        })->toArray()
        ;
        $view_data['model']      = post($hashid);
        return view('depointlottery::crud.update-modal', $view_data);
    }



    /**
     * show delete modal
     *
     * @param $hashid
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDeleteModal($hashid)
    {
        $view_data['model'] = post($hashid);
        return view('depointlottery::crud.delete-modal', $view_data);
    }



    /**
     * show reset modal
     *
     * @param $hashid
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetModal($hashid)
    {
        $view_data['model'] = post($hashid);
        return view('depointlottery::crud.reset-modal', $view_data);
    }



    /**
     * add new item for lottery
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function add(AddLotteryRequest $request)
    {
        if ($this->checkLotteryNumber($request)) {
            return $this->jsonAjaxSaveFeedback(false, [
                 'danger_message' => trans('depointlottery::general.exist-number'),
            ]);
        }
        $post       = post();
        $post->type = 'depoint-lottery';
        $is_saved   = $post->batchSave([
             'type'         => "depoint-lottery",
             'title'        => $request->main_gift,
             'title2'       => $request->lottery_number,
             'second-gift'  => $request->second_gift,
             'third-gift'   => $request->third_gift,
             'published_at' => Carbon::now()->toDateTimeString(),
        ]);

        $is_saved->attachCategories($request->category);

        return $this->jsonAjaxSaveFeedback($is_saved, [
             'success_refresh' => '1',
        ]);
    }



    /**
     * update lottery item
     *
     * @param AddLotteryRequest $request
     *
     * @return string
     */
    public function updateLottery(AddLotteryRequest $request)
    {
        if ($this->checkLotteryNumber($request, $request->get("post_hashid"))) {
            return $this->jsonAjaxSaveFeedback(false, [
                 'danger_message' => trans('depointlottery::general.exist-number'),
            ]);
        }

        $post       = post($request->get('post_hashid'));
        $post->type = 'depoint-lottery';
        $is_saved   = $post->batchSave([
             'type'         => "depoint-lottery",
             'title'        => $request->main_gift,
             'title2'       => $request->lottery_number,
             'second-gift'  => $request->second_gift,
             'third-gift'   => $request->third_gift,
             'published_at' => Carbon::now()->toDateTimeString(),
        ]);


        $post->attachCategories($request->category);

        return $this->jsonAjaxSaveFeedback($is_saved, [
             'success_callback' => "rowUpdate('lotteries','$post->hashid')",
        ]);
    }



    /**
     * delete lottery item
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function delete(SimpleYasnaRequest $request)
    {

        $hashid = $request->get('post_hashid');
        $post   = post($hashid);

        $is_delete = $post->delete();

        return $this->jsonAjaxSaveFeedback($is_delete, [
             'success_callback' => "rowUpdate('lotteries','$hashid')",
        ]);
    }



    /**
     * reset state of lottery
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function reset(SimpleYasnaRequest $request)
    {
        $hashid     = $request->get('post_hashid');
        $post       = post($hashid);
        $post->type = 'depoint-lottery';
        $is_saved   = $post->batchSave([
             'selected' => "",
        ]);

        return $this->jsonAjaxSaveFeedback($is_saved, [
             'success_callback' => "rowUpdate('lotteries','$hashid')",
        ]);
    }



    /**
     * check lottery number
     *
     * @param Request $request
     * @param string  $hashid
     *
     * @return bool
     */
    private function checkLotteryNumber($request, $hashid = "")
    {
        $check = model('post')->whereHas('categories', function ($qu) use ($request) {
            return $qu->where('categories.id', $request->category);
        })->where('title2', $request->lottery_number)->first()
        ;
        if ($check and $check->hashid != $hashid) {
            return true;
        } else {
            return false;
        }
    }
}
