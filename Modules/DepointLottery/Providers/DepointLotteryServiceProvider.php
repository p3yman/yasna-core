<?php

namespace Modules\DepointLottery\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class DepointLotteryServiceProvider
 *
 * @package Modules\DepointLottery\Providers
 */
class DepointLotteryServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerSideBar();
    }



    /**
     * register sidebar link
     */
    private function registerSideBar()
    {
        service('manage:sidebar')
             ->add('lottery')
             ->blade('DepointLottery::sidebar')
             ->order(80)
        ;
    }
}
