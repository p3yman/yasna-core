<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/25/18
 * Time: 12:02 PM
 */

namespace Modules\Frontier\Providers;


use Modules\Frontier\Http\Endpoints\V1\UserEditEndpoint;

trait FrontierUserEditTrait
{
    /**
     * Registers all the needs of the user edit process.
     */
    protected function registerUserEdit()
    {
        if ($this->cannotUseModule('endpoint')) {
            return;
        }

        $this->registerUserEditEndpoints();
        $this->registerUserEditModels();
    }



    /**
     * Registers the endpoints of the user edit process.
     */
    protected function registerUserEditEndpoints()
    {
        endpoint()->register(UserEditEndpoint::class);
    }



    /**
     * Registers the models of the user edit process.
     */
    protected function registerUserEditModels()
    {
        return; //This is destroying the forms in the manage environment.
        
        $this->addModelTrait('UserEditFormTrait', 'User');
    }
}
