<?php

namespace Modules\Frontier\Providers;

use Modules\Frontier\Http\Endpoints\V1\HaveUserEndpoint;
use Modules\Frontier\Http\Endpoints\V1\LoginEndpoint;
use Modules\Frontier\Http\Endpoints\V1\RegisterEndpoint;
use Modules\Frontier\Http\Endpoints\V1\RoleDeleteEndpoint;
use Modules\Frontier\Http\Endpoints\V1\RoleEditorEndpoint;
use Modules\Frontier\Http\Endpoints\V1\RoleListEndpoint;
use Modules\Frontier\Http\Endpoints\V1\RoleRestoreEndpoint;
use Modules\Frontier\Http\Endpoints\V1\RoleUsersCountEndpoint;
use Modules\Frontier\Http\Endpoints\V1\SettingCustomValueEndpoint;
use Modules\Frontier\Http\Endpoints\V1\SettingDeleteEndpoint;
use Modules\Frontier\Http\Endpoints\V1\SettingEditEndpoint;
use Modules\Frontier\Http\Endpoints\V1\SettingGetEndpoint;
use Modules\Frontier\Http\Endpoints\V1\SettingRestoreEndpoint;
use Modules\Frontier\Http\Endpoints\V1\SetupEndpoint;
use Modules\Frontier\Http\Endpoints\V1\TransEndpoint;
use Modules\Frontier\Http\Middleware\WhiteHouseAuthMiddleware;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class FrontierServiceProvider
 *
 * @package Modules\Frontier\Providers
 */
class FrontierServiceProvider extends YasnaProvider
{
    use FrontierRegisterConfigsTrait;
    use FrontierPasswordResetTrait;
    use FrontierUserEditTrait;



    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerWidgets();
        $this->addRegisterConfigs();
        $this->registerMiddleware();
        $this->registerEndpoints();
        $this->registerPasswordReset();
        $this->registerUserEdit();
    }



    /**
     * Register Dashboard Widgets
     */
    public function registerWidgets()
    {
        if ($this->cannotUseModule('Manage')) {
            return;
        }

        module('manage')
             ->service('widgets_handler')
             ->add('frontier')
             ->method('frontier:handleWidgets')
             ->order(90)
        ;
    }



    /**
     * Handle Dashboard Widgets
     */
    public static function handleWidgets()
    {
        $service = 'widgets';

        module('manage')
             ->service($service)
             ->add('register-in-month')
             ->blade('frontier::widgets.posttype')
             ->trans('frontier::posttype.widget-title')
             ->color('pink')
             ->icon('posts')
             ->condition(function () {
                 user()->isDeveloper();
             })
             ->order(30)
        ;
    }



    /**
     * add middleware
     */
    public function registerMiddleware()
    {
        $this->addMiddleware('WhiteHouseMiddleware', WhiteHouseAuthMiddleware::class);
    }



    /**
     * Registers the endpoints only when the `Endpoint` module is available.
     *
     * @return void
     */
    protected function registerEndpoints()
    {
        if ($this->cannotUseModule("endpoint")) {
            return;
        }

        endpoint()->register(SettingGetEndpoint::class);
        endpoint()->register(SetupEndpoint::class);
        endpoint()->register(RegisterEndpoint::class);
        endpoint()->register(LoginEndpoint::class);
        endpoint()->register(TransEndpoint::class);
        endpoint()->register(RoleUsersCountEndpoint::class);
        endpoint()->register(RoleDeleteEndpoint::class);
        endpoint()->register(RoleEditorEndpoint::class);
        endpoint()->register(RoleListEndpoint::class);
        endpoint()->register(RoleRestoreEndpoint::class);
        endpoint()->register(SettingDeleteEndpoint::class);
        endpoint()->register(SettingEditEndpoint::class);
        endpoint()->register(SettingRestoreEndpoint::class);
        endpoint()->register(SettingCustomValueEndpoint::class);
        endpoint()->register(HaveUserEndpoint::class);
    }

}
