<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/23/18
 * Time: 4:05 PM
 */

namespace Modules\Frontier\Providers;


use Modules\Frontier\Http\Endpoints\V1\PasswordRecoveryEndpoint;
use Modules\Frontier\Http\Endpoints\V1\PasswordRecoveryRequestEndpoint;
use Modules\Frontier\Http\Endpoints\V1\PasswordRecoveryVerifyTokenEndpoint;
use Modules\Frontier\Services\PasswordReset\PasswordResetHelper;

trait FrontierPasswordResetTrait
{
    /**
     * Registers all the needs of the password reset process.
     */
    protected function registerPasswordReset()
    {
        $this->registerPasswordResetSingleton();

        if ($this->passwordReset()->isNotAvailable()) {
            return;
        }

        $this->registerPasswordResetEndpoints();
        $this->registerPasswordResetModelTraits();
    }



    /**
     * Returns the password reset singleton.
     *
     * @return PasswordResetHelper
     */
    public static function passwordReset()
    {
        return app('frontier.password.reset');
    }



    /**
     * Registers a new instance of the `Modules\Frontier\Services\PasswordReset\PasswordResetHelper` as the
     * `frontier.password.reset` singleton.
     */
    protected function registerPasswordResetSingleton()
    {
        $this->app->singleton('frontier.password.reset', function ($app) {
            return new PasswordResetHelper();
        });
    }



    /**
     * Registers the endpoints of the password reset process.
     */
    protected function registerPasswordResetEndpoints()
    {
        endpoint()->register(PasswordRecoveryRequestEndpoint::class);
        endpoint()->register(PasswordRecoveryVerifyTokenEndpoint::class);
        endpoint()->register(PasswordRecoveryEndpoint::class);
    }



    /**
     * Registers the model traits which are needed by the password reset process.
     */
    protected function registerPasswordResetModelTraits()
    {
        $this->addModelTrait('UserPasswordResetTrait', 'User');
    }
}
