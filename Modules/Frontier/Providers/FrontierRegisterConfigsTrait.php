<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/2/18
 * Time: 6:51 PM
 */

namespace Modules\Frontier\Providers;


use Illuminate\Validation\Rule;

trait FrontierRegisterConfigsTrait
{
    /**
     * Registers dynamic configs for the register action.
     */
    protected function addRegisterConfigs()
    {
        $this->addRegisterConfigServices();
        $this->addAllRegisterValidationRules();
        $this->addAllRegisterPurificationRules();
        $this->addAllRegisterFillableFields();
    }



    /**
     * Adds services about register action's configs.
     */
    protected function addRegisterConfigServices()
    {
        module($this->moduleName())
             ->register(frontier()->getRegisterValidationRulesServiceName(),
                  "Validation rules to be used while in the API registration.")
             ->register(frontier()->getRegisterPurificationRulesServiceName(),
                  "Purification rules to be used while in the API registration.")
             ->register(frontier()->getRegisterFillableFieldsServiceName(),
                  "Fillable fields for the API registration.")
        ;
    }



    /**
     * Adds some validation rules for the register action.
     */
    protected function addAllRegisterValidationRules()
    {
        if ($this->userModelIsNotReady()) {
            return;
        }

        $this->addRegisterValidationRule(user()->usernameField(), 'required', 'username_required');
        $this->addRegisterValidationRule(user()->usernameField(), Rule::unique('users'), 'username_unique');
        $this->addRegisterValidationRule('email', 'email', 'email_email');
        $this->addRegisterValidationRule('password', 'required', 'password_required');
        $this->addRegisterValidationRule('password', 'same:password2', 'password_match');
        $this->addRegisterValidationRule('password', 'string', 'password_string');
        $this->addRegisterValidationRule('password', 'min:6', 'password_length');
    }


    /**
     * Adds some purification rules for the register action.
     */
    protected function addAllRegisterPurificationRules()
    {
        if ($this->userModelIsNotReady()) {
            return;
        }

        $this->addRegisterPurificationRule(user()->usernameField(), 'ed', 'username_ed');
        $this->addRegisterPurificationRule('password', 'ed', 'password_ed');
    }



    /**
     * Adds some fillable fields for the register action.
     */
    protected function addAllRegisterFillableFields()
    {
        if ($this->userModelIsNotReady()) {
            return;
        }

        $this->addRegisterFillabelField(user()->usernameField());
        $this->addRegisterFillabelField('password');
        $this->addRegisterFillabelField('password2');
    }



    /**
     * Registers a validation rule for the register action.
     *
     * @param string $filed
     * @param mixed  $rule
     * @param string $name
     */
    public function addRegisterValidationRule(string $filed, $rule, string $name)
    {
        $service = $this->module()->getAlias()
             . ':'
             . frontier()->getRegisterValidationRulesServiceName();

        service($service)
             ->add($name)
             ->set('field', $filed)
             ->set('rule', $rule)
        ;
    }



    /**
     * Registers a purification rule for the register action.
     *
     * @param string $filed
     * @param mixed  $rule
     * @param string $name
     */
    public function addRegisterPurificationRule(string $filed, $rule, string $name)
    {
        $service = $this->module()->getAlias()
             . ':'
             . frontier()->getRegisterPurificationRulesServiceName();

        service($service)
             ->add($name)
             ->set('field', $filed)
             ->set('rule', $rule)
        ;
    }



    /**
     * Add a field to the fillable fields of the register action.
     *
     * @param string $filed
     */
    public function addRegisterFillabelField(string $filed)
    {
        $service = $this->module()->getAlias()
             . ':'
             . frontier()->getRegisterFillableFieldsServiceName();

        service($service)
             ->add($filed);
    }



    /**
     * Returns the name of the service which is handling the register action's validation rules.
     *
     * @return string
     */
    public static function getRegisterValidationRulesServiceName()
    {
        return 'register_validation_rules';
    }



    /**
     * Returns the name of the service which is handling the register action's purification rules.
     *
     * @return string
     */
    public static function getRegisterPurificationRulesServiceName()
    {
        return 'register_purification_rules';
    }



    /**
     * Returns the name of the service which is handling the register action's fillable fields.
     *
     * @return string
     */
    public static function getRegisterFillableFieldsServiceName()
    {
        return 'register_fillable_fields';
    }



    /**
     * Checks if the User model is ready.
     *
     * @return bool
     */
    protected function userModelIsReady()
    {
        return class_exists(MODELS_NAMESPACE . 'User');
    }



    /**
     * Checks if the User model is not ready.
     *
     * @return bool
     */
    protected function userModelIsNotReady()
    {
        return !$this->userModelIsReady();
    }
}
