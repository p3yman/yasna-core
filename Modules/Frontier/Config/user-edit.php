<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/25/18
 * Time: 12:48 PM
 */

return [
     'edu-levels' => [
          'not-specified'     => '0',
          'less-than-diploma' => '1',
          'diploma'           => '2',
          'associate-degree'  => '3',
          'bachelor-degree'   => '4',
          'master-degree'     => '5',
          'phd-or-more'       => '6',
     ],

     'genders' => [
          'male'   => 1,
          'female' => 2,
          'other'  => 3,
     ],

     'marital-statuses' => [
          'single'  => '1',
          'married' => '2',
     ],
];
