<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/2/19
 * Time: 4:12 PM
 */

return [
     'faker' => [
          'locales' => [
               'ar' => 'ar_SA',
               'en' => 'en_US',
               'fa' => 'fa_IR',
               'fr' => 'fr_FR',
          ],
     ],

     'shop' => [
          'posttype' => [
               'products' => [
                    'icon'     => 'shopping-basket',
                    'template' => 'products',
                    'features' => [
                         'manage_sidebar',
                         'shop',
                         'text',
                         'abstract',
                         'featured_image',
                    ],
               ],
          ],
     ],
];
