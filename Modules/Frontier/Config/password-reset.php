<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/23/18
 * Time: 3:33 PM
 */

return [
     "channels" => [
          "email",
          "mobile",
     ],

     "token" => [
          "length"   => 6,
          "validity" => 30,
     ],
];
