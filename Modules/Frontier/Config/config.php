<?php

return [
     'name' => 'Frontier',

     'password-reset' => include(__DIR__ . DIRECTORY_SEPARATOR . 'password-reset.php'),
     'user-edit'      => include(__DIR__ . DIRECTORY_SEPARATOR . 'user-edit.php'),
     'setup'          => include(__DIR__ . DIRECTORY_SEPARATOR . 'setup.php'),
];
