<?php

namespace Modules\Frontier\Entities\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

trait UserPasswordResetTrait
{
    /**
     * Saves the specified token for the user with the specified validity time.
     *
     * @param string $token    The token to be sent.
     * @param int    $validity The time of the token's validity in minutes.
     *
     * @return User
     */
    public function savePasswordResetToken(string $token, int $validity)
    {
        $reset_token            = Hash::make($token);
        $reset_token_expires_at = now()->addMinutes($validity)->toDateTimeString();

        return $this->batchSave(compact('reset_token', 'reset_token_expires_at'));
    }
}
