<?php

namespace Modules\Frontier\Entities\Traits;

use Illuminate\Validation\Rule;

trait UserEditFormTrait
{
    /**
     * The Form Items for the Personal Information
     */
    public function personalFormItems()
    {
        $this->modifyFormItem('name_first')->whichIsRequired();


        $this->addFormItem('birth_date')
             ->whichIsRequired()
             ->withValidationRule('before_or_equal:' . date('Y-m-d'))
             ->withPurificationRule('date')
        ;

        $this->addFormItem('edu_level')
             ->withValidationRule('numeric|in:' . implode(',', $this->acceptableEduLevels()))
        ;

        $this->addFormItem('gender')
             ->whichIsRequired()
             ->withValidationRule('numeric|in:' . implode(',', $this->acceptableGenders()))
        ;
    }



    /**
     * The Form Items for the Marital Information
     */
    public function maritalFormItems()
    {

        $this->addFormItem('marital')
             ->whichIsRequired()
             ->withValidationRule('numeric|in:' . implode(',', $this->acceptableMaritalStatuses()))
        ;


        //$this->addFormItem('marriage_date')
        //     ->withValidationRule(
        //          'date'
        //          . '|'
        //          . 'required_if:marital,' . $this->getMaritalStatusValue('married')
        //          . '|'
        //          . 'before_or_equal:' . date('Y-m-d')
        //     )
        //     ->withPurificationRule('date')
        //; //TODO: Destroys the form in the manage environment!
    }



    /**
     * The Form Items for the Contact Information
     */
    public function contactFormItems()
    {
        $this->modifyFormItem('mobile')
             ->whichIsRequired()
             ->withPurificationRule('ed')
        ;

        $this->addFormItem('tel')
             ->whichIsRequired()
             ->withValidationRule('phone:fixed')
             ->withPurificationRule('ed')
        ;

        $this->addFormItem('home_address')
             ->withValidationRule('string|min:10|max:40')
        ;
    }



    /**
     * The Form Items for the Password Fields
     */
    public function passwordFormItems()
    {
        $this->addFormItem('new_password')
             ->withValidationRule('same:_new_password2|min:8|max:50')
        ;

        $this->addFormItem('_new_password2');
    }



    /**
     * The Form Items for the Divisions Fields
     */
    public function divisionFormItems()
    {
        if (module('frontier')->cannotUse('divisions')) {
            return;
        }

        $this->addFormItem('city')
             ->whichIsRequired()
             ->withValidationRule(
                  Rule::exists('divisions', 'id')
                      ->whereNot('province_id', 0)
                      ->where('city_id', 0)
             )->withPurificationRule('dehash')
        ;
    }



    /**
     * Returns the acceptable values for the `edu_level` column.
     *
     * @return array
     */
    public function acceptableEduLevels()
    {
        return array_values(
             module('frontier')->getConfig('user-edit.edu-levels')
        );
    }



    /**
     * Returns the acceptable values for the `gender` column.
     *
     * @return array
     */
    protected function acceptableGenders()
    {
        return array_values(
             module('frontier')->getConfig('user-edit.genders')
        );
    }



    /**
     * Returns the acceptable values for the `marital` column.
     *
     * @return array
     */
    protected function acceptableMaritalStatuses()
    {
        return array_values(
             module('frontier')->getConfig('user-edit.marital-statuses')
        );
    }



    /**
     * Returns the value of the specified marital status.
     *
     * @param string $status
     *
     * @return string
     */
    protected function getMaritalStatusValue(string $status)
    {
        $values = module('frontier')->getConfig('user-edit.marital-statuses');

        return $values[$status];
    }
}
