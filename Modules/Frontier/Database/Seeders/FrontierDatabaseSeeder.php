<?php

namespace Modules\Frontier\Database\Seeders;

use Illuminate\Database\Seeder;

class FrontierDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsTableSeeder::class);
    }
}
