<?php

namespace Modules\Frontier\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class SettingsTableSeeder extends Seeder
{
    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('settings', $this->data());
    }



    /**
     * Returns the data to be seeded.
     * <br>
     * This data is combination of many methods.
     *
     * @return array
     */
    protected function data()
    {
        return array_merge(
             ...
             $this
                  ->getDataMethods()
                  ->map(function (string $method) {
                      return $this->$method();
                  })
        );
    }



    /**
     * Returns the list of methods to be ran for generating the seeding data.
     *
     * @return Collection
     */
    protected function getDataMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 return ends_with($method, 'Data');
             });
    }



    /**
     * Returns an array of data based on the reset password availability.
     *
     * @return array
     */
    protected function passwordResetData()
    {
        if (frontier()->passwordReset()->isNotAvailable()) {
            return [];
        }

        $module                  = $this->runningModule();
        $password_reset_channels = frontier()->passwordReset()->basicChannels();

        return [
             [
                  'slug'             => 'password_reset_channels',
                  'title'            => $module->getTrans('seeder.setting.password-reset-channels'),
                  'category'         => 'upstream',
                  'order'            => '40',
                  'data_type'        => 'array',
                  'default_value'    => implode(HTML_LINE_BREAK, $password_reset_channels),
                  'hint'             => $module->getTrans('seeder.setting.password-reset-channels-hint', [
                       'example' => implode(', ', $password_reset_channels),
                  ]),
                  'css_class'        => 'ltr',
                  'is_localized'     => '0',
                  'api_discoverable' => '1',
             ],
             [
                  'slug'             => 'password_reset_token_length',
                  'title'            => $module->getTrans('seeder.setting.password-reset-token-length'),
                  'category'         => 'upstream',
                  'order'            => '41',
                  'data_type'        => 'text',
                  'default_value'    => frontier()->passwordReset()->basicTokenLength(),
                  'hint'             => $module->getTrans('seeder.setting.password-reset-token-length-hint'),
                  'css_class'        => 'ltr',
                  'is_localized'     => '0',
                  'api_discoverable' => '1',
             ],
             [
                  'slug'             => 'password_reset_token_validity',
                  'title'            => $module->getTrans('seeder.setting.password-reset-token-validity'),
                  'category'         => 'upstream',
                  'order'            => '42',
                  'data_type'        => 'text',
                  'default_value'    => frontier()->passwordReset()->basicTokenValidity(),
                  'hint'             => $module->getTrans('seeder.setting.password-reset-token-validity-hint'),
                  'css_class'        => 'ltr',
                  'is_localized'     => '0',
                  'api_discoverable' => '1',
             ],
             [
                  'slug'          => 'default_user_role',
                  'title'         => $module->getTrans('seeder.setting.default-user-role'),
                  'category'      => 'users',
                  'order'         => '1',
                  'data_type'     => 'array',
                  'default_value' => 'member',
                  'hint'          => '',
                  'css_class'     => '',
                  'is_localized'  => '0',

             ],
        ];
    }



    /**
     * Returns an array of data based on the login process.
     *
     * @return array
     */
    protected function loginData()
    {
        $module = $this->runningModule();

        if ($module->cannotUse('endpoint')) {
            return [];
        }

        return [
             [
                  'slug'          => 'token_validity_time',
                  'title'         => $module->getTrans('seeder.setting.token-validity-time'),
                  'category'      => 'users',
                  'order'         => '10',
                  'data_type'     => 'text',
                  'default_value' => '10',
                  'hint'          => $module->getTrans('seeder.setting.token-validity-time-hint'),
                  'css_class'     => '',
                  'is_localized'  => '0',

             ],
             [
                  'slug'          => 'username_fields',
                  'title'         => $module->getTrans('seeder.setting.username_fields'),
                  'category'      => 'upstream',
                  'order'         => '10',
                  'data_type'     => 'array',
                  'default_value' => user()->usernameField(),
                  'css_class'     => '',
                  'is_localized'  => '0',
             ],
        ];
    }
}
