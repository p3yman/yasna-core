<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/2/18
 * Time: 4:07 PM
 */

return [
     "1000"  => "User token is missed or incorrect.",
     "1001"  => "The action is unauthorized.",
     "1002"  => "The given data was invalid.",
     "40001" => "Invalid Credentials",

     "frontier-4001" => "Unable to process the setup request because of an unhandled exception.",
];
