<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 9/26/18
 * Time: 5:00 PM
 */

return [
     "setup"               => "Posttypes",
     'news-title'          => 'News',
     'news-title-single'   => 'New',
     'static-title'        => 'Static Pages',
     'static-title-single' => 'Static Page',
     'widget-title'        => 'Make Posttype',
     'with-dummy'          => 'With Dummy Data',
     'exist-posttype'      => "Posttypes exist ",
     'created-posttype'    => "Created Posttype",
     'build'               => 'Build',
     'building'            => 'Quick Posttype Building',
     'default-posts'       => [
          'about-us' => 'About us',
     ],
     'separator'           => '-',
     "no-select-posttype"  => "You didn't choose a posttype.",
     "active-language"     => 'Active Languages',
];
