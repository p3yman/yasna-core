<?php
return [
     "title"        => "Reset Password Token",
     "notification" => [
          "email"  => [
               "greeting" => "Hi :name،",
               "line1"    => "Your Reset Password Token: :token",
          ],
          "mobile" => "Hi :name،"
               . "\n\rYour Reset Password Token: :token"
               . "\n\r:site_title"
               . "\n\r:site_url"
          ,
     ],

     "token-sent"            => "A reset password token has been sent to :channel.",
     "token-invalid"         => "The reset password token is not valid.",
     "password-unacceptable" => "Password is not acceptable.",
];
