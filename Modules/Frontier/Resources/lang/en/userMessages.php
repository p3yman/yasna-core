<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/2/18
 * Time: 4:21 PM
 */

return [
     "1000"  => "Token is incorrect.",
     "1001"  => "The action is unauthorized.",
     "1002"  => "The given data was invalid.",
     "40001" => "Credentials are not valid.",

     "frontier-4001" => "Unable to process the setup request.",
];
