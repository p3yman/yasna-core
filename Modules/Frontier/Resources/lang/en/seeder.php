<?php
return [
     "setting" => [
          "password-reset-channels"            => "Password Reset Channels",
          "password-reset-channels-hint"       => "Enter each one of the channels which the reset password token will be sent "
               . "to users through them in a single line. "
               . "For example: :example"
          ,
          "password-reset-token-length"        => "Password Reset Token Length",
          "password-reset-token-length-hint"   => "Integer Number",
          "password-reset-token-validity"      => "Password Reset Token Validity Time",
          "password-reset-token-validity-hint" => "As Integer Number and in Minutes",
          "setting.default-user-role"          => "Default User Role",
          "token-validity-time"                => "Token Validity Time",
          "token-validity-time-hint"           => "The Validity Time of the Token in Days",
          "username_fields"                    => "Username Fields",
     ],
];
