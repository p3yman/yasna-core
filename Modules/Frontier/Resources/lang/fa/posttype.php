<?php
/**
 * Created by PhpStorm.
 * User: parsa
 * Date: 9/26/18
 * Time: 5:00 PM
 */

return [
     "setup"               => "گونه‌های مطلب اولیه",
     'news-title'          => 'اخبار',
     'news-title-single'   => 'خبر',
     'static-title'        => 'صفحات ایستا',
     'static-title-single' => 'صفحه ایستا',
     'widget-title'        => 'ساخت پست تایپ',
     'with-dummy'          => 'با دیتای دامی',
     'exist-posttype'      => "این پست تایپ ها از قبل موجود بود",
     'created-posttype'    => "این پست تایپ ها ساخته شد",
     'build'               => 'ساخت',
     'building'            => 'ساخت سریع پست تایپ',
     'default-posts'       => [
          'about-us' => 'درباره ‌ما',
     ],
     'separator'           => '-',
     "no-select-posttype"  => "شما پست تایپی انتخاب نکرده‌اید",
     "active-language"     => 'زبان‌های فعال',
];
