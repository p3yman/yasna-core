<?php
return [
     "user-not-found"       => "کاربر پیدا نشد.",
     "model_not_found"      => "داده ارسال‌شده اشتباه است.",
     "g-recaptcha-response" => "کد امنیتی",
];
