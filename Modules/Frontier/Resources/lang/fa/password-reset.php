<?php
return [
     "title"        => "بازنشانی گذرواژه",
     "notification" => [
          "email"  => [
               "greeting" => "سلام :name،",
               "line1"    => "کد بازنشانی گذرواژه‌ی شما: :token",
          ],
          "mobile" => "سلام :name،"
               . "\n\rکد بازنشانی گذرواژه‌ی شما: :token"
               . "\n\r:site_title"
               . "\n\r:site_url"
          ,
     ],

     "token-sent"            => "کد بازنشانی گذرواژه به :channel ارسال شد.",
     "token-invalid"         => "کد بازنشانی گذرواژه صحیح نیست.",
     "password-unacceptable" => "گذرواژه قابل قبول نیست.",
     "mobile"                => "موبایل شما",
     "email"                 => "ایمیل شما",
     "user_name"             => "شناسه کاربری",
     "channel"               => "کانال",
];
