<?php
return [
     "setting" => [
          "password-reset-channels"            => "کانال‌های بازنشانی گذرواژه",
          "password-reset-channels-hint"       => "هر یک از کانال‌هایی که با استفاده از آن‌ها کد بازنشانی گذرواژه برای کاربران "
               . "ارسال می‌شود را در یک خط وارد کنید. "
               . "مثلاً: :example"
          ,
          "password-reset-token-length"        => "طول توکن بازنشانی گذرواژه",
          "password-reset-token-length-hint"   => "عدد صحیح",
          "password-reset-token-validity"      => "مدت زمان اعتبار توکن بازنشانی گذرواژه",
          "password-reset-token-validity-hint" => "به صورت عدد صحیح و در واحد دقیقه",
          "setting.default-user-role"          => "نقش کاربری پیش‌فرض",
          "token-validity-time"                => "مدت زمان اعتبار توکن",
          "token-validity-time-hint"           => "مدت زمان اعتبار توکن بر حسب روز",
          "username_fields"                    => "فیلدهای نام کاربری",
     ],
];
