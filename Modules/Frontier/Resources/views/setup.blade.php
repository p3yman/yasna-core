{!!
 	widget('modal')
 	->target(route('save-posttypes'))
 	    ->method("POST")
 	->label(trans('frontier::posttype.setup'))
 !!}

<div class="modal-body">
	@foreach($posttypes as $slug=>$type)
		{{--todo : this `if` is hard code. we can separate static posttypes. now we have just one static posttype and it works --}}
		@if($slug=='static')
			@continue
		@endif
		<div class="panel panel-default">
			<div class="panel-heading">
				{!!
					widget('checkbox')
					->class('js_panelOpener')
					->name($slug)
					->value("")
					->label($type['posttype']['title'])
				!!}
			</div>

			<div class="panel-body" style="{{ false? '': 'display: none;' }}">

				{!!
				widget('input')
				->label(trans('frontier::posttype.active-language'))
				->name("$slug-locales")
				->value(implode(',',$locales))
				->inForm()
				 !!}

				@foreach($type['default-features'] as $feature=>$state)
					@if($state=='required')
						@continue
					@endif
					{!!
					widget('checkbox')
					->label(model('feature',$feature)->title)
					->name("$slug-features"."[".$feature."]")
				 	!!}
				@endforeach
				{!!
					widget('checkbox')
					->label(trans('frontier::posttype.with-dummy'))
					->name("$slug-dummy")
				!!}
			</div>
		</div>

	@endforeach

	{{--todo : this `if` is hard code. we can separate static posttypes. now we have just one static posttype and it works --}}
	@if(isset($posttypes['static']))
		<div class="panel panel-default">
			<div class="panel-heading">
				{!!
					widget('checkbox')
					->class('js_panelOpener')
					->name('static')
					->value("")
					->label($posttypes['static']['posttype']['title'])
				!!}
			</div>

			<div class="panel-body" style="{{ false? '': 'display: none;' }}">
				{!!
					widget('input')
					->label(trans('frontier::posttype.active-language'))
					->name("$slug-locales")
					->value(implode(',',$locales))
					->inForm()
				 !!}

				<div class="list-group">
					@foreach($posttypes['static']['default-posts'] as $post=>$info)
						<div class="list-group-item">
							<div class="row">
								<div class="col-sm-6">
									{!!
										widget('checkbox')
										->label($info['label'])
										->name("$slug-posts"."[".$post."]")
									 !!}
								</div>
								<div class="col-sm-6">
									{!!
										widget('checkbox')
										->label(trans('frontier::posttype.with-dummy'))
										->name("$post-dummy")
									 !!}

								</div>
							</div>
						</div>
					@endforeach
				</div>


			</div>
		</div>
	@endif
</div>


<div class="modal-footer">
	{!!
	 widget('feed')
	 !!}

	{!!
	 	widget('button')
	      ->type('sumbit')
	 	->label('tr:frontier::posttype.build')
	 	->class('btn-success')
	 !!}
</div>

<script>
    $(document).ready(function () {

        $('.js_panelOpener').on('change', function () {
            togglePanel($(this))
        });


        /*
		* Show / Hide panel body based on checkbox value
		* */
        function togglePanel(checkbox) {

            let relevantBody = checkbox.closest('.panel-heading')
                .siblings('.panel-body');

            if (checkbox.is(':checked')) {
                relevantBody.slideDown();
            } else {
                relevantBody.slideUp();
            }

        }
    })
</script>