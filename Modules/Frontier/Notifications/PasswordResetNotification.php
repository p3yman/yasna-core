<?php

namespace Modules\Frontier\Notifications;

use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PasswordResetNotification extends YasnaNotificationAbstract implements ShouldQueue
{
    use ModuleRecognitionsTrait;

    /**
     * The Token to be sent
     *
     * @var string
     */
    protected $token;

    /**
     * The Channel to send the token.
     *
     * @var string
     */
    protected $channel;



    /**
     * PasswordResetNotification constructor.
     *
     * @param string $token
     * @param string $channel
     */
    public function __construct(string $token, string $channel)
    {
        $this->token   = $token;
        $this->channel = $channel;
    }



    /**
     * Returns the notification's delivery channels.
     *
     * @param User $notifiable
     *
     * @return array
     */
    public function via(User $notifiable)
    {
        $map = $this->channelsMap();

        return array_values(array_only($map, $this->channel));
    }



    /**
     * Returns the map to find the usable channels based on the requested one.
     *
     * @return array
     */
    protected function channelsMap()
    {
        return [
             'email'  => $this->yasnaMailChannel(),
             'mobile' => $this->yasnaSmsChannel(),
        ];
    }



    /**
     * Returns the mail representation of the notification.
     *
     * @param User $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(User $notifiable)
    {
        $module     = $this->runningModule();
        $subjet     = $module->getTrans('password-reset.title');
        $greeting   = $module->getTrans('password-reset.notification.email.greeting', [
             'name' => $notifiable->full_name,
        ]);
        $line1      = $module->getTrans('password-reset.notification.email.line1', [
             'token' => $this->token,
        ]);
        $site_title = get_setting('site_title');
        $site_url   = get_setting('site_url');

        return (new MailMessage)
             ->subject($subjet)
             ->greeting($greeting)
             ->line($line1)
             ->action($site_title, $site_url)
             ->salutation($site_title)
             ;
    }



    /**
     * Returns the sms representation of the notification.
     *
     * @param User $notifiable
     *
     * @return string
     */
    public function toSms(User $notifiable)
    {
        return $this->runningModule()->getTrans('password-reset.notification.mobile', [
             'name'       => $notifiable->full_name,
             'token'      => $this->token,
             'site_title' => get_setting('site_title'),
             'site_url'   => get_setting('site_url'),
        ])
             ;
    }



    /**
     * Returns the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray(User $notifiable)
    {
        return [
             'username' => $notifiable->username,
             'channel'  => $this->channel,
             'token'    => $this->token,
        ];
    }
}
