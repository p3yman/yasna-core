<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/23/18
 * Time: 4:08 PM
 */

namespace Modules\Frontier\Services\PasswordReset;


use App\Models\User;
use Modules\Frontier\Notifications\PasswordResetNotification;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class PasswordResetHelper
{
    use ModuleRecognitionsTrait;



    /**
     * Handles the static method calls.
     * <br>
     * Calls all the static methods as a non-static method.
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public static function __callStatic(string $method, array $parameters)
    {
        return frontier()
             ->passwordReset()
             ->$method(...$parameters)
             ;
    }



    /**
     * Whether the password reset process is available.
     *
     * @return bool
     */
    public function isAvailable()
    {
        return ($this->canUseEndpoint() and $this->canUseNotifier());
    }



    /**
     * Whether the password reset process is not available.
     *
     * @return bool
     */
    public function isNotAvailable()
    {
        return !$this->isAvailable();
    }



    /**
     * Whether this module can use the `Endpoint` module.
     *
     * @return bool
     */
    protected function canUseEndpoint()
    {
        return $this->runningModule()->canUse('endpoint');
    }



    /**
     * Whether this module can use the `notifier` module.
     *
     * @return bool
     */
    protected function canUseNotifier()
    {
        return $this->runningModule()->canUse('notifier');
    }



    /**
     * Returns the default password reset channel of the site.
     *
     * @return string
     */
    public function defaultChannel()
    {
        return array_first($this->channels());
    }



    /**
     * Returns a list of reset password channels.
     *
     * @return array
     */
    public function channels(): array
    {
        $setting_channels = $this->settingChannels();

        return empty($setting_channels)
             ? $this->basicChannels()
             : $setting_channels;
    }



    /**
     * Returns an array of basic channels.
     *
     * @return array
     */
    public function basicChannels(): array
    {
        return $this->getConfig('channels');
    }



    /**
     * Returns an array of reset password channels read from the settings.
     *
     * @return array
     */
    public function settingChannels()
    {
        $setting_channels = get_setting('password_reset_channels');

        if ($setting_channels and is_array($setting_channels)) {
            return $setting_channels;
        } else {
            return [];
        }
    }



    /**
     * Whether the specified channel is acceptable.
     *
     * @param string $channel
     *
     * @return bool
     */
    public function channelIsAcceptable(string $channel): bool
    {
        return in_array($channel, $this->channels());
    }



    /**
     * Saves a reset password token for the specified user and sends it through the specified channel.
     *
     * @param User   $user
     * @param string $channel
     */
    public function sendUserToken(User $user, string $channel)
    {
        $token    = $this->generateToken();
        $validity = $this->tokenValidity();

        $user->savePasswordResetToken($token, $validity);

        $user->notify(new PasswordResetNotification($token, $channel));
    }



    /**
     * Generates a new token for resetting password.
     *
     * @return string
     */
    protected function generateToken(): string
    {
        $length = $this->tokenLength();

        return rand(str_repeat(1, $length), str_repeat(9, $length));
    }



    /**
     * Returns the length of the token to be generated.
     *
     * @return int
     */
    public function tokenLength(): int
    {
        return ($this->settingTokenLength() ?: $this->basicTokenLength());
    }



    /**
     * Returns the length of the token to be generated based on the settings.
     *
     * @return int
     */
    public function settingTokenLength(): int
    {
        $setting = get_setting('password_reset_token_length');

        if ($setting and is_numeric($setting)) {
            return intval($setting);
        } else {
            return 0;
        }
    }



    /**
     * Returns the length of the token to be generated based on the configs.
     *
     * @return int
     */
    public function basicTokenLength(): int
    {
        return $this->getConfig('token.length');
    }



    /**
     * Returns the validity time the token to be generated.
     * This time is in minutes.
     *
     * @return int
     */
    public function tokenValidity(): int
    {
        return ($this->settingTokenValidity() ?: $this->basicTokenValidity());
    }



    /**
     * Returns the validity time the token to be generated based on the settings.
     * This time is in minutes.
     *
     * @return int
     */
    public function settingTokenValidity(): int
    {
        $setting = get_setting('password_reset_token_validity');

        if ($setting and is_numeric($setting)) {
            return intval($setting);
        } else {
            return 0;
        }
    }



    /**
     * Returns the validity time the token to be generated based on the configs.
     * This time is in minutes.
     *
     * @return int
     */
    public function basicTokenValidity(): int
    {
        return $this->getConfig('token.validity');
    }



    /**
     * Returns the value of a config of the password reset section.
     *
     * @param string $config
     *
     * @return mixed
     */
    public function getConfig(string $config)
    {
        return $this
             ->runningModule()
             ->getConfig('password-reset.' . $config)
             ;
    }
}
