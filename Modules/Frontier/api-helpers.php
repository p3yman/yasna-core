<?php

if (!function_exists('apiAuth')) {

    /**
     * Returns the available auth instance for API.
     *
     * @return \Tymon\JWTAuth\JWTGuard|\Tymon\JWTAuth\JWTAuth
     */
    function apiAuth()
    {
        return auth('api');
    }
}
