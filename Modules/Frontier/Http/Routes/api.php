<?php

Route::group([
     'namespace' => module('Frontier')->getControllersNamespace() . '\Api',
     'prefix'    => 'api',
], function () {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
});
