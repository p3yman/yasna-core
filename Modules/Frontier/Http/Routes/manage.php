<?php

if (module('frontier')->canUse('Manage')) {

    /**
     * manage side routes
     */
    Route::group([
         'middleware' => ['web', 'auth', 'is:admin'],
         'prefix'     => 'manage/frontier',
         'namespace'  => 'Modules\Frontier\Http\Controllers',
    ], function () {

        Route::get('posttype/generator', "PosttypeGeneratorController@index");
        Route::get('posttype-generator', "PosttypeGeneratorController@showUI");
        Route::post('save-posttype', "PosttypeGeneratorController@savePosttypes")->name('save-posttypes');
    });

}
