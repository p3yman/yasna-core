<?php

namespace Modules\Frontier\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;


class LoginRequest extends YasnaFormRequest
{
    /**
     * keep JWT token, in case of successful login.
     *
     * @var null
     */
    public $token = null;

    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "username"             => 'required',
             'password'             => 'required',
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
             "credentials"          => $this->token ? "" : "accepted",
        ];
    }



    /**
     * Returns the credentials array based on the requests's parameters.
     *
     * @return array
     */
    public function credentials()
    {
        return [
             user()->usernameField() => $this->getData("username"),
             "password"              => $this->getData("password"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->attempt();
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "credentials.accepted" => trans("frontier::userMessages.40001"),
        ];
    }



    /**
     * manually attempt to login the user
     *
     * @return void
     */
    private function attempt()
    {
        $this->setTokenValidityTime();

        $fields  = get_setting('username_fields');
        $builder = user()->whereRaw('1=0');

        foreach ($fields as $field) {
            $builder = $builder->orWhere($field, $this->getData('username'));
        }
        $user = $builder->first();

        if (!$user or !Hash::check($this->getData('password'), $user->password)) {
            return;
        }

        $this->token = JWTAuth::fromUser($user);
    }



    /**
     * Sets the token validity time from the settings.
     *
     * @return void
     */
    private function setTokenValidityTime()
    {
        $days = (int)get_setting('token_validity_time');
        if (!$days) {
            $days = 1;
        }

        $minutes = $days * 24 * 60;

        apiAuth()->factory()->setTTL($minutes);
    }
}
