<?php

namespace Modules\Frontier\Http\Requests\V1;


class PasswordResetTokenVerifyRequest extends PasswordResetAbstractRequest
{


    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'token',
             'user_name',
        ];
    }



    /**
     * The Token parts of the Rules
     *
     * @return array
     */
    public function tokenRules()
    {
        return [
             'token' => 'required|' . $this->tokenValidation(),
        ];
    }

}
