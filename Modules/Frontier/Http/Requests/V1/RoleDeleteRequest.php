<?php

namespace Modules\Frontier\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class RoleDeleteRequest extends YasnaRequest
{
    protected $model_name = "Role";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return !boolval($this->model->slug == 'manager');
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "_submit" => "in:delete,restore",
        ];
    }
}
