<?php

namespace Modules\Frontier\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class DeleteSettingRequest extends YasnaRequest
{
    protected $responder       = 'white-house';
    protected $model_name      = "setting";
    protected $model_not_found = true;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'hashid',
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'hashid'          => 'required',
             'model_not_found' => $this->model_not_found == true ? '' : 'accepted',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'model_not_found.accepted' => trans_safe('frontier::validation.model_not_found'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if ($this->model->not_exists) {
            $this->model_not_found = false;
        }
    }
}
