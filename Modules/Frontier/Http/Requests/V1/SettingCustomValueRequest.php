<?php

namespace Modules\Frontier\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class SettingCustomValueRequest extends YasnaRequest
{
    protected $responder       = 'white-house';
    protected $model_name      = "setting";
    protected $model_not_found = true;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'hashid',
             'slug',
             'custom_value',
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'custom_value'    => 'required',
             'model_not_found' => $this->model_not_found == true ? '' : 'accepted',
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'model_not_found.accepted' => trans_safe('frontier::validation.model_not_found'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->model->category == "upstream") {
            return dev();
        }
        return user()->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if ($this->model->not_exists) {
            $this->model_not_found = false;
        }
    }



    /**
     * Tries to find the model in question, using $this->model_name and the submitted id/hashid.
     *
     * @param bool $id_or_hashid
     *
     * @return void
     */
    protected function loadRequestedModel($id_or_hashid = false)
    {
        if (!$this->model_name) {
            return;
        }

        if ($this->getRequestedModelId($id_or_hashid) and $this->getData('slug')) {
            $this->model_not_found = false;
        }

        $identifier = $this->getRequestedModelId($id_or_hashid) ?: $this->getData('slug');

        $this->data['id']    = $identifier;
        $this->data['model'] = $this->model = model($this->model_name, $this->data['id'], $this->model_with_trashed);
        $this->refillRequestHashid();
        $this->failIfModelNotExist();
    }
}
