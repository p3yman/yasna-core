<?php

namespace Modules\Frontier\Http\Requests\V1;


class PasswordResetRequest extends PasswordResetAbstractRequest
{
    /**
     * Whether the token field is required.
     *
     * @var bool
     */
    protected $token_required = true;



    /**
     * The Password of the Rules
     *
     * @return array
     */
    public function passwordRules()
    {
        return [
             "password"             => "required|string|min:6|same:password2",
             'g-recaptcha-response' => !env('APP_DEBUG') ? 'required|captcha' : '',
        ];
    }



    /**
     * The Token parts of the Rules
     *
     * @return array
     */
    public function tokenRules()
    {
        return [
             'token' => 'required|' . $this->tokenValidation(),
        ];
    }



    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'password',
             'password2',
             'user_name',
             'token',
        ];
    }

}
