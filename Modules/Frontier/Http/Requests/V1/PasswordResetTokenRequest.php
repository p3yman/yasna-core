<?php

namespace Modules\Frontier\Http\Requests\V1;


use Illuminate\Validation\Rule;

class PasswordResetTokenRequest extends PasswordResetAbstractRequest
{


    /**
     * channel validation.
     *
     * @return array
     */
    public function channelRules()
    {
        return [
             'channel' => Rule::in($this->channels()),
        ];
    }



    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'user_name',
             'channel',
        ];
    }



    /**
     * @inheritDoc
     */
    public function corrections()
    {
        $this->correctChannel();
    }



    /**
     * set default channel.
     */
    protected function correctChannel()
    {
        if (!$this->getData('channel')) {
            $this->setData('channel', 'email');
        }
    }



    /**
     * Returns an array of the valid channels.
     *
     * @return array
     */
    protected function channels()
    {
        return frontier()
             ->passwordReset()
             ->channels()
             ;
    }

}
