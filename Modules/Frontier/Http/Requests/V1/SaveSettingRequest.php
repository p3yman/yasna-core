<?php

namespace Modules\Frontier\Http\Requests\V1;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;


class SaveSettingRequest extends YasnaRequest
{
    protected $responder = 'white-house';

    protected $model_name = "setting";



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'hashid',
             'slug',
             'title',
             'category',
             'order',
             'data_type',
             'default_value',
             'custom_value',
             'is_localized',
             'api_discoverable',
             'css_class',
             'hint',
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id             = $this->getData('id');
        $reserved_slugs = $this->model->reservedSlugs();

        return [
             'slug'             => "required|not_in:$reserved_slugs|unique:settings,slug,$id",
             'title'            => 'required|persian:60',
             'category'         => 'required|in:' . implode(',', $this->model->categoriesArray()),
             'order'            => 'required|numeric',
             'data_type'        => 'required|in:' . implode(',', $this->model->dataTypesArray()),
             'default_value'    => 'required',
             'custom_value'     => 'required',
             'is_localized'     => 'required|boolean',
             'api_discoverable' => 'required|boolean',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'title' => 'pd',
             'slug'  => 'ed',
             'order' => 'ed',
        ];
    }
}
