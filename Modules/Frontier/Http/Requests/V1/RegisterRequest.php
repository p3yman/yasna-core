<?php

namespace Modules\Frontier\Http\Requests\V1;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\YasnaRequest;


class RegisterRequest extends YasnaRequest
{
    protected $responder  = 'white-house';
    protected $model_name = "user";



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'name_first',
             'name_last',
             'email',
             'mobile',
             'gender',
             'password',
             'city',
             'code_melli',
             'marital',
        ];

    }



    /**
     * @inheritdoc
     */
    public function mainRules()
    {
        return [
             'name_first' => 'required|persian:60',
             'name_last'  => 'required|persian:60',
             'email'      => 'required|email',
             'mobile'     => 'required|phone:mobile',
             'gender'     => 'required|numeric|min:1|max:3',
             'password'   => 'min:8',
             'city'       => 'required|numeric|min:1',
             'code_melli' => 'required|code_melli|unique:users',
             'marital'    => 'required|numeric|min:1|max:2',
        ];
    }



    /**
     * @inheritdoc
     */
    public function usernameRules()
    {
        return [
             $this->usernameField() => 'required',
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
        ];
    }



    /**
     * return the name of the username field.
     *
     * @return string
     */
    public function usernameField()
    {
        return user()->usernameField();
    }



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return !user()->exists;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->isNotSetPassword();
    }



    /**
     * if password filed is empty , set mobile for password
     */
    public function isNotSetPassword()
    {
        if (!isset($this->data['password'])) {
            $this->data['password'] = Hash::make($this->data['mobile']);
        } else {
            $this->data['password'] = Hash::make($this->data['password']);
        }
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name_first'           => 'pd',
             'name_last'            => 'pd',
             $this->usernameField() => 'ed',
             'email'                => 'ed',
             'mobile'               => 'ed',
             'gender'               => 'ed',
             'password'             => 'ed',
             'city'                 => 'ed',
             'code_melli'           => 'ed',
             'marital'              => 'ed',
        ];
    }



    /**
     * Returns the user which should be edited.
     *
     * @return User
     */
    public function getSafeModel()
    {
        $model = $this->model;

        return $model->exists
             ? $model
             : user();
    }
}
