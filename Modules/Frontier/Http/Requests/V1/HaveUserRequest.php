<?php

namespace Modules\Frontier\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class HaveUserRequest extends YasnaFormRequest
{
    protected $automatic_injection_guard = false; //<~~ This endpoint doesn't save anything into the database.

    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "field" => "required",
             "value" => "required",
        ];
    }



    /**
     * get the common meta response for this request
     *
     * @return array
     */
    public function commonMetaResponse()
    {
        return [
             "field" => $this->getData("field"),
             "value" => $this->getData("value"),
        ];
    }

}
