<?php

namespace Modules\Frontier\Http\Requests\V1;

use Modules\Yasna\Services\TransCollector\Guesser\LocalesGuesser;
use Modules\Yasna\Services\TransCollector\Guesser\ModulesGuesser;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class TransRequest
 *
 * @property array|null $modules
 * @property array|null $locales
 * @property array|null $phrases
 */
class TransRequest extends YasnaRequest
{
    /** @var string */
    protected $responder = 'white-house';



    /**
     * Guesses the modules to be used based on the request.
     *
     * @return array|string[]
     */
    public function guessModules()
    {
        return (new ModulesGuesser($this->modules))->getModules();
    }



    /**
     * Guesses the locales to be used based on the request.
     *
     * @return array|string[]
     */
    public function guessLocales()
    {
        return (new LocalesGuesser($this->locales))->getLocales();
    }



    /**
     * If the overall translations are needed.
     *
     * @return bool
     */
    public function withOveralls()
    {
        if (empty($this->modules)) {
            return true;
        }

        return (
             in_array('overall', $this->modules)
             or
             in_array('Overall', $this->modules)
        );
    }



    /**
     * If the dynamic translations are needed.
     *
     * @return bool
     */
    public function withDynamics()
    {
        if (empty($this->modules)) {
            return true;
        }

        return (
             in_array('dynamic', $this->modules)
             or
             in_array('Dynamic', $this->modules)
        );
    }



    /**
     * If specific phrases are being requested.
     *
     * @return bool
     */
    public function requestingPhrases()
    {
        return !empty($this->phrases);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctModules();
        $this->correctLocales();
        $this->correctPhrases();
    }



    /**
     * Corrects the `modules` field.
     */
    protected function correctModules()
    {
        $modules = $this->getData('modules');

        if (empty($modules)) {
            return;
        }

        $this->setData('modules', (array)$modules);
    }



    /**
     * Corrects the `locales` field.
     */
    protected function correctLocales()
    {
        $locales = $this->getData('locales');

        if (empty($locales)) {
            return;
        }

        $this->setData('locales', (array)$locales);
    }



    /**
     * Corrects the `phrases` field.
     */
    protected function correctPhrases()
    {
        $phrases = $this->getData('phrases');

        if (empty($phrases)) {
            return;
        }

        $this->setData('phrases', (array)$phrases);
    }
}
