<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/25/18
 * Time: 12:07 PM
 */

namespace Modules\Frontier\Http\Requests\V1;


use App\Models\User;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class UserEditRequest
 *
 * @property User $model
 */
class UserEditRequest extends YasnaRequest
{
    /**
     * The Main Model
     *
     * @var string
     */
    protected $model_name = 'user';

    /**
     * Whether a trashed user is acceptable
     *
     * @var bool
     */
    protected $model_with_trashed = false;

    /**
     * The Main Responder
     *
     * @var string
     */
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->userExists()
             and
             $this->userIsEditable()
        );
    }



    /**
     * Whether the user model exists
     *
     * @return bool
     */
    protected function userExists()
    {
        return $this->getSafeModel()->exists;
    }



    /**
     * Whether the user model is editable by the logged in user.
     *
     * @return bool
     */
    protected function userIsEditable()
    {
        return (
             $this->isSelfEditing()
             or
             $this->hasEditPermission()
        );
    }



    /**
     * Whether the user is trying to edit the data of itself.
     *
     * @return bool
     */
    protected function isSelfEditing()
    {
        return ($this->getSafeModel()->id == user()->id);
    }



    /**
     * Whether the logged in user can edit permission the user model.
     *
     * @return bool
     */
    protected function hasEditPermission()
    {
        return $this->getSafeModel()->canEdit();
    }



    /**
     * Returns the user which should be edited.
     *
     * @return User
     */
    public function getSafeModel()
    {
        $model = $this->model;

        return $model->exists
             ? $model
             : user();
    }



    /**
     * The Dynamic Part of the Rules
     *
     * @return array
     */
    public function dynamicRules()
    {
        return $this->getFieldsColumn('validation_rules');
    }



    /**
     * The Username Part of the Rules
     *
     * @return array
     */
    public function usernameRules()
    {
        $user_name_field = $this->getSafeModel()->usernameField();

        return [
             $user_name_field => Rule::unique('users', $user_name_field)
                                     ->whereNot('id', $this->getSafeModel()->id),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return $this->getFieldsColumn('purification_rules');
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_keys($this->getFormItems());
    }



    /**
     * Returns the specified column of the form data.
     *
     * @param string $column
     *
     * @return array
     */
    protected function getFieldsColumn(string $column)
    {
        return collect($this->getFormItems())
             ->map(function (array $field) use ($column) {

                 return $field[$column];

             })
             ->filter()
             ->toArray()
             ;
    }



    /**
     * Returns the form data.
     *
     * @return array
     */
    protected function getFormItems()
    {
        return $this->getSafeModel()->renderFormItems();
    }
}
