<?php

namespace Modules\Frontier\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class RoleSaveRequest extends YasnaRequest
{
    protected $responder  = 'white-house';
    protected $model_name = "Role";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $id       = $this->data['id'];
        $reserved = role()->reserved_slugs;

        return [
             'slug'         => "required|alpha_dash|not_in:$reserved|unique:roles,slug,$id,id",
             'title'        => "required|unique:roles,title,$id,id",
             'plural_title' => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'slug'     => "lower",
             "is_admin" => "boolean",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->ensureAdminStateOfManager();
    }



    /**
     * Makes sure the `manager` role is and remains admin.
     */
    private function ensureAdminStateOfManager()
    {
        if ($this->data['slug'] == 'manager') {
            $this->data['is_admin'] = true;
        }
    }
}
