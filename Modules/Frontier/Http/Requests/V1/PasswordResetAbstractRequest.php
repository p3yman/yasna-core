<?php

namespace Modules\Frontier\Http\Requests\V1;


use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PasswordResetAbstractRequest
 * <br>
 * The request to be used in all the steps of the password reset process.
 *
 * @property User $model
 */
abstract class  PasswordResetAbstractRequest extends YasnaFormRequest
{

    protected $model_name = 'User';

    protected $should_allow_create_mode = false;



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return [
             frontier()->passwordReset()->isAvailable(),
        ];
    }



    /**
     * The Rules to Validate Channel Info
     *
     * @return array
     */
    public function mainRules()
    {
        return [
             'user_name' => 'required|' . $this->userValidation(),
        ];
    }



    /**
     * @return string
     */
    protected function userValidation()
    {
        if ($this->model->exists) {
            return '';
        }
        return 'invalid';
    }



    /**
     * @inheritDoc
     */
    public function loadModel()
    {
        $fields  = get_setting('username_fields');
        $builder = user()->whereRaw('1=0');
        foreach ($fields as $field) {
            $builder = $builder->orWhere($field, $this->getData('user_name'));
        }
        $user = $builder->first() ?? null;

        if ($user && $user->exists) {
            $this->model = $user;
        }
    }



    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [
             'user_name.invalid' => trans_safe('frontier::validation.user-not-found'),
             'token.invalid'     => trans_safe('frontier::password-reset.token-invalid'),
        ];
    }



    /**
     * @inheritDoc
     */
    public function attributes()
    {
        return [
             'user_name' => trans_safe('frontier::password-reset.user_name'),
             'channel'   => trans_safe('frontier::password-reset.channel'),
        ];
    }



    /**
     * token validation.
     *
     * @return string
     */
    public function tokenValidation()
    {
        $token = $this->getData('token');
        if ($this->userTokenIsValid($token)) {
            return '';
        }

        return 'invalid';

    }



    /**
     * Whether the specified token is acceptable for the found user.
     *
     * @param string $token
     *
     * @return bool
     */
    protected function userTokenIsValid($token)
    {
        $user = $this->model;
        return (
            // check if token matchs
             Hash::check($token, $user->reset_token)
             and
             // check if token has not been expired
             Carbon::parse($user->reset_token_expires_at)->greaterThan(now())
        );
    }

}
