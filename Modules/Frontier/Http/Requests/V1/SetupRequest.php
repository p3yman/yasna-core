<?php

namespace Modules\Frontier\Http\Requests\V1;

use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;


class SetupRequest extends YasnaRequest
{
    /**
     * @var string
     */
    protected $responder = 'white-house';

    /** @var array */
    protected $acceptable_features;

    /** @var array */
    protected $acceptable_inputs;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $parts = collect($this->fillableFieldsMethods())
             ->map(function (string $method) {
                 return $this->$method();
             })->toArray()
        ;

        return array_merge(...$parts);
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $parts = collect($this->attributesMethods())
             ->map(function (string $method) {
                 return $this->$method();
             })->toArray()
        ;

        return array_merge(...$parts);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $methods = $this->correctionMethods();

        foreach ($methods as $method) {
            $this->$method();
        }
    }



    /**
     * Returns an array of the methods to be called in the `fillableFields()` method.
     *
     * @return array
     */
    protected function fillableFieldsMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 // method should end with `FillableFields` and have at least one character before it
                 return preg_match('/^(.)+FillableFields$/', $method);
             })->values()
             ->toArray()
             ;
    }



    /**
     * Returns an array of the methods to be called in the `attributes()` method.
     *
     * @return array
     */
    protected function attributesMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 // method should end with `Attributes` and have at least one character before it
                 return preg_match('/^(.)+Attributes$/', $method);
             })->values()
             ->toArray()
             ;
    }



    /**
     * Returns an array of the methods to be called in the `corrections()` method.
     *
     * @return array
     */
    protected function correctionMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 // method should end with `Corrections$` and have at least one character before it
                 return preg_match('/^(.)+Corrections$/', $method);
             })->values()
             ->toArray()
             ;
    }



    /**
     * Returns an array of the rules related to filemanager.
     *
     * @return array
     */
    protected function fileManagerRules()
    {
        if ($this->runningModule()->cannotUse('filemanager')) {
            return [];
        }

        return [
             'custom_file_settings'          => 'boolean',
             'file_settings'                 => 'array',
             'file_settings.*'               => 'array',
             'file_settings.*.slug'          => 'required_if:custom_file_settings,1',
             'file_settings.*.title'         => 'required_if:custom_file_settings,1',
             'file_settings.*.value'         => 'required_if:custom_file_settings,1|array',
             'file_settings.*.posttypes'     => 'array',
             'file_settings.*.input_types'   => 'array',
             'file_settings.*.input_types.*' => Rule::in($this->acceptableFileManagerInputTypes()),
        ];
    }



    /**
     * Returns an array of the acceptable values for file manager input types.
     *
     * @return array
     */
    protected function acceptableFileManagerInputTypes()
    {
        return ['featured_image', 'attachments'];
    }



    /**
     * Returns an array of the fillable fields related to file manager.
     *
     * @return array
     */
    protected function fileManagerFillableFields()
    {
        return [
             'custom_file_settings',
             'file_settings',
        ];
    }



    /**
     * Returns an array of the attributes related to file manager.
     *
     * @return array
     */
    protected function filemanagerAttributes()
    {
        if ($this->runningModule()->cannotUse('filemanager')) {
            return [];
        }

        $module = $this->runningModule();

        return [
             'custom_file_settings'          => $module->getTrans('setup.filemanager.custom-file-settings'),
             'file_settings'                 => $module->getTrans('setup.filemanager.file-settings'),
             'file_settings.*'               => $module->getTrans('setup.filemanager.file-settings-*'),
             'file_settings.*.slug'          => $module->getTrans('setup.filemanager.file-settings-*-slug'),
             'file_settings.*.title'         => $module->getTrans('setup.filemanager.file-settings-*-title'),
             'file_settings.*.value'         => $module->getTrans('setup.filemanager.file-settings-*-value'),
             'file_settings.*.posttypes'     => $module->getTrans('setup.filemanager.file-settings-*-posttypes'),
             'file_settings.*.input_types'   => $module->getTrans('setup.filemanager.file-settings-*-input-types'),
             'file_settings.*.input_types.*' => $module->getTrans('setup.filemanager.file-settings-*-input-types-*'),
        ];
    }



    /**
     * Returns an array of the rules related to the settings.
     *
     * @return array
     */
    public function settingsRules()
    {
        return [
             'settings'         => 'array',
             'settings.*'       => 'array',
             'settings.*.slug'  => 'required|exists:settings,slug',
             'settings.*.value' => 'required',

             'site_settings' => 'array',

             'locales' => 'array',
        ];
    }



    /**
     * Returns an array of the fillable fields related to the settings.
     *
     * @return array
     */
    protected function settingsFillableFields()
    {
        return ['settings', 'site_settings', 'locales'];
    }



    /**
     * Returns an array of the attributes related to settings.
     *
     * @return array
     */
    protected function settingsAttributes()
    {
        $module = $this->runningModule();

        return [
             'settings'         => $module->getTrans('setup.yasna.settings'),
             'settings.*'       => $module->getTrans('setup.yasna.settings-*'),
             'settings.*.slug'  => $module->getTrans('setup.yasna.settings-*-slug'),
             'settings.*.value' => $module->getTrans('setup.yasna.settings-*-value'),
             'locales'          => $module->getTrans('setup.yasna.locales'),
        ];
    }



    /**
     * Does the corrections related to the settings.
     */
    protected function settingsCorrections()
    {
        $settings = $this->getData('settings');

        if (!$settings and !is_array($settings)) {
            return;
        }

        foreach ($settings as $key => $info) {
            $slug = ($info['slug'] ?? null);

            if (!$slug) {
                unset($settings[$key]);
            }
        }

        $this->setData('settings', $settings);
    }



    /**
     * Returns an array of the rules related to the posttypes.
     *
     * @return array
     */
    public function posttypesRules()
    {
        if ($this->runningModule()->cannotUse('posts')) {
            return [];
        }

        return [
             'default_post_types'                => 'array',
             'default_post_types.*'              => 'array',
             'default_post_types.*.create'       => 'boolean',
             'default_post_types.*.dummy'        => 'boolean',
             'default_post_types.*.dummy_counts' => 'integer',
             'default_post_types.*.slug'         => 'required',
             'default_post_types.*.title'        => 'required',
             'default_post_types.*.icon'         => 'required',
             'default_post_types.*.template'     => 'required',
             'default_post_types.*.locales'      => 'array|nullable',
             'default_post_types.*.features'     => 'array|nullable',
             'default_post_types.*.inputs'       => 'array|nullable',
             'default_post_types.*.refresh'      => 'boolean',
             'default_post_types.*.sample_file'  => Rule::in($this->posttypesAcceptableSampleFile()),

             'custom_post_types'         => 'boolean',
             'post_types'                => 'required_if:custom_post_types,1|array',
             'post_types.*'              => 'array',
             'post_types.*.dummy'        => 'boolean',
             'post_types.*.dummy_counts' => 'integer',
             'post_types.*.slug'         => 'required_if:custom_post_types,1',
             'post_types.*.title'        => 'required_if:custom_post_types,1',
             'post_types.*.icon'         => 'required_if:custom_post_types,1',
             'post_types.*.template'     => 'required_if:custom_post_types,1',
             'post_types.*.locales'      => 'array|nullable',
             'post_types.*.features'     => 'array|nullable',
             'post_types.*.inputs'       => 'array|nullable',
             'post_types.*.refresh'      => 'boolean',
             'post_types.*.sample_file'  => Rule::in($this->posttypesAcceptableSampleFile()),
        ];
    }



    /**
     * Returns an array of the acceptable sample files.
     *
     * @return array
     */
    protected function posttypesAcceptableSampleFile()
    {
        return [
             'slideshow',
             'content',
             'product',
        ];
    }



    /**
     * Returns an array of the fillable fields related to the posttypes.
     *
     * @return array
     */
    protected function posttypesFillableFields()
    {
        if ($this->runningModule()->cannotUse('posts')) {
            return [];
        }

        return ['default_post_types', 'custom_post_types', 'post_types'];
    }



    /**
     * Returns an array of the attributes related to posttypes.
     *
     * @return array
     */
    protected function posttypesAttributes()
    {
        if ($this->runningModule()->cannotUse('posts')) {
            return [];
        }

        $module = $this->runningModule();

        return [
             'default_post_types'                  => $module->getTrans('setup.posts.default-post-types'),
             'default_post_types.*'                => $module->getTrans('setup.posts.default-post-types-*'),
             'default_post_types.*.create'         => $module->getTrans('setup.posts.default-post-types-*-create'),
             'default_post_types.*.dummy'          => $module->getTrans('setup.posts.default-post-types-*-dummy'),
             'default_post_types.*.dummy_counts'   => $module->getTrans('setup.posts.default-post-types-*-dummy_counts'),
             'default_post_types.*.slug'           => $module->getTrans('setup.posts.default-post-types-*-slug'),
             'default_post_types.*.title'          => $module->getTrans('setup.posts.default-post-types-*-title'),
             'default_post_types.*.singular_title' => $module->getTrans('setup.posts.default-post-types-*-singular_title'),
             'default_post_types.*.icon'           => $module->getTrans('setup.posts.default-post-types-*-icon'),
             'default_post_types.*.template'       => $module->getTrans('setup.posts.default-post-types-*-template'),
             'default_post_types.*.locales'        => $module->getTrans('setup.posts.default-post-types-*-locales'),
             'default_post_types.*.features'       => $module->getTrans('setup.posts.default-post-types-*-features'),
             'default_post_types.*.features.*'     => $module->getTrans('setup.posts.default-post-types-*-features-*'),
             'default_post_types.*.inputs'         => $module->getTrans('setup.posts.default-post-types-*-inputs'),
             'default_post_types.*.inputs.*'       => $module->getTrans('setup.posts.default-post-types-*-inputs-*'),
             'default_post_types.*.refresh'        => $module->getTrans('setup.posts.default-post-types-*-refresh'),
             'default_post_types.*.sample_file'    => $module->getTrans('setup.posts.default-post-types-*-sample-file'),

             'custom_post_types'           => $module->getTrans('setup.posts.custom-post-types'),
             'post_types'                  => $module->getTrans('setup.posts.post-types'),
             'post_types.*'                => $module->getTrans('setup.posts.post-types-*'),
             'post_types.*.dummy'          => $module->getTrans('setup.posts.post-types-*-dummy'),
             'post_types.*.dummy_counts'   => $module->getTrans('setup.posts.post-types-*-dummy_counts'),
             'post_types.*.slug'           => $module->getTrans('setup.posts.post-types-*-slug'),
             'post_types.*.title'          => $module->getTrans('setup.posts.post-types-*-title'),
             'post_types.*.singular_title' => $module->getTrans('setup.posts.post-types-*-singular_title'),
             'post_types.*.icon'           => $module->getTrans('setup.posts.post-types-*-icon'),
             'post_types.*.template'       => $module->getTrans('setup.posts.post-types-*-template'),
             'post_types.*.locales'        => $module->getTrans('setup.posts.post-types-*-locales'),
             'post_types.*.features'       => $module->getTrans('setup.posts.post-types-*-features'),
             'post_types.*.features.*'     => $module->getTrans('setup.posts.post-types-*-features-*'),
             'post_types.*.inputs'         => $module->getTrans('setup.posts.post-types-*-inputs'),
             'post_types.*.inputs.*'       => $module->getTrans('setup.posts.post-types-*-inputs-*'),
             'post_types.*.refresh'        => $module->getTrans('setup.posts.post-types-*-refresh'),
             'post_types.*.sample_file'    => $module->getTrans('setup.posts.post-types-*-sample-file'),


        ];
    }



    /**
     * Does the corrections related to the pages.
     */
    protected function posttypesCorrections()
    {
        $this->correctPosttypesArray('default_post_types');
        $this->correctPosttypesArray('post_types');
    }



    /**
     * Corrects posttypes array in the given field.
     *
     * @param string $field
     */
    protected function correctPosttypesArray(string $field)
    {
        $posttypes = ($this->data[$field] ?? []);

        if (!is_array($posttypes)) {
            return;
        }

        $keys = array_keys($posttypes);

        foreach ($keys as $key) {
            $this->correctPosttypeArrayItem($field, $key);
        }
    }



    /**
     * Corrects a posttype array in the given field and the given index.
     *
     * @param string     $field
     * @param string|int $index
     */
    protected function correctPosttypeArrayItem(string $field, $index)
    {
        $this->correctPosttypeItemFeatures($field, $index);
        $this->correctPosttypeItemInputs($field, $index);
    }



    /**
     * Corrects the features of a posttype array in the given field and the given index.
     *
     * @param string     $field
     * @param string|int $index
     */
    protected function correctPosttypeItemFeatures(string $field, $index)
    {
        $posttype = $this->data[$field][$index];
        $features = ($posttype['features'] ?? null);

        if (empty($features) or !is_array($features)) {
            return;
        }

        // remove invalid items
        foreach ($features as $key => $feature) {
            if (in_array($feature, $this->acceptableFeatures())) {
                continue;
            }

            unset($features[$key]);
        }

        $this->data[$field][$index]['features'] = $features;
    }



    /**
     * Returns an array of the acceptable values for the posttypes' features.
     *
     * @return array
     */
    protected function acceptableFeatures()
    {
        if (!$this->acceptable_features) {
            $this->acceptable_features = model('feature')->pluck('slug')->toArray();
        }

        return $this->acceptable_features;
    }



    /**
     * Corrects the inputs of a posttype array in the given field and the given index.
     *
     * @param string     $field
     * @param string|int $index
     */
    protected function correctPosttypeItemInputs(string $field, $index)
    {
        $posttype = $this->data[$field][$index];
        $inputs   = ($posttype['inputs'] ?? null);

        if (empty($inputs) or !is_array($inputs)) {
            return;
        }


        // remove invalid items
        foreach ($inputs as $key => $feature) {
            if (in_array($feature, $this->acceptableInputs())) {
                continue;
            }

            unset($inputs[$key]);
        }

        $this->data[$field][$index]['inputs'] = $inputs;
    }



    /**
     * Returns an array of the acceptable values for the posttypes' inputs.
     *
     * @return array
     */
    protected function acceptableInputs()
    {
        if (!$this->acceptable_inputs) {
            $this->acceptable_inputs = model('input')->pluck('slug')->toArray();
        }

        return $this->acceptable_inputs;
    }



    /**
     * Returns an array of the rules related to the roles.
     *
     * @return array
     */
    public function rolesRules()
    {
        return [
             'custom_roles'         => 'boolean',
             'roles'                => 'array',
             'roles.*'              => 'array',
             'roles.*.title'        => 'required',
             'roles.*.plural_title' => 'required',
             'roles.*.slug'         => 'required',
             'roles.*.is_admin'     => 'required|boolean',
        ];
    }



    /**
     * Returns an array of the fillable fields related to the roles.
     *
     * @return array
     */
    protected function rolesFillableFields()
    {
        return ['custom_roles', 'roles'];
    }



    /**
     * Returns an array of the attributes related to roles.
     *
     * @return array
     */
    protected function rolesAttributes()
    {
        $module = $this->runningModule();

        return [
             'custom_roles'         => $module->getTrans('setup.yasna.custom-roles'),
             'roles'                => $module->getTrans('setup.yasna.roles'),
             'roles.*'              => $module->getTrans('setup.yasna.roles-*'),
             'roles.*.title'        => $module->getTrans('setup.yasna.roles-*-title'),
             'roles.*.plural_title' => $module->getTrans('setup.yasna.roles-*-plural-title'),
             'roles.*.slug'         => $module->getTrans('setup.yasna.roles-*-slug'),
             'roles.*.is_admin'     => $module->getTrans('setup.yasna.roles-*-is-admin'),
        ];
    }



    /**
     * Returns an array of the rules related to the pages.
     *
     * @return array
     */
    protected function pagesRules()
    {
        if ($this->runningModule()->cannotUse('posts')) {
            return [];
        }

        return [
             'pages'          => 'array',
             'pages.*'        => 'array',
             'pages.*.slug'   => 'required|' . Rule::in($this->acceptablePageSlugs()),
             'pages.*.create' => 'boolean',
             'pages.*.dummy'  => 'boolean',
        ];
    }



    /**
     * Returns an array of the acceptable page slugs.
     *
     * @return array
     */
    protected function acceptablePageSlugs()
    {
        return [
             'about',
             'participate',
             'jobs',
             'return-order',
             'how-to-order',
             'how-we-deliver',
             'how-to-pay',
             'delivery-terms',
             'privacy',
             'terms-and-conditions',
        ];
    }



    /**
     * Returns an array of the fillable fields related to the pages.
     *
     * @return array
     */
    protected function pagesFillableFields()
    {
        if ($this->runningModule()->cannotUse('posts')) {
            return [];
        }

        return ['pages'];
    }



    /**
     * Returns an array of the attributes related to pages.
     *
     * @return array
     */
    protected function pagesAttributes()
    {
        if ($this->runningModule()->cannotUse('posts')) {
            return [];
        }

        $module = $this->runningModule();

        return [
             'pages'          => $module->getTrans('setup.posts.pages'),
             'pages.*'        => $module->getTrans('setup.posts.pages-*'),
             'pages.*.create' => $module->getTrans('setup.posts.pages-*-create'),
             'pages.*.slug'   => $module->getTrans('setup.posts.pages-*-slug'),
             'pages.*.dummy'  => $module->getTrans('setup.posts.pages-*-dummy'),
        ];
    }



    /**
     * Does the corrections related to the pages.
     */
    protected function pagesCorrections()
    {
        if ($this->runningModule()->cannotUse('posts')) {
            return;
        }

        $pages = $this->getData('pages');

        if (!$pages and !is_array($pages)) {
            return;
        }

        $new_pages = [];
        foreach ($pages as $slug => $info) {
            if (is_array($info)) {
                $info['slug'] = $slug;
                $new_pages[]  = $info;
            } else {
                $new_pages[$slug] = $info;
            }
        }

        $this->setData('pages', $new_pages);
    }



    /**
     * Returns an array of the rules related to the shop dummy.
     *
     * @return array
     */
    protected function shopRules()
    {
        if ($this->runningModule()->cannotUse('shop')) {
            return [];
        }

        return [
             'shop_dummy'    => 'boolean',
             'shop'          => 'array',
             'shop.posts'    => 'required_if:shop_dummy,1|integer',
             'shop.wares'    => 'required_if:shop_dummy,1|integer',
             'shop.packages' => 'required_if:shop_dummy,1|integer',
             'shop.locales'  => 'required_if:shop_dummy,1|array',
        ];
    }



    /**
     * Returns an array of the fillable fields related to the shop dummy.
     *
     * @return array
     */
    protected function shopFillableFields()
    {
        if ($this->runningModule()->cannotUse('shop')) {
            return [];
        }

        return ['shop_dummy', 'shop'];
    }



    /**
     * Returns an array of the attributes related to shop dummy.
     *
     * @return array
     */
    protected function shopAttributes()
    {
        if ($this->runningModule()->cannotUse('shop')) {
            return [];
        }

        $module = $this->runningModule();

        return [
             'shop_dummy'    => $module->getTrans('setup.shop.shop-dummy'),
             'shop'          => $module->getTrans('setup.shop.shop'),
             'shop.posts'    => $module->getTrans('setup.shop.shop-posts'),
             'shop.wares'    => $module->getTrans('setup.shop.shop-wares'),
             'shop.locales'  => $module->getTrans('setup.shop.shop-locales'),
             'shop.packages' => $module->getTrans('setup.shop.shop-packages'),
        ];
    }
}
