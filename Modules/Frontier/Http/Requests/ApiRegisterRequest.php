<?php

namespace Modules\Frontier\Http\Requests;

use Closure;
use Illuminate\Support\Collection;
use Modules\Yasna\Services\YasnaRequest;

class ApiRegisterRequest extends YasnaRequest
{
    protected $model_name = "user";
    protected $responder  = 'white-house';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return $this->rulesFromService(
             frontier()->getRegisterValidationRulesServiceName(),
             function (Collection $group_rules) {
                 return $group_rules->map(function ($rule) {
                     return $rule['rule'];
                 });
             }
        );
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return $this->rulesFromService(
             frontier()->getRegisterPurificationRulesServiceName(),
             function (Collection $group_rules) {
                 return $group_rules->last()['rule'];
             }
        );
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $registered = service(
             $this->runningModule()->getAlias()
             . ':'
             . frontier()->getRegisterFillableFieldsServiceName()
        )->read();

        return collect($registered)
             ->map(function ($item) {
                 return $item['key'];
             })
             ->values()
             ->toArray()
             ;

    }



    /**
     * Returns an array of rules based on the given service.
     *
     * @param string  $service
     * @param Closure $group_map This closure will be run on each rule group.
     *
     * @return array
     */
    protected function rulesFromService(string $service, Closure $group_map)
    {
        $registered = service(
             $this->runningModule()->getAlias()
             . ':'
             . $service
        )->read();

        return collect($registered)
             ->groupBy('field')
             ->map($group_map)
             ->toArray()
             ;
    }
}
