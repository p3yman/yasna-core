<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\RegisterController;

/**
 * @api               {GET}
 *                    /api/modular/v1/frontier-have-user
 *                    Have User
 * @apiDescription    Check if a user with given `mobile` exists in the users table
 * @apiVersion        1.0.0
 * @apiName           Have User
 * @apiGroup          Frontier
 * @apiPermission     Guest
 * @apiParam {string} field  the field name, in the users table to be checked in.
 * @apiParam {string} value  the value of which the user is checked against.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      },
 *      "results": {
 *          "hashid" => "sGhLkDs",
 *      }
 * }
 * @method RegisterController controller()
 */
class HaveUserEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Have User";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RegisterController@haveUser';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "hashid" => hashid(rand(1, 100)),
        ], [
        ]);
    }
}
