<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\LoginController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-login
 *                    Login
 * @apiDescription    Login user via API.
 *                    <br>
 *                    This method has one dynamic parameter:
 * - The Username: A field with name of the site's username should be sent to identify the user.
 * @apiVersion        1.0.1
 * @apiName           Login
 * @apiGroup          Frontier
 * @apiPermission     Guest
 * @apiParam {String} username The username of the user (could be anything in nature)
 * @apiParam {String} password The password of the user
 * @apiParam {String} g-recaptcha-response The value of the google recaptcha
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "access_token":
 *           "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb2R1bGFyXC92MVwvZnJvbnRpZXItbG9naW4iLCJpYXQiOjE1NDk4MTAzMDIsImV4cCI6MTU0OTgxMDkwMiwibmJmIjoxNTQ5ODEwMzAyLCJqdGkiOiJ0MEF6Snl5ZDJyNUtRNEQ1Iiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.oHlljpPtAKwOqouy0ADuRtuj_MLmFx9a4VMiT2nEh-o",
 *           "token_type": "bearer",
 *           "expires_in": 600
 *      }
 * }
 * @apiErrorExample   Invalid-Inputs:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frontier.moreInfo.422",
 *      "errors": {
 *      "password": [
 *           [
 *                "The Password field is required."
 *           ]
 *      ]
 *      }
 * }
 * @method  LoginController controller()
 */
class LoginEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Login";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->not_exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'LoginController@login';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'status'   => 200,
             'metadata' =>
                  [
                       'authenticated_user' => 'qKXVA',
                  ],
             'results'  =>
                  [
                       'access_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb2R1bGFyXC92MVwvZnJvbnRpZXItbG9naW4iLCJpYXQiOjE1NDk4MTAzMDIsImV4cCI6MTU0OTgxMDkwMiwibmJmIjoxNTQ5ODEwMzAyLCJqdGkiOiJ0MEF6Snl5ZDJyNUtRNEQ1Iiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.oHlljpPtAKwOqouy0ADuRtuj_MLmFx9a4VMiT2nEh-o',
                       'token_type'   => 'bearer',
                       'expires_in'   => 600,
                  ],
        ]);
    }
}
