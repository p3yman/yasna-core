<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-password-recovery
 *                    Password Reset
 * @apiDescription    Reset user's password.
 *                    <br>
 *                    This method has one dynamic parameter:
 * - The Username: A field with name of the site's username should be sent to identify the user
 * @apiVersion        1.0.0
 * @apiName           Password Reset
 * @apiGroup          Frontier
 * @apiPermission     None
 * @apiParam {String} user_name the User ID based on valid fields in the settings
 * @apiParam {String} password The new password to be set as the user's password
 * @apiParam {String} password2 The confirmation of the `password` field
 * @apiParam {String} token The token sent via email or SMS.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *           "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb",
 *           "token_type": "bearer",
 *           "expires_in": 3600000
 *      }
 * }
 * @apiErrorExample   Bad-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frontier.moreInfo.422",
 *      "errors": {
 *           "password": [
 *                [
 *                     "The Password field is required."
 *                ]
 *           ],
 *           "token": [
 *                [
 *                     "The reset password token is not valid."
 *                ]
 *           ]
 *      }
 * }
 * @method  \Modules\Frontier\Http\Controllers\V1\PasswordResetController controller()
 */
class PasswordRecoveryEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Password Reset";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PasswordResetController@reset';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "access_token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb",
             "token_type"   => "bearer",
             "expires_in"   => 3600000,
        ]);
    }
}
