<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\RoleController;

/**
 * @api               {GET}
 *                    /api/modular/v1/frontier-role-list
 *                    Role List
 * @apiDescription    get role list
 * @apiVersion        1.0.0
 * @apiName           Role List
 * @apiGroup          Frontier
 * @apiPermission     developer
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *              "reached": "RoleListEndpoint",
 *              "authenticated_user": "qKXVA"
 *      },
 *      "results": [
 *          {
 *              "id": 1,
 *               "slug": "superadmin",
 *               "title": "مدیرکل",
 *                "plural_title": "مدیران کل",
 *                "modules": null,
 *               "is_admin": 1,
 *               "meta": [],
 *               "created_at": "2019-02-27 09:56:46",
 *                 "updated_at": null,
 *               "deleted_at": null,
 *               "converted": 0,
 *               "created_by": 0,
 *               "updated_by": 0,
 *               "deleted_by": 0
 *        },
 *        {
 *               "id": 2,
 *               "slug": "manager",
 *               "title": "مدیر",
 *               "plural_title": "مدیران",
 *               "modules": null,
 *               "is_admin": 1,
 *               "meta": [],
 *               "created_at": "2019-02-27 09:56:46",
 *               "updated_at": null,
 *               "deleted_at": null,
 *               "converted": 0,
 *               "created_by": 0,
 *               "updated_by": 0,
 *               "deleted_by": 0
 *        },
 *      ]
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  RoleController controller()
 */
class RoleListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Role List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RoleController@roleList';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done"  => "1",
             "roles" => [
                  [
                       "id"           => 1,
                       "slug"         => "superadmin",
                       "title"        => dummy()::persianName(),
                       "plural_title" => dummy()::persianName(),
                       "modules"      => null,
                       "is_admin"     => 1,
                       "meta"         => [],
                       "created_at"   => now()->toDateTimeString(),
                       "updated_at"   => null,
                       "deleted_at"   => null,
                       "converted"    => 0,
                       "created_by"   => 0,
                       "updated_by"   => 0,
                       "deleted_by"   => 0,
                  ],
                  [
                       "id"           => 2,
                       "slug"         => "manager",
                       "title"        => dummy()::persianName(),
                       "plural_title" => dummy()::persianName(),
                       "modules"      => null,
                       "is_admin"     => 1,
                       "meta"         => [],
                       "created_at"   => now()->toDateTimeString(),
                       "updated_at"   => null,
                       "deleted_at"   => null,
                       "converted"    => 0,
                       "created_by"   => 0,
                       "updated_by"   => 0,
                  ],

             ],
        ], [
             "reached" => "RoleListEndpoint",
        ]);
    }
}
