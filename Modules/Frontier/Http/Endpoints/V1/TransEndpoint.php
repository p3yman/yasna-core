<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\TransController;

/**
 * @api               {GET}
 *                    /api/modular/v1/frontier-trans
 *                    Trans
 * @apiDescription    Get the translations.
 * @apiVersion        1.0.0
 * @apiName           Trans
 * @apiGroup          Frontier
 * @apiPermission     None
 * @apiParam {array} [locales] An array of the locales to be used while fetching trans files. If you post a single
 *           string instead of an array, it will be assumed as an array with one item. This array may include any item
 *           from site locales.
 * @apiParam {array} [modules] An array of the modules to be used while fetching trans files. If you post a single
 *           string instead of an array, it will be assumed as an array with one item. The items in this array may be
 *           from three groups:
 * - The Names of the Active Modules: If you don't include any item from this group, all active modules will be
 * applied.
 * - The `Overall` Word: With using this word, the response will include the overall translations.
 * - The `Dynamic` Word: With using this word, the response will include the dynamic translations (Only if the Trans
 * module is available).
 * @apiParam {array} [phrases] An array of the phrases to be used while fetching trans files. If you post a single
 *           string instead of an array, it will be assumed as an array with one item. If you specify this field, the
 *           modules field will be effectless.
 * @apiSuccessExample Modules-Requested:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "locales": [
 *                "en"
 *           ],
 *           "modules": [
 *                "Endpoint",
 *                "Dynamic"
 *           ],
 *           "phrases": [],
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *           "en": {
 *                "endpoint": {
 *                     "userMessages": {
 *                          "endpoint-403": "Forbidden!",
 *                          "endpoint-404": "Not Found!",
 *                          "endpoint-405": "Bad Method Call!",
 *                          "endpoint-501": "Not Implemented!",
 *                          "endpoint-410": "Gone!",
 *                          "endpoint-600": "Onion Termination!"
 *                     }
 *                },
 *                "dynamic": {
 *                     "word": "Word",
 *                     "computer": "Computer"
 *                }
 *           }
 *      }
 * }
 * @apiSuccessExample Phrases-Requested:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "locales": [
 *                "en"
 *           ],
 *           "modules": [],
 *           "phrases": [
 *                "validation.attributes.title",
 *                "endpoint::userMessages.endpoint-403",
 *                "invalid.validation"
 *           ],
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *           "en": {
 *                "validation.attributes.title": "Title",
 *                "endpoint::userMessages.endpoint-403": "Forbidden!",
 *                "invalid.validation": null
 *           }
 *      }
 * }
 * @apiErrorExample   Not-Implemented:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-501",
 *      "userMessage": "Not Implemented!",
 *      "errorCode": "endpoint-501",
 *      "moreInfo": "endpoint.moreInfo.endpoint-501",
 *      "errors": []
 * }
 * @method  TransController controller()
 */
class TransEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Trans";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'TransController@get';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $modules = boolval(rand(0, 1));

        if ($modules) {
            return $this->getModulesMockData();
        } else {
            return $this->getPhrasesMockData();
        }
    }



    /**
     * Returns a mock data based on the modules.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function getModulesMockData()
    {
        return api()->successRespond([
             'en' => [
                  'endpoint' => [
                       'userMessages' => [
                            'endpoint-403' => 'Forbidden!',
                            'endpoint-404' => 'Not Found!',
                            'endpoint-405' => 'Bad Method Call!',
                            'endpoint-501' => 'Not Implemented!',
                            'endpoint-410' => 'Gone!',
                            'endpoint-600' => 'Onion Termination!',
                       ],
                  ],
                  'dynamic'  => [
                       'word'     => 'Word',
                       'computer' => 'Computer',
                  ],
             ],
        ], [
             'locales' => [
                  'en',
             ],
             'modules' => [
                  'Endpoint',
                  'Dynamic',
             ],
             'phrases' => [],
        ]);
    }



    /**
     * Returns a mock data based on the phrases.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function getPhrasesMockData()
    {
        return api()->successRespond([
             'en' => [
                  'validation.attributes.title'         => 'Title',
                  'endpoint::userMessages.endpoint-403' => 'Forbidden!',
                  'invalid.validation'                  => null,
             ],
        ], [
             'locales' => [
                  'en',
             ],
             'modules' => [],
             'phrases' => [
                  'validation.attributes.title',
                  'endpoint::userMessages.endpoint-403',
                  'invalid.validation',
             ],
        ]);
    }
}
