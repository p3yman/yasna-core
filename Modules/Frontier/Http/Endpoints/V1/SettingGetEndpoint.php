<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/frontier-setting-get
 *                    Get Setting
 * @apiDescription    This endpoint can be used to read site's settings. If the `slug` parameter has been filled, this
 *                    endpoint works like the single of the setting. Otherwise, it works like the list of the settings
 *                    and even can be paginated.
 * @apiVersion        1.0.0
 * @apiName           Get Setting
 * @apiGroup          Frontier
 * @apiPermission     None
 * @apiParam {String} [language] Is the locale to be used while finding the values of the settings in the response.
 * @apiParam {String} [slug] Is the slug of a setting to get its single resource.
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer} [per_page=15] Indicate the number of results on per page.
 *                                  (available only with `paginated=1` and the `slug` has been presented).
 * @apiParam {Integer} [page=1] Page number of returned result
 *                             (available only with `paginated=1` and the `slug` has been presented).
 * @apiSuccessExample Success-List:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "total": 9,
 *           "per_page": 2,
 *           "last_page": 5,
 *           "current_page": 1,
 *           "next_page_url": "http://yasna.local/api/modular/v1/frontier-setting-get?paginated=1&per_page=2&page=2",
 *           "previous_page_url": null,
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": [
 *           {
 *                "id": "ZAQrx",
 *                "slug": "telephone",
 *                "title": "Telephone",
 *                "category": "contact",
 *                "is_localized": 0,
 *                "value": [
 *                     "+98(21)22334455"
 *                ]
 *                "updated_at": 1545545420
 *           },
 *           {
 *                "id": "dNRoA",
 *                "slug": "favicon",
 *                "title": "Favicon",
 *                "category": "template",
 *                "is_localized": 0,
 *                "value": null,
 *                "updated_at": null
 *           }
 *      ]
 * }
 * @apiSuccessExample Success-Single:
 * {
 *      "status": 200,
 *      "metadata": {
 *           "slug": "address",
 *           "language": "en",
 *           "authenticated_user": "PKEnN"
 *      },
 *           "results": {
 *           "id": "pxkYN",
 *           "slug": "address",
 *           "title": "Address",
 *           "category": "contact",
 *           "is_localized": 1,
 *           "value": "Tehran, Iran, Asia",
 *           "updated_at": 1545548193,
 *           "order": 22,
 *           "data_type": "textarea",
 *           "default_value": "Tehran, Iran, Asia",
 *           "css_class": "rtl",
 *           "hint": "The Postal Address"
 *      }
 * }
 * @apiErrorExample   Not-Found:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Not Found!",
 *      "userMessage": "The requested resource could not be found but may be available in the future.",
 *      "errorCode": 404,
 *      "moreInfo": "frontier.moreInfo.404",
 *      "errors": []
 * }
 * @method  \Modules\Frontier\Http\Controllers\V1\SettingController controller()
 */
class SettingGetEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get Setting";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SettingController@get';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $model1 = model('setting')
             ->fill([
                  "slug"         => "setting_slug_1",
                  "title"        => "Setting 1 Title",
                  "category"     => "template",
                  "data_type"    => "text",
                  "custom_value" => "Value of Setting 1",
                  "is_localized" => 0,
                  "updated_at"   => now()->getTimestamp(),
             ]);

        $model2 = model('setting')
             ->fill([
                  "slug"         => "setting_slug_2",
                  "title"        => "Setting 2 Title",
                  "category"     => "contact",
                  "data_type"    => "array",
                  "custom_value" => "Value of Setting 2",
                  "is_localized" => 1,
                  "updated_at"   => now()->getTimestamp(),
             ]);

        return api()->successRespond([
             $model1->toListResource(),
             $model2->toListResource(),
        ], [
             "language" => "en",
             "total"    => 2,
        ]);
    }
}
