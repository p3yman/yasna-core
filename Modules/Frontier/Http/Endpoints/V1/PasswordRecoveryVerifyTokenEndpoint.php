<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-password-recovery-verify-token
 *                    Password Reset Verify Token
 * @apiDescription    Verify a reset password token.
 *                    <br>
 *                    This method has one dynamic parameter:
 * - The Username: A field with name of the site's username should be sent to identify the user
 * @apiVersion        1.0.0
 * @apiName           Password Reset Verify Token
 * @apiGroup          Frontier
 * @apiPermission     token The token sent via email or SMS.
 * @apiPermission     user_name the User ID based on valid fields in the settings
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Bad-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frontier.moreInfo.422",
 *      "errors": {
 *           "token": [
 *                     [
 *                          "The reset password token is not valid."
 *                     ]
 *                ]
 *           }
 * }
 * @method  \Modules\Frontier\Http\Controllers\V1\PasswordResetController controller()
 */
class PasswordRecoveryVerifyTokenEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Password Reset Verify Token";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PasswordResetController@tokenVerify';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ]);
    }
}
