<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-password-recovery-request
 *                    Password Reset Request
 * @apiDescription    Request a password reset token.
 *                    <br>
 *                    This method has two dynamic parameters:
 * - The Username: A field with name of the site's username should be sent to identify the user
 * - The channel value: A field with the name of selected channel should be filled to authorize the user
 *                    user
 * @apiVersion        1.0.0
 * @apiName           Password Reset Request
 * @apiGroup          Frontier
 * @apiPermission     None
 * @apiParam {String}  user_name the User ID based on valid fields in the settings
 * @apiParam {String}  [channel] The channel to send the token.
 *                               <br> The value of this field should be one of the site's reset token channels.
 *                               <br> The default value of this field is the first item of the site's reset token
 *                               channels.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *           "done": "1",
 *           "message": "A reset password token has been sent to someone@somewhere.com."
 *      }
 * }
 * @apiErrorExample   Bad-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frontier.moreInfo.422",
 *      "errors": {
 *           "mobile": [
 *                [
 *                     "The Mobile Number field is required."
 *                ]
 *           ]
 *      }
 * }
 * @method  \Modules\Frontier\Http\Controllers\V1\PasswordResetController controller()
 */
class PasswordRecoveryRequestEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Password Reset Request";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'PasswordResetController@tokenRequest';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done"    => "1",
             "message" => "A reset password token has been sent to someone@somewhere.com.",
        ]);
    }
}
