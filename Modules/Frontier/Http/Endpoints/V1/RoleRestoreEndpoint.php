<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\RoleController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-role-restore
 *                    Role Restore
 * @apiDescription    restore role
 * @apiVersion        1.0.0
 * @apiName           Role Restore
 * @apiGroup          Frontier
 * @apiPermission     developer
 * @apiParam {string} hashid The hashid of role
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "EAVjA",
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  RoleController controller()
 */
class RoleRestoreEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Role Restore";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RoleController@restore';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "reached" => "RoleRestoreEndpoint",
        ]);
    }
}
