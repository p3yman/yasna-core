<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\RoleController;

/**
 * @api               {GET}
 *                    /api/modular/v1/frontier-role-users-count
 *                    Role Users Count
 * @apiDescription    Returns the number of users per role
 * @apiVersion        1.0.0
 * @apiName           Role Users Count
 * @apiGroup          Frontier
 * @apiPermission     developer
 * @apiParam {string}  hashid The hashid of role
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "count": 2
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  RoleController controller()
 */
class RoleUsersCountEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Role Users Count";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RoleController@getCount';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done"  => "1",
             "count" => "6",
        ], [
             "reached" => "RoleUsersCountEndpoint",
        ]);
    }
}
