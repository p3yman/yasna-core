<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\SetupController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-setup
 *                    Setup
 * @apiDescription    Setup the Project
 * @apiVersion        1.0.1
 * @apiName           Setup
 * @apiGroup          Frontier
 * @apiPermission     Developer
 * @apiParam {Array} [locales] An array of the site's locales
 * @apiParam {Array} [default_post_types] An array of default posttypes and their information
 * @apiParam {Array} [pages] An array of the static pages
 * @apiParam {Boolean} [custom_post_types] If the custom post types should be created
 * @apiParam {Array} [post_types] An array of the custom posttypes
 * @apiParam {Boolean} [custom_roles] If the custom roles should be created
 * @apiParam {Array} [roles] An array of the roles
 * @apiParam {Boolean} [custom_file_settings] If the custom file settings should be created
 * @apiParam {Array} [file_settings] An array of the file settings
 * @apiParam {Array} [site_settings] An array of the site settings
 * @apiParam {Array} [settings] An array of the settings
 * @apiParam {Boolean} [shop_dummy] If the shop dummy should be created
 * @apiParam {Array} [shop] The information of the shop dummy.
 * @apiExample {JSON} Example usage:
 * {
 *      "locales": {
 *           "fa": 1,
 *           "en": 1,
 *           "ar": 0
 *      },
 *      "default_post_types": {
 *           "blog": {
 *                "create": 1,
 *                "dummy": 1,
 *                "dummy_counts": 30,
 *                "slug": "blog",
 *                "title": "مطالب وبلاگ",
 *                "singular_title": "مطلب وبلاگ",
 *                "icon": "file-o",
 *                "template": "post",
 *                "locales": [
 *                     "fa",
 *                     "en"
 *                ],
 *                "features": [
 *                     "title2",
 *                     "text",
 *                     "abstract",
 *                     "manage_sidebar",
 *                     "featured_image"
 *                ],
 *                "inputs": [
 *                     "max_per_page"
 *                ],
 *                "refresh": 1
 *           },
 *           "news": {
 *                "create": 1,
 *                "dummy": 1,
 *                "dummy_counts": "20",
 *                "slug": "news",
 *                "title": "اخبار",
 *                "singular_title": "خبر",
 *                "icon": "newspaper",
 *                "template": "post",
 *                "locales": [
 *                     "en"
 *                ],
 *                "features": [
 *                     "text",
 *                     "abstract",
 *                     "manage_sidebar",
 *                     "long_title"
 *                ],
 *                "inputs": [
 *                     "max_per_page"
 *                ]
 *           },
 *           "page": {
 *                "create": 1,
 *                "dummy": 1,
 *                "dummy_counts": 30,
 *                "slug": "page",
 *                "title": "برگه‌های عمومی",
 *                "singular_title": "برگه",
 *                "icon": "file-text",
 *                "template": "post",
 *                "locales": [
 *                     "fa",
 *                     "en"
 *                ],
 *                "features": [
 *                     "text",
 *                     "abstract",
 *                     "manage_sidebar"
 *                ],
 *                "inputs": [
 *                     "max_per_page",
 *                     "fresh_time_duration"
 *                ]
 *           },
 *           "portfolio": {
 *                "create": 0,
 *                "dummy": 1,
 *                "dummy_counts": 30,
 *                "slug": "portfolio",
 *                "title": "نمونه کارها",
 *                "singular_title": "نمونه کار",
 *                "icon": "file-text",
 *                "template": "post",
 *                "locales": null,
 *                "features": [],
 *                "inputs": []
 *           },
 *           "slideshow": {
 *                "create": 1,
 *                "dummy": 1,
 *                "dummy_counts": 30,
 *                "slug": "slideshow",
 *                "title": "اسلایدشوی اصلی",
 *                "singular_title": "اسلاید",
 *                "icon": "file-text",
 *                "template": "slideshow",
 *                "locales": "",
 *                "features": [
 *                     "title2",
 *                     "text",
 *                     "abstract",
 *                     "featured_image",
 *                     "slideshow"
 *                ],
 *                "inputs": [
 *                     "slideshow_auto",
 *                     "slideshow_pause",
 *                     "slideshow_speed",
 *                     "slideshow_timeout",
 *                     "view_slide"
 *                ]
 *           },
 *           "our_team": {
 *                "create": 0,
 *                "dummy": 1,
 *                "dummy_counts": 30,
 *                "slug": "our-team",
 *                "title": "تیم ما",
 *                "singular_title": "عضو تیم",
 *                "icon": "file-text",
 *                "template": "post",
 *                "locales": "",
 *                "features": [
 *                     "title2",
 *                     "text",
 *                     "abstract",
 *                     "manage_sidebar",
 *                     "featured_image",
 *                     "slug"
 *                ],
 *                "inputs": [
 *                     "max_per_page",
 *                     "fresh_time_duration"
 *                ]
 *           },
 *           "projects": {
 *                "create": 0,
 *                "dummy": 1,
 *                "dummy_counts": 30,
 *                "slug": "projects",
 *                "title": "پروژه‌ها",
 *                "singular_title": "پروژه",
 *                "icon": "file-text",
 *                "template": "post",
 *                "locales": "",
 *                "features": [
 *                     "title2",
 *                     "text",
 *                     "abstract",
 *                     "manage_sidebar",
 *                     "featured_image",
 *                     "slug"
 *                ],
 *                "inputs": [
 *                     "max_per_page",
 *                     "fresh_time_duration"
 *                ]
 *           }
 *       },
 *      "pages": {
 *           "about": {
 *                "create": 1,
 *                "dummy": 1
 *           },
 *           "participate": {
 *                "create": 1,
 *                "dummy": 0
 *           },
 *           "how-to-pay": {
 *                "create": 1,
 *                "dummy": 0
 *           }
 *      },
 *      "custom_post_types": "1",
 *      "post_types": [
 *           {
 *                "slug": "help",
 *                "title": "راهنماها",
 *                "singular_title": "راهنما",
 *                "icon": "help",
 *                "template": "postak",
 *                "locales": [
 *                     "fa"
 *                ],
 *                "features": [
 *                     "title2",
 *                     "manage_sidebar"
 *                ],
 *                "inputs": null
 *           }
 *      ],
 *      "custom_roles": 1,
 *      "roles": [
 *           {
 *                "title": "حسابدار",
 *                "plural_title": "حسابداران",
 *                "slug": "accountant",
 *                "is_admin": 0
 *           },
 *           {
 *                "title": "مدیر محتوا",
 *                "plural_title": "مدیران محتوا",
 *                "slug": "content-manager",
 *                "is_admin": 1
 *           }
 *      ],
 *      "custom_file_settings": 1,
 *      "file_settings": [
 *           {
 *                "slug": "cover",
 *                "title": "کاور وبلاگ",
 *                "value": {
 *                     "uploadDir": "default",
 *                     "fileTypes": {
 *                          "image": {
 *                               "status": 1,
 *                               "maxFileSize": 5,
 *                               "maxFiles": 5,
 *                               "extraFiles": [
 *                                    {
 *                                         "title": "default",
 *                                         "width": "300",
 *                                         "height": "400"
 *                                    },
 *                                    {
 *                                         "title": "lg",
 *                                         "width": "800",
 *                                         "height": "1000"
 *                                    }
 *                               ],
 *                               "acceptedExtensions": [
 *                                    "jpg",
 *                                    "jpeg",
 *                                    "png"
 *                               ],
 *                               "acceptedFiles": [
 *                                    "image/jpeg"
 *                               ]
 *                          },
 *                          "video": {
 *                               "status": 1,
 *                               "maxFileSize": 5,
 *                               "maxFiles": 5,
 *                               "acceptedExtensions": [
 *                                    "avi",
 *                                    "mkv"
 *                               ],
 *                               "acceptedFiles": [
 *                                    "video/avi"
 *                               ]
 *                          },
 *                          "audio": {
 *                               "status": 0,
 *                               "maxFileSize": 5,
 *                               "maxFiles": 5,
 *                               "acceptedExtensions": null,
 *                               "acceptedFiles": null
 *                          },
 *                          "text": {
 *                               "status": 0,
 *                               "maxFileSize": 5,
 *                               "maxFiles": 5,
 *                               "acceptedExtensions": null,
 *                               "acceptedFiles": null
 *                          },
 *                          "compressed": {
 *                               "status": 0,
 *                               "maxFileSize": 5,
 *                               "maxFiles": 5,
 *                               "acceptedExtensions": null,
 *                               "acceptedFiles": null
 *                          },
 *                          "icon": {
 *                               "status": 0,
 *                               "maxFileSize": 5,
 *                               "maxFiles": 5,
 *                               "acceptedExtensions": null,
 *                               "acceptedFiles": null
 *                          }
 *                     }
 *                },
 *                "posttypes": [
 *                     "blog",
 *                     "news"
 *                ],
 *                "input_types": [
 *                     "featured_image"
 *                ]
 *           }
 *      ],
 *      "site_settings": {
 *           "site_title": {
 *                "fa": "عنوان فارسی سایت",
 *                "en": "عنوان انگلیسی سایت"
 *           },
 *           "telephone": [
 *                "02133333333",
 *                "02122222222"
 *           ],
 *           "instagram_link": "http://instagram.com/account2"
 *      },
 *      "settings": [
 *           {
 *                "slug": "telephone",
 *                "value": "133242343"
 *           },
 *           {
 *                "slug": "address",
 *                "value": "Number 1, Tehran, Iran, Asia,"
 *           }
 *      ],
 *      "shop_dummy": 1,
 *      "shop": {
 *           "posts": 3,
 *           "wares": 5,
 *           "packages": 5,
 *           "locales": [
 *                "fa",
 *                "en"
 *           ]
 *      }
 * }
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   Invalid-Inputs:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frontier.moreInfo.422",
 *      "errors": {
 *           "settings.1.slug": [
 *                [
 *                     "The selected The Slug of Each of the Settings is invalid."
 *                ]
 *           ]
 *      }
 * }
 * @method  SetupController controller()
 */
class SetupEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Setup";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SetupController@setup';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "authenticated_user" => user()->hashid,
        ]);
    }
}
