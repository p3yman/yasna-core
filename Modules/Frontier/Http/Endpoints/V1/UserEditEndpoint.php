<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-user-edit
 *                    User Edit
 * @apiDescription    Edit the Profile of Self or Another User Under Permission
 * @apiVersion        1.0.0
 * @apiName           User Edit
 * @apiGroup          Frontier
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "PKEnN",
 *           "authenticated_user": "QKgJx"
 *      },
 *           "results": {
 *           "done": "1"
 *      }
 * }
 * @apiErrorExample   Forbidden:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Forbidden Access",
 *      "userMessage": "The action is unauthorized.",
 *      "errorCode": 403,
 *      "moreInfo": "frontier.moreInfo.403",
 *      "errors": []
 * }
 * @apiErrorExample   Bad-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frontier.moreInfo.422",
 *      "errors": {
 *           "email": [
 *                [
 *                     "The email has already been taken."
 *                ]
 *           ]
 *      }
 * }
 * @method  \Modules\Frontier\Http\Controllers\V1\UserEditController controller()
 */
class UserEditEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Edit";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserEditController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
