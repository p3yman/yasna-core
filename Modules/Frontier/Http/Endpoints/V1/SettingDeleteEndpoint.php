<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\SettingController;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/frontier-setting-delete
 *                    Delete and restore settings
 * @apiDescription    Delete and restore settings
 * @apiVersion        1.0.0
 * @apiName           Delete and restore settings
 * @apiGroup          Frontier
 * @apiPermission     Developer
 * @apiParam {String} hashid Hashid of setting
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "xwyWN",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": 1
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  SettingController controller()
 */
class SettingDeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Delete And Restore Settings";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SettingController@delete';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid"             => "xwyWN",
             "authenticated_user" => "qKXVA",
        ]);
    }
}
