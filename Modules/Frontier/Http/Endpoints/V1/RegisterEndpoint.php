<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\RegisterController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-register
 *                    Register form
 * @apiDescription    Register form
 * @apiVersion        1.0.0
 * @apiName           Register form
 * @apiGroup          Frontier
 * @apiPermission     None
 * @apiParam {String} name_first     FirstName of user
 * @apiParam {String} name_last      LastName of user
 * @apiParam {String} email          Email of user
 * @apiParam {String} mobile         Mobile of user
 * @apiParam {int=1,2,3} gender      Gender of user
 * @apiParam {int}    city           City of user
 * @apiParam {String} code_melli     Code melli of user
 * @apiParam {int=1,2} marital       Martial of user
 * @apiParam {String} [password]     Password for user
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "oxljN",
 *          "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample   Bad Request!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  RegisterController controller()
 */
class RegisterEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Register Form";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RegisterController@register';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "hashid"             => "oxljN",
             "authenticated_user" => "PKEnN",
        ], [
             "done" => "1",
        ]);
    }
}
