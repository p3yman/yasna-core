<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\RoleController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-role-editor
 *                    Role Editor
 * @apiDescription    create and edit role
 * @apiVersion        1.0.0
 * @apiName           Role Editor
 * @apiGroup          Frontier
 * @apiPermission     developer
 * @apiParam {string} [hashid] Hashid of transfer for edit
 * @apiParam {string} slug The slug of role
 * @apiParam {string} title The title of role
 * @apiParam {string} plural_title The plural_title of role
 * @apiParam {string} [modules] The modules of role
 * @apiParam {Integer=0,1} [is_admin]
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "BNwWN",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *            "done" => "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  RoleController controller()
 */
class RoleEditorEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Role Editor";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'RoleController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
             "role" => [
                  "slug"         => "role_slug_1",
                  "title"        => "role 1 Title",
                  "plural_title" => dummy()::persianName(),
             ],
        ], [
             "reached" => "RoleEditorEndpoint",
        ]);
    }
}
