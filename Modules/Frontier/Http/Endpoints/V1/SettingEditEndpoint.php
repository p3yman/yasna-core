<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\SettingController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-save-setting
 *                    create and edit settings
 * @apiDescription    create and edit settings
 * @apiVersion        1.0.0
 * @apiName           create and edit settings
 * @apiGroup          Frontier
 * @apiPermission     Developer
 * @apiParam {String}   [hashid]    Hashid of setting
 * @apiParam {String}   [css_class]    Css class for setting
 * @apiParam {String}   [hint]      Hint for setting
 * @apiParam {String}   slug        slug of setting
 * @apiParam {String}   title       title of setting
 * @apiParam {String}   category    category of setting
 * @apiParam {Int}      order       order of setting
 * @apiParam {String}   data_type   data type of setting
 * @apiParam {String}   default_value       default value of setting
 * @apiParam {String}   custom_value        custom value of setting
 * @apiParam {Boolean=0,1}  is_localized    localized of setting
 * @apiParam {Boolean=0,1}  api_discoverable api discoverable of setting
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid" : "qKXVA",
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  SettingController controller()
 */
class SettingEditEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Create And Edit Settings";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isDeveloper();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SettingController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid"             => hashid(1),
             "authenticated_user" => "qKXVA",
        ]);
    }
}
