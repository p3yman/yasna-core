<?php

namespace Modules\Frontier\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frontier\Http\Controllers\V1\SettingController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frontier-setting-custom-value
 *                    set custom value for setting
 * @apiDescription    set custom value for setting
 * @apiVersion        1.0.0
 * @apiName           set custom value for setting
 * @apiGroup          Frontier
 * @apiPermission     None
 * @apiParam {String} [hashid] Hashid of setting . Hashid or slug is required for set custom_value
 * @apiParam {String} [slug] Slug of setting
 * @apiParam {String} custom_value Custom value for setting
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "hashid": "xwyWN",
 *           "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *           "done": 1
 *      }
 * }
 * @apiErrorExample   Access Denied!
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  SettingController controller()
 */
class SettingCustomValueEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Set Custom Value For Setting";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frontier\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SettingController@setCustomValue';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid"             => "xwyWN",
             "authenticated_user" => "qKXVA",
        ]);
    }
}
