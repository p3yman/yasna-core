<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/29/19
 * Time: 3:03 PM
 */

namespace Modules\Frontier\Http\Controllers\V1\Traits;


use App\Models\Setting;
use Illuminate\Support\Arr;
use Modules\Frontier\Http\Requests\V1\SetupRequest;

trait SetupSettingTrait
{
    /**
     * Does the setup part related to the settings.
     *
     * @param SetupRequest $request
     */
    public function doSettingSetup(SetupRequest $request): void
    {
        if (!$request->filled('settings')) {
            return;
        }

        foreach ($request->settings as $setting) {
            $this->saveSettingItem($setting);
        }
    }



    /**
     * Does the setup part related to the site settings.
     *
     * @param SetupRequest $request
     */
    public function doSiteSettingSetup(SetupRequest $request): void
    {
        if (!$request->filled('site_settings')) {
            return;
        }

        foreach ($request->site_settings as $key => $value) {
            $this->saveSiteSettingItem($key, $value);
        }
    }



    /**
     * Does the setup part related to the site locales.
     *
     * @param SetupRequest $request
     */
    public function doLocalesSetup(SetupRequest $request): void
    {
        if (!$request->filled('locales')) {
            return;
        }

        $locales_codes = collect($request->locales)
             ->filter()
             ->keys()
             ->toArray()
        ;

        setting('site_locales')->setValue($locales_codes);
    }



    /**
     * Saves the value of a single item from the settings.
     *
     * @param array $setting
     */
    protected function saveSettingItem(array $setting)
    {
        /** @var Setting $model */
        $model = model('setting', $setting['slug']);

        if ($model->not_exists) {
            return;
        }

        $value = $setting['value'];

        $this->setSettingValue($model, $value);
    }



    /**
     * Saves the value of a single item of the site settings.
     *
     * @param string                    $slug
     * @param array|string|integer|bool $value
     */
    protected function saveSiteSettingItem(string $slug, $value)
    {
        if (empty($value)) {
            return;
        }

        /** @var Setting $model */
        $model = model('setting', $slug);

        if ($model->not_exists) {
            return;
        }

        $this->setSettingValue($model, $value);
    }



    /**
     * Sets the given value on the specified setting based on the value type.
     *
     * @param Setting                   $setting
     * @param array|string|integer|bool $value
     */
    protected function setSettingValue(Setting $setting, $value)
    {
        if ($this->valueIsLocalized($value) and $setting->isLocalized()) {
            $this->setSettingLocalizedValue($setting, $value);
        } else {
            $this->setSettingNotLocalizedValue($setting, $value);
        }
    }



    /**
     * Sets the given values of locales on the specified setting.
     *
     * @param Setting $setting
     * @param array   $value
     */
    protected function setSettingLocalizedValue(Setting $setting, array $value)
    {
        foreach ($value as $locale => $locale_value) {
            $setting->in($locale);
            $this->setSettingNotLocalizedValue($setting, $locale_value);
        }
    }



    /**
     * Sets the given values of locales on the specified setting.
     *
     * @param Setting                   $setting
     * @param array|string|integer|bool $value
     */
    protected function setSettingNotLocalizedValue(Setting $setting, $value)
    {
        $setting->setValue($value);
    }



    /**
     * If the given value is a localized array.
     *
     * @param array|string|integer|bool $value
     *
     * @return bool
     */
    protected function valueIsLocalized($value)
    {
        return (
             is_array($value)
             and
             Arr::isAssoc($value)
        );
    }
}
