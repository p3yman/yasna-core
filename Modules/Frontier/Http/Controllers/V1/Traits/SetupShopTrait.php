<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/2/19
 * Time: 12:59 PM
 */

namespace Modules\Frontier\Http\Controllers\V1\Traits;


use App\Models\Post;
use App\Models\Posttype;
use App\Models\Unit;
use App\Models\Ware;
use Illuminate\Database\Eloquent\Collection;
use Modules\Frontier\Http\Requests\V1\SetupRequest;

trait SetupShopTrait
{
    /** @var Posttype */
    protected $products_posttype;

    /** @var array */
    protected $shop_locales = [];

    /** @var array|Post[] */
    protected $inserted_shop_posts = [];

    /** @var Collection */
    protected $shop_units;

    /** @var Collection */
    protected $shop_packages;

    /** @var Collection|Ware[] */
    protected $inserted_shop_wares;



    /**
     * Does the setup part related to the shop dummy.
     *
     * @param SetupRequest $request
     */
    public function doShopDummySetup(SetupRequest $request): void
    {
        // if the shop module is not ready to use
        if ($this->runningModule()->cannotUse('shop')) {
            return;
        }


        // if the `shop_dummy` field has not been filled and set to true
        if (!$request->shop_dummy) {
            return;
        }

        $switches           = $request->shop;
        $this->shop_locales = $switches['locales'];

        $this->seedShopDummyPackages($switches['packages']);
        $this->seedShopDummyPosts($switches['posts'], $switches['wares']);
    }



    /**
     * Seeds packages for the products posttype.
     *
     * @param int $packages_count
     */
    protected function seedShopDummyPackages(int $packages_count)
    {
        $data = [];

        for ($i = 1; $i <= $packages_count; $i++) {
            $posttype = $this->getProductsPosttype();

            $data[] = [
                 'posttype_id'  => $posttype->id,
                 'package_id'   => 0,
                 'unit_id'      => 0,
                 'sisterhood'   => null,
                 'locales'      => "fa,en",
                 'currencies'   => "IRR",
                 'order'        => rand(1, 50),
                 'is_available' => rand(0, 1),
                 'meta-titles'  => $this->generateFakeWareTitles(),
            ];
        }

        yasna()->seed('wares', $data);
    }



    /**
     * Seeds the shop dummy
     *
     * @param int $posts_count
     * @param int $wares_count
     */
    protected function seedShopDummyPosts(int $posts_count, int $wares_count)
    {
        $posttype           = $this->getProductsPosttype();
        $locales            = $this->shop_locales;
        $fake_posttype_item = [
             'slug'        => $posttype->slug,
             'sample_file' => 'product',
             'features'    => $this->productsPosttypeFeatures(),
        ];


        foreach ($locales as $locale) {
            for ($i = 0; $i < $posts_count; $i++) {

                $this->inserted_shop_posts[] = post()->batchSave(
                     $this->generateDummyArrayForPosttype($fake_posttype_item, $locale)
                );

            }
        }

        $this->seedPostsWares($wares_count);
        $this->seedWaresPrices();
        $this->seedWaresInventories();
    }



    /**
     * Seeds the wares for the given posts.
     *
     * @param int $max_count
     */
    protected function seedPostsWares(int $max_count)
    {
        $data = [];

        foreach ($this->inserted_shop_posts as $post) {
            $data = array_merge($data, $this->postWaresData($post, $max_count));
        }

        yasna()->seed('wares', $data, false);
    }



    /**
     * Returns an array of wares to be seeded for the given post.
     *
     * @param Post $post
     * @param int  $max_count
     *
     * @return array
     */
    protected function postWaresData(Post $post, int $max_count)
    {
        $data = [];

        if ($post->not_exists) {
            return $data;
        }

        $package  = $this->randomShopPackage();
        $posttype = $this->getProductsPosttype();

        $count = rand(0, $max_count);

        for ($i = 0; $i < $count; $i++) {

            $data[] = [
                 'posttype_id'      => $posttype->id,
                 'package_id'       => $package->exists ? $package->id : 0,
                 'unit_id'          => $this->randomShopUnit()->id,
                 'sisterhood'       => $post->sisterhood,
                 'locales'          => implode(',', $this->shop_locales),
                 'order'            => rand(1, $count),
                 'is_available'     => rand(0, 1),
                 'package_capacity' => rand(1, 100),
                 'meta-titles'      => $this->generateFakeWareTitles(),
            ];
        }

        return $data;
    }



    /**
     * Seeds dummy data into prices table.
     */
    private function seedWaresPrices()
    {
        $data = [];

        foreach ($this->insertedShopWares() as $ware) {
            $on_sale        = boolval(rand(0, 1));
            $original_price = rand(1000, 100000);
            $sale_prices    = $on_sale ? rand(1000, $original_price - 1) : $original_price;
            $date           = now()->subDays(rand(0, 12));

            $data[] = [
                 'ware_id'     => $ware->id,
                 'amount'      => $original_price,
                 'currency'    => "IRR",
                 'type'        => "original",
                 'expires_at'  => null,
                 'effected_at' => $date,
                 'created_by'  => user()->id,
                 'created_at'  => $date,
            ];


            $data[] = [
                 'ware_id'     => $ware->id,
                 'amount'      => $sale_prices,
                 'currency'    => "IRR",
                 'type'        => "sale",
                 'expires_at'  => $date->addDays(rand(0, 10)),
                 'effected_at' => $date->addDays(rand(0, 3)),
                 'created_by'  => user()->id,
                 'created_at'  => $date,
            ];
        }

        yasna()->seed('prices', $data);
    }



    /**
     * Seeds dummy data into inventories table
     */
    private function seedWaresInventories()
    {
        foreach ($this->insertedShopWares() as $ware) {
            $ware->setInventory([
                 'alteration' => rand(100, 1000),
                 'fee'        => rand(1000, 100000),
            ]);
        }
    }



    /**
     * Returns an array of the titles for a ware.
     *
     * @return array
     */
    protected function generateFakeWareTitles()
    {
        $titles = [];

        foreach ($this->shop_locales as $locale) {
            $titles[$locale] = $this->fakerFor($locale)->realText(rand(20, 50));
        }

        return $titles;
    }



    /**
     * Returns the products posttype.
     *
     * @return Posttype
     */
    protected function getProductsPosttype()
    {
        if (!$this->products_posttype) {
            $this->products_posttype = $this->findProductsPosttype();
        }

        $this->makeProductPosttypeReady();

        return $this->products_posttype;
    }



    /**
     * Makes the products posttype ready.
     */
    protected function makeProductPosttypeReady()
    {
        $this->products_posttype->attachFeatures($this->productsPosttypeFeatures());
        $this->products_posttype->cacheFeatures();
        $this->products_posttype->attachRequiredInputs();
    }



    /**
     * Finds and returns the products posttype.
     *
     * @return Posttype
     */
    protected function findProductsPosttype()
    {
        /** @var Posttype $existed */
        $existed = model('posttype', $this->productsPosttypeSlug());

        if ($existed->exists) {
            return $existed;
        }

        return $existed->batchSave($this->productsPosttypeData());
    }



    /**
     * Returns the data to create a new products posttype.
     *
     * @return array
     */
    protected function productsPosttypeData()
    {
        $slug   = $this->productsPosttypeSlug();
        $module = $this->runningModule();

        return [
             'slug'           => $slug,
             'title'          => $module->getTrans("setup.shop.posttype.$slug.title"),
             'singular_title' => $module->getTrans("setup.shop.posttype.$slug.singular_title"),
             'icon'           => $module->getConfig("setup.shop.posttype.$slug.icon"),
             'template'       => $module->getConfig("setup.shop.posttype.$slug.template"),
             'locales'        => implode('|', $this->shop_locales),
        ];
    }



    /**
     * Returns an array of the features for the products posttype.
     *
     * @return array
     */
    protected function productsPosttypeFeatures()
    {
        $slug = $this->productsPosttypeSlug();

        return $this->runningModule()->getConfig("setup.shop.posttype.$slug.features");
    }



    /**
     * Returns the slug of the products posttype.
     *
     * @return string
     */
    protected function productsPosttypeSlug()
    {
        return 'products';
    }



    /**
     * Returns a random unit.
     *
     * @return Unit
     */
    protected function randomShopUnit()
    {
        $units = $this->shopUnits();

        return $units->count() ? $units->random() : model('unit');
    }



    /**
     * Returns a random collection of the units.
     *
     * @return Collection
     */
    protected function shopUnits()
    {
        if (!$this->shop_units) {
            $this->shop_units = model('unit')->whereConcept('weight')->inRandomOrder()->get();
        }

        return $this->shop_units;
    }



    /**
     * Returns a random unit.
     *
     * @return Ware
     */
    protected function randomShopPackage()
    {
        $packages = $this->shopPackages();

        return $packages->count() ? $packages->random() : model('ware');
    }



    /**
     * Returns a random collection of the packages.
     *
     * @return Collection
     */
    protected function shopPackages()
    {
        if (!$this->shop_packages) {
            $this->shop_packages = $this->getProductsPosttype()->packages()->limit(10)->get();
        }

        return $this->shop_packages;
    }



    /**
     * Returns the wares of the inserted shop posts.
     *
     * @return Ware[]|Collection
     */
    protected function insertedShopWares()
    {
        if (!$this->inserted_shop_wares) {
            $inserted_posts_sisterhood = collect($this->inserted_shop_posts)
                 ->pluck('sisterhood')
                 ->toArray()
            ;

            $this->inserted_shop_wares = model('ware')
                 ->whereIn('sisterhood', $inserted_posts_sisterhood)
                 ->get()
            ;
        }

        return $this->inserted_shop_wares;
    }
}
