<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/30/19
 * Time: 3:12 PM
 */

namespace Modules\Frontier\Http\Controllers\V1\Traits;


use Modules\Frontier\Http\Requests\V1\SetupRequest;

trait SetupRolesTrait
{
    /**
     * Does the setup part related to the roles.
     *
     * @param SetupRequest $request
     */
    public function doRolesSetup(SetupRequest $request): void
    {
        if (!$request->custom_roles or !$request->filled('roles')) {
            return;
        }

        foreach ($request->roles as $role) {
            $model = role($role['slug']);

            $data = array_only($role, ['title', 'plural_title', 'slug', 'is_admin']);

            $model->batchSave($data);
        }
    }
}
