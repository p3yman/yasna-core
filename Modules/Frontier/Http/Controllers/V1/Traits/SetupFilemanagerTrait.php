<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/29/19
 * Time: 12:34 PM
 */

namespace Modules\Frontier\Http\Controllers\V1\Traits;


use App\Models\Posttype;
use App\Models\Setting;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File as FileFacades;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;
use Modules\Filemanager\Services\Tools\VirtualUploader\Uploader;
use Modules\Frontier\Http\Requests\V1\SetupRequest;
use Symfony\Component\HttpFoundation\File\File;

trait SetupFilemanagerTrait
{
    /**
     * @var array|Collection[]
     */
    protected $sample_files_directories = [];

    /**
     * @var array|Posttype[]
     */
    protected $posttypes_with_upload_configs = [];



    /**
     * Does the setup part related to the filemanager module.
     *
     * @param SetupRequest $request
     */
    public function doFilemanagerSetup(SetupRequest $request): void
    {
        // if the filemanager module is not ready to use
        if ($this->runningModule()->cannotUse('filemanager')) {
            return;
        }

        // if the `upload_configs` field has not been filled
        if (!$request->custom_file_settings or !$request->filled('file_settings')) {
            return;
        }

        foreach ($request->file_settings as $config_item) {
            $model = $this->filemanagerSaveConfigItem($config_item);
            $this->filemanagerSetConfigItemForPosttypes($model, $config_item);
        }
    }



    /**
     * Saves a single filemanager upload config.
     *
     * @param array $config
     *
     * @return Setting
     */
    protected function filemanagerSaveConfigItem(array $config)
    {
        $config_slug = $config['slug'];
        $slug        = $this->filemanagerConfigGenerateSlug($config_slug);
        $setting     = model('setting', $slug);

        if ($setting->exists) {
            return $setting;
        }

        return model('setting')
             ->batchSave([
                  'slug'          => $slug,
                  'title'         => $config['title'],
                  'default_value' => json_encode($this->correctUploadConfigValue($config['value'])),
                  'category'      => 'invisible',
                  'data_type'     => 'textarea',
             ]);
    }



    /**
     * Corrects the value of an upload config.
     *
     * @param array $value
     *
     * @return array
     */
    protected function correctUploadConfigValue(array $value)
    {
        $file_types = (isset($value['fileTypes']) and is_array($value['fileTypes']))
             ? $value['fileTypes']
             : [];

        foreach ($file_types as $file_type_key => $file_type) {
            // correct status
            $status                                       = boolval($file_type['status'] ?? false);
            $value['fileTypes'][$file_type_key]['status'] = $status;

            // correct extra files
            $extra_files = (isset($file_type['extraFiles']) and is_array($file_type['extraFiles']))
                 ? $file_type['extraFiles']
                 : [];


            $value['fileTypes'][$file_type_key]['extraFiles'] = $this->correctUploadConfigExtraFiles($extra_files);
        }

        return $value;
    }



    /**
     * Corrects the extra files of a file type in the value of an upload config.
     *
     * @param array $extra_files
     *
     * @return array
     */
    protected function correctUploadConfigExtraFiles(array $extra_files)
    {
        $new_extra_files = [];

        foreach ($extra_files as $extra_file) {
            if (!is_array($extra_file)) {
                continue;
            }

            $title = $extra_file['title'];
            $value = array_except($extra_file, ['title']);

            $new_extra_files[$title] = $value;

        }

        return $new_extra_files;
    }



    /**
     * Sets the given config item for the needed posttypes.
     *
     * @param Setting $saved_config
     * @param array   $config_item
     */
    protected function filemanagerSetConfigItemForPosttypes(Setting $saved_config, array $config_item)
    {
        $posttypes_slugs = ($config_item['post_types'] ?? []);
        $input_types     = ($config_item['input_types'] ?? []);

        if (empty($posttypes_slugs) or empty($input_types)) {
            return;
        }

        foreach ($posttypes_slugs as $posttype_slug) {
            $posttype = $this->posttypeForFilemanagerConfig($posttype_slug);

            if ($posttype->not_exists) {
                continue;
            }

            $this->filemanagerSetInputsForPosttype($posttype, $saved_config, $input_types);
        }
    }



    /**
     * Returns a posttype with the given slug.
     *
     * @param string $slug
     *
     * @return Posttype
     */
    protected function posttypeForFilemanagerConfig(string $slug)
    {
        if (!array_key_exists($slug, $this->posttypes_with_upload_configs)) {
            $this->posttypes_with_upload_configs[$slug] = model('posttype', $slug);
        }

        return $this->posttypes_with_upload_configs[$slug];
    }



    /**
     * Fills the given inputs of the given posttypes with the given config.
     *
     * @param Posttype $posttype
     * @param Setting  $config
     * @param array    $inputs
     */
    protected function filemanagerSetInputsForPosttype(Posttype $posttype, Setting $config, array $inputs)
    {
        $data = [];
        foreach ($inputs as $input) {
            $slug = $input . '_' . ConfigsServiceProvider::getUploadRelatedInputsPostfix();

            if ($posttype->isNotMeta($slug)) {
                continue;
            }

            $data[$slug] = $config->id;
        }

        $posttype->batchSave($data);
    }



    /**
     * Generates the slug to be saved in the `settings` table based on the given config slug.
     *
     * @param string $config_slug
     *
     * @return string
     */
    protected function filemanagerConfigGenerateSlug(string $config_slug)
    {
        $postfix = '_' . ConfigsServiceProvider::getUploadRelatedInputsPostfix();
        $base    = snake_case(camel_case($config_slug));


        if (ends_with($base, $postfix)) {
            return $postfix;
        }

        return $base . $postfix;
    }



    /**
     * Returns the featured image part of the dummy data for the given posttype item and in the given locale.
     *
     * @param array  $posttype_item
     * @param string $locale
     *
     * @return array
     */
    protected function postDummyFeaturedImageData(array $posttype_item, string $locale): array
    {
        // if the filemanager module is not ready to use
        if ($this->runningModule()->cannotUse('filemanager')) {
            return [];
        }

        $posttype = $this->posttypeForFilemanagerConfig($posttype_item['slug']);

        // if the posttype doesn't need featured image
        if ($posttype->hasnot('featured_image') and $posttype->hasnot('slideshow')) {
            return [];
        }

        return [
             'featured_image' => $this->uploadFeaturedImageForPosttypeItem($posttype_item),
        ];
    }



    /**
     * Returns the features image for the dummies of the given posttype item.
     *
     * @param array $posttype_item
     *
     * @return string|null
     */
    protected function uploadFeaturedImageForPosttypeItem(array $posttype_item)
    {
        $posttype     = $this->posttypeForFilemanagerConfig($posttype_item['slug']);
        $files_groups = ($posttype_item['sample_file'] ?? 'content');
        $file         = $this->generateFakeUploadedFile($files_groups);
        $uploader     = new Uploader($file);
        $uploader->setPosttypeConfig($posttype, 'featured_image');

        return $uploader->upload() ?: null;
    }



    /**
     * Returns the attachments part of the dummy data for the given posttype item and in the given locale.
     *
     * @param array  $posttype_item
     * @param string $locale
     *
     * @return array
     */
    protected function postDummyAttachmentsData(array $posttype_item, string $locale): array
    {
        // if the filemanager module is not ready to use
        if ($this->runningModule()->cannotUse('filemanager')) {
            return [];
        }

        $posttype = $this->posttypeForFilemanagerConfig($posttype_item['slug']);

        // if the posttype doesn't need attachments
        if ($posttype->hasnot('attachments')) {
            return [];
        }

        return [
             'meta-post_files' => $this->generateAttachmentsForPosttypeItem($posttype_item),
        ];
    }



    /**
     * Returns the featured image part of the dummy data for the given page item and in the given locale.
     *
     * @param array  $page_item
     * @param string $locale
     *
     * @return array
     */
    protected function pageDummyFeaturedImageData(array $page_item, string $locale): array
    {
        // if the filemanager module is not ready to use
        if ($this->runningModule()->cannotUse('filemanager')) {
            return [];
        }


        $posttype      = $this->posttypeForFilemanagerConfig($this->getPagePosttypeSlug());
        $posttype_item = $this->pages_posttype_item;

        // if the posttype doesn't need featured image
        if ($posttype->hasnot('featured_image')) {
            return [];
        }

        return [
             'featured_image' => $this->uploadFeaturedImageForPosttypeItem($posttype_item),
        ];
    }



    /**
     * Returns the array of the attachments for a dummy posts based on the given posttype item.
     *
     * @param array $posttype_item
     *
     * @return array
     */
    protected function generateAttachmentsForPosttypeItem(array $posttype_item)
    {
        $data         = [];
        $count        = rand(1, 5);
        $posttype     = $this->posttypeForFilemanagerConfig($posttype_item['slug']);
        $files_groups = ($posttype['sample_file'] ?? 'content');

        for ($i = 0; $i < $count; $i++) {
            $file     = $this->generateFakeUploadedFile($files_groups);
            $uploader = new Uploader($file);
            $uploader->setPosttypeConfig($posttype, 'featured_image');

            if ($uploader->isNotValid()) {
                continue;
            }

            $data[] = [
                 'src'   => $uploader->upload(),
                 'label' => '',
                 'link'  => '',
            ];
        }

        return $data;
    }



    /**
     * Generates and returns a new uploaded file from the files in the specified group.
     *
     * @param string $group
     *
     * @return UploadedFile
     */
    protected function generateFakeUploadedFile(string $group)
    {
        $file = $this->generateNewTempFile($group);
        $path = $file->getPathname();
        $name = $file->getBaseName();
        $type = $file->getMimeType();

        return app()->make(UploadedFile::class, [
             'path'         => $path,
             'originalName' => time() . $name,
             'mimeType'     => $type,
             'test'         => true,
        ]);
    }



    /**
     * Generates a new temp file to simulate the upload process from the files in the given group.
     *
     * @param string $group
     *
     * @return File
     */
    protected function generateNewTempFile(string $group)
    {
        $file_pathname = $this->randomSampleFileForGroup($group);
        $file          = new File($file_pathname);
        $temp          = tempnam(sys_get_temp_dir(), '')
             . '.'
             . $file->getExtension();

        FileFacades::copy($file->getPathname(), $temp);

        return new File($temp);
    }



    /**
     * Returns the pathname to a random sample file in the given group.
     *
     * @param string $group
     *
     * @return string
     */
    protected function randomSampleFileForGroup(string $group)
    {
        return $this->allGroupSampleFiles($group)->random();
    }



    /**
     * Returns a collection of the sample files for the given group. Each item is a full pathname.
     *
     * @param string $group
     *
     * @return Collection
     */
    protected function allGroupSampleFiles(string $group)
    {
        if (!array_key_exists($group, $this->sample_files_directories)) {
            $this->sample_files_directories[$group] = $this->readGroupSampleFiles($group);
        }

        return $this->sample_files_directories[$group];
    }



    /**
     * Reads and returns a collection of the sample files for the given group. Each item is a full pathname.
     *
     * @param string $group
     *
     * @return Collection
     */
    protected function readGroupSampleFiles(string $group)
    {
        $directory = $this->sampleFileDirectory($group);

        return collect(scandir($directory))
             ->filter(function ($file_name) {
                 return !in_array($file_name, ['.', '..']);
             })->map(function (string $file_name) use ($directory) {
                 return $directory . DIRECTORY_SEPARATOR . $file_name;
             })
             ;
    }



    /**
     * Returns the path to the directory of the sample files for the given group.
     *
     * @param string $group
     *
     * @return string
     */
    protected function sampleFileDirectory(string $group)
    {
        $parts   = $this->sampleFilesDirectoryParts();
        $parts[] = $group;

        return $this->runningModule()->getPath(implode(DIRECTORY_SEPARATOR, $parts));
    }



    /**
     * Returns the part of the path to the sample files' directory.
     *
     * @return array
     */
    protected function sampleFilesDirectoryParts()
    {
        return [
             'Assets',
             'images',
             'posts-samples',
        ];
    }
}
