<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/30/19
 * Time: 4:03 PM
 */

namespace Modules\Frontier\Http\Controllers\V1\Traits;


use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Support\Facades\DB;
use Modules\Frontier\Http\Requests\V1\SetupRequest;

trait SetupPagesTrait
{
    /**
     * @var Posttype
     */
    protected $page_posttype;

    /**
     * @var array
     */
    protected $pages_sisterhoods = [];

    /** @var array */
    protected $pages_posttype_item = [];



    /**
     * Does the setup part related to the default pages.
     *
     * @param SetupRequest $request
     */
    public function finalizePagesSetup(SetupRequest $request): void
    {
        // if the posts module is not ready to use
        if ($this->runningModule()->cannotUse('posts')) {
            return;
        }

        // if the `pages` field has not been filled
        if (!$request->filled('pages')) {
            return;
        }

        // if the page posttype has not been created
        if ($this->pagePosttypeIsNotReady()) {
            return;
        }

        $this->pages_posttype_item = ($request->default_post_types[$this->getPagePosttypeSlug()] ?? []);
        $pages                     = $request->pages;
        $seeding_data              = [];

        foreach ($pages as $page) {
            $seeding_data = array_merge($seeding_data, $this->generatePageSeedingData($page));
        }

        $normalized_data = $this->normalizePagesSeedingData($seeding_data);

        DB::table('posts')->insert($normalized_data);
    }



    /**
     * Normalizes the given seeding data to make sure all the items have same columns.
     *
     * @param array $data
     *
     * @return array
     */
    protected function normalizePagesSeedingData(array $data)
    {
        $keys = array_map('array_keys', $data);
        if (empty($keys)) {
            return [];
        }

        $all_column           = array_unique(array_merge(...$keys));
        $normalizing_template = array_fill_keys($all_column, null);

        foreach ($data as $key => $datum) {
            $data[$key] = array_normalize($datum, $normalizing_template);
        }

        return $data;
    }



    /**
     * If the page posttype is ready.
     *
     * @return bool
     */
    protected function pagePosttypeIsReady()
    {
        return $this->getPagePosttype()->exists;
    }



    /**
     * If the page posttype is not ready.
     *
     * @return bool
     */
    protected function pagePosttypeIsNotReady()
    {
        return !$this->pagePosttypeIsReady();
    }



    /**
     * Returns the page posttype model.
     *
     * @return Posttype
     */
    protected function getPagePosttype()
    {
        if (!$this->page_posttype) {
            // if the posttype has been created in the same request, the value of the `posttype()` helper does not help.
            // so a fresh instance of the Posttype model is being used.
            $this->page_posttype = model('posttype', $this->getPagePosttypeSlug());
        }

        return $this->page_posttype;
    }



    /**
     * Returns the slug of the page posttype.
     *
     * @return string
     */
    protected function getPagePosttypeSlug()
    {
        return 'page';
    }



    /**
     * Generates the seeding data for a page.
     *
     * @param array $page_item
     *
     * @return array
     */
    protected function generatePageSeedingData(array $page_item)
    {
        $create      = ($page_item['create'] ?? false);
        $seeder_data = [];

        if (!$create) {
            return $seeder_data;
        }

        $posttype = $this->getPagePosttype();
        $locales  = $posttype->localesArray();


        foreach ($locales as $locale) {
            $page_locale_data = $this->generatePageSeedingDataForLocale($page_item, $locale);

            if (empty($page_locale_data)) {
                continue;
            }

            $seeder_data[] = $page_locale_data;
        }

        return $seeder_data;
    }



    /**
     * Generates the seeding data for the given page item in the given locale.
     *
     * @param array  $page_item
     * @param string $locale
     *
     * @return array
     */
    protected function generatePageSeedingDataForLocale(array $page_item, string $locale)
    {
        $dummy_methods = $this->pageDummyArrayMethods();
        $result        = [];

        foreach ($dummy_methods as $method) {
            $result = array_merge($result, $this->$method($page_item, $locale));
        }

        return $result;
    }



    /**
     * Returns an array of the methods to be called while generating the dummy array for a page.
     *
     * @return array
     */
    protected function pageDummyArrayMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 return (starts_with($method, 'pageDummy') and ends_with($method, 'Data'));
             })
             ->values()
             ->toArray()
             ;
    }



    /**
     * Returns the basic part of the dummy data for the given page item and in the given locale.
     *
     * @param array  $page_item
     * @param string $locale
     *
     * @return array
     */
    protected function pageDummyBasicData(array $page_item, string $locale)
    {
        $slug  = $page_item['slug'];
        $dummy = ($page_item['dummy'] ?? false);

        if ($this->pageExisted($slug, $locale)) {
            return [];
        }

        $faker = $this->fakerFor($locale);

        $seeding_item = [
             'type'         => $this->getPagePosttypeSlug(),
             'title'        => $this->runningModule()->getTrans("setup.posts.pages-titles.$slug", [], $locale),
             'is_draft'     => 0,
             'created_by'   => user()->id,
             'created_at'   => $now = now()->toDateTimeString(),
             'updated_by'   => user()->id,
             'updated_at'   => $now,
             'published_by' => user()->id,
             'published_at' => $now,
             'sisterhood'   => $this->pageSisterhood($slug),
             'locale'       => $locale,
             'text'         => '',
             'slug'         => $slug,
        ];


        if ($dummy) {
            $seeding_item['title2']     = $faker->realText(rand(50, 150));
            $seeding_item['long_title'] = $faker->realText(rand(200, 500));
            $seeding_item['abstract']   = $faker->realText(rand(1000, 2000));
            $seeding_item['text']       = $faker->realText(rand(5000, 10000));
        }

        return $seeding_item;
    }



    /**
     * Returns the sisterhood for a page with the given slug.
     *
     * @param string $slug
     *
     * @return string
     */
    protected function pageSisterhood(string $slug)
    {
        if (!array_key_exists($slug, $this->pages_sisterhoods)) {
            $this->pages_sisterhoods[$slug] = $this->guessPageSisterhood($slug);
        }

        return $this->pages_sisterhoods[$slug];
    }



    /**
     * Guesses the sisterhood for a page with the given slug.
     * <br>
     * _This method checks if a page with the given slug exists, returns it sisterhood. Otherwise, returns a new
     * sisterhood_
     *
     * @param string $slug
     *
     * @return string
     */
    protected function guessPageSisterhood(string $slug)
    {
        $any_existed = post()->firstOrNew([
             'slug' => $slug,
             'type' => $this->getPagePosttypeSlug(),
        ]);

        return $any_existed->sisterhood ?? $this->newPageSisterhood();
    }



    /**
     * Returns a new sisterhood value.
     *
     * @return string
     */
    protected function newPageSisterhood()
    {
        return hashid(rand(1000, 100000), 'main');
    }



    /**
     * If a page with the given slug and locale exists.
     *
     * @param string $slug
     * @param string $locale
     *
     * @return bool
     */
    protected function pageExisted(string $slug, string $locale)
    {
        /** @var Post $existed_post */
        $existed_post = post()->firstOrNew([
             'type'   => $this->getPagePosttypeSlug(),
             'locale' => $locale,
             'slug'   => $slug,
        ]);

        return $existed_post->exists;
    }
}
