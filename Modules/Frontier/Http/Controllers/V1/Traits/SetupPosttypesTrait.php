<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 1/29/19
 * Time: 5:52 PM
 */

namespace Modules\Frontier\Http\Controllers\V1\Traits;


use Modules\Frontier\Http\Requests\V1\SetupRequest;
use Modules\Posts\Services\PosttypeSeedingTool;

trait SetupPosttypesTrait
{
    /** @var $dummy_posttypes array */
    protected $dummy_posttypes = [];



    /**
     * Does the setup part related to the default posttypes.
     *
     * @param SetupRequest $request
     */
    public function doDefaultPosttypesSetup(SetupRequest $request): void
    {
        // if the posts module is not ready to use
        if ($this->runningModule()->cannotUse('posts')) {
            return;
        }

        // if the `default_post_types` field has not been filled
        if (!$request->filled('default_post_types')) {
            return;
        }

        $posttypes_array      = $request->default_post_types;
        $actionable_posttypes = $this->getActionablePosttypes($posttypes_array);
        $this->createPosttypes($actionable_posttypes);
    }



    /**
     * Does the setup part related to the custom posttypes.
     *
     * @param SetupRequest $request
     */
    public function doCustomPosttypesSetup(SetupRequest $request): void
    {
        // if the posts module is not ready to use
        if ($this->runningModule()->cannotUse('posts')) {
            return;
        }


        // if the `custom_post_types` is false or the `post_types` field has not been filled
        if (!$request->custom_post_types or !$request->filled('post_types')) {
            return;
        }

        $posttypes_array = $request->post_types;
        $this->createPosttypes($posttypes_array);
    }



    /**
     * Finalizes the setup process about the post dummies
     *
     * @param SetupRequest $request
     */
    public function finalizePosttypesDummiesSetup(SetupRequest $request): void
    {
        $dummies = [];

        foreach ($this->dummy_posttypes as $posttype_item) {
            $dummies = array_merge($dummies, $this->generateDummyDataForPosttypeItem($posttype_item));
        }

        if (empty($dummies)) {
            return;
        }

        yasna()->seed('posts', $dummies);
    }



    /**
     * Deletes the refreshing posttypes and theirs posts.
     *
     * @param array $posttypes_array
     */
    protected function deleteRefreshingPosttypes(array $posttypes_array)
    {
        $slugs = $this->findRefreshingPosttypesSlugs($posttypes_array);

        foreach ($slugs as $slug) {
            // delete posttype
            model('posttype')->whereSlug($slug)->forceDelete();

            // delete posts
            model('post')->elector(['type' => $slug])->forceDelete();
        }
    }



    /**
     * Returns an array of the posttype which should be refreshed based on the given posttypes array.
     *
     * @param array $posttypes_array
     *
     * @return array
     */
    protected function findRefreshingPosttypesSlugs(array $posttypes_array)
    {
        return collect($posttypes_array)
             ->map(function (array $posttype_item) {
                 $refresh = boolval($posttype_item['refresh'] ?? 0);

                 if ($refresh) {
                     return $posttype_item['slug'];
                 }

                 return null;
             })
             ->filter()
             ->values()
             ;
    }



    /**
     * Creates the posttypes in the given array.
     *
     * @param array $posttypes_array
     */
    protected function createPosttypes(array $posttypes_array)
    {
        $this->deleteRefreshingPosttypes($posttypes_array);

        $seeding_data = $this->generatePosttypesSeedingData($posttypes_array);
        $seeder       = new PosttypeSeedingTool();
        $seeder->seedPosttypes($seeding_data);

        $this->reservePosttypesForDummy($posttypes_array);
    }



    /**
     * Filters the given posttypes array to the posttypes which should be actioned.
     *
     * @param array $posttypes_array
     *
     * @return array
     */
    protected function getActionablePosttypes(array $posttypes_array)
    {
        return collect($posttypes_array)
             ->filter(function (array $posttype_item) {
                 $to_action = boolval($posttype_item['create'] ?? 0);

                 return $to_action;
             })
             ->toArray()
             ;
    }



    /**
     * Generate the seeding data based on the given posttypes array.
     *
     * @param array $posttypes_array
     *
     * @return array
     */
    protected function generatePosttypesSeedingData(array $posttypes_array)
    {
        return collect($posttypes_array)
             ->map(function (array $posttype_item) {
                 return $this->generatePosttypeItemSeedingData($posttype_item);
             })->toArray()
             ;
    }



    /**
     * Generate the seeding data based on the given posttype item.
     *
     * @param array $posttype_item
     *
     * @return array
     */
    protected function generatePosttypeItemSeedingData(array $posttype_item)
    {
        $posttype            = array_only($posttype_item, [
             'slug',
             'title',
             'singular_title',
             'icon',
             'template',
        ]);
        $posttype['locales'] = $this->correctPosttypeItemLocale($posttype_item);
        $features            = ($posttype_item['features'] ?? []);
        $inputs              = ($posttype_item['inputs'] ?? []);

        return compact('posttype', 'features', 'inputs');
    }



    /**
     * Returns the value of the `locales` column in the `posttypes` table based on the given posttype item.
     *
     * @param array $posttype_item
     *
     * @return string
     */
    protected function correctPosttypeItemLocale(array $posttype_item)
    {
        $locales = ($posttype_item['locales'] ?? []);

        if (is_array($locales)) {
            $locales = implode(',', $locales);
        }

        if (is_string($locales)) {
            return $locales;
        }

        return "";
    }



    /**
     * Reserves the given posttypes array to generate dummies in the finalizing step of the setup process.
     *
     * @param array $posttypes_array
     */
    protected function reservePosttypesForDummy(array $posttypes_array)
    {
        $this->dummy_posttypes = array_merge($this->dummy_posttypes, $posttypes_array);
    }



    /**
     * Generates dummy data for the given posttype item
     *
     * @param array $posttype_item
     *
     * @return array
     */
    protected function generateDummyDataForPosttypeItem(array $posttype_item)
    {
        $dummy_data   = [];
        $dummy        = ($posttype_item['dummy'] ?? false);
        $dummy_counts = ($posttype_item['dummy_counts'] ?? 0);

        if (!$dummy or !$dummy_counts) {
            return $dummy_data;
        }

        $locales = $this->guessLocalesForDummy($posttype_item);

        foreach ($locales as $locale) {

            for ($i = 0; $i < $dummy_counts; $i++) {
                $dummy_data[] = array_merge(
                     $this->generateDummyArrayForPosttype($posttype_item, $locale),
                     compact('locale')
                );
            }

        }

        return $dummy_data;
    }



    /**
     * Generates a new dummy array for the given posttype item.
     * <br>
     * _This array will work like a post item_
     *
     * @param array  $posttype_item
     * @param string $locale
     *
     * @return array
     */
    protected function generateDummyArrayForPosttype(array $posttype_item, string $locale)
    {
        $dummy_methods = $this->postDummyArrayMethods();
        $result        = [];

        foreach ($dummy_methods as $method) {
            $result = array_merge($result, $this->$method($posttype_item, $locale));
        }

        return $result;
    }



    /**
     * Returns an array of the methods to be called while generating the dummy array for a post.
     *
     * @return array
     */
    protected function postDummyArrayMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 return (starts_with($method, 'postDummy') and ends_with($method, 'Data'));
             })
             ->values()
             ->toArray()
             ;
    }



    /**
     * Returns the basic part of the dummy data for the given posttype item and in the given locale.
     *
     * @param array  $posttype_item
     * @param string $locale
     *
     * @return array
     */
    protected function postDummyBasicData(array $posttype_item, string $locale): array
    {
        $faker = $this->fakerFor($locale);
        $title = $faker->realText(rand(20, 50));

        return [
             'type'         => $posttype_item['slug'],
             'title'        => $title,
             'title2'       => $faker->realText(rand(50, 150)),
             'long_title'   => $faker->realText(rand(200, 500)),
             'abstract'     => $faker->realText(rand(1000, 2000)),
             'text'         => $faker->realText(rand(5000, 10000)),
             'slug'         => str_slug($title),
             'is_draft'     => 0,
             'created_by'   => user()->id,
             'created_at'   => $now = now()->toDateTimeString(),
             'updated_by'   => user()->id,
             'updated_at'   => $now,
             'published_by' => user()->id,
             'published_at' => $now,
             'sisterhood'   => hashid(rand(1000, 100000), 'main'),
        ];
    }



    /**
     * Guesses the locales array for creating dummies.
     *
     * @param array $posttype_item
     *
     * @return array
     */
    protected function guessLocalesForDummy(array $posttype_item)
    {
        $locales_array = ($posttype_item['locales'] ?? null);

        if (!$locales_array or !is_array($locales_array)) {
            $locales_array = [config('app.locale')];
        }

        return $locales_array;
    }
}
