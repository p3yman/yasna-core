<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 2/2/19
 * Time: 4:10 PM
 */

namespace Modules\Frontier\Http\Controllers\V1\Traits;


use Faker\Factory;
use Faker\Generator;

trait SetupFakerTrait
{
    /**
     * @var array
     */
    protected $fakers = [];



    /**
     * Generates and returns a new faker instance based on the given locale.
     *
     * @param string $locale
     *
     * @return Generator
     */
    protected function fakerFor(string $locale)
    {
        if (!array_key_exists($locale, $this->fakers)) {

            $faker_locale = $this->guessFakerLocale($locale);

            $this->fakers[$locale] = Factory::create($faker_locale);
        }

        return $this->fakers[$locale];
    }



    /**
     * Guesses the faker locale based on the given locale.
     *
     * @param string $locale
     *
     * @return string
     */
    protected function guessFakerLocale(string $locale)
    {
        $map = $this->getLocalesMap();
        return ($map[$locale] ?? $map[config('app.locale')]);
    }



    /**
     * Returns a map from the locales to the faker locales.
     *
     * @return array
     */
    protected function getLocalesMap()
    {
        return $this->runningModule()->getConfig('setup.faker.locales');
    }
}
