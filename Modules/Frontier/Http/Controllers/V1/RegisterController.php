<?php

namespace Modules\Frontier\Http\Controllers\V1;

use Illuminate\Foundation\Auth\RedirectsUsers;
use Modules\Frontier\Http\Requests\V1\HaveUserRequest;
use Modules\Frontier\Http\Requests\V1\RegisterRequest;
use Modules\Yasna\Services\YasnaApiController;

class RegisterController extends YasnaApiController
{
    use RedirectsUsers;



    /**
     * register users
     *
     * @param RegisterRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function register(RegisterRequest $request)
    {
        $roles = $this->roleSettingInfo();

        $data  = $this->generateSaveData($request);
        $model = $request->getSafeModel()->batchSave($data);

        foreach ($roles as $role) {
            $model->attachRole($role);
        }

        return $this->modelSaveFeedback($model);
    }



    /**
     * check if a user with specific [field] (which could be anything) exists in the database.
     *
     * @param HaveUserRequest $request
     *
     * @return array
     */
    public function haveUser(HaveUserRequest $request)
    {
        if (user()->hasnotField($request->field)) {
            return $this->success(["hashid" => null], $request->commonMetaResponse());
        }

        $first = user()->where($request->field, $request->value)->first();

        if (!$first) {
            return $this->success(["hashid" => null], $request->commonMetaResponse());
        }

        return $this->success([
             "hashid" => $first->hashid,
        ],
             $request->commonMetaResponse()
        );

    }



    /**
     * Reads role setting from the DB
     *
     * @return array
     */
    protected function roleSettingInfo()
    {
        $result = get_setting('default_user_role');

        return is_null($result) ? [] : $result;
    }



    /**
     * Generates the data to be saved.
     *
     * @param RegisterRequest $request
     *
     * @return array
     */
    protected function generateSaveData(RegisterRequest $request)
    {
        $data = $request->toArray();

        return $data;
    }
}
