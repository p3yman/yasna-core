<?php

namespace Modules\Frontier\Http\Controllers\V1;

use App\Models\Setting;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Frontier\Http\Requests\V1\DeleteSettingRequest;
use Modules\Frontier\Http\Requests\V1\SaveSettingRequest;
use Modules\Frontier\Http\Requests\V1\SettingCustomValueRequest;
use Modules\Frontier\Http\Requests\V1\SettingRestoreRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class SettingController extends YasnaApiController
{
    /**
     * The Request In Use.
     *
     * @var SimpleYasnaRequest
     */
    protected $request;



    /**
     * Returns resource(s) of setting(s) based on the request.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function get(SimpleYasnaRequest $request)
    {
        $this->request = $request;

        $builder = $this->generateBuilder();

        if ($this->singleRequested()) {
            return $this->single($builder);
        } else {
            return $this->list($builder);
        }
    }



    /**
     * Generates the builder of the settings.
     *
     * @return Builder
     */
    protected function generateBuilder()
    {
        $elector_array = $this->getElectorArray();

        return model('setting')
             ->elector($elector_array)
             ->orderBy('order')
             ;
    }



    /**
     * Returns the array to be used in the elector.
     *
     * @return array
     */
    protected function getElectorArray()
    {
        return [
             'for-api' => 1,
        ];
    }



    /**
     * Whether a single item has been requested.
     *
     * @return bool
     */
    protected function singleRequested()
    {
        return boolval($this->request->slug);
    }



    /**
     * Returns the response if a single item has been requested.
     *
     * @param Builder $builder
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function single(Builder $builder)
    {
        $slug  = $this->request->slug;
        $model = $builder->grabSlug($slug);


        if ($model->not_exists) {
            return $this->clientError(404);
        }

        return $this->success($this->toResource($model), $this->automaticMetaData());
    }



    /**
     * Returns the response if no single item has been requested.
     *
     * @param Builder $builder
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function list(Builder $builder)
    {
        if ($this->request->isPaginated()) {
            return $this->listPaginated($builder);
        }

        return $this->listFlat($builder);
    }



    /**
     * List by Pagination
     *
     * @param Builder $builder
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listPaginated(Builder $builder)
    {
        $request        = $this->request;
        $paginator      = $builder->paginate($request->perPage());
        $result         = $this->mappedArray($paginator);
        $automatic_meta = $this->automaticMetaData();
        $paginator_meta = $this->paginatorMetadata($paginator);
        $meta           = array_merge($automatic_meta, $paginator_meta);

        return $this->success($result, $meta);
    }



    /**
     * List All Together
     *
     * @param Builder $builder
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function listFlat(Builder $builder)
    {
        $result        = $builder->get();
        $meta          = $this->automaticMetaData();
        $meta['total'] = $result->count();

        $data = $this->mappedArray($result);

        return $this->success($data, $meta);
    }



    /**
     * Returns the mapped array based on the specified collection of items.
     *
     * @param Collection|LengthAwarePaginator $collection
     *
     * @return array
     */
    protected function mappedArray($collection)
    {
        return $collection->map(function (Setting $setting) {
            return $this->toResource($setting);
        })->toArray()
             ;
    }



    /**
     * Returns the resource of the specified model based on the request.
     * <br>
     * - If any single item has been requested, the returning value will be the single resource.
     * - If no single item has been requested, the returning value will be the list resource.
     *
     * @param Setting $model
     *
     * @return array
     */
    protected function toResource(Setting $model)
    {
        $model->in($this->applicableLocale());

        if ($this->singleRequested()) {
            return $model->toSingleResource();
        } else {
            return $model->toListResource();
        }
    }



    /**
     * Returns the applicable locale.
     * <br>
     * The returning locale will be the `language` field of the request or the site locale.
     *
     * @return string
     */
    protected function applicableLocale()
    {
        return (
        $this->request->language
             ?: getLocale()
        );
    }



    /**
     * Returns the automatic meta data based on the request.
     *
     * @return array
     */
    protected function automaticMetaData()
    {
        return $this
             ->request
             ->only('slug', 'language');
    }



    /**
     * create and edit settings
     *
     * @param SaveSettingRequest $request
     *
     * @return array
     */
    public function save(SaveSettingRequest $request)
    {
        $result = $request->model->batchSave($request);

        return $this->modelSaveFeedback($result);
    }



    /**
     * delete settings
     *
     * @param DeleteSettingRequest $request
     *
     * @return array
     */
    public function delete(DeleteSettingRequest $request)
    {
        $deleted = $request->model->delete();

        return $this->typicalSaveFeedback($deleted, [
             "hashid" => $request->hashid,
        ]);
    }



    /**
     * restore settings
     *
     * @param SettingRestoreRequest $request
     *
     * @return array
     */
    public function restore(SettingRestoreRequest $request)
    {
        $restore = $request->model->restore();

        return $this->typicalSaveFeedback($restore, [
             "hashid" => $request->hashid,
        ]);
    }



    /**
     * set custom value for setting
     *
     * @param SettingCustomValueRequest $request
     *
     * @return array
     */
    public function setCustomValue(SettingCustomValueRequest $request)
    {
        $result = $request->model->batchSave($request);

        return $this->modelSaveFeedback($result);
    }
}
