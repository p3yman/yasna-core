<?php

namespace Modules\Frontier\Http\Controllers\V1;

use App\Models\Trans;
use Illuminate\Database\Eloquent\Collection;
use Modules\Frontier\Http\Requests\V1\TransRequest;
use Modules\Yasna\Services\YasnaApiController;

class TransController extends YasnaApiController
{
    /**
     * Returns the trans based on the given request.
     *
     * @param TransRequest $request
     *
     * @return array
     */
    public function get(TransRequest $request)
    {
        return $request->requestingPhrases()
             ? $this->getResponseForPhrases($request)
             : $this->getResponseForBasic($request);
    }



    /**
     * Returns the response if phrase are being requested.
     *
     * @param TransRequest $request
     *
     * @return array
     */
    protected function getResponseForPhrases(TransRequest $request)
    {
        $this->disableFallbackLocale();

        $locales = $request->guessLocales();
        $phrases = $request->phrases;

        $results  = $this->getTranslationsOfPhrases($phrases, $locales);
        $metadata = $this->normalizeMetadata(compact('phrases', 'locales'));

        return $this->success($results, $metadata);
    }



    /**
     * Disables the fallback locale.
     * <br>
     * _The fallback locale could make problems while checking the existence of translation phrases._
     */
    protected function disableFallbackLocale()
    {
        config(['app.fallback_locale' => false]);
    }



    /**
     * Returns the translations of the given phrases in the given locales.
     *
     * @param array $phrases
     * @param array $locales
     *
     * @return array
     */
    protected function getTranslationsOfPhrases(array $phrases, array $locales)
    {
        $result = [];

        foreach ($locales as $locale) {
            $result[$locale] = $this->getTranslationsOfPhrasesInLocale($phrases, $locale);
        }

        return $result;
    }



    /**
     * Returns the translations of the given phrases in the given locales.
     *
     * @param array  $phrases
     * @param string $locale
     *
     * @return array
     */
    protected function getTranslationsOfPhrasesInLocale(array $phrases, string $locale)
    {
        $result = [];

        foreach ($phrases as $phrase) {
            $result[$phrase] = trans()->has($phrase, $locale)
                 ? trans($phrase, [], $locale)
                 : null;
        }

        return $result;
    }



    /**
     * Returns the response for the basic mode of the endpoint (when no specific phrase(s) requested).
     *
     * @param TransRequest $request
     *
     * @return array
     */
    protected function getResponseForBasic(TransRequest $request)
    {
        $modules = $request->guessModules();
        $locales = $request->guessLocales();

        $static  = $this->getStaticTranslations($modules, $locales, $request->withOveralls());
        $dynamic = $request->withDynamics() ? $this->getDynamicTranslations($locales) : [];

        $result   = array_merge_recursive($static, $dynamic);
        $metadata = $this->normalizeMetadata([
             'modules' => $this->generateModulesArrayForMetadata($modules, $request),
             'locales' => $locales,
        ]);

        return $this->success($result, $metadata);
    }



    /**
     * Generates and returns the list of modules to be shown in the metadata.
     *
     * @param array        $modules
     * @param TransRequest $request
     *
     * @return array
     */
    protected function generateModulesArrayForMetadata(array $modules, TransRequest $request)
    {
        $result = collect($modules);

        if ($request->withOveralls()) {
            $result->prepend('Overall');
        }

        if ($request->withDynamics()) {
            $result->push('Dynamic');
        }

        return $result->toArray();
    }



    /**
     * Returns the static translations of the given modules in the given locales.
     *
     * @param array $modules
     * @param array $locales
     * @param bool  $with_overalls
     *
     * @return array
     */
    protected function getStaticTranslations(array $modules, array $locales, bool $with_overalls)
    {
        return trans_array($modules, $locales, $with_overalls);
    }



    /**
     * Returns an array of the dynamic translations.
     *
     * @param array $locales
     *
     * @return array
     */
    protected function getDynamicTranslations(array $locales)
    {
        if ($this->runningModule()->cannotUse('Trans')) {
            return [];
        }

        /** @var Collection $trans_records */
        $trans_records = model('trans')->whereIn('locale', $locales)->get();

        return $trans_records
             // group the records based on their locales.
             ->groupBy('locale')
             // map the groups of records to a formatted translation array
             ->map(function (Collection $locale_records) {
                 return [
                      'dynamic' => $this->formatDynamicTranslations($locale_records),
                 ];
             })
             ->toArray()
             ;
    }



    /**
     * Returns a formatted array of the dynamic translations based on the given collection.
     *
     * @param Collection|Trans[] $records
     *
     * @return array
     */
    protected function formatDynamicTranslations(Collection $records)
    {
        $parts = $records
             ->map(function (Trans $trans) {
                 return [
                      $trans->slug => $trans->value,
                 ];
             })
             ->toArray()
        ;

        return array_merge(...$parts);
    }



    /**
     * Normalizes and returns the metadata.
     *
     * @param array $data
     *
     * @return array
     */
    protected function normalizeMetadata(array $data)
    {
        return array_normalize($data, [
             'locales' => [],
             'modules' => [],
             'phrases' => [],
        ]);
    }
}
