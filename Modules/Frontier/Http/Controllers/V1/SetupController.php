<?php

namespace Modules\Frontier\Http\Controllers\V1;

use Exception;
use Modules\Frontier\Http\Controllers\V1\Traits\SetupFakerTrait;
use Modules\Frontier\Http\Controllers\V1\Traits\SetupFilemanagerTrait;
use Modules\Frontier\Http\Controllers\V1\Traits\SetupPagesTrait;
use Modules\Frontier\Http\Controllers\V1\Traits\SetupPosttypesTrait;
use Modules\Frontier\Http\Controllers\V1\Traits\SetupRolesTrait;
use Modules\Frontier\Http\Controllers\V1\Traits\SetupSettingTrait;
use Modules\Frontier\Http\Controllers\V1\Traits\SetupShopTrait;
use Modules\Frontier\Http\Requests\V1\SetupRequest;
use Modules\Yasna\Services\YasnaApiController;

class SetupController extends YasnaApiController
{
    use SetupFakerTrait;
    use SetupSettingTrait;
    use SetupPosttypesTrait;
    use SetupFilemanagerTrait;
    use SetupRolesTrait;
    use SetupPagesTrait;
    use SetupShopTrait;



    /**
     * Do the setup process.
     *
     * @param SetupRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function setup(SetupRequest $request)
    {
        $this->doSetupProcess($request);
        $this->finalizeSetupProcess($request);

        return $this->success(["done" => "1"]);
    }



    /**
     * Does the setup process.
     *
     * @param SetupRequest $request
     */
    protected function doSetupProcess(SetupRequest $request)
    {
        $setup_methods = $this->setupProcessMethods();

        foreach ($setup_methods as $method) {
            $this->$method($request);
        }
    }



    /**
     * Returns an array of the methods to be called while the setup process.
     *
     * @return array
     */
    protected function setupProcessMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 return (starts_with($method, 'do') and ends_with($method, 'Setup'));
             })
             ->values()
             ->toArray()
             ;
    }



    /**
     * Finalizes the setup process.
     *
     * @param SetupRequest $request
     */
    protected function finalizeSetupProcess(SetupRequest $request)
    {
        $setup_methods = $this->setupFinalizeMethods();

        foreach ($setup_methods as $method) {
            $this->$method($request);
        }
    }



    /**
     * Returns an array of the methods to be called while finalizing the setup process.
     *
     * @return array
     */
    protected function setupFinalizeMethods()
    {
        return collect(get_class_methods($this))
             ->filter(function (string $method) {
                 return (starts_with($method, 'finalize') and ends_with($method, 'Setup'));
             })
             ->values()
             ->toArray()
             ;
    }
}
