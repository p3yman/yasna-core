<?php

namespace Modules\Frontier\Http\Controllers\V1;

use Modules\Frontier\Http\Requests\V1\LoginRequest;
use Modules\Yasna\Services\YasnaApiController;

class LoginController extends YasnaApiController
{
    /**
     * Attempts to login a user with the given credentials.
     *
     * @param LoginRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function login(LoginRequest $request)
    {
        return $this->success([
             "access_token" => $request->token,
             "token_type"   => "bearer",
             "expires_in"   => apiAuth()->factory()->getTTL() * 60,
        ]);
    }
}
