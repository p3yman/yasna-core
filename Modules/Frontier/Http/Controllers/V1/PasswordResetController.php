<?php

namespace Modules\Frontier\Http\Controllers\V1;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\Frontier\Http\Requests\V1\PasswordResetRequest;
use Modules\Frontier\Http\Requests\V1\PasswordResetTokenRequest;
use Modules\Frontier\Http\Requests\V1\PasswordResetTokenVerifyRequest;
use Modules\Yasna\Services\YasnaApiController;

class PasswordResetController extends YasnaApiController
{
    /**
     * Generates a password reset token and send it to user.
     *
     * @param PasswordResetTokenRequest $request
     *
     * @return array
     */
    public function tokenRequest(PasswordResetTokenRequest $request)
    {
        $user = $request->model;

        // save and send token
        frontier()->passwordReset()->sendUserToken($user, $request->channel);

        // return response
        return $this->success([
             'done'    => 1,
             'message' => $this->runningModule()->getTrans('password-reset.token-sent', [
                  'channel' => $this->runningModule()->getTrans('password-reset.' . $request->channel),
             ]),
        ]);
    }



    /**
     * Verifies a reset password token.
     *
     * @param PasswordResetTokenVerifyRequest $request
     *
     * @return array
     */
    public function tokenVerify(PasswordResetTokenVerifyRequest $request)
    {
        return $this->success([
             'done' => 1,
        ]);
    }



    /**
     * Resets the user's password.
     *
     * @param PasswordResetRequest $request
     *
     * @return array
     */
    public function reset(PasswordResetRequest $request)
    {
        $user     = $request->model;
        $password = $request->password;

        $user->batchSave([
             'password'               => Hash::make($password),
             'reset_token'            => null,
             'reset_token_expires_at' => null,
        ]);

        auth()->login($user);

        return $this->success($this->regenerateTokenForNewPassword($user, $password));
    }



    /**
     * Generates a new token after saving the new password.
     *
     * @param User   $user
     * @param string $password
     *
     * @return array|null
     */
    private function regenerateTokenForNewPassword(User $user, string $password): ?array
    {
        $token = auth('api')->attempt([
             $user->usernameField() => $user->username,
             "password"             => $password,
        ]);

        if (!$token) {
            return null;
        }

        $token_info = [
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ];

        return $token_info;
    }
}
