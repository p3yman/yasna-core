<?php

namespace Modules\Frontier\Http\Controllers\V1;

use Illuminate\Support\Facades\Hash;
use Modules\Frontier\Http\Requests\V1\UserEditRequest;
use Modules\Yasna\Services\YasnaApiController;

class UserEditController extends YasnaApiController
{
    /**
     * Saves a edit request of a user.
     *
     * @param UserEditRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(UserEditRequest $request)
    {
        $data  = $this->generateSaveData($request);
        $model = $request->getSafeModel()->batchSave($data, ['new_password', 'new_password2']);

        return $this->modelSaveFeedback($model);
    }



    /**
     * Generates the data to be saved.
     *
     * @param UserEditRequest $request
     *
     * @return array
     */
    protected function generateSaveData(UserEditRequest $request)
    {
        $data = $request->toArray();

        if (isset($data['new_password'])) {
            $data['password'] = Hash::make($data['new_password']);
        }

        return $data;
    }
}
