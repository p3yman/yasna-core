<?php

namespace Modules\Frontier\Http\Controllers\V1;

use Modules\Frontier\Http\Requests\V1\RoleDeleteRequest;
use Modules\Frontier\Http\Requests\V1\RoleSaveRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class RoleController extends YasnaApiController
{
    /**
     * The Request In Use.
     *
     * @var SimpleYasnaRequest
     */
    protected $request;



    /**
     * get role users count
     *
     * @param SimpleYasnaRequest $request
     *
     * @return array
     */
    public function getCount(SimpleYasnaRequest $request)
    {
        $count = role($request->hashid)->users()->count();

        return $this->success(compact('count'));
    }



    /**
     * create or edit role.
     *
     * @param RoleSaveRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function save(RoleSaveRequest $request)
    {
        $saved = $request->model->batchSave($request);

        return $this->modelSaveFeedback($saved);
    }



    /**
     * delete roles
     *
     * @param RoleDeleteRequest $request
     *
     * @return array
     */
    public function delete(RoleDeleteRequest $request)
    {
        $done = role($request->hashid)->delete();
        $done = intval($done);

        return $this->success(compact('done'));
    }



    /**
     * return list of role
     *
     * @return array
     */
    public function roleList()
    {
        return role()->get();
    }



    /**
     * restore role
     *
     * @param RoleDeleteRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function restore(RoleDeleteRequest $request)
    {

        $restore = $request->model->restore();

        return $this->typicalSaveFeedback($restore, [
             "hashid" => $request->hashid,
        ]);

    }


}
