<?php

namespace Modules\Frontier\Http\Controllers;

use App\Models\Posttype;
use Carbon\Carbon;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Providers\DummyServiceProvider;
use Modules\Yasna\Services\YasnaController;

class PosttypeGeneratorController extends YasnaController
{
    private $request;



    /**
     * PosttypeGeneratorController constructor.
     */
    public function __construct()
    {
        $this->middleware('is:dev');
    }


    /**
     * show ui of posttype generator
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUI()
    {
        $view_data['locales']   = setting('site_locales')->gain();
        $view_data['posttypes'] = $this->getListOfPosttypes();
        return view("frontier::setup", $view_data);
    }



    /**
     * save posttypes and return response
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function savePosttypes(SimpleYasnaRequest $request)
    {
        $this->request = $request;
        $normals       = $this->makeArrayForNormalPosttypes();
        $static        = $this->makeArrayForStaticPosttype();
        $all_array     = array_merge($normals, $static);
        $result        = $this->seedPosttype($all_array);
        $message_arr   = $this->makeMessage($result);
        $is_saved      = true;

        if ($message_arr['state'] == "success") {
            $array_result['success_message'] = $message_arr['message'];
        } else {
            $array_result['danger_message'] = $message_arr['message'];
            $is_saved                       = false;
        }
        $array_result['success_modalClose'] = '0';

        return $this->jsonAjaxSaveFeedback($is_saved, $array_result);
    }



    /**
     * make success message
     *
     * @param array $result
     *
     * @return array
     */
    private function makeMessage(array $result): array
    {
        $created = [];
        $skipped = [];

        if (isset($result['skipped'])) {
            $skipped = array_map(function ($item) {
                return $this->getListOfPosttypes()[$item]['posttype']['title'];
            }, $result['skipped']);
        }

        if (isset($result['created'])) {
            $created = array_map(function ($item) {
                return $this->getListOfPosttypes()[$item]['posttype']['title'];
            }, $result['created']);
        }

        return $this->createUserMessage($created, $skipped);

    }



    /**
     * create user side message
     *
     * @param array $created
     * @param array $skipped
     *
     * @return array
     */
    private function createUserMessage(array $created, array $skipped)
    {
        $created_message = "";
        $skipped_message = "";

        if (empty($skipped) and empty($created)) {
            return ["message" => trans("frontier::posttype.no-select-posttype"), "state" => "danger"];
        }

        if (!empty($created)) {
            $created_message = trans('frontier::posttype.created-posttype') . " : " . implode(trans('frontier::posttype.separator') . " ",
                      $created) . "<br>";
        }

        if (!empty($skipped)) {
            $skipped_message = trans('frontier::posttype.exist-posttype') . " : " . implode(trans('frontier::posttype.separator') . " ",
                      $skipped);
        }

        return ["message" => $created_message . $skipped_message, "state" => "success"];
    }



    /**
     *  generate seed array for normal posttype
     *
     * @return array
     */
    private function makeArrayForNormalPosttypes()
    {
        $posttypes = $this->getListOfPosttypes();
        //generate correct array to make posttype
        $result_array = [];
        foreach ($posttypes as $slug => $data) {
            if ($slug == 'static') {
                continue;
            }
            if ($this->request->get($slug) == '1') {
                $result_array[$slug]                        = $posttypes[$slug];
                $result_array[$slug]['features']            = $this->getPosttypeFeature($result_array[$slug]['default-features'],
                     $this->request->get("$slug-features"));
                $result_array[$slug]['with-dummy']          = (bool)$this->request->get("$slug-dummy");
                $result_array[$slug]['posttype']['locales'] = $this->request->get("$slug-locales");
            }
        }
        return $result_array;
    }



    /**
     * generate seed array for static posttype
     *
     * @return array
     */
    private function makeArrayForStaticPosttype()
    {
        $posttype = $this->getListOfPosttypes()['static'];
        if ($this->request->get('static') == '0') {
            return [];
        }
        $posttype['posttype']['locales'] = $this->request->get('static-locales');
        foreach ($posttype['default-posts'] as $post => $info) {
            if ($this->request->get('static-posts')[$post] == '0') {
                unset($posttype['default-posts'][$post]);
            } else {
                if ($this->request->get("$post-dummy") == '1') {
                    $posttype['default-posts'][$post]['with-dummy'] = true;
                }
            }

        }
        return ['static' => $posttype];
    }



    /**
     * find selected features plus require features of posttype
     *
     * @param array $default_features
     * @param array $user_features
     *
     * @return array
     */
    private function getPosttypeFeature(array $default_features, array $user_features): array
    {
        $res_array = [];
        foreach ($default_features as $feature => $state) {
            if ($state == 'required') {
                $res_array[] = $feature;
            }
        }
        foreach ($user_features as $feature => $state) {
            if ($state == '1') {
                $res_array[] = $feature;
            }
        }
        return $res_array;
    }



    /**
     * seed selected posttype and seed their dummy
     *
     * @param array $types
     *
     * @return array
     */
    private function seedPosttype(array $types)
    {
        $seed_log = [];
        foreach ($types as $slug => $item) {
            if (posttype($slug)->exists) {
                $seed_log['skipped'][] = $slug;
                continue;
            }
            yasna()->seed('posttypes', [$item['posttype']]);
            $posttype = posttype($slug);
            $posttype->attachFeatures($item['features']);
            $posttype->attachRequiredInputs();
            $posttype->cacheFeatures();
            if ($item['with-dummy'] and !$item['is-static']) {
                $this->createPosts(30, $posttype, true);
            }
            if (!empty($item['default-posts']) and $item['is-static']) {
                $this->seedDefaultPosts($item['default-posts'], $posttype);
            }
            $seed_log['created'][] = $slug;
        }
        return $seed_log;
    }



    /**
     * return array of posttypes
     *
     * @return array
     */
    private function getListOfPosttypes(): array
    {
        return [
             "news"   => [
                  "posttype"         => [
                       'slug'           => 'news',
                       'order'          => '16',
                       'title'          => trans('frontier::posttype.news-title'),
                       'singular_title' => trans('frontier::posttype.news-title-single'),
                       'template'       => 'special',
                       'icon'           => 'newspaper-o',
                       'locales'        => 'fa,en',
                  ],
                  "default-features" => [
                       "manage_sidebar" => 'optional',
                       "abstract"       => 'optional',
                       "text"           => 'required',
                  ],
                  "default-posts"    => [],
                  'is-static'        => false,
                  "with-dummy"       => false,
             ],
             "static" => [
                  "posttype"      => [
                       'slug'           => 'static',
                       'order'          => '16',
                       'title'          => trans('frontier::posttype.static-title'),
                       'singular_title' => trans('frontier::posttype.static-title-single'),
                       'template'       => 'special',
                       'icon'           => 'newspaper-o',
                       'locales'        => 'fa,en',
                  ],
                  "features"      => [
                       "manage_sidebar",
                       "abstract",
                       "text",
                  ],
                  "default-posts" => [
                       'about-us' => [
                            'label'      => trans('frontier::posttype.default-posts.about-us'),
                            'with-dummy' => false,
                       ],
                  ],
                  'is-static'     => true,
                  "with-dummy"    => false,
             ],
        ];
    }



    /**
     * return array of default posts
     *
     * @return array
     */
    private function getListOfDefaultPost(): array
    {
        return [
             "about-us" => [
                  'slug'         => "about-us",
                  'type'         => "",
                  'title'        => DummyServiceProvider::persianTitle(),
                  'title2'       => rand(1, 5) > 4 ? DummyServiceProvider::persianTitle() : '',
                  'locale'       => "",
                  'sisterhood'   => hashid(rand(5000, 500000)),
                  //'abstract'     => DummyServiceProvider::persianWord(rand(10, 50)),
                  'text'         => "",
                  'published_at' => Carbon::now()->toDateTimeString(),
                  'published_by' => model('user')::inRandomOrder()->first()->id,
                  'created_by'   => model('user')::inRandomOrder()->first()->id,
                  'owned_by'     => model('user')::inRandomOrder()->first()->id,
                  'moderated_by' => "",
                  'moderated_at' => Carbon::now()->toDateTimeString(),
             ],
        ];
    }



    /**
     * make dummy data for specific posttype
     *
     * @param int      $count
     * @param Posttype $posttype
     * @param bool     $truncate
     */
    public function createPosts($count, $posttype, $truncate = true)
    {
        if ($truncate) {
            model('post')->where('type', $posttype->slug)->forceDelete();
        }

        $counter = 0;
        while ($counter < $count) {
            $counter++;

            $deleted   = boolval(rand(1, 20) > 18);
            $published = boolval(rand(1, 20) > 10);
            $type      = $posttype;

            model('post')->batchSave([
                 'slug'         => str_slug("dummy-$type->slug-" . strtolower(DummyServiceProvider::englishWord())),
                 'type'         => $type->slug,
                 'title'        => DummyServiceProvider::persianTitle(),
                 'title2'       => rand(1, 5) > 4 ? DummyServiceProvider::persianTitle() : '',
                 'locale'       => array_random($type->locales_array),
                 'sisterhood'   => hashid(rand(5000, 500000)),
                 'abstract'     => DummyServiceProvider::persianWord(rand(10, 50)),
                 'text'         => DummyServiceProvider::persianText(rand(1, 30)),
                 'deleted_at'   => $deleted ? Carbon::now()->toDateTimeString() : null,
                 'deleted_by'   => $deleted ? model('user')::inRandomOrder()->first()->id : 0,
                 'published_at' => $published ? Carbon::now()->toDateTimeString() : null,
                 'published_by' => $published ? model('user')::inRandomOrder()->first()->id : 0,
                 'created_by'   => model('user')::inRandomOrder()->first()->id,
                 'owned_by'     => model('user')::inRandomOrder()->first()->id,
                 'moderated_by' => $published ? model('user')::inRandomOrder()->first()->id : rand(0, 1),
                 'moderated_at' => Carbon::now()->toDateTimeString(),
            ]);
        }
    }



    /**
     * seed default posts for a posttype
     *
     * @param array    $posts
     * @param Posttype $posttype
     */
    private function seedDefaultPosts($posts, $posttype)
    {

        model('post')->where('type', $posttype->slug)->forceDelete();

        foreach ($posts as $slug => $info) {
            $default_posts = $this->getListOfDefaultPost();
            foreach ($posttype->locales_array as $lang) {
                $default_posts[$slug]['locale'] = $lang;
                $default_posts[$slug]['type']   = $posttype->slug;
                if ($info['with-dummy']) {
                    if ($lang == 'fa') {
                        $default_posts[$slug]['text']  = DummyServiceProvider::persianText(rand(1, 30));
                        $default_posts[$slug]['title'] = DummyServiceProvider::persianText(rand(1, 10));
                    } else {
                        $default_posts[$slug]['text']  = DummyServiceProvider::englishText(rand(1, 30));
                        $default_posts[$slug]['title'] = DummyServiceProvider::englishText(rand(1, 10));
                    }
                }
                model('post')->batchSave($default_posts[$slug]);
            }
        }
    }

}
