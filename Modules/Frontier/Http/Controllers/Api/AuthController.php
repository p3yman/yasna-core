<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 10/2/18
 * Time: 12:47 PM
 */

namespace Modules\Frontier\Http\Controllers\Api;


use Illuminate\Support\Facades\Hash;
use Modules\Frontier\Http\Requests\ApiRegisterRequest;
use Modules\Yasna\Events\LoginFailed;
use Modules\Yasna\Events\NewUserRegistered;
use Modules\Yasna\Events\UserLoggedIn;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaApiController;

class AuthController extends YasnaApiController
{
    /**
     * The Login Action for API.
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function login(SimpleYasnaRequest $request)
    {
        $credentials = $request->only([user()->usernameField(), 'password']);

        if (!$token = apiAuth()->attempt($credentials)) {
            event(new LoginFailed());
            return $this->clientError("40001");
        }

        event(new UserLoggedIn(apiAuth()->user()));

        return $this->respondWithToken($token);
    }



    /**
     * Registers a user.
     *
     * @param ApiRegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(ApiRegisterRequest $request)
    {
        $saving_data = $request->toArray();

        if (array_key_exists('password', $saving_data)) {
            $saving_data['password'] = Hash::make($saving_data['password']);
        }

        $saved_user = model('user')->batchSave($saving_data, ['password2']);
        $token      = apiAuth()->fromUser($saved_user);

        event(new NewUserRegistered($saved_user));

        return $this->respondWithToken($token);
    }



    /**
     * Responds with the specified token.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken(string $token)
    {
        return response()->json($this->success([
             'access_token' => $token,
             'token_type'   => 'bearer',
             'expires_in'   => apiAuth()->factory()->getTTL() * 60,
        ]));
    }

}
