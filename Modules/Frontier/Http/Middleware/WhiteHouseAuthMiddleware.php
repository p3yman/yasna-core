<?php

namespace Modules\Frontier\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class WhiteHouseAuthMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $this->authenticate($request);
        } catch (HttpException $exception) {
            return response()->json(api()->inModule('frontier')->serverErrorRespond("1000"));
        }


        return $next($request);
    }
}
