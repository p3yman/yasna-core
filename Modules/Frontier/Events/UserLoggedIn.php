<?php

namespace Modules\Frontier\Events;

use Modules\Yasna\Services\YasnaEvent;

class UserLoggedIn extends YasnaEvent
{
    public $model_name = 'user';
}
