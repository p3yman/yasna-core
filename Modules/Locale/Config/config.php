<?php

$config = [
     'name' => 'Locale',

     'site_locales'        => ['fa'],
     'ip_stack_access_key' => env('IP_STACK_ACCESS_KEY', null),
];

include('countries.php');

return $config;
