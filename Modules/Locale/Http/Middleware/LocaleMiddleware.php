<?php

namespace Modules\Locale\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Locale\Services\IpReader;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = request()->route()->parameter('lang');
        $availableLocales = availableLocales();

        if ($lang) { // There is a `lang` parameter in url
            if (in_array($lang, $availableLocales)) { // Specified `lang` is one of site available locales
                app()->setLocale($lang);
            } else { // Specified `lang` is't one of site available locales
                if (preg_match('/^_(.)*$/', $lang)) {
                    // If $lang starts with underscore do nothing (Ex:_debugger)
                    return $next($request);
                } else {
                    return redirect('/');
                }
            }
        } else { // There is't a `lang` parameter in url
            $autoDetection = setting('auto_detect_language')->gain();
            if (!is_null($autoDetection) and $autoDetection) { // Language Auto Detection is enabled
                $detectedLanguage = ipReader(request()->ip())->getLang();
                if ($detectedLanguage) {
                    if (in_array($detectedLanguage, $availableLocales)) {
                        $targetLocale = $detectedLanguage;
                    } elseif (in_array('en', $availableLocales)) {
                        $targetLocale = 'en';
                    }
                }
            }

            if (!isset($targetLocale)) {
                $targetLocale = array_first($availableLocales);
            }

            app()->setLocale($targetLocale);
        }

        return $next($request);
    }
}
