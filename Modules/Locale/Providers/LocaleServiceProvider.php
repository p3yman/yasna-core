<?php

namespace Modules\Locale\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\Locale\Http\Middleware\LocaleMiddleware;
use Modules\Yasna\Providers\ModuleEnrichmentTrait;
use Modules\Yasna\Services\YasnaProvider;

class LocaleServiceProvider extends YasnaProvider
{
    protected $middlewares = [
        'locale' => LocaleMiddleware::class,
    ];

    public function index()
    {
        $this->addMiddlewares();
    }

    protected function addMiddlewares()
    {
        foreach ($this->middlewares as $alias => $middleware) {
            $this->addMiddleware($alias, $middleware);
        }
    }
}
