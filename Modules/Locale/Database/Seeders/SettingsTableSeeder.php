<?php

namespace Modules\Locale\Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @throws \Exception
     */
    public function run()
    {
        $ip_reader    = new \Modules\Locale\Services\IP\IpReader(null, false);
        $ip_providers = implode('|', $ip_reader->getAllServices());

        $data = [
             [
                  'slug'          => 'auto_detect_language',
                  'title'         => trans_safe("locale::seeders.auto_detect_language.title"),
                  'category'      => 'template',
                  'order'         => '32',
                  'data_type'     => 'boolean',
                  'default_value' => '1',
                  'hint'          => trans_safe("locale::seeders.auto_detect_language.hint"),
             ],
             [
                  'slug'          => 'ip_tracker_providers',
                  'title'         => trans_safe("locale::seeders.auto_detect_language.providers"),
                  'category'      => 'upstream',
                  'order'         => '0',
                  'data_type'     => 'array',
                  'default_value' => $ip_providers,
             ],
        ];
        yasna()->seed('settings', $data);
    }
}
