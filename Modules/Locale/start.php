<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists("availableLocales")) {
    /**
     * Return the list of available locales.
     *
     * @return array
     */
    function availableLocales()
    {
        $setting = setting()->ask('site_locales')->gain();
        if ($setting and is_array($setting) and count($setting)) {
            return $setting;
        }

        return config('locale.site_locales');
    }
}

if (!function_exists("ipReader")) {

    /**
     * Return an object to get location information from the client's ip.
     *
     * @param string $ip
     *
     * @return \Modules\Locale\Services\IP\IpReader
     * @throws Exception
     */
    function ipReader($ip = null)
    {
        return (new \Modules\Locale\Services\IP\IpReader($ip));
    }
}

if (!function_exists("changeRouteParameters")) {

    /**
     * Change parameters in current route
     *
     * @param mixed ...$parameters
     *
     * @return null|string|string[]
     */
    function changeRouteParameters(...$parameters)
    {
        if (is_array($parameters[0])) {
            $newParams  = $parameters[0];
            $routeIndex = 1;
        } else {
            $newParams  = [$parameters[0] => $parameters[1]];
            $routeIndex = 2;
        }

        if (isset($parameters[$routeIndex]) and $parameters[$routeIndex] instanceof \Illuminate\Routing\Route) {
            $route = $parameters[$routeIndex];
        } else {
            $route = request()->route();
        }

        foreach ($newParams as $newParamName => $newParamValue) {
            $route->setParameter($newParamName, $newParamValue);
        }

        $parameters = $route->parameters();
        $uri        = $route->uri();

        foreach ($parameters as $key => $value) {
            $uri = preg_replace("/{{$key}\??}/", $value, $uri);
        }

        return $uri;
    }
}

if (!function_exists("url_locale")) {

    /**
     * Injects the locale string to the url(), adding the optional $additive to the end.
     *
     * @param string $additive
     *
     * @return string
     */
    function url_locale($additive = null)
    {
        return url('/' . getLocale() . '/' . $additive);
    }
}

if (!function_exists("route_locale")) {

    /**
     * Generates the URL to a named route with the current language.
     *
     * @param  string $name       Name of Target Route
     * @param  array  $parameters Array of parameters to be sent to route() function (except `lang`)
     * @param  bool   $absolute   If true result will contain base url of project
     *
     * @return string
     */
    function route_locale($name, $parameters = [], $absolute = true)
    {
        return route($name, array_merge($parameters, ['lang' => getLocale()]), $absolute);
    }
}

if (!function_exists("action_locale")) {

    /**
     * Generates the URL to a controller action, based on the current locale.
     *
     * @param       $name
     * @param array $parameters
     * @param array ...$otherParameters
     *
     * @return string
     */
    function action_locale($name, $parameters = [], ...$otherParameters)
    {
        return action($name, array_merge($parameters, ['lang' => getLocale()]), ...$otherParameters);
    }
}