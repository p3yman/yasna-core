<?php
return [
     'auto_detect_language' => [
          "title"     => "Language Auto Detect",
          "hint"      => "Automatic language detection based on the user's IP address.",
          "providers" => "IP tracker providers",
     ],
];
