<?php

namespace Modules\Locale\Services\IP;

abstract class IpReaderDriver
{
    protected $ip;
    protected $default_provider = '';
    protected $data             = [];
    protected $flag_url         = 'http://www.countryflags.io/:country_code/:style/:size.png';
    protected $flag_size        = 64; // 16, 24, 32, 48, 64
    protected $flag_style       = 'flat'; // flat, shiny



    /**
     * IpReader constructor.
     *
     * @param string $ip
     *
     * @throws \Exception
     */
    public function __construct($ip = null)
    {
        $this->ip = $ip;
    }



    /**
     * Returns the ISO "ALPHA-2 code of the country
     *
     * @return string|null
     */
    public function getCountryCode()
    {
        return $this->data['country_code'] ?? null;
    }



    /**
     * Returns the full name of the country that has been read from ip
     *
     * @return string|null
     */
    public function getCountryName()
    {
        return $this->data['country_name'] ?? null;
    }



    /**
     * Returns the code of the region that has been read from ip
     *
     * @return string|integer|null
     */
    public function getRegionCode()
    {
        return $this->data['region_code'] ?? null;
    }



    /**
     * Returns the name of the region that has been read from ip
     *
     * @return string|null
     */
    public function getRegionName()
    {
        return $this->data['region_name'] ?? null;
    }



    /**
     * Returns the name of the city that has been read from ip
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->data['city'] ?? null;
    }



    /**
     * Returns the zip code that has been read from ip
     *
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->data['zip'] ?? null;
    }



    /**
     * Returns the time zone of the location that has been read from ip
     *
     * @return string|null
     */
    public function getTimeZone()
    {
        return $this->data['time_zone'] ?? null;
    }



    /**
     * Returns the latitude that has been read from ip
     *
     * @return float|null
     */
    public function getLatitude()
    {
        return $this->data['latitude'] ?? null;
    }



    /**
     * Returns the longitude that has been read from ip
     *
     * @return float|null
     */
    public function getLongitude()
    {
        return $this->data['longitude'] ?? null;
    }



    /**
     * Returns the code of the language that has been read from ip
     *
     * @return array|null
     */
    public function getLang()
    {
        $countryCode = $this->getCountryCode();
        if ($countryCode) {
            return config("locale.countries.$countryCode.languages.0");
        }

        return null;
    }



    /**
     * Returns url of flag image
     *
     * @param integer $size
     * @param string  $style
     *
     * @return string|null
     */
    public function getFlag($size = null, $style = null)
    {
        $size  = $size ?? $this->flag_size;
        $style = $style ?? $this->flag_style;
        return $this->getCountryCode()
             ? str_replace(
                  [':country_code', ':style', ':size'],
                  [strtolower($this->getCountryCode()), $style, $size],
                  $this->flag_url
             )
             : null;
    }



    /**
     * Return <img /> tag containing flag
     *
     * @param integer $size
     * @param string  $style
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    public function getFlagImg($size = null, $style = null)
    {
        $size  = $size ?? $this->flag_size;
        $style = $style ?? $this->flag_style;
        return $this->getCountryCode()
             ? view('locale::html-tags.img', [
                  'src' => $this->getFlag($size, $style),
                  'alt' => $this->getCountryCode(),
             ])
             : null;
    }



    /**
     * Returns latitude and longitude together
     *
     * @return array|null
     */
    public function getCoordinates()
    {
        return $this->getLatitude() ? $this->getLatitude() . ', ' . $this->getLongitude() : null;
    }



    /**
     * Return all the values the server has provider collected
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }



    /**
     * get current provider
     *
     * @return mixed|null
     */
    public function getProvider()
    {
        return $this->data['provider'] ?? null;
    }



    /**
     *  check isset country_name
     */
    public function hasData()
    {
        if (isset($this->data['country_name']) and $this->data['country_name']) {
            return true;
        }

        return false;
    }

}
