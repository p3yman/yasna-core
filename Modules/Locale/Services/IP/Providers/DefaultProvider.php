<?php

namespace Modules\Locale\Services\IP\Providers;

use Modules\Locale\Services\IP\IpReaderDriver;


class DefaultProvider extends IpReaderDriver
{


    /**
     * IpApi constructor.
     *
     * @param string $ip
     *
     * @throws \Exception
     */
    public function __construct($ip)
    {
        parent::__construct($ip);
        $this->build();
    }



    /**
     * Uniform data structure
     */
    public function build()
    {
        $data                   = [];
        $data['ip']             = request()->ip();
        $data['type']           = 'ipv4';
        $data['continent_code'] = 'AS';
        $data['continent_name'] = 'Asia';
        $data['country_code']   = 'IR';
        $data['country_name']   = 'Iran';
        $this->data             = $data;
    }

}
