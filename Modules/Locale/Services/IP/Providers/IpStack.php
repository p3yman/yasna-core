<?php

namespace Modules\Locale\Services\IP\Providers;

use GuzzleHttp\Client;
use Modules\Locale\Services\IP\IpReaderDriver;
use Modules\Locale\Services\IP\IpReaderInterface;


class IpStack extends IpReaderDriver implements IpReaderInterface
{

    private $response = [];
    private $access_key;



    /**
     * IpStack constructor.
     *
     * @param string $ip
     *
     * @throws \Exception
     */
    public function __construct($ip)
    {
        parent::__construct($ip);
        $this->access_key = config('locale.ip_stack_access_key');
        $this->build();
    }



    /**
     * Send rest request to http://api.ipstack.com
     * http://api.ipstack.com/5.117.45.240?access_key=5ea861320400cf607df8ed336a2f8dfd
     * 10.000 requests / mo / free /
     *
     * @return array
     */
    public function sendRequest()
    {
        try {
            $client = new Client();
            $res    = $client->get("http://api.ipstack.com/$this->ip?access_key=$this->access_key");
            if ($res->getStatusCode() == 200) {
                $data = json_decode($res->getBody(), true);
                if ($data and is_array($data) and isset($data['country_code']) and $data['country_code']) {
                    $this->response = $data;
                }
            }
        } catch (\Exception $e) {
            $this->response = [];
        }

        return $this->response;
    }



    /**
     * Uniform data structure
     *
     * @return array|null|void
     */
    public function build()
    {
        $data                       = [];
        $res                        = $this->sendRequest();
        $data['provider']           = 'IpStack';
        $data['ip']                 = $res['ip'] ?? null;
        $data['type']               = $res['type'] ?? null;
        $data['continent_code']     = $res['continent_code'] ?? null;
        $data['continent_name']     = $res['continent_name'] ?? null;
        $data['country_code']       = $res['country_code'] ?? null;
        $data['country_name']       = $res['country_name'] ?? null;
        $data['region_code']        = $res['region_code'] ?? null;
        $data['region_name']        = $res['region_name'] ?? null;
        $data['city']               = $res['city'] ?? null;
        $data['latitude']           = $res['latitude'] ?? null;
        $data['longitude']          = $res['longitude'] ?? null;
        $data['zip']                = $res['zip'] ?? null;
        $data['geoname_id']         = $res['location']['geoname_id'] ?? null;
        $data['capital']            = $res['location']['capital'] ?? null;
        $data['country_flag']       = $res['location']['country_flag'] ?? null;
        $data['calling_code']       = $res['location']['calling_code'] ?? null;
        $data['country_flag_emoji'] = $res['location']['country_flag_emoji'] ?? null;
        $data['language_code']      = $res['location']['languages'][0]['code'] ?? null;
        $data['language_name']      = $res['location']['languages'][0]['name'] ?? null;
        $data['language_native']    = $res['location']['languages'][0]['native'] ?? null;
        $data['time_zone']          = $res['getTimeZone'] ?? null; // return null
        $this->data                 = $data;
    }

}
