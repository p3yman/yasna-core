<?php

namespace Modules\Locale\Services\IP\Providers;

use GuzzleHttp\Client;
use Modules\Locale\Services\IP\IpReaderDriver;
use Modules\Locale\Services\IP\IpReaderInterface;


class IpApi extends IpReaderDriver implements IpReaderInterface
{

    private $response = [];



    /**
     * IpApi constructor.
     *
     * @param string $ip
     *
     * @throws \Exception
     */
    public function __construct($ip)
    {
        parent::__construct($ip);
        $this->build();
    }



    /**
     * Send rest request to http://ip-api.com
     * http://ip-api.com/json/5.117.45.40
     * 10.000 requests / mo / free /
     *
     * @return array
     */
    public function sendRequest()
    {
        try {
            $client = new Client();
            $res    = $client->get("http://ip-api.com/json/$this->ip");
            if ($res->getStatusCode() == 200) {
                $data = json_decode($res->getBody(), true);
                if ($data and is_array($data) and isset($data['countryCode']) and $data['countryCode']) {
                    $this->response = $data;
                }
            }
        } catch (\Exception $e) {
            $this->response = [];
        }

        return $this->response;
    }



    /**
     * Uniform data structure
     *
     * @return array|null|void
     */
    public function build()
    {
        $data                 = [];
        $res                  = $this->sendRequest();
        $data['provider']     = 'IpApi';
        $data['ip']           = $res['query'] ?? null;
        $data['country_code'] = $res['countryCode'] ?? null;
        $data['country_name'] = $res['country'] ?? null;
        $data['region_code']  = $res['region'] ?? null;
        $data['region_name']  = $res['regionName'] ?? null;
        $data['city']         = $res['city'] ?? null;
        $data['latitude']     = $res['lat'] ?? null;
        $data['longitude']    = $res['lon'] ?? null;
        $data['zip']          = $res['zip'] ?? null;
        $data['time_zone']    = $res['timezone'] ?? null;
        $this->data           = $data;
    }

}
