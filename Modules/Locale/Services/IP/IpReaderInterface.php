<?php

namespace Modules\Locale\Services\IP;


interface IpReaderInterface
{

    /**
     * Uniform data structure
     *
     * @return array|null
     */
    public function build();



    /**
     * Send rest request to provider
     *
     * @return array
     */
    public function sendRequest();

}
