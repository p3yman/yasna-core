<?php

namespace Modules\Locale\Services\IP;

use Modules\Locale\Services\IP\Providers\DefaultProvider;

class IpReader
{

    protected $object;



    /**
     * IpReader constructor.
     *
     * @param null|string $ip
     * @param bool        $handle
     *
     * @throws \Exception
     */
    public function __construct($ip = null, $handle = true)
    {
        if ($handle) {
            $ip = $ip ?? request()->ip();
            $this->handle($ip);
        }
    }



    /**
     * handle the providers on order and update order
     *
     * @param string $ip
     *
     * @throws \Exception
     */
    protected function handle($ip)
    {
        foreach ($this->allProviders() as $provider) {
            $namespace       = "Modules\Locale\Services\IP\Providers";
            $full_class_name = $namespace . "\\" . $provider;
            $object          = new $full_class_name($ip);
            if ($object->hasData()) {
                $this->object = $object;
                $this->updateProvidersOrder($object->getProvider());
                break;
            }
        }
        if (!is_object($this->object)) {
            $this->object = new DefaultProvider($ip);
        }
    }



    /**
     * update order of providers
     *
     * @param string $provider
     */
    protected function updateProvidersOrder($provider)
    {
        $providers = setting('ip_tracker_providers')->noCache()->gain()[0] ?? implode('|', $this->allProviders());
        $providers = (array)explode('|', $providers);
        $providers = array_diff($providers, [$provider]);
        $providers = array_prepend($providers, $provider);

        setting('ip_tracker_providers')->setValue(implode('|', $providers));
    }



    /**
     * set default provider
     *
     * @param string $provider
     *
     * @return $this
     */
    protected function setProvider($provider)
    {
        if (in_array($provider, $this->getAllServices())) {
            $this->updateProvidersOrder($provider);
        }
        return $this;
    }



    /**
     * Return all handle able providers.
     *
     * @return array
     */
    public function getAllServices(): array
    {
        $module  = module('locale');
        $classes = [];
        $path    = $module->getPath('Services' . DIRECTORY_SEPARATOR . 'IP' . DIRECTORY_SEPARATOR . 'Providers');

        foreach (scandir($path) as $file_name) {
            if (is_file($path . DIRECTORY_SEPARATOR . $file_name)) {
                array_push($classes, str_before($file_name, '.php'));
            }
        }

        return array_diff($classes, ['DefaultProvider']);
    }



    /**
     * call method from specified object
     *
     * @param string $method
     * @param array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (substr($method, 0, 3) == 'get' && method_exists($this->object, $method)) {
            $object = $this->object;

            return $object->$method();
        }
    }



    /**
     * Return list of providers
     *
     * @return array
     */
    public function allProviders(): array
    {
        if (isset(get_setting('ip_tracker_providers')[0])) {
            $ip_tracker_providers = setting('ip_tracker_providers')->gain()[0];
            return (array)explode('|', $ip_tracker_providers);
        }

        return $this->getAllServices();
    }


}
