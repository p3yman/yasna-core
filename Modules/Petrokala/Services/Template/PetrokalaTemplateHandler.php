<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 12:22 PM
 */

namespace Modules\Petrokala\Services\Template;

use Nwidart\Modules\Facades\Module;

class PetrokalaTemplateHandler extends TemplateHandler
{
    protected static $page_title      = [];
    protected static $site_title;
    protected static $site_nickname;
    protected static $site_languages;
    protected static $yasna_title;
    protected static $yasna_url;
    protected static $contact_info    = [];
    protected static $socials         = [];
    protected static $links           = [];
    protected static $menu_items      = [];
    protected static $other_companies = [];

    protected static $site_logo_url;

    protected static $module_name = 'Petrokala';



    /**
     * Find and return the URL of site's logo
     *
     * @return string|null
     */
    public static function siteLogoUrl()
    {
        if (is_null(static::$site_logo_url)) {
            if (($settingLogo = setting('site_logo')->gain()) and ($logoFile = doc($settingLogo)->getUrl())) {
                static::$site_logo_url = $logoFile;
            } else {
                static::$site_logo_url = Module::asset(static::$module_name . ':images/logo.png');
            }
        }

        return static::$site_logo_url;
    }
}
