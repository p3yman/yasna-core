<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 12:17 PM
 */

namespace Modules\Petrokala\Services\Template;

use Exception;
use Symfony\Component\Debug\Exception\UndefinedMethodException;

class TemplateHandler
{
    private static $method_prefixes = [
         'appending' => 'appendTo',
         'merging'   => 'mergeWith',
         'imploding' => 'implode',
         'assigning' => 'set',
    ];
    protected static $__called_method = null;



    /**
     * Handle called method with proper property
     *
     * @param string $name      The name of the called method
     * @param array  $arguments All passed arguments
     *
     * @return bool|string
     * @throws UndefinedMethodException
     */
    public static function __callStatic($name, $arguments)
    {
        static::$__called_method = $name;

        if (starts_with($name, self::$method_prefixes['appending'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['appending']));
            return self::appendToProperty($property, $arguments);
        } elseif (starts_with($name, self::$method_prefixes['merging'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['merging']));
            return self::mergeWithProperty($property, ...$arguments);
        } elseif (starts_with($name, self::$method_prefixes['imploding'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['imploding']));
            return self::implodeProperty($property, $arguments);
        } elseif (starts_with($name, self::$method_prefixes['assigning'])) {
            $property = snake_case(str_after($name, self::$method_prefixes['assigning']));
            return self::setProperty($property, $arguments);
        } else {
            $property = snake_case($name);
            if (property_exists(static::class, $property)) {
                return static::$$property;
            }
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param mixed  $appending
     *
     * @return bool
     * @throws UndefinedMethodException
     */
    protected static function appendToProperty($property, $appending)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge(static::$$property, $appending);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param array  $merging
     *
     * @return bool
     * @throws UndefinedMethodException
     */
    protected static function mergeWithProperty($property, $merging)
    {
        if (property_exists(static::class, $property)) {
            if (is_array(static::$$property)) {
                static::$$property = array_merge(static::$$property, $merging);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param mixed  $parameters
     *
     * @return bool
     * @throws UndefinedMethodException
     * @throws Exception
     */
    protected static function setProperty($property, $parameters)
    {
        if (property_exists(static::class, $property)) {
            if (count($parameters)) {
                static::$$property = $parameters[0];
            } else {
                static::tooFewArgumentsException(1);
            }

            return true;
        }

        self::undefinedMethodException();
    }



    /**
     * @param string $property
     * @param array  $parameters
     *
     * @return string
     * @throws UndefinedMethodException
     * @throws Exception
     */
    protected static function implodeProperty($property, $parameters)
    {
        if (property_exists(static::class, $property) and is_array($propertyVal = static::$$property)) {
            if (count($parameters)) {
                return implode($parameters[0], $propertyVal);
            } else {
                static::tooFewArgumentsException(1);
            }
        }

        self::undefinedMethodException();
    }



    /**
     * @throws UndefinedMethodException
     */
    private static function undefinedMethodException()
    {
        trigger_error(
             'Call to undefined method ' . __CLASS__ . '::' . static::$__called_method . '()', E_USER_ERROR
        );
    }



    /**
     * @param $passedArgumentsCount
     *
     * @throws Exception
     */
    private static function tooFewArgumentsException($passedArgumentsCount)
    {
        trigger_error(
             'Too few arguments to function '
             . __CLASS__ . '::' .
             static::$__called_method
             . ', '
             . $passedArgumentsCount
             . ' passed', E_USER_ERROR
        );
    }
}
