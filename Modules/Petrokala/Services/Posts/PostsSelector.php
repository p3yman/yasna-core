<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 7/8/18
 * Time: 2:07 PM
 */

namespace Modules\Petrokala\Services\Posts;

class PostsSelector
{
    protected $switches;
    protected $builder;



    /**
     * PostsSelector constructor.
     *
     * @param array $switches
     */
    public function __construct($switches = [])
    {
        $this->switches = $switches;

        $this->fillLocale()
             ->fillCriteria()
        ;

        $this->builder = post()->elector($this->switches);
    }



    /**
     * @param $method
     * @param $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->getBuilder()->$method(...$parameters);
    }



    /**
     * @return $this
     */
    protected function fillLocale()
    {
        $this->switches['locale'] = ($this->switches['locale'] ?? getLocale());

        return $this;
    }



    /**
     * @return $this
     */
    protected function fillCriteria()
    {
        $this->switches['criteria'] = ($this->switches['criteria'] ?? 'published');

        return $this;
    }



    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getBuilder()
    {
        return $this->builder;
    }
}
