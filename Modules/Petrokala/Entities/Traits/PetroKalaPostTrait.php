<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 21/01/2018
 * Time: 05:04 PM
 */

namespace Modules\Petrokala\Entities\Traits;

use Illuminate\Database\Eloquent\Collection;
use Modules\Petrokala\Providers\RouteServiceProvider;

trait PetroKalaPostTrait
{
    /**
     * @return string
     */
    public function getDirectUrlAttribute()
    {
        switch ($this->type) {
            case 'news':
                return RouteServiceProvider::actionLocale('PetrokalaController@newsSingle', [
                     'hashid' => $this->hashid,
                ]);
                break;

            case 'services':
                return RouteServiceProvider::actionLocale('PetrokalaController@servicesSingle', [
                     'hashid' => $this->hashid,
                ]);
                break;

            default:
                return '#';
                break;
        }
    }



    /**
     * @param int|null $number
     *
     * @return Collection
     */
    public function similars($number = null)
    {
        $this->spreadMeta();
        $selectArray = [
             'type' => $this->type, // similar post type
        ];
        // similar categories
        if (($categories = $this->categories) and $categories->count()) {
            $selectArray['category'] = $categories->pluck('slug')->toArray();
        }
        $q = static::selector($selectArray)->where('id', '<>', $this->id);
        if ($number and is_int($number)) {
            // limit
            $q->limit($number);
        }
        // sort and return
        return $q->orderBy('pinned_at', 'desc')->orderBy('published_at', 'desc')->get();
    }



    /**
     * apply default condition on post electors for locale field.
     */
    public function consistentElectorLocale()
    {
        $this->elector()->where('locale', getLocale());
    }
}
