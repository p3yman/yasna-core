/*
 |--------------------------------------------------------------------------
 | ISKAST App
 |--------------------------------------------------------------------------
 */
$(document).ready(function () {

    /*-----------------------------------------------------------------
    - Slider
    -----------------------------------------------------------------*/
    $("#slider .rslides").responsiveSlides({
        auto    : true,
        speed   : 500,
        timeout : 4000,
        nav     : true,
        prevText: '<span class="slide-arrow slide-prev icon-arrow-right"></span>',
        nextText: '<span class="slide-arrow slide-next icon-arrow-left"></span>',
    });

    /*-----------------------------------------------------------------
    - Map
    -----------------------------------------------------------------*/
    if ($('.map').length) {
        let map_container = $('.map');
        map_container.each(function (index, container) {
            initLeafletMap($(container))
        });
    }

    function initLeafletMap(container) {
        let lat           = container.data('lat');
        let lng           = container.data('lng');
        let title         = container.data('title');
        let zoom          = 17;
        let token         = window.mapbox.token;
        let layer_uri     = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}';
        let marker_icon   = new L.Icon({
            iconUrl    : 'https://cdn.rawgit.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
            shadowUrl  : 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
            iconSize   : [25, 41],
            iconAnchor : [12, 41],
            popupAnchor: [1, -34],
            shadowSize : [41, 41]
        });

        let my_map = L.map(container[0], {
            center: [lat, lng],
            zoom  : zoom
        });

        L.tileLayer(layer_uri, {
            maxZoom    : 18,
            id         : 'mapbox.streets',
            accessToken: token,
        }).addTo(my_map);


        L.marker([lat, lng], {
            icon   : marker_icon,
            autoPan: true,
            title  : title,
        }).addTo(my_map)
            .bindPopup(title)
            .openPopup();
    }
    /*-----------------------------------------------------------------
    - Footer
    -----------------------------------------------------------------*/
    // var fh = $('footer#main-footer').height();
    // $('body').css('padding-bottom', fh);

    /*-----------------------------------------------------------------
    - Product gallery
    -----------------------------------------------------------------*/
    $(".product-gallery .rslides").responsiveSlides({
        auto          : true,
        speed         : 500,
        timeout       : 4000,
        nav           : false,
        manualControls: '#product-gallery-pager'
    });
    $('#product-gallery-pager').slick({
        dots        : false,
        infinite    : true,
        speed       : 300,
        slidesToShow: 4,
        rtl         : true,
        prevArrow   : '<span class="slide-arrow slide-prev icon-arrow-right"></span>',
        nextArrow   : '<span class="slide-arrow slide-next icon-arrow-left"></span>',
    });

    /*-----------------------------------------------------------------
    - Menu
    -----------------------------------------------------------------*/
    $('a#open-menu').on('click', function (e) {
        e.preventDefault();
        $('body').addClass('menu-opened');
        $('ul.menu').addClass('opened');
    });
    $('a#close-menu').on('click', function (e) {
        e.preventDefault();
        $('body').removeClass('menu-opened');
        $('ul.menu').removeClass('opened');
    });
    var toggleLink = '<a href="#" class="icon-angle-down sub-toggle"></a>';
    $('#menu-wrapper ul.menu li.has-child').append(toggleLink);
    $(document).on('click', 'a.sub-toggle', function (e) {
        e.preventDefault();
        $(this).toggleClass('toggled').prev().slideToggle().parent('li').toggleClass('active');
    });

    $(window).on('resize', function () {
        if ($(window).outerWidth() > 767) {
            $('.menu .has-child>ul').removeAttr('style');
        }
    });

    /*-----------------------------------------------------------------
    - Gallery
    -----------------------------------------------------------------*/
    $('.gallery-wrapper').each(function (index) {
        let that = $(this);
        let id   = '#' + $(this).attr('id');
        that.lightGallery({
            download : false,
            thumbnail: true,
        });

        $('[data-gallery="' + id + '"]').click(function (e) {
            e.preventDefault();
            that.find('.gallery-item').first().trigger('click');
        })
    });

});
