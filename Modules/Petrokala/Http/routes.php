<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => 'Modules\Petrokala\Http\Controllers',
], function () {
    Route::group(['namespace' => 'Auth'], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
    });

    Route::get('/', 'PetrokalaController@index')->middleware('locale');

    Route::group(['middleware' => 'locale', 'prefix' => '{lang?}',], function () {
        Route::get('/', 'PetrokalaController@index');
        Route::get('projects', 'PetrokalaController@projects');
        Route::get('gallery', 'PetrokalaController@gallery');

        Route::group(['prefix' => 'contact'], function () {
            Route::get('/', 'PetrokalaController@contact');
            Route::post('/', 'CommentController@save');
        });
        Route::group(['prefix' => 'about'], function () {
            Route::get('managers', 'AboutController@members');
            Route::get('licences', 'PetrokalaController@licenses');
            Route::get('{part?}', 'AboutController@aboutStatic');
        });

        Route::group(['prefix' => 'news',], function () {
            Route::get('/', 'PetrokalaController@newsList');
            Route::get('{hashid}', 'PetrokalaController@newsSingle');
        });

        Route::group(['prefix' => 'services',], function () {
            Route::get('/', 'PetrokalaController@services');
            Route::get('{hashid}', 'PetrokalaController@servicesSingle');
        });

        Route::get('search', 'SearchController@index');
    });
});
