<?php

namespace Modules\Petrokala\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Petrokala\Http\Requests\CommentSaveRequest;

class CommentController extends PetrokalaFrontControllerAbstract
{
    protected $indexesToChange = [
        'name'    => 'guest_name',
        'email'   => 'guest_email',
        'mobile'  => 'guest_mobile',
        'message' => 'text',
    ];

    protected $sectionFeedTransMap = [
        'contact-commenting' => 'send-message',
    ];

    public function save(CommentSaveRequest $request)
    {
        $savedComment = model('comment')
            ->batchSave($this->changeIndexes($request->all()));

        return $this->jsonAjaxSaveFeedback($savedComment->exists, [
            'success_message'    => $this->getFeed(($post = $request->getPost())->slug),
            'success_form_reset' => 1,

            'danger_message' => $this->getFeed(($post = $request->getPost())->slug, false),
        ]);
    }

    protected function changeIndexes(array $data)
    {
        foreach ($this->indexesToChange as $from => $to) {
            if (array_key_exists($from, $data)) {
                $data[$to] = $data[$from];
                unset($data[$from]);
            }
        }
        return array_default($data, $this->additionalIndexes());
    }

    protected function additionalIndexes()
    {
        return [
            'guest_ip' => \request()->ip(),
            'user_id'  => user()->id,
        ];
    }

    protected function getFeed($section = null, bool $success = true)
    {
        if (!is_null($section) and $section and ($feedIndex = $this->getSectionFeedIndex($section))) {
            return trans(
                'petrokala::general.alert.' .
                ($success ? 'success' : 'error') .
                ".$feedIndex"
            );
        } else {
            return trans('petrokala::form.feed.' . ($success ? 'done' : 'error'));
        }
    }

    protected function getSectionFeedIndex($section)
    {
        return array_key_exists($section, $this->sectionFeedTransMap)
            ? $this->sectionFeedTransMap[$section]
            : false;
    }
}
