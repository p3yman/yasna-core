<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 11:24 AM
 */

namespace Modules\Petrokala\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Pagination\Paginator;
use Modules\Petrokala\Http\Controllers\Traits\PetroKalaTemplateTrait;
use Modules\Petrokala\Http\Controllers\Traits\FeedbackControllerTrait;

abstract class PetrokalaFrontControllerAbstract extends PetrokalaControllerAbstract
{
    use FeedbackControllerTrait {
        abort as protected traitAbort;
    }
    use PetroKalaTemplateTrait;

    protected $posttype_slugs = [
         'sliders'         => 'sliders',
         'statics'         => 'statics',
         'news'            => 'news',
         'commenting'      => 'commenting',
         'members'         => 'members',
         'gallery'         => 'gallery',
         'services'        => 'services',
         'licenses'        => 'licenses',
         'other-companies' => 'other-companies',
    ];



    /**
     * @param mixed ...$parameters
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function template(...$parameters)
    {
        $this->readSettings()
             ->readLanguages()
             ->readYasnaInfo()
             ->readContactInfo()
             ->readSocials()
             ->readOtherCompanies()
        ;
        return parent::template(...$parameters);
    }



    /**
     * @param      $errorCode
     * @param bool $minimal
     * @param null $data
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function abort($errorCode, $minimal = false, $data = null)
    {
        if (is_null($data)) {
            $data = $this->template(
                 'errors' . ($minimal ? '.m' : '') . '.' . $errorCode,
                 $this->variables
            );
        }
        return $this->traitAbort($errorCode, $minimal, $data);
    }



    /**
     * @return $this
     */
    protected function changePaginationView()
    {
        Paginator::defaultView('petrokala::pagination.default');
        return $this;
    }
}
