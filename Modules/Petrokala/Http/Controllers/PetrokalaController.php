<?php

namespace Modules\Petrokala\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use Modules\Petrokala\Providers\PetrokalaServiceProvider;
use Modules\Petrokala\Providers\PostsServiceProvider;
use Modules\Petrokala\Providers\RouteServiceProvider;

class PetrokalaController extends PetrokalaFrontControllerAbstract
{
    protected $limits = [
         'home' => [
              'news' => 3,
         ],
         'list' => [
              'news'     => 12,
              'services' => 24,
         ],
    ];

    protected $static_posts_slugs = [
         'contact'  => 'contact-commenting',
         'projects' => 'projects',
         'services' => 'services',
    ];



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->readHomeSliders()
                    ->readHomeAboutInfo()
                    ->readHomeNews()
                    ->template('home.index', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function readHomeSliders()
    {
        $this->variables['sliders'] = post()->elector(['type' => $this->posttype_slugs['sliders']])
                                            ->orderBy('published_at', 'desc')
                                            ->get()
        ;
        return $this;
    }



    /**
     * @return $this
     */
    protected function readHomeAboutInfo()
    {
        return $this->readEstablishmentYear()
                    ->readHomeAboutPost()
             ;
    }



    /**
     * @return $this
     */
    protected function readEstablishmentYear()
    {
        $this->variables['establishmentYear'] = get_setting('year_of_establishment');
        return $this;
    }



    /**
     * @return $this
     */
    protected function readHomeAboutPost()
    {
        $this->variables['homeAboutStaticPost'] = post()->elector([
             'type' => $this->posttype_slugs['statics'],
             'slug' => 'home-about',
        ])->first()
        ;
        return $this;
    }



    /**
     * @return $this
     */
    protected function readHomeNews()
    {
        $this->variables['news'] = post()->elector(['type' => $this->posttype_slugs['news']])
                                         ->orderBy('published_at', 'desc')
                                         ->limit($this->limits['home']['news'])
                                         ->get()
        ;
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function services()
    {
        return $this->servicesStaticPost()
                    ->servicesPosts()
                    ->changePaginationView()
                    ->template('services.list.index', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function servicesStaticPost()
    {
        $this->variables['static_post'] = post()->elector([
                  'type'   => $this->posttype_slugs['statics'],
                  'slug'   => $this->static_posts_slugs['services'],
                  'locale' => getLocale(),
             ])->first() ?? post();

        return $this;
    }



    /**
     * @return $this
     */
    protected function servicesPosts()
    {
        $this->variables['posts'] = post()->elector([
             'type'   => $this->posttype_slugs['services'],
             'locale' => getLocale(),
        ])->orderByDesc('created_at')
                                          ->paginate($this->limits['list']['services'])
        ;

        return $this;
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function servicesSingle(Request $request)
    {
        return $this->findServicesSingle($request)
                    ->renderServicesSingleView()
             ;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function findServicesSingle(Request $request)
    {
        $this->variables['post'] = post()->elector([
             'type'   => $this->posttype_slugs['services'],
             'hashid' => $request->hashid,
        ])->first()
        ;
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderServicesSingleView()
    {
        $post = $this->variables['post'];

        if ($post) {
            return $this
                 ->singleSetLocaleUrlParameter($post)
                 ->template('services.single.index', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function projects()
    {
        return $this->readProjectsStaticPost()
                    ->renderProjectsView()
             ;
    }



    /**
     * @return $this
     */
    protected function readProjectsStaticPost()
    {
        $this->variables['post'] = post()->elector([
             'type' => $this->posttype_slugs['statics'],
             'slug' => $this->static_posts_slugs['projects'],
        ])->first()
        ;
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderProjectsView()
    {
        $post = $this->variables['post'];

        if ($post) {
            return $this
                 ->singleSetLocaleUrlParameter($post, '')
                 ->template('projects.index', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function newsList()
    {
        return $this->readNewsListPosts()
                    ->renderNewsListView()
             ;
    }



    /**
     * @return $this
     */
    protected function readNewsListPosts()
    {
        $posttype                    = posttype($this->posttype_slugs['news']);
        $this->variables['posttype'] = $posttype;
        $this->variables['posts']    = post()->elector([
             'type' => $this->posttype_slugs['news'],
        ])->orderBy('published_at', 'desc')
                                             ->paginate($this->limits['list']['news'])
        ;

        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderNewsListView()
    {
        $posttype = $this->variables['posttype'];
        if ($posttype->exists) {
            return $this->changePaginationView()
                        ->template('news.list.index', $this->variables)
                 ;
        } else {
            $this->variables['errorMessage'] = trans('petrokala::general.alert.error.post-found');
            return $this->abort(404);
        }
    }



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function newsSingle(Request $request)
    {
        return $this->findNewsSingle($request)
                    ->renderNewsSingleView()
             ;
    }



    /**
     * @param Request $request
     *
     * @return $this
     */
    protected function findNewsSingle(Request $request)
    {
        $this->variables['post'] = (
             ($post = Post::findByHashid($request->hashid)) and
             $post->exists and
             ($post->type == $this->posttype_slugs['news']) and
             $post->isPublished()
        ) ? $post : null;
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderNewsSingleView()
    {
        $post = $this->variables['post'];

        if ($post) {
            return $this
                 ->singleSetLocaleUrlParameter($post)
                 ->template('news.single.index', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function gallery()
    {
        return $this->readGalleryPosts()
                    ->renderGalleryView()
             ;
    }



    /**
     * @return PetrokalaController
     */
    protected function readGalleryPosts()
    {
        $posttype                    = posttype($this->posttype_slugs['gallery']);
        $this->variables['posttype'] = $posttype;
        $this->variables['posts']    = PostsServiceProvider::collectPosts(['type' => $posttype->slug]);
        return $this->filterGalleryPosts();
    }



    /**
     * @return $this
     */
    protected function filterGalleryPosts()
    {
        $this->variables['posts'] = $this->variables['posts']->filter(function ($item) {
            return $item->files and count($item->files);
        });
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderGalleryView()
    {
        $posttype = $this->variables['posttype'];

        if ($posttype->exists) {
            return $this->template('gallery.index', $this->variables);
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return $this->readCommentingPost()
                    ->readLocationInfo()
                    ->readBranchesContactInfo()
                    ->template('contact.index', $this->variables)
             ;
    }



    /**
     * @return $this
     */
    protected function readLocationInfo()
    {
        $this->variables['mapCoordinates'][0] = get_setting('location');
        $this->variables['mapCoordinates'][1] = get_setting('location2');
        return $this;
    }



    /**
     * @return $this
     */
    protected function readBranchesContactInfo()
    {
        $this->variables['branches_contact_info'] = PetrokalaServiceProvider::allBranchesContactInfo();
        return $this;
    }



    /**
     * @return $this
     */
    protected function readCommentingPost()
    {
        $this->variables['commentingPost'] = Post::selector([
             'type' => $this->posttype_slugs['commenting'],
             'slug' => $this->static_posts_slugs['contact'],
        ])->first()
        ;
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function licenses()
    {
        return $this->readLicensesPosts()
                    ->renderLicensesView()
             ;
    }



    /**
     * @return $this
     */
    protected function readLicensesPosts()
    {
        $posttype                    = posttype($this->posttype_slugs['licenses']);
        $this->variables['posttype'] = $posttype;
        $this->variables['posts']    = PostsServiceProvider::collectPosts([
             'type' => $posttype->slug,
        ]);
        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderLicensesView()
    {
        if ($this->variables['posttype']->exists) {
            return $this->template('licenses.index', $this->variables);
        } else {
            return $this->abort(404);
        }
    }



    /**
     * Sets needed parameters for generate sisterhood URLs.
     *
     * @param Post   $post
     * @param string $parameter_name
     *
     * @return $this
     */
    protected function singleSetLocaleUrlParameter(Post $post, string $parameter_name = 'hashid')
    {
        $current_locales = getLocale();
        $locales         = availableLocales();

        foreach ($locales as $locale) {
            if ($locale == $current_locales) {
                continue;
            }

            $sister = $post->in($locale);
            if ($sister->exists and $sister->isPublished()) {
                if (!$parameter_name) {
                    continue;
                }

                RouteServiceProvider::forceUrlParameters([
                     $locale => [
                          $parameter_name => $sister->hashid,
                     ],
                ]);
            } else {
                RouteServiceProvider::forceUrlParameters([
                     $locale => false,
                ]);
            }
        }

        return $this;
    }
}
