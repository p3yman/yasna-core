<?php

namespace Modules\Petrokala\Http\Controllers;

use App\Models\Post;
use App\Models\Posttype;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Petrokala\Providers\PostsServiceProvider;
use Modules\Petrokala\Providers\RouteServiceProvider;

class AboutController extends PetrokalaFrontControllerAbstract
{
    protected $aboutSlug         = 'about';
    protected $postSlugSeparator = '-';



    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function aboutStatic(Request $request)
    {
        return $this->readAboutPost($request->part)
                    ->renderAboutStaticView()
             ;
    }



    /**
     * @param $part
     *
     * @return string
     */
    protected function getAboutStaticPostSlug($part)
    {
        return implode($this->postSlugSeparator, array_merge((array)$this->aboutSlug, ($part ? (array)$part : [])));
    }



    /**
     * @param $part
     *
     * @return $this
     */
    protected function readAboutPost($part)
    {
        $this->variables['post'] = Post::selector([
             'type' => $this->posttype_slugs['statics'],
             'slug' => $this->getAboutStaticPostSlug($part),
        ])->first()
        ;

        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderAboutStaticView()
    {
        $post = $this->variables['post'];

        if ($post) {
            return $this
                 ->singleSetLocaleUrlParameter($post, '')
                 ->template('about.static.main', $this->variables)
                 ;
        } else {
            return $this->abort(404);
        }
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function members()
    {
        return $this->readMembersLists()
                    ->readMembersCategories()
                    ->renderMembersView()
             ;
    }



    /**
     * @return $this
     */
    protected function readMembersLists()
    {
        return $this->readMembersPosttype();
    }



    /**
     * @return $this
     */
    protected function readMembersPosttype()
    {
        $this->variables['posttype'] = posttype($this->posttype_slugs['members']);
        return $this;
    }



    /**
     * @return $this
     */
    protected function readMembersCategories()
    {
        $this->variables['categories'] = $this->variables['posttype']
             ->categories()
             ->select('categories.*')
             ->where('is_folder', 0)
             ->where('parent_id', 0)
             ->whereNotNull('posts.published_at')
             ->whereNull('posts.deleted_at')
             ->where('posts.locale', getLocale())
             ->orderBy('created_at')
             ->join('category_post', 'category_post.category_id', 'categories.id')
             ->join('posts', 'posts.id', 'category_post.post_id')
             ->groupBy('category_post.category_id')
             ->get()
        ;
        return $this;
    }



    /**
     * @return $this
     */
    protected function readMembersPosts()
    {
        $this->variables['posts'] = [];

        foreach ($this->variables['categories'] as $category) {
            $this->variables['posts'][$category->id] = PostsServiceProvider::selectPosts([
                 'type'       => $this->posttype_slugs['members'],
                 'categories' => [$category->id],
            ])->orderBy('pinned_at', 'desc')
                                                                           ->orderBy('published_at')
                                                                           ->get()
            ;
        }

        return $this;
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    protected function renderMembersView()
    {
        return (
             ($posttype = $this->variables['posttype']) and
             $posttype->exists and
             $this->variables['categories']->count()
        )
             ? $this->readMembersPosts()
                    ->template('about.about-members.main', $this->variables)
             : $this->abort('404');
    }



    /**
     * Sets needed parameters for generate sisterhood URLs.
     *
     * @param Post   $post
     * @param string $parameter_name
     *
     * @return $this
     */
    protected function singleSetLocaleUrlParameter(Post $post, string $parameter_name = 'slug')
    {
        $current_locales = getLocale();
        $locales         = availableLocales();

        foreach ($locales as $locale) {
            if ($locale == $current_locales) {
                continue;
            }

            $sister = $post->in($locale);
            if ($sister->exists and $sister->isPublished()) {
                if (!$parameter_name) {
                    continue;
                }

                RouteServiceProvider::forceUrlParameters([
                     $locale => [
                          $parameter_name => $sister->slug,
                     ],
                ]);
            } else {
                RouteServiceProvider::forceUrlParameters([
                     $locale => false,
                ]);
            }
        }

        return $this;
    }
}
