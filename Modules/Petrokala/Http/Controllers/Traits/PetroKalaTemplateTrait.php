<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 21/01/2018
 * Time: 06:15 PM
 */

namespace Modules\Petrokala\Http\Controllers\Traits;

use Modules\Petrokala\Providers\PostsServiceProvider;
use Modules\Petrokala\Services\Template\PetrokalaTemplateHandler as Template;

trait PetroKalaTemplateTrait
{
    /**
     * @return $this
     */
    private function readSettings()
    {
        return $this->readSiteTitleSetting()
                    ->readSiteNickname()
             ;
    }



    /**
     * @return $this
     */
    private function readSiteTitleSetting()
    {
        $site_title = get_setting('site_title');
        Template::setSiteTitle($site_title);

        return $this;
    }



    /**
     * @return $this
     */
    private function readLanguages()
    {
        Template::setSiteLanguages(
             (($setting = get_setting('site_locales')) and is_array($setting) and count($setting))
                  ? $setting
                  : config('locale.site_locales')
        );
        return $this;
    }



    /**
     * @return $this
     */
    private function readSiteNickname()
    {
        $nickname = get_setting('nickname');
        Template::setSiteNickname($nickname);
        Template::appendToPageTitle($nickname);
        
        return $this;
    }



    /**
     * @return $this
     */
    protected function readYasnaInfo()
    {
        Template::setYasnaTitle(
             get_setting('yasna-group-title')
                  ?: trans('petrokala::general.yasna')
        );
        Template::setYasnaUrl(get_setting('yasna-group-url') ?: '#');
        return $this;
    }



    /**
     * @return $this
     */
    protected function readContactInfo()
    {
        if ($telephone = get_setting('telephone')) {
            if (!is_array($telephone)) {
                $telephone = [$telephone];
            }
            Template::appendToContactInfo([
                 'title' => trans('petrokala::general.contact.phone_number'),
                 'text'  => ad(implode(' , ', $telephone)),
            ]);
        }
        if ($email = get_setting('email')) {
            if (!is_array($email)) {
                $email = [$email];
            }
            Template::appendToContactInfo([
                 'title' => trans('petrokala::general.contact.email'),
                 'text'  => implode(' , ', $email),
            ]);
        }
        if ($address = get_setting('address')) {
            Template::appendToContactInfo([
                 'title' => trans('petrokala::general.contact.address'),
                 'text'  => $address,
            ]);
        }
        return $this;
    }



    /**
     * @return $this
     */
    protected function readSocials()
    {
        if ($instagram = get_setting('instagram_link')) {
            Template::appendToSocials([
                 "link"  => $instagram,
                 "icon"  => "instagram",
                 "title" => "instagram",
            ]);
        }
        if ($facebook = get_setting('facebook_link')) {
            Template::appendToSocials([
                 "link"  => $facebook,
                 "icon"  => "facebook-official",
                 "title" => "facebook",
            ]);
        }
        if ($telegram = get_setting('telegram_link')) {
            Template::appendToSocials([
                 "link"  => $telegram,
                 "icon"  => "telegram",
                 "title" => "telegram",
            ]);
        }
        if ($twitter = get_setting('twitter_link')) {
            Template::appendToSocials([
                 "link"  => $twitter,
                 "icon"  => "twitter",
                 "title" => "twitter",
            ]);
        }
        return $this;
    }



    /**
     * @return $this
     */
    public function readOtherCompanies()
    {
        $other_companies = PostsServiceProvider::selectPosts([
             'type' => $this->posttype_slugs['other-companies'],
        ])->orderByDesc('published_at')
                                               ->limit(6)
                                               ->get()
        ;

        foreach ($other_companies as $post) {
            $link = $post->spreadMeta()->link;

            if (!$link) {
                continue;
            }

            Template::appendToOtherCompanies([
                 'title' => $post->title,
                 'link'  => $link,
            ]);
        }

        return $this;
    }
}
