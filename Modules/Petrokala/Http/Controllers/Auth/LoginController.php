<?php

namespace Modules\Petrokala\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Petrokala\Http\Controllers\Traits\PetroKalaTemplateTrait;
use Modules\Yasna\Http\Controllers\Auth\LoginController as Controller;

class LoginController extends Controller
{
    use PetroKalaTemplateTrait;

    public function showLoginForm()
    {
        return $this->template('petrokala::login.index');
    }

    protected function template(...$parameters)
    {
        $this->readSettings()
            ->readLanguages()
            ->readYasnaInfo()
            ->readContactInfo()
            ->readSocials()
            //            ->readLinks()
        ;
        return view(...$parameters);
    }
}
