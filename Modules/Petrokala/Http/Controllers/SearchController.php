<?php

namespace Modules\Petrokala\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Petrokala\Providers\PostsServiceProvider;
use Modules\Petrokala\Providers\RouteServiceProvider;

class SearchController extends PetrokalaFrontControllerAbstract
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $this->variables['search'] = $request->search;

        if (!$this->variables['search']) {
            return redirect(RouteServiceProvider::actionLocale('PetrokalaController@index'));
        }

        return $this
             ->searchInPosts()
             ->changePaginationView()
             ->template('search.index', $this->variables)
             ;
    }



    /**
     * @param $request
     *
     * @return $this
     */
    protected function searchInPosts()
    {
        $this->variables['posts'] = PostsServiceProvider::selectPosts([
             'type'   => [
                  $this->posttype_slugs['news'],
                  $this->posttype_slugs['services'],
             ],
             'search' => $this->variables['search'],
        ])->orderByDesc('published_at')->paginate(24)
        ;

        return $this;
    }
}
