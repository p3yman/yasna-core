<?php
/**
 * Created by PhpStorm.
 * User: EmiTis Yousefi
 * Date: 03/01/2018
 * Time: 11:18 AM
 */

namespace Modules\Petrokala\Http\Controllers;

abstract class PetrokalaControllerAbstract extends ModuleControllerAbstract
{
    protected static $moduleAlias = 'petrokala';

    protected $variables = [];
}
