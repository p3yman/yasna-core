<?php

namespace Modules\Petrokala\Http\Requests;

use App\Models\Post;
use Modules\Petrokala\Providers\CommentServiceProvider;
use Modules\Yasna\Services\YasnaRequest;

class CommentSaveRequest extends YasnaRequest
{
    protected $post;
    protected $general_rules = ['post_id' => 'exists:posts,id'];



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if ($this->getPost()->exists) {
            return true;
        } else {
            return false;
        }
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'post_id' => 'dehash',
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge($this->general_rules, $this->getPostRules());
    }



    /**
     * Returns the rules based on the specified commenting post.
     * 
     * @return array
     */
    protected function getPostRules()
    {
        $post = $this->getPost();
        if ($post->exists) {
            return CommentServiceProvider::translateRules($post->getMeta('rules'));
        } else {
            return [];
        }
    }



    /**
     * Finds the specified commenting post.
     *
     * @return Post
     */
    protected function findPost()
    {
        $post_id = ($this->data['post_id'] ?? 0);

        return post($post_id);
    }



    /**
     * Returns the specified commenting post.
     *
     * @return Post
     */
    public function getPost()
    {
        if (!$this->post) {
            $this->post = $this->findPost();
        }

        return $this->post;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['posttype_id'] = $this->getPost()->posttype->id;
    }
}
