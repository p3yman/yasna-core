<?php

namespace Modules\Petrokala\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PetrokalaDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(InputsTableSeeder::class);
        $this->call(PosttypesTableSeeder::class);
        $this->call(StaticPostsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
