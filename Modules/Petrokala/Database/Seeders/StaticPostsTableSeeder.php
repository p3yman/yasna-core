<?php

namespace Modules\Petrokala\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class StaticPostsTableSeeder extends Seeder
{
    const POSTTYPE = 'statics';



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->seedPosts();
    }



    /**
     * Seed static posts
     */
    protected function seedPosts()
    {
        $slugs   = $this->slugs();
        $locales = availableLocales();

        foreach ($slugs as $slug) {
            foreach ($locales as $locale) {
                $conditions = [
                     'slug'   => $slug,
                     'type'   => static::POSTTYPE,
                     'locale' => $locale,
                ];
                $model      = model('post')
                     ->firstOrNew($conditions);


                if ($model->exists) {
                    continue;
                }

                $model->batchSave(array_merge($conditions, [
                     'title'        => trans("petrokala::seeder.static-posts.$slug", [], $locale),
                     'published_at' => Carbon::now(),
                     'published_by' => model('user')->inRandomOrder()->first()->id,
                ]));
            }
        }
    }



    /**
     * @return array
     */
    protected function slugs()
    {
        return [
             'services',
        ];
    }
}
