<?php

namespace Modules\Petrokala\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Filemanager\Providers\Upstream\ConfigsServiceProvider;

class PosttypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->seedPosttypes();
        $this->seedFeaturesAndRequiredInputs();
        $this->seedInputs();
        $this->seedUploadConfigs();
    }



    /**
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'posttype'       => [
                       'slug'           => 'services',
                       'title'          => trans('petrokala::seeder.service.title.plural'),
                       'order'          => '9',
                       'singular_title' => trans('petrokala::seeder.service.title.singular'),
                       'template'       => 'special',
                       'icon'           => 'server',
                       'locales'        => 'fa,en',
                  ],
                  'features'       => [
                       'text',
                       'abstract',
                       'locales',
                       'single_view',
                       'featured_image',
                       'manage_sidebar',
                  ],
                  'inputs'         => [],
                  'upload_configs' => [
                       'featured_image' => [
                            'input' => 'featured_image_upload_configs',
                            'file'  => 'services_featured_image',
                            'title' => trans('petrokala::seeder.service.upload-config.featured-image'),
                       ],
                  ],
             ],

             [
                  'posttype'       => [
                       'slug'           => 'licenses',
                       'title'          => trans('petrokala::seeder.licence.title.plural'),
                       'order'          => '10',
                       'singular_title' => trans('petrokala::seeder.licence.title.singular'),
                       'template'       => 'special',
                       'icon'           => 'server',
                       'locales'        => 'fa,en',
                  ],
                  'features'       => [
                       'text',
                       'abstract',
                       'locales',
                       'single_view',
                       'featured_image',
                       'attachments',
                       'manage_sidebar',
                  ],
                  'inputs'         => [],
                  'upload_configs' => [
                       'featured_image' => [
                            'input' => 'featured_image_upload_configs',
                            'file'  => 'licences_featured_image',
                            'title' => trans('petrokala::seeder.licence.upload-config.featured-image'),
                       ],
                       'attachments'    => [
                            'input' => 'attachments_upload_configs',
                            'file'  => 'licences_attachments',
                            'title' => trans('petrokala::seeder.licence.upload-config.attachments'),
                       ],
                  ],
             ],

             [
                  'posttype'       => [
                       'slug'           => 'other-companies',
                       'title'          => trans('petrokala::seeder.other-companies.title.plural'),
                       'order'          => '11',
                       'singular_title' => trans('petrokala::seeder.other-companies.title.singular'),
                       'template'       => 'special',
                       'icon'           => 'server',
                       'locales'        => 'fa,en',
                  ],
                  'features'       => [
                       'locales',
                       'manage_sidebar',
                  ],
                  'inputs'         => ['link'],
                  'upload_configs' => [],
             ],

        ];
    }



    /**
     * Seed data in the posttypes table
     */
    protected function seedPosttypes()
    {
        yasna()->seed('posttypes', array_column($this->data(), 'posttype'));
    }



    /**
     * Seed features of posttypes
     */
    protected function seedFeaturesAndRequiredInputs()
    {
        $data = $this->data();

        foreach ($data as $item) {
            $model = posttype($item['posttype']['slug']);

            if (!$model->exists) {
                continue;
            }

            $model->attachFeatures($item['features']);
            $model->attachRequiredInputs();
        }
    }



    /**
     * Attach inputs to posttypes
     */
    protected function seedInputs()
    {
        $data = $this->data();

        foreach ($data as $item) {
            $model        = posttype($item['posttype']['slug']);
            $inputs_slugs = $item['inputs'];

            if (!$model->exists or empty($inputs_slugs)) {
                continue;
            }

            $inputs_ids = model('input')->whereIn('slug', $inputs_slugs)->pluck('id');

            $model->inputs()->syncWithoutDetaching($inputs_ids);
        }
    }



    /**
     * Seed upload configs and assign them to posttypes
     */
    protected function seedUploadConfigs()
    {
        $data    = $this->data();
        $postfix = ConfigsServiceProvider::getUploadRelatedInputsPostfix();

        foreach ($data as $item) {
            $posttype_slug = $item['posttype']['slug'];
            $model         = posttype($posttype_slug)->spreadMeta();

            if (!$model->exists) {
                continue;
            }

            foreach ($item['upload_configs'] as $key => $info) {
                $setting_slug = snake_case($posttype_slug) . '_' . $key . '_' . $postfix;
                $input        = $info['input'];
                $this->seedUploadConfigsSetting($setting_slug, $info);

                $setting = setting($setting_slug)->spreadMeta();

                if ($model->$input) {
                    continue;
                }

                $model->refresh();

                $model->batchSave([
                     $input => $setting->id,
                ]);
            }
        }
    }



    /**
     * Seed upload configs in the settings table
     *
     * @param string $slug
     * @param array  $info
     */
    protected function seedUploadConfigsSetting($slug, $info)
    {
        $value = $this->uploadConfigJson($info['file']);
        $data  = [
             'slug'          => $slug,
             'title'         => $info['title'],
             'category'      => 'invisible',
             'order'         => '120',
             'data_type'     => 'textarea',
             'default_value' => $value,
             'css_class'     => '',
             'is_localized'  => '0',
        ];

        yasna()->seed('settings', [$data]);
    }



    protected function uploadConfigJson($file)
    {
        $file = implode(DIRECTORY_SEPARATOR, [
                  module('petrokala')->getPath(),
                  'Resources',
                  'json',
                  'upload-configs',
                  $file,
             ]) . '.json';

        return json_encode(json_decode(file_get_contents($file), true));
    }
}
