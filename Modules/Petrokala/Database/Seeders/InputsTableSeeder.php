<?php

namespace Modules\Petrokala\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InputsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('inputs', $this->data());
    }



    /**
     * @return array
     */
    protected function data()
    {
        return [
             [
                  'slug'              => 'link',
                  'title'             => trans('petrokala::seeder.input.link'),
                  'order'             => '52',
                  'type'              => 'editor',
                  'data_type'         => 'text',
                  'css_class'         => '',
                  'validation_rules'  => '',
                  'purifier_rules'    => 'url',
                  'default_value'     => '',
                  'default_value_set' => '0',
                  'hint'              => '',
                  'meta-options'      => '',

             ],
        ];
    }
}
