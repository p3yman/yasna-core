<?php

namespace Modules\Petrokala\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Petrokala\Providers\PetrokalaServiceProvider;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->seedBranchesContactInfo();
    }



    /**
     * Seed branches's contact info in the settings table
     */
    protected function seedBranchesContactInfo()
    {
        $info     = $this->contactInfo();
        $branches = PetrokalaServiceProvider::branches();

        foreach ($branches as $key => $branch) {
            foreach ($info as $record) {
                $slug = $record['slug'];

                $record['slug']  = PetrokalaServiceProvider::getBranchContactSlug($branch, $slug);
                $record['title'] = PetrokalaServiceProvider::getBranchContactTitle($branch, $slug);
                $record['order'] = 50 + ($key * 10) + $record['order'];

                yasna()->seed('settings', [$record]);
            }
        }
    }



    /**
     * @return array
     */
    protected function contactInfo()
    {
        return [
             [
                  'slug'          => 'telephone',
                  'title'         => '',
                  'category'      => 'contact',
                  'order'         => 1,
                  'data_type'     => 'array',
                  'default_value' => '',
                  'hint'          => '',
                  'css_class'     => 'ltr',
                  'is_localized'  => '0',
             ],
             [
                  'slug'         => 'address',
                  'title'        => '',
                  'category'     => 'contact',
                  'order'        => 2,
                  'data_type'    => 'textarea',
                  'hint'         => '',
                  'css_class'    => '',
                  'is_localized' => '1',

             ],
             [
                  'slug'          => 'email',
                  'title'         => '',
                  'category'      => 'contact',
                  'order'         => 3,
                  'data_type'     => 'array',
                  'default_value' => '',
                  'hint'          => '',
                  'css_class'     => 'ltr',
                  'is_localized'  => '0',

             ],
        ];
    }
}
