<!-- Scripts -->
<!-- libs -->
{!! Html::script(Module::asset('petrokala:libs/jquery/jquery.js')) !!}
{!! Html::script(Module::asset('petrokala:libs/responsiveslides.js')) !!}
{!! Html::script(Module::asset('petrokala:libs/slick.min.js')) !!}
{!! Html::script(Module::asset('petrokala:libs/lightgallery.js')) !!}
{!! Html::script(Module::asset('petrokala:libs/lightgallery-thumbnail.js')) !!}
<!-- Site Scripts -->
{!! Html::script(Module::asset('petrokala:js/app.js')) !!}
