<a href="{{ $link or "" }}" class="news-item">
    <span class="cover">
        <img src="{{ $src or "" }}" alt="{{ $alt or "" }}">
    </span>
    <div class="content">
        <div class="title">{{ $title or "" }}</div>
        <div class="ta-l">
            <p>{{ $text or "" }}</p>
        <span class="more">
            <span>{{ trans('petrokala::general.read_more') }}</span>
            @if(isLangRtl())
                <i class="icon-arrow-left"></i>
            @else
                <i class="icon-arrow-right"></i>
            @endif
        </span>
        </div>
    </div>
</a>