<a href="{{ $link }}" id="{{ $id }}">
    <img src="{{ $logo or "" }}" alt="logo">
    <span class="detail">
        <span class="title">{{ $title or "" }}</span>
        <span class="sub-title">{{ $subtitle or "" }}</span>
    </span>
</a>