<div class="with-caption">
    <img src="{{ $src or "" }}" alt="{{ $alt }}">
    <span class="caption"> {{ $caption or "" }} </span>
</div>