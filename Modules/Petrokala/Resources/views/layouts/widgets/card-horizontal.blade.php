<a href="{{ $link }}" class="service-item">
    <span class="cover">
        <img src="{{ $src or "" }}" alt="{{ $alt or "" }}">
    </span>
    <div class="title">{{ $title or "" }}</div>
</a>