<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style a2a_custom_style">
    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
    <a class="a2a_button_facebook"></a>
    <a class="a2a_button_twitter"></a>
    <a class="a2a_button_email"></a>
    <a class="a2a_button_telegram"></a>
</div>
<script>
    var a2a_config = a2a_config || {};
    a2a_config.locale = "fa";
    a2a_config.color_main = "dddddd";
    a2a_config.color_border = "c3c3c3";
    a2a_config.color_link_text = "333333";
    a2a_config.color_link_text_hover = "333333";
    a2a_config.icon_color = "transparent,#868f95";
</script>
@section('end-body')
    {!! Html::script(Module::asset('petrokala:libs/addtoany/page.js')) !!}
@append
<!-- AddToAny END -->