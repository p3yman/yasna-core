<!DOCTYPE html>
<html lang="{{ getLocale() }}">
    <head>
        @include('petrokala::layouts.head')
    </head>
    <body>
        @include('petrokala::layouts.body')
    </body>
</html>
