@php
    $customer = trans('petrokala::general.site_title.full')
@endphp

<div class="copyright">
    <div class="text">© {{ trans('petrokala::general.credit.costumer',["company" => "$customer" , ]) }}</div>
    <div class="text">{{ trans('petrokala::general.credit.yasna') }}
        <a href="{{ Template::yasnaUrl() }}">
            {{ Template::yasnaTitle() }}
        </a>
    </div>
</div>