<div class="col-title">{{ trans('petrokala::general.contact_us') }}</div>
@foreach(Template::contactInfo() as $contactItem)
    <div class="contact-item">
        <span> {{ $contactItem['title'] }}: </span> {{ $contactItem['text'] }}
    </div>
@endforeach

<div class="socials mt10">
    @foreach($socials as $social)
        <a href="{{ $social['link'] }}" class="fa fa-{{ $social['icon']}}"></a>
    @endforeach
</div>