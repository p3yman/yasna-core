@php $other_companies = Template::otherCompanies() @endphp

@if (count($other_companies))
	<div class="row">
		<div class="col-xs-12" style="margin-top: 20px;color: #ffffff">
			{{ trans('petrokala::general.other-group-companies') }}

			@foreach($other_companies as $company)
				<br>
				<a href="{{ $company['link'] }}" target="_blank">{{ $company['title'] }}</a>
			@endforeach
		</div>
	</div>
@endif