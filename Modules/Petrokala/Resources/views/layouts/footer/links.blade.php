{{--<div class="col-title">{{ trans('petrokala::general.links') }}</div>--}}
<ul class="foot-links">
    @foreach($links as $link)
        <li><a href="{{ $link['href'] }}">{{ $link['title'] }}</a></li>
    @endforeach
</ul>