<!-- Footer -->
<footer id="main-footer">

    <div id="foot-content">

        <div class="container">

            <div class="col-sm-4">
                @include('petrokala::layouts.widgets.logo',[
                    "link" => RouteTools::actionLocale('PetrokalaController@index')  ,
                    "id" => "foot-logo" ,
                    "logo" => Template::siteLogoUrl() ,
                    "title" => Template::siteNickname() ,
                    "subtitle" => Template::siteTitle(),
                ])

                @include('petrokala::layouts.footer.other-companies')
            </div>

            <div class="col-sm-2">
                @include('petrokala::layouts.footer.links',[
                    "links" => [
                        [
                            "href" => RouteTools::actionLocale('PetrokalaController@index') ,
                            "title" => trans('petrokala::general.home')
                        ],
                        [
                            "href" => RouteTools::actionLocale('PetrokalaController@newsList')  ,
                            "title" => trans('petrokala::general.news') ,
                        ],
                        [
                            "href" => RouteTools::actionLocale('PetrokalaController@projects')  ,
                            "title" => trans('petrokala::general.projects') ,
                        ],
                        [
                            "href" => RouteTools::actionLocale('PetrokalaController@services') ,
                            "title" => trans('petrokala::general.services') ,
                        ],
                        [
                            "href" => RouteTools::actionLocale('AboutController@aboutStatic')  ,
                            "title" => trans('petrokala::general.about_us') ,
                        ],
                        [
                            "href" => RouteTools::actionLocale('PetrokalaController@gallery')  ,
                            "title" => trans('petrokala::general.gallery') ,
                        ],
                        [
                            "href" => RouteTools::actionLocale('PetrokalaController@contact') ,
                            "title" => trans('petrokala::general.contact_us') ,
                        ],
                    ] ,
                ])
            </div>

            <div class="col-sm-3">
                @include('petrokala::layouts.footer.contact-info',["socials" => Template::socials()])
            </div>

            {{--<div class="col-sm-3">
                @include('petrokala::layouts.footer.newsletter')
            </div>--}}

        </div>

    </div>

    @include('petrokala::layouts.footer.credit')

</footer>
<!-- !END Footer -->