<!-- Meta tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>@yield('site-title', Template::implodePageTitle(' | '))</title>
<!-- Head Assets -->
{!! Html::style(Module::asset('petrokala:libs/fontawesome/css/font-awesome.min.css')) !!}

@if(isLangRtl())
	{!! Html::style(Module::asset('petrokala:css/front.min.css')) !!}
@else
	{!! Html::style(Module::asset('petrokala:css/front-en.min.css')) !!}
@endif

@yield('html-header')

