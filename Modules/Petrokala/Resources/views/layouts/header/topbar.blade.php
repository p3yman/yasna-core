<!-- Top bar -->
<nav id="top-bar">
    <div class="container">
        @if (count($socials))
        <!-- Socials -->
            <div class="socials">
                @foreach($socials as $social)
                    <a href="{{ $social['link'] }}" class="fa fa-{{ $social['icon'] }}"></a>
                @endforeach
            </div>
        @endif
        @php
            $availableLocales = get_setting('site_locales');
            $currentLocale =  getLocale();
        @endphp

        @if(isset($availableLocales) and count($availableLocales) > 1)
            <div class="langs">
                @foreach($availableLocales as $lang)
                    @php($isCurrent = ($lang === $currentLocale) )

                    <a href="{{ $isCurrent ? request()->url() : Petrokala::localeLink($lang) }}"
                       class="{{ $isCurrent? "active" : "" }} simptip-position-bottom simptip-fade"
                       data-tooltip="{{ trans("petrokala::langs.$lang") }}">
                        <img src="{{ Module::asset("petrokala:images/lang-$lang.png") }}" width="21">
                    </a>
                @endforeach
            </div>
        @endif
    </div>
</nav>
<!-- !END Top bar -->
