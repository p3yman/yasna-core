<div id="search-form">
    <div class="field icon">
        {!! Form::open([
            'url' => RouteTools::actionLocale('SearchController@index'),
            'method' => 'get'
        ]) !!}
            <input type="text" name="search" placeholder="{{ trans('petrokala::general.search_statement')."..." }}">
            <button class="icon-search" type="submit"></button> {{--@TODO: Alter style--}}
        {!! Form::close() !!}
    </div>
</div>