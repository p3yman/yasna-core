<nav id="menu-wrapper">

    <div class="container">
        @include('petrokala::layouts.header.site-header.nav',[
            "nav" => [
                [
                    "link" => RouteTools::actionLocale('PetrokalaController@index') ,
                    "title" => trans('petrokala::general.home') ,
                    "submenu" => [] ,
                ],
                [
                    "link" => RouteTools::actionLocale('PetrokalaController@newsList')  ,
                    "title" => trans('petrokala::general.news') ,
                    "submenu" => [] ,
                ],
                [
                    "link" => RouteTools::actionLocale('PetrokalaController@projects')  ,
                    "title" => trans('petrokala::general.projects') ,
                    "submenu" => [] ,
                ],
                [
                    "link" => RouteTools::actionLocale('PetrokalaController@services') ,
                    "title" => trans('petrokala::general.services') ,
                    "submenu" => [] ,
                ],
                [
                    "link" => "#"  ,
                    "title" => trans('petrokala::general.about_us') ,
                    "submenu" => [
                        [
                            "link" => RouteTools::actionLocale('AboutController@aboutStatic') ,
                            "title" => trans('petrokala::general.about_us') ,
                        ],
                        [
                            "link" => RouteTools::actionLocale('AboutController@aboutStatic', ['part' => 'goals']) ,
                            "title" => trans('petrokala::general.about.goals') ,
                        ],
                        [
                            "link" => RouteTools::actionLocale('AboutController@aboutStatic', ['part' => 'safety']) ,
                            "title" => trans('petrokala::general.about.safety') ,
                        ],
                        [
                            "link" => RouteTools::actionLocale('AboutController@members'),
                            "title" => trans('petrokala::general.about.managers.title') ,
                        ],
                        [
                            "link" => RouteTools::actionLocale('AboutController@aboutStatic', ['part' => 'policy']) ,
                            "title" => trans('petrokala::general.about.policy') ,
                        ],
                        [
                            "link" => RouteTools::actionLocale('AboutController@aboutStatic', ['part' => 'history']) ,
                            "title" => trans('petrokala::general.about.history') ,
                        ],
                        [
                            "link" => RouteTools::actionLocale('AboutController@aboutStatic', ['part' => 'licences']) ,
                            "title" => trans('petrokala::general.about.licences') ,
                        ],
                    ] ,
                ],
                [
                    "link" => RouteTools::actionLocale('PetrokalaController@gallery')  ,
                    "title" => trans('petrokala::general.gallery') ,
                    "submenu" => [] ,
                ],
                [
                    "link" => RouteTools::actionLocale('PetrokalaController@contact') ,
                    "title" => trans('petrokala::general.contact_us') ,
                    "submenu" => [] ,
                ],
            ] ,
        ])
        @include('petrokala::layouts.header.site-header.search')
    </div>

</nav>