<div class="xs-show">
    <a href="#" class="icon-bars" id="open-menu"></a>
</div>

<a href="#" class="icon-close xs-show" id="close-menu"></a>

<ul class="menu">
    @foreach($nav as $item)
        <li @if(isset($item['submenu']) and count($item['submenu'])) class="has-child" @endif>
                <a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
                    @if(isset($item['submenu']) and count($item['submenu']))
                        <ul>
                            @foreach($item['submenu'] as $submenu)
                                <li><a href="{{ $submenu['link'] }}">{{ $submenu['title'] }}</a></li>
                            @endforeach
                        </ul>
                    @endif
        </li>
    @endforeach
</ul>