<!-- Site Header -->
<header id="main-header">
    <div class="container">
        @include('petrokala::layouts.widgets.logo',[
            "link" => RouteTools::actionLocale('PetrokalaController@index')  ,
            "id" => "logo" , 
            "logo" => Template::siteLogoUrl() ,
            "title" => Template::siteNickname() ,
            "subtitle" => Template::siteTitle(),
        ])
    </div>
    @include('petrokala::layouts.header.site-header.menu')
</header>
<!-- !END Site Header -->
@yield('page-header')