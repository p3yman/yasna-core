<!-- Header -->
@include('petrokala::layouts.header.topbar',["socials" => Template::socials()])
@include('petrokala::layouts.header.site-header.0header')
@if(isset($hero_header) and is_array($hero_header))
    @include('petrokala::layouts.header.hero-header',[
        "bg_img" => $hero_header['bg_img'] ,
        "links" => $hero_header['links'] ,
        "title" => $hero_header['title'] ,
    ])
@endif
<!-- !END Header -->