<div id="page-header" @isset($bg_img) style="background-image: url('{{ $bg_img }}');" @endisset>
    <ul class="breadcrumbs">
        @foreach($links as $link)
            <li>
                <a href="{{ $link['href'] }}">
                    {{ $link['text'] }}
                </a>
            </li>
        @endforeach
    </ul>
    @isset($title)
        <div class="page-title">{{ $title }}</div>
    @endisset
</div>