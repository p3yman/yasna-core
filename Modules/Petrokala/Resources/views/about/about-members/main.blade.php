@extends('petrokala::layouts.plane')

@php
	$hero_header = [
		"bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
		 "links" => [
			[
				"href" => RouteTools::actionLocale('PetrokalaController@index') ,
				"text" => trans('petrokala::general.home') ,
			],
			[
				"href" => RouteTools::actionLocale('AboutController@aboutStatic') ,
				"text" => trans('petrokala::general.about_us') ,
			],
			[
				"href" => RouteTools::actionLocale('AboutController@members') ,
				"text" => trans('petrokala::general.about.managers.title') ,
			],
		 ] ,
		 "title" => trans('petrokala::general.about.managers.title') ,
	];

	Template::appendToPageTitle(trans('petrokala::general.about_us'));
	Template::appendToPageTitle(trans('petrokala::general.about.managers.title'));
@endphp

@section('content')
	<div class="container">
		<div id="content">
			@if ($categories->count())
				<div class="members">
					<div class="">
						@foreach($categories as $category)
							@include('petrokala::about.about-members.group')
						@endforeach
					</div>
				</div>
			@else
				<div class="col-sm-8 col-center">
					<div id="content">
						<div class="alert alert-danger">
							{!! $__module->getTrans('general.alert.error.post-found') !!}
						</div>
					</div>
				</div>
			@endif
		</div>
	</div>

@endsection
