@if ($postImage = doc($post->featured_image)->getUrl())
    @php $image = $postImage @endphp
@elseif ($posttypeImage = doc($posttype->meta('default_featured_image'))->getUrl())
    @php $image = $posttypeImage @endphp
@else
    @php $image = Module::asset('petrokala:images/user/avatar-tmp-technician.png') @endphp
@endif

<div class="member-col">
    @include('petrokala::about.about-members.id-box',[
        "src" => $image,
        "alt" => $post->title ,
        "name" => $post->title ,
        "position" => $post->meta('seat') ,
    ])
</div>
