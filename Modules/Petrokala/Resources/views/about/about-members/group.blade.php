@php
    $groupPosts = $posts[$category->id]
@endphp
<div class="part-title">{{ $category->titleIn(getLocale()) }}</div>
<div class="members">
    @foreach($groupPosts as $post)
        @include('petrokala::about.about-members.person')
    @endforeach
</div>
