<div class="id-box">
    <div class="image-box">
        <img src="{{ $src or "" }}" alt="{{ $alt or "members" }}">
    </div>
    <div class="info-box">
        @if ($name)
            <div class="name">
                {{ $name }}
            </div>
        @endif
        @if ($position)
            <div class="position">
                {{ $position }}
            </div>
        @endif
    
    </div>
</div>