@extends('petrokala::layouts.plane')

@section('content')
    <!-- Abstract -->
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                <div id="content">
                    @yield('about-content')
                </div>
            </div>
        </div>
    </div>
    <!-- !END Abstract -->
@endsection