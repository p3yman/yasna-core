@extends('petrokala::about.layouts.index')

@php
    $hero_header = [
        "bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
         "links" => [
            [
                "href" => RouteTools::actionLocale('PetrokalaController@index') ,
                "text" => trans('petrokala::general.home') ,
            ],
            [
                "href" => RouteTools::actionLocale('AboutController@aboutStatic'),
                "text" => trans('petrokala::general.about_us') ,
            ],
         ] ,
         "title" => $post->title,
    ];

    if ($hero_header['links'][1]['href'] != request()->url()) {
        Template::appendToPageTitle(trans('petrokala::general.about_us'));
        $hero_header['links'][] = [
                "href" => request()->url(),
                "text" => $post->title,
            ];
    }
    
    Template::appendToPageTitle($post->title);
@endphp

@section('about-content')
    {!! $post->text !!}
@endsection
