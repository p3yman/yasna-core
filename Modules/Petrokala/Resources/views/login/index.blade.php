@extends('petrokala::layouts.plane')

@section('content')
    <div class="container">

        <div class="row">

            <div class="col-sm-4 col-center">

                <div id="content" class="auth">

                    <div class="part-title">{{ trans('petrokala::general.login') }} </div>

                    @include('petrokala::login.from')

                </div>

            </div>

        </div>

    </div>
@endsection