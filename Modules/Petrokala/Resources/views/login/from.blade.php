{!! Form::open(['url' => route('login')]) !!}

<div class="field">
	<label> {{ trans('petrokala::validation.attributes.email') }} </label>
	<input type="text" id="email" name="email" value="{{ old('email') }}" required autofocus>
</div>

<div class="field">
	<label> {{ trans('petrokala::validation.attributes.password') }} </label>
	<input type="password" id="password" name="password" required>
</div>

<div class="field">
	<div class="checkbox">
		<input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''  }}>
		<label for="remember"> {{ trans('petrokala::validation.attributes.remember_me') }} </label>
	</div>
</div>

@if (!env('APP_DEBUG'))
	{!! app('captcha')->render(getLocale()) !!}
@endif

@if ($errors->has('email'))
	<div class="field">
		<div class="alert alert-danger">
			{{ $errors->first('email') }}
		</div>
	</div>
@endif

<div class="action ta-l">
	<button class="button blue"> {{ trans('petrokala::form.button.login') }} </button>
</div>

{!! Form::close() !!}