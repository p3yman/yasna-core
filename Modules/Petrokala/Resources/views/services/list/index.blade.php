@extends('petrokala::layouts.plane')

@php
    $hero_header = [
        "bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
         "links" => [
            [
                "href" => RouteTools::actionLocale('PetrokalaController@index') ,
                "text" => trans('petrokala::general.home') ,
            ],
            [
                "href" => RouteTools::actionLocale('PetrokalaController@services') ,
                "text" => trans('petrokala::general.services') ,
            ]
         ] ,
         "title" => trans('petrokala::general.services') ,
    ];

    Template::appendToPageTitle(trans('petrokala::general.services'));
@endphp


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                <div id="content">
                    @include('petrokala::services.list.abstract')
                </div>
            </div>
            @include('petrokala::services.list.services')

            @if (!$static_post->text and !$posts->count())
                <div class="col-sm-8 col-center">
                    <div id="content">
                        <div class="alert alert-danger">
                            {!! $__module->getTrans('general.alert.error.post-found') !!}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
