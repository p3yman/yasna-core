@if ($posts->count())
	@foreach($posts as $post)
		@php $image = fileManager()->file($post->featured_image)->version('thumb')->resolve() @endphp
		<div class="col-sm-4">
			@include('petrokala::layouts.widgets.card-horizontal',[
				"link" => $post->direct_url ,
				"src" => $image->getUrl() ,
				"alt" => $image->getTitle() ,
				"title" => $post->title ,
			])
		</div>
	@endforeach

	<div class="col-xs-12 col-center" style="margin-bottom: 15px">
		{{ $posts->render() }}
	</div>

@endif
