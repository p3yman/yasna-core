@extends('petrokala::layouts.plane')

@php
    $hero_header = [
        "bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
         "links" => [
            [
                "href" => RouteTools::actionLocale('PetrokalaController@index') ,
                "text" => trans('petrokala::general.home') ,
            ],
            [
                "href" => RouteTools::actionLocale('PetrokalaController@services') ,
                "text" => trans('petrokala::general.services') ,
            ]
         ] ,
         "title" => $post->title,
    ];
    
    Template::appendToPageTitle(trans('petrokala::general.services'));
    Template::appendToPageTitle($post->title);

    $posttype = $post->posttype;
@endphp

@section('content')
    @include('petrokala::services.single.content')
@endsection