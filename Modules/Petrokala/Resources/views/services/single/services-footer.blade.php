<div class="news-footer">
    
    <div class="meta">
        <span>
            {{ trans('petrokala::general.posts.date',[
                "date" =>  ad(echoDate($post->published_at, 'd F Y')),
            ]) }}
        </span>
    </div>
    
    <div class="socials">
        @include('petrokala::layouts.widgets.addtoany')
    </div>

</div>