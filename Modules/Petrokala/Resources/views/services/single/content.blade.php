<div class="container">

    <div class="row">

        <div class="col-sm-8 col-center">

            <div id="content">
                {!! $post->text !!}

                @include('petrokala::services.single.services-footer')

                @include('petrokala::services.single.related')

            </div>

        </div>

    </div>

</div>