@extends('petrokala::layouts.plane')

@php
	$title = trans('petrokala::general.search_for', ['search' => $search]);

	$hero_header = [
		"bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
		 "links" => [
			[
				"href" => RouteTools::actionLocale('PetrokalaController@index') ,
				"text" => trans('petrokala::general.home') ,
			],
			[
				"href" => request()->fullUrl() ,
				"text" => $title ,
			],
		 ] ,
		 "title" => $title ,
	];

	Template::appendToPageTitle($title);
@endphp

@section('content')
	@include('petrokala::search.list')
@append