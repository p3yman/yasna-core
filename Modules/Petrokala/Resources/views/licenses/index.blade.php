@extends('petrokala::layouts.plane')

@php
	$hero_header = [
		"bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
		 "links" => [
			[
				"href" => RouteTools::actionLocale('PetrokalaController@index') ,
				"text" => trans('petrokala::general.home') ,
			],
			[
				"href" => request()->url() ,
				"text" => trans('petrokala::general.about.licences') ,
			],
		 ] ,
		 "title" => trans('petrokala::general.about.licences') ,
	];

	Template::appendToPageTitle(trans('petrokala::general.about.licences'));
@endphp


@section('content')
	<div class="container">
		@include('petrokala::gallery.gallery')
	</div>
@endsection
