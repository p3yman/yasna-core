@extends('petrokala::errors.layouts.plane')

@php
    $errorCode = 404;
    $errorMessage = isset($errorMessage) ? $errorMessage : trans('validation.http.Error' . $errorCode)
@endphp