<!DOCTYPE html>
<html lang="{{ getLocale() }}">
<head>
    <meta charset="UTF-8">
    <title>{{ $errorCode }} !</title>
    {!! Html::style(Module::asset('petrokala:libs/fontawesome/css/font-awesome.min.css')) !!}
    @if(isLangRtl())
        {!! Html::style(Module::asset('petrokala:css/error.min.css')) !!}
    @else
        {!! Html::style(Module::asset('petrokala:css/error-en.min.css')) !!}
    @endif
    
</head>
<body>
<div class="bg">
    <img src="{{ Module::asset('petrokala:images/gears.png') }}" alt="background">
</div>
<a href="{{ RouteTools::actionLocale('PetrokalaController@index') }}" class="fa fa-home home"></a>
<div class="error">
    <h1>{{ $errorCode }}</h1>
    <p>{{ $errorMessage }}</p>
 </div>
</body>
</html>