<div class="container">

    <div class="row">

        <div class="col-sm-12 col-center">

            <div id="content">

                <div class="row">

                    <div class="col-sm-5">

                        @include('petrokala::contact.contact-form')

                    </div>

                    <div class="col-sm-1"></div>

                    <div class="col-sm-6">
                        @include('petrokala::contact.contact-info', [
                            "socials" => Template::socials()
                        ])
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>