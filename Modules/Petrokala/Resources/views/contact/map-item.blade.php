@if ($coordinates and is_array($coordinates) and (count($coordinates) >= 2))
	<div class="col-md-6">
		<div class="part-title">{{ $title or '' }}</div>
		<div class="map" data-lat="{{ $coordinates[0] }}" data-lng="{{ $coordinates[1] }}"
			 data-title="{{ $maker_title or '' }}"
			 style="height: 400px; margin-bottom: 15px"></div>
	</div>
@endif
