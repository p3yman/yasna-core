@if($mapCoordinates and is_array($mapCoordinates) and (count($mapCoordinates) >= 1))
	@include('petrokala::contact.map-item', [
		'coordinates' => $mapCoordinates[0],
		'title' => ($branches_contact_info['tehran']['title'] ?? ''),
		'maker_title' => ($branches_contact_info['tehran']['title'] ?? Template::siteTitle()),
	])
	@include('petrokala::contact.map-item', [
		'coordinates' => $mapCoordinates[1],
		'title' => ($branches_contact_info['kish']['title'] ?? ''),
		'maker_title' => ($branches_contact_info['kish']['title'] ?? Template::siteTitle()),
	])
@endif



@section('html-header')
	@if ($mapCoordinates and is_array($mapCoordinates) and (count($mapCoordinates) >= 1))
		{!!
			Html::style('https://unpkg.com/leaflet@1.3.3/dist/leaflet.css', [
				'integrity' => 'sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==',
				'crossorigin' => ''
			])
		!!}
	@endif
@append

@section('end-body')
	@if ($mapCoordinates and is_array($mapCoordinates) and (count($mapCoordinates) >= 1))
		{!!
			Html::script('https://unpkg.com/leaflet@1.3.3/dist/leaflet.js', [
				'integrity' => 'sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q==',
				'crossorigin' => ''
			])
		!!}

		<script>
            window.mapbox = {
                token: "{{ config('petrokala.mapbox.token') }}",
            };
		</script>
	@endif
@append
