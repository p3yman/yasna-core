@extends('petrokala::layouts.plane')

@php
    $hero_header = [
        "bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
         "links" => [
            [
                "href" => RouteTools::actionLocale('PetrokalaController@index') ,
                "text" => trans('petrokala::general.home') ,
            ],
            [
                "href" => RouteTools::actionLocale('PetrokalaController@contact') ,
                "text" => trans('petrokala::general.contact_us') ,
            ],
         ] ,
         "title" => trans('petrokala::general.contact_us') ,
    ];

    Template::appendToPageTitle(trans('petrokala::general.contact_us'));
@endphp

@section('content')
    @include('petrokala::contact.content')
    @include('petrokala::contact.map')
@endsection