@if ($commentingPost)
    @if ($commentingPost->title2)
        <div class="part-title">{{ $commentingPost->title2 }}</div>
    @endif
    
    @php
        $commentingPost->spreadMeta();
        $fields = CommentingTools::translateFields($commentingPost->fields);
    @endphp
    
    {!! Form::open([
        'url' => RouteTools::actionLocale('CommentController@save'),
        'id' => 'contact-form',
        'class' => 'js'
    ]) !!}

    <input type="hidden" name="post_id" value="{{ $commentingPost->hashid }}">
    
    @foreach($fields as $fieldName => $fieldValue)
        <div class="field">
            <label> {{ trans("petrokala::validation.attributes.$fieldName") }} </label>
            @switch($fieldName)
                @case('name')
                @case('mobile')
                @case('email')
                <input type="text" name="{{ $fieldName }}" id="{{ $fieldName }}">
                @break
                
                @case('text')
                @case('message')
                <textarea cols="30" rows="6" name="{{ $fieldName }}" id="{{ $fieldName }}"></textarea>
                @break
            @endswitch
        </div>
    @endforeach
    
    @include('petrokala::form.feed')
    
    <div class="action ta-l" style="margin-top: 15px">
        <button class="button blue">{{ trans('petrokala::form.button.send') }}</button>
    </div>
    
    {!! Form::close() !!}


    @section('end-body')
        @include('petrokala::assets.form')
    @append
@endif
