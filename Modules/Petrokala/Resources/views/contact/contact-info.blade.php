<div class="part-title">{{ trans('petrokala::general.contact.contact_info') }}</div>

@foreach($branches_contact_info as $branch => $info)
	<h4>{{ $info['title'] }}</h4>
	@foreach($info['contact_info'] as $contact_type => $contact_item)
		<div class="contact-info">
			<div class="name">{{ trans("petrokala::general.contact.$contact_type") }}</div>
			<div class="value">
				@if (is_array($contact_item))
					{{ ad(implode(' , ', $contact_item)) }}
				@else
					{{ $contact_item }}
				@endif
			</div>
		</div>
	@endforeach
@endforeach

<div class="contact-info">
	<div class="name"> {{ trans('petrokala::general.contact.socials') }}  </div>
	<div class="value socials">
		@foreach($socials as $social)
			<a href="{{ $social['link'] }}" class="fa fa-{{ $social['icon']}}"></a>
		@endforeach
	</div>
</div>