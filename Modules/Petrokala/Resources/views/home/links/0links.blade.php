<div id="home-links">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-9 col-center">
                @foreach($links as $link)
                    <div class="col-sm-4">
                        @include('petrokala::home.links.link-box',[
                            "link" => $link['link'] ,
    
                            "icon" => $link['icon'] ,
                            "title" => $link['title']
                        ])
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>