<a href="{{ $link or "" }}" class="item">
    <span class="icon">
        @if(isset($src) )
            <img src="{{$src}}">
        @elseif(isset($icon))
            <i class="fa fa-{{ $icon }}" aria-hidden="true"></i>
        @endif
    </span>
    <span class="back-title"> {{ $title }} </span>
    <span class="title"> {{ $title }} </span>
</a>