@if($news->count())
    @php $newsPosttype = $news->first()->posttype @endphp
    <section id="latest-news">
        <div class="container">
            <div class="part-title">{{ trans('petrokala::general.latest-news') }}</div>
            <div class="row">
                @foreach($news as $newsPost)
                    <div class="col-sm-4">
                        @include('petrokala::layouts.widgets.card-vertical',[
                            "link" => $newsPost->direct_url,
                            "src" => doc($newsPost->featured_image)
                                ->posttype($newsPosttype)
                                ->config('featured_image')
                                ->version('300x200')
                                ->getUrl(),
                            "alt" => $newsPost->title,
                            "title" => $newsPost->title,
                            "text" => $newsPost->abstract,
                        ])
                    </div>
                @endforeach
            </div>
        
        </div>
    
    </section>
@endif