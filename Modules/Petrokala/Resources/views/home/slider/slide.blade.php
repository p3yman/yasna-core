<li>
    <img src="{{ $src or "" }}" alt="{{ $alt or "" }}">
    <div class="content center">
        <div class="title">{{ $title or "" }}</div>
        <div class="subtitle">{{ $subtitle or "" }}</div>
    </div>
</li>
