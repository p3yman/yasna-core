<div id="slider">
    <ul class="rslides">
        @foreach($sliders as $slider)
            @if ($slideImage = doc($slider->featured_image)->getUrl())
                @include('petrokala::home.slider.slide',[
                    "src" => $slideImage ,
                    "alt" => $slider->title2 ,
                    "title" => $slider->title2 ,
                    "subtitle" => $slider->abstract ,
                ])
            @endif
        @endforeach
    </ul>
</div>
