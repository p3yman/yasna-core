<section id="about">
    
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                
                <div id="next-congress" @isset($bg_image) style="background-image: url('{{ $bg_image }}');" @endisset>
                    <div class="content">
                        @if($siteTitle = Template::siteTitle())
                            <div class="subtitle"> {{ $siteTitle }} </div>
                        @endif
                        @if($nickname = Template::siteNickname())
                            <div class="title">{{ $nickname }}</div>
                        @endif
                        @if ($establishmentYear)
                            <div class="date">
                                {{ trans('petrokala::general.founded_in',["date" => ad($establishmentYear)])  }}
                            </div>
                        @endif
                    </div>
                    <div class="action">
                        <a href="{{ RouteTools::actionLocale('AboutController@aboutStatic') }}">
                            {{ trans('petrokala::general.about_us') }}
                        </a>
                    </div>
                </div>
                
                @if ($homeAboutStaticPost)
                    <div id="about-content">
                        
                        @if ($homeAboutStaticPost->title2)
                            <div class="part-title"> {{ $homeAboutStaticPost->title2 }} </div>
                        @endif
                        
                        @if ($homeAboutStaticPost->abstract)
                            <div class="text">
                                <p style="text-align: justify;">
                                    {{ $homeAboutStaticPost->abstract }}
                                </p>
                            </div>
                        @endif
                    
                    </div>
                @endif
            
            </div>
        </div>
    </div>

</section>