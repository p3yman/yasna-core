@extends('petrokala::layouts.plane')

@section('content')
    <!-- Main Slider -->
    @include('petrokala::home.slider.0slider')
    <!-- !END Main Slider -->
    <!-- About -->
    @include('petrokala::home.about.0about')
    <!-- !END About -->
    <!-- Links -->
    @include('petrokala::home.links.0links',[
        "links" => [
            [
                "link" => RouteTools::actionLocale('PetrokalaController@projects') ,
                "icon" => "industry" ,
                "title" => trans('petrokala::general.projects') ,
            ],
            [
                "link" => RouteTools::actionLocale('PetrokalaController@services') ,
                "icon" => "cogs" ,
                "title" => trans('petrokala::general.services') ,
            ],
            [
                "link" => RouteTools::actionLocale('PetrokalaController@gallery') ,
                "icon" => "image" ,
                "title" => trans('petrokala::general.gallery') ,
            ],
        ] ,
    ])
    <!-- !END Links -->
    <!-- Services -->
    {{--@include('petrokala::home.services.0services')--}}
    <!-- !END Services -->
    <!-- Latest News -->
    @include('petrokala::home.latest-news.0latest-news')
    <!-- !END Latest News -->
@endsection