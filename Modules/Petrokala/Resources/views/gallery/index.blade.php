@extends('petrokala::layouts.plane')

@php
	$hero_header = [
		"bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
		 "links" => [
			[
				"href" => RouteTools::actionLocale('PetrokalaController@index') ,
				"text" => trans('petrokala::general.home') ,
			],
			[
				"href" => RouteTools::actionLocale('PetrokalaController@gallery') ,
				"text" => trans('petrokala::general.gallery') ,
			],
		 ] ,
		 "title" => trans('petrokala::general.gallery') ,
	];

	Template::appendToPageTitle(trans('petrokala::general.gallery'));
@endphp


@section('content')
	<div class="container">
		@include('petrokala::gallery.gallery')
	</div>
@endsection
