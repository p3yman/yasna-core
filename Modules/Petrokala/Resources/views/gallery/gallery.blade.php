<div class="gallery">
	<div class="row">
		@if ($posts->count())
			@foreach($posts as $post)
				@php
					$albumId = "lightgallery-" . $post->hashid;
				@endphp
				<div class="col-sm-4">
					@include('petrokala::gallery.album')
				</div>
			@endforeach
		@else
			<div class="col-sm-8 col-center">
				<div id="content">
					<div class="alert alert-danger">
						{!! $__module->getTrans('general.alert.error.post-found') !!}
					</div>
				</div>
			</div>
		@endif

	</div>

</div>
