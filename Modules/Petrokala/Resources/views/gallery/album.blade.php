@php
    if ($postImage = fileManager()->file($post->featured_image)
            ->version('300x200')
            ->config('featured_image')
            ->posttype($posttype)
            ->getUrl()) {
        $coverImage = $postImage;
    } elseif ($posttypeImage = fileManager()->file($posttype->default_featured_image)
            ->version('300x200')
            ->config('featured_image')
            ->posttype($posttype)
            ->getUrl()) {
        $coverImage = $posttypeImage;
    } else {
        $coverImage = Module::asset('petrokala:images/image-not-found.png');
    }
@endphp

<a href="#" class="gallery-item" data-sub-html="{{ trans('petrokala::general.image_title') }}"
   data-gallery="#{{ $albumId }}">
    <img src="{{ $coverImage }}">
    <span class="title"> {{ $post->title }} </span>
    <i class="icon-plus-circle"></i>
</a>
<div class="gallery-wrapper" id="{{ $albumId }}">
    @if(count($files = $post->files))
        @foreach($files as $file)
            @php
                $originalImage = fileManager()->file($file['src'])->getUrl();
                $thumbImage = fileManager()->file($file['src'])
                    ->version('96x76')
                    ->config('attachments')
                    ->posttype($posttype)
                    ->getUrl();
            @endphp
            <a href="{{ $originalImage }}" class="gallery-item d-n">
                <img src="{{ $thumbImage }}" alt="{{ $file['label'] or '' }}">
            </a>
        @endforeach
    @endif
</div>