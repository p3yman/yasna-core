<div id="related">
    <div class="part-title small">{{ trans('petrokala::general.related_news') }}</div>

    @php $relatedPosts = $post->similars(2) @endphp
    <div class="row">
        @foreach($relatedPosts as $relatedPost)
            @if ($postImage = doc($relatedPost->featured_image)->getUrl())
                @php $image = $postImage @endphp
            @elseif ($posttypeImage = doc($posttype->meta('default_featured_image'))->getUrl())
                @php $image = $posttypeImage @endphp
            @else
                @php $image = null @endphp
            @endif
            <div class="col-sm-6">
                @include('petrokala::layouts.widgets.card-vertical',[
                    "link" => $relatedPost->direct_url,
                    "src" => $image,
                    "alt" => $relatedPost->title,
                    "title" => $relatedPost->title,
                    "text" => $relatedPost->abstract,
                ])
            </div>
        @endforeach
    </div>

</div>