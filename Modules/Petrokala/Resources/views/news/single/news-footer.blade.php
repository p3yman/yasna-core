<div class="news-footer">
    
    <div class="meta">
        <span>
            {{ trans('petrokala::general.posts.date',[
                "date" =>  ad(echoDate($post->published_at, 'd F Y')),
            ]) }}
        </span>
    </div>
    
    <div class="socials">
        {{--@foreach($socials as $social)
            <a href="{{ $social['link'] }}" class="{{ $social['icon'] }}"></a>
        @endforeach--}}
        @include('petrokala::layouts.widgets.addtoany')
    </div>

</div>