<div class="container">

    <div class="row">

        <div class="col-sm-8 col-center">

            <div id="content">
                {!! $post->text !!}

                @include('petrokala::news.single.news-footer')

                @include('petrokala::news.single.related',[
                    "posts" => [
                        [
                            "link" => '#' ,
                            "src" => Module::asset('petrokala:images/slider/1.jpg') ,
                            "alt" => 'news' ,
                            "title" => 'خبر جدید',
                            "text" => ' وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد
                                آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی
و صفحه‌بندی را به پایان برند.' ,
                        ],
                        [
                            "link" => '#' ,
                            "src" => Module::asset('petrokala:images/slider/1.jpg') ,
                            "alt" => 'news' ,
                            "title" => 'خبر جدید',
                            "text" => ' وظیفه رعایت حق تکثیر متون را ندارند و در همان حال کار آنها به نوعی وابسته به متن می‌باشد
                                آنها با استفاده از محتویات ساختگی، صفحه گرافیکی خود را صفحه‌آرایی می‌کنند تا مرحله طراحی
و صفحه‌بندی را به پایان برند.' ,
                        ]
                    ] ,
                ])

            </div>

        </div>

    </div>

</div>