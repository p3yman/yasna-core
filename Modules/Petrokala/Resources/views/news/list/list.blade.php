<div class="container">
	<div class="row">
		<div class="col-sm-8 col-center">
			<div id="content">
			@if ($posts->count())

				@foreach($posts as $post)
					@include('petrokala::news.list.item')
				@endforeach

				<!-- Pagination -->
				{!! $posts->render() !!}
				<!-- !END Pagination -->

				@else

					<div class="alert alert-danger">
						{!! $__module->getTrans('general.alert.error.post-found') !!}
					</div>

				@endif
                
			</div>
		</div>
	</div>
</div>
