@extends('petrokala::layouts.plane')

@php
    $hero_header = [
        "bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
         "links" => [
            [
                "href" => RouteTools::actionLocale('PetrokalaController@index') ,
                "text" => trans('petrokala::general.home') ,
            ],
            [
                "href" => RouteTools::actionLocale('PetrokalaController@newsList') ,
                "text" => trans('petrokala::general.news') ,
            ],
         ] ,
         "title" => trans('petrokala::general.news') ,
    ];
    
    Template::appendToPageTitle(trans('petrokala::general.news'));
@endphp

@section('content')
    @include('petrokala::news.list.list')
@endsection
