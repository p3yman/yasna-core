<a href="{{ $post->direct_url }}" class="news-item archive">
    @if ($postImage = doc($post->featured_image)->getUrl())
        @php $image = $postImage @endphp
    @elseif ($posttypeImage = doc($posttype->meta('default_featured_image'))->getUrl())
        @php $image = $posttypeImage @endphp
    @else
        @php $image = null @endphp
    @endif
    <span class="cover" style="background-image: url('{{ $image }}');"></span>
    <div class="content">
        <div class="title">{{ $post->title }}</div>
        <p>
            {{ $post->abstract }}
        </p>
        <div class="ta-l">
            <span class="more">
                <span>{{ trans('petrokala::general.read_more') }}</span>
                @if(isLangRtl())
                    <i class="icon-arrow-left"></i>
                @else
                    <i class="icon-arrow-right"></i>
                @endif
            </span>
        </div>
    </div>
</a>