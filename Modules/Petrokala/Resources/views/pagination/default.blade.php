@if ($paginator->hasPages())
    <div class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="arrow first"><span class="icon-arrow-right"></span></a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="arrow first" rel="prev">
                <span class="icon-arrow-right"></span>
            </a>
        @endif
        <ul>
            
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif
                
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @php $viewNumber = ad($page) @endphp
                        @if ($page == $paginator->currentPage())
                            <li class="active"><a>{{ $viewNumber }}</a></li>
                        @else
                            <li><a href="{{ $url }}">{{ $viewNumber }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
        
        </ul>
        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="arrow last" rel="next">
                <span class="icon-arrow-left"></span>
            </a>
        @else
            <a class="arrow last"><span class="icon-arrow-left"></span></a>
        @endif
    </div>
@endif