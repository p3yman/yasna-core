@extends('petrokala::layouts.plane')

@php
    $hero_header = [
        "bg_img" => Module::asset('petrokala:images/slider/18.jpg') ,
         "links" => [
            [
                "href" => RouteTools::actionLocale('PetrokalaController@index') ,
                "text" => trans('petrokala::general.home') ,
            ],
            [
                "href" => RouteTools::actionLocale('PetrokalaController@projects') ,
                "text" => trans('petrokala::general.projects') ,
            ]
         ] ,
         "title" => $post->title ,
    ];

    Template::appendToPageTitle($post->title);
@endphp

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-center">
                <div id="content">
                    {!! $post->text !!}
                </div>
            </div>
        </div>
    </div>
@endsection