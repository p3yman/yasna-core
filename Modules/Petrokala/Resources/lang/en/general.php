<?php
return [
     "site_title"            => [
          "full"         => "Petrokala Pasargad Kish",
          "abbreviation" => "PEKA",
     ],
     "home"                  => "Home",
     "news"                  => "News",
     "related_news"          => "Related News",
     "related_posts"         => "Related Posts",
     "projects"              => "Projects",
     "services"              => "Services",
     "about_us"              => "About Us",
     "gallery"               => "Gallery",
     "image_title"           => "Image Title",
     "contact_us"            => "Contact Us",
     "login"                 => "Login",
     "search_statement"      => "Search keyword",
     "search_for"            => "Search for \":search\"",
     "founded_in"            => "Founded in :date",
     "read_more"             => "Read more",
     "links"                 => "Links",
     "yasna"                 => "Yasna Team",
     "latest-news"           => "Latest News",
     "other-group-companies" => "Other Group Companies",
     "contact"               => [
          "address"      => "Address",
          "phone_number" => "Phone Number",
          "telephone"    => "Phone Number",
          "fax"          => "Fax",
          "email"        => "Email",
          "socials"      => "Social Media",
          "send_message" => "Send a message",
          "contact_info" => "Contact info",
     ],

     "credit" => [
          "costumer" => "All rights are reserved for :company",
          "yasna"    => "Provided by",
     ],

     "about" => [
          "members"  => [
               "title" => "Members",
          ],
          "managers" => [
               "title" => "Company Managers",
          ],
          "goals"    => "Goals and Perspective",
          "safety"   => "Hygiene and Environmental Safety",
          "history"  => "History",
          "policy"   => "Company Policy",
          "licences" => "International Certifications",
     ],

     "posts" => [
          "date" => "Written on :date",

     ],

     "alert" => [
          "success" => [
               "send-message" => "Your message is sent successfully.",
          ],
          "error"   => [
               "send-message" => "Message is not sent.",
               "login-input"  => "Username/password is not correct.",
               "post-found"   => "Post not Found.",
          ],
     ],

];
