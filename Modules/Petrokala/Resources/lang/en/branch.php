<?php
return [
     "the-branch" => "The :branch Branch",

     "contact-info" => [
          "main"      => "Contact info of the :branch Branch",
          "telephone" => "Telephone of the :branch Branch",
          "address"   => "Address of the :branch Branch",
          "email"     => "Email of the :branch Branch",
     ],

     "office" => [
          "tehran" => "Tehran",
          "kish"   => "Kish",
     ],
];
