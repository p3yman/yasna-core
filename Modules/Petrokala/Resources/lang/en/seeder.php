<?php
return [
     'service' => [
          'title'         => [
               'plural'   => 'Services',
               'singular' => 'Service',
          ],
          'upload-config' => [
               'featured-image' => 'Services Featured Image',
          ],
     ],

     'static-posts' => [
          'services' => 'Services',
     ],

];
