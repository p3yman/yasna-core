<?php
return [
    "attributes" => [
        "name"         => "Name",
        "username"     => "Username",
        "email"        => "Email",
        "phone_number" => "Phone Number",
        "mobile"       => "Mobile Number",
        "password"     => "Password",
        "remember_me"  => "Remember me",
        "address"      => "Address",
        "fax"          => "Fax",
        "message"      => "Send a message",
    ],
];
