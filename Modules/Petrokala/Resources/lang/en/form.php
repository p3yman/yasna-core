<?php
return [
    'button' => [
        "send"   => "Send",
        'save'   => 'Save',
        'submit' => 'Submit',
        "back"   => "Back",
        "login"  => "Login",
    ],

    'feed' => [
        'error' => 'Error!',
        'wait'  => 'Wait...',
        'done'  => 'Done.',
    ],

];
