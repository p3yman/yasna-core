<?php
return [
     'service' => [
          'title'         => [
               'plural'   => 'خدمات',
               'singular' => 'خدمت',
          ],
          'upload-config' => [
               'featured-image' => 'عکس سربرگ خدمات',
          ],
     ],

     'licence' => [
          'title'         => [
               'plural'   => 'گواهی‌ها و مدارک بین‌المللی',
               'singular' => 'گواهی و مدرک بین‌المللی',
          ],
          'upload-config' => [
               'featured-image' => 'عکس سربرگ گواهی‌ها و مدارک',
               'attachments'    => 'فایل‌های ضمیمه‌ی گواهی‌ها و مدارک',
          ],
     ],

     'other-companies' => [
          'title' => [
               'plural'   => 'سایر شرکت‌های گروه',
               'singular' => 'شرکت گروه',
          ],
     ],

     'static-posts' => [
          'services' => 'خدمات',
     ],

     'input' => [
          'link' => 'لینک',
     ],

];
