<?php
return [
    'button' => [
        "send"   => "ارسال",
        'save'   => 'ذخیره',
        'submit' => 'ثبت',
        "back"   => "بازگشت",
        "login"  => "ورود",
    ],

    'feed' => [
        'error' => 'بروز خطا',
        'wait'  => 'اندکی صبر...',
        'done'  => 'انجام شد',
    ],

];
