<?php
return [
     "the-branch" => "شعبه‌ی :branch",

     "contact-info" => [
          "main"      => "اطلاعات تماس شعبه‌ی :branch",
          "telephone" => "شماره‌ی تماس شعبه‌ی :branch",
          "address"   => "نشانی پستی شعبه‌ی :branch",
          "email"     => "ایمیل شعبه‌ی :branch",
     ],

     "office" => [
          "tehran" => "تهران",
          "kish"   => "کیش",
     ],
];
