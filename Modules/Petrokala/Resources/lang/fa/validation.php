<?php
return [
    "attributes" => [
        "name"         => "نام",
        "username"     => "نام کاربری",
        "email"        => "ایمیل",
        "phone_number" => "تلفن",
        "mobile"       => "تلفن همراه",
        "password"     => "گذرواژه",
        "remember_me"  => "مرا به خاطر بسپار",
        "address"      => "آدرس",
        "fax"          => "فکس",
        "message"      => "پیامی بفرستید",
    ],
];
