<?php
return [
     "site_title"            => [
          "full"         => "پتروکالا پاسارگاد کیش",
          "abbreviation" => "PEKA",
     ],
     "home"                  => "خانه",
     "news"                  => "اخبار",
     "related_news"          => "اخبار مرتبط",
     "related_posts"         => "مطالب مرتبط",
     "projects"              => "پروژه‌ها",
     "services"              => "خدمات",
     "about_us"              => "درباره‌ی ما",
     "gallery"               => "گالری",
     "image_title"           => "عنوان تصویر",
     "contact_us"            => "تماس با ما",
     "login"                 => "ورود به سایت",
     "search_statement"      => "جست‌وجوی کلیدواژه",
     "search_for"            => "جست‌و‌جو برای «:search»",
     "founded_in"            => "تاسیس در :date",
     "read_more"             => "ادامه مطلب",
     "links"                 => "پیوند‌ها",
     "yasna"                 => "گروه یسنا",
     "latest-news"           => "آخرین اخبار",
     "other-group-companies" => "سایر شرکت‌های گروه",
     "contact"               => [
          "address"      => "آدرس",
          "phone_number" => "تلفن",
          "telephone"    => "تلفن",
          "fax"          => "فکس",
          "email"        => "ایمیل",
          "socials"      => "شبکه‌های اجتماعی",
          "send_message" => "پیامی بفرستید",
          "contact_info" => "اطلاعات تماس",
     ],

     "credit" => [
          "costumer" => "تمامی حقوق سایت متعلق به :company می‌باشد.",
          "yasna"    => "با افتخار قدرت گرفته از ",
     ],

     "about" => [
          "members"  => [
               "title" => "اعضا",
          ],
          "managers" => [
               "title" => "مدیران شرکت",
          ],
          "goals"    => "اهداف کیفی و چشم انداز",
          "safety"   => "ایمنی بهداشت  و محیط زیست",
          "history"  => "تاریخچه و موضوع فعالیت",
          "policy"   => "خط مشی شرکت",
          "licences" => "گواهی و مدارک بین المللی",
     ],

     "posts" => [
          "date" => "نوشته شده در :date",

     ],

     "alert" => [
          "success" => [
               "send-message" => "پیام شما با موفقیت ارسال شد.",
          ],
          "error"   => [
               "send-message" => "پیام ارسال نشد.",
               "login-input"  => "نام کاربری/رمز عبور اشتباه است.",
               "post-found"   => "مطلبی برای نمایش پیدا نشد.",
          ],
     ],

];
