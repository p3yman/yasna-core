<?php

return [
     'name' => 'Petrokala',

     'mapbox' => [
          'token' => env(
               'MAPBOX_TOKEN',
               'pk.eyJ1IjoieWFzbmF0ZWFtIiwiYSI6ImNqazlib2gzcjJ1NGgzcW1sYXF5aDZoM3kifQ.BLoLXT33-GG6NwVp7SaNQQ'
          ),
     ],
];
