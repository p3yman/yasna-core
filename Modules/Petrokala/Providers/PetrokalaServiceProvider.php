<?php

namespace Modules\Petrokala\Providers;

use Modules\Petrokala\Services\Template\PetrokalaTemplateHandler;
use Modules\Yasna\Services\YasnaProvider;

class PetrokalaServiceProvider extends YasnaProvider
{

    private $aliases = [
         'Template'        => PetrokalaTemplateHandler::class,
         'RouteTools'      => RouteServiceProvider::class,
         'CommentingTools' => CommentServiceProvider::class,
    ];



    /**
     * Register dependencies
     */
    public function index()
    {
        $this->registerProviders()->registerAliases()->registerModelTraits();
    }



    /**
     * @return $this
     */
    private function registerProviders()
    {
        $providers = [
             RouteServiceProvider::class,
             CommentServiceProvider::class,
             PostsServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->addProvider($provider);
        }
        return $this;
    }



    /**
     * @return $this
     */
    private function registerAliases()
    {
        foreach ($this->aliases as $alias => $class) {
            $this->addAlias($alias, $class);
        }
        return $this;
    }



    /**
     * Register model traits
     */
    protected function registerModelTraits()
    {
        module("yasna")
             ->service("traits")
             ->add()->trait("PetroKala:PetroKalaPostTrait")->to("Post")
        ;
    }



    /**
     * @return string
     */
    public static function getControllersNamespace()
    {
        return "\\" . str_replace('Providers', 'Http\Controllers', __NAMESPACE__) . "\\";
    }



    /**
     * @return array
     */
    public static function branches()
    {
        return [
             'tehran',
             'kish',
        ];
    }



    /**
     * @param $branch
     *
     * @return array
     */
    public static function branchContactInfo($branch)
    {
        $contact_types = ['telephone', 'address', 'email'];
        $info          = [];

        foreach ($contact_types as $slug) {
            $setting_slug = static::getBranchContactSlug($branch, $slug);
            $setting      = get_setting($setting_slug);

            if ($setting) {
                $info[$slug] = $setting;
            }
        }

        return $info;
    }



    /**
     * @return array
     */
    public static function allBranchesContactInfo()
    {
        $branches_info = [];

        foreach (static::branches() as $branch) {
            $info = static::branchContactInfo($branch);

            if (empty($info)) {
                continue;
            }

            $branches_info[$branch] = [
                 'title'        => trans(
                      'petrokala::branch.the-branch',
                      ['branch' => static::getBranchTitle($branch)]
                 ),
                 'contact_info' => $info,
            ];
        }

        return $branches_info;
    }



    /**
     * @param $branch
     *
     * @return string
     */
    public static function getBranchTitle($branch)
    {
        return trans("petrokala::branch.office.$branch");
    }



    /**
     * @param $branch
     * @param $contact
     *
     * @return string
     */
    public static function getBranchContactTitle($branch, $contact)
    {
        return trans("petrokala::branch.contact-info.$contact", [
             'branch' => static::getBranchTitle($branch),
        ]);
    }



    /**
     * @param $branch
     * @param $contact
     *
     * @return string
     */
    public static function getBranchContactSlug($branch, $contact)
    {
        return implode('_', [
             'branch',
             snake_case($branch),
             $contact,
        ]);
    }



    /**
     * provides url of given locale
     *
     * @param string $locale
     *
     * @return string
     */
    public static function localeLink($locale)
    {
        $current_route = request()->route();
        if (!$current_route->hasParameter('lang')) {
            return static::homeUrl();
        }

        $forced_parameters = RouteServiceProvider::getForcedUrlparameters($locale);


        if ($forced_parameters === false) {
            return static::homeUrl();
        }

        $current_parameters = $current_route->parameters();
        $parameters         = array_merge($current_parameters, $forced_parameters, ['lang' => $locale]);

        return action('\\' . $current_route->getActionName(), $parameters);
    }



    public static function homeUrl($locale = null)
    {
        if (is_null($locale)) {
            $locale = getLocale();
        }

        return RouteServiceProvider::action('PetrokalaController@index', ['lang' => $locale]);;
    }
}
