<?php

namespace Modules\Petrokala\Providers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\ServiceProvider;
use Modules\Petrokala\Services\Posts\PostsSelector;

class PostsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }



    /**
     * @param array $switches
     *
     * @return PostsSelector|Builder
     */
    public static function selectPosts($switches = [])
    {
        return (new PostsSelector($switches));
    }



    /**
     * @param array $switches
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function collectPosts($switches = [])
    {
        return static::selectPosts($switches)
                     ->orderBy('published_at', 'desc')
                     ->get()
             ;
    }
}
