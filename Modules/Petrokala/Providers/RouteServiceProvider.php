<?php

namespace Modules\Petrokala\Providers;

use Illuminate\Support\ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var array Forced values to be used in sisterhood links
     */
    private static $forced_url_parameters = [];

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function action(string $action, ...$otherParameters)
    {
        return action(PetrokalaServiceProvider::getControllersNamespace() . $action, ...$otherParameters);
    }

    public static function actionLocale(string $action, ...$otherParameters)
    {
        return action_locale(PetrokalaServiceProvider::getControllersNamespace() . $action, ...$otherParameters);
    }



    /**
     * Forces variables to be used while generating url of sister pages.
     * <p>
     *     <i>
     *         Note: Language should be specified in parameters.
     *         <ul>
     *             <li>Correct Example: ['en' => ['name' => 'John']];</li>
     *             <li>Wrong Example: ['name' => 'John'];</li>
     *         </ul>
     *     </i>
     * </p>
     *
     * @param array $parameters
     */
    public static function forceUrlParameters($parameters = [])
    {
        $site_locales = availableLocales();
        foreach ($parameters as $locale => $fields) {
            if (in_array($locale, $site_locales) === false) {
                continue;
            }

            static::$forced_url_parameters = array_merge(static::$forced_url_parameters, [
                 $locale => $fields,
            ]);
        }
    }



    /**
     * Returns forced variables to be used while generating url of sister pages.
     *
     * @param string $locale
     *
     * @return array
     */
    public static function getForcedUrlparameters($locale = '')
    {
        if ($locale) {
            if (array_key_exists($locale, static::$forced_url_parameters)) {
                return static::$forced_url_parameters[$locale];
            } else {
                return [];
            }
        }

        return static::$forced_url_parameters;
    }
}
