@include("manage::widgets.toolbar" , [
    'buttons' => [
            [
                'target' => 'modal:manage/temp-companies/label/create',
                'type' => "primary",
                'caption' => trans('tempcompanies::downstream.add-new-label'),
                "class" => "btn btn-success btn-sm" ,
                'icon' => "plus-circle",
            ],
	],
    ])
