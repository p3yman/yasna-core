@include('manage::widgets.grid-rowHeader' , [
	'refresh_url' => route('temp-companies.downstream.row.update', ['hashid' => $model->hashid]) ,
	'handle' => "counter" ,
])

<td>
	{{ $model->title }}
</td>

<td>
	@include('tempcompanies::downstream.labels.browse.actions')
</td>

@if($model->trashed())
	<script>
        $('tr-{{ $model->hashid }}').addClass('deleted-content')
	</script>
@endif
