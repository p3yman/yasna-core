@include("manage::layouts.modal-delete" , [
	'form_url' => route('temp-companies.downstream.row.delete.submit'),
	'modal_title' => trans('tempcompanies::downstream.label-delete') . SPACE . $model->title,
	'title_value' => $model->title,
]     )
