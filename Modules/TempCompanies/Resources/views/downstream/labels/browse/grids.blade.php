@include("manage::widgets.grid" , [
		'headings'          => [
			trans('tempcompanies::downstream.title'),
		],
		'row_view'          => 'tempcompanies::downstream.labels.browse.row',
		'table_id'          => 'labels-table',
		'handle'            => 'counter',
		'operation_heading' => true,
	])