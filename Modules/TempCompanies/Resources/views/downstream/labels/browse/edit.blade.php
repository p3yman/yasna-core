{!!
	widget("modal")
		->target(route("temp-companies.downstream.row.edit.submit"))
		->labelIf($model->exists , trans('tempcompanies::downstream.label-info-edit') . $model->title)
		->labelIf(!$model->exists, "tr:tempcompanies::downstream.add-new-label")
!!}

<div class='modal-body'>

	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}

	{!!
	widget('hidden')
		->name('locales')
		->value(getLocale())
	!!}

	{!!
	widget('hidden')
		->name('model_name')
		->value('TempCompany')
	!!}


	{!!
	widget('hidden')
		->name('parent_id')
		->value(1)
	!!}


	{!!
		widget('input')
			->name('titles')
			->label('tr:tempcompanies::downstream.title')
			->value($model->exists ? $model->title : '')
			->inForm()
			->required()
	!!}


	{!!
		widget('input')
			->name('slug')
			->label('tr:tempcompanies::downstream.slug')
			->value($model->slug)
			->inForm()
			->required()
	!!}

</div>

<div class='modal-footer'>
	@include('manage::forms.buttons-for-modal')
</div>

@include('manage::layouts.modal-end')