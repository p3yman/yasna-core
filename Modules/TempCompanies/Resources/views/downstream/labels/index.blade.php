@extends('manage::layouts.template')

@section('content')

	@include("manage::downstream.tabs")
	@include('tempcompanies::downstream.labels.browse.toolbar')
	@include('tempcompanies::downstream.labels.browse.grids')

@endsection