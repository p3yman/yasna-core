@include("manage::widgets.toolbar" , [
    'title'             => trans('tempcompanies::toolbar.temp-companies'),
    'search' => [
        'target' => url("manage/temp-companies/browse/search"),
        'label'  => trans('tempcompanies::toolbar.search'),
        'value'  => isset($keyword)? $keyword : '',
     ],
    'buttons' => [
            [
                'target' => 'modal:manage/temp-companies/create',
                'type' => "primary",
                'caption' => trans('tempcompanies::grids.add-new-company'),
                "class" => "btn btn-success btn-sm" ,
                'icon' => "plus-circle",
            ],
	],
    ])
