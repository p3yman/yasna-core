@include("manage::widgets.grid-actionCol" , [
	"actions" => [
		//['recycle' , trans('manage::forms.button.undelete') , "modal:manage/posts/upstream/feature/activeness/-hashid-" , $model->trashed()],
		['pencil' , trans('manage::forms.button.edit') , "modal:manage/temp-companies/edit/-hashid-" ],
		['trash-o' , trans('manage::forms.button.soft_delete') , "modal:manage/temp-companies/delete/-hashid-" , !$model->trashed()] ,
	]
])
