@include("manage::layouts.modal-delete" , [
	'form_url' => route('temp-companies.row.delete.submit'),
	'modal_title' => trans('tempcompanies::edit.temp-company-delete') . SPACE . $model->title,
	'title_value' => $model->title,
]     )
