@include("manage::widgets.grid" , [
		'headings'          => [
			trans('tempcompanies::grids.company'),
			trans('tempcompanies::grids.alias'),
			trans('tempcompanies::grids.contact-person'),
			trans('tempcompanies::grids.telephone-number'),
			trans('tempcompanies::grids.website'),
			trans('tempcompanies::grids.email'),
		],
		'row_view'          => 'tempcompanies::browse.row',
		'table_id'          => 'temp-companies-table',
		'handle'            => 'counter',
		'operation_heading' => true,
	])
