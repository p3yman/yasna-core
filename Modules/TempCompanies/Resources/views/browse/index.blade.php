@extends('manage::layouts.template')

@section('content')

	@include('tempcompanies::browse.toolbar')

	@include('tempcompanies::browse.grids')

@endsection