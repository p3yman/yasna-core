{!!
	widget("modal")
		->target(route("temp-companies.row.edit.submit"))
		->labelIf($model->exists , trans('tempcompanies::edit.company-info-edit') . $model->title)
		->labelIf(!$model->exists, "tr:tempcompanies::edit.add-new-company")
!!}

<div class='modal-body'>

	{!!
		widget('hidden')
			->name('hashid')
			->value($model->hashid)
	!!}


	{!!
		widget('input')
			->name('title')
			->label(trans('tempcompanies::edit.title'))
			->value($model->title)
			->inForm()
			->required()
	!!}


	{!!
		widget('input')
			->name('alias')
			->label(trans('tempcompanies::edit.alias'))
			->value($model->alias)
			->inForm()
	!!}


	{!!
		widget('input')
			->name('contact_person')
			->label(trans('tempcompanies::edit.contact-person'))
			->value($model->contact_person)
			->inForm()
			->required()
	!!}


	{!!
	widget('input')
		->name('email')
		->label(trans('tempcompanies::edit.email'))
		->value($model->email)
		->inForm()
	!!}


	{!!
 		widget('combo')
		 	->id('combowidget')
		 	->name('state')
		 	->label('tr:tempcompanies::edit.state')
		 	->inForm()
		 	->options(model("State")->combo())
		 	->searchable()
		 	->value($model->state_id)
		 	->required()
	!!}


	{!!
		widget('textarea')
			->name('postal_address')
			->label(trans('tempcompanies::edit.postal-address'))
			->value($model->postal_address)
			->rows(1)
			->autoSize()
			->inForm()
    !!}


	{!!
		widget('input')
			->name('telephone_number')
			->label(trans('tempcompanies::edit.telephone-number'))
			->value(ad($model->telephone_number))
			->inForm()
			->required()
	!!}


	{!!
		widget('input')
			->name('register_number')
			->label(trans('tempcompanies::edit.register-number'))
			->value($model->register_number)
			->inForm()
	!!}


	{!!
		widget('input')
			->name('website')
			->label(trans('tr:tempcompanies::edit.website'))
			->value($model->website)
			->inForm()
	!!}


	{!!
	widget('textarea')
		->name('custom_note')
		->label(trans('tempcompanies::edit.custom-note'))
		->value($model->custom_note)
		->rows(1)
		->autoSize()
		->inForm()
	!!}


	{!!
	widget("selectize")
		->name('_label')
		->inForm()
		->value($model->exists ? $model->selectizeValue() : '')
		->options($model->selectizeList())
		->valueField('slug')
		->captionField('title')
		->searchable()
		->label("tr:tempcompanies::edit.label(s)")
	!!}

</div>

<div class='modal-footer'>
	@include('manage::forms.buttons-for-modal')
</div>

@include('manage::layouts.modal-end')