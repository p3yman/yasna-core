@include('manage::widgets.grid-rowHeader' , [
	'refresh_url' => route('temp-companies.row.update', ['hashid' => $model->hashid]) ,
	'handle' => "counter" ,
])

<td>
	{{ $model->title }}
</td>

<td>
	{{ $model->alias }}
</td>

<td>
	{{ $model->contact_person }}
</td>

<td>
	{{ ad($model->telephone_number) }}
</td>

<td>
	{{ $model->website }}
</td>

<td>
	{{ $model->email }}
</td>

<td>
	@include('tempcompanies::browse.actions')
</td>

@if($model->trashed())
	<script>
        $('tr-{{ $model->hashid }}').addClass('deleted-content')
	</script>
@endif
