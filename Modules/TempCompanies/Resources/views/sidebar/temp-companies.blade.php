@include('manage::layouts.sidebar-link' , [
    'icon'       => 'building',
    'caption'    => trans('tempcompanies::side-bar.title'),
	'link' => 'temp-companies/browse',
])