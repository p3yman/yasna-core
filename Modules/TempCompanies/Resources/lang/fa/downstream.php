<?php

return [
     'labels'               => 'برچسب‌ها',
     'add-new-label'        => 'افزودن برچسب جدید',
     'label'                => 'برچسب',
     'title'                => 'عنوان',
     'titles'               => 'عنوان‌ها',
     'slug'                 => 'نامک',
     'label-info-edit'      => 'ویرایش اطلاعات برچسب',
     'saved-successfully'   => 'با موفقیت ذخیره شد.',
     'label-delete'         => 'حذف برچسب',
     'deleted-successfully' => 'با موفقیت حذف شد.',
];