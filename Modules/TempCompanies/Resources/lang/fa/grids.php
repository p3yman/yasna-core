<?php

return [
     'company'          => 'شرکت',
     'alias'            => 'نام مستعار',
     'website'          => 'وبسایت',
     'telephone-number' => 'شماره تلفن',
     'contact-person'   => 'رابط',
     'email'            => 'ایمیل',
     'add-new-company'  => 'افزودن شرکت جدید',
];
