<?php

return [
     'company-info-edit'    => 'ویرایش اطلاعات شرکت ',
     'add-new-company'      => 'افزودن شرکت جدید',
     'title'                => 'عنوان',
     'alias'                => 'نام مستعار',
     'contact-person'       => 'رابط',
     'postal-address'       => 'آدرس پستی',
     'email'                => 'ایمیل',
     'telephone-number'     => 'شماره تلفن',
     'register-number'      => 'شماره ثبت',
     'state'                => 'شهر',
     'website'              => 'وب‌سایت',
     'custom-note'          => 'یادداشت',
     'label(s)'             => 'برچسب(ها)',
     'temp-company-delete'  => 'حذف شرکت موقت',
     'saved-successfully'   => 'با موفقیت ذخیره شد.',
     'deleted-successfully' => 'با موفقیت حذف شد.',
];
