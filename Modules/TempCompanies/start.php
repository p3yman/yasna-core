<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}

if (!function_exists('temp_company')) {
    /**
     * Represent an alternative notation to access to the 'TempCompany' model.
     *
     * @param int  $id
     * @param bool $with_trashed
     *
     * @return \App\Models\TempCompany
     */
    function temp_company($id = 0, $with_trashed = false)
    {
        return model('TempCompany', $id, $with_trashed);
    }
}

