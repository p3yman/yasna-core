<?php

namespace Modules\TempCompanies\Providers;

use Modules\Yasna\Services\YasnaProvider;

/**
 * Class TempCompaniesServiceProvider
 *
 * @package Modules\TempCompanies\Providers
 */
class TempCompaniesServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerSidebar();
        $this->registerDownstream();
    }



    /**
     * Manage Sidebar.
     */
    public function registerSidebar()
    {
        service('manage:sidebar')
             ->add('temp-companies')
             ->blade('tempcompanies::sidebar.temp-companies')
             ->order(12)
        ;

    }



    /**
     * Register downstream settings tabs, required for the purpose of this module.
     */
    protected function registerDownstream()
    {
        service('manage:downstream')
             ->add('temp-companies-labels')
             ->link('labels')
             ->trans('tempcompanies::downstream.labels')
             ->method('tempcompanies:DownStreamController@labelsIndex')
             ->order(5)
        ;
    }


}
