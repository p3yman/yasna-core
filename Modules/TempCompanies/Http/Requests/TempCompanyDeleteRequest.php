<?php

namespace Modules\TempCompanies\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TempCompanyDeleteRequest extends YasnaRequest
{
    protected $model_name = "temp_company";
}
