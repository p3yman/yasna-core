<?php

namespace Modules\TempCompanies\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class TempCompanySaveRequest extends YasnaRequest
{
    protected $model_name = "temp_company";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'title'            => 'required|string',
             'contact_person'   => 'required|string',
             'email'            => 'email|nullable',
             'state'            => 'required',
             'postal_address'   => 'string|max:250|nullable',
             'telephone_number' => 'required|integer',
             'register_number'  => 'alpha_num|nullable',
             'website'          => 'url|nullable',
             'custom_note'      => 'string|nullable',
             'label'            => 'string|nullable',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->convertLabelsToArray();

        if ($this->websiteBeginsWithWww()) {
            $this->data['website'] = 'http://' . $this->data['website'];
        }

        $this->data['title']            = ad($this->data['title']);
        $this->data['contact_person']   = ad($this->data['contact_person']);
        $this->data['postal_address']   = ed($this->data['postal_address']);
        $this->data['telephone_number'] = ed($this->data['telephone_number']);
        $this->data['register_number']  = ed($this->data['register_number']);
        $this->data['custom_note']      = ad($this->data['custom_note']);
    }



    /**
     * Checks if the entered website begins with 'www.' or not.
     *
     * @return bool
     */
    protected function websiteBeginsWithWww()
    {
        if (substr($this->data['website'], 0, 4) == 'www.') {
            return true;
        }
        return false;
    }



    /**
     * convert comma separated list of labels to a single array
     *
     * @return void
     */
    protected function convertLabelsToArray()
    {
        $inlet  = $this->data['_label'];
        $outlet = explode(",", $inlet);

        $this->data['_label'] = $outlet;
    }
}
