<?php

namespace Modules\TempCompanies\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DownStreamDeleteRequest extends YasnaRequest
{
    protected $model_name = "label";
}
