<?php

namespace Modules\TempCompanies\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class DownStreamSaveRequest extends YasnaRequest
{
    protected $model_name = "label";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'titles' => 'required|string|max:20',
             'slug'   => 'required|string|max:20',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->data['titles'] = ad($this->data['titles']);
        $this->data['slug']   = ad($this->data['slug']);
    }
}
