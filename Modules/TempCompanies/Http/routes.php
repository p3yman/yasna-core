<?php

Route::group([
     'middleware' => 'web',
     'prefix'     => 'manage/temp-companies',
     'namespace'  => 'Modules\TempCompanies\Http\Controllers',
], function () {
    // TempCompanies Sidebar Index, cruds and search
    Route::get('browse', 'TempCompaniesBrowseController@index');
    Route::get('create', 'TempCompaniesBrowseController@createForm')->name('temp-companies.row.create');
    Route::post('save', 'TempCompaniesBrowseController@saveSubmit')->name('temp-companies.row.edit.submit');
    Route::get('edit/{hashid}', 'TempCompaniesBrowseController@edit')->name('temp-companies.row.edit');
    Route::get('update/{hashid}', 'TempCompaniesBrowseController@updateRow')->name('temp-companies.row.update');
    Route::get('delete/{hashid}', 'TempCompaniesBrowseController@delete')->name('temp-companies.row.delete');
    Route::post('delete', 'TempCompaniesBrowseController@deleteSubmit')->name('temp-companies.row.delete.submit');
    Route::get('browse/search', 'TempCompaniesBrowseController@search');

    // TempCompanies Downstream cruds for labels
    Route::get('label/create', 'DownStreamController@createForm')->name('temp-companies.downstream.row.create');
    Route::post('label/save', 'DownStreamController@saveSubmit')->name('temp-companies.downstream.row.edit.submit');
    Route::get('label/edit/{hashid}', 'DownStreamController@edit')->name('temp-companies.downstream.row.edit');
    Route::get('label/update/{hashid}', 'DownStreamController@updateRow')->name('temp-companies.downstream.row.update');
    Route::get('label/delete/{hashid}', 'DownStreamController@delete')->name('temp-companies.downstream.row.delete');
    Route::post('label/delete', 'DownStreamController@deleteSubmit')->name('temp-companies.downstream.row.delete.submit');
});
