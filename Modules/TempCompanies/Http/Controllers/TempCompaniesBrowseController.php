<?php

namespace Modules\TempCompanies\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Modules\TempCompanies\Http\Requests\TempCompanyDeleteRequest;
use Modules\TempCompanies\Http\Requests\TempCompanySaveRequest;
use Modules\Yasna\Services\YasnaController;

class TempCompaniesBrowseController extends YasnaController
{
    protected $base_model  = "temp_company";
    protected $view_folder = "tempcompanies::browse";



    /**
     * Returns the index page.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $models = $this->prepareModels();

        return $this->view('index', compact('models'));
    }



    /**
     * Prepares the models.
     *
     * @return Collection
     */
    private function prepareModels()
    {
        $models = model('temp_company')->orderBy('created_at', 'desc')->paginate(10);

        return $models;
    }



    /**
     * Returns the 'edit' blade which creates the form.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm()
    {
        $model = model('temp_company');
        return $this->view('edit', compact('model'));
    }



    /**
     * Saves the requested data in the database table.
     *
     * @param TempCompanySaveRequest $request
     *
     * @return string
     */
    public function saveSubmit(TempCompanySaveRequest $request)
    {
        $saved_model = $request->model->batchSave($request);


        if ($saved_model->exists) {
            $saved_model->attachLabels($request->_label);
        }

        return $this->jsonAjaxSaveFeedback($saved_model->exists, [
             'success_message'  => trans('tempcompanies::edit.saved-successfully'),
             'success_callback' => "rowUpdate('temp-companies-table', '$request->model_hashid')",
        ]);
    }



    /**
     * Passes the selected model to the 'edit' blade.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $model = model('temp_company')->grabHashid($request->hashid);

        if (!$model->exists) {
            return $this->abort(404);
        }

        return view('tempcompanies::browse.edit', compact('model'));
    }



    /**
     * Updates the row.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function updateRow(Request $request)
    {
        $model = model('temp_company', $request->hashid, true);

        if (!$model->exists) {
            return $this->abort(404);
        }

        return $this->view('row', compact('model'));
    }



    /**
     * Returns the 'delete' blade.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request)
    {
        $model = model('temp_company')->grabHashid($request->hashid);

        if (!$model->exists) {
            return $this->abort('410');
        }

        return $this->view('delete', compact('model'));
    }



    /**
     * Deletes the row.
     *
     * @param TempCompanyDeleteRequest $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteSubmit(TempCompanyDeleteRequest $request)
    {
        $model = $request->model;

        if (!$model->exists) {
            return $this->abort('410');
        }

        $model->delete();


        return $this->jsonAjaxSaveFeedback($model->exists, [
             'success_message'  => trans('tempcompanies::edit.deleted-successfully'),
             'success_callback' => "rowUpdate('temp-companies-table', '$model->hashid')",
        ]);
    }



    /**
     * return the search keyword and its corresponding result(s) to the 'index' page.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $models  = $this->model()->elector([
             'search' => $request->keyword,
        ])->orderBy('created_at', 'desc')->paginate(10)
        ;

        return $this->view('index', compact('models', 'keyword'));
    }

}
