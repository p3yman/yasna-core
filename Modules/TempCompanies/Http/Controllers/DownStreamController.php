<?php

namespace Modules\TempCompanies\Http\Controllers;

use Modules\TempCompanies\Http\Requests\DownStreamDeleteRequest;
use Modules\TempCompanies\Http\Requests\DownStreamSaveRequest;
use Modules\Yasna\Services\YasnaController;
use Illuminate\Http\Request;

class DownStreamController extends YasnaController
{
    protected $base_model  = "label";
    protected $view_folder = "tempcompanies::downstream.labels.browse";



    /**
     * Provide the view of the downstream settings, labels tab and the desired models.
     *
     * @return array
     */
    public static function labelsIndex()
    {
        return [
             "view_file" => "tempcompanies::downstream.labels.index",
             "models"    => temp_company()->definedLabels()->orderBy('created_at', 'desc')->get(),
        ];
    }



    /**
     * Return the 'edit' view.
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createForm()
    {
        $model = $this->model();
        return $this->view('edit', compact('model'));
    }



    /**
     * Save the requested data in the database table.
     *
     * @param DownStreamSaveRequest $request
     *
     * @return string
     */
    public function saveSubmit(DownStreamSaveRequest $request)
    {
        $label = $request->model;
        if ($label->exists) {
            $is_updated = $label->changeTitlesAndSlug($request->titles, $request->slug);

            return $this->jsonAjaxSaveFeedback($is_updated, [
                 'success_message'  => trans('tempcompanies::downstream.saved-successfully'),
                 'success_callback' => "rowUpdate('labels-table', '$request->model_hashid')",
            ]);
        }

        $is_created = temp_company()->defineLabel($request->slug, $request->titles, temp_company()::PARENT_LABEL_SLUG);

        return $this->jsonAjaxSaveFeedback($is_created, [
             'success_message'  => trans('tempcompanies::downstream.saved-successfully'),
             'success_callback' => "rowUpdate('labels-table', '$request->model_hashid')",
        ]);
    }



    /**
     * Edit the requested data.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $model = $this->model()->grabHashid($request->hashid);

        if (!$model->exists) {
            return $this->abort(404);
        }

        return $this->view('edit', compact('model'));
    }



    /**
     * Update the selected row.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function updateRow(Request $request)
    {
        $model = model('label', $request->hashid, true);

        if (!$model->exists) {
            return $this->abort(404);
        }

        return $this->view('row', compact('model'));
    }



    /**
     * Return the 'delete' view.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Request $request)
    {
        $model = model('label')->grabHashid($request->hashid);

        if (!$model->exists) {
            return $this->abort('410');
        }

        return $this->view('delete', compact('model'));
    }



    /**
     * Delete the row.
     *
     * @param DownStreamDeleteRequest $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteSubmit(DownStreamDeleteRequest $request)
    {
        $model = $request->model;

        if (!$model->exists) {
            return $this->abort('410');
        }

        $model->delete();


        return $this->jsonAjaxSaveFeedback($model->exists, [
             'success_message'  => trans('tempcompanies::downstream.deleted-successfully'),
             'success_callback' => "rowUpdate('labels-table', '$model->hashid')",
        ]);
    }
}
