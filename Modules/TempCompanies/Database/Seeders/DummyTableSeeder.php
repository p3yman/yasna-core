<?php

namespace Modules\TempCompanies\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\TempCompanies\Entities\TempCompany;
use Modules\Yasna\Providers\DummyServiceProvider;

class DummyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // $this->call("OthersTableSeeder");
        $this->tempCompaniesTable();
    }



    /**
     * Inserts dummy data to the 'test_companies' table.
     *
     * @param int $total
     */
    public function tempCompaniesTable($total = 200)
    {
        $counter = 0;
        $attend  = new TempCompany;

        while ($counter < $total) {
            $counter++;

            $attend->batchSave([
                 'title'            => $this->randomTitleGenerator(),
                 'alias'            => dummy()::slug(),
                 'contact_person'   => $this->randomContactPersonGenerator(),
                 'postal_address'   => $this->randomPostalAddressGenerator(),
                 'email'            => DummyServiceProvider::email(),
                 'telephone_number' => "0" . rand(2111111111, min(7999999999, getrandmax())),
                 'register_number'  => (string)rand(100, 999),
                 'state'            => (string)rand(1, 50),
                 'website'          => 'http://www.' . strtolower($this->randomTitleGenerator()) . '-co.com',
                 'label'            => $this->randomLabelGenerator(),
            ]);
        }
    }



    /**
     * Returns a random company 'title'.
     *
     * @return string
     */
    private function randomTitleGenerator()
    {
        $company_titles = ['Apple', 'Microsoft', 'Google', 'Tesla', 'Amazon'];
        return $company_titles[rand(0, count($company_titles) - 1)];
    }



    /**
     * Returns a random 'contact_person'.
     *
     * @return string
     */
    private function randomContactPersonGenerator()
    {
        $contact_persons = ['Jack', 'Tom', 'Sam', 'Julie', 'Ted'];
        return $contact_persons[rand(0, count($contact_persons) - 1)];
    }



    /**
     * Returns a random 'postal_address' between '1000000000' and '9999999999'.
     *
     * @return string
     */
    private function randomPostalAddressGenerator()
    {
        return (string)rand(1000000000, 9999999999);
    }



    /**
     * Returns a random 'label'.
     *
     * @return string
     */
    private function randomLabelGenerator()
    {
        $labels = ['Technology', 'Cars', 'Architecture', 'Civil', 'Computers'];
        return $labels[rand(0, count($labels) - 1)];
    }
}
