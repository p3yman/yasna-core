<?php

namespace Modules\TempCompanies\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LabelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        yasna()->seed('labels', $this->initialData());
    }



    /**
     * Initialize the `labels` table, with one specific row.
     *
     * @return array
     */
    public function initialData()
    {
        return [
             [
                  'parent_id'  => 1,
                  'model_name' => 'TempCompany',
                  'slug'       => temp_company()::PARENT_LABEL_SLUG,
                  'titles'     => 'The Helping Title.',
                  'locales'    => getLocale(),
             ],
        ];
    }
}
