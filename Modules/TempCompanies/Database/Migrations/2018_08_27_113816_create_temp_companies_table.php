<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable()->index();
            $table->string('alias')->nullable()->index();
            $table->string('contact_person')->nullable()->index();
            $table->text('postal_address')->nullable();
            $table->string('email')->nullable()->index();
            $table->string('telephone_number')->nullable()->index();
            $table->string('register_number')->nullable()->index();
            $table->integer('state')->nullable()->index();
            $table->string('website', 200)->nullable()->index();
            $table->text('custom_note')->nullable();
            $table->string('label')->nullable()->inedex();

            $table->timestamps();
            yasna()->additionalMigrations($table);
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_companies');
    }
}
