<?php

namespace Modules\TempCompanies\Entities;

use Modules\Label\Entities\Traits\LabelsTrait;
use Modules\TempCompanies\Entities\Traits\TempCompanyElectorTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TempCompany extends YasnaModel
{
    const PARENT_LABEL_SLUG = "TEMP_PARENT";

    use SoftDeletes;
    use TempCompanyElectorTrait;
    use LabelsTrait;
}
