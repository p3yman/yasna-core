<?php

namespace Modules\TempCompanies\Entities\Traits;

trait TempCompanyElectorTrait
{
    /**
     * elect the keyword according to the corresponding field..
     *
     * @param $keyword
     */
    public function electorSearch($keyword)
    {
        $this->elector()->where(function ($query) use ($keyword) {
            $query->where('title', $keyword)
                  ->orWhere('title', 'like', '%' . $keyword . '%')
                  ->orWhere('contact_person', $keyword)
                  ->orWhere('contact_person', 'like', '%' . $keyword . '%')
                  ->orWhere('postal_address', $keyword)
                  ->orWhere('postal_address', 'like', '%' . $keyword . '%')
                  ->orWhere('email', $keyword)
                  ->orWhere('email', 'like', '%' . $keyword . '%')
                  ->orWhere('telephone_number', $keyword)
                  ->orWhere('telephone_number', 'like', '%' . $keyword . '%')
                  ->orWhere('register_number', $keyword)
                  ->orWhere('register_number', 'like', '%' . $keyword . '%')
                  ->orWhere('state', $keyword)
                  ->orWhere('website', $keyword)
                  ->orWhere('website', 'like', '%' . $keyword . '%')
                  ->orWhere('custom_note', $keyword)
                  ->orWhere('label', $keyword)
                  ->orWhere('label', 'like', '%' . $keyword . '%')
            ;
        })
        ;
    }


    //
    ///**
    // * elect `title`.
    // *
    // * @param $title
    // */
    //public function electorTitle($title)
    //{
    //    $this->elector()->where('title', $title);
    //}
    //
    //
    //
    ///**
    // * elect `contact_person`.
    // *
    // * @param $contact_person
    // */
    //public function electorContactPerson($contact_person)
    //{
    //    $this->elector()->where('contact_person', $contact_person);
    //}
    //
    //
    //
    ///**
    // * elect `postal_address`.
    // *
    // * @param $postal_address
    // */
    //public function electorPostalAddress($postal_address)
    //{
    //    $this->elector()->where('postal_address', $postal_address);
    //}
    //
    //
    //
    ///**
    // * elect `email`.
    // *
    // * @param $email
    // */
    //public function electorEmail($email)
    //{
    //    $this->elector()->where('email', $email);
    //}
    //
    //
    //
    ///**
    // * elect `telephone_number`.
    // *
    // * @param $telephone_number
    // */
    //public function electorTelephoneNumber($telephone_number)
    //{
    //    $this->elector()->where('telephone_number', $telephone_number);
    //}
    //
    //
    //
    ///**
    // * elect `register_number`.
    // *
    // * @param $register_number
    // */
    //public function electorRegisterNumber($register_number)
    //{
    //    $this->elector()->where('register_number', $register_number);
    //}
    //
    //
    //
    ///**
    // * elect `state`.
    // *
    // * @param $state
    // */
    //public function electorState($state)
    //{
    //    $this->elector()->where('state', $state);
    //}
    //
    //
    //
    ///**
    // * elect `website`.
    // *
    // * @param $website
    // */
    //public function electorWebsite($website)
    //{
    //    $this->elector()->where('website', $website);
    //}
    //
    //
    //
    ///**
    // * elect `custom_note`.
    // *
    // * @param $custom_note
    // */
    //public function electorCustomNote($custom_note)
    //{
    //    $this->elector()->where('custom_note', $custom_note);
    //}
    //
    //
    //
    ///**
    // * elect `label`.
    // *
    // * @param $label
    // */
    //public function electorLabel($label)
    //{
    //    $this->elector()->where('label', $label);
    //}
    //
}
