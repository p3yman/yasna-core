<?php
return [
     "authentication_error" => "اطلاعات احراز هویت صحیح نیستند.",
     "authentication_token" => "کد تأیید شما: :token",
     "user-not-found"       => "کاربر پیدا نشد.",
     "token_sent"           => "کد تأیید برای شما ارسال شد.",
];
