<?php

namespace Modules\Frosty\Notifications;

use App\Models\User;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;

class SendTokenNotification extends YasnaNotificationAbstract
{
    public $token;



    /**
     * RepairOrderSubmittedNotification constructor.
     *
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via(User $notifiable)
    {
        return [
             $this->yasnaSmsChannel(),
        ];
    }



    /**
     * get the sms representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return string
     */
    public function toSms(User $notifiable)
    {
        $message = trans_safe("frosty::messages.authentication_token", [
             'token' => $this->token,
        ]);
        return $message;
    }
}
