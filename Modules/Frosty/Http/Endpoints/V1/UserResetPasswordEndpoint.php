<?php

namespace Modules\Frosty\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frosty\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frosty-user-reset-password
 *                    User Reset Password
 * @apiDescription    reset password
 * @apiVersion        1.0.0
 * @apiName           User Reset Password
 * @apiGroup          Frosty
 * @apiPermission     user
 * @apiParam {String} mobile The mobile of user
 * @apiParam {String} password The new password to be set as the user's password
 * @apiParam {String} password2 The confirmation of the `password` field
 * @apiParam {String} token The token sent via email or SMS.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *           "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb",
 *           "token_type": "bearer",
 *           "expires_in": 3600000
 *      }
 * }
 * @apiErrorExample
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frosty.moreInfo.422",
 *      "errors": {
 *           "password": [
 *                [
 *                     "The Password field is required."
 *                ]
 *           ],
 *           "token": [
 *                [
 *                     "The reset password token is not valid."
 *                ]
 *           ]
 *      }
 * }
 * @method UserController controller()
 */
class UserResetPasswordEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Reset Password";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frosty\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@resetPassword';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "access_token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb",
             "token_type"   => "bearer",
             "expires_in"   => 3600000,
        ]);
    }
}
