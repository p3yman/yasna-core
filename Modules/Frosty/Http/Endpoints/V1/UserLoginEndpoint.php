<?php

namespace Modules\Frosty\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frosty\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frosty-user-login
 *                    User Login
 * @apiDescription    login the user
 * @apiVersion        1.0.0
 * @apiName           User Login
 * @apiGroup          Frosty
 * @apiPermission     user
 * @apiParam {string}  mobile the mobile of user
 * @apiParam {string}  password the password of user
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *          "done": "1"
 *           "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS1jb3JlLnNoXC9hcGlcL21vZHVsYXJcL3YxXC9mcm9zdHktdXNlci1sb2dpbiIsImlhdCI6MTU2Mzc4Nzk1MywiZXhwIjoxNTYzODc0MzUzLCJuYmYiOjE1NjM3ODc5NTMsImp0aSI6IjdZRTFvS2E4enhLWmJ5NjQiLCJzdWIiOjUsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjciLCJwYXlsb2FkIjp7ImlkIjoiREF2UU4iLCJmdWxsX25hbWUiOiJcdTA2NDVcdTA2NDdcdTA2NDRcdTA2MjcgXHUwNjQ1XHUwNjJkXHUwNjQ1XHUwNjJmXHUwNmNjIiwicm9sZXMiOltdfX0.LiXqt4XYyFWVBQwe2R-86NLjrl8zGUMH_Hi6LXZTIjY",
 *           "token_type": "bearer",
 *           "expires_in": 86400
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frosty.moreInfo.422",
 *      "errors": {
 *      "password": [
 *           [
 *                "The Password field is required."
 *           ]
 *      ]
 *      }
 * }
 * @method UserController controller()
 */
class UserLoginEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Login";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frosty\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@login';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             'status'   => 200,
             'metadata' =>
                  [
                       'authenticated_user' => 'qKXVA',
                  ],
             'results'  =>
                  [
                       'access_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS5sb2NhbFwvYXBpXC9tb2R1bGFyXC92MVwvZnJvbnRpZXItbG9naW4iLCJpYXQiOjE1NDk4MTAzMDIsImV4cCI6MTU0OTgxMDkwMiwibmJmIjoxNTQ5ODEwMzAyLCJqdGkiOiJ0MEF6Snl5ZDJyNUtRNEQ1Iiwic3ViIjoxLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.oHlljpPtAKwOqouy0ADuRtuj_MLmFx9a4VMiT2nEh-o',
                       'token_type'   => 'bearer',
                       'expires_in'   => 600,
                  ],
        ]);
    }
}
