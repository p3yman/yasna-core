<?php

namespace Modules\Frosty\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frosty\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frosty-user-edit-profile
 *                    User Edit Profile
 * @apiDescription    edit profile
 * @apiVersion        1.0.0
 * @apiName           User Edit Profile
 * @apiGroup          Frosty
 * @apiPermission     user
 * @apiParam {string} [name_first] the first name of user
 * @apiParam {string} [name_last] the last name of user
 * @apiParam {string} [name_alias] the alias name of user
 * @apiParam {string} [password] the password of user
 * @apiParam {integer} [gender] the gender of user
 * @apiParam {integer} [avatar] the avatar of user
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *           "done"       => 1,
 *           "token"      =>
 *           "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS1jb3JlLnNoXC9hcGlcL21vZHVsYXJcL3YxXC9mcm9zdHktdXNlci1lZGl0LXByb2ZpbGUiLCJpYXQiOjE1NjM5NTI0NDMsImV4cCI6MTU2NzU1MjQ0MywibmJmIjoxNTYzOTUyNDQzLCJqdGkiOiJsWmJqYzM5dGYySHh3UWVIIiwic3ViIjozLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3IiwicGF5bG9hZCI6eyJpZCI6IlBOT0xBIiwiZnVsbF9uYW1lIjoiXHUwNjQ1XHUwNjQ3XHUwNjJmXHUwNmNjIFx1MDY0Nlx1MDY0NVx1MDYzM1x1MDYzNVx1MDYyYlx1MDZjY1x1MDYzNSBcdTA2MzlcdTA2MzhcdTA2Y2NcdTA2NDVcdTA2Y2NmZmYiLCJyb2xlcyI6W10sIm5hbWVfYWxpYXMiOiJcdTA2NDVcdTA2NDdcdTA2MzNcdTA2MjciLCJhdmF0YXJfZ2FtZXIiOiI5IiwibmFtZV9maXJzdCI6Ilx1MDY0NVx1MDY0N1x1MDYyZlx1MDZjYyBcdTA2NDZcdTA2NDVcdTA2MzNcdTA2MzVcdTA2MmJcdTA2Y2NcdTA2MzUiLCJuYW1lX2xhc3QiOiJcdTA2MzlcdTA2MzhcdTA2Y2NcdTA2NDVcdTA2Y2NmZmYiLCJmcm9zdHlfc2NvcmUiOiIxMjAxMiJ9fQ.0_E04Kr7Fe_JcJCrUCF9QqKXdUhToV7ZvvBpU6dcWGY",
 *           "token_type" => "bearer",
 *           "expires_in" => 3600000,
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method UserController controller()
 */
class UserEditProfileEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Edit Profile";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frosty\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@editProfile';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done"       => 1,
             "token"      => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS1jb3JlLnNoXC9hcGlcL21vZHVsYXJcL3YxXC9mcm9zdHktdXNlci1lZGl0LXByb2ZpbGUiLCJpYXQiOjE1NjM5NTI0NDMsImV4cCI6MTU2NzU1MjQ0MywibmJmIjoxNTYzOTUyNDQzLCJqdGkiOiJsWmJqYzM5dGYySHh3UWVIIiwic3ViIjozLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3IiwicGF5bG9hZCI6eyJpZCI6IlBOT0xBIiwiZnVsbF9uYW1lIjoiXHUwNjQ1XHUwNjQ3XHUwNjJmXHUwNmNjIFx1MDY0Nlx1MDY0NVx1MDYzM1x1MDYzNVx1MDYyYlx1MDZjY1x1MDYzNSBcdTA2MzlcdTA2MzhcdTA2Y2NcdTA2NDVcdTA2Y2NmZmYiLCJyb2xlcyI6W10sIm5hbWVfYWxpYXMiOiJcdTA2NDVcdTA2NDdcdTA2MzNcdTA2MjciLCJhdmF0YXJfZ2FtZXIiOiI5IiwibmFtZV9maXJzdCI6Ilx1MDY0NVx1MDY0N1x1MDYyZlx1MDZjYyBcdTA2NDZcdTA2NDVcdTA2MzNcdTA2MzVcdTA2MmJcdTA2Y2NcdTA2MzUiLCJuYW1lX2xhc3QiOiJcdTA2MzlcdTA2MzhcdTA2Y2NcdTA2NDVcdTA2Y2NmZmYiLCJmcm9zdHlfc2NvcmUiOiIxMjAxMiJ9fQ.0_E04Kr7Fe_JcJCrUCF9QqKXdUhToV7ZvvBpU6dcWGY",
             "token_type" => "bearer",
             "expires_in" => 3600000,
        ], [
             "id" => hashid(rand(1, 100)),
        ]);
    }
}
