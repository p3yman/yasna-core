<?php

namespace Modules\Frosty\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frosty\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frosty-user-recovery-password
 *                    User Recovery Password
 * @apiDescription    send request for recovery password
 * @apiVersion        1.0.0
 * @apiName           User Recovery Password
 * @apiGroup          Frosty
 * @apiPermission     user
 * @apiParam {string} mobile the mobile of user
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "status": 200,
 *      "metadata": {
 *           "authenticated_user": "PKEnN"
 *      },
 *      "results": {
 *           "done": "1",
 *           "message": "A reset password token has been sent to someone@somewhere.com."
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frosty.moreInfo.422",
 *      "errors": {
 *           "mobile": [
 *                [
 *                     "The Mobile Number field is required."
 *                ]
 *           ]
 *      }
 * }
 * @method UserController controller()
 */
class UserRecoveryPasswordEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Recovery Password";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frosty\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@tokenRequest';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done"    => "1",
             "message" => "A reset password token has been sent.",
        ]);
    }
}
