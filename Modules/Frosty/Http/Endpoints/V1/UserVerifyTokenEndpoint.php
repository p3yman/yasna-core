<?php

namespace Modules\Frosty\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Frosty\Http\Controllers\V1\UserController;

/**
 * @api               {POST}
 *                    /api/modular/v1/frosty-user-verify-token
 *                    User Verify Token
 * @apiDescription    verify token
 * @apiVersion        1.0.0
 * @apiName           User Verify Token
 * @apiGroup          Frosty
 * @apiPermission     user
 * @apiParam {string} mobile the mobile of the user
 * @apiParam {string} token The token sent via SMS.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "id": "hA5h1d"
 *      },
 *      "results": {
 *          "done": "1"
 *           "access_token":
 *           "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95YXNuYS1jb3JlLnNoXC9hcGlcL21vZHVsYXJcL3YxXC9mcm9zdHktdXNlci1sb2dpbiIsImlhdCI6MTU2Mzc4Nzk1MywiZXhwIjoxNTYzODc0MzUzLCJuYmYiOjE1NjM3ODc5NTMsImp0aSI6IjdZRTFvS2E4enhLWmJ5NjQiLCJzdWIiOjUsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjciLCJwYXlsb2FkIjp7ImlkIjoiREF2UU4iLCJmdWxsX25hbWUiOiJcdTA2NDVcdTA2NDdcdTA2NDRcdTA2MjcgXHUwNjQ1XHUwNjJkXHUwNjQ1XHUwNjJmXHUwNmNjIiwicm9sZXMiOltdfX0.LiXqt4XYyFWVBQwe2R-86NLjrl8zGUMH_Hi6LXZTIjY",
 *           "token_type": "bearer",
 *           "expires_in": 86400
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": 422,
 *      "moreInfo": "frosty.moreInfo.422",
 *      "errors": {
 *      "password": [
 *           [
 *                "Authentication information is not correct"
 *           ]
 *      ]
 *      }
 * }
 * @method UserController controller()
 */
class UserVerifyTokenEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Verify Token";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Frosty\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UserController@verifyToken';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ]);
    }
}
