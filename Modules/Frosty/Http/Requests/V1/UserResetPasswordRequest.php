<?php

namespace Modules\Frosty\Http\Requests\V1;


class UserResetPasswordRequest extends PasswordRecoveryAbstractRequest
{
    /**
     * Whether the token field is required.
     *
     * @var bool
     */
    protected $token_required = true;



    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'password',
             'password2',
             'mobile',
             'token',
        ];
    }



    /**
     * The Password of the Rules
     *
     * @return array
     */
    public function passwordRules()
    {
        return [
             "password" => "required|string|min:6|same:password2",
        ];
    }



    /**
     * The Token parts of the Rules
     *
     * @return array
     */
    public function tokenRules()
    {
        return [
             'token' => 'required|' . $this->tokenValidation(),
        ];
    }
}
