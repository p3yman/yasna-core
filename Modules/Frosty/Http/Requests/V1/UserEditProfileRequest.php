<?php

namespace Modules\Frosty\Http\Requests\V1;

use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class UserEditProfileRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "User";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = false;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "name_first",
             "name_last",
             "name_alias",
             "gender",
             "password",
             "password2",
             "avatar_gamer",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name_first'   => 'required|persian:60',
             'name_last'    => 'required|persian:60',
             'name_alias'   => 'required|persian:60',
             'gender'       => 'required|integer|in:1,2',
             'password'     => 'same:password2|min:8|max:50',
             'avatar_gamer' => 'required|integer|in:1,9',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name_first' => 'pd',
             'name_last'  => 'pd',
             'name_alias' => 'pd',
        ];
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {
        $this->unsetPassword2();
        $this->hashPassword();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("frosty::attributes");
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        if (!$this->getData('password')) {
            $this->unsetData('password');
        }
    }



    /**
     * @inheritdoc
     */
    protected function loadModel()
    {
        $this->model = $this->user();
    }



    /**
     * unset password2
     *
     * @return void
     */
    private function unsetPassword2()
    {
        $this->unsetData('password2');
    }



    /**
     * hash the password.
     *
     * @return void
     */
    private function hashPassword()
    {
        $password = $this->getData('password');
        if ($password) {
            $this->setData('password', Hash::make($password));
            $this->setData('reset_token', null);
            $this->setData('reset_token_expires_at', null);
        }
    }
}
