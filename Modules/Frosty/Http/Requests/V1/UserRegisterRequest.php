<?php

namespace Modules\Frosty\Http\Requests\V1;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class UserRegisterRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "User";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'name_first'           => 'required|persian:60',
             'name_last'            => 'required|persian:60',
             'name_alias'           => 'required|persian:60',
             'gender'               => 'required|integer|in:1,2',
             'mobile'               => [
                  'required',
                  'phone:mobile',
                  Rule::unique('users'),
             ],
             'password'             => 'required|same:password2|min:8|max:50',
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
        ];
    }



    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [
             "neighborhood.accepted" => trans("mobile-salam::validation.neighborhood_not_valid"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'name_first' => 'pd',
             'name_last'  => 'pd',
             'name_alias' => 'pd',
             'mobile'     => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'name_first',
             'name_last',
             'name_alias',
             'gender',
             'mobile',
             'password',
             'password2',
             'g-recaptcha-response',
        ];
    }



    /**
     * @inheritDoc
     */
    public function mutators()
    {
        $this->setHashPassword();
        $this->unsetPassword2();
        $this->unsetGRecaptcha();
    }



    /**
     * hash the password
     *
     * @return void
     */
    protected function setHashPassword()
    {
        $password = $this->getData('password');

        $password_hash = Hash::make($password);

        $this->setData('password', $password_hash);
    }



    /**
     * unset password2
     *
     * @return void
     */
    protected function unsetPassword2()
    {
        $this->unsetData('password2');
    }



    /**
     * unset g-recaptcha-response
     *
     * @return void
     */
    protected function unsetGRecaptcha()
    {
        $this->unsetData('g-recaptcha-response');
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("frosty::attributes");
    }

}
