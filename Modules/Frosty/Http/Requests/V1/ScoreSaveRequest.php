<?php

namespace Modules\Frosty\Http\Requests\V1;

use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class ScoreSaveRequest extends YasnaFormRequest
{
    /**
     * @inheritdoc
     */
    protected $model_name = "FrostyScore";

    /**
     * @inheritdoc
     */
    protected $should_allow_create_mode = true;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "point",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "point" => "required",
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        //TODO set the browser and os
        $this->setData('user_id', user()->id);
        $this->setData('ip', request()->ip());
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("frosty::attributes");
    }

}
