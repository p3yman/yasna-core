<?php

namespace Modules\Frosty\Http\Requests\V1;

class UserRecoveryPasswordRequest extends PasswordRecoveryAbstractRequest
{
    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'mobile'
        ];
    }
}
