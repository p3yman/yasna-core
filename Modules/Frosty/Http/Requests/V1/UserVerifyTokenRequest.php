<?php

namespace Modules\Frosty\Http\Requests\V1;

class UserVerifyTokenRequest extends PasswordRecoveryAbstractRequest
{
    /**
     * @inheritDoc
     */
    public function fillableFields()
    {
        return [
             'mobile',
             'token',
        ];
    }



    /**
     * The Token parts of the Rules
     *
     * @return array
     */
    public function tokenRules()
    {
        return [
             'token'                => 'required|' . $this->tokenValidation(),
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
        ];
    }
}
