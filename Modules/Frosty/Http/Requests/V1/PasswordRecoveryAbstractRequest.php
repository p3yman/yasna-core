<?php

namespace Modules\Frosty\Http\Requests\V1;


use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;

/**
 * Class PasswordResetAbstractRequest
 * <br>
 * The request to be used in all the steps of the password reset process.
 *
 * @property User $model
 */
abstract class  PasswordRecoveryAbstractRequest extends YasnaFormRequest
{

    protected $model_name = 'User';

    protected $should_allow_create_mode = false;



    /**
     * @inheritDoc
     */
    public function authorize()
    {
        return [
             $this->model->exists,
        ];
    }



    /**
     * The Rules to Validate Channel Info
     *
     * @return array
     */
    public function mainRules()
    {
        return [
             'mobile' => 'required|phone:mobile|' . $this->userValidation(),
        ];
    }



    /**
     * check existing user
     *
     * @return string
     */
    protected function userValidation()
    {
        if ($this->model->exists) {
            return '';
        }
        return 'invalid';
    }



    /**
     * @inheritDoc
     */
    public function loadModel()
    {
        $model = model('user')->Where('mobile', $this->getData('mobile'))->first();
        $user  = $model ?? null;
        if ($user && $user->exists) {
            $this->model = $user;
        }
    }



    /**
     * @inheritDoc
     */
    public function messages()
    {
        return [
             'mobile.invalid' => trans_safe('frosty::messages.user-not-found'),
             'token.invalid'  => trans_safe('frosty::messages.authentication_error'),
        ];
    }



    /**
     * token validation.
     *
     * @return string
     */
    public function tokenValidation()
    {
        $token = $this->getData('token');
        if ($this->userTokenIsValid($token)) {
            return '';
        }

        return 'invalid';

    }



    /**
     * Whether the specified token is acceptable for the found user.
     *
     * @param string $token
     *
     * @return bool
     */
    protected function userTokenIsValid($token)
    {
        $user = $this->model;
        return (
            // check if token matchs
             Hash::check($token, $user->reset_token)
             and
             // check if token has not been expired
             Carbon::parse($user->reset_token_expires_at)->greaterThan(now())
        );
    }

}
