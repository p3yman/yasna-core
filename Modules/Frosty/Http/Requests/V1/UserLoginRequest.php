<?php

namespace Modules\Frosty\Http\Requests\V1;

use Illuminate\Support\Facades\Hash;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserLoginRequest extends YasnaFormRequest
{
    /**
     * keep JWT token, in case of successful login.
     *
     * @var null
     */
    public $token = null;

    /**
     * @inheritdoc
     */
    protected $automatic_injection_guard = false;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "mobile"               => 'required|phone:mobile',
             'password'             => 'required',
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
             "credentials"          => $this->token ? "" : "accepted",
        ];
    }


    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->attempt();
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "credentials.accepted" => trans("frosty::messages.authentication_error"),
        ];
    }



    /**
     * manually attempt to login the user
     *
     * @return void
     */
    private function attempt()
    {
        $this->setTokenValidityTime();

        $user = user()->Where('mobile', $this->getData('mobile'))->first();

        if (!$user or !Hash::check($this->getData('password'), $user->password)) {
            return;
        }

        $this->token = JWTAuth::fromUser($user);
    }



    /**
     * Sets the token validity time from the settings.
     *
     * @return void
     */
    private function setTokenValidityTime()
    {
        $days = (int)get_setting('token_validity_time');
        if (!$days) {
            $days = 1;
        }

        $minutes = $days * 24 * 60;

        auth('api')->factory()->setTTL($minutes);
    }
}
