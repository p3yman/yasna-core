<?php

namespace Modules\Frosty\Http\Controllers\V1;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Modules\Frosty\Http\Requests\V1\UserEditProfileRequest;
use Modules\Frosty\Http\Requests\V1\UserLoginRequest;
use Modules\Frosty\Http\Requests\V1\UserRecoveryPasswordRequest;
use Modules\Frosty\Http\Requests\V1\UserRegisterRequest;
use Modules\Frosty\Http\Requests\V1\UserResetPasswordRequest;
use Modules\Frosty\Http\Requests\V1\UserVerifyTokenRequest;
use Modules\Frosty\Notifications\SendTokenNotification;
use Modules\Yasna\Services\YasnaApiController;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends YasnaApiController
{

    /**
     * register the user.
     *
     * @param UserRegisterRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function register(UserRegisterRequest $request)
    {
        /**@var $user User */
        $user = $request->model->batchSave($request);
        $user->attachRole('gamer');

        $token = JWTAuth::fromUser($user);

        if (!$token) {
            return $this->clientError('40001');
        }

        return $this->typicalSaveFeedback(true, [
             "id"           => hashid($user->id),
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ]);
    }



    /**
     * login the user and save token in the session
     *
     * @param UserLoginRequest $request
     *
     * @return array
     */
    public function login(UserLoginRequest $request)
    {
        session()->put('_token', $request->token);

        return $this->success([
             "access_token" => $request->token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ]);
    }



    /**
     * Generates a password reset token and send it to user.
     *
     * @param UserRecoveryPasswordRequest $request
     *
     * @return array
     */
    public function tokenRequest(UserRecoveryPasswordRequest $request)
    {
        /** @var User $user */
        $user = $request->model;

        // save and send token
        $this->sendUserToken($user);

        // return response
        return $this->success([
             'done'    => 1,
             'message' => trans_safe("frosty::messages.token_sent"),
        ]);
    }



    /**
     * Saves a reset password token for the specified user and send by SMS.
     *
     * @param User $user
     *
     * @return void
     */
    private function sendUserToken(User $user)
    {
        $token = $this->generateToken();

        $reset_token            = Hash::make($token);
        $reset_token_expires_at = now()->addMinutes(5)->toDateTimeString();

        $user->batchSave(compact('reset_token', 'reset_token_expires_at'));

        $user->notifyNow(new SendTokenNotification($token));
    }



    /**
     * Generates a new token for resetting password.
     *
     * @return string
     */
    private function generateToken(): string
    {
        return rand(str_repeat(1, 6), str_repeat(9, 6));
    }



    /**
     * verify token.
     *
     * @param UserVerifyTokenRequest $request
     *
     * @return array
     */
    public function verifyToken(UserVerifyTokenRequest $request)
    {
        return $this->success([
             'done' => 1,
        ]);
    }



    /**
     * save new password.
     *
     * @param UserResetPasswordRequest $request
     *
     * @return array
     */
    public function resetPassword(UserResetPasswordRequest $request)
    {
        $user     = $request->model;
        $password = $request->password;

        $user->batchSave([
             'password'               => Hash::make($password),
             'reset_token'            => null,
             'reset_token_expires_at' => null,
        ]);

        auth()->login($user);

        return $this->success($this->regenerateTokenForNewPassword($user, $password));
    }



    /**
     * edit profile
     *
     * @param UserEditProfileRequest $request
     *
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function editProfile(UserEditProfileRequest $request)
    {
        $user = $request->model;

        $user->batchSave($request);
        return $this->success([
             'done'       => 1,
             'token'      => JWTAuth::fromUser($user),
             "token_type" => "bearer",
             "expires_in" => auth('api')->factory()->getTTL() * 60,
        ]);
    }



    /**
     * Generates a new token after saving the new password.
     *
     * @param User   $user
     * @param string $password
     *
     * @return array|null
     */
    private function regenerateTokenForNewPassword(User $user, string $password): ?array
    {
        $token = auth('api')->attempt([
             $user->usernameField() => $user->username,
             "password"             => $password,
        ]);

        if (!$token) {
            return null;
        }

        $token_info = [
             "access_token" => $token,
             "token_type"   => "bearer",
             "expires_in"   => auth('api')->factory()->getTTL() * 60,
        ];

        return $token_info;
    }


}
