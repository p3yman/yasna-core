<?php

namespace Modules\Frosty\Http\Controllers\V1;

use Modules\Frosty\Http\Requests\V1\ScoreSaveRequest;
use Modules\Yasna\Services\YasnaApiController;

class FrostyScoreController extends YasnaApiController
{

    /**
     * save the score
     *
     * @param ScoreSaveRequest $request
     *
     * @return array
     */
    public function saveScore(ScoreSaveRequest $request)
    {
        $saved         = $request->model->batchSave($request);
        $highest_score = model('frosty_score')
             ->where('user_id', $saved->user_id)
             ->orderby('point', 'asc')
             ->pluck('point')
             ->last()
        ;
        user($saved->user_id)->batchSave([
             'frosty_score' => $highest_score,
        ]);

        return $this->modelSaveFeedback($saved);
    }
}
