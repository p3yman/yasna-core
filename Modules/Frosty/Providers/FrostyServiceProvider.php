<?php

namespace Modules\Frosty\Providers;

use Modules\Frosty\Http\Endpoints\V1\ScoreSaveEndpoint;
use Modules\Frosty\Http\Endpoints\V1\UserEditProfileEndpoint;
use Modules\Frosty\Http\Endpoints\V1\UserLoginEndpoint;
use Modules\Frosty\Http\Endpoints\V1\UserRecoveryPasswordEndpoint;
use Modules\Frosty\Http\Endpoints\V1\UserRegisterEndpoint;
use Modules\Frosty\Http\Endpoints\V1\UserResetPasswordEndpoint;
use Modules\Frosty\Http\Endpoints\V1\UserVerifyTokenEndpoint;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class FrostyServiceProvider
 *
 * @package Modules\Frosty\Providers
 */
class FrostyServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerEndPoint();
        $this->registerModelTraits();
    }



    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndPoint()
    {
        endpoint()->register(UserRegisterEndpoint::class);
        endpoint()->register(UserLoginEndpoint::class);
        endpoint()->register(UserRecoveryPasswordEndpoint::class);
        endpoint()->register(UserVerifyTokenEndpoint::class);
        endpoint()->register(UserResetPasswordEndpoint::class);
        endpoint()->register(UserEditProfileEndpoint::class);
        endpoint()->register(ScoreSaveEndpoint::class);
    }

    /**
     * register ModelTraits
     *
     * @return void
     */
    private function registerModelTraits()
    {
        $this->addModelTrait("UserDataTrait", "User");
    }
}
