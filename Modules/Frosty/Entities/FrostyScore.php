<?php

namespace Modules\Frosty\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class FrostyScore extends YasnaModel
{
    use SoftDeletes;
}
