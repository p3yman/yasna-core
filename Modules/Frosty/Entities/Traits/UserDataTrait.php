<?php

namespace Modules\Frosty\Entities\Traits;

trait UserDataTrait
{

    /**
     * get the vary basic user token claims
     *
     * @return array
     */
    public function getJWTCustomClaimsFrosty()
    {
        return [
             "name_alias"   => $this->name_alias,
             "avatar_gamer" => $this->avatar_gamer,
             "name_first"   => $this->name_first,
             "name_last"    => $this->name_last,
             "frosty_score" => $this->frosty_score,
             "gender"       => $this->gender,
             "mobile"       => $this->mobile,
        ];
    }
}
