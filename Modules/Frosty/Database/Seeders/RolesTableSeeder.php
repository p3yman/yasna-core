<?php

namespace Modules\Frosty\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Yasna\Services\ModuleTraits\ModuleRecognitionsTrait;

class RolesTableSeeder extends Seeder
{

    use ModuleRecognitionsTrait;



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        yasna()->seed('roles', $this->data());
    }



    /**
     * Return the data to seed.
     *
     * @return array
     */
    protected function data()
    {

        return [
             [
                  'slug'         => 'gamer',
                  'title'        => $this->runningModule()->getTrans('seeder.gamer'),
                  'plural_title' => $this->runningModule()->getTrans('seeder.gamers'),
             ],
        ];
    }
}
