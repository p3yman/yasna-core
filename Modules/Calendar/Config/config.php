<?php

return [
     'name'            => 'Calendar',
     'google_calendar' => [
          'clientID'      => env("GOOGLE_ACCESS_CLIENT_ID", null),
          'projectID'     => env("GOOGLE_ACCESS_PROJECT_ID", null),
          'auth_uri'      => env("GOOGLE_ACCESS_AUTH_URI", "https://accounts.google.com/o/oauth2/auth"),
          'token_uri'     => env("GOOGLE_ACCESS_TOKEN_URI", "https://accounts.google.com/o/oauth2/token"),
          'auth_provider' => env("GOOGLE_ACCESS_AUTH_PROVIDER", "https://www.googleapis.com/oauth2/v1/certs"),
          'client_secret' => env("GOOGLE_ACCESS_CLIENT_SECRET", null),
          'app_name'      => env("GOOGLE_CALENDAR_APP_NAME", 'Yasna'),
          'summary'       => env("GOOGLE_CALENDAR_SUMMARY", 'Yasna Calendar'),
          'access_type'   => env("GOOGLE_ACCESS_TYPE", 'web'),
     ],
];
