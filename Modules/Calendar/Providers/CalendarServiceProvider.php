<?php

namespace Modules\Calendar\Providers;

use Modules\Calendar\Events\GoogleCalendarChannelCreated;
use Modules\Calendar\Events\GoogleCalendarDeleted;
use Modules\Calendar\Events\GoogleCalendarEventDeleted;
use Modules\Calendar\Events\GoogleCalendarCreated;
use Modules\Calendar\Events\GoogleCalendarEventCreated;
use Modules\Calendar\Events\GoogleCalendarSync;
use Modules\Calendar\Events\GoogleCalendarUpdated;
use Modules\Calendar\Events\GoogleEventUpdated;
use Modules\Calendar\Listeners\CreateGoogleCalendarChannelCreated;
use Modules\Calendar\Listeners\DeleteRemoteEventOnGoogleDeleted;
use Modules\Calendar\Listeners\DeleteRemoteCalendarOnGoogleDeleted;
use Modules\Calendar\Listeners\CreateRemoteEventOnGoogleCreated;
use Modules\Calendar\Listeners\CreateRemoteCalendarOnGoogleCreated;
use Modules\Calendar\Listeners\SyncRemoteCalendarsOnGoogleSync;
use Modules\Calendar\Listeners\UpdateRemoteCalendarOnGoogleUpdated;
use Modules\Calendar\Listeners\UpdateRemoteEventOnGoogleUpdated;
use Modules\Calendar\Schedules\RebuildingCalendarChannelSchedule;
use Modules\Calendar\Schedules\RebuildingCalendarEventChannelSchedule;
use Modules\Calendar\Schedules\StopCalendarChannelSchedule;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class CalendarServiceProvider
 *
 * @package Modules\Calendar\Providers
 */
class CalendarServiceProvider extends YasnaProvider
{

    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->registerSidebar();
        $this->registerBottomAssets();
        $this->registerTopAssets();
        $this->registerTraits();
        $this->registerListeners();
        $this->registerRoleSampleModules();
        $this->registerSchedules();
    }



    /**
     * add calender item in sidebar
     */
    public function registerSidebar()
    {
        module('manage')
             ->service('sidebar')
             ->add('calendar')
             ->blade('calendar::sidebar')
             ->order(40)
             ->permit("")
        ;
    }



    /**
     *Load custom js and css files
     */
    public function registerBottomAssets()
    {
        module('manage')
             ->service('template_bottom_assets')
             ->add()
             ->link("calendar:js/calendar-scripts.js")
             ->order(40)
        ;
    }



    /**
     *Load js and css for new full calendar
     */
    public function registerTopAssets()
    {
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:libs/new-fullcalendar/css/fullcalendar.css")
             ->order(35)
        ;
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:libs/new-fullcalendar/libs/moment.min.js")
             ->order(37)
        ;
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:libs/new-fullcalendar/libs/moment-jalaali.js")
             ->order(38)
        ;
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:libs/new-fullcalendar/js/fullcalendar.min.js")
             ->order(39)
        ;
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:libs/new-fullcalendar/js/fa.js")
             ->order(40)
        ;

        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:js/fullcalendar-config.js")
             ->order(41)
        ;


        module('manage')
             ->service('template_assets')
             ->add("calendar-module-styles")
             ->link("calendar:css/calendar-styles.min.css")
             ->order(42)
        ;
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:js/fileinput/fileinput.js")
             ->order(38)
        ;
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:js/fileinput/locales/fa.js")
             ->order(39)
        ;
        module('manage')
             ->service('template_assets')
             ->add()
             ->link("calendar:css/fileinput.css")
             ->order(41)
        ;
    }



    /**
     *relation calendar by user
     */
    public function registerTraits()
    {
        $this->addModelTrait("UserCalendarTrait", "User");
        $this->addModelTrait("RemarkCalendarTrait", "Remark");
    }



    /**
     * register listeners of this module.
     */
    protected function registerListeners()
    {
        $this->listen(GoogleCalendarCreated::class, CreateRemoteCalendarOnGoogleCreated::class);
        $this->listen(GoogleCalendarEventCreated::class, CreateRemoteEventOnGoogleCreated::class);
        $this->listen(GoogleCalendarDeleted::class, DeleteRemoteCalendarOnGoogleDeleted::class);
        $this->listen(GoogleCalendarEventDeleted::class, DeleteRemoteEventOnGoogleDeleted::class);
        $this->listen(GoogleCalendarUpdated::class, UpdateRemoteCalendarOnGoogleUpdated::class);
        $this->listen(GoogleEventUpdated::class, UpdateRemoteEventOnGoogleUpdated::class);
        $this->listen(GoogleCalendarSync::class, SyncRemoteCalendarsOnGoogleSync::class);
        $this->listen(GoogleCalendarChannelCreated::class, CreateGoogleCalendarChannelCreated::class);
    }



    /**
     * Module string, to be used as sample for role definitions.
     */
    public function registerRoleSampleModules()
    {
        module('users')
             ->service('role_sample_modules')
             ->add('calendar')
             ->value('browse ,  create ,  edit  ,  delete')
        ;
        module('users')
             ->service('role_sample_modules')
             ->add('calendar-event')
             ->value('browse ,  create ,  edit  ,  delete')
        ;
        module('users')
             ->service('role_sample_modules')
             ->add('calendar-comments')
             ->value('browse ,  create ,  edit  ,  delete')
        ;
    }



    /**
     * Register Scheduled Tasks
     */
    public function registerSchedules()
    {
        $this->addSchedule(RebuildingCalendarChannelSchedule::class);
        $this->addSchedule(RebuildingCalendarEventChannelSchedule::class);
    }
}
