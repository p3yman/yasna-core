<?php

namespace Modules\Calendar\Schedules;

use Exception;
use Modules\Calendar\Classes\GCalendarChannel;
use Modules\Yasna\Services\YasnaSchedule;

class RebuildingCalendarChannelSchedule extends YasnaSchedule
{

    public $channel;



    /**
     * RebuildingCalendarChannelSchedule constructor.
     *
     * @param $schedule
     */
    public function __construct($schedule)
    {
        $this->channel = new GCalendarChannel();
        parent::__construct($schedule);
    }



    /**
     * stop old channel and create new channel for sync Calendar
     *
     * @throws Exception
     */
    protected function job()
    {
        $channels = model('google-channel')->get();
        if (!$channels->isEmpty()) {
            foreach ($channels as $channel) {
                if ($channel->calendar_id === null) {
                    $user  = model('user', $channel->user_id);
                    $token = $user->preference('GoogleCalendar')['access_token'];

                    $this->channel->stopChannel($channel, $token);
                    $this->channel->createChannel($token, $channel->user_id);
                }

            }
        }
    }



    /**
     * duration of run schedule
     *
     * @return string
     */
    protected function frequency()
    {
        return 'weeklyOn:1,00:00';
    }

}
