<?php

namespace Modules\Calendar\Schedules;

use Exception;
use Modules\Calendar\Classes\GEventChannel;
use Modules\Yasna\Services\YasnaSchedule;

class RebuildingCalendarEventChannelSchedule extends YasnaSchedule
{

    public $channel;



    /**
     * RebuildingCalendarEventChannelSchedule constructor.
     *
     * @param $schedule
     */
    public function __construct($schedule)
    {
        $this->channel = new GEventChannel();
        parent::__construct($schedule);
    }



    /**
     * stop old channel and create new channel for sync Calendar event
     *
     * @throws Exception
     */
    protected function job()
    {
        $channels = model('google-channel')->get();
        if (!$channels->isEmpty()) {
            foreach ($channels as $channel) {
                if ($channel->calendar_id !== null) {
                    $user  = model('user', $channel->user_id);
                    $token = $user->preference('GoogleCalendar')['access_token'];

                    $this->channel->stopChannel($channel, $token);
                    $this->channel->createChannel($channel->calendar_id, $token, $channel->user_id);
                }

            }
        }
    }



    /**
     * duration of run schedule
     *
     * @return string
     */
    protected function frequency()
    {
        return 'weeklyOn:1,02:00';
    }
}
