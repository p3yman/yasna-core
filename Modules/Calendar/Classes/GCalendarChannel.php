<?php

namespace Modules\Calendar\Classes;


use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use Webpatser\Uuid\Uuid;

class GCalendarChannel
{


    /**
     * Stop the channel before expiration
     *
     * @param Collection $channel
     * @param string     $token
     */
    public function stopChannel($channel, $token)
    {
        $url = sprintf("https://www.googleapis.com/calendar/v3/channels/stop");

        $fields  = $this->setStopParameters($channel);
        $headers = $this->setStopHeaders($token);

        /* send POST request */
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($channel, CURLOPT_URL, $url);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_POST, true);
        curl_setopt($channel, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($channel, CURLOPT_TIMEOUT, 3);
        $response = curl_exec($channel);

        curl_close($channel);
    }



    /**
     * setup the POST parameters
     *
     * @param Collection $channel
     *
     * @return false|string
     */
    public function setStopParameters($channel)
    {
        return json_encode([
             'id'         => $channel->channel_id,
             'resourceId' => $channel->resource_id,
        ]);
    }



    /**
     * setup POST headers
     *
     * @param string $token
     *
     * @return array
     */
    public function setStopHeaders($token)
    {
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;

        return $headers;
    }



    /**
     * create channel for syn calendars from google on app
     *
     * @param string $token
     * @param int    $user_id
     *
     * @return bool
     * @throws Exception
     */
    public function createChannel($token, $user_id)
    {
        $url = sprintf("https://www.googleapis.com/calendar/v3/users/me/calendarList/watch");

        $fields  = $this->setParameters();
        $headers = $this->setHeaders($token);
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($channel, CURLOPT_URL, $url);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_POST, true);
        curl_setopt($channel, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($channel, CURLOPT_TIMEOUT, 3);
        $response = curl_exec($channel);

        if (isset(json_decode($response, true)['error']) || !$response) {
            return false;
        }

        $this->saveChannel($response, $user_id);

        curl_close($channel);

        return true;
    }



    /**
     * setup POST headers
     *
     * @param string $token
     *
     * @return array
     */
    public function setHeaders($token)
    {
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;

        return $headers;
    }



    /**
     * setup the POST parameters
     *
     * @return false|string
     * @throws Exception
     */
    public function setParameters()
    {
        $uuid = Uuid::generate();
        return json_encode([
             'id'      => $uuid->string,
             'type'    => "web_hook",
             'token'   => "token_$uuid->string",
             'address' => sprintf("https://cb.yasna.team/callback/b1071d6a-ce5b-4268-8f4e-3a6668c349e6"),
        ]);
    }



    /**
     * save channel response in DB
     *
     * @param JsonResource $response
     * @param int          $user_id
     */
    public function saveChannel($response, $user_id)
    {
        $response = json_decode($response);
        $model    = model('google-channel')->where('user_id', $user_id)->whereNull('calendar_id')->first();

        if (!$model) {
            $model = model('google-channel');
        }

        $model->batchSave([
             'user_id'      => $user_id,
             'channel_id'   => $response->id,
             'resource_id'  => $response->resourceId,
             'resource_uri' => $response->resourceUri,
             'token'        => $response->token,
             'expiration'   => $response->expiration,
        ]);

    }
}
