<?php
/**
 * Created by PhpStorm.
 * User: habibe
 * Date: 1/27/19
 * Time: 3:43 PM
 */

namespace Modules\Calendar\Classes;

use DateTime;
use Modules\Calendar\Entities\CalendarEvent;
use Modules\Calendar\Entities\EventNotification;

class GEvent
{

    public $event;



    /**
     * GEvent constructor.
     *
     * @param CalendarEvent $data
     */
    public function __construct($data)
    {
        $this->event = $data;
    }



    /**
     * return event notifications
     *
     * @return \Illuminate\Support\Collection
     */
    public function getNotifications()
    {
        $notifications = $this->event->notifications()->get();
        if (!$notifications->isEmpty()) {
            if ($this->event->all_day == 0) {
                $result = $this->convertTime($notifications);
            } else {
                $result = $this->convertTimeWithAllDay($notifications);
            }
            return collect($result);
        }
        return;
    }



    /**
     * convert notification time to minuets
     *
     * @param EventNotification $notifications
     *
     * @return array
     */
    public function convertTime($notifications)
    {
        //example : ['method' => 'email', 'minutes' => 24 * 60]
        foreach ($notifications as $key => $notification) {
            if ($notification->type == 0) {
                $notification->type = 'email';
            } else {
                $notification->type = 'popup';
            }
            $notify_meta            = $notification['meta']['notify'];
            $number_time            = $notify_meta['number_time'];
            $time_type              = $notify_meta['time_type'];
            $result[$key]['method'] = $notification->type;
            if ($time_type == '0') {//minutes
                $result[$key]['minutes'] = $number_time;
            }
            if ($time_type == '1') {//hours
                $result[$key]['minutes'] = $number_time * 60;
            }
            if ($time_type == '2') {//day
                $result[$key]['minutes'] = $number_time * 24 * 60;
            }
            if ($time_type == '3') {//week
                $result[$key]['minutes'] = $number_time * 7 * 24 * 60;
            }

        }
        return $result;
    }



    /**
     * convert date to minutes if all-day checked
     *
     * @param Collection $notifications
     *
     * @return array
     */
    public function convertTimeWithAllDay($notifications)
    {

        //example : ['method' => 'email', 'minutes' => 24 * 60]
        foreach ($notifications as $key => $notification) {
            if ($notification->type == 0) {
                $notification->type = 'email';
            } else {
                $notification->type = 'popup';
            }
            $notify_meta            = $notification['meta']['notify'];
            $number_time            = $notify_meta['number_time'];
            $time_type              = $notify_meta['time_type'];
            $time_array             = explode(':', $notify_meta['time']);
            $time_minutes           = $this->convertToMinutes($time_array);
            $result[$key]['method'] = $notification->type;
            if ($time_type == '2') {//day
                $result[$key]['minutes'] = $number_time * 24 * 60 - $time_minutes;
            }
            if ($time_type == '3') {//week
                $result[$key]['minutes'] = $number_time * 7 * 24 * 60 - $time_minutes;
            }

        }
        return $result;
    }



    /**
     * return  event calendar for listener
     *
     * @return CalendarEvent
     */
    public function getEvent()
    {
        return $this->event;
    }



    /**
     * relation event by calendar
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getCalendar()
    {
        return $this->getEvent()->calendar();
    }



    /**
     * convert date according to RFC3339
     *
     * @param string $date
     *
     * @return string
     * @throws \Exception
     */
    public function convertDate($date)
    {
        $datetime = new DateTime($date);
        $time     = $datetime->format(DateTime::ATOM);

        return $time;
    }



    /**
     * return allDay bool
     *
     * @param int $all_day
     *
     * @return bool
     */
    public function getAllDay($all_day)
    {
        return boolval($all_day);
    }



    /**
     * convert date for google api (delete time of string)
     *
     * @param string $date
     *
     * @return array
     */
    public function convertDateWithAllDay($date)
    {
        $date = explode(' ', $date);

        return $date[0];
    }



    /**
     * convert time to minutes
     *
     * @param string $time
     *
     * @return float|int
     */
    public function convertToMinutes($time)
    {
        $hour    = $time[0] * 60;
        $minutes = $hour + $time[1];

        return $minutes;
    }


}
