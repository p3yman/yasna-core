<?php

namespace Modules\Calendar\Classes;

use Google_Client;
use Google_Exception;
use Google_Service_Calendar;

class GClient
{


    /**
     * get config to send the request
     *
     * @return array
     */
    protected function getConfig()
    {
        return [
             'auth_config_path' => GConfig::getAuthConfig(),
             'application_name' => GConfig::getAppName(),
             'access_type'      => GConfig::getAccessType(),
             'redirect_url'     => route('google_set_auth_code'),
             'calendar_summary' => GConfig::getCalendarSummary(),
             'scopes'           =>
                  [
                       Google_Service_Calendar::CALENDAR,
                  ],
        ];
    }



    /**
     * initialize request
     *
     * @return Google_Client
     * @throws Google_Exception
     */
    public function initialClient()
    {
        $calendar_config = $this->getConfig();
        $client          = new Google_Client();

        $client->setApplicationName($calendar_config['application_name']);
        $client->setScopes($calendar_config['scopes']);
        $client->setRedirectUri($calendar_config['redirect_url']);
        $client->setAuthConfig($calendar_config['auth_config_path']);
        $client->setAccessType($calendar_config['access_type']);

        return $client;
    }



    /**
     * get auth url
     *
     * @return string
     * @throws Google_Exception
     */
    public function generateAccessLink()
    {
        return $this->initialClient()->createAuthUrl();
    }



    /**
     * get client data by API
     *
     * @param int $user_id
     *
     * @return Google_Client
     * @throws Google_Exception
     */
    public function getClient($user_id = 0)
    {
        $client      = $this->setConfig();
        $accessToken = $this->accessToken($user_id);
        if ($accessToken) {
            $client->setAccessToken($accessToken);
        }
        if ($client->isAccessTokenExpired()) {
            $refresh_token = $this->refreshToken($user_id);
            if ($refresh_token) {
                $client->fetchAccessTokenWithRefreshToken($refresh_token);
            } else {
                $accessToken = $this->accessToken($user_id);
                $client->setAccessToken($accessToken);
            }
        }
        return $client;
    }



    /**
     * make instance of Google_Service_Calendar
     *
     * @param int $user_id
     *
     * @return Google_Service_Calendar
     * @throws Google_Exception
     */
    public function makeService($user_id = 0)
    {
        $client  = $this->getClient($user_id);
        $service = new Google_Service_Calendar($client);

        return $service;
    }



    /**
     * get access token on user table
     *
     * @param int $user_id
     *
     * @return string
     */
    public function accessToken($user_id = 0)
    {
        return model('user', $user_id)->preference('GoogleCalendar')['access_token'];
    }



    /**
     * get refresh token on user table
     *
     * @param int $user_id
     *
     * @return string
     */
    public function refreshToken($user_id = 0)
    {
        return model('user', $user_id)->preference('GoogleCalendar')['refresh_token'] ?? '';
    }



    /**
     * set config for insert calendar by API
     *
     * @return Google_Client
     * @throws Google_Exception
     */
    public function setConfig()
    {
        $client          = new Google_Client();
        $calendar_config = $this->getConfig();
        $client->setApplicationName($calendar_config['application_name']);
        $client->setAuthConfig($calendar_config['auth_config_path']);
        $client->setScopes(Google_Service_Calendar::CALENDAR);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');


        return $client;
    }



    /**
     * check connecting to google
     *
     * @param int $user_id
     *
     * @return bool
     */
    public function hasAccessToken($user_id = 0)
    {
        return trim($this->accessToken($user_id)) != null;
    }
}
