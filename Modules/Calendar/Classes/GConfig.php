<?php

namespace Modules\Calendar\Classes;

class GConfig
{


    /**
     * get auth config
     *
     * @return array
     */
    public static function getAuthConfig()
    {
        return [
             self::googleAccessType() => [
                  'client_id'                   => self::getClientId(),
                  'project_id'                  => self::getProjectId(),
                  'auth_uri'                    => self::getAuthUri(),
                  'token_uri'                   => self::getTokenUri(),
                  'auth_provider_x509_cert_url' => self::getAuthProvider(),
                  'client_secret'               => self::getClientSecret(),
                  'redirect_uris'               => [route('google_set_auth_code')],
             ],
        ];
    }



    /**
     * get clientId from config file
     *
     * @return string
     */
    private static function getClientId()
    {

        return config('calendar.google_calendar.clientID');
    }



    /**
     * get projectId from config file
     *
     * @return string
     */
    private static function getProjectId()
    {
        return config('calendar.google_calendar.projectID');
    }



    /**
     * get AutUri from config file
     *
     * @return string
     */
    private static function getAuthUri()
    {
        return config('calendar.google_calendar.auth_uri');
    }



    /**
     * get tokenUri from config file
     *
     * @return string
     */
    private static function getTokenUri()
    {
        return config('calendar.google_calendar.token_uri');
    }



    /**
     * get authProvider from config file
     *
     * @return string
     */
    private static function getAuthProvider()
    {
        return config('calendar.google_calendar.auth_provider');
    }



    /**
     * get client secret from config file
     *
     * @return string
     */
    private static function getClientSecret()
    {
        return config('calendar.google_calendar.client_secret');
    }



    /**
     * get app name from evn file
     *
     * @return string
     */
    public static function getAppName()
    {
        return config('calendar.google_calendar.app_name');
    }



    /**
     * get access type from config file
     *
     * @return string
     */
    public static function getAccessType()
    {
        return 'offline';
    }



    /**
     * get calendar summary from config file
     *
     * @return string
     */
    public static function getCalendarSummary()
    {
        return config('calendar.google_calendar.summary');
    }



    /**
     * get access type from config file
     *
     * @return string
     */
    private static function googleAccessType()
    {
        return config('calendar.google_calendar.access_type');
    }
}
