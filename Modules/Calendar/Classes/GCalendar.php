<?php

namespace Modules\Calendar\Classes;


use Exception;
use Google_Exception;
use Google_Service_Calendar_CalendarList;
use Modules\Calendar\Events\GoogleCalendarChannelCreated;

class GCalendar
{
    public $user_id;



    /**
     * GEvent constructor.
     *
     * @param int $user_id
     */
    public function __construct($user_id)
    {
        $this->user_id = $user_id;
    }



    /**
     * return calendar list by Google API
     *
     * @return Google_Service_Calendar_CalendarList
     * @throws Google_Exception
     */
    public function calendarList()
    {
        $g_client      = new GClient();
        $service       = $g_client->makeService($this->user_id);
        $calendar_list = $service->calendarList->listCalendarList([]);

        return $calendar_list;
    }



    /**
     * save syncToken in meta field in users table
     *
     * @param string $token
     */
    public function saveSyncToken($token)
    {
        user($this->user_id)->setPreference('CalendarNextSyncToken', $token);
    }



    /**
     * save calendar from Google in DataBase
     *
     * @param Google_Exception $calendars
     *
     * @throws Exception
     */
    public function saveGoogleCalendar($calendars)
    {
        foreach ($calendars as $calendar) {

            $is_model = true;
            $model    = model('calendar')->where('google_id', $calendar->id)->first();

            if (!$model) {
                $model    = model('calendar');
                $is_model = false;
            }

            if (user($this->user_id)->can('calendar.create')) {

                $calendar_model = $model->batchSave([
                     'user_id'     => $this->user_id,
                     'google_id'   => $calendar->id,
                     'title'       => $calendar->summary,
                     'description' => $calendar->description,
                     'time_zone'   => $calendar->timeZone,
                     'color'       => $calendar->backgroundColor,
                ]);

                $this->createCalendarEventChannel($calendar_model);

                if ($is_model == false) {
                    $model->users()->attach($this->user_id);
                }

            }
        }
    }



    /**
     * Create calendars for calendars that sync in the first connection
     *
     * @param Google_Exception $calendar
     */
    public function createCalendarEventChannel($calendar)
    {

        $model = model('google-channel')->where('calendar_id', $calendar->id)->get();
        if ($model->isEmpty()) {
            event(new GoogleCalendarChannelCreated($calendar));
        }

    }


}
