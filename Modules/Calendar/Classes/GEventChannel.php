<?php

namespace Modules\Calendar\Classes;


use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use Webpatser\Uuid\Uuid;

class GEventChannel
{


    /**
     * Stop the channel before expiration
     *
     * @param Collection $channel
     * @param string     $token
     */
    public function stopChannel($channel, $token)
    {
        $url = sprintf("https://www.googleapis.com/calendar/v3/channels/stop");

        $fields  = $this->setStopParameters($channel);
        $headers = $this->setStopHeaders($token);

        /* send POST request */
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($channel, CURLOPT_URL, $url);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_POST, true);
        curl_setopt($channel, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($channel, CURLOPT_TIMEOUT, 3);
        $response = curl_exec($channel);

        curl_close($channel);
    }



    /**
     * setup the POST parameters
     *
     * @param Collection $channel
     *
     * @return false|string
     */
    public function setStopParameters($channel)
    {
        return json_encode([
             'id'         => $channel->channel_id,
             'resourceId' => $channel->resource_id,
        ]);
    }



    /**
     * setup POST headers
     *
     * @param string $token
     *
     * @return array
     */
    public function setStopHeaders($token)
    {
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;

        return $headers;
    }



    /**
     * create channel for syn events from google on app
     *
     * @param string $calendar_id
     * @param string $token
     * @param int    $user_id
     *
     * @throws Exception
     */
    public function createChannel($calendar_id, $token, $user_id)
    {
        $url     = sprintf("https://www.googleapis.com/calendar/v3/calendars/$calendar_id/events/watch");
        $fields  = $this->setParameters();
        $headers = $this->setHeaders($token);

        /* send POST request */
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($channel, CURLOPT_URL, $url);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_POST, true);
        curl_setopt($channel, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($channel, CURLOPT_TIMEOUT, 3);
        $response = curl_exec($channel);
        chalk('res')->write($response);
        try {
            $this->saveChannel($response, $calendar_id, $user_id);
        } catch (Exception $e) {
            error_log($response);
        }

        curl_close($channel);
    }



    /**
     * setup POST headers
     *
     * @param string $token
     *
     * @return array
     */
    public function setHeaders($token)
    {
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;

        return $headers;
    }



    /**
     * setup the POST parameters
     *
     * @return false|string
     * @throws Exception
     */
    public function setParameters()
    {
        $uuid = Uuid::generate();

        return json_encode([

             'id'      => $uuid->string,
             'type'    => "web_hook",
             'token'   => "token_$uuid->string",
             'address' => sprintf("https://cb.yasna.team/callback/b1071d6a-ce5b-4268-8f4e-3a6668c349e6"),
        ]);
    }



    /**
     * save channel response in DB
     *
     * @param JsonResource $response
     * @param string       $calendar_id
     * @param int          $user_id
     */
    public function saveChannel($response, $calendar_id, $user_id)
    {
        $response = json_decode($response);
        $model    = model('google-channel')->where('calendar_id', $calendar_id)->first();

        if (!$model) {
            $model = model('google-channel');
        }
        $model->batchSave([
             'user_id'      => $user_id,
             'channel_id'   => $response->id,
             'resource_id'  => $response->resourceId,
             'resource_uri' => $response->resourceUri,
             'token'        => $response->token,
             'expiration'   => $response->expiration,
             'calendar_id'  => $calendar_id,
        ]);
    }


}
