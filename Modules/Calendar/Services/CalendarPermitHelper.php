<?php

namespace Modules\Calendar\Services;

class CalendarPermitHelper
{
    use CalendarHelperPermitTrait;
    use EventHelperPermitTrait;
    use CommentHelperPermitTrait;
}
