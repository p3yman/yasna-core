<?php

namespace Modules\Calendar\Services;

trait CalendarHelperPermitTrait
{
    /**
     * check if the current admin can browse calendar
     *
     * @return bool
     */
    public function canBrowseCalendar(): bool
    {
        return user()->can("calendar.browse");
    }



    /**
     * check if the current admin can create calendar
     *
     * @return bool
     */
    public function canCreateCalendar(): bool
    {
        return user()->can("calendar.create");
    }



    /**
     * check if the current admin can delete calendar
     *
     * @return bool
     */
    public function canDeleteCalendar(): bool
    {
        return user()->can("calendar.delete");
    }



    /**
     * check if the current admin can delete calendar
     *
     * @return bool
     */
    public function canEditCalendar(): bool
    {
        return user()->can("calendar.edit");
    }
}
