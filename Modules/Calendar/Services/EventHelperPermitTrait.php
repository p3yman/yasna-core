<?php

namespace Modules\Calendar\Services;

trait EventHelperPermitTrait
{
    /**
     * check if the current admin can browse calendar event
     *
     * @return bool
     */
    public function canBrowseEvent(): bool
    {
        return user()->can("calendar-event.browse");
    }



    /**
     * check if the current admin can create calendar event
     *
     * @return bool
     */
    public function canCreateEvent(): bool
    {
        return user()->can("calendar-event.create");
    }



    /**
     * check if the current admin can delete calendar event
     *
     * @return bool
     */
    public function canDeleteEvent(): bool
    {
        return user()->can("calendar-event.delete");
    }



    /**
     * check if the current admin can edit calendar event
     *
     * @return bool
     */
    public function canEditEvent(): bool
    {
        return user()->can("calendar-event.edit");
    }


}
