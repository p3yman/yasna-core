<?php

namespace Modules\Calendar\Services;

trait CommentHelperPermitTrait
{
    /**
     * check if the current admin can browse comment event
     *
     * @return bool
     */
    public function canBrowseComment(): bool
    {
        return user()->can("calendar-comments.browse");
    }



    /**
     * check if the current admin can create comment event
     *
     * @return bool
     */
    public function canCreateComment(): bool
    {
        return user()->can("calendar-comments.create");
    }



    /**
     * check if the current admin can delete comment event
     *
     * @return bool
     */
    public function canEditComment(): bool
    {
        return user()->can("calendar-comments.edit");
    }



    /**
     * check if the current admin can delete comment event
     *
     * @return bool
     */
    public function canDeleteComment()
    {
        return user()->can("calendar-comments.delete");
    }
}
