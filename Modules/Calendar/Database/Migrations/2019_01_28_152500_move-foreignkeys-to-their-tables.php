<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveForeignkeysToTheirTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendar_user', function (Blueprint $table) {
            $table->foreign('calendar_id')->references('id')->on('calendars')->onDelete('cascade');
        });

        Schema::table('calendar_events', function (Blueprint $table) {
            $table->foreign('calendar_id')->references('id')->on('calendars')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendar_user', function (Blueprint $table) {
            $table->dropForeign('calendar_id');
        });

        Schema::table('calendar_events', function (Blueprint $table) {
            $table->dropforeign('calendar_id')->references('id')->on('calendars')->onDelete('cascade');
        });
    }
}
