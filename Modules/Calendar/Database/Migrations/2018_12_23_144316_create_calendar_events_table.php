<?php 

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('google_id')->nullable();
            $table->string('title');
            $table->unsignedInteger('calendar_id')->index();
            $table->timestamp("starts_at")->nullable()->index();
            $table->timestamp("ends_at")->nullable()->index();
            $table->string('description')->nullable();
            $table->string('location')->nullable();
            $table->boolean('all_day');
            $table->string('repeater')->nullable();
            $table->timestamps();
            yasna()->additionalMigrations($table);


        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_events');
    }
}
