<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('google_id')->unique()->nullable();
            $table->string('title');
            $table->longText('description')->nullable();
            $table->string('color');
            $table->string('time_zone');
            $table->unsignedInteger('user_id')->index();

            $table->timestamps();
            yasna()->additionalMigrations($table);

        });

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
