<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type');//<~~ Specifies the type of alert , notification or email
            $table->timestamp('to_be_sent_at');
            $table->unsignedInteger('event_id');
            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->foreign('event_id')->references('id')->on('calendar_events')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_notifications');
    }
}
