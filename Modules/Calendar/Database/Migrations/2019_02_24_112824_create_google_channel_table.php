<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('google_channel', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->index();
            $table->string('channel_id')->index()->unique();
            $table->string('resource_uri');
            $table->string('resource_id')->index();
            $table->string('token');
            $table->string('expiration')->index();
            $table->string('calendar_id')->index()->nullable();

            $table->timestamps();
            yasna()->additionalMigrations($table);

            $table->foreign('calendar_id')->references('google_id')->on('calendars')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('google_channel');
    }
}
