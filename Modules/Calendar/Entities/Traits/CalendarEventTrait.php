<?php

namespace Modules\Calendar\Entities\Traits;

trait CalendarEventTrait
{


    /**
     * check existing calendar channel
     *
     * @return bool
     */
    public function hasCalendarChannel()
    {
        $model = model('googleChannel')->where('user_id', $this->id)->where('calendar_id', $this->calendar->id);
        if (!$model) {
            return true;
        }
        return false;
    }
}
