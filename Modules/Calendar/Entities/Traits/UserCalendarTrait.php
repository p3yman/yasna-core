<?php

namespace Modules\Calendar\Entities\Traits;

trait UserCalendarTrait
{

    /**
     * relation by user
     *
     * @return mixed
     */
    public function calendars()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Calendar');
    }



    /**
     * check existing google account
     *
     * @return bool
     */
    public function hasGoogleAccount()
    {
        if (user()->preference('GoogleCalendar')) {
            return true;
        }
        return false;
    }



    /**
     * check existing calendar channel
     *
     * @return bool
     */
    public function hasUserChannel()
    {

        $model = model('GoogleChannel')->where('id', $this->id)->whereNull('calendar_id')->get();

        if (!$model->isEmpty()) {
            return true;
        }

        return false;
    }
}
