<?php

namespace Modules\Calendar\Entities\Traits;

use Modules\Uploader\Services\Entity\FilesTrait;

trait RemarkCalendarTrait
{
    use FilesTrait;
}
