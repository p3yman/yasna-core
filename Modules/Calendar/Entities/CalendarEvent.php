<?php

namespace Modules\Calendar\Entities;

use Modules\Calendar\Entities\Traits\CalendarEventTrait;
use Modules\Remark\Services\Traits\RemarkTrait;
use Modules\Uploader\Services\Entity\FilesTrait;
use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Label\Entities\Traits\LabelsTrait;

class CalendarEvent extends YasnaModel
{
    use SoftDeletes;
    use LabelsTrait;
    use FilesTrait;
    use RemarkTrait;
    use CalendarEventTrait;



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
            //TODO: Fill this with the names of your meta fields, or remove the method if you do not want meta fields at all.
        ];
    }



    /**
     * Save in the column with the real name
     *
     * @param string $value
     */
    public function setDateAttribute($value)
    {
        $this->attributes['starts_at'] = $value;
    }



    /**
     * Save in the column with the real name
     *
     * @param string $value
     */
    public function setToDateAttribute($value)
    {
        $this->attributes['ends_at'] = $value;
    }



    /**
     * create uniq slug With random strings
     *
     * @param string $label
     *
     * @return string
     */
    public function defineSlug($label)
    {
        $slug = str_slug($label) . '-' . str_random(5) . '-' . str_random(5);

        return $slug;
    }



    /**
     * relation by calendar
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function calendar()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'Calendar');
    }



    /**
     * relation by EventNotifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'EventNotification', 'event_id');
    }


}
