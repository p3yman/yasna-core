<?php

namespace Modules\Calendar\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventNotification extends YasnaModel
{
    use SoftDeletes;



    /**
     * relation by calendarEvent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo(MODELS_NAMESPACE . 'CalendarEvent');
    }
}
