<?php

namespace Modules\Calendar\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoogleChannel extends YasnaModel
{
    use SoftDeletes;

    protected $table = 'google_channel';
}
