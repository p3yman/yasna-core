<?php

namespace Modules\Calendar\Entities;

use Modules\Yasna\Services\YasnaModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends YasnaModel
{
    use SoftDeletes;



    /**
     * get the main meta fields of the table.
     *
     * @return array
     */
    public function mainMetaFields()
    {
        return [
            //TODO: Fill this with the names of your meta fields, or remove the method if you do not want meta fields at all.
        ];
    }



    /**
     * relation by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'User');
    }



    /**
     * relation by event
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany(MODELS_NAMESPACE . 'CalendarEvent');
    }



    /**
     * relation by role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departments()
    {
        return $this->belongsToMany(MODELS_NAMESPACE . 'Role');
    }
}
