<?php

namespace Modules\Calendar\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CalendarSettingRequest extends YasnaRequest
{
    protected $model_name = "User";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

             'first_day_of_week' => 'required',
             'calendar_type'     => 'required',
             'business_days'     => 'required',
             'start_of_day'      => 'required',
             'end_of_day'        => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'start_of_day' => 'date',
             'end_of_day'   => 'date',
        ];
    }
}
