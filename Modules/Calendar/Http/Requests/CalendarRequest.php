<?php

namespace Modules\Calendar\Http\Requests;

use Modules\Calendar\Helpers\CalendarHelper;
use Modules\Yasna\Services\YasnaRequest;

class CalendarRequest extends YasnaRequest
{
    protected $model_name = "Calendar";

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

             'title' => 'required'
        ];
    }



    /**
     * @inheritdoc
     */
    function corrections()
    {
        $helper=new CalendarHelper();
        $this->data['user_id'] = user()->id;
        $this->data['color']=$this->color ?? $helper->random_color();
    }

}
