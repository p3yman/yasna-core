<?php

namespace Modules\Calendar\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CalandarEventRequest extends YasnaRequest
{
    protected $model_name = "CalendarEvent";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

             'title'       => 'required',
             'calendar_id' => 'required',
             'date'        => 'required',
             'to_date'     => 'required',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'date'    => 'date',
             'to_date' => 'date',
        ];
    }



    /**
     * validation event date
     *
     * @return bool
     */
    public function dateValidation()
    {
        if ($this->date > $this->to_date) {
            $is_saved = false;
        } else {
            $is_saved = true;
        }
        return $is_saved;
    }

}
