<?php

namespace Modules\Calendar\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CelandarUpdateRequest extends YasnaRequest
{
    protected $model_name = "Calendar";

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

             'title' => 'required'
        ];
    }



    /**
     * @inheritdoc
     */
    function corrections()
    {
        $this->data['user_id'] = user()->id;
    }
}
