<?php

namespace Modules\Calendar\Http\Requests;

use Modules\Yasna\Services\YasnaRequest;

class CalendarCommentRequest extends YasnaRequest
{
    protected $model_name = "CalendarEvent";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

             'comment' => 'required|min:2',
        ];
    }
}
