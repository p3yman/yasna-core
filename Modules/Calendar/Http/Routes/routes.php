<?php

Route::group([
     'middleware' => 'web',
     'namespace'  => module('Calendar')->getControllersNamespace(),
     'prefix'     => 'manage/calendar',
], function () {
    Route::get('/', 'CalendarController@index')->name('calendar');
    Route::get('/view', 'CalendarController@calendarView');/*return view for after page refresh*/
    Route::get('/create/{calendar?}', 'CalendarActionController@createCalendar')->name('createCalendar');
    Route::post('/store', 'CalendarActionController@storeCalendar')->name('storeCalendar');
    Route::post('/update', 'CalendarActionController@updateCalendar')->name('updateCalendar');
    Route::get('/show', 'CalendarActionController@getCalendar')->name('getCalendar');
    Route::get('/delete/alert/{id}', 'CalendarActionController@showAlert')->name('deleteAlert');
    Route::post('/delete', 'CalendarActionController@deleteCalendar')->name('deleteCalendar');


    Route::group(['prefix' => "setting"], function () {
        Route::get('/', 'CalendarSettingController@createSetting')->name('createSetting');
        Route::post('/store', 'CalendarSettingController@storeSetting')->name('storeSetting');
    });

    Route::group(['prefix' => "event"], function () {
        Route::get('/create/{date?}', 'CalendarEventController@create')->name('createEvent');
        Route::post('/store', 'CalendarEventController@storeEvent')->name('storeEvent');
        Route::get('/show', 'CalendarEventController@getAllEvent')->name('getAllEvent');
        Route::get('/edit/{id}', 'CalendarEventController@editEvent')->name('editEvent');
        Route::post('/update', 'CalendarEventController@updateEvent')->name('updateEvent');
        Route::get('/get', 'CalendarEventController@getEvent')->name('getEvent');
        Route::get('/single/{id}', 'CalendarEventController@singleEvent')->name('singleEvent');
        Route::get('/delete/alert/{id}', 'CalendarEventController@showAlert')->name('eventDeleteAlert');
        Route::post('/delete', 'CalendarEventController@deleteEvent')->name('deleteEvent');
        Route::get('/move/{hashid}', 'CalendarEventController@moveEvent');

        Route::group(['prefix' => "comment"], function () {
            Route::post('/store', 'CalendarCommentController@store')->name('storeComment');
            Route::get('/delete/alert/{id}', 'CalendarCommentController@showAlert')->name('commentDeleteAlert');
            Route::post('/delete', 'CalendarCommentController@deleteComment')->name('deleteComment');
            Route::get('/reload/{id}', 'CalendarCommentController@reloadComment')->name('reloadComment');
        });

    });
    Route::group(['prefix' => "google"], function () {
        Route::get('/set-auth-code', 'GCalendarController@setAuthCode')->name('google_set_auth_code');
        Route::get('/calendars/response', 'GCalendarController@getCallbackCalendar')->name('google_calendar_list');
        Route::get('/calendars/stop', 'GCalendarController@stopChannel');
    });
});

Route::group([
     'namespace' => module('Calendar')->getControllersNamespace(),
     'prefix'    => 'manage/calendar',
], function () {
    Route::group(['prefix' => "google"], function () {
        Route::any('/notifications', 'GCalendarController@googleCalendarsCallback');

    });
});
