<?php

namespace Modules\Calendar\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\View;
use Modules\Calendar\Entities\CalendarEvent;
use Modules\Calendar\Entities\EventNotification;
use Modules\Calendar\Events\GoogleCalendarEventDeleted;
use Modules\Calendar\Events\GoogleCalendarEventCreated;
use Modules\Calendar\Events\GoogleEventUpdated;
use Modules\Calendar\Helpers\eventCalendarHelper;
use Modules\Calendar\Http\Requests\CalandarEventRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;
use Symfony\Component\HttpFoundation\Response;

class CalendarEventController extends YasnaController
{
    protected $base_model  = "CalendarEvent";
    protected $view_folder = "calendar::event";
    protected $helper;



    /**
     * CalendarEventController constructor.
     *
     * @param eventCalendarHelper $helper
     */
    public function __construct(eventCalendarHelper $helper)
    {
        $this->helper = $helper;
    }



    /**
     * return event form.
     *
     * @param null|string $date
     *
     * @return View|Response
     * @throws Exception
     */
    public function create($date = null)
    {
        //$notification = $this->helper->notifications();
        $notification['combo'] = $this->helper->notificationsCombo();
        $calendars             = $this->helper->getCalendars();
        $labels                = model('CalendarEvent')->selectizeList();

        return $this->view('editor', compact('notification', 'calendars', 'date', 'labels'));
    }



    /**
     * create calendar events.
     *
     * @param CalandarEventRequest $request
     *
     * @return string
     */
    public function storeEvent(CalandarEventRequest $request)
    {
        $is_saved = $request->dateValidation();

        if ($is_saved == false) {
            $array_result['danger_message'] = trans("calendar::general.message.range_time_error");
            return $this->jsonAjaxSaveFeedback($is_saved, $array_result);
        }

        $model = $this->saveData($request);
        $this->saveLabels($model, $request->labels);
        $this->saveNotifications($model, $request->notifications);
        $is_files = $this->saveFiles($model, $request->dropzone);

        if ($is_files == false) {
            $is_saved                       = false;
            $array_result['danger_message'] = trans("calendar::general.message.file_error");
        }

        $data = $this->helper->getCalendarEvent($model);
        $this->callEventCreate($data);

        $array_result['success_callback'] = "divReload('event-calendar');addNewEvent(" . json_encode($data) . ")";

        return $this->jsonAjaxSaveFeedback($is_saved, $array_result);
    }



    /**
     * update events.
     *
     * @param CalandarEventRequest $request
     *
     * @return string
     */
    public function updateEvent(CalandarEventRequest $request)
    {
        $is_saved = $request->dateValidation();

        if ($is_saved == false) {
            $array_result['danger_message'] = trans("calendar::general.message.range_time_error");
            return $this->jsonAjaxSaveFeedback($is_saved, $array_result);
        }

        $data = $this->saveData($request);
        $this->saveLabels($data, $request->labels);
        $this->saveNotifications($data, $request->notifications);
        $is_files = $this->saveFiles($data, $request->dropzone);

        if ($is_files == false) {
            $is_saved                       = false;
            $array_result['danger_message'] = trans("calendar::general.message.file_error");
        }
        $this->callEventUpdate($data);

        $array_result['success_callback'] = "divReload('reload_calendar')";

        return $this->jsonAjaxSaveFeedback($is_saved, $array_result);
    }



    /**
     * return all events
     *
     * @return View|Response
     */
    public function getAllEvent()
    {
        $events = $this->helper->getEvents();

        return $this->view('show_event_list', compact('events'));
    }



    /**
     * return edit form for update calendar event
     *
     * @param int $id
     *
     * @return View|Response
     * @throws Exception
     */
    public function editEvent($id)
    {
        $event                  = $this->model($id);
        $files                  = $this->getFiles($event);
        $calendars              = $this->helper->getCalendars();
        $labels                 = model('CalendarEvent')->selectizeList();
        $eventLabels            = $this->getLabels($event);
        $notification['combo']  = $this->helper->notificationsCombo();
        $notification['object'] = $this->helper->notifications();
        $eventNotification      = $this->getNotify($event);

        return $this->view('editor',
             compact('event', 'calendars', 'labels', 'files', 'eventLabels', 'notification', 'eventNotification'));
    }



    /**
     * return json for view event on the calendar
     *
     * @return JsonResource
     */
    public function getEvent()
    {
        $events = $this->helper->getEvents();

        return response()->json($events, 200);

    }



    /**
     * attach uploaded files
     *
     * @param CalendarEvent $model
     * @param null|string   $files
     *
     * @return bool
     */
    public function saveFiles($model, $files = null)
    {
        $this->detachFiles($model);
        if ($files != null) {
            foreach ($files as $file) {
                uploader()->file($file)->attachTo($model);
            }
        }
        return true;
    }



    /**
     * detach uploaded files
     *
     * @param CalendarEvent $model
     */
    public function detachFiles($model)
    {
        $uploadFiles = $model->files()->pluck('id');
        foreach ($uploadFiles as $file) {
            uploader()->file($file)->detachFromEntity();
        }
    }



    /**
     * get uploaded files if existing
     *
     * @param CalendarEvent $event
     *
     * @return array
     */
    public function getFiles($event)
    {
        $uploadFiles = $event->files()->pluck('id');
        if (!$uploadFiles->isEmpty()) {
            foreach ($uploadFiles as $file) {
                $files[] = hashid($file);
            }
            return $files;
        }
        return [];
    }



    /**
     * get a slugs array of the attached labels.
     *
     * @param CalendarEvent $event
     *
     * @return array
     */
    public function getLabels($event)
    {
        $eventLabels = $event->labelsArray();
        if (count($eventLabels) > 0) {
            $labels = implode(',', $eventLabels);
            return $labels;
        }
        return;
    }



    /**
     * Saves labels if not exists.
     * detach labels if delete .
     * store many-to-many relationships.
     *
     * @param CalendarEvent $data
     * @param null|string   $labels
     */
    public function saveLabels($data, $labels = null)
    {
        if ($labels != null) {
            $labelExists = model('CalendarEvent')->definedLabelsArray();
            $labels      = explode(',', $labels);
            $eventLabels = $data->labelsArray();
            foreach ($eventLabels as $oldLabel) {
                if (!in_array($oldLabel, $labels)) {
                    $data->detachLabel($oldLabel);
                }
            }
            foreach ($labels as $label) {
                if (!in_array($label, $labelExists)) {
                    $slug = $this->model()->defineSlug($label);
                    $data->defineLabel($slug, $label);
                    $data->attachLabel($slug);
                } else {
                    $data->attachLabel($label);
                }
            }
        } else {
            $data->detachAllLabels();
        }
        return;
    }



    /**
     * Returns the modal to view the event and post comments.
     *
     * @param int $id
     *
     * @return View|Response
     */
    public function singleEvent($id)
    {
        $event = $this->getSingleEvent($id);
        $data  = $this->getComments($event['data']);

        return $this->view('event_single', compact('event', 'data'));
    }



    /**
     * Returns total comments and files in array format
     *
     * @param CalendarEvent $event
     *
     * @return Collection
     */
    public function getComments($event)
    {
        $comments = $event->remarks()->orderBy('created_at', 'DESC')->get();
        if (!$comments->isEmpty()) {
            foreach ($comments as $comment) {
                $model = model('remark', $comment->id)->files()->get();
                if (!$model->isEmpty()) {
                    $data[$comment->id]['files'] = $model;
                }
                $data[$comment->id]['comment'] = $comment;
            }
            return $data;
        }
    }



    /**
     * delete event
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function deleteEvent(SimpleYasnaRequest $request)
    {
        $id    = $request->id;
        $event = $this->model($id);
        $event->delete();

        $success_callback = "divReload('event-calendar'); removeEvent($id)";

        $this->callEventDelete($event);

        return $this->jsonAjaxSaveFeedback(true, compact('success_callback'));
    }



    /**
     * Show message to ensure that event delete
     *
     * @param int $id
     *
     * @return View|Response
     */
    public function showAlert($id)
    {
        return $this->view('alert', compact('id'));
    }



    /**
     * save event notifications
     *
     * @param CalendarEvent $data
     * @param array         $request
     */
    public function saveNotifications($data, $request)
    {
        if ($request != null) {
            foreach ($request as $key => $item) {

                if ($key != 'new') {
                    $this->updateNotifications($data, $item, $key);
                    $this->deleteNotifications($data, $request);
                } else {
                    $this->deleteNotifications($data, $request);
                    $this->saveNewNotifications($data, $item);
                }
            }
        } else {
            $this->deleteAllNotifications($data);
        }

    }



    /**
     * update event notifications
     *
     * @param CalendarEvent $data
     * @param array         $item
     * @param string        $key
     */
    public function updateNotifications($data, $item, $key)
    {
        $date_time   = $this->helper->convertTime($data, $item);
        $data_notify = $this->helper->preferenceNotify($item);
        $data->notifications()->where('id', hashid($key))->update([
             'type'          => $item['type'],
             'to_be_sent_at' => $date_time,
             'meta'          => $data_notify,
        ])
        ;
    }



    /**
     * delete event notifications
     *
     * @param CalendarEvent $data
     * @param array         $request
     */
    public function deleteNotifications($data, $request)
    {
        $oldNotifications = $data->notifications()->get();
        if (!$oldNotifications->isEmpty()) {
            foreach ($oldNotifications as $notification) {
                if (!array_key_exists($notification->hashid, $request)) {
                    model('EventNotification', $notification->hashid)->delete();
                }
            }
        }
    }



    /**
     * save new notifications for relevant event
     *
     * @param CalendarEvent $data
     * @param array         $item
     */
    public function saveNewNotifications($data, $item)
    {
        foreach ($item as $notify) {
            $date_time    = $this->helper->convertTime($data, $notify);
            $data_notify  = $this->helper->preferenceNotify($notify);
            $notification = new EventNotification([
                 'type'          => $notify['type'],
                 'to_be_sent_at' => $date_time,
                 'meta'          => $data_notify,
            ]);
            $data->notifications()->save($notification);
        }
    }



    /**
     * delete all notifications for relevant event
     *
     * @param CalendarEvent $data
     */
    public function deleteAllNotifications($data)
    {
        model('EventNotification')->where('event_id', hashid($data->hashid))->delete();
    }



    /**
     * Returns saved notifications for an event
     *
     * @param calendarEvent $event
     *
     * @return Collection
     */
    public function getNotify($event)
    {
        $notify = $event->notifications()->get();

        return $notify;
    }



    /**
     *  * save data in calendar-events table
     *
     * @param CalandarEventRequest $request
     *
     * @return Collection
     */
    public function saveData($request)
    {
        $data = $request->model->batchSave($request, [
             'upload',
             'dropzone',
             'labels',
             'notifications',

        ]);

        return $data;
    }



    /**
     * return data for single event page
     *
     * @param int $id
     *
     * @return array
     */
    public function getSingleEvent($id)
    {
        $event['data']          = $this->model($id);
        $event['files']         = $this->getSingleFiles($event['data']);
        $event['labels']        = $this->getSingleLabels($event['data']);
        $event['notifications'] = $this->getNotify($event['data']);

        return $event;
    }



    /**
     * return event labels for single page
     *
     * @param CalendarEvent $event
     *
     * @return array|bool
     */
    public function getSingleLabels($event)
    {
        $labelsArray = $event->labelsArray();
        if (count($labelsArray) > 0) {
            foreach ($labelsArray as $slug) {
                $labels[] = $event->getLabelTitles($slug);
            }
            return $labels;
        }
        return false;
    }



    /**
     * return event files for single
     *
     * @param CalendarEvent $event
     *
     * @return array|bool
     */
    public function getSingleFiles($event)
    {
        $uploadFiles = $event->files()->get();
        if (count($uploadFiles) > 0) {
            foreach ($uploadFiles as $key => $file) {
                $files[$key]['link']  = "/uploads/$file->file_name";
                $files[$key]['title'] = $file->original_name;
            }

            return $files;
        }
        return false;
    }



    /**
     * call event after store calendar event
     *
     * @param CalendarEvent $data
     */
    public function callEventCreate($data)
    {
        if ($data->calendar->google_id) {
            event(new GoogleCalendarEventCreated($data));
        }
    }



    /**
     * calling event file for delete event in google
     *
     * @param CalendarEvent $event
     */
    public function callEventDelete($event)
    {
        if ($event->google_id != null) {
            event(new GoogleCalendarEventDeleted($event));
        }

    }



    /**
     * calling event file for update event in google
     *
     * @param CalendarEvent $event
     */
    public function callEventUpdate($event)
    {
        if ($event->google_id != null) {
            event(new GoogleEventUpdated($event));
        }
    }



    /**
     * move event on calendar
     *
     * @param SimpleYasnaRequest $request
     * @param int                $id
     *
     * @return string
     */
    public function moveEvent(SimpleYasnaRequest $request, $id)
    {
        $event           = $this->model($id);
        $day_delta       = $request->get('day_delta');
        $event_starts_at = Carbon::parse($event->starts_at)
                                 ->addDays($day_delta['days'])
                                 ->addHours($day_delta['hours'])
        ;
        $event_ends_at   = Carbon::parse($event->ends_at)
                                 ->addDays($day_delta['days'])
                                 ->addHours($day_delta['hours'])
        ;

        $event->starts_at = $event_starts_at->toDateTimeString();

        $event->ends_at = $event_ends_at->toDateTimeString();


        $saveEvent = $event->save();

        //Call Event
        $this->callEventUpdate($event);

        return $this->jsonAjaxSaveFeedback($saveEvent);
    }

}
