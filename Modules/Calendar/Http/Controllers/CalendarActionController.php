<?php

namespace Modules\Calendar\Http\Controllers;

use Modules\Calendar\Entities\Calendar;
use Modules\Calendar\Events\GoogleCalendarDeleted;
use Modules\Calendar\Events\GoogleCalendarCreated;
use Modules\Calendar\Events\GoogleCalendarUpdated;
use Modules\Calendar\Helpers\CalendarHelper;
use Modules\Calendar\Http\Requests\CalendarRequest;
use Modules\Calendar\Http\Requests\CelandarUpdateRequest;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class CalendarActionController extends YasnaController
{
    protected $base_model  = "Calendar";
    protected $view_folder = "calendar::calendar";

    protected $calendar;



    /**
     * CalendarActionController constructor.
     *
     * @param CalendarHelper $calendar
     */
    public function __construct(CalendarHelper $calendar)
    {
        $this->calendar = $calendar;
    }



    /**
     * display new calendar or update calendar form
     *
     * @param Calendar $calendar
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createCalendar(Calendar $calendar)
    {
        $time_zone      = $this->calendar->getTimeZone();
        $department     = $this->calendar->getDepartment();
        $old_department = $this->calendar->getOldDepartment($calendar);

        return $this->view('editor', compact('time_zone', 'calendar', 'department', 'old_department'));
    }



    /**
     * create calendar
     *
     * @param CalendarRequest $request
     *
     * @return string
     */
    public function storeCalendar(CalendarRequest $request)
    {
        $data = $request->model->batchSave($request, [
             'department',
        ]);
        if ($data->exists) {
            $data->users()->attach(user()->id);
            $this->attachDepartment($data, $request->department);
        }

        $this->GStoreCalendar($data);

        $success_callback = "divReload('calendar-box')";

        return $this->jsonAjaxSaveFeedback(true, compact('success_callback'));
    }



    /**
     * update calendar
     *
     * @param CelandarUpdateRequest $request
     *
     * @return string
     */
    public function updateCalendar(CelandarUpdateRequest $request)
    {
        $calendar = $request->model->batchSave($request, [
             'department',
        ]);
        if ($calendar->exists) {
            $this->attachDepartment($calendar, $request->department);
        }

        $success_callback = "divReload('calendar-box');divReload('reload_calendar')";

        $this->callEventCalendarUpdate($calendar);

        return $this->jsonAjaxSaveFeedback(true, compact('success_callback'));
    }



    /**
     * display each user's calendars
     *
     * @param CalendarHelper $helper
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function getCalendar(CalendarHelper $helper)
    {
        $calendars = $helper->userCalendars();

        return $this->view('showCalendar', compact('calendars'));
    }



    /**
     * delete calendar
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function deleteCalendar(SimpleYasnaRequest $request)
    {
        $calendar = $this->model($request->id);
        $calendar->delete();

        $success_callback = "divReload('calendar-box');divReload('event-calendar');divReload('reload_calendar')";

        $this->callEventCalendarDelete($calendar);

        return $this->jsonAjaxSaveFeedback(true, compact('success_callback'));
    }



    /**
     * Show message to ensure that calendar delete
     *
     * @param int $id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function showAlert($id)
    {
        return $this->view('alert', compact('id'));
    }



    /**
     * call to event for insert calendar in googleCalendar
     *
     * @param Calendar $data
     */
    public function GStoreCalendar($data)
    {
        if (user()->hasUserChannel()) {
            event(new GoogleCalendarCreated($data));
        }
    }



    /**
     * calling event file for delete calendar in google
     *
     * @param Calendar $calendar
     */
    public function callEventCalendarDelete($calendar)
    {
        if ($calendar->google_id != null) {
            event(new GoogleCalendarDeleted($calendar));
        }

    }



    /**
     * calling event file for update calendar in google
     *
     * @param Calendar $calendar
     */
    public function callEventCalendarUpdate($calendar)
    {
        if ($calendar->google_id != null) {
            event(new GoogleCalendarUpdated($calendar));
        }

    }



    /**
     * attaching or detaching user role to the calendar
     *
     * @param Calendar        $calendar
     * @param CalendarRequest $department
     */
    public function attachDepartment($calendar, $department)
    {
        if ($department) {
            $department = explode(',', $department);
            $calendar->departments()->detach($department);
            $calendar->departments()->attach($department);
            return;
        }
        $this->calendar->detachDepartmentAll($calendar);
    }


}
