<?php

namespace Modules\Calendar\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Modules\Calendar\Helpers\CalendarHelper;
use Modules\Calendar\Helpers\eventCalendarHelper;
use Modules\Yasna\Services\YasnaController;

class CalendarController extends YasnaController
{
    protected $view_folder = "calendar::browse";



    /**
     * Display a listing of the resource and calendar.
     *
     * @param CalendarHelper $helper
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function index(CalendarHelper $helper)
    {
        $data      = json_encode($this->checkSetting()['setting']);
        $message   = $this->checkSetting()['message'];
        $calendars = $helper->userCalendars();
        $events    = $this->getEvent();

        return $this->view('index', compact('data', 'message', 'calendars', 'events'));
    }



    /**
     * Checking the availability of calendar settings per user
     *
     * @return bool|string
     */
    public function checkSetting()
    {
        if (user()->preference('CalendarSetting')) {
            $data['setting'] = user()->preference('CalendarSetting');
            $data['message'] = false;
        } else {
            $data['message'] = 'Please apply your calendar settings';
            $data['setting'] = false;
        }

        return $data;
    }



    /**
     * Display a listing of the resource and calendar after page refresh
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function calendarView()
    {
        $data = json_encode($this->checkSetting()['setting']);

        return $this->view('calendar_view', compact('data'));
    }



    /**
     * Returns the existing calendar event
     *
     * @return Collection
     */
    public function getEvent()
    {
        $helper = new eventCalendarHelper;
        $events = $helper->getEvents();

        return $events;
    }



    /**
     * get calendar users
     *
     * @param int $calendar_id
     *
     * @return Collection
     */
    public function calendarUser($calendar_id)
    {
        $users = model('calendar', $calendar_id)
             ->departments()
             ->join('users', 'calendar_role.role_id', 'roles.id')
             ->select('users.*')
             ->get()
        ;

        return $users;
    }


}
