<?php

namespace Modules\Calendar\Http\Controllers;


use App\Models\EventNotification;
use Carbon\Carbon;
use Exception;
use Google_Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Modules\Calendar\Classes\GAuthConfig;
use Modules\Calendar\Classes\GCalendarChannel;
use Modules\Calendar\Classes\GClient;
use Modules\Calendar\Classes\GEventChannel;
use Modules\Calendar\Events\GoogleCalendarSync;
use Modules\Yasna\Services\YasnaController;
use Webpatser\Uuid\Uuid;

class GCalendarController extends YasnaController
{

    protected $g_client;



    /**
     * save access token in meta field on users table
     *
     * @param Request $request
     * @param GClient $g_client
     *
     * @return RedirectResponse
     * @throws Google_Exception
     * @throws Exception
     */
    public function setAuthCode(Request $request, GClient $g_client)
    {
        $code   = $request->get('code');
        $client = $g_client->initialClient();

        $accessToken = $client->fetchAccessTokenWithAuthCode($code);
        user()->setPreference('GoogleCalendar', $accessToken);

        if (!$this->isChannel()) {
            $this->createChannelMethod($accessToken);
        }

        $this->callEventSyncCalendars();

        return redirect()->route('calendar');
    }



    /**
     * get callback Notifications and save response in meta filed on users table
     * if exist result from google,save calendars in DB
     *
     * @param Request $request
     *
     * @return void
     * @throws Google_Exception
     */
    public function googleCalendarsCallback(Request $request)
    {
        $response   = $request->headers->all();
        $channel_id = $response['x-goog-channel-id'];

        $model   = $this->getChannelData($channel_id);
        $user_id = $model->user_id;
        if ($user_id != null) {
            $this->syncGoogle($model, $user_id);
        }

    }



    /**
     * call event for first sync  calendars from google on app
     */
    public function callEventSyncCalendars()
    {
        event(new GoogleCalendarSync(user()->id));
    }



    /**
     * save changes calendar from google in DB
     *
     * @param Google_Exception $calendars
     * @param int              $user_id
     *
     * @throws Exception
     */
    public function saveGoogleCalendar($calendars, $user_id)
    {
        foreach ($calendars as $calendar) {

            $exist = true;
            $model = model('calendar')->where('google_id', $calendar->id)->first();

            if (!$model) {
                $exist = false;
                $model = model('calendar');
            }

            //if can delete calendar, will delete on DB
            if ($calendar->summary == null && $exist == true) {
                if (user($user_id)->can('calendar.delete')) {
                    $model->update([
                         'deleted_by' => $user_id,
                    ]);
                } else {
                    continue;
                }
            }

            //if can create or update calendar, will delete on DB
            if ($calendar->summary != null) {
                if (user($user_id)->can('calendar.create') || user($user_id)->can('calendar.update')) {
                    $calendar_data = $model->batchSave([
                         'google_id'   => $calendar->id,
                         'title'       => $calendar->summary,
                         'description' => $calendar->description,
                         'time_zone'   => $calendar->timeZone,
                         'user_id'     => $user_id,
                         'color'       => $calendar->backgroundColor,
                    ]);

                    $calendar_data->users()->attach($user_id);

                    //create channel
                    $this->createChannelAfterWebhook($calendar->id, $user_id);

                } else {
                    continue;
                }
            }


        }
    }



    /**
     * return channel data
     *
     * @param string $channel_id
     *
     * @return int
     */
    public function getChannelData($channel_id)
    {
        $model = model('google-channel')
             ->where('channel_id', $channel_id)
             ->first()
        ;
        return $model;
    }



    /**
     * get list calendar from Google after the last sync
     *
     * @param string $next_token
     * @param int    $user_id
     *
     * @throws Google_Exception
     */
    public function calendarList($next_token = '', $user_id)
    {
        $g_client = new GClient();
        $service  = $g_client->makeService($user_id);

        if ($next_token != '') {
            $optParams    = ['syncToken' => $next_token];
            $calendarList = $service->calendarList->listCalendarList($optParams);
            user($user_id)->setPreference('CalendarNextSyncToken', $calendarList->nextSyncToken);

        } else {
            //if not exist next_token,get all calendar from google
            $calendarList = $service->calendarList->listCalendarList([]);
            user($user_id)->setPreference('CalendarNextSyncToken', $calendarList->nextSyncToken);
        }

        $this->saveGoogleCalendar($calendarList, $user_id);


    }



    /**
     * setup POST headers
     *
     * @param string $token
     *
     * @return array
     */
    public function setHeaders($token)
    {
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;

        return $headers;
    }



    /**
     * setup the POST parameters
     *
     * @return false|string
     * @throws Exception
     */
    public function setParameters()
    {
        $uuid = Uuid::generate();
        return json_encode([
             'id'      => $uuid->string,
             'type'    => "web_hook",
             'token'   => "token_$uuid->string",
             'address' => sprintf("https://cb.yasna.team/callback/b1071d6a-ce5b-4268-8f4e-3a6668c349e6"),
        ]);
    }



    /**
     * Check the presence of the calendar channel
     *
     * @return bool
     */
    public function isChannel()
    {
        $model = model('google-channel')->where('user_id', user()->id)->whereNull('calendar_id')->get();

        if (!$model->isEmpty()) {
            return true;
        }
        return false;

    }



    /**
     * Synchronize information back from Webhook
     *
     * @param Collection $model
     * @param int        $user_id
     *
     * @throws Google_Exception
     */
    public function syncGoogle($model, $user_id)
    {
        $calendar_id = $model->calendar_id;

        if ($calendar_id == null) {
            $next_token = user($user_id)->preference('CalendarNextSyncToken');
            $this->calendarList($next_token, $user_id);

        } else {

            $this->EventList($model, $user_id, $calendar_id);
        }
    }



    /**
     * get list calendar event from Google after the last sync
     *
     * @param Collection $model
     * @param int        $user_id
     * @param string     $calendar_id
     *
     * @throws Google_Exception
     */
    public function EventList($model, $user_id, $calendar_id)
    {
        $next_token = $model['meta']['EventNextSyncToken'] ?? '';
        $g_client   = new GClient();
        $service    = $g_client->makeService($user_id);

        if ($next_token != '') {
            $optParams = ['syncToken' => $next_token];
            $events    = $service->events->listEvents($calendar_id, $optParams);

        } else {
            //if not exist next_token,get all calendar from google
            $events = $service->events->listEvents($calendar_id, []);
        }

        $this->saveMeta($model, $events);
        $this->saveGoogleCalendarEvent($events, $calendar_id, $user_id);


    }



    /**
     * save changes calendar event from google in DB
     *
     * @param Google_Exception $events
     * @param string           $calendar_id
     * @param int              $user_id
     */
    public function saveGoogleCalendarEvent($events, $calendar_id, $user_id)
    {
        foreach ($events as $event) {

            $c_id  = model('calendar')->where('google_id', $calendar_id)->pluck('id')->first();
            $exist = true;
            $model = model('calendar-event')->where('google_id', $event->id)->first();

            if (!$model) {
                $exist = false;
                $model = model('calendar-event');
            }

            //if can delete calendar, will delete on DB
            if ($event->summary == null && $exist == true) {
                if (user($user_id)->can('calendar-event.delete')) {
                    $model->update([
                         'deleted_by' => $user_id,
                    ]);
                } else {
                    continue;
                }
            }
            //if can create or update calendar, will delete on DB
            if ($event->summary != null) {
                if (user($user_id)->can('calendar-event.create') || user($user_id)->can('calendar-event.update')) {
                    $model = $model->batchSave([
                         'calendar_id' => $c_id,
                         'google_id'   => $event->id,
                         'title'       => $event->summary,
                         'description' => $event->description,
                         'starts_at'   => $event->start->dateTime ?? $event->start->date,
                         'ends_at'     => $event->end->dateTime ?? $event->end->date,
                         'all_day'     => $event->start->date ? 1 : 0,
                    ]);

                } else {
                    continue;
                }
                $this->syncNotification($event, $model);
            }
        }
    }



    /**
     * save last sync token in meta field on google_calendar table
     *
     * @param Collection       $model
     * @param Google_Exception $events
     */
    public function saveMeta($model, $events)
    {
        $next_token = json_encode([
             'EventNextSyncToken' => $events->nextSyncToken,
        ]);
        $model->update([
             'meta' => $next_token,
        ]);
    }



    /**
     * sync notification from google on App
     *
     * @param Google_Exception $event
     * @param Collection       $model
     */
    public function syncNotification($event, $model)
    {
        foreach ($event->reminders->overrides as $notification) {

            $method    = $this->methodType($notification->method);
            $date_time = Carbon::now()->addMinutes($notification->minutes);
            $meta      = $this->metaFieldData($date_time);

            $notification = new EventNotification([
                 'type'          => $method,
                 'to_be_sent_at' => $date_time,
                 'meta'          => $meta,
            ]);

            $model->notifications()->save($notification);
        }

    }



    /**
     * save data in meta field
     *
     * @param string $date_time
     *
     * @return false|string
     */
    public function metaFieldData($date_time)
    {
        $difference = ($date_time->diff(Carbon::now()));
        if ($difference->days != 0) {
            if ($difference->days % 7 == 0) {
                $number_time = $difference->days / 7;
                $time_type   = '3';
            } else {
                $number_time = $difference->days;
                $time_type   = '2';
            }
        } elseif ($difference->h != 0) {
            $number_time = $difference->h;
            $time_type   = '1';
        } elseif ($difference->i != 0) {
            $number_time = $difference->i;
            $time_type   = '0';
        }
        $notify = [
             'notify' => [
                  "number_time" => $number_time,
                  'time_type'   => $time_type,
                  'time'        => '00:00',
             ],
        ];
        return json_encode($notify);
    }



    /**
     * save notification kind in the meta field
     *
     * @param string $type
     *
     * @return int
     */
    public function methodType($type)
    {
        if ($type === 'email') {
            return 0;
        } elseif ($type === 'popup') {
            return 1;
        }
    }



    /**
     * Create a channel after saving the calendar announced by webhook
     *
     * @param string $calendar_id
     * @param int    $user_id
     *
     * @throws Exception
     */
    public function createChannelAfterWebhook($calendar_id, $user_id)
    {
        $token   = user($user_id)->preference('GoogleCalendar')['access_token'];
        $channel = new GEventChannel();
        $channel->createChannel($calendar_id, $token, $user_id);
    }



    /**
     * create user channel if not exist channel
     *
     * @param array $accessToken
     *
     * @return $this
     * @throws Exception
     */
    protected function createChannelMethod($accessToken)
    {
        $token   = $accessToken['access_token'];
        $channel = new GCalendarChannel();

        if (!$channel->createChannel($token, user()->id)) {
            return redirect()->route('calendar')->withErrors([
                 'error' => trans('calendar::general.message.error_connect'),
            ])
                 ;
        }
    }

}
