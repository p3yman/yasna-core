<?php

namespace Modules\Calendar\Http\Controllers;

use Modules\Calendar\Http\Requests\CalendarCommentRequest;
use Modules\Remark\Entities\Remark;
use Modules\Yasna\Http\Requests\SimpleYasnaRequest;
use Modules\Yasna\Services\YasnaController;

class CalendarCommentController extends YasnaController
{
    protected $base_model  = "CalendarEvent";
    protected $view_folder = "calendar::comment";



    /**
     * store comments for each event
     *
     * @param CalendarCommentRequest $request
     *
     * @return string
     */
    public function store(CalendarCommentRequest $request)
    {
        $model    = $this->model($request->event_id);
        $comments = $this->saveComment($model, $request);
        $this->saveFiles($comments, $request->dropzone);

        return $this->jsonAjaxSaveFeedback(true);
    }



    /**
     * save comment in remark table
     *
     * @param CalendarEvent          $model
     * @param CalendarCommentRequest $request
     *
     * @return mixed
     */
    public function saveComment($model, $request)
    {
        $comment = $model->addRemark([
             'text' => $request->comment,
        ]);
        return $comment;
    }



    /**
     * Save the uploaded files in the comment
     *
     * @param Remark     $comment
     * @param null|array $files
     *
     * @return bool
     */
    public function saveFiles($comment, $files = null)
    {
        if ($files != null) {
            foreach ($files as $file) {
                uploader()->file($file)->attachTo($comment);
            }
            return true;
        }
        return true;
    }



    /**
     * delete comment
     *
     * @param SimpleYasnaRequest $request
     *
     * @return string
     */
    public function deleteComment(SimpleYasnaRequest $request)
    {
        model('Remark', $request->id)->delete();

        return $this->jsonAjaxSaveFeedback(true);
    }



    /**
     * Show message to ensure that comment delete
     *
     * @param int $id
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function showAlert($id)
    {
        return $this->view('alert', compact('id'));
    }



    /**
     * reload comment box after send comment
     *
     * @param SimpleYasnaRequest $request
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function reloadComment(SimpleYasnaRequest $request)
    {
        $comments = model('remark')->where('entity_id', $request->id)->orderBy('created_at', 'DESC')->get();
        if (!$comments->isEmpty()) {
            foreach ($comments as $comment) {
                $files = model('remark', $comment->id)->files()->get();
                if (!$files->isEmpty()) {
                    $data[$comment->id]['files'] = $files;
                }
                $data[$comment->id]['comment'] = $comment;
            }
        }
        return $this->view('comment', compact('data'));
    }

}
