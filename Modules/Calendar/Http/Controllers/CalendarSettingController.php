<?php

namespace Modules\Calendar\Http\Controllers;

use Modules\Calendar\Helpers\CalendarHelper;
use Modules\Calendar\Http\Requests\CalendarSettingRequest;
use Modules\Yasna\Services\YasnaController;

class CalendarSettingController extends YasnaController
{
    protected $view_folder = "calendar::setting";
    protected $setting;



    public function __construct(CalendarHelper $calendarHelper)
    {
        $this->setting = $calendarHelper;
    }



    /**
     * create a modal form for calendar setting
     *
     * @return \Illuminate\Support\Facades\View|\Symfony\Component\HttpFoundation\Response
     */
    public function createSetting()
    {

        $data = $this->getSettingsParams();

        return $this->view('setting', compact('data'));
    }



    /**
     * @param CalendarSettingRequest $request
     * store calendar setting in the meta field on users table
     *
     * @return string
     */
    public function storeSetting(CalendarSettingRequest $request)
    {
        $data = [
             'first_day'     => $request->first_day_of_week,
             'calendar_type' => $request->calendar_type,
             'business_days' => $request->business_days,
             'start_day'     => $request->start_of_day,
             'end_day'       => $request->end_of_day,
        ];
        user()->setPreference('CalendarSetting', $data);

        $success_callback = "divReload('reload_calendar');deleteWarning()";

        return $this->jsonAjaxSaveFeedback(true, compact('success_callback'));

    }



    /**
     * get calendar setting
     *
     * @return array
     */
    public function getSettingsParams(): array
    {
        $data['days']          = $this->setting->days();
        $data['types']         = $this->setting->types();
        $data['first_day']     = $this->setting->firstDay();
        $data['calendar_type'] = $this->setting->calendarType();
        $data['business_days'] = $this->setting->businessDays();
        $data['start_day']     = $this->setting->startDay();
        $data['end_day']       = $this->setting->endDay();

        return $data;
    }


}
