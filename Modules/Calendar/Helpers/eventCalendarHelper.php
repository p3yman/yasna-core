<?php

namespace Modules\Calendar\Helpers;


use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class eventCalendarHelper
{


    /**
     * return notification type
     *
     * @return array
     */
    public function notificationType()
    {

        $notifications = ['email', 'notification'];

        return $notifications;
    }



    /**
     * return time type
     *
     * @return array
     */
    public function timeType()
    {
        $timeType = ['minutes', 'hours', 'day', 'week'];

        return $timeType;
    }



    /** time array based on every half hour
     *
     * @param int    $lower
     * @param int    $upper
     * @param int    $step
     * @param string $format
     *
     * @return array
     * @throws Exception
     */
    public function hoursRange($lower = 0, $upper = 86400, $step = 3600, $format = '')
    {
        $times = [];

        if (empty($format)) {
            $format = 'g:i a';
        }

        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('H:i', $increment);

            list($hour, $minutes) = explode(':', $increment);

            $date = new DateTime($hour . ':' . $minutes);

            $times[(string)$increment] = $date->format($format);
        }
        return $times;
    }



    /**
     * Return time combo based on every half hour
     *
     * @return array
     * @throws Exception
     */
    public function setTime()
    {

        $time = $this->hoursRange(0, 86400, 60 * 30, 'h:i a');
        return $time;
    }



    /**
     * Return all notification values.
     *
     * @return string
     * @throws Exception
     */
    public function notifications()
    {
        $notification['notificationType'] = $this->notificationType();
        $notification['timeType']         = $this->timeType();
        $notification['time']             = $this->setTime();

        return json_encode($notification);
    }



    /**
     * return resources for notifications
     *
     * @return array
     * @throws Exception
     */
    public function notificationsCombo()
    {

        $notification['notificationType'] = collect($this->notificationType())->map(function ($key, $value) {
            return [
                 'id'    => $value,
                 'title' => $key,
            ];
        });
        $notification['timeType']         = collect($this->timeType())->map(function ($key, $value) {
            return [
                 'id'    => $value,
                 'title' => $key,
            ];
        });
        $notification['time']             = collect($this->setTime())->map(function ($key, $value) {
            return [
                 'id'    => $value,
                 'title' => $key,
            ];
        });

        return $notification;
    }



    /**
     * return online user calendars
     *
     * @return Collection
     */
    public function getCalendars()
    {
        $calendars = user()->calendars()->get();
        $calendars = $calendars->map(function ($calendars) {
            return [
                 'id'    => $calendars->id,
                 'title' => $calendars->title,
                 'color' => $calendars->color,
            ];
        });

        return $calendars;
    }



    /**
     * Returns the existing calendar event
     *
     * @return Collection
     */
    public function getEvents()
    {
        $events = user()
             ->roles()
             ->join('calendar_role', 'roles.id', '=', 'calendar_role.role_id')
             ->join('calendars', 'calendar_role.calendar_id', 'calendars.id')
             ->join('calendar_events', 'calendars.id', '=', 'calendar_events.calendar_id')
             ->select('calendar_events.*', 'calendars.color')
             ->where('calendar_events.deleted_by', '0')
             ->orderBy('created_at', 'ASC')
             ->get()
        ;

        $personal_event   = $this->getPersonalEvent();
        $mergedCollection = $personal_event->merge($events);
        $unique_events    = $mergedCollection->unique()->values('id')->all();

        return collect($unique_events);
    }



    /**
     * Convert date according to input data
     *
     * @param calendarEvent $data
     * @param array         $notify
     *
     * @return string
     */
    public function convertTime($data, $notify)
    {
        $date_time = '';
        $number    = $notify['number_time'];
        if ($data->all_day == 0) {
            if ($notify['time_type'] == '0') {
                $date_time = Date('y-m-d H:i', strtotime("+$number minutes"));
            } elseif ($notify['time_type'] == '1') {
                $date_time = Date('y-m-d H:i', strtotime("+$number hours"));
            } elseif ($notify['time_type'] == '2') {
                $date_time = Date('y-m-d H:i', strtotime("+$number days"));
            } elseif ($notify['time_type'] == '3') {
                $date_time = Date('y-m-d H:i', strtotime("+$number week"));
            }
        } else {
            $time = $notify['time'];
            if ($notify['time_type'] == '2') {
                $date_time = Date("y-m-d $time", strtotime("+$number days"));
            } elseif ($notify['time_type'] == '3') {
                $date_time = Date("y-m-d $time", strtotime("+$number week"));
            }
        }
        return $date_time;
    }



    /**
     * join to calendars table for get color
     *
     * @param CalendarEvent $model
     *
     * @return Collection
     */
    public function getCalendarEvent($model)
    {
        $data = $model->with('calendar')->find($model->id);

        return $data;
    }



    /**
     * return event calendars that haven't the department
     *
     * @return Collection
     */
    public function getPersonalEvent()
    {
        return user()
             ->calendars()
             ->join('calendar_events', 'calendars.id', '=', 'calendar_events.calendar_id')
             ->select('calendar_events.*')
             ->where('calendar_events.deleted_by', '0')
             ->get()
             ;
    }



    /**
     * save extra field in meta field
     *
     * @param array $notify
     *
     * @return false|string
     */
    public function preferenceNotify($notify)
    {
        $data_notify = [
             'notify' =>
                  [
                       'number_time' => $notify['number_time'],
                       'time_type'   => $notify['time_type'],
                       'time'        => $notify['time'],
                  ],
        ];
        return json_encode($data_notify);
    }
}
