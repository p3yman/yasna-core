<?php

namespace Modules\Calendar\Helpers;


use Modules\Calendar\Entities\Calendar;
use Modules\Calendar\Entities\CalendarEvent;

class CalendarHelper
{


    /**
     * Return Days of the Week
     *
     * @return array
     */
    public function days(): array
    {
        //$days = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
        $days = [
             '6' => 'Saturday',
             '0' => 'Sunday',
             '1' => 'Monday',
             '2' => 'Tuesday',
             '3' => 'Wednesday',
             '4' => 'Thursday',
             '5' => 'Friday',
        ];
        return $days;
    }



    /**
     * Return calendar type
     *
     * @return array
     */
    public function types(): array
    {
        return ['Jalali', 'Gregorian'];
    }



    /**
     * Get calendar settings from meta field on users table
     *
     * @return array|string
     */
    public function getSetting()
    {
        $settings = user()->preference('CalendarSetting');
        if (is_array($settings)) {
            return $settings;
        }
        return [];
    }



    /**
     * Get first day of the week if it exists in the database
     *
     * @return int|mixed
     */
    public function firstDay()
    {
        return $this->getSetting()['first_day'] ?? 0;
    }



    /**
     *  get first day of the week if it exists in the database
     *
     * @return int|mixed
     */
    public function calendarType()
    {
        return $this->getSetting()['calendar_type'] ?? '0';
    }



    /**
     * get first day of the week if it exists in the database
     *
     * @return array
     */
    public function businessDays(): array
    {
        return $this->getSetting()['business_days'] ?? [0, 1, 2];
    }



    /**
     * get start of day if it exists in the database
     *
     * @return false|string
     */
    public function startDay()
    {
        return $this->getSetting()['start_day'] ?? '09:30';
    }



    /**
     * get start of day if it exists in the database
     *
     * @return mixed|string
     */
    public function endDay()
    {
        return $this->getSetting()['end_day'] ?? '18:00';
    }



    /**
     * create part random color
     *
     * @return string
     */
    public function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }



    /**
     * create random color
     *
     * @return string
     */
    public function random_color()
    {
        return "#" . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }



    /**
     * return time zone list for combo
     *
     * @return array
     */
    public function getTimeZone()
    {

        $time_zone = $this->getListTimeZone();
        $time_zone = $time_zone
             ->map(function ($key, $value) {
                 return [
                      'id'    => $value,
                      'title' => $key,
                 ];
             })->toArray()
        ;
        $time_zone = array_values($time_zone);

        return $time_zone;
    }



    /**
     * return user department
     *
     * @return array|\Illuminate\Support\Collection
     */
    public function getDepartment()
    {
        $department = user()->roles()->get();
        $department = $department
             ->map(function ($collection) {
                 return [
                      'id'    => $collection->id,
                      'title' => $collection->title,
                      'slug'  => $collection->slug,
                 ];
             })->toArray()
        ;
        return $department;
    }



    /**
     * return time zone list for combo
     *
     * @return \Illuminate\Support\Collection
     */
    public function getListTimeZone()
    {
        $timezones = [
             'Pacific/Midway'       => "(GMT-11:00) Midway Island",
             'US/Samoa'             => "(GMT-11:00) Samoa",
             'US/Hawaii'            => "(GMT-10:00) Hawaii",
             'US/Alaska'            => "(GMT-09:00) Alaska",
             'US/Pacific'           => "(GMT-08:00) Pacific Time (US &amp; Canada)",
             'America/Tijuana'      => "(GMT-08:00) Tijuana",
             'US/Arizona'           => "(GMT-07:00) Arizona",
             'US/Mountain'          => "(GMT-07:00) Mountain Time (US &amp; Canada)",
             'America/Chihuahua'    => "(GMT-07:00) Chihuahua",
             'America/Mazatlan'     => "(GMT-07:00) Mazatlan",
             'America/Mexico_City'  => "(GMT-06:00) Mexico City",
             'America/Monterrey'    => "(GMT-06:00) Monterrey",
             'Canada/Saskatchewan'  => "(GMT-06:00) Saskatchewan",
             'US/Central'           => "(GMT-06:00) Central Time (US &amp; Canada)",
             'US/Eastern'           => "(GMT-05:00) Eastern Time (US &amp; Canada)",
             'US/East-Indiana'      => "(GMT-05:00) Indiana (East)",
             'America/Bogota'       => "(GMT-05:00) Bogota",
             'America/Lima'         => "(GMT-05:00) Lima",
             'America/Caracas'      => "(GMT-04:30) Caracas",
             'Canada/Atlantic'      => "(GMT-04:00) Atlantic Time (Canada)",
             'America/La_Paz'       => "(GMT-04:00) La Paz",
             'America/Santiago'     => "(GMT-04:00) Santiago",
             'Canada/Newfoundland'  => "(GMT-03:30) Newfoundland",
             'America/Buenos_Aires' => "(GMT-03:00) Buenos Aires",
             'Greenland'            => "(GMT-03:00) Greenland",
             'Atlantic/Stanley'     => "(GMT-02:00) Stanley",
             'Atlantic/Azores'      => "(GMT-01:00) Azores",
             'Atlantic/Cape_Verde'  => "(GMT-01:00) Cape Verde Is.",
             'Africa/Casablanca'    => "(GMT) Casablanca",
             'Europe/Dublin'        => "(GMT) Dublin",
             'Europe/Lisbon'        => "(GMT) Lisbon",
             'Europe/London'        => "(GMT) London",
             'Africa/Monrovia'      => "(GMT) Monrovia",
             'Europe/Amsterdam'     => "(GMT+01:00) Amsterdam",
             'Europe/Belgrade'      => "(GMT+01:00) Belgrade",
             'Europe/Berlin'        => "(GMT+01:00) Berlin",
             'Europe/Bratislava'    => "(GMT+01:00) Bratislava",
             'Europe/Brussels'      => "(GMT+01:00) Brussels",
             'Europe/Budapest'      => "(GMT+01:00) Budapest",
             'Europe/Copenhagen'    => "(GMT+01:00) Copenhagen",
             'Europe/Ljubljana'     => "(GMT+01:00) Ljubljana",
             'Europe/Madrid'        => "(GMT+01:00) Madrid",
             'Europe/Paris'         => "(GMT+01:00) Paris",
             'Europe/Prague'        => "(GMT+01:00) Prague",
             'Europe/Rome'          => "(GMT+01:00) Rome",
             'Europe/Sarajevo'      => "(GMT+01:00) Sarajevo",
             'Europe/Skopje'        => "(GMT+01:00) Skopje",
             'Europe/Stockholm'     => "(GMT+01:00) Stockholm",
             'Europe/Vienna'        => "(GMT+01:00) Vienna",
             'Europe/Warsaw'        => "(GMT+01:00) Warsaw",
             'Europe/Zagreb'        => "(GMT+01:00) Zagreb",
             'Europe/Athens'        => "(GMT+02:00) Athens",
             'Europe/Bucharest'     => "(GMT+02:00) Bucharest",
             'Africa/Cairo'         => "(GMT+02:00) Cairo",
             'Africa/Harare'        => "(GMT+02:00) Harare",
             'Europe/Helsinki'      => "(GMT+02:00) Helsinki",
             'Europe/Istanbul'      => "(GMT+02:00) Istanbul",
             'Asia/Jerusalem'       => "(GMT+02:00) Jerusalem",
             'Europe/Kiev'          => "(GMT+02:00) Kyiv",
             'Europe/Minsk'         => "(GMT+02:00) Minsk",
             'Europe/Riga'          => "(GMT+02:00) Riga",
             'Europe/Sofia'         => "(GMT+02:00) Sofia",
             'Europe/Tallinn'       => "(GMT+02:00) Tallinn",
             'Europe/Vilnius'       => "(GMT+02:00) Vilnius",
             'Asia/Baghdad'         => "(GMT+03:00) Baghdad",
             'Asia/Kuwait'          => "(GMT+03:00) Kuwait",
             'Africa/Nairobi'       => "(GMT+03:00) Nairobi",
             'Asia/Riyadh'          => "(GMT+03:00) Riyadh",
             'Europe/Moscow'        => "(GMT+03:00) Moscow",
             'Asia/Tehran'          => "(GMT+03:30) Tehran",
             'Asia/Baku'            => "(GMT+04:00) Baku",
             'Europe/Volgograd'     => "(GMT+04:00) Volgograd",
             'Asia/Muscat'          => "(GMT+04:00) Muscat",
             'Asia/Tbilisi'         => "(GMT+04:00) Tbilisi",
             'Asia/Yerevan'         => "(GMT+04:00) Yerevan",
             'Asia/Kabul'           => "(GMT+04:30) Kabul",
             'Asia/Karachi'         => "(GMT+05:00) Karachi",
             'Asia/Tashkent'        => "(GMT+05:00) Tashkent",
             'Asia/Kolkata'         => "(GMT+05:30) Kolkata",
             'Asia/Kathmandu'       => "(GMT+05:45) Kathmandu",
             'Asia/Yekaterinburg'   => "(GMT+06:00) Ekaterinburg",
             'Asia/Almaty'          => "(GMT+06:00) Almaty",
             'Asia/Dhaka'           => "(GMT+06:00) Dhaka",
             'Asia/Novosibirsk'     => "(GMT+07:00) Novosibirsk",
             'Asia/Bangkok'         => "(GMT+07:00) Bangkok",
             'Asia/Jakarta'         => "(GMT+07:00) Jakarta",
             'Asia/Krasnoyarsk'     => "(GMT+08:00) Krasnoyarsk",
             'Asia/Chongqing'       => "(GMT+08:00) Chongqing",
             'Asia/Hong_Kong'       => "(GMT+08:00) Hong Kong",
             'Asia/Kuala_Lumpur'    => "(GMT+08:00) Kuala Lumpur",
             'Australia/Perth'      => "(GMT+08:00) Perth",
             'Asia/Singapore'       => "(GMT+08:00) Singapore",
             'Asia/Taipei'          => "(GMT+08:00) Taipei",
             'Asia/Ulaanbaatar'     => "(GMT+08:00) Ulaan Bataar",
             'Asia/Urumqi'          => "(GMT+08:00) Urumqi",
             'Asia/Irkutsk'         => "(GMT+09:00) Irkutsk",
             'Asia/Seoul'           => "(GMT+09:00) Seoul",
             'Asia/Tokyo'           => "(GMT+09:00) Tokyo",
             'Australia/Adelaide'   => "(GMT+09:30) Adelaide",
             'Australia/Darwin'     => "(GMT+09:30) Darwin",
             'Asia/Yakutsk'         => "(GMT+10:00) Yakutsk",
             'Australia/Brisbane'   => "(GMT+10:00) Brisbane",
             'Australia/Canberra'   => "(GMT+10:00) Canberra",
             'Pacific/Guam'         => "(GMT+10:00) Guam",
             'Australia/Hobart'     => "(GMT+10:00) Hobart",
             'Australia/Melbourne'  => "(GMT+10:00) Melbourne",
             'Pacific/Port_Moresby' => "(GMT+10:00) Port Moresby",
             'Australia/Sydney'     => "(GMT+10:00) Sydney",
             'Asia/Vladivostok'     => "(GMT+11:00) Vladivostok",
             'Asia/Magadan'         => "(GMT+12:00) Magadan",
             'Pacific/Auckland'     => "(GMT+12:00) Auckland",
             'Pacific/Fiji'         => "(GMT+12:00) Fiji",
        ];
        return collect($timezones);
    }



    /**
     * Getting the roles that are in the calendar
     *
     * @param Calendar $calendar
     *
     * @return mixed
     */
    public function getOldDepartment($calendar)
    {
        $roles        = $calendar->departments()->get();
        $unique_roles = $roles->unique()->values('id')->pluck('id')->toArray();
        if (count($unique_roles) > 0) {
            $roles = implode(',', $unique_roles);
            return $roles;
        }
    }



    /**
     * Detaches all labels from the row
     *
     * @param Calendar $calendar
     *
     * @return int
     */
    public function detachDepartmentAll($calendar)
    {
        $ids = $calendar->departments()->get()->pluck('id')->toArray();

        return $calendar->departments()->detach($ids);
    }

    /**
     * Lists the calendar according to the user's existing roles
     *
     * @return \Illuminate\Support\Collection
     */
    public function userCalendars()
    {
        $user_calendars = user()
             ->roles()
             ->join('calendar_role', 'roles.id', '=', 'calendar_role.role_id')
             ->join('calendars', 'calendar_role.calendar_id', 'calendars.id')
             ->where('calendars.deleted_by', '0')
             ->select('calendars.*')
             ->get()
        ;

        $my_calendars     = $this->myCalendars();
        $mergedCollection = $user_calendars->merge($my_calendars);
        $calendars        = $mergedCollection->unique()->values('id')->all();

        return $calendars;
    }



    /**
     * get calendars that haven't department (personal Calendar)
     *
     * @return Collection
     */
    public function myCalendars()
    {
        $calendars_personal = user()
             ->calendars()
             ->where('calendars.deleted_by', '0')
             ->get()
        ;

        return $calendars_personal;
    }

}
