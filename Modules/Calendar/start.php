<?php


if (!function_exists("calendar")) {

    /**
     *  get an instance of CalendarPermitHelper
     *
     * @return \Modules\Calendar\Services\CalendarPermitHelper
     */
    function calendar()
    {
        return new \Modules\Calendar\Services\CalendarPermitHelper();
    }
}

if (!function_exists('googleClient')) {
    /**
     * get an instance of GClient.php
     *
     * @return \Modules\Calendar\Classes\GClient
     */
    function googleClient()
    {
        return new Modules\Calendar\Classes\GClient();
    }
}
