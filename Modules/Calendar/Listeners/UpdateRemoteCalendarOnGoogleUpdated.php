<?php

namespace Modules\Calendar\Listeners;

use Google_Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Classes\GClient;
use Modules\Calendar\Events\GoogleCalendarUpdated;

class UpdateRemoteCalendarOnGoogleUpdated implements ShouldQueue
{

    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param GoogleCalendarUpdated $event
     *
     * @throws Google_Exception
     */
    public function handle(GoogleCalendarUpdated $event)
    {
        $calendar_model = $event->model;
        $user_id        = $calendar_model->user_id;

        $g_client = new GClient();
        $service  = $g_client->makeService($user_id);
        $calendar = $service->calendars->get($calendar_model->google_id);
        $calendar->setSummary($calendar_model->title);
        $calendar->setDescription($calendar_model->description);
        $calendar->setTimeZone($calendar_model->time_zone);

        $service->calendars->update($calendar_model->google_id, $calendar);
    }
}
