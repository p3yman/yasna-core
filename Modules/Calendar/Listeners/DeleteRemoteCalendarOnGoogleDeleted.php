<?php

namespace Modules\Calendar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Classes\GClient;
use Modules\Calendar\Events\GoogleCalendarDeleted;
use \Google_Exception;

class DeleteRemoteCalendarOnGoogleDeleted implements ShouldQueue
{


    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param GoogleCalendarDeleted $event
     *
     * @throws Google_Exception
     */
    public function handle(GoogleCalendarDeleted $event)
    {
        $calendar = $event->model;
        $user_id  = $calendar->deleted_by;

        $g_client = new GClient();
        $service  = $g_client->makeService($user_id);

        $service->calendars->delete($calendar->google_id);
    }
}
