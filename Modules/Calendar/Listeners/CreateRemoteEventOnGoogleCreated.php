<?php

namespace Modules\Calendar\Listeners;

use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use Google_Service_Calendar_EventReminders;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Classes\GClient;
use Modules\Calendar\Entities\CalendarEvent;
use Modules\Calendar\Events\GoogleCalendarEventCreated;
use \Google_Exception;

class CreateRemoteEventOnGoogleCreated implements ShouldQueue
{

    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param GoogleCalendarEventCreated $event
     *
     * @throws Google_Exception
     */
    public function handle(GoogleCalendarEventCreated $event)
    {
        $event_data = $event->model;
        $calendarId = $event->model->calendar->google_id;
        $user_id    = $event_data->created_by;

        $g_client = new GClient();
        $service  = $g_client->makeService($user_id);

        $google_event = $this->saveInGoogle($event);


        $result = $service->events->insert($calendarId, $google_event);
        $this->insertEventId($result->id, $event_data);
    }



    /**
     * save google_id on event table
     *
     * @param string        $id
     * @param CalendarEvent $event
     */
    public function insertEventId($id, $event)
    {
        $event->update([
             'google_id' => $id,
        ]);
    }



    /**
     * insert event in google calendar
     *
     * @param GoogleCalendarEventCreated $event
     *
     * @return Google_Service_Calendar_Event
     * @throws \Exception
     */
    public function saveInGoogle($event)
    {
        $google_event = new Google_Service_Calendar_Event();
        $google_event->setSummary($event->model->title);
        $google_event->setDescription($event->model->description);

        if ($event->model->all_day == 0) {
            $start = new Google_Service_Calendar_EventDateTime();
            $start->setDateTime($event->google_event->convertDate($event->model->starts_at));
            $google_event->setStart($start);
            $end = new Google_Service_Calendar_EventDateTime();
            $end->setDateTime($event->google_event->convertDate($event->model->ends_at));
            $google_event->setEnd($end);
        } else {
            $start = new Google_Service_Calendar_EventDateTime();
            $start->setDate($event->google_event->convertDateWithAllDay($event->model->starts_at));
            $google_event->setStart($start);

            $end = new Google_Service_Calendar_EventDateTime();
            $end->setDate($event->google_event->convertDateWithAllDay($event->model->ends_at));
            $google_event->setEnd($end);
        }

        $SendEventNotifications = new Google_Service_Calendar_EventReminders();
        $SendEventNotifications->setUseDefault(false);
        $SendEventNotifications->setOverrides($event->google_event->getNotifications());
        $google_event->setReminders($SendEventNotifications);

        return $google_event;
    }
}
