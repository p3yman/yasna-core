<?php

namespace Modules\Calendar\Listeners;

use Exception;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use Google_Service_Calendar_EventReminders;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Classes\GClient;
use Modules\Calendar\Events\GoogleCalendarEventCreated;
use Modules\Calendar\Events\GoogleEventUpdated;

class UpdateRemoteEventOnGoogleUpdated implements ShouldQueue
{

    public $tries = 5;



    /**
     * handle the event.
     *
     * @param GoogleEventUpdated $event
     *
     * @throws \Google_Exception
     */
    public function handle(GoogleEventUpdated $event)
    {
        $event_data = $event->model;
        $calendarId = $event->model->calendar->google_id;
        $user_id    = $event_data->updated_by;

        $g_client     = new GClient();
        $service      = $g_client->makeService($user_id);
        $google_event = $service->events->get($calendarId, $event_data->google_id);

        $this->saveInGoogle($google_event, $event);

        $service->events->update($calendarId, $google_event->getId(), $google_event);

    }



    /**
     * insert event in google calendar
     *
     * @param Google_Service_Calendar_Event $google_event
     * @param GoogleCalendarEventCreated    $event
     *
     * @throws Exception
     */
    public function saveInGoogle($google_event, $event)
    {
        $google_event->setSummary($event->model->title);
        $google_event->setDescription($event->model->description);

        if ($event->model->all_day == 0) {
            $start = new Google_Service_Calendar_EventDateTime();
            $start->setDateTime($event->google_event->convertDate($event->model->starts_at));
            $google_event->setStart($start);
            $end = new Google_Service_Calendar_EventDateTime();
            $end->setDateTime($event->google_event->convertDate($event->model->ends_at));
            $google_event->setEnd($end);
        } else {
            $start = new Google_Service_Calendar_EventDateTime();
            $start->setDate($event->google_event->convertDateWithAllDay($event->model->starts_at));
            $google_event->setStart($start);

            $end = new Google_Service_Calendar_EventDateTime();
            $end->setDate($event->google_event->convertDateWithAllDay($event->model->ends_at));
            $google_event->setEnd($end);
        }

        $SendEventNotifications = new Google_Service_Calendar_EventReminders();
        $SendEventNotifications->setUseDefault(false);
        $SendEventNotifications->setOverrides($event->google_event->getNotifications());
        $google_event->setReminders($SendEventNotifications);
    }

}
