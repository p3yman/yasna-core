<?php

namespace Modules\Calendar\Listeners;

use Google_Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Events\GoogleCalendarSync;

class SyncRemoteCalendarsOnGoogleSync implements ShouldQueue
{

    public $tries = 15;



    /**
     * Handle the event.
     *
     * @param GoogleCalendarSync $event
     *
     * @throws Google_Exception
     * @throws \Exception
     */
    public function handle(GoogleCalendarSync $event)
    {
        $google_calendar = $event->calendar->calendarList();
        $this->saveNextSyncToken($event, $google_calendar->nextSyncToken);
        $event->calendar->saveGoogleCalendar($google_calendar);
    }



    /**
     * save syncToken in meta field in users table
     *
     * @param GoogleCalendarSync $event
     * @param string             $token
     */
    public function saveNextSyncToken($event, $token)
    {
        $event->calendar->saveSyncToken($token);
    }

}
