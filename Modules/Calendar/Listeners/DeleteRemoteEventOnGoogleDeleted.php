<?php

namespace Modules\Calendar\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Classes\GClient;
use Modules\Calendar\Events\GoogleCalendarEventDeleted;
use \Google_Exception;

class DeleteRemoteEventOnGoogleDeleted implements ShouldQueue
{

    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param GoogleCalendarEventDeleted $event
     *
     * @throws Google_Exception
     */
    public function handle(GoogleCalendarEventDeleted $event)
    {
        $event_data = $event->model;
        $calendarId = $event->model->calendar->google_id;
        $user_id    = $event_data->deleted_by;

        $g_client = new GClient();
        $service  = $g_client->makeService($user_id);

        $service->events->delete($calendarId, $event_data->google_id);
    }
}
