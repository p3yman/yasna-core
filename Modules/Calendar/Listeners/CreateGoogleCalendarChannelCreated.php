<?php

namespace Modules\Calendar\Listeners;

use Exception;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Events\GoogleCalendarChannelCreated;

class CreateGoogleCalendarChannelCreated implements ShouldQueue
{
    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param GoogleCalendarChannelCreated $event
     *
     * @throws Exception
     */
    public function handle(GoogleCalendarChannelCreated $event)
    {
        $calendar_id = $event->model->google_id;
        $user_id     = $event->model->user_id;
        $token       = user($user_id)->preference('GoogleCalendar')['access_token'];
        $event->channel->createChannel($calendar_id, $token, $user_id);
    }
}
