<?php

namespace Modules\Calendar\Listeners;

use Exception;
use Google_Exception;
use Google_Service_Calendar_Calendar;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Calendar\Classes\GClient;
use Modules\Calendar\Entities\Calendar;
use Modules\Calendar\Events\GoogleCalendarCreated;
use Webpatser\Uuid\Uuid;

class CreateRemoteCalendarOnGoogleCreated implements ShouldQueue
{

    public $tries = 5;



    /**
     * Handle the event.
     *
     * @param GoogleCalendarCreated $event
     *
     * @throws Google_Exception
     * @throws Exception
     */
    public function handle(GoogleCalendarCreated $event)
    {
        $data_calendar = $event->model;
        $user_id       = $data_calendar->user_id;
        $g_client      = new GClient();
        $service       = $g_client->makeService($user_id);
        $calendar      = new Google_Service_Calendar_Calendar();
        $calendar->setSummary($data_calendar->title);
        $calendar->setDescription($data_calendar->description);
        $calendar->setTimeZone($data_calendar->time_zone);
        $calendar = $service->calendars->insert($calendar);

        $this->insertCalendarId($calendar->id, $data_calendar);
        $this->createChannel($calendar->id, $user_id);


    }



    /**
     * save google_id on calendar table
     *
     * @param string   $id
     * @param Calendar $calendar
     */
    public function insertCalendarId($id, $calendar)
    {
        $calendar->update([
             'google_id' => $id,
        ]);
    }



    /**
     * create channel for syn events from google on app
     *
     * @param string $calendar_id
     * @param int    $user_id
     *
     * @throws Exception
     */
    public function createChannel($calendar_id, $user_id)
    {
        $token = user($user_id)->preference('GoogleCalendar')['access_token'];

        $url     = sprintf("https://www.googleapis.com/calendar/v3/calendars/$calendar_id/events/watch");
        $fields  = $this->setParameters();
        $headers = $this->setHeaders($token);

        /* send POST request */
        $channel = curl_init();
        curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($channel, CURLOPT_URL, $url);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($channel, CURLOPT_POST, true);
        curl_setopt($channel, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($channel, CURLOPT_TIMEOUT, 3);
        $response = curl_exec($channel);

        try {
            $this->saveChannel($response, $calendar_id, $user_id);
        } catch (Exception $e) {
            error_log($response);
        }

        curl_close($channel);
    }



    /**
     * setup POST headers
     *
     * @param string $token
     *
     * @return array
     */
    public function setHeaders($token)
    {
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;

        return $headers;
    }



    /**
     * setup the POST parameters
     *
     * @return false|string
     * @throws Exception
     */
    public function setParameters()
    {
        $uuid = Uuid::generate();

        return json_encode([

             'id'      => $uuid->string,
             'type'    => "web_hook",
             'token'   => "token_$uuid->string",
             'address' => sprintf("https://cb.yasna.team/callback/b1071d6a-ce5b-4268-8f4e-3a6668c349e6"),
        ]);
    }



    /**
     * save channel response in DB
     *
     * @param JsonResource $response
     * @param string       $calendar_id
     * @param int          $user_id
     */
    public function saveChannel($response, $calendar_id, $user_id)
    {
        $response = json_decode($response);
        $model    = model('google-channel');

        $model->batchSave([
             'user_id'      => $user_id,
             'channel_id'   => $response->id,
             'resource_id'  => $response->resourceId,
             'resource_uri' => $response->resourceUri,
             'token'        => $response->token,
             'expiration'   => $response->expiration,
             'calendar_id'  => $calendar_id,
        ]);
    }


}
