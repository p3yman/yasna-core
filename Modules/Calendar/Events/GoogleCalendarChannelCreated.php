<?php

namespace Modules\Calendar\Events;

use Illuminate\Database\Eloquent\Collection;
use Modules\Calendar\Classes\GEventChannel;
use Modules\Yasna\Services\YasnaEvent;

class GoogleCalendarChannelCreated extends YasnaEvent
{
    public $model_name = 'Calendar';

    public $channel;



    /**
     * GoogleCalendarChannelCreated constructor.
     *
     * @param Collection|bool $model
     * @param YasnaModel|null $old_instance
     */
    public function __construct($model = false, ?YasnaModel $old_instance = null)
    {
        $this->channel = new GEventChannel();
        parent::__construct($model, $old_instance);
    }
}
