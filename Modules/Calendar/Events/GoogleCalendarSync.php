<?php

namespace Modules\Calendar\Events;

use Modules\Calendar\Classes\GCalendar;
use Modules\Calendar\Entities\Calendar;
use Modules\Yasna\Services\YasnaEvent;

class GoogleCalendarSync extends YasnaEvent
{
    public $model_name = 'Calendar';
    public $calendar;



    /**
     * GoogleCalendarSync constructor.
     *
     * @param bool|Calendar   $model
     * @param YasnaModel|null $old_instance
     */
    public function __construct($model = false, ?YasnaModel $old_instance = null)
    {
        $this->calendar = new GCalendar($model);
        parent::__construct($model, $old_instance);
    }
}
