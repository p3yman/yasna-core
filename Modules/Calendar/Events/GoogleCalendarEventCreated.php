<?php

namespace Modules\Calendar\Events;

use Modules\Calendar\Classes\GEvent;
use Modules\Calendar\Entities\CalendarEvent;
use Modules\Yasna\Services\YasnaEvent;

class GoogleCalendarEventCreated extends YasnaEvent
{
    public $model_name = 'calendarEvent';
    public $google_event;



    /**
     * GoogleCalendarEventCreated constructor.
     *
     * @param bool|CalendarEvent $model
     * @param YasnaModel|null    $old_instance
     */
    public function __construct($model = false, ?YasnaModel $old_instance = null)
    {
        $this->google_event = new GEvent($model);
        parent::__construct($model, $old_instance);
    }

}
