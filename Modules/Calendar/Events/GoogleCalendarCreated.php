<?php

namespace Modules\Calendar\Events;

use Modules\Yasna\Services\YasnaEvent;

class GoogleCalendarCreated extends YasnaEvent
{
    public $model_name = "calendar";

}
