<?php

namespace Modules\Calendar\Events;

use Modules\Yasna\Services\YasnaEvent;

class GoogleCalendarUpdated extends YasnaEvent
{
    public $model_name = 'Calendar';
}
