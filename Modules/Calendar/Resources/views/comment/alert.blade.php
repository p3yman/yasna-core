{!!
	Widget('modal')
	->target(route('deleteComment'))
	->label(trans_safe('calendar::general.comments.delete_comment'))
	->class('form-horizontal')
!!}

<div class="modal-body">

	{!!
		widget('hidden')
		->name('id')
		->value($id)
	!!}

	<div>{{trans_safe('calendar::general.message.delete_comment')}}</div>
</div>
<div class="modal-footer">
	{!!
		Widget('button')
		->type('submit')
		->label("tr:calendar::general.forms.button.delete")
		->class('btn-primary')
	 !!}

	{!!
		Widget('button')
		->label(trans('manage::forms.button.cancel'))
		->id('btnCancel')
		->shape('link')
		->onClick('$(".modal").modal("hide")')
	 !!}

	{!!
		Widget('feed')
	!!}

</div>
