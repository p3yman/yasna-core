@php
	isset($format) ?: $format = 'j F Y [H:i]';
@endphp
@if(is_array($data))
	@foreach($data as $item)
		<div class="box-comment clearfix">
			<div class="user-name-comment">
				<span class="text-primary text-bold mr">
					{{$item['comment']->user->name_first . ' '. $item['comment']->user->name_last}}
				</span>

				<small>
					<i>
						{{ pd(jDate::forge($item['comment']->created_at)->format($format)) }}
					</i>
				</small>
				<div class="pull-right">
					@if(calendar()->canDeleteComment())
						<a class="btn btn-outline btn-xs btn-danger" href="javascript:void(0)"
						   onclick="{{'masterModal("'. route('commentDeleteAlert',$item['comment']->id) .'")'}}"
						   style="font-size: 12px">
							<i class="fa fa-trash fa-fw m0"></i>
						</a>
					@endif
				</div>
			</div>
			<div class="text-comment">
				<div class="mv10 pl-xl">{{$item['comment']->text}}</div>
				@if(isset($item['files']) && !$item['files']->isEmpty())
					<div class="mt">
						@foreach($item['files'] as $file)
							<a href="/uploads/{{$file->file_name}}" target="_blank" class="btn btn-default">
								<i class="fa fa-file"></i>
								{{$file->original_name}}
							</a>
						@endforeach
					</div>
				@endif

			</div>
		</div>
	@endforeach
@endif
