@include('manage::layouts.sidebar-link' , [
	'icon'       => 'calendar-alt',
	'caption'    => trans('calendar::general.sidebar_menu_title'),
	'link'       => 'calendar',
])
