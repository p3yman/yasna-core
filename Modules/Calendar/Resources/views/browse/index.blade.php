@extends('manage::layouts.template')
@section('content')
	<div class="row ">
		<div class="col-lg-9 col-md-8">
			@if($errors->any())
				<div class="alert alert-warning calendar-warning">
					{{$errors->first()}}
				</div>
			@endif
			@if($message)
				<div class="alert alert-warning calendar-warning">
					{{trans('calendar::general.message.apply_settings')}}
				</div>
			@endif
			<div>
				<i class="fa fa-spin fa-spinner hidden" id="calendar-loading-icon"
				   style="position: absolute;top:-1.2em;font-size: 1.5em;"></i>
			</div>
			<div class="panel panel-default" id="yasnaCalendar-panel">
				<div id="reload_calendar" class="panel-body" data-src="{{url('manage/calendar/view')}}">
					@include('calendar::browse.calendar_view')
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-4">
			<div class="panel">
				<div class="panel-heading">
					{{ trans('calendar::general.calendars') }}

					<div class="pull-right">
						@if(calendar()->canCreateCalendar())
							<span style="cursor: pointer"
								  data-toggle="tooltip"
								  title="{{ trans('calendar::general.create_calendar') }}"
								  onclick='masterModal("{{ route('createCalendar') }}")'>
								<i class="fa fa-plus text-primary"></i>
							</span>
						@endif
					</div>
				</div>
				<div class="panel-body">
					<div class="calendar-box" id="calendar-box" data-src="{{url('manage/calendar/show')}}">
						@include('calendar::calendar.showCalendar')
					</div>
				</div>
			</div>

			<div class="panel">
				<div class="panel-heading">
					{{ trans('calendar::general.event.events') }}

					<div class="pull-right">
						@if(calendar()->canCreateEvent())
							<span style="cursor: pointer"
								  data-toggle="tooltip"
								  title="{{ trans('calendar::general.event.add_event') }}"
								  onclick='masterModal("{{ route('createEvent') }}")'>
								  <i class="fa fa-plus text-primary"></i>
							</span>
						@endif
					</div>
				</div>
				<div class="panel-body">
					<div class="event-box" id="event-calendar" data-src="{{url('manage/calendar/event/show')}}">
						@include('calendar::event.show_event_list')
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
@section('html_footer')
	<script>
        $(document).ready(function () {
            var fetch = true;

            // function LoopForever(callback = function () {
            // }) {
            //     $('#yasnaCalendar').fullCalendar('refetchEvents');
            //     callback();
            // }
            //
            // self.setInterval(function () {
            //     if (fetch) {
            //         fetch = false;
            //         LoopForever(function () {
            //             fetch = true;
            //         });
            //     }
            // }, 15000);
        });
        $('.whirl-body').removeClass('whirl standard');

        function reloadFullCalender() {
            $('#yasnaCalendar').fullCalendar('refetchEvents');
            divReload('event-lists');
        }

        function newEvent(event) {
            event.preventDefault();
            masterModal(url("manage/calendar/event"), "lg");
        }

        function openEvent($event, $hashid) {
            $event.preventDefault();
            masterModal(url("manage/calendar/event/detail/" + $hashid), "lg");
        }

        function deleteFile($this) {
            var file_hash_id  = $($this).data('file-hashid');
            var event_hash_id = $($this).data('event-hashid');

            $.ajax({
                method : 'DELETE',
                url    : url('manage/calendar/event/delete/' + event_hash_id + '/file/' + file_hash_id),
                data   : {
                    '_token': '{{ csrf_token() }}'
                },
                success: function () {
                    $('#' + file_hash_id).remove();
                }
            })
        }

	</script>
@append

