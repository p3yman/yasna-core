<div class="calendar-list">
	@if(count($calendars) > 0)
		@foreach($calendars as $calendar)
			<div class="row-calendar">
				<div>
					{!!
						widget('checkbox')
						->label($calendar->title)
						->containerClass('js_customColorfulCheckbox')
						->extra_array([
						'checked'=>'true',
						'data-color'=>$calendar->color
						])
					!!}
				</div>
				<div>
					@if(user()->as('admin'))
						<div class="dropdown">
							<button class="btn-action dropdown-toggle" type="button" data-toggle="dropdown">
								<i class="fas fa-ellipsis-v"></i>
							</button>
							<ul class="dropdown-menu animated flipInX">
								@if(calendar()->canEditCalendar())
									<li>
										<a href="javascript:void(0)"
										   onclick="{{'masterModal("'. route('createCalendar',$calendar->id) .'")'}}"
										   style="font-size: 12px">
											<i class="fa fa-pencil fa-fw"></i>
											{{trans('calendar::general.forms.button.edit')}}
										</a>
									</li>
								@endif
								@if(calendar()->canDeleteCalendar())
									<li>
										<a href="javascript:void(0)"
										   onclick="{{'masterModal("'. route('deleteAlert',$calendar->id) .'")'}}"
										   style="font-size: 12px">
											<i class="fa fa-trash-o fa-fw"></i>
											{{trans('calendar::general.delete')}}
										</a>
									</li>
								@endif
							</ul>
						</div>
					@endif
				</div>
			</div>
		@endforeach
	@else
		<small class="ph-lg">
			{{ trans('calendar::general.no_calendar') }}
		</small>
	@endif
</div>
<script>
    $(document).ready(function () {
        $('.row-calendar').find('.js_customColorfulCheckbox input').each(function () {
            var color = $(this).attr('data-color');
            $(this).siblings('span.fa-check').css({
                "background-color": color,
                "border-color"    : color
            });
        })
    })
</script>
