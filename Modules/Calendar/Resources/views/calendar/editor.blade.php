{!!
Widget('modal')
->targetIf(!$calendar->exists,route('storeCalendar'))
->targetIf($calendar->exists,route('updateCalendar'))
->labelIf(!$calendar->exists,trans_safe('calendar::general.create_calendar'))
->labelIf($calendar->exists,trans_safe('calendar::general.update_calendar'))
->class('form-horizontal update-form')
!!}


<div class="modal-body">
	{!!
		widget('hidden')
		->name('hashid')
		->value($calendar->hashid ?? '')
	!!}

	{!!
		widget('input')
		->name('title')
		->value($calendar->title ?? '')
		->inForm()
		->label(trans_safe('calendar::general.title'))
	!!}
	{!!
		widget('input')
		->name('description')
		->value($calendar->description ?? '')
		->inForm()
		->label(trans_safe('calendar::general.forms.label.description'))
	!!}
	@if (isset($calendar->color))
		{!!
			widget('colorPicker')
			->name('color')
			->inForm()
			->value($calendar->color)
			->label(trans_safe('calendar::general.color'))
		!!}
	@endif
	{!!
		widget('combo')
		->name('time_zone')
		->inForm()
		->options($time_zone)
		->value($calendar->time_zone ?? 'Asia/Tehran')
		->class('ltr')
		->searchable()
		->label(trans_safe('calendar::general.time_zone'))
	!!}
	{!!
		widget('selectize')
		->name('department')
		->inForm()
		->options($department ?? '')
		->value($old_department ?? '')
		->valueField('id')
		->labelField('title')
		->label(trans_safe('calendar::general.department'))
	!!}


</div>
<div class="modal-footer">
	{!!
		Widget('button')
		->type('submit')
		->label("tr:calendar::general.forms.button.save")
		->class('btn-success btn-taha')
	 !!}

	{!!
		Widget('button')
		->label(trans('manage::forms.button.cancel'))
		->id('btnCancel')
		->shape('link')
		->onClick('$(".modal").modal("hide")')
	 !!}

	{!!
		Widget('feed')
	!!}

</div>
<script>
	@if(isset($calendar->color))
    $(document).ready(function () {
        var color = '{!! $calendar->color !!}';
        $('.current-color').css({
            "background-color": color,
            "border-color"    : color
        });
    });
	@endif
</script>
