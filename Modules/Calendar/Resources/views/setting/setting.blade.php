{!!
	Widget('modal')
	->target(route('storeSetting'))
	->label('tr:calendar::general.settings')
	->class('form-horizontal')
!!}

<div class="modal-body">
	<div class="form-group">
		<label class="col-sm-2 control-label">{{ trans('calendar::general.first_day_of_week') }}</label>
		<div class="col-sm-10">
			<div class="btn-group btn-group-toggle" data-toggle="buttons" id="days">

				@foreach($data['days'] as $key => $day)
					<label class="btn btn-primary btn-outline @if($key==$data['first_day']) active @endif">
						<input type="radio" name="first_day_of_week" id="{{ $day }}" value="{{ $key }}"
							   autocomplete="off" @if($key==$data['first_day']) checked @endif>
						{{ trans('calendar::general.week.days.full.' . strtolower($day)) }}
					</label>
				@endforeach
			</div>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">{{ trans('calendar::general.calendar_type') }}</label>

		<div class="col-sm-10">
			<div class="btn-group btn-group-toggle" data-toggle="buttons" id="calendar_types">
				@foreach($data['types'] as $key => $type)
					<label class="btn btn-primary btn-outline @if($key == $data['calendar_type']) active @endif">
						<input type="radio" name="calendar_type" id="{{ $type }}" value="{{ $loop->index }}"
							   autocomplete="off"
							   @if($key == $data['calendar_type']) checked @endif>
						{{ trans('calendar::general.type.values.' . strtolower($type)) }}
					</label>
				@endforeach
			</div>
		</div>

	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label">{{ trans('calendar::general.business_days') }}</label>

		<div class="col-sm-10">
			<div class="btn-group btn-group-toggle" data-toggle="buttons" id="days">
				@foreach($data['days'] as $key => $day)
					<label class="btn btn-primary btn-outline @if(in_array($key,$data['business_days'])) active @endif">
						<input type="checkbox" name="business_days[]" id="{{ $day }}" value="{{ $key }}"
							   autocomplete="off" @if(in_array($key,$data['business_days'])) checked @endif>
						{{ trans('calendar::general.week.days.full.' . strtolower($day)) }}
					</label>
				@endforeach
			</div>
		</div>

	</div>
	{!!
	   widget('persian-date-picker')
	   ->id('start_of_day')
	   ->class('pdatepickerClass3')
	   ->label(trans('calendar::general.start_of_day'))
	   ->name('start_of_day')
	   ->inForm()
	   ->value($data['start_day'])
	   ->canNotScroll()
	   ->onlyTimePicker()
	!!}

	{!!
	   widget('persian-date-picker')
	   ->id('end_of_day')
	   ->class('pdatepickerClass3')
	   ->label(trans('calendar::general.end_of_day'))
	   ->name('end_of_day')
	   ->inForm()
	   ->value($data['end_day'])
	   ->canNotScroll()
	   ->onlyTimePicker()
	!!}
</div>
<div class="modal-footer">
	{!!
		Widget('button')
		->type('submit')
		->label("tr:calendar::general.forms.button.save")
		->class('btn-success btn-taha')
	 !!}

	{!!
		Widget('button')
		->label(trans('manage::forms.button.cancel'))
		->id('btnCancel')
		->shape('link')
		->onClick('$(".modal").modal("hide")')
	 !!}

	{!!
	Widget('feed')
	!!}


	<div style="float: left;">
		@if(!googleClient()->hasAccessToken(user()->id))
			<a class="btn btn-success" style="margin-right: 10px;" href="{{ googleClient()->generateAccessLink() }}">
				{{ trans('calendar::general.google.connect_google') }}
			</a>
		@endif
	</div>

</div>
