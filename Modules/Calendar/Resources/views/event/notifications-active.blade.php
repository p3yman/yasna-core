@php
	$notif_type = [
		    	[
		    		"id" => "0" ,
		    		"title" => trans('calendar::general.notifications.email') ,
		    	],
		    	[
		    		"id" => "1" ,
		    		"title" => trans('calendar::general.notifications.google_notification') ,
		    	],
		    ];

	$time_unit_list = [
		[
			"id" => "0" ,
			"title" => trans('calendar::general.time_units.minutes') ,
		],
		[
			"id" => "1" ,
			"title" => trans('calendar::general.time_units.hours') ,
		],
		[
			"id" => "2" ,
			"title" => trans('calendar::general.time_units.day') ,
		],
		[
			"id" => "3" ,
			"title" => trans('calendar::general.time_units.week') ,
		],
	];
	$time_unit_list_all = [
			[
				"id" => "2" ,
				"title" => trans('calendar::general.time_units.day') ,
			],
			[
				"id" => "3" ,
				"title" => trans('calendar::general.time_units.week') ,
			],
		];
@endphp
@foreach($eventNotification as $notify)
	<div class="notification-form-row panel active-notif" id="notification_row_{{ $notify->hashid }}">
		<div class="panel-heading">
			{{trans('calendar::general.notifications.notification')}}

			<div class="pull-right">
				<span style="cursor: pointer"
					  data-toggle="tooltip"
					  class="js_removeNotificationRow"
					  data-id="{{ $notify->hashid }}"
					  title="{{ trans('calendar::general.notifications.remove') }}">
					  <i class="fa fa-trash text-red"></i>
				</span>
			</div>
		</div>

		<div class="panel-body">
			<div class="row mb-lg">
				<div class="col-md-10">
					{!!
						widget('combo')
						->label('tr:calendar::general.notifications.type')
						->inForm()
						->options($notif_type)
						->value($notify->type)
						->name('notifications[' . $notify->hashid . '][type]')
					 !!}
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-sm-10">
							{!!
								widget('input')
								->type('number')
								->label(' ')
								->value($notify->meta['notify']['number_time'])
								->extra_array([
								'min'=>'1',
								])
								->inForm()
								->name('notifications[' . $notify->hashid . '][number_time]')
							 !!}
						</div>
						<div class="col-sm-2 time-combo">
							{!!
								widget('combo')
								->inForm()
								->label(' ')
								->labelClass('noDisplay')
								->options($time_unit_list)
								->class('time_type_combo')
								->valueField('id')
								->labelField('title')
								->value($notify->meta['notify']['time_type'])
								->name('notifications[' . $notify->hashid . '][time_type]')
							 !!}
						</div>
						<div class="col-sm-2 all-time-combo" style="display: none;">
							{!!
								widget('combo')
								->inForm()
								->label(' ')
								->labelClass('noDisplay')
								->options($time_unit_list_all)
								->valueField('id')
								->labelField('title')
								->class('time_type_combo')
								->name('notifications[' . $notify->hashid . '][time_type]')
							 !!}
						</div>
					</div>
				</div>
			</div>
			<div class="row time_combo">
				<div class="col-md-10">
					{!!
					   widget('combo')
					   ->id('notify_date_'. $notify->hashid)
					   ->label('tr:calendar::general.notifications.before_at')
					   ->options($notification['time'])
					   ->value($notify->meta['notify']['time'])
					   ->inForm()
					   ->name('notifications[' . $notify->hashid . '][time]')
					!!}
				</div>
			</div>
		</div>
	</div>
@endforeach
