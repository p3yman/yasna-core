<?php
isset($format) ?: $format = 'j F Y [H:i]';

$time_unit_list = [
     [
          "id"    => "0",
          "title" => trans('calendar::general.time_units.minutes'),
     ],
     [
          "id"    => "1",
          "title" => trans('calendar::general.time_units.hours'),
     ],
     [
          "id"    => "2",
          "title" => trans('calendar::general.time_units.day'),
     ],
     [
          "id"    => "3",
          "title" => trans('calendar::general.time_units.week'),
     ],
];

?>
{!!
Widget('modal')
//->target(route('storeComment'))
->label(trans_safe('calendar::general.event.event_title'))
->class('form-horizontal comment-form')
!!}

<div class="modal-body">
	<div class="mt0 mv40">
		<h3>
			<i class="fa fa-calendar"></i>
			{{ $event['data']->title }}
		</h3>

		<hr>

		<div class="row mb">
			<div class="col-xs-2">
				{{ trans('calendar::general.event.starts_at').": " }}

			</div>
			<div class="col-xs-10">
				<b>
					{{pd(jDate::forge($event['data']->starts_at)->format('j F Y [H:i]'))}}
				</b>
			</div>

		</div>
		<div class="row mb">
			<div class="col-xs-2">
				{{ trans('calendar::general.event.ends_at').": " }}

			</div>
			<div class="col-xs-10">
				<b>
					{{pd(jDate::forge($event['data']->ends_at)->format('j F Y [H:i]'))}}
				</b>
			</div>
		</div>

		<div class="row mb">
			<div class="col-xs-2">
				{{ trans('calendar::general.description').": " }}

			</div>
			<div class="col-xs-10">
				<b>
					{{pd($event['data']->description)}}
				</b>
			</div>
		</div>

		<div class="row mb">
			<div class="col-xs-2">
				{{ trans('calendar::general.label').": " }}

			</div>
			<div class="col-xs-10">
				@if(is_array($event['labels']))
					@foreach($event['labels'] as $label)
						<label id="{{$id or ''}}" class="badge badge-custom bg-info">
							<span>
								{{ pd($label['fa']) }}
							</span>
						</label>
					@endforeach
				@endif
			</div>
		</div>

		<div class="row mb">
			<div class="col-xs-2">
				{{ trans('calendar::general.attached_files').": " }}

			</div>
			<div class="col-xs-10">
				@if(is_array($event['files']))
					@foreach($event['files'] as $file)
						<a href="{{ $file['link'] }}" target="_blank" class="btn btn-default">
							<i class="fa fa-file"></i>
							{{ $file['title'] }}
						</a>
					@endforeach
				@endif
			</div>
		</div>
		<div class="row mb">
			<div class="col-xs-2">
				{{ trans('calendar::general.notifications.notification').": " }}

			</div>
			<div class="col-xs-10">
				@if(!$event['notifications']->isEmpty())
					@foreach($event['notifications'] as $notification)
						<div>
							<i class="fas fa-bell"></i>
							@if($event['data']->all_day == 1)
								<span>{{$notification->meta['notify']['number_time']}}</span>
								<span>{{$time_unit_list[$notification->meta['notify']['time_type']]['title']}}</span>
								<span>{{ trans('calendar::general.notifications.before_at')}}</span>
								<span>{{$notification->meta['notify']['time']}}</span>
							@else
								<span>{{$notification->meta['notify']['number_time']}}</span>
								<span>{{$time_unit_list[$notification->meta['notify']['time_type']]['title']}}</span>
								<span>{{ trans('calendar::general.notifications.before')}}</span>
							@endif

						</div>
					@endforeach
				@endif
			</div>
		</div>


	</div>


	<div class="panel panel-primary mt0 mv40">
		<div class="panel-heading">
			<i class="fa fa-comment"></i>
			{{trans_safe('calendar::general.comments.send_comment')}}
		</div>
		{!!
			widget('form-open')
			->target(route('storeComment'))
			->class('js')
		 !!}

		<div class="panel-body">

			{!!
				widget('hidden')
				->name('commentFiles')
				->id('file_id')
				->value('')
			!!}
			{!!
				widget('Textarea')
				->name('comment')
				->inForm()
				->id('comment-text')
				->label(trans_safe('calendar::general.comments.comment_text'))
			!!}
			{!!
				widget('hidden')
				->name('event_id')
				->id('event_id')
				->value($event['data']->hashid)
			!!}

			<hr>
			@include('uploader::dropzone',[
						"in_form" => true ,
						"name" => "dropzone" ,
						"required" => false ,
						"id" => "files" ,
						"max_file" => 2 ,
						"files" => [],
						"label"=>null
					])

		</div>
		<div class="panel-footer row">
			{!!
				Widget('button')
					->type('submit')
					->label("tr:calendar::general.comments.send_comment")
					->class('btn-success btn-lg btn-taha pull-right')
					->id('submitComment')
			 !!}

			{!!
				Widget('feed')
			!!}
		</div>

		{!! widget('form-close') !!}

	</div>


	<div>
		<h4 class="mb-xl">
			<i class="fa fa-comments"></i>
			{{trans_safe('calendar::general.comments.comments')}}
		</h4>
		<div class="text-comment" id="parent-box-comment"
			 data-src="{{route('reloadComment',$event['data']->id)}}">
			@include('calendar::comment.comment',['data'=>$data])
		</div>
	</div>


</div>

<div class="modal-footer">
	{!!
		Widget('button')
		->label(trans('manage::forms.button.edit'))
		->shape('primary')
		->class('btn-taha')
		->onClick('masterModal("'. route('editEvent',$event['data']->id) .'")')
	 !!}

	{!!
		Widget('button')
		->label(trans('manage::forms.button.cancel'))
		->id('btnCancel')
		->shape('link')
		->onClick('$(".modal").modal("hide")')
	 !!}


</div>

<script>

    $(document).ready(function () {
        $('#submitComment').click(function (e) {
            var comment      = $('#comment-text').val();
            var event_id     = $('#event_id').val();
            var commentFiles = $('#files').val();
            dropzone         = [];
            $('#files_files a').each(function () {
                dropzone.push($(this).prop('id'));
            });
            e.preventDefault();
            $.ajax({
                url    : "{{route('storeComment')}}",
                data   : {
                    _token      : '{{csrf_token()}}',
                    comment     : comment,
                    event_id    : event_id,
                    commentFiles: commentFiles,
                    dropzone    : dropzone.length ? dropzone : []
                },
                type   : "POST",
                success: function () {
                    divReload('parent-box-comment');
                }
            });
        });


    });
</script>
