{!!
Widget('modal')
->targetIf(!isset($event),route('storeEvent'))
->targetIf(isset($event),route('updateEvent'))
->labelIf(!isset($event),trans_safe('calendar::general.event.add_event'))
->labelIf(isset($event),trans_safe('calendar::general.event.edit_event'))
->class('form-horizontal update-form')
!!}

<div class="modal-body">

	<fieldset>
		<legend>
			{{trans('calendar::general.event.event_info')}}
		</legend>
		{!!
			widget('hidden')
			->name('hashid')
			->value($event->hashid ?? '')
		!!}

		{!!
			widget('input')
			->name('title')
			->value($event->title ?? '')
			->inForm()
			->label(trans_safe('calendar::general.event.event_title'))
		!!}
		{!!
			widget('input')
			->name('description')
			->value($event->description ?? '')
			->inForm()
			->label(trans_safe('calendar::general.forms.label.description'))
		!!}
		{!!
			widget('combo')
			->name('calendar_id')
			->options($calendars)
			->value($event->calendar_id ?? '')
			->inForm()
			->label(trans_safe('calendar::general.calendars'))
		!!}
		<div id="is_all_day_body"></div>
		<div id="is_not_all_day_body"></div>

		{!!
			widget('checkbox')
			->name('all_day')
			->inForm()
			->id('all_day')
			->value($event->all_day ?? 0)
			->label(trans_safe('calendar::general.all_day'))
		!!}


		{!!
			widget("selectize")
			->name('labels')
			->inForm()
			->options($labels)
			->value($eventLabels ?? '')
			->valueField('slug')
			->captionField('title')
			->create(true)
			->label(trans_safe('calendar::general.label'))
		!!}
	</fieldset>
	@if(isset($eventNotification))
		@include('calendar::event.notifications-active',['notification'=>$notification['combo'],'eventNotification'=>$eventNotification])
	@endif
	<fieldset>
		<legend>
			{{trans('calendar::general.notifications.set')}}
		</legend>

		<div id="notification-rows-body">
		</div>


		<div class="row">
			<div class="col-sm-12 col-center">
				<button class="btn btn-primary"
						type="button"
						id="addNotificationRow">
					<i class="fa fa-bell-o"></i>
					{{trans_safe('calendar::general.notifications.add')}}
				</button>
			</div>
		</div>
	</fieldset>


	<fieldset>
		<legend>
			{{trans('calendar::general.attached_file')}}
		</legend>
		@include('uploader::dropzone',[
					"in_form" => true ,
					"name" => "dropzone" ,
					"required" => false ,
					"id" => "dr1" ,
					"max_file" => 2 ,
					"files" => $files ?? []
				])
	</fieldset>

</div>

<div class="modal-footer">
	{!!
		Widget('button')
		->type('submit')
		->label("tr:calendar::general.forms.button.save")
		->class('btn-success btn-taha')
		->id('upload')
	 !!}

	{!!
		Widget('button')
		->label(trans('manage::forms.button.cancel'))
		->id('btnCancel')
		->shape('link')
		->onClick('$(".modal").modal("hide")')
	 !!}

	{!!
		Widget('feed')
	!!}

</div>

<script id="notificationFormRow" type="calendar/template">
	@include('calendar::event.notifications-row',['notification'=>$notification['combo']])
</script>
<script id="not_all_day_Form_Row" type="calendar/template">
	<div id="is_not_all_day">
		{!!
		   widget('PersianDatePicker')
		   ->id('pdatepicker_not-time')
		   ->class('pdatepickerClass')
		   ->name('date')
		   ->inForm()
		   ->canNotScroll()
		   ->inRange()
		   ->withoutTime()
		   ->valueFrom($event->starts_at ?? $date ?? true)
		   ->valueTo($event->ends_at ?? $date ?? true)
		   ->placeholderFrom('tr:manage::forms.general.from')
		   ->placeholderTo('tr:manage::forms.general.till')
		!!}
	</div>
</script>

<script id="all_day_Form_Row" type="calendar/template">
	<div id="is_all_day">
		{!!
		   widget('PersianDatePicker')
		   ->id('pdatepickerTest')
		   ->class('pdatepickerClass')
		   ->name('date')
		   ->inForm()
		   ->canNotScroll()
		   ->inRange()
		   ->valueFrom($event->starts_at ?? $date ?? true)
		   ->valueTo($event->ends_at ?? $date ?? true)
		   ->placeholderFrom('tr:manage::forms.general.from')
		   ->placeholderTo('tr:manage::forms.general.till')
		!!}
	</div>
</script>
<script>

    $(document).ready(function () {

        $('#all_day').click(function () {


            let alldayCheck   = $(this);
            let notifications = $(this).parents('.modal-body').find('#notification-rows-body');

            let notifications_act = $(this).parents('.modal-body').find('.active-notif');
            if (notifications_act) {
                notifications_act.html('');
            }

            notifications.find('.notification-form-row').each(function () {
                let el_allDay = $(this).find('.all-time-combo select.time_type_combo');
                let el        = $(this).find('.time-combo select.time_type_combo');
                if (alldayCheck.is(':checked')) {
                    $('.time_combo').show();
                    $(this).find('.time-combo').hide();
                    $(this).find('.all-time-combo').show();
                    let l_name = el_allDay.attr('name');
                    let name   = l_name.substring(1);
                    el_allDay.attr('name', name);
                    el.attr('name', '_' + name);

                } else {

                    $(this).find('.time_combo').hide();
                    $(this).find('.time-combo').show();
                    $(this).find('.all-time-combo').hide();
                    let l_name = el.attr('name');
                    let name   = l_name.substring(1);
                    el.attr('name', name);
                    el_allDay.attr('name', '_' + name);
                }
            });

        });

        //initial rendering
        autoloadInitial();


        //Add the PersianDatePicker when the medal opens
        allDay();


    });
</script>
