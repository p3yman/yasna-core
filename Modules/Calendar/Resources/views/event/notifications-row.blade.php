@php
	$id_str = $id ?? '{{id}}';
	$notif_type = [
		    	[
		    		"id" => "0" ,
		    		"title" => trans('calendar::general.notifications.email') ,
		    	],
		    	[
		    		"id" => "1" ,
		    		"title" => trans('calendar::general.notifications.google_notification') ,
		    	],
		    ];

	$time_unit_list = [
		[
			"id" => "0" ,
			"title" => trans('calendar::general.time_units.minutes') ,
		],
		[
			"id" => "1" ,
			"title" => trans('calendar::general.time_units.hours') ,
		],
		[
			"id" => "2" ,
			"title" => trans('calendar::general.time_units.day') ,
		],
		[
			"id" => "3" ,
			"title" => trans('calendar::general.time_units.week') ,
		],
	];
	$time_unit_list_all = [
	 	[
			"id" => "2" ,
			"title" => trans('calendar::general.time_units.day') ,
		],
		[
			"id" => "3" ,
			"title" => trans('calendar::general.time_units.week') ,
		],
	];
@endphp
<div class="notification-form-row panel" id="notification_row_{{ $id_str }}">
	<div class="panel-heading">
		{{trans('calendar::general.notifications.notification')}}


		<div class="pull-right">
				<span style="cursor: pointer"
					  data-toggle="tooltip"
					  class="js_removeNotificationRow"
					  data-id="{{ $id_str }}"
					  title="{{ trans('calendar::general.notifications.remove') }}">
					  <i class="fa fa-trash text-red"></i>
				</span>
		</div>
	</div>

	<div class="panel-body">
		<div class="row mb-lg">
			<div class="col-md-10">
				{!!
					widget('combo')
					->label('tr:calendar::general.notifications.type')
					->inForm()
					->options($notif_type)
					->name('notifications[new][' . $id_str . '][type]')
				 !!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-sm-10">
						{!!
							widget('input')
							->type('number')
							->value(1)
							->extra_array([
							'min'=>'1',
							])
							->label(' ')
							->inForm()
							->name('notifications[new][' . $id_str . '][number_time]')
						 !!}
					</div>
					<div class="col-sm-2 time-combo">
						{!!
							widget('combo')
							->inForm()
							->label(' ')
							->labelClass('noDisplay')
							->options($time_unit_list)
							->class('time_type_combo')
							->valueField('id')
							->labelField('title')
							->name('notifications[new][' . $id_str . '][time_type]')
						 !!}
					</div>
					<div class="col-sm-2 all-time-combo" style="display: none;">
						{!!
							widget('combo')
							->inForm()
							->label(' ')
							->labelClass('noDisplay')
							->options($time_unit_list_all)
							->valueField('id')
							->labelField('title')
							->class('time_type_combo')
							->name('_notifications[new][' . $id_str . '][time_type]')
						 !!}
					</div>

				</div>
			</div>
		</div>
		<div class="row time_combo">
			<div class="col-md-10">
				{!!
				   widget('combo')
				   ->name('time')
				   ->id('notify_date_'. $id_str)
				   ->label('tr:calendar::general.notifications.before_at')
				   ->options($notification['time'])
				   ->inForm()
				   ->name('notifications[new][' . $id_str . '][time]')
				!!}
			</div>
		</div>
	</div>
</div>
