<div class="calendar-list">
	@if(!$events->isEmpty())
		@foreach($events as $event)
			<div class="row-calendar">
				<div class="event-title mv">
					{{$event->title}}
				</div>
				<div>
					@if(user()->as('admin'))
						<div class="dropdown">
							<button class="btn-action dropdown-toggle" type="button" data-toggle="dropdown">
								<i class="fas fa-ellipsis-v"></i>
							</button>
							<ul class="dropdown-menu animated flipInX">
								<li>
									<a href="javascript:void(0)"
									   onclick="{{'masterModal("'. route('singleEvent',$event->id) .'")'}}"
									   style="font-size: 12px">
										<i class="fa fa-eye fa-fw"></i>
										{{trans('calendar::general.forms.button.show')}}
									</a>
								</li>
								@if(calendar()->canEditEvent())
									<li>
										<a href="javascript:void(0)"
										   onclick="{{'masterModal("'. route('editEvent',$event->id) .'")'}}"
										   style="font-size: 12px">
											<i class="fa fa-pencil fa-fw"></i>
											{{trans('calendar::general.forms.button.edit')}}
										</a>
									</li>
								@endif
								@if(calendar()->canDeleteEvent())
									<li>
										<a href="javascript:void(0)"
										   onclick="{{'masterModal("'. route('eventDeleteAlert',$event->id) .'")'}}"
										   style="font-size: 12px">
											<i class="fa fa-trash-o fa-fw"></i>
											{{trans('calendar::general.delete')}}
										</a>
									</li>
								@endif
							</ul>
						</div>
					@endif
				</div>
			</div>
		@endforeach
	@else
		<small class="ph-lg">
			{{ trans('calendar::general.event.no-event') }}
		</small>
	@endif
</div>

