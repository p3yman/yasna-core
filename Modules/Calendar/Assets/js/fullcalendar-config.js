/**
 * New Full Calendar (Jalali) Config
 * -------------------------
 * Created Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2018-04-29
 */
function fullCalendar(data) {
    (data.calendar_type == '0') ? data.calendar_type = 'fa' : data.calendar_type = 'en';
    $('#yasnaCalendar').fullCalendar({

        customButtons: {
            setting: {
                text : data.calendar_type === 'en' ? 'setting' : 'تنظیمات',
                click: function () {
                    masterModal(url("manage/calendar/setting"), "lg");
                }
            }
        },

        header          : {
            left  : 'next,prev today setting',
            prev  : 'circle-triangle-w',
            center: 'title',
            right : 'month,agendaWeek,agendaDay,listMonth'
        },
        isJalaali       : true,
        defaultDate     : data.defaultDate,
        locale          : data.calendar_type,
        buttonIcons     : false, // show the prev/next text
        weekNumbers     : false,
        navLinks        : true, // can click day/week names to navigate views
        editable        : true,
        dayPopoverFormat: "L",
        titleFormat     : data.calendar_type === 'fa' ? 'jYYYY jMMMM' : 'YYYY MMM',
        views           : {
            month: {
                timeFormat: "HH:mm"
            }
        },
        dayRender       : function (date, cell) {
            var enDate        = moment(date._d).format('MMM D');
            cell[0].innerHTML = '<span class="secondDate">' + enDate + '</span>';

        },
        businessHours   : {

            // days of week. an array of zero-based day of week integers (0=Sunday)
            dow: data.business_days, // Saturday - Wednesday

            start: data.start_day, // a start time (10am in this example)
            end  : data.end_day // an end time (6pm in this example)
        },
        eventLimit      : true, // allow "more" link when too many events
        events          : function (start, end, timezone, callback) {
            $('#yasnaCalendar-panel').addClass('whirl standard');
            $.ajax({
                url     : url('manage/calendar/event/get'),
                dataType: 'json',
                data    : {
                    start: start.unix(),
                    end  : end.unix()
                },
                success : function (res) {
                    var events = [];
                    $(res).each(function (key, event) {
                        events.push({
                            id             : event.id,
                            title          : event.title,
                            start          : event.starts_at,
                            end            : event.ends_at,
                            backgroundColor: event.color,
                            borderColor    : event.color
                        });
                    });
                    callback(events);

                    $('#yasnaCalendar-panel').removeClass('whirl standard');
                }
            });
        },
        eventClick      : function (calEvent, jsEvent, view) {
            masterModal(url("manage/calendar/event/single/" + calEvent.id), "lg");
        },
        dayClick        : function (date) {
            masterModal(url("manage/calendar/event/create/" + ed(moment(date).format())), "lg");
        },
        eventDrop       : function (event, dayDelta) {
            $('#calendar-loading-icon').removeClass('hidden');
            $.ajax({
                url    : url('manage/calendar/event/move/' + event.id),
                method : 'get',
                data   : {
                    'all_day'  : event._allDay,
                    'day_delta': dayDelta._data
                },
                success: function () {
                    $('#calendar-loading-icon').addClass('hidden');
                    divReload('event-lists')
                },
                error  : function () {
                }
            });


        },
        firstDay        : data.first_day

    });

    $('#yasnaCalendar').fullCalendar('option', 'locale', data.calendar_type);
    $('#yasnaCalendar').fullCalendar('option', 'isJalaali', (data.calendar_type === 'fa'));
}


/**
 * Adds new events to calendar
 *
 * @param event
 */
function addNewEvent(event) {
    $('#yasnaCalendar').fullCalendar('renderEvent', {
        id             : event.id,
        title          : event.title,
        start          : event.starts_at,
        end            : event.ends_at,
        allDay         : !!event.all_day,
        backgroundColor: event.calendar.color,
        borderColor    : event.calendar.color

    });
}


/**
 * Removes requested event from calendar
 *
 * @param id
 */
function removeEvent(id) {
    $('#yasnaCalendar').fullCalendar('removeEvents', id)
}


jQuery(function ($) {

    // Change calendar view on resize
    $(window).on('resize', checkWindowWidth);

    // Change calendar view on load
    checkWindowWidth();


    /**
     * Change calendar view based on window size
     */
    function checkWindowWidth() {
        let winWidth = $(window).outerWidth(true);
        let calendar = $('#yasnaCalendar');

        if (winWidth <= 500) {
            calendar.fullCalendar('changeView', 'listMonth');
            calendar.find('.fc-toolbar .fc-right').hide();
        } else {
            calendar.find('.fc-toolbar .fc-right').show();
        }
    }
}); //End Of siaf!
