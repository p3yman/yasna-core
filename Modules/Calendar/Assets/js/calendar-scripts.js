/**
 * Calndar Scripts
 * -------------------------
 * Created by Negar Jamalifard
 * n.jamalifard@gmail.com
 * On 2019-01-15
 */

jQuery(function ($) {
    let notify_id = 1;
    $(document).on('click', '#addNotificationRow', function () {
        let template = $.trim($('#notificationFormRow').html());
        let el       = template.replace(/{{id}}/ig, notify_id);
        notify_id++;
        $(el).appendTo('#notification-rows-body');
        autoloadInitial();
    });


    $(document).on('click', '.js_removeNotificationRow', function () {
        let rowId = $(this).attr('data-id');
        $('#notification_row_' + rowId).remove();
    })

    $(document).on('click', '#all_day', function () {
        let template_un_check = $.trim($('#not_all_day_Form_Row').html());
        let template_check    = $.trim($('#all_day_Form_Row').html());
        if ($('#all_day').is(':checked')) {
            $(template_check).appendTo('#is_all_day_body');
            $('#is_not_all_day').remove();
        } else {
            $(template_un_check).appendTo('#is_not_all_day_body');
            $('#is_all_day').remove();
        }
    });


}); //End Of siaf!

/**
 * delete warning after set setting
 *
 */
function deleteWarning() {
    $('.calendar-warning').remove();
}


/**
 * initial rendering
 *
 */
function autoloadInitial() {
    let notifications = $('.modal-body').find('#notification-rows-body');

    notifications.find('.notification-form-row').each(function () {
        let el_allDay = $(this).find('.all-time-combo select.time_type_combo');
        let el        = $(this).find('.time-combo select.time_type_combo');
        if ($('#all_day').is(':checked')) {
            $('.time_combo').show();
            $('.time-combo').hide();
            $('.all-time-combo').show();
            let l_name    = el_allDay.attr('name');
            var firstChar = l_name.substr(0, 1);
            if (firstChar == '_') {
                let name = l_name.substring(1);
                el_allDay.attr('name', name);
            }
            el.attr('name', '_' + name);

        } else {
            $('.time_combo').hide();
            $('.time-combo').show();
            $('.all-time-combo').hide();
            let l_name     = el.attr('name');
            var first_char = l_name.substr(0, 1);
            if (first_char == '_') {
                let name = l_name.substring(1);
                el.attr('name', name);
            }
            el_allDay.attr('name', '_' + name);
        }
    });


}


/**
 * Add the PersianDatePicker when the medal opens
 *
 */
function allDay() {
    if ($('#all_day').is(':checked')) {
        $('#is_not_all_day').remove();
        let template_check = $.trim($('#all_day_Form_Row').html());
        $(template_check).appendTo('#is_all_day_body');
    } else {
        $('#is_all_day').remove();
        let template_check = $.trim($('#not_all_day_Form_Row').html());
        $(template_check).appendTo('#is_not_all_day_body');
    }
}
