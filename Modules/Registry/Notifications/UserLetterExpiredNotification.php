<?php

namespace Modules\Registry\Notifications;

use App\Models\User;
use Modules\Notifier\Notifications\YasnaNotificationAbstract;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserLetterExpiredNotification extends YasnaNotificationAbstract // implements ShouldQueue
{
    /**
     * keep the instance of the user with expired letter
     *
     * @var User
     */
    protected $user;



    /**
     * UserLetterExpiredNotification constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }



    /**
     * get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [
             'database',
        ];
    }



    /**
     * get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toDatabase($notifiable)
    {
        $message = trans("registry::notifications.seconder_letter_expiry_message" , [
             "name" => $this->user->full_name,
        ]);

        return notifier()
             ->database()
             ->dataProvider()
             ->group("warnings")
             ->link("Link") //@TODO: Think of a link pattern
             ->title($message)
             ->content("")
             ->toArray()
             ;
    }
}
