<?php
return [
     "master"               => "صاحب امتیاز",
     "university_president" => "رئیس دانشگاه",
     "university_deputy"    => "معاون درمان دانشگاه",

     'chief-seconders'                  => "سرتیم تأییدکنندگان",
     "chief-neurologist-seconders"      => "سرگروه تأییدکنندگان متخصص نورولوژی",
     'chief-neurosurgeons-seconders'    => "سرگروه تأییدکنندگان متخصص جراحی اعصاب",
     'chief-internist-seconders'        => "سرگروه تأییدکنندگان متخصص داخلی",
     'chief-anesthesiologist-seconders' => "سرگروه تأییدکنندگان متخصص بی‌هوشی",
     'neurologist-seconders'            => "تأییدکننده متخصص نورولوژی",
     'neurosurgeons-seconders'          => "تأییدکننده متخصص جراحی اعصاب",
     'internist-seconders'              => "تأییدکننده متخصص داخلی",
     'anesthesiologist-seconders'       => "تأییدکننده متخصص بی‌هوشی",

     'chief-coordinator'  => "سرتیم هماهنگ‌کننده اهدای عضو",
     'coordinator'        => "هماهنگ‌کننده اهدای عضو",
     'harvest'            => "برداشت‌کننده ارگان",
     'harvester'          => 'جراحان برداشت‌کننده',
     'consultant'         => "پزشک مشاور",
     'transporter'        => "انتقال‌دهنده ارگان",
     'legal-medicine'     => "نماینده سازمان پزشکی قانونی",
     'opu-manager'        => "رئیس واحد",
     'inspector-tel'      => "غربالگر تلفنی",
     'inspector-clinical' => "غربالگر حضوری",
     'expert-in-charge'   => "رئیس اداره بیماری",
     'donor-expert'       => "کارشناس فراهم‌آوری و پیوند اعضا",

     'protector'       => 'مراقبت‌کننده',
     'chief-protector' => 'مسئول تیم مراقبت',

     'university-supervisor' => 'ناظر دانشگاه',
     'opu-supervisor'        => 'ناظر واحد',
];
