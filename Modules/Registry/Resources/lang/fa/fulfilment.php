<?php
return [
     "super_register_one_university" => "ثبت حداقل یک دانشگاه",
     "super_register_one_expert"     => "ثبت یک رئیس اداره بیماری یا کارشناس فراهم‌آوری و پیوند اعضا",

     "expert_university_settings"           => "انجام تنظیمات دانشگاه",
     "expert_university_president"          => "ثبت اطلاعات رئیس دانشگاه",
     "expert_university_deputy"             => "ثبت اطلاعات معاون درمان دانشگاه",
     "expert_subs"                          => "ثبت کارشناس فراهم‌آوری و پیوند اعضا",
     "expert_one_neurologist_seconder"      => "ثبت حداقل یک تأییدکننده متخصص نورولوژی",
     "expert_one_neurosurgeons_seconder"    => "ثبت حداقل یک تأییدکننده متخصص جراحی اعصاب",
     "expert_one_internist_seconder"        => "ثبت حداقل یک تأییدکننده متخصص داخلی",
     "expert_one_anesthesiologist_seconder" => "ثبت حداقل یک تأییدکننده متخصص بی‌هوشی",
     "expert_one_opu"                       => "ثبت اطلاعات حداقل یک واحد فراهم‌آوری یا شناسایی",
     "expert_one_opu_manager"               => "ثبت اطلاعات یک مسئول واحد",
     "expert_one_hospital"                  => "ثبت اطلاعات حداقل یک بیمارستان",
     "expert_one_ward"                      => "ثبت اطلاعات بخش‌های یک بیمارستان",

     "opu_settings"                 => "انجام تنظیمات واحد",
     "opu_one_consultant"           => "ثبت حداقل یک پزشک مشاور",
     "opu_one_legal_medicine"       => "ثبت حداقل یک نماینده سازمان پزشکی قانونی",
     "opu_one_chief_legal_medicine" => "ثبت حداقل یک مدیر کل سازمان پزشکی قانونی",
     "opu_one_coordinator"          => "ثبت حداقل یک کوردیناتور",
     "opu_one_chief_coordinator"    => "ثبت حداقل یک چیف کوردیناتور",
     "opu_one_inspector"            => "ثبت حداقل یک غربالگر",
];
