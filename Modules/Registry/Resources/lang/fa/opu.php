<?php
return [
     "hospital-hosts-another-opu" => "این بیمارستان، محل استقرار یک واحد فناوری دیگر است.",
     "organ_harvesting_required"  => "برای واحدهای فراهم‌آوری باید امکان برداشت ارگان را مشخص کنید.",
];
