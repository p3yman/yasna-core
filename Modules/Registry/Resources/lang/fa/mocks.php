<?php

return [
     'name'                => 'پارسا',
     'family'              => 'میهن دوست',
     'mobile'              => '09377410886',
     'tel'                 => '2225487',
     'birth_date'          => '2018-11-21',
     'email'               => 'mihandoost.p@gmail.com',
     'gender'              => '0',
     'edu_field'           => 'PHD',
     'expertise'           => '',
     'job_history'         => 'مدیربخش',
     'latest_job_position' => 'رئیس بیمارستان',
     'service_location'    => 'رئیس',
     'is_retired'          => '1',
     'second_job'          => 'تاکسی',
     'code_melli'          => '0020518595',
];
