<?php
return [
     "user_not_found"  => "کاربر مورد نظر پیدا نشد.",
     "role_not_found"  => "نقش مورد نظر پیدا نشد.",
     "unity_not_found" => "واحد مورد نظر پیدا نشد.",

     "hospital_accepted"   => "شناسه‌ی بیمارستان معتبر نیست.",
     "university_accepted" => "شناسه‌ی دانشگاه معتبر نیست.",

     "universities_required" => "حداقل یک دانشگاه را انتخاب کنید.",
     "nationality_required"  => "کشور محل تابعیت درست انتخاب نشده است.",
     "blood_type_invalid"    => "گروه خونی واردشده مورد پذیرش نیست",

     "code_melli_duplicate" => "کد ملی تکراری است.",

     "required_transfer_case"                 => "ورود شناسه‌ی بیمار الزامی است.",
     "invalid_transfer_case"                  => "شناسه‌ی بیمار معتبر نیست.",
     "has_open_transfer_case"                 => "بیمار دارای درخواست انتقال در انتظار بررسی است و نمی‌توان برای او درخواست جدید ثبت کرد.",
     "required_transfer_city"                 => "ورود شناسه‌ی شهر مقصد الزامی است.",
     "invalid_transfer_city"                  => "شناسه‌ی شهر انتخابی معتبر نیست.",
     "required_transfer_destination_hospital" => "ورود شناسه‌ی بیمارستان مقصد الزامی است.",
     "invalid_transfer_destination_hospital"  => "شناسه‌ی بیمارستان مقصد معتبر نیست.",


     "inconsistent_with_error_message" => "تنها یکی از پارامترهای «:field1» و «:field2» می‌توانند به صورت هم‌زمان دارای مقدار باشند.",
];
