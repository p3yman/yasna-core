<?php

return [

     'consciousness-disorder' => [
          "accident"         => "تصادف",
          "fall"             => "سقوط از ارتفاع",
          "fight"            => "نزاع",
          "bullet-hit"       => "اصابت گلوله",
          "strokes"          => "سکته‌های مغزی",
          "aneurysm-rupture" => "پارگی آنوریسم‌های مغزی",
          "chocking"         => "خفگی",
          "hanging"          => "دارآویختگی",
          "post-cpr"         => "پس از احیای قلبی ریوی",
          "drug-poisoning"   => "مسمومیت دارویی",
          "post-convulsion"  => "پس از تشنج",
          "burn"             => "سوختگی",
          "brain-tumors"     => "تومورهای مغزی",
          "others"           => "سایر",
     ],

     "identification-type" => [
          "clinical"           => "حضوری",
          "over-tel"           => "تلفنی",
          "hospital-reporting" => "گزارش بیمارستانی",
     ],

     "bacteriology-type" => [
          "u-a"           => "U/A",
          "u-c"           => "U/C",
          "b-c"           => "B/C",
          "BAL-culture"   => "BAL Culture",
          "wound-culture" => "Wound Culture",
          "pro-calcition" => "Pro-calcition",
     ],

     "bacteriology-type-positive" => [
          "estafilocock" => "استافیلوکوک",
          "pesudomona"   => "پسودومونا",
          "calbesila"    => "کلبسیلا",
          "penomocock"   => "پنوموکوک",
          "masgocock"    => "مسگوکوک",
          "ecoli"        => "E Coli",
          "other"        => "سایر",
     ],

     'clinical-situation' => [
          "gcs3-died"  => "جی‌سی‌اس ۳ مرگ مغزی شده",
          "gcs3-alive" => "جی‌سی‌اس ۳ مرگ مغزی نشده",
          "gcs4and5"   => "جی‌سی‌اس ۴ و ۵",
          "unfeasible" => "نامناسب برای اهدا",
     ],

     'case-situation' => [
          "new"           => "بیمار جدید",
          "follow-up"     => "در حال پی‌گیری",
          "bd-confirmed"  => "تشخیص و تأیید مرگ مغزی",
          "under-consent" => "رضایت‌گیری",
          "move-to-opu"   => "انتقال به واحد فراهم‌آوری",
          "organ-checks"  => "بررسی ارگان‌ها",
          "surgery-ready" => "آمادگی اتاق عمل",
          "healed"        => "ارتقای سطح هوشیاری",
          "donated"       => "اهداشده",
          "unfeasible"    => "غیر قابل اهدا",

          "died-before-consent" => "مرگ قلبی قبل از اقدام به رضایت‌گیری",
          "died-no-consent"     => "مرگ قلبی به علت عدم رضایت",
          "died-after-consent"  => "مرگ قلبی پس از اخذ رضایت",
          "died"                => "فوت شده",
     ],

     'unfeasible-reason' => [
          "brain-tumors"                     => "تومورهای مغزی",
          "unusable-other-tumors"            => "تومورهای مغزی غیر قابل استفاده",
          "uncontrolled-systemic-infections" => "عفونت‌های کنترل‌نشده سیستمیک",
          "local-acute-infections"           => "عفونت‌های فعال موضعی مانند انسفالیت، عفونت‌های مقاوم",
          "hiv"                              => "HIV",
          "creutzfeldt-jakob"                => "Creutzfeldt Jakob",
          "unknown-brain-death-reasons"      => "مشخص نبودن علت مرگ مغزی",
          "unknown-identity"                 => "مشخص نبودن هویت",
          "legal-prohibitions"               => "ممنوعیت‌های قانونی",
          "foreigners"                       => "اتباع بیگانه",
          "old-age"                          => "سن بالا",
          "others"                           => "سایر",
     ],

     'inner-brain-tumor' => [
          "astrocytoma"             => "Astrocytoma",
          "medulloblastoma"         => "Medulloblastoma",
          "glioblastoma-multiforme" => "Glioblastoma Multiforme",
          "neuroblastoma"           => "Neuroblastoma",
          "meningioma"              => "Meningioma",
          "malignant-meningioma"    => "Malignant Meningioma",
          "benign-angioblastoma"    => "Benign Angioblastoma",
          "unknown"                 => "Unknown",
     ],

     'outer-brain-tumor' => [
          "kidney"               => "کلیه",
          "breast"               => "پستان",
          "thyroid"              => "تیروئید",
          "tongue-throat-larynx" => "زبان، گلو و حنجره",
          "lung"                 => "ریه",
          "leukemia-lymphoma"    => "لوسمی و غدد لنفاوی",
          "liver"                => "کبد",
          "unknown"              => "ناشناس",
     ],

     'skin-tumor' => [
          "squamous-cell" => "سلول سنگفرشی",
          "basal-cell"    => "سلول پایه",
          "melanoma"      => "ملانوم",
          "unknown"       => "ناشناس",
     ],

     'known-heart-disease' => [
          "vascular-stenosis" => "تنگی عروق",
          "valves"            => "دریچه‌ای",
          "congenital"        => "مادرزادی",
          "others"            => "دیگر",
     ],

     "transmission-reason" => [
          "family-satisfaction"                            => "رضایت شخصی خانواده",
          "lack-of-hospital-required-treatment-facilities" => "عدم وجود امکانات درمانی سرویس بستری مورد نیاز",
          "lack-of-expert"                                 => "عدم وجود متخصص",
          "lack-of-hospitalization-space"                  => "عدم وجود فضای بستری",
          "transmission-to-opu"                            => "انتقال به واحد فراهم آوری",
     ],

     "not-transmission-reason" => [
          "lack-capacity-on-destination-hospital"      => "عدم وجود ظرفیت در بیمارستان مقصد",
          "rejected-by-opu"                            => "عدم پذیرش توسط واحد فراهم‌آوری",
          "patient-die-before-transfer"                => "فوت بیمار قبل از انتقال",
          "non-suitable-conditions-among-transferring" => "ایجاد وضعیت بالینی نامناسب حین انتقال",
          "lack-of-transportation"                     => "عدم وجود امکانات ترابری",
          "bad-weather-conditions"                     => "شرایط بد جوی",
     ],

     "next-operations" => [
          "report-to-moien-opu"                    => "گزارش به واحد فراهم‌آوری معین",
          "report-to-transplant-management-center" => "گزارش به مرکز مدیریت پیوند",
          "report-to-diseases-organ-manager"       => "گزارش به رئیس اداره‌ی بیماری‌ها",
     ],

     "final-state" => [
          "transform-to-an-equipped-hospital"          => "انتقال به بیمارستان مجهز",
          "death"                                      => "فوت",
          "move-to-opu-center"                         => "انتقال به واحد فراهم‌آوری",
          "continuing-admission-to-the-emergency-room" => "ادامه‌ی بستری در اورژانس",
     ],

     "transfusion-type" => [
          "whole-blood"     => "Whole Blood",
          "ffp"             => "FFP",
          "cryopercipitate" => "Cryopercipitate",
          "packed-cell"     => "Packed Cell",
          "platelet"        => "Platelet",
     ],

     'surgical-history' => [
          "chest"   => "قفسه صدری",
          "abdomen" => "شکم",
          "brain"   => "مغز",
          "limbs"   => "اندام‌ها",
          "others"  => "سایر",
     ],

     'use-course' => [
          "daily"        => "روزانه",
          "weekly"       => "هفتگی",
          "monthly"      => "ماهانه",
          "occasionally" => "گهگاهی",
     ],

     'antibiotic'       => [
          "ciprofloxacin" => "سیپروفلوکساسین",
          "meropenem"     => "مروپنم",
          "imipenem"      => "ایمی‌پنم",
          "vancomycin"    => "وانکومایسین",
          "taozovin"      => "تازوسین",
          "ceftriaxone"   => "سفتریاکسون",
          "ceftazidime"   => "سفتازیدیم",
          "others"        => "سایر",
     ],

     // Blood Pressure Medicines
     'Antihypertensive' => [
          "labetalol"     => "لابتولول",
          "esmolol"       => "اسمولول",
          "nitroglycerin" => "نیتروگلیسیرین",
          "hydralazine"   => "هیدرالازین",
          "others"        => "سایر",
     ],

     'organ-engagement'     => [
          "blood"                            => "خون",
          "urine"                            => "ادرار",
          "feces"                            => "مدفوع",
          "csf"                              => "مایع مغزی نخاعی",
          "bronchoalveolar-lavage-secretion" => "ترشح برونکوآلوئولار لاواژ",
          "liver"                            => "کبد",
          "kidney"                           => "کلیه",
          "skin"                             => "پوست",
          "lung"                             => "ریه",
          "heart"                            => "قلب",
          "intestine"                        => "روده",
          "others"                           => "سایر",
     ],

     // A: نوع کشت مثبت
     'positive-cultivation' => [
          "pseudomonas-aeruginosa"            => "سودوموناس آئروژینوزا",
          "staphylococcus-aureus"             => "استافیلوکوک اورئوس",
          "klebsiella"                        => "کلبسیلا",
          "acinetobacter"                     => "آسینتوباکتر",
          "citrobacter"                       => "سیتروباکتر",
          "enterococci"                       => "انتروکوک",
          "negative-coagulase-staphylococcus" => "استافیلوکوک کوآگولاز منفی",
          "candidate"                         => "کاندیدا",
          "aspergillus"                       => "آسپرژیلوس",
          "others"                            => "سایر",
     ],

     'insulin-type' => [
          "regular" => "رگولار",
          "nph"     => "NPH",
     ],

     'treatment-reply' => [
          "reduce_fever"        => "کاهش تب",
          "reduce_leukocytosis" => "کاهش لکوسیتوز",
          "others"              => "سایر",
     ],

     "opu_bed_type" => [
          "dedicated" => "دارای تخت اختصاصی",
          "marked"    => "دارای تخت نشان‌دار",
          "guest"     => "استفاده از تخت سایر بخش‌ها",
     ],

     'medical_degree' => [
          "general"        => "پزشک عمومی",
          "specialist"     => "پزشک متخصص",
          "sub-specialist" => "پزشک فوق تخصص",
     ],

     'heart-prohibit-desc' => [
          "age-over-60"                    => "سن بالای شصت سال",
          "age-over-45-with-blood-tension" => "سن بالای ۴۵ سال با سابقه ده سال یا بیشتر فشار خون",
          "age-over-45-with-diabetes"      => "سن بالای ۴۵ سال با سابقه ده سال یا بیشتر دیابت تیپ یک یا دو",
          "cabg"                           => "سابقه CABG",
          "mi"                             => "سابقه MI",
          "stent"                          => "سابقه استنت عروق کرونر",
          "cardiomyopathy"                 => "نارسایی قلبی ناشی از کاردیومیوپاتی",
          "defibrillator"                  => "داشتن دفیبریلاتور یا پیس میکر",
          "heart-valve-repair"             => "سابقه ترمیم دریچه",
          "myocarditis"                    => "میوکاردیت حاد",
          "endocarditis"                   => "اندکاردیت حاد",
     ],

     'heart-prohibit-electrocardiogram' => [
          "mi"                    => "MI فعلی",
          "cardiomyopathy"        => "نارسایی قلبی ناشی از کاردیومیوپاتی",
          "one-heart-valve-clash" => "درگیری متوسط تا شدید یک دریچه قلب",
          "two-heart-valve-clash" => "درگیری دو دریچه",
          "hypocyncy"             => "هیپوکینزی گلوبال قلب",
          "myxoma"                => "میگزوما",
          "congenital"            => "نقص مادرزادی قلب درمان شده یا نشده با جراحی",
     ],

     'heart-prohibit-angiography' => [
          "stenosis" => "تنگی شدید عروق کرونر",
     ],

     'heart-valvular-problem' => [
          "ai" => "AI",
          "as" => "AS",
          "mr" => "MR",
          "ms" => "MS",
          "pi" => "PI",
          "ps" => "PS",
          "tr" => "TR",
          "ts" => "TS",
     ],

     'severity' => [
          "mild"     => "خفیف",
          "moderate" => "متوسط",
          "severe"   => "شدید",
     ],

     'organ-size' => [
          "nl"       => "NL",
          "enlarged" => "Enlarged",
     ],

     'organ-function' => [
          "nl"     => "NL",
          "mild"   => "Mild Dysfunction",
          "mod"    => "Mod Dysfunction",
          "severe" => "Severe Dysfunction",
     ],

     'organ-normality' => [
          "have-not"      => "ندارد",
          "have-normal"   => "دارد، طبیعی است.",
          "have-abnormal" => "دارد، طبیعی نیست.",
     ],

     'wall-motion' => [
          "no"        => "No",
          "segmental" => "Segmental",
          "global"    => "Global",
     ],

     'lung-prohibit-desc' => [
         //"age-over-60"  => "سن بالای شصت سال",
         "copd"         => "سابقه بیماری‌های انسدادی ریه COPD",
         "asthma"       => "سابقه آسم نیازمند درمان روزانه",
         "asthma-death" => "آسم به عنوان علت مرگ",
         "fibrosis"     => "سابقه فیبروز ریوی",
         //"lobectomy"    => "سابقه جراحی لوبکتومی ریه",
         "hypertension" => "قشار خون بالای ریه",
         "chronic"      => "سابقه بیماری‌های عفونی مزمن ریوی (برونشکتازی یا سل)",
     ],

     'lung-prohibit-radiology' => [
          "pneumonia" => "پنومونی تأییدشده در عکس ساده ریه یا برونکوسکوپی یا کشت یا سی‌تی‌اسکن ریه",
          "cantigene" => "کانتیوژن شدید ریوی تأییدشده در سی‌تی‌اسکن",
          "cat"       => "بول‌های متعدد تأییدشده در Computed Axial Scan",
     ],

     'lung-prohibit-o2challenge' => [
          "less-than-300" => "میزان نهایی fiO2/PaO2 کمتر از ۳۰۰ میلی لیتر",
     ],

     'lung-prohibit-bronchoscopy' => [
          "pneumonia" => "پنومونی تأیید شده در عکس ساده ریه یا سی‌تی‌اسکن ریه",
     ],

     'lung-ventilator-mode' => [
          "ac"     => "A/C",
          "cmv"    => "CMV",
          "simv"   => "SIMV",
          "prvc"   => "PRVC",
          "aprv"   => "APRV",
          "hfov"   => "HFOV",
          "others" => "Others",
     ],

     'lung-abnormality' => [
          "suppuration" => "ترشح چرکی",
          "aspiration"  => "آسپیراسیون مواد خارجی",
          "blood"       => "خون",
          "anatomy"     => "آناتومی غیر طبیعی",
          "others"      => "سایر",
     ],

     'o2challenge-cause' => [
          "unfit-lung"        => "ریه نامناسب است.",
          "no-receiver"       => "گیرنده ندارد.",
          "lack-of-personnel" => "امکان جراحی پیوند به لحاظ پرسنلی فراهم نیست.",
          "lack-of-equipment" => "امکان جراحی پیوند به لحاظ تجهیزات فراهم نیست.",
     ],

     'lung-pneumothorax' => [
          "trauma"       => "به دنبال تروما",
          "manipulation" => "به دنبال مانیپولاسیون",
          "auto"         => "خود به خود",
     ],

     'body-side' => [
          "left"  => "چپ",
          "right" => "راست",
          "both"  => "هر دو طرف",
     ],

     'lung-side' => [
          "left"  => "ریه چپ",
          "right" => "ریه راست",
          "both"  => "هر دو ریه",
     ],

     'lung-ctscan-cause' => [
          "infiltration" => "بررسی دقیق انفیلتراسیون ریوی",
          "tumor"        => "بررسی تومور مشکوک",
          "others"       => "سایر",
     ],

     'pulmonary-secretion-culture' => [
          "bacteria"      => "باکتری",
          "fungus"        => "قارچ",
          "mycobacterium" => "مایکوباکتریوم",
     ],

     'kidney-prohibit-desc' => [
          "age-over-70"               => "سن بالای هفتاد سال",
          "age-over-45-with-diabetes" => "سن بین ۵۰ تا ۶۹ سال با سابقه بیست سال یا بیشتر دیابت نوع یک",
          "polycystic"                => "سابقه کلیه پلی کیستیک",
          "chronic"                   => "سابقه نارسایی مزمن کلیه",
          "no-urine"                  => "نداشتن خروجی ادرار (آنوری) به مدت ۲۴ ساعت یا بیشتر",
     ],

     'kidney-glomerulus' => [
          "0-5"      => "صفر تا ۵",
          "6-10"     => "۶ تا ۱۰",
          "11-15"    => "۱۱ تا ۱۵",
          "16-20"    => "۱۶ تا ۲۰",
          "above-20" => "بیشتر از ۲۰",
     ],

     'kidney-prohibit-tests' => [
          "creatinine" => "میزان نهایی کراتینین سرم بیش از ۴ mg/dL",
     ],

     'kidney-prohibit-biopsy' => [
          "glomerulosclerosis" => "گلومرولواسکلروز مساوی یا بیشتر از بیست درصد در کلیه",
     ],

     'kidney-size' => [
          "normal" => "طبیعی",
          "small"  => "کوچک‌تر از طبیعی",
          "large"  => "بزرگ‌تر از طبیعی",
     ],

     'kidney-abnormality' => [
          "stone"          => "سنگ",
          "hematoma"       => "هماتوم",
          "tumor"          => "تومور",
          "hydronephrosis" => "هیدرونفروز",
          "cyst"           => "کیست",
          "others"         => "سایر",
     ],

     'kidney-biopsy-type' => [
          "needle" => "سوزنی",
          "wedge"  => "وج",
          "others" => "سایر",
     ],

     'kidney-biopsy-severity' => [
          "have-not" => "ندارد",
          "minor"    => "جزئی",
          "mild"     => "خفیف",
          "moderate" => "متوسط",
          "severe"   => "شدید",
          "unknown"  => "نامشخص",
     ],

     'liver-prohibit-desc' => [
         //"age-over-70"               => "سن بالای هفتاد سال",
         //"age-over-45-with-diabetes" => "سن بین ۵۰ تا ۶۹ سال با سابقه بیست سال یا بیشتر دیابت نوع یک",
         "cirrhosis"               => "سابقه سیروز",
         "fulminant"               => "نارسایی کبد فولمینانت",
         "hypertension"            => "هایپرتانسیون پورت",
         "macro-staathose-over-60" => "ماکرواستئاتوز بیشتر یا مساوی شصت درصد",
         "fibrosis"                => "فیبروز درجه دو یا بالاتر",
     ],

     'liver-prohibit-tests' => [
          "serum" => "میزان نهایی بیلی روبین و سرم بیشتر از ۴ mg/dL",
          "ast"   => "میزان نهایی AST و ALT بالاتر از ۷۰۰ U/L",
     ],

     "liver-prohibit-sonography" => [
          "hypertension" => "هایپرتانسیون پورت",
          "cirrhosis"    => "سیروز",
     ],

     'liver-prohibit-observation' => [
          "macro"    => "ماکرواستناتوز بیشتر یا مساوی ۵۰ درصد",
          "fibrosis" => "فیبروز درجه دو یا بالاتر",
     ],

     'liver-size' => [
          "normal" => "طبیعی",
          "small"  => "کوچک‌تر از طبیعی",
          "large"  => "بزرگ‌تر از طبیعی",
     ],

     'liver-ecosystem' => [
          "normal" => "طبیعی",
          "hyper"  => "غیر طبیعی",
     ],

     'liver-mass' => [
          "hipo"  => "هیپو اکو",
          "hyper" => "هیپر اکو",
     ],

     'liver-mass-position' => [
          "left"   => "لوب چپ",
          "right"  => "لوب راست",
          "others" => "سایر",
     ],

     'liver-biopsy-type' => [
          "blind"  => "کور",
          "wedge"  => "وج",
          "others" => "سایر",
     ],

     'liver-biopsy-result' => [
          "fibrosis-a"           => "No Fibrosis",
          "fibrosis-b"           => "Fibrosis expansion of some portal areas, with or without short fibrous septa",
          "fibrosis-c"           => "Fibrosis expansion of most portal areas, with occasional portal to portal bridging",
          "fibrosis-d"           => "Fibrosis expansion of portal areas, with marked bridging (portal to portal as well as portal to central)",
          "fibrosis-e"           => "Marked bridging with occasional nodules (incomplete cirrhosis)",
          "fibrosis-f"           => "Cirrhosis, probable or definite",
          'portal-infiltrates_a' => 'None Noted',
          'portal-infiltrates_b' => 'Mild, some or all portal areas',
          'portal-infiltrates_c' => 'Moderate, some or all portal areas',
          'portal-infiltrates_d' => 'Moderate/Marked',
          'portal-infiltrates_e' => 'Marked, all portal areas',
     ],

     'intestine-prohibit-desc' => [
          'age-over-55'           => 'سن بالای ۵۵ سال',
          'weight-over-80'        => 'وزن بالای ۸۰ کیلوگرم',
          'bmi-over-30'           => 'بی‌ام‌آی بالای ۳۰',
          'suspicious-desc'       => 'شرح حال مشکوک از مشکلات گوارشی همچون تهوع و استفراغ مزمن و یا TPN',
          'heart-attack'          => 'ایست قلبی طولانی بیشتر از ۱۵ دقیقه',
          'high-inotropic-intake' => 'مصرف بیش از دو اینوتروپ در زمان کراس کلمپ آئورت',
          //'donor-recipient-wight' => 'بهتر است وزن اهداکننده پنجاه تا هشتاد درصد وزن گیرنده باشد.',
     ],

     'pancreas-prohibit-desc' => [
          "diabetes"      => "سابقه دیابت (تیپ ۱ یا تیپ ۲ یا دیابت حاملگی)",
          "surgery"       => "جراحی قبلی پانکراس یا ترومای متوسط تا شدید پانکراس",
          "pancreatitis"  => "پانکراتیت حاد یا مزمن",
          "contamination" => "آلودگی قابل توجه شکمی (Contamination)",
          "chronic"       => "مصرف مزمن الکل یا اعتیاد تزریقی یا روابط جنسی پرخطر",
          "fat"           => "چاقی شدید (بی‌ام‌آی بالاتر از ۳۰)",
     ],


     "hospital-sector" => [
          "bank"     => "بانکی",
          "private"  => "خصوصی",
          "charity"  => "خیریه‌ای",
          "academic" => "دانشگاهی",
          "martyrs"  => "شاهد",
          "oil"      => "شرکت نفت",
          "defence"  => "وزارت دفاع",
          "social"   => "تأمین اجتماعی",
          "others"   => "سایر",
     ],

     "university-type" => [
          "university" => "دانشگاه",
          "faculty"    => "دانشکده",
     ],

     'boolean' => [
          "yes"     => "بله",
          "no"      => "خیر",
          "unknown" => "نامشخص",
     ],

     'education' => [
          "age-under-6"       => "کمتر از شش سال",
          "illiterate"        => "بی‌سواد",
          "less-than-diploma" => "زیر دیپلم",
          "diploma"           => "دیپلم",
          "bachelor"          => "کارشناسی",
          "master"            => "کارشناسی‌ارشد",
          "phd-and-higher"    => "دکترا و بالاتر",
     ],

     'job-type' => [
          "student"   => "دانش‌آموز یا دانشجو است.",
          "full-time" => "کار تمام‌وقت دارد.",
          "part-time" => "کار نیمه‌وقت دارد.",
     ],

     'unemployment-reason' => [
          "does-not-work" => "کار ندارد.",
          "retired"       => "بازنشسته است.",
          "age-under-7"   => "سن کمتر از هفت سال دارد.",
     ],

     'reason-not-full-time' => [
          "own-choice"         => "به انتخاب خود",
          "treatment"          => "به علت درمان",
          "inability"          => "به علت ناتوانی",
          "not-find-full-time" => "کار تمام‌وقت پیدا نکرده است.",
     ],

     'cause-not-work' => [
          "own-choice"          => "به انتخاب خود",
          "family-choice"       => "به انتخاب خانواده",
          "treatment"           => "به علت درمان",
          "inability"           => "به علت ناتوانی",
          "not-find-proper-job" => "کار مناسب پیدا نکرده است.",
     ],

     'insurance-type' => [
          "social-security"   => "تامین اجتماعی",
          "health-service"    => "خدمات درمانی",
          "armed-forces"      => "نیروهای مسلح",
          "health"            => "سلامت",
          "relief-foundation" => "کمیته امداد",
          "supplementary"     => "تکمیلی",
          "rural"             => "روستایی",
     ],

     'source-of-funding' => [
          "itself"         => "خود فرد",
          "insurance"      => "بیمه",
          "charity-forums" => "انجمن‌های خیریه",
          "unknown"        => "نامشخص",
     ],

     'lung-disease-to-graft' => [
          "group-a"                        => "Group A: بیماری‌های انسدادی",
          "a-allergic-broncho"             => "Allergic Bronchopulmonary Aspergillosis",
          "a-alpha-1-antitry-defici"       => "Alpha-1 Antitrypsin Deficiency",
          "a-bronchiectasis"               => "Bronchiectasis",
          "a-broncho-dysplasia"            => "Bronchopulmonary Dysplasia",
          "a-chronic-pulmonary"            => "Chronic Obstructive Pulmonary Disease / Emphysema",
          "a-ehlers-danlos-syndrome"       => "Ehlers-Danlos Syndrome",
          "a-granulomatous-lung"           => "Granulomatous Lung Disease",
          "a-inhalation-burns-trauma"      => "Inhalation Burns / Trauma",
          "a-kartagener-syndrome"          => "Kartagener’s Syndrome",
          "a-lymphangiole"                 => "Lymphangioleiomyomatosis",
          "a-obstructive-lung"             => "Obstructive Lung Disease",
          "a-ciliary-dyskinesia"           => "Primary Ciliary Dyskinesia",
          "a-sarcoidosis-pulmonary"        => "Sarcoidosis with Mean Pulmonary Artery Pressure of 30 mm Hg or Less",
          "a-tuberous-sclerosis"           => "Tuberous Sclerosis",
          "a-wegener-granuloma"            => "Wegener’s Granuloma – Bronchiectasis",
          "group-b"                        => "Group B: بیماری‌های عروق ریه",
          "b-congenital-malformation"      => "Congenital Malformation",
          "b-crest-pulmonary-hyper"        => "CREST – Pulmonary Hypertension",
          "b-syndrome-asd"                 => "Eisenmenger’s Syndrome: Atrial Septal Defect (ASD)",
          "b-syndrome-multi-congenital"    => "Eisenmenger’s Syndrome: Multi-Congenital Anomalies",
          "b-syndrome-other-specify"       => "Eisenmenger’s Syndrome: Other Specify",
          "b-syndrome-pda"                 => "Eisenmenger’s Syndrome: Patent Ductus Arteriosus (PDA)",
          "b-syndrome-vsd"                 => "Eisenmenger’s Syndrome: Ventricular Septal Defect (VSD)",
          "b-portopulmonary-hyper"         => "Portopulmonary Hypertension",
          "b-pulmonary-h-p-arterial"       => "Primary Pulmonary Hypertension / Pulmonary Arterial Hypertension",
          "b-capillary-hemangiomatosis"    => "Pulmonary Capillary Hemangiomatosis",
          "b-telangiectasia–hypertension"  => "Pulmonary Telangiectasia – Pulmonary Hypertension",
          "b-thromboembolic-disease"       => "Pulmonary Thromboembolic Disease",
          "b-vascular-disease"             => "Pulmonary Vascular Disease",
          "b-veno-occlusive-disease"       => "Pulmonary Veno - Occlusive Disease",
          "b-pulmonic-stenosis"            => "Pulmonic Stenosis",
          "b-right-hypoplastic-lung"       => "Right Hypoplastic Lung",
          "b-scleroderma-hypertension"     => "Scleroderma – Pulmonary Hypertension",
          "b-secondary-pulmonary-hyper"    => "Secondary Pulmonary Hypertension",
          "b-thromboembolic-pulmonary"     => "Thromboembolic Pulmonary Hypertension",
          "group-c"                        => "Group C: بیماری‌های عفونی ریه",
          "c-common-variable-immune"       => "Common Variable Immune Deficiency",
          "c-cystic-fibrosis"              => "Cystic Fibrosis",
          "c-fibrocavitary-lung-disease"   => "Fibrocavitary Lung Disease",
          "c-hypogammaglobulinemia"        => "Hypogammaglobulinemia",
          "c-schwachman-diamond-syndrome"  => "Schwachman - Diamond Syndrome",
          "group-d"                        => "Group D: بیماری‌های بافت بینابینی",
          "d-abca3-transporter-mutation"   => "ABCA3 Transporter Mutation",
          "d-alveolar-proteinosis"         => "Alveolar Proteinosis",
          "d-amyloidosis"                  => "Amyloidosis",
          "d-acute-respiratory-syndrome"   => "Acute Respiratory Distress Syndrome or Pneumonia",
          "d-bronchioloalveolar-bac"       => "Bronchioloalveolar carcinoma (BAC)",
          "d-carcinoid-tumorlets"          => "Carcinoid Tumorlets",
          "d-chronic-pneumonitis-infancy"  => "Chronic Pneumonitis of Infancy",
          "d-constrictive-bronchiolitis"   => "Constrictive Bronchiolitis",
          "d-crest-restrictive"            => "CREST – Restrictive",
          "d-eosinophilic-granuloma"       => "Eosinophilic Granuloma",
          "d-fibrosing-mediastinitis"      => "Fibrosing Mediastinitis",
          "d-graft-versus-host-gvhd"       => "Graft Versus Host Disease (GVHD)",
          "d-hermansky-pudlak-syndrome"    => "Hermansky Pudlak Syndrome",
          "d-hypersensitivity-pneumonitis" => "hypersensitivity-pneumonitis",
          "d-idiopathic-pneumonia"         => "Idiopathic Interstitial Pneumonia",
     ],

     'type-of-supportive-treatment' => [
          "ecmo"                  => "اکمو",
          "intra-aortic-balloon"  => "بالن پمپ داخل آئورتی",
          "infusion-prostacyclin" => "انفوزیون پروستاسیکلین",
          "inhaled-prostacyclin"  => "پروستاسیکلین استنشاقی",
          "nitric-oxide-gas"      => "گاز مونواکسید نیتروژن",
          "ventilator"            => "ونتیلاتور",
          "others"                => "سایر",
     ],

     'smoking' => [
          "active" => "مصرف‌کننده فعال",
          "quit"   => "ترک کرده",
     ],

     'smoking-per-year' => [
          "0-10"     => "۰-۱۰",
          "11-20"    => "۱۱-۲۰",
          "21-30"    => "۲۱-۳۰",
          "31-40"    => "۳۱-۴۰",
          "41-50"    => "۴۱-۵۰",
          "above-50" => "بیش از ۵۰",
     ],

     'opium' => [
          "edible"  => "خوراکی",
          "inhaler" => "استنشاقی",
     ],

     'no-smoking-period' => [
          "0-2"      => "۰-۲",
          "3-12"     => "۳-۱۲",
          "13-24"    => "۱۳-۲۴",
          "25-36"    => "۲۵-۳۶",
          "37-48"    => "۳۷-۴۸",
          "49-60"    => "۴۹-۶۰",
          "above-60" => "بیش از ۶۰",
     ],

     'chest-surgery-type' => [
          "pneumoreduction"      => "Pneumoreduction",
          "pneumothorax-decorti" => "Pneumothorax Decortication",
          "pneumothorax-surgery" => "Pneumothorax Surgery-Nodule",
          "lobectomy"            => "Lobectomy",
          "pneumonectomy"        => "Pneumonectomy",
          "left-thoracotomy"     => "Left Thoracotomy",
          "right-thoracotomy"    => "Right Thoracotomy",
          "cabg"                 => "CABG",
          "valve-replace"        => "Valve Replacement / Repair",
          "congenital"           => "Congenital",
          "left-ventri-remodel"  => "Left Ventricular Remodeling",
          "others"               => "Others",
     ],

     'number-of-sternotomy' => [
          "unknown" => "نامشخص",
          "0"       => "۰",
          "1"       => "۱",
          "2"       => "۲",
          "3"       => "۳",
          "4"       => "۴",
     ],

     'graft-other-organs' => [
          "heart"    => "قلب",
          "kidney"   => "کلیه",
          "liver"    => "کبد",
          "pancreas" => "پانکراس",
          "gut"      => "روده",
          "marrow"   => "مغز استخوان",
     ],

     'organ-status' => [
          "acceptable-performance" => "عملکرد قابل قبول",
          "relative-performance"   => "عملکرد نسبی",
          "no-performance"         => "بدون عملکرد",
     ],

     'diabetes-type' => [
          "type-1" => "نوع ۱",
          "type-2" => "نوع ۲",
     ],

     'diabetes-control' => [
          "c"  => "کنترل‌شده",
          "nc" => "کنترل‌نشده",
     ],

     'diabetes-cure' => [
          "injection" => "تزریقی",
          "edible"    => "خوراکی",
     ],

     'earlier-peptic-ulcer' => [
          "active-in-last-year"   => "فعال در یک سال اخیر",
          "inactive-in-last-year" => "غیرفعال در یک سال اخیر",
     ],

     'type-of-malignancy' => [
          "melanoma"               => "ملانوم پوستی",
          "other-malignant-skin"   => "سایر بدخیمی‌های پوست",
          "central-nervous-system" => "دستگاه عصبی مرکزی",
          "genital"                => "ادراری تناسلی",
          "breast"                 => "پستان",
          "tongue-throat-larynx"   => "زبان یا حلق یا حنجره",
          "thyroid"                => "تیروئید",
          "lung"                   => "ریه",
          "leukemia-or-lymphoma"   => "لوکمی / لنفوم",
          "liver"                  => "کبد",
          "others"                 => "سایر",
     ],

     'hospitalized-from-last-to-recent-visit' => [
          "organ-rejection" => "بستری به علت رد پیوند",
          "infection"       => "بستری به علت عفونت",
          "others"          => "سایر",
     ],

     'transplanted-lung-status' => [
          "functioning" => "Functioning",
          "failed"      => "Failed",
     ],

     'graft-failure-cause' => [
          "initial-failure"         => "عدم کارکرد اولیه",
          "acute-rejection"         => "رد پیوند حاد",
          "chronic-rejection"       => "رد پیوند مزمن",
          "relapse-primary-illness" => "عود بیماری اولیه",
          "noncompliance-patient"   => "عدم کمپلیانس بیمار",
          "infection"               => "عفونت",
          "others"                  => "سایر",
     ],

     'clinical-evaluation-patient-status' => [
          "alive"   => "زنده",
          "dead"    => "فوت",
          "unknown" => "نامعلوم",
     ],

     'clinical-evaluation-cause-death' => [
          "acute-pulmonary-graft-rejection"   => "رد پیوند حاد ریوی",
          "chronic-pulmonary-graft-rejection" => "رد پیوند مزمن ریوی",
          "other-pulmonary-causes"            => "سایر علل ریوی",
          "infection"                         => "عفونت",
          "bleeding"                          => "خونریزی",
          "renal-failure"                     => "نارسایی کلیوی",
          "heart"                             => "قلبی",
          "other-non-pulmonary-causes"        => "سایر علل غیر ریوی",
     ],

     'death-place' => [
          "house"                          => "منزل",
          "hospital-of-graft-site"         => "بیمارستان محل پیوند",
          "hospital-other-than-graft-site" => "بیمارستان غیر از محل پیوند",
     ],

     'bronchiolitis-obliterans-syndrome' => [
          "does-not-have"   => "ندارد",
          "has-it"          => "دارد",
          "grade-op"        => "درجه OP",
          "grade-1"         => "درجه ۱",
          "grade-2"         => "درجه ۲",
          "grade-3"         => "درجه ۳",
          "uncertain-grade" => "درجه نامشخص",
          "unknown"         => "نامشخص",
     ],

     'ras' => [
          "has-it"        => "دارد",
          "does-not-have" => "ندارد",
          "dubious"       => "مشکوک",
     ],

     'sampling-number' => [
          "first"   => "اول",
          "second"  => "دوم",
          "third"   => "سوم",
          "fourth"  => "چهارم",
          "fifth"   => "پنجم",
          "sixth"   => "ششم",
          "seventh" => "هفتم",
          "eighth"  => "هشتم",
          "ninth"   => "نهم",
          "tenth"   => "دهم",
     ],

     'sampling-method' => [
          "bronchoscopy"  => "برونکوسکوپی",
          "ct-scan-guide" => "نمونه‌برداری با گاید سی‌تی اسکن",
          "open-sampling" => "نمونه‌برداری باز",
     ],

     'pathology-result' => [
          "a-acute-rejection"     => "A: Acute Rejection",
          "a-grade-0-none"        => "Grade 0: None",
          "a-grade-1-minimal"     => "Grade 1: Minimal",
          "a-grade-2-mild"        => "Grade 2: Mild",
          "a-grade-3-moderate"    => "Grade 3: Moderate",
          "a-grade-4-severe"      => "Grade 4: Severe",
          "b-airway-inflammation" => "B: Airway Inflammation",
          "b-grade-0-none"        => "Grade 0: None",
          "b-grade-1r-low-grade"  => "Grade 1R: Low Grade",
          "b-grade-2r-high-grade" => "Grade 2R: High Grade",
          "b-grade-x-ungradable"  => "Grade X: Ungradable",
          "c-chronic-airway"      => "C: Chronic Airway Rejection (Obliterative Bronchiolitis)",
          "c-0-absent"            => "0: Absent",
          "c-1-present"           => "1: Present",
          "d-chronic-vascular"    => "D: Chronic Vascular Rejection",
          "d-0-absent"            => "0: Absent",
          "d-1-present"           => "1: Present",
          "others"                => "Others",
     ],

     'required-graft-type' => [
          "liver"    => "کبد",
          "heart"    => "قلب",
          "kidney"   => "کلیه",
          "pancreas" => "پانکراس",
          "lung"     => "ریه",
          "gut"      => "روده",
          "marrow"   => "مغز استخوان",
     ],

     'previous-graft-type' => [
          "heart"    => "قلب",
          "kidney"   => "کلیه",
          "liver"    => "کبد",
          "pancreas" => "پانکراس",
          "gut"      => "روده",
     ],

     'injected-vaccines' => [
          "influenza"    => "آنفلوانزا",
          "pneumococcus" => "پنوموکوک",
          "others"       => "سایر",
     ],

     'injected-vaccines-2' => [
          "influenza"    => "آنفلوانزا",
          "pneumococcus" => "پنوموکوک",
          "hepatitis-b"  => "هپاتیت ب",
          "others"       => "سایر",
     ],

     'complication-after-transplantation' => [
          "pulmonary"           => "ریوی",
          "cardiovascular"      => "قلبی عروقی",
          "digestive"           => "گوارشی",
          "uric"                => "ادراری",
          "muscular-bone"       => "استخوانی عضلانی",
          "neurological"        => "نورولوژیک",
          "rheumatologic"       => "روماتولوژیک",
          "lymphatic-drainage"  => "خونی لنفی",
          "endocrine-Metabolic" => "غدد و متابولیک",
          "cutaneous"           => "پوستی",
          "optic"               => "چشمی",
          "ear-nose-throat"     => "گوش و حلق و بینی",
          "genital"             => "دستگاه تناسلی",
          "mental"              => "روحی روانی",
          "drug-abuse"          => "سوء مصرف مواد",
          "infection"           => "عفونت",
          "malignancy"          => "بدخیمی",
          "fever-without-cause" => "تب بدون علت",
          "severe-weight-loss"  => "کاهش وزن شدید",
          "severe-weight-gain"  => "افزایش وزن شدید",
     ],

     'type-of-complication-after-graft' => [
          "spirometry"               => "اسپیرومتری",
          "hrct"                     => "دمی و بازدمی HRCT",
          "ct-scan"                  => "سی‌تی اسکن",
          "biopsy"                   => "بیوپسی",
          "simple-chest-radiography" => "رادیوگرافی ساده سینه",
          "blood-gas"                => "گاز خون",
          "sleep-test"               => "تست خواب",
          "checkup"                  => "معاینه",
          "ecg"                      => "نوار قلبی",
          "holter-monitoring"        => "هولتر مونیتورینگ",
          "echocardiography"         => "اکوکاردیوگرافی",
          "cardiac-catheterization"  => "کتتریزاسیون قلبی",
          "catheterization"          => "کاتتریزاسیون",
          "fitness-test"             => "تست ورزش",
          "radioisotope-scan"        => "اسکن رادیوایزوتوپ",
          "echo-stress"              => "استرس اکو",
          "doppler-ultrasound"       => "سونوگرافی داپلر",
          "venography"               => "ونوگرافی",
     ],

     'therapeutic-action' => [
          "surgery"  => "جراحی",
          "medicine" => "دارویی",
          "others"   => "سایر",
     ],

     'result-of-treatment' => [
          "complete-recovery" => "بهبودی کامل",
          "partial-recovery"  => "بهبودی نسبی",
          "lack-of-recovery"  => "عدم بهبودی",
     ],

     'organ-type' => [
          "lung"                 => "ریه",
          "heart"                => "قلب",
          "kidney"               => "کلیه",
          "liver"                => "کبد",
          "soft-tissue"          => "بافت نرم",
          "bones-and-joints"     => "استخوان و مفاصل",
          "digestion"            => "گوارش",
          "genitourinary-system" => "سیستم ادراری تناسلی",
          "others"               => "سایر",
     ],

     'pathogenic-agent' => [
          "virus"         => "ویروس",
          "bacteria"      => "باکتری",
          "mushroom"      => "قارچ",
          "mycobacterium" => "مایکوباکتریوم",
     ],

     'heart-disease-type' => [
          "heart-attack"                => "سکته قلبی",
          "permanent-pacemaker"         => "پیس‌میکر دایمی",
          "temporary-pacemaker"         => "پیس‌میکر موقت",
          "ventricular-arrhythmia"      => "آریتمی بطنی",
          "supraventricular-arrhythmia" => "آریتمی فوق‌بطنی",
          "others"                      => "سایر",
     ],

     'cause-malignancy' => [
          "related-to-donor"          => "مرتبط با اهداکننده",
          "previous-tumor-recurrence" => "عود تومور قبلی",
          "new-tumor"                 => "تومور جدید",
          "new-lymphoproliferative"   => "بیماری لنفوپرولیفراتیو جدید",
     ],

     'sequel' => [
          "positive" => "مثبت",
          "negative" => "منفی",
     ],

     'cyclosporine-category-1' => [
          "neurale"  => "نئورال",
          "iminoral" => "ایمینورال",
          "others"   => "سایر",
     ],

     'tacrolimus-category-1' => [
          "prograph" => "پروگراف",
          "cograf"   => "کوگراف",
          "others"   => "سایر",
     ],

     'mycophenolic-acid' => [
          "cellcept"  => "سلسپت",
          "myofortic" => "مایفورتیک",
          "supreme"   => "سوپریمون",
          "others"    => "سایر",
     ],

     'immunosuppressive-drugs' => [
          "methyl-prednisolone" => "متیل پردنیزولون",
          "cyclosporine"        => "سیکلوسپورین",
          "tacrolimus"          => "تاکرولیموس",
          "acid-mycophenolate"  => "مایکوفنولات اسید",
          "atg"                 => "ATG",
          "il-2-receptor"       => "IL-2 Receptor Inhibitor",
          "others"              => "سایر",
     ],

     'l-c-e-patient-status' => [
          "waiting-for-graft"  => "منتظر پیوند",
          "death-before-graft" => "فوت قبل از پیوند",
          "transplanted"       => "پیوند شده",
          "death-after-graft"  => "فوت بعد از پیوند",
          "unknown"            => "نامعلوم",
     ],

     'preferred-waiting-list' => [
          "active"   => "فعال",
          "inactive" => "غیرفعال",
     ],

     'active-priority-type' => [
          "p-1-children" => "اولویت ۱ اطفال",
          "p-2-children" => "اولویت ۲ اطفال",
          "12-17"        => "۱۲ تا ۱۷ سال",
          "above-17"     => "بالای ۱۷ سال",
     ],

     'reason-disabled-waiting-list' => [
          "very-bad-physical-condition" => "شدیداً بدحال",
          "recovery"                    => "بهبودی",
          "technical"                   => "تکنیکی",
          "disrupted-experiments"       => "آزمایشات مختل",
          "active-infection"            => "عفونت فعال",
          "poor-economic-support"       => "حمایت ضعیف اقتصادی یا اجتماعی",
          "psychological-disorder"      => "اختلال سایکولوژیک",
          "active-addiction"            => "اعتیاد فعال",
          "other-organs-disease"        => "بیماری سایر ارگان‌ها",
          "canceling-transplantation"   => "انصراف از پیوند",
          "other-cases"                 => "سایر موارد",
     ],

     'technical-reason-disabling' => [
          "need-to-not-available-equipment" => "نیاز به تجهیزات پیشرفته ناموجود",
          "high-mortality-probability"      => "احتمال بسیار بالای مرگ و میر",
          "others"                          => "سایر",
     ],

     'lung-waiting-list-patient-status' => [
          "waiting-for-graft"  => "منتظر پیوند",
          "death-before-graft" => "فوت قبل از پیوند",
          "unknown"            => "نامعلوم",
     ],

     'patient-general-cause-death' => [
          "pulmonary"     => "ریوی",
          "non-pulmonary" => "غیر ریوی",
     ],

     'patient-status-at-time-of-graft' => [
          "hospitalized-in-icu"     => "بستری در ICU",
          "hospitalized-except-icu" => "بستری در بیمارستان به جز ICU",
          "non-hospitalization"     => "غیر بستری",
     ],

     'blood-transfusion-type' => [
          "ffp"              => "FFP",
          "cryopercipitate"  => "Cryopercipitate",
          "packed-cell"      => "Packed Cell",
          "whole-blood-cell" => "Whole Blood Cell",
     ],

     'heart-surgery-type' => [
          "open-heart-surgery"          => "جراحی قلب باز",
          "restoration-of-valve"        => "ترمیم یا تعویض دریچه",
          "congenital"                  => "مادرزادی",
          "left-ventricular-correction" => "اصلاح بطن چپ",
          "others"                      => "سایر",
     ],

     'pulmonary-surgery-type' => [
          "pneumonia"                    => "پنوموریداکشن",
          "pneumothorax-nodules-surgery" => "جراحی ندول پنوموتوراکس",
          "pneumotorax-decompression"    => "دکورتیکاسیون پنوموتوراکس",
          "lobectomy"                    => "لوبکتومی",
          "pneumonectomy"                => "پنومونکتومی",
          "left-thoracotomy"             => "توراکوتومی چپ",
          "right-thoracotomy"            => "توراکوتومی راست",
          "others"                       => "سایر",
     ],

     'time-of-use-ecmo' => [
          "at-time-of-transplantation"      => "در زمان پیوند",
          "three-months-before-transplant"  => "سه ماه قبل از پیوند",
          "more-than-3-months-before-graft" => "بیش از سه ماه قبل از پیوند",
     ],

     'transplant-surgery-type' => [
          "left-lung"             => "ریه چپ",
          "right-lung-en-bloc"    => "ریه راست (EN-BLOC)",
          "bilateral-double-lung" => "Bilateral Sequential Double Lung",
          "en-bloc-double-lung"   => "EN-BLOC Double Lung",
          "right-lobe"            => "لوب راست",
          "left-lobe"             => "لوب چپ",
     ],

     'type-of-donor-lung-preservative' => [
          "perfadex" => "پرفادکس",
          "others"   => "سایر",
     ],

     'injected-prostaglandin-e1-into-donor' => [
          "systemic" => "سیستمیک",
          "central"  => "سنترال",
     ],

     'no-title-cardiopulmonary' => [
          "subclavian" => "ساب کلاوین",
          "femoral"    => "فمورال",
     ],

     'use-of-ecmo-bypass' => [
          "central"       => "سنترال",
          "environmental" => "محیطی",
     ],

     'ecmo-type' => [
          "av"  => "AV",
          "vv"  => "VV",
          "avv" => "AVV",
     ],

     'complications-during-surgery' => [
          "arrhythmia"             => "آریتمی",
          "bleeding"               => "خون‌ریزی",
          "long-hypoxia"           => "هیپوکسی طولانی",
          "long-hypotension"       => "هیپوتانسیون طولانی",
          "cardiac-arrest"         => "ایست قلبی",
          "lack-of-organ-function" => "عدم کارکرد اولیه ارگان",
          "ecmo-complications"     => "عوارض اکمو",
          "right-lung-failure"     => "نارسایی ریه راست",
          "left-lung-failure"      => "نارسایی ریه چپ",
          "valve-failure"          => "نارسایی دریچه",
          "decreased-urine-output" => "کاهش برون‌ده ادراری",
          "others"                 => "سایر",
     ],

     'treatment_type' => [
          "surgery"  => "جراحی",
          "medicine" => "دارویی",
     ],

     'severity-of-failure' => [
          "mild"     => "خفیف",
          "severe"   => "شدید",
          "moderate" => "متوسط",
     ],

     'arrangements-for-failure' => [
          "ecmo"         => "اکمو",
          "balloon-pump" => "بالن پمپ",
     ],

     'graft-failure-cause-2' => [
          "initial-failure"            => "عدم کارکرد اولیه",
          "acute-rejection"            => "رد پیوند حاد",
          "relapse-of-primary-illness" => "عود بیماری اولیه",
          "discontinue-medication"     => "قطع دارو",
          "infection"                  => "عفونت",
          "others"                     => "سایر",
     ],

     'final-result' => [
          "complete-recovery"            => "بهبودی کامل",
          "partial-recovery"             => "بهبودی نسبی",
          "chronic-transplant-rejection" => "رد پیوند مزمن",
          "lack-of-organ-function"       => "عدم عملکرد عضو",
     ],

     'cxr' => [
          "normal"  => "Normal",
          "diffuse" => "Diffuse Allograft Infiltration",
     ],

     'discharge-list-patient-status' => [
          "waiting-for-graft" => "منتظر پیوند مجدد",
          "death-after-graft" => "فوت بعد از پیوند",
     ],

     'lung-follow-up-list-patient' => [
          "need-to-re-graft"  => "نیاز به پیوند مجدد",
          "death-after-graft" => "فوت بعد از پیوند",
     ],

     'not-accepted-disapproval-factor' => [
          "donor"                  => "اهداکننده",
          "receiver"               => "گیرنده",
          "graft-center-personnel" => "مرکز پیوند - پرسنل",
          "graft-center-equipment" => "مرکز پیوند - تجهیزات",
     ],

     'no-transplant-disapproval-factor' => [
          "donor"        => "اهداکننده",
          "receiver"     => "گیرنده",
          "graft-center" => "مرکز پیوند",
     ],

];
