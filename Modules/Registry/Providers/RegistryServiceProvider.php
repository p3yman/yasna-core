<?php

namespace Modules\Registry\Providers;

use Illuminate\Support\Facades\Validator;
use Modules\Registry\Events\UnityUpdated;
use Modules\Registry\Http\Endpoints\V1\AssigneeListEndpoint;
use Modules\Registry\Http\Endpoints\V1\AssignMasterEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseCreateEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseFetchEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseListEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseOrgansEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseOrgansFetchEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseSaveEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseSingleEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseTransferEditorEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseTransferListEndpoint;
use Modules\Registry\Http\Endpoints\V1\CaseTrashEndpoint;
use Modules\Registry\Http\Endpoints\V1\ChangePasswordEndpoint as ChangePasswordEndpointV1;
use Modules\Registry\Http\Endpoints\V1\CheckFulfilmentEndpoint;
use Modules\Registry\Http\Endpoints\V1\GetLoggedInUserEndpoint;
use Modules\Registry\Http\Endpoints\V1\GetUserEndpoint;
use Modules\Registry\Http\Endpoints\V1\HospitalEditorEndpoint as HospitalEditorEndpointV1;
use Modules\Registry\Http\Endpoints\V1\HospitalListEndpoint;
use Modules\Registry\Http\Endpoints\V1\LastLoginsEndpoint;
use Modules\Registry\Http\Endpoints\V1\LoginsEndpoint;
use Modules\Registry\Http\Endpoints\V1\OpuEditorEndpoint;
use Modules\Registry\Http\Endpoints\V1\OpuListEndpoint;
use Modules\Registry\Http\Endpoints\V1\RoleRevokeEndpoint;
use Modules\Registry\Http\Endpoints\V1\SaveSelectedRoleEndpoint;
use Modules\Registry\Http\Endpoints\V1\SimilarCasesEndpoint;
use Modules\Registry\Http\Endpoints\V1\SuperadminAssignSupervisorEndpoint;
use Modules\Registry\Http\Endpoints\V1\SuperadminDeleteEndpoint;
use Modules\Registry\Http\Endpoints\V1\SuperadminEndpoint;
use Modules\Registry\Http\Endpoints\V1\SuperadminSaveEndpoint;
use Modules\Registry\Http\Endpoints\V1\SuperadminsEndpoint;
use Modules\Registry\Http\Endpoints\V1\UnityDeactivateEndpoint;
use Modules\Registry\Http\Endpoints\V1\UnityDeleteEndpoint;
use Modules\Registry\Http\Endpoints\V1\UnityRolesEndpoint;
use Modules\Registry\Http\Endpoints\V1\UserDeactivateEndpoint;
use Modules\Registry\Http\Endpoints\V1\UserEditEndpoint;
use Modules\Registry\Http\Endpoints\V1\UserEditPasswordEndpoint;
use Modules\Registry\Http\Endpoints\V1\UserFieldsEndpoint;
use Modules\Registry\Http\Endpoints\V1\UserProfileEndpoint;
use Modules\Registry\Http\Endpoints\V1\UserRolesEndpoint;
use Modules\Registry\Http\Endpoints\V1\UserValidationRulesEndpoint;
use Modules\Registry\Http\Endpoints\V1\WardEditorEndpoint as WardEditorEndpointV1;
use Modules\Registry\Http\Endpoints\V1\LoginEndpoint;
use Modules\Registry\Http\Endpoints\V1\OpuInformationCompleteEndpoint;
use Modules\Registry\Http\Endpoints\V1\RoleAssignEndpoint;
use Modules\Registry\Http\Endpoints\V1\UnitySingleEndpoint;
use Modules\Registry\Http\Endpoints\V1\UniversityEditorEndpoint;
use Modules\Registry\Http\Endpoints\V1\UniversityListEndpoint;
use Modules\Registry\Http\Endpoints\V1\WardListEndpoint;
use Modules\Registry\Http\Middleware\V1\DeactivatedCheckMiddleware;
use Modules\Registry\Listeners\UpdateCasesOnUnityUpdate;
use Modules\Registry\Listeners\UpdateChildrenParentsOnUnityUpdate;
use Modules\Registry\Schedules\SeconderExpiryCheckSchedule;
use Modules\Yasna\Services\YasnaProvider;

/**
 * Class RegistryServiceProvider
 *
 * @package Modules\Registry\Providers
 */
class RegistryServiceProvider extends YasnaProvider
{
    /**
     * This will be automatically loaded in the app boot sequence if the module is active.
     *
     * @return void
     */
    public function index()
    {
        $this->redefineUsernameField();
        $this->registerEndpoints();
        $this->registerModelTraits();
        $this->mapVersions();
        $this->registerSchedules();
        $this->registerGlobalMiddlewares();
        $this->registerListeners();
        $this->registerCustomValidators();
    }



    /**
     * register endpoints
     *
     * @return void
     */
    private function registerEndpoints()
    {
        $this->registerUnityEndpoints();
        $this->registerRoleManagementEndpoints();
        $this->registerAuthenticationEndpoints();
        $this->registerProfileModificationEndpoints();
        $this->registerCaseEndpoints();
        $this->registerSuperadminEndpoints();
    }



    /**
     * register all the endpoints relating to the cases (patients)
     *
     * @return void
     */
    private function registerCaseEndpoints()
    {
        endpoint()->register(CaseSaveEndpoint::class);
        endpoint()->register(CaseListEndpoint::class);
        endpoint()->register(CaseSingleEndpoint::class);
        endpoint()->register(CaseFetchEndpoint::class);
        endpoint()->register(CaseTransferEditorEndpoint::class);
        endpoint()->register(CaseTransferListEndpoint::class);
        endpoint()->register(CaseOrgansEndpoint::class);
        endpoint()->register(CaseOrgansFetchEndpoint::class);
        endpoint()->register(SimilarCasesEndpoint::class);
        endpoint()->register(CaseTrashEndpoint::class);
    }



    /**
     * register all the endpoints relating to the profile modifications
     *
     * @return void
     */
    private function registerProfileModificationEndpoints()
    {
        endpoint()->register(UserProfileEndpoint::class);
        endpoint()->register(UserValidationRulesEndpoint::class);
        endpoint()->register(UnityRolesEndpoint::class);
        endpoint()->register(GetUserEndpoint::class);
        endpoint()->register(LastLoginsEndpoint::class);
        endpoint()->register(UserEditEndpoint::class);
        endpoint()->register(UserFieldsEndpoint::class);
        endpoint()->register(UserDeactivateEndpoint::class);
    }



    /**
     * register all the endpoints relating to the the authentication and password resets
     *
     * @return void
     */
    private function registerAuthenticationEndpoints()
    {
        endpoint()->register(LoginEndpoint::class);
        endpoint()->register(UserEditPasswordEndpoint::class);
        endpoint()->register(ChangePasswordEndpointV1::class);
        endpoint()->register(GetLoggedInUserEndpoint::class);
        endpoint()->register(LoginsEndpoint::class);
    }



    /**
     * register all the endpoints relating to the unities
     *
     * @return void
     */
    private function registerUnityEndpoints()
    {
        /*-----------------------------------------------
        | General Purpose ...
        */
        endpoint()->register(UnitySingleEndpoint::class);
        endpoint()->register(UnityDeleteEndpoint::class);
        endpoint()->register(UnityDeactivateEndpoint::class);

        /*-----------------------------------------------
        | Universities ...
        */
        endpoint()->register(UniversityEditorEndpoint::class);
        endpoint()->register(UniversityListEndpoint::class);

        /*-----------------------------------------------
        | Hospitals ...
        */
        endpoint()->register(HospitalEditorEndpointV1::class);
        endpoint()->register(HospitalListEndpoint::class);

        /*-----------------------------------------------
        | Ward ...
        */
        endpoint()->register(WardEditorEndpointV1::class);
        endpoint()->register(WardListEndpoint::class);


        /*-----------------------------------------------
        | OPU ...
        */
        endpoint()->register(OpuInformationCompleteEndpoint::class);
        endpoint()->register(OpuEditorEndpoint::class);
        endpoint()->register(OpuListEndpoint::class);
    }



    /**
     * register role management endpoints
     */
    private function registerRoleManagementEndpoints()
    {
        endpoint()->register(RoleAssignEndpoint::class);
        endpoint()->register(RoleRevokeEndpoint::class);
        endpoint()->register(AssigneeListEndpoint::class);
        endpoint()->register(UserRolesEndpoint::class);
        endpoint()->register(SuperadminAssignSupervisorEndpoint::class);
        endpoint()->register(CheckFulfilmentEndpoint::class);
        endpoint()->register(SaveSelectedRoleEndpoint::class);
    }



    /**
     * register endpoints related to the superadmins actions
     */
    private function registerSuperadminEndpoints()
    {
        endpoint()->register(SuperadminsEndpoint::class);
        endpoint()->register(SuperadminEndpoint::class);
        endpoint()->register(SuperadminSaveEndpoint::class);
        endpoint()->register(SuperadminDeleteEndpoint::class);

        endpoint()->register(AssignMasterEndpoint::class);
    }



    /**
     * redefine Username Field
     *
     * @return void
     */
    private function redefineUsernameField()
    {
        config([
             "auth.providers.users.field_name" => "code_melli",
        ]);
    }



    /**
     * register model traits
     */
    private function registerModelTraits()
    {
        $this->addModelTrait("UserRegistryTrait", "User");
        $this->addModelTrait("UserDemographicRelationTrait", "User");
        $this->addModelTrait("UserDemographicActionTrait", "User");
        $this->addModelTrait("UserUnityRelationsTrait", "User");
        $this->addModelTrait("UserJwtTrait", "User");
        $this->addModelTrait("UserPasswordSetterTrait", "User");
        $this->addModelTrait("CaseCensorStatsTrait", "Censor");
    }



    /**
     * map modules versions
     */
    private function mapVersions()
    {
        endpoint()->mapModulesVersion(1, [
             "registry"  => "1",
             "notifier"  => "1",
             "endpoint"  => "1",
             "combo"     => "1",
             "uploader"  => "1",
             "divisions" => "1",
             "help"      => "1",
             "census"    => "1",
        ]);
    }



    /**
     * register scheduled cron jobs
     */
    private function registerSchedules()
    {
        $this->addSchedule(SeconderExpiryCheckSchedule::class);
    }



    /**
     * Register middleware which must surrounded around all endpoints
     */
    private function registerGlobalMiddlewares()
    {
        $this->app['router']->pushMiddlewareToGroup('api', DeactivatedCheckMiddleware::class);
    }



    /**
     * register listeners
     */
    private function registerListeners()
    {
        $this->listen(UnityUpdated::class, UpdateChildrenParentsOnUnityUpdate::class);
        $this->listen(UnityUpdated::class, UpdateCasesOnUnityUpdate::class);
    }



    /**
     * register custom request validators
     */
    private function registerCustomValidators()
    {
        Validator::extend('inconsistent_with', 'Modules\Registry\Services\CustomValidators@inconsistentWith');
        Validator::replacer('inconsistent_with', 'Modules\Registry\Services\CustomValidators@inconsistentWithMessage');
    }
}
