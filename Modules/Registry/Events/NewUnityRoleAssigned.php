<?php 

namespace Modules\Registry\Events;

use Modules\Yasna\Services\YasnaEvent;

class NewUnityRoleAssigned extends YasnaEvent
{
    public $model_name = "RegistryUnityRole";
}
