<?php

namespace Modules\Registry\Events;

use Modules\Yasna\Services\YasnaEvent;

class UnityUpdated extends YasnaEvent
{
    public $model_name = "RegistrySharedUnity";
}
