<?php 

namespace Modules\Registry\Events;

use Modules\Yasna\Services\YasnaEvent;

class NewCaseCreated extends YasnaEvent
{
    public $model_name = "registry-case";
}
