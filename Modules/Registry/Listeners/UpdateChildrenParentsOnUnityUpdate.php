<?php

namespace Modules\Registry\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Events\UnityUpdated;

class UpdateChildrenParentsOnUnityUpdate implements ShouldQueue
{
    public $tries = 5;



    /**
     * handle the listener
     *
     * @param UnityUpdated $event
     */
    public function handle(UnityUpdated $event)
    {
        $this->process($event->model, $event->old);
    }



    /**
     * process the changes
     *
     * @param UnityAbstract $model
     * @param UnityAbstract $old
     *
     * @return void
     */
    private function process($model, $old)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($model->type == "university" or $model->type == "ward") {
            return;
        }


        /*-----------------------------------------------
        | Real Process ...
        */
        if ($model->type == "opu") {
            $this->processOpu($model, $old);
        }

        if ($model->type == "hospital") {
            $this->processHospital($model, $old);
        }

    }



    /**
     * process the changes
     *
     * @param UnityAbstract $model
     * @param UnityAbstract $old
     *
     * @return void
     */
    private function processOpu($model, $old)
    {
        model("RegistrySharedUnity")->where("opu_id", $model->id)->update([
             "university_id" => $model->university_id,
        ])
        ;
    }



    /**
     * process the changes
     *
     * @param UnityAbstract $model
     * @param UnityAbstract $old
     *
     * @return void
     */
    private function processHospital($model, $old)
    {
        model("RegistrySharedUnity")->where("hospital_id", $model->id)->update([
             "university_id" => $model->university_id,
             "opu_id"        => $model->opu_id,
        ])
        ;

    }
}
