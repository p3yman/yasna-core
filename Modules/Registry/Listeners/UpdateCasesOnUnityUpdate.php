<?php

namespace Modules\Registry\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Registry\Events\UnityUpdated;

class UpdateCasesOnUnityUpdate implements ShouldQueue
{
    public $tries = 5;



    /**
     * handle the listener
     *
     * @param UnityUpdated $event
     */
    public function handle(UnityUpdated $event)
    {
        $this->process($event->model, $event->old);
    }



    /**
     * process the changes
     *
     * @param UnityAbstract $model
     * @param UnityAbstract $old
     *
     * @return void
     */
    private function process($model, $old)
    {
        /*-----------------------------------------------
        | Bypass ...
        */
        if ($model->type == "university") {
            return;
        }

        /*-----------------------------------------------
        | Real Process ...
        */
        if ($model->type == "opu") {
            $this->processOpu($model, $old);
        }

        if ($model->type == "hospital") {
            $this->processHospital($model, $old);
        }

        if ($model->type == "ward") {
            $this->processWard($model, $old);
        }
    }



    /**
     * process the changes on opu
     *
     * @param UnityAbstract $model
     * @param UnityAbstract $old
     *
     * @return void
     */
    private function processOpu($model, $old)
    {
        if ($model->university_id == $old->university_id) {
            return;
        }

        foreach ($model->currentCases()->get() as $case) {
            $case->batchSave([
                 "university_id" => $model->university_id,
            ]);
        }
    }



    /**
     * process the changes on hospital
     *
     * @param UnityAbstract $model
     * @param UnityAbstract $old
     *
     * @return void
     */
    private function processHospital($model, $old)
    {
        if ($model->university_id == $old->university_id and $model->opu_id == $old->opu_id) {
            return;
        }

        foreach ($model->currentCases()->get() as $case) {
            $case->batchSave([
                 "university_id" => $model->university_id,
                 "opu_id"        => $model->opu_id,
            ]);
        }

    }



    /**
     * process the changes on ward
     *
     * @param UnityAbstract $model
     * @param UnityAbstract $old
     *
     * @return void
     */
    private function processWard($model, $old)
    {
        if ($model->university_id == $old->university_id and $model->opu_id == $old->opu_id and $model->hospital_id == $old->hospital_id) {
            return;
        }

        foreach ($model->currentCases()->get() as $case) {
            $case->batchSave([
                 "university_id" => $model->university_id,
                 "opu_id"        => $model->opu_id,
                 "hospital_id"   => $model->hospital_id,
            ]);
        }

    }
}
