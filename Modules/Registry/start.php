<?php

/*
|--------------------------------------------------------------------------
| Register Namespaces And Routes
|--------------------------------------------------------------------------
|
| When a module starting, this file will executed automatically. This helps
| to register some namespaces like translator or view. Also this file
| will load the routes file for each module. You may also modify
| this file as you want.
|
*/

if (!app()->routesAreCached()) {
    require __DIR__ . '/Http/routes.php';
}



if (!function_exists("university")) {

    /**
     * get university model
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\RegistryUniversity
     */
    function university($id_or_hashid = 0, $with_trashed = false)
    {
        if ($id_or_hashid and !$with_trashed) {
            return model('RegistryUniversity')->find($id_or_hashid);
        }

        return model('RegistryUniversity', $id_or_hashid, $with_trashed);
    }
}



if (!function_exists("opu")) {

    /**
     * get opu model
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\RegistryOpu
     */
    function opu($id_or_hashid = 0, $with_trashed = false)
    {
        if ($id_or_hashid and !$with_trashed) {
            return model('RegistryOpu')->find($id_or_hashid);
        }

        return model('RegistryOpu', $id_or_hashid, $with_trashed);
    }
}



if (!function_exists("hospital")) {

    /**
     * get hospital model
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\RegistryHospital
     */
    function hospital($id_or_hashid = 0, $with_trashed = false)
    {
        if ($id_or_hashid and !$with_trashed) {
            return model('RegistryHospital')->find($id_or_hashid);
        }

        return model('RegistryHospital', $id_or_hashid, $with_trashed);
    }
}



if (!function_exists("ward")) {

    /**
     * get ward model
     *
     * @param int  $id_or_hashid
     * @param bool $with_trashed
     *
     * @return \App\Models\RegistryWard
     */
    function ward($id_or_hashid = 0, $with_trashed = false)
    {
        if ($id_or_hashid and !$with_trashed) {
            return model('RegistryWard')->find($id_or_hashid);
        }

        return model('RegistryWard', $id_or_hashid, $with_trashed);
    }
}
