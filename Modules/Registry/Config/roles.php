<?php

return [

     'purification_rules' => [
          "code_melli"                      => "ed",
          "name_title"                      => "pd",
          "name_first"                      => "pd",
          "name_last"                       => "pd",
          "mobile"                          => "ed",
          "gender"                          => "ed|numeric",
          "edu_field"                       => "pd",
          "edu_level"                       => "",
          "is_retired"                      => "bool",
          "birth_date"                      => "date",
          "latest_job_position"             => "pd",
          "tel"                             => "ed",
          "tel_emergency"                   => "ed",
          "letter_issuance"                 => "date",
          "letter_expiry"                   => "date",
          "letter_file"                     => "",
          "work_unity"                      => "ed",
          "work_unity_other"                => "ed",
          "email"                           => "ed",
          "international_donor_certificate" => "",
          "iran_donor_certificate"          => "",
          "city"                            => "",
          "province"                        => "",
          "medical_system_number"           => "ed",
          "expertise"                       => "ed",
          "job"                             => "",
          "second_job"                      => "",
          "organs"                          => "",
          "job_history"                     => "",
          "is_in_science_committee"         => "",
     ],

     /*
     |--------------------------------------------------------------------------
     | Superadmin
     |--------------------------------------------------------------------------
     |
     */
     "superadmin"         => [
          'fields' => [
               "code_melli" => "",
               "name_first" => "",
               "name_last"  => "",
          ],
     ],

     /*
     |--------------------------------------------------------------------------
     | University Administrators
     |--------------------------------------------------------------------------
     | President and Deputy
     */


     'president' => [
          'fields' => [
               "code_melli" => "",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "tel"        => "required|phone:fixed",
               "mobile"     => "required|phone:mobile",
               "email"      => "email",
          ],
     ],

     'deputy'           => [
          'fields' => [
               "code_melli" => "",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "tel"        => "required|phone:fixed",
               "mobile"     => "required|phone:mobile",
               "email"      => "email",
          ],
     ],


     /*
     |--------------------------------------------------------------------------
     | University Experts
     |--------------------------------------------------------------------------
     |
     */
     'expert-in-charge' => [
          'fields' => [
               "code_melli" => "required",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "tel"        => "required|phone:fixed",
               "mobile"     => "required|phone:mobile",
               "email"      => "email",
          ],
     ],

     'donor-expert' => [
          'fields' => [
               "code_melli" => "required",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "tel"        => "phone:fixed",
               "mobile"     => "required|phone:mobile",
               "email"      => "email",
          ],
     ],

     'transplant-expert' => [
          'fields' => [
               "code_melli" => "required",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "tel"        => "phone:fixed",
               "mobile"     => "required|phone:mobile",
               "email"      => "email",
          ],
     ],

     /*
     |--------------------------------------------------------------------------
     | Seconders
     |--------------------------------------------------------------------------
     | seconders are in four groups, each having one chief, and one overall chief
     */


     'chief-seconder'                  => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'chief-neurologist-seconder'      => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'chief-neurosurgeons-seconder'    => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'chief-internist-seconder'        => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'chief-anesthesiologist-seconder' => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'neurologist-seconder'            => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'neurosurgeons-seconder'          => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'internist-seconder'              => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],
     'anesthesiologist-seconder'       => [
          'fields' => [
               "code_melli"              => "required",
               "name_first"              => "required",
               "name_last"               => "required",
               "mobile"                  => "required|phone:mobile",
               "tel"                     => "phone:fixed",
               "letter_issuance"         => "required",
               "letter_expiry"           => "required",
               "letter_file"             => "required",
               "work_unity"              => "required",
               "medical_system_number"   => "required",
               "is_in_science_committee" => "required|bool",
               "gender"                  => "required|numeric|min:0|max:3",
          ],
     ],

     /*
     |--------------------------------------------------------------------------
     | OPU Coordinators
     |--------------------------------------------------------------------------
     |
     */
     'chief-coordinator'               => [
          'fields' => [
               "code_melli"                      => "required",
               "name_first"                      => "required",
               "name_last"                       => "required",
               "gender"                          => "required|numeric|min:0|max:3",
               "birth_date"                      => "required",
               "mobile"                          => "required|phone:mobile",
               "edu_field"                       => "required",
               "work_unity"                      => "required",
               "international_donor_certificate" => "",
               "iran_donor_certificate"          => "",
          ],
     ],
     'coordinator'                     => [
          'fields' => [
               "code_melli"                      => "required",
               "name_first"                      => "required",
               "name_last"                       => "required",
               "gender"                          => "required|numeric|min:0|max:3",
               "birth_date"                      => "required",
               "mobile"                          => "required|phone:mobile",
               "edu_field"                       => "required",
               "work_unity"                      => "required",
               "international_donor_certificate" => "",
               "iran_donor_certificate"          => "",
          ],
     ],


     /*
     |--------------------------------------------------------------------------
     | OPU Protectors
     |--------------------------------------------------------------------------
     |
     */
     'protector'                       => [
          'fields' => [
               "code_melli"            => "required",
               "name_first"            => "required",
               "name_last"             => "required",
               "gender"                => "required|numeric|min:0|max:3",
               "mobile"                => "required|phone:mobile",
               "expertise"             => "required",
               "work_unity"            => "required",
               "medical_system_number" => "required",
          ],
     ],
     'chief-protector'                 => [
          "fields" => [
          ],
     ],


     /*
      |--------------------------------------------------------------------------
      | University Harvesters
      |--------------------------------------------------------------------------
      |
      */
     'harvester'                       => [
          'fields' => [
               "code_melli"            => "required",
               "name_first"            => "required",
               "name_last"             => "required",
               "gender"                => "required|numeric|min:0|max:3",
               "mobile"                => "required|phone:mobile",
               "expertise"             => "required",
               "work_unity"            => "required",
               "medical_system_number" => "required",
               "organs"                => "array|required",
          ],
     ],

     /*
      |--------------------------------------------------------------------------
      | Legal Medicines
      |--------------------------------------------------------------------------
      |
      */
     'legal-medicine'                  => [
          'fields' => [
               "code_melli"            => "required",
               "name_first"            => "required",
               "name_last"             => "required",
               "gender"                => "required|numeric|min:0|max:3",
               "mobile"                => "required|phone:mobile",
               "medical_system_number" => "required",
               "expertise"             => "",
               "edu_level"             => "required",
          ],
     ],

     'chief-legal-medicine'  => [
          "fields" => [],
     ],

     /*
      |--------------------------------------------------------------------------
      | OPU Manager
      |--------------------------------------------------------------------------
      |
      */
     'opu-manager'           => [
          'fields' => [
               "code_melli"                      => "required",
               "name_first"                      => "required",
               "name_last"                       => "required",
               "gender"                          => "required|numeric|min:0|max:3",
               "mobile"                          => "required|phone:mobile",
               "email"                           => "",
               "edu_field"                       => "required",
               "expertise"                       => "",
               "job_history"                     => "",
               "letter_issuance"                 => "required",
               "letter_file"                     => "",
               "second_job"                      => "",
               "international_donor_certificate" => "",
               "iran_donor_certificate"          => "",
          ],
     ],

     /*
      |--------------------------------------------------------------------------
      | OPU Inspectors
      |--------------------------------------------------------------------------
      |
      */
     'inspector-tel'         => [
          'fields' => [
               "code_melli"                      => "required",
               "name_first"                      => "required",
               "name_last"                       => "required",
               "gender"                          => "required|numeric|min:0|max:3",
               "birth_date"                      => "required",
               "tel_emergency"                   => "required",
               "mobile"                          => "required|phone:mobile",
               "edu_field"                       => "required",
               "is_retired"                      => "numeric|min:0|max:1",
               "job"                             => "required_without:is_retired",
               "work_unity"                      => "required_without:is_retired",
               "work_unity_other"                => "required_if:work_unity,1",
               "city"                            => "required",
               "province"                        => "required",
               "latest_job_position"             => "required",
               "second_job"                      => "",
               "international_donor_certificate" => "",
               "iran_donor_certificate"          => "",
          ],
     ],
     'inspector-clinical'    => [
          'fields' => [
               "code_melli"                      => "required",
               "name_first"                      => "required",
               "name_last"                       => "required",
               "gender"                          => "required|numeric|min:0|max:3",
               "tel_emergency"                   => "required",
               "mobile"                          => "required|phone:mobile",
               "edu_field"                       => "required",
               "is_retired"                      => "numeric|min:0|max:1",
               "job"                             => "required_without:is_retired",
               "work_unity"                      => "required_without:is_retired",
               "work_unity_other"                => "required_if:work_unity,1",
               "city"                            => "required",
               "province"                        => "required",
               "latest_job_position"             => "required",
               "second_job"                      => "",
               "international_donor_certificate" => "",
               "iran_donor_certificate"          => "",
          ],
     ],

     /*
      |--------------------------------------------------------------------------
      | University Transporters
      |--------------------------------------------------------------------------
      |
      */
     'transporter'           => [
          'fields' => [
               "code_melli" => "required",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "mobile"     => "required|phone:mobile",
          ],
     ],

     /*
      |--------------------------------------------------------------------------
      | University Consultants
      |--------------------------------------------------------------------------
      |
      */
     'consultant'            => [
          'fields' => [
               "code_melli"            => "required",
               "name_first"            => "required",
               "name_last"             => "required",
               "gender"                => "required|numeric|min:0|max:3",
               "mobile"                => "required|phone:mobile",
               "expertise"             => "required",
               "work_unity"            => "required",
               "medical_system_number" => "required",
          ],
     ],

     /*
      |--------------------------------------------------------------------------
      | Supervisors
      |--------------------------------------------------------------------------
      |
      */
     'university-supervisor' => [
          'fields' => [
               "code_melli"       => "required",
               "name_first"       => "required",
               "name_last"        => "required",
               "gender"           => "required|numeric|min:0|max:3",
               "edu_field"        => "required",
               "city"             => "required",
               "province"         => "required",
               "edu_level"        => "required",
               "job"              => "required",
               "mobile"           => "required|phone:mobile",
          ],
     ],
     'opu-supervisor'        => [
          'fields' => [
               "code_melli"       => "required",
               "name_first"       => "required",
               "name_last"        => "required",
               "gender"           => "required|numeric|min:0|max:3",
               "city"             => "required",
               "province"         => "required",
               "edu_level"        => "",
               "job"              => "",
               "mobile"           => "required|phone:mobile",
          ],
     ],


     /*
      |--------------------------------------------------------------------------
      | Hospital Administrators
      |--------------------------------------------------------------------------
      | President and Deputy
      */
     'hospital-president'    => [
          'fields' => [
               "code_melli" => "required",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "edu_level"  => "",
               "mobile"     => "required|phone:mobile",
          ],
     ],
     'hospital-deputy'       => [
          'fields' => [
               "code_melli" => "required",
               "name_first" => "required",
               "name_last"  => "required",
               "gender"     => "required|numeric|min:0|max:3",
               "edu_field"  => "",
               "edu_level"  => "",
               "job"        => "",
               "mobile"     => "required|phone:mobile",
          ],
     ],


     /*
      |--------------------------------------------------------------------------
      | Hospital Ward
      |--------------------------------------------------------------------------
      | Chief
      */
     'chief-ward'            => [
          'fields' => [
               "name_first" => "required",
               "name_last"  => "required",
               "job"        => "",
               "mobile"     => "required|phone:mobile",
          ],
     ],
];
