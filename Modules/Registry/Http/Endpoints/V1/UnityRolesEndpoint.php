<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\UsersController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-unity-roles
 *                    Unity Roles
 * @apiDescription    get a list of roles relative to a unity type
 * @apiVersion        1.0.0
 * @apiName           Unity Roles
 * @apiGroup          Registry
 * @apiPermission     guest
 * @apiParam {string} type    the unity type
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "type": "university"
 *      },
 *      "results": {
 *          "administrative_roles": [
 *              "expert-in-charge",
 *              "donor-expert",
 *              "transplant-expert",
 *              "president",
 *              "deputy"
 *          ],
 *          "dependant_roles": []
 *          }
 *     }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 404,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Not Found!",
 *      "errorCode": "404",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method UsersController controller()
 */
class UnityRolesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Unity Roles";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UsersController@unityRoles';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "type" => "university",
        ], [
             'administrative_roles' => [
                  "expert-in-charge",
                  "donor-expert",
                  "transplant-expert",
                  "president",
                  "deputy",
             ],
             'dependant_roles'      => [
                  'folan',
             ],
        ]);
    }
}
