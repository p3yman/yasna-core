<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-get-logged-in-user
 *                    Get Logged In User
 * @apiDescription    a general checker endpoint for the current user
 * @apiVersion        1.0.1
 * @apiName           Get Logged In User
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": {
 *          "id": "qKXVA",
 *          "name_title": null,
 *          "name_first": "Sir",
 *          "name_last": "Programmer",
 *          "full_name": "Sir Programmer",
 *          "code_melli": "664654654",
 *          "email": "dev@yasnateam.com",
 *          "mobile": null,
 *          "edu_level": null,
 *          "expertise": null,
 *          "job_history": null,
 *          "tel": null,
 *          "gender": 0,
 *          "latest_job_position": null,
 *          "is_retired": 0,
 *          "service_location": null,
 *          "second_job": null,
 *          "must_complete_unity_profile": true,
 *          "unities_for_complete_profile": [
 *              {
 *                  "name": "Tehran University",
 *                  "id": "qKXVA",
 *                  "type": "university"
 *              },
 *              {
 *                  "name": "Shahid Beheshti University",
 *                  "id": "gxnDN",
 *                  "type": "opu"
 *              }
 *          ],
 *          "roles": [
 *              "expert-in-charge",
 *              "inspector-clinical",
 *              "superadmin"
 *          ],
 *          "unities": [
 *              {
 *                  "name": "Tehran University",
 *                  "id": "qKXVA",
 *                  "type": "university",
 *                  "user_role": "expert-in-charge"
 *              },
 *              {
 *                  "name": "Shahid Beheshti University",
 *                  "id": "gxnDN",
 *                  "type": "opu",
 *                  "user_role": "inspector-clinical"
 *              }
 *          ],
 *          "birth_date": 1543869000
 *      }
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\UsersController controller()
 */
class GetLoggedInUserEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get Logged In User";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UsersController@userChecker';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"                           => "qKXVA",
             "name_title"                   => null,
             "name_first"                   => dummy()::englishWord(),
             "name_last"                    => dummy()::englishWord(),
             "full_name"                    => dummy()::englishWord(2),
             "code_melli"                   => "65654654",
             "email"                        => "dev@yasnateam.com",
             "mobile"                       => null,
             "edu_level"                    => null,
             "expertise"                    => null,
             "job_history"                  => null,
             "tel"                          => null,
             "gender"                       => 0,
             "latest_job_position"          => null,
             "is_retired"                   => 0,
             "service_location"             => null,
             "second_job"                   => null,
             "must_complete_unity_profile"  => true,
             "unities_for_complete_profile" => [
                  [
                       "name" => dummy()::englishWord(),
                       "id"   => "qKXVA",
                       "type" => "university",
                  ],
                  [
                       "name" => dummy()::englishWord(),
                       "id"   => "gxnDN",
                       "type" => "opu",
                  ],
             ],
             "roles"                        => [
                  "expert-in-charge",
                  "inspector-clinical",
                  "superadmin",
             ],
             "unities"                      => [
                  [
                       "name"      => dummy()::englishWord(),
                       "id"        => "qKXVA",
                       "type"      => "university",
                       "user_role" => "expert-in-charge",
                  ],
                  [
                       "name"      => dummy()::englishWord(),
                       "id"        => "gxnDN",
                       "type"      => "opu",
                       "user_role" => "inspector-clinical",
                  ],
             ],
             "birth_date"                   => 1543869000,
        ]);
    }
}
