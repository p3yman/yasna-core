<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\CaseController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-case-fetch
 *                    Case Fetch
 * @apiDescription    Fetch a case, as it is, without demographics info
 * @apiVersion        1.0.0
 * @apiName           Case Fetch
 * @apiGroup          Registry
 * @apiPermission     Admin
 * @apiParam {string} id
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *      },
 *      "results": {
 *          "id":"qKXVA",
 *          "file_no":"Xcytqn1vHP",
 *          "ward_id":"xrVrx",
 *          "hospital_id":"NPLdN",
 *          "opu_id":"rNYjA",
 *          "university_id":"QKgJx",
 *          "inspector_id":"xqDoA"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CaseController controller()
 */
class CaseFetchEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Case Fetch";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CaseController@fetch';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"            => "qKXVA",
             "file_no"       => "Xcytqn1vHP",
             "ward_id"       => "xrVrx",
             "hospital_id"   => "NPLdN",
             "opu_id"        => "rNYjA",
             "university_id" => "QKgJx",
             "inspector_id"  => "xqDoA",
        ], [
             "reached" => "CaseFetchEndpoint",
        ]);
    }
}
