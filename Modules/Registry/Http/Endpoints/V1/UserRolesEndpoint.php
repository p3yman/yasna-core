<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-user-roles
 *                    User Roles
 * @apiDescription    An Endpoint to Access the Roles of a User and Information About Each Role
 * @apiVersion        1.0.0
 * @apiName           User Roles
 * @apiGroup          Registry
 * @apiPermission     none
 * @apiParam {string} [hashid] The Hashid of the User. If missed, the logged in user will be considered.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "status": 200,
 *     "metadata": {
 *         "hashid": "qKXVA"
 *     },
 *     "results": [
 *         {
 *             "role": "donor-expert",
 *             "unity": "JAJwx",
 *             "unity_type": "university"
 *         },
 *         {
 *             "role": "opu-manager",
 *             "unity": "kxPdx",
 *             "unity_type": "opu"
 *         },
 *         {
 *             "role": "superadmin",
 *             "unity": null,
 *             "unity_type": null
 *         }
 *     ]
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *     "status": 400,
 *     "developerMessage": "registry::developerMessages.registry-101",
 *     "userMessage": "No user found with this code.",
 *     "errorCode": "registry-101",
 *     "moreInfo": "registry.moreInfo.registry-101",
 *     "errors": []
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\UsersController controller() controller
 */
class UserRolesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Roles";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UsersController@roles';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $result = [

             [
                  'role'       => 'donor-expert',
                  'unity'      => 'qKXVA',
                  'unity_type' => 'university',
             ],

             [
                  'role'       => 'opu-manager',
                  'unity'      => 'kxPdx',
                  'unity_type' => 'opu',
             ],

             [
                  'role'       => 'superadmin',
                  'unity'      => null,
                  'unity_type' => null,
             ],
        ];

        return api()->successRespond(
             $result
             , [
             "hashid" => hashid(0),
        ]);
    }
}
