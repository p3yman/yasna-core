<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-get-user
 *                    Get User
 * @apiDescription    Return information of a requested user.
 * @apiVersion        1.0.0
 * @apiName           Get User
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {String} hashid The hashid of the user which must be searched.
 * @apiParam {String} code_melli Code melli of the user which must be searched. This parameter will be used just when
 *                    `hashid` parameter is absent.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": {
 *          "id": "qKXVA",
 *          "full_name": "Sir Programmer",
 *          "code_melli": "3040055437",
 *          "email": "dev@yasnateam.com",
 *          "mobile": null,
 *          "edu_level": null,
 *          "expertise": null,
 *          "job_history": null,
 *          "tel": null,
 *          "gender": 0,
 *          "latest_job_position": null,
 *          "is_retired": 0,
 *          "service_location": null,
 *          "second_job": null,
 *          "birth_date": null,
 *          "unity_roles": {}
 *      }
 * }
 * @apiErrorExample   Unauthorized access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User not found:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Not Found",
 *      "userMessage": "Nothing has been found.",
 *      "errorCode": 404,
 *      "moreInfo": "registry.moreInfo.404",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @see               \Modules\Registry\Http\Controllers\V1\ActionOnUserAccountsControlle::controller()
 */
class GetUserEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Get User";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ActionOnUserAccountsController@getUser';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond(user(1)->toFront());
    }
}
