<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\UsersController;

/**
 * @api                                 {GET}
 *                                      /api/modular/v1/registry-logins
 *                                      Logins History
 * @apiDescription                      Logins History
 * @apiVersion                          1.0.0
 * @apiName                             Logins History
 * @apiGroup                            Registry
 * @apiPermission                       User
 * @apiParam {String=asc,desc}          [sort=desc] the sort type, whether ascending or descending
 * @apiParam {Integer}                  [from] if specified, limits the result to the given beginning of time
 * @apiParam {Integer}                  [to] if specified, limits the result to the given end of time
 * @apiParam {String=all,valid,invalid} [criteria:all] the criteria whether or not to include failed/successful logins
 * @apiParam {Integer}                  [per_page=10] the number of results per page
 * @apiParam {Integer}                  [page=1] the page in which the result is shown
 * @apiSuccessExample                   Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 3,
 *          "count": 3,
 *          "per_page": 15,
 *          "last_page": 1,
 *          "current_page": 1,
 *          "next_page_url": null,
 *          "previous_page_url": null,
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          {
 *              "ip": "127.0.0.1",
 *              "time": 1546956647,
 *              "was_valid": 1
 *          },
 *          {
 *              "ip": "127.0.0.1",
 *              "time": 1546956575,
 *              "was_valid": 0
 *          },
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  UsersController controller()
 */
class LoginsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Logins History";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UsersController@loginsHistory';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "ip"        => "127.0.0.1",
                  "time"      => 1546956647,
                  "was_valid" => 1,
             ],
             [
                  "ip"        => "127.0.0.1",
                  "time"      => 1546956575,
                  "was_valid" => 0,
             ],
             [
                  "ip"        => "127.0.0.1",
                  "time"      => 1546955777,
                  "was_valid" => 0,
             ],
        ], [
             "total"              => 3,
             "count"              => 3,
             "per_page"           => 15,
             "last_page"          => 1,
             "current_page"       => 1,
             "next_page_url"      => null,
             "previous_page_url"  => null,
             "authenticated_user" => "qKXVA",
        ]);
    }
}
