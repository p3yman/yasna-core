<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {PUT}
 *                    /api/modular/v1/registry-user-deactivate
 *                    User Deactivation
 * @apiDescription    Activate/Deactivate a user
 * @apiVersion        1.0.0
 * @apiName           User Deactivation
 * @apiGroup          Registry
 * @apiPermission     SuperAdmin
 * @apiParam {hashid}   Hashid of user which must deactivated/activated
 * @apiParam {Integer=0,1} [restore=0] Indicate that it's an restore request or not (`0` will deactivate a given user
 *           and `1` will active it.)
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "RNplN",
 *          "type": null,
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\ActionOnUserAccountsController controller()
 */
class UserDeactivateEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Deactivation";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_PUT;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ActionOnUserAccountsController@deactivate';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "type"   => request()->type,
             "hashid" => request()->hashid,
        ], [
             "done" => "1",
        ]);
    }
}
