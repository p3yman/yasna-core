<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\UnityController;

/**
 * @api               {POST}
 *                    /api/modular/v1/registry-role-assign
 *                    Role Assignment
 * @apiDescription    General Role Assignment Endpoint
 * @apiVersion        1.0.0
 * @apiName           Role Assignment
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {string} unity the unity hashid in which the new role is going to be assigned to. (required)
 * @apiParam {string} type  the unity type in which the new role is going to be assigned to. (required)
 * @apiParam {string} role  the role slug in question. (required)
 * @apiParam {string} [user]  the hashid of the user in question.
 * @apiParam  {string} [code_melli] the code_melli of the user in question
 * @apiParam  {string} [name_title] the title (like Dr.) of the user in question
 * @apiParam  {string} [name_first] the first name of the user in question
 * @apiParam  {string} [name_last] the last name of the user in question
 * @apiParam  {string} [mobile] the mobile number of the user in question
 * @apiParam  {int:1-3}[gender] the gender of the user in question (1:male, 2:female, 3:unknown)
 * @apiParam  {string} [edu_field] the edu_field of the user in question
 * @apiParam  {int=0,1}[is_retired] the retirement status of the user in question (default=0)
 * @apiParam  {string} [birth_date] the birth_date of the user in question
 * @apiParam  {string} [latest_job_position] the last job of the user in question
 * @apiParam  {string} [tel] the telephone number of the user in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "user": "qKXVA",
 *          "role": "deputy",
 *          "unity": "QKgJx",
 *          "type": "university"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": "422",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": [
 *          "unity": [
 *              "registry::validation.unity_not_found"
 *          ]
 *      ]
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class RoleAssignEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Role Assignment";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        $controller = studly_case(request()->type) . "Controller";

        return "$controller@assign";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "user"  => "jafar",
             "role"  => "folan",
             "unity" => "bisar",
             "type"  => "university",
        ]);
    }
}
