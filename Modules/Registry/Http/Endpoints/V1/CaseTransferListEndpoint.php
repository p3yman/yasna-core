<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

use function time;
use function dummy;
use function sprintf;
use function array_random;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-case-transfer-list
 *                    Case Transfer List
 * @apiDescription    Case Transfer List
 * @apiVersion        1.0.1
 * @apiName           Case Transfer List
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {String} hashid  Hashid of case who is requested for transfer records
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 1,
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": [
 *          {
 *              "id": "qKXVA",
 *              "case": "QKgJx",
 *              "case_name": "Ehsan Damala",
 *              "status": "pending",
 *              "city": "xrEZA",
 *              "city_title": "Bokan",
 *              "province": "QKgJx",
 *              "province_title": "Wes Azerbaijan",
 *              "hospital": "KeMmK",
 *              "hospital_title": "Beauty Hospital",
 *              "origin_hospital": "NqVoA",
 *              "origin_hospital_title": "Paradise Hospital",
 *              "transfer_reason": 1,
 *              "dismiss_transfer_reason": null,
 *              "transfer_next_action": [],
 *              "case_final_state": null,
 *              "transferred_at": 1548749536,
 *              "created_at": 1548750537
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\CaseController controller()
 */
class CaseTransferListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Case Transfer List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CaseController@listPatientTransfers';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $reason               = '';
            $dismiss_reason       = '';
            $transfer_next_action = [];

            $god_choice = rand(0, 1);
            if ($god_choice) {
                $reason = rand(1, 5);
            } else {
                $dismiss_reason       = rand(1, 6);
                $transfer_next_action = array_random(range(1, 3), rand(1, 3));
            }

            $data[] = [
                 "id"                      => hashid(rand(1, 50)),
                 "case"                    => hashid(rand(1, 50)),
                 "case_name"               => dummy()::persianName() . dummy()::persianFamily(),
                 "status"                  => array_random(["pending", "confirmed", "dismissed"]),
                 "city"                    => hashid(rand(1, 50)),
                 "city_title"              => dummy()::persianName(),
                 "hospital"                => hashid(rand(1, 50)),
                 "hospital_title"          => sprintf("بیمارستان%s", dummy()::persianWord(2)),
                 "origin_hospital"         => hashid(rand(1, 50)),
                 "origin_hospital_title"   => sprintf("بیمارستان%s", dummy()::persianWord(2)),
                 "province"                => hashid(rand(1, 50)),
                 "province_title"          => dummy()::persianName(),
                 "transfer_reason"         => $reason,
                 "transferred_at"          => rand(time() - 50000, time()),
                 "created_at"              => rand(time() - 100000, time() - 30000),
                 "dismiss_transfer_reason" => $dismiss_reason,
                 "transfer_next_action"    => $transfer_next_action,
                 "case_final_state"        => '',
            ];
        }

        return api()->successRespond([
             $data,
        ], [
             "reached" => "CaseTransferListEndpoint",
             "total"   => count($data),
        ]);
    }
}
