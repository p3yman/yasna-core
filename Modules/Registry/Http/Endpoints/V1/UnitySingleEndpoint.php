<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/V1/registry-unity-single
 *                    Unity Single
 * @apiDescription    an endpoint for the single view of all the entities
 * @apiVersion        1.0.0
 * @apiName           Unity Single
 * @apiGroup          Registry
 * @apiPermission     user (accurate check will be performed later)
 * @apiParam {string} [hashid] the hashid of the unity in question
 * @apiParam {string} [type] the type of the unity in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "folan",
 *          "type":"university",
 *      },
 *      "results": {
 *          "id": "JX84l",
 *          "name": " Katharine done. Lucius",
 *          "president": " begun",
 *          "deputy": " desire,",
 *          "expert-in-charge": " Cedric",
 *          "opu_count": "10",
 *          "hospital_count": "20",
 *          "coordinator_count": "30",
 *          "donor_count": "40",
 *          "inspector_count": "50",
 *          "seconder_count": "0",
 *          "created_at": {
 *          "date": "2018-11-28 17:15:27.381961",
 *          "timezone_type": 3,
 *          "timezone": "Asia/Tehran"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class UnitySingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Unity Single";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        $controller = studly_case(request()->type) . "Controller";

        return "$controller@single";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "type"   => request()->type,
             "hashid" => request()->hashid,
        ], [
             $this->mockModel(),
        ]);
    }



    /**
     * get a mock model
     *
     * @return array
     */
    protected function mockModel()
    {
        return [
             "id"                => str_random(5),
             "name"              => dummy()::persianWord(3),
             "president"         => dummy()::persianName(),
             "deputy"            => dummy()::persianName(),
             "expert-in-charge"  => dummy()::persianName(),
             "opu_count"         => "10",
             "hospital_count"    => "20",
             "coordinator_count" => "30",
             "donor_count"       => "40",
             "inspector_count"   => "50",
             "seconder_count"    => "0",
             "created_at"        => now(),
        ];
    }

}
