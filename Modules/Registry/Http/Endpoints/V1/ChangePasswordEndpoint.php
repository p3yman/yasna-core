<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/registry-change-password
 *                    Change password
 * @apiDescription    Change password of the current logged-in account. After the changing process, the token will be
 *                    invalided and a new token will be returned.
 * @apiVersion        1.0.0
 * @apiName           Change password
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {String} current_password Current password of the user.
 * @apiParam {String} password The new password which must be set on the account.
 * @apiParam {String} password_confirmation The confirmation of the new password.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": {
 *          "access_token":
 *          "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95Y21zLm1wZ1wvYXBpXC92MVwvcmVnaXN0cnktY2hhbmdlLXBhc3N3b3JkIiwiaWF0IjoxNTQzMjUyNDU3LCJleHAiOjE1NDY4NTI0NTcsIm5iZiI6MTU0MzI1MjQ1NywianRpIjoibmFSWWtCekptdHFISWs3byIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.DjaE9TX3jVmrqPiLYK77ZvfMUdEQS9ZaSVTheDaead8",
 *          "token_type": "bearer",
 *          "expires_in": 3600000
 *      }
 * }
 * @apiErrorExample   Unauthenticated-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   Not Given Params Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "registry::developerMessages.1002",
 *      "userMessage": "registry::userMessages.1002",
 *      "errorCode": 1002,
 *      "moreInfo": "registry.moreInfo.1002",
 *      "errors": {
 *          "current_password": [
 *              [
 *                  "وارد کردن «گذرواژه فعلی» الزامی است."
 *              ]
 *          ],
 *          "password": [
 *              [
 *                  "وارد کردن «گذرواژه» الزامی است."
 *              ]
 *          ]
 *      }
 * }
 * @apiErrorExample   Error on Not Matched Confirmation:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "registry::developerMessages.1002",
 *      "userMessage": "registry::userMessages.1002",
 *      "errorCode": 1002,
 *      "moreInfo": "registry.moreInfo.1002",
 *      "errors": {
 *          "password": [
 *              [
 *                  "گذرواژه با تاییدیه مطابقت ندارد."
 *              ]
 *          ]
 *      }
 * }
 * @apiErrorExample   Error on Not Wrong Current Password:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "registry::developerMessages.1002",
 *      "userMessage": "registry::userMessages.1002",
 *      "errorCode": 1002,
 *      "moreInfo": "registry.moreInfo.1002",
 *      "errors": {
 *          "current_password": [
 *              [
 *                  "گذرواژه\u200cی قبلی ارائه شده معتبر نمی\u200cباشد."
 *              ]
 *      ]
 * }
 */
class ChangePasswordEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Change Password";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ActionOnUserAccountsController@resetMyPassword';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "access_token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC95Y21zLm1wZ1wvYXBpXC92MVwvcmVnaXN0cnktY2hhbmdlLXBhc3N3b3JkIiwiaWF0IjoxNTQzMjUyNDU3LCJleHAiOjE1NDY4NTI0NTcsIm5iZiI6MTU0MzI1MjQ1NywianRpIjoibmFSWWtCekptdHFISWs3byIsInN1YiI6MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.DjaE9TX3jVmrqPiLYK77ZvfMUdEQS9ZaSVTheDaead8",
             "token_type"   => "bearer",
             "expires_in"   => 3600000,
        ]);
    }
}
