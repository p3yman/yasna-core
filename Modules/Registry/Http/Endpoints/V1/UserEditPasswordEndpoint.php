<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/registry-user-edit-password
 *                    User Edit Password
 * @apiDescription    Password reset option for the users under the current authenticated user management
 * @apiVersion        1.0.0
 * @apiName           User Edit Password
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {String} user Hashid of user which his/her password must be reset
 * @apiParam {String} password The new password which must be set on user account
 * @apiParam {String} password_confirmation Confirmation of `password` field
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "results": {
 *          "done": 1
 *      }
 * }
 * @apiErrorExample   Unauthenticated Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\ActionOnUserAccountsController controller()
 */
class UserEditPasswordEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Edit Password";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'ActionOnUserAccountsController@resetPasswordByAuthorizedUser';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ]);
    }
}
