<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\UniversityController;

/**
 * @api               {GET}
 *                    /api/modular/V1/registry-university-list
 *                    University List
 * @apiDescription    lists the universities, based on the user role and given arguments
 * @apiVersion        1.0.0
 * @apiName           University List
 * @apiGroup          Registry
 * @apiPermission     Superadmin
 * @apiParam {String} [name] The name which must be searched.
 * @apiParam {String} [city] The hashid of the city to be filtered against.
 * @apiParam {String} [province] The hashid of the province to be filtered against.
 * @apiParam {String} [paginated=0,1] Indicate that the format of the result. 1 is for paginated result.
 * @apiParam {String} [per_page=15] Indicate the number of results on per page. (available only with `paginated=1`).
 * @apiParam {String} [page=1] Page number of returned result (available only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          total: 100
 *      },
 *      "results": {
 *          id": "JX84l",
 *          "name": " Katharine done. Lucius",
 *          "president": " begun",
 *          "deputy": " desire,",
 *          "expert-in-charge": " Cedric",
 *          "opu_count": "10",
 *          "hospital_count": "20",
 *          "coordinator_count": "30",
 *          "donor_count": "40",
 *          "inspector_count": "50",
 *          "seconder_count": "0",
 *          "created_at": {
 *          "date": "2018-11-28 17:15:27.381961",
 *          "timezone_type": 3,
 *          "timezone": "Asia/Tehran"
 *      }
 * }
 * @apiErrorExample   Unauthenticated-Request:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  UniversityController controller()
 */
class UniversityListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "University List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UniversityController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        if (request()->paginated) {
            return api()->successRespond(
                 [
                      $this->mockModel(),
                      $this->mockModel(),
                      $this->mockModel(),
                      $this->mockModel(),
                 ],
                 [
                      "total"             => "110",
                      "per_page"          => "5",
                      "last_page"         => "22",
                      "current_page"      => "1",
                      "next_page_url"     => url("api/v1/registry-university-list?page=2"),
                      "previous_page_url" => url("api/v1/registry-university-list?page=22"),
                 ]
            );
        }

        return api()->successRespond(
             [
                  $this->mockModel(),
                  $this->mockModel(),
                  $this->mockModel(),
                  $this->mockModel(),
             ],
             [
                  "total" => "5",
             ]
        );
    }



    /**
     * get a mock model
     *
     * @return array
     */
    protected function mockModel()
    {
        return [
             "id"                => str_random(5),
             "name"              => dummy()::persianWord(3),
             "president"         => dummy()::persianName(),
             "deputy"            => dummy()::persianName(),
             "expert-in-charge"  => dummy()::persianName(),
             "opu_count"         => "10",
             "hospital_count"    => "20",
             "coordinator_count" => "30",
             "donor_count"       => "40",
             "inspector_count"   => "50",
             "seconder_count"    => "0",
             "created_at"        => now(),
        ];
    }
}
