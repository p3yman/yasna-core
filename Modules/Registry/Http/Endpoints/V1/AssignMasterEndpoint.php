<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\UsersController;

/**
 * @api               {PUT}
 *                    /api/modular/v1/registry-assign-master
 *                    Assign Master
 * @apiDescription    An endpoint to assign a superadmin as the master (who can assign other superadmins)
 * @apiVersion        1.0.0
 * @apiName           Assign Master
 * @apiGroup          Registry
 * @apiPermission     developer
 * @apiParam {String} user hashid of the user in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *         "hashid": "nAyBx"
 *      },
 *      "results": {
 *         "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  UsersController controller()
 */
class AssignMasterEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Assign Master";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_PUT;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UsersController@assignMaster';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "reached" => "AssignMasterEndpoint",
        ]);
    }
}
