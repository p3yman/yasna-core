<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\SuperadminsController;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/registry-superadmin-delete
 *                    Superadmin Delete
 * @apiDescription    Superadmin Delete
 * @apiVersion        1.0.0
 * @apiName           Superadmin Delete
 * @apiGroup          Registry
 * @apiPermission     Master
 * @apiParam {string} user the hashid of the user in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "done": 1,
 *      },
 *      "results": {
 *          "deleted":"jsdf",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method SuperadminsController controller()
 */
class SuperadminDeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Superadmin Delete";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev() or user()->hasRole(["master", "superadmin"]);
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SuperadminsController@delete';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "deleted" => hashid(0),
        ]);
    }
}
