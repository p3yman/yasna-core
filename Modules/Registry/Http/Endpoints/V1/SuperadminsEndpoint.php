<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\SuperadminsController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-superadmins
 *                    List of Superadmins
 * @apiDescription    List of Superadmins
 * @apiVersion        1.0.0
 * @apiName           List of Superadmins
 * @apiGroup          Registry
 * @apiPermission     Master
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 1,
 *      },
 *      "results": [
 *          {
 *              "id": "PNOLA",
 *              "name_first": "Taha",
 *              "name_last": "Kamkar",
 *              "gender": 1,
 *              "mobile": "09122835030",
 *              "code_melli": "1234567890",
 *              "is_master":false,
 *          }
 *      ]
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  SuperadminsController controller()
 */
class SuperadminsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List of Superadmins";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev() or user()->hasRole(["master", "superadmin"]);
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SuperadminsController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "total" => "1",
        ], [
             [
                  "id"         => "PNOLA",
                  "name_first" => "Taha",
                  "name_last"  => "Kamkar",
                  "mobile"     => "09122835030",
                  "gender"     => "1",
                  "code_melli" => "1234567890",
                  "is_master"  => true,
             ],
        ]);
    }
}
