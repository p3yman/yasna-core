<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-opu-list
 *                    Opu List
 * @apiDescription    List all OPUs for a expert in charge or donor expert
 * @apiVersion        1.0.2
 * @apiName           Opu List
 * @apiGroup          Registry
 * @apiPermission     ExpertInCharge,DonorExpert
 * @apiParam {String}      [university]  The hashid of a university
 *                                       which the client is ExpertInCharge or DonorExpert in it
 * @apiParam {String}      [name]        Name of hospital
 * @apiParam {String}      [city]        Hashid of city
 * @apiParam {String}      [province]    Hashid of province
 * @apiParam {String} [university] Hashid of university
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer}     [page=1]      Specifies the page number of results (available only with `paginated=1`).
 * @apiParam {Integer}     [per_page=15] Specifies the number of results on each page (available only with
 *           `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "status": 200,
 *     "metadata": {
 *         "total": 1,
 *         "per_page": 15,
 *         "last_page": 1,
 *         "current_page": 1,
 *         "next_page_url": null,
 *         "previous_page_url": null
 *     },
 *     "results": [
 *         {
 *             "id": "kxPdx",
 *             "name": "OPU Name",
 *             "opu_type": "procurement",
 *             "telephone_detection": true,
 *             "clinical_detection": true,
 *             "dedicated_icu_beds": "100",
 *             "marked_icu_beds": "70",
 *             "population": "1000",
 *             "pmp": "200",
 *             "university": {
 *                 "id": "qKXVA",
 *                 "type": "university",
 *                 "name": "University Name"
 *             },
 *             "service_location": {
 *                 "id": "QKgJx",
 *                 "name": "Hospital Name",
 *                 "city": "AXDdA",
 *                 "city_title": "Tehran",
 *                 "province": "kxPdx",
 *                 "province_title": "Tehran"
 *             },
 *             "phones": "09121234567",
 *             "chief": {
 *                 "id": "QKgJx",
 *                 "name_title": null,
 *                 "name_first": "Jane",
 *                 "name_last": "Doe"
 *             }
 *         }
 *     ]
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\OpuController controller()
 */
class OpuListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Opu List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'OpuController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             0 =>
                  [
                       'id'                  => 'kxPdx',
                       'name'                => 'OPU Name',
                       'opu_type'            => 'procurement',
                       'telephone_detection' => true,
                       'clinical_detection'  => true,
                       'dedicated_icu_beds'  => '100',
                       'marked_icu_beds'     => '70',
                       'population'          => '1000',
                       'pmp'                 => '200',
                       'university'          =>
                            [
                                 'id'   => 'qKXVA',
                                 'type' => 'university',
                                 'name' => 'University Name',
                            ],
                       'service_location'    =>
                            [
                                 'id'             => 'QKgJx',
                                 'name'           => 'Hospital Name',
                                 'city'           => 'AXDdA',
                                 'city_title'     => 'Tehran',
                                 'province'       => 'kxPdx',
                                 'province_title' => 'Tehran',
                            ],
                       'phones'              => '09121234567',
                       'chief'               =>
                            [
                                 'id'         => 'QKgJx',
                                 'name_title' => null,
                                 'name_first' => 'Jane',
                                 'name_last'  => 'Doe',
                            ],
                  ],
        ], [
             'total'             => 1,
             'per_page'          => 15,
             'last_page'         => 1,
             'current_page'      => 1,
             'next_page_url'     => null,
             'previous_page_url' => null,
        ]);
    }
}
