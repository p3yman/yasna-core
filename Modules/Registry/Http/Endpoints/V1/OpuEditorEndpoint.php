<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/V1/registry-opu-editor
 *                    Opu Editor
 * @apiDescription    An Endpoint to Create or Edit OPUs
 * @apiVersion        1.0.2
 * @apiName           Opu Editor
 * @apiGroup          Registry
 * @apiPermission     ExpertInCharge, DonorExpert
 * @apiParam {String}  [hashid]            The Hashid of the OPU. If presented, an OPU with that hashid will be
 *                                         updated. If omitted, a new OPU will be created.
 * @apiParam {String}  university_id       The Hashid of the University
 * @apiParam {String}  name                The Name of the OPU
 * @apiParam {String}  phones              The phones of the OPU in the coma separated format.
 * @apiParam {String}  service_location    The hashid of a hospital in the same university.
 *                                         (A hospital can host only one OPU at the same time.)
 * @apiParam {String=detection,procurement}  opu_type            One of the `detection` and `procurement` values.
 * @apiParam {Boolean} telephone_detection If the OPU has telephone detection.
 * @apiParam {Boolean} clinical_detection  If the OPU has clinical detection.
 * @apiParam {Integer} dedicated_icu_beds  The number of the dedicated ICU beds.
 * @apiParam {Integer} marked_icu_beds     The number of the ICU marked beds.
 * @apiParam {Integer} population          An integer which shows the under control population.
 * @apiParam {Integer} pmp                 An integer which shows PMP.
 * @apiParam {String[]} hospitals          List of hospitals which must be attached to the OPU. Each item is a hashid
 *           of a hospital. If a hospital is attched to an OPU and its hashid has been removed from this array, the
 *           hospital will be detached from the OPU.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "status": 200,
 *     "metadata": {
 *         "hashid": "nAyBx"
 *     },
 *     "results": {
 *         "done": "1"
 *     }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class OpuEditorEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Opu Editor";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'OpuController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
