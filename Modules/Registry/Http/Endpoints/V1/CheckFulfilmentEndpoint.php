<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\FulfilmentController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-check-fulfilment
 *                    Check Fulfilment
 * @apiDescription    Check the obligatory data fulfilment the current user has to carry out
 * @apiVersion        1.0.0
 * @apiName           Check Fulfilment
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total_checks": 19,
 *          "total_passed": 17,
 *          "total_failed": 2,
 *          authenticated_user": "nAyBx"
 *      },
 *      "results": {
 *         "expert_university_settings": false,
 *         "expert_university_president": true,
 *         "expert_university_deputy": true,
 *         "expert_subs": true,
 *         "expert_one_neurologist_seconder": true,
 *         "expert_one_neurosurgeons_seconder": true,
 *         "expert_one_internist_seconder": true,
 *         "expert_one_anesthesiologist_seconder": true,
 *         "expert_one_opu": true,
 *         "expert_one_opu_manager": true,
 *         "expert_one_hospital": true,
 *         "expert_one_ward": true,
 *         "opu_settings": false,
 *         "opu_one_consultant": true,
 *         "opu_one_legal_medicine": true,
 *         "opu_one_chief_legal_medicine": true,
 *         "opu_one_coordinator": true,
 *         "opu_one_chief_coordinator": true,
 *         "opu_one_inspector": true
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method FulfilmentController controller()
 */
class CheckFulfilmentEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Check Fulfilment";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'FulfilmentController@check';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "super_register_one_university"        => true,
             "super_register_one_expert"            => true,
             "expert_university_settings"           => true,
             "expert_university_president"          => true,
             "expert_university_deputy"             => true,
             "expert_subs"                          => false,
             "expert_one_neurologist_seconder"      => true,
             "expert_one_neurosurgeons_seconder"    => true,
             "expert_one_internist_seconder"        => true,
             "expert_one_anesthesiologist_seconder" => true,
             "expert_one_opu"                       => true,
             "expert_one_opu_manager"               => true,
             "expert_one_hospital"                  => false,
             "expert_one_ward"                      => true,
             "opu_settings"                         => true,
             "opu_one_consultant"                   => true,
             "opu_one_legal_medicine"               => true,
             "opu_one_chief_legal_medicine"         => true,
             "opu_one_coordinator"                  => false,
             "opu_one_chief_coordinator"            => true,
             "opu_one_inspector"                    => true,
        ], [
             "total_checks"       => 19,
             "total_passed"       => 16,
             "total_failed"       => 3,
             "authenticated_user" => "folan",
        ]);
    }
}
