<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\SuperadminsController;

/**
 * @api               {POST}
 *                    /api/modular/v1/registry-superadmin-save
 *                    Superadmin Save
 * @apiDescription    Save (Create / Update) a Superadmin Data
 * @apiVersion        1.0.0
 * @apiName           Save (Create / Update) a Superadmin Data
 * @apiGroup          Registry
 * @apiPermission     Master
 * @apiParam {string} id          the hashid of the user in question
 * @apiParam {string} name_first  the first name of the user in question
 * @apiParam {string} name_last   the first name of the user in question
 * @apiParam {string} code_melli  the code_melli of the user in question
 * @apiParam {string} gender      the gender of the user in question
 * @apiParam {string} mobile      the mobile number of the user in question
 * @apiParam {string} password    the (new) password of the user in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "status": 200,
 *     "metadata": {
 *         "hashid": "nAyBx"
 *     },
 *     "results": {
 *         "done": "1"
 *     }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method SuperadminsController controller()
 */
class SuperadminSaveEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Save Superadmin";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return dev() or user()->hasRole(["master", "superadmin"]);
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SuperadminsController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(0),
        ]);
    }
}
