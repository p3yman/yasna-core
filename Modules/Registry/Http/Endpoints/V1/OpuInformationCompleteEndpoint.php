<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api                {POST}
 *                     /api/modular/V1/registry-opu-profile-save
 *                     Opu Information Complete
 * @apiDescription     This endpoint can be used for saving the profile of an OPU by the OPU manager
 * @apiVersion         1.0.0
 * @apiName            Opu Information Complete
 * @apiGroup           Registry
 * @apiPermission      User
 * @apiParam {string}  hashid              The hashid of the OPU to be updated.
 * @apiParam {boolean} portable_monitor    If the OPU has portable monitor.
 * @apiParam {boolean} portable_ventilator If the OPU has portable ventilator.
 * @apiParam {boolean} portable_eeg        If the OPU has portable EEG.
 * @apiParam {boolean} vessels_angiography If the OPU has portable cerebral angiography.
 * @apiParam {boolean} isotope_scan        If the OPU has portable brain isotope.
 * @apiParam {boolean} organ_harvesting    If the OPU has portable organ harvesting.
 *                                         Needed for procurement OPUs.
 * @apiParam {array}   harvestable_organs  The list of organs and their information.
 *                                         Information of each organ should contain:
 * * `organ` the slug of the organ
 * * `harvester` the hashid of the transplant surgeon user
 * * `harvester_city` the hashid of the city of the transplant surgeon
 * @apiSuccessExample  Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "status": 200,
 *     "metadata": {
 *         "hashid": "PNOLA"
 *     },
 *     "results": {
 *         "done": "1"
 *     }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *     "status": 400,
 *     "developerMessage": "Unprocessable Entity",
 *     "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *     "errorCode": 422,
 *     "moreInfo": "registry.moreInfo.422",
 *     "errors": {
 *         "portable_eeg": [
 *             [
 *                 "The Portable EEG field is required."
 *             ]
 *         ]
 *     }
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class OpuInformationCompleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Opu Information Complete";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'OpuController@informationComplete';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => "PKEnN",
        ]);
    }
}
