<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\CaseController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-similar-cases
 *                    check for similar cases
 * @apiDescription    get the similar cases by code_melli, name_first and name_last
 * @apiVersion        1.0.0
 * @apiName           check for similar cases
 * @apiGroup          Registry
 * @apiPermission     user
 * @apiParam {string} [name_first] the first name of the candidate
 * @apiParam {string} [name_last] the lsat name of the candidate
 * @apiParam {string} [code_melli] the `code_melli` of the candidate
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "code_melli": 0072224232,
 *          "name_first": "Taha",
 *          "name_last": "Jafar",
 *          "total": 1
 *      }
 *      "results": [
 *          {
 *              "id": "gxnDN",
 *              "file_no": "dtoO1J27zO",
 *              "code_melli": "2126222355",
 *              "name_first": "Taha",
 *              "name_last": "Jafar",
 *              "province": "Tehran",
 *              "city": "Tehran",
 *              "university": "Folan University",
 *              "opu": "Folan Opu",
 *              "hospital": "Folan Hospital",
 *              "ward": "Folan Ward"
 *          }
 *      ]
 * }
 * @method CaseController controller()
 */
class SimilarCasesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "check for similar cases";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CaseController@similarCases';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             [
                  "id"         => 'asdasd',
                  "file_no"    => 'asdasda',
                  "code_melli" => request()->code_melli,
                  "name_first" => request()->name_first,
                  "name_last"  => request()->name_last,
                  "province"   => 'Tehran',
                  "city"       => 'Tehran',
                  "university" => 'Folan University',
                  "opu"        => 'Folan Opu',
                  "hospital"   => 'Folan Hospital',
                  "ward"       => 'Folan Ward',
             ],
        ], [
             "code_melli" => request()->code_melli,
             "name_first" => request()->name_first,
             "name_last"  => request()->name_last,
             "total"      => 1,
        ]);
    }
}
