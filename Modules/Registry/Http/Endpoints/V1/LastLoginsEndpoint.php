<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-last-logins
 *                    Last Logins Records
 * @apiDescription    Get `x` last login information of current user
 * @apiVersion        1.0.0
 * @apiName           Last Login
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {Integer} [counts=3] Number of records to return. The maximum amount is `10`.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": null,
 *      "results": [
 *          {
 *              "ip": "127.0.0.1",
 *              "time": 1543924293
 *          },
 *          {
 *              "ip": "127.0.0.1",
 *              "time": 1543924322
 *          },
 *          {
 *              "ip": "127.0.0.1",
 *              "time": 1543924330
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\UsersController controller()
 */
class LastLoginsEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Last Login";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UsersController@lastLogins';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "ip"   => "127.0.0.1",
             "time" => 1543924293,
        ]);
    }
}
