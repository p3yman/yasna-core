<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-assignee-list
 *                    Role List
 * @apiDescription    General Assignee List Endpoint
 * @apiVersion        1.0.1
 * @apiName           Role List
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {String} unity the unity hashid in which the list of assigned roles of it must be returned
 * @apiParam {String} type  the unity type in which the list of assigned roles of it must be showed
 * @apiParam {String} role  the role slug in question.
 * @apiParam {Integer=0,1} [history=0] Show the history of assignments of a given role on a unity
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 1
 *      },
 *      "results": [
 *          {
 *              "id": "oxljN",
 *              "full_name": "Dr. Arash Naraghi",
 *              "code_melli": "5023624522",
 *              "email": "arash@naraghi.com",
 *              "mobile": "09123456789",
 *              "edu_level": 5,
 *              "expertise": "Top",
 *              "job_history": "Presenter",
 *              "tel": null,
 *              "gender": 1,
 *              "latest_job_position": null,
 *              "is_retired": 0,
 *              "service_location": null,
 *              "second_job": null,
 *              "birth_date": null,
 *              "unity_roles": {"expert-in-charge"}
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class AssigneeListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Role List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        $controller = studly_case(request()->type) . "Controller";
        $role       = request()->role;

        if(is_array($role)) {
            return "$controller@multiRoleAssignees";
        }

        return "$controller@assignees";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"                  => "oxljN",
             "full_name"           => dummy()::persianName(),
             "code_melli"          => "5023624522",
             "email"               => sprintf("%s@%s.com", dummy()::englishWord(), dummy()::englishWord()),
             "mobile"              => "09139946922",
             "edu_level"           => 5,
             "expertise"           => dummy()::englishWord(),
             "job_history"         => dummy()::englishWord(),
             "tel"                 => null,
             "gender"              => 1,
             "latest_job_position" => null,
             "is_retired"          => 0,
             "service_location"    => null,
             "second_job"          => null,
             "birth_date"          => null,
             "unity_roles"         => ["expert-in-charge"],
        ]);
    }
}
