<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-hospital-list
 *                    Hospital List
 * @apiDescription    List all hospitals for a expert in charge or donor expert
 * @apiVersion        1.0.1
 * @apiName           Hospital List
 * @apiGroup          Registry
 * @apiPermission     ExpertInCharge,DonorExpert,SuperAdmin
 * @apiParam {String} [name] Name of hospital
 * @apiParam {String} [city] Hashid of city
 * @apiParam {String} [university] Hashid of university
 * @apiParam {String} [opu] Hashid of opu
 * @apiParam {String} [province] Hashid of province
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer} [page=1] Specifies the page number of results (available only with `paginated=1`).
 * @apiParam {Integer} [per_page=15] Specifies the number of results on each page (available only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 1,
 *          "per_page": 15,
 *          "last_page": 1,
 *          "current_page": 1,
 *          "next_page_url": null,
 *          "previous_page_url": null
 *      },
 *      "results": [
 *          {
 *              "id": "PNOLA",
 *              "name": "Masih",
 *              "city": "AXDdA",
 *              "city_title": "Tehran",
 *              "province": "kxPdx",
 *              "province_hashid": "Tehran",
 *              "phones": "21565236",
 *              "sector": "private",
 *              "neurosurgery_services": false,
 *              "neurological_services": true,
 *              "trauma_center": true,
 *              "bed_counts": 25,
 *              "death_counts": [
 *                  {
 *                      "year": 98,
 *                      "counts": 352
 *                  },
 *                  {
 *                      "year": 97,
 *                      "counts": 250
 *                  },
 *                  {
 *                      "year": 96,
 *                      "counts": 111
 *                  }
 *              ]
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\HospitalController controller()
 */
class HospitalListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Hospital List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'HospitalController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "id"                    => "PNOLA",
             "name"                  => dummy()::persianName(),
             "city"                  => "AXDdA",
             "city_title"            => dummy()::persianName(),
             "province"              => "kxPdx",
             "province_hashid"       => dummy()::persianName(),
             "phones"                => "21565236",
             "sector"                => "private",
             "neurosurgery_services" => false,
             "neurological_services" => true,
             "trauma_center"         => true,
             "bed_counts"            => 25,
             "death_counts"          => [
                  [
                       "year"   => 98,
                       "counts" => 352,
                  ],
                  [
                       "year"   => 97,
                       "counts" => 250,
                  ],
                  [
                       "year"   => 96,
                       "counts" => 111,
                  ],
             ],
        ]);
    }
}
