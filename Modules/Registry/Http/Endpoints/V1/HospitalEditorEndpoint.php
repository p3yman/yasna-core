<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\HospitalController;

/**
 * @api               {POST}
 *                    /api/modular/v1/registry-hospital-editor
 *                    Hospital Editor
 * @apiDescription    Create or Update A Hospital.
 * @apiVersion        1.0.1
 * @apiName           Hospital Editor
 * @apiGroup          Registry
 * @apiPermission     ExpertInCharge,DonorExpert
 * @apiParam {opu}    opu Hashid of the opu (required only if not filled already)
 * @apiParam {String} hashid Hashid of a hospital which might be updated. If no hashid given, the hospital will be
 *           supposed as a new hospital unity.
 * @apiParam {String} name Name of the hospital.
 * @apiParam {String} city Hashid of the city.
 * @apiParam {String} university Hashid of the university.
 * @apiParam {String} address Address of the hospital.
 * @apiParam {String} postal_code Postal code of the hospital.
 * @apiParam {String} phones Phone of the hospital.
 * @apiParam {String} website Website of the hospital.
 * @apiParam {String} sector_type Type of the hospital, selected from a list of combo: hospital-sector.
 * @apiParam {String} sector_other other types of sector not included in the above combo.
 * @apiParam {Boolean=0,1} neurosurgery_services Indicate the hospital has neurosurgery services (`1`) or doesn't
 *           have (`0`).
 * @apiParam {Boolean=0,1} neurological_services Indicate the hospital has neurological services (`1`) or doesn't have
 *           (`0`).
 * @apiParam {Boolean=0,1} trauma_center Indicate the hospital is a trauma center (`1`) or not (`0`).
 * @apiParam {Float} opu_distance The OPU distance in KM.
 * @apiParam {Integer} bed_counts Number of beds.
 * @apiParam {Array} death_counts Array of death counts. Each element of this array is an object with `year` and
 *           `counts` as keys and their corresponded values (i.e. `[{year: 67, counts: 20000}]`.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "PNOLA"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample   Unauthorized access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method HospitalController controller()
 */
class HospitalEditorEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Hospital Editor";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return user()->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'HospitalController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => true,
        ], [
             "hashid" => "PNOLA",
        ]);
    }
}
