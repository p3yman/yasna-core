<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\UnityController;

/**
 * @api               {DELETE}
 *                    /api/modular/v1/registry-role-revoke
 *                    Role Revoke
 * @apiDescription    General Role Revoking Endpoint
 * @apiVersion        1.0.0
 * @apiName           Role Revoke
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {string} unity the unity hashid in which the new role is going to be assigned to. (required)
 * @apiParam {string} type  the unity type in which the new role is going to be assigned to. (required)
 * @apiParam {string} role  the role slug in question. (required)
 * @apiParam {string} user  the hashid of the user in question.
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "user": "qKXVA",
 *          "role": "deputy",
 *          "unity": "QKgJx",
 *          "type": "university"
 *      },
 *      "results": {
 *          "done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "Unprocessable Entity",
 *      "userMessage": "The request was well-formed but was unable to be followed due to semantic errors.",
 *      "errorCode": "422",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": [
 *          "unity": [
 *              "registry::validation.unity_not_found"
 *          ]
 *      ]
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class RoleRevokeEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Role Revoke";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        $controller = studly_case(request()->type) . "Controller";

        return "$controller@revoke";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "user"  => "jafar",
             "role"  => "folan",
             "unity" => "bisar",
             "type"  => "university",
        ]);
    }
}
