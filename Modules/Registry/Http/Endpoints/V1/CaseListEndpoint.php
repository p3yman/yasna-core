<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-case-list
 *                    List of cases
 * @apiDescription    Get list of cases
 * @apiVersion        1.0.3
 * @apiName           List of cases
 * @apiGroup          Registry
 * @apiPermission     ExpertInCharge,DonorExpert,OPUChief,Inspector(Tel,Clinical),Coordinator
 * @apiParam {String} [q] Search on `name_first`, `name_last`, 'code_melli` and `file_no` (like what google do).
 * @apiParam {String} [code_melli] Code melli of case
 * @apiParam {String} [name_first] First name of case
 * @apiParam {String} [name_last] Last name of case
 * @apiParam {String} [name] Full name of case
 * @apiParam {String} [file_no] File No. of case
 * @apiParam {String=1,2,3} [gender] Gender of case
 * @apiParam {String} [university] Hashid of university for filter
 * @apiParam {String} [opu] Hashid of OPU for fitler
 * @apiParam {String} [hospital] Hashid of hospital for filter
 * @apiParam {String} [ward] Hashid of ward for filter
 * @apiParam {String} [inspector] Hashid of last inspector for filter
 * @apiParam {String} [coordinator] Hashid of last coordinator for filter
 * @apiParam {String=new,follow-up,under-action,healed,donated,unfeasible} [case_situation] Case situation of case
 * @apiParam {String=clinical,over-tel} [identification_type] Identification type of case
 * @apiParam {String} [consciousness_disorder_cause] Consciousness disorder cause of case
 * @apiParam {String} [sort] Enable user to sort fields. On this scope sort can be done based on items on `columns`.
 *           Default sort type will be `ascending` and for changing this direction to descending you must append a `-`
 *           sign in heading of field. (for example: `sort[]=year&sort[]=-month` will do an ascending sort first on
 *           `year` column, then will do a descending sort on `month` column for each collection.).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 2,
 *          "authenticated_user": "qKXVA"
 *      },
 *      "results": [
 *          {
 *              "id": "XAGkA",
 *              "code_melli": null,
 *              "full_name": null,
 *              "file_no": null,
 *              "age": 0,
 *              "gcs_on_identification": null,
 *              "gcs": null,
 *              "color": "white",
 *              "hospital": {
 *                  "id": "PNOLA",
 *                  "name": "Masih Daneshvari",
 *                  "type": "hospital"
 *              },
 *              "ward": {
 *                  "id": "kxPdx",
 *                  "name": "Bala",
 *                  "type": "ward"
 *              },
 *              "opu": {
 *                  "id": "gxnDN",
 *                  "name": "Chamran",
 *                  "type": "opu"
 *              },
 *              "university": {
 *                  "id": "qKXVA",
 *                  "name": "Tehran University",
 *                  "type": "university"
 *              },
 *              "city": {
 *                  "id": "AXDdA",
 *                  "slug": "tehran",
 *                  "type": "city",
 *                  "title": "Tehran"
 *              },
 *              "province": {
 *                  "id": "kxPdx",
 *                  "slug": "tehranp",
 *                  "type": "province",
 *                  "title": "Tehran"
 *              },
 *              "case_creator": {
 *                  "id": "PKEnN",
 *                  "name": "Michael Jackson"
 *              },
 *              "last_inspector": {
 *                  "id": "QKgJx",
 *                  "name": "GhorbanAli Sadeghi"
 *              },
 *              "last_coordinator": {
 *                  "id": "qKXVA",
 *                  "name": "Sir Programmer"
 *              },
 *              "identification_type": null,
 *              "consciousness_disorder_cause": null,
 *              "remarks": null,
 *              "reflex_breathing": 0,
 *              "reflex_cornea": 0,
 *              "reflex_pupil": 0,
 *              "reflex_face": 0,
 *              "reflex_body": 0,
 *              "reflex_doll": 0,
 *              "reflex_gag": 0,
 *              "reflex_cough": 0,
 *              "sedation": null,
 *              "case_identified_at": 1544473800,
 *              "updated_at": 1545040267
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\CaseController controller()
 */
class CaseListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "List of cases";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CaseController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        $result = [];
        for ($i = 0; $i < 15; $i++) {
            $result[] = $this->makeMockResult();
        }
        return api()->successRespond($result);
    }



    /**
     * Generate mock data array
     *
     * @return array
     */
    protected function makeMockResult()
    {

        $colors = [
             "green",
             "yellow",
             "white",
             "black",
             "red",
             "purple",
             "cyan",
        ];

        return [

             "id"                    => strtoupper(substr(md5(microtime()), rand(0, 26), 5)),
             "code_melli"            => rand(1536372645, 9536372645),
             "full_name"             => dummy()::persianName() . ' ' . dummy()::persianFamily(),
             "file_no"               => rand(1243454534, 5243454534),
             "age"                   => rand(10, 65),
             "gcs_on_identification" => rand(3, 15),
             "gcs"                   => rand(3, 15),
             "color"                 => array_random($colors),

             "hospital"   => [
                  "id"   => substr(md5(microtime()), rand(0, 26), 5),
                  "name" => dummy()::persianWord(3),
             ],
             "ward"       => [
                  "id"   => substr(md5(microtime()), rand(0, 26), 5),
                  "name" => dummy()::persianWord(3),
             ],
             "opu"        => [
                  "id"   => substr(md5(microtime()), rand(0, 26), 5),
                  "name" => dummy()::persianWord(3),
             ],
             "university" => [
                  "id"   => substr(md5(microtime()), rand(0, 26), 5),
                  "name" => dummy()::persianWord(3),
             ],

             "city"     => "Amol",
             "province" => "Mazandaran",

             "case_creator"     => [
                  "id"   => substr(md5(microtime()), rand(0, 26), 5),
                  "name" => dummy()::persianName(),
             ],
             "last_inspector"   => [
                  "id"   => substr(md5(microtime()), rand(0, 26), 5),
                  "name" => dummy()::persianName(),
             ],
             "last_coordinator" => [
                  "id"   => substr(md5(microtime()), rand(0, 26), 5),
                  "name" => dummy()::persianName(),
             ],

             "identification_type"          => "over-tel",
             "consciousness_disorder_cause" => "over-tel",
             "remarks"                      => "Remarks of case",

             "reflex_breathing" => "p",
             "reflex_cornea"    => "n",
             "reflex_pupil"     => "n",
             "reflex_face"      => "u",
             "reflex_body"      => "p",
             "reflex_doll"      => "u",
             "reflex_gag"       => "p",
             "reflex_cough"     => "n",
             "sedation"         => true,

             "case_identified_at" => now()->timestamp,
             "updated_at"         => now()->timestamp,

        ];

    }
}
