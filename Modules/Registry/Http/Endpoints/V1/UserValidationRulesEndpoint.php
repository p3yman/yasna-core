<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\UsersController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-user-validation-rules
 *                    User Validation Rules
 * @apiDescription    get a list of validation rules of a specific user role
 * @apiVersion        1.0.0
 * @apiName           User Validation Rules
 * @apiGroup          Registry
 * @apiPermission     guest
 * @apiParam {string} role
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "role":"president"
 *      },
 *      "results": {
 *          "code_melli": "",
 *          "name_title": "",
 *          "name_first": "required",
 *          "name_last": "required",
 *          "gender": "numeric|min:0|max:3"
 *      }
 * }
 * @method  UsersController controller()
 */
class UserValidationRulesEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "User Validation Rules";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return true;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UsersController@validationRules';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        if (!request()->role) {
            return api()->successRespond([
                 "code_melli"          => "ed",
                 "name_title"          => "pd",
                 "name_first"          => "pd",
                 "name_last"           => "pd",
                 "mobile"              => "ed",
                 "gender"              => "ed|numeric",
                 "edu_field"           => "pd",
                 "is_retired"          => "bool",
                 "birth_date"          => "date",
                 "latest_job_position" => "pd",
                 "tel"                 => "ed",
            ], [
                 "role" => "[Purification Rules]",
            ]);

        }

        return api()->successRespond([
             "code_melli" => "",
             "name_title" => "",
             "name_first" => "required",
             "name_last"  => "required",
             "gender"     => "numeric|min:0|max:3",
        ], [
             "role" => "president",
        ]);

    }
}
