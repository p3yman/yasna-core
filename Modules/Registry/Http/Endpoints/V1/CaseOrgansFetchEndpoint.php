<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Http\Controllers\V1\CaseController;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-case-organs-fetch
 *                    Case Organs Fetch
 * @apiDescription    Fetch data of the case organs, just enough to show an edit form
 * @apiVersion        1.0.0
 * @apiName           Case Organs Fetch
 * @apiGroup          Registry
 * @apiPermission     OPU Manager or coordinator (Checked in the request layer)
 * @apiParam {string}   id          hashid of the case
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "QKgJx"
 *      },
 *      "results": {
 *         "heart_prohibit": "1",
 *         "heart_prohibit_by_desc:"folan"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method CaseController controller()
 */
class CaseOrgansFetchEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Case Organs Fetch";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return "CaseController@fetchOrgans";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "reached" => "CaseOrgansFetchEndpoint",
        ]);
        //TODO: Return a mock data similar to the one you would do in the controller layer.
    }
}
