<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;
use Modules\Registry\Entities\RegistryUnityRole;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-case-single
 *                    Case Single
 * @apiDescription    View the a single resource of a case
 * @apiVersion        1.0.0
 * @apiName           Case Single
 * @apiGroup          Registry
 * @apiPermission     ExpertInCharge,DonorExpert,OPUChief,Inspector(Tel,Clinical),Coordinator
 * @apiParam {String} case Hashid of case
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          TODO: Fill this!
 *      },
 *      "results": {
 *          TODO: Fill this!
 *      }
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\CaseController controller()
 */
class CaseSingleEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Case Single";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CaseController@single';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "reached" => "CaseSingleEndpoint",
        ]);
    }
}
