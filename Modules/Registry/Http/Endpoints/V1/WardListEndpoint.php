<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {GET}
 *                    /api/modular/v1/registry-ward-list
 *                    Ward List
 * @apiDescription    List of all wards which the authenticated user can view.
 * @apiVersion        1.0.1
 * @apiName           Ward List
 * @apiGroup          Registry
 * @apiPermission     ExpertInCharge,DonorExpert,SuperAdmin
 * @apiParam {String} [name] Name of ward
 * @apiParam {String} [city] Hashid of city
 * @apiParam {String} [province] Hashid of province
 * @apiParam {String} [university] Hashid of university
 * @apiParam {String} [opu] Hashid of opu
 * @apiParam {String} [hospital] Hashid of hospital
 * @apiParam {Integer=0,1} [paginated=0] Specifies that the results must be paginated (`1`) or not (`0`).
 * @apiParam {Integer} [page=1] Specifies the page number of results (available only with `paginated=1`).
 * @apiParam {Integer} [per_page=15] Specifies the number of results on each page (available only with `paginated=1`).
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "total": 1
 *      },
 *      "results": [
 *          {
 *              "name": "سی\u200cسی\u200cیو",
 *              "city": "تهران",
 *              "city_hashid": "AXDdA",
 *              "province": "تهران",
 *              "province_hashid": "kxPdx",
 *              "hospital": "مسیح دانشوری",
 *              "hospital_hashid": "PNOLA",
 *              "phones": null
 *          }
 *      ]
 * }
 * @apiErrorExample   Unauthenticated Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\WardController controller()
 */
class WardListEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Ward List";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isLoggedIn();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_GET;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'WardController@list';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "name"            => dummy()::persianName(),
             "city"            => dummy()::persianName(),
             "city_hashid"     => "AXDdA",
             "province"        => dummy()::persianName(),
             "province_hashid" => "kxPdx",
             "hospital"        => dummy()::persianName(),
             "hospital_hashid" => "PNOLA",
             "phones"          => "2565255",
        ]);
    }
}
