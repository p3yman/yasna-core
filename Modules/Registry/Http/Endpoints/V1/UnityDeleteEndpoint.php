<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {DELETE}
 *                    /api/modular/V1/registry-unity-delete
 *                    Unity Delete
 * @apiDescription    an endpoint for the deletion of an entity, with any type
 * @apiVersion        1.0.0
 * @apiName           Unity Delete
 * @apiGroup          Registry
 * @apiPermission     user (accurate check will be performed later in the request layer)
 * @apiParam {string} hashid the hashid of the unity in question
 * @apiParam {string} type the type of the unity in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "folan",
 *          "type":"university",
 *      },
 *      "results": {
 *          "done": "1",
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class UnityDeleteEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Unity Delete";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_DELETE;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        $controller = studly_case(request()->type) . "Controller";

        return "$controller@delete";
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "type"   => request()->type,
             "hashid" => request()->hashid,
        ], [
             "done" => "1",
        ]);
    }
}
