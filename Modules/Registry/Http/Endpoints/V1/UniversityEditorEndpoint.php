<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/V1/Registry-university-editor
 *                    University Editor (Create and Update)
 * @apiDescription    Endpoint to create and edit universities from different user roles
 * @apiVersion        1.0.0
 * @apiName           University Editor (Create and Update)
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {string} hashid          the model hashid (required in the edit mode)
 * @apiParam {string} name            the university name
 * @apiParam {string} city            the city hashid
 * @apiParam {string} university_type the university type selected from a combo: university-type
 * @apiParam {string} [address]       the university address
 * @apiParam {string} [phones]        the university telephone number(s)
 * @apiParam {string} [postal_code]   the university postal code
 * @apiParam {string} [website]       the university website
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "QKgJx"
 *      },
 *      "results": {
 *          done": "1"
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *  "status": 400,
 *  "developerMessage": "registry::developerMessages.1002",
 *  "userMessage": "registry::userMessages.1002",
 *  "errorCode": 1002,
 *  "moreInfo": "registry.moreInfo.1002",
 *  "errors": {
 *      "city_id": [
 *          [
 *              "The City field is required."
 *          ]
 *     ]
 *  }
 * }
 * @apiErrorExample   User Access with No Role on System:
 * HTTP/1.1 600 Onion Termination
 * {
 *      "status": 500,
 *      "developerMessage": "endpoint::developerMessages.endpoint-600",
 *      "userMessage": "Onion Termination!",
 *      "errorCode": "endpoint-600",
 *      "moreInfo": "endpoint.moreInfo.endpoint-600",
 *      "errors": [
 *           "Currently your account is restricted and you can't perform this action."
 *      ]
 * }
 */
class UniversityEditorEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "University Editor (Create and Update)";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'UniversityController@save';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        if (rand(0, 1)) {
            return api()->successRespond(["done" => "1",], ["hashid" => "jAfAr",]);
        }

        return api()->errorArray(507);
    }
}
