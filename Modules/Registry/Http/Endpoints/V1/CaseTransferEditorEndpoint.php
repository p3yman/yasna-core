<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/registry-case-transfer-editor
 *                    Case Transfer Editor
 * @apiDescription    Case Transfer Editor
 * @apiVersion        1.0.0
 * @apiName           Case Transfer Editor
 * @apiGroup          Registry
 * @apiPermission     User
 * @apiParam {String} [hashid] Hashid of transfer for edit
 * @apiParam {String} city Hashid of destination city
 * @apiParam {String} hospital Hashid of destination hospital
 * @apiParam {String} case Hashid of case requested for transfer
 * @apiParam {String=pending,confirmed,dismissed} status Status of transfer
 * @apiParam {Integer=1..5} [transfer_reason] Reason of transfer
 * @apiParam {Integer=1..5} [dismiss_transfer_reason] Reason of transfer dismiss
 * @apiParam {Integer[]=1,2,3} [transfer_next_action] Next action after transfer
 * @apiParam {Integer=1..4} [case_final_state] Final state of case
 * @apiParam {Integer} [transferred_at] Time of transfer (unix timestamp)
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *          "hashid": "QKgJx"
 *      },
 *      "results": {
 *          done": "1"
 *      }
 * }
 * @apiErrorExample   Unauthorized Access:
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\CaseController controller()
 */
class CaseTransferEditorEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Case Transfer Editor";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->exists;
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'CaseController@savePatientTransfer';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "hashid" => hashid(rand(1, 50)),
        ]);
    }
}
