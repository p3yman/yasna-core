<?php

namespace Modules\Registry\Http\Endpoints\V1;

use Modules\Endpoint\Services\EndpointAbstract;

/**
 * @api               {POST}
 *                    /api/modular/v1/registry-superadmin-assign-supervisor
 *                    Superadmin Assign Supervisor
 * @apiDescription    Assign a user as a supervisor of one or more university
 * @apiVersion        1.0.0
 * @apiName           Superadmin Assign Supervisor
 * @apiGroup          Registry
 * @apiPermission     Superadmin
 * @apiParam {Array} universities An Array of Hashids of Universities
 * @apiParam {String} [user] The Hashid of the User to Be Assigned. Should be presented if you want to assign an
 *           existing user as a supervisor.
 * @apiParam {String} [code_melli] The code_melli of the user in question
 * @apiParam {String} [name_first] The  first name of the user in question
 * @apiParam {String} [name_last] The  last name of the user in question
 * @apiParam {String} [name_title] The  title (like Dr.) of the user in question
 * @apiParam  {string} [mobile] the mobile number of the user in question
 * @apiParam {Integer} [gender] The  gender of the user in question (1:male, 2:female, 3:unknown)
 * @apiParam {Integer} [edu_level] The  edu level of the user in question
 * @apiParam {String} [job] The job of the user in question
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *      "status": 200,
 *      "metadata": {
 *           "user": "PNOLA",
 *           "universities": [
 *                "qKXVA",
 *                "JAJwx"
 *           ]
 *      },
 *      "results": {
 *          "done": 1
 *      }
 * }
 * @apiErrorExample
 * HTTP/1.1 400 Bad Request
 * {
 *      "status": 400,
 *      "developerMessage": "endpoint::developerMessages.endpoint-403",
 *      "userMessage": "Forbidden!",
 *      "errorCode": "endpoint-403",
 *      "moreInfo": "endpoint.moreInfo.endpoint-403",
 *      "errors": []
 * }
 * @method  \Modules\Registry\Http\Controllers\V1\SupervisorsController controller()
 */
class SuperadminAssignSupervisorEndpoint extends EndpointAbstract
{
    /**
     * @inheritdoc
     */
    public static function getTitle(): string
    {
        return "Superadmin Assign Supervisor";
    }



    /**
     * @inheritdoc
     */
    public function hasPermit(): bool
    {
        return $this->user->isSuperadmin();
    }



    /**
     * @inheritdoc
     */
    public function getValidMethod(): string
    {
        return static::HTTP_POST;
    }



    /**
     * @inheritdoc
     */
    public function getControllerNamespace(): string
    {
        return 'Modules\Registry\Http\Controllers\V1';
    }



    /**
     * @inheritdoc
     */
    public function getController(): string
    {
        return 'SupervisorsController@assignBySuperadmin';
    }



    /**
     * @inheritdoc
     */
    public function getMockResult()
    {
        return api()->successRespond([
             "done" => "1",
        ], [
             "user"         => "PNOLA",
             "universities" => [
                  "qKXVA",
                  "JAJwx",
             ],
        ]);
    }
}
