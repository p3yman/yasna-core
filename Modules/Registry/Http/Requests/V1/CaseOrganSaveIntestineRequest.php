<?php

namespace Modules\Registry\Http\Requests\V1;

class CaseOrganSaveIntestineRequest extends CaseOrganSaveRequest
{
    /**
     * get validation rules for prohibitions
     *
     * @return array
     */
    protected function prohibitionRules()
    {
        $rule = $this->getComboValidator("intestine_prohibit_by_desc", "intestine-prohibit-desc");

        return [
             "intestine_prohibit_by_desc" => $rule,
        ];
    }



    /**
     * get validation rules for tests (section 1)
     *
     * @return array
     */
    protected function testRules()
    {
        $required    = false;
        $requirement = $required ? "required|" : "";

        return [
             "test_serum_amylase"  => $requirement . "numeric|between:0,5000",
             "test_serum_lipase"   => $requirement . "numeric|between:0,5000",
             "test_ast"            => $requirement . "between:1,3000",
             "test_alt"            => $requirement . "between:1,3000",
             "test_alk_p"          => $requirement . "numeric|between:10,3000",
             "test_bili_t"         => $requirement . "numeric|between:0.1,30",
             "test_bili_d"         => $requirement . "numeric|between:0.1,30",
             "test_hemoglobin_a1c" => $requirement . "numeric|between:3,20",

             "tests_group3_done_at" => "date",
             "tests_group6_done_at" => "date",
             "tests_group7_done_at" => "date",

        ];
    }



    /**
     * get validation rules for sonography (section 2)
     *
     * @return array
     */
    protected function sonographyRules()
    {
        $required    = !boolval($this->getData('intestine_prohibit'));
        $requirement = $required ? "required|" : "";

        return [
             "intestine_abdominal_sonography"        => $requirement . "in:normal,abnormal",
             "intestine_abdominal_sonography_report" => $requirement . "array",
        ];
    }



    /**
     * get validation rules for ctscan (section 3)
     *
     * @return array
     */
    protected function ctscanRules()
    {
        $required    = false;
        $requirement = $required ? "required|" : "";

        return [
             "intestine_abdominal_ctscan"        => $requirement . "in:normal,abnormal",
             "intestine_abdominal_ctscan_report" => $requirement . "array",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "test_serum_amylase"   => "ed",
             "test_serum_lipase"    => "ed",
             "test_ast"             => "ed",
             "test_alt"             => "ed",
             "test_alk_p"           => "ed",
             "test_bili_t"          => "ed",
             "test_bili_d"          => "ed",
             "test_hemoglobin_a1c"  => "ed",
             "tests_group3_done_at" => "date",
             "tests_group6_done_at" => "date",
             "tests_group7_done_at" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->addOrganFilled();
        $this->setProhibitionFlag();
    }



    /**
     * set `heart_organ_filled` to true
     */
    private function addOrganFilled()
    {
        $this->setData("intestine_organ_filled", 1);
    }



    /**
     * set `heart_prohibit` flag if necessary
     */
    private function setProhibitionFlag()
    {
        if ($this->getData("intestine_prohibit_by_desc")) {
            $this->setData("intestine_prohibit", 1);
        }

        $this->setData("intestine_prohibit", 0);
    }

}
