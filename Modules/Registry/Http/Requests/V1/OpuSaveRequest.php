<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\RegistryOpu;
use App\Models\RegistryUnityRole;
use App\Models\RegistryUniversity;
use Modules\Registry\Http\Requests\V1\Traits\RequestComboValidatorTrait;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class OpuEditorRequest
 *
 * @property RegistryOpu $model
 * @package Modules\Registry\Http\Requests\V1
 */
class OpuSaveRequest extends YasnaRequest
{
    use RequestComboValidatorTrait;
    /**
     * @inheritdoc
     */
    protected $model_name = "registry-opu";

    /**
     * @inheritdoc
     */
    protected $responder = 'white-house';

    /**
     * keep the university if set by the user
     *
     * @var RegistryUniversity|null
     */
    protected $university = null;

    /**
     * keep the cached value for being edited by opu manager to eliminate database queries
     *
     * @var null|bool
     */
    protected $cached_value_for_being_edited_by_opu_manager = null;



    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * check if the model is bing edited by an opu manager or not (either create, or edit by someone else).
     *
     * @return bool
     */
    protected function isBeingEditedByOpuManager()
    {
        if (!$this->model->exists) {
            return false;
        }

        if (!is_null($this->cached_value_for_being_edited_by_opu_manager)) {
            return $this->cached_value_for_being_edited_by_opu_manager;
        }

        if (user()->isSuperadmin()) {
            $return = false;
        } else {
            $return = !$this->model->canCreate(); // <~~ It means the editor is someone eligible, other than the opu-manager
        }

        $this->cached_value_for_being_edited_by_opu_manager = $return;

        return $return;
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_keys($this->rules());
    }



    /**
     * get general validation rules
     *
     * @return array
     */
    public function generalRules()
    {
        return [
             "phones"              => "required",
             "service_location"    => "",
             "telephone_detection" => "required|bool",
             "clinical_detection"  => "required|bool",
             "opu_type"            => "required|in:1,2",
             "hospitals"           => "array",
             "yearly_population"   => "array",
             "yearly_donation"     => "array",
             "yearly_pmp"          => "array",
             "opening_year"        => "required|numeric",
        ];
    }



    /**
     * get the rules that are required only when the opu-manager is editing the model
     *
     * @return array
     */
    public function complementaryRules()
    {
        $required = "";
        if ($this->isBeingEditedByOpuManager()) {
            $required = "required";
        }

        return [
             "portable_monitor"    => "$required|bool",
             "portable_ventilator" => "$required|bool",
             "portable_eeg"        => "$required|bool",
             "vessels_angiography" => "$required|bool",
             "isotope_scan"        => "$required|bool",
             "tcd_tools"           => "$required|bool",
             //"organ_harvesting"    => "$required|bool",
             //"harvestable_organs"  => $this->getData("organ_harvesting") ? "$required" : "",
        ];
    }



    /**
     * get validation rules for the university attribute
     *
     * @return array
     */
    public function universityRules()
    {
        if (!$this->model->exists) {
            return [
                 "university" => "required|accepted",
            ];
        }

        return [
             "university" => "accepted",
        ];
    }



    /**
     * get validation rules for the bed type
     *
     * @return array
     */
    public function bedTypeRules()
    {
        return [
             "bed_type" => $this->getComboValidator("bed_type", "opu_bed_type", true),
        ];
    }



    /**
     * get validation rules for the bed type
     *
     * @return array
     */
    public function bedCountRules()
    {
        $type = $this->getData("bed_type");

        if ($type == "opu-bed-type-guest") {
            $this->setData("bed_counts", 0);
            $rule = "";
        } else {
            $rule = "required|numeric|between:1,100";
        }

        return [
             "bed_counts" => $rule,
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "bed_counts"   => "ed",
             "opening_year" => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->injectUnityType();
        $this->resolveUniversity();
        $this->resolveServiceLocation();
        //$this->correctOrganHarvesting();
        $this->filterHospitals();
    }



    /**
     * inject unity type
     */
    private function injectUnityType()
    {
        $this->setData("type", "opu");
    }



    /**
     * resolve university model
     */
    private function resolveUniversity()
    {
        $attribute = "university";

        if ($this->isBeingEditedByOpuManager()) {
            $this->setData($attribute, true); // <~~ Neglects user input when the user cannot change university
            return;
        }

        $hashid     = $this->getData($attribute);
        $university = model("registry-university")->grabHashid($hashid);

        if ($university->exists) {
            $this->setData("university_id", $university->id);
            $this->setData($attribute, true);
            $this->university = $university;
            $this->model->setRelation('university', $university);
            return;
        }

        $this->unsetData($attribute);
    }



    /**
     * resolve the hospital which is going to be the service location of the opu
     */
    private function resolveServiceLocation()
    {
        $attribute = "service_location";
        if (!$this->isset($attribute)) {
            return;
        }

        $hashid   = $this->getData($attribute);
        $hospital = model("registry-hospital")->grabHashid($hashid);

        $this->setData($attribute, $hospital->id);
    }



    /**
     * correct harvest-related fields
     */
    private function correctOrganHarvesting()
    {
        if (!$this->getData("opu_type")) {
            $this->setData("organ_harvesting", 0);
        }

        $harvestable = $this->getData("harvestable_organs");

        if (!$this->getData("organ_harvesting")) {
            $harvestable = [];
        }

        if (!is_array($harvestable)) {
            $harvestable = [];
        }

        $this->setData("harvestable_organs", implode(",", $harvestable));
    }



    /**
     * filter hospitals by removing the invalid ones and replacing the ids
     */
    private function filterHospitals()
    {
        $hashids = $this->getData("hospitals");
        if (!$hashids or !is_array($hashids)) {
            return;
        }

        $ids        = hashid($hashids);
        $university = $this->university?? $this->model->university_model;
        $filtered   = $university->hospitals()->whereIn("id", $ids)->pluck("id")->toArray();

        $this->setData("hospitals", $filtered);
    }
}
