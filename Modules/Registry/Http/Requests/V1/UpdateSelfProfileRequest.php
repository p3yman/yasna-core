<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class UpdateSelfProfileRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_keys($this->rules());
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $array = [];
        $roles = user()->unityRolesArray();

        foreach ($roles as $role) {
            $second = (array)config("registry.roles.$role.fields");
            $array  = $this->ruleMerger($array, $second);
        }

        return $array;

    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return config("registry.roles.purification_rules");
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * merge the two rules and get the sum
     *
     * @param array $first
     * @param array $second
     *
     * @return array
     */
    private function mergeRules(array $first, array $second): array //TODO: To be deleted if everything goes right
    {
        $items = array_merge($first, $second);

        foreach ($items as $field => $rules) {
            $first_rules  = isset($first[$field]) ? (array)$first[$field] : [];
            $second_rules = isset($second[$field]) ? (array)$second[$field] : [];
            $merged_rules = array_unique(array_merge($first_rules, $second_rules));

            $items[$field] = implode("|", $merged_rules);
        }

        return $items;
    }
}
