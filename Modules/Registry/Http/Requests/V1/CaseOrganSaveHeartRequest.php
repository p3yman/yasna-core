<?php

namespace Modules\Registry\Http\Requests\V1;


class CaseOrganSaveHeartRequest extends CaseOrganSaveRequest
{
    /**
     * get validation rules for prohibitions
     *
     * @return array
     */
    protected function prohibitionRules()
    {
        $first  = $this->getComboValidator("heart_prohibit_by_desc", "heart-prohibit-desc");
        $second = $this->getComboValidator("heart_prohibit_by_electrocardiogram", "heart-prohibit-electrocardiogram");
        $third  = $this->getComboValidator("heart_prohibit_by_angiography", "heart-prohibit-angiography");

        return [
             "heart_prohibit_by_desc"              => $first,
             "heart_prohibit_by_electrocardiogram" => $second,
             "heart_prohibit_by_angiography"       => $third,
        ];
    }



    /**
     * get validation rules for cardiography
     *
     * @return array
     */
    protected function cardiographyRules()
    {
        $requirement = $this->getData("heart_prohibit_by_electrocardiogram") ? "required|" : "";

        return [
             "heart_cardiography"        => $requirement . "boolean",
             "heart_cardiography_report" => $requirement . "array",
             "heart_cardiography_test"   => $requirement . "array",
             "heart_echocardiography"    => $requirement . "boolean",
             "inotrop_eco"               => "required|array",
        ];
    }



    /**
     * get validation rules for echocardiography
     *
     * @return array
     */
    protected function echocardiographyRules()
    {
        $required          = $this->getData("heart_prohibit") ? true : false;
        $requirement       = $required ? "required|" : "";
        $valvular_problem  = $this->getComboValidator(
             "heart_valvular_problem_type",
             "heart-valvular-problem",
             $this->getData("heart_valvular_problem")
        );
        $valvular_severity = $this->getComboValidator(
             "heart_valvular_problem_severity",
             "severity",
             $this->getData("heart_valvular_problem")
        );
        $diastolic         = $this->getComboValidator(
             "heart_diastolic_dysfunction_severity",
             "severity",
             $this->getData("heart_diastolic_dysfunction")
        );
        $heart_rv_size     = $this->getComboValidator("heart_rv_size", "organ-size", $required);
        $heart_rv_function = $this->getComboValidator("heart_rv_function", "organ-function", $required);
        $heart_la_size     = $this->getComboValidator("heart_la_size", "organ-size", $required);
        $heart_la_function = $this->getComboValidator("heart_la_function", "organ-function", $required);
        $heart_lv_size     = $this->getComboValidator("heart_lv_size", "organ-size", $required);
        $heart_lv_function = $this->getComboValidator("heart_lv_function", "organ-function", $required);
        $heart_ra_size     = $this->getComboValidator("heart_ra_size", "organ-size", $required);
        $heart_ra_function = $this->getComboValidator("heart_ra_function", "organ-function", $required);
        $wall_motion       = $this->getComboValidator("heart_wall_motion_abnormality", "wall-motion", $required);
        $consultant        = $this->getConsultantValidator("heart_tests_consultant", $required);

        return [
             "heart_tests_ef"                       => $requirement . "numeric|between:0,70",
             "heart_tests_lvh"                      => $requirement . "numeric|between:1,30",
             "heart_tests_pap"                      => $requirement . "numeric|between:0,150",
             "heart_tests_trg"                      => $requirement . "numeric|between:0,150",
             "heart_wall_motion_abnormality"        => $wall_motion,
             "heart_valvular_problem"               => $requirement . "boolean",
             "heart_valvular_problem_type"          => $valvular_problem,
             "heart_valvular_problem_severity"      => $valvular_severity,
             "heart_diastolic_dysfunction"          => $requirement,
             "heart_diastolic_dysfunction_severity" => $diastolic,
             "heart_lv_size"                        => $heart_lv_size,
             "heart_lv_function"                    => $heart_lv_function,
             "heart_rv_size"                        => $heart_rv_size,
             "heart_rv_function"                    => $heart_rv_function,
             "heart_la_size"                        => $heart_la_size,
             "heart_la_function"                    => $heart_la_function,
             "heart_ra_size"                        => $heart_ra_size,
             "heart_ra_function"                    => $heart_ra_function,
             "heart_tests_consultant"               => $consultant,
             "heart_tests_consultant_report"        => $requirement . "array",
        ];
    }



    /**
     * get validation rules for CXR
     *
     * @return array
     */
    protected function cxrRules()
    {
        $required    = ($this->getData("heart_prohibit_by_angiography") or $this->getData("heart_prohibit_by_electrocardiogram")) ? true : false;
        $requirement = $required ? "required|" : "";

        return [
             "heart_cxr"        => $requirement . "boolean",
             "heart_cxr_report" => $requirement . "array",
        ];
    }



    /**
     * get validation rules for angiography
     *
     * @return array
     */
    protected function angiographyRules()
    {
        $required    = $this->getData("heart_prohibit_by_angiography") ? true : false;
        $requirement = $required ? "required|" : "";

        return [
             "heart_angiography"        => $this->getComboValidator("heart_angiography", "organ-normality", $required),
             "heart_angiography_report" => $requirement . "array",
        ];
    }



    /**
     * get validation rules for troponin test
     *
     * @return array
     */
    protected function troponinRules()
    {
        $required    = !boolval($this->getData("heart_prohibit"));
        $requirement = $required ? "required|" : "";

        return [
             "test_troponin_i"     => $requirement . "numeric|between:0,10",
             "heart_tests_done_at" => $requirement . "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->addOrganFilled();
        $this->setProhibitionFlag();
        //$this->allowOneProhibition();
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        parent::mutators();
        $this->dehashPeople();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "heart_tests_ef"      => "ed",
             "heart_tests_lvh"     => "ed",
             "heart_tests_pap"     => "ed",
             "heart_tests_trg"     => "ed",
             "test_troponin_i"     => "ed",
             "heart_tests_done_at" => "ed|date",
        ];
    }



    /**
     * set `heart_organ_filled` to true
     */
    private function addOrganFilled()
    {
        $this->setData("heart_organ_filled", 1);
    }



    /**
     * set `heart_prohibit` flag if necessary
     */
    private function setProhibitionFlag()
    {
        if (
             $this->getData("heart_prohibit_by_desc")
             or $this->getData("heart_prohibit_by_electrocardiogram")
             or $this->getData("heart_prohibit_by_angiography")
        ) {
            $this->setData("heart_prohibit", 1);
        }

        $this->setData("heart_prohibit", 0);
    }



    /**
     * allow one prohibition at a time
     */
    private function allowOneProhibition()
    {
        if ($this->getData("heart_prohibit_by_desc")) {
            $this->setData("heart_prohibit_by_electrocardiogram", null);
            $this->setData("heart_prohibit_by_angiography", null);
        }

        if ($this->getData("heart_prohibit_by_electrocardiogram")) {
            $this->setData("heart_prohibit_by_desc", null);
            $this->setData("heart_prohibit_by_angiography", null);
        }

        if ($this->getData("heart_prohibit_by_angiography")) {
            $this->setData("heart_prohibit_by_desc", null);
            $this->setData("heart_prohibit_by_electrocardiogram", null);
        }
    }



    /**
     * @inheritdoc
     */
    protected function dehashPeople()
    {
        $this->setData("heart_tests_consultant", hashid($this->getData("heart_tests_consultant")));
    }

}
