<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\RegistryOpu;
use Modules\Yasna\Services\V4\Request\YasnaFormRequest;


class CaseTrashRequest extends YasnaFormRequest
{
    protected $model_name               = "RegistryCase";
    protected $should_allow_create_mode = false;

    /** @var RegistryOpu */
    private $opu;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (
             user()->isSuperadmin() or
             $this->userIsCoordinator() or
             $this->userIsOpuManager() or
             $this->userIsInspectorTel() or
             $this->userIsInspectorClinical()
        ) {
            return true;
        }

        return false;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->opu = opu($this->model->opu_id);
    }



    /**
     * checks if the online user is a valid coordinator of the ward in question
     *
     * @return bool
     */
    private function userIsCoordinator(): bool
    {
        $coordinators = $this->opu->coordinators->pluck('id')->toArray();

        $result = in_array(user()->id, $coordinators);

        if ($result) {
            $this->setData("coordinator_id", user()->id);
        }

        return $result;
    }



    /**
     * checks if the online user is the opu-manager of the ward in question
     *
     * @return bool
     */
    private function userIsOpuManager(): bool
    {
        return $this->opu->currentUserIsOpuManager();
    }



    /**
     * checks if the online user is a valid inspector-tel of the ward in question
     *
     * @return bool
     */
    private function userIsInspectorTel(): bool
    {
        $inspectors = $this->opu->inspectorTels()->pluck('id')->toArray();

        $result = in_array(user()->id, $inspectors);

        if ($result) {
            $this->setData("inspector_id", user()->id);
        }

        return $result;
    }



    /**
     * checks if the online user is a valid inspector-clinical of the ward in question
     *
     * @return bool
     */
    private function userIsInspectorClinical(): bool
    {
        $inspectors = $this->opu->inspectorClinicals()->pluck('id')->toArray();

        $result = in_array(user()->id, $inspectors);

        if ($result) {
            $this->setData("inspector_id", user()->id);
        }

        return $result;
    }
}
