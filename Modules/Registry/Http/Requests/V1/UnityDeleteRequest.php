<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class UnityDeleteRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canDelete();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "unity" => $this->model->exists ? "" : "accepted",
             "type"  => "required", //<~~ never happens!
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "role.required"  => trans("registry::validation.role_not_found"),
             "unity.accepted" => trans("registry::validation.unity_not_found"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveModelName();
        $this->resolveUnity();
    }



    /**
     * resolve $this->model_name, based on the given type
     *
     * @return void
     */
    private function resolveModelName()
    {
        $this->model_name = studly_case("registry-" . $this->data['type']);
    }



    /**
     * resolve the unity
     *
     * @return void
     */
    private function resolveUnity()
    {
        $this->model = model($this->model_name)->grabHashid($this->data['hashid']);
    }

}
