<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\RegistryUnityRole;
use Modules\Registry\Http\Requests\V1\Traits\GetUserInformationFromRequestTrait;
use Modules\Yasna\Services\YasnaRequest;

class UserChangePasswordRequest extends YasnaRequest
{
    use GetUserInformationFromRequestTrait;


    /**
     * @inheritdoc
     */
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isSuper() or $this->canChangeUserPassword();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             'username' => 'required_without:user',
             'user'     => 'required_without:username',
             'password' => 'required|string|min:8|confirmed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'username',
             'user',
             'password',
             'password_confirmation',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'username' => 'ed',
             'user'     => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * Indicate current authenticated user can change user password or not
     *
     * @return bool
     */
    protected function canChangeUserPassword(): bool
    {
        $user = $this->getUser(); // User whom we want to change his/her password

        // If current authenticated user, has permission to assign a role in at least one of the user unity,
        // so s|he can change her/his password.
        return $user->unityRoles
                  ->map(function ($unity_role) {
                      /** @var RegistryUnityRole $unity_role */
                      return $unity_role->unity->canAssign($unity_role->role);
                  })
                  ->filter()
                  ->count() > 0;
    }
}
