<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Registry\Entities\RegistryUnityRole;
use Modules\Registry\Http\Requests\V1\Traits\RequestComboValidatorTrait;
use Modules\Yasna\Services\YasnaRequest;

class GetCaseListRequest extends YasnaRequest
{
    use RequestComboValidatorTrait;

    protected $model_name = "registry_case";
    protected $responder  = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (user()->isSuperadmin()) {
            return true;
        }

        $allowed_roles = [
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
             RegistryUnityRole::ROLE_OPU_MANAGER,
             RegistryUnityRole::ROLE_COORDINATOR,
             RegistryUnityRole::ROLE_INSPECTOR_CLINICAL,
             RegistryUnityRole::ROLE_INSPECTOR_TEL,
             RegistryUnityRole::ROLE_OPU_SUPERVISOR,
             RegistryUnityRole::ROLE_UNIVERSITY_SUPERVISOR,
        ];

        return array_intersect($allowed_roles, user()->unityRolesArray()) != null;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        $case_situation      = $this->getComboValidator("case_situation", "case-situation");
        $identification_type = $this->getComboValidator("identification_type", "identification-type");

        return [
             'q'                            => 'string',
             'code_melli'                   => 'string',
             'name_first'                   => 'string',
             'name_last'                    => 'string',
             'file_no'                      => 'string',
             'gender'                       => 'integer|in:1,2,3',
             'name'                         => 'string',
             'sort'                         => 'array|min:1',
             'sort.*'                       => 'string',
             'university'                   => 'string',
             'opu'                          => 'string',
             'hospital'                     => 'string',
             'ward'                         => 'string',
             'inspector'                    => 'string',
             'coordinator'                  => 'string',
             'case_situation'               => "string|$case_situation",
             'identification_type'          => "string|$identification_type",
             'consciousness_disorder_cause' => 'string',
             'paginated'                    => 'integer|in:0,1',
             'per_page'                     => 'integer|min:1|max:50',
             'page'                         => 'integer|min:1',
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'q',
             'code_melli',
             'name_first',
             'name_last',
             'file_no',
             'gender',
             'name',
             'sort',
             'university',
             'opu',
             'hospital',
             'ward',
             'inspector',
             'coordinator',
             'case_situation',
             'identification_type',
             'consciousness_disorder_cause',
             'paginated',
             'per_page',
             'page',
        ];
    }
}
