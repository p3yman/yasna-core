<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class ChangePasswordRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return user()->isLoggedIn();
    }



    /**
     * A set of rules for validating the given set of passwords
     *
     * @return array
     */
    public function passwordRules()
    {
        $rules = [
             "current_password" => "required|string",
             "password"         => "required|string|confirmed|max:40",
        ];

        if (!user()->checkPasswordSeverity($this->input('password', ''))) {
            $rules["password"] .= "|accepted";
        }

        if (!user()->givenPasswordIsSameOfTheCurrentPassword($this->input("current_password", ''))) {
            $rules["current_password"] .= "|accepted";
        }

        return $rules;
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "password.accepted"         => trans("registry::userMessages.bad_pattern_on_new_password"),
             "current_password.accepted" => trans("registry::userMessages.current_password_doesnt_match"),
        ];
    }
}
