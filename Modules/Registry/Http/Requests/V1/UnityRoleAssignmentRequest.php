<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Registry\Entities\Abstracts\UnityAbstract;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class UniversitySaveRequest
 *
 * @property UnityAbstract $model
 * @package Modules\Registry\Http\Requests\V1
 */
class UnityRoleAssignmentRequest extends YasnaRequest
{
    protected $responder             = 'white-house';
    protected $model_name            = "registry-university"; // <~~ This will be updated during the process
    protected $detected_invalid_city = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true;
    }



    /**
     * get main validation rules
     *
     * @return array
     */
    public function mainRules()
    {

        return [
             "unity" => $this->model->exists ? "" : "accepted",
             "type"  => "required", //<~~ never happens!
             "city"  => $this->detected_invalid_city ? "accepted" : "",
        ];
    }



    /**
     * get roles rules
     *
     * @return array
     */
    public function rolesRules()
    {
        $allowed_roles = implode(",", $this->model->userRoles());

        if (is_string($this->getData('role'))) {
            return [
                 "role" => "required|in:$allowed_roles",
            ];
        }

        return [
             "role"   => "array",
             "role.*" => "required|in:$allowed_roles",
        ];
    }



    /**
     * get the user validation rules when specific user is given with expressly supplied hashid
     *
     * @return array
     */
    public function specificUserRules()
    {
        if (!$this->askedForSpecificUser() or $this->data['user']->exists) {
            return [];
        }

        return [
             "user" => "accepted",
        ];
    }



    /**
     * get the validation rules when user information is given by a bunch of data
     *
     * @return array
     */
    public function unspecificUserRules()
    {
        if ($this->askedForSpecificUser()) {
            return [];
        }

        $roles = (array)$this->data['role'];
        $rules = [];

        foreach ($roles as $role) {
            $new_rule = config("registry.roles.$role.fields");
            $rules    = $this->ruleMerger($rules, $new_rule);
        }

        return $rules;
    }



    /**
     * check if the request specifies a user by expressly giving their hashid
     *
     * @return bool
     */
    public function askedForSpecificUser()
    {
        return isset($this->data['user']);
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return config("registry.roles.purification_rules");
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "user.accepted"  => trans("registry::validation.user_not_found"),
             "role.in"        => trans("registry::validation.role_not_found"),
             "role.required"  => trans("registry::validation.role_not_found"),
             "unity.accepted" => trans("registry::validation.unity_not_found"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $array1 = array_keys($this->purifier());
        $array2 = ["unity", "role", "type", "user"];

        return array_merge($array1, $array2);
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveDivisions();
        $this->resolveModelName();
        $this->resolveUnity();
        $this->resolveUser();
        $this->resolveWorkingUnity();
    }



    /**
     * resolve administrative divisions if given
     */
    private function resolveDivisions()
    {
        if (!isset($this->data['city'])) {
            return;
        }

        $city = model('division')->grabHashid($this->data['city']);

        if ($city->type != 'city') {
            $this->detected_invalid_city = true;
        }

        $this->data['city']     = $city->id;
        $this->data['province'] = $city->province_id;
    }



    /**
     * resolve $this->model_name, based on the given type
     *
     * @return void
     */
    private function resolveModelName()
    {
        $this->model_name = studly_case("registry-" . $this->data['type']);
    }



    /**
     * resolve the unity
     *
     * @return void
     */
    private function resolveUnity()
    {
        $this->model = model($this->model_name)->grabHashid($this->data['unity']);
    }



    /**
     * resolve user model
     *
     * @return void
     */
    private function resolveUser()
    {
        if (isset($this->data['user'])) {
            $this->data['user'] = user()->grabHashid($this->data['user']);
        }
    }



    /**
     * resolve working unity
     */
    private function resolveWorkingUnity()
    {
        if ($this->isset('work_unity')) {
            $this->setData('work_unity', hashid($this->getData('work_unity')));
        }
    }
}
