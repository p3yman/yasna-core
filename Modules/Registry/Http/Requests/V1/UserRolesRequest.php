<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\User;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class UserRolesRequest
 * <br>
 * The Request to Access the Unity Roles of a User
 *
 * @property User $model
 */
class UserRolesRequest extends YasnaRequest
{
    /**
     * The Name of the Main Model
     *
     * @var string
     */
    protected $model_name = "user";
    /**
     * If a Trashed Model Record is Accepted
     *
     * @var bool
     */
    protected $model_with_trashed = false;
    /**
     * The Name of the Responder
     *
     * @var string
     */
    protected $responder = "white-house";
    /**
     * The Error Code which will be returned in case of authorization's failure.
     *
     * @var string
     */
    protected $error_code_when_authorization_fails = 'registry-101';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return (
             $this->model->exists
             or user()->exists
        );
    }

}
