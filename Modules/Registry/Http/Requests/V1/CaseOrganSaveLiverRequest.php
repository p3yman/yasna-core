<?php

namespace Modules\Registry\Http\Requests\V1;

class CaseOrganSaveLiverRequest extends CaseOrganSaveRequest
{
    /**
     * get validation rules for prohibitions
     *
     * @return array
     */
    protected function prohibitionRules()
    {
        $first = $this->getComboValidator("liver_prohibit_by_desc", "liver-prohibit-desc");
        //$second = $this->getComboValidator("liver_prohibit_by_tests", "liver-prohibit-tests");
        $third  = $this->getComboValidator("liver_prohibit_by_sonography", "liver-prohibit-sonography");
        $fourth = $this->getComboValidator("liver_prohibit_by_observation", "liver-prohibit-observation");

        return [
             "liver_prohibit_by_desc"        => $first,
             //"liver_prohibit_by_tests"       => $second,
             "liver_prohibit_by_sonography"  => $third,
             "liver_prohibit_by_observation" => $fourth,
        ];
    }



    /**
     * get validation rules regarding section 1
     *
     * @return array
     */
    protected function section1Rules()
    {
        $required    = !boolval($this->getData('kidney_prohibit'));
        $requirement = $required ? "required|" : "";
        $position    = $this->getComboValidator("liver_mass_position", "liver-mass-position", $required);

        return [
             "liver_size"                => $this->getComboValidator("liver_size", "liver-size", $required),
             "liver_size_cm"             => $requirement . "numeric",
             "liver_ecosystem"           => $this->getComboValidator("liver_ecosystem", "liver-ecosystem", $required),
             "liver_mass"                => $this->getComboValidator("liver_mass", "liver-mass", $required),
             "liver_mass_count"          => $requirement . "numeric|between:0,20",
             "liver_mass_size"           => $requirement . "numeric|between:0,100",
             "liver_mass_position"       => $position,
             "liver_mass_position_other" => "required_if:liver_mass_position,liver-mass-position-others",
             "liver_gallstones"          => $requirement . "boolean",
             "liver_bile_ducts_inside"   => $requirement . "boolean",
             "liver_bile_ducts_outside"  => $requirement . "boolean",
             "liver_sonography_report"   => $requirement . "array",
        ];

    }



    /**
     * get validation rules regarding section 2
     *
     * @return array
     */
    protected function section2Rules()
    {
        $required    = false;
        $requirement = $required ? "required|" : "";
        $type        = $this->getComboValidator("liver_biopsy_type", "liver-biopsy-type");
        $result      = $this->getComboValidator("liver_biopsy_result", "liver-biopsy-result");

        return [
             "liver_biopsy"                           => $requirement . "boolean",
             "liver_biopsy_type"                      => "required_if:liver_biopsy,1|$type",
             "liver_biopsy_type_other"                => "required_if:liver_biopsy_type,liver-biopsy-type-others",
             "liver_biopsy_result"                    => "required_if:liver_biopsy,1|$result",
             "liver_biopsy_report"                    => "required_if:liver_biopsy,1|array",
             "liver_macro_vesicular_fat"              => "required_if:liver_biopsy,1|numeric|between:0,100",
             "liver_micro_intermediate_vesicular_fat" => "required_if:liver_biopsy,1|numeric|between:0,100",
        ];
    }



    /**
     * get validation rules regarding section 3
     *
     * @return array
     */
    protected function section3Rules()
    {
        $required    = false;
        $requirement = $required ? "required|" : "";

        return [
             "liver_ctscan"        => $requirement . "boolean",
             "liver_ctscan_report" => $requirement . "array",
        ];
    }



    /**
     * get validation rules regarding section 4
     *
     * @return array
     */
    protected function section4Rules()
    {
        $required    = false;
        $requirement = $required ? "required|" : "";

        return [
             "liver_hla_typing_report" => $requirement . "array",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "liver_mass_count"                       => "ed",
             "liver_mass_size"                        => "ed",
             "liver_macro_vesicular_fat"              => "ed",
             "liver_micro_intermediate_vesicular_fat" => "ed",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->addOrganFilled();
        $this->setProhibitionFlag();
        //$this->allowOneProhibition();
    }



    /**
     * set `liver_organ_filled` to true
     */
    private function addOrganFilled()
    {
        $this->setData("liver_organ_filled", 1);
    }



    /**
     * set `liver_prohibit` flag if necessary
     */
    private function setProhibitionFlag()
    {
        if (
             $this->getData("liver_prohibit_by_desc")
             //or $this->getData("liver_prohibit_by_tests")
             or $this->getData("liver_prohibit_by_sonography")
             or $this->getData("liver_prohibit_by_observation")
        ) {
            $this->setData("liver_prohibit", 1);
        }

        $this->setData("liver_prohibit", 0);
    }



    /**
     * allow one prohibition at a time
     */
    private function allowOneProhibition()
    {
        if ($this->getData("liver_prohibit_by_desc")) {
            $this->setData("liver_prohibit_by_tests", null);
            $this->setData("liver_prohibit_by_sonography", null);
            $this->setData("liver_prohibit_by_observation", null);
        }
        if ($this->getData("liver_prohibit_by_tests")) {
            $this->setData("liver_prohibit_by_desc", null);
            $this->setData("liver_prohibit_by_sonography", null);
            $this->setData("liver_prohibit_by_observation", null);
        }
        if ($this->getData("liver_prohibit_by_sonography")) {
            $this->setData("liver_prohibit_by_tests", null);
            $this->setData("liver_prohibit_by_desc", null);
            $this->setData("liver_prohibit_by_observation", null);
        }
        if ($this->getData("liver_prohibit_by_observation")) {
            $this->setData("liver_prohibit_by_tests", null);
            $this->setData("liver_prohibit_by_sonography", null);
            $this->setData("liver_prohibit_by_desc", null);
        }

    }

}
