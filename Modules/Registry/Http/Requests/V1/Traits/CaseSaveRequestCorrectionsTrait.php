<?php

namespace Modules\Registry\Http\Requests\V1\Traits;

use App\Models\Division;
use App\Models\RegistryOpu;
use App\Models\RegistryWard;
use Modules\Registry\Services\BloodType;

/**
 * Class CaseSaveRequest
 * @method bool isset(string $attribute)
 * @method mixed getData(string $attribute)
 * @method void setData(string $attribute, $value)
 * @method void unsetData(string $attribute)
 */
trait CaseSaveRequestCorrectionsTrait
{
    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveUnities();
        $this->resolveUnknownIdentities();
        $this->resolveBloodType();
        $this->resolveDivisions();
        $this->resolveGCS();
        $this->fillIdentifiedBy();
        $this->considerFullName();
        $this->resetReflexesWhenDied();
        $this->preventUreaAndBunSimultaneity();
        $this->fillCaseSituationIfNecessary();
        $this->correctOpuTransferThings(); // <~~ defined in CaseSaveRequestOpuTransferTrait
    }



    /**
     * resolve the unity instances
     */
    private function resolveUnities()
    {
        $this->ward = $this->getWardModel();
        $this->opu  = $this->getOPUModel();

        $this->unsetData("ward");
        $this->setData("ward_id", $this->ward->id);
        $this->setData("hospital_id", $this->ward->hospital_id);
        $this->setData("opu_id", $this->opu->id);
        $this->setData("university_id", $this->ward->university_id);
    }



    /**
     * get ward model
     *
     * @return RegistryWard
     */
    private function getWardModel()
    {
        $ward = $this->getData('ward');

        if (!$ward) {
            return model("registry-ward");
        }

        return model("registry-ward")->grabHashid($ward);
    }



    /**
     * get OPU model
     *
     * @return RegistryOpu
     */
    private function getOPUModel()
    {
        return $this->ward->opu_model;
    }



    /**
     * resolve unknown identities by setting the values to null
     */
    private function resolveUnknownIdentities()
    {
        if ($this->getData("unknown")) {
            $this->is_unknown = true;
            $this->setData("name_first", null);
            $this->setData("name_last", null);
            $this->setData("code_melli", null);
            $this->setData("birth_date", null);
        }
    }



    /**
     * resolve divisions
     */
    private function resolveDivisions()
    {
        $this->resolveNationality();
        $this->resolveCountry();
        $this->resolveCity();
        $this->resetCityIfNotFromIran();
    }



    /**
     * resolve Nationality
     *
     * @return void
     */
    private function resolveNationality()
    {
        /** @var Division $country */
        $country = model("division")->grabHashid($this->getData('nationality_id'));

        if ($country->type == 'country') {
            $this->setData("nationality_id", $country->id);
            $this->nationality = $country;
        }

    }



    /**
     * resolve Country
     *
     * @return void
     */
    private function resolveCountry()
    {
        /** @var Division $country */
        $country = model("division")->grabHashid($this->getData('country_id'));

        if ($country->type == 'country') {
            $this->setData("country_id", $country->id);
            $this->country = $country;
        }
    }



    /**
     * resolve City
     *
     * @return void
     */
    private function resolveCity()
    {
        /** @var Division $city */
        $city = model("division")->grabHashid($this->getData('city_id'));

        if ($city->type == 'city') {
            $this->setData("city_id", $city->id);
            $this->setData("province_id", $city->province_id);
            $this->city = $city;
        }
    }



    /**
     * reset city and province if the country is not set to Iran
     */
    private function resetCityIfNotFromIran()
    {
        if ($this->isset("country_id") and $this->getData('country_id') != 495) {
            $this->setData("province_id", 0);
            $this->setData("city_id", 0);
        }
    }



    /**
     * resolve GCS
     *
     * @return void
     */
    private function resolveGCS()
    {
        if ($this->createMode()) {
            $this->setData('gcs', $this->getData('gcs_on_identification'));
        }
    }



    /**
     * parse `blood` into `blood_type` and `blood_rh`
     */
    private function resolveBloodType()
    {
        if (!$this->isset("blood")) {
            return;
        }

        $array = BloodType::parseTypeAndRH($this->getData("blood"));

        if (!$array['type'] or !$array['rh']) {
            $this->unsetData("blood");
            return;
        }

        $this->setData("blood_type", $array['type']);
        $this->setData("blood_rh", $array['rh']);
    }



    /**
     * fill the `identified_by` field in create mode
     */
    private function fillIdentifiedBy()
    {
        if ($this->createMode()) {
            $this->setData("identified_by", user()->id);
        }
    }



    /**
     * consider a `full_name` in the data, to be filled by mutator in the model layer
     */
    private function considerFullName()
    {
        $this->setData("full_name", "");
    }



    /**
     * reset reflexes when the case is dead already
     */
    private function resetReflexesWhenDied()
    {
        $reflexes = [
             "reflex_breathing",
             "reflex_breathing_rhythm",
             "reflex_cornea",
             "reflex_pupil",
             "reflex_face",
             "reflex_face_remarks",
             "reflex_body",
             "reflex_body_remarks",
             "reflex_gag",
             "reflex_cough",
        ];

        if ($this->getData("clinical_situation") == "clinical-situation-gcs3-died") {
            foreach ($reflexes as $reflex) {
                $this->setData($reflex, "0");
            }
            $this->setData("reflex_doll", 1);
        }
    }



    /**
     * prevent `urea` and `bun` from being filled at the same time
     */
    private function preventUreaAndBunSimultaneity()
    {
        if (intval($this->getData("test_urea")) > 0) {
            $this->unsetData("test_bun");
        }

        if (intval($this->getData("test_bun")) > 0) {
            $this->unsetData("test_urea");
        }
    }



    /**
     * forcefully fill case-situation if necessary
     */
    private function fillCaseSituationIfNecessary()
    {
        if ($this->createMode() and $this->userIsInspectorClinical()) {
            $this->setData("case_situation", "case-situation-new");
        }
    }
}
