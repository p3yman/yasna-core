<?php

namespace Modules\Registry\Http\Requests\V1\Traits;

use App\Models\RegistryHospital;

trait CaseSaveRequestOpuTransferTrait
{
    /**
     * @var RegistryHospital
     */
    public $destination_hospital;

    /**
     * @var bool
     */
    private $destination_ward_accepted = true;



    /**
     * get an array of transfer fields, to be bypassed in the process of batch save
     *
     * @return array
     */
    public function transferFields()
    {
        return [
             "transfer_to_opu",
             "destination_hospital_id",
             "destination_ward_id",
             "transferred_at",
             "chief_protector_name",
             "chief_protector_degree",
             "escorting_cooperator_name",
        ];
    }



    /**
     * get validation rules regarding opu transfer section
     *
     * @return array
     */
    protected function opuTransferRules()
    {
        if ($this->createMode()) {
            return [];
        }

        if (!boolval($this->getData("transfer_to_opu"))) {
            return [
                 "transfer_to_opu"           => "boolean",
                 "destination_hospital_id"   => "",
                 "destination_ward_id"       => "",
                 "transferred_at"            => "",
                 "chief_protector_name"      => "",
                 "chief_protector_degree"    => "",
                 "escorting_cooperator_name" => "",
            ];
        }

        return [
             "transfer_to_opu"           => "boolean",
             "destination_hospital_id"   => "required|" . ($this->destination_hospital->exists ? "" : "invalid"),
             "destination_ward_id"       => $this->destination_ward_accepted ? "" : "invalid",
             "transferred_at"            => "required|date",
             "chief_protector_name"      => "required",
             "chief_protector_degree"    => "required",
             "escorting_cooperator_name" => "",
        ];
    }



    /**
     * apply corrections regarding opu transfer section
     *
     * @return void
     */
    protected function correctOpuTransferThings()
    {
        $this->preventOpuTransferInCreateMode();
        $this->resolveOpuTransferHospital();
        $this->resolveOpuTransferWard();
    }



    /**
     * prevent opu transfer in create mode
     *
     * @return void
     */
    private function preventOpuTransferInCreateMode()
    {
        if ($this->createMode()) {
            $this->setData("transfer_to_opu", false);
        }

        if ($this->case_situation->slug == "case-situation-move-to-opu") {
            $this->setData("transfer_to_opu", true);
        }
    }



    /**
     * resolve opu transfer hospital (which will lead to the province and city resolves)
     *
     * @return void
     */
    private function resolveOpuTransferHospital()
    {
        $value = $this->getData("destination_hospital_id");
        $unity = model("RegistryHospital")->grabHashid($value);

        $this->destination_hospital = $unity;
        $this->setData("destination_hospital_id", hashid($value));

        if ($unity->id == $this->model->hospital_id) {
            $this->destination_hospital = model("RegistryHospital");
        }
    }



    /**
     * resolve opu transfer ward (which will be compared with the hospital, if provided)
     *
     * @return void
     */
    private function resolveOpuTransferWard()
    {
        $value = $this->getData("destination_ward_id");

        if (!$value) {
            return;
        }

        $unity = model("RegistryWard")->grabHashid($value);
        $this->setData("destination_ward_id", hashid($value));

        if (!$unity->exists or $unity->hospital_id != $this->destination_hospital->id) {
            $this->destination_ward_accepted = false;
        }
    }
}
