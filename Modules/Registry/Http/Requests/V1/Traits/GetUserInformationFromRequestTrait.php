<?php
/**
 * Created by PhpStorm.
 * User: yasna
 * Date: 11/26/18
 * Time: 5:15 PM
 */

namespace Modules\Registry\Http\Requests\V1\Traits;


use App\Models\User;

/**
 * Trait GetUserInformationFromRequestTrait add ability of working with user
 * information given in a request.
 *
 * @package Modules\Registry\Http\Requests\V1\Traits
 */
trait GetUserInformationFromRequestTrait
{
    /**
     * @var User $requested_user The user which is requested to change his/her
     *      password.
     */
    private $requested_user;



    /**
     * Return the user model of one which is requested to change his/her
     * password.
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        if (is_null($this->requested_user)) {
            $user = $this->getUserFromRequestInformation();
            if ($user->exists) {
                $this->requested_user = $user;

                return $user;
            }

            $this->requested_user = false;

            return null;
        } elseif (is_bool($this->requested_user)) {
            return null;
        }

        return $this->requested_user;
    }



    /**
     * Get the user model from request information.
     *
     * @return User
     */
    private function getUserFromRequestInformation(): User
    {
        if ($this->filled('username')) {
            $username = $this->input('username');

            return model('user')->findByUsername($username);
        } elseif ($this->filled('user')) { // it's an hashid
            return model('user')->grabHashid($this->input('user'));
        }

        return model('user', $this->input('user_id', 0));
    }
}
