<?php

namespace Modules\Registry\Http\Requests\V1\Traits;

use App\Models\RegistryOpu;

/**
 * Class CaseSaveRequest
 *
 * @property RegistryOpu $opu
 */
trait CaseSaveRequestAuthTrait
{
    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return true; // <~~ TODO: Remove this after test!
        
        if (user()->isSuperadmin()) {
            return true;
        }

        if ($this->userIsCoordinator() or $this->userIsOpuManager() or $this->userIsInspectorTel()) {
            return true;
        }

        // now, only clinical inspector is supposed to be remained. The below condition makes sure it is true.
        if (!$this->userIsInspectorClinical()) {
            return false;
        }

        if ($this->createMode()) {
            return true;
        }

        //from this position on, only clinical inspectors in edit mode are remained unprocessed.

        $situation = "clinical-situation-gcs3-died";
        if ($this->getData("clinical_situation") == $situation and $this->model->clinical_situation != $situation) {
            return false;
        }

        return true;
    }



    /**
     * checks if the online user is a valid coordinator of the ward in question
     *
     * @return bool
     */
    private function userIsCoordinator(): bool
    {
        $coordinators = $this->opu->coordinators->pluck('id')->toArray();

        $result = in_array(user()->id, $coordinators);

        if ($result) {
            $this->setData("coordinator_id", user()->id);
        }

        return $result;
    }



    /**
     * checks if the online user is a valid expert in charge or donor expert of the ward in question
     *
     * @return bool
     */
    private function userIsExperts(): bool
    {
        $expert_in_charges = $this->opu->expertInCharges()->pluck('id')->toArray();
        $donor_expert      = $this->opu->donorExperts()->pluck('id')->toArray();

        return in_array(user()->id, $expert_in_charges) or in_array(user()->id, $donor_expert);
    }



    /**
     * checks if the online user is the opu-manager of the ward in question
     *
     * @return bool
     */
    private function userIsOpuManager(): bool
    {
        return $this->opu->currentUserIsOpuManager();
    }



    /**
     * checks if the online user is a valid inspector-tel of the ward in question
     *
     * @return bool
     */
    private function userIsInspectorTel(): bool
    {
        $inspectors = $this->opu->inspectorTels()->pluck('id')->toArray();

        $result = in_array(user()->id, $inspectors);

        if ($result) {
            $this->setData("inspector_id", user()->id);
        }

        return $result;
    }



    /**
     * checks if the online user is a valid inspector-clinical of the ward in question
     *
     * @return bool
     */
    private function userIsInspectorClinical(): bool
    {
        $inspectors = $this->opu->inspectorClinicals()->pluck('id')->toArray();

        $result = in_array(user()->id, $inspectors);

        if ($result) {
            $this->setData("inspector_id", user()->id);
        }

        return $result;
    }

}
