<?php

namespace Modules\Registry\Http\Requests\V1\Traits;

use App\Models\Combo;
use Carbon\Carbon;

trait CaseSaveRequestRulesTrait
{
    use RequestComboValidatorTrait;
    /**
     * keep the disorder cause combo instance
     *
     * @var Combo
     */
    protected $disorder_cause = null;

    /**
     * keep the clinical_situation combo instance
     *
     * @var Combo
     */
    protected $clinical_situation = null;

    /**
     * keep the case_situation combo instance
     *
     * @var Combo
     */
    protected $case_situation = null;

    /**
     * keep the identification_type combo instance
     *
     * @var Combo
     */
    protected $identification_type = null;

    /**
     * keep an array of seconder ids
     *
     * @var null|array
     */
    protected $seconders = null;

    /**
     * keep a variable to make sure the filling check is over
     *
     * @var bool
     */
    private $filling_checked = false;



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $return = array_keys($this->rules());

        $this->filling_checked = true;

        return $return;
    }



    /**
     * get validation rules for the unities
     *
     * @return array
     */
    protected function unityRules(): array
    {
        return [
             "ward" => $this->ward->exists ? "" : "accepted",
        ];
    }



    /**
     * get validation rules for code_melli
     *
     * @return array
     */
    protected function codeMelliRules(): array
    {
        $id   = $this->model->id;
        $rule = "code_melli|unique:registry_cases,code_melli,$id";

        return [
             "code_melli" => $this->isCodeMellyRequired() ? "required|$rule" : "",
        ];
    }



    /**
     * get validation rules for personal fields
     *
     * @return array
     */
    protected function personalFieldsRules()
    {
        if ($this->is_unknown) {
            $birth_date_rule = "";
        } elseif ($this->isCaseSituationUnderAction()) {
            $birth_date_rule = "required|date";
        } else {
            $birth_date_rule = "date";
        }


        return [
             "name_first"     => $this->is_unknown ? "" : "required",
             "name_last"      => $this->is_unknown ? "" : "required",
             "birth_date"     => $birth_date_rule,
             "proximate_age"  => $this->is_unknown ? "required" : "",
             "gender"         => "numeric|between:1,3",
             "unknown"        => "boolean",
             "nationality_id" => $this->is_unknown ? "" : "required",
             "country_id"     => $this->is_unknown ? "" : "required",
             "province_id"    => $this->is_unknown ? "" : "required",
             "city_id"        => $this->is_unknown ? "" : "required",
             "weight"         => "required|numeric|between:5,250",
             "height"         => "required|numeric|between:20,250",
             "blood"          => $this->isCaseSituationUnderAction() ? "required|" : "",
             "file_no"        => "required|min:3",
             "admitted_at"    => "required|date",
        ];
    }



    /**
     * emergency phone rules
     *
     * @return array
     */
    protected function emergencyPhonesRules()
    {
        return [
             "emergency_phones" => "array",
        ];
    }



    /**
     * case souvenir information rules
     *
     * @return array
     */
    protected function caseSouvenirInformationRules()
    {
        return [
             "photo" => "string",
        ];
    }



    /**
     * get validation rules for divisions
     *
     * @return array
     */
    protected function divisionalRules()
    {
        $rules = [
             "nationality_id" => $this->nationality->exists ? "" : "accepted",
             "country_id"     => ($this->isset("country_id") and !$this->country->exists) ? "accepted" : "",
             "province_id"    => "",
             "city_id"        => "",
             "address"        => "",
        ];

        if ($this->getData('country_id') == 495) {
            $rules['province_id'] = ($this->isset("province_id") and !$this->city->exists) ? "accepted" : "";
            $rules['city_id']     = ($this->isset("city_id") and !$this->city->exists) ? "accepted" : "";
        }

        return $rules;
    }



    /**
     * get GCS rules
     *
     * @return array
     */
    protected function gcsRules()
    {
        $rule = "required|numeric|between:3,15";

        return [
             "gcs_on_arrival"        => $rule,
             "gcs_on_identification" => $rule,
             "gcs"                   => $this->createMode() ? "" : $rule,
        ];
    }



    /**
     * get validation rules on clinical attributes
     *
     * @return array
     */
    protected function clinicalRules()
    {
        $cause = $this->getComboValidator("consciousness_disorder_cause", 'consciousness-disorder', true);

        $disorder_tumor = "";
        if ($this->getData('consciousness_disorder_cause_detail') == "consciousness-disorder-brain-tumors") {
            $disorder_tumor = "required";
        }

        return [
             "consciousness_disorder_cause_detail"  => $cause,
             "consciousness_disorder_tumor"         => $disorder_tumor,
             "consciousness_disorder_cause_remarks" => "",

             "is_suicide" => "required|bool",
             "remarks"    => "",

             "identification_type"           => $this->getIdentificationTypeValidator(),
             "clinical_situation"            => $this->getClinicalSituationValidator(),
             "case_situation"                => $this->getCaseSituationValidator(),
             "case_unfeasible_reason"        => $this->getCaseUnfeasibleReasonValidator(),
             "case_unfeasible_reason_others" => "required_if:case_unfeasible_reason,unfeasible-reason-others",
             "case_unfeasible_tumor"         => "required_if:case_unfeasible_reason,unfeasible-reason-brain-tumors",
        ];
    }



    /**
     * get validation rules for reflex fields
     *
     * @return array
     */
    protected function reflexFieldsRules()
    {
        return [
             "reflex_breathing" => "required|numeric|in:-1,0,1",
             "reflex_cornea"    => "required|numeric|in:-1,0,1",
             "reflex_pupil"     => "required|numeric|in:-1,0,1",
             "reflex_face"      => "required|numeric|in:-1,0,1",
             "reflex_body"      => "required|numeric|in:-1,0,1",
             "reflex_doll"      => "required|numeric|in:-1,0,1",
             "reflex_gag"       => "required|numeric|in:-1,0,1",
             "reflex_cough"     => "required|numeric|in:-1,0,1",

             "reflex_breathing_rhythm" => "required_if:reflex_breathing,1",
             "reflex_face_remarks"     => "required_if:reflex_face,1",
             "reflex_body_remarks"     => "required_if:reflex_body,1",
        ];
    }



    /**
     * get stability fields rules for stability fields
     *
     * @return array
     */
    protected function stabilityFieldsRules()
    {
        return [
             "stability_dbp"   => "required|numeric|between:0,300",
             "stability_sbp"   => "required|numeric|between:0,200",
             "stability_out"   => "required|numeric|between:0,3000",
             "stability_o2sat" => "required|numeric|between:0,100",
             "stability_fio2"  => "required|numeric|between:0,100",
             "stability_rr"    => "required|numeric|between:0,100",
             "stability_pr"    => "required|numeric|between:0,200",
             "stability_t"     => "required|numeric|between:20,50",

             "stability_sedation"        => "",
             "stability_sedation_type"   => "required_if:stability_sedation,1",
             "stability_sedation_amount" => "required_if:stability_sedation,1",
             "stability_sedation_time"   => "required_if:stability_sedation,1|date",
        ];
    }



    /**
     * get biochemistry Basic Test Rules
     *
     * @return array
     */
    protected function biochemistryBasicTestRules()
    {
        $required    = !($this->clinical_situation and $this->clinical_situation->key == "gcs4and5");
        $requirement = $required ? "required|" : "";

        return [
             "test_bs"   => $requirement . "numeric|integer|between:20,1000",
             "test_bun"  => "numeric|integer|between:0,200",
             "test_urea" => "numeric|integer|between:0,300",
             "test_cr"   => $requirement . "numeric|between:0.1,15",
             "test_wbc"  => $requirement . "numeric|integer|between:1,200000",
             "test_hgb"  => $requirement . "numeric|between:1,30",
             "test_plt"  => $requirement . "numeric|integer|between:10,900000",
             "test_ast"  => "numeric|integer|between:1,3000",
             "test_alt"  => "numeric|integer|between:1,3000",
             "test_na"   => $requirement . "numeric|integer|between:50,400",
             "test_k"    => $requirement . "numeric|between:0.1,20",
             "test_ca"   => "numeric|between:1,20",
        ];
    }



    /**
     * validation rules for biochemistry advanced tests
     *
     * @return array
     */
    protected function biochemistryAdvancedTestRules()
    {
        return [
             "test_alk_p"  => "numeric|integer|between:10,3000",
             "test_bili_t" => "numeric|integer|between:0.1,30",
             "test_bili_d" => "numeric|integer|between:0.1,30",
             "test_mg"     => "numeric|between:1,20",
             "test_pt"     => "numeric|integer|between:1,60",
             "test_ptt"    => "numeric|integer|between:1,200",
             "test_inr"    => "numeric|between:1,30",
        ];
    }



    /**
     * validation rules for bacteriology tests
     *
     * @return array
     */
    protected function bacteriologyTestsRules()
    {
        $required    = boolval($this->case_situation == "case-situation-organ-checks");
        $requirement = $required ? "required|" : "";

        return [
             "bacteria_ua"                  => $requirement . "boolean",
             "bacteria_ua_type"             => "required_if:bacteria_ua,1",
             "bacteria_ua_other"            => "",
             "bacteria_uc"                  => $requirement . "boolean",
             "bacteria_uc_type"             => "required_if:bacteria_uc,1",
             "bacteria_uc_other"            => "",
             "bacteria_bc"                  => $requirement . "boolean",
             "bacteria_bc_type"             => "required_if:bacteria_bc,1",
             "bacteria_bc_other"            => "",
             "bacteria_wound_culture"       => $requirement . "boolean",
             "bacteria_wound_culture_type"  => "required_if:bacteria_wound_culture,1",
             "bacteria_wound_culture_other" => "",
        ];
    }



    /**
     * get test time rules
     *
     * @return array
     */
    protected function testTimeRules()
    {
        return [
             'tests_group1_done_at'  => "date",
             'tests_group2_done_at'  => "date",
             'tests_group3_done_at'  => "date",
             'tests_group4_done_at'  => "date",
             'tests_group5_done_at'  => "date",
             'tests_group6_done_at'  => "date",
             'tests_group7_done_at'  => "date",
             'tests_group8_done_at'  => "date",
             'tests_group9_done_at'  => "date",
             'tests_group10_done_at' => "date",
             'tests_group11_done_at' => "date",
             'tests_group12_done_at' => "date",
        ];
    }



    /**
     * validation rules for virology tests
     *
     * @return array
     */
    protected function virologyTestsRules()
    {
        $required    = boolval($this->case_situation == "case-situation-organ-checks");
        $requirement = $required ? "required|" : "";

        return [
             "virology_hbs_ag"      => 'boolean',
             "virology_hbs_ab"      => 'numeric|between:0,2',
             "virology_hbc_ab"      => 'boolean',
             "virology_hcv_ab"      => 'boolean',
             "virology_hcv_nat"     => 'boolean',
             "virology_hiv_ab"      => 'boolean',
             "virology_hiv_nat"     => 'boolean',
             "virology_htlv_ab"     => 'boolean',
             "virology_cmv_ab_igm"  => 'boolean',
             "virology_cmv_ab_igg"  => 'boolean',
             "virology_ebv_ab_igm"  => 'boolean',
             "virology_ebv_ab_igg"  => 'boolean',
             "virology_toxo_ab_igg" => $requirement . 'boolean',
             "virology_toxo_ab_igm" => $requirement . 'boolean',
             "virology_vdrl"        => $requirement . 'boolean',
        ];
    }



    /**
     * validation rules for blood analysis
     *
     * @return array
     */
    protected function bloodAnalysisRules()
    {
        $required    = $this->getData("clinical_situation") == 'clinical-situation-gcs3-died';
        $requirement = false; //$required ? "required|" : "";

        return [
             "test_ph"    => $requirement . "numeric|between:4,9",
             "test_pco2"  => $requirement . "numeric|between:1,200",
             "test_po2"   => $requirement . "numeric|between:1,700",
             "test_be"    => $requirement . "numeric|between:-10,10",
             "test_o2sat" => $requirement . "numeric|between:1,100",
             "test_hco3"  => $requirement . "numeric|between:1,100",
        ];
    }



    /**
     * get validation rules for diseases
     *
     * @return array
     */
    protected function diseasesRules()
    {
        return [
             "hypertension"                 => "boolean",
             "hypertension_time"            => "numeric|between:0,30",
             "hypertension_is_controlled"   => "boolean",
             "hypertension_cure_edible"     => "boolean",
             "hypertension_cure_non_edible" => "boolean",

             "diabetes"                 => "boolean",
             "diabetes_time"            => "numeric|between:0,30",
             "diabetes_is_controlled"   => "boolean",
             "diabetes_cure_edible"     => "boolean",
             "diabetes_cure_non_edible" => "boolean",
        ];
    }



    /**
     * get validation rules for malignancy
     *
     * @return array
     */
    protected function malignancyRules()
    {
        return [
             "malignancy"                  => "boolean",
             "malignancy_organs"           => "array",
             "malignancy_others"           => "array",
             "malignancy_detected_at"      => "date",
             "malignancy_pathology_result" => "",
             "malignancy_pathology_scan"   => "",
             "malignancy_chemotherapy_at"  => "date",
             "craniotomy"                  => "boolean",
             "ventriculoatrial_shunt"      => "boolean",
        ];
    }



    /**
     * get validation rules for heart Diseases
     *
     * @return array
     */
    protected function heartDiseasesRules()
    {
        return [
             "heart_disease"        => "boolean",
             "heart_diseases_type"  => "required_if:heart_disease,1|" . $this->getHeartDiseasesValidator(),
             "heart_diseases_other" => "required_if:heart_diseases_type,known-heart-disease-others",
        ];
    }



    /**
     * get validation rules for surgical histories
     *
     * @return array
     */
    protected function surgicalHistoryRules()
    {
        return [
             "surgical_history"         => "boolean",
             "surgical_history_details" => "array",
             //"surgical_history_type"  => "required_if:surgical_history,1|" . $this->getSurgicalHistoryValidator(),
             //"surgical_history_other" => "required_if:surgical_history_type,surgical-history-others",
        ];
    }



    /**
     * get validation rules for habits
     *
     * @return array
     */
    protected function habitsRules()
    {
        $cigar_recent_required   = ($this->getData("cigars_took") == "boolean-yes");
        $alcohol_recent_required = ($this->getData("alcohol_took") == "boolean-yes");
        $drug_recent_required    = ($this->getData("drug_took") == "boolean-yes");
        $drug_other_required     = ($this->getData("drug_took_other") == "boolean-yes");

        return [
             "cigars_took"   => $this->getComboValidator("cigars_took", "boolean", false),
             "cigars_recent" => $this->getComboValidator("cigars_recent", "boolean", $cigar_recent_required),

             "alcohol_took"   => $this->getComboValidator("alcohol_took", "boolean", false),
             "alcohol_recent" => $this->getComboValidator("alcohol_recent", "boolean", $alcohol_recent_required),

             "drug_took"              => $this->getComboValidator("drug_took", "boolean", false),
             "drug_recent"            => $this->getComboValidator("drug_recent", "boolean", $drug_recent_required),
             "drug_took_other"        => $this->getComboValidator("drug_took_other", "boolean", false),
             "drug_took_other_type"   => $drug_other_required ? "required" : "",
             "drug_took_other_recent" => $this->getComboValidator("drug_took_other_recent", "boolean",
                  $drug_other_required),

             "had_risky_sex"   => "numeric|between:-1,1",
             "had_tattoo"      => "boolean",
             "had_prison_past" => "boolean",
        ];
    }



    /**
     * get validation rules for recovery history
     *
     * @return array
     */
    protected function recoveryRules()
    {
        return [
             'cpr_details'   => "array",
             'shock_details' => "array",
        ];
    }



    /**
     * get validation rules for medicines received
     *
     * @return array
     */
    protected function medicinesRules()
    {
        return [
             "received_antibiotics"              => "array",
             "received_antibiotics_remarks"      => "",
             "received_serum"                    => "boolean",
             "received_serum_details"            => "array|required_if:received_serum,1",
             "received_thyroid_hormone"          => "boolean",
             "received_steroid"                  => "boolean",
             "received_anticonvulsant"           => "boolean",
             "received_antihypertensives"        => $this->getAntihypertensiveValidator(),
             "received_antihypertensives_others" => "",
             "received_heparin"                  => "boolean",
             "received_vasopressin"              => "boolean",
             "received_diuretic"                 => "boolean",
             "received_insulin"                  => "boolean",
             "received_insulin_type"             => "required_if:received_insulin,1|" . $this->getInsulinTypeValidator(),
             "received_insulin_daily_dose"       => "numeric",
             "received_others"                   => "",
        ];
    }



    /**
     * get validation rules for blood Transfusion
     *
     * @return array
     */
    protected function bloodTransfusionRules()
    {
        return [
             "received_blood" => "array",
        ];
    }



    /**
     * get validation rules for infection tests
     *
     * @return array
     */
    protected function infectionTestsRules()
    {
        $array = [
             "infection_organ_engagement"              => "array",
             "infection_organ_cultivation"             => $this->getCultivationValidator(),
             "infection_organ_engagement_text"         => "",
             "infection_organ_cultivation_text"        => "",
             "infection_treatment_started_at"          => "date",
             "infection_treatment_successful"          => "boolean",
             "infection_treatment_doc"                 => "",
             "infection_treatment_positive_signs"      => "",
             "infection_treatment_negative_consultant" => "",
        ];

        if ($this->clinical_situation->slug == 'clinical-situation-gcs3-died') {
            $array["infection_treatment_positive_signs"]      = "required_if:infection_treatment_successful,1";
            $array["infection_treatment_negative_consultant"] = "";
            $array["infection_treatment_doc"]                 = "";
        }

        return $array;
    }



    /**
     * get stability fields rules for test fields
     *
     * @return array
     */
    protected function identificationTimeRules()
    {
        $field = "identified_at";
        $data  = $this->getData($field);

        if (!$data or is_numeric($data)) {
            return [
                 $field => "required",
            ];
        }

        $carbon = new Carbon($this->data[$field]);
        if ($this->createMode()) {
            if ($carbon->lessThan(now()->subDay()->startOfDay()) or $carbon->greaterThan(now())) {
                return [
                     $field => "accepted",
                ];
            }
        } elseif ($this->editMode()) {
            if ($carbon->greaterThan(now())) {
                return [
                     $field => "accepted",
                ];
            }
        }


        return [
             $field => "",
        ];
    }



    /**
     * get rules in accordance with brain death status
     *
     * @return array
     */
    protected function brainDeathRules()
    {
        return [
             "bd_detected_at" => "date",
             "bd_claimed_at"  => "date",

             "bd_confirmed_at_1" => "date",
             "bd_confirmed_at_2" => "date",
             "bd_confirmed_at_3" => "date",
             "bd_confirmed_at_4" => "date",

             "bd_confirmed_by_1" => $this->getSeconderValidator(1),
             "bd_confirmed_by_2" => $this->getSeconderValidator(2),
             "bd_confirmed_by_3" => $this->getSeconderValidator(3),
             "bd_confirmed_by_4" => $this->getSeconderValidator(4),

             "bd_confirmation_1" => $this->getData("bd_confirmed_by_1") ? "required" : "",
             "bd_confirmation_2" => $this->getData("bd_confirmed_by_2") ? "required" : "",
             "bd_confirmation_3" => $this->getData("bd_confirmed_by_3") ? "required" : "",
             "bd_confirmation_4" => $this->getData("bd_confirmed_by_4") ? "required" : "",
        ];
    }



    /**
     * get rules in accordance with brain death confirmation (story #3393)
     *
     * @return array
     */
    protected function brainDeathConfirmationRules()
    {
        if (!$this->clinical_situation or $this->clinical_situation->slug != 'clinical-situation-gcs3-died') {
            return [];
        }

        return [
             "bd_confirmed_by_1" => "",
        ];

    }



    /**
     * get rules in accordance with legal medicines confirmation (story #3393)
     *
     * @return array
     */
    protected function legalMedicinesRules()
    {
        return [
             "legal_medicine_1"              => "",
             "legal_medicine_2"              => "",
             "legal_medicine_1_confirmed_at" => "date",
             "legal_medicine_2_confirmed_at" => "date",
             "legal_medicine_1_file"         => "array",
             "legal_medicine_2_file"         => "array",
        ];
    }



    /**
     * get validation rules according to the donation agreements
     *
     * @return array
     */
    protected function agreementRules()
    {
        return [
             "has_donation_card"         => "boolean",
             "verbally_allowed_donation" => "boolean",

             "donation_agreement_confirmed_at" => "date",
             "donation_agreement_confirmed_by" => $this->getData("donation_agreement_confirmed_at") ? "required" : "",
             "donation_agreement"              => $this->getData("donation_agreement_confirmed_at") ? "required" : "",
        ];
    }



    /**
     * get validation rules for misc timestamps
     *
     * @return array
     */
    protected function timestampRules()
    {
        return [
             "died_at"                 => "date",
             "heart_death_detected_at" => "date",
             "aortic_clamped_at"       => "date",
             "donated_at"              => "date",
        ];
    }



    /**
     * get validation rules for inotrop
     *
     * @return array
     */
    protected function inotropRules()
    {
        $situation           = $this->getData("case_situation");
        $icu_requirement     = in_array($situation, [
             "case-situation-organ-checks",
             "case-situation-surgery-ready",
        ]) ? "required|" : "";
        $surgery_requirement = in_array($situation, [
             "case-situation-donated",
             "case-situation-surgery-ready",
        ]) ? "required|" : "";


        return [
             "inotrop_icu"     => $icu_requirement . "array",
             "inotrop_surgery" => $surgery_requirement . "array",
        ];
    }



    /**
     * get validation rules for donated organs and donated photo
     *
     * @return array
     */
    protected function donatedRules()
    {
        return [
             "organs"        => "array",
             "donated_photo" => "array",
        ];
    }



    /**
     * get validator rule for `identification_type`
     *
     * @return string
     */
    private function getIdentificationTypeValidator()
    {
        $field = "identification_type";
        $data  = $this->getData($field);

        if (!$data) {
            return "required";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "identification-type") {
            return "accepted";
        }

        $this->identification_type = $model;
        return "";
    }



    /**
     * get validator rule for `clinical_situation`
     *
     * @return string
     */
    private function getClinicalSituationValidator()
    {
        $field = "clinical_situation";
        $data  = $this->getData($field);

        if (!$data) {
            return "required";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "clinical-situation") {
            return "accepted";
        }

        if ($model->key == "gcs3-died") {
            $this->setAllReflexesToNegative();
        }

        $this->clinical_situation = $model;

        return "";
    }



    /**
     * set all stability status fields to negative
     */
    private function setAllReflexesToNegative()
    {
        $this->setData("reflex_breathing", -1);
        $this->setData("reflex_cornea", -1);
        $this->setData("reflex_pupil", -1);
        $this->setData("reflex_face", -1);
        $this->setData("reflex_body", -1);
        $this->setData("reflex_doll", -1);
        $this->setData("reflex_gag", -1);
        $this->setData("reflex_cough", -1);
    }



    /**
     * get validator rule for `case_situation`
     *
     * @return string
     */
    private function getCaseSituationValidator()
    {
        $field = "case_situation";
        $data  = $this->getData($field);

        if (!$data) {
            return "required";
        }
        if ($this->is_unknown and $this->isCaseSituationUnderAction()) {
            return "accepted";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "case-situation") {
            return "accepted";
        }

        $this->case_situation = $model;

        return "";

    }



    /**
     * get under action case situations
     *
     * @return array
     */
    private function getUnderActionSituations(): array
    {
        return [
             "case-situation-under-consent",
             "case-situation-move-to-opu",
             "case-situation-organ-checks",
             "case-situation-surgery-ready",
             "case-situation-donated",
        ];
    }



    /**
     * check if the case situation is under action
     *
     * @return bool
     */
    private function isCaseSituationUnderAction(): bool
    {
        $field = "case_situation";
        $data  = $this->getData($field);

        return in_array($data, $this->getUnderActionSituations());
    }



    /**
     * get validator rule for `case_unfeasible_reason`
     *
     * @return string
     */
    private function getCaseUnfeasibleReasonValidator()
    {
        $field = "case_unfeasible_reason";
        $data  = $this->getData($field);

        if (!$this->case_situation or $this->case_situation->key != "unfeasible") {
            $this->setData($field, null);
            return "";
        }

        if (!$data) {
            return "required";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "unfeasible-reason") {
            return "accepted";
        }

        return "";
    }



    /**
     * get validator rule for `heart_diseases_type`
     *
     * @return string
     */
    private function getHeartDiseasesValidator()
    {
        $field = "heart_diseases_type";
        $data  = $this->getData($field);

        if (!$data) {
            return "";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "known-heart-disease") {
            return "accepted";
        }

        return "";
    }



    /**
     * get validator rule for `surgical_history_type`
     *
     * @return string
     */
    private function getSurgicalHistoryValidator()
    {
        $field = "surgical_history_type";
        $data  = $this->getData($field);

        if (!$data) {
            return "";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "surgical-history") {
            return "accepted";
        }

        return "";
    }



    /**
     * get validator rule for `drug_amount` and `alcohol_amount`
     *
     * @param string $field
     *
     * @return string
     */
    private function geUseCourseValidator(string $field)
    {
        $data = $this->getData($field);

        if (!$data) {
            return "";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "use-course") {
            return "accepted";
        }

        return "";
    }



    /**
     * get validator rule for `received_insulin_type`
     *
     * @return string
     */
    private function getInsulinTypeValidator()
    {
        $field = "received_insulin_type";
        $data  = $this->getData($field);

        if (!$data) {
            return "";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "insulin-type") {
            return "accepted";
        }

        return "";
    }



    /**
     * get validator for Antihypertensive
     *
     * @return string
     */
    private function getAntihypertensiveValidator()
    {
        $field = "received_antihypertensives";
        $data  = $this->getData($field);

        if (!$data) {
            return "";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "Antihypertensive") {
            return "accepted";
        }

        return "";
    }



    /**
     * get validator for cultivation
     *
     * @return string
     */
    private function getCultivationValidator()
    {
        $field = "infection_organ_cultivation";
        $data  = $this->getData($field);

        if (!$data) {
            return "";
        }

        $model = combo()->grabSlug($data);

        if ($model->subject != "positive-cultivation") {
            return "accepted";
        }

        return "";
    }



    /**
     * get validator for seconders
     *
     * @param int $index
     *
     * @return string
     */
    private function getSeconderValidator($index)
    {
        $field = "bd_confirmed_by_{$index}";
        $data  = $this->getData($field);

        if (!$this->filling_checked) {
            return "";
        }

        if (!$data) {
            return $this->getData("bd_confirmed_at_{$index}") ? "required" : "";
        }

        if (is_null($this->seconders)) {
            $this->seconders = $this->opu->university_model->seconders()->get()->pluck('id')->toArray();
        }

        if (in_array(hashid($data), $this->seconders)) {
            return "";
        }

        return "accepted";
    }



    /**
     * determine if submitting code_melli is required
     *
     * @return bool
     */
    private function isCodeMellyRequired(): bool
    {
        if ($this->is_unknown) {
            return false;
        }

        if ($this->createMode() and !$this->userIsCoordinator()) {
            return false;
        }

        if ($this->getData('nationality_id') == 495) {
            return false;
        }

        return true;
    }
}
