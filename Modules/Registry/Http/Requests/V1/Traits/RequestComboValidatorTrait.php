<?php

namespace Modules\Registry\Http\Requests\V1\Traits;

use App\Models\RegistryOpu;

/**
 * Class CaseSaveRequest
 *
 * @property RegistryOpu $opu
 */
trait RequestComboValidatorTrait
{
    /**
     * get combo validator
     *
     * @param string $field
     * @param string $subject
     * @param bool   $required
     *
     * @return string
     */
    protected function getComboValidator(string $field, string $subject, ?bool $required = false): string
    {
        $data = $this->getData($field);

        if (!$data) {
            return $required ? "required" : "";
        }

        $model = combo()->grabSlug($data);
        if ($model->subject != $subject) {
            return "accepted";
        }

        return "";
    }

}
