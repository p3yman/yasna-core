<?php

namespace Modules\Registry\Http\Requests\V1\Traits;

use Illuminate\Database\Eloquent\Collection;

trait OpuHospitalRequestAssignmentTrait
{
    private $hospital_models;



    /**
     * Return list of hospitals to attach
     *
     * @return Collection
     */
    public function getHospitals(): Collection
    {
        if (!$this->hospital_models) {
            return new Collection();
        }

        return $this->hospital_models;
    }



    /**
     * Checks that the given set of universities hashids are under the given university or not.
     *
     * @return bool
     */
    public function hospitalsAreOnGivenUniversity(): bool
    {
        $hospitals = $this->hospital_models;
        $dehashed  = [];

        if (empty($hospitals)) {
            return true;
        }

        foreach ($hospitals as $hospital) {
            if (($hashid = $this->dehash($hospital)) !== false) {
                $dehashed[] = $hashid;
            } else { // to avoid passing ID instead of hashid
                return false;
            }
        }

        return $this->checkHospitalsIDsAreValid($dehashed);
    }



    /**
     * Remove hospitals from request
     */
    protected function correctHospitals()
    {
        if (isset($this->data['hospitals'])) {
            $this->hospital_models = $this->data['hospitals'];
            unset($this->data['hospitals']);
        }
    }



    /**
     * Query to DB for checking that the given hospitals are under the given university of not.
     *
     * @param array $hospitals Integer IDs of hospital for checking
     *
     * @return bool
     */
    private function checkHospitalsIDsAreValid(array $hospitals): bool
    {
        $given_counts = count($hospitals);
        $db           = $this->getHospitalModels($hospitals);
        $db_counts    = $db->count();
        $result       = $db_counts === $given_counts;

        if ($result) {
            $this->hospital_models = $db;
        }

        return $result;
    }



    /**
     * Run the query for fetching the hospitals of request.
     *
     * @param array $hospitals
     *
     * @return Collection
     */
    private function getHospitalModels(array $hospitals): Collection
    {
        return model('registry_hospital')
             ->where('university_id', $this->model->university->id)
             ->where('type', 'hospital')
             ->whereIn('id', $hospitals)
             ->get()
             ;
    }



    /**
     * Convert a given hashid to its integer equivalent. If the given parameter is integer it returns false.
     *
     * @param $hashid
     *
     * @return bool|int
     */
    private function dehash($hashid)
    {
        if (!$hashid or $hashid == (string)intval($hashid)) {
            return false; // it's empty or an integer
        }

        return hashid($hashid);
    }
}
