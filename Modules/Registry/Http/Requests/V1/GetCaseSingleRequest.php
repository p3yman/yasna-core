<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\RegistryUnityRole;
use Modules\Yasna\Services\YasnaRequest;

class GetCaseSingleRequest extends YasnaRequest
{
    protected $model_name = "registry-case";
    protected $responder  = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canView();
    }
}
