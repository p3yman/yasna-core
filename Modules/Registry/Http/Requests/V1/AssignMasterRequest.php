<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class AssignMasterRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return dev();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->model->exists and $this->model->isSuperadmin()) {
            return [];
        }

        return [
             "user" => "accepted",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($id = false)
    {
        $hashid      = $this->getData("user");
        $this->model = model("User")->grabHashid($hashid);
    }
}
