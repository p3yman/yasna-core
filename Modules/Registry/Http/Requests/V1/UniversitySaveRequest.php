<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\Division;
use App\Models\RegistryUniversity;
use Modules\Registry\Http\Requests\V1\Traits\RequestComboValidatorTrait;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class UniversitySaveRequest
 *
 * @property RegistryUniversity $model
 * @package Modules\Registry\Http\Requests\V1
 */
class UniversitySaveRequest extends YasnaRequest
{
    use RequestComboValidatorTrait;

    protected $responder  = 'white-house';
    protected $model_name = "RegistryUniversity";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "name"        => user()->isSuperadmin() ? "required" : "",
             "city_id"     => user()->isSuperadmin() ? "required" : "",
             "phones"      => "phone:fixed",
             "address"     => user()->isSuperadmin() ? "" : "required",
             "postal_code" => "",

             "university_type" => $this->getComboValidator("university_type", "university-type",false),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return ['name', 'address', 'phones', 'city', "postal_code", "website","university_type"];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->fillTypeField();
        $this->removeRestrictedData();
        $this->resolveDivisions();
    }



    /**
     * fill type field
     */
    protected function fillTypeField()
    {
        $this->data['type'] = 'university';
    }



    /**
     * resolve administrative divisions
     */
    protected function resolveDivisions()
    {
        if ($this->isset("city_id")) {
            return;
        }

        $hashid = isset($this->data['city']) ? $this->data['city'] : 0;
        unset($this->data['city']);

        /** @var Division $model */
        $model = model('division')->grabHashid($hashid);

        if ($model->type == 'city') {
            $this->data['city_id']     = $model->id;
            $this->data['province_id'] = $model->province_id;
        }
    }



    /**
     * remove restricted data from the request
     */
    protected function removeRestrictedData()
    {
        if (!user()->isSuperadmin()) {
            $this->unsetData("name");
            $this->unsetData("city_id");
        }
    }

}
