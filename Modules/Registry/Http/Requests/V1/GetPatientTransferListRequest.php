<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class GetPatientTransferListRequest extends YasnaRequest
{
    protected $responder  = 'white-house';
    protected $model_name = "registry-case";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->exists and $this->model->canView();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }
}
