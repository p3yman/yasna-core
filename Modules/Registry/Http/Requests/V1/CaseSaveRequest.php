<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\Division;
use App\Models\RegistryDemographic;
use App\Models\RegistryOpu;
use App\Models\RegistryWard;
use Modules\Registry\Http\Requests\V1\Traits\CaseSaveRequestAuthTrait;
use Modules\Registry\Http\Requests\V1\Traits\CaseSaveRequestCorrectionsTrait;
use Modules\Registry\Http\Requests\V1\Traits\CaseSaveRequestOpuTransferTrait;
use Modules\Registry\Http\Requests\V1\Traits\CaseSaveRequestRulesTrait;
use Modules\Registry\Http\Requests\V1\Traits\RequestComboValidatorTrait;
use Modules\Yasna\Services\YasnaRequest;

class CaseSaveRequest extends YasnaRequest
{
    use CaseSaveRequestRulesTrait;
    use CaseSaveRequestCorrectionsTrait;
    use CaseSaveRequestAuthTrait;
    use CaseSaveRequestOpuTransferTrait;

    /**
     * @inheritdoc
     */
    protected $responder = "white-house";

    /**
     * @inheritdoc
     */
    protected $model_name = "registry-case";


    /**
     * keep the ward model
     *
     * @var RegistryWard
     */
    protected $ward = null;

    /**
     * keep the OPU model
     *
     * @var RegistryOpu
     */
    protected $opu = null;

    /**
     * keep the value indicating if the case in question has unknown identity
     *
     * @var bool
     */
    protected $is_unknown = false;

    /**
     * keep the country of nationality
     *
     * @var Division
     */
    protected $nationality = null;

    /**
     * keep the country of residence
     *
     * @var Division
     */
    protected $country = null;

    /**
     * keep the city of residence
     *
     * @var Division
     */
    protected $city = null;



    /**
     * CaseCreationRequest constructor.
     */
    public function __construct()
    {
        if (!$this->ward) {
            $this->ward = model("registry-ward");
            $this->opu  = model("registry-opu");
        }

        $this->nationality = model('Division');
        $this->country     = model('Division');
        $this->city        = model("Division");

        $this->case_situation = combo();

        $this->destination_hospital = model("registry-hospital");

        parent::__construct();
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "blood.required" => trans("registry::validation.blood_type_invalid"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {

        return [
             "admitted_at"   => "date",
             "identified_at" => "date",

             "code_melli" => "ed",
             "gender"     => "ed",
             "birth_date" => "ed|date",
             "unknown"    => "ed",
             "weight"     => "ed",
             "height"     => "ed",

             "gcs_on_arrival"        => "ed",
             "gcs_on_identification" => "ed",

             "stability_dbp"   => "ed",
             "stability_sbp"   => "ed",
             "stability_out"   => "ed",
             "stability_o2sat" => "ed",
             "stability_fio2"  => "ed",
             "stability_rr"    => "ed",
             "stability_pr"    => "ed",
             "stability_t"     => "ed",

             "stability_sedation"      => "ed",
             "stability_sedation_time" => "ed|date",

             'tests_group1_done_at'  => "ed|date",
             'tests_group2_done_at'  => "ed|date",
             'tests_group3_done_at'  => "ed|date",
             'tests_group4_done_at'  => "ed|date",
             'tests_group5_done_at'  => "ed|date",
             'tests_group6_done_at'  => "ed|date",
             'tests_group7_done_at'  => "ed|date",
             'tests_group8_done_at'  => "ed|date",
             'tests_group9_done_at'  => "ed|date",
             'tests_group10_done_at' => "ed|date",
             'tests_group11_done_at' => "ed|date",
             'tests_group12_done_at' => "ed|date",

             "test_bs"     => "ed",
             "test_bun"    => "ed",
             "test_urea"   => "ed",
             "test_cr"     => "ed",
             "test_wbc"    => "ed",
             "test_hgb"    => "ed",
             "test_plt"    => "ed",
             "test_ast"    => "ed",
             "test_alt"    => "ed",
             "test_alk_p"  => "ed",
             "test_bili_t" => "ed",
             "test_bili_d" => "ed",
             "test_na"     => "ed",
             "test_k"      => "ed",
             "test_ca"     => "ed",
             "test_mg"     => "ed",
             "test_pt"     => "ed",
             "test_ptt"    => "ed",
             "test_inr"    => "ed",
             "test_ph"     => "ed",
             "test_pco2"   => "ed",
             "test_po2"    => "ed",
             "test_be"     => "ed",
             "test_o2sat"  => "ed",
             "test_hco3"   => "ed",

             "received_insulin_daily_dose"    => "ed",
             "received_blood_platelet"        => "ed",
             "received_blood_packed_cell"     => "ed",
             "received_blood_cryopercipitate" => "ed",
             "received_blood_ffp"             => "ed",
             "received_blood_whole"           => "ed",

             "infection_treatment_started_at" => "date",

             "bd_detected_at"    => "date",
             "bd_claimed_at"     => "date",
             "bd_confirmed_at_1" => "date",
             "bd_confirmed_at_2" => "date",
             "bd_confirmed_at_3" => "date",
             "bd_confirmed_at_4" => "date",

             "donation_agreement_confirmed_at" => "date",
             "died_at"                         => "date",
             "heart_death_detected_at"         => "date",
             "aortic_clamped_at"               => "date",
             "donated_at"                      => "date",

             "malignancy_detected_at"     => "date",
             "malignancy_chemotherapy_at" => "date",

             "legal_medicine_1_confirmed_at" => "date",
             "legal_medicine_2_confirmed_at" => "date",

             "transferred_at" => "ed|date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->encodemalignancyOrgans();
        $this->arrayToStringConversions();
        $this->dehashSeconders();
        $this->nullifyEmptyTests();
    }



    /**
     * indicate if the request is in edit mode
     *
     * @return bool
     */
    protected function editMode()
    {
        $handle = $this->getData('id');
        if (!$handle) {
            $handle = $this->getData('hashid');
        }

        $id = hashid_number($handle);

        return boolval($id);
    }



    /**
     * indicate if the request is in create mode
     *
     * @return bool
     */
    protected function createMode()
    {
        return !$this->editMode();
    }



    /**
     * dehash the seconder ids
     *
     * @return void
     */
    private function dehashSeconders()
    {
        $this->setData("bd_confirmed_by_1", hashid($this->getData("bd_confirmed_by_1")));
        $this->setData("bd_confirmed_by_2", hashid($this->getData("bd_confirmed_by_2")));
        $this->setData("bd_confirmed_by_3", hashid($this->getData("bd_confirmed_by_3")));
        $this->setData("bd_confirmed_by_4", hashid($this->getData("bd_confirmed_by_4")));

        $this->setData("donation_agreement_confirmed_by", hashid($this->getData("donation_agreement_confirmed_by")));
    }



    /**
     * nullify empty tests
     *
     * @return void
     */
    private function nullifyEmptyTests()
    {
        $tests = [
             "test_bs",
             "test_bun",
             "test_urea",
             "test_cr",
             "test_wbc",
             "test_hgb",
             "test_plt",
             "test_ast",
             "test_alt",
             "test_alk_p",
             "test_bili_t",
             "test_bili_d",
             "test_na",
             "test_k",
             "test_ca",
             "test_mg",
             "test_pt",
             "test_ptt",
             "test_inr",
             "test_ph",
             "test_pco2",
             "test_po2",
             "test_be",
             "test_o2sat",
             "test_hco3",
        ];

        foreach ($tests as $test) {
            if (empty($this->data[$test])) {
                $this->data[$test] = null;
            }
        }
    }



    /**
     * convert all arrays to strings, ready to be saved in the database
     */
    private function arrayToStringConversions()
    {
        $array_fields = array_merge(RegistryDemographic::arrayClues(), RegistryDemographic::fileClues());
        $meta_fields  = $this->model->getMetaFields();

        foreach ($array_fields as $field) {
            $value = $this->getData($field);
            if (!is_array($value) or in_array($field, $meta_fields)) {
                continue;
            }

            $this->setData($field, implode(",", $value));
        }
    }



    /**
     * json encode `malignancy_organs`
     */
    private function encodemalignancyOrgans()
    {
        $value = $this->getData('malignancy_organs');

        if (is_array($value)) {
            $this->setData("malignancy_organs", json_encode($value));
        }
    }
}
