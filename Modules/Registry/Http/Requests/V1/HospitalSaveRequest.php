<?php

namespace Modules\Registry\Http\Requests\V1;


use App\Models\RegistryHospital;
use Modules\Registry\Http\Requests\V1\Traits\RequestComboValidatorTrait;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class HospitalSaveRequest
 *
 * @package Modules\Registry\Http\Requests\V1
 * @property RegistryHospital $model
 */
class HospitalSaveRequest extends YasnaRequest
{
    use RequestComboValidatorTrait;

    protected $model_name = "registry-hospital";
    protected $responder  = "white-house";

    private $death_counts = [];



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * general Validation Rules
     *
     * @return array
     */
    public function generalRules()
    {
        $rule = $this->getComboValidator("intestine_prohibit_by_desc", "intestine-prohibit-desc");

        return [
             "name"                  => "required|string",
             "city"                  => "string",
             "university"            => isset($this->data['university']) ? "accepted" : "",
             "address"               => "string",
             "postal_code"           => "string",
             "phones"                => "string|phone:fixed",
             "website"               => "url",
             "neurosurgery_services" => "required|boolean",
             "neurological_services" => "required|boolean",
             "trauma_center"         => "required|boolean",
             "opu_distance"          => "required|numeric|min:0",
             "bed_counts"            => "required|numeric|min:0",
             "latitude"              => "required_with:longitude|numeric|min:-90|max:90",
             "longitude"             => "required_with:latitude|numeric|min:-180|max:180",
        ];
    }



    /**
     * get the validation rules with regards of hospital sectors
     *
     * @return array
     */
    public function sectorRules()
    {
        return [
             "sector_type"  => $this->getComboValidator("sector_type", "hospital-sector", true),
             "sector_other" => "required_if:sector_type,hospital-sector-others",
        ];
    }



    /**
     * OPU Validation Rules
     *
     * @return array
     */
    public function opuValidationRules()
    {
        if ($this->model->opu_id) {
            return [
                 "opu" => "required|accepted",
            ];
        }

        if (isset($this->data['opu']) and !empty($this->data['opu'])) {
            return [
                 "opu" => "accepted",
            ];
        }

        return [];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields(): array
    {
        return [
             "name",
             "city",
             "university",
             "opu",
             "address",
             "postal_code",
             "phones",
             "website",
             "sector_type",
             "sector_other",
             "neurosurgery_services",
             "neurological_services",
             "trauma_center",
             "opu_distance",
             "bed_counts",
             "death_counts",
             "latitude",
             "longitude",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveUniversity();
        $this->fillTypeField();
        $this->resolveDivisions();
        $this->correctSectorType();
        $this->removeDeathCounts();
        $this->resolveOpu();
    }



    /**
     * Return array contains information of death counts.
     *
     * @return array
     */
    public function getDeathCounts(): array
    {
        return $this->death_counts;
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "university.accepted" => trans("registry::validation.university_accepted"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * fill type field
     */
    protected function fillTypeField()
    {
        $this->data['type'] = 'hospital';
    }



    /**
     * resolve administrative divisions
     */
    protected function resolveDivisions()
    {
        $hashid = isset($this->data['city']) ? $this->data['city'] : 0;
        unset($this->data['city']);

        /** @var \Modules\Divisions\Entities\Division $model */
        $model = model('division')->grabHashid($hashid);

        if ($model->type == 'city') {
            $this->data['city_id']     = $model->id;
            $this->data['province_id'] = $model->province_id;
        }
    }



    /**
     * convert hashid of university to int
     */
    protected function resolveUniversity()
    {
        if(!$this->isset("university")) {
            return;
        }

        $hashid     = isset($this->data['university']) ? $this->data["university"] : 0;
        $university = model('registry-university')->grabHashid($hashid);

        $this->data['university_id'] = $university->id;
        $this->model->university_id  = $university->id;

        if ($university->exists) {
            unset($this->data["university"]);
        }
    }



    /**
     * resolve the optionally-entered opu
     */
    protected function resolveOpu()
    {
        if (!isset($this->data['opu'])) {
            return;
        }

        $opu = model("registry-opu")->grabHashid($this->data['opu']);

        if ($opu->exists) {
            $this->data['opu_id'] = $opu->id;
            $this->data['opu']    = true;
        }
    }



    /**
     * Remove sector type for governmental sectors
     */
    protected function correctSectorType()
    {
        if ($this->filled('sector_type') and $this->input('sector_type') != 'hospital-sector-others') {
            $this->data['sector_other'] = null;
        }
    }


    /**
     * remove death counts from data array.
     */
    protected function removeDeathCounts()
    {
        if (isset($this->data['death_counts'])) {
            $death_counts = $this->data['death_counts'];

            foreach ($death_counts as $index => $death_count) {
                if (!isset($death_count['year']) or (empty($death_count['year']) and $death_count['year'] != 0)) {
                    unset($death_counts[$index]);
                    continue;
                }

                $death_counts[$index]['year'] = jdate(intval($death_count['year']))->format('Y');
            }

            $this->death_counts = $death_counts;

            unset($this->data['death_counts']);
        }
    }
}
