<?php

namespace Modules\Registry\Http\Requests\V1;


use App\Models\User;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class SuperadminAssignSupervisorRequest
 * <br>
 * This is the request to assign a user as the supervisor of one or more universities.
 */
class SuperadminDeleteRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        if (user()->id == $this->model->id) {
            return false;
        }

        return true;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        if ($this->model->exists and $this->model->isSuperadmin()) {
            return [];
        }

        return [
             "user" => "accepted",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($hashid = false)
    {
        $this->data['model'] = $this->model = model("user")->grabHashid($this->getData('user'));
    }

}
