<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class PatientTransferSaveRequest extends YasnaRequest
{
    protected $responder  = 'white-house';
    protected $model_name = "registry-patient-transfer";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * @inheritdoc
     */
    public function generalRules()
    {
        $final_states = array_keys(trans("registry::combo.final-state"));
        array_walk($final_states, function(&$v) { $v = "final-state-$v"; } );
        $final_states    = implode(',', $final_states);

        $transfer_reason = array_keys(trans("registry::combo.transmission-reason"));
        array_walk($transfer_reason, function(&$v) { $v = "transmission-reason-$v"; } );
        $transfer_reason = implode(',', $transfer_reason);

        $dismiss_reason = array_keys(trans("registry::combo.not-transmission-reason"));
        array_walk($dismiss_reason, function(&$v) { $v = "not-transmission-reason-$v"; } );
        $dismiss_reason  = implode(',', $dismiss_reason);

        $next_action = array_keys(trans("registry::combo.next-operations"));
        array_walk($next_action, function(&$v) { $v = "next-operations-$v"; } );
        $next_action     = implode(',', $next_action);

        return [
             'status' => 'required|string|in:pending,confirmed,dismissed',

             'transfer_reason'         => 'inconsistent_with:dismiss_transfer_reason|string|in:' . $transfer_reason,
             'dismiss_transfer_reason' => 'required_if:status,dismissed|inconsistent_with:transfer_reason|string|in:' . $dismiss_reason,
             'transfer_next_action'    => 'array',
             'transfer_next_action.*'  => 'string|in:' . $next_action,
             'case_final_state'        => 'required_if:status,dismissed|string|in:' . $final_states,
             'transferred_at'          => 'required_if:status,confirmed|int|min:1000000000',
        ];
    }



    /**
     * city rules
     *
     * @return array
     */
    public function cityRules()
    {
        return [
             'city' => 'accepted' // <~~ missed parameter
                  . '|not_in:yes' // <~~ invalid given data
        ];
    }



    /**
     * destination hospital rules
     *
     * @return array
     */
    public function destinationHospitalRules()
    {
        return [
             'hospital' => 'accepted' // <~~ missed parameter
                  . '|not_in:yes' // <~~ invalid given data
        ];
    }



    /**
     * case rules
     *
     * @return array
     */
    public function caseRules()
    {
        return [
             'case' => 'accepted' // <~~ missed parameter
                  . '|not_in:on' // <~~ invalid given data
                  . '|max:2' // <~~ has open transfer request
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "case.accepted"     => trans("registry::validation.required_transfer_case"),
             "case.not_in"       => trans("registry::validation.invalid_transfer_case"),
             "case.max"          => trans("registry::validation.has_open_transfer_case"),
             "city.accepted"     => trans("registry::validation.required_transfer_city"),
             "city.not_in"       => trans("registry::validation.invalid_transfer_city"),
             "hospital.accepted" => trans("registry::validation.required_transfer_destination_hospital"),
             "hospital.not_in"   => trans("registry::validation.invalid_transfer_destination_hospital"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             'case',
             'city',
             'hospital',
             'status',
             'transfer_reason',
             'dismiss_transfer_reason',
             'transfer_next_action',
             'case_final_state',
             'transferred_at',
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             'case'           => 'ed',
             'city'           => 'ed',
             'hospital'       => 'ed',
             'transferred_at' => 'ed',
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->setCaseInformation();
        $this->setDivisionsInformation();
        $this->setHospitalsInformation();
        $this->fillOriginHospitalOnCreateRequest();
    }



    /**
     * Fill case information
     */
    private function setCaseInformation()
    {
        if ($this->isEditRequest()) {
            $this->setData('case', 1);
            return;
        }

        if (!$this->isset('case')) {
            $this->setData('case', 0);
            return;
        }

        $hashid = $this->getData('case');
        $case   = model('RegistryCase')->grabHashid($hashid);

        if ($case->exists) {
            $has_request = model('RegistryPatientTransfer')->whereCaseId($case->id)->whereStatus('pending')->exists();
            $this->setData('origin_hospital_id', $case->hospital_id); // <~~ will be used on create situations

            if ($has_request) {
                $this->setData('case', 'yes');
                return;
            }

            $this->setData('case', 1);
            $this->setData('case_id', $case->id);
        } else {
            $this->setData('case', 'on');
        }
    }



    /**
     * fill division information
     */
    private function setDivisionsInformation()
    {
        if ($this->isEditRequest()) {
            $this->setData('city', 1);
            return;
        }

        if (!$this->isset('city')) {
            $this->setData('city', 0);
            return;
        }

        $id   = hashid($this->getData('city'));
        $city = model('division')->whereCityId(0)->whereId($id)->first();

        if ($city) {
            $this->setData('city', 1);
            $this->setData('city_id', $city->id);
            $this->setData('province_id', $city->province_id);
        } else {
            $this->setData('city', 'yes');
        }
    }



    /**
     * Fill hospitals information
     */
    private function setHospitalsInformation()
    {
        if ($this->isEditRequest()) {
            $this->setData('hospital', 1);
            return;
        }

        if (!$this->isset('hospital')) {
            $this->setData('hospital', 0);
            return;
        }

        $city     = $this->getData('city_id') ?? 0;
        $hashid   = hashid($this->getData('hospital'));
        $hospital = model('registry_hospital')->whereId($hashid)->whereCityId($city)->first();

        if ($hospital && $hospital->id != $this->getData('origin_hospital_id')) {
            $this->setData('hospital', 1);
            $this->setData('destination_hospital_id', $hospital->id);
        } else {
            $this->setData('hospital', 'yes');
        }
    }



    /**
     * origin hospital will used on checking permissions
     */
    private function fillOriginHospitalOnCreateRequest()
    {
        if ($this->isCreateRequest()) {
            $this->model->origin_hospital_id = $this->getData('origin_hospital_id');
        }
    }



    /**
     * Indicate this request is an edit request
     *
     * @return bool
     */
    private function isEditRequest(): bool
    {
        return $this->model->exists;
    }



    /**
     * Indicate this request is a create request
     *
     * @return bool
     */
    private function isCreateRequest(): bool
    {
        return !$this->isEditRequest();
    }
}
