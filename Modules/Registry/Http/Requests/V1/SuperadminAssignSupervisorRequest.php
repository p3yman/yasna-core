<?php

namespace Modules\Registry\Http\Requests\V1;


use App\Models\User;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class SuperadminAssignSupervisorRequest
 * <br>
 * This is the request to assign a user as the supervisor of one or more universities.
 */
class SuperadminAssignSupervisorRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * Returns the validation rules about the universities.
     *
     * @return array
     */
    public function universitiesRules()
    {
        return [
             'universities'   => 'required|array',
             'universities.*' => Rule::exists('registry_unities', 'id')
                                     ->where('type', 'university')
             ,
        ];
    }



    /**
     * Returns the user validation rules when specific user is given with expressly supplied hashid
     *
     * @return array
     */
    public function specificUserRules()
    {
        if (!$this->askedForSpecificUser() or $this->data['user']->exists) {
            return [];
        }

        return [
             "user" => "accepted",
        ];
    }



    /**
     * Returns the validation rules when user information is given by a bunch of data
     *
     * @return array
     */
    public function unspecificUserRules()
    {
        if ($this->askedForSpecificUser()) {
            return [];
        }

        return $this->runningModule()->getConfig('roles.university-supervisor.fields');
    }



    /**
     * Checks if the request specifies a user by expressly giving their hashid
     *
     * @return bool
     */
    public function askedForSpecificUser()
    {
        return isset($this->data['user']);
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return $this->runningModule()->getConfig('roles.purification_rules');
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->correctUniversities();
        $this->resolveUser();
    }



    /**
     * resolve user model
     *
     * @return void
     */
    private function resolveUser()
    {
        if (isset($this->data['user'])) {
            $this->data['user'] = user()->grabHashid($this->data['user']);
        }
    }



    /**
     * Corrects the `universities` array.
     * <br>
     * This array should have been filled with valid hashids of at least university.
     */
    protected function correctUniversities()
    {
        $hashids = ($this->data['universities'] ?? []);

        if (!is_array($hashids)) {
            return;
        }

        $ids = array_map('hashid', $hashids);

        $this->data['universities'] = array_filter($ids);
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        $module                  = $this->runningModule();
        $general_attributes      = $module->getTrans('fields');
        $universities_attributes = [
             'universities.*' => $module->getTrans('validation.attributes.university_id'),
        ];

        return array_merge($general_attributes, $universities_attributes);
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'user.accepted'         => $this->runningModule()->getTrans('validation.user_not_found'),
             'universities.required' => $this->runningModule()->getTrans('validation.universities_required'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        $array1 = array_keys($this->unspecificUserRules());
        $array2 = ["user", "universities"];

        return array_merge($array1, $array2);
    }
}
