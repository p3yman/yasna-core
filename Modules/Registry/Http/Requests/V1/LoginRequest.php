<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class LoginRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * get the main validation rules
     *
     * @return array
     */
    public function mainRules()
    {
        return [
             "username"             => "required|size:10",
             'g-recaptcha-response' => debugMode() ? '' : 'required|captcha',
        ];
    }



    /**
     * get the validation rules related to the password.
     *
     * @return array
     */
    public function passwordRules()
    {
        $rules = "required";

        if (isset($this->data['password']) and !user()->checkPasswordSeverity($this->data['password'])) {
            $rules .= "|accepted";
        }

        return [
             "password" => $rules,
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "username.size"     => trans("auth.failed"),
             "password.accepted" => trans("auth.failed"),
        ];
    }
}
