<?php
/**
 * Created by PhpStorm.
 * User: emitis
 * Date: 12/9/18
 * Time: 4:05 PM
 */

namespace Modules\Registry\Http\Requests\V1;


use App\Models\Division;
use Illuminate\Database\Eloquent\Collection;
use Modules\Yasna\Services\YasnaRequest;

class UserEditRequest extends YasnaRequest
{

    protected $model_with_trashed = false;
    protected $responder          = 'white-house';

    protected $unity_roles;
    protected $assignable_roles = [];



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->canAssignAnyRole();
    }



    /**
     * Checks if logged in user has the permission to assign any of editing user's roles.
     *
     * @return bool
     */
    protected function canAssignAnyRole()
    {
        $unity_roles      = $this->unityRoles();
        $assignable_roles = [];

        foreach ($unity_roles as $unity_role) {
            $unity = $unity_role->unity;
            $role  = $unity_role->role;

            if ($unity->canAssign($role)) {
                $assignable_roles[] = $role;
            }
        }

        $this->assignable_roles = array_unique($assignable_roles);

        return !empty($assignable_roles);
    }



    /**
     * Returns the general validation rules.
     *
     * @return array
     */
    protected function generalRules()
    {
        return [
             'code_melli' => 'required_without:hashid',
        ];
    }



    /**
     * Returns the validation rules based on the editing user's roles.
     *
     * @return array
     */
    public function rolesRules()
    {
        $roles_slugs = $this->assignable_roles;
        $rules_array = [];

        if (empty($roles_slugs)) {
            return $rules_array;
        }

        foreach ($roles_slugs as $role_slug) {
            $role_rules  = $this->runningModule()->getConfig("roles.$role_slug.fields");
            $rules_array = $this->ruleMerger($rules_array, $role_rules);
        }

        return $rules_array;
    }



    /**
     * Returns a collection of the editing user's unity roles.
     *
     * @return Collection
     */
    protected function unityRoles()
    {
        if (is_null($this->unity_roles)) {
            $this->unity_roles = $this->model->unityRoles;
        }

        return $this->unity_roles;
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveDivisions();
        $this->resolveUser();
        $this->resolveModelInformation();
        $this->correctHashids();
    }



    /**
     * resolve divisions
     */
    protected function resolveDivisions()
    {
        $this->resolveCity();
        $this->resolveProvince();
    }



    /**
     * resolve province
     */
    protected function resolveProvince()
    {
        $value = hashid_number($this->getData("province"));
        /** @var Division $division */
        $division = model('division', $value);

        if (!$division->isProvince()) {
            $this->unsetData("province");
            return;
        }

        $this->setData("province", $division->id);
    }



    /**
     * resolve city
     */
    protected function resolveCity()
    {
        $value = hashid_number($this->getData("city"));
        /** @var Division $division */
        $division = model('division', $value);

        if (!$division->isCity()) {
            $this->unsetData("city");
            return;
        }

        $this->setData("city", $division->id);
    }



    /**
     * Resolves the model.
     */
    protected function resolveModelInformation()
    {
        if (!isset($this->data['hashid'])) {
            return;
        }

        $this->model_name = 'user';

        $this->loadRequestedModel();
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return $this->runningModule()->getConfig('roles.purification_rules');
    }



    /**
     * Resolves the user.
     */
    protected function resolveUser()
    {
        if ($this->input('hashid')) {
            return;
        }

        if (isset($this->data['code_melli'])) {
            $this->data['hashid'] = model('user')
                 ->findByUsername($this->data['code_melli'])
                 ->hashid
            ;
        }
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return $this->runningModule()->getTrans('fields');
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             'code_melli.required_without' => $this->runningModule()->getTrans('validation.user_not_found'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_keys($this->purifier());
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->convertLetterFileToString();
    }



    /**
     * dehash all hashid fields which must saved directly on db
     */
    protected function correctHashids()
    {
        $hashids = ['work_unity'];

        foreach ($hashids as $hashid) {
            if ($this->isset($hashid)) {
                $this->setData($hashid, hashid($this->getData($hashid)));
            }
        }
    }



    /**
     * convert `letter_file` to comma-separated string
     */
    protected function convertLetterFileToString()
    {
        $value = $this->getData("letter_file");

        if (!$value or !is_array($value)) {
            return;
        }

        $this->setData("letter_file", implode(",", $value));
    }
}
