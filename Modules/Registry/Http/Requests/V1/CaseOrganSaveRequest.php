<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\RegistryCase;
use Modules\Registry\Http\Requests\V1\Traits\RequestComboValidatorTrait;
use Modules\Yasna\Services\YasnaRequest;
use App\Models\RegistryUnityRole;
use App\Models\User;
use App\Models\RegistryDemographic;

/**
 * Class CaseOrganSaveRequest
 *
 * @property RegistryCase $model
 */
abstract class CaseOrganSaveRequest extends YasnaRequest
{
    use RequestComboValidatorTrait;
    /**
     * @inheritdoc
     */
    protected $responder = 'white-house';

    /**
     * @inheritdoc
     */
    protected $model_name = "registry-case";



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        $opu = $this->model->opu;
        if (!$opu) {
            return false;
        }

        return $opu->hasAnyUnityRoles([
             RegistryUnityRole::ROLE_OPU_MANAGER,
             RegistryUnityRole::ROLE_COORDINATOR,
             RegistryUnityRole::ROLE_CHIEF_COORDINATOR,
             RegistryUnityRole::ROLE_INSPECTOR_TEL,
        ]);
    }



    /**
     * get validation rule for the organ which is form tab name herein
     *
     * @return array
     */
    public function organRules()
    {
        return [
             "organ" => "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return array_keys($this->rules());
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * @inheritdoc
     */
    public function mutators()
    {
        $this->implodeArrayFiles();
        $this->unsetData("organ");
    }



    /**
     * get consultant validator
     *
     * @param string $field
     * @param bool   $required
     *
     * @return string
     */
    protected function getConsultantValidator(string $field, bool $required = false): string
    {
        $data = $this->getData($field);

        if (!$data or is_null($this->model->university_id)) {
            return $required ? "required" : "";
        }

        /** @var User $consultant */
        $consultant = user()->grabHashid($data);

        if (
             $consultant->exists or
             $consultant->hasUnityRole($this->model->university_id, RegistryUnityRole::ROLE_CONSULTANT)
        ) {
            return "";
        }

        return "accepted";
    }



    /**
     * implode all file arrays into a comma-separated list
     */
    private function implodeArrayFiles()
    {
        $file_fields = RegistryDemographic::fileClues();

        foreach ($file_fields as $field) {
            $value = $this->getData($field);
            if (!is_array($value)) {
                continue;
            }

            $value = implode(",", $value);
            $this->setData($field, $value);
        }
    }

}
