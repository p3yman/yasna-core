<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class UnityDeactivateRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canDeactivate();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "unity"   => $this->model->exists ? "" : "accepted",
             "type"    => "required", //<~~ never happens!
             "restore" => "integer|in:0,1",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "role.required"  => trans("registry::validation.role_not_found"),
             "unity.accepted" => trans("registry::validation.unity_not_found"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveModelName();
        $this->resolveUnity();
    }



    /**
     * resolve $this->model_name, based on the given type
     *
     * @return void
     */
    private function resolveModelName()
    {
        $this->model_name = studly_case("registry-" . $this->input('type', '0'));
    }



    /**
     * resolve the unity
     *
     * @return void
     */
    private function resolveUnity()
    {
        $restore = $this->input('restore', false);
        $this->model  = model($this->model_name);
        if ($restore) {
            $this->model = $this->model->whereNotNull('deactivated_at');
        }
        $this->model = $this->model->grabHashid($this->input('hashid', '0'));
    }

}
