<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class GetUserFieldsRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "user" => $this->model->exists ? "" : "accepted",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "user.accepted" => trans("registry::validation.user_not_found"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveUser();
    }



    /**
     * get merged field rules of a specific user
     *
     * @return array
     */
    public function getMergedFieldRules()
    {
        $array = [];
        $roles = $this->model->unityRolesArray();

        foreach ($roles as $role) {
            $second = (array)config("registry.roles.$role.fields");
            $array  = $this->ruleMerger($array, $second);
        }

        return $array;
    }



    /**
     * resolve user by hashid, code_melli or simply by getting the online user, respectively
     */
    private function resolveUser()
    {
        if (isset($this->data['hashid'])) {
            $this->model = user()->grabHashid($this->data['hashid']);
            return;
        }

        if (isset($this->data['code_melli'])) {
            $this->model = user()->findByUsername($this->data['hashid']);
            return;
        }

        $this->model = user();
        return;
    }
}
