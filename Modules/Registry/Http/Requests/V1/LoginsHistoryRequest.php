<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class LoginsHistoryRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = "white-house";



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "sort"     => "in:asc,desc",
             "criteria" => "in:all,valid,invalid",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "to"   => "date",
             "from" => "date",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->setData("user", user()->id);
    }
}
