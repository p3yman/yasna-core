<?php

namespace Modules\Registry\Http\Requests\V1;

use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Modules\Yasna\Services\YasnaRequest;

class SuperadminSaveRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = "white-house";

    /**
     * @inheritdoc
     */
    protected $model_name = "user";



    /**
     * get validation rules, when a new superadmin record is being created
     *
     * @return array
     */
    public function creationRules()
    {
        if ($this->getData('id')) {
            return [];
        }

        if (!$this->getData("password")) {
            $this->setData("password", $this->getData("mobile"));
            $this->setData("password_force_change", 1);
        }

        return [
             "name_first" => "required",
             "name_last"  => "required",
             "gender"     => "required",
             "mobile"     => "required",
             "code_melli" => "required",
        ];
    }



    /**
     * get validation rules, when an existing superadmin is being edited
     *
     * @return array
     */
    public function editionRules()
    {
        if (!$this->getData('id')) {
            return [];
        }

        if ($this->model->exists and $this->model->isSuperadmin()) {
            return [];
        }

        return [
             "id" => "accepted",
        ];
    }



    /**
     * get validation rules, common for edit and create
     *
     * @return array
     */
    public function commonRules()
    {
        $password_rule = "";
        if ($this->getData('password') and !user()->checkPasswordSeverity($this->getData('password'))) {
            $password_rule = "accepted";
        }

        if ($this->getData('password')) {
            $crypt = Hash::make($this->getData('password'));
            $this->setData("password", $crypt);
        }

        return [
             "gender"     => "numeric|between:1,3",
             "mobile"     => "phone:mobile",
             "code_melli" => $this->isCodeMelliUnique() ? "code_melli" : "code_melli|accepted",
             "password"   => $password_rule,
        ];
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "id",
             "name_first",
             "name_last",
             "gender",
             "mobile",
             "code_melli",
             "password",
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return trans("registry::fields");
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "code_melli.accepted" => trans("registry::validation.code_melli_duplicate"),
        ];
    }



    /**
     * @inheritdoc
     */
    protected function loadRequestedModel($hashid = false)
    {
        $this->data['model'] = $this->model = model("user")->grabHashid($this->getData('id'));
    }



    /**
     * check if the given code melli is unique
     *
     * @return bool
     */
    private function isCodeMelliUnique()
    {
        $total = user()
             ->where("code_melli", $this->getData("code_melli"))
             ->where("id", "!=", $this->model->id)
             ->count()
        ;

        return !boolval($total);
    }
}
