<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\Division;
use App\Models\RegistryCase;
use App\Models\RegistryOpu;
use App\Models\RegistryWard;
use Modules\Registry\Http\Requests\V1\Traits\CaseSaveRequestAuthTrait;
use Modules\Registry\Http\Requests\V1\Traits\CaseSaveRequestCorrectionsTrait;
use Modules\Registry\Http\Requests\V1\Traits\CaseSaveRequestRulesTrait;
use Modules\Yasna\Services\YasnaRequest;

/**
 * Class CaseFetchRequest
 *
 * @property RegistryCase $model;
 */
class CaseFetchRequest extends YasnaRequest
{
    /**
     * @inheritdoc
     */
    protected $responder = "white-house";

    /**
     * @inheritdoc
     */
    protected $model_name = "registry-case";

    /**
     * @inheritdoc
     */
    protected $model_with_trashed = false;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canView();
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        if (!$this->model->exists) {
            return [
                 "id" => "accepted",
            ];
        }

        return [];
    }
}
