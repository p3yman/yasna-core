<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;


class SimilarCasesRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "code_melli" => "code_melli",
             "name_first" => "",
             "name_last"  => "",
        ];
    }



    /**
     * @inheritdoc
     */
    public function purifier()
    {
        return [
             "code_melli" => "ed",
        ];
    }
}
