<?php

namespace Modules\Registry\Http\Requests\V1;

use Modules\Yasna\Services\YasnaRequest;

class WardSaveRequest extends YasnaRequest
{
    protected $model_name = "registry-ward";
    protected $responder  = "white-house";

    private $death_counts;



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        return $this->model->canCreateOrEdit();
    }



    /**
     * @inheritdoc
     */
    public function fillableFields()
    {
        return [
             "hospital",
             "name",
             "bed_counts",
             //"death_counts",
        ];
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "name"          => "required|string",
             "hospital_id"   => "required|int",
             "hospital"      => isset($this->data['hospital']) ? "accepted" : "",
             "province_id"   => "required|int",
             "city_id"       => "required|int",
             "university_id" => "required|int",
             "bed_counts"    => "int",
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->fillValuesFromGivenHospital();
        $this->fillType();
        //$this->removeDeathCounts();
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "hospital.accepted" => trans("registry::validation.hospital_accepted"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
             "bed_counts" => trans("registry::validation.attributes.bed_counts"),
        ];
    }



    /**
     * return death counts.
     *
     * @return array
     */
    public function getDeathCounts(): array
    {
        if (is_null($this->death_counts)) {
            return [];
        }

        return $this->death_counts;
    }



    /**
     * Fill type of Ward request.
     */
    private function fillType()
    {
        $this->data['type'] = 'ward';
    }



    /**
     * Fill all shared information (province, city, university) based on hospital model.
     */
    private function fillValuesFromGivenHospital()
    {
        $hashid   = isset($this->data['hospital']) ? $this->data["hospital"] : 0;
        $hospital = model('registry-hospital')->grabHashid($hashid);

        $this->data['hospital_id'] = $hospital->id;
        $this->data['opu_id']      = $hospital->opu_id;

        if ($hospital->exists) {
            $this->data['city_id']       = $hospital->city_id;
            $this->data['province_id']   = $hospital->province_id;
            $this->data['university_id'] = $hospital->university_id;
            $this->model->university_id  = $hospital->university_id;

            unset($this->data["hospital"]);
        } else {
            $this->data['city_id']       = 0;
            $this->data['province_id']   = 0;
            $this->data['university_id'] = 0;
        }
    }



    /**
     * Remove death counts from request data.
     */
    private function removeDeathCounts()
    {
        if (isset($this->data['death_counts'])) {
            $death_counts = $this->data['death_counts'];

            foreach ($death_counts as $index => $death_count) {
                if (!isset($death_count['year']) or (empty($death_count['year']) and $death_count['year'] != 0)) {
                    unset($death_counts[$index]);
                    continue;
                }

                $death_counts[$index]['year'] = jdate(intval($death_count['year']))->format('Y');
            }

            $this->death_counts = $death_counts;

            unset($this->data['death_counts']);
        }
    }
}
