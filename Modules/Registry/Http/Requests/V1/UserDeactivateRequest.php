<?php

namespace Modules\Registry\Http\Requests\V1;

use App\Models\RegistryUnityRole;
use Modules\Yasna\Services\YasnaRequest;

class UserDeactivateRequest extends YasnaRequest
{
    protected $responder = 'white-house';



    /**
     * @inheritdoc
     */
    public function authorize()
    {
        // @TODO: implement a firm logic, that's a temporary solution
        $user_roles       = user()->unityRolesArray();
        $authorized_roles = [
             RegistryUnityRole::ROLE_EXPERT_IN_CHARGE,
             RegistryUnityRole::ROLE_DONOR_EXPERT,
             RegistryUnityRole::ROLE_OPU_MANAGER,
        ];

        if (user()->isSuperadmin() or user()->isDeveloper()) {
            return true;
        }

        return array_intersect($user_roles, $authorized_roles) != false;
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
             "user"    => $this->model->exists ? "" : "accepted",
             "restore" => "integer|in:0,1",
        ];
    }



    /**
     * @inheritdoc
     */
    public function messages()
    {
        return [
             "user.accepted" => trans("registry::validation.user_not_found"),
        ];
    }



    /**
     * @inheritdoc
     */
    public function corrections()
    {
        $this->resolveUser();
    }



    /**
     * resolve the user
     *
     * @return void
     */
    private function resolveUser()
    {
        $restore     = $this->input('restore', false);
        $this->model = model('user');
        if ($restore) {
            $this->model = $this->model->whereNotNull('deactivated_at');
        }
        $this->model = $this->model->grabHashid($this->input('hashid', '0'));
    }

}
